requirejs.config({
    urlArgs: "oso_Release=v0.0.1",
    paths:{
        "oso.bower":"/Sync10-ui/bower_components",
        "oso.lib":"/Sync10-ui/lib",
        "jquery":"/Sync10-ui/bower_components/jquery/dist/jquery.min",
        "jquery1.9":"/Sync10-ui/lib/jquery1.9/jquery-1.9.0",
        "jqueryui-css":"/Sync10-ui/bower_components/jqueryui/themes/base/all",
        "jqueryui":"/Sync10-ui/bower_components/jqueryui/ui",
        "jqueryui_min":"/Sync10-ui/bower_components/jqueryui/jquery-ui.min",
        "underscore":"/Sync10-ui/bower_components/underscore/underscore.min",
        "backbone":"/Sync10-ui/bower_components/backbone/backbone.min",
        "handlebars.runtime":"/Sync10-ui/bower_components/handlebars/handlebars.runtime.amd.min",
        "handlebars":"/Sync10-ui/bower_components/handlebars/handlebars.amd.min",
        "jsoneditor":"/Sync10-ui/bower_components/jsoneditor/jsoneditor.min",
        "highlightjs":"/Sync10-ui/bower_components/highlightjs/highlight.amd",
        "templates":"templates/templates.amd",
        "handlebars_ext":"/Sync10-ui/lib/handlebars_ext",
        "auto":"/Sync10-ui/lib/autocomplete/autocomplete",
        "blockui":"/Sync10-ui/lib/blockui/jquery.blockUI.233",
        "assembleUtils":"/Sync10-ui/lib/assembleUtils",
        "domready":"/Sync10-ui/bower_components/requirejs-domready/domReady",
	    "jstree":"/Sync10-ui/lib/jstree/dist/jstree.min",
        "jcrop":"/Sync10-ui/bower_components/jcrop/js/jquery.Jcrop.min",
        "validate":"/Sync10-ui/bower_components/jquery-validation/dist/jquery.validate",
        "completer":"/Sync10-ui/bower_components/completer/dist/completer.min",
        "ztree-css":"/Sync10-ui/bower_components/ztree/css/zTreeStyle/zTreeStyle",
        "ztree":"/Sync10-ui/bower_components/ztree/js/jquery.ztree.all-3.5.min",
        "ztree_exhide":"/Sync10-ui/bower_components/ztree/js/jquery.ztree.exhide-3.5",
        "select2":"/Sync10-ui/bower_components/select2/select2.min",      
        "artDialog":"/Sync10-ui/lib/art/plugins/oldartDialog",
        "darktooltip":"/Sync10-ui/lib/darktooltip/js/jquery.darktooltip.min",
        "ajaxFileUploader":"/Sync10-ui/lib/AjaxFileUploaderV2.1/ajaxfileupload",
        "showMessage":"/Sync10-ui/lib/showMessage",
        "jqtree":"/Sync10-ui/bower_components/jqtree/tree.jquery",
        "wrapToSelect":"/Sync10-ui/lib/wrap_select/wrap_js/wrapToSelect",
        "art_Dialog":"/Sync10-ui/lib/artDialog/src",
        "Paging":"/Sync10-ui/lib/Paging/Paging",
        "JSONTOHTML":"/Sync10-ui/lib/json2html/js/json2html",
        "json2htmlEvents":"/Sync10-ui/lib/json2html/js/jquery.json2html",
        "Font-Awesome":"/Sync10-ui/bower_components/fontawesome/css/font-awesome.min",
        "bootstrap-css":"/Sync10-ui/bower_components/bootstrap/dist/css",
        "bootstrap":"/Sync10-ui/bower_components/bootstrap/dist/js/bootstrap.min",
        "bootstrap_tooltip":"/Sync10-ui/bower_components/bootstrap/js/tooltip",
        "bootstrap_tab":"/Sync10-ui/bower_components/bootstrap/js/tab",
//        "multiselect":"/Sync10-ui/bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect",
        "metisMenu":"/Sync10-ui/lib/bootstrap/js/plugins/metisMenu/metisMenu",
        "CompondList":"/Sync10-ui/lib/CompondList/js",
        "immybox":"/Sync10-ui/lib/immybox-master/jquery.immybox",
        "mCustomScrollbar":"/Sync10-ui/lib/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min",
        "jquery-file-upload":"/Sync10-ui/bower_components/jquery-file-upload/js",
        "FileUpload":"/Sync10-ui/lib/jQueryFileUpload/js",
        "advancedSearch":"/Sync10-ui/lib/advancedSearch/js/advancedSearch",        
        "fancybox":"/Sync10-ui/bower_components/fancybox",
        "dateUtil":"/Sync10-ui/lib/DateUtil",
        "dateTimePicker":"/Sync10-ui/lib/jquery-ui-timepicker-addon",
        "iToggle":"/Sync10-ui/lib/iToggle-master/itoggle.jquery",
        "require_css":"/Sync10-ui/lib/require-css/css.min",
        "oso.demo.masterdetail": "/Sync10-ui/lib/demo_masterdetail/component",
        "nprogress": "/Sync10-ui/lib/nprogress/nprogress",
        "slidePanel": "/Sync10-ui/lib/slidePanel/js/slidePanel",
        "createBoxPanel": "/Sync10-ui/lib/slidePanel/js/createPanel",
        "compondBox": "/Sync10-ui/lib/compondBox/js/compondBox",
        "bootstrap.datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "bootstrap.datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "bootstrap3-wysiwyg-dist":"/Sync10-ui/bower_components/bootstrap3-wysiwyg/dist",        
        "rangy":'/Sync10-ui/bower_components/rangy-1.3/rangy-core',
        "bootstrap.wysihtml5":"/Sync10-ui/bower_components/bootstrap3-wysiwyg/dist/amd/bootstrap3-wysihtml5.all.min",
         "bootstrap-wysihtml5.en-US":"/Sync10-ui/bower_components/bootstrap3-wysiwyg/dist/locales/bootstrap-wysihtml5.en-US",
         "moment":"/Sync10-ui/bower_components/moment/min/moment.min",
         "fullcalendar":"/Sync10-ui/bower_components/fullcalendar/dist",
        "lhgdialog":"/Sync10-ui/lib/lhgdialog",        
        "jquery.browser":"/Sync10-ui/bower_components/jquery.browser/dist/jquery.browser.min",
        "fancyBox":"/Sync10-ui/bower_components/fancyBox/source/jquery.fancybox.pack",
        "fancyBox-thumbs":"/Sync10-ui/bower_components/fancyBox/source/helpers/jquery.fancybox-thumbs",
        "fancyBox-mousewheel":"/Sync10-ui/bower_components/fancyBox/lib/jquery.mousewheel-3.0.6.pack",
        "three": "/Sync10-ui/lib/threejs/three.min",
        "stats": "/Sync10-ui/lib/threejs/stats.min",
        "css3drenderer": "/Sync10-ui/lib/threejs/CSS3DRenderer",
        "tween": "/Sync10-ui/lib/threejs/tween.min",
		"strophe":"/Sync10-ui/lib/strophejs-1.2.0/examples/strophe"
    },
    shim: {
        'JSONTOHTML': {
            exports: 'json2html'
        },
        "json2htmlEvents":{
           exports: 'json2htmlEvents',
            deps:['jquery']  
        },
        'bootstrap': {
          deps: [
          "jquery",
          "require_css!bootstrap-css/bootstrap.min"
          ],
          exports: "$.fn.popover"
        },
        'metisMenu': {
            exports: 'metisMenu',
            deps:['jquery']
        },
        'mCustomScrollbar' : {
            deps:['jquery']
        },
        'immybox' : {
            deps:['jquery']
        },
        'ztree_exhide' : {
            deps:['ztree']
        },
        'bootstrap_tab' : {
            deps:['bootstrap']
        },
        "blockui" :{
        	deps:['jquery']
        },
        "dateTimePicker" :{
            deps:['jquery-ui']
        },
	   "wrapToSelect" :{
            deps:['jquery']
        },
        "lhgdialog/lhgcore.lhgdialog.min" :{
            deps:['jquery']
        },
        'fullcalendar': ['jquery'],
        'bootstrap.datetimepicker':{
         	deps:['jquery','require_css!bootstrap.datetimepicker-css']
		}  
    }
});


