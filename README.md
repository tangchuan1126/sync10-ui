VVME Sync10项目前端工程框架
========================


# 1. 培训材料参考


请先阅读本项目的Wiki页。


# 2. 前端工程使用方法



## 2.1 开发环境设置

前端开发环境需先安装以下工具。


### 2.1.1 Node.js

去[Node.js官网](http://nodejs.org)下载适合自己操作系统的安装包进行安装。

### 2.1.2 确保Git在命令行可访问

如果之前安装Git时，没有选中适当选项，需要设置PATH环境变量(例如，在Windows下，需将Git安装目录下的bin目录加入PATH环境变量)，使得git命令在命令行可以访问。

### 2.1.3 安装前端开发工具

在前面的设置已完成的前提下，打开一个命令窗口，执行以下命令：

	npm install -g grunt-cli
	npm install -g handlebars
	npm install -g bower
	
这一组命令将安装命令行工具:

- grunt：自动任务构建工具，本工程模板用于实现自动监视Handlebars模板的任务

- handlebars：Handlebars模板的预编译工具

- bower：前端资源包管理工具


## 2.2 建立页面目录

请每个参与Sync10前端开发的工程师，克隆本工程到自己开发环境的Tomcat安装目录下的webapps目录(并指定目录名称为首字母大写的`Sync10-ui`)：

	cd <Tomcat安装目录>/webapps
	git clone git@192.168.1.12:vvme/sync10-ui.git Sync10-ui
	
在克隆下来时，通常不要修改工程根目录下这些文件。
	
在开发每一个页面模块（SPA，Single-page Application，每一个SPA大体可对应于Sync10的Web用户界面上左侧导航栏中的一个菜单项）时，请在pages目录下建立一个页面模块的目录，如同工程中已包含的demo和resttool一样。并且每个页面模块目录下要有一个templates目录，用于专门放置本页面模块中视图层用到的Handlebars模板源文件和编译后的模板JS。

## 2.3 RequireJS全局配置

在工程根目录下的`requirejs_config.js`文件是用于设置全部页面模块共享的JavaScript开发依赖包的名称设置。其中，每个依赖包名称，映射到了一个带有相对路径的位置。**请注意，这个相对位置是针对`pages/<页面模块目录>`而言的。**

为了使用这些预定义的依赖包名称，需要在页面模块加载时引入。例如，在页面引入RequireJS处：

	<script type="text/javascript" 
			src="../../bower_components/requirejs/require.js" 
			data-main="page"></script>

在`page.js`中，以require方式加载页面应用的主模块代码`app.js`：

	require([
     	"../../requirejs_config", 
	], function(){   
    	require(["./app"]);
	});


## 2.4 模板的编译与使用

本工程自带自动监测Handlebars模板改动的工具，使用方法：在工程根目录下，执行命令

	grunt watch
	
自己开发的页面模块下的templates目录用于放置当前模块的视图模板，如果不使用上述监视工具或最初刚建立模板源文件时，可以进行手工编译，可采用Git Bash命令窗口执行如下：

	cd <tomcat安装目录>/webapps/Sync10-ui/pages/<页面模块>/templates
	handlebars *.handlebars -a -f templates.amd.js
	
在编写依赖自定义的模块的JS代码中，**注意：依赖中要声明Handlebars依赖，且在templates依赖之前**。例如如下所示：

	//在声明templates依赖之前，必须声明handlebars依赖，即使本模块没有直接使用Handlebars！
	define(['config','jquery','backbone','handlebars','templates','jqueryui/tabs'],
	function(config, $, Backbone, Handlebars, templates) {
		var MyView = Backbone.View.extend({
			el:"#myview",
			template:templates.myview,
			……
		});
	});



## 2.5 前端包管理工具的使用


本工程模板中已经预制了本项目常用的依赖包，包含：jquery,jqueryui,backbone,requirejs,handlebars等。如果需要引入其他依赖包，可以采用bower包管理工具来安装，例如，加入需引入[jsTree插件](http://www.jstree.com)：

	bower install jstree

以下命令会安装jstree默认版本（通常是指最新稳定版本）。

如果不是要安装默认版本，而是需安装某指定版本的，请先查看可用版本的版本标识，再在install命令行指定版本标识，例如handlebars目前稳定版本为1.3.0，但我们要使用2.0的版本，那么查找handlebars最新版本并安装的命令如下：

	C:\tomcat-7.0.47\Sync-ui>bower info handlebars
	……
	Available versions:
		- 2.0.0
  	……
  	C:\tomcat-7.0.47\Sync-ui>bower install handlebars
  	

## 2.6 前端开发环境


### 2.6.1 nginx

已经配置好Server端/Sync10等代理路径指向开发服务器的nginx绿色安装包（Windows）：

- [点此下载](http://192.168.1.15/~frank/nginx.zip)

下载后，解压到一个目录，**然后将Sync10-ui项目克隆到nginx目录下的html目录下**。


	cd  nginx-1.7.4/html
	git clone git@192.168.1.12:vvme/sync10-ui.git Sync10-ui

如果有需要，可以修改nginx目录下`conf/nginx.conf`，修改监听端口（默认为80），或增加新的反向代理设置。

- 启动nginx：执行命令`nginx.exe`

- 重启nginx：执行命令`nginx.exe -s reload`

- 停止nginx：执行命令`nginx.exe -s stop`

### 2.6.2 编辑工具

- [Sublime Text](http://www.sublimetext.com)


  