{
	"metadata": {
		"version": 4.3,
		"type": "Object",
		"generator": "ObjectExporter"
	},
	"geometries": [
		{
			"uuid": "CDD3F178-2F46-4601-B05D-47F4629E5135",
			"type": "BoxGeometry",
			"width": 400,
			"height": 100,
			"depth": 100,
			"widthSegments": 1,
			"heightSegments": 1,
			"depthSegments": 1
		}],
	"materials": [
		{
			"uuid": "FA35FDB1-594D-46DD-8525-903A72AAFC25",
			"type": "MeshPhongMaterial",
			"color": 16777215,
			"emissive": 0,
			"specular": 1118481,
			"shininess": 30
		}],
		"vertices": [ 0,0,0, 0,0,1, 1,0,1, 1,0,0 ],
	"object": {
		"uuid": "9C4FB0C1-33D1-4643-BD26-10E0A8E7A9DA",
		"type": "Mesh",
		"name": "Box 2",
		"geometry": "CDD3F178-2F46-4601-B05D-47F4629E5135",
		"material": "FA35FDB1-594D-46DD-8525-903A72AAFC25",
		"matrix": [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]
	}
}
