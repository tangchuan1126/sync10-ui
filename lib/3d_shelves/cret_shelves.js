(function(root, factory) {
    if (typeof define === 'function' && define.amd) {   

        // AMD. Register 
        define(['jquery', '3d_shelves'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'), require('three')(root));

    } else {
        // Browser globals
        //root===Window
        root.cret_shelwes = factory(root.$, root.THREE);
    }
}(this, function($) {
    'use strict';


    var shelves = function(options) {

        var __this = this;

        this.mouseXOnMouseDown = 0;
        this.windowHalfX = window.innerWidth / 2;
        this.windowHalfY = window.innerHeight / 2;
        this.targetRotationOnMouseDown = 0;
        this.targetRotation = 0;
        this.mouseX = 0;
        //---------------------------


        this.container;
        this.camera;
        this.scene;
        this.renderer;
        this.plane;
        this.cube;
        //this.mouse;
        //this.raycaster
        this.isShiftDown = false;
        this.rollOverMesh;
        this.rollOverMaterial;
        this.cubeGeo;
        this.cubeMaterial;
        this.group;

        //this.objects = [];


        if (options) {


            __this._int(options);

            window.setTimeout(function() {
                //运行--开始渲染
                __this.animate();
                //__this.render();

            }, 50);


        } else {

            console.log("no options");
        }

    }



    shelves.prototype = {

        addEvent: function(__this) {

            //var __this = shelves;
            //鼠标拖动事件
            document.addEventListener('mousedown', onDocumentMouseDown, false);

            //console.log(__this);


            function onDocumentMouseDown(event) {

                event.preventDefault();

                document.addEventListener('mousemove', onDocumentMouseMove, false);
                document.addEventListener('mouseup', onDocumentMouseUp, false);
                document.addEventListener('mouseout', onDocumentMouseOut, false);

                __this.mouseXOnMouseDown = event.clientX - __this.windowHalfX;
                __this.targetRotationOnMouseDown = __this.targetRotation;

            }

            function onDocumentMouseMove(event) {

                __this.mouseX = event.clientX - __this.windowHalfX;
                __this.targetRotation = __this.targetRotationOnMouseDown + (__this.mouseX - __this.mouseXOnMouseDown) * 0.02;

               // console.log(__this.targetRotation);

            }

            function onDocumentMouseUp(event) {
                //鼠标移入卸载
                //卸载时间
                document.removeEventListener('mousemove', onDocumentMouseMove, false);
                document.removeEventListener('mouseup', onDocumentMouseUp, false);
                document.removeEventListener('mouseout', onDocumentMouseOut, false);
            }

            function onDocumentMouseOut(event) {
                //鼠标移出时卸载

                document.removeEventListener('mousemove', onDocumentMouseMove, false);
                document.removeEventListener('mouseup', onDocumentMouseUp, false);
                document.removeEventListener('mouseout', onDocumentMouseOut, false);

            }



            //改变窗口时，起到缩放
            window.addEventListener('resize', onWindowResize, false);



            function onWindowResize() {

                //  var __this = this.prototype;


                __this.windowHalfX = window.innerWidth / 2;
                __this.windowHalfY = window.innerHeight / 2;

                __this.camera.aspect = window.innerWidth / window.innerHeight;
                __this.camera.updateProjectionMatrix();


                __this.renderer.setSize(window.innerWidth, window.innerHeight);

            }

        },
        _int: function(options) {

            var __this = shelves.prototype;
            //容器
            if (options.jqbox && options.jqbox != "" && options.jqbox != "undefined") {

                __this.container = $(jqbox)[0];

            } else {
                __this.container = document.createElement('div');
                __this.container.id="shelvesbox";
                document.body.appendChild(__this.container);
            }

            //设置相机--set--整个body-45度
            __this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
            __this.camera.position.set(500, 800, 1300);
            __this.camera.lookAt(new THREE.Vector3());

            //---------end

            //场景
            __this.scene = new THREE.Scene();

            //####--组
            __this.group = new THREE.Group();


            //####把组加到场景内
            __this.scene.add(__this.group);


            //创建-物体---箱子---set
            __this.cubeGeo = new THREE.BoxGeometry(50, 50, 50);
            //创建4方体
            __this.cubeMaterial = new THREE.MeshLambertMaterial({
                color: 0xfeb74c,
                map: THREE.ImageUtils.loadTexture("square-outline-textured.png")
            });


            var Longitudinal_y = 0; //往高层
            var Depth_z = 0; //往下方排
            var Lateral_x = 0; //往前排




            //往前推排 ---1排
            function y_x_Axis(layers, x_ture, z_ture) {


                for (var z_val = 0; z_val < z_ture; z_val++) {

                    if (z_val > 0) {
                        Longitudinal_y = 0;
                        Lateral_x = 0;
                        if (Depth_z == 0) {
                            Depth_z = 50;
                        }


                    }



                    for (var xval = 0; xval < x_ture; xval++) {

                        //进入第二排
                        if (xval > 0) {
                            Longitudinal_y = 0;
                            if (Lateral_x == 0)
                                Lateral_x = 50;

                        }
                        // Lateral_x=Lateral_x * xval;

                        Column(layers, Lateral_x, xval, Depth_z, z_val);
                    }

                }



            }

            //堆出1高柱
            function Column(layers, Lateral_x, xval, Depth_z, z_val) {


                for (var c = 0; c < layers; c++) {

                    if (c > 0) {

                        if (Longitudinal_y == 0)
                            Longitudinal_y = 50;

                    }

                    //画出物体
                    var voxel = new THREE.Mesh(__this.cubeGeo, __this.cubeMaterial);
                    //写入位置
                    voxel.position.set(Depth_z * z_val, Longitudinal_y * c, Lateral_x * xval);

                    //添加物体到场景
                    //__this.scene.add(voxel);

                    //###把方体放入组内
                    __this.group.add(voxel);

                }


            }

            //options.c层, options.g格,options.p排
            y_x_Axis(options.c, options.g, options.p);

            //-----------end           


            // grid---画出场景网格set每格step--50
            //每隔50画格

            var size = 500,
                step = 50;

            var geometry = new THREE.Geometry();

            for (var i = -size; i <= size; i += step) {

                geometry.vertices.push(new THREE.Vector3(-size, 0, i));
                geometry.vertices.push(new THREE.Vector3(size, 0, i));

                geometry.vertices.push(new THREE.Vector3(i, 0, -size));
                geometry.vertices.push(new THREE.Vector3(i, 0, size));

            }

            //网格的线--效果
            var material = new THREE.LineBasicMaterial({
                color: 0x000000,
                opacity: 0.2,
                transparent: true
            });
            //画出线
            var line = new THREE.Line(geometry, material, THREE.LinePieces);
            //写入场景
            //__this.scene.add(line);

            //###把网格放入组内
            __this.group.add(line);

            //画出场景网格--end

            //灯光

            // __this.raycaster = new THREE.Raycaster();
            // __this.mouse = new THREE.Vector2();

            //获取网格坐标？保存到objects中

            //  var geometry = new THREE.PlaneBufferGeometry(1000, 1000);
            //  geometry.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));

            //  __this.plane = new THREE.Mesh(geometry);
            //  __this.plane.visible = false;

            //  //__this.scene.add(__this.plane);

            //   //###把放入组内
            // __this.group.add(__this.plane);


            // Lights---环境光

            var ambientLight = new THREE.AmbientLight(0x606060);
            __this.scene.add(ambientLight);
            //###把放入组内
            //__this.group.add(ambientLight);


            //定向光
            var directionalLight = new THREE.DirectionalLight(0xffffff);
            directionalLight.position.set(1, 0.75, 0.5).normalize();
            __this.scene.add(directionalLight);

            //###把放入组内
            // __this.group.add(directionalLight);


            __this.renderer = new THREE.WebGLRenderer({
                antialias: true
            });

            __this.renderer.setClearColor(0xf0f0f0);
            __this.renderer.setPixelRatio(window.devicePixelRatio);
            __this.renderer.setSize(window.innerWidth, window.innerHeight);
            __this.container.appendChild(__this.renderer.domElement);


            __this.addEvent(this);

        },
        render: function() {

            var __this = this;

            __this.group.rotation.y += (__this.targetRotation - __this.group.rotation.y) * 0.05;
            __this.renderer.render(__this.scene, __this.camera);

        },
        animate: function() {

            var __this = this;

            //启动后一直在播放--转动时也会播放
            requestAnimationFrame(function() {
                __this.animate();
            });

            __this.render();
        }

    };


      return shelves;



}));
