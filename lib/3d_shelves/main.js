requirejs.config({
    paths: {
        "3d_shelves": "/Sync10-ui/lib/3d_shelves/three.min",
        "cret_shelves":"/Sync10-ui/lib/3d_shelves/cret_shelves",
        "jquery": "/Sync10-ui/bower_components/jquery/dist/jquery.min"
    }
});


define(['jquery', 'cret_shelves'], function($,shelves) {

    $(function() {

        $(".certform input").click(function(e) {

            e.stopPropagation();
            e.preventDefault();
            $(this).focus();

        });

        $("#cretshelves").click(function(event) {

            var cval = $("#c").val();
            var gval = $("#g").val();
            var pval = $("#p").val();

            if (/\d/.test(cval) && /\d/.test(gval) && /\d/.test(pval)) {

                cval = window.parseInt(cval);
                gval = window.parseInt(gval);
                pval = window.parseInt(pval);

                if (cval > 0 && gval > 0 && pval > 0) {

                    var shelvesbox = $("#shelvesbox");
                    if (shelvesbox.length > 0) {

                        shelvesbox.remove();
                    }


                    new shelves({
                        jqbox: "",
                        c: cval,
                        g: gval,
                        p: pval
                    });

                } else {

                    return false;

                }


            } else {
                return false;
            }


        });


        //c层,g格,p排
        new shelves({
            jqbox: "",
            c: 5,
            g: 1,
            p: 2
        });


    });

});
