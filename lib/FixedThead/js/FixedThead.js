;
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register 
    define(['jquery', 'oso.lib/css3.0_demo/ExpandingOverlayEffect/js/modernizr.custom'], factory);
  } else if (typeof module !== 'undefined' && module.exports) {
    // CommonJS
    module.exports = factory(require('jquery'), require('modernizr.custom'));

  } else {
    // Browser globals
    root.Magnifier = factory(root.$, root.Modernizr);
  }
}(this, function($, Modernizr, art_Dialog) {
  'use strict';

  $.fn.fixedThead = function() {
    var currentTable = this;
    currentTable.parent().css("overflow-y", "auto");
    var _thead = currentTable.eq("0").find("thead");
    var tableth = _thead.html();
    var style = _thead.attr("style");
    var className = _thead.attr("class");

    var parentObj = currentTable[0].parentNode;

    //console.log(parentObj);
    var width = parentObj.nodeName == "BODY" ? $(window).width() : currentTable.width();

    var topthead = $("<div><table style='width:100%;height:100%;'><thead style='width:100%;'>" + tableth + "</thead></table></div>");

    topthead.attr("style", style);
    topthead.addClass(className);
    //topthead.attr("class",className);

    var css = {
      width: width + "px",
      "display": "none",
      position: "fixed",
      top: ($(parentObj).position().top) + "px",
      left: ($(parentObj).position().left) + parseInt(currentTable.css("margin-left")) + "px",
    };
    topthead.css(css);
    parentObj.nodeName == "BODY" ? $("body").append(topthead) : currentTable.append(topthead);
    var scrollObj = parentObj.nodeName == "BODY" ? window : parentObj;
    $(scrollObj).scroll(function(event) {
      /* Act on the event */
      var toppx = $(this).scrollTop();
      var _toppx=currentTable.position().top + _thead.height();
      if (toppx > _toppx) {
        topthead.css("display", "block");
      } else {
        topthead.css("display", "none");
      }
      
    });
    if(parentObj.nodeName != "BODY"){
      $(scrollObj).scroll(function(event){

      });
      $(window).scroll(function(event){
        topthead.css("left",(($(parentObj).position().left) - $(window).scrollLeft()) + parseInt(currentTable.css("margin-left")) + "px");
        topthead.css("top",(($(parentObj).position().top) - $(window).scrollTop()) + "px");
      });
    }
    
    window.onresize = function(){
      var wh = parentObj.nodeName == "BODY" ? $(window).width() : currentTable.width();
      topthead.css("width",wh + "px");
    };
  };
}));