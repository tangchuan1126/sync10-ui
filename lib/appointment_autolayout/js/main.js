requirejs.config({
    paths: {
        "oso.bower": "/Sync10-ui/bower_components",
        "oso.lib": "/Sync10-ui/lib",
        "require_css": "/Sync10-ui/lib/require-css/css.min",
        "jquery1.9": "/Sync10-ui/lib/appointment_autolayout/js/jquery-latest.min",
        "jquery": "/Sync10-ui/lib/appointment_autolayout/js/jquery-latest.min",
        "fullPage": "/Sync10-ui/lib/appointment_autolayout/js/fullPage",
        "select2": "/Sync10-ui/lib/appointment_autolayout/js/select2.full",
        "moment": "/Sync10-ui/bower_components/moment/min/moment.min",
        "iscroll": "/Sync10-ui/lib/appointment_autolayout/js/iscroll",
        "smalot-bootstrap-datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "datetimepicker-plus": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "Font-Awesome": "/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min",
        "bootstrap-css": "/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min",
        "select2-css": "/Sync10-ui/lib/appointment_autolayout/css/select2.min",
        "datetimepicker-css": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "smalot-datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "datetimepicker-plus-css": "/Sync10-ui/lib/appointment_autolayout/css/bootstrap-datetimepicker-plus",
        "default-css": "/Sync10-ui/lib/appointment_autolayout/css/default",
        "mCustomScrollbar": "/Sync10-ui/lib/appointment_autolayout/js/jquery.mCustomScrollbar.concat.min"
    },
    shim: {
        // "abc2": {
        //     deps: ["require_css!abc2_css"]
        // }
    }
});


var browserv = navigator.userAgent;
var iebrowsev = /MSIE/.test(browserv);

var liball = [
    "jquery",
    "select2",
    "moment",
    "datetimepicker-plus",
    "smalot-bootstrap-datetimepicker",
    "iscroll",
    "fullPage"
];

if (iebrowsev) {
    liball.push("mCustomScrollbar");
}

require(liball, function(jq_, select2, moment, Acars_datetimepicker, smalot_datetimepicker, _iscroll, _fullPage) {

    $(function() {




        var cssall = [
            "require_css!Font-Awesome",
            "require_css!bootstrap-css",
            "require_css!default-css",
            "require_css!select2-css",
            "require_css!datetimepicker-css",
            "require_css!smalot-datetimepicker-css",
            "require_css!datetimepicker-plus-css"
        ]



        if (iebrowsev) {
            cssall = [];
        }

        require(cssall, function() {

            var timeouts = 0;
            if (iebrowsev) {
                timeouts = 400;
            }

     var ua = window.navigator.userAgent;
     var Mobiles=/Android/i.test(ua) || /Mobile/i.test(ua) || /X11;/i.test(ua);
    


            var unslider_data = "";
            var header = $("#header");
            var footer = $("#footer");
            //页号
            var pageindex = 0;
            var left_btn = header.find('.fa-chevron-left');
            var right_btn = header.find('.fa-chevron-right');
            var toptitle = header.find('.pull-left .panel_title');

            //左右导航按钮
            var Step5scrll = true,
                Step3scrll = true,
                Step2scrll = true;

            var page_index = 0;
            var IScroll_Slidcontainer = "",
                IScroll_Step3_body_box = "",
                IScroll_Step5body_box = "";

            setTimeout(function() {

                unslider_data = new FullPage({
                    id: 'apphome_body',
                    slideTime: 800,
                    effect: {
                        transform: {
                            translate: 'X'
                        },
                        opacity: [0, 1]
                    },
                    mode: 'wheel, touch',
                    easing: 'ease',
                    Initial: function() {
                        $("#Loading").removeClass('hide');
                    },
                    callback: function(indexNow, pageno) {

                        page_index = indexNow; 

                        if(!Step2scrll && indexNow == 3 && typeof IScroll_Slidcontainer!="undefined") {
                          IScroll_Slidcontainer.refresh();                           
                        }  


                        if(indexNow == 4 && !Step3scrll && typeof IScroll_Step3_body_box!="undefined") {
                          IScroll_Step3_body_box.refresh();                           
                        } 

                         if(indexNow == 6 && !Step5scrll && typeof IScroll_Step5body_box!="undefined") {
                          IScroll_Step5body_box.refresh();                           
                        }                    
                  


                        //=============== iscroll 1=========================



                        if (indexNow == 3 && Step2scrll) {


                       

                            Step2scrll = false;
                            var Slidcontainer = $(".Slidcontainer");

                            var Slidecard_box = Slidcontainer.find(".Slidecard_box");

                            var sub_panel = Slidecard_box.find(".panel");
                            var panel_w = 200;
                            if (sub_panel.eq(0).width() > 0) {
                                panel_w = sub_panel.eq(0).width();
                            }
                            var scrollw = (panel_w * sub_panel.length) + 60;

                            Slidecard_box.width(scrollw);

                            Slidecard_box.removeClass('hide');


                            if (!iebrowsev) {

                                IScroll_Slidcontainer = new IScroll('.Slidcontainer', {
                                    scrollX: true,
                                    scrollY: false,
                                    mouseWheel: true
                                });

                            } else {

                                Slidcontainer.mCustomScrollbar({
                                    axis: "x"
                                });
                            }
                        

                        }

                        //=============== iscroll 2=========================

                        if (indexNow == 4 && Step3scrll) {


                            Step3scrll = false;
                            var clicks=false;
                            if(Mobiles){
                               clicks=true;
                            }

                            var Step3_box = $(".Step3_body_box");

                            if (!iebrowsev) {
                                IScroll_Step3_body_box = new IScroll('.Step3_body_box', {
                                    scrollX: true,
                                    scrollY: false,
                                    mouseWheel: true,
                                    click:clicks
                                });
                            } else {

                                Step3_box.mCustomScrollbar({
                                    axis: "x"
                                });

                            }

                            var picker12=$('#datetimepicker12');
                            picker12.Acars_datetimepicker({
                                inline: true,
                                icons: {
                                    previous: 'fa fa-chevron-circle-left',
                                    next: 'fa fa-chevron-circle-right'
                                },
                                toolbar: false,
                                format: 'YYYY-MM-DD',
                                tdcellclick: function(dates) {
                                
                                var maxScrollX=window.parseFloat("-"+ (picker12.width() +7));
                                   IScroll_Step3_body_box.scrollTo(maxScrollX,0);

                                },
                                Initialization: function(setbuttondom) {

                                    setbuttondom.show();
                                    setbuttondom.click(function() {
                                        //console.log(2233444);
                                    });

                                }

                            });


                        }


                        if (indexNow == 6 && Step5scrll) {

                            var Step5body_box_h = $("#apphome_body").height();

                            var step5_box_h = Step5body_box_h - 110;
                            $(".Step5body_box").height(step5_box_h).removeClass('hide');

                            Step5scrll = false;
                            if (!iebrowsev) {
                                IScroll_Step5body_box = new IScroll(".Step5body_box");
                            } else {
                                $(".Step5body_box").mCustomScrollbar();
                            }

                        }


                    }
                });

                //进入登录
                unslider_data.next();






            }, timeouts);


           var psng=false;
           $(window).resize(function(event) {
               psng=true;
           });


            //登录
            $("button:submit").click(function(e) {
                e.preventDefault();
                unslider_data.next();
                header.removeClass('hide');
                pageindex = 0;
            });



            header.find(".fa-chevron-right,.fa-chevron-left").click(function(e) {

                var this_ = $(this);
                //right-btn
                var is_right = this_.hasClass('fa-chevron-right');
                if (is_right && pageindex < 4) {

                    unslider_data.next();
                    pageindex++;

                }

                //left-btn
                var is_left = this_.hasClass('fa-chevron-left');
                if (is_left && pageindex > 0) {

                    unslider_data.prev();
                    pageindex--;
                }







                //===================隐藏============================

                if (pageindex < 1)
                    left_btn.addClass('hide');



                if (pageindex != 1) {

                    footer.addClass('hide').find('#footef_Step1').addClass('hide');
                }

                if (pageindex != 4) {

                    footer.addClass('hide').find('#footef_Step3').addClass('hide');
                    right_btn.removeClass('hide');
                }


                //===================显示============================

                if (pageindex == 1) {
                    footer.removeClass('hide').find("#footef_Step1").removeClass('hide');
                    left_btn.removeClass('hide');
                }

                if (pageindex == 4) {

                    footer.removeClass('hide').find('#footef_Step3').removeClass('hide');
                    right_btn.addClass('hide');
                }


                //===================头部标题============================
                toptitle.html("Stpe " + pageindex + ".");



                return false;

            });



        });
        //css end
    });
    //window.onload end

});
//js end
