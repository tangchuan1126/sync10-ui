/*
 * Pallets.js
 * Copyright 2014, Connor
 * http://127.0.0.1/Sync10-ui/ilb/PalletsControls/
 */
;
(function(root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register 
		define(['jquery', 'mCustomScrollbar'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		module.exports = factory(require('jquery'), require('mCustomScrollbar'));

	} else {
		// Browser globals
		root.exports = factory(root.jquery, root.mCustomScrollbar);
	}
}(this, function($, mCustomScrollbar) {
	'use strict';

	var _public={};	

	function PalletsPlugin(opts) {

		if (!(this instanceof PalletsPlugin))
			return new PalletsPlugin(opts);

		if (typeof opts === 'string')
			opts = {
				renderTo: opts
			};


		var dataname = "_Pallets_" + $.now();
	     opts._Dom = $('<div id="' + dataname + '"></div>');

		if (opts.renderTo && opts.renderTo != "" && typeof opts.renderTo != "undefined") {
			dataname = opts.renderTo.replace(/\.|\/|#/g, "");
			var _todombox = $(opts.renderTo);
			if (_todombox.length > 0) {
				opts._Dom = _todombox;
			}
		}

		opts.jsondataid=dataname;
		this._opts = opts;

		Componbox.__int(this._opts);

		//__allfn里的一体
		var __allfn = {
			
			on: _public.On,
			render: _public.Render,
			command: _public.Command,
			_opts:this._opts
		}

		return __allfn;

	};


	
	//命令回调
	_public.Command = function(comsing,val) {

		var _comp = Componbox;
		if (_comp[comsing]) {
			var _opts = this._opts;
			return _comp[comsing](_opts,val);
		}

	};


	_public.Render = function() {

		var _opts = this._opts;
		if (_opts._Dom.length > 0) {
			_opts._Dom.show();
		} else {

			var errorobj = {
				txt: "没有容器",
				error: ""
			};
			Componbox.error(_opts, errorobj);

		}

	}

	_public.On = function(events, callback) {

		//写入用户事件
		var _opts = this._opts;
		if (events != "" && typeof events != "undefined") {

			var onevents = Componbox.ONEVENTS[_opts.jsondataid];

			onevents[events] = callback;

		}

	};



	var Componbox = {
		ConfingDATA: {},
		//事件表
		ONEVENTS: {},
		//控件数据表
		JSONDATA: {},
		__int:function(_opts){

		var _comp = Componbox;		
		if (!_comp.ONEVENTS[_opts.jsondataid]) {

			_comp.ONEVENTS[_opts.jsondataid] = {};

		};

		if (!_comp.ONEVENTS[_opts.jsondataid]) {

			_comp.ONEVENTS[_opts.jsondataid] = {};

		}

		_opts._Dom.hide();
		_comp.loadingdata(_opts, "createboxhtml");
		_comp.createcss(_opts);


		},
		error: function(opts, errorobj) {

			if (opts.error) {
				opts.error(errorobj);
			} else {

				console.log(errorobj.txt);
				//alert(errorobj.txt);
			}

			return false;

		},
		GetData:function(_opts,id){
          
          var dataname = _opts.jsondataid;
		  var _data=Componbox.JSONDATA[dataname];

		  if(id && typeof id!="undefined" && id!=""){
            _data=_data.filter(function(dataitme) {
            return dataitme.id == id;
            });
		  }

			return _data;
		},
		ALLchecked:function(_opts,check){

			var _Dom=_opts._Dom;
			var checkedinput=_Dom.find(":checkbox");
			var dataname = _opts.jsondataid;
			var _data=Componbox.JSONDATA[dataname];
            checkedinput.prop("checked",check);
         if(check){
          return {checked:checkedinput,data:_data};
          }else{
          	return {checked:checkedinput,data:""};
          }

		},
		loadingdata: function(_opts, _fn) {

			var _dataurl = _opts.dataUrl;
			var dataname = _opts.jsondataid;

		 if(_dataurl.url){
          _dataurl=_dataurl.url;
         } 
			//------------data----- 
			if (typeof _dataurl == "string" && typeof _dataurl != "undefined") {
				var paramdata = {};
				if (_opts.postData) {
					paramdata = _opts.postData;
				}

				$.getJSON(_dataurl, paramdata, function(json) {

			Componbox.JSONDATA[dataname] = json;
              if(_opts.dataUrl.formatjson){
              Componbox.JSONDATA[dataname] = _opts.dataUrl.formatjson(json);
             }

					Componbox[_fn](_opts);


				}).error(function(e) {

					var errorobj = {
						txt: "服务端请求失败",
						error: e
					};
					Componbox.error(_opts, errorobj);

				});

			} else if (typeof _dataurl == "object" && !_opts.dataUrl.url) {

				Componbox.JSONDATA[dataname] = _dataurl;
				_opts.dataUrl = null;
				Componbox[_fn](_opts);

			} else {

				var errorobj = {
					txt: "数据无效",
					error: ""
				};
				Componbox.error(_opts, errorobj);

			}

		},
		tabledataurl: function(opts, id, table) {

			var tdloding = table.find('tbody tr td');
			tdloding.html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
			var _dataurl = opts.tabledataurl;	

		  if(_dataurl.url){
             _dataurl=_dataurl.url;
          }

			//------------data----- 
			if (typeof _dataurl == "string" && typeof _dataurl != "undefined") {

				_dataurl = _dataurl.replace(/\{.*\}/, "");

				$.getJSON(_dataurl, id, function(json) {

					var __json=json;
					if(opts.tabledataurl.formatjson){
                       __json = opts.tabledataurl.formatjson(json);
					}


					Componbox.createtable(opts, table, __json, id);
				}).error(function(e) {

					var errorobj = {
						txt: "服务端请求失败",
						error: e
					};
					Componbox.error(opts, errorobj);

				});

			} else if (typeof _dataurl == "object" && !opts.tabledataurl.url) {
				Componbox.createtable(opts, table, _dataurl, id);

			} else {

				var errorobj = {
					txt: "数据无效",
					error: ""
				};
				Componbox.error(opts, errorobj);

			}

		},
		createtable: function(opts, table, data, id) {
			//创建单个托盘表格
			var pan_allcheck = "";
			Componbox.newitmestable(data, table, pan_allcheck);
			//第二个参数不绑定开合，title等事件
			Componbox._addevents(opts, false);

			//要和表头数据合并       
			var palletsitmedata = Componbox.JSONDATA[opts.jsondataid];
			for (var keys in palletsitmedata) {
				var itmes = palletsitmedata[keys];
				if (itmes.id == id) {
					itmes.table = data.table;
					break;
				}
			}

		},
		createcss: function(opts) {


			var cssfileall = [
				"require_css!bootstrap-css/bootstrap.min",
				"require_css!Font-Awesome",
				"require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css",
				"require_css!oso.lib/PalletsControls/css/style.css"
			];

			require(cssfileall);

		},
		createboxhtml: function(_opts) {

			//数据完成

			var _this = Componbox;
			var p_dom = _opts._Dom;
			var JSONDATA = _this.JSONDATA[_opts.jsondataid];

			var palletall = "";

			var results = JSONDATA.map((function() {

				return function(itmes) {

					var btnstyle = " fa-toggle-up ";
					var tableshowan = "",
						borderbottomw = "";
					if (itmes.open == "false" || typeof itmes.table == "string" || !itmes.table) {
						btnstyle = " fa-toggle-down ";
						tableshowan = " tablehide";
						borderbottomw = " borderbottomw0px";
					}

					var colstyle = "col-md-3 Palletsbox";
					if (_opts.colstyle) {

						if (Number(_opts.colstyle)) {
						  colstyle = "col-md-3 Palletsbox" + "'" + " style='width:" + _opts.colstyle + "px";
						} else {
						  colstyle = _opts.colstyle + " Palletsbox";
						}
					}

					var tableboxstyleh = "";
					if (_opts.scrollH) {
						tableboxstyleh = ' style="max-height:' + _opts.scrollH + 'px;height:' + _opts.scrollH + 'px"';
					}

					var itmediv = $("<div class='" + colstyle + "'><div class='pallets_title" + borderbottomw + "'><ul></ul></div><div class='Palletstablebox" + tableshowan + "'" + tableboxstyleh + "><table class='table table-bordered table-striped'><thead></thead><tbody><tr><td class='tdnodata'>无数据</td></tr></tbody></table></div></div>");

					var toptitle = itmediv.find('.pallets_title ul');


					//topli				        
					var topli = '<li class="closing"><span><i class="fa' + btnstyle + 'fa-fw"></i></span></li>';

					//title
					var titlecoent = "";
					for (var titlekey in itmes.title) {
						var titleitme = itmes.title[titlekey];
						if(titleitme=="\n"){
							titlecoent +="<div></div>";
						}else{
						titlecoent += '<span>' + titleitme + '</span>';
					   }
					}

					//单个头部checkbox
					var titlecheckbox = "",
						pan_allcheck = "";

					if (typeof itmes.titlecheckbox != "undefined") {

						if (itmes.titlecheckbox == "true") {
							pan_allcheck = " checked";
						}

						titlecheckbox = '<span><input type="checkbox"' + pan_allcheck + '/></span>';
					}

					var topcolnet = '<li><div class="toptitlebody">' + titlecheckbox + titlecoent + '</div></li>';

					toptitle.html(topli + topcolnet);

					if (itmes.table && typeof itmes.table == "object") {
						_this.newitmestable(itmes, itmediv, pan_allcheck);
					}

					var idsring = "pallets_" + itmes.id + "_" + $.now();
					itmediv.attr("id", idsring);
					return itmediv[0].outerHTML;
				};
			})(this));

			if (results.length > 0) {
				var _i = 1;
				for (var p_html in results) {
					//如果有要求固定列数时
					if (_opts.columns) {

						var sorwr = parseInt(_i % _opts.columns);

						if (sorwr == 0 && _i > 1) {
							palletall += results[p_html] + "</div><div class='row'>";
						} else {
							palletall += results[p_html];
						}

						_i++;


					} else {

						palletall += results[p_html];

					}

				}
			}

			p_dom.html("<div class='row'>" + palletall + "</div>").addClass('rownomargin');

			//处理半选状态
			var __Palletsbox = p_dom.find(".Palletsbox");
			for (var __Pkey = 0; __Pkey < __Palletsbox.length; __Pkey++) {
				var Pallets_box = __Palletsbox.eq(__Pkey);

				var tablebody_box = __Palletsbox.find("tbody");

				var _checkedlen = tablebody_box.find(':checked').length;
				var _checkboxlen = tablebody_box.find(':checkbox').length;

				if (_checkboxlen != _checkedlen && _checkedlen > 0) {
					var inputall_ = Pallets_box.find(".toptitlebody :checkbox");
					inputall_.prop("indeterminate", true);
					var thcheckbox = Pallets_box.find(".thcheckbox");
					thcheckbox.prop("indeterminate", true);
				}

			}
			//处理半选状态 end 

			Componbox._addevents(_opts, true);

			if (_opts.scrollH) {

				var _Palletstablebox = p_dom.find('.Palletstablebox');
				_Palletstablebox.mCustomScrollbar({
					axis: "y",
					scrollInertia: _opts.scrollH,
					scrollbarPosition: "outside"
				});

				_Palletstablebox.find(".mCSB_scrollTools").css("right", "-10px");

			}

			//事件
			var __onevents = Componbox.ONEVENTS[_opts.jsondataid];

			if (__onevents["tohtml"] && !_opts.renderTo) {
				var _html = p_dom.html();
				__onevents["tohtml"](_html);
			}

			Componbox._autoheigth(_opts);

		},
		newitmestable: function(itmes, itmediv, pan_allcheck) {

			var tbodycheckbox = "";
			var thead = itmediv.find("thead");
			var tbody = itmediv.find("tbody");

			//table
			var theaddata = itmes.table.thead;
			var tbodydata = itmes.table.tbody;
			var theadhtml = "";
			var tdlen = theaddata.length;
			for (var tablekey in theaddata) {

				var headth = theaddata[tablekey];

				if (tablekey == 0 && headth == "true" || headth == "false") {

					if (headth == "true" && pan_allcheck == "" || typeof pan_allcheck == "undefined") {
						pan_allcheck = " checked";
					}

					if (headth == "false" && pan_allcheck != "") {
						pan_allcheck = "";
					}

					theadhtml = '<th class="p_5px"><input class="thcheckbox" type="checkbox"' + pan_allcheck + '/></th>';
					tbodycheckbox = '<input type="checkbox"' + pan_allcheck + '/>';

				}

				if (tablekey == 0 && headth != "true" && headth != "false") {

					theadhtml = '<th>' + headth + '</th>';
					tbodycheckbox = "";

				}

				if (tablekey > 0) {

					theadhtml += '<th>' + headth + '</th>';
				}

			}



			thead.html(theadhtml);

			var tbodyhtml = "";
			for (var tbodykey in tbodydata) {

				var trid = tbodydata[tbodykey].id;
				var tbodyitme = tbodydata[tbodykey].td;
				var tbodytrhtml = "";
				//一行的遍历
				for (var td_ = 0; td_ < tbodyitme.length; td_++) {

					var tditmedata = tbodyitme[td_];
					//第一列0--按自己判断
					if (typeof tbodycheckbox != "undefined" && td_ == 0 && tbodycheckbox != "" && tditmedata != "true" && tditmedata != "false") {

						tbodytrhtml = '<td class="p_5px">' + tbodycheckbox + '</td><td>' + tditmedata + '</td>';

					}

					//第一列1--按thead判断
					if (td_ == 0 && tditmedata == "true" || tditmedata == "false") {

						var _checked = "";

						if (tditmedata == "true" && !itmes.titlecheckbox) {
							_checked = " checked";
						} else {

							if (itmes.titlecheckbox == "true") {
								_checked = " checked";
							}

						}

						var tdcheckbox = '<input type="checkbox"' + _checked + '/>'

						if (tbodycheckbox != "" && typeof tbodycheckbox != "undefined") {

							tdcheckbox = tbodycheckbox;

						}

						tbodytrhtml = '<td class="p_5px">' + tdcheckbox + '</td>';

					}

					//第一列2--按thead判断

					if (td_ == 0 && tbodycheckbox == "") {

						tbodytrhtml = '<td>' + tditmedata + '</td>';

					}

					if (td_ > 0) {
						tbodytrhtml += '<td>' + tditmedata + '</td>';
					}
				}
				tbodyhtml += '<tr id="tritme_' + trid + '">' + tbodytrhtml + '</tr>';
			}
			tbody.html(tbodyhtml);

		},
		titlecheckboxall: function(_opts) {
			//取回所有勾选的title-checkbox
			var _dom = _opts._Dom;
			var _this = Componbox;
			var toptitlebody = _dom.find(".toptitlebody");
			var _Palletdata = [],
				Palletdomid = [];
			for (var __i = 0; __i < toptitlebody.length; __i++) {
				var itmebody = toptitlebody.eq(__i);
				var titlecheck = itmebody.find(":checked");
				if (titlecheck.length > 0) {
					var Palletsbox = titlecheck.parents(".Palletsbox").eq(0);
					Palletdomid.push(Palletsbox.attr("id"));
					_Palletdata.push(_this.itmedata(_opts, Palletsbox)[0]);
				}

			}

			return {
				Palletdata: _Palletdata,
				Palletdom: Palletdomid
			};
		},
		theadcheckboxall: function(_opts) {
			//取回所有勾选的thead-checkbox
			var _dom = _opts._Dom;
			var _this = Componbox;
			var jsondata = _this.JSONDATA[_opts.jsondataid];
			var table = _dom.find(".table-bordered");
			var filertable = [],
				itmejson = [];

			for (var i = 0; i < table.length; i++) {
				var itmetable = table.eq(i);
				var thadcheck = itmetable.find("th").eq(0).find(":checked");
				if (thadcheck.length > 0) {
					var Palletsbox = thadcheck.parents(".Palletsbox").eq(0);
					var id_Palletsbox=Palletsbox.attr("id");
					filertable.push(id_Palletsbox);
					itmejson.push(_this.itmedata(_opts, Palletsbox)[0]);
				}
			}

			return {
				Pallettable: itmejson,
				Palletdom: filertable
			};

		},
		tbodycheckboxall: function(_opts) {
			//取回所有勾选的tbody-itme-checkbox
			var _dom = _opts._Dom;
			var _this = Componbox;
			var jsondata = _this.JSONDATA[_opts.jsondataid];
			var table = _dom.find(".table-bordered");

			var itmejsonall = [],trdomidall=[];

			for (var i = 0; i < table.length; i++) {
				var itmetable = table.eq(i);

				var tbodycheckall = itmetable.find("tbody").find(":checked");

				if (tbodycheckall.length > 0) {

					var Palletsbox = tbodycheckall.parents(".Palletsbox").eq(0);
					var __trcheck=Palletsbox.find("tbody :checked");
					var itmejson = _this.itmedata(_opts, Palletsbox)[0];
                    var tr_selectdata = [],tr_itmedom=[];
					for (var c = 0; c < __trcheck.length; c++) {
						var tditme = __trcheck.eq(c);
						var tritme = tditme.parents("tr").eq(0);
						var domidsring = tritme.attr("id");
						var trid_sring = domidsring.split(/_/)[1];						
						for (var keytr in itmejson.table.tbody) {
							//行数据
							var trd = itmejson.table.tbody[keytr];
							if (trd.id == trid_sring) {
								tr_itmedom.push(domidsring);
								tr_selectdata.push(trd);
								break;
							}
						}
						
					}

					if(tr_selectdata.length > 0 && tr_itmedom.length > 0){

					itmejson.table.tbody = [];
					itmejson.table.tbody = tr_selectdata;
					itmejsonall.push(itmejson);
					var Palletsboxid=Palletsbox.attr("id");
                    trdomidall.push({Palletsid:Palletsboxid,tbody:tr_itmedom});

				   }

				}

			}

			return {
				trdomid: trdomidall,
				select_trdata: itmejsonall
			};


		},
		_autoheigth: function(_opts) {
			//title高对齐作用
			setTimeout(function() {

				  var brotab = $("#tab_content");
                    brotab.show();

				var p_dom = _opts._Dom;
				p_dom.show();				
				var row = p_dom.find('.row');
				for (var rowi = 0; rowi < row.length; rowi++) {

					var rudrow = row.eq(rowi);

					var toptitlebody = rudrow.find('.toptitlebody');
					var hall = [];
					for (var i = 0; i < toptitlebody.length; i++) {
						var titlebody = toptitlebody.eq(i);
						var idstirn = titlebody.parents(".Palletsbox").eq(0);
						hall.push(titlebody.innerHeight());
					}

					toptitlebody.height(Math.max.apply(null, hall));

				}

				var __onevents = Componbox.ONEVENTS[_opts.jsondataid];

				if (__onevents["Initialize"]) {
					__onevents["Initialize"](p_dom);
				}

				    if (brotab.length > 0) {
                    brotab.find("div[role='tabpanel']").addClass('tab-pane');
                }


				return false;

			}, 50);

		},
		itmedata: function(_opts, Palletsbox) {
			var __JSONDATA = Componbox.JSONDATA[_opts.jsondataid];
			var ids = Palletsbox.attr("id");
			var _id = ids.split(/_/)[1];

			var itmejson = __JSONDATA.filter(function(itmedata) {
				return itmedata.id == _id;
			});
			return itmejson;
		},
		_addevents: function(_opts, noevnte) {

			//创建事件
			var p_dom = _opts._Dom;

			var _this = Componbox;
			var JSONDATA = _this.JSONDATA[_opts.jsondataid];
			var __onevents = Componbox.ONEVENTS[_opts.jsondataid];

			var __Palletsbox = p_dom.find(".Palletsbox");

			//填充表格时不再绑事件
			if (noevnte) {
				var closing = __Palletsbox.find('.pallets_title .closing');
				var closingbtn = closing.find("span");
				//开合
				closingbtn.click(function(e) {
					e.preventDefault();
					var btn_this = $(this);
					var p_box = btn_this.parents(".Palletsbox");
					var ids = p_box.attr("id");
					var _id = ids.split(/_/)[1];

					
					var itmejson = _this.itmedata(_opts, p_box);

					var _pallets_title = p_box.find(".pallets_title");
					var tablebox = p_box.find(".Palletstablebox");
					var _visible = tablebox.is(":visible");
					if (_visible) {
						tablebox.slideUp(300, function() {
							_pallets_title.css("border-bottom-width", "0px");
						});
						btn_this.find('i').eq(0).removeClass('fa-toggle-up').addClass('fa-toggle-down');

						if (__onevents["events.titlebtnclose"]) {

							__onevents["events.titlebtnclose"]({
								button: btn_this,
								Pallets: p_box,
								data: itmejson[0],
								visible: false
							});

						}


					} else {

						tablebox.slideDown(300);
						btn_this.find('i').eq(0).removeClass('fa-toggle-down').addClass('fa-toggle-up');
						_pallets_title.css("border-bottom-width", "1px");

						if (__onevents["events.titlebtnopen"]) {

							__onevents["events.titlebtnopen"]({
								button: btn_this,
								Pallets: p_box,
								data: itmejson[0],
								visible: true
							});

						}

						//如果没有表格时填充表格
						var __tabledata = true;
						if (itmejson.table && typeof itmejson.table == "object") {
							__tabledata = false;
						}
						if (_opts.tabledataurl && __tabledata) {
							var __tablejq = tablebox.find(".table-bordered");
							_this.tabledataurl(_opts, _id, __tablejq);
						}

					}

					return false;
				});

				//标题checkbox
				var toptitlebody = __Palletsbox.find('.toptitlebody :checkbox');
				toptitlebody.click(function(e) {

					checkboxclick(this);

					var btn_this = $(this);
					var p_box = btn_this.parents(".Palletsbox");				


					var itmejson = _this.itmedata(_opts, p_box);

					if (__onevents["events.titlecheckbox"]) {

						__onevents["events.titlecheckbox"]({
							checked: btn_this.prop("checked"),
							Pallets: p_box,
							data: itmejson[0]
						});

					}


				});

			}

			function checkboxclick(_this) {

				var btn_this = $(_this);
				var p_box = btn_this.parents(".Palletsbox");
				var tablebox = p_box.find(".Palletstablebox");
				var checkboxall = tablebox.find(":checkbox");
				var allset = btn_this.prop("checked");
				checkboxall.prop("checked", allset);

			}

			//表头--thcheckbox
			var _thcheckbox = __Palletsbox.find(".thcheckbox");
			_thcheckbox.click(function() {

				checkboxclick(this);
				var btn_this = $(this);
				var p_box = btn_this.parents(".Palletsbox");
			

				var itmejson = _this.itmedata(_opts, p_box);


				var _table = p_box.find(".table-bordered");
				if (__onevents["events.thcheckbox"]) {

					__onevents["events.thcheckbox"]({
						checked: btn_this.prop("checked"),
						table: _table,
						data: itmejson[0].table
					});

				}


			});



			//table内所有
			var __tableboxall = __Palletsbox.find(".Palletstablebox tbody :checkbox");
			__tableboxall.click(function() {

				var btn_this = $(this);
				var p_box = btn_this.parents(".Palletsbox");
				//标题-头
				var toptitle_box = p_box.find(".toptitlebody :checkbox");
				//表格-头
				var _thcheckbox = p_box.find(".thcheckbox");
               
              
				//内
				var tablebox = p_box.find(".Palletstablebox tbody");
				var checkboxall = tablebox.find(":checkbox");
				var checkedall = tablebox.find(":checked");
				if (checkboxall.length > checkedall.length && checkedall.length > 0) {
					toptitle_box.prop("indeterminate", true);
					_thcheckbox.prop("indeterminate", true);
				}

				if (checkboxall.length == checkedall.length && checkedall.length > 0) {
					toptitle_box.prop("indeterminate", false);
					toptitle_box.prop("checked", true);

					_thcheckbox.prop("indeterminate", false);
					_thcheckbox.prop("checked", true);

				}

				if (checkedall.length == 0) {
					toptitle_box.prop("indeterminate", false);
					toptitle_box.prop("checked", false);
					_thcheckbox.prop("indeterminate", false);
					_thcheckbox.prop("checked", false);
				}



				var ids = p_box.attr("id");
				var _id = ids.split(/_/)[1];

				var itmejson = JSONDATA.filter(function(itmedata) {
					return itmedata.id == _id;
				});

				var _tabletr = btn_this.parents("tr").eq(0);
				var _trid=_tabletr.attr("id").split(/_/)[1];

				var _tbodydata = itmejson[0].table.tbody;

                var _trdata={};
				for(var tdkey in _tbodydata){
                     var itmetd=_tbodydata[tdkey];
                    if(itmetd.id == _trid){
                     _trdata=itmetd;
                     break;
                    }
				} 

				if (__onevents["events.tdcheckbox"]) {

					__onevents["events.tdcheckbox"]({
						checked: btn_this.prop("checked"),
						tabletr: _tabletr,
						data: _trdata
					});

				}


			});


		}


	};



	function isElement(o) {
		return !!o && (typeof o === 'object') && (
			/object|function/.test(typeof Element) ? o instanceof Element //DOM2
			: o.nodeType === 1 && typeof o.nodeName === "string");
	}

	function isObject(thing) {
		return thing instanceof Object;
	}

	function isArray(thing) {
		return thing instanceof Array;
	}

	function isFunction(thing) {
		return typeof thing === 'function';
	}

	function isNumber(thing) {
		return typeof thing === 'number';
	}

	function isBool(thing) {
		return typeof thing === 'boolean';
	}

	function isString(thing) {
		return typeof thing === 'string';
	}


	return PalletsPlugin;
}));