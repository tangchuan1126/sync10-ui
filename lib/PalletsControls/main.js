 requirejs(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

     requirejs(["oso.lib/nprogress/nprogress.min", "jquery", "require_css!oso.lib/nprogress/nprogress", "require_css!bootstrap-css/bootstrap.min"],
         function(NProgress, $) {


             $("body").show();

             NProgress.start();

             var cssfileall = [
                 "oso.lib/CodeMirror/mode/javascript/javascript",
                 "oso.lib/CodeMirror/lib/util/foldcode",
                 "require_css!oso.lib/CodeMirror/lib/codemirror.css",
                 "require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css",
                 "bootstrap"
             ];


             requirejs(cssfileall, function() {


                 requirejs(['oso.lib/PalletsControls/Pallets'], function(pallets) {

                     $(function() {

                         var __test_val1 = {
                             renderTo: ".Express",
                             dataUrl: "./d.json",
                             tabledataurl: "./table.json?{id}",
                             scrollH: 200, //最高200超出会滚动
                             columns: 4, //每行的列数，默认流式布局
                             colstyle: "250" //可以数字也可以col-md-*默认是col-md-3
                         };

                         var __p_list = new pallets(__test_val1);

                         __p_list.on("tohtml", function(html) {
                             console.log(html);
                         });

                         //表格表身checkbox
                         __p_list.on("events.tdcheckbox", function(e) {
                             console.log(e);
                         });

                         //表格表头checkbox
                         __p_list.on("events.thcheckbox", function(e) {
                             console.log(e);
                         });

                         //标题头部checkbox
                         __p_list.on("events.titlecheckbox", function(e) {
                             console.log(e);
                         });

                         //开起
                         __p_list.on("events.titlebtnopen", function(e) {
                             console.log(e);
                         });
                         //合起-关闭
                         __p_list.on("events.titlebtnclose", function(e) {
                             console.log(e);
                         });


                         // $(".buttonall button").click(function(){
                         //             var comsrin=$(this).data("events");
                         //             //titlecheckboxall
                         //             //theadcheckboxall
                         //             //tbodycheckboxall
                         //              var dataval = __p_list.command(comsrin);
                         //              console.log(dataval);
                         //           });

                         $("#panel_allcheck").click(function() {
                             var _this = $(this);
                             var dftcheck = _this.prop("checked");
                             var allval = __p_list.command("ALLchecked", dftcheck);
                             console.log(allval);

                         });



                         __p_list.on("Initialize", function() {

                             require(['oso.lib/CodeMirror/demo/CodeMirror_demo'], function() {
                                 $("#run").click();
                                 //console.log(__p_list);         

                             });

                         });

                         NProgress.done();

                     });
                     // window onlonad end

                 });
                 //main end 


             });
             // codemirror end      

         });
     // requirejs_config end
 });
 // nprogress end
