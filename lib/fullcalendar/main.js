requirejs(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

     requirejs([
     	"oso.lib/nprogress/nprogress.min", "jquery", 
     	"require_css!oso.lib/nprogress/nprogress",
     	 "require_css!bootstrap-css/bootstrap.min",

     	 "oso.lib/CodeMirror/mode/javascript/javascript",
                 "oso.lib/CodeMirror/lib/util/foldcode",
                 "require_css!oso.lib/CodeMirror/lib/codemirror.css",
                 "require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css",
                 "oso.lib/fullcalendar/fullcalendar","bootstrap"],
         function(NProgress, $) {


             $("body,#tab_content").show();

             NProgress.start();         


 //#####==========calendar -set


             var cssfileall = [ 
              "require_css!Font-Awesome",
               "require_css!oso.lib/fullcalendar/fullcalendar",              
               "require_css!oso.lib/fullcalendar/style.calendar"                             
             ];

             requirejs(cssfileall, function(fullCalendar) {


          $(function() { 

          /*

         fullcalendar
         如果events:[]为空时，showTdDetailed属性也有禁用
         原始fullcalendar--这时td单元格回调tdcellclick才起作用

          */

                
         var brotab = $("#tab_content");
          brotab.show();

         $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'config'
            },
            defaultDate: '2015-02-11',
            selectable: false,
            editable: true,
            eventLimit: true,  
            inboundshow:true,
            outboundshow:false,
            selectTdbgcolor:"#ccc",          
            //showTdDetailed:"./json_data.json",
           // showTdDetailed:"/Sync10/_load/getBaseWorkByWeek",
            TdDetailedval:function(){
              //var obj=$(".d").val();
             //storage_id=1000005&start=2015-02-08&end=2015-02-14
             return {storage_id:"1000005"};

            },
            ringhtconfigclick:function(){

              console.log(22222);

            },
            tdcellclick:function(todate){
            //每单击td事件回调反回3种年月日dates是被点击的td,其他两个是当前行的左和右
            	console.log(todate);

            },
            Initialization:function(){
              

              console.log(54444); 

            },
            events:[]
             //events:[{"lentxt":"3","title":"","start":"2015-02-09","type":"inbound"},{"lentxt":"0","title":"","start":"2015-02-09","type":"outbound"},{"lentxt":"6","title":"","start":"2015-02-12","type":"inbound"},{"lentxt":"0","title":"","start":"2015-03-02","type":"inbound"},{"lentxt":"0","title":"","start":"2015-03-02","type":"outbound"},{"lentxt":"0","title":"","start":"2015-03-03","type":"inbound"},{"lentxt":"0","title":"","start":"2015-03-03","type":"outbound"},{"lentxt":"3","title":"","start":"2015-01-28","type":"inbound"},{"lentxt":"0","title":"","start":"2015-01-28","type":"outbound"},{"lentxt":"0","title":"","start":"2015-03-02","type":"inbound"},{"lentxt":"0","title":"","start":"2015-03-02","type":"outbound"},{"lentxt":"0","title":"","start":"2015-03-03","type":"inbound"},{"lentxt":"0","title":"","start":"2015-03-03","type":"outbound"}]
            
            //动态           
  //            events: {
		//         url: '/Sync10/_load/sumWorkByStartEnd',
		//         type: 'GET',
		//         data: {
		//           storage_id:"1000005"
		//         },
  //           Initialization:function(){

              

  //             console.log(456); 

  //           },
		//         error: function(e) {
		//            console.log(e);
		//         }
		// }
		
		
            
        });

         $("#ssr").click(function(){
         	$('#calendar').fullCalendar("restorage_id","20000");          

         });
//gettddata
        $("#gettddata").click(function(){

           var dsd=$('#calendar').fullCalendar("Get_Tdate","2015-02-08 00:00");
           console.log(dsd);


         });
         

      if (brotab.length > 0) {
               brotab.find("div[role='tabpanel']").addClass('tab-pane');
             }

//.Calendar_push();
        

            NProgress.done();  
    


                  });
                


          });

  //####==========calendar end 


         });
     // requirejs_config end
 });
 // nprogress end