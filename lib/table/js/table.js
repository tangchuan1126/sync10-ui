"use strict";

(function() {

    var defineval = ["jquery", "Paging", "require_css!bootstrap-css/bootstrap.min.css", "require_css!../css/style.css"];

    var msieindexof = /msie/.test(navigator.userAgent.toLowerCase());

    if (msieindexof) {
        defineval = [];
        defineval = ["jquery1.9","Paging"];
    }


    define(defineval, function(jq_, Paging) {

        var Table = function(options) {

            if (!options.container) {
                throw new Error("Do not specify a container!");
            }

            var this_table = this;

            var defaults = {
                    url: undefined,
                    localData: [], //本地数据
                    title: '',
                    width: undefined,
                    noDataMsg: "No Data",
                    height: 'auto',
                    titlePosition: 'top',
                    columns: [],
                    toolbar: null,
                    queryParams: {}, //传递给后台的数据的查询条件(字段的)
                    method: 'get', //请求方式
                    datasName: undefined, //返回的数据的对应字段名
                    pageSizeName: undefined, //传递给后台的当前页数的字段名
                    pageNumberName: undefined, //传递给后台的每页数据条数字段名
                    totalName: undefined, //返回的数据总条数的对应字段名
                    groupKey: undefined, //按字段分组
                    infoField: undefined, //鼠标移动到行显示的行信息字段
                    infoFormat: undefined, //鼠标移动到行显示的信息格式
                    singleSelection: false,
                    //container : null,
                    border: true,
                    striped: true,
                    pagination: true,
                    pageSize: 10,
                    pageNumber: 1,
                    pageList: ["10", "20", "30", "40", "50"],
                    onLoadSuccess: function() {},
                    onLoadError: function(jqXHR, textStatus, SyntaxError) {
                        alert("Request failed: " + textStatus + "," + SyntaxError);
                    },
                    onClickRow: function(rowIndex, rowData, events) {},
                    isEnable: function(row) {}
                },
                opts = $.extend({}, defaults, options),
                container = $(opts.container),
                table, table_header, localData = $.merge([], opts.localData),
                events = {},
                total = 0,
                icons = {
                    "add": "glyphicon glyphicon-plus",
                    "remove": "glyphicon glyphicon-remove",
                    "update": "glyphicon glyphicon-pencil",
                    "view": "glyphicon glyphicon-search"
                };

            function initEvents() {
                events["events.loadSuccess"] = opts.onLoadSuccess;
                events["events.loadError"] = opts.onLoadError;
                events["events.clickRow"] = opts.onClickRow;
                events["getLocalData"] = null
            }

            function pageIndex(pageSize) {
                var pageList = opts.pageList;
                for (var i = 0, len = pageList.length; i < len; i++) {
                    var size = pageList[i];
                    if (size === pageSize + "") {
                        return i;
                    }
                }
                return 0;
            }

            function request(options, pager) {

                opts = $.extend(opts, options);

                if (!opts.url) return;

                localData = [];

                var param = opts.queryParams;

                if (opts.pagination) {
                    var pageParam = {};
                    opts.pageSizeName ? pageParam[opts.pageSizeName] = opts.pageSize : pageParam.rows = opts.pageSize;
                    opts.pageNumberName ? pageParam[opts.pageNumberName] = opts.pageNumber : pageParam.page = opts.pageNumber;

                    param = $.extend({}, param, pageParam);
                }

                $.ajax({
                    type: opts.method,
                    url: opts.url,
                    data: param,
                    dataType: 'json',
                    success: function(data) {

                        total = opts.totalName ? eval("data." + opts.totalName) : data.total;

                        if (opts.pagination) {

                            var pageboxId = container.find('.pagebox').get(0).id;

                            //总页数，当前页号，容器id，回调
                            Paging.update_page(Math.ceil(total / opts.pageSize), opts.pageNumber, pageboxId, function(pageNo) {
                                container.find('.modal').show();
                                opts.pageNumber = pageNo;
                                request();
                            }, {
                                option: opts.pageList,
                                index: pageIndex(opts.pageSize),
                                callback: function(_v) {
                                    container.find('.modal').show();
                                    opts.pageNumber = 1;
                                    opts.pageSize = _v;
                                    request();
                                }
                            });
                        }

                        if (opts.groupKey) {
                            _loadData(processData(data[opts.datasName] || data.rows));
                        } else {
                            _loadData(data[opts.datasName] || data.rows);
                        }

                        if (events["events.loadSuccess"]) {
                            events["events.loadSuccess"].apply(this, arguments);
                        }

                        container.find('.modal').hide();
                    },
                    error: function(e) {
                        if (events["events.loadError"]) {
                            events["events.loadError"].apply(this, arguments);
                        }
                        total = 0;
                        container.find('.modal').hide();
                    }
                });
            }



            function _createRow(i, row, tbody, columns, len) {

                var tr = $('<tr data-index="' + i + '" class="group-' + row._groupKey + '"></tr>').data('row', row);
                //checkbox
                if (opts.checkbox) {
                    $('<td class="text-center" width="50px"><input type="checkbox" class="table-checkbox"' + (opts.isEnable(row) !== false ? '' : ' disabled="disabled"') + '></td>')
                        .on('click', '.table-checkbox', function(e) {

                            var tr = $(this).parent().parent();
                            if (this.checked) {

                                //单选
                                if (opts.singleSelection) {
                                    var checkeds = table.find('tr.info');
                                    for (var j = 0, length = checkeds.length; j < length; j++) {
                                        $(checkeds[j]).parent().parent().trigger('click');
                                    }
                                }

                                tr.trigger('click');

                            } else {
                                tr.trigger('click');

                                clearCheckAll();
                            }

                            e.stopPropagation();

                        }).appendTo(tr);
                }

                //序号				
                if (opts.rownumbers) {
                    var rownumber = i + 1;
                    if (opts.pagination) {
                        rownumber += (opts.pageNumber - 1) * opts.pageSize;
                    }

                    tr.append('<td class="text-center" width="50px">' + rownumber + '</td>');
                }

                for (var j = 0; j < columns.length; j++) {

                    var column = columns[j],
                        rowspan = row._rowspan,
                        td;

                    if (column.field == opts.groupKey) {
                        if (rowspan) {
                            td = $('<td' + (rowspan != 1 ? ' rowspan=' + rowspan : '') + ' class="group-td-' + row._groupKey + '"></td>')
                                .css('vertical-align', 'middle')
                                .on('click', function() {
                                    var rowspanTr = table.find('.group-' + row._groupKey),
                                        check_len = table.find('.group-' + row._groupKey + '.info').length;

                                    if (rowspanTr.length === check_len) {
                                        rowspanTr.each(function() {
                                            var self = $(this);
                                            if (self.hasClass('info')) {
                                                self.trigger('click');
                                            }
                                        });
                                    } else {
                                        rowspanTr.each(function() {
                                            var self = $(this);
                                            if (!self.hasClass('info')) {
                                                self.trigger('click');
                                            }
                                        });
                                    }

                                    return false;
                                }).hover(function() {
                                    $(this).addClass('bg-c4e3f3');
                                    table.find('.group-' + row._groupKey).each(function(i, elem) {
                                        $(elem).addClass('bg-c4e3f3');
                                    });
                                }, function() {
                                    $(this).removeClass('bg-c4e3f3');
                                    table.find('.group-' + row._groupKey).each(function(i, elem) {
                                        $(elem).removeClass('bg-c4e3f3');
                                    });
                                });
                        } else {

                            if (i + 1 == len) {
                                table.find('.group-td-' + row._groupKey).css('border-bottom-width', '0');
                            }
                            continue;
                        }
                    } else {
                        td = $('<td></td>');
                    }

                    if (column.formatter) {
                        td.append(column.formatter(row[column.field], row)).appendTo(tr);

                    } else {
                        td.append(row[column.field]).appendTo(tr);
                    }

                    if (i === 0 && column.width) {
                        td.width(column.width);
                    }

                    if (column.align) {
                        switch (column.align) {
                            case 'left':
                                td.addClass('text-left');
                                break;
                            case 'right':
                                td.addClass('text-right');
                                break;
                            case 'center':
                                td.addClass('text-center');
                                break;
                        }
                    }
                }

                tr.on('click', function() {

                    var self = $(this);
                    var checkbox = null;
                    if (opts.checkbox) {
                        checkbox = self.find('.table-checkbox')[0];
                        if (checkbox.disabled) {
                            return;
                        }
                    }

                    var index = self.attr('data-index'),
                        rowspanTd = table.find('.group-td-' + row._groupKey),
                        rowspanTr = table.find('.group-' + row._groupKey);



                    if (self.hasClass('info')) {
                        self.removeClass('info');
                        if (rowspanTd.length) {
                            for (var j = 0, len = rowspanTr.length; j < len; j++) {
                                if ($(rowspanTr[j]).hasClass('info')) {
                                    break;
                                }
                            }
                            if (j == len) {
                                rowspanTd.removeClass('bg-d9edf7').removeClass('bg-c4e3f3');
                            } else {
                                rowspanTd.removeClass('bg-c4e3f3');
                            }
                        }
                        if (checkbox) {
                            checkbox.checked = false;
                        }
                        clearCheckAll();
                    } else {

                        //单选
                        if (opts.singleSelection) {
                            var checked = table.find('tr.info');
                            if (checked.length) {
                                checked.trigger('click');
                            }
                        }

                        self.addClass('info');
                        if (rowspanTd.length) {
                            rowspanTd.addClass('bg-d9edf7 bg-c4e3f3');
                        }

                        if (checkbox) {
                            checkbox.checked = true;
                        }
                    }

                    events["getLocalData"] = null;
                    events["getLocalData"] = this_table.getLocalData();
                    events["events.clickRow"](index, row, checkbox);

                }).appendTo(tbody);

                var timer = null;
                tr.hover(function(event) {

                    clearTimeout(timer);
                    timer = null;

                    var self = $(this);
                    var tip = $.data(this, 'row')._tip;
                    var info = $.data(this, 'row')[opts.infoField];

                    if (opts.checkbox) {
                        var checkbox = self.find('.table-checkbox')[0];
                        if (checkbox.disabled) {
                            self.css('cursor', 'not-allowed');
                        }
                    }

                    var rowspanTd = table.find('.group-td-' + row._groupKey)

                    if (self.hasClass('info')) {
                        rowspanTd.addClass('bg-c4e3f3');
                    }

                    //行提示信息
                    if (!tip && info) {

                        tip = $('<div class="table-tip" >' + (opts.infoFormat ? opts.infoFormat(info) : info) + '</div>')
                            .hover(function() {
                                clearTimeout(timer);
                                timer = null;
                            }, function() {
                                var tip = $(this);
                                timer = setTimeout(function() {
                                    tip.hide();
                                }, 500);
                            }).appendTo('body');
                        $.data(this, 'row')._tip = tip;
                    }

                    if (tip && tip.css('display') === 'none') {
                        timer = setTimeout(function() {
                            tip.css({
                                "left": event.pageX,
                                "top": event.pageY,
                                "max-width": $(window).width() - event.pageX - 20,
                                "display": 'block'
                            });
                        }, 500);
                    }

                }, function(event) {

                    clearTimeout(timer);
                    timer = null;
                    var tip = $.data(this, 'row')._tip;
                    if (tip) {
                        timer = setTimeout(function() {
                            tip.hide();
                        }, 500);
                    }

                    var self = $(this).css('cursor', 'default');

                    var rowspanTd = table.find('.group-td-' + row._groupKey)

                    if (self.hasClass('info')) {
                        rowspanTd.removeClass('bg-c4e3f3');
                    }
                });
            }

            function processData(rows) {
                if (!rows.length) {
                    return rows;
                }

                var groups = {},
                    groupKey = opts.groupKey,
                    result = [],
                    mark = 0;

                for (var i = 0, len = rows.length; i < len; i++) {
                    var row = rows[i],
                        key = row[groupKey];

                    if (!groups[key]) {
                        groups[key] = [];
                    }
                    row._rowspan = undefined;
                    groups[key].push(row);
                }

                for (var obj in groups) {
                    var group = groups[obj];
                    mark += 1;
                    group[0]._rowspan = group.length;
                    for (var i = 0, len = group.length; i < len; i++) {
                        group[i]._groupKey = mark;
                    }
                    result = result.concat(group);
                }

                return result;
            }

            function _loadData(rows) {

                if (localData.length && opts.pagination) {

                    total = localData.length;

                    rows = localData.slice(opts.pageSize * (opts.pageNumber - 1), opts.pageSize * opts.pageNumber);

                    var pageboxId = container.find('.pagebox').get(0).id;

                    //总页数，当前页号，容器id，回调
                    Paging.update_page(Math.ceil(total / opts.pageSize), opts.pageNumber, pageboxId, function(pageNo) {
                        container.find('.modal').show();
                        opts.pageNumber = pageNo;
                        _loadData(localData.slice(opts.pageSize * (pageNo - 1), opts.pageSize * pageNo));
                        container.find('.modal').hide();
                    }, {
                        option: opts.pageList,
                        index: pageIndex(opts.pageSize),
                        callback: function(_v) {
                            container.find('.modal').show();
                            opts.pageNumber = 1;
                            opts.pageSize = _v;
                            _loadData(localData.slice(0, _v));
                            container.find('.modal').hide();
                        }
                    });
                }

                var columns = opts.columns;

                if (!table) {

                    var title_height = 0;

                    table = $('<table></table>').addClass(table_header[0].className)
                        .appendTo($('<div class="table-content panel"></div>').css({
                            "max-height": opts.height,
                            width: opts.width
                        }).insertAfter(table_header));
                } else {
                    table.empty();
                }

                if (opts.groupKey && rows.length && !rows[0]._rowspan) {
                    //合并组翻页，第一项没有rowspan的情况
                    var rowspan = 1;
                    for (var i = 1, len = rows.length; i < len; i++) {
                        if (rows[i]._rowspan) {
                            break;
                        }
                        rowspan += 1;
                    }
                    rows[0]._rowspan = rowspan;
                }

                for (var i = 0, len = rows.length; i < len; i++) {
                    var row = rows[i];
                    row._index = (opts.pageNumber - 1) * opts.pageSize + i;
                    _createRow(i, row, table, columns, len);
                }

                var _p = table.next('p');
                if (i === 0) {
                    if (!_p.length) {
                        table.parent().append('<p class="table-nodata-msg">' + opts.noDataMsg + '</p>');
                    }
                } else if (_p.length) {
                    _p.remove();
                }

                //width
                setTimeout(function(){
                setHeaderWidth(table_header, table);
                },100);

                $(window).resize(function() {
                    setHeaderWidth(table_header, table);
                });
            }

            function createTable() {

                if (!container.css('position') || container.css('position') === 'static') {
                    container.css('position', 'relative');
                }

                table_header = $('<table class="table table-hover"></table>');

                var panel = $('<div class="panel panel-default table-panel"></div>').append(table_header).appendTo(container);

                //表头
                if (opts.title) {

                    if (opts.titlePosition === 'top') {
                        $('<div class="panel-heading">' + opts.title + '</div>').insertBefore(table_header);
                    } else {
                        panel.append('<div class="panel-footer">' + opts.title + '</div>');
                    }
                }

                if (opts.toolbar) {
                    creatToolbar(opts.toolbar);
                }

                if (opts.border == true) {
                    table_header.addClass('table-bordered');
                } else {
                    table_header.removeClass('table-bordered');
                }

                if (opts.striped) {
                    table_header.addClass('table-striped');
                } else {
                    table_header.removeClass('table-striped');
                }

                if (opts.columns) {
                    var t = createColumnHeader(opts.columns);
                    table_header.html(t);
                }

                if (opts.pagination) {
                    var id = 'pagebox_' + (new Date()).getTime();
                    var pager = $("<ul id=" + id + " class='clearfix pagebox'></ul>").appendTo(container);
                }

                //loading...
                var loadingModal = $('<div class="modal"><div class="loading"></div></div>').appendTo(container).show();

                if (opts.url) {
                    request(null);
                } else {

                    if (opts.groupKey) {
                        localData = processData(localData);
                    }

                    _loadData(localData);

                    if (events["events.loadSuccess"]) {
                        events["events.loadSuccess"].apply(this, arguments);
                    }

                    loadingModal.hide();
                }

                initEvents();
            }

            function creatToolbar(toolbar) {

                var body = $('<div class="panel-body table-panel-body"></div>').insertBefore(table_header);

                for (var i = 0, len = toolbar.length; i < len; i++) {
                    var btn = toolbar[i];
                    var tool = $('<a class="tabel-tool-icon btn ' + (btn.btnClass ? btn.btnClass : '') + '" href="javascript:void(0)"></a>');
                    tool[0].onclick = btn.handler || function() {};
                    tool.append('<i class = "' + (icons[btn.iconCls] || btn.iconCls || '') + '"></i>')
                        .append(btn.text)
                        .appendTo(body);
                }
            }

            function createColumnHeader(columns) {
                var t = $('<tr></tr>');
                //checkbox
                if (opts.checkbox) {
                    $('<th class="text-center"><div class="table-th-div"><input type="checkbox" class="check-all"></div></th>')
                        .on('click', '.check-all', function() {

                            //单选
                            if (opts.singleSelection) {
                                return this.checked = false;
                            }

                            var _tr=table.find('tbody').find('tr');

                             var inputs = table.find('tbody').find('tr').find('.table-checkbox');

                            if (this.checked) {                               

                                inputs.prop("checked", true);
                                _tr.addClass('info');

                                table.find('.bg-d9edf7').each(function() {
                                    $(this).removeClass('bg-c4e3f3');
                                });

                            } else {

                                 inputs.prop("checked", false);
                                 _tr.removeClass('info');
                            }
                        }).appendTo(t);
                }
                //序号
                if (opts.rownumbers) {
                    t.append('<th><div class="table-th-div"></div></th>');
                }
                for (var i = 0; i < columns.length; i++) {
                    var col = columns[i];
                    var th = $('<th><div class="table-th-div"></div></th>').attr('field', col.field).appendTo(t).find('.table-th-div');

                    //排序
                    if (col.order) {
                        th.html(col.title + '<div class="tabel-sort-icon"><i class="tabel-icon-up"></i><i class="tabel-icon-down"></i><div>')
                            .css('position', 'relative')
                            .on('click', '.tabel-icon-up', function() {
                                //排序
                                var orderBy = $(this).parent().parent().parent().attr('field')
                                var queryParams = {};
                                queryParams["orderBy_" + orderBy] = 'desc';
                                opts.pageNumber = 1;
                                request(queryParams);
                            }).on('click', '.tabel-icon-down', function() {
                                var orderBy = $(this).parent().parent().parent().attr('field')
                                var queryParams = {};
                                queryParams["orderBy_" + orderBy] = 'asc';
                                opts.pageNumber = 1;
                                request(queryParams);
                            });
                    } else {
                        th.html(col.title);
                    }

                    if (col.align) {
                        switch (col.align) {
                            case 'left':
                                th.addClass('text-left');
                                break;
                            case 'right':
                                th.addClass('text-right');
                                break;
                            case 'center':
                                th.addClass('text-center');
                                break;
                        }
                    }
                }

                return $('<thead></thead>').append(t);
            }

            function setHeaderWidth(header, table) {
                var ths = header.find('tr:first').children(),
                    tds = table.find('tr:first').children();

            var window_width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

                if(tds < 1)
                  return false               
              
                for (var i = 0, len = tds.length; i < len; i++) {

                     var table_td_w=tds.eq(i).width();
                     if(window_width < 1370 && i==1){
                       table_td_w= table_td_w -1; 
                     }                                   
                   ths.eq(i).find('.table-th-div').width(table_td_w);
                }

                 if (tds.length) {
                    var table_w=table.outerWidth();
                    header.width(table_w);
                }

                return false


            }

            function clearCheckAll() {
                var allCheckbox = table_header.find('.check-all')[0];
                if (allCheckbox && allCheckbox.checked) {
                    allCheckbox.checked = false;
                }
            }

            //扩展方法
            if (this) {

                this.refreshWidth = function() {
                    setHeaderWidth(table_header, table);
                }

                this.on = function(eventType, fn) {

                    events[eventType] = fn;
                }

                this.reload = function(options) { //isLocalData
                    container.find('.modal').show();
                    opts.pageNumber = 1;
                    if (options === true) {
                        _loadData(localData);
                        container.find('.modal').hide();
                    } else {
                        request(options);
                    }
                    clearCheckAll();
                }

                this.reloadCurrent = function(options) { //isLocalData
                    container.find('.modal').show();
                    if (options === true) {
                        if (localData.length <= opts.pageSize * (opts.pageNumber - 1)) {
                            opts.pageNumber--;
                        }

                        _loadData(localData);
                        container.find('.modal').hide();
                    } else {
                        request(options);
                    }
                    clearCheckAll();
                }

                this.getSelected = function() {
                    var rows = [];
                    var trs = table.find('tr.info');
                    for (var i = 0, len = trs.length; i < len; i++) {
                        $.merge(rows, [$(trs[i]).data('row')]);
                    }
                    return rows;
                }

                this.loadData = function(rows) {

                    if (!(rows instanceof Array) || !rows.length) {
                        return;
                    }

                    container.find('.modal').show();
                    if (opts.groupKey) {
                        localData = processData(rows);
                    } else {
                        localData = $.merge([], rows);
                    }

                    _loadData(localData);

                    if (events["events.loadSuccess"]) {
                        events["events.loadSuccess"].apply(this, arguments);
                    }

                    container.find('.modal').hide();
                }

                this.deleteLocalData = function(index, callback) {

                    if (!index || !localData || !localData.length) {
                        throw new Error('Parameter error or no load local data');
                    }

                    if (index instanceof Array) {
                        for (var i = 0, len = index.length; i < len; i++) {
                            localData[index[i]._index] = undefined;
                        }
                        for (i = 0, len = localData.length; i < len; i++) {
                            if (!localData[i]) {
                                localData.splice(i, 1);
                                i--;
                                len = localData.length;
                            }
                        }
                    } else if (index instanceof Object) {
                        localData.splice(index._index, 1);
                    } else {
                        var row = localData.splice(index, 1);
                        index = row;
                    }

                    if (opts.groupKey) {
                        localData = processData(localData);
                    }

                    total = localData.length;

                    callback(index);
                }

                this.addLocalData = function(row, callback) {
                    if (!localData || !row) {
                        return;
                    }
                    if (row instanceof Array) {
                        localData = localData.concat(row);
                    } else if (row instanceof Object) {
                        localData.push(row);
                    } else {
                        throw new Error('parameter error');
                    }

                    if (opts.groupKey) {
                        localData = processData(localData);
                    }
                    total = localData.length;
                    callback(row);
                }

                this.getLocalData = function() {
                    return $.merge([], localData);
                }

                this.getTotal = function() {
                    return total;
                }
            }

            createTable();

        };

        return Table;
    });
})(this);
