"use strict";

require([
     "../../requirejs_config", 
], function(){
    require(["jquery", "js/table"], function($, Table){
        var rows = [{
                "code":"000000001",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 30",
                "info" : "事件属性onabort, onbeforeunload, onblur, onchange"
            },{
                "code":"00000002",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 30"
            },{
                "code":"00000003",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 30"
            },{
                "code":"00000004",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "40 30"
            },{
                "code":"00000005",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "40 30"
            },{
                "code":"00000006",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 40"
            },{
                "code":"00000007",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 40"
            },{
                "code":"00000008",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 30"
            },{
                "code":"00000009",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "40 30"
            },{
                "code":"00000010",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 40"
            },{
                "code":"00000011",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 30"
            },{
                "code":"00000012",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "40 30"
            },{
                "code":"00000013",
                "first":"Otto",
                "last":"Mark",
                "username":"@mdo",
                "age" : "20 40"
            }];

    	var table = new Table({
    		container : '#bb',
    		//url : 'a.json',
            localData : rows,
    		//title : 'Test Table',
            height : 300,
            datasName : 'data',
            pageSizeName:'size',
            pageNumberName:'num',
            infoField :'info',
            onClickRow:function(rowIndex,rowData,checkbox){
                

                //localData返回所有数据
                //this.getLocalData

                 console.dir(this.getLocalData);
              
                 //checkbox --input document
                 var row_checked=checkbox.checked;

                 //dom--tr document
                 var tr_obj=$(checkbox).parents("tr").eq(0);
                 //如果row_checked为true那就表示当前行是被选中的
                 console.log(row_checked);

                  var a="123";

                 if(a=="123"){
 
                  //去除tr的classname --info 选中效果
                   tr_obj.removeClass('info');
                    checkbox.checked=false;
                 }



            },
            
            //groupKey : 'age',
            isEnable:function(row){
                if(row.age.indexOf('20') !== -1){
                    return false;
                }
            },
    		rownumbers : true,
            checkbox : true,
            singleSelection : false,
    		pagination:false,
    		columns:[
                {field:'age',title:'Age', width:100},
	    		{field:'code',title:'Code',width:'10%', order:true},
	    		{field:'first',title:'First Name',formatter : function(data, row){
	    			return '<a href="#">' + data + '</a>';
	    		}},
	    		{field:'last',title:'Last Name', align: 'center'},
	    		{field:'username',title:'Username'},
                {field:'username',title:'Username'},
                {field:'username',title:'Username'},
                {field:'username',title:'Username'}
    		],
            toolbar : [
                {
                    text : 'add',
                    iconCls : 'add',
                    btnClass : 'btn-abc',
                    handler : function(){
                       
                        var rows = table.getSelected();
                                    
                        table.deleteLocalData(rows, function(row){
                            table.reloadCurrent(true);
                        });

                         table2.addLocalData(rows,function(row){
                            table2.reloadCurrent(true);
                        });

                    }
                },
                {
                    text : 'update',
                    iconCls : 'update',
                    handler : function(){
                        alert(table.getTotal());
                    }
                },{
                    text : 'delete',
                    iconCls : 'remove',
                    handler : function(){
                        var rows = table.getSelected();

                        table.deleteLocalData(rows, function(row){
                            console.log(row);
                            table.reloadCurrent(true);
                        });
                    }
                },
                {
                    text : 'Test',
                    iconCls : 'glyphicon glyphicon-refresh',
                    handler:function(){
                        table.reload({
                            url : 'a.json',
                            queryParams:{'a':100000}
                        });
                    }
                }
            ]
    	});
table.on("events.loadSuccess", function(){
    alert(table.getTotal());
});

        // var table2 = new Table({
        //     container : '#aa',
        //     //url : 'a.json',
        //     title : 'Test Table',
        //     //height : 400,
        //     rownumbers : true,
        //     groupKey : 'age',
        //     singleSelection : false,
        //     //pagination:false,
        //     columns:[
        //     {field:'age',title:'Age', width:100},
        //         {field:'code',title:'Code',width:100, order:true},
        //         {field:'first',title:'First Name',width:100, formatter : function(data, row){
        //             return '<a href="#">' + data + '</a>';
        //         }},
        //         {field:'last',title:'Last Name', align: 'center',width:100},
        //         {field:'username',title:'Username',width:100}
        //     ],
        //     toolbar : [
        //         {
        //             text : 'add',
        //             iconCls : 'add',
        //             handler : function(){
        //                 table.addLocalData({
        //                     "code":"New001",
        //                     "first":"Otto",
        //                     "last":"Mark",
        //                     "username":"@mdo"
        //                 },function(row){
        //                     table.reloadCurrent(true);
        //                 });
        //             }
        //         }
        //     ]
        // });

        $('#load').click(function(){

            

            //table2.loadData(rows);

            //table.loadData(rows);
           
        });

        $('#sel').click(function(){
            console.log(table.getSelected());
            
        });

        $('#load').click(function(){
            $('#bb').show(function(){
                table.refreshWidth();
            });
            
        });

    });

});
