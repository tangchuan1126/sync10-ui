﻿"use strict";
(function () {

  var msieindexof = /msie/.test(navigator.userAgent.toLowerCase());
   var defineval=["jquery"];
        if (msieindexof) {
         defineval=["jquery1.9"];
        }

    define(defineval, function () {
        
        var CompondBox = function (options) {
            options = $.extend({
                content: '',
                title: '',
                container:'',
                isExpand: true,
                isMerger:false
            }, options);
            var container = $(options.container), content;
            

            function init() {
                content = $('<div class="info-box"><div class="info-title"><span class="title">' +
                    options.title +
                    '</span></div><div class="info-body">' + options.content + '</div></div>')
                    .appendTo(container);
                
                if (options.isExpand) {
                    content.find(".info-title").append('<span class="handle pull-right"><i class="glyphicon glyphicon-minus"></i></span>')
                    .on("click", function () {
                        expand();
                    });
                }
                merger();
            }

            function merger() {
                if (options.isMerger) {
                    content.find(".handle").find("i").attr("class", "glyphicon glyphicon-plus");
                    content.find(".info-body").hide();
                } else {
                    content.find(".handle").find("i").attr("class", "glyphicon glyphicon-minus");
                    content.find(".info-body").show();
                }
            }

            function expand() {
                if (content.find(".info-body").is(":hidden")) {
                    content.find(".handle").find("i").attr("class", "glyphicon glyphicon-minus");
                    content.find(".info-body").slideDown();
                }
                else {
                    content.find(".handle").find("i").attr("class", "glyphicon glyphicon-plus");
                    content.find(".info-body").slideUp();
                }
                
            }
            init();
        }
        return CompondBox;
    });
})(this);