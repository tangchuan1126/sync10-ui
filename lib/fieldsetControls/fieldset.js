/*
 * fieldset.js
 * Copyright 2014, Connor
 * http://127.0.0.1/Sync10-ui/ilb/fieldsetControls/
 */
;
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register 
    define(['jquery','JSONTOHTML'], factory);
  } else if (typeof exports === 'object') {
    // CommonJS
    module.exports = factory(require('jquery'),require('JSONTOHTML'));

  } else {
    // Browser globals
    root.exports = factory(root.jquery,root.JSONTOHTML);
  }
}(this, function($,JSONTOHTML) {
  'use strict';

 
  function fieldsetPlugin(opts) {

    if (!(this instanceof fieldsetPlugin))
      return new fieldsetPlugin(opts);

    if (typeof opts === 'string')
      opts = {
        renderTo: opts
      }; 
 

    var thisconstructor=this.constructor;
    
    thisconstructor.opts=opts;
    thisconstructor.opts.id = "fi_"+$.now();
    thisconstructor.opts._Dom=null;
    thisconstructor.opts.jsondataid=null;
   
    thisconstructor.int();   

    var allfn = {
      on:thisconstructor.On,
      render:thisconstructor.Render,
      addEvent:thisconstructor.AddEvent,
      tohtml:thisconstructor.toHTML
    } 

    allfn.constructor.opts=this.constructor.opts

    return allfn;

  };


  fieldsetPlugin.toHTML=function(){

    var _opts = this.constructor.opts;
   var html=Componbox.tohtml[_opts.jsondataid];

    return html;
  };


  fieldsetPlugin.int = function() {

    var _opts = this.opts; 

    var dataname = "_fieldset_" + $.now();
    _opts._Dom = $('<div id="' + dataname + '"></div>');    

    if (_opts.renderTo && _opts.renderTo != "" && typeof _opts.renderTo != "undefined") {
      dataname = _opts.renderTo.replace(/\.|\/|#/g, "");
      var _todombox=$(_opts.renderTo);
      if(_todombox.length > 0){
      _opts._Dom = _todombox;
      }
    }

    _opts.jsondataid = dataname;

    Componbox.ONEVENTS[_opts.jsondataid] = {};
    Componbox.JSONDATA[_opts.jsondataid] = {};
    Componbox.tohtml[_opts.jsondataid] = {};
    _opts._Dom.addClass('rootboxhide');
    if(!_opts.Eventparentbox && !_opts.renderTo){
     Componbox.loadingdata(_opts);
    }
    Componbox.createcss(_opts);

  }

//暴露
  fieldsetPlugin.Render = function() {

    var _opts = this.constructor.opts;
    if (_opts._Dom.length > 0) {
      _opts._Dom.removeClass('rootboxhide');
      Componbox.loadingdata(_opts);
    } else {

      var errorobj = {
        txt: "没有容器",
        error: ""
      };
      Componbox.error(_opts, errorobj);

    }

  }


  fieldsetPlugin.AddEvent = function(p_jqdom) {
    var _opts = this.constructor.opts;
    Componbox._addevents(p_jqdom, _opts);
  }

//暴露
  fieldsetPlugin.On = function(events, callback) {

    //写入用户事件
    var _opts = this.constructor.opts;
    var onevents = Componbox.ONEVENTS[_opts.jsondataid];
    if(events!="" && typeof events!="undefined"){
    onevents[events]=callback;
    }

  };



  var Componbox = {
    ConfingDATA: {},
    //事件表
    ONEVENTS: {},
    tohtml:{},
    //控件数据表
    JSONDATA: {},
    error: function(opts, errorobj) {

      if (opts.error) {
        opts.error(errorobj);
      } else {

        console.log(errorobj.txt);
        //alert(errorobj.txt);
      }

      return false;

    },
    loadingdata: function(_opts) {

      //var _this = this;
      //var _opts = _this.opts;
      var _dataurl = _opts.dataUrl;
      var dataname = _opts.jsondataid;
      Componbox.JSONDATA[dataname] = [];
      
      if(_dataurl.url){
        _dataurl=_dataurl.url;
      }    


      //------------data----- 
      if (typeof _dataurl == "string" && typeof _dataurl != "undefined") {
        var paramdata = {};
        if (_opts.postData) {
          paramdata = _opts.postData;
        }

        $.getJSON(_dataurl, paramdata, function(json) {
        
         Componbox.JSONDATA[dataname] = json;
          if(_opts.dataUrl.formatjson){
              Componbox.JSONDATA[dataname] = _opts.dataUrl.formatjson(json);
          }
          
          Componbox.createboxhtml(_opts);

        }).error(function(e) {

          var errorobj = {
            txt: "服务端请求失败",
            error: e
          };
          Componbox.error(_opts, errorobj);

        });

      } else if (typeof _dataurl == "object" && !_opts.dataUrl.url) {

        Componbox.JSONDATA[dataname] = _dataurl;        
        Componbox.createboxhtml(_opts);

      } else {

        var errorobj = {
          txt: "数据无效",
          error: ""
        };
        Componbox.error(_opts, errorobj);

      }

    },
    createcss: function(opts) {


      var cssfileall = [
        "require_css!bootstrap-css/bootstrap.min",
        "require_css!Font-Awesome",
        "require_css!oso.lib/fieldsetControls/css/style.css"
      ];

      require(cssfileall);

    },
    createboxhtml: function(_opts) {

      var _this = Componbox;
      var p_dom = _opts._Dom;
      var JSONDATA = _this.JSONDATA[_opts.jsondataid];
      var ONEVENTS=_this.ONEVENTS[_opts.jsondataid]
      var tag = JSONDATA.Rootbox.tag;
      //进入标记模块
      _this[tag](p_dom, JSONDATA, _opts, true);


      _this.tohtml[_opts.jsondataid]=_opts._Dom.html();
     

      if (ONEVENTS["tohtml"]) {

        var _html = _opts._Dom.html();
        ONEVENTS["tohtml"]({
          tohtml: _html,
          tojson: JSONDATA
        });

      } 

//var _dataurl=_opts.dataUrl;
// if (typeof _dataurl == "string" && typeof _dataurl != "undefined") {
 //      Componbox._addevents(_opts._Dom, _opts);
//}
     
      if(ONEVENTS["Initialize"]){

         ONEVENTS["Initialize"](_opts);
      }



    },

    Title: function(Titledata) {

      var Title = "";

      if (typeof Titledata == "object") {

        var _html = JSONTOHTML.transform({}, Titledata);

        var jqlegend = $(_html);

        //jqlegend.addClass('rootfieldset');

        var children = Titledata.children;



        if (Titledata.children) {

          var lefthtml = "",
            middlehtml = "",
            righthtml = "";

          if (children.left) {

            if (!children.right && !children.middle) {

              jqlegend.attr("align", "left").addClass('root_title_legend');

            }

            if (typeof children.left == "string") {

              lefthtml = children.left;

              lefthtml = '<span class="root_title_left">' + lefthtml + '</span>';

            } else {

              lefthtml = JSONTOHTML.transform({}, children.left);
              var leftobj = $(lefthtml);
              leftobj.addClass('root_title_left');
              lefthtml = leftobj[0].outerHTML;

            }


          }

          if (children.middle) {


            if (!children.right && !children.left)
              jqlegend.attr("align", "center");

            if (typeof children.middle == "string") {

              middlehtml = children.middle;
              middlehtml = '<span class="root_title_middle">' + middlehtml + '</span>';

            } else {

              middlehtml = JSONTOHTML.transform({}, children.middle);
              var middleobj = $(middlehtml);
              middleobj.addClass('root_title_middle');
              middlehtml = middleobj[0].outerHTML;

            }


          }

          // console.log(jqlegend);

          Title = jqlegend.html(lefthtml + middlehtml);

          Title = Title[0].outerHTML;

          if (children.right) {

            jqlegend.attr("align", "left");

            if (typeof children.right == "string") {

              righthtml = '<span class="root_title_right">' + children.right + '</span>';

            } else {
              righthtml = JSONTOHTML.transform({}, children.right);
              var tojqhtml = $(righthtml).addClass('root_title_right');
              righthtml = tojqhtml[0].outerHTML;
            }

            Title = righthtml + jqlegend[0].outerHTML;
          }

          if (children.right) {

            Title = '<div class="root_title">' + jqlegend.html() + righthtml + '</div>';

          }


        }

      }


      return Title;


    },
    Foot: function(Footdata) {

      var foothtml = "";

      if (typeof Footdata == "object") {

        if (Footdata.tag) {
          foothtml = JSONDATA.transform({}, Footdata);
          foothtml = $(foothtml);
        }

        var lefthtml = "",
          middlehtml = "",
          righthtml = "";

        if (Footdata.left) {

          if (typeof Footdata.left == "string") {

            lefthtml = '<span class="Footleft">' + Footdata.left + '</span>';

          }

          if (typeof Footdata.left == "object") {

            lefthtml = JSONTOHTML.transform({}, Footdata.left);
            var leftobj = $(lefthtml);
            leftobj.addClass('Footleft');
            lefthtml = leftobj[0].outerHTML;
          }

        }


        if (Footdata.middle) {


          if (typeof Footdata.middle == "string") {

            middlehtml = '<span class="Footmiddle">' + Footdata.middle + '</span>';

          }

          if (typeof Footdata.middle == "object") {

            middlehtml = JSONTOHTML.transform({}, Footdata.middle);
            var middleobj = $(middlehtml);
            middleobj.addClass('Footmiddle');
            middlehtml = middleobj[0].outerHTML;

          }

        }

        if (Footdata.right) {

          if (typeof Footdata.right == "string") {

            righthtml = '<span class="Footright">' + Footdata.right + '</span>';

          }

          if (typeof Footdata.right == "object") {

            righthtml = JSONTOHTML.transform({}, Footdata.right);

            var rightobj = $(righthtml);
            rightobj.addClass('Footright');
            righthtml = rightobj[0].outerHTML;

          }

        }

        var foobox_center = "";
        if (!Footdata.left && !Footdata.right) {

          foobox_center = ' foot_center';

        }

        if (typeof foothtml != "undefined" && foothtml != "") {

          if (typeof foothtml == "object" && Footdata.right || Footdata.middle || Footdata.left) {

            foothtml.html(lefthtml + middlehtml + righthtml);

            foothtml.addClass('fieldsetboot' + foobox_center + '');

          }

        } else {

          foothtml = '<div class="fieldsetboot' + foobox_center + '">' + lefthtml + middlehtml + righthtml + '</div>';

        }

      }


      return foothtml;


    },
    fieldset: function(p_dom, JSONDATA, _opts, sub) {

      var _this = Componbox;

      //第一容器
      var labelfomat = JSONDATA.Rootbox;

      var roothtml = JSONTOHTML.transform({}, labelfomat);

      var Rootlabel = $(roothtml);

      Rootlabel.addClass('rootfieldset');

      var titlehtml = "",
        foothtml = "",
        Contenthtml = "";

      //标题
      if (JSONDATA.Title) {

        titlehtml = _this.Title(JSONDATA.Title);

      }

      var __Content = '<span class="nodata">无数据...</span>';

      if (JSONDATA.Content[0] && typeof JSONDATA.Content[0] == "string" && JSONDATA.Content.length == 1) {

        __Content = JSONDATA.Content;

      }


      Contenthtml = '<div class="fieldsetContent">' + __Content + '</div>';

      //脚

      if (JSONDATA.Foot) {

        foothtml = _this.Foot(JSONDATA.Foot);

      }

      //提前创建--Content
      var allhtml = Rootlabel.html(titlehtml + Contenthtml + foothtml);
      var newdomid = "_sub_" + $.now();
      allhtml.attr("controlsid", newdomid);

      var rehtmls = allhtml[0].outerHTML;

      $(rehtmls).appendTo(p_dom);
      //身体
      var subdom = p_dom.find('[controlsid="' + newdomid + '"]');

      // if (!/_fieldset_/g.test(_opts.jsondataid) && rehtmls != "")
      //   _this._addevents(subdom, _opts);


      if (JSONDATA.Content && JSONDATA.Content.length > 0 && typeof JSONDATA.Content[0] == "object") {

        var sub_p_dom = subdom.find(".fieldsetContent");

        if (sub_p_dom.find('.nodata').length > 0) {
          sub_p_dom.empty();
        }

        for (var c_ = 0; c_ < JSONDATA.Content.length; c_++) {
          var c_tag = JSONDATA.Content[c_];
          //递归标记模块
          var subtag = c_tag.Rootbox.tag;
          _this[subtag](sub_p_dom, c_tag, _opts);

        }

      }

      subdom.removeAttr('controlsid');


    },
    ul: function(p_dom, JSONDATA, _opts) {

      var _this = Componbox;
      //第一容器
      var labelfomat = JSONDATA.Rootbox;
      var roothtml = JSONTOHTML.transform({}, labelfomat);
      var divider = true;
      if (labelfomat.divider) {
        divider = labelfomat.divider;
      }

      var Rootlabel = $(roothtml);
      var newdomid = "_sub_" + $.now();
      Rootlabel.attr("controlsid", newdomid);
      $(Rootlabel[0].outerHTML).appendTo(p_dom);

      var subdom = $('[controlsid="' + newdomid + '"]');

      var liall = JSONDATA.Content;
      var column = liall[0];
      var lihtmlset = '<span class="nodata">无数据...</span>';
      if (typeof column == "string" && liall) {

        lihtmlset = liall;

      }


      var lihtml = '<li>' + lihtmlset + '</li>';

      //一列时
      if (!$.isArray(column) && liall.length > 0 && typeof column == "object") {
        lihtml = "";
        var box1px = "";
        for (var likey in liall) {

          var itmeli = liall[likey];

          if (lihtml != "" && divider) {
            box1px = '<li class="borderbottom1px"></li>';
          }

          if (typeof itmeli == "object") {
            lihtml += box1px + '<li>' + JSONTOHTML.transform({}, itmeli) + '</li>';
          }

          if (typeof itmeli == "string") {

            lihtml += box1px + '<li>' + itmeli + '</li>';

          }

        }

      }


      //多列时
      if ($.isArray(column) && liall.length > 0 && typeof column == "object") {
        lihtml = "";
        var box1px = "";
        for (var colkey in liall) {

          var rowitme = liall[colkey];
          var rowhtml = "";
          for (var td = 0; td < rowitme.length; td++) {
            var tditme = rowitme[td];
            if (typeof tditme == "object") {
              rowhtml += JSONTOHTML.transform({}, tditme);
            }
            if (typeof tditme == "string") {
              rowhtml += '<span>' + tditme + '</span>';
            }
          }

          if (lihtml != "" && divider) {
            box1px = '<li class="borderbottom1px"></li>';
          }

          lihtml += box1px + '<li>' + rowhtml + '</li>';

        }


      }


      $(lihtml).appendTo(subdom);
      var subdom = $('[controlsid="' + newdomid + '"]');

      // if (!/_fieldset_/g.test(_opts.jsondataid) && lihtml != "") {
      //   _this._addevents(subdom, _opts);
      // }


      subdom.removeAttr('controlsid');

    },
    table: function(p_dom, JSONDATA, _opts) {

    

      var _this = Componbox;
      //第一容器
      var labelfomat = JSONDATA.Rootbox;
      var roothtml = JSONTOHTML.transform({}, labelfomat);
      var table = $(roothtml); //.addClass('table table-condensed');
      var thead = JSONDATA.thead;
      var tbody = JSONDATA.tbody;
      var _tablehtml = "";

      if (thead.length > 0 && tbody.length > 0) {

        var thead_tr = "";
        for (var thekey in thead) {
          var _th = thead[thekey];
          if (typeof _th == "string") {
            thead_tr += '<th>' + _th + '</th>';
          }

          if (typeof _th == "object") {
            thead_tr += '<th>' + JSONTOHTML.transform({}, _th); + '</th>';
          }

        }

        thead_tr = '<thead><tr>' + thead_tr + '</tr></thead>';


        var tbody_tr = "";
        for (var tbodykey in tbody) {
          var _tr = tbody[tbodykey];

          var tbody_td = "";
          for (var _tdkey in _tr) {
            var _td = _tr[_tdkey];
            if (typeof _td == "string") {
              tbody_td += '<td>' + _td + '</td>';
            }

            if (typeof _td == "object") {
              tbody_td += '<td>' + JSONTOHTML.transform({}, _td); + '</td>';
            }

          }

          tbody_tr += '<tr>' + tbody_td + '</tr>';


        }

        tbody_tr = '<tbody>' + tbody_tr + '</tbody>';

        table.html(thead_tr + tbody_tr);

        _tablehtml = table[0].outerHTML;
      }


      $(_tablehtml).appendTo(p_dom);
      // if (!/_fieldset_/g.test(_opts.jsondataid) && _tablehtml != "") {
      //   _this._addevents(p_dom, _opts);
      // }

    },
    _addevents: function(subdom, _opts) {

      //创建事件      
      
      var event_obj_all = subdom.find('[data-eventname*="events."]');

      if (event_obj_all.length > 0) {

         var onevents = Componbox.ONEVENTS[_opts.jsondataid];

         //data-eventname

        for (var i = 0; i < event_obj_all.length; i++) {

          var itmeobj = event_obj_all.eq(i);
          var datasring = itmeobj.data("eventname");
          if (datasring != "" && typeof datasring != "undefined") {

            var Eventparentbox=$("body");
            if(_opts.Eventparentbox){
              Eventparentbox=$(_opts.Eventparentbox);
            }

            Eventparentbox.delegate(itmeobj,"click",function(){
             
                if (onevents[datasring]) {
                var btnthis = $(this);
                onevents[datasring](btnthis);

              }
           });

          }
        }

      }

      return false;

    }


  };



  function isElement(o) {
    return !!o && (typeof o === 'object') && (
      /object|function/.test(typeof Element) ? o instanceof Element //DOM2
      : o.nodeType === 1 && typeof o.nodeName === "string");
  }

  function isObject(thing) {
    return thing instanceof Object;
  }

  function isArray(thing) {
    return thing instanceof Array;
  }

  function isFunction(thing) {
    return typeof thing === 'function';
  }

  function isNumber(thing) {
    return typeof thing === 'number';
  }

  function isBool(thing) {
    return typeof thing === 'boolean';
  }

  function isString(thing) {
    return typeof thing === 'string';
  }


  return fieldsetPlugin;
}));