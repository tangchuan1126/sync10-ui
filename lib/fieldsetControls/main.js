require(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

  require(["oso.lib/nprogress/nprogress.min", "jquery", "require_css!oso.lib/nprogress/nprogress", "require_css!bootstrap-css/bootstrap.min"],
    function(NProgress, $) {

      $("body").show();

      NProgress.start();

      var cssfileall = [
        "oso.lib/CodeMirror/mode/javascript/javascript",
        "oso.lib/CodeMirror/lib/util/foldcode",
        "require_css!oso.lib/CodeMirror/lib/codemirror.css",
        "require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css",
        "bootstrap"
      ];


      require(cssfileall, function() {


        require(["oso.lib/fieldsetControls/fieldset", "art_Dialog/dialog-plus"], function(fieldset, dialog) {


          $(function() {             


            var _fieldset_data = {
              renderTo: "#Compond_box",
              //容器
              dataUrl:"./d.json"
            };

            var _fieldset_liste = new fieldset(_fieldset_data); 
            

            //再组装html时耗时，这里用监听方式取回数据
            _fieldset_liste.on("tohtml", function(e) {

              console.log(e.tohtml);
              //console.log(e.tojson);
              //事件，父容器--最好规定个范围这样来提高jq效率
              //var abc=$("#td123");
              //_fieldset_liste.addEvent(abc);

            });

          _fieldset_liste.on("events.leftclick",function(e){
             console.log(e);
          });

            _fieldset_liste.on("Initialize",function(){

            var brotab = $("#tab_content");
              brotab.show();

            require(["oso.lib/CodeMirror/demo/CodeMirror_demo"], function() {
                        
             if (brotab.length > 0) {
               brotab.find("div[role='tabpanel']").addClass('tab-pane');
             }


            });
           
           //var abc=$("#Compond_box");
           //_fieldset_liste.addEvent(abc);

          });


            _fieldset_liste.render();           
         




            NProgress.done();


          });
          // window end

        });
        //fieldset end

      });
      // codemirror end

    });
  // requirejs_config end
});
// nprogress end