
//基本单位--1
{

//ul---列表方式

"Rootbox":{"tag":"ul","class":"subul","id":"ul_234"}	

}


//基本单位--2
{

//fieldset---方式

"Rootbox":{"tag":"fieldset","class":"aastyle","id":"fieldset_234"}	

}

//基本单位 --3

//table ---方式

{
	"Rootbox":{"tag":"table","class":"table table-condensed","id":"rrw11234"},
	"thead":["a1","a2","a3"],
	"tbody":[["111","222",{"tag":"select","id":"arrs","html":"<option>Volvo</option>"}],["444","555","666"]]
}



Rootbox -- Content 数据模式

模式1

 "Content":[{},{}]

模式2

"Content":["1123345"] //比如可以html --"<span>html</span>"

//===================
//列表内容方式
//divider==true时输出分割线

 {
	 	"Rootbox":{"tag":"ul","class":"subul","id":"ul_234"},
	 	//一列可以如下
         "Content":[{"tag":"span","html":"112"},{"tag":"span","html":"456"}]
         //也可以如下
         "Content":[112,456]
         //多列时如下
          "Content":[
           [{"tag":"span","html":"0-112"},{"tag":"span","html":"0-456"}],
           [{"tag":"span","html":"1-112"},{"tag":"span","html":"1-456"}]
	 	 ]
	 	 //也可以如下--
	 	   "Content":[
           ["0-112","0-456"],
           ["1-112","1-456"]
	 	 ]
	 }
