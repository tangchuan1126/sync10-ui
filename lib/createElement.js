define(["jquery", "underscore"], function($) {


  var createfileclass = {

    _default: {
      renderTo: document.getElementsByTagName("head")[0],
      dataUrl: null,
      Monitorthenumber:0
    },
    _int: function(obj) {

      var _def = createfileclass._default;

      _def = _.extend(_def, obj);

      return {
        render: createfileclass.render,
        on: createfileclass.events
      };

    },
    fileonloadEvent:function(fileobj){
       
    if(fileobj.attachEvent){ // IE
       fileobj.attachEvent('onload',fileload);
      }else{ // nonIE
     fileobj.onload = fileload;
     }

     function fileload(){
      
      var _this=createfileclass;
      var _def = _this._default;
       _def.Monitorthenumber--; 
     if(_def.Monitorthenumber == 0 && _def.fileload){
          
        _def.fileload();
        return false; 
       }         

     }


    },
    create: function() {

      //{tag:"",id:"",rel:"",type:"",media:"",href:"",src:""}
     var _this=createfileclass;

      var _def = _this._default;
      var dataUrl = _def.dataUrl;
      if (typeof dataUrl == "object" && dataUrl.length > 0) {

        var createsring = "";

        for (var key in dataUrl) {

          var fileitme = dataUrl[key];
          if (_.has(fileitme, "tag"))
            createsring = document.createElement(fileitme.tag);
          if (_.has(fileitme, "id") && !_.has(fileitme, "classtxt"))
            createsring.id = fileitme.id;
          if (_.has(fileitme, "rel"))
            createsring.rel = fileitme.rel;
          if (_.has(fileitme, "type"))
            createsring.type = fileitme.type;
          if (_.has(fileitme, "media"))
            createsring.media = fileitme.media;

          if (_.has(fileitme, "href")) {

            if (fileitme.href != "" && typeof fileitme.href != "undefined") {
              createsring.href = fileitme.href;
              _def.Monitorthenumber++;
              _this.fileonloadEvent(createsring);
            }

          }

          if (_.has(fileitme, "src")) {

            if (fileitme.src != "" && typeof fileitme.src != "undefined") {
              createsring.src = fileitme.src;
              _def.Monitorthenumber++;
              _this.fileonloadEvent(createsring);
            }

          }

          if (_.has(fileitme, "lable")) {

            var lableid = document.getElementById(fileitme.id);
            if (lableid == null) {

              $("<" + fileitme.lable + " id='" + fileitme.id + "'>").appendTo("head");

              if (fileitme.lable == "style" && _.has(fileitme, "classtxt")) {

                $("#" + fileitme.id).html(fileitme.classtxt);

              } else {

                createsring.id = "";

                $("#" + fileitme.id)[0].appendChild(createsring);

              }


            }

          } else {

            var fileid = document.getElementById(fileitme.id);
            if (fileid == null) {
              _def.renderTo.appendChild(createsring);
            }

          }


        }


      }



    },
    render: function() {

      createfileclass.create();

    },events: function(functionname, callback) {

      var _def = createfileclass._default;

      switch (functionname) {
        case "events.Error":
          _def.treeError = callback;
          break;
        case "events.fileload":
          _def.fileload = callback;
          break;
      }

    }



  }


  return createfileclass._int;

});