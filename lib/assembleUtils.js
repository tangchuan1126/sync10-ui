function assembleTemplate(data, content){  
      var target;
      if(typeof(data) == 'undefined' || data == null || data == ""){
        return content;
      }
      for(var i in data){    
        i = i.toUpperCase();
        var placeHolder = ["{"+i+"}", "$"+i+"$"];
        for(var t in placeHolder){
          var index = content.indexOf(placeHolder[t]);
          //在content中发现data中的key值
          if(index >= 0){        
            target = data[i];      
            content = content.replace(placeHolder[t], target);
          }
        }
        
      }
      return content;
    }