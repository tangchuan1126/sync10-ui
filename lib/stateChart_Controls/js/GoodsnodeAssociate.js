;
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register 
        define(['', 'jquery', 'Gaojs'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'), require('Gaojs')(root));

    } else {
        // Browser globals
        root.GoodsnodeAssociate = factory(root, root.$);
    }
}(this, function(root, $) {
    'use strict';

    var gomake,
        myDiagram;


    var Goodsnode = function(options) {
        this.options;
        if (typeof options == "object" && options != "") {

            this.options = options;
        } else {
            return false
        }


        if (window.goSamples) goSamples();
        gomake = Gao.GraphObject.Picture;



    }

    Goodsnode.prototype = {
        render: function() {
            var this_ = this;
            var options = this_.options;

            if (options == "" || typeof options == "undefined") {
                return false
            }

            var texts = this_.select();
            this_.DiagramTemplate(texts);

        },
        ExportJson: function() {
            //装载数据
            var this_ = this;
            var options = this_.options;

            var json = myDiagram.model.toJson();
            var inputobj = $(options.EnterJson)
            if (inputobj.length > 0) {
                inputobj.val(json);
            }
            myDiagram.isModified = false;
            return json
        },
        GaoJS_Fn: function() {
            //反回画布对象全局
            return myDiagram;
        },
        select: function() {

            var this_ = this;
            var options = this_.options;
            var select = $(options.select);

            select.change(function() {
                var this_select = $(this);
                var text_ = this_select.find('option:selected');
                var slectjson = {
                    text: text_.text(),
                    key: text_.val()
                }
                this_.ReFirstnodesval(slectjson.text);

            });

            return select.find('option:selected').text()

        },
        ReFirstnodesval: function(slectjson) {

            //选中时修改left
            var this_ = this;
            var options = this_.options;
            var model = myDiagram.model;

            var toorlinkall = model.linkDataArray;
            //可以加个是否保留其他节点连线
            //清除其他opint线  
            var dlelinkall = [];
            for (var keylink = 0; keylink < toorlinkall.length; keylink++) {
                var itmeslink = toorlinkall[keylink];
                if (itmeslink.category != "Sourcelink") {
                    dlelinkall.push(itmeslink);
                }
            }


            if (dlelinkall.length > 0) {

               // console.dir(dlelinkall);

                model.startTransaction("del link");

                for (var delkey in dlelinkall) {
                    var itmedata = dlelinkall[delkey];
                    model.removeLinkData(itmedata);
                }

                model.commitTransaction("enddel link");

            }



            model.startTransaction("up increment");
            var nodefirst = model.findNodeDataForKey("-1");
            model.setDataProperty(nodefirst, "text", slectjson);
            model.commitTransaction("end increment");
            if (typeof options.Selectlinkjson == "object") {
                var uplinkdata = options.Selectlinkjson["" + slectjson + ""];
                this_.AaddLinkDate(uplinkdata);
            }

        },
        AaddLinkDate: function(linkdata) {
            if (typeof linkdata != "object") {
                return false;
            }
            var model = myDiagram.model;
            //add
            model.startTransaction("add link");
            for (var addkey in linkdata) {
                var additmelink = linkdata[addkey];

                model.addLinkData(additmelink);
            }
            model.commitTransaction("add link");

        },
        DiagramTemplate: function(texts) {

            var this_ = this;
            var options = this_.options;

            var yellowgrad = gomake(Gao.Brush, Gao.Brush.Linear, {
                0: "rgb(254, 201, 0)",
                1: "rgb(254, 162, 0)"
            });
            var greengrad = gomake(Gao.Brush, Gao.Brush.Linear, {
                0: "#98FB98",
                1: "#9ACD32"
            });
            var bluegrad = gomake(Gao.Brush, Gao.Brush.Linear, {
                0: "#B0E0E6",
                1: "#87CEEB"
            });
            var redgrad = gomake(Gao.Brush, Gao.Brush.Linear, {
                0: "#C45245",
                1: "#7D180C"
            });
            var whitegrad = gomake(Gao.Brush, Gao.Brush.Linear, {
                0: "#F0F8FF",
                1: "#E6E6FA"
            });

            var bigfont = "14px Helvetica, Arial, sans-serif";
            var smallfont = "bold 13pt Helvetica, Arial, sans-serif";

            // Common text styling
            function textStyle() {
                return {
                    margin: 6,
                    wrap: Gao.TextBlock.WrapFit,
                    textAlign: "center",
                    editable: false,
                    font: smallfont
                }
            }

            myDiagram =
                gomake(Gao.Diagram, "Goodsno_Diagram", {

                    "toolManager.mouseWheelBehavior": Gao.ToolManager.WheelZoom,
                    allowMove: false,
                    allowDrop: true,
                    initialAutoScale: Gao.Diagram.Uniform,
                    "linkingTool.direction": Gao.LinkingTool.ForwardsOnly,
                    initialContentAlignment: Gao.Spot.Center,
                    // layout: gomake(Gao.LayeredDigraphLayout, { isInitial: false, isOngoing: false, layerSpacing: 50 }),
                    layout: gomake(Gao.TreeLayout, {
                        alignment: Gao.TreeLayout.AlignmentStart,
                        angle: 0,
                        compaction: Gao.TreeLayout.CompactionNone,
                        layerSpacing: 16,
                        layerSpacingParentOverlap: 1,
                        nodeIndent: 2,
                        nodeIndentPastParent: 0.88,
                        nodeSpacing: 0,
                        setsPortSpot: false,
                        setsChildPortSpot: false
                    }),
                    "undoManager.isEnabled": true
                });


            myDiagram.addDiagramListener("AnimationStarting", function(e) {
                this_.ResetTimeout(10);
            });


            myDiagram.addDiagramListener("DocumentBoundsChanged", function(e) {
                this_.Relocation();
            });


            myDiagram.addDiagramListener("Modified", function(e) {

                var button = document.getElementById("SaveButton");
                if (button) button.disabled = !myDiagram.isModified;
                var idx = document.title.indexOf("*");
                if (myDiagram.isModified) {
                    if (idx < 0) document.title += "*";
                } else {
                    if (idx >= 0) document.title = document.title.substr(0, idx);
                }
            });

            var defaultAdornment =
                gomake(Gao.Adornment, "Spot",
                    gomake(Gao.Panel, "Auto",
                        gomake(Gao.Shape, {
                            fill: null,
                            stroke: "dodgerblue",
                            strokeWidth: 2
                        }),
                        gomake(Gao.Placeholder)),
                    gomake("Button", {
                            alignment: Gao.Spot.TopRight,
                            click: addNodeAndLink
                        },
                        new Gao.Binding("visible", "", function(a) {
                            return !a.diagram.isReadOnly;
                        }).ofObject(),
                        gomake(Gao.Shape, "PlusLine", {
                            desiredSize: new Gao.Size(6, 6)
                        })
                    )
                );

            //默认节点
            myDiagram.nodeTemplate =
                gomake(Gao.Node, "Spot", {
                        selectionAdornmentTemplate: defaultAdornment
                    },
                    new Gao.Binding("location", "loc", Gao.Point.parse).makeTwoWay(Gao.Point.stringify),
                    gomake(Gao.Panel,"Auto",
                    gomake(Gao.Shape, "Rectangle", {
                            fill: null,
                            strokeWidth: 0,
                            portId: "",
                            toLinkable: true,
                            cursor: "pointer",
                            toEndSegmentLength: 50,
                            fromEndSegmentLength: 40
                        },
                        new Gao.Binding("fill", "isSelected", function(sel) {
                            return sel ? "dodgerblue" : null;
                        }).ofObject()),
                    gomake(Gao.TextBlock, "Page", {
                            margin: new Gao.Margin(3,2),
                            font: bigfont,
                            editable: true
                        },
                        new Gao.Binding("text", "text").makeTwoWay())),
                    gomake("TreeExpanderButton", {
                        alignment: Gao.Spot.Left,
                        alignmentFocus: Gao.Spot.Right
                    },{ visible: false })
                );


            //出发线
            myDiagram.nodeTemplateMap.add("Source",
                gomake(Gao.Node, "Vertical",
                    new Gao.Binding("location", "loc").makeTwoWay(),
                    gomake(Gao.Shape, "Ellipse", {
                        fill: bluegrad,
                        width: 45,
                        height: 45,
                        portId: "",
                        fromLinkable: true,
                        cursor: "pointer",
                        fromEndSegmentLength: 40
                    }),
                    gomake(Gao.TextBlock, "Source", {
                            editable: false
                        }, textStyle(),
                        new Gao.Binding("text", "text").makeTwoWay())
                ));


            function addReason(e, obj) {
                var adorn = obj.part;
                if (adorn === null) return;
                e.handled = true;
                //var list = adorn.adornedPart.findObject("ReasonList");
                var arr = adorn.adornedPart.data.reasonsList;
                // and add it to the Array of port data
                myDiagram.startTransaction("add reason");
                myDiagram.model.addArrayItem(arr, {});
                myDiagram.commitTransaction("add reason");
            }

            //添加节点线条时
            function addNodeAndLink(e, obj) {
                var adorn = obj.part;
                if (adorn === null) return;
                e.handled = true;
                var diagram = adorn.diagram;
                diagram.startTransaction("Add State");
                var fromNode = adorn.adornedPart;
                var fromData = fromNode.data;
                var toData = {
                    text: "new"
                };
                var p = fromNode.location;
                toData.loc = p.x + 200 + " " + p.y;
                var model = diagram.model;
                model.addNodeData(toData);
                var linkdata = {};
                linkdata[model.linkFromKeyProperty] = model.getKeyForNodeData(fromData);
                linkdata[model.linkToKeyProperty] = model.getKeyForNodeData(toData);
                linkdata.category = "Sourcelink";
                model.addLinkData(linkdata);
                var newnode = diagram.findNodeForData(toData);
                diagram.select(newnode);
                diagram.commitTransaction("Add State");
            }

           //连线样式
          myDiagram.linkTemplate =
            gomake(Gao.Link,
              { routing: Gao.Link.Orthogonal,
                corner: 10 }, 
             gomake(Gao.Shape),
              gomake(Gao.Shape, { toArrow: "Standard" })
            );


            //默认连线
            // myDiagram.linkTemplate =
            //     gomake(Gao.Link,
            //      // the whole link panel
            //         new Gao.Binding("points").makeTwoWay(), {
            //             curve: Gao.Link.Bezier,
            //             toShortLength: 15
            //         },
            //         new Gao.Binding("curviness", "curviness"),
            //         gomake(Gao.Shape, // the link shape
            //             {
            //                 isPanelMain: true,
            //                 stroke: "#2F4F4F",
            //                 strokeWidth: 1
            //             }),
            //         gomake(Gao.Shape, // the arrowhead
            //             {
            //                 toArrow: "kite",
            //                 fill: "#2F4F4F",
            //                 stroke: null,
            //                 scale: 2
            //             })
            //     );


            //树结构图线
            myDiagram.linkTemplateMap.add("Sourcelink",
                gomake(Gao.Link));

            myDiagram.model = this_.EnterJson();

            this_.ReFirstnodesval(texts);

            myDiagram.layoutDiagram(true);

            this_.ResetTimeout(100);
            if (options.Initialization && typeof options.Initialization == "function") {

                setTimeout(function() {

                    options.Initialization();

                }, 110);

            }

        },
        EnterJson: function() {
            //装载数据
            var this_ = this;
            var options = this_.options;
            var json = $(options.EnterJson)[0];
            return Gao.Model.fromJson(json.value);
        },
        ResetTimeout: function(tim) {
            var this_ = this;
            window.setTimeout(function() {
                this_.Relocation();
            }, tim);

        },
        Relocation: function() {

            myDiagram.startTransaction("shift node");
            var data = myDiagram.model.nodeDataArray[0];
            var node = myDiagram.findNodeForData(data);
            var p = node.location.copy();
            p.x = -200;
            p.y = 250;
            node.location = p;
            myDiagram.commitTransaction("shift node");
        }
    };

    return Goodsnode;

}));
