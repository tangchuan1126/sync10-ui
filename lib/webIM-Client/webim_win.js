/*
 * webim_win.js
 * Copyright 2014, Connor
 * http://127.0.0.1/Sync10-ui/ilb/Miniwindow/
 */
;
(function(root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register 
		define(['jquery','strophe','oso.lib/Miniwindow/nicEdit','oso.lib/Miniwindow/lhgdialog/lhgdialog', 'mCustomScrollbar','./Recorder','require_css!fancybox/source/jquery.fancybox','fancybox/source/jquery.fancybox'], factory);
	} else if (typeof module !== 'undefined' && module.exports) {
		// CommonJS
		module.exports = factory(require('jquery'),require('bootstrap'),require('./lhgdialog/lhgcore.lhgdialog.min'), require('mCustomScrollbar')(root));

	} else {
		// Browser globals
		//root===Window
		root.webim_win = factory(root.$,root.Strophe);
	}
}(this, function($,Strophe) {
	'use strict'; 
 

	var _public = {},
		_private = {},
		webim_win=function(opts){

		if (!(this instanceof webim_win))
			return new webim_win(opts);

		if (typeof opts === 'string')
			opts = {
				renderTo: opts
			};

		//test
		opts.nohover = false;
		//默认ctrl+enter发送
        opts.enters=true;

		var dataname = "_webinbox_" + $.now();
		opts._Dom = $('<div id="' + dataname + '"></div>');

		if (opts.renderTo && opts.renderTo != "" && typeof opts.renderTo != "undefined") {
			dataname = opts.renderTo.replace(/\.|\/|#/g, "");
			var _todombox = $(opts.renderTo);
			if (_todombox.length > 0) {
				opts._Dom = _todombox;
			}
		}

		opts.jsondataid = dataname + "_" + $.now();
		this._opts = opts;

		_private.int(opts);

		var publicapi = {

			on: _public.on,
			_opts: opts

		};

		return publicapi;

	};

	//连接状态
	_private.Connectionstatus = false;

	_private.events = {};
	_public.on = function(namefn, callback) {

		var __this = this._opts;
		var events = _private.events[__this.jsondataid];
		if (typeof events == "undefined" || events == "") {
			events = _private.events[__this.jsondataid] = {};
		}
		events[namefn] = callback;


	};

	//写入没有打开聊天窗口的用户信息
	_private.message = {};
	_private.addusermessage = function(_opts, userjson) {

		//切换到在线理性处理
		if (userjson.type == "presence") {

			_private.Onlineoroffline(_opts, userjson);

			return false;

		}

			//切换到消息通知处理
		if(userjson.type == "notice"){

             _private.alertsaction(_opts, userjson);


              return false;
			}

		//Object { time: "2014-12-24 12:33:07", userid: "100051", msg: "344443" }
        var msglen=[];
		if(userjson.userid!="" && typeof userjson.userid!="undefined" && userjson.userid!=null){

          
		if (_private.message[userjson.userid]) {

			_private.message[userjson.userid].push(userjson);

		} else {
			_private.message[userjson.userid] = [];

			_private.message[userjson.userid].push(userjson);

		}

		msglen = _private.message[userjson.userid].length;

	  }

		//var msglen = _private.message[userjson.userid].length;

		//写入--信息数

		if (msglen > 0) {

			var msgnumber_li = $("li[id='osouser_itme_" + userjson.userid + "']");

          //
			_opts.delHistroy=false;
			_opts.chatId="";

			var msgnumber_box = msgnumber_li.find(".col-md-1");
			msgnumber_box.html("(" + msglen + ")");

			//拿用户id做key-------------set

			var spearrys=_private.Speakseveral;
            
			if(typeof spearrys[0] !="undefined" &&  spearrys.length > 0){

              var adduserids=true;

				for(var uidkye in _private.Speakseveral){

					var userids=_private.Speakseveral[uidkye];
					if(userjson.userid != userids){
                      adduserids=false;
					}
				}

				if(!adduserids){

					_private.Speakseveral.push(userjson.userid);

				}


			}else{

				 _private.Speakseveral=[];

				_private.Speakseveral.push(userjson.userid);

			}

			//拿用户id做key-------------end

			msgnumber_li.parents("ul").eq(0).show();
			$(".WB_webimbox .bottombox .fa-comments-o").css("color", "#0F17E6");
         
         var visible_root=false;
          if(_opts.dialog){
			var _msgbox = _opts.dialog.DOM.dialog;
			var dialog_dom_root=_msgbox.parents("div").eq(0);
			 visible_root=(dialog_dom_root.css("visibility") =="visible");
		  }

           //启动闪动
           var set_speak=_private.Speakseveral;
			if(typeof set_speak[0] !="undefined" && set_speak.length > 0){

				_private.everymsgunread(_opts);

			}



		}   



	};


	//信息呼吸ions
	_private.msg_mationbreathing_ions=function(_opts){
       
        var WB_webimbtn=$(".WB_webimbtn");

      if(_opts.ionstimeset!="" && typeof _opts.ionstimeset!="undefined" && _opts.ionstimeset){
        	window.clearTimeout(_opts.ionstimeset);
        }

       var set_speak=_private.Speakseveral;
		
     
     if(WB_webimbtn.is(":visible") && typeof set_speak[0] !="undefined" && set_speak.length > 0){

        var iobj=WB_webimbtn.find("i");    
        _private.showImg(_opts,iobj[0]);

      }

	};

	_private.showImg=function(_opts,imgId){ 

	    if(imgId.style.visibility == "visible") 
	        imgId.style.visibility = "hidden"; 
	    else 
	        imgId.style.visibility = "visible";
	    _opts.ionstimeset=setTimeout(function(){
	    	_private.showImg(_opts,imgId);
	    },300);               
	//间隔的毫秒
	}

    //收到通知处理
    _private.alertsaarry="";
	_private.alertsaction=function(_opts, userjson){     
   	
		var box_ss=new Date().getMilliseconds();
		var sid="alerbox_"+box_ss+"_"+userjson.userid;
		var alertsinrhtml='<div class="lidivbox" id="'+sid+'"><ul class="msxbox"><li>发起人：'+userjson.userid+'</li><li>'+userjson.msg+'</li><li>'+userjson.time+'</li><li class="alertfoome"><button rel="'+sid+'" type="button" class="btn btn-default">知道了</button></li></ul></div>';

      var alertsobj=$("#alertsactionbox");
      var dlog=$(".ui_state_visible").parents("div").eq(0) || $(".WB_webimbtn");
      var _zindex=window.parseInt(dlog.css("z-index")) + 2;
      alertsobj.show().css("z-index",_zindex);
      var chonete=alertsobj.find("#alertsactioncenet");
      $(alertsinrhtml).appendTo(chonete);

      var btnobjt=chonete.find("button[rel='"+sid+"']");

      var foomtbtn=$(".WB_webimbox");
      var bullhorms=foomtbtn.find(".fa-bullhorn");
      bullhorms.css("color","#001DFF");
      var mgslen=chonete.find(".lidivbox").length;

      var Anglebox=foomtbtn.find(".Anglebox");
     if(mgslen > 0){     
       Anglebox.html(mgslen);
       Anglebox.css("visibility","visible");
   }


        btnobjt.click(function(event) {

        	   var thisob=$(this);
				event.stopPropagation();
				_opts.msgType=2;
				_opts.delHistroy=true;
				_opts.chatId=userjson.userid;
				var pdiv=thisob.parents(".lidivbox").eq(0);
                 pdiv.remove();

               var msglens=chonete.find(".lidivbox").length;
             
              if(msglens > 0){

               Anglebox.html(msglens);
               Anglebox.css("visibility","visible");

             }

               if(msglens < 1){
               	 bullhorms.css("color","#ccc");
               	 alertsobj.hide();
                Anglebox.css("visibility","hidden");

               }

				return false;
			});      


		return false;

	};



   //图片事件
   _private.imgidevente=function(domroot){

   	  var imgpptbtn=domroot.find("img[id^='imgid_']");

			   for(var imgs=0;imgs < imgpptbtn.length;imgs++){

			   	var imgobjs=imgpptbtn.eq(imgs);

               imgobjs[0].onload=function(){
                  
                  var imgthis=$(this);
                 
                 // if(imgthis.attr("setdatas")!="ok"){                  

                  	imgthis.click(function(e){
                     e.stopPropagation();
                     var _thisbtn = $(this);                    
                     var imgsrc=_thisbtn.attr("src");
                     $.fancybox.open(imgsrc);
                     return false;
                  });

                //  }

               // imgthis.attr("setdatas","ok");

               }

           }



   };

   //音频事件
   _private.audiosevente=function(rootdom){

    
                var prybtn=rootdom.find("span[rel^='audios_']");
                //判断音频是否加载
                for(var spanbtn_=0;spanbtn_ < prybtn.length;spanbtn_++){
                	var spanlable=prybtn.eq(spanbtn_);

                   if(spanlable.attr("setdatas")!="ok"){

                	spanlable.removeClass('fa-volume-up').addClass("fa-volume-off");


                	var spanlablerel=spanlable.attr("rel");
                	var audioslable=$("#"+spanlablerel+"");
                	audioslable[0].oncanplay=function(){

                		spanlable.css("color","#000");
                		spanlable.attr("setdatas","ok");

                	spanlable.click(function(e){

                   e.stopPropagation();
                   var _thisbtn = $(this);
                   var relids=_thisbtn.attr("rel");
                   var audiodom=document.getElementById(relids);
        		audiodom.play();        		

        		audiodom.onplay=function(){

         _thisbtn.removeClass('fa-volume-off').addClass("fa-volume-up");

        		}

        		audiodom.onended=function(){

        	_thisbtn.removeClass('fa-volume-up').addClass("fa-volume-off");

        		}

        		return false;

                  });



                	}

                }else{
                	spanlable.css("color","#ccc");
                }


                }


   };


	//写入当前用户正在聊天窗口
	_private.addmessage = function(_opts, userjson) {

		//检查在线状态
		if (_private.Connectionstatus) {


			//切换到在线离线通信处理
			if (userjson.type == "presence") {

				_private.Onlineoroffline(_opts, userjson);

				return false;

			}

			//切换到消息通知处理
			if(userjson.type == "notice"){

             _private.alertsaction(_opts, userjson);


              return false;
			}



			var __this = _opts;
			var chetdom = __this.dialog;



			var mgspoin = " msgleft",usernames="";
			if (_opts.user.userid == userjson.userid) {

				mgspoin = " msgright";
				usernames="";

			}else{
				usernames="<span class='usernamestyle'>"+userjson.userid+"</span>";
			}

			//如果是当前用户是，聊天窗口打开时
			if (!chetdom.closed) {

				//chetdom.id = "chatwindow_" + userjson.userid;
				var __dom = chetdom.DOM.dialog;
				var chatshowbox = __dom.find(".chatshowbox .mCSB_container");
				//var form_control = __dom.find(".form-control");

				if (userjson.msg != "" && typeof userjson.msg != "undefined") {
					var _txt = "<div class='inforow" + mgspoin + "'>"+usernames + userjson.msg + "</div>";
					chatshowbox.html(chatshowbox.html() + _txt);
              
              //图片预览			 
             _private.imgidevente(chatshowbox);

              //音频事件处理
              _private.audiosevente(chatshowbox); 
                 

					//form_control.val("");
					//_private.locatePoint(form_control[0]);
					__dom.find(".chatshowbox").mCustomScrollbar("scrollTo", "last");
					//缓存历史
					_private.addhistoricalrecord(_opts, _txt);
				}

				// console.log(chetdom);

			}

			$(".errorinfobox").empty();

		} else {

			$(".errorinfobox").text("网络异常，掉线了");

			//_private.error(_opts, userjson);

		}


	};

	//定位光标
	// _private.locatePoint = function(tea) {
	// 	if (tea.setSelectionRange) {
	// 		setTimeout(function() {
	// 			tea.setSelectionRange(0, 0);
	// 			tea.focus();
	// 		}, 0);
	// 	} else if (tea.createTextRange) {
	// 		var txt = tea.createTextRange();
	// 		txt.moveEnd("character", 0 - txt.text.length);
	// 		txt.select();
	// 	}
	// };


	_private.int = function(_opts) {

		if (typeof define === 'function' && define.amd)
			_private.createcss();
       //登录
		_private.reLogin(_opts);		

	};

	_private.createcss = function() {


		var cssfileall = [
			"require_css!bootstrap-css/bootstrap.min",
			"require_css!Font-Awesome",
			"require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar",
			"require_css!oso.lib/Miniwindow/css/style"
		];

		require(cssfileall);

	};

	_private.createhtml = function(_opts) {

		//var solls=_private.isScroll();

		//console.log(solls);

		var openbtn = $('<div class="WB_webimbtn"><i class="fa fa-pagelines"></i><span>' + _opts.openbtntxt + '</span></div><div id="alertsactionbox"><p class="alerttitlebox"><span class="fa fa-times"></span></p><div id="alertsactioncenet"></div></div>');
		openbtn.appendTo($('body'));

		//点开用户列表
		var btnobj = _opts._Dom.find(".WB_webimbtn");
        var colosarlt=$("#alertsactionbox .alerttitlebox span");

        colosarlt.click(function(e){
        	e.stopPropagation();
        	$("#alertsactionbox").hide();
        	  // if($("#alertsactioncenet").find(".lidivbox").length < 1){
           //     	 bullhorms.css("color","#ccc");
           //     }
        	return false;
        });

		btnobj.click(function(e) {
			var body=$('body');
			$(this).hide();
			body.find(".WB_webimbox").show();	


		}).addClass('rbottom0');



	};

	_private.JSONDATA = {};

	_private.loadingdata = function(_opts) {

		var _dataurl = _opts.dataUrl;
		var dataname = _opts.jsondataid;
		_opts.ionstimeset="";

		if (_dataurl.url) {
			_dataurl = _dataurl.url;
		}
		//------------data----- 
		if (typeof _dataurl == "string" && typeof _dataurl != "undefined") {
			var paramdata = {};
			if (_opts.postData) {
				paramdata = _opts.postData;
			}

			$.getJSON(_dataurl, paramdata, function(json) {


              if(json.data){

				_private.Connectionstatus = true;

				_private.JSONDATA[dataname] = json;

				if (_opts.dataUrl.formatjson) {
					_private.JSONDATA[dataname] = _opts.dataUrl.formatjson(json);
				}

				var datas = _private.JSONDATA[dataname];

				_opts.user = datas.data.currentUser;

				_private.openwin(_opts);

				//时时监听
				_private.Listeningtotheuser(_opts);

               }else{
               
               var errorobj = {
					txt: "请重新登陆",
					error: "没有data对象"
				};
				_private.error(_opts, errorobj);

				_private.Connectionstatus = false;


               }


			}).error(function(e) {

				var errorobj = {
					txt: "服务端请求失败",
					error: e
				};
				_private.error(_opts, errorobj);

				_private.Connectionstatus = false;

			});

		} else if (typeof _dataurl == "object" && !_opts.dataUrl.url) {

			_private.JSONDATA[dataname] = _dataurl;
			_opts.dataUrl = null;

			_private.Connectionstatus = true;

			var datas = _private.JSONDATA[dataname];
			_opts.user = datas.data.currentUser;
			_private.openwin(_opts);
			_private.Listeningtotheuser(_opts);

		} else {

			var errorobj = {
				txt: "数据无效",
				error: ""
			};
			_private.error(_opts, errorobj);
			_private.Connectionstatus = false;

		}

	};

	_private.error = function(_opts, errorobj) {

		console.log(12);

	}

	_private.openwin = function(_opts) {

		var body = $('body');
		var winbox = body.find('.WB_webimbox');
         //已更改
		/*//登陆成功亮起
		var WB_webimbtn=body.find('.WB_webimbtn');
		WB_webimbtn.css("color","#000");*/

		//屏幕可工作高度
		var hw = _private.windowofset();
		_opts.dialogh = hw.h;
		if (_opts.scrollH < hw.h) {
			_opts.dialogh = _opts.scrollH;
		}


		var userdataitme = _private.JSONDATA[_opts.jsondataid];

		//_opts.user=userdataitme.data.currentUser;

		var groups = userdataitme.data.groups;
		//var useritmehtml = "";
		var li = "";
		for (var keys in groups) {


			var groups_itme = groups[keys];
			li += "<li class='parent'><a href='#'><span>" + groups_itme.groupName + "</span></a>";

			var ulli = groups_itme.contacts;

			var ullihtml = "<ul id='" + groups_itme.groupId + "' class='children'>";

			for (var ulkey in ulli) {

				var __li = ulli[ulkey];

				var icons = '<i class="fa fa-file-image-o fa-6"></i>';
				if (__li.icons != "" && typeof __li.icons != "undefined") {
					icons = '<img src="' + __li.icons + '"/>';
				}

				var online = " available"
				if (__li.status == "unavailable") {
					online = " unavailable";
				}

				ullihtml += '<li id="osouser_itme_' + __li.id + '" class="row' + online + '"><div class="col-md-3">' + icons + '</div><div class="col-md-6"><div class="u_itmename"><div>' + __li.username + '</div></div></div><div class="col-md-1"></div></li>';

			}


			li += ullihtml + "</ul></li>";


		}


		if (winbox.length < 1) {

			var _html = '<div class="toptitlebox"><span class="fa fa-times"></span></div><div class="input-group"><div class="input-group-addon"><i class="fa fa-search fa-fw"></i></div><input class="form-control" type="text" placeholder="Username"></div>';

			//用户体
			_html += '<div class="userliste"><ul class="nav nav-pills nav-stacked">' + li + '</ul></div>';
			//底部按钮
			_html += '<div class="bottombox"><div class="btn-group"><a href="javascript:void(0)" title="最近通信用户"><i class="fa fa-clock-o"></i></a><a title="未读消息" href="javascript:void(0)"><i class="fa fa-comments-o"></i></a><a title="只显示在线用户" href="javascript:void(0)"><i class="fa fa-users"></i></a><a title="消息通知" href="javascript:void(0)"><span class="Anglebox">2</span><i class="fa fa-bullhorn"></i></a></div></div>';

			//'+_html+'
			//'+_h+'
			var windbox = $('<div style="height:' + _opts.dialogh + 'px" class="WB_webimbox">' + _html + '</div>');

			windbox.appendTo(body);

			var fa_users=windbox.find(".bottombox .fa-users");
            var usreonlin=true;

            var toptitlebox=windbox.find(".toptitlebox span");
            toptitlebox.click(function(e){

            e.stopPropagation();
			  var _thisbtn = $(this);
             $(".WB_webimbox").hide();
             WB_webimbtn.show();

            });

            //最近通信用户
            var fa_clock_o=windbox.find(".fa-clock-o");
            fa_clock_o.click(function(e){

            	e.stopPropagation();
			  var _thisbtn = $(this);

			  console.log("最近通信用户");

            });


                //最近通信用户
            var fa_bullhorn=windbox.find(".fa-bullhorn,.Anglebox");
            fa_bullhorn.click(function(e){

            	e.stopPropagation();
			  //var _thisbtn = $(this);
              var alertsobj=$("#alertsactionbox");
              var linen=alertsobj.find(".lidivbox");
              // var Anglebox=_thisbtn.prev(".Anglebox");
              // Anglebox.css("visibility","hidden");
              if(linen.length > 0){
              	alertsobj.show();
              }

              return false;

			 // console.log("最近通知消息");

            });




			//查找用户
			var __inputs = windbox.find(".form-control");
			__inputs.on('input', function(e) {

				var __this = $(this);
				var _val = $.trim(__this.val());
				_val = _val.replace(/\/|\.|<|>/g, "");
				if (_val != "" && typeof _val != "undefined") {

					_private.Quicksearch(_opts, _val);

				} else {

					var parentli = $(".userliste ul li:has(ul)");
					parentli.find("ul").hide();
					parentli.find("ul li").show();

					//var ulli = $(".userliste ul li").show();

				}
			  //开启==隐藏/显示离线用户按钮
               usreonlin=true;
               fa_users.css("color","#001DFF");

			});

			//隐藏/显示离线用户			
			fa_users.click(function(e) {

              e.stopPropagation();
			  var _thisbtn = $(this);
              var nav_stacked = windbox.find(".nav-stacked");
              var unavailable=nav_stacked.find(".unavailable");
              

             // var p_li = nav_stacked.find("li:has(ul)");
             // var p_ul=p_li.find("ul");



              if(usreonlin){

               unavailable.hide();

              var _available=nav_stacked.find(".available");
              for(var lio=0;lio < _available.length;lio++){
                  var _lipobj=_available.eq(lio);
                  var p__li=_lipobj.parents(".children").eq(0);
                   p__li.show();
              }

               usreonlin=false;
               _thisbtn.css("color","#ccc");
               _thisbtn.attr("title","显示离线用户");
              }else{
              
              unavailable.show();
              //unavailable.parents(".parent").eq(0).hide();
              usreonlin=true;
              _thisbtn.css("color","#001DFF");
              _thisbtn.attr("title","只显示在线用户");

              }



			  return false;

			});




			//未读信息
			windbox.find(".bottombox .fa-comments-o").click(function(e) {
             e.stopPropagation();
				//console.log(1223);

				var _thisbtn = $(this);

				//_thisbtn.css("color","#ccc");

				var userids = [];

				userids = _private.Numberofunreadmessages();

				if (userids.length > 0) {

					var nav_stacked = windbox.find(".nav-stacked");
					var p_li = nav_stacked.find("li:has(ul)");
					p_li.find("li").hide();
					p_li.find("ul").hide();
					p_li.hide();

					for (var userli in userids) {

						var userlijq = $("li[id='osouser_itme_" + userids[userli] + "']");

						userlijq.show();
						userlijq.parents("ul").eq(0).show();
						userlijq.parents(".parent").eq(0).show();

					}


				} else {


					var nav_stacked = windbox.find(".nav-stacked");
					var p_li = nav_stacked.find("li:has(ul)");
					p_li.find("li").show();
					//p_li.find("ul").hide();
					p_li.show();



				}

				return false;


			});


			body.find(".WB_webimbox").hover(
				function(e) {

					e.stopPropagation();
					var time = _opts.WB_webimbox_time;
					if (time != "" && typeof time != "undefined") {
						window.clearTimeout(_opts.WB_webimbox_time);
					}
					body.find(".WB_webimbtn").hide();
					$(this).show();

				},
				function(e) {

					e.stopPropagation();

				//if(typeof window.nohover!="undefined" && window.nohover!=null)
				//	_opts.nohover = window.nohover;

					if (_opts.nohover) {

						var time = _opts.WB_webimbox_time;
						if (time != "" && typeof time != "undefined") {
							window.clearTimeout(_opts.WB_webimbox_time);
						}
						var __box = $(this);
						_opts.WB_webimbox_time = setTimeout(function() {
							__box.hide();
							body.find(".WB_webimbtn").show();

	          //时时检查本地是否有，没有阅读过的消息
               _private.everymsgunread(_opts);


						}, 300);

					}

				}).addClass('rbottom0');

			var liobj = body.find(".WB_webimbox .userliste ul li:has(ul)");
			//console.log(liobj.length);
			liobj.click(function() {

				var _thisul = $(this).find("ul").eq(0)
				if (_thisul.is(":visible")) {
					_thisul.hide();
					return false;
				}

				liobj.find("ul").hide();
				var liul = $(this).find("ul").eq(0);
				if (liul.is(":hidden")) {

					liul.show();

				}


			});

			var allli = liobj.find("ul li");
			allli.click(function(e) {
				e.stopPropagation();
				var itmeids = $(this).attr("id");
				var groupsid = $(this).parents("ul").eq(0).attr("id");
				var id = itmeids.replace(/osouser_itme_/, "");
				_opts.userid = id;
				_private.chatwindow(_opts, groupsid);

				//判断是否还有未读消息按钮状态
				_private.SetUnreadmessagesbtn();

			});

			var userliste = body.find(".WB_webimbox .userliste");
			var sollh = _opts.dialogh - 104;
			//需要滚动条   
			// if (sollh.height() > sollh) {

			userliste.css({
				"max-height": sollh + "px",
				"height": sollh + "px"
			});
			userliste.mCustomScrollbar({
				axis: "y",
				scrollInertia: sollh,
				scrollbarPosition: "outside"
			});
			userliste.find(".mCSB_scrollTools").css("right", "-10px");

			//}


		} else {

			winbox.show();

		}

		//body.find(".WB_webimbtn").hide();



	};

   //遍历所有用户信息
	_private.Numberofunreadmessages = function() {

		var userids = [];
		for (var key in _private.message) {

			var itmemesobj = _private.message[key];
			if (itmemesobj.length > 0) {
				userids.push(key);
			}

		}

		return userids;
	}


	//设置未读消息按钮
	_private.SetUnreadmessagesbtn = function() {

		var msglen = _private.Numberofunreadmessages();
		if (msglen.length < 1) {

			$(".WB_webimbox .bottombox .fa-comments-o").css("color", "inherit");

		}

	}

	//当前窗口的聊天历史记录写入--html
	_private.historicalrecord = {};
	_private.addhistoricalrecord = function(_opts, histmsg) {

		if (_private.historicalrecord[_opts.userid]) {
			_private.historicalrecord[_opts.userid].push(histmsg);
		} else {
			_private.historicalrecord[_opts.userid] = [];
			_private.historicalrecord[_opts.userid].push(histmsg);
		}

	};

	//当前窗口的聊天历史记录读取--html
	_private.Readthehistory = function(userid) {

		if (_private.historicalrecord[userid]) {

			var storyhtml = "";
			for (var key in _private.historicalrecord[userid]) {

				var itmehtml = _private.historicalrecord[userid][key];
				storyhtml += itmehtml;

			}

			return storyhtml;

		} else {

			return false;

		}

	};

	//搜索用户
	_private.Quicksearch = function(_opts, _val) {

		var dataname = _opts.jsondataid;
		var _data = _private.JSONDATA[dataname];


		var _searchval = new RegExp(_val, 'i');
		var parentli = $(".userliste ul li:has(ul)");
		parentli.find("ul").hide();
		parentli.find("ul li").hide();

		_data.data.groups.map(function(itme) {

			for (var itmekey in itme.contacts) {
				var contitme = itme.contacts[itmekey];

				if (_searchval.test(contitme.username)) {
					var osoli = $("li[id='osouser_itme_" + contitme.id + "']");
					var parentul = osoli.parents("ul").eq(0);
					parentul.show();
					osoli.show();
				}

			}


		});

	};


	_private.groupsdata = function(_opts, groupId) {

		var _data = _private.JSONDATA[_opts.jsondataid];

		return _data.data.groups.filter(function(itme) {
			return itme.groupId == groupId;
		});

	};

	_private.userdata = function(_opts, id, groups) {


		return groups.contacts.filter(function(itme) {
			return itme.id == id;
		});

	};


	//开启聊天窗口

	_private.chatwindow = function(_opts, groupsid) {

		var groupsdata = _private.groupsdata(_opts, groupsid)[0];

		var userdatas = _private.userdata(_opts, _opts.userid, groupsdata)[0];

		//console.log(userdatas);

		//历史记录
		var storyhtml = _private.Readthehistory(_opts.userid);
		if (storyhtml == null || typeof storyhtml == "undefined" || !storyhtml) {
			storyhtml = "欢迎...";
		}

		_opts.groupId = groupsid;

		var titletxt = userdatas.username;
		var body = _opts._Dom;
		var userliste = body.find('.WB_webimbox');


		var closed = true;

		if (_opts.dialog) {
			closed = _opts.dialog.closed
		}

		if (!closed) {



			var chetdom = _opts.dialog;

			if (chetdom.id != "chatwindow_" + userdatas.id) {

				chetdom.id = "chatwindow_" + userdatas.id;
			}
				chetdom.title(titletxt);

				var __dom = chetdom.DOM.dialog;

                var contdomroot=__dom.find(".chatshowbox .mCSB_container");

				contdomroot.empty().html(storyhtml);
				//清除服务端--下次请求就可以发起
				_opts.delHistroy=true;
				 _opts.chatId=userdatas.id;
				//__dom.find(".form-control").val("").focus();
				var myeditorobj=nicEditors.findEditor('mangbox');
				myeditorobj.setContent("");
				$(myeditorobj.elm).focus();
                
                //图片预览
				_private.imgidevente(contdomroot);

				//音频处理
				_private.audiosevente(contdomroot);


				var time = _opts.WB_webimbox_time;
				if (time != "" && typeof time != "undefined") {
					window.clearTimeout(_opts.WB_webimbox_time);
				}

				var userlistep = userliste.offset();
				chetdom.left = userlistep.left - 510;
				chetdom.top = userlistep.top;
				chetdom.show();


		} else {

			//dialogh外高
			var dloh = _opts.dialogh - 97;
			var re_h = dloh - 140;
			var chatshowbox_h = (re_h - 46);

			// _opts.dialog = new lhgdialog();

			var userlistep = userliste.offset();

			_opts.dialog = new lhgdialog({
				width: 490,
				height: dloh,
				title: titletxt,
				fixed: true,
				left: userlistep.left - 510,
				top: userlistep.top,
				content: "<div class='chatshowbox' style='height:" + chatshowbox_h + "px'>" + storyhtml + "</div><div class='chattootms btn-group'></div><div class='proresbox'><progress id='progressBar' value='0' max='100'></progress></div><div class='inputbox'><textarea id='mangbox' class='form-control'></textarea></div>",
				init: function() {
					var __dialogthis = this.DOM;
					var nodedom = __dialogthis.dialog;
					var ui_buttons = nodedom.find(".ui_buttons");
					var buttons = $('<div class="errorinfobox"></div><div class="btn-group"><button type="button" class="btn btn-success">发送</button><button type="button" class="btn btn-success dropdown-toggle"><span class="caret"></span></button></div>');

					//清除服务端-----下次请求就可以发起
				    _opts.delHistroy=true;
				    _opts.chatId=userdatas.id;

					buttons.find("button:first").click(function(e) {

						// console.log(e.keyCode);
						_private.sendfn(_opts, nodedom);

						return false;
					});


					var setbntlast = buttons.find("button:last");


					buttons.appendTo(ui_buttons);

					ui_buttons.show();

                 new nicEditor({buttonList : ['fontSize','ol','ul','hr','uploadfile','Recordericos'],fileservpath:_opts.fileservpath}).panelInstance('mangbox');
          
                var myeditorobj=nicEditors.findEditor('mangbox');

                 var elm=$(myeditorobj.elm);
                 elm.focus();                  
                 elm.keypress(function(e){

                     if(!_opts.enters && e.keyCode === 13 && !e.ctrlKey){

                     	_private.sendfn(_opts, nodedom);
                     	e.preventDefault();

                       return false;
                     }

                     if(_opts.enters && e.ctrlKey){
                     
                      if(e.keyCode === 10 || e.keyCode ===13){
                     	_private.sendfn(_opts, nodedom);
                     	e.preventDefault();

                     	 return false;

                     	}

                     }
         
                     
                });

                  //console.log(dsr);


					setbntlast.click(function(e) {
						e = e ? e : window.event;
						e.cancelBubble = true;
						e.stopPropagation();
						var topbox = __dialogthis.border.parents("div").eq(0);
						//var p__div=nodedom.
						var zindex = window.parseInt(topbox.css("z-index")) + 2;
						_private.setEnter(_opts, $(this), zindex);
					});

					setbntlast.mouseleave(function() {

						var times = _opts.setentertime;
						if (times != "" && times != "undefined") {
							window.clearTimeout(_opts.setentertime);
						};

						_opts.setentertime = window.setTimeout(function() {
							$(".setenter__box").hide();
						}, 400);

					});


					//整个dialog

					nodedom.hover(function(e) {

						e.stopPropagation();
						var time = _opts.WB_webimbox_time;
						if (time != "" && typeof time != "undefined") {
							window.clearTimeout(_opts.WB_webimbox_time);
						}

						body.find(".WB_webimbtn").hide();
						userliste.show();
						_opts.dialog.show();


					}, function(e) {

						e.stopPropagation();

					//if(typeof window.nohover!="undefined" && window.nohover!=null)
					//_opts.nohover = window.nohover;

						if (_opts.nohover) {

							var time = _opts.WB_webimbox_time;
							if (time != "" && typeof time != "undefined") {
								window.clearTimeout(_opts.WB_webimbox_time);
							}
							//var __box = $(this);
							var __windbox =
								_opts.WB_webimbox_time = setTimeout(function() {									
									_opts.dialog.hide();
									userliste.hide();
									body.find(".WB_webimbtn").show();
									
                       //时时检查本地是否有，没有阅读过的消息
                      _private.everymsgunread(_opts);

								}, 300);

						}

					});



					//var form_control = nodedom.find(".form-control");
					var chatshowbox = nodedom.find(".chatshowbox");

					// form_control.keydown(function(e) {

					// 	//console.log(e.keyCode);


					// 	if (e.keyCode == 13) {

					// 		_private.sendfn(_opts, nodedom);

					// 	}

					// });


					//滚动条        
					chatshowbox.mCustomScrollbar({
						axis: "y",
						scrollInertia: chatshowbox.height(),
						scrollbarPosition: "outside"
					});
					chatshowbox.find(".mCSB_scrollTools").css("right", "-10px");

					nodedom.find(".chatbox").css({
						"transform": "initial",
						"opacity": "initial"
					});

				}

			}).show();

			var dialogdomroot = _opts.dialog.DOM.dialog;
			_opts.dialog.id = "chatwindow_" + _opts.userid;
			dialogdomroot.addClass('chatbox');
			dialogdomroot.find(".ui_content").css({
				"padding": "0px",
				"width": "100%"
			});


		};


		//显示未读消息时-缓存
		if (_private.message[_opts.userid]) {

			var usermsg_data = _private.message[_opts.userid];
			if (usermsg_data.length > 0) {

				var msghtml = "";
				var _msgbox = _opts.dialog.DOM.dialog;
				var chatshowbox = _msgbox.find(".chatshowbox .mCSB_container");
				msghtml = "<div class='inforow time'>" + usermsg_data[0].time + "</div>";
				var usernames="<span class='usernamestyle'>"+_opts.userid+"</span>";
				for (var ukeys in usermsg_data) {

					var useritmemsg = usermsg_data[ukeys];
					msghtml += "<div class='inforow msgleft'>"+usernames + useritmemsg.msg + "</div>";


				}

				chatshowbox.html(msghtml);
				_private.message[_opts.userid] = [];
				var msgnumber_li = $("li[id='osouser_itme_" + _opts.userid + "']");
				//清除--信息数量
				var msgnumber_box = msgnumber_li.find(".col-md-1");
				msgnumber_box.empty();
				 
				 for(var useridskey in _private.Speakseveral){

                   var useridkeys=_private.Speakseveral[useridskey];
                   if(useridkeys == _opts.userid){

                   	 delete _private.Speakseveral[useridskey];

                   	 break;
                   }

				 }


				_msgbox.find(".chatshowbox").mCustomScrollbar("scrollTo", "last");

				//写入缓存历史
				_private.addhistoricalrecord(_opts, msghtml);

				//_opts.delHistroy=true;
                //_opts.chatId=_opts.userid;

			}


		}

		return false;

	};

	//发起消息
	_private.sendfn = function(_opts, nodedom) {

		var user = _opts.user;


		var groupsdata = _private.groupsdata(_opts, _opts.groupId)[0];

		var userdatas = _private.userdata(_opts, _opts.userid, groupsdata)[0];

		// var userdatas = _private.userdata(_opts, id)[0];

		var chatshowbox = nodedom.find(".chatshowbox");
		//var form_control = nodedom.find(".form-control");

		// console.log(_private);

		var events = _private.events[_opts.jsondataid];

		//_private.String.call(String.prototype);


		//var __valtxt = $.trim(form_control.val());
		var __valtxt = $.trim(nicEditors.findEditor('mangbox').getContent());

		//console.log(__valtxt);

		//form_control.val("");
		//清空
		var myeditorobj=nicEditors.findEditor('mangbox');
		myeditorobj.setContent("");
        $(myeditorobj.elm).focus();		

		//光标
		//_private.locatePoint(form_control[0]);


		if (__valtxt != null && __valtxt != "" && typeof __valtxt != "undefined") {

			//__valtxt = __valtxt.encodeHtml();

			var userjson = {
				userid: user.userid,
				msg: __valtxt,
				chatId: userdatas.id,
				groupId: _opts.groupId
			}
			_private.Speakarequest(_opts, userjson);

		}


	};

	//时时监听信息
	_private.intess="";
	_private.Listeningtotheuser = function(_opts) {	
     

        _private.Retrievethechatrecord(_opts.ListeningmsgData,_opts);

	};
 
  //重新登录或登出
   _private.reLogin=function(_opts){


   	_private.createhtml(_opts);
   	var conn = new Strophe.Connection('/http-bind');
   	var connData= Chat.getCookie();
   	conn.attach(connData.jid,connData.sid,connData.rid,Chat.onConnect);
   	Chat.connection=conn;
   	
   //var loginurl=_opts.dataUrl;

///Sync10/action/pushMessageByOpenfire/PushMessageByOpenfireAction.action?Method=Login

     
  /*$.ajax({
     	url: loginurl,
     	type: 'GET',
     	dataType:'json',
     	data: {Method: 'Login'},
     })
    .fail(function() {
     	console.log("error");
     })
     .always(function() {
     	_private.loadingdata(_opts);
		_private.createhtml(_opts);
     });*/   


   };



	_private.Retrievethechatrecord=function(__data,_opts){

	  if (typeof _private.intess != "undefined" && _private.intess != "") {
		window.clearTimeout(_private.intess);
	  }        
     


      _private.intess=setTimeout(function(){

      	 //清空服务端未读消息
        __data.delHistroy=_opts.delHistroy;
        __data.chatId=_opts.chatId;
        if(_opts.msgType && typeof _opts.msgType!="undefined" && _opts.msgType!=""){
        	__data.msgType=_opts.msgType;
        }     


		$.ajax({
					url:_opts.dataUrl,
					type: 'POST',
					dataType: 'json',
					data: __data,
					timeout : 4500,
                 error:function(xhr,textStatus){
                    console.log('超时');
                  }
				})
				.fail(function() {

					_private.Connectionstatus = false;

					if (typeof _private.intess != "undefined" && _private.intess != "") {
						window.clearTimeout(_private.intess);
					}
					console.log("error");
				})
				.always(function(d) {	

					_private.Connectionstatus = true;

					   //清空服务端未读消息完成
					if(_opts.delHistroy && _opts.msgType!="notice"){
					 _opts.delHistroy=false;
                     _opts.chatId="";
                   };

                   if(_opts.delHistroy && _opts.msgType=="notice" && _opts.msgType){

                    delete _opts.msgType;
				    _opts.delHistroy=false;
				    _opts.chatId="";

                   };

					//有信息
					if (d.hasmessage) {



						//__data.option="del";
						__data.delTemp=true;



						for (var keys in d.data) {

							var useritme = d.data[keys];
							var userids = "chatwindow_" + useritme.userid;

							//聊天窗口已开启
							if (_opts.dialog) {

								//正在说话的窗口
								var _msgbox = _opts.dialog.DOM.dialog;
								var dialog_dom_root=_msgbox.parents("div").eq(0);
								var visible_root=(dialog_dom_root.css("visibility") =="visible");
								if (!_opts.dialog.closed && _opts.dialog.id == userids && visible_root) {
									//写入窗口
									_private.addmessage(_opts, useritme);
                                    _opts.delHistroy=true;
				                   _opts.chatId=useritme.userid;
									
									 _private.Retrievethechatrecord(__data,_opts);

								} else {
									//写脚标,缓存起信息
									//console.log(useritme);
									_private.addusermessage(_opts, useritme);

								
                                    
                                   // __data.delHistroy=false;
               

                                    
                                     _private.Retrievethechatrecord(__data,_opts);

								}

							} else {

								//写脚标,缓存起信息
								_private.addusermessage(_opts, useritme);

								 // if(__data.delHistroy){
									// 	__data.delHistroy=false;
									// }else{
									// 	__data.delHistroy=false;
									// }

                                  //__data.delHistroy=false;

               

								 _private.Retrievethechatrecord(__data,_opts);

							}

						}


					} else {

						//没信息

						//开聊天窗口未开启,要闪动，打开的按钮
						//console.log("no info");						
                        __data.option="";
						_private.Retrievethechatrecord(__data,_opts);

						return false;



					}


				});

         //时时检查本地是否有，没有阅读过的消息
           _private.everymsgunread(_opts);


         }, _opts.refreshdata);


	};

    //时时检查本地是否有，没有阅读过的消息,
    //开启图标闪动功能
     _private.Speakseveral=[];
     _private.everymsgunread=function(_opts){
        
       var WB_webimbtn=$(".WB_webimbtn");
       if(WB_webimbtn.is(":visible")){

       	  var set_speak=_private.Speakseveral;
			if(typeof set_speak[0] !="undefined" && set_speak.length > 0){
          _private.msg_mationbreathing_ions(_opts);
        }else{

        if(_opts.ionstimeset!="" && typeof _opts.ionstimeset!="undefined" && _opts.ionstimeset){
        	window.clearTimeout(_opts.ionstimeset);
        }

         var WB_webimbtn=$(".WB_webimbtn");
            var iobj=WB_webimbtn.find("i");
            iobj.fadeTo("slow", 1);


        }

       }


     }


	//在线或离线处理--刷新
	_private.Onlineoroffline = function(_opts, userjson) {

		var pbox = $(".WB_webimbox");
		var userli = pbox.find("li[id='osouser_itme_" + userjson.userid + "']");
		userli.show();
		//var online="available";
		if (userjson.status == "unavailable") {
			if (userli.hasClass('available'))
				userli.removeClass('available');

			userli.addClass('unavailable');
		}

		if (userjson.status == "available") {
			if (userli.hasClass('unavailable'))
				userli.removeClass('unavailable');

			userli.addClass('available');
		}


	};


	//送出说话请求
	_private.Speakarequest = function(_opts, userjson) {

		var __data = $.extend(_opts.AmessageData, userjson);



		_private.addmessage(_opts, __data);

		var _msgbox = _opts.dialog.DOM.dialog;
		var chatshowbox = _msgbox.find(".chatshowbox .mCSB_container");

		$.ajax({
				url: _opts.dataUrl,
				type: 'POST',
				dataType: 'json',
				data: __data,
			})
			.done(function(data) {
				_private.Connectionstatus = true;
				if (data.issuccess) {

				
                chatshowbox.find(".msgright:last").css("color","#000");
					//console.log(data);
					//_private.addmessage(_opts, __data);
				}

			})
			.fail(function() {
				_private.Connectionstatus = false;
				var msglilastbox=chatshowbox.find(".msgright:last");
				$('<i class="fa fa-warning"></i>').appendTo(msglilastbox);
				console.log("error");
			})
			.always(function(e) {
				_private.Connectionstatus = true;
				//console.log("complete");
			});

	};


	//设置回车发送
	_private.setEnter = function(_opts, btn, pzindex) {

		var setenterbox = $(".setenter__box");
		if (setenterbox.length < 1) {
			setenterbox = $('<div class="setenter__box"><ul><li><i class="fa"></i><span>按回车发送</span></li><li><i class="fa fa-check" rel=""></i><span>按Ctrl+回车发送</span></li></ul></div>');
			var potinon = btn.offset();
			potinon.top = 34;
			var style = {
				"left": potinon.left + "px",
				"bottom": potinon.top + "px",
				"z-index": pzindex
			}
			setenterbox.css(style);
			setenterbox.appendTo('body');
			var __box = $(".setenter__box");
			__box.hover(function() {

				 var time = _opts.WB_webimbox_time;
				 if (time != "" && typeof time != "undefined") {
				  window.clearTimeout(_opts.WB_webimbox_time);
				 }

				var times = _opts.setentertime;
				if (times != "" && times != "undefined") {
					window.clearTimeout(_opts.setentertime);
				}

				$(this).show();

			}, function() {

				var times = _opts.setentertime;
				if (times != "" && times != "undefined") {
					window.clearTimeout(_opts.setentertime);
				}

				_opts.setentertime = window.setTimeout(function() {

					__box.hide();

				}, 300);


			});

			var __li = __box.find("li");

			__li.click(function() {
				var __this = $(this);
				if(__this.index() == 1){
                   _opts.enters=true;
				}else{
					_opts.enters=false;
				}
				__li.find("i").removeClass('fa-check');
				__this.find("i").addClass('fa-check');

			});

		} else {

			var potinon = btn.offset();
			potinon.top = 44;
			var style = {
				"left": potinon.left + "px",
				"bottom": potinon.top + "px",
				"z-index": pzindex,
				"display": "block"
			}
			setenterbox.css(style);


		}


	}


/*
	//字符处理

	_private.String = function() {

		this.REGX_HTML_ENCODE = /"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g;

		this.REGX_HTML_DECODE = /&\w+;|&#(\d+);/g;

		this.REGX_TRIM = /(^\s*)|(\s*$)/g;

		this.HTML_DECODE = {
			"&lt;": "<",
			"&gt;": ">",
			"&amp;": "&",
			"&nbsp;": " ",
			"&quot;": "\"",
			"&copy;": ""
		};

		this.encodeHtml = function(s) {
			s = (s != undefined) ? s : this.toString();
			return (typeof s != "string") ? s :
				s.replace(this.REGX_HTML_ENCODE,
					function($0) {
						var c = $0.charCodeAt(0),
							r = ["&#"];
						c = (c == 0x20) ? 0xA0 : c;
						r.push(c);
						r.push(";");
						return r.join("");
					});
		};

		this.decodeHtml = function(s) {
			var HTML_DECODE = this.HTML_DECODE;

			s = (s != undefined) ? s : this.toString();
			return (typeof s != "string") ? s :
				s.replace(this.REGX_HTML_DECODE,
					function($0, $1) {
						var c = HTML_DECODE[$0];
						if (c == undefined) {
							// Maybe is Entity Number
							if (!isNaN($1)) {
								c = String.fromCharCode(($1 == 160) ? 32 : $1);
							} else {
								c = $0;
							}
						}
						return c;
					});
		};

		this.trim = function(s) {
			s = (s != undefined) ? s : this.toString();
			return (typeof s != "string") ? s :
				s.replace(this.REGX_TRIM, "");
		};


		this.hashCode = function() {
			var hash = this.__hash__,
				_char;
			if (hash == undefined || hash == 0) {
				hash = 0;
				for (var i = 0, len = this.length; i < len; i++) {
					_char = this.charCodeAt(i);
					hash = 31 * hash + _char;
					hash = hash & hash; // Convert to 32bit integer
				}
				hash = hash & 0x7fffffff;
			}
			this.__hash__ = hash;

			return this.__hash__;
		};

	};

	_private.String.call(_private.String);

	*/

	_private.isScroll = function(el) {
		// test targets
		var elems = el ? [el] : [document.documentElement, document.body];
		var scrollX = false,
			scrollY = false;
		for (var i = 0; i < elems.length; i++) {
			var o = elems[i];
			// test horizontal
			var sl = o.scrollLeft;
			o.scrollLeft += (sl > 0) ? -1 : 1;
			o.scrollLeft !== sl && (scrollX = scrollX || true);
			o.scrollLeft = sl;
			// test vertical
			var st = o.scrollTop;
			o.scrollTop += (st > 0) ? -1 : 1;
			o.scrollTop !== st && (scrollY = scrollY || true);
			o.scrollTop = st;
		}
		// ret
		return {
			scrollX: scrollX,
			scrollY: scrollY
		};
	};


	_private.windowofset = function() {

		var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;

		return {
			h: height,
			w: width
		}
	};





	return webim_win;
}));

/*
*strophejs chat webIM
*author tw
*/
var Chat={
	connection: null,
	dialogh:null,
	dialog:null,
	rec_jid:null,
	fileservpath:"/Sync10/_fileserv/upload",
	rec_user_msg:{},
	rec_user_msg_numb:{},
	senderArray:[],//信息发送人，或者当前登录人发送信息的接收人
	//groups:[],
	getCookie:function(){
	    //var cookieStr=document.cookie;
	    return {'jid':$.cookie('jid'),'rid': $.cookie('rid'),'sid': $.cookie('sid')};
	    
	    
	},
	onConnect:function(status){
		var body = $('body');
		var WB_webimbtn=body.find('.WB_webimbtn');
		if (status == Strophe.Status.ATTACHED){
	        var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});//构建查询用户iq包
    		this.sendIQ(iq, Chat.on_roster);//发送查询用户的iq包，并接收查询返回的用户列表iq  
	        // 当接收到<message>节，调用onMessage回调函数  
	        this.addHandler(Chat.onMessage, null, 'message', "chat");
	        this.addHandler(Chat.on_roster_changed,
                              "jabber:iq:roster", "iq", "set");//监听好友变化
			//登陆成功亮起
			WB_webimbtn.css("color","#000");
    	}else{
    		//登录失败变暗
    		WB_webimbtn.css("color","#ccc");
    		console.log(status);
    	}
    	//Gab.connection = this;
	},
	on_roster:function(iq){
		var groups=[];
		$(iq).find('item').each(function () {
			var name=$(this).attr("name");
			var jid=$(this).attr("jid");
			if(Chat.connection.jid.split('/')[0]=='jid'){
				return;
			}
			var jid_to_id_=Chat.jid_to_id(jid);
			var user_html="<li id='"+jid_to_id_+"' class='row unavailable'>"+
				"<div class='col-md-3'><i class='fa fa-file-image-o fa-6'></i>"+
				"<img src=''/></div><div class='col-md-6'><div class='u_itmename'>"+
				"<div>"+name+"</div></div></div><div class='col-md-1'></div></li>";//拼接用户li
			//var userDt={'userName':name,'jid':jid};
			$(this).find("group").each(function(){
				var group=this.textContent;
				if(groups.length>0){
					for (var i = 0; i < groups.length; i++) {
						if(groups[i].groupName==group){
							groups[i].s_html=groups[i].s_html+user_html;
							break;
						}else if(i==groups.length-1 && groups[i].groupName!=group){
							//Chat.groups.push({'groupName':group,contacts:[userDt]});
							var group_html="<li class='parent'><a href='#'><span>"+group+"</span>"+
								"</a><ul id="+group+" class='children'>";//拼接group li
							groups.push({'groupName':group,s_html:group_html+user_html});
							break;
						}
					};
				}else{
					var group_html="<li class='parent'><a href='#'><span>"+group+"</span>"+
						"</a><ul id="+group+" class='children'>";//拼接group li
					groups.push({'groupName':group,s_html:group_html+user_html});
				}
			});
		});
		//头部信息
		var html_="<div class='toptitlebox'><span class='fa fa-times'></span></div><div class='input-group'><div class='input-group-addon'><i class='fa fa-search fa-fw'></i></div><input class='form-control' type='text' placeholder='Username'></div>";
		//用户列表html
		var users_html="";
		for(var i=0;i<groups.length;i++){
			users_html+=groups[i].s_html+"</ul></li>";
		}
		//用户体
		html_ += "<div class='userliste'><ul class='nav nav-pills nav-stacked'>" + users_html + "</ul></div>";
		//底部按钮
		html_ += "<div class='bottombox'><div class='btn-group'><a href='javascript:void(0)' title='最近通信用户'><i class='fa fa-clock-o'></i></a><a title='未读消息' href='javascript:void(0)'><i class='fa fa-comments-o'></i></a><a title='只显示在线用户' href='javascript:void(0)'><i class='fa fa-users'></i></a><a title='消息通知' href='javascript:void(0)'><span class='Anglebox'>2</span><i class='fa fa-bullhorn'></i></a></div></div>";

		//屏幕可工作高度
		var hw = Chat.windowofset();
			Chat.dialogh= hw.h;
		if (500 < hw.h) {
			Chat.dialogh = 500;
		}
		//拼接完整的用户列表窗口
		var windbox = $('<div style="height:' + Chat.dialogh + 'px" class="WB_webimbox">' + html_ + '</div>');

		windbox.appendTo($('body'));
		Chat.WB_webimbox_bindClick(windbox);
		Chat.userlisteCss();
		// set up presence handler and send initial presence
		Chat.connection.send($pres({type:'unavailable'}));//在页面刷新时先将登录人状态改为下线是为再次拉起在线用户
        Chat.connection.addHandler(Chat.on_presence, null, "presence");//监听用户上下线监听
        Chat.connection.send($pres());//发送presence包，$pres()构建presence包
	},
	onMessage:function(message){
		var closed
		if (Chat.dialog) {
			closed = Chat.dialog.closed
		}
		var id=$(message).attr("id");
		var senderJid=$(message).attr('from').split('/')[0];
		var msgContent=$(message).children("body").text();
		//收到消息时将消息按发送人的jid存储起来
		var dt={};
		dt[senderJid]=msgContent;
		Chat.users_mgs_cache(senderJid,dt);
		/*var dt={};
		dt[senderJid]=msgContent;
		if($.isEmptyObject(Chat.rec_user_msg)){
				Chat.rec_user_msg[senderJid]=[dt];
				Chat.senderArray.push(senderJid);
		}else{
			for(var i=0;i<Chat.senderArray.length;i++){
				if(Chat.senderArray[i]==senderJid){
					Chat.rec_user_msg[senderJid].push(dt);
					break;
				}else if(i==Chat.senderArray.length-1){
					//Chat.rec_user_msg[senderJid]=[dt];
					Chat.senderArray.push(senderJid);
				}
			}
		}*/
		//信息回执
		var result_msg="$msg({to:'"+senderJid+"'}).c('received',{xmlns:'urn:xmpp:receipts',id:'"+id+"'})"
    	Chat.connection.send(eval(result_msg));
		//如果与他的聊天窗口已打开将接收到的消息添加到聊天窗口
		if(senderJid==Chat.rec_jid && !closed){
			Chat.addMessage(senderJid,msgContent);
			var reallyMsgs= Chat.rec_user_msg[Chat.rec_jid];
			if(reallyMsgs){
				//到打开对话框时，如果已读消息大于50条，
				//将保存已读消息的后50条作为下次打开聊天窗口的历史消息
				if(reallyMsgs.length>5){
					reallyMsgs.reverse();
					delete Chat.rec_user_msg[Chat.rec_jid];
					reallyMsgs.splice(5);
					reallyMsgs.reverse();
					Chat.rec_user_msg[Chat.rec_jid]=reallyMsgs;
				}
				
			}
		}else{
			var msg_number= Chat.rec_user_msg_numb[senderJid];
			if(!msg_number){
				Chat.rec_user_msg_numb[senderJid]=1;
			}else{
				Chat.rec_user_msg_numb[senderJid]=Chat.rec_user_msg_numb[senderJid]+1;
			}
			Chat.write_user_msgNumber(senderJid);//未打开的聊天床头追加信息条数
			Chat.msg_mationbreathing_ions();
			
		}
		
		return true;
	},
	on_roster_changed:function(iq){
		console.log(iq);
		return true;
	},
	on_presence:function(presence){
		var presence_type = $(presence).attr('type'); // unavailable, subscribed, etc...
      	var from = $(presence).attr('from'); // the jabber_id of the contact
      	//var from_=Chat.jid_to_id(from);
      	var Resource=Strophe.getResourceFromJid(from);
      	var userNodes=$('body').find('li#'+Chat.jid_to_id(from));
      	if (presence_type !== 'error' && presence_type === 'unavailable') {//用户下线
  			userNodes.removeClass(Resource);
  			if(userNodes.attr('class')=='row available'){
  				userNodes.removeClass("available");
            	userNodes.addClass("unavailable");
  			}
        } else {//用户上线
        	userNodes.removeClass("unavailable");
            userNodes.addClass("available");
            userNodes.addClass(Resource);
        }
		return true;
	},
	jid_to_id: function (jid) {
        return Strophe.getBareJidFromJid(jid)
            .replace("@", "-")
            .replace(/\./g,"-");
    },
    windowofset : function() {
		var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
		return {
			h: height,
			w: width
		}
	},
	WB_webimbox_bindClick:function(windbox){
		var WB_webimbtn=$('body').find('.WB_webimbtn');
		var liobj = $('body').find(".WB_webimbox .userliste ul li:has(ul)");
		//console.log(liobj.length);
		liobj.click(function() {

			var _thisul = $(this).find("ul").eq(0)
			if (_thisul.is(":visible")) {
				_thisul.hide();
				return false;
			}

			liobj.find("ul").hide();
			var liul = $(this).find("ul").eq(0);
			if (liul.is(":hidden")) {

				liul.show();

			}


		});
		var allli = liobj.find("ul li");
		allli.click(function(e) {
			var nodeId=$(this).attr('id');
			Chat.rec_jid=nodeId.split("-")[0]+"@"+document.domain;
			var titletxt=$($(this).find('.col-md-6 .u_itmename')[0]).children()[0].innerHTML;
			Chat.clear_user_msgNumber();
			Chat.chatwindow(titletxt);

		});
		//关闭用户列表时WB_webimbtn显示
		var toptitlebox=windbox.find(".toptitlebox span");
            toptitlebox.click(function(e){

            e.stopPropagation();
			  var _thisbtn = $(this);
             $(".WB_webimbox").hide();
             WB_webimbtn.show();

        });	
        //查找用户
		var __inputs = windbox.find(".form-control");
		__inputs.on('input', function(e) {

			var __this = $(this);
			var _val = $.trim(__this.val());
			if (_val != "" && typeof _val != "undefined") {
				Chat.Quicksearch(_val);
			} else {
				var parentli = $(".userliste ul li:has(ul)");
				parentli.find("ul").hide();
				parentli.find("ul li").show();
			}
		}); 
		//隐藏/显示离线用户	
		var fa_users=windbox.find(".bottombox .fa-users");
        var usreonlin=true;
		fa_users.click(function(e) {
		  var _thisbtn = $(this);
          var nav_stacked = windbox.find(".nav-stacked");
          var p_li = nav_stacked.find("li:has(ul)");
          p_li.show();
		  p_li.find("ul").show();
          var unavailable=nav_stacked.find(".unavailable");
          if(usreonlin){
              unavailable.hide();

	          var _available=nav_stacked.find(".available");
	          for(var lio=0;lio < _available.length;lio++){
	              var _lipobj=_available.eq(lio);
	              var p__li=_lipobj.parents(".children").eq(0);
	               p__li.show();
	          }

           	  usreonlin=false;
              _thisbtn.css("color","#ccc");
             _thisbtn.attr("title","显示离线用户");
          }else{
          
	          unavailable.show();
	          //unavailable.parents(".parent").eq(0).hide();
	          usreonlin=true;
	          _thisbtn.css("color","#001DFF");
	          _thisbtn.attr("title","只显示在线用户");

          }

		  return true;

		});
		//未读信息
		windbox.find(".bottombox .fa-comments-o").click(function(e) {
			var _thisbtn = $(this);
			//usreonlin=false;
			var rec_user_msg_number = Chat.rec_user_msg_numb;
			if(!$.isEmptyObject(rec_user_msg_number)){
				var nav_stacked = windbox.find(".nav-stacked");
				var p_li = nav_stacked.find("li:has(ul)");
				p_li.find("li").hide();
				p_li.find("ul").hide();
				p_li.hide();
				for(var key in rec_user_msg_number){
					var userlijq =windbox.find("li[id='" +key.replace("@", "-").replace(/\./g,"-")  + "']") ;
					for(var i=0;i<userlijq.length;i++){
						$(userlijq[i]).show();
						$(userlijq[i]).parents("ul").eq(0).show();
						$(userlijq[i]).parents(".parent").eq(0).show();
					}
				}
			}
			return true;


		});

	},
	userlisteCss:function(){
		var userliste = $('body').find(".WB_webimbox .userliste");
		var sollh =Chat.dialogh - 104;
		//需要滚动条   
		// if (sollh.height() > sollh) {

		userliste.css({
			"max-height": sollh + "px",
			"height": sollh + "px"
		});
		userliste.mCustomScrollbar({
			axis: "y",
			scrollInertia: sollh,
			scrollbarPosition: "outside"
		});
		userliste.find(".mCSB_scrollTools").css("right", "-10px");
	},
	

	chatwindow : function(titletxt) {//开启聊天窗口

		//var groupsdata = _private.groupsdata(_opts, groupsid)[0];

		//var userdatas = _private.userdata(_opts, _opts.userid, groupsdata)[0];

		//console.log(userdatas);
		//稍后补加
		//历史记录
		//var storyhtml = _private.Readthehistory(_opts.userid);
		//if (storyhtml == null || typeof storyhtml == "undefined" || !storyhtml) {
			//storyhtml = "欢迎...";
		//}

		//_opts.groupId = groupsid;

		//点击打开对话窗口时，先加载历史信息跟未读信息，
		var msgs= Chat.rec_user_msg[Chat.rec_jid];
		var storyhtml = "欢迎...";
		if(msgs){
			for(var i=0;i<msgs.length;i++){
				//把发送到消息添加到上方对话框中
				for(var key in msgs[i]){
					if(key==Chat.rec_jid){
						var userName_="<span class='usernamestyle'>"+$($('body').find('li#'+Chat.jid_to_id(Chat.rec_jid)+' .u_itmename')[0])[0].innerHTML+"</span>"
		
					var _txt = "<div class='inforow msgleft'>"+userName_ + msgs[i][key] + "</div>";
					storyhtml+=_txt;
					}else if(key==Chat.connection.jid.split('/')[0]){
						var userName_="<span class='usernamestyle'></span>"
		
					var _txt = "<div class='inforow msgright'>"+userName_ + msgs[i][key] + "</div>";
					storyhtml+=_txt;
					}
				}
				/*if(msgs[i][Chat.rec_jid]){
					var userName_="<span class='usernamestyle'>"+$($('body').find('li#'+Chat.jid_to_id(Chat.rec_jid)+' .u_itmename')[0])[0].innerHTML+"</span>"
		
					var _txt = "<div class='inforow msgleft'>"+userName_ + msgs[i] + "</div>";
					storyhtml+=_txt;
				}else if(msgs[i][Chat.connection.jid]){
					var userName_="<span class='usernamestyle'></span>"
		
					var _txt = "<div class='inforow msgright'>"+userName_ + msgs[i] + "</div>";
					storyhtml+=_txt;
				}*/
				
				//Chat.addMessage(Chat.connection.jid,notReallyMsgs[i]);
			}
			//到打开对话框时，如果历史消息或者未读消息大于50条，
			//将保存历史消息或者未读消息的后50条作为下次打开聊天窗口的历史消息
			if(msgs.length>5){
				msgs.reverse();
				delete Chat.rec_user_msg[Chat.rec_jid];
				msgs.splice(5);
				msgs.reverse();
				Chat.rec_user_msg[Chat.rec_jid]=msgs;
			}
			
		}
		//打开聊天窗口时判断rec_user_msg_numb是否为空，如果为空将不再闪烁
		if(!$.isEmptyObject(Chat.rec_user_msg_numb)){
			if(Chat.rec_user_msg_numb[Chat.rec_jid]){
				delete Chat.rec_user_msg_numb[Chat.rec_jid];
				if($.isEmptyObject(Chat.rec_user_msg_numb)){
					Chat.msg_mationbreathing_ions();
				}
			}
		}
		
		
		
		var titletxt = titletxt;
		var body = $('body');
		var userliste = body.find('.WB_webimbox');


		var closed = true;

		if (Chat.dialog) {
			closed = Chat.dialog.closed
		}

		if (!closed) {



			var chetdom = Chat.dialog;

			if (chetdom.id != Chat.rec_jid) {

				chetdom.id = Chat.rec_jid;
			}
				chetdom.title(titletxt);

				var __dom = chetdom.DOM.dialog;

                var contdomroot=__dom.find(".chatshowbox .mCSB_container");

				contdomroot.empty().html(storyhtml);
				//清除服务端--下次请求就可以发起
				/*_opts.delHistroy=true;
				 _opts.chatId=userdatas.id;*/
				//__dom.find(".form-control").val("").focus();
				var myeditorobj=nicEditors.findEditor('mangbox');
				myeditorobj.setContent("");
				$(myeditorobj.elm).focus();
                
              /*  //图片预览
				_private.imgidevente(contdomroot);

				//音频处理
				_private.audiosevente(contdomroot);*/


				/*var time = _opts.WB_webimbox_time;
				if (time != "" && typeof time != "undefined") {
					window.clearTimeout(_opts.WB_webimbox_time);
				}*/

				var userlistep = userliste.offset();
				chetdom.left = userlistep.left - 510;
				chetdom.top = userlistep.top;
				chetdom.show();


		} else {

			//dialogh外高
			var dloh = Chat.dialogh - 97;
			var re_h = dloh - 140;
			var chatshowbox_h = (re_h - 46);

			// _opts.dialog = new lhgdialog();

			var userlistep = userliste.offset();

			Chat.dialog = new lhgdialog({
				width: 490,
				height: dloh,
				title: titletxt,
				fixed: true,
				id:Chat.rec_jid,
				left: userlistep.left - 510,
				top: userlistep.top,
				content: "<div class='chatshowbox' style='height:" + chatshowbox_h + "px'>" + storyhtml + "</div><div class='chattootms btn-group'></div><div class='proresbox'><progress id='progressBar' value='0' max='100'></progress></div><div class='inputbox'><textarea id='mangbox' class='form-control'></textarea></div>",
				init: function() {
					var __dialogthis = this.DOM;
					var nodedom = __dialogthis.dialog;
					var ui_buttons = nodedom.find(".ui_buttons");
					var buttons = $('<div class="errorinfobox"></div><div class="btn-group"><button type="button" class="btn btn-success">发送</button><button type="button" class="btn btn-success dropdown-toggle"><span class="caret"></span></button></div>');

					//清除服务端-----下次请求就可以发起
				    /*_opts.delHistroy=true;
				    _opts.chatId=userdatas.id;*/

					buttons.find("button:first").click(function(e) {

						// console.log(e.keyCode);
						/*_private.sendfn(_opts, nodedom);*/
						var jid=Chat.rec_jid;
						var myeditorobj=nicEditors.findEditor('mangbox');
						var __valtxt = $.trim(myeditorobj.getContent());
						//var body=$(".nicEdit-main").innerText;
						//发送消息
						Chat.sendChatMsg(jid,__valtxt);
						//把发送到消息添加到上方对话框中
						Chat.addMessage(Chat.connection.jid,__valtxt);
						//将消息放到缓存中
						var dt={};
						dt[Chat.connection.jid.split('/')[0]]=__valtxt;
						Chat.users_mgs_cache(jid,dt);
						//清空
						myeditorobj.setContent("");
						return true;
					});
					var setbntlast = buttons.find("button:last");
					buttons.appendTo(ui_buttons);
					ui_buttons.show();
                 new nicEditor({buttonList : ['fontSize','ol','ul','hr','uploadfile','Recordericos'],fileservpath:Chat.fileservpath}).panelInstance('mangbox');
                var myeditorobj=nicEditors.findEditor('mangbox');
                 var elm=$(myeditorobj.elm);
                 elm.focus();                  
                 elm.keypress(function(e){

                     if(/*!_opts.enters && */e.keyCode === 13 && !e.ctrlKey){

                     	//_private.sendfn(_opts, nodedom);
                     	var jid=Chat.rec_jid;
						var myeditorobj=nicEditors.findEditor('mangbox');
						var __valtxt = $.trim(myeditorobj.getContent());
						//var body=$(".nicEdit-main").innerText;
						//发送消息
						Chat.sendChatMsg(jid,__valtxt);
						//把发送到消息添加到上方对话框中
						Chat.addMessage(Chat.connection.jid,__valtxt);
						//将消息放到缓存中
						var dt={};
						dt[Chat.connection.jid]=__valtxt;
						Chat.users_mgs_cache(jid,dt);
						//清空
						myeditorobj.setContent("");
						return true;
                     }

                     if(/*_opts.enters &&*/ e.ctrlKey){
                     
                      if(e.keyCode === 10 || e.keyCode ===13){
                     	//_private.sendfn(_opts, nodedom);
                     	e.preventDefault();

                     	 return false;

                     	}

                     }
         
                     
                });

                  //console.log(dsr);


					setbntlast.click(function(e) {
						e = e ? e : window.event;
						e.cancelBubble = true;
						e.stopPropagation();
						var topbox = __dialogthis.border.parents("div").eq(0);
						//var p__div=nodedom.
						var zindex = window.parseInt(topbox.css("z-index")) + 2;
						//_private.setEnter(_opts, $(this), zindex);
					});

					setbntlast.mouseleave(function() {

						/*var times = _opts.setentertime;
						if (times != "" && times != "undefined") {
							window.clearTimeout(_opts.setentertime);
						};*/

						/*_opts.setentertime = window.setTimeout(function() {
							$(".setenter__box").hide();
						}, 400);*/

					});


					//整个dialog

					nodedom.hover(function(e) {

						e.stopPropagation();
						/*var time = _opts.WB_webimbox_time;
						if (time != "" && typeof time != "undefined") {
							window.clearTimeout(_opts.WB_webimbox_time);
						}*/

						body.find(".WB_webimbtn").hide();
						userliste.show();
						Chat.dialog.show();


					}, function(e) {

						e.stopPropagation();

					//if(typeof window.nohover!="undefined" && window.nohover!=null)
					//_opts.nohover = window.nohover;

						/*if (_opts.nohover) {

							var time = _opts.WB_webimbox_time;
							if (time != "" && typeof time != "undefined") {
								window.clearTimeout(_opts.WB_webimbox_time);
							}
							//var __box = $(this);
							var __windbox =
								_opts.WB_webimbox_time = setTimeout(function() {									
									_opts.dialog.hide();
									userliste.hide();
									body.find(".WB_webimbtn").show();
									
                       //时时检查本地是否有，没有阅读过的消息
                      _private.everymsgunread(_opts);

								}, 300);

						}*/

					});



					//var form_control = nodedom.find(".form-control");
					var chatshowbox = nodedom.find(".chatshowbox");

					// form_control.keydown(function(e) {

					// 	//console.log(e.keyCode);


					// 	if (e.keyCode == 13) {

					// 		_private.sendfn(_opts, nodedom);

					// 	}

					// });


					//滚动条        
					chatshowbox.mCustomScrollbar({
						axis: "y",
						scrollInertia: chatshowbox.height(),
						scrollbarPosition: "outside"
					});
					chatshowbox.find(".mCSB_scrollTools").css("right", "-10px");

					nodedom.find(".chatbox").css({
						"transform": "initial",
						"opacity": "initial"
					});

				}

			}).show();

			var dialogdomroot = Chat.dialog.DOM.dialog;
			//_opts.dialog.id = "chatwindow_" + _opts.userid;
			dialogdomroot.addClass('chatbox');
			dialogdomroot.find(".ui_content").css({
				"padding": "0px",
				"width": "100%"
			});
			dialogdomroot.css({
				"top":"800px"
			});

		};


	},
	sendChatMsg:function(jid,body){//发送普通信息
		var id=jid.replace("@", "-").replace(/\./g,"-")+"-"+Chat.connection.rid;
		var msg = "$msg({to: '"+jid+"', type: 'chat',id:'"+id+"'}).c('body').t('"+body+"').up().c('request',{'xmlns':'urn:xmpp:receipts'})";
		Chat.connection.send(eval(msg));
	},
	addMessage:function(jid,__valtxt){//将消息添加到上发聊天框中
		var __dom = Chat.dialog.DOM.dialog;
		var chatshowbox = __dom.find(".chatshowbox .mCSB_container");
		var mgspoin = " msgleft",userName;
		if(jid==Chat.connection.jid){
			mgspoin = " msgright";
			userName="";
		}else{
			userName="<span class='usernamestyle'>"+$('body').find('li#'+Chat.jid_to_id(jid)+' .u_itmename')[0].innerText+"</span>"
		}
		var _txt = "<div class='inforow" + mgspoin + "'>"+userName + __valtxt + "</div>";
		//chatshowbox.html(chatshowbox.html() + _txt);
		chatshowbox.append(_txt);
		$(".chatshowbox").mCustomScrollbar("scrollTo", "last");
		Chat.imgidevente(chatshowbox);
		Chat.audiosevente(chatshowbox);
	},
	write_user_msgNumber:function(senderJid){

		var msgnumber_li = $("li[id='" +Chat.jid_to_id(senderJid)+ "']");
		var msgnumber_box = msgnumber_li.find(".col-md-1");

		//var mgsNumber=Number(msgnumber_box.html().replace('(','').replace(')',''))+1;
		msgnumber_box.html("(" + Chat.rec_user_msg_numb[senderJid] + ")");
	},
	clear_user_msgNumber:function(){
		var msgnumber_li = $("li[id='" +Chat.jid_to_id(Chat.rec_jid)+ "']");
		var msgnumber_box = msgnumber_li.find(".col-md-1");
		msgnumber_box.html("");
	},
	//信息呼吸ions
	msg_mationbreathing_ions:function(){
        var WB_webimbtn=$(".WB_webimbtn");
        var iobj=WB_webimbtn.find("i"); 
		if(!$.isEmptyObject(Chat.rec_user_msg_numb)){
		    Chat.showImg(iobj[0]);
		}else{
			iobj[0].style.visibility = "hidden";
		}

	},
	showImg:function(imgId){ 

	    if(imgId.style.visibility == "visible") 
	        imgId.style.visibility = "hidden"; 
	    else 
	        imgId.style.visibility = "visible";
	    setTimeout(function(){
	    	Chat.showImg(imgId);
	    },300);               
	//间隔的毫秒
	},
	//搜索用户
	Quicksearch:function( _val) {
		var _searchval = new RegExp(_val, 'i');
		var parentli = $(".userliste ul li:has(ul)");
		parentli.find("ul").hide();
		parentli.find("ul li").hide();
		var nameParentNodes=parentli.find('.u_itmename');
		for(var i=0;i<nameParentNodes.length;i++){
			var name=$(nameParentNodes[i]).children()[0].innerHTML;
			if(_searchval.test(name)){
				$(nameParentNodes[i]).parents('ul').eq(0).show();
				$(nameParentNodes[i]).parents('li').eq(0).show();
			}
		}
		

	},
	//图片事件
   imgidevente:function(domroot){

   	  var imgpptbtn=domroot.find("img[id^='imgid_']");

			   for(var imgs=0;imgs < imgpptbtn.length;imgs++){

			   	var imgobjs=imgpptbtn.eq(imgs);

               imgobjs[0].onload=function(){
                  
                  var imgthis=$(this);
                 
                 // if(imgthis.attr("setdatas")!="ok"){                  

                  	imgthis.click(function(e){
                     e.stopPropagation();
                     var _thisbtn = $(this);                    
                     var imgsrc=_thisbtn.attr("src");
                     $.fancybox.open(imgsrc);
                     return false;
                  });

                //  }

               // imgthis.attr("setdatas","ok");

               }

           }



   },
   //音频事件
   audiosevente:function(rootdom){

    
        var prybtn=rootdom.find("span[rel^='audios_']");
        //判断音频是否加载
        for(var spanbtn_=0;spanbtn_ < prybtn.length;spanbtn_++){
        	var spanlable=prybtn.eq(spanbtn_);

           if(spanlable.attr("setdatas")!="ok"){

        	spanlable.removeClass('fa-volume-up').addClass("fa-volume-off");


        	var spanlablerel=spanlable.attr("rel");
        	var audioslable=$("#"+spanlablerel+"");
        	audioslable[0].oncanplay=function(){

        		spanlable.css("color","#000");
        		spanlable.attr("setdatas","ok");

        	spanlable.click(function(e){

           e.stopPropagation();
           var _thisbtn = $(this);
           var relids=_thisbtn.attr("rel");
           var audiodom=document.getElementById(relids);
		audiodom.play();        		

		audiodom.onplay=function(){

 			_thisbtn.removeClass('fa-volume-off').addClass("fa-volume-up");

		}

		audiodom.onended=function(){

			_thisbtn.removeClass('fa-volume-up').addClass("fa-volume-off");

		}

		return false;

          });



        	}

        }else{
        	spanlable.css("color","#ccc");
        }


        }


   },
   users_mgs_cache:function(jid,jsonDt){
   		/*var dt={};
		dt[jid]=msgContent;*/
		if($.isEmptyObject(Chat.rec_user_msg)){
				Chat.rec_user_msg[jid]=[jsonDt];
				Chat.senderArray.push(jid);
		}else{
			for(var i=0;i<Chat.senderArray.length;i++){
				if(Chat.senderArray[i]==jid){
					Chat.rec_user_msg[jid].push(jsonDt);
					return;
				}else if(i==Chat.senderArray.length-1){
					Chat.rec_user_msg[jid]=[jsonDt];
					Chat.senderArray.push(jid);
					return;
				}
			}
		}
   }
	
}
