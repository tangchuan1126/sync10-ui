(function() {

  $(function() {

    // d3.json('data/miserables.json', function(data) {
    //   return new NetworkView({
    //     selector: '#network1, #network2',
    //     graph: data
    //   });
    // });

//========================

   var json_graph={
  "nodes": [
    {"name": "A", "type": "one", "size": 4},
    {"name": "B", "type": "one", "size": 2},
    {"name": "C", "type": "two", "size": 7},
    {"name": "a1", "type": "two", "size": 3},
    {"name": "a2", "type": "two", "size": 3}
  ],
  "links": [
    {"source": 2, "target": 0, "type": 0, "weight": 1},
    {"source": 2, "target": 1, "type": 0, "weight": 1},
    {"source": 0, "target": 3, "type": 0, "weight": 1},
    {"source": 0, "target": 4, "type": 0, "weight": 1}
  ]
}

/*

nodes--画圆点
==========================
name--是唯一的如 kye
type--表示颜色
size--点的大小



links--画线
=============================
weight--表示线的粗细度，如果是级别高就可以加粗
type--点的颜色

一根线链接有两端，起点和终点
target--终点
source --起点

   var json_graph={
  "nodes": [
    {"name": "A", "type": "one", "size": 4},
    {"name": "B", "type": "one", "size": 2},
    {"name": "C", "type": "two", "size": 7}
  ],
  "links": [
    {"source": 0, "target": 1, "type": 1, "weight": 2},
    {"source": 0, "target": 2, "type": 1, "weight": 1},
    {"source": 1, "target": 2, "type": 2, "weight": 3}
  ]
}

*/

var net = new NetworkView({selector: '#network3', graph: json_graph});

    // return d3.json('data/foo.json', function(data) {
    //   return new NetworkView({
    //     selector: '#network3',
    //     graph: data
    //   });
    // });


  });

}).call(this);
