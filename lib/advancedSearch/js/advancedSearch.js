"use strict";

(function(){
	
	define(["jquery", "art_Dialog/dialog-plus", "require_css!bootstrap-css/bootstrap.min", "require_css!oso.lib/advancedSearch/css/advancesearch.css"], function($, Dialog){

		var AdvancedSearch = function(options){

			if(!options.container){
				throw new Error("Do not specify a container!");
			}

			var	defaults = {
					data : [],
					url : "",
					searchBtn : true,
					conditionTitle : "Condition",
					border : true,
					dialogWidth : 650, 
					dialogHeight :  180,
					condition : "top",
					search : function(){},
					clear : function(){},
				},
				opts = $.extend(defaults, options),
				events = {"search.event" : function(){}, "clear.event" : function(){}},
				_createHTML, _createChild, _createCondition, _clearChild, _bindEvent, _buildSearch, _getSelectedData,
				panel;

			opts.container = $(opts.container);

			_createHTML = function(data){

				if(!data || !data.length){
					return;
				}

				if(opts.container.data('advancedSearch')){
					opts.container.find('.panel-select').remove();
				}

				var	ul = $('<ul class="list-group">'),
					i, len, j, length, json, li, dl, data_item, dd, btn;

				panel = $('<div class="panel panel-select"></div>');
				if(opts.border){
					panel.addClass('panel-default');
				}

				for(i = 0, len = data.length; i < len; i++){
					json = data[i];
					li = $('<li class="list-group-item"></li>');
					dl = $('<dl><dt>' + json.type+ '</dt></dl>');

					for(j = 0, length = json.data.length; j < length; j++){
						data_item = json.data[j];
						dd = $('<dd><a href="javascript:void(0)">' + data_item.name+ '</a></dd>');
						data_item.mulit = json.mulit;
						data_item.type = json.type;
						data_item.url = json.url;
						data_item.key = json.key;
						dd.data('data', data_item);
						dl.append(dd);
					}
					if(json.mulit){
						btn = $('<button type="button" class="btn btn-primary mulit">mulit</button>').data('data', json);
						li.append(btn);
					}

					ul.append(li.append(dl));
				}
				panel.append(ul);

				_bindEvent(panel);

				opts.container.append(panel).data('advancedSearch',true);
				
				opts.container.find('li').each(function(index, elem){
					var _self = $(elem);
					_self.find('dt').css('top', (_self.height() / 2) + 'px');
				});
			};

			_createChild = function(data, parent, parentId, parentKey){
				
				if(!data||!data.data){
					return;
				}

				var li = $('<li class="list-group-item ' + parentId.join(' ') + ' ' + parentKey.join(' ')+ '"></li>').data('parent', {id : parentId, key : parentKey}),
					dl = $('<dl><dt>' + data.type+ '</dt></dl>'),
					i, len, item, dd, btn;
				for(i = 0, len = data.data.length; i < len; i++){
					item = data.data[i];
					dd = $('<dd><a href="javascript:void(0)">' + item.name+ '</a></dd>');
					item.mulit = data.mulit;
					item.type = data.type;
					item.key = data.key;
					item.url = data.url;
					dd.data('data', item);
					dl.append(dd);
				}
				if(data.mulit){
					btn = $('<button type="button" class="btn btn-primary mulit">mulit</button>').data('data', data);
					li.append(btn);
				}

				li.append(dl).insertAfter(parent);
				li.find('dt').css('top', (li.height() / 2) + 'px');
			};

			_createCondition = function(data, li, parentId, parentKey, event){

				var condition = panel.find('.condition'),
					dd, i, len, item, 
					text = '', 
					type = '',
					isArray = data instanceof Array;	

				//构建condition列
				if(!condition.length){
					condition = $('<li class="list-group-item condition"><dl><dt>' + opts.conditionTitle+ '</dt></dl></li>');
					//search按钮
					if(opts.searchBtn){
						condition.append('<div class="search-btns"><button type="button" class="btn btn-primary btn-search"><i class="glyphicon glyphicon-search"></i>search</button><button type="button" class="btn btn-default btn-clear">clear</button></div>');
					}				
					condition.on('click', '.btn', function(event){

						var _self = $(this);

						if(_self.hasClass('btn-search')){
							//搜索
							opts.search(_getSelectedData(condition), event);
						}else if(_self.hasClass('btn-clear')){
							//清除条件
							opts.clear(_getSelectedData(condition), event);
							condition.find('a').trigger('click');
						}
					}).on('click', '.btn-search', events["search.event"]).on('click', '.btn-clear', events["clear.event"]);

					if(opts.condition === "top"){
						condition.insertBefore(panel.find('li').first());
					}else{
						panel.find('ul').append(condition);
					}
					
				}

				//构建标签
				if(isArray){
					for(i = 0, len = data.length; i < len; i++){
						item = data[i];
						if(i == len - 1){
							text += item.name;
						}else{
							text += item.name + ', ';
						}
					}
					type = data[0].type;
				}else{
					text = data.name;
					type = data.type;
				}

				dd = $('<dd class="' + (parentId ? parentId.join(' ') : '') + ' ' + (parentKey ? parentKey.join(' ') : '') + '"><a href="javascript:void(0)"><button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><span class="selected-text">' + type + ' </span>'  +  text + '</a></dd>')
					.on('click', 'a', function(event){

						_clearChild(data, isArray, panel, condition);

						li.show();
						
						if(!opts.searchBtn){
							//没有search按钮，直接执行clear
							opts.clear(_getSelectedData(condition), event);
						}
						
					}).data('data', data);

				condition.find('dl').append(dd);

				if(!opts.searchBtn){
					//没有search按钮，直接查询数据
					opts.search(_getSelectedData(condition), event);
				}
				condition.find('dt').css('top', (condition.height() / 2) + 'px');
			};

			_getSelectedData = function(condition){
				var	arr = condition.find('dd'),
					arr_data = [], id,
					i, len, data, item, j, length;

				for(i = 0, len = arr.length; i < len; i++){
					data = $(arr[i]).data('data');
					if(data instanceof Array){
						id = [];
						for(j = 0, length = data.length; j < length; j++){
							id.push(data[j].id);
						}
						item = $.extend(true, {}, data[0]);
						item.id = id.join(',');
						arr_data.push(item);
					} else{
						arr_data.push(data);
					}
				}
				return arr_data;
			};

			_clearChild = function(data, isArray, panel, condition){

				var parentId, parentKey, i, len;

				if(isArray){
					parentId = data[0].id;
					parentKey = data[0].key;
				}else{
					parentId = data.id;
					parentKey = data.key;
				}

				panel.find('.' + parentId + '.' + parentKey).remove();

				if(condition.find('dd').length == 0){
					condition.remove();
				}
			}

			_bindEvent = function(panel){
				panel.on('click', 'a', function(event) {

					var _self = $(this),
						li = _self.parents('li').eq(0),
						data = _self.parent().data('data'),
						parentDate = li.data('parent');
					
					_buildSearch(data.url, data.data, _createChild, li, [data.id], data.key);
					
					_createCondition(data, li, (parentDate ? parentDate.id : []).concat([data.id]), (parentDate ? parentDate.key : []).concat([data.key]), event);

					li.hide();

				}).on('click', '.mulit', function(event){
					var _self = $(this),
					 	data = _self.data('data'),
						parent = _self.parents('li').eq(0),
						ul = $('<ul class="list-inline panel-select-dialog"></ul>'),
						i, len, arr = data.data, item, li, dialog;

					if(!data){
						return;
					}

					for(i = 0, len = arr.length; i < len; i++){
						item = arr[i];
						li = $('<li><a href="javascript:void(0)">' + item.name+ '</a></li>').find('a').on('click', function(){
							var _self = $(this);
							if(_self.hasClass('selected')){
								_self.removeClass('selected');
							}else{
								$(this).addClass('selected');
							}
							
						}).data('data', item).end();

						ul.append(li);
					}

					dialog = new Dialog({
					    content: ul,
					    lock:true,
					    width: opts.dialogWidth,
					    height: opts.dialogHeight,
					    title:data.type,
					    ok: function (event) {
					    	var arr = [], 
					    		parentId = [],
					    		i, len, data, 
					    		parentDate = parent.data('parent'),
					    		selecteds = ul.find('.selected');

							for(i = 0, len = selecteds.length; i < len; i++){
								data = $(selecteds[i]).data('data');
								parentId.push(data.id);
								arr.push(data);
							}

							if(!arr.length){
								dialog.close();
								return;
							}

							_createCondition(arr, parent, parentId.concat(parentDate ? parentDate.id : []),(parentDate ? parentDate.key : []).concat([data.key]), event);
							_buildSearch(data.url, null, _createChild, parent, parentId, data.key);
							parent.hide();

							dialog.close();
					    },
					    cancelVal: 'No',
					    cancel: function(){
					    	dialog.close();
						}
					}).showModal();

				});

			};

			_buildSearch = function(url, data, fn, parent, parentId, key){

				var parentLiData, temp, parentLiDataId, parentLiDataKey;

				if(parent){
					parentLiData = parent.data('parent');
				} 

				parentLiDataId = parentLiData ? parentLiData.id : [];
				parentLiDataKey = parentLiData ? parentLiData.key : [];

				if(parentId){
					temp = {};
					temp[key] = parentId.join(',');
				}else{
					temp = [];
				}
				

				//获取data
			    if(url){
			    	$.ajax({
			    		url : url,
			    		data : temp

			    	}).done(function(data){
			    		if(data){
			    			//请求成功
			    			fn(data, parent, parentLiDataId.concat(parentId), parentLiDataKey.concat([key]));
			    		}

			    	}).fail(function(jqXHR, textStatus, SyntaxError){
			    		alert("Request failed: " + textStatus + "," + SyntaxError);
			    	});
			    }else if(data){
			    	fn(data, parent, parentLiDataId.concat(parentId), parentLiDataKey.concat([key]));
			    }

			};

			this.on = function(str, fn){
				events[str] = fn;
				return this;
			};

			this.setSearch = function(fn){
				if(!$.isFunction(fn)){
					return;
				}
				opts.search = fn;
				return this;
			};

			this.setClear = function(fn){
				if(!$.isFunction(fn)){
					return;
				}
				opts.clear = fn;
				return this;
			};

			this.setData = function(data){
				_buildSearch("", data, _createHTML);
				return this;
			};

			this.setUrl = function(url){
				_buildSearch(url, [], _createHTML);
				return this;
			};

			this.remove = function(){
				panel.remove().end().data('advancedSearch',false);
			};

			this.getSelectedData = function(){
				return _getSelectedData(panel.find('.condition'));
			};

			_buildSearch(opts.url, opts.data, _createHTML);

		};

		return AdvancedSearch;
	});
})(this);