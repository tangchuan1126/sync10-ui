"use strict";

require([
     "../../requirejs_config", 
], function(){
    require(["jquery", "advancedSearch"], function($, AdvancedSearch){

    	$(function () {
    		var a = new AdvancedSearch({
				container : '.container',
//				url : 'data.json',
				data : [],
				searchBtn : false, 				//默认true
				conditionTitle : "Condition: ",
				border : true,					//默认true
				condition : "top",				//默认“top”
				search : function(data, event){
					console.log(data);
				},
				clear : function(data){
					console.log(data);
				}
			});

			

			$("#remove").click(function(){
				a.remove();
			});

			$("#addUrl").click(function(){
				a.on("search.event", function(event){
					alert("search.event")
				}).on("clear.event", function(){
					alert("clear");
				}).setUrl("data.json");
			});

	    });
    });

});
