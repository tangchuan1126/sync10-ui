
(function() {

	var Collection_init = function($,Backbone,CheckinModel) {
        
       
	       
		    var CheckinCollection = Backbone.Collection.extend({
        model: CheckinModel,
        url: null,
        dft:null,
        View_control:null,
        PostData:{},
        initialize:function(){
                // console.log(this);
        },
        parse: function(response) {
          return response;
        }

      }, {
        pageCtrl: {
          pageNo: 1,
          pageSize: 10
        }        
      });


      return CheckinCollection;

	}; 

	if (typeof define === 'function' && define.amd) {
		define([
			"jquery",
			"backbone",
      "./Model"
		],Collection_init);
	} else {
		Collection_init($,Backbone,CheckinModel);
	}

}).call(this);

