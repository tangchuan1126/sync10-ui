
var __GLOBALATH=window.location.pathname.replace(/Sync10-ui\/.*/,"Sync10-ui/");

requirejs.config({
    baseUrl:__GLOBALATH,
    paths:{
        "jquery":"bower_components/jquery/dist/jquery.min",
        "backbone":"bower_components/backbone/backbone.min",
        "underscore":"bower_components/underscore/underscore.min",
        "art_Dialog":"bower_components/artDialog/src",
        "Paging":"bower_components/Paging/Paging",
        "JSONTOHTML":"bower_components/json2html/js/json2html",
        "Bootstrap":"bower_components/Bootstrap/js/bootstrap.min",
        "metisMenu":"bower_components/Bootstrap/js/plugins/metisMenu/metisMenu.min",
        "checkinJS":"pages/Examples/bootstrap/checkin/js"
    },
    shim: {
        'JSONTOHTML': {
            exports: 'json2html'
        },
        'Bootstrap': {
            exports: 'Bootstrap',
            deps:['jquery']
        },
        'metisMenu': {
            exports: 'metisMenu',
            deps:['jquery']
        }
    }
});
