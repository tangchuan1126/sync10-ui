/**
 *
 * @authors gaowenzhen (gaowenzhen@msn.com)
 * @date    2014-08-28 17:56:49
 * @version 0.01
 * @check_in
 */

define([
  "jquery",
  "backbone",
  "./Collection",
  "Paging",
  "JSONTOHTML",
], function($, Backbone, Collection,Paging,JSONTOHTML) {

 var _paths=requirejs.s.contexts._.config.paths;
     var serverpat= _paths["jquery"];
    serverpat=serverpat.replace(/\/bower_components\/.*/,"");
 var cssbtn="require_css!"+serverpat+"/css/buttons/buttons.css";

  require(["require_css!oso.lib/Paging/css/pagestyle.css","require_css!oso.lib/CompondList/css/style.css",cssbtn])

  var View = Backbone.View.extend({
    el: null,
    Theadtemplate: function(data, thead) {
      return JSONTOHTML.transform(data, thead)
    },
    setTimeoutsirng: "",
    collection: new Collection(),
    initialize: function(Viewconfig, setval) {
      //初始

      if (setval) {

        this.collection.View_control = Viewconfig.View_control;
        this.collection.dft = setval;               
        var collinfo = this.collection.dft;
        this.$el = $(collinfo.renderTo);
        this.el = "'" + collinfo.renderTo + "'";
        this.collection.url = collinfo.dataUrl;
        if (collinfo.PostData)
          this.collection.PostData = collinfo.PostData;
        this.additionalEvents = collinfo.events;
        if (collinfo.pageSize)
          Collection.pageCtrl.pageSize = collinfo.pageSize;
        this.on(collinfo.onEvent);

      }

      this.loding();
    // this.resetcollectionfetch();



    },
    loding: function() {
      //载入等待

      var _thisbox = $("body");
      var loding = $(".loding");
      var win = $(window);
      var wininnerh = win.innerHeight();
      var wininnerw = win.innerWidth();
      var bodyh = _thisbox.height();
      var top = (wininnerh / 2) - 62;
      var left = (wininnerw / 2) - 62;
      var modalstyle = {
        "width": wininnerw + "px",
        "height": bodyh + "px",
        "display": "block"
      };
      var lodingstyle = {
        "top": top + "px",
        "left": left + "px",
        "display": "block"
      };
      if (loding.length < 1) {
        $("<div class='LodingModal'></div>").css(modalstyle).appendTo(_thisbox);
        $("<div class='loding'></div>").css(lodingstyle).appendTo(_thisbox);
      } else {
        loding.css(lodingstyle);
        $(".LodingModal").css(modalstyle);
      }


    },
    originalEvents: {
      //本身
      "click .fieldsetconte .select": "triggerDoucmentstatus",
      "mouseenter .SelectMorebox": "SelectMoreOn",
      "mouseleave .SelectMorebox": "SelectMoreOff",
      "mouseleave .select": "SelectMoreOffbtn",
      "mouseenter .tr_itme_style": "TrMoreOn",
      "mouseleave .tr_itme_style": "TrMoreOff",
      "click *[data-eventname]":"triggerOptions"
      //"click .TFOOTbutton .buttons": "triggertfootbtnEvent",
      //"click .td_Options button,.buttonallevnts": "triggerOptions"
    },
    LineDatan: function(rowobj_id) {
      //取到第n页记录
      //取出行数据

      var _row_id = rowobj_id.split("|");

    //console.log(_row_id);

      var _data = this.collection.toJSON()[0];
      var _linedata = {};
      for (var key in _data.DATA) {
        var itmedata = _data.DATA[key];

        var rowobjname = eval("itmedata." + _row_id[0]);
        if (rowobjname != "" && typeof rowobjname != "undefined") {


          if (rowobjname == _row_id[1]) {
            _linedata = itmedata;
            break;
          }


        }


      }
      return {
        data: _linedata,
        meta: _data.meta
      };

    },
    additionalEvents: {
      //外部扩展     
    },
    triggerOptions: function(e) {

      var btn = $(e.currentTarget);
      var __trobj = btn.parents(".tr_itme_style").eq(0);
      var lineid = $(__trobj).data("itmeid");

      var _linedata={},itmedata={};

      if(__trobj.length < 1 && typeof lineid=="undefined" || lineid=="" || lineid==null){

          var lineID = $(btn.parents("div").eq(0)).data("itmeid");

      _linedata = this.LineDatan(lineID);

       itmedata = {
        linedata: _linedata.data,
        meta: _linedata.meta
      };


      itmedata={
        button: btn,
        data: itmedata
      }



      }else{

    _linedata = this.LineDatan(lineid);

       itmedata = {
        button: btn,
        data: _linedata.data
      };


      }      

   
      this.trigger(btn.data("eventname"), itmedata);

    },
    triggertfootbtnEvent: function(e) {

      // 创建外部监听一组按钮
      var btn = $(e.currentTarget);
      var lineID = $(btn.parents("div").eq(0)).data("itmeid");

      var _linedata = this.LineDatan(lineID);

      var itmedata = {
        linedata: _linedata.data,
        meta: _linedata.meta
      };

      var eventnamesring = btn.data("eventname");

      this.trigger(eventnamesring, {
        button: btn,
        data: itmedata
      });

    },
    events: function() {
      return _.extend({}, this.originalEvents, this.additionalEvents);
    },
    TrMoreOn: function(e) {
      var _this = $(e.currentTarget);
      _this.addClass("trOn");
    },
    TrMoreOff: function(e) {
      var _this = $(e.currentTarget);
      _this.removeClass("trOn");
    },
    SelectMoreOn: function(e) {
      //入
      var _this = $(e.currentTarget);
      var timeo = this.setTimeoutsirng;
      if (typeof timeo != "undefined" && timeo != "") {
        clearTimeout(this.setTimeoutsirng);
      }
      _this.show();
    },
    SelectMoreOff: function(e) {
      //出
      var _this = $(e.currentTarget);
      this.SelectMoreboxhide(_this);

    },
    SelectMoreOffbtn: function() {
      //离开btn时
      var _this = $(".SelectMorebox");
      this.SelectMoreboxhide(_this);
    },
    SelectMoreboxhide: function(obj) {

      var timeo = this.setTimeoutsirng;
      if (typeof timeo != "undefined" && timeo != "") {
        clearTimeout(this.setTimeoutsirng);
      }
      this.setTimeoutsirng = setTimeout(function() {
        obj.hide();
      }, 300);

    },
    triggerDoucmentstatus: function(e) {

      var _this = $(e.currentTarget);

      //具体的select
      var selectjson = _this.data("select");
      var selectarry = selectjson.split(/,/g);

      var div = $(".SelectMorebox");
      var liitme = "";
      for (var keyli in selectarry) {
        var selectimte = selectarry[keyli];
        var selevales = selectimte.split(/#-#/);
        liitme += "<a href='javascript:void(0)' data-para='" + selevales[0] + "'>" + selevales[1] + "</a>";
      }

      var opinsset = _this.offset();
      var h = _this.height();
      var tops = opinsset.top + h;
      var style = {
        "left": opinsset.left + "px",
        "top": tops + "px",
        "display": "block"
      }
      div.css(style).html(liitme);
      var a = div.find("a");

      var Viewthis = this;

      a.click(function() {

        var _athis = $(this);

        _athis.parents("div").eq(0).hide();
        var para = _athis.attr("para");
        var text = _athis.text();
        _this.find("span").html(text);


        Viewthis.trigger("selectbtnEvent", {
          button: _athis,
          data: _athis.data("para")
        });


      });

    },
    Add_Template: function(jsons, pageset) {

      var _this = this;
      var collinfo = _this.collection.dft;
      var Viewconfig = _this.collection.View_control;


      _this.$el = $(collinfo.renderTo);
      _this.el = "'" + collinfo.renderTo + "'";

      //表身
      var tbodytlp = _this.$el.find("tbody");

      //console.log(tbodytlp.length);


      if (pageset != 1) {
        tbodytlp.empty();
      }

      var JSONTmp = Viewconfig.templates;

      tbodytlp.html(JSONTmp.tbody.int(jsons, Viewconfig));

      var _table = _this.$el.find("table.checkin_box").eq(0);
      var tablew = _table.width();
      $(".topthead").width(tablew);
      var _panel = _table.parents(".panel").eq(0);
      if (_panel.length > 0) {
        var _bodyw = _this.$el.parents("body").eq(0).width();
        if (_bodyw < tablew) {
          _panel.css({
            "width": tablew + "px",
            "margin": "0px auto"
          });
        }
      }

      _this.topnvascroll(_table);

      //_this.Settopnvaheigth();


      var Firefox = navigator.userAgent.indexOf("Firefox");
      if (Firefox > 0) {
        var TRAILER_ring_title = $(".TRAILER .ring_title");
        TRAILER_ring_title.css("top", "-20px");
      }


      //总页数，当前页号，容器id，回调
      //当前页号
      var pageto = Collection.pageCtrl.pageNo;

      //服务端返回
      var pageCtrl = jsons.PAGECTRL;
      //总记录数
      var pageCount = pageCtrl.pageCount;
      if(pageCtrl.PAGECOUNT){
        pageCount=pageCtrl.PAGECOUNT;
      }

      if(pageCount==0 || typeof pageCount == "undefined" || pageCount==null){
         pageCount=1;
         pageto=1;
      }      

     if(collinfo.pageselect){

     // if(!collinfo.pageselect.index){

      // var jqbox=_this.$el.find(".pagebox");
      // var select_=jqbox.find(".Setpagesize select");
      // collinfo.pageselect.index=select_.index();
      // var selectindex=select_.val();
      // _this.collection.PostData.pageSize=selectindex;

     // }

     var page_list_Number=3;

      Paging.language="cn";
      Paging.update_page(pageCount, pageto, "pagebox", function(pageNo) {

        Collection.pageCtrl.pageNo = pageNo;

        var List = new View();
        List.render(2);

      },collinfo.pageselect,page_list_Number);


    }else{

    Paging.update_page(pageCount, pageto, "pagebox", function(pageNo) {

        Collection.pageCtrl.pageNo = pageNo;

        var List = new View();
        List.render(2);

      });

    }

    },
    deletetr:function(trjq,dataitmeid){

     // var dataitmeid==1234
    
      var colldata=this.collection;
      var _data = colldata.at(0).get("DATA");

      var deleteset=false;
     
      for (var key in _data) {

        var itmedata = _data[key];
         eval("var itmds=itmedata."+dataitmeid);
        if(itmds){
          delete itmedata;
          deleteset=true;
          break;
        }

      }

      
       if(trjq.length > 0){
          trjq.next().remove();
          trjq.remove();
       }

       return deleteset;


    },
    render: function(pageset) {

      var _this = this;

      $(".loding,.LodingModal").show();  

      // console.log(postdataall);

     _this.resetcollectionfetch(pageset);

     

    },
    resetcollectionfetch:function(pageset){

      var _this=this;

          var postdataall = {};

      var _pagesize = Collection.pageCtrl.pageSize;

       var collinfo = _this.collection.dft;
      // if(collinfo.pageSize){

      //   _pagesize=collinfo.pageSize;
      // }

      if(collinfo.PostData){

         if(collinfo.PostData.pageSize){
          
          _pagesize=collinfo.PostData.pageSize;

         }

      }


      var pageall = {
        pageNo: Collection.pageCtrl.pageNo,
        pageSize: _pagesize
      };

      if (_this.collection.PostData)
        var postdataall = _.extend(pageall, _this.collection.PostData);

 //请求服务端
      var fetchtype='GET';

      if(_this.collection.dft.fetchtype){
        fetchtype=_this.collection.dft.fetchtype;
      }

      _this.collection.fetch({
        //传递页号
        type: fetchtype,
        data: postdataall,
        //成功回调
        success: function(jsondata) {


          if (typeof jsondata == "string") {
            jsondata = JSON.parse(jsondata);
          }

         
     //  console.dir(_this.collection.fetch);
         

          //填写模版
          var jsons = jsondata.toJSON()[0];     

          //表头-只有第一次加载时执行
          if (typeof pageset == "undefined" || pageset == "") {

            var collectall = _this.collection;

           

            var Viewconfig = collectall.View_control;

            var tabledata = {
              "table": "",
              "children": [{
                "head": "",
                "children": {
                  "th": "",
                  "children": Viewconfig.head
                }
              }, {
                "tbody": ""
              }]
            };

            var JSONTmp = Viewconfig.templates;

            _this.$el.html(_this.Theadtemplate(tabledata, JSONTmp.table.thead));



          }

          _this.Add_Template(jsons, pageset);

          var bodyhs = $("body").height();
          $(".LodingModal").css("height", bodyhs + "px");
          $(".loding,.LodingModal").hide();

       
       // if(_this._events.editor_data_source){

         _this.trigger("editor_data_source", _this.collection.at(0));         
       

       // }


        },
        error: function(e) {

         _this.trigger("events.Error", {
          title:'请求服务端状态',
          Error: e
        });        

        }

      });



    },
    setQuery: function(keyobj) {
      //启用搜索
      var _this = this;

       Collection.pageCtrl.pageNo = 1;

      if (keyobj.dataUrl) { 
       
        _this.collection.url = keyobj.dataUrl;
        var _valobj = _.omit(keyobj, 'dataUrl');

        if (_valobj) {
          _this.collection.PostData={};
          var PostData = _this.collection.PostData;
          PostData = _.extend(PostData, _valobj);
        }

        var QueryList = new View();
        QueryList.render(2);



      } else {


        var collinfo = _this.collection.dft;
        if (keyobj)
          _this.collection.PostData = keyobj;     
        var QueryList = new View();
        QueryList.render(2);


      }


    },
    reloadData:function(){
        var QueryList = new View();
        QueryList.render(2);
    },
    upSubitmeData:function(updata){

     var _this=this;
      
     var _correct=[];

      if(typeof updata.upitme){

      var colldata=_this.collection;
      
        for(var key in updata.upitme){
      
         var rootkey=updata.upitme[key];
         var rotdataall = colldata.at(0).get(rootkey.Root);

         for(var rootkeyi in rotdataall){
          
           var allitme=rotdataall[rootkeyi];
           if(allitme[rootkey.itmekey] == rootkey.itmeid){
             var upitme_data=updata[rootkey.Root];
             rotdataall[rootkeyi]=upitme_data;
             _correct.push({id:rootkey.itmeid,correct:true});
             break;
           }

         }

        }


      }else{
       
         _this.trigger("events.updataError", {
          title:'修改数据时传值json结构错误',
          Error: ''
        }); 


     }

    return _correct;

    },
    topnvascroll: function(table) {
      //滚动时浮动 
      var _this = this

      if ($(".topthead").length < 1) {

        var _thead = table.eq("0").find("thead");

        setTimeout(function() {


          var tableth = _thead.html();

          var bodys = $("body");

          $("<div class='topthead'><table class='checkin_box'><thead>" + tableth + "</thead></table></div>").appendTo(bodys);

          var topthead = $(".topthead");

          _this.Settopnvaheigth();
 

          var _toppx = window.parseInt(table.position().top);

          $(window).scroll(function(e) {

            var toppx = $(this).scrollTop();

            if (toppx > _toppx) {

             topthead.show();

             // _this.Settopnvaheigth();

            } else {
              topthead.hide();
            }

        var win_scrollLeft=$(window).scrollLeft();

          if(win_scrollLeft > 0){
           
            topthead.css("left","-"+win_scrollLeft+"px");

          }




          });


          $(window).resize(function(){
           
            var topthead = $(".topthead");
            var __table = _this.$el.find("table").eq("0");
            var table_left=__table.offset();
            topthead.css("left",table_left.left+"px");
            $(window).scrollLeft(0); 

          
           _this.Settopnvaheigth();

          });


        }, 300);

      }else{

        _this.Settopnvaheigth();

      }

      //end 滚动时

    },
    Settopnvaheigth: function() {

      var topthead = $(".topthead");
      

      if (topthead.length > 0) {
        
        var _this = this;
               
        var _toptheadfind_table=topthead.find("table").eq(0);

        var __table = _this.$el.find("table").eq("0");

       var bodyw=$("body").width();
      
       var list_tablew=0;

       if(bodyw > __table.width()){  

        list_tablew=(bodyw - _this.$el.width());

       }

       topthead.css("left",list_tablew / 2);

        var _thead = __table.find("thead");
        var top_th = topthead.find("th");

        var thwall = [];
        var _thall = _thead.find("th");
        for (var th_i = 0; th_i < _thall.length; th_i++) {
          var itmeth = _thall.eq(th_i);
          thwall.push(itmeth.width());
        }

       var p1px=0;
        if(__table.width() == 1240){
         p1px=1;
        }

        for (var th_b = 0; th_b < top_th.length; th_b++) {
          var itmethb = top_th.eq(th_b);
          itmethb.width(thwall[th_b] - p1px);
        }

        _toptheadfind_table.width(__table.width());

      }

    }

  });



  return View;

});
