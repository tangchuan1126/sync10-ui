define(["./jsontohtml_templates"], function(tmp) {


	return {
		"View_control": {
			"head": [{
				"title": "Trailer",
				"field": "TRAILER"
			}, {
				"title": "Tractor",
				"field": "TRACTOR"
			}, {
				"title": "Time",
				"field": "TIME"
			}, {
				"title": "Log",
				"field": "CHECKINLOG"
			}, {
				"title": "Options",
				"field": [
					["Edit", "events.Edit"],
					["Print", "events.Print"]
				]
			}],
			"meta": {

				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"footer": [
				[
					"events.Photos", "Photos"
				],
				[
					"events.WindowCheckIn", "WindowCheckIn"
				],
				[
					"events.WindowCheckOut", "WindowCheckOut"
				],
				[
					"events.warehouseCheckIn", "WarehouseCheckIn"
				],
				[
					"events.gateCheckOut", "GateCheckOut"
				]
			],
			"templates": tmp
		}
	}

});