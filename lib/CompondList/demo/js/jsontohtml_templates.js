define(["JSONTOHTML"], function(JSONTOHTML) {

	//第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index) {
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}"
		}

	};

	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		head:null,
		int: function(jsons,View_control) {

			//console.log(jsons);

			//数据转换成表格形式
			// console.log(jsons.data);
			//["tr":"index","children":[{},{}]]
			var headallobj =View_control.head;			
			var footer = View_control.footer;
			_tbody.head=headallobj;
			var trfooterdata = {
				"colspan": headallobj.length,
				"children": footer,
				"trid":""
			};

			//tr表脚
			//var trfooter = (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));

			var pageSize=jsons.PAGECTRL.pageSize;


			//按表头转换成行和列

           var __tr = _tbody.TotrJson(jsons.DATA,pageSize);

			/*
			var __tr=[];			
			for (var tri = 0; tri < jsons.DATA.length; tri++) {
				var tritme = jsons.DATA[tri];
                var __td = [];
				for (var headkey in headallobj) {
					var tritmes = headallobj[headkey];
					var Rowname = tritmes.field;
					var Rowobj = {};
					if (typeof Rowname == "object") {
						__td.push({
							Options: Rowname
						});
					} else {
						Rowobj = eval("tritme." + Rowname);
						var setobjs = eval("({" + Rowname + ":Rowobj})");
						__td.push(setobjs);
					}

				}

				__tr.push({"trid":tritme.DLO_ID,"children": __td});
				if(tri >= pageSize){
					break;
				}
			}

			*/



			//拼装tr,trfooter，
			var __trhtml = "";
			for (var trkey in __tr) {
				var _tritmes = __tr[trkey];
				__trhtml += (JSONTOHTML.transform(_tritmes, _tbody.tr));				
				trfooterdata["trid"]=_tritmes.trid;
				__trhtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
			}

			return __trhtml;

		},
		TotrJson:function(DATA,pageSize){

         var headallobj=_tbody.head;
			var __tr=[];			
			for (var tri = 0; tri < DATA.length; tri++) {
				var tritme = DATA[tri];
                var __td = [];
				for (var headkey in headallobj) {
					var tritmes = headallobj[headkey];
					var Rowname = tritmes.field;
					var Rowobj = {};
					if (typeof Rowname == "object") {
						__td.push({
							Options: Rowname
						});
					} else {
						Rowobj = eval("tritme." + Rowname);
						var setobjs = eval("({" + Rowname + ":Rowobj})");
						__td.push(setobjs);
					}

				}

				__tr.push({"trid":tritme.DLO_ID,"children": __td});
				if(tri >= pageSize){
					break;
				}
			}

			return __tr;

		},
		keyname: function(obj) {
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id":function(obj){
              return obj.trid;
			},
			"data-itmeid":function(obj){

				//行的对象名|对比的key -（如id）

				return "DLO_ID|"+obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {

				//console.log(obj.children);			
				//自由列数
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {

				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
          
            //--------------------

				//##
				//行的对象名|对比的key -（如id）
				trhtmls += '<div data-itmeid="DLO_ID|'+obj.trid+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) {
					var itmea = obj.children[akey];
					ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
				}
				trhtmls +=ahtml;
				trhtmls += '</div>';

        //--------------------------------

				trhtmls +='</td>';
				return trhtmls;
			}
		},
		td: {
			tag: "td",
			class: function(obj, index) {

				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				classname = "td_" + keynames;
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "top";
				}
				return valignsing;
			},
			html: function(obj, index) {

				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);

				if (keynames != "" && typeof keynames != "undefined") {

					if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
					}

				}

				return retuhtml;
			}
		},
		TRAILER: {
			tag: "fieldset",
			class: "TRAILER",
			children: [{
				tag: "span",
				class: "ring_title",
				html: "<em>${TRAILER.ISLIVEKEY}</em>"
			}, {
				tag: 'legend',
				class: "td_legend",
				html: function(obj, index) {
					var legendhtml = "";
					var E_DLO_ID = obj.TRAILER.E_DLO_ID;
					legendhtml = '<span class="E_DLO_ID">' + E_DLO_ID + '</span>';
					if (obj.TRAILER.CONTAINER_NO) {
						var CONTAINER_NO = obj.TRAILER.CONTAINER_NO;
						legendhtml += '<i>|</i><span class="Sub_trailer">' + CONTAINER_NO + '</span>';
					}
					return legendhtml;
				}
			}, {
				tag: 'div',
				class: "fieldset_itme",
				html: function(obj, index) {
					var detailshtml = "";
					var details = obj.TRAILER.DETAILS[0];
					var DOORID = details.DOORID,
						NUMBERS = details.NUMBERS;
					if (DOORID != "" && typeof DOORID != "undefined" && NUMBERS != "" && typeof NUMBERS != "undefined") {
						detailshtml = (JSONTOHTML.transform(obj.TRAILER.DETAILS, _tbody.fieldset2));
					}
					return detailshtml;
				}
			},{
			  tag:"span",
			  class:"fieldset_tfoot",
			  html:function(obj,index){
			  	var txt=obj.TRAILER.ENTRYTYPE;
			  	if(txt!="" && typeof txt!="undefined"){
			  	return "<em>"+obj.TRAILER.ENTRYTYPE+"</em>";
			    }
			  }
			}]
		},
		fieldset2: {
			tag: "fieldset",
			children: [{
				tag: "legend",
				class: "itme_legend",
				align: "center",
				html: function(obj, index) {
					var returnlegend = "";
					returnlegend = '<span class="DOOR">DOOR：' + obj.DOORID + '</span>';

					if (obj.RELEASE.NAME) {
						returnlegend += '<button>' + obj.RELEASE.NAME + '</button>';
					} else {
						returnlegend += '<span class="Reserved">' + obj.DOORSTATUSTYPE + '</span>';
					}

					return returnlegend;
				}

			}, {
				tag: "div",
				class: "fieldsetconte",
				children: [{
					tag: "ul",
					html: function(obj, index) {
						var _NUMBERS = obj.NUMBERS;
						var ulhtml = "";
						for (var key in _NUMBERS) {
							var itmes = _NUMBERS[key];
							ulhtml = (JSONTOHTML.transform(itmes, _tbody.ulli));
						}
						return ulhtml;

					}

				}]
			}]


		},
		ulli: {
			tag: "li",
			class: "clearfix",
			html: function(obj, index) {
				var lihtml = "";
				lihtml = '<div class="fl">';

				if (obj.NUMBERTYPE) {

					var s = parseInt(obj.NUMBER_TYPE);
					var color = "";
					if (!isNaN(s)) {
						if (s == 10 || s == 11 || s == 17 || s == 18) {
							color = " blue";
						}
						if (s == 12 || s == 13 || s == 14) {
							color = " red";
						}
					}

					lihtml += '<span class="NUMBERTYPE ' + color + '">' + obj.NUMBERTYPE + ':</span><span class="NUMBER">' + obj.NUMBER + '</span>';
				}
				lihtml += '</div>';
				var slectdata = [];
				for (var keys in obj.SELECT) {
					var opinte = obj.SELECT[keys];
					var _PARA_VALUE = opinte.PARA + "#-#" + opinte.VALUE;
					slectdata.push(_PARA_VALUE);
				}

				lihtml += '<div class="fr">';
				if (obj.SELECT) {
					lihtml += '<label data-eventname="selectbtn.Event" class="select" id="btn_' + obj.NUMBER + '" data-select="' + slectdata + '">';
					lihtml += '<span>More</span><em><i class="button-dropdown"></i></em>';
					lihtml += '</label>';
				}
				lihtml += '<span class="Closed">' + obj.DOUCMENTSTATUS + '</span>';
				lihtml += '</div>';


				return lihtml;
			}
		},
		TRACTOR: {
			tag: "fieldset",
			children: [{
				tag: "legend",
				html: function(obj, index) {
					var legendhtml = "";
					legendhtml = '<span class="Sub_trailer">' + obj.TRACTOR.TRACTOR + '</span>';
					if (obj.TRACTOR.DELIVERY) {
						legendhtml += '<i>|</i><span class="deliverystyle">' + obj.TRACTOR.DELIVERY + '</span>';
					}
					return legendhtml;
				}
			}, {
				tag: "div",
				class: "Tractor_ulitme",
				children: [{
					tag: "ul",
					html: function(obj, index) {
						
						//console.log(obj.TRACTOR.CHILDREN);
						 return (JSONTOHTML.transform(obj.TRACTOR.CHILDREN, _tbody.fieldset3divulli));
					}
				}]

			}]

		},
		fieldset3divulli: {
			tag: "li",
			html: function(obj, index) {

					return '<span class="namestyle">'+obj.LABLE+':</span><span class="valstyle">' + obj.VALUE + '</span>';

			}
		},
		TIME: {
			tag: "fieldset",
			class: "Time",
			html: function(obj, index) {
				var Timehtml = "";

				Timehtml = '<legend class="td_legend"><span class="E_DLO_ID">' + obj.TIME.WHSE + '</span></legend>';
				Timehtml += '<span class="Timetitle">GateCheckIn:' + obj.TIME.GATECHECKIN + '</span>';
				Timehtml += '<div class="Time_fieldset">';

				if (obj.TIME.WINDOW_CHECK_IN_OPERATE_TIME) {

					Timehtml += '<fieldset id="' + obj.AREA_ID + '">';
					Timehtml += '<legend class="td_legend" align="center"><span>WindowCheckIn:' + obj.TIME.WINDOW_CHECK_IN_OPERATE_TIME + '</span></legend>';
					Timehtml += '<ul>';
					for (var WINDOWRESULTkey in obj.TIME.WINDOWRESULT) {
						var WINDOWRitmes = obj.TIME.WINDOWRESULT[WINDOWRESULTkey];

						for (var Ritmeskey in WINDOWRitmes.WINDOW_NOTICES_LOAD) {
							var itmeWINDOWRi = WINDOWRitmes.WINDOW_NOTICES_LOAD[Ritmeskey];
							//console.log(itmeWINDOWRi); 
							Timehtml += '<li id="' + itmeWINDOWRi.ZONE_ID + '"><span>Notified:' + itmeWINDOWRi.EMPLOYE_NAME + '</span><span data-eventname="events.detail" class="detail">' + itmeWINDOWRi.DETAIL.VALUE + '</span></li>';
						}

					}

					Timehtml += '</ul>';
					Timehtml += '</fieldset>';

				}


				if (obj.TIME.WAREHOUSE_CHECK_IN_OPERATE_TIME) {

					Timehtml += '<fieldset id="' + obj.AREA_ID + '">';
					Timehtml += '<legend class="td_legend" align="center"><span>WHSECheckIn:' + obj.TIME.WAREHOUSE_CHECK_IN_OPERATE_TIME + '</span></legend>';
					Timehtml += '<ul>';

					for (var keysWINDOWRESULT in obj.TIME.WINDOWRESULT) {
						var WINDOWRESULTitme = obj.TIME.WINDOWRESULT[keysWINDOWRESULT];
						for (var _LOADkey in WINDOWRESULTitme.WAREHOUSE_NOTICES_LOAD) {
							var NOTICES_LOADitme = WINDOWRESULTitme.WAREHOUSE_NOTICES_LOAD[_LOADkey];
							Timehtml += '<li id="' + NOTICES_LOADitme.ZONE_ID + '"><span>Notified:' + NOTICES_LOADitme.EMPLOYE_NAME + '</span><span class="detail">' + NOTICES_LOADitme.DETAIL.VALUE + '</span></li>';
						}
					}
					Timehtml += '</ul>';
					Timehtml += '</fieldset>';
				}


				Timehtml += '</div>';


				return Timehtml;
			}
		},
		CHECKINLOG: {
			tag: "div",
			class: "Log_container",
			html: function(obj, index) {

				var ulsring = "";
				var arryjson = obj.CHECKINLOG.LOG;
				var box1px = "<div class='box1px'></div>";
				for (var _key = 0; _key < arryjson.length; _key++) {

					var arryitmes = arryjson[_key];
					var ul = '<ul><li><span>' + arryitmes.EMPLOYE_NAME + '</span><span class="valuestyle">' + arryitmes.LOG_TYPE.VALUE + '</span></li><li><span>' + arryitmes.NOTE + '</span></li><li><span>' + arryitmes.OPERATOR_TIME + '</span></li>';

					ul += '</ul>';

					if (ulsring != "" && typeof ulsring != "undefined") {
						ulsring = ulsring + box1px + ul;
					} else {
						ulsring = ul;
					}


					if (_key == 2) {
						break;
					}

				}

				// ulsring;
				if (obj.CHECKINLOG.MORE) {

					ulsring += '<label>';
					ulsring += '<span class="Morestyle" id="more_' + obj.CHECKINLOG.MORE.PARA + '">' + obj.CHECKINLOG.MORE.VALUE + '</span>';
					ulsring += '</label>';

				}

				return ulsring;

			}

		},
		Options: {
			tag: "ul",
			html: function(obj, index) {

				var ullihtml = "";
				if (typeof obj == "object") {
					for (var key in obj.Options) {
						var btusring = obj.Options[key];
						ullihtml += '<li><button class="' + btusring[0] + '" data-eventname="' + btusring[1] + '">' + btusring[0] + '</button></li>';
					}
				}

				return ullihtml;

			}

		},
		updatatr:function(jsondata,trid){
             
             var tritmeobj=$(trid);

             //console.log(jsondata);

             //console.log(tritmeobj);
             var tritmedata=[];
             tritmedata.push(jsondata);

             var uptrdata=_tbody.TotrJson(tritmedata,1);

             console.log(uptrdata[0].children);
             
           // console.log(JSONTOHTML.transform(uptrdata[0].children, _tbody.td));

             tritmeobj.empty().html(JSONTOHTML.transform(uptrdata[0].children, _tbody.td));


		}
	}

	return {
		tbody: _tbody,
		table: _table
	}

});