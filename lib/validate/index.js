"use strict";

require([
     "../../requirejs_config", 
], function(){
    require(["jquery", "oso.lib/validate/js/validate"], function($, validate){

    	$(function(){
            
    		$('#b1').click(function(){
    			validate.isValidated('#form');
    			
    		});
            $('#b2').click(function(){
                validate.isValidated('#form', true);
                
            });

            $('#btn2').click(function(){
                validate.setCallback(function(elem){
                    $(elem).css({
                        'border':'1px solid blue',
                        'box-shadow':'none'
                    });
                
                },function(elem,msg){
                    $(elem).css({
                        'border':'1px solid red',
                        'box-shadow': 'inset 0 1px 0 red,0 1px 2px red'
                    });
                    return true;
                });
            });

            $('#btn3').click(function(){
                validate.setCallback(function(elem){
                    $(elem).css({
                        'border':'1px solid blue',
                        'box-shadow':'none'
                    });
                
                },function(elem,msg){
                    $(elem).css({
                        'border':'1px solid red',
                        'box-shadow': 'inset 0 1px 0 red,0 1px 2px red'
                    });
                });
            });
    	})
    
    });

});
