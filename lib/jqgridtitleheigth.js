/**
 * 
 * @authors gaowenzhen (gaowenzhen@msn.com)
 * @date    2014-08-21 14:14:17
 * @version 0.01
 */

(function($) { 

 	tabletitleauto={

        settitle:function(tableconfit){
         if(typeof tableconfit == "object"){
         var domid=tableconfit.domid;
         var height=tableconfit.height;
         var tabledom=$(domid);  
         var styleidstyle=domid+"_style";
         var stylelen=$("#"+styleidstyle+"").length;
         
             var fontsize=tableconfit.fontsize;
             var styletxt=domid+" table tr th{height:"+height+"px}";
             styletxt+=domid+" table tr th .ui-jqgrid-sortable{line-height:"+height+"px;height:"+height+"px;font-size:"+fontsize+"px}";
             var spanh=parseInt(height) / 2;
             styletxt+=domid+" table tr th .ui-jqgrid-sortable em{font-style:normal;vertical-align: middle;display:block;line-height:"+spanh+"px;height:"+spanh+"px}";

             if(stylelen < 1){
             $('<style id="'+styleidstyle+'" type="text/css">'+styletxt+'</style>').appendTo(tabledom);
             }


        }

    }


 	};


 	window.tabletitleauto=tabletitleauto||{}; 

 	//用法

  //   $(function(){ 		
 	// 	tabletitleauto.settitle({domid:"#gbox_gridtest",height:35,fontsize:14});
 	// });

 })(jQuery);