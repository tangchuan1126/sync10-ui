"use strict";

(function() {

    var defval = ["jquery", "require_css!bootstrap-css/bootstrap.min.css", "require_css!oso.lib/slidePanel/css/style.css", "require_css!oso.lib/slidePanel/css/style2.css", "require_css!oso.lib/animate.css"];

    if (/msie/.test(navigator.userAgent.toLowerCase())) {
        defval=[];
        defval["jquery1.9"];
    }

    define(defval, function($) {

        var createPanel = function(options) {
            options = $.extend({
                content: '',
                title: '',
                id: "",
                type: "",
                container: 'body',
                closePos: ".bol-title",
                close: function() {
                    return true;
                }
            }, options);

            var container = $(options.container),
                panel, timer;

            function init() {
                panel = $(options.content).appendTo(container).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
                    panel.removeClass("fadeInLeft")
                });

                panel.find(options.closePos).append($('<div class="bol-close"><i class="glyphicon glyphicon-remove"></i></div>').click(function() {
                    if (!options.close()) {
                        return false;
                    }
                    panel.addClass("fadeOutRight").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
                        panel.remove();
                    });
                    return false;
                }));

                return panel;
            }

            return init();
        };

        return createPanel;

    });
})(this);
