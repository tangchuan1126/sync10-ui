"use strict";

(function(){
    define(["jquery"], function ($) {

		var SlidePanel = function(options){
			options = $.extend({
				id:'',
				content : '',
				url : '',
				title : '',	
				slideLine : '50%',
				duration : 400,
				topLine: 0,
				dynamicParam: "",
				zindex:"9999",
			}, options);

			var container, content, self = this, win = $(window);

           var el=$(".slide-wrap,.slide-modal");
			if(el.length > 0){
              el.remove();
			}

			function createHtml(){

             var ids="";
				if(options.id){
                 ids=' id="'+options.id+'"';
				}

				container = $('<div class="slide-wrap"><div'+ids+' class="slide-container"></div></div').css({
					width : win.width(),
					height : win.height(),
					"z-index":options.zindex
				}).on('click', function(){

					self.close();
				}).insertBefore(
					$('<div class="slide-modal"></div>').css({
						width : win.width(),
						height : win.height(),
						"z-index":window.parseInt(options.zindex) - 1
					}).appendTo('body')
				).find('.slide-container').on('click', function(e){
				 	e.stopPropagation();
					return false;
				}).css({
					height : win.height()- options.topLine,
					top:options.topLine,
					width:options.slideLine,
					left : win.width()
				});

				var titles="";

				if(options.title){
                 titles=options.title
				}

				if(options.url){
					container.append('<div class="slide-header-close-box"><span class="slide-header-title">'+titles+'</span><span class="slide-header-close"></span></div>').append('<div class="slide-content"></div>');
					content = container.css('padding', 0).find('.slide-content').css({
						height : win.height()- options.topLine
					}).append('<iframe src="' + options.url + '" frameborder="0"></iframe>');

				}else{
					container.append('<div class="slide-header"><h4 class="slide-header-title">' + options.title + '</h4><div class="slide-header-close"></div></div>')
						.append('<div class="slide-content"></div>');
					content = container.find('.slide-content').css({
						height : win.height()- options.topLine - 80
					}).append(options.content);
				}

				container.on('click', '.slide-header-close', function(){
					self.close();
				});

				resize();
			}

			function resize(){
				win.resize(function(){
					container.css({
						height : win.height() - options.topLine
					}).parent().css({
						width : win.width(),
						height : win.height()
					}).next().css({
						width : win.width(),
						height : win.height()
					});

					if(options.url){
						content.css({
							height : win.height() - options.topLine
						});
					}else{
						content.css({
							height : win.height() - 80 - options.topLine
						});
					}

					if(container.parent().css('display') !== 'none'){
						container.css({
							left : win.width() - container.outerWidth()
						});
					}else{
						container.css({
							left : win.width()
						});
					}
				});
			}

			self.open = function(fn){
				container.parent().show(function(){
					container.stop().animate({
						left : win.width() - container.outerWidth()
					}, options.duration, fn);
				}).next().show();
				
			}

			self.close = function(fn){
				var closebefore=true;
				if(options.closebefore){

					var b=options.closebefore();
					if(typeof b == "boolean"){
                      
                      closebefore=b;                     
					}

				}

				if(fn && !fn() || !closebefore){
					return;
				}
				container.stop().animate({
					left : win.width()
				}, options.duration, function(){
					container.parent().hide().next().hide();

				if(options.ShutdownCallback){
				var closeifrme=$(".slide-header-close");

                var addset=closeifrme.attr("pifrmereset");
                if(addset=="true"){
                   addset=true;
                }

             	options.ShutdownCallback(addset);

               }

				});
			}

			createHtml();            

		};

		return SlidePanel;
		
	});
})(this);