/*!
 * drag.js
 * Date: 2013-12-06
 * https://github.com/aui/artDialog
 * (c) 2009-2014 TangBin, http://www.planeArt.cn
 *
 * This is licensed under the GNU LGPL, version 2.1 or later.
 * For details, see: http://www.gnu.org/licenses/lgpl-2.1.html
 */
define(function (require) {

var $ = require('jquery');


var _dragEvent, _use,
    _$window = $(window),
    _$document = $(document),
    _elem = document.documentElement,
    _isIE6 = !('minWidth' in _elem.style),
    _isLosecapture = 'onlosecapture' in _elem,
    _isSetCapture = 'setCapture' in _elem;

var DragEvent = function () {

   var that = this,
        proxy = function (name) {
            var fn = that[name];
            that[name] = function () {
                return fn.apply(that, arguments);
            };
        };
        
    proxy('start');
    proxy('move');
    proxy('end');

};

DragEvent.prototype ={

    // 开始拖拽
    onstart: $.noop,
    start: function (event) {
        _$document
        .bind('mousemove', this.move)
        .bind('mouseup', this.end);
            
        this._sClientX = event.clientX;
        this._sClientY = event.clientY;
        this.onstart(event.clientX, event.clientY);

        return false;
    },
    
    // 正在拖拽
    onmove: $.noop,
    move: function (event) {        
        this._mClientX = event.clientX;
        this._mClientY = event.clientY;
        this.onmove(
            event.clientX - this._sClientX,
            event.clientY - this._sClientY
        );
        
        return false;
    },
    
    // 结束拖拽
    onend: $.noop,
    end: function (event) {
        _$document
        .unbind('mousemove', this.move)
        .unbind('mouseup', this.end);
        
        this.onend(event.clientX, event.clientY);
        return false;
    }
    
};

_use = function (event) {
    var limit, startWidth, startHeight, startLeft, startTop, isResize,
        api = artDialog.focus,
        //config = api.config,
        DOM = api.DOM,
        wrap = DOM.wrap,
        title = DOM.title,
        main = DOM.main;

    // 清除文本选择
    var clsSelect = 'getSelection' in window ? function () {
        window.getSelection().removeAllRanges();
    } : function () {
        try {
            document.selection.empty();
        } catch (e) {};
    };
    
    // 对话框准备拖动
    _dragEvent.onstart = function (x, y) {
        if (isResize) {
            startWidth = main[0].offsetWidth;
            startHeight = main[0].offsetHeight;
        } else {
            startLeft = wrap[0].offsetLeft;
            startTop = wrap[0].offsetTop;
        };
        
        _$document.bind('dblclick', _dragEvent.end);
        !_isIE6 && _isLosecapture ?
            title.bind('losecapture', _dragEvent.end) :
            _$window.bind('blur', _dragEvent.end);
        _isSetCapture && title[0].setCapture();
        
        wrap.addClass('aui_state_drag');
        api.focus();
    };
    
    // 对话框拖动进行中
    _dragEvent.onmove = function (x, y) {
        if (isResize) {
            var wrapStyle = wrap[0].style,
                style = main[0].style,
                width = x + startWidth,
                height = y + startHeight;
            
            wrapStyle.width = 'auto';
            style.width = Math.max(0, width) + 'px';
            wrapStyle.width = wrap[0].offsetWidth + 'px';
            
            style.height = Math.max(0, height) + 'px';
            
        } else {
            var style = wrap[0].style,
                left = Math.max(limit.minX, Math.min(limit.maxX, x + startLeft)),
                top = Math.max(limit.minY, Math.min(limit.maxY, y + startTop));

            style.left = left  + 'px';
            style.top = top + 'px';
        };
            
        clsSelect();
        api._ie6SelectFix();
    };
    
    // 对话框拖动结束
    _dragEvent.onend = function (x, y) {
        _$document.unbind('dblclick', _dragEvent.end);
        !_isIE6 && _isLosecapture ?
            title.unbind('losecapture', _dragEvent.end) :
            _$window.unbind('blur', _dragEvent.end);
        _isSetCapture && title[0].releaseCapture();
        
        _isIE6 && !api.closed && api._autoPositionType();
        
        wrap.removeClass('aui_state_drag');
    };
    
    isResize = event.target === DOM.se[0] ? true : false;
    limit = (function () {
        var maxX, maxY,
            wrap = api.DOM.wrap[0],
            fixed = wrap.style.position === 'fixed',
            ow = wrap.offsetWidth,
            oh = wrap.offsetHeight,
            ww = _$window.width(),
            wh = _$window.height(),
            dl = fixed ? 0 : _$document.scrollLeft(),
            dt = fixed ? 0 : _$document.scrollTop(),
            
        // 坐标最大值限制
        maxX = ww - ow + dl;
        maxY = wh - oh + dt;
        
        return {
            minX: dl,
            minY: dt,
            maxX: maxX,
            maxY: maxY
        };
    })();
    
    _dragEvent.start(event);
};


/**
 * 启动拖拽
 * @param   {HTMLElement}   被拖拽的元素
 * @param   {Event} 触发拖拽的事件对象。可选，若无则监听 elem 的按下事件启动
 */
DragEvent.create = function (elem, event) {
    var $elem = $(elem);
    var dragEvent = new DragEvent();
    var startType = DragEvent.types.start;
    var noop = function () {};
    var className = elem.className
        .replace(/^\s|\s.*/g, '') + '-drag-start';

    var minX;
    var minY;
    var maxX;
    var maxY;

    var api = {
        onstart: noop,
        onover: noop,
        onend: noop,
        off: function () {
            $elem.off(startType, dragEvent.start);
        }
    };


    dragEvent.onstart = function (event) {
        var isFixed = $elem.css('position') === 'fixed';
        var dl = $document.scrollLeft();
        var dt = $document.scrollTop();
        var w = $elem.width();
        var h = $elem.height();

        minX = 0;
        minY = 0;
        maxX = isFixed ? $window.width() - w + minX : $document.width() - w;
        maxY = isFixed ? $window.height() - h + minY : $document.height() - h;

        var offset = $elem.offset();
        var left = this.startLeft = isFixed ? offset.left - dl : offset.left;
        var top = this.startTop = isFixed ? offset.top - dt  : offset.top;

        this.clientX = event.clientX;
        this.clientY = event.clientY;

        $elem.addClass(className);
        api.onstart.call(elem, event, left, top);
    };
    

    dragEvent.onover = function (event) {
        var left = event.clientX - this.clientX + this.startLeft;
        var top = event.clientY - this.clientY + this.startTop;
        var style = $elem[0].style;

        left = Math.max(minX, Math.min(maxX, left));
        top = Math.max(minY, Math.min(maxY, top));

        style.left = left + 'px';
        style.top = top + 'px';
        
        api.onover.call(elem, event, left, top);
    };
    
// 对话框拖动结束
    dragEvent.onend = function (event) {
        var position = $elem.position();
        var left = position.left;
        var top = position.top;
        $elem.removeClass(className);
        api.onend.call(elem, event, left, top);
    };


    dragEvent.off = function () {
        $elem.off(startType, dragEvent.start);
    };


    if (event) {
        dragEvent.start(event);
    } else {
        $elem.on(startType, dragEvent.start);
    }

    return api;
};

return DragEvent;

});