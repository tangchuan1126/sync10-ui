requirejs(['/Sync10-ui/requirejs_config.js'], function() {

  var Magnifierpath = "oso.lib/css3.0_demo/ExpandingOverlayEffect/";

  requirejs(["oso.lib/nprogress/nprogress.min", "jquery",'handlebars.runtime','../templates/templates.amd',"require_css!oso.lib/nprogress/nprogress", "require_css!bootstrap-css/bootstrap.min", "require_css!" + Magnifierpath + "css/Magnifier"],
    function(NProgress, $,Handlebars,templates) {
      requirejs(['oso.lib/css3.0_demo/ExpandingOverlayEffect/js/modernizr.custom', "oso.lib/css3.0_demo/ExpandingOverlayEffect/js/Magnifier.Lightweight", "oso.lib/jquery-file-upload-extension/upload-plugin",'handlebars',"bootstrap_tab",'handlebars.runtime'], function(Modernizr, Magnifier, uploadplugin,Handlebars) {

        var obj = {
          obj: {
            name: "小明"
          },
          obj2: {
            age: 24
          }
        };
        //不全屏
        //var demo = new Magnifier("index8.html", 600, 300);
        var demo = new Magnifier(templates.test(obj), 600, 300);
        //开启新窗口
        $("#openBtn").click(function() {

          demo.open();

        });

        //关闭当前窗口
        $("#closeBtn").click(function() {

          parent.window.demo.close();

        });

      });
      

    });


});