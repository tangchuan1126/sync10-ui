;
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register 
        define(['jquery', 'oso.lib/css3.0_demo/ExpandingOverlayEffect/js/modernizr.custom', 'art_Dialog/dialog-plus', 'jquery.easydrag'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'), require('modernizr.custom'), require('art_Dialog/dialog-plus')(root), require('jquery.easydrag'));

    } else {
        // Browser globals
        root.Magnifier = factory(root.$, root.Modernizr, root.art_Dialog);
    }
}(this, function($, Modernizr, art_Dialog) {
    'use strict';
    if (!Modernizr || typeof Modernizr == "undefined") {
        var Modernizr = window.Modernizr;
    }
    //run
    var Magnifier = function() {
        //是否全屏
        this.isZoomAuto = false;
        this.isNormal = true;
        this.isTemplate = false;
        if (arguments.length == 1) {
            this.isZoomAuto = true;
            this.source = arguments[0];
        } else if (arguments.length == 3) {
            this.source = arguments[0];
            this.width = arguments[1];
            this.height = arguments[2];
        } else {
            this.isNormal = false;
        }
        if (this.source.indexOf(".html") < 0) {
            this.isTemplate = true;
        } else {
            this.isTemplate = false;
        }
        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        };
        this.transEndEventName = transEndEventNames[Modernizr.prefixed('transition')];
        this.supportTransitions = Modernizr.csstransitions;
        this.container = {};
    };

    Magnifier.prototype = {
        open: function() {
            var _this = this;
            if (!_this.isNormal) {
                return;
            }
            var _source = _this.source;
            var dialogWidth = _this.isZoomAuto ? $(window).width() : _this.width;
            var dialogHeight = _this.isZoomAuto ? $(window).height() : _this.height;

            _this.modalDiv = $("<div style='position:absolute;left:0px;top:0px;width:100%;height:100%;background-color:rgba(255,255,255,0.5);z-index:999;'></div>");
            $("body").append(_this.modalDiv);
            this.container;
            if (_this.isTemplate) {
                this.container = $("<div><div style='width:100%;height:50px;background-color:rgb(63,139,203);'></div><span class='rb-close' style='color:white;'>close</span>" + _source + "</div>");
            } else {
                this.container = $("<div><div style='width:100%;height:50px;background-color:rgb(63,139,203);'></div><span class='rb-close' style='color:white;'>close</span><iframe src='" + _source + "' frameborder=0 width='100%' height='100%' style='padding-bottom:50px;'></iframe></div>");
            }
            _this.container = this.container;

            $("body").append(_this.container);

            var windowWidth = $(window).width();
            var windowHeight = $(window).height();

            var topOrBottom = (windowHeight - dialogHeight) / 2;
            var leftOrRight = (windowWidth - dialogWidth) / 2;

            var beforeCss = {
                //border:_this.isZoomAuto?"null":"1px solid rgb(50,80,108)",
                border: _this.isZoomAuto ? "null" : "1px solid rgb(190,190,190)",
                "-webkit-box-shadow": _this.isZoomAuto ? "null" : "0px 0px 6px rgb(150,150,150)",
                "-moz-box-shadow": _this.isZoomAuto ? "null" : "0px 0px 6px rgb(150,150,150)",
                "box-shadow": _this.isZoomAuto ? "null" : "0px 0px 6px rgb(150,150,150)",
                position: "fixed",
                left: dialogWidth < ($(window).width()) ? leftOrRight : 0 + "px",
                top: dialogHeight < ($(window).height()) ? topOrBottom : 0 + "px",
                bottom: _this.isZoomAuto ? "50px" : "0px",
                width: _this.isZoomAuto ? "100%" : dialogWidth + "px",
                height: _this.isZoomAuto ? "100%" : dialogHeight + "px",
                zIndex: -1,
                "background-color": "white",
                opacity: 0,
                "-webkit-transition": "all 0.4s ease",
                "-moz-transition": "all 0.4s ease",
                transition: "all 0.4s ease"
            };
            _this.container.css(beforeCss);
            var dragHelp = $("<div style='position:absolute;top:50px;left:0px;right:0px;bottom:0px;'></div>");
            if (!_this.isZoomAuto) {
                _this.container.easydrag();
                _this.container.ondrag(function() {
                    _this.container.css('-webkit-transition', "null");
                    _this.container.css('-moz-transition', "null");
                    _this.container.css('transition', "null");
                    _this.container.append(dragHelp);
                });
                _this.container.ondrop(function() {
                    _this.container.css('-webkit-transition', "all 0.4s ease");
                    _this.container.css('-moz-transition', "all 0.4s ease");
                    _this.container.css('transition', "all 0.4s ease");
                    dragHelp.remove();
                });
            }

            var clipPropFirst = 'rect(' + dialogHeight / 2 + 'px ' + dialogWidth / 2 + 'px ' + dialogHeight / 2 + 'px ' + dialogWidth / 2 + 'px)';
            var clipPropLast = 'rect(0px ' + (dialogWidth + 6) + 'px ' + (dialogHeight + 6) + 'px 0px)';
            //var clipPropLast = 'rect(0px ' + $(window).width() + 'px ' + $(window).height() + 'px 0px)';
            var clipPropLastForClose = "auto";
            var afterCss = {
                clip: clipPropFirst,
                zIndex: 1000,
                opacity: 1
            };

            _this.container.css(afterCss);
            if (_this.supportTransitions) {
                _this.container.on(_this.transEndEventName, function() {
                    _this.container.off(_this.transEndEventName);
                    _this.container.css('clip', clipPropLast).on(_this.transEndEventName, function() {
                        _this.container.off(_this.transEndEventName);
                    });
                });
            } else {
                _this.container.css('z-index', -1);
            }

            if (_this.isZoomAuto) {
                //窗体大小改变
                window.onresize = function() {
                    //var clipPropLast = 'rect(0px ' + dialogWidth + 'px ' + dialogHeight + 'px 0px)';
                    var clipPropLast = 'rect(0px ' + $(window).width() + 'px ' + $(window).height() + 'px 0px)';
                    _this.container.css('clip', clipPropLast);
                    //var r = document.body.offsetWidth / window.screen.availWidth;
                    //$(document.body).css("-webkit-transform","scale(" + r + ")");
                };
            }

            //关闭
            $("span.rb-close").click(function() {
                _this.close();
            });
        },
        close: function() {
            var _this = this;
            _this.modalDiv.remove();
            _this.container = this.container;
            var dialogWidth = _this.isZoomAuto ? $(window).width() : _this.width;
            var dialogHeight = _this.isZoomAuto ? $(window).height() : _this.height;
            var clipPropFirst = 'rect(' + dialogHeight / 2 + 'px ' + dialogWidth / 2 + 'px ' + dialogHeight / 2 + 'px ' + dialogWidth / 2 + 'px)';
            var clipPropLast = 'rect(0px ' + dialogWidth + 'px ' + dialogHeight + 'px 0px)';
            var clipPropLastForClose = "auto";
            _this.container.css({
                clip: clipPropFirst,
                pointerEvents: 'none'
            });
            if (_this.supportTransitions) {
                _this.container.on(_this.transEndEventName, function() {
                    _this.container.css('opacity', 0).on(_this.transEndEventName, function() {
                        _this.container.off(_this.transEndEventName).css({
                            clip: clipPropLastForClose,
                            zIndex: -1
                        });
                    });
                    _this.container.remove();
                });
            } else {
                _this.container.css('z-index', -1);
                _this.container.remove();
            }
        }
    };

    return Magnifier;

}));