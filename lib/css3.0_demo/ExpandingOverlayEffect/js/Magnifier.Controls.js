;
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register 
        define(['jquery', 'oso.lib/css3.0_demo/ExpandingOverlayEffect/js/modernizr.custom', 'art_Dialog/dialog-plus'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'), require('modernizr.custom'), require('art_Dialog/dialog-plus')(root));

    } else {
        // Browser globals
        root.Magnifier = factory(root.$, root.Modernizr, root.art_Dialog);
    }
}(this, function($, Modernizr, art_Dialog) {
    'use strict';



    if (!Modernizr || typeof Modernizr == "undefined") {

        var Modernizr = window.Modernizr;

    }




    //窗口改变大小时的处理
    var $event = $.event,
        $special,
        resizeTimeout;

    $special = $event.special.debouncedresize = {
        setup: function() {
            $(this).on("resize", $special.handler);
        },
        teardown: function() {
            $(this).off("resize", $special.handler);
        },
        handler: function(event, execAsap) {
            // Save the context
            var context = this,
                args = arguments,
                dispatch = function() {
                    // set correct event type
                    event.type = "debouncedresize";
                    $event.dispatch.apply(context, args);
                };

            if (resizeTimeout) {
                clearTimeout(resizeTimeout);
            }

            execAsap ?
                dispatch() :
                resizeTimeout = setTimeout(dispatch, $special.threshold);
        },
        threshold: 50
    };

    //run
    var Magnifier = function(options) {

        var _this = this;

        this.options = {};

        if (!options || typeof options == "undefined") {

            return false;
        }

        if (!options.rootbox || options.rootbox == "undefined")
            return false

        this.options = options;

        this.options._rootdom = $(options.rootbox);

        var rootbox = this.options._rootdom;

        if ($.isArray(options.CloseKey)) {
            this.options.CloseKey = options.CloseKey;
        }

        if (!options.CloseKey || typeof options.CloseKey == "undefined")
            this.options.CloseKey = "27";

        _this.options.Sourceall = [];

        //开启自动填充隐藏部分
        if (options.openAfter != "" && typeof options.openAfter != "undefined")
            _this.options.openAfter = options.openAfter;
        else
            _this.options.openAfter = true;


        //openid
        this.options.OpenId = "";

        if (options.itemskey)
            this.options.$items = rootbox.find(options.itemskey);
        else
            this.options.$items = rootbox.find(".grid_items");

        this.initple();

        this.options.$window = $(window);

        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        };

        // transition end event name
        this.options.transEndEventName = transEndEventNames[Modernizr.prefixed('transition')];
        this.options.$body = $('BODY');
        // transitions support
        this.options.supportTransitions = Modernizr.csstransitions;
        // current item's index
        this.options.current = -1;
        // window width and height
        this.options.winsize = this.getWindowSize();

        var isCloseKey = $.isArray(_this.options.CloseKey);

        if (this.options.CloseKey != "" || isCloseKey) {

            $(document).on("keydown", function(e) {


                var nodeName = e.target.nodeName;
                if (nodeName == "TEXTAREA" || nodeName == "INPUT") {
                    e.stopPropagation();                    
                }else{


                var keycode = (e.keyCode ? e.keyCode : e.which);
                var OpenId = _this.options.OpenId;
                var u_keycode = false;

                if ($.isArray(_this.options.CloseKey)) {
                    for (var keys in _this.options.CloseKey) {

                        var itmekeycode = _this.options.CloseKey[keys];
                        if (keycode == window.parseInt(itmekeycode)) {
                            u_keycode = true;
                            break;
                        }

                    }
                }

                if (typeof _this.options.CloseKey == "string") {

                    if (keycode == window.parseInt(_this.options.CloseKey)) {
                        u_keycode = true;
                    }

                }


                if (OpenId != "" && typeof OpenId != "undefined" && u_keycode) {
                    var rb_closebtn = $("#" + OpenId + " .rb-close");
                    if (rb_closebtn.length > 0)
                        rb_closebtn.click();
                }

                if (keycode == 8) {

                    e.preventDefault();

                    return false;

                }

            }

            })


        }

    };


    Magnifier.prototype = {
        initple: function() {

            var _this = this;
            var options = _this.options;

            var bgcolor = "";
            if (options.bgcolor != "" && typeof options.bgcolor != "undefined") {
                bgcolor = ' style="background-color:' + options.bgcolor + '"';
            }

            options.$items.each(function() {
                var $item = $(this);
                var idsring = $item.attr("id");
                var rb_overlay = $item.find(".rb-overlay");
                if (rb_overlay.length < 1) {

                    var weekhtml = "";
                    if (options.itmedata) {
                        if (options.itmedata.length > 0) {

                            var itmeobj = options.itmedata.filter(function(itmes) {
                                // console.dir(itmes);
                                if (itmes.itmeid == idsring) {
                                    return itmes;
                                }

                            });
                        }


                        if (itmeobj.length > 0) {
                            weekhtml = itmeobj[0].html;
                        }

                    }


                    $('<div class="rb-overlay"><span class="rb-close">close</span><div class="rb-week"' + bgcolor + '><div class="auto_y_scroll">' + weekhtml + '</div></div></div>').appendTo($item);

                }
            });


        },
        getrb_overlay: function(itmeid) {

            //获取格子
            var _rootdom = $(this.options.rootbox);
            var liobj = _rootdom.find(itmeid);

            var itme_rbweek = liobj.find(".rb-week");
            if (itme_rbweek.length > 0) {
                return itme_rbweek;
            } else {

                return null;
            }


        },
        AutoloadSource: function(Sourceboj) {
            //自动填充
            var _this = this;
            var options = _this.options;

            var rb_week = $("#" + Sourceboj.itmepid + " .rb-week");
            if (rb_week.length > 0) {
                rb_week.load(Sourceboj.Sourceurl);
            }

        },
        render: function() {


            var _this = this;
            var options = _this.options;

            //自动装载预备数据
            if (options.itmedata) {
                var dataitmes = options.itmedata;
                if (dataitmes.length > 0 && _this.options.openAfter) {

                    for (var keydata in options.itmedata) {
                        var itmesobj = options.itmedata[keydata];
                        if (itmesobj.itmeid != "" && typeof itmesobj.itmeid != "undefined" && itmesobj.Source != "" && typeof itmesobj.Source != "undefined") {

                            _this.options.Sourceall.push({
                                Sourceurl: itmesobj.Source,
                                itmepid: itmesobj.itmeid
                            });
                        }
                    }
                }
            };

            options.$items.each(function() {

                var $item = $(this),
                    $close = $item.find('span.rb-close'),
                    $overlay = $item.children('div.rb-overlay');



                var Source = $item.attr("Source");
                var p_itmeid = $item.attr("id");
                if (Source != "" && Source != "undefined") {
                    _this.options.Sourceall.push({
                        Sourceurl: Source,
                        itmepid: p_itmeid
                    });
                }




                $close.on('click', function() {

                    var li_this = $(this);
                    var pli_obj = "";
                    if (options.itemskey)
                        pli_obj = li_this.parents(options.itemskey);
                    else
                        pli_obj = li_this.parents(".grid_items");

                    if (typeof options.itmeclosebefore == "function") {

                        var pli_id = pli_obj.attr("id");

                        if (!options.itmeclosebefore(pli_id)) {

                            return false;

                        }

                    }


                    options.$body.css('overflow-y', 'auto');

                    var layoutProp = _this.getItemLayoutProp($item),
                        clipPropFirst = 'rect(' + layoutProp.top + 'px ' + (layoutProp.left + layoutProp.width) + 'px ' + (layoutProp.top + layoutProp.height) + 'px ' + layoutProp.left + 'px)',
                        clipPropLast = 'auto';

                    // reset current
                    options.current = -1;

                    $overlay.css({
                        clip: options.supportTransitions ? clipPropFirst : clipPropLast,
                        opacity: options.supportTransitions ? 1 : 0,
                        pointerEvents: 'none'
                    });

                    _this.options.OpenId = "";

                    if (options.supportTransitions) {
                        $overlay.on(options.transEndEventName, function() {

                            $overlay.off(options.transEndEventName);
                            setTimeout(function() {
                                $overlay.css('opacity', 0).on(options.transEndEventName, function() {
                                    $overlay.off(options.transEndEventName).css({
                                        clip: clipPropLast,
                                        zIndex: -1
                                    });
                                    $item.data('isExpanded', false);
                                });
                            }, 25);

                        });
                    } else {
                        $overlay.css('z-index', -1);
                        $item.data('isExpanded', false);
                    }

                    if (_this.options.refresh) {

                        pli_obj.find(".rb-week").empty();

                    }

                    return false;

                });

            });


            $(document).delegate("input:file,input:checkbox,input:radio", "click", function(e) {
                e.stopPropagation();
            });

            var _liobj = "";
            if (options.itemskey)
                _liobj = options.itemskey;
            else
                _liobj = ".grid_items";

            $(document).on('click', _liobj, function(e) {

                var $item = $(this);
                var $overlay = $item.children('div.rb-overlay');

                if ($overlay.css("opacity") == 1) {

                    console.log(3);

                    return false;
                }


                if ($item.data('isExpanded')) {
                    return false;
                }
                $item.data('isExpanded', true);
                // save current item's index
                options.current = $item.index();


                //点击后非自动填充时
                var _Source = $item.attr("Source");
                var _p_itmeid = $item.attr("id");
                var _rb_week = $item.find(".rb-week");
                var _rb_weekhtml = _rb_week.html();
                if (_Source != "" && _Source != "undefined" && _this.options.openAfter && (_rb_weekhtml == "" || typeof _rb_weekhtml == "undefined")) {
                    _this.AutoloadSource({
                        Sourceurl: _Source,
                        itmepid: _p_itmeid
                    });
                }


                //----------处理弹出特效

                var layoutProp = _this.getItemLayoutProp($item),
                    clipPropFirst = 'rect(' + layoutProp.top + 'px ' + (layoutProp.left + layoutProp.width) + 'px ' + (layoutProp.top + layoutProp.height) + 'px ' + layoutProp.left + 'px)',
                    clipPropLast = 'rect(0px ' + options.winsize.width + 'px ' + options.winsize.height + 'px 0px)';
               var z_index=9999;
                if(options.zindex && options.zindex > 0){
                    z_index=options.zindex;
                }

                $overlay.css({
                    clip: options.supportTransitions ? clipPropFirst : clipPropLast,
                    opacity: 1,
                    zIndex: z_index,
                    pointerEvents: 'auto'
                });

                _this.options.OpenId = $item.attr("id");

                if (options.supportTransitions) {
                    $overlay.on(options.transEndEventName, function() {

                        $overlay.off(options.transEndEventName);

                        setTimeout(function() {
                            $overlay.css('clip', clipPropLast).on(options.transEndEventName, function() {
                                $overlay.off(options.transEndEventName);
                                options.$body.css('overflow-y', 'hidden');
                            });
                        }, 25);

                    });
                } else {
                    options.$body.css('overflow-y', 'hidden');
                }

                if (options.itmeclick) {
                    var rb_week = $item.children('div.rb-week');
                    options.itmeclick(rb_week);
                }


            });


            $(window).on('debouncedresize', function() {
                options.winsize = _this.getWindowSize();
                // todo : cache the current item
                if (options.current !== -1) {
                    options.$items.eq(options.current).children('div.rb-overlay').css('clip', 'rect(0px ' + options.winsize.width + 'px ' + options.winsize.height + 'px 0px)');
                }
            });

            if (_this.options.Sourceall.length > 0 && _this.options.openAfter) {

                for (var sourcekey in _this.options.Sourceall) {
                    var itmesource = _this.options.Sourceall[sourcekey];
                    _this.AutoloadSource(itmesource);
                }

            }


        },
        tab_Plugin: function(clickthis) {
            //扩展tab

            if ($.fn.tab) {

                var _rb_overlay = clickthis.children('div.rb-overlay');
                var taball = _rb_overlay.find('[data-toggle="tab"]');

                if (taball.length) {
                    taball.each(function() {
                        var a_this = $(this);
                        var jqevents = $._data(this, "events");
                        if (!jqevents || typeof jqevents == "undefined") {

                            a_this.click(function(e) {
                                e.preventDefault()
                                $(this).tab('show');
                            });

                        }
                    });
                }

            }
        },
        getItemLayoutProp: function($item) {

            var _this = this;
            var options = _this.options;


            var scrollT = options.$window.scrollTop(),
                scrollL = options.$window.scrollLeft(),
                itemOffset = $item.offset();

            return {
                left: itemOffset.left - scrollL,
                top: itemOffset.top - scrollT,
                width: $item.outerWidth(),
                height: $item.outerHeight()
            };


        },
        getWindowSize: function() {

            var _this = this;
            var options = _this.options;

            options.$body.css('overflow-y', 'hidden');
            var w = options.$window.width(),
                h = options.$window.height();
            if (options.current === -1) {
                options.$body.css('overflow-y', 'auto');
            }
            return {
                width: w,
                height: h
            };
        }

    };


    return Magnifier;

}));
