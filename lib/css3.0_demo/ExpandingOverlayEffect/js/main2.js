requirejs(['/Sync10-ui/requirejs_config.js'], function() {

  var Magnifierpath = "oso.lib/css3.0_demo/ExpandingOverlayEffect/";

  requirejs(["oso.lib/nprogress/nprogress.min", "jquery","jquery.easydrag", "require_css!oso.lib/nprogress/nprogress", "require_css!bootstrap-css/bootstrap.min", "require_css!" + Magnifierpath + "css/Magnifier"],
    function(NProgress, $) {

      $("body").show();

      NProgress.start();

      requirejs(['oso.lib/css3.0_demo/ExpandingOverlayEffect/js/modernizr.custom', "oso.lib/css3.0_demo/ExpandingOverlayEffect/js/Magnifier.Lightweight", "oso.lib/jquery-file-upload-extension/upload-plugin", "bootstrap_tab"], function(Modernizr, Magnifier, uploadplugin) {

        //全屏

        //开启
        $("#btn").click(function() {
          //window.demo = new Magnifier("index7.html",1300,600);
          window.demo = new Magnifier("index7.html");
          demo.open();
        });

      });

      NProgress.done();

    });

});