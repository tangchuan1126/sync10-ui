define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<span style=\"color:red;\">我是第二个弹出来的,我不是html页面，我是handlebars模板</span>\r\n</br>\r\n我叫"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.obj : depth0)) != null ? stack1.name : stack1), depth0))
    + "，今年"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.obj2 : depth0)) != null ? stack1.age : stack1), depth0))
    + "岁";
},"useData":true});
templates['test2'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<span style=\"color:blue;\">我叫"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.obj : depth0)) != null ? stack1.name : stack1), depth0))
    + "，今年"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.obj2 : depth0)) != null ? stack1.age : stack1), depth0))
    + "岁</span>";
},"useData":true});
return templates;
});