 requirejs(['/Sync10-ui/requirejs_config.js'], function() {

      requirejs.config({
        baseUrl: './',
        paths: {
            abc: 'abc',//注意abc.js里又引用了jquery和edf.js
            edf:'edf'//edf.js在这里配上的，并不在requirejs_config.js内
        },
        shim: {
            'template': {
                deps: ['abc']
            }
        }
    });

//如果你不想把你工程下的包都写到requirejs_config.js里可以考虑用这种方式

     requirejs(['jquery','abc'],
         function($,abc) {

            console.log(abc);


            });


 });
