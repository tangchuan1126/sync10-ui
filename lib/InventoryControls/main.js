require(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/nprogress/nprogress.js','/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function(config, NProgress) {

	require(["require_css!bootstrap-css/bootstrap.min", "require_css!oso.lib/nprogress/nprogress.css"], function() {

		document.body.style.display = "block";

		NProgress.start();

		var cssfileall = [
			"oso.lib/CodeMirror/mode/javascript/javascript",
			"oso.lib/CodeMirror/lib/util/foldcode",
			"require_css!oso.lib/CodeMirror/lib/codemirror.css",
			"require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css",
			"bootstrap"
		];

		// require.config({
  //           paths: {
  //               "worker": '/Sync10-ui/bower_components/requirejs-web-workers/src/worker',
  //               "worker-fake":'/Sync10-ui/bower_components/requirejs-web-workers/src/worker-fake'
  //           }
  //       });


		require(cssfileall, function() {
			

            // var _worker = new Worker('/Sync10-ui/lib/InventoryControls/Inventory.js');

            // console.dir(_worker);

			require(["jquery", "oso.lib/InventoryControls/Inventory", "art_Dialog/dialog-plus"], function($, Inventory, dialog) {


				$(function() {


					var invdata = {
						renderTo: ".invdatabox", //容器
						dataUrl: "d.json", //数据或[{},{}...]
						postData: {
							a: "123",
							b: "345"
						}, //往服务端传数据
						searchfieldkey: "", //被匹配字段名
						scrollH: 300, //高度，出滚动条
						itmepanelw: 300, //宽度
						inventorytitle: "select All Areas",
						placeholder: "条件过滤2",
						filterbuttonstxt: "Assign User1",
						title_in_input: true
							//configurl:"config.json"//数据或{"buttons":[{},{}...]}
					};

					

					var Inventoryliste = new Inventory(invdata);


					//查找按钮
					Inventoryliste.on("events.filter", function(jsondata) {

						//console.log(jsondata);

					});

					//监听底部按钮
					Inventoryliste.on("events.footbutton", function(e) {

						//console.log(e);

					});

					Inventoryliste.on("events.all-areas-checkbox", function(e) {

						//console.log(e);

					});


					Inventoryliste.on("events.Error", function(e) {

						//alert(e);

						//console.log(e);

					});

					//Loading is completed--初始化完成后执行
					Inventoryliste.on("Initialize", function() {

						var brotab = $("#tab_content");
						brotab.show();

						require(["oso.lib/CodeMirror/demo/CodeMirror_demo"], function() {

							if (brotab.length > 0) {
								brotab.find("div[role='tabpanel']").addClass('tab-pane');
							}

							$("#run").click();

						});



						NProgress.done();

					});

					Inventoryliste.on("events.titleallclick", function(jsondata) {

						// jsondata.checkliset --The selected checkbox object (jq)

						//jsondata.dataitme --data itme (LOCATIONS all)

						console.log(jsondata);


					});

					Inventoryliste.on("events.itmeclick", function(jsondata) {

						// jsondata.checkliset --The selected checkbox object (jq)

						//jsondata.dataitme --datasub LOCATIONS  itme (LOCATIONS all)

						//{checkliset:chekedlen,dataitme:subitmedata,selected:{thischeck:chkeckthis,thisid:boxid}}
						//selected --selected yourself
						//selected.thischeck--jq object
						//selected.thisid--selected checkbox id
						console.log(jsondata);


					});


					Inventoryliste.render();



				});
				// window end

			});
			// inventory end

		});
		// codemirror end

	});
	//bootstrap css end


});
//requirejs_config