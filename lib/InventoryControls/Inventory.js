/*
 * Inventory.js
 * Copyright 2014, Connor
 * http://127.0.0.1/Sync10-ui/ilb/InventoryControls/
 */

;
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {

        // AMD. Register as an anonymous module.
        define(['jquery', 'underscore', 'mCustomScrollbar'], factory);

    } else if (typeof exports === 'object') {

        // Node. Does not work with strict CommonJS

        module.exports = factory(require('jquery'), require('underscore'), require('mCustomScrollbar'));

    } else {
        // Browser globals (root is window)
        root.Inventory = factory(root.jquery, root.underscore, root.mCustomScrollbar);
    }
}(this, function($, underscore, mCustomScrollbar) {
    'use strict';

    function InventoryPlugin(opts) {

        if (!(this instanceof InventoryPlugin))
            return new InventoryPlugin(opts);

        if (typeof opts === 'string')
            opts = {
                renderTo: opts
            };

        if (!opts.renderTo)
            return this.error('A renderTo needs to be specified');

        this.opts = opts;
        this._Dom = null;
        this._Int(this.opts);
        this.id = false;

    }


    InventoryPlugin.prototype._Int = function(opts) {

        var _this = this;
        var _opts = _this.opts;
        var dom = $(_opts.renderTo);
        _this.opts._Dom = dom;
        _this.opts = _.extend(_this.opts, opts);
        dom.addClass('inventoryhide');

    }


    InventoryPlugin.prototype.on = function(events, callback) {

        Componbox._events(events, callback, this);

    };

    InventoryPlugin.prototype.render = function() {

        var _this = this;

        var _opts = _this.opts;

        if (_opts._Dom.length > 0) {

            //_this.opts._Dom = dom;
            Componbox.createcss(_opts);
            Componbox.createboxhtml(_opts);

        } else {

            var errorobj = {
                txt: "没有容器",
                error: ""
            };
            Componbox.error(_opts, errorobj);

        }


        //_this.opts._Dom.fadeIn("slow");

    }


    var Componbox = {
        ConfingDATA: {},
        JSONDATA: {},
        SearchvalDate: {},
        //提供的事件
        _events: function(functionname, callback, _this) {

            switch (functionname) {

                case "events.Error":
                    _this.opts.error = callback;
                    break;
                case "events.lableclick":
                    _this.opts.treeclick = callback;
                    break;
                case "Initialize":
                    _this.opts.Initialize = callback;
                    break;

                case "events.filter":
                    _this.opts.filter = callback;
                    break;

                case "events.footbutton":
                    _this.opts.footbutton = callback;
                    break;

                case "events.titleallclick":
                    _this.opts.titleallclick = callback;
                    break;

                case "events.itmeclick":
                    _this.opts.itmeclick = callback;
                    break;

                case "events.all-areas-checkbox":
                    _this.opts.all_areas_checkbox = callback;
                    break;

                case "events.on-render-complete":
                    _this.opts.on_render_complete = callback;
                    break;

            }

        },
        error: function(opts, errorobj) {

            if (opts.error) {
                opts.error(errorobj);
            } else {

                if (window.console)
                    window.console.log(errorobj.txt);
                //alert(errorobj.txt);
            }

            return false;

        },
        createitme: function(_opts, data) {
            //var _opts=_this.opts;
            var Inventory_body = _opts._Dom.find('.Inventory_body').eq(0);
            Inventory_body.empty();
            var csstxt = "";
            var dataname = _opts.jsondataid;
            if (_opts.itmepanelw) {
                csstxt = " style='width:" + _opts.itmepanelw + "px'";
            }
            var rowhtml = "",
                p_boxre = "";
            if (_opts.renderTo.indexOf(".") > -1 || _opts.renderTo.indexOf("#") > -1)
                p_boxre = _opts.renderTo.replace(/^\.|^#/, "") + "_";



            for (var key in data) {

                var itmedata = data[key];
                //子项
                var LOCATIONS = itmedata.LOCATIONS;

                //标题头
                var _title = itmedata.AREA_NAME; //itmedata.TITLE + itmedata.AREA_NAME;
                var _AREA_ID = itmedata.AREA_ID;


                rowhtml += "<div" + csstxt + " class='col-md-2 subitmebox' id='" + p_boxre + "title_" + itmedata.AREA_ID + "'>" + Componbox.row_colmd(LOCATIONS, dataname, _title, _AREA_ID, p_boxre) + "</div>";
            }

            $('<div class="row">' + rowhtml + '</div>').appendTo(Inventory_body);

            if (_opts.Initialize) {
                _opts.Initialize();
            }

            Componbox.subevntes(_opts);
            Componbox.topevntes(_opts);

        },
        subevntes: function(_opts) {

            var scrollH = _opts.scrollH;
            var panel_default = _opts._Dom.find('.panel-default');

            var p_boxre = "";
            if (_opts.renderTo.indexOf(".") > -1 || _opts.renderTo.indexOf("#") > -1)
                p_boxre = _opts.renderTo.replace(/^\.|^#/, "") + "_";

            if (scrollH) {

                for (var i = 0; i < panel_default.length; i++) {

                    var list_unstyled = panel_default.find(".itmeui_box").eq(i);
                    // _ztreebox.height(_def.scrollH);
                    list_unstyled.css("max-height", scrollH + "px").mCustomScrollbar({
                        axis: "y",
                        scrollInertia: scrollH,
                        scrollbarPosition: "outside"
                    });

                    list_unstyled.find(".mCSB_scrollTools").css("right", "-18px");

                }

            }

            //头部选择

            var checkboxall = panel_default.find('.checkboxall');

            var RegExp__ = new RegExp(p_boxre + "title_", "g");
      //panl--top checkbox
            checkboxall.click(function() {

                var itme_this = $(this);
                var panel_default = itme_this.parents(".panel-default").eq(0);
                var subitmebox = itme_this.parents(".subitmebox").eq(0);



                var boxid = subitmebox.attr("id").replace(RegExp__, "");
                var checklist_unstyled = panel_default.find('.list-unstyled').eq(0);
                var allcheck = itme_this.find(':checkbox').eq(0);
                var allset = allcheck.prop("checked");

                 //筛选出每个显示的li
                var panl_itmeli=checklist_unstyled.find("li:visible");
                var chekboxalls_len=0,addchekt_len=0;
                var __checkbox=new $.fn.init(),__checked=new $.fn.init();
                for(var li_key=0;li_key < panl_itmeli.length;li_key++){
                   var itmeli_checkbox=panl_itmeli.eq(li_key);

                 __checkbox.push(itmeli_checkbox.find(':checkbox')[0]);                
                 __checked.push(itmeli_checkbox.find(':checked')[0]);

                }

                //console.dir(__checkbox);

                __checkbox.prop("checked", allset);

                //li-所有的checkbox
               // var chekboxalls = checklist_unstyled.find(':checkbox');

            

              //  chekboxalls.prop("checked", allset);
              //li--选中的
              //  var addchekt = checklist_unstyled.find(':checked');


                if (_opts.titleallclick) {

                    if (__checked.length > 0) {

                        var itmedata = Componbox.JSONDATA[_opts.jsondataid];
                        var itmedataall = {};
                        for (var itkey in itmedata) {
                            var listdata = itmedata[itkey];
                            if (listdata.AREA_ID == boxid) {
                                itmedataall = listdata;
                                break;
                            }

                        }

                    }

                    _opts.titleallclick({
                        checkliset: __checked,
                        dataitme: itmedataall
                    });

                }

                // chekboxalls.trigger("change");
                if (allset) {
                    panel_default.addClass('focusedInput');
                } else {
                    panel_default.removeClass('focusedInput');
                }

                //title select
                Componbox.settopchekce(_opts);


            });

            //主体选择      
            var subcheckboxall = _opts._Dom.find('.list-unstyled :checkbox');

            for (var ulchek = 0; ulchek < subcheckboxall.length; ulchek++) {

                var itmesubcheck = subcheckboxall.eq(ulchek);
                //每个子集
                itmesubcheck.click(function() {


                    var chkeckthis = $(this);
                    var chkeids = chkeckthis.data("checkboxid");

                    var p_ul = chkeckthis.parents(".list-unstyled").eq(0);

                    

                   // var chekedlen = p_ul.find(':checked');
                    //var checkboxall = p_ul.find(':checkbox');

                     var chekedlen = new $.fn.init();
                    var checkboxall =new $.fn.init();

                    var p_ul_li_visible=p_ul.find("li:visible");
                    for(var sub_li_key=0;sub_li_key< p_ul_li_visible.length;sub_li_key++){

                        var sub_checked=p_ul_li_visible.eq(sub_li_key).find(':checked');
                        var sub_checkbox=p_ul_li_visible.eq(sub_li_key).find(':checkbox');
                        if(sub_checked.length > 0)
                       chekedlen.push(sub_checked[0]);
                      if(sub_checkbox.length > 0)
                       checkboxall.push(sub_checkbox[0]);
                    }


                    var p_panel_default = p_ul.parents(".panel-default").eq(0);

                    //indeterminate 
                    var subitmebox = p_ul.parents(".subitmebox").eq(0);

                    var _titlecheckbox = subitmebox.find('.checkboxall :checkbox');

                    if (chekedlen.length < checkboxall.length && chekedlen.length > 0) {
                        _titlecheckbox.prop("indeterminate", true);
                    };

                    if (chekedlen.length == checkboxall.length && chekedlen.length > 0) {

                        _titlecheckbox.prop("indeterminate", false);
                        _titlecheckbox.prop("checked", true);

                    };

                //没有选中的
                    if (chekedlen.length < 1) {

                        _titlecheckbox.prop("indeterminate", false);

                         _titlecheckbox.prop("checked", false);

                         //return false

                    }


                    var boxid = subitmebox.attr("id").replace(RegExp__, "");

                    var itmedata = Componbox.JSONDATA[_opts.jsondataid];

                    var p_dataitmedata = {};

                    for (var itkey in itmedata) {
                        var listdata = itmedata[itkey];
                        if (listdata.AREA_ID == boxid) {

                            p_dataitmedata = listdata;

                            break;
                        }

                    }

                    var subitmedata = [];
                    for (var ojbi = 0; ojbi < chekedlen.length; ojbi++) {
                        var checkids = chekedlen.eq(ojbi);
                        var idsirngs = checkids.data("checkboxid");
                        for (var subkey in p_dataitmedata.LOCATIONS) {

                            var subitmedatas = p_dataitmedata.LOCATIONS[subkey];
                            if (subitmedatas.SLC_ID == idsirngs) {

                                subitmedata.push(subitmedatas);

                                break;

                            }


                        }

                    }


                    if (_opts.itmeclick) {

                        //返回所以选中的，和当前自己

                        var _select = {};

                        if (chkeckthis.prop("checked")) {

                            _select = {
                                thischeck: chkeckthis,
                                thisid: chkeids
                            }

                        }

                        _opts.itmeclick({
                            checkliset: chekedlen,
                            dataitme: subitmedata,
                            selected: _select
                        });

                    }



                    if (chekedlen.length > 0) {

                        p_panel_default.addClass('focusedInput');
                    } else {
                        p_panel_default.removeClass('focusedInput');
                    }


                    //title select
                    Componbox.settopchekce(_opts);


                });


            }



            var _buttonall = panel_default.find('.Inventory_foot button');
            if (_buttonall.length > 0) {

                _buttonall.click(function() {

                    var buttondata = {};
                    var but_this = $(this);
                    var spantxt = but_this.next("span").eq(0);
                    //var p_litxt=but_this.parents("li").eq(0);
                    buttondata.id = but_this.attr("id");
                    buttondata.button = but_this;
                    buttondata.checkboxdata = [];
                    var panel_default = but_this.parents(".panel-default").eq(0);
                    buttondata.p_boxid = panel_default.attr("id");
                    var list_unstyled = panel_default.find('.list-unstyled');
                    var checkdeall = list_unstyled.find('input:checked');

                    for (var checki = 0; checki < checkdeall.length; checki++) {
                        var itmecheckobj = checkdeall.eq(checki);
                        var _idsrings = itmecheckobj.data('checkboxid');
                        buttondata.checkboxdata.push({
                            id: _idsrings,
                            label: spantxt.text()
                        });
                    }


                    if (_opts.footbutton) {

                        _opts.footbutton(buttondata);

                    }


                });


            }



        },
        settopchekce: function(_opts) {


            var _all_Inventory_body = _opts._Dom.find('.Inventory_body');
            var panelboxs = _all_Inventory_body.find(".subitmebox:not(:hidden)");

            var p_sub_checkboxall_len = 0,
                chekdslen_len = 0;

             var subcheckboxall_box=[];

            for (var panelekey = 0; panelekey < panelboxs.length; panelekey++) {

                var chekcboxitme = panelboxs.eq(panelekey);
                var visibleboxall = chekcboxitme.find(".checkboxall:not(:hidden) :checkbox");
                p_sub_checkboxall_len = p_sub_checkboxall_len + visibleboxall.length;
                var checkeds_all = chekcboxitme.find(".checkboxall:not(:hidden) :checked");
                chekdslen_len = chekdslen_len + checkeds_all.length;

                var subcheckboxall_li=chekcboxitme.find(".list-unstyled li:not(:hidden)");
                subcheckboxall_box.push(subcheckboxall_li);

            }

            //var p_sub_checkboxall = _all_Inventory_body.find(".checkboxall:not(:hidden) :checkbox");
            // var chekdslen = _all_Inventory_body.find(".checkboxall:not(:hidden) :checked");

            var pagelcheckbox_all = _opts._Dom.find('.panel_topleft_box .pagelcheckbox_all');
            var page_allchekbox = pagelcheckbox_all.find(':checkbox');
            var focusedInput = _all_Inventory_body.find(".focusedInput");

//===========================
             //子集--li
            var li_checked_all=0,liobjxall=0;
           if(subcheckboxall_box.length > 0){
            
             for(var subitmeli=0;subitmeli < subcheckboxall_box.length;subitmeli++){

                var itme_liobj=subcheckboxall_box[subitmeli];
                 if(itme_liobj.length > 0){
                     //所有显示的li
                      var li__len=itme_liobj.length;
                      //所有选中的
                      var li_len_checkbox=0;
                      for(var checkbox_li=0;checkbox_li < itme_liobj.length;checkbox_li++){
                        var itmescksr=itme_liobj.eq(checkbox_li);
                        li_len_checkbox=li_len_checkbox + itmescksr.find("input:checked").length;

                      }

                     liobjxall=liobjxall+li__len;
                     li_checked_all=li_checked_all+li_len_checkbox;

                 }

             }

            //说明有没选中的
             if(liobjxall > li_checked_all && li_checked_all > 0){
               
                page_allchekbox.prop("indeterminate", true);

                return false;
                
             }

             if(liobjxall == li_checked_all){

                page_allchekbox.prop("indeterminate", false);
                page_allchekbox.prop("checked", true);

                return false;
                  
             }

             if(li_checked_all == 0){

              page_allchekbox.prop("indeterminate", false);
              page_allchekbox.prop("checked", false);

              return false;

             }



         }

 //======================
           


            //  console.log(page_allchekbox);

            if (p_sub_checkboxall_len == chekdslen_len && chekdslen_len > 0) {

                page_allchekbox.prop("indeterminate", false);
                page_allchekbox.prop("checked", true);

                return false;

            };

            //没选满时
            if (chekdslen_len < p_sub_checkboxall_len && focusedInput.length > 0) {

                page_allchekbox.prop("indeterminate", true);

                return false;

            }



            if (chekdslen_len == 0 && p_sub_checkboxall_len == 0) {

                page_allchekbox.prop("indeterminate", false);

                return false;

            }




        },
        loadingdata: function(_opts) {

            //var _this = this;
            //var _opts = _this.opts;
            var _dataurl = _opts.dataUrl;
            var dataname = _opts.renderTo.replace(/\.|\/|#/g, "");
            _opts.jsondataid = dataname;
            Componbox.JSONDATA[dataname] = [];
            Componbox.SearchvalDate[dataname] = {};


            Componbox.Confingloading(_opts);

            //------------data----- 
            if (typeof _dataurl == "string" && typeof _dataurl != "undefined") {
                var paramdata = {};
                if (_opts.postData) {
                    paramdata = _opts.postData;
                }

                $.getJSON(_dataurl, paramdata, function(json) {

                    Componbox.JSONDATA[dataname] = json;
                    Componbox.createitme(_opts, json);

                }).error(function(e) {

                    var errorobj = {
                        txt: "服务端请求失败",
                        error: e
                    };
                    Componbox.error(_opts, errorobj);

                });


            } else if (typeof _dataurl == "object") {

                Componbox.JSONDATA[dataname] = _dataurl;
                Componbox.createitme(_opts, _dataurl);

            } else {

                var errorobj = {
                    txt: "没有dataUrl",
                    error: ""
                };
                Componbox.error(_opts, errorobj);

            }

        },
        Confingloading: function(_opts) {


            var _configurl = _opts.configurl;

            //------------data----- 
            if (typeof _configurl == "string" && typeof _configurl != "undefined") {


                $.getJSON(_configurl, function(json) {

                    Componbox.ConfingDATA[_opts.jsondataid] = json;

                }).error(function(e) {

                    var errorobj = {
                        txt: "config请求失败",
                        error: e
                    };
                    Componbox.error(_opts, errorobj);

                });


            } else if (typeof _configurl == "object") {

                Componbox.ConfingDATA[_opts.jsondataid] = _configurl;

            } else {

                var errorobj = {
                    txt: "no config",
                    error: ""
                };
                Componbox.error(_opts, errorobj);

            }


        },
        row_colmd: function(LOCATIONS, dataname, subtitle, _AREA_ID, p_boxre) {

            //var subtitle = LOCATIONS[0].SLC_POSITION_ALL;
            //subtitle = subtitle.substring(0, (subtitle.length - 4));



            var subid = subtitle + _.now();

            var confindata = Componbox.ConfingDATA[dataname];
            var buttongroup = "";

            if (confindata) {
                var buttonsall = "";
                for (var conkey in confindata.buttons) {
                    var itmebutton = confindata.buttons[conkey];
                    if (itmebutton.label) {
                        buttonsall += "<button id='" + itmebutton.id + "'>" + itmebutton.label + "</button>";
                    }

                }

                if (buttonsall != "" && typeof buttonsall != "undefined") {
                    buttongroup = "<div class='Inventory_foot'>" + buttonsall + "</div>";
                }

            }

            var row_td_html = "";
            row_td_html = "<div id='" + subid + "' class='panel panel-default'><div class='panel-heading'><label class='checkboxall'><input type='checkbox' /></label><span class='panel_heading_title'>" + subtitle + "</span></div><div class='panel-body itmeInventory_box'><div class='itmeui_box'><ul class='list-unstyled'>#Inventory_box#</ul></div>" + buttongroup + "</div></div>";

            var ulli = "";
            var SearchvalDate = Componbox.SearchvalDate[dataname];

            //SearchvalDate={}

            var title_AREA_ID = subtitle + "__" + _AREA_ID;

            SearchvalDate["" + title_AREA_ID + ""] = [];



            for (var key in LOCATIONS) {
                var itme = LOCATIONS[key];
                ulli += "<li id='" + p_boxre + "inven_" + itme.SLC_ID + "'><div class='checkbox'><label><input type='checkbox' data-checkboxid='" + itme.SLC_ID + "'/><span>" + itme.SLC_POSITION + "</span></label></div></li>";


                SearchvalDate["" + title_AREA_ID + ""].push({
                    SLC_ID: itme.SLC_ID,
                    SLC_POSITION: itme.SLC_POSITION
                });
            }

            row_td_html = row_td_html.replace("#Inventory_box#", ulli);

            return row_td_html;

        },
        createcss: function(opts) {


            var cssfileall = [
                "require_css!bootstrap-css/bootstrap.min",
                "require_css!Font-Awesome",
                "require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css",
                "require_css!oso.lib/InventoryControls/css/style.css"
            ];

            var _dom = opts._Dom;

            require(cssfileall, function() {

                setTimeout(function() {

                    _dom.removeClass('inventoryhide');

                }, 10);

            });

        },
        createboxhtml: function(_opts) {

            //var _opts=_this.opts;
            var placeholder = "过滤条件";
            var inventorytitle = "库存筛选";
            if (_opts.inventorytitle) {
                inventorytitle = _opts.inventorytitle;
            }

            if (_opts.placeholder) {
                placeholder = _opts.placeholder;
            }

            var buttontxt = "Assign User";
            var _bnt_txt = _opts.filterbuttonstxt;
            if (_bnt_txt && typeof _bnt_txt == "string" && _bnt_txt != "") {
                buttontxt = _bnt_txt;
            }

            var AssignUser = "";

            if (_opts.filter) {

                AssignUser = "<div class='col-xs-1'><button type='button' class='btn btn-primary'>" + buttontxt + "</button></div>";

            }

            var _quey = "<div class='Inventory_filter'><div class='row'><div class='col-xs-9'><input class='form-control' type='text' placeholder='" + placeholder + "' /></div>" + AssignUser + "</div></div>";

            var titlequey = "",
                inveboxquey = "",
                _heading_p5px = "";
            if (_opts.title_in_input) {
                titlequey = "<label class='titleinputlable'>" + _quey + "</label>";
                _heading_p5px = " ptopfoot5px";
            } else {
                inveboxquey = _quey;
            }


            if (_opts._Dom.length > 0) {

                window.setTimeout(function() {

                    $("<div class='panel panel-default'><div class='panel-heading" + _heading_p5px + "'><label class='panel_topleft_box'><span class='pagelcheckbox_all'><input type='checkbox'/></span><span class='panel_heading_title'>" + inventorytitle + "</span></label>" + titlequey + "</div><div class='panel-body Inventory_box'>" + inveboxquey + "<div class='Inventory_body'><span class='loadingbox'></span></div></div></div>").appendTo(_opts._Dom);

                }, 10);

                window.setTimeout(function() {
                    Componbox.loadingdata(_opts);

                    if (_opts.on_render_complete) {
                        _opts.on_render_complete();
                    }

                }, 100);


            }


        },
        topevntes: function(_opts) {

            var dom = _opts._Dom;
            var Inventory_filter = dom.find('.Inventory_filter');
            var form_control = Inventory_filter.find('.form-control');
            var btn_primary = Inventory_filter.find('.btn-primary');
            var toptitlecheckbox = dom.find('.pagelcheckbox_all :checkbox');
           //全局checkbox
            toptitlecheckbox.click(function() {

                Componbox.clrerallhiddencheck(_opts);

                var _all_Inventory_body = dom.find('.Inventory_body');

                var show_subitmebox = _all_Inventory_body.find(".subitmebox:not(:hidden)");


                var p_checkboxall = show_subitmebox.find(":checkbox");
                var indeterminate = show_subitmebox.find(':indeterminate');

                var _thischeckd = $(this).prop("checked");

                p_checkboxall.prop("checked", _thischeckd);

                indeterminate.prop("indeterminate", false);

                var panel_default = show_subitmebox.find('.panel-default');

                if (_thischeckd) {
                    panel_default.addClass('focusedInput');
                } else {
                    panel_default.removeClass('focusedInput');
                }



                if (_opts.all_areas_checkbox) {

                    var jqcheckall = _all_Inventory_body.find(':checked');

                    var chekcidsirn = [];
                    for (var _ck = 0; _ck < jqcheckall.length; _ck++) {
                        var itme_ck = jqcheckall.eq(_ck);
                        var _idsring = itme_ck.data("checkboxid");
                        if (_idsring != "" && typeof _idsring != "undefined") {
                            chekcidsirn.push(_idsring);
                        }
                    }

                    var _jsonalldata = [];

                    if (jqcheckall.length > 0 && chekcidsirn.length > 0) {
                        var _alldata_ = Componbox.JSONDATA[_opts.jsondataid];
                        for (var allkey_ in _alldata_) {
                            var _LOCATIONS_ = _alldata_[allkey_].LOCATIONS;
                            for (var locakey in _LOCATIONS_) {

                                var _LOCATIitme = _LOCATIONS_[locakey];
                                for (var _key in chekcidsirn) {
                                    var idsirns = chekcidsirn[_key];
                                    if (idsirns == _LOCATIitme.SLC_ID) {
                                        _jsonalldata.push(_LOCATIitme);
                                        break;
                                    }

                                }

                            }

                        }

                    }

                    _opts.all_areas_checkbox({
                        checkliset: jqcheckall,
                        dataitme: _jsonalldata
                    });

                }

            });

            form_control.on('input', function(e) {

                var _thisinput = $(this);
                var _val = $.trim(_thisinput.val());
                _val = _val.replace(/\/|<|>/g, "");


                if (_opts.timeoutsring != "" && typeof _opts.timeoutsring != "undefined") {
                    window.clearTimeout(_opts.timeoutsring);
                }


                if (_val != "" && typeof _val != "undefined") {

                    //条件显示==========
                    _opts.timeoutsring = setTimeout(function() {

                        Componbox.Screendisplay(_opts, _val);

                        _thisinput.focus();

                    }, 400);


                } else {

                    //全部显示
                    Componbox.showallitme(_opts);

                }


                e.preventDefault();

                return false;


            });

            form_control.keyup(function(e) {
                var _val = $.trim($(this).val());
                if (_val == "" || typeof _val == "undefined" && e.keyCode == 8) {

                    if (_opts.timeoutsring != "" && typeof _opts.timeoutsring != "undefined") {
                        window.clearTimeout(_opts.timeoutsring);
                    }

                    var _all_Inventory_body = _opts._Dom.find('.Inventory_body');

                    var _subitmebox = _all_Inventory_body.find('.subitmebox');
                    for (var subkye = 0; subkye < _subitmebox.length; subkye++) {
                        var _subitmes = _subitmebox.eq(subkye);
                        var _indeterm_ = _subitmes.find(".checkboxall :indeterminate");
                        var _checked_ = _subitmes.find(".checkboxall :checked");
                        if (_indeterm_.length < 1 && _checked_.length < 1) {

                            var subitme_checkbox = _subitmes.find(":checkbox");
                            var subitme_indeterminate = _subitmes.find(":indeterminate");
                            subitme_indeterminate.prop("indeterminate", false);
                            subitme_checkbox.prop("checked", false);
                            var _panel_default = _subitmes.find('.panel-default');
                            _panel_default.removeClass('focusedInput');

                        }

                    }



                    var p_sub_checkboxall = _all_Inventory_body.find(".checkboxall :checkbox");
                    var chekdslen = _all_Inventory_body.find(".checkboxall :checked");
                    var __indeterminate = _all_Inventory_body.find(".checkboxall :indeterminate");

                    var paneltitlecheck = _opts._Dom.find('.pagelcheckbox_all :checkbox');
                    //半选
                    if (__indeterminate.length > 0 && __indeterminate.length < chekdslen.length || chekdslen.length < p_sub_checkboxall.length) {
                        paneltitlecheck.prop("indeterminate", true);
                    }

                    //不选
                    if (__indeterminate.length == 0 && chekdslen.length == 0) {

                        paneltitlecheck.prop("indeterminate", false);
                        paneltitlecheck.prop("checked", false);
                    }

                }
            });

            if (btn_primary.length > 0) {
 //button
                btn_primary.click(function() {

                    var title_idall = [];
                    var Inventory_body = _opts._Dom.find('.Inventory_body');



                    var subitmebox = Inventory_body.find('.subitmebox');
                    for (var title_i = 0; title_i < subitmebox.length; title_i++) {
                        var itmebox = subitmebox.eq(title_i);
                        if (itmebox.is(":visible")) {


                            var subitmeall = [];
                            var subitmeulliall = itmebox.find('.list-unstyled li');

                            for (var sub_i = 0; sub_i < subitmeulliall.length; sub_i++) {

                                var subitmeobj = subitmeulliall.eq(sub_i);
                                if (subitmeobj.is(":visible")) {
                                    var checkboxobj = subitmeobj.find('input');
                                    var _id = checkboxobj.data("checkboxid");
                                    var cheksr = checkboxobj.prop("checked");
                                    var spantxt = checkboxobj.next("span").text();
                                    subitmeall.push({
                                        id: _id,
                                        check: cheksr,
                                        label: spantxt
                                    });
                                }

                            }

                            title_idall.push({
                                pid: itmebox.attr("id"),
                                subitme: subitmeall
                            });


                        }

                    }

                    if (_opts.filter) {

                        _opts.filter(title_idall);

                    }

                });

            }


        },
        clrerallhiddencheck: function(_opts) {


            var _all_Inventory_body = _opts._Dom.find('.Inventory_body');

            var subitmebox = _all_Inventory_body.find('.subitmebox:hidden');

            var _checked = subitmebox.find(':checked');
            _checked.prop("checked", false);

            var _indeterminate = subitmebox.find(':indeterminate');
            _indeterminate.prop("indeterminate", false);

            var panel_default = subitmebox.find('.panel-default');

            panel_default.removeClass('focusedInput');


            //focusedInput

        },
        showallitme: function(_opts) {

            var p_dom = _opts._Dom;



            var Inventory_body = p_dom.find('.Inventory_body');
            var subitmeboxall = Inventory_body.find('.subitmebox');

           // if (_opts.searchfieldkey != "AREA_NAME") {

                for (var boxi = 0; boxi < subitmeboxall.length; boxi++) {

                    var boxdom = subitmeboxall.eq(boxi);
                    var liall = boxdom.find('.list-unstyled li:hidden');
                    liall.show();
                }

           // }

           var subitmeboxall_hide = Inventory_body.find('.subitmebox:hidden');

            subitmeboxall_hide.show();


        },
        Screendisplay: function(_opts, _val) {

            //筛选触发显示
            var p_boxre = "";
            if (_opts.renderTo.indexOf(".") > -1 || _opts.renderTo.indexOf("#") > -1)
                p_boxre = _opts.renderTo.replace(/^\.|^#/, "") + "_";


            var subtimeouts = "";

            var JSONDATA = Componbox.SearchvalDate[_opts.jsondataid];

            // var SubitmeDate=Componbox.SubitmeDate[_opts.jsondataid];
            var newitmeidall = [],
                new_sub_itmeidall = [];

            // console.dir(JSONDATA);

            //title-key ---
            var slcstinrs = [];
            var _searchval = new RegExp(_val, "i");

            for (var itmek in JSONDATA) {

                var itmedata = JSONDATA[itmek];

                //1，先筛选父项

                //分离出----所有父项----AREA_ID

                // if (searchfieldkey == "AREA_NAME") {


                var __txt = itmek;
                var splitstirns = __txt.split(/__/);

                var txt_AREA_ID = splitstirns[1];
                var test_key_txt = splitstirns[0];

                if (_searchval.test(test_key_txt)) {
                    // console.log(__txt);
                    newitmeidall.push(txt_AREA_ID);
                }


            }


            //2，同时缓存子项
            //分离出----所有的子项----SLC_ID

            //  if (searchfieldkey != "AREA_NAME") {    
            //这里可能是慢半拍
            for (var itmek1 in JSONDATA) {

                var itmedata1 = JSONDATA[itmek1];

                if (itmedata1.length > 0) {

                    for (var subitmeskey in itmedata1) {

                        var itmesubdata = itmedata1[subitmeskey];
                        if (_searchval.test(itmesubdata.SLC_POSITION)) {

                            slcstinrs.push(itmesubdata.SLC_POSITION);

                            new_sub_itmeidall.push(itmesubdata.SLC_ID);

                        }

                    }

                }

            }


            var p_dom = _opts._Dom;
            var Inventory_body = p_dom.find('.Inventory_body');



            //优先匹配父项ui
            var subitmeboxall = Inventory_body.find('.subitmebox');
            if (newitmeidall.length > 0) {



                //隐藏所有的
                subitmeboxall.hide();

                for (var titlekey in newitmeidall) {

                    var title_id = newitmeidall[titlekey];


                    var _li_itme_dom = $("#" + p_boxre + "title_" + title_id + "");

                    if (_li_itme_dom.length > 0) {
                        _li_itme_dom.show();
                        var p_liall = _li_itme_dom.find('.list-unstyled li');
                        p_liall.show();


                    }

                }


            }




            //其次匹配子项ui


            if (new_sub_itmeidall.length > 0) {

                // console.log(new_sub_itmeidall.length);
                // console.log(slcstinrs.length);


                for (var boxi = 0; boxi < subitmeboxall.length; boxi++) {

                    var boxdom = subitmeboxall.eq(boxi);
                    var liall = boxdom.find('.list-unstyled li');
                    liall.hide();
                }

                // if (subtimeouts != "" && typeof subtimeouts != "undefined") {

                //     window.clearTimeout(subtimeouts);

                // }

               // subtimeouts = window.setTimeout(function() {

                    //console.dir(slcstinrs);
                    // console.dir(new_sub_itmeidall);

                    for (var subnewitmekey1 in new_sub_itmeidall) {

                        var newitmeidsring1 = new_sub_itmeidall[subnewitmekey1];

                        // console.log(newitmeidsring1);

                        var _li_itme_dom = $("#" + p_boxre + "inven_" + newitmeidsring1 + "");
                        if (_li_itme_dom.length > 0) {
                            _li_itme_dom.show();
                            var p_subitmebox = _li_itme_dom.parents(".subitmebox").eq(0);
                            if (p_subitmebox.is(":hidden")){
                                p_subitmebox.show();
                            }

                        }

                    }

               // }, 60);

             //处理无子项的
 
              var subitmeboxall_visible = Inventory_body.find('.subitmebox:visible');
               if(subitmeboxall_visible.length > 0){

                 for(var sub_viskey=0;sub_viskey < subitmeboxall_visible.length;sub_viskey++){

                   var itme__subitmebox_visible=subitmeboxall_visible.eq(sub_viskey);
                    var li__visible=itme__subitmebox_visible.find(".list-unstyled li:visible");
                    if(li__visible.length < 1){
                       itme__subitmebox_visible.hide();
                    }

                 }

               }

            }




            //被匹配上的不销毁--和--勾选上的都不销毁


            var _allsubitmebox = Inventory_body.find('.subitmebox');

            for (var sub_i = 0; sub_i < _allsubitmebox.length; sub_i++) {

                var allitmeobj = _allsubitmebox.eq(sub_i);
                var _subidsring = allitmeobj.attr("id");
                var clrebox = true;
                for (var titlekey in newitmeidall) {

                    var title_id = newitmeidall[titlekey];
                    var _li_itme_dom = p_boxre + "title_" + title_id;
                    var _checkboxall = allitmeobj.find('.checkboxall');

                    var indeter__ = _checkboxall.find(':indeterminate');
                    var checked__ = _checkboxall.find(':checked');

                    if (_subidsring == _li_itme_dom || indeter__.length > 0 || checked__.length > 0) {
                        clrebox = false;
                        break;
                    }


                }

                if (clrebox) {

                    var __allcheckbox = allitmeobj.find(":checkbox");
                    __allcheckbox.prop("indeterminate", false);
                    __allcheckbox.prop("checked", false);
                    var _panel_default = allitmeobj.find('.panel-default').eq(0);
                    _panel_default.removeClass('focusedInput');

                }

            }

            // Componbox.clrerallhiddencheck(_opts);

            var _focusedInput = _allsubitmebox.find('.focusedInput');
            var paneltitlecheck = _opts._Dom.find('.pagelcheckbox_all :checkbox');

            var _all_Inventory_body = _opts._Dom.find('.Inventory_body');
            var p_sub_checkboxall = _all_Inventory_body.find(".checkboxall :checkbox");
            var chekdslen = _all_Inventory_body.find(".checkboxall :checked");
            var __indeterminate = _all_Inventory_body.find(".checkboxall :indeterminate");

            //半选
            if (_allsubitmebox.length > _focusedInput.length && __indeterminate.length > 0) {
                paneltitlecheck.prop("indeterminate", true);
                return false;
            }

            //不选
            if (chekdslen.length == 0 && p_sub_checkboxall.length > 0) {

                paneltitlecheck.prop("indeterminate", false);
                paneltitlecheck.prop("checked", false);
                return false;

            }

            //全选
            if (p_sub_checkboxall.length == chekdslen.length) {

                paneltitlecheck.prop("indeterminate", false);
                paneltitlecheck.prop("checked", true);

            }



            //sub_itme-key


        }


    };



    return InventoryPlugin;

}));
