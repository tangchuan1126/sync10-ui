 requirejs(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

     requirejs(["oso.lib/nprogress/nprogress.min", "jquery", "jquery.browser", "require_css!oso.lib/nprogress/nprogress", "require_css!Font-Awesome","require_css!bootstrap-css/bootstrap.min","require_css!oso.lib/bootstrap/css/bootstrap_input_icos.css"],
         function(NProgress, $) {


             $("body").show();

             NProgress.start();         



             var jqrinds = [
                 "/Sync10-ui/bower_components/jqgrid/js/i18n/grid.locale-en.js",
                 "/Sync10-ui/bower_components/jqgrid/js/jquery.jqGrid.js"
             ];


             requirejs(jqrinds, function(jqGrid) {

                 $(function() {

//################

                 	 var mydata = [
                    {id: "1",  invdate: "2007-10-01", name: "test",   note: "note",   amount: "200.00", tax: "10.00", closed: true,  ship_via: "TN", total: "210.00"},
                    {id: "2",  invdate: "2007-10-02", name: "test2",  note: "note2",  amount: "300.00", tax: "20.00", closed: false, ship_via: "FE", total: "320.00"},
                    {id: "3",  invdate: "2007-09-01", name: "test3",  note: "note3",  amount: "400.00", tax: "30.00", closed: false, ship_via: "FE", total: "430.00"},
                    {id: "4",  invdate: "2007-10-04", name: "test4",  note: "note4",  amount: "200.00", tax: "10.00", closed: true,  ship_via: "TN", total: "210.00"},
                    {id: "5",  invdate: "2007-10-31", name: "test5",  note: "note5",  amount: "300.00", tax: "20.00", closed: false, ship_via: "FE", total: "320.00"},
                    {id: "6",  invdate: "2007-09-06", name: "test6",  note: "note6",  amount: "400.00", tax: "30.00", closed: false, ship_via: "FE", total: "430.00"},
                    {id: "7",  invdate: "2007-10-04", name: "test7",  note: "note7",  amount: "200.00", tax: "10.00", closed: true,  ship_via: "TN", total: "210.00"},
                    {id: "8",  invdate: "2007-10-03", name: "test8",  note: "note8",  amount: "300.00", tax: "20.00", closed: false, ship_via: "FE", total: "320.00"},
                    {id: "9",  invdate: "2007-09-01", name: "test9",  note: "note9",  amount: "400.00", tax: "30.00", closed: false, ship_via: "TN", total: "430.00"},
                    {id: "10", invdate: "2007-09-08", name: "test10", note: "note10", amount: "500.00", tax: "30.00", closed: true,  ship_via: "TN", total: "530.00"},
                    {id: "11", invdate: "2007-09-08", name: "test11", note: "note11", amount: "500.00", tax: "30.00", closed: false, ship_via: "FE", total: "530.00"},
                    {id: "12", invdate: "2007-09-10", name: "test12", note: "note12", amount: "500.00", tax: "30.00", closed: false, ship_via: "FE", total: "530.00"}
                ],
                lastColIndex,
                lastRowIndex,__iRow,list2this;

            $("#list2").jqGrid({
                datatype: 'local',
                url:'dataProducts.html',
                postData:{aaa:""},
                data: mydata,
                colNames: ['Client', 'Date', 'Amount', 'Tax', 'Total', 'Closed', 'Shipped via', 'Notes'],
                colModel: [
                    {name: 'name', index: 'name', width: 70, editable: true},
                    {name: 'invdate', index: 'invdate', width: 80, align: 'center', sorttype: 'date',
                        formatter: 'date', formatoptions: {newformat: 'm/d/Y'}, datefmt: 'm/d/Y'},
                    {name: 'amount', index: 'amount', width: 75, editable: true, formatter: 'number', align: 'right'},
                    {name: 'tax', index: 'tax', width: 50, editable: true, formatter: 'number', align: 'right'},
                    {name: 'total', index: 'total', width: 60, editable: true, formatter: 'number', align: 'right'},
                    {name: 'closed', index: 'closed', width: 70, editable: true, align: 'center',
                        formatter: 'checkbox', edittype: 'checkbox', editoptions: {value: 'Yes:No', defaultValue: 'Yes'}},
                    {name: 'ship_via', index: 'ship_via', width: 100, editable: true, align: 'center', formatter: 'select',
                        edittype: 'select', editoptions: {value: 'FE:FedEx;TN:TNT;IN:Intim', defaultValue: 'Intime'}},
                    {name: 'note', index: 'note', width: 70, sortable: false}
                ],
                rowNum: 10,
                rowList: [5, 10, 20],
                pager: '#pager2',
                gridview: true,
                rownumbers: true,
                sortname: 'invdate',
                viewrecords: true,
                mtype: "GET",
                sortorder: 'desc',
                cellsubmit: 'clientArray',
                onCellSelect: function (rowid,iCol){
                	list2this=this;

                //选中单元格时2
                    var $this = $(this);

                    $this.jqGrid('setGridParam', {cellEdit: true});
                    $this.jqGrid('editCell', __iRow, iCol, true);
                    $this.jqGrid('setGridParam', {cellEdit: false});
                    lastRowIndex = __iRow;
                    lastColIndex = iCol;

                },
                beforeSelectRow: function (rowid, e) {
                	//选中该行前 1
                	list2this=this;
                    var $this = $(this),
                        $td = $(e.target).closest('td'),
                        $tr = $td.closest('tr'),
                        iRow = $tr[0].rowIndex,
                        iCol = $.jgrid.getCellIndex($td);
                        __iRow=iRow;


                   
                    if (typeof lastRowIndex !== "undefined" && typeof lastColIndex !== "undefined" &&
                            (iRow !== lastRowIndex || iCol !== lastColIndex)) {
                 
                  $("#list2").saveRow(iRow, true,'clientArray');
                   ClearEditor(this,iRow,iCol);
                

                    }
                    

                    return true;
                }, 
                beforeEditCell:function(rowid, cellname, value, iRow, iCol){
                //单元格编辑前触发3
                	console.log(3333);

                },       
                afterEditCell: function (rowid, cellName, cellValue, iRow) {
                	list2this=this;
                //单元格进入编辑后4
                    var cellDOM = this.rows[iRow], oldKeydown,
                        $cellInput = $('input, select, textarea', cellDOM),
                        events = $cellInput.data('events'),
                        $this = $(this);
                    if (events && events.keydown && events.keydown.length) {
                        oldKeydown = events.keydown[0].handler;
                        $cellInput.unbind('keydown', oldKeydown);
                        $cellInput.bind('keydown', function (e) {
                            $this.jqGrid('setGridParam', {cellEdit: true});
                            oldKeydown.call(this, e);
                            $this.jqGrid('setGridParam', {cellEdit: false});
                        });
                    }
                },
                afterSaveCell:function(rowid, name, val, iRow, iCol){
                	list2this=this;
                //保存后
                   var $this = $(this);

                	setTimeout(function(){

               //..

                	

               if(typeof lastRowIndex!="undefined" && typeof lastColIndex!="undefined"){
                	 
                	 ClearEditor(this,iRow,iCol);
                    }

                	}, 120);

                },
                caption: 'How to enable cell editing on double-click',
                height: 'auto',
                    cellurl:'php1.php',
                    editurl:'php2.php'

            });

//撤销编辑

function ClearEditor(giridobj,iRow,iCol){
 
  var $this=$(giridobj);
    if (typeof lastRowIndex != "undefined" && typeof lastColIndex != "undefined" ){
                    	
                        $this.jqGrid('setGridParam', {cellEdit: true});                        
                        $this.jqGrid('saveCell', lastRowIndex, lastColIndex);
                       // $this.jqGrid('restoreCell', lastRowIndex, lastColIndex, true);
                        $this.jqGrid('setGridParam', {cellEdit: false});

                       // $(giridobj.rows[lastRowIndex].cells[lastColIndex]).removeClass("ui-state-highlight");
                    }


}


//关闭开启的单元格
 $(document).click(function(e){

 

e.preventDefault();
//gets the element where the click event happened
var clickedElement = e.target;

if(!clickedElement.offsetParent){


 if(typeof lastRowIndex!="undefined" && typeof lastColIndex!="undefined" && list2this){


 ClearEditor(list2this,lastRowIndex,lastColIndex);


 }

 return false;

}

  

});
 

//##############








var lastsel2;

                     var jqidss=jQuery("#addgrid").jqGrid({
                         url: 'd.xml',
                         datatype: "xml",
                         colNames: ['Inv No', 'Date', 'Client', 'Amount', 'Tax', 'Total', 'Closed', 'Ship via', 'Notes'],
                         colModel: [{
                             name: 'id',
                             index: 'id',
                             width: 55,
                             editable: false,
                             editoptions: {
                                 readonly: true,
                                 size: 10
                             }
                         }, {
                             name: 'invdate',
                             index: 'invdate',
                             width: 80,
                             editable: true,
                             editoptions: {
                                 size: 10
                             },
                             dataEvents:[{type:'click',fn: function(e) {
                                var thisval = $(e.target).val();
                                console.log(thisval);
                            } //end func
                        } // end type
                     ]
                         }, {
                             name: 'name',
                             index: 'name',
                             width: 90,
                             editable: true,
                             editoptions: {
                                 size: 25
                             }
                         }, {
                             name: 'amount',
                             index: 'amount',
                             width: 60,
                             align: "right",
                             editable: true,
                             editoptions: {
                                 size: 10
                             }
                         }, {
                             name: 'tax',
                             index: 'tax',
                             width: 60,
                             align: "right",
                             editable: true,
                             editoptions: {
                                 size: 10
                             }
                         }, {
                             name: 'total',
                             index: 'total',
                             width: 60,
                             align: "right",
                             editable: true,
                             editoptions: {
                                 size: 10
                             }
                         }, {
                             name: 'closed',
                             index: 'closed',
                             width: 55,
                             align: 'center',
                             editable: true,
                             edittype: "checkbox",
                             editoptions: {
                                 value: "Yes:No"
                             }
                         }, {
                             name: 'ship_via',
                             index: 'ship_via',
                             width: 70,
                             editable: true,
                             edittype: "select",
                             editoptions: {
                                 value: "FE:FedEx;TN:TNT"
                             }
                         }, {
                             name: 'note',
                             index: 'note',
                             width: 100,
                             sortable: false,
                             editable: true,                             
                             edittype: "textarea",
                             editoptions: {
                                 rows: "2",
                                 cols: "20"
                             }
                         }],
                         onSelectRow: function(id){

                         	//换行时
                         	if(id && id!==lastsel2){
							jQuery('#addgrid').jqGrid('restoreRow',lastsel2);
							//jQuery('#addgrid').jqGrid('editRow',id,true);
							lastsel2=id;
						};
							
						
			          },
			           beforeEditCell: function(id, name, val, iRow, iCol){



			           	console.log(id);

			    // var p_name = jQuery('#addgrid').jqGrid('getCell',id,'p_name');

            // jQuery("#union").jqGrid('setGridParam', {url: "subDataProducts.html?id=" + id,page: 1});
            // jQuery("#union").jqGrid('setCaption', p_name + " Suit Relationship").trigger('reloadGrid');
                        

			           },
	                 ajaxStart:function(e){

	                 	console.dir(e);

	                 },ajaxStop:function(e){

	                 	console.log(e);

	                 },
                         rowNum: 10,
                         rowList: [10, 20, 30],
                         pager: '#pagerad',
                         sortname: 'id',
                         viewrecords: true,
                         cellEdit: true,
                         sortorder: "desc",
                         caption: "Adding data Example",
                         editurl: "server.php"
                     });

       

                     $("#badata").click(function() {
                         jQuery("#addgrid").jqGrid('editGridRow', "new",{
                             height: 380,
                             reloadAfterSubmit: false,
                             beforeSubmit: function(postdata) {

                                 postdata.id=Math.ceil(Math.random()) +"_we";

                                 console.log(postdata);                                 
                               $("#addgrid").addRowData(postdata.id, postdata);

                                 return false;

                             },
                             onInitializeForm:function(e){
                                
                              var inputs=e.find("#invdate");

                              inputs.focusout(function(){
                                var thiss=$(this);
                                 console.log(thiss.val());
                              });

                             }
                         });
                     });


           		

                     jQuery("#addgrid").jqGrid = {
                         edit: {
                             addCaption: "Add Record",
                             editCaption: "Edit Record",
                             bSubmit: "Submit",
                             bCancel: "Cancel",
                             bClose: "Close",
                             saveData: "Data has been changed! Save changes?",
                             bYes: "Yes",
                             bNo: "No",
                             bExit: "Cancel",
                         }
                     }



                     //-----------------------------


                     var data = {
                             page: "1",
                             records: "3",
                             rows: [{
                                 id: "10",
                                 Name: "Name 10000x",
                                 PackageCode: "83123a",
                                 other: "x",
                                 subobject: {
                                     x: "a",
                                     y: "b",
                                     z: [1, 2, 3]
                                 }
                             }, {
                                 id: "20",
                                 Name: "Name 3",
                                 PackageCode: "83432a",
                                 other: "y",
                                 subobject: {
                                     x: "c",
                                     y: "d",
                                     z: [4, 5, 6]
                                 }
                             }, {
                                 id: "30",
                                 Name: "Name 2",
                                 PackageCode: "83566a",
                                 other: "z",
                                 subobject: {
                                     x: "e",
                                     y: "f",
                                     z: [7, 8, 9]
                                 }
                             }]
                         },
                         $grid = $("#43rowed3");

                     $grid.jqGrid({
                         gridComplete:function(d){

                           var p_gridtable=$("#addgrid");
                           var id = p_gridtable.jqGrid('getGridParam','selrow');
                           var rowDatas = p_gridtable.jqGrid('getRowData', id);

                           console.log(rowDatas);


                         },
                         datastr: data,
                         datatype: "jsonstring",
                         jsonReader: {
                             repeatitems: false
                         },
                         colNames:['PackageCode','Name'],
                         colModel: [{
                             name: "PackageCode",
                             index: 'PackageCode',
                             width: 110,
                             editable: false
                         }, {
                             name: "Name",
                             index: 'Name',
                             width: 300,
                             editable: false
                         }],
                         pager: "#p43rowed3",
                         rowNum: 2,
                         rowList: [1, 2, 10],
                         viewrecords: true,
                         rownumbers: true,
                         sortname: 'id',
                         sortorder: "desc",
                         caption: "Using navigator",
                         editurl:"someurl.php",
                     });

  


                     // $grid.jqGrid('navGrid', "#p43rowed3", {
                     //     edit: false,
                     //     add: true,
                     //     del: false
                     // });


               $grid.jqGrid('navGrid', '#p43rowed3',
                 {}, //options
                    {
                        height: 280,
                        reloadAfterSubmit: false
                    }, // edit options
                    {
                        height: 280,
                        reloadAfterSubmit: false
                    }, // add options
                    {
                        reloadAfterSubmit: false
                    }, // del options
                    {} // search options
                );

                     $("#bedata").click(function() {
                         var idsr = Math.ceil(Math.random());
                         var ranodes = Math.ceil(Math.random() * (1000 - 100) + 100);
                         var rowdatas = {
                             id: idsr,
                             Name: "Name 1" + ranodes,
                             PackageCode: "83123a",
                             other: "x",
                             subobject: {
                                 x: "a",
                                 y: "b",
                                 z: [1, 2, 3]
                             }
                         };

                         $grid.addRowData(rowdatas.id, rowdatas,"first");
                        
                //           for (i=0;i<newData.length;i++) {
                // grid.jqGrid('addRowData', myData.length+i, newData[newData.length-i-1], "first");
            //}


                     });



                     //var tables=jQuery("#43rowed3");
                     //tables[0].addJSONData({"total":1,"page":1,"records":2,"rows":[{"projectId":"1022","name":"john"}]});
                

                         NProgress.done();


                 });

             });

              

         });
     // requirejs_config end
 });
 // nprogress end