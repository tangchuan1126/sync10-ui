"use strict";

/*

比如自己的某些控件并不用写到全局的（requirejs.config.sj)里，
又想在自己项目里全局用，那就请如下
扩展一下requirejs.config配置

*/
//deps: ["require_css!abc2_css"]依赖写法
requirejs.config({
    paths: {
        "abc1": "./abc",
        "abc2": "./abc2",
        "abc1_css": "./abc",
        "abc2_css": "./abc2"
    },
    shim: {
        "abc2": {
            deps: ["require_css!abc2_css"]
        }
    }
});

//加载公共控件,并引用自定义的模块
require(["/Sync10-ui/requirejs_config.js"], function() {

    //require_css!abc1_css在head头部创建*.css文件

    require(["jquery", "abc1", "abc2", "require_css!abc1_css"], function($, abc1, abc2) {

        $(function() {

            console.log(abc1);

            console.log(abc2);


        });


    });


});
