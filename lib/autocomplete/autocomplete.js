define(["jquery",'jqueryui/autocomplete'],function($){
	var result = {
		highlight:function(match, keywords) 
		{

			keywords = keywords.replace("\"","");
			keywords = keywords.replace("\"",""); 
			return match.replace(new RegExp("("+keywords+")","gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},
		addAutoComplete:function(jqObj,jsonUrl,labelKey,valueKey)
		{  
			jqObj.autocomplete({ 
				source: function( request, response ) 
				{
					$.ajax({
						url: jsonUrl,
						dataType: "json",
						data: {
							q:jqObj.val()
						},
						success: function( data ) {							
							response( $.map( data, function( item ) {
								return {
									label:result.highlight(item[labelKey],jqObj.val()),
									value: item[valueKey ? valueKey: labelKey ]
								};
							}));
						}
					});
				},
				dataType: 'json', 
				html:true
			});
			
			var forrest=false;

			var uibox=jqObj.data('ui-autocomplete');

			// jqObj.focus(function(event) {
			// 	console.log($(this));
			// });
			// console.dir(uibox);

			uibox._renderItem = function (ul, item) {

         if(!forrest){
         	
			var p_div=jqObj.parents(".art-popup-show").eq(0);
            if(p_div.length < 1){             
               p_div=jqObj.parents(".ui-jqdialog").eq(0);
            }

           if(p_div.length > 0){
           	forrest=true;
			var __zindex=window.parseInt(p_div.css("z-index") + 1);
			ul.css("z-index",__zindex);
		   }

		}

			return $("<li>").attr("data-value", item.value).append($("<a>").html(item.label)).appendTo(ul);

			};
		}
	};

	return result;
})