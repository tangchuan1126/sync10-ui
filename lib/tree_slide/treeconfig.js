var tree_PATH=window.location.pathname;
tree_PATH=tree_PATH.replace(/Sync10-ui\/.*/,"Sync10-ui/");

requirejs.config({
　　　　paths: {
　　　　　　"jquery": tree_PATH+"bower_components/jquery/dist/jquery.min",
            "backbone": tree_PATH+"bower_components/backbone/backbone.min",
            "tree":tree_PATH+"lib/tree_slide/tree",
            "mCustomScrollbar":tree_PATH+"bower_components/mCustomScrollbar/js/jquery.mCustomScrollbar.min",
            "art_Dialog":tree_PATH+"bower_components/artDialog/src",
　　　　},
        shim : {
		'mCustomScrollbar' : {deps:['jquery']}
	 }

　　});


require(['jquery','tree','art_Dialog/dialog'], function($,tree,dialog) {
   
  

     var urlsring="json.js";
	//必选
	//Container==容器对象
	//jso_nurl ==url或[数组];

	var Container=$("#treebox");
	var zreeconfig={Container:Container,json_url:urlsring,Radio:true,Breadcrumbs:true,rows:5,lineheight:25};
	tree.int(zreeconfig);

	var Container1=$("#treeboxrsd");
	var zreeconfig1={Container:Container1,json_url:urlsring,rows:3,lineheight:25};
	tree.int(zreeconfig1);

	$("#buttontest").click(function(){	

		var d = dialog({
		    title: 'art_Dialog-tree实例',
		    content: "<div class='clearfix art_Dialogconte'><div id='dialogtree' class='treebox fl'></div><div class='fr'><ul><li>1</li><li>2</li></ul></div></div>",
		    width:600,
		    heigth:400,
		    onshow: function(){

       var treedemo=$("#dialogtree");
	  var treeobj={Container:treedemo,json_url:urlsring,rows:3,lineheight:25};
	    tree.int(treeobj);

		    }
		});
        d.showModal();


	});



});



