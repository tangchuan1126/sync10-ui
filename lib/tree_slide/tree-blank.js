/**
 *
 * @authors gaowenzhen (gaowenzhen@msn.com)
 * @date    2014-08-16 09:31:09
 * @version 0.0.1
 */

	 treeslide = {
		global: {
			setTimesr: "",
			bodydom: ".treebox",
			rows: 4,
			pageheith: 104,
			selectcolor: "#D3EB9A",
			subselctdata: {},
			bodywidthauto: null,
			slidemov: true,
			GetServerJson:[],
			GetServerurl:null,
			Radio:false
		},
		_int: function(reetconfig) {

		if (typeof reetconfig =="object") {
            
            var Container=reetconfig.Container;            
             if(Container.length < 1){
				alert("缺少容器");
				return false;
			 }

			this.treestyle(Container);

            var rows=reetconfig.rows;
            var lineheight=reetconfig.lineheight;
            var json_url=reetconfig.json_url;

			 // if(!isNaN(rows) && !isNaN(lineheight)){
				// this.global.rows = rows;
				// this.global.pageheith = (rows * lineheight) + rows;
			 // }
         

			 // var Radio=reetconfig.Radio;
			 // if(Radio){
			 // 	this.global.Radio=true;
			 // }           


			 if(json_url!="" && json_url!="undefined"){	

              if(typeof json_url ==="string"){
              	 this.GetServerJsonData(reetconfig);
              }else{

                this.treerun(reetconfig);
              
              }
			  


			 }else{
				alert("缺少jsonurl");
				return false;
			 }
            
           

			}else{
				alert("缺少配置参数");
			}

			
			

		},
		GetServerJsonData:function(reetconfig){
        var _this=this;
        _this.loding();       
        var url=reetconfig.json_url;
		$.getJSON(url, function(json){            
            _this.treerun(json,reetconfig);
           });             

		},
		loding:function(){
			//加载效果
          var lodingbody=$(this.global.bodydom);
          var loding=$(".loding");
          var z=lodingbody.css("z-index");
          var w=lodingbody.width();
          var h=lodingbody.height();
          var poff=lodingbody.offset();
          var l=poff.left;
          var r=poff.top;
          var logingstyle={"left":l+"px","top":r+"px","height":h+"px","width":w+"px","z-index":""+z+""};
          if(loding.length < 1){
          	$("<div class='loding'></div>").css(logingstyle).appendTo("body").show();
          }else{
          	loding.css(logingstyle).show();
          }

		},
		treerun:function(json,reetconfig){
	  //var reetconfig={Container:Container,json_url:urlsring,rows:5,lineheight:25};
		//生成菜单          
		  var _this=this;		 
			if(json.length > 0 && json!= null && typeof json!="undefined"){
           	//_this.global.GetServerJson=json;
           	var Radio = reetconfig.Radio;
           	var domid=reetconfig.Container;
           	if(Radio){
           		_this.Radiostyle(domid);
           	}
           	_this.noadmScrollbarJs();           	
            _this.colerul(json,reetconfig);
			_this.demotest();
			
		    }else{
		    	alert("返回json为空");
		    	return false;
		    }

		},
		Radiostyle:function(dom){

             var domid=dom.attr("id");
             var styleidstyle=domid+"_style";
             var stylelen=$("#"+styleidstyle+"").length;
             var styletxt="#"+domid+" li em{display: none}";
             if(stylelen < 1){
             $('<style id="'+styleidstyle+'" type="text/css">'+styletxt+'</style>').appendTo(dom);
             }


		},
		treestyle:function(dom){

			if ($('#treeslidestyle').length < 1) {
				var treeboxlink = '<link id="treeslidestyle" href="treebox.css" rel="stylesheet" type="text/css" />';
				$('head').append(treeboxlink);
			}
			//var bodycomde = $(this.global.bodydom);
			dom.html('<div class="body clearfix"></div><div class="foomme"></div>');

		},
		noadmScrollbarJs: function() {		
       //

			if ($('#mCustomScrollbarCss').length < 1) {
				var link = '<link id="mCustomScrollbarCss" href="/Sync10-ui/bower_components/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />';
				$('head').append(link);
			}

			if ($('#mCustomScrollbarJs').length < 1) {
				var script = '<script id="mCustomScrollbarJs" type="text/javascript" src="/Sync10-ui/bower_components/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>';
				$('body').append(script);
			}

		},
		GetTreeData: function() {
			return this.global.subselctdata;
		},
		colerul: function(datajson,reetconfig) {
			//生成一级
			//var datajson=this.global.GetServerJson;
			var _pageidsring=reetconfig.Container.attr("id");
			var ullihtml = "";			
			var rows = this.global.rows;
			if(!isNaN(parseInt(reetconfig.rows))){
             rows =parseInt(reetconfig.rows);
			}
			var _i = 1;
			for (var i = 0; i < datajson.length; i++) {
				var jsonitme = datajson[i];
				if (jsonitme.parentid == "0") {


					var sorwr = parseInt(_i % rows);

					if (sorwr == 0 && _i > 1) {
						ullihtml += "<li id='" + jsonitme.id + "'><em class='checkfalse'></em><span>" + jsonitme.title + "</span></li></ul><ul>";
					} else {
						ullihtml += "<li id='" + jsonitme.id + "'><em class='checkfalse'></em><span>" + jsonitme.title + "</span></li>";
					}

					_i++;

				}
			}

			var u = "<div class='srcoll clearfix'><ul>" + ullihtml + "</ul></div>"; 
         
           var lineheight=parseInt(reetconfig.lineheight);
           var pageh = (this.global.pageheith * rows) + rows;
			if(!isNaN(lineheight)){
             pageh= (lineheight * rows) + rows;
			}		
		   
			//var zreeconfig={Container:Container,json_url:urlsring,rows:5,lineheight:25};
			var comed = reetconfig.Container;

			//父容器宽度
			var bodywidt = comed.width();
			this.global.bodywidthauto = bodywidt;

			var ul = "<div style='height:" + pageh + "px;' id='"+_pageidsring+"_0' class='pagebox'>" + u + "</div>";
			var formmspan = "<span>prev</span><span>next</span>";

			var foomme = comed.find(".foomme");
			var foomearrt = {
				"page": _pageidsring+"_0",
				"previd": ""
			};
			$(".loding").hide();
			foomme.html(formmspan).attr(foomearrt);
			var bodyobj = comed.find(".body");
			//获取最上层父容器的宽

			//bodyobj.width(bodywidt);
			$(ul).appendTo(bodyobj);
			var _lievn = $("#"+_pageidsring+"_0 span");
			var emall = $("#"+_pageidsring+"_0 em");
			this.subliEvnet(_lievn,datajson,reetconfig);
			this.selectbtn(emall,datajson,reetconfig);
			this.spanbtn(comed,datajson,reetconfig);

		},
		selectbtn: function(emall,datajson,reetconfig) {
			var _treethis = this;

			//check勾选时
			emall.click(function() {

				var _this = $(this);
				var checktrue = _this.hasClass("checktrue");

				if (checktrue) {
					_this.removeClass("checkalltrue").removeClass("checktrue").addClass("checkfalse");
				} else {
					_this.removeClass("checkalltrue").removeClass("checkfalse").addClass("checktrue");

				}
				var p_li = _this.parents("li").eq(0);
				var liobj_id = p_li.attr("id");

				var thisid=liobj_id;

				var _pageidsring=reetconfig.Container.attr("id");
				//如是根节点就写入id
				var gitme=true;
				var root_id = p_li.parents(".pagebox").eq(0).attr("id");
				if (root_id == _pageidsring+"_0") {
					p_li.attr("rootid", liobj_id);

				} else {
					var comed =reetconfig.Container;// $(_treethis.global.bodydom);
					liobj_id = comed.find(".foomme").attr("rootid");
					gitme=false;
				}

				//点击chck时传入当前节点id
				//被点击id--thisid,是否选中状态checktrue
				//是否根节点--gitme
				_treethis.select_truedata(liobj_id,reetconfig,thisid,checktrue,gitme);


			});


		},
		select_truedata: function(rootid,reetconfig,thisid,checktrue,gitme) {

            

			//当前页的勾选状态
			var comed = reetconfig.Container;
			var _pidsirng=comed.attr("id");
			var foomme = comed.find(".foomme");
			var page = foomme.attr("page");
			var previd = foomme.attr("previd");
           

			var ulidobj = $("#" + page + "");
			var selectem = ulidobj.find("em");
			var _pid = page.replace(/\D/g, "");

            
           //如果子节点已经存在，父节点是全选，子节点根着改变
			var itme_this_obj=comed.find("#"+thisid+"");
			var chekstru=itme_this_obj.find(".checktrue");
			if(itme_this_obj.length > 0 && chekstru.length && gitme){

              for(var subi=0;subi < selectem.length;subi++){
              	var _emitme=selectem.eq(subi);
              	_emitme.removeClass("checkfalse").removeClass("checkalltrue").addClass("checktrue");
              }

			}

		   
		   var p_div_obj = $("#" + previd + "");
		   var _pliobjd = p_div_obj.find("#" + _pid + "");
		   var p_me = _pliobjd.find("em");

			if (ulidobj.find(".checkfalse").length > 0 && !gitme) {
				//如果没有选满时，上级父节点的就进入半选
				p_me.removeClass("checkfalse").removeClass("checktrue").addClass("checkalltrue");

			}else{
               //如果没有选满时，上级父节点的就进入全选
				p_me.removeClass("checkfalse").removeClass("checkalltrue").addClass("checktrue");
			}



			//取出当前勾选的
			//this.global.subselctdata=[]; 
			//cheks==false时是半选

			var selctarry = [],itmesubarry=[];
			for (var i = 0; i < selectem.length; i++) {
				var _itme_em = selectem.eq(i);
				var liitme = _itme_em.parents("li").eq(0);

				if (!_itme_em.hasClass("checkfalse")) {

					var subcheskey = false;
					if (_itme_em.hasClass("checktrue")) {
						subcheskey = true;
					}
					var idsirng = liitme.attr("id");
					selctarry.push({
						id: idsirng,
						parentid: _pid,
						cheks: subcheskey
					});

					itmesubarry.push({
						id: idsirng,
						parentid: _pid,
						cheks: subcheskey
					});

				}

			}


		
             
          //n父节点数据--遍历父节点--选中和半选的
			 var _index = ulidobj.index();
			
            //找出当前上级的半选和全选的
            var p_arryall=[];
			while (_index--) {

				var p_idsirng = comed.find(".pagebox").eq(_index);
				var pareid = p_idsirng.attr("id").replace(/\D/g, "");
				var p_ll = p_idsirng.find("li");


				for (var u = 0; u < p_ll.length; u++) {

					var itme_li = p_ll.eq(u);
					var check = itme_li.find("em");

					if (!check.hasClass("checkfalse")) {


						var liidsring = itme_li.attr("id");
						var cheskey = false;
						if (check.hasClass("checktrue")) {
							cheskey = true;
						}

                       if(liidsring==_pid){

						selctarry.push({
							id: liidsring,
							parentid: pareid,
							cheks: cheskey
						});
                     
                       //父级节点
						p_arryall.push({
							id: liidsring,
							parentid: pareid,
							cheks: cheskey
						});

					 }



					}

				}


			}

			//第一级节点的第二次


			 //查找全局缓存
             //selctarry是当前所以打勾和半选的节点		

            //如果不存在这个根结点--就新建一个根结点
			var arryli = eval("this.global.subselctdata."+_pidsirng+"_" + rootid);
			if (typeof arryli == "undefined" || arryli == "") {
				this.global.subselctdata[""+_pidsirng+"_"+ rootid + ""] = selctarry;
			}else{
                 //thisid,checktrue

                 //如果这个节点已经缓存过时
                 //过滤回不是当前的节点
                 var subarry=[];
                 //不是根结点-
                 if(!gitme){

                 //去掉勾选时-就要排除这个节点下的子
                 if(checktrue){

                 //	console.log(JSON.stringify(this.global.subselctdata));

                  for(var key in arryli){

                 	var arryitme=arryli[key];
                 	if(arryitme.parentid != thisid && arryitme.id!=thisid){
                     subarry.push(arryitme);
                 	}

                 	}

                 	//修改父节点数据-找到勾去的那个父节点，改成半选
                 	for(var subkeys in subarry){
                     var subitmes=subarry[subkeys];
                     var suitmes_id=subitmes.id;
                     if(suitmes_id==_pid){
                      subitmes.cheks=false;
                      break;                     
                     }

                 	}


                 	if(subarry.length > 0){
                 	//arryli=[];
                 	this.global.subselctdata[""+_pidsirng+"_"+ rootid + ""]=subarry;
                 	}



                 }else{

                 //新增子==勾选时添加子节点
                 //itmesubarry 
                 // console.log("上一级父节点");
                 // console.log(JSON.stringify(p_arryall));               
                     
                     //当级所有节点
                      for(var _key in itmesubarry){ 
                       var _arryliitme=itmesubarry[_key];
                       var _subid=_arryliitme.id;
                         if(thisid == _subid){                      
                          arryli.push(_arryliitme);
                           }
                        }



                     //补父节点数据 
                        var p_ge0=p_arryall[0];
                        var p_addset=false;
                          for(var pitme_key in arryli){

                          var p_itmearryli=arryli[pitme_key];
                          var arryids=p_itmearryli.id;
                            if(arryids == p_ge0.id){
                             p_addset=true;
                             break;                             
                            }

                          }

                         //不存在时
                         if(!p_addset){
                         	arryli.push(p_ge0);
                         }

                     
                     
                     // console.log("当级所有节点");
                     // console.log(JSON.stringify(arryli));  

                  }

                 }       

			}

			// console.log(JSON.stringify(this.global.subselctdata));


		},
		spanbtn: function(comed,datajson,reetconfig) {
			//左右滑动--按钮
			var _treethis = this;
			var foomme = comed.find(".foomme");
			var spanbtn = foomme.find("span");

			spanbtn.click(function() {

				var spanbtnthis = $(this);
				var slidemov = _treethis.global.slidemov;

				if (slidemov) {
					_treethis.global.slidemov = false;
					var _span = $(this);
					var p_div = _span.parents("div").eq(0);
					var bodyobj = comed.find(".body");
					var bodyleft = parseInt(bodyobj.css("left").replace(/\D/g, ""));

					if (isNaN(bodyleft)) {
						bodyleft = 0;
					}
					//往上像素
					var p_div_width = parseInt(spanbtnthis.attr("prevpx"));
					if (isNaN(p_div_width)) {
						p_div_width = 0;
					}

					//往上层
					if (_span.index() == 0) {

						var prevulid = "";
						if (bodyleft > 0) {
							bodyleft = bodyleft - p_div_width;
							//_treethis.global.slidemov=false;
							bodyobj.animate({
								left: '-' + bodyleft + 'px'
							}, "slow", function() {

								var pagesinr = p_div.attr("page"); 
								var ulobj = $("#" + pagesinr + "");
								var p_ullisring = pagesinr.replace(/\D/g, "");
								var p_check = $("#" + p_ullisring + " em");
								var checkfalse = ulobj.find(".checkfalse");
								var checkalltrue = ulobj.find(".checkalltrue");
								var ulemlen = ulobj.find("em").length;
								var alltrues = (checkfalse.length == 0 && checkalltrue.length > 0);
								if (checkfalse.length > 0 && checkfalse.length != ulemlen || alltrues) {
									p_check.removeClass("checkfalse").removeClass("checktrue").addClass("checkalltrue");
									var pid_sring=$("#"+p_ullisring+"");
									var p_pagebox=pid_sring.parents(".pagebox").eq(0).attr("id");
									var pli_sirng=p_pagebox.replace(/\D/g,"");
									var rootlisirng=$("#"+pli_sirng+"");
									rootlisirng.find("em:first").removeClass("checkfalse").removeClass("checktrue").addClass("checkalltrue");


								} else if (checkfalse.length == ulemlen) {
									p_check.removeClass("checktrue").removeClass("checkalltrue").addClass("checkfalse");

								} else {
									p_check.removeClass("checkfalse").removeClass("checkalltrue").addClass("checktrue");
								}

								var ulpve = ulobj.prev("div").eq(0);
								prevulid = ulpve.attr("id");
								p_div.attr("page", prevulid);
								ulobj.remove();
								var body = comed.find(".body");
								body.width("auto");
								_treethis.global.slidemov = true;

							});
						}



					} else {

						//进入下一层
						var nextpageid = foomme.attr("page");
						var pageobj = $("#" + nextpageid + "");
						var li = pageobj.find("li[rel=1]");
						if (li.length > 0) {
							var lispanobj = li.find("span");
							_treethis.lispanclick(lispanobj,datajson,reetconfig);
						}

					}

				}

			});

		},
		subliEvnet: function(itmeall,datajson,reetconfig) {
			
			//进入下级批量的点击
			var _this = treeslide;
			var rows = _this.global.rows;
			if(!isNaN(parseInt(reetconfig.rows))){
             rows =parseInt(reetconfig.rows);
			}

			if (itmeall.length > 0) {

				itmeall.click(function() {

					var _lithis = $(this);
					_this.lispanclick(_lithis,datajson,reetconfig);

				});

			}

		},
		lispanclick: function(_lithis,datajson,reetconfig) {
			//单个按钮点击
			var _this = treeslide;
			var rows = _this.global.rows; 			
			if(!isNaN(parseInt(reetconfig.rows))){
             rows =parseInt(reetconfig.rows);
			}

			    
      
	       
	        //当前点击的对象 
			var p_li = _lithis.parents("li").eq(0);


			var pagebox = p_li.parents(".pagebox").eq(0);
			var pagedivid = pagebox.attr("id");

			var pageindex=pagebox.index();
			var bodys = reetconfig.Container;



           // var nextidsring=pagebox.next(".pagebox").eq(0).attr("id");
           // var _nextid=nextidsring.replace(/\D/g,"");

			var gtpage=bodys.find(".pagebox:gt("+pageindex+")");
			if(gtpage.length > 0){

				for(var page_i=0;page_i < gtpage.length;page_i++){
					var pageitme=gtpage.eq(page_i);
					var pageidsring=pageitme.attr("id").replace(/\D/g,"");
                   if(p_li.attr("id")!=pageidsring){
                     pageitme.remove();
                   }
                  
				}

			}

           //如果存在就停止
           var _pageidsring=bodys.attr("id");
           var ulid_0=_pageidsring+"_0";
			var thisobjs=$("#"+_pageidsring+"_"+p_li.attr("id")+"");
            if(pagedivid == ulid_0 && thisobjs.length > 0){           
             return false;
            }


			//如果是根级-就修改rootid
			if (pagedivid == ulid_0) {
				var _foomme=bodys.find(".foomme")
				var setrootid = p_li.attr("id");
				_foomme.attr("rootid", setrootid);
				_foomme.find("span:first").attr("prevpx","");
				var gt0div = bodys.find(".pagebox:gt(0)");
				gt0div.remove();
			}		


           //如果没就创建
			if(thisobjs.length < 1 ){
           
            //获取自己的check对象
			var em = p_li.find("em");
			var check = em.hasClass("checktrue");
			var checksring = "checkfalse";
			if (check) {
				checksring = "checktrue";
			}

            //是否是半选状态
			var checkalltrue = em.hasClass("checkalltrue");

            //获取li的父容器 .srcoll
			var p_div = p_li.parents("div").eq(0);
			var ul_li_all = p_div.find("li");
			//撤销所有选中效果
			for (var li = 0; li < ul_li_all.length; li++) {
				var itme_li = ul_li_all.eq(li);
				var allstyle = {
					"background": "transparent",
					"color": "#000"
				}
				itme_li.css(allstyle).attr("rel", "");
			}

           //自己被选中效果
			var style = {
				"background": _this.global.selectcolor,
				"color": "#fff"
			};
			p_li.css(style).attr("rel", "1");
			//------------------------------------
          
            //取得自己id
			var _thisid = p_li.attr("id");
			var subli = "",
				_i = 0;


	     //单选时
			if(reetconfig.Radio){
	          _this.Radiofn(reetconfig);
			} 


		

           //取得rootid
			var rootid = bodys.find(".foomme").attr("rootid");
            //var datajson=_this.global.GetServerJson;
            //遍历总数据
			for (var i = 0; i < datajson.length; i++) {
				var subjson = datajson[i];

				//查出子节点，如果是子节点时就创建一个li
				if (_thisid == subjson.parentid) {
					var itme_id = subjson.id;
					//当在半选状态时点入时，就会去查找上次操作的记录，
					//会反回三种状态
					if (checkalltrue) {
						checksring = _this.shoucheckalltrue(rootid, itme_id,_pageidsring);
					}

                   //在遍历中取模--产生列效果
					var sorwr = parseInt(_i % rows);

					if (sorwr == 0 && _i > 0) {

						subli += "</ul><ul><li id='" + itme_id + "'><em class='" + checksring + "'></em><span>" + subjson.title + "</span></li>";

					} else {
						subli += "<li id='" + itme_id + "'><em class='" + checksring + "'></em><span>" + subjson.title + "</span></li>";
					}

					_i++;



				}
			}

            //如果有子节时
			if (subli != "" && typeof subli != "undefined") {

               //设置.page-div的高度

            var lineheight=parseInt(reetconfig.lineheight);
            var pageh = (_this.global.pageheith * rows) + rows;
			if(!isNaN(lineheight)){
             pageh= (lineheight * rows) + rows;
			}
	

				//最外层父容器宽度
				//var comed = $(_this.global.bodydom);
				//var bodywidt = bodys.width();

				//取得当前显示page宽度
				//间隔
				var marginleftconfig=20;

				var u = "<div class='srcoll clearfix'><ul>" + subli + "</ul></div>";
				var subul = "<div style='height:" + pageh + "px;margin-left:"+marginleftconfig+"px' id='"+ _pageidsring+"_"+_thisid + "' class='pagebox'>" + u + "</div>";

				var bodyobj = bodys.find(".body");

				

				//最外层宽度
				var p_div_width =bodys.width();
				// _this.global.bodywidthauto;				
               
                //填入子节点
				$(subul).appendTo(bodyobj);


				//获取所有的，.pagebox
				var bodysubdiv = bodyobj.find(".pagebox");				
				var rebodywidth = 0;
               
                //@@@@@@@@@@@
				//取出第一个pagebox的第一个ul的宽度列宽度--？这里可能有问题
				var liwidth = parseInt(bodysubdiv.eq(0).find("ul:first").width());
                
                //@@@@@@@@@@
               //计算出当前所有ul的宽度，总合，一第一个ul宽为准，好像不太对？？
				for (var subd = 0; subd < bodysubdiv.length; subd++) {
					var subdivitme = bodysubdiv.eq(subd);
					var ullens = subdivitme.find("ul").length;
					var pagewidth = ullens * liwidth;
					rebodywidth = parseInt(rebodywidth) + parseInt(pagewidth);
				}

                //给page 上级容器，.body设置宽度
                var _pagelen=bodysubdiv.length;
                var ppxs=(_pagelen * 2) + (_pagelen * marginleftconfig);
				bodyobj.width(rebodywidth + ppxs);



				//子节点绑定事件
				var _lievn = $("#"+_pageidsring+"_" + _thisid + " span");
				_this.subliEvnet(_lievn,datajson,reetconfig);
				var emall = $("#"+_pageidsring+"_" + _thisid + " em");
				_this.selectbtn(emall,datajson,reetconfig);
				
              
              //如果全选打勾时-进入下级时补数据
              var _checkalltrue = $("#"+_pageidsring+"_" + _thisid + " .checkalltrue");
              var _checkfalse = $("#"+_pageidsring+"_" + _thisid + " .checkfalse");
              if(_checkfalse.length == 0 && _checkalltrue.length == 0){
               var _chek_li=$("#"+_pageidsring+"_" + _thisid + " li");
               var slectdata=[];
			   slectdata = eval("_this.global.subselctdata."+_pageidsring+"_" + rootid);

			   for(var subli_i=0;subli_i < _chek_li.length;subli_i++){
                 var subli_itmes=_chek_li.eq(subli_i);
                 var subli_sirng=subli_itmes.attr("id");
                 slectdata.push({
							id: subli_sirng,
							parentid: _thisid,
							cheks: true
						});
			   }

			   // console.log(JSON.stringify(_this.global.subselctdata));
               
              }

				//-------------------------

             //====================如下开始处理滑屏=========================



                 //获取左右上一级下一级，容器对象
				var spanbtn = bodys.find(".foomme");


				//获取当前最后显示的容器--宽度---最后一个，也就点击后生成的那个
				var showpageidsring = spanbtn.attr("page");
				var showobj = $("#" + showpageidsring + "");
				//最后一个显示的宽度--二级或n级新生成的page宽度
				var showobj_w = parseInt(showobj.width());
				if(isNaN(showobj_w)){
                 showobj_w=0;
				}

				//showobj当前点击的节点---pagebox对象,

				//找出当前屏，的宽度（要除去最后新生产的）
				//当前屏显示是两个page拼起来的
               

               //=====点击是的上一屏========
                //当前节点级索引号

				//var pageindex=pageindex;
                //往上级找去
                var prevpagewidth=0;
				for(var page_i=pageindex;page_i > -1; page_i --){
                   var pageitme=bodysubdiv.eq(page_i);
                   var pageitmewidth=parseInt(pageitme.width());
                   var marginleft=parseInt(pageitme.css("margin-left").replace(/\D/g,""));
                   if(isNaN(marginleft)){
                   marginleft=0;
                   }

                    //小于或等于最外层容器就加上
                    if(prevpagewidth <= p_div_width){
                     prevpagewidth=(prevpagewidth + pageitmewidth) + marginleft;
                    }
                   //如果大于或等于就认为是是整屏
                   if(prevpagewidth >= p_div_width){

                   	break;
                   }

				}

                //得到滑动数
				//console.log(prevpagewidth);

                

              

                //给srcoll设置宽度---也是求出下一屏宽度
				//未来总共列数的宽度'ulid_" + _thisid + "'
				var newdivobj = $("#"+_pageidsring+"_" + _thisid + "");
				var srcollobj = newdivobj.find(".srcoll");
				var srcollullen = srcollobj.find("ul").length;
				var srcollwidths = parseInt(srcollullen) * liwidth;
				srcollobj.width(srcollwidths);

				//showobj_w是当前被点击的pagebox的宽度
				//刚产生的宽度
				var sortw = newdivobj.width();

				var allsoillwidth=sortw + showobj_w;  

              //剩下的宽度
				var swidth= p_div_width - prevpagewidth;

                
                //要滚屏
               if(allsoillwidth >= p_div_width || sortw > swidth){
               	 //够一屏，或超出就删除margin-left
               	  newdivobj.css("margin-left","0px");
                   spanbtn.show();
                   //取得body最新的left
					var bodyleft = parseInt(bodyobj.css("left").replace(/\D/g, ""));
					if (isNaN(bodyleft) || typeof bodyleft =="undefined" || bodyleft==null) {
						bodyleft = 0;
					}
					bodyleft = bodyleft + prevpagewidth;
					spanbtn.find("span:first").attr("prevpx", bodyleft);
					_this.global.slidemov = false;
					bodyobj.animate({
						left: '-' + bodyleft + 'px'
					}, "slow", function() {
						_this.global.slidemov = true;
					});



                 }else {
					spanbtn.hide();
				}

  
                //绑定滚动事件
                if(srcollwidths > p_div_width){

					newdivobj.width(p_div_width);
					newdivobj.mCustomScrollbar({
						axis: "x",
						scrollButtons: {
							enable: true
						},
						theme: "rounded"
					});

				}
				


				//写入当前页属性
				var foommearrt = {
					"page": _pageidsring+"_" + _thisid,
					"previd": pagedivid
				};
				spanbtn.attr(foommearrt);


			}

			
            

         }


		},
		shoucheckalltrue:function(rootid,id,_pageidsring){
          //半选状态进入时，取回上次操作状态
         var _this = treeslide;
			  //console.log(id);
              
              var slectdata=[];
			  slectdata = eval("_this.global.subselctdata."+_pageidsring+"_" + rootid);

			// console.log(JSON.stringify(slectdata));

				//缓存已有选中

				var _checksring = "checkfalse";

				for (var y = 0; y < slectdata.length; y++) {
					var selecitme = slectdata[y];
					if (id == selecitme.id) {

						if (selecitme.cheks) {
							_checksring = "checktrue";
						} else {
							_checksring = "checkalltrue";
						}

						break;
					}

				}

				return _checksring;


         
		},
		Radiofn:function(reetconfig){
			var dom=reetconfig.Container;
			var domidsoring=dom.attr("id");
			var all_li_rel=dom.find("li[rel=1]");
			var subarrydata=this.global.subselctdata;

			var subarrys = eval("this.global.subselctdata."+domidsoring);
			if (typeof subarrys != "undefined" || subarrys != "") {
				subarrys=[];				
			}else{
                 subarrys=subarrydata[""+domidsoring+""]=[];
			}
			
			for(var key=0;key < all_li_rel.length;key++){
				liitme=all_li_rel.eq(key);
				var thisid=liitme.attr("id");
				var pratidsring=liitme.parents(".pagebox").eq(0).attr("id").replace(/\D/g,"");
				subarrys.push({
							id: thisid,
							parentid: pratidsring,
							cheks: true
						});

			}

			console.log(JSON.stringify(subarrys));

		},
		demotest: function() {
			var _this = this;
			var buttontest = $("#buttontest");
			if (buttontest.length > 0) {

				buttontest.click(function() {

					var page = parseInt($("#page").val());
					if (!isNaN(page)) {

						var lineheight = parseInt($(".pagebox li:first").height());
						

							//行数：rows,行高：lineheight
	var urlsring="http://192.168.1.69/Sync10/administrator/product/ct_product_catalog_json.html";
	
	//必选
	//Container==容器对象
	//jso_nurl ==url或[数组];

	var Container=$("#treebox");
	Container.empty().height("auto");
	var zreeconfig={Container:Container,json_url:urlsring,rows:page,lineheight:25};

						_this._int(zreeconfig);

					}

				});


			}

		}

	}


if ( typeof define === "function" && define.amd ) {

   define(["jquery"], function($) { 

      return treeslide;
    });

} else { 
 
(function($) { 

  window.treeslide = treeslide ||{};

 })(jQuery);

  
}

