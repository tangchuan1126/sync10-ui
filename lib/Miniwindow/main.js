 requirejs(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

     requirejs(["oso.lib/nprogress/nprogress.min", "jquery", "jquery.browser", "require_css!oso.lib/nprogress/nprogress", "require_css!Font-Awesome","require_css!bootstrap-css/bootstrap.min","require_css!oso.lib/bootstrap/css/bootstrap_input_icos.css"],
         function(NProgress, $) {


             $("body").show();

             NProgress.start();         



             var cssfileall = [
                 "oso.lib/CodeMirror/mode/javascript/javascript",
                 "oso.lib/CodeMirror/lib/util/foldcode",
                 "require_css!oso.lib/CodeMirror/lib/codemirror.css",
                 "require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css",
                 "bootstrap"
             ];

             requirejs(cssfileall, function() {



                 requirejs(['oso.lib/Miniwindow/webim_win'], function(webim_win) {

                     $(function() {

                         var pah_ = "/Sync10/action/pushMessageByOpenfire/";

                         

                         var crewin = {

                             renderTo: "body",
                             //用户列表
                             dataUrl: pah_ + "PushMessageByOpenfireAction.action",
                             postData: {
                                 Method: "AquireContacts"
                             },
                             //监听当前用户所有消息
                             ListeningmsgData: {
                                 Method: "RoundRobin"
                             },
                             //当前用户发起消息
                             AmessageData: {
                                 Method: "SendMsg"
                             },
                             scrollH: 480,
                             refreshdata: 1000, //毫秒，刷新
                             openbtntxt: "私信聊天",
                             fileservpath:"/Sync10/_fileserv/upload"
                         }

                         var win = new webim_win(crewin);


                         NProgress.done();

    
    


                     });
                     // window onlonad end

                 });
                 //main end 


             });
             // codemirror end      

         });
     // requirejs_config end
 });
 // nprogress end