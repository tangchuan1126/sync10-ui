define(["jquery", "ztree", "ztree_exhide", "underscore", "mCustomScrollbar", "domready!"], function($, ztree) {

    var body_width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

    var treeclass = {
        _global: {},
        _default: function() {
            return {
                Asyncopentimeout:null,
                treeError: null,
                treeclick: null,
                JSONData: null,
                KeyName: null,
                Numberofrequests: 0,
                Async: false,
                Asynselectswitch: false,
                zTreeitmeclick: false,
                multiselect: false,
                Parentclick: false, //父节点不能操作
                selectpost: true, //是否请求默认选择路径状态
                selectdft: false, //选择路径请求当中
                defaultselectpath: null, //选择路径数据
                mouseleave: true //鼠标移出事件
            }

        },
        setTim: function() {
            return {
                zTree_box_settime: "",
                timeoutsring: "",
                ztree_show: false
            }
        },
        _int: function(obj) {

            var _this = treeclass;
            _this.style();

            if (obj.renderTo) {

                _this._intprototype(obj);

            }

        },
        _intprototype: function(obj) {

            var _this = treeclass;
            var _intfun = _this._int;
            var _def = new _this._default();
            var _tim = new _this.setTim();
            var _allobj = _.extend(_tim, _def);
            var classid = obj.renderTo;

            _this._global[classid] = _.extend(_allobj, obj);

            _intfun.prototype.render = function() {

                _this.render(classid);

            };

            _intfun.prototype.value = function() {

                return _this._value(classid);

            };

            _intfun.prototype.on = function(eventsname, cllblock) {

                _this.events(eventsname, cllblock, classid);

            };

            _intfun.prototype.reselect_id = function(selectid, cllblock) {

                var dfts = _this._global[classid];
                dfts.selectid = selectid;

                treeclass._resetselectid(dfts);
                // if(cllblock){
                //   cllblock();
                // }

                // console.log(dfts);
                //console.log(selectid);

            };



        },
        style: function() {



            var cssfileall = [
                "require_css!bootstrap-css/bootstrap.min",
                "require_css!Font-Awesome",
                "require_css!oso.lib/immybox-master/immybox.css",
                "require_css!ztree-css",
                "require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css",
                "require_css!oso.lib/bootstrap/css/VisiStyle.css"
            ];

            require(cssfileall);


        },
        Dombox: function(classid) {

            var _this = treeclass;
            var _def = _this._global[classid];

            var renderTo = _def._Dom;
            var d = new Date();

            var placeholder = "Search...";
            if (_def.placeholder) {
                placeholder = _def.placeholder;
            }
            var Pleaseselect = "";
            if (_def.Pleaseselect && typeof _def.Pleaseselect != "undefined") {
                Pleaseselect = '<ul class="Pleaseselect"><li>' + _def.Pleaseselect + '</li></ul>';
            }


            var htmlsring = "<div class='treecontrolsbox'><div class='form-control'><div class='selectbox'><input class='Searchinput' type='text' placeholder='" + placeholder + "'/></div></div><div class='ztree_box_'><div class='Scrollbar_box'>" + Pleaseselect + "<ul class='ztree' id='tree_" + d.getTime() + "'></ul></div></div></div>";

           
            if (renderTo[0].nodeName == "INPUT") {

                htmlsring = "<div class='treecontrolsbox border1px'><div class='asyntopwidth'><div class='input-group'><input class='Searchinput form-control' type='text' placeholder='" + placeholder + "'/><div class='input-group-addon'><i class='fa fa-search fa-fw'></i></div></div></div><div class='ztree_box_ noborder'><div class='Scrollbar_box'>" + Pleaseselect + "<ul class='ztree' id='tree_" + d.getTime() + "'></ul></div></div></div>";

                renderTo.after(htmlsring);
                var treeboxall = renderTo.next(".treecontrolsbox").eq(0);



                //var clicktitme="";
                renderTo.click(function() {
                    treeboxall.show();
                    treeclass.show_ztreebox(_def);
                    var _thisvaltxt = $.trim($(this).val());
                    if (_thisvaltxt != "" && typeof _thisvaltxt != "") {
                        treeclass.reinputwidth(_def);
                    } else {


                        if (renderTo.outerWidth() > treeboxall.outerWidth()) {
                            treeboxall.outerWidth(renderTo.outerWidth());
                        }

                    }

                }).focusout(function() {

                    if (typeof _def.timeoutsring != "undefined" && _def.timeoutsring != "") {
                        clearTimeout(_def.timeoutsring);
                    }

                    _def.timeoutsring = setTimeout(function() {
                        treeboxall.hide();
                        _this.unLockingparScrollbar();
                    }, 300);

                    console.log(13);

                });

                //renderTo.prop("disabled",true);    

                treeboxall.height("auto").hide();
                _this.unLockingparScrollbar();


            } else {


                var tree_w = renderTo.width();
                _def.renderTo_w = tree_w;
                renderTo.html(htmlsring);
                var treecontrolsbox = renderTo.find(".treecontrolsbox");
                treecontrolsbox.width(tree_w);
                renderTo.height(36).css("position", "relative");


            }


            _this.inputevntes(classid);


        },
        show_ztreebox: function(_def) {

            //var _def = treeclass._default;
            var renderTo = _def._Dom;
            var _inputh = 0;
            var treebox = renderTo,
                treecontrolsbox = renderTo.find(".treecontrolsbox");

            if (renderTo[0].nodeName == "INPUT") {
                treebox = renderTo.next(".treecontrolsbox").eq(0);
                treecontrolsbox = treebox;
                _inputh = renderTo.outerHeight();
            }

            var ztree_box_ = treebox.find(".ztree_box_");
            var form_control = treebox.find(".form-control");

            var zTree_box_settime = _def.zTree_box_settime;

            if (zTree_box_settime != "" && typeof zTree_box_settime != "undefined") {
                window.clearTimeout(zTree_box_settime);
            }

            var p_posi = renderTo.position();
            //var treecontrolsbox = renderTo.find(".treecontrolsbox");

            var setwindt = treecontrolsbox.data("setwindt");

            var p_w = renderTo.width();
            if (p_w < 200) {
                p_w = 200
            }

            if (setwindt = "set" && renderTo[0].nodeName != "INPUT") {

                p_w = treecontrolsbox.width();

            }


            var _top = p_posi.top + _inputh;
            var _left = p_posi.left;


            treecontrolsbox.css({
                "top": _top + "px",
                "left": _left + "px",
                "width": p_w + "px",
                "overflow": "initial"
            });


            //renderTo.height(500);
            if (renderTo[0].nodeName != "INPUT") {

                form_control.addClass("bottomradius0");
            } else {
                if(!_def.Async)
                form_control.val("");
            }

            ztree_box_.show();

            treeclass.LockingparScrollbar();

            treeclass.resetoffste(_def, treecontrolsbox);


        },
        LockingparScrollbar: function() {

            $(window).on('scroll touchmove mousewheel', function(e) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            });

        },
        unLockingparScrollbar: function() {

            $(window).off('scroll touchmove mousewheel');

        },
        treecontrolsboxstatic: function(classid) {

            var _this = treeclass;
            var _def = _this._global[classid];

            if (_def.mouseleave) {

                var zTree_box_settime = _def.zTree_box_settime;
                var renderTo = _def._Dom;

                var treebox = renderTo;
                var ztree_box_ = treebox.find(".ztree_box_");
                if (renderTo[0].nodeName == "INPUT") {
                    treebox = renderTo.next(".treecontrolsbox").eq(0);
                    ztree_box_ = treebox;
                }



                if (zTree_box_settime != "" && typeof zTree_box_settime != "undefined") {
                    window.clearTimeout(zTree_box_settime);
                }

                treebox.find(".treecontrolsbox").css({
                    "position": "static",
                    "top": "0px",
                    "left": "0px",
                    "width": _def.renderTo_w + "px",
                    "overflow": "hidden"
                });

                ztree_box_.hide();
                _this.unLockingparScrollbar();

                if (renderTo[0].nodeName != "INPUT") {
                    var _form_control = treebox.find(".form-control");
                    _form_control.removeClass("bottomradius0");
                }

                var _Searchinput = treebox.find(".Searchinput");
                var _val = $.trim(_Searchinput.val());


                if (_val == "" || typeof _val == "undefined") {
                    renderTo.removeData(["name", "val"]).attr("title", "");
                }

            }


            return false;

        },
        inputevntes: function(classid) {

            var _this = treeclass;
            var _def = _this._global[classid];

            var renderTo = _def._Dom;

            var treebox = renderTo,
                _treecontrolsbox = renderTo.find(".treecontrolsbox");
            if (renderTo[0].nodeName == "INPUT") {
                treebox = renderTo.next(".treecontrolsbox").eq(0);
                _treecontrolsbox = treebox;

            }


            var __input = treebox.find(".Searchinput").eq(0);

            var ztreebox = treebox.find(".ztree");
            var idsring = ztreebox.attr("id");
            var ztree_box_ = treebox.find(".ztree_box_");
            // var _treecontrolsbox = renderTo.find(".treecontrolsbox")

            var Pleaseselect = _treecontrolsbox.find(".Pleaseselect");

            Pleaseselect.click(function() {

                if (renderTo[0].nodeName != "INPUT") {
                    console.log(444);
                    __input.val("").attr("data-val", "").attr("data-name", "");
                } else {
                    console.log(555);
                    renderTo.val("").attr("data-val", "").attr("data-name", "");
                }
               
                treebox.hide();
                _this.unLockingparScrollbar();

                if (_def.Pleaseselectfn) {
                    _def.Pleaseselectfn(renderTo);
                }



            });



            var zTree_box_settime = _def.zTree_box_settime;

            //树的外容器
            if (renderTo[0].nodeName != "INPUT") {

                ztree_box_.hover(function() {

                    _def.ztree_show = true;

                    treeclass.show_ztreebox(_def);

                }, function() {

                    _def.ztree_show = false;

                    hide_ztreebox();

                });


            } else {
               

                _treecontrolsbox.hover(function() {

                     if(_def.Asyncopentimeout!="" && typeof _def.Asyncopentimeout!="undefined")
                        clearTimeout(_def.Asyncopentimeout);

                    _def.ztree_show = true; 

                    treeclass.show_ztreebox(_def);

                }, function() {

                    if(_def.Asyncopentimeout!="" && typeof _def.Asyncopentimeout!="undefined")
                        clearTimeout(_def.Asyncopentimeout);

                    _def.Asyncopentimeout=setTimeout(function(){
                    _def.ztree_show = false;                    
                    hide_ztreebox();
                    },400);


                });


            }




            //整体的外容器

            _treecontrolsbox.mouseleave(function() {

                //###      
                if (renderTo[0].nodeName != "INPUT") {
                    var ztree_box_ = treebox.find(".ztree_box_");
                    if (ztree_box_.is(":visible")) {
                        _def.ztree_show = false;

                        hide_ztreebox();
                    }

                    _def.mouseleave = true;
                } // else {

                // hide_ztreebox();

                //}


            });

            _treecontrolsbox.scroll(function(e) {

                e = e || window.event;
                if (e && e.preventDefault) {
                    e.preventDefault();
                    e.stopPropagation();
                } else {
                    e.returnvalue = false;
                    return false;
                }

            });


            function hide_ztreebox() {

                if (zTree_box_settime != "" && typeof zTree_box_settime != "undefined") {
                    window.clearTimeout(zTree_box_settime);
                }



                if (!_def.ztree_show) {

                    zTree_box_settime = setTimeout(function() {

                        _this.treecontrolsboxstatic(classid);

                    }, 400);

                }

            }

            if (renderTo[0].nodeName != "INPUT") {

                _treecontrolsbox.click(function(e) {

                    $(".ztree_box_").hide();
                    _this.unLockingparScrollbar();
                    //树节点点击时阻止
                    if (!_def.zTreeitmeclick)
                        _this.show_ztreebox(_def);

                    _def.zTreeitmeclick = false;

                    _this.defaultselectitme(idsring, _def);

                });

            } else {

                _treecontrolsbox.click(function(e) {

                    if (typeof _def.timeoutsring != "undefined" && _def.timeoutsring != "") {
                        clearTimeout(_def.timeoutsring);
                    }
                });


            };



            __input.click(function() {

                if (_def.timeoutsring != "" && typeof _def.timeoutsring != "undefined") {
                    window.clearTimeout(_def.timeoutsring);
                }

                //选中input框内文本
                if (this.value != "" && typeof this.value != "undefined") {
                    this.setSelectionRange(0, this.value.length);
                }

                //异步时，查找默认选中的
                _this.defaultselectitme(idsring, _def);

                //重置下拉select的宽度
                _this.reinputwidth(_def);

            }).on('input', function(e) {

                _def.ztree_show = false;

                var _thisinput = $(this);

                if (renderTo[0].nodeName != "INPUT") {
                    //打开input一整漂浮
                    _this.show_ztreebox(_def);
                }


                var treeObj = _def._treeObj;

                var timeoutsring = _def.timeoutsring;

                var _val = $.trim(_thisinput.val());
                _val = _val.replace(/\/|<|>/g, "");



                if (timeoutsring != "" && typeof timeoutsring != "undefined") {
                    window.clearTimeout(timeoutsring);
                }


                if (_val != "" && typeof _val != "undefined") {


                   timeoutsring = setTimeout(function() {
                        treeclass.searchtree(classid, _val);
                       // console.log(22);                                             
                        _thisinput.focus();

                    }, 400);


                } else {

                    if (renderTo[0].nodeName != "INPUT"){                

                                 
                        _this.treecontrolsboxstatic(classid); 

                    }

                    treeclass.hideNodesall(classid, e);

                }



            }).focusout(function(e) {

                if (renderTo[0].nodeName != "INPUT") {
                    hide_ztreebox();
                }

            }).keyup(function(e) {

                var _thisinput = $(this);
                var _val = $.trim(_thisinput.val());

                if (e.keyCode == 8) {

                    e = e || window.event;
                    if (e && e.preventDefault) {
                        e.preventDefault();
                        e.stopPropagation();
                    } else {
                        e.returnvalue = false;
                        return false;
                    }

                }

                if (e.keyCode == 8 && _val == "" || _val == "undefined") {

                    var timeoutsring = _def.timeoutsring;
                    if (timeoutsring != "" && typeof timeoutsring != "undefined") {
                        window.clearTimeout(timeoutsring);
                    }


                }


            });

        },
        reinputwidth: function(_def) {
            //重置宽度
            var renderTo = _def._Dom;
            var boxwidth = renderTo.outerWidth();

            var treebox = renderTo,
                _treecontrolsbox = renderTo.find(".treecontrolsbox");
            var INPUT = (renderTo[0].nodeName == "INPUT");


            if (INPUT) {

                treebox = renderTo.next(".treecontrolsbox").eq(0);
                _treecontrolsbox = treebox;

                if (boxwidth > _treecontrolsbox.outerWidth()) {
                    treebox.width(boxwidth);
                    treeclass.resetoffste(_def, treebox);
                }


            }



            var zTreeulbox = treebox.find(".ztree");

            var center_open = zTreeulbox.find(".root_open");
            if (center_open.length < 1) {
                center_open = zTreeulbox.find(".center_open");
            }



            if (center_open.length > 0 && boxwidth < 620) {

                setTimeout(function() {

                    var center_openallobj = [];
                    for (var span = 0; span < center_open.length; span++) {
                        var root_open = center_open.eq(span);
                        var p_li_open = root_open.parents("li").eq(0);
                        var center_ul_len = p_li_open.find('.center_open');
                        center_openallobj.push({
                            center_open: p_li_open,
                            ullen: center_ul_len.length
                        });
                    }


                    //取出打开最多节点的那个
                    var subitmeobj = _.max(center_openallobj, function(center_itme_) {
                        return center_itme_.ullen
                    });

                    var open_li = subitmeobj.center_open.find('.center_open');
                    if (open_li.length < 1) {
                        open_li = subitmeobj.center_open.find('.bottom_open');
                    }

                    var p_ul_li_a = "";

                    if (open_li.length > 1) {
                        p_ul_li_a = subitmeobj.center_open;
                    } else {
                        p_ul_li_a = open_li.parents("li").eq(0);
                    }


                    var sub_aobj = p_ul_li_a.find("a");

                    var aarry = [];
                    for (var _a_i = 0; _a_i < sub_aobj.length; _a_i++) {
                        var suba = sub_aobj.eq(_a_i);
                        aarry.push(suba.width());
                    }

                    var maxwid = _.max(aarry);

                    if (maxwid > 0) {

                        var itme_w = window.parseInt(maxwid) + 100;

                        if (boxwidth > itme_w) {
                            itme_w = boxwidth;
                        }

                        //var treeIdp_w = _def._Dom.width();
                        if (itme_w > boxwidth) {
                            _treecontrolsbox.width(itme_w).data("setwindt", "set");
                        }
                    }

                    treeclass.resetoffste(_def, _treecontrolsbox);

                }, 50);

            } else {

                var __all_w = [];
                var all_li_a = zTreeulbox.find("li a");
                for (var li_a_ = 0; li_a_ < all_li_a.length; li_a_++) {
                    var __a = all_li_a.eq(li_a_);
                    __all_w.push(__a.width());
                }

                var maxwid = _.max(__all_w);

                var itme_w = window.parseInt(maxwid) + 100;

                if (itme_w > boxwidth) {
                    _treecontrolsbox.width(itme_w).data("setwindt", "set");
                }
                treeclass.resetoffste(_def, _treecontrolsbox);

            }



        },
        resetoffste: function(_def, treecontrolsbox) {
            //靠右对齐   
            var renderTo = _def._Dom;
            //var treecontrolsbox=$(".treecontrolsbox");
            var Align = _def.ztreebox_Align;
            var inoost = renderTo.position();
            var _left = inoost.left;
            if (Align == "right") {
                //input--0到右边线
                var rightleft = renderTo[0].clientWidth + _left;
                //左边+弹窗w
                var wrs = _left + treecontrolsbox.width();
                if (wrs > rightleft) {
                    _left = _left - (wrs - rightleft);

                    treecontrolsbox.css("left", _left + "px");

                }

            }

        },
        hideNodesall: function(classid, e) {

            var _this = treeclass;
            var _def = _this._global[classid];
            var renderTo = _def._Dom;

            var treebox = renderTo;
            if (renderTo[0].nodeName == "INPUT") {
                treebox = renderTo.next(".treecontrolsbox").eq(0);
            }

            var ztreebox = treebox.find(".ztree");
            var idsring = ztreebox.attr("id");
            var _thisinput = treebox.find(".Searchinput").eq(0);
            var treeObj = _def._treeObj;


            if (_def.multiselect) {
                treeObj.checkAllNodes(false);
            }

            _def.ztree_show = true;
            //##################树抖动问题还在1
            var nodes_ = treeObj.transformToArray(treeObj.getNodesByParam("isHidden", true));
            treeObj.showNodes(nodes_);

            var nodesall = treeObj.transformToArray(treeObj.getNodes());
             
             
           if(nodesall.length > 0){

            _.map(nodesall, function(allitmeval, allkey) {



                if (allitmeval.isParent) {
                   //会造成上跳动
                    treeObj.expandNode(allitmeval, false, null, null);
                    treeObj.showNodes(allitmeval);
                }

                if ((nodesall.length - 1) == allkey && renderTo[0].nodeName != "INPUT") {
                    console.log(444);
                    _thisinput.focus();
                    return false;                

                } else {

                    //2015-06-15

                    //#######################树抖动问题还在--
                    //如下处理滚动条跳动问题

                    if((nodesall.length - 1) == allkey){

                    if ((e == "undefined" || e == null && !e ) && !_def.Async){                      
                      renderTo.focus();
                    }else{
                        _thisinput.focus();
                    }

                  }

                    return false;

                }


            });

         }

            treeclass.show_ztreebox(_def);



        },
        searchtree: function(classid, _val) {

            var _this = treeclass;
            var _def = _this._global[classid];
            var renderTo = _def._Dom;

            var treebox = renderTo;
            if (renderTo[0].nodeName == "INPUT") {
                treebox = renderTo.next(".treecontrolsbox").eq(0);
            }


            var ztreebox = treebox.find(".ztree");
            var idsring = ztreebox.attr("id");
            var _thisinput = treebox.find(".Searchinput").eq(0);
            var treeObj = _def._treeObj;


            if (_def.multiselect) {
                
                treeObj.checkAllNodes(false);
            }

            _def.ztree_show = true;
            //得到搜索后结果

            var nodeList = treeObj.transformToArray(treeObj.getNodesByParamFuzzy("name", _val, null));


            if (nodeList.length > 0) {
                //本地有结果时
                treeclass.searchture(idsring, nodeList, _val, _def);

            } else {

                // var _def = treeclass._default;

                if (!_def.Async) {

                    var nodesall = treeObj.transformToArray(treeObj.getNodes());
                    treeObj.hideNodes(nodesall);

                    return false;
                } else {
                    //异步树--路线
                    //重新去服务端拉取数据
                    var postval = {
                        key: _val
                    };
                    if(_def.Asyncopentimeout!=null && typeof _def.Asyncopentimeout!="undefined"){
                        clearTimeout(_def.Asyncopentimeout);
                    }
                    treeclass.searchfalse(idsring, postval, _def);
                }

            }



        },
        searchture: function(idsring, nodeList, _val, _def) {

            //全部隐藏
            var treeObj = _def._treeObj;

            var nodesall = treeObj.transformToArray(treeObj.getNodes());



            treeObj.hideNodes(nodesall);

            var _searchval = new RegExp(_val, 'i');

            //遍历结果
            _.map(nodeList, function(nodesdli, subkey) {


                //n上级递归

                showparentnode(nodesdli, _def);

                if (nodesdli.isParent) {
                    treeObj.expandNode(nodesdli, true, null, null);
                    if (nodesdli.children) {
                        treeObj.hideNodes(nodesdli.children);
                    }
                }

                //显示出匹配上的具体itme
                var __name = nodesdli.name;
                if (_searchval.test(__name)) {

                    treeObj.showNode(nodesdli);
                }

                // treeObj.showNode(nodesdli);


            });


            //向上查找父节点递归---显示
            function showparentnode(pnode, _def) {


                var pare_node_ = pnode.getParentNode();
                if (pare_node_) {

                    if (pare_node_.isHidden) {
                        //显示父节点
                        //var __pretname=pare_node_.name;
                        // if(_searchval.test(__pretname)){
                        treeObj.showNode(pare_node_);
                        //}

                        //打开自己父节点
                        treeObj.expandNode(pare_node_, true, null, null);
                        //查找上级父节点                     
                        var p_nodes = pare_node_.getParentNode();

                        if (p_nodes) {


                            showparentnode(pare_node_, _def);

                        }


                    }

                }

                treeclass.inputfocus("", _def);


            }

            treeclass.reinputwidth(_def);


        },
        inputfocus: function(readonly, _def) {

            //var _def = treeclass._default;
            var renderTo = _def._Dom;

            var treebox = renderTo;
            if (renderTo[0].nodeName == "INPUT") {
                treebox = renderTo.next(".treecontrolsbox").eq(0);
            }


            var input = treebox.find(".Searchinput");

            if (readonly != "" && typeof readonly != "undefined" || !readonly) {

                if (readonly)
                    input.prop("readonly", true);
                else
                    input.prop("readonly", false);

            }

            //内部搜索框
           if(!_def.Async)
            input.focus();

            // treeclass.show_ztreebox();

        },
        searchfalse: function(idsring, _val, _def) {


            //重新服务端拉取
            // var _def = treeclass._default;

            _def.Numberofrequests = -1;

            // if($.isArray(_def.dataUrl) && _def.dataUrl.length > 0){

            //     addjsondateset(_def.dataUrl);
            // }

            //  if(typeof _def.dataUrl == "string" && typeof _def.dataUrl!="undefined" && _def.dataUrl!=""){

            $.getJSON(_def.dataUrl, _val, function(JSONData) {


                addjsondateset(JSONData);

            }).error(function(e) {

                if (_def.treeError) {
                    _def.treeError(e);
                }

            });

            // }



            function addjsondateset(JSONData) {


                //现有的数据
                var treeObj = _def._treeObj;
                var _nodes = treeObj.transformToArray(treeObj.getNodes());


                //搜索服务端后临时数据
                if (JSONData.length > 0) {

                    console.log(11111);

                    //pid 父节点更新
                    var allpid = [];

                    for (var jsonkey in JSONData) {
                        var _jsonitme = JSONData[jsonkey];
                        if (_jsonitme.pId) {
                            allpid.push(_jsonitme.pId);
                        }
                    }

                    allpid = _.uniq(allpid);

                    if (allpid.length > 0) {

                        _def.Numberofrequests = allpid.length;

                        for (var nodeikey in _nodes) {

                            var nodeiitme = _nodes[nodeikey];

                            for (var allpkey in allpid) {
                                var _pids = allpid[allpkey];
                                //加载子节点
                                if (_pids == nodeiitme.id && !nodeiitme.children) {
                                    _def.JSONData = null;
                                    treeObj.reAsyncChildNodes(nodeiitme, "add", true);
                                    _def.JSONData = JSONData;

                                    _def.KeyName = _val.key;

                                    treeclass.inputfocus(true, _def);

                                    break;
                                }
                            }
                        }


                    }

                    // end pid   
                    // subitme --> zTreeOnAsyncSuccess

                } else {

                    
               
               var timeoutsring = _def.timeoutsring;
                if (timeoutsring != "" && typeof timeoutsring != "undefined") {
                    window.clearTimeout(timeoutsring);
                }



                    //触发顶级--找不到时加载父节点
                    _def.KeyName = _val.key;
                    _def.JSONData = null;
                    for (var pretkey in _nodes) {

                        var prtitmenode = _nodes[pretkey];
                        var pnames = prtitmenode.name;
                        if (pnames != "" && typeof pnames == "string" && !prtitmenode.children && prtitmenode.isParent) {
                       
                            treeObj.reAsyncChildNodes(prtitmenode, "add", true);
                            _def.Numberofrequests++;

                        }

                    }


                    if (_def.Numberofrequests == -1) {

                        treeObj.hideNodes(_nodes);
                        return false;

                    }            
             


                }

             

            }



        },
        render: function(classid) {

            $(function() {

                var _this = treeclass;
                var _def = _this._global[classid];

                if(typeof _def.renderTo =="string")
                _def._Dom = $(_def.renderTo);

                _this.Dombox(classid);

                if (!_def.Async && _def.dataUrl) {

                    var _PostData = {};
                    if (_def.PostData) {
                        _PostData = _def.PostData;
                    }


                    if ($.isArray(_def.dataUrl) && _def.dataUrl.length > 0 && !_def.Async) {
                      
                        _def.JSONData = _def.dataUrl;
                        _this.createtree(classid);
                    }

                    if (typeof _def.dataUrl == "string" && typeof _def.dataUrl != "undefined" && _def.dataUrl != "") {



                        $.getJSON(_def.dataUrl, _PostData, function(_JSONData) {
                            //存储临时数据
                            _def.JSONData = _JSONData;

                            _this.createtree(classid);

                        }).error(function(e) {

                            if (_this.treeError) {
                                _this.treeError(e);
                            }

                        });

                    }

                }

                if (_def.Async && _def.dataUrl) {

                    _this.createtree(classid);

                }

            });

        },
        _value: function(classid) {

            var _this = treeclass;

            var _def = _this._global[classid];

            var renderTo = _def._Dom;


            // var treebox=renderTo;
            // if(renderTo[0].nodeName == "INPUT"){
            //   treebox=renderTo.next(".treecontrolsbox").eq(0);        
            // }

            if (!_def.multiselect) {

                var _name = renderTo.data("name");
                var _val = renderTo.data("val");

                return {
                    name: _name,
                    val: _val
                };

            } else {

                var subitme = [];

                var treeObj = _def._treeObj;
                var nodes = treeObj.getChangeCheckedNodes();



                for (var key in nodes) {
                    var itme = nodes[key];
                    if (itme.name && itme.data) {
                        subitme.push({
                            name: itme.name,
                            val: itme.data
                        });
                    }

                }


                return subitme;

            }


        },
        _resetselectid: function(_def, classid) {


            if (_def.selectid) {

                var treeObj = _def._treeObj;

                var selectnodesall = treeObj.transformToArray(treeObj.getNodes());

                for (var keyselect in selectnodesall) {

                    var selectsubitme = selectnodesall[keyselect];

                    if (selectsubitme.id == _def.selectid) {

                        if (selectsubitme.pId != "" && selectsubitme.pId != "undefined")
                        //p_itmeshow(selectsubitme.pId);
                            treeObj.selectNode(selectsubitme);
                        //addinputval(selectsubitme);

                        var renderTo = _def._Dom;
                        var treebox = renderTo;

                        var __inputs = treebox.find(".Searchinput");

                        if (renderTo[0].nodeName == "INPUT") {
                            __inputs = _def._Dom;
                            console.log(3343);
                            treebox.find(".Searchinput").val("");
                            treeclass.hideNodesall(classid);
                        }

                        __inputs.val(selectsubitme.name).attr({
                            "data-val": selectsubitme.data,
                            "data-name": selectsubitme.name,
                            "title": selectsubitme.name
                        });


                        //#########

                        if (_def.defaultselectpath) {

                            _def.mouseleave = false;

                            treeclass.reinputwidth(_def);

                        }

                        break;

                    }

                }

                _.omit(_def, "selectid");

                _def.Asynselectswitch = false;


            }

            return false;

        },
        createtree: function(classid) {

            //创建树
            var _this = treeclass;
            var _def = _this._global[classid];

            var ztredata = _def.JSONData;
            var renderTo = _def._Dom;

            var treebox = renderTo;
            var ztreebox = renderTo.find(".ztree");
            if (renderTo[0].nodeName == "INPUT") {
                treebox = renderTo.next(".treecontrolsbox").eq(0);
                ztreebox = treebox.find(".ztree");

            }


            var idsring = ztreebox.attr("id");

            var setting = {
                view: {
                    dblClickExpand: false //,
                        //addDiyDom: addDiyDom
                },
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                callback: {
                    beforeClick: zTreeBeforeClick,
                    onClick: onClickulr,
                    beforeAsync: Noasynerror,
                    onExpand: zTreeOnExpand
                }
            };

            if (_def.Async) {


                setting.async = {
                    enable: true,
                    url: _def.dataUrl,
                    autoParam: ["id=type", "level=lv"],
                    otherParam: {
                        "Reqnumr": _.now()
                    },
                    dataFilter: filter,
                    type: "get"
                };

                // setting.callback = {
                //   beforeClick:zTreeBeforeClick,
                //   onClick: onClickulr,
                //   onAsyncError: onAsyncError,
                //   onAsyncSuccess: zTreeOnAsyncSuccess,
                //   onExpand: zTreeOnExpand
                // }

                setting.callback.beforeAsync = null;

                setting.callback.onAsyncError = onAsyncError;
                setting.callback.onAsyncSuccess = zTreeOnAsyncSuccess;


                if (_def.PostData) {
                    var _Reqnumr = setting.async.otherParam;
                    setting.async.otherParam = _.extend(_Reqnumr, _def.PostData);
                }


            }

            //多选

            if (_def.multiselect) {

                setting.check = {
                    enable: true,
                    nocheckInherit: true
                }

            }


            function zTreeBeforeClick(treeId, treeNode, clickFlag) {

                if (treeNode.isParent) {

                    return _def.Parentclick

                }

            }


            //改变宽度
            function zTreeOnExpand() {

                _this.reinputwidth(_def);

            }


            //终止异步加载
            function Noasynerror() {
                return false;
            }


            function onAsyncError(event, treeId, treeNode) {


                var errortxt = _def.renderTo + "，树节点加载错误";
                if (_def.treeError) {
                    _def.treeError(errortxt);
                    return false;
                } else {
                    alert(errortxt);
                    return false;
                }

            }

            function addinputval(treeNode) {


                var __inputs = treebox.find(".Searchinput");

                if (renderTo[0].nodeName == "INPUT") {
                    __inputs = _def._Dom;
                    console.log(6666);
                    treebox.find(".Searchinput").val("");
                    treeclass.hideNodesall(classid);
                }

                __inputs.val(treeNode.name).attr({
                    "data-val": treeNode.data,
                    "data-name": treeNode.name,
                    "title": treeNode.name
                });

                return {
                    data: treeNode.data,
                    name: treeNode.name
                };

            }

            function addinputvalall(treeNode) {

                var treeObj = _def._treeObj;
                var _seectnodes = treeObj.getChangeCheckedNodes();

                var _datavalall = [],
                    _datanameall = [],
                    _data_val_all = [];
                for (var allkey in _seectnodes) {
                    var sub_itms = _seectnodes[allkey];
                    if (sub_itms.data && sub_itms.name) {
                        _datavalall.push(sub_itms.data);
                        _datanameall.push(sub_itms.name);
                        _data_val_all.push({
                            val: sub_itms.data,
                            name: sub_itms.name
                        });
                    }

                }

                var stringnameall = JSON.stringify(_datanameall);
                var __search_input = treebox.find(".Searchinput");

                if (renderTo[0].nodeName == "INPUT") {
                    __search_input = _def._Dom;
                   console.log(777);
                    __search_input.find(".Searchinput").val("");
                    treeclass.hideNodesall(classid);


                }

                //   treebox.attr({
                //   "data-val": JSON.stringify(_datavalall),
                //   "data-name": stringnameall,
                //   "title": stringnameall
                // });

                __search_input.attr({
                    "data-val": JSON.stringify(_datavalall),
                    "data-name": stringnameall,
                    "title": stringnameall
                });

                if (_datanameall.length > 0) {

                    __search_input.val(stringnameall);
                }



                return _data_val_all;

            }

            function onClickulr(event, treeId, treeNode) {

                var _data_val_all = {};
                if (!_def.multiselect) {
                    _data_val_all = addinputval(treeNode);

                //2015-06-15
                if(_def.Async && treeNode.isParent){

                  treebox.hide();

                }

                    if (_data_val_all.name == "" || typeof _data_val_all.name == "undefined") {

                        if (_def.treeError) {

                            _def.treeError("数据不完整");

                        }

                    }




                } else {

                    _data_val_all = addinputvalall(treeNode);

                    if (_data_val_all.length < 1 && !treeNode.isParent) {

                        console.log("请勾选");
                        // if(_def.treeError){

                        // _def.treeError("请勾选");

                        // }

                    }


                }



                if (!treeNode.isParent) {

                    _def.zTreeitmeclick = true;

                _this.treecontrolsboxstatic(classid);

                }

                if (treeNode.isParent && treeNode.children) {

                    _def._treeObj.expandNode(treeNode);

                }


                if (_def.treeclick) {

                    _def.treeclick(treeId, _data_val_all, treeNode);
                    // return false;

                }


             




                if (treeNode.isParent) {
                    _this.reinputwidth(_def);
                }

                  
                   //第一个节点
                   var firstli=ztreebox.find("li:first");
                   var nodestreeid=firstli.attr("id");
                   var _nodes=_def._treeObj.getNodeByTId(nodestreeid);
                    
                   if(_nodes && typeof _nodes!="undefined")
                   _def._treeObj.expandNode(_nodes, false, false, false);



                return false;

            }


            //请求完成
            function filter(treeId, parentNode, childNodes) {


                if (childNodes.length < 1) {

                    treeclass.hideNodesall(classid);
                    treeclass.show_ztreebox(_def);

                    return null;

                }

                for (var i = 0, l = childNodes.length; i < l; i++) {
                    childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
                }

                //都为空时
                if (childNodes.length < 1) {

                    var filter_treeObj = _def._treeObj;
                    var filter_data = filter_treeObj.transformToArray(filter_treeObj.getNodes());
                    filter_treeObj.hideNodes(filter_data);
                }


                return childNodes;
            }


            _def._treeObj = $.fn.zTree.init(ztreebox, setting, ztredata);


            if (_def.scrollH) {

                var Scrollbar_box = treebox.find(".Scrollbar_box");
                // _ztreebox.height(_def.scrollH);
                Scrollbar_box.css("max-height", _def.scrollH + "px").mCustomScrollbar({
                    axis: "y",
                    scrollInertia: _def.scrollH,
                    scrollbarPosition: "outside"
                });

                Scrollbar_box.find(".mCSB_scrollTools").css("right", "-5px");


            }


            // 选中器
            function showselectsubitme(_def) {

                _this._resetselectid(_def, classid);                

            }



            //选中请求
            if (!_def.Async) {

                showselectsubitme(_def);

            }



            //加载完成监听
            var Tofindthe = "";

            function zTreeOnAsyncSuccess() {



                var idsring = ztreebox.attr("id");
                var treeObj = _def._treeObj;

                // sub itme
                var _nodes = treeObj.transformToArray(treeObj.getNodes());
                var _JSONData = _def.JSONData;

                if (_JSONData && _JSONData != "" && typeof _JSONData != "undefined") {

                    //  _def.Numberofrequests=_def.Numberofrequests + (_JSONData.length -1);

                    if (_def.selectdft && _JSONData.length > 0) {

                        //开启选择的监听
                        _def.Asynselectswitch = true;
                        _def.selectid = _def.defaultselectpath.id;
                        _def.Numberofrequests = (_def.Numberofrequests - 1);

                    }


                    for (var jsoni = 0; jsoni < _JSONData.length; jsoni++) {

                        var subitme = _JSONData[jsoni];


                        if (_def.selectdft) {

                            //选择请求-子节点
                            _def.KeyName = null;

                            if (subitme.isSubitme) {

                                for (var nodei = 0; nodei < _nodes.length; nodei++) {
                                    var _ondesitme = _nodes[nodei];
                                    //subitme.isSubitme && 是否有子节点
                                    if (subitme.id == _ondesitme.id) {
                                        if (!_ondesitme.children && _ondesitme.isParent) {

                                            treeObj.reAsyncChildNodes(_ondesitme, "add", true);
                                            _def.Numberofrequests++;

                                            break;
                                        }
                                    }
                                }


                            }


                        } else {

                            //搜索请求

                            var reAsynrs = false;
                            var _searchregexp = new RegExp(_def.KeyName, 'i');
                            //如果父节点名包含搜索，也要进入抓取
                            if (_searchregexp.test(subitme.name)) {
                                reAsynrs = true;
                            }

                            if (subitme.isSubitme) {

                                for (var nodei = 0; nodei < _nodes.length; nodei++) {
                                    var _ondesitme = _nodes[nodei];
                                    //subitme.isSubitme && 是否有子节点
                                    if (subitme.id == _ondesitme.id) {
                                        if (!_ondesitme.children && _ondesitme.isParent || reAsynrs) {

                                            treeObj.reAsyncChildNodes(_ondesitme, "add", true);
                                            _def.Numberofrequests++;

                                            break;
                                        }
                                    }
                                }
                            }


                        }


                    }



                    _def.JSONData = null;

                    //end if

                }

                _def.Numberofrequests--;


                if (Tofindthe != "" && typeof Tofindthe != "undefined") {
                    window.clearTimeout(Tofindthe);
                }

                //进入查找
                if (_def.Numberofrequests < 1) {


                    if (_def.KeyName != null && typeof _def.KeyName != "undefined") {

                        Tofindthe = setTimeout(function() {

                            var _val = _def.KeyName;
                            var jsondatasr = treeObj.getNodesByParamFuzzy("name", _val, null);
                            _def.KeyName = null;
                            var nodeList = treeObj.transformToArray(jsondatasr);


                            if (nodeList.length > 0) {

                                //本地有结果时
                                treeclass.searchture(idsring, nodeList, _val, _def);

                                treeclass.setTim.ztree_show = false;

                            }

                        }, 100);
                    }


                    treeclass.inputfocus(false, _def);


                    //点击后的加载完成选中节点
                    if (_def.Asynselectswitch) {
                        //请求完成销毁
                        _def.selectdft = false;
                        showselectsubitme(_def);
                    }


                }

                //异步加载有默认选择时-取得面包屑defaultselectpath
                //一次就销毁
                if (_def.defaultselectpath == null && typeof _def.selectid == "object") {

                    $.getJSON(_def.dataUrl, _def.selectid, function(selectdata) {
                        var _deftvla = {};
                        for (var selectkey in selectdata) {

                            var selectitm = selectdata[selectkey];
                            if (!selectitm.isParent && !selectitm.isSubitme) {
                                _deftvla = selectitm;
                                break;
                            }

                        }

                        if (_deftvla != null) {

                            var _renderTobox = _def._Dom;
                            var _inputobj = treebox.find(".Searchinput");

                            if (renderTo[0].nodeName == "INPUT") {
                                _inputobj = _def._Dom;
                            }


                            //   _renderTobox.attr({
                            // "data-val": _deftvla.data,
                            // "data-name": _deftvla.name,
                            // "title": _deftvla.name
                            //  });

                            _inputobj.attr({
                                "data-val": _deftvla.data,
                                "data-name": _deftvla.name,
                                "title": _deftvla.name
                            });

                            _inputobj.val(_deftvla.name);


                        }


                        _def.defaultselectpath = _deftvla;

                    });


                }


            }


            if (_def.Initialize) {
                _def.Initialize(_def._treeObj);
            }



        },
        defaultselectitme: function(idsring, _def) {

            //异步加载有默认选择时-开启查找面包屑
            if (_def.Async) {

                var renderTo = _def._Dom;

                // var treebox=renderTo;
                // if(renderTo[0].nodeName == "INPUT"){
                //   treebox=renderTo.next(".treecontrolsbox").eq(0);        
                // }



                var data_var = renderTo.data("val");

                if (_def.defaultselectpath) {

                    var json_val_data = _def.defaultselectpath.data;

                    if (_def.selectpost && json_val_data == data_var) {

                        _def.selectpost = false;
                        _def.selectdft = true;

                        treeclass.searchfalse(idsring, _def.selectid, _def);


                    }

                }

            }

        },
        events: function(functionname, callback, classid) {

            var _this = treeclass;

            var _def = _this._global[classid];

            switch (functionname) {
                case "events.treeError":
                    _def.treeError = callback;
                    break;
                case "events.Itemclick":
                    _def.treeclick = callback;
                    break;
                case "Initialize":
                    _def.Initialize = callback
                    break;

                case "Pleaseselect":
                    _def.Pleaseselectfn = callback
                    break;
            }

        }


    }

    return treeclass._int;


});
