require(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

  require(["oso.lib/nprogress/nprogress.min", "jquery", "require_css!oso.lib/nprogress/nprogress", "require_css!bootstrap-css/bootstrap.min"],
    function(NProgress, $) {

      $("body").show();

      NProgress.start();

      var cssfileall = [
        "oso.lib/CodeMirror/mode/javascript/javascript",
        "oso.lib/CodeMirror/lib/util/foldcode",
        "require_css!oso.lib/CodeMirror/lib/codemirror.css",
        "require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css",
        "bootstrap",
        "require_css!bootstrap.datetimepicker-css"
      ];




      require(cssfileall, function() {



      require(["jquery","ztree","oso.lib/AsynLoadQueryTree/AsynLoadQueryTree1","artDialog","bootstrap.datetimepicker","domready!"], function($,ztree,AsynLoadQueryTree,dialog,datetimepicker) {




            $(function() {






   //{renderTo: "容器",dataUrl: "数据源","Async:是否异步加载节点"}
//静态数据
var __drs=[{"id" : "0","pId" : "","name" : "xxxx收货仓库","data":"11101"},{"id" : "1","pId" : "","name" : "自有仓库","data":"11101"},{"id" : "100000","pId" : "1","name" : "BJ","data":"11101"},{"id" : "100006","pId" : "1","name" : "GZ","data":"11101"},{"id" : "100043","pId" : "1","name" : "LA","data":"11101"},{"id" : "100197","pId" : "1","name" : "BS","data":"11101"},{"id" : "100203","pId" : "1","name" : "PA","data":"11101"}];


//多个input共享一颗树,多个input只有一颗树--inputShare
         var allinput=new AsynLoadQueryTree({
                        renderTo: ["#input1","#input2","#input3"],
                        Pleaseselect:"请选择",
                        //dataUrl: "./d.json",
                        //异步时，value是服务端写好的面包屑路径
                        //非异步是直接id--selectid:11110
                        //multiselect:true,
                        //selectid:{value:"1189716"},
                        dataUrl:__drs,
                        scrollH:400
                  });

         allinput.render();


     //console.log(__drs);

     var aatree = new AsynLoadQueryTree({
                        renderTo: "#ztreebox",
                        Pleaseselect:"请选择",
                        //dataUrl: "./d.json",
                        //异步时，value是服务端写好的面包屑路径
                        //非异步是直接id--selectid:11110
                        //multiselect:true,
                        //selectid:{value:"1189716"},
                        dataUrl:__drs,
                        scrollH:400
                  });

         aatree.on("Pleaseselect",function(jqinput){
           
           jqinput.val("123");

         });
     

                aatree.render();


                /*
                数据--如下父子节点
                 [
                 {"id" : "1","pId" : "0","name" : "BJ","data":"1000"},
                 {"id" : "101","pId" : "1","name" : "BJ--abc","data":"10001"}
                 ]

                */

      
          
                var tree = new AsynLoadQueryTree({
                        renderTo:".aaztreebox",
                        //dataurl:"Sync10-ui/pages/_load/"
                        dataUrl:"/Sync10-ui/lib/AsynLoadQueryTree/d.json",
                        //异步时，value是服务端写好的面包屑路径selectid:{value:"1189716"}
                         selectid:100199,//非异步时默认选中
                        scrollH:400,//多高后出现滚动条
                        Async:false,//非异步获取树节点，（一次加载完成）
                        multiselect:false,//多选
                        PostData:{"a":"1","b":"2"},//异步时请求每个节点时往服务端传值,
                        placeholder:"请查找",//默认input框值
                        Parentclick:true,//开启父节点可以选中
                        ztreebox_Align:"right"//弹出菜单对齐
                  });

                $("#abcd").click(function(){
                 
                // console.log(tree.value());
                //console.log(tree);
                tree.reselect_id("1000002",function(){
                  console.log(22334);
                });

                });

           
            //点击节点时监听
             tree.on("events.Itemclick",function(treeId,treeNode){                   
                   console.log(treeNode);                  
             });



            tree.on("Initialize",function(e){

              //初始化，e整棵ztree树
              console.log(e);

            });


          //出错时监听
          tree.on("events.treeError",function(e){
                  
                    var d = new dialog({
                    content: e,
                    icon: 'question',
                    lock: true,
                    width: 200,
                    height: 70,
                    title: '提示'
                  });
                  d.show();

              });
           
            tree.render();

          var dssr=$(".datetimepicker_box .form_datetime").datetimepicker({format:'yyyy-mm-dd'});

    //console.log(dssr);

          var focusboot=true;
      
         $("#fua1").focusout(function(){
          if(focusboot){
           console.log("fua1");
          }
         });

          $("#fua2").click(function(e){
           console.log("fua2");
         }).hover(function() {
           focusboot=false;
         }, function() {
          focusboot=true;
         });

            

            NProgress.done();
            

            });
         // window lod end

      });
       // AsynLoadQueryTree end



      });
      // codemirror end

    });
  // requirejs_config end
});
// nprogress end