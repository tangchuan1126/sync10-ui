"use strict";
require([
     "../../requirejs_config", 
], function(){
    require(["wrapToSelect"], function(){
    	$(function () {


	        $('#i1').wrapToSelect({
	            select : $('#selectWarehouse'),
	            onClose : function(e){console.log(e);}
	        });

	        $('#selectWarehouse li').on('click',function(){
	            $('#i1').wrapToSelect('setTitle', $(this).find('.select2-result-label').text())
	            //console.log($(this).find('.select2-result-label').text());
	        });

			$('#i2').wrapToSelect({
	            select : $('#selectWarehouse2')
	        });
	    });
    });

});
