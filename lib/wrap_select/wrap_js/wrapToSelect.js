"use strict";

(function(){
  define(["jquery"],function($){
    $.fn.wrapToSelect = function(options) {

    if(arguments[0] === 'setTitle'){

      var title = arguments[1], _this = $(this);

      if(typeof title !== 'string'){
        throw new Error('参数类型错误!');
      }

      if(title){
        _this.parent().find('.wrap-chosen').text(title).removeClass('select2-default').next().show();
      }else{
        
        _this.parent().find('.wrap-chosen').text(_this.data('wrap-option').title).addClass('select2-default').next().hide();
      }

      return;
    }

    if(arguments[0] === 'openSelect'){
        var _this=$(this), 
            _choice = _this.parent();
        if(_choice.hasClass('wrap-choice')){
            if(!_choice.hasClass('select2-dropdown-open')){
                _choice.trigger('click');
            }
            _this.data('wrap-option')._open = true;
        }
        return;
    }

    var defaults = {
      select: "" ,
      container : window,
      title : "请选择",
      width : 242,
      dropHeight : 0,
      choiceHeight : 28,
      onClose:function(event){}
    };

    var opts = $.extend({}, defaults, options);

    return this.each(function() {

      var _this=$(this),
          width = opts.width,
          choiceHeight,
          dropHeight = opts.select.outerHeight(true),
          position,
          isOpen = false,
          _parent = _this.parent(),
          _choice,
          _drop,
          _container = $(opts.container),
          arrWrap = _container.data('wrap');

      _this.data('wrap-option', opts);

      //设置parent的position
      var parentPosition = _parent.css('position');
      if(!parentPosition || parentPosition === 'static'){
        _parent.css('position', 'relative');
      }

      //span
      _this.wrap('<div class="wrap-choice select2-container"></div>')
           .addClass('select2-focusser select2-offscreen')
           .on('focus', function(e){

              //_choice.addClass('select2-container-active');
              _choice.trigger('click');

           }).on('keydown', function(e){
              if(e.which === 27){
                if(isOpen){
                  _choice.trigger('click');
                }
              }else if(e.which === 13){
                _choice.trigger('click');
              }else if(e.which === 9){
                if(isOpen){
                 _choice.trigger('click');
                }
                _choice.removeClass('select2-container-active');
                mask.hide();
              }
            });

      _choice = $(_parent.find('.wrap-choice'));
      var mask = $('<div class="select2-drop-mask" style="display: none;"></div>').appendTo(_parent).on('click', function(){
         clearTimeout(timer);
          timer = null;
        $(_container).trigger('click');
      });
      var timer = null;
      _choice.css('width', width + 'px')
        .append('<a tabindex="-1" class="select2-choice" href="javascript:void(0)"><span class="select2-default select2-chosen wrap-chosen">'+ opts.title +'</span><abbr class="select2-search-choice-close"></abbr><span role="presentation" class="select2-arrow"><b role="presentation"></b></span></a>')
        .on('click', function(event){

         if(this.className.match('open')){
          $(this).removeClass('select2-dropdown-open');
          _drop.hide();
          mask.hide();
          _choice.css('z-index',0).find('.select2-choice').focus();
          isOpen = false;
          
        }else if(this.className.match('active')){
          clearTimeout(timer);
          timer = null;
          $(this).addClass('select2-dropdown-open');
          _drop.show();
          _this.show();
          mask.show();
          _choice.css('z-index',9999);
          isOpen = true;

        }else{

          $(this).addClass('select2-dropdown-open').addClass('select2-container-active');

          _drop.show();
          mask.show();   
          _choice.css('z-index',9999);
          isOpen = true;  
        }
      }).on('click', '.select2-search-choice-close', function(event){

        $(this).hide().prev().text(opts.title).addClass('select2-default');
        opts.onClose(event);
        event.stopPropagation();
        return false
       }).on('blur', '.select2-choice', function(){
        timer = setTimeout(function(){
          if(!_choice.hasClass('select2-dropdown-open')){
            _choice.removeClass('select2-container-active');
          }
        }, 100);
      });

      position = _choice.position();

      choiceHeight = opts.choiceHeight || _choice.outerHeight();

      //drop
      opts.select.wrap($('<div class="select2-drop select2-display-none select2-with-searchbox select2-drop-active warp-drop"></div>').css({
        'left' : position.left + 'px',
        'top' : (position.top + choiceHeight) + 'px',
        'width' : width + 'px',
        'height' : opts.dropHeight || dropHeight + 'px'
      })).show();

      _drop = $(_parent.find('.warp-drop'));

      //点击区域不冒泡
      _parent.on('click', function(event){
        event.stopPropagation();
        return false;
      });

    //绑定点击页面其他地方事件
    _container.on('click', function(){
        if(_this.data('wrap-option') && _this.data('wrap-option')._open){
            _this.data('wrap-option')._open = false;
            return;
        }
        //展开，点击在面板范围外
        if(isOpen){
          _this.focus();
          mask.hide();
          _choice.css('z-index',0);
        }
      });

    });

  }
});
}).call(this);
