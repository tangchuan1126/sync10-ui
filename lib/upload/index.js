"use strict";

require([
     "../../requirejs_config", 
], function(){
    require(["jquery", "oso.lib/upload/js/upload"], function($, upload){

    	upload({
            elem:'#upload',
            fileType:'jpg,jpg,gif,png,jpeg,bmp',
            success : function(data){
                console.log(data);
            }
        });
    
    });

});
