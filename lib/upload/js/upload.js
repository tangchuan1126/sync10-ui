"use strict";

(function(){
	define(["jquery", "art_Dialog/dialog-plus", "require_css!../css/style.css"], function($, Dialog){

		var Upload = function(opts){

			if(!opts.elem){
				throw "Do not specify a element!";
			}

			var defaults = {
				elem:'',	
				width:undefined,
				height:undefined,
				fileType:undefined,
				data : {"author" : "hooyes"},
				url:'/Sync10/_fileserv/upload',
				receiveName : 'file',
				success:function(data, textStatus, jqXHR){},
				error:function(jqXHR, textStatus, errorThrown ){}
			}, options = $.extend({}, defaults, opts);

			function _init(){
				var $elem = $(options.elem),
					height = options.height || $elem.outerHeight(),
					width = options.width || $elem.outerWidth();


				$elem.wrap('<span class="wrap-upload"></span>').parent().css({
						height:height,
						width:width
					}).append('<input type="file" required />').find('input').css({
						height:height,
						width:width
					}).on('change', function(event){

						var file = this.files[0];
						this.value = "";

						if(!file){
							return;
						}

						if(options.fileType){

							var fileType = file.name.match(/(\.\w+)$/)[0].replace(/\./i,"");
							var reg = new RegExp(options.fileType.replace(/\s*,\s*/g, '|'), 'i');
							if(!reg.test(fileType)){ 

								var dialog = new Dialog({
					    			content: 'Can only upload the specified file type: <strong style="color:red;">' + options.fileType + '</strong>',
					    			lock:true,
					    			title:'Error',
					    			cancel: function(){
								    	dialog.close();
									}
					    		}).showModal();

					    		return;
							}
						}

						sendForm(file, options.url, options.success, options.error);
					});

			}

			//发送数据
			function sendForm(file, url, success, error) {

				var oData = new FormData();
				for(var o in options.data){
					oData.append(o, options.data[o]); 
				}
				oData.append(options.receiveName, file);

				$.ajax({
				  	url: url,
				  	type: "POST",
				  	data: oData,
				  	processData: false,  // 告诉jQuery不要去处理发送的数据
				  	contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
				  	success : options.success,
				  	error : options.error
				});
			}

			_init();
		};

		return Upload;
		
	});
})(this);