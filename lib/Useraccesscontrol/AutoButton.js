/*
 * autobutton.js
 * Copyright 2014, Connor
 * http://127.0.0.1/Sync10-ui/ilb/Useraccesscontrol/
 */
;
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register 
    define(['jquery'], factory);
  } else if (typeof module !== 'undefined' && module.exports) {
    // CommonJS
    module.exports = factory(require('jquery')(root));

  } else {
    // Browser globals
    //root===Window
    root.AutoButton = factory(root.$);
  }
}(this, function($) {

  'use strict';



  // console.log(d);
  var tlpbtnall = "";

  var Mfypmis = function(objs) {



    var _this = this;
    _this.data = objs.jsondata;
    _this.Mfypmis_ = "";
    _this.options = "";
    var allmfypmis = "";
    _this.Competence = false;

    if (objs) {
      _this.options = objs;

      if (objs.el) {
        _this.options.el = $(objs.el);
        allmfypmis = _this.options.el.find("*[RootSet]");
      }

    }



      //只为静态用
      if (!objs.Rookey && !objs.buttonList && allmfypmis == "" || typeof allmfypmis == "undefined" || !allmfypmis) {

        allmfypmis = $("body").find("*[RootSet]");

      }

      if (objs.Rookey) {

        _this.Competence = _this.user_rootMatch(objs.Rookey);

      }

      //静态下处理
      if (allmfypmis.length > 0) {
        _this.Staticbatch(allmfypmis);
      }


      //{"RootSet":"disabled",RootKey:"",buttontlp":"abc001","lable":"abc1",lefticon:"",righticon:"",class:""}

      //动态创建button
      if (objs.buttonList) {

        //loadbuttontlp--动态加载按钮模版
        tlpbtnall = {
          "bs3button": '<button type="button" class="btn btn-default#classname#">#lable#</button>',
          "btn_group_select": '<div class="btn-group"><a class="btn btn-primary" href="#"><i class="icon-user icon-white"></i>#lable#</a><a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a><ul class="dropdown-menu">#selectli#</ul></div>',
          "bs3_abutton": '<a class="btn" href="javascript:void(0)">#lefticon##lable##righticon#</a>',
          "bs3_button_icon": '<button type="button" class="btn btn-default btn-lg#classname#">#lefticon##lable##righticon#</button>',
          "input_button": '<input type="button" value="#lable#" />',
          "button": '<button>#lable#</button>',
          "input_checkbox": '<input type="checkbox" value="#lable#"/>',
          "input_radio": '<input type="radio" value="#lable#"/>'
        };

        _this.Mfypmis_ = _this.Dynamicbatch(objs.buttonList);

      }


    //});

  };

  Mfypmis.prototype = {

    Tohtml: function() {
      return this.Mfypmis_;
    },
    Toobject: function() {
      var _this = this;
      var objs = _this.options;
      if (objs.el.length > 0) {
        var jq_obj = $(_this.Mfypmis_);

        jq_obj.appendTo(objs.el);

        return jq_obj;

      }

    },
    Dynamicbatch: function(buttonList) {

      var buttonhtml = "";

      if (buttonList.length > 0) {

        for (var listkey = 0; listkey < buttonList.length; listkey++) {

          var jsonitme = buttonList[listkey];

          if (jsonitme.RootKey && typeof jsonitme.RootKey != "undefined" && jsonitme.RootKey != "") {
            var RootKey = jsonitme.RootKey;
            var roots = this.user_rootMatch(RootKey);
            var attrs = jsonitme.RootSet;

            //如果用户列表存在roots为true,这时remove不会删除，还是会输出button
            if (attrs == "remove" && roots) {
              buttonhtml += this.createbutton(jsonitme);
            }


            if (attrs == "disabled" || attrs == "hidden" && !roots) {

              buttonhtml += this.createbutton(jsonitme);

            }

          }


        }

      }

      return buttonhtml;

    },
    createbutton: function(itmebutton) {

      //{"RootSet":"disabled",RootKey:"",buttontlp":"abc001","lable":"abc1",lefticon:"",righticon:"",classname:""}
      var buttonhtml = "";
      if (itmebutton.buttontlp && tlpbtnall != "" && typeof tlpbtnall != "undefined") {

        var itmehtmls = tlpbtnall[itmebutton.buttontlp];
        if (itmehtmls != "" && typeof itmehtmls != "undefined") {

          itmehtmls = itmehtmls.replace(/#lable#/, itmebutton.lable);

          if (itmebutton.lefticon && itmebutton.lefticon != "undefined")
            itmehtmls = itmehtmls.replace(/#lefticon#/, itmebutton.lefticon);
          else
            itmehtmls = itmehtmls.replace(/#lefticon#/, "");

          if (itmebutton.righticon && itmebutton.righticon != "undefined")
            itmehtmls = itmehtmls.replace(/#righticon#/, itmebutton.righticon);
          else
            itmehtmls = itmehtmls.replace(/#righticon#/, "");

          if (itmebutton.classname && typeof itmebutton.classname != "undefined")
            itmehtmls = itmehtmls.replace(/#classname#/, itmebutton.classname);
          else
            itmehtmls = itmehtmls.replace(/#classname#/, "");

          buttonhtml = itmehtmls;
        }


      }

      return buttonhtml;

    },
    Staticbatch: function(allmfypmis) {

      for (var keys = 0; keys < allmfypmis.length; keys++) {

        var itmeobjs = allmfypmis.eq(keys);
        var attrs = itmeobjs.attr("RootSet");
        var RootKey = itmeobjs.attr("RootKey");
        if (RootKey != null && typeof RootKey != "undefined" && RootKey != "") {
          var roots = this.user_rootMatch(RootKey);
          if (attrs == "remove" && !roots) {
            itmeobjs.remove();
          }

          if (attrs == "hidden" && !roots) {
            itmeobjs.hide();
          }



          if (attrs == "disabled" && !roots) {

            var types = itmeobjs[0].nodeName;

            if (types == "BUTTON" || types == "INPUT")
              itmeobjs.prop("disabled", "true");

            if(itmeobjs.is(".buttons")){
              itmeobjs.addClass('disabled');
            }

          }

        }

      }

    },
    user_rootMatch: function(urlkey) {
      //如果为真说明存在user列表里
      var d = this.data;
      var users = false;
      if (urlkey.indexOf("|") > -1) {
        var urlall = urlkey.split("|");
        var METHOD = urlall[0].toUpperCase();
        var URI = urlall[1];

        //全局
        var CTRLPERM_all = d.ctrlperm.perms;
        var all = false;
        for (var allkeyitme = 0; allkeyitme < CTRLPERM_all.length; allkeyitme++) {
          var allitms = CTRLPERM_all[allkeyitme];
          var _methodval = allitms.method.toUpperCase();
          if (_methodval == METHOD && allitms.uri == URI) {
            all = true;
            break;
          }

        }

        //用户
        var USERPERM_all = d.userperm.perms;
        for (var ukeyitme = 0; ukeyitme < USERPERM_all.length; ukeyitme++) {
          var uallitms = USERPERM_all[ukeyitme];
          if (uallitms.method == METHOD && uallitms.uri == URI) {
            users = true;
            break;
          }

        }


      }

      return users;
    }



  }



  return Mfypmis;

}));