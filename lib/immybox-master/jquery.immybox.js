(function() {
    var __slice = [].slice,
        __bind = function(fn, me) {
            return function() {
                return fn.apply(me, arguments);
            };
        },
        __indexOf = [].indexOf || function(item) {
            for (var i = 0, l = this.length; i < l; i++) {
                if (i in this && this[i] === item) return i;
            }
            return -1;
        };

    (function($, window, document) {

       //ie8--扩展--
        var msieindexof = /msie/.test(navigator.userAgent.toLowerCase());
        if (msieindexof) {

            if (!Array.prototype.filter) {

                Array.prototype.filter = function(fun /*, thisp */ ) {
                    "use strict";

                    if (this === void 0 || this === null)
                        throw new TypeError();

                    var t = Object(this);
                    var len = t.length >>> 0;
                    if (typeof fun !== "function")
                        throw new TypeError();

                    var res = [];
                    var thisp = arguments[1];
                    for (var i = 0; i < len; i++) {
                        if (i in t) {
                            var val = t[i]; 
                            if (fun.call(thisp, val, i, t))
                                res.push(val);
                        }
                    }

                    return res;
                };
            }


            if (!Array.prototype.map) {

                Array.prototype.map = function(callback, thisArg) {

                    var T, A, k;

                    if (this == null) {
                        throw new TypeError(" this is null or not defined");
                    }

                    var O = Object(this);

                    var len = O.length >>> 0;

                    if (typeof callback !== "function") {
                        throw new TypeError(callback + " is not a function");
                    }

                    if (thisArg) {
                        T = thisArg;
                    }

                    A = new Array(len);

                    k = 0;
                    while (k < len) {

                        var kValue, mappedValue;

                        if (k in O) {

                            kValue = O[k];

                            mappedValue = callback.call(T, kValue, k, O);

                            A[k] = mappedValue;
                        }
                        k++;
                    }
                    return A;
                };

            }

        }

        //控件

        var ImmyBox, defaults, objects, pluginName;
        pluginName = "immybox", settimes = "";

        function touse(modeuitme) {

            var __txt = "",
                __value = "";

            if (modeuitme.text) {
                __txt = modeuitme.text;
            }

            if (modeuitme.TEXT) {

                __txt = modeuitme.TEXT;

            }

            if (modeuitme.value) {
                __value = modeuitme.value;
            }
            if (modeuitme.VALUE) {

                __value = modeuitme.VALUE;

            }

            return {
                _txt: __txt,
                _value: __value
            }
        };


        defaults = {
            choices: [],
            maxResults: 50,
            showArrow: true,
            openOnClick: true,
            filterFn: function(query) {
                return function(choice) {
                    var totxts = touse(choice);
                    if (/\w/.test(totxts._txt) && /\w/.test(query) && !/\d/.test(totxts._txt))
                        return totxts._txt.toLowerCase().indexOf(query.toLowerCase()) >= 0;
                    else
                        return totxts._txt.indexOf(query) >= 0;
                };
            },
            formatChoice: function(choice, query) {

                var head, i, matchedText, tail, _ref;
                var __txt = "";
                if (choice.text) {
                    __txt = choice.text;
                }
                if (choice.TEXT) {

                    __txt = choice.TEXT;

                }

                if (/\w/.test(__txt) && /\w/.test(query) && !/\d/.test(__txt))
                    i = __txt.toLowerCase().indexOf(query.toLowerCase());
                else
                    i = __txt.indexOf(query);

                if (i >= 0) {
                    matchedText = __txt.slice(i, i + query.length);
                    _ref = __txt.split(matchedText), head = _ref[0], tail = 2 <= _ref.length ? __slice.call(_ref, 1) : [];
                    return "" + head + "<span class='highlight'>" + matchedText + "</span>" + (tail.join(matchedText));
                } else {
                    return __txt;
                }
            }
        };
        objects = [];

        ImmyBox = (function() {

            //填写选择的----
            function Reselectvals(self, datavals) {

                //  var datavals=self.element.data('value');
                var selectvals = [];
                if (typeof datavals != "undefined" && datavals != "") {
                    if (datavals.indexOf(",") > -1) {
                        selectvals = datavals.split(",");
                    } else {
                        selectvals.push(datavals);
                    }

                }

                self.options.selectvals = selectvals;

            };

            function checkclick(self) {

                var divpluginName = $("div." + pluginName + "_results");
                var liall = divpluginName.find("ul li");

                var modeuleall = [];


                for (var likey = 0; likey < liall.length; likey++) {
                    var _li = liall.eq(likey);
                    if (_li.find(':checked').length > 0) {
                        var value = _li.data('value');

                        var _modeule = self.choices.filter(function(choice) {

                            if (choice.value) {
                                return choice.value == value;
                            }

                            if (choice.VALUE) {
                                return choice.VALUE == value;
                            }

                        });

                        if (_modeule[0]) {

                            modeuleall.push(_modeule[0]);

                        }

                    }

                }

                if (modeuleall.length > 0) {

                    var valueall = "",
                        valopint = "";
                    for (var _key in modeuleall) {
                        var modeuitme = modeuleall[_key];




                        if (valueall != "" && valueall != "unde") {
                            var totxts = touse(modeuitme);
                            valueall += "," + totxts._txt;
                            valopint += "," + totxts._value;
                        } else {
                            var totxts = touse(modeuitme);
                            valueall = totxts._txt;
                            valopint = totxts._value;
                        }

                    }


                }


                self.element.val(valueall);
                self.element.attr("data-value", valopint);




                Reselectvals(self, valopint);

            };



            function checkboxeventsall(box) {
                var self = box;
                self.queryResultArea.on('click', "li." + pluginName + "_choice .checkboxstyle", function(e) {

                    e.stopPropagation();
                    checkclick(self);


                });

            };



            function ImmyBox(element, options) {
                this.reposition = __bind(this.reposition, this);
                this.revert = __bind(this.revert, this);
                this.openResults = __bind(this.openResults, this);
                this.doSelection = __bind(this.doSelection, this);
                this.doQuery = __bind(this.doQuery, this);
                this.reDefaultselect = __bind(this.reDefaultselect, this);
                var self, _base;
                self = this;
                this.element = $(element);
                this.element.addClass(pluginName);
                this.element.attr('autocomplete', 'off');
                this._defaults = defaults;
                this._name = pluginName;
                this.options = $.extend({}, defaults, options);
                this.choices = this.options.choices;
                this.selectedChoice = null;

                if (this.options.showArrow) {
                    this.element.addClass("" + pluginName + "_witharrow");
                }
                if (this.options.openOnClick) {
                    this.element.on('click', this.openResults);
                }

                this.selectChoiceByValue(this.element.val());
                this.queryResultArea = $("<div class='" + pluginName + "_results'></div>");
                if (typeof(_base = this.queryResultArea).scrollLock === "function") {
                    _base.scrollLock();
                }
                this.queryResultAreaVisible = false;
                this._val = this.element.val();
                this.oldQuery = this._val;


                // this.queryResultArea.on('mouseenter', "li." + pluginName + "_choice", function() {
                //   self.queryResultArea.find("li." + pluginName + "_choice.active").removeClass('active');
                //   $(this).addClass('active');
                // });

                this.element.on('keyup change search', this.doQuery);
                this.element.on('keydown', this.doSelection);
            }

            //重新设置默认值
            ImmyBox.prototype.reDefaultselect = function(value) {

                var __thisimm = this;
                if (value != "" && typeof value != "undefind") {
                    __thisimm.options.Defaultselect = value;
                    var itmeobj = __thisimm.returnitmeval(value)[0];
                    __thisimm.selectChoiceByValue(itmeobj.value);
                    __thisimm.element.val(itmeobj.text);

                }

            }




            //回车时--keyup change search这几个事件都走
            ImmyBox.prototype.doQuery = function(e) {

                if (this.options.checkbox) {
                    var check_vals = this.element.val();
                    if (e.which == 8 && check_vals == "" || check_vals == "undefind") {
                        this.options.selectvals = [];
                        this.element.attr("data-value", "");
                        var _checkedall = this.queryResultArea.find(':checked');
                        _checkedall.prop("checked", false);
                    }
                }

                if (!this.reselects(e)) {
                    return false;
                }

                var query;
                query = this.element.val();
                if (this._val !== query) {
                    this._val = query;
                    this.oldQuery = query;
                    if (query === '') {
                        this.hideResults();

                        if (e.which == 8)
                            this.element.attr("data-value", "");

                    } else {
                        //进入查找功能--不是多选时，多选没有保留值时
                        // var dataval=this.element.attr("data-value");
                        //if(dataval=="" || typeof dataval == "undefined")
                        if (query.indexOf(",") > -1) {

                            var arrselect = query.split(",");
                            query = arrselect[arrselect.length - 1];

                            this.oldQuery = query;

                        }

                        this.insertFilteredChoiceElements(query);

                    }
                }


            };

            //input keydown 当按下按键时
            ImmyBox.prototype.doSelection = function(e) {

                if (e.which === 27) {

                    this.display();
                    this.hideResults();
                }
                if (this.queryResultAreaVisible) {
                    switch (e.which) {
                        case 9:
                            this.selectHighlightedChoice();
                            break;
                        case 13:
                            e.preventDefault();
                            this.selectHighlightedChoice();
                            break;
                        case 38:
                            e.preventDefault();
                            this.highlightPreviousChoice();
                            this.scroll();
                            break;
                        case 40:
                            e.preventDefault();
                            this.highlightNextChoice();
                            this.scroll();
                    }
                } else {
                    switch (e.which) {
                        case 40:
                            e.preventDefault();

                            if (this.selectedChoice != null && !this.options.selectvals) {

                                this.insertFilteredChoiceElements(this.oldQuery);
                            } else {
                                this.insertFilteredChoiceElements('');
                            }

                            break;
                        case 9:
                            this.revert();
                    }
                }


            };

            ImmyBox.prototype.reselects = function(e) {
                //如果鼠标点击时，同时是多选

                var opo = true;
                if (this.options.checkbox) {

                    var checkeds = $("." + pluginName + "_results ul :checked");
                    if (e.type == "keyup" && e.which == 123 && checkeds.length > 0) {
                        opo = false;
                    }

                    if (e.type == "click" && e.which == 1 && checkeds.length > 0) {
                        opo = false;
                    }


                }

                return opo;

            };

            //打开
            ImmyBox.prototype.openResults = function(e) {
                e.stopPropagation();

                $("." + pluginName + "_results").remove();

                //this.options.selectvals=[];

                //写入上次-多选的值
                var valall = this.element.attr("data-value");
                Reselectvals(this, valall);

                if (this.element.val() != this.oldQuery) {
                    this.oldQuery = null;
                    this.selectedChoice = null;
                }

                if (!this.reselects(e)) {

                    return false;
                }

                this.revertOtherInstances();
                if (this.selectedChoice != null) {
                    this.insertFilteredChoiceElements(this.oldQuery);
                } else {
                    this.insertFilteredChoiceElements('');
                }


            };

            //销毁
            ImmyBox.prototype.revert = function(e) {
                if (this.queryResultAreaVisible) {

                    this.display();
                    this.hideResults();
                } else if (this.element.val() === '') {

                    this.selectChoiceByValue(null);
                }

            };

            //位置
            ImmyBox.prototype.reposition = function() {
                if (this.queryResultAreaVisible) {
                    this.positionResultsArea();
                }
            };

            ImmyBox.prototype.insertFilteredChoiceElements = function(query) {


                var filteredChoices, format, info, list, results, selectedOne, truncatedChoices;
                //为空时所有
                if (query === '') {
                    filteredChoices = this.choices;
                } else {
                    //不为空是去遍历--那input 的value去找
                    filteredChoices = this.choices.filter(this.options.filterFn(this.oldQuery));
                }

                var check_vals = this.element.val();
                if (this.options.checkbox && check_vals == "" || typeof check_vals == "undefind") {

                    this.options.selectvals = [];
                    this.element.attr("data-value", "");

                }

                //数据
                // console.log(filteredChoices);
                //重长度
                // console.log(this.options.maxResults);
                var datalen = this.options.maxResults;
                if (filteredChoices.length > datalen) {
                    datalen = filteredChoices.length;
                }

                truncatedChoices = filteredChoices.slice(0, datalen);
                //返回数据
                //格式化函数
                format = this.options.formatChoice;
                selectedOne = false;
                var textall = "";
                var checkbox = "";
                //变量
                results = truncatedChoices.map((function(_this) {
                    return function(choice) {
                        //_this是整个自己
                        //choice --当前一条记录{text: 'Alabama', value: 'AL'}
                        if (_this.options.checkbox) {
                            checkbox = '';
                            var selectvals = _this.options.selectvals;
                            if (selectvals.length > 0) {
                                for (var key_ in selectvals) {
                                    var itme_val = selectvals[key_];

                                    var totxts = touse(choice);

                                    if (itme_val == totxts._txt) {

                                        if (textall != "" && typeof textall != "undefined")
                                            textall += "," + totxts._txt;
                                        else
                                            textall = totxts._txt;

                                        checkbox = '<span class="checkboxstyle"><input type="checkbox" checked/></span>';
                                        break;
                                    } else {
                                        checkbox = '<span class="checkboxstyle"><input type="checkbox"/></span>';
                                    }

                                }

                            } else {
                                checkbox = '<span class="checkboxstyle"><input type="checkbox"/></span>';
                            }
                        }


                        var li;
                        li = $("<li class='" + pluginName + "_choice'></li>");

                        var __choice = touse(choice);

                        li.attr('data-value', __choice._value);
                        li.html(checkbox + format(choice, query));
                        if (choice === _this.selectedChoice) {
                            selectedOne = true;
                            li.addClass('active');
                        }
                        return li;
                    };
                })(this));

                if (results.length === 0) {
                    info = $("<p class='" + pluginName + "_noresults'>no matches</p>");
                    this.queryResultArea.empty().append(info);
                } else {
                    if (!selectedOne) {
                        results[0].addClass('active');
                    }


                    if (this.options.Pleaseselect) {
                        var notli = this.queryResultArea.find('.' + pluginName + '_noresults');
                        if (notli.length > 0) {
                            notli.remove();
                        }
                        var Pleaseselect = $('<li class="noselect ' + pluginName + '_choice">' + this.options.Pleaseselect + '</li>');
                        results.unshift(Pleaseselect);

                    }

                    var __ulobj = this.queryResultArea.find("ul");
                    if (__ulobj.length > 0) {
                        //__ulobj.find("li").unbind();
                        __ulobj.remove();
                    }

                    list = $('<ul></ul>').append(results);
                    this.queryResultArea.append(list);
                    // if(this.options.selectvals.length > 0){

                    var self = this;

                    var __objli = this.queryResultArea.find("ul li");

                    __objli.mouseenter(function(event) {
                        event.stopPropagation();
                        self.queryResultArea.find("li." + pluginName + "_choice.active").removeClass('active');
                        $(this).addClass('active');
                    }).click(function(e) {

                        e.stopPropagation();
                        var value;
                        var __this = $(this);


                        if (!self.options.checkbox && !__this.hasClass("noselect")) {
                            value = __this.data('value');
                            self.selectChoiceByValue(value);
                            self.hideResults();
                            self._val = self.element.val();
                            self.element.focus();
                            if (self.options.change) {
                                var _selects = self.selectedChoice;
                                self.options.change(e, _selects);

                            }

                        } else {

                            self.element.val("").attr("data-value", "");
                            self.element.focus();

                            //ImmyBox.prototype.hmtlclock(e);  

                            if (self.options.Pleaseselectclick) {
                                self.options.Pleaseselectclick(e);
                            }

                            $("plugin_" + pluginName).remove();
                            self.revert();


                        }

                        return false;


                    });

                    /*
                      this.queryResultArea.on('mouseenter', "li." + pluginName + "_choice", function() {
                              self.queryResultArea.find("li." + pluginName + "_choice.active").removeClass('active');
                              $(this).addClass('active');
                            });


                          this.queryResultArea.on('click', "li." + pluginName + "_choice", function(e) {

                             e.stopPropagation();
                              var value;
                              var __this=$(this);
                              if(!self.options.checkbox && !__this.hasClass("noselect")){
                              value = __this.data('value');                    
                              self.selectChoiceByValue(value);
                              self.hideResults();
                              self._val = self.element.val();
                              self.element.focus();
                              if(self.options.change){
                               var _selects=self.selectedChoice;
                               self.options.change(e,_selects);

                              }

                             }else{

                              self.element.val("").attr("data-value","");
                              self.element.focus();
                              ImmyBox.prototype.hmtlclock(e);         
                             
                             }

                              return false;

                            });
                    */


                    // }

                }
                this.showResults();
                if (this.options.checkbox) {

                    checkboxeventsall(this);

                }


            };

            ImmyBox.prototype.scroll = function() {
                var highlightedChoice, highlightedChoiceBottom, highlightedChoiceHeight, highlightedChoiceTop, resultsBottom, resultsHeight, resultsTop;
                resultsHeight = this.queryResultArea.height();
                resultsTop = this.queryResultArea.scrollTop();
                resultsBottom = resultsTop + resultsHeight;
                highlightedChoice = this.getHighlightedChoice();
                if (highlightedChoice == null) {
                    return;
                }
                highlightedChoiceHeight = highlightedChoice.outerHeight();
                highlightedChoiceTop = highlightedChoice.position().top + resultsTop;
                highlightedChoiceBottom = highlightedChoiceTop + highlightedChoiceHeight;
                if (highlightedChoiceTop < resultsTop) {
                    this.queryResultArea.scrollTop(highlightedChoiceTop);
                }
                if (highlightedChoiceBottom > resultsBottom) {
                    this.queryResultArea.scrollTop(highlightedChoiceBottom - resultsHeight);
                }
            };

            //容器重定位
            ImmyBox.prototype.positionResultsArea = function() {
                var inputHeight, inputOffset, inputWidth, resultsBottom, resultsHeight, windowBottom;
                inputOffset = this.element.offset();
                inputHeight = this.element.outerHeight();
                inputWidth = this.element.outerWidth();
                resultsHeight = this.queryResultArea.outerHeight();
                resultsBottom = inputOffset.top + inputHeight + resultsHeight;
                windowBottom = $(window).height() + $(window).scrollTop();
                this.queryResultArea.outerWidth(inputWidth);

                this.queryResultArea.css({
                    left: inputOffset.left
                });

                if (resultsBottom > windowBottom) {
                    this.queryResultArea.css({
                        top: inputOffset.top - resultsHeight
                    });
                } else {
                    this.queryResultArea.css({
                        top: inputOffset.top + inputHeight
                    });
                }



            };

            ImmyBox.prototype.getHighlightedChoice = function() {
                var choice;
                choice = this.queryResultArea.find("li." + pluginName + "_choice.active");
                if (choice.length === 1) {
                    return choice;
                } else {
                    return null;
                }
            };

            ImmyBox.prototype.highlightNextChoice = function() {
                var highlightedChoice, nextChoice;
                highlightedChoice = this.getHighlightedChoice();
                if (highlightedChoice != null) {
                    nextChoice = highlightedChoice.next("li." + pluginName + "_choice");
                    if (nextChoice.length === 1) {
                        highlightedChoice.removeClass('active');
                        nextChoice.addClass('active');
                    }
                }
            };

            ImmyBox.prototype.highlightPreviousChoice = function() {
                var highlightedChoice, previousChoice;
                highlightedChoice = this.getHighlightedChoice();
                if (highlightedChoice != null) {
                    previousChoice = highlightedChoice.prev("li." + pluginName + "_choice");
                    if (previousChoice.length === 1) {
                        highlightedChoice.removeClass('active');
                        previousChoice.addClass('active');
                    }
                }
            };

            //方向键移动后回车时
            ImmyBox.prototype.selectHighlightedChoice = function() {
                var highlightedChoice, value;
                highlightedChoice = this.getHighlightedChoice();

                if (this.options.checkbox) {

                    var checkbox = highlightedChoice.find(":checkbox");
                    if (checkbox.prop("checked")) {
                        checkbox.prop("checked", false);
                    } else {
                        checkbox.prop("checked", true);
                    }


                    this.selectedChoice = null;
                    checkclick(this);


                    //this.insertFilteredChoiceElements('');

                    return false;

                } else {

                    if (highlightedChoice != null) {
                        value = highlightedChoice.data('value');
                        this.selectChoiceByValue(value);
                        this._val = this.element.val();
                        this.hideResults();
                    } else {
                        this.revert();
                    }

                }


                if (this.options.change) {
                    var _e = "";
                    this.options.change(_e, this.selectedChoice);

                }

            };

            ImmyBox.prototype.display = function() {


                if (this.selectedChoice != null) {

                    var _selects = touse(this.selectedChoice);
                    //this.selectedChoice.text;

                    this.element.val(_selects._txt);
                    this.element.attr("data-value", _selects._value);

                } else {

                    var _val = this.element.val();
                    var _data = this.element.data("value");
                    var valbo = (typeof _val != "undefined" && _val != "" && typeof _data != "undefined" && _data != "");
                    if (!valbo && !this.options.checkbox)
                        this.element.val('');

                }
                this._val = this.element.val();



            };

      //重新填充数据
       ImmyBox.prototype.Rechoicesdata=function(json_choices,selectid){
     
       if(json_choices)
        this.choices=json_choices;
        this.revert();
        if(selectid &&  selectid != "")
        {
            this.selectChoiceByValue(selectid);
          
        }else
	{
       	    this.element.val("");
            this.element.attr("data-value","");
	}
       }
 //匹配

            ImmyBox.prototype.returnitmeval = function(value) {

                var matches = null;

                if ((value != null) && value !== '') {

                    matches = this.choices.filter(function(choice) {

                        if (choice.value) {
                            return choice.value == value;
                        }

                        if (choice.VALUE) {
                            return choice.VALUE == value;
                        }

                        // return choice.value == value;
                    });

                }

                return matches;

       };


            //截取数组中的一个{}
            ImmyBox.prototype.selectChoiceByValue = function(value) {
                var matches, newValue, oldValue;
                oldValue = this.getValue();
                if ((value != null) && value !== '') {

                    matches = this.choices.filter(function(choice) {

                        if (choice.value) {
                            return choice.value == value;
                        }

                        if (choice.VALUE) {
                            return choice.VALUE == value;
                        }

                        // return choice.value == value;
                    });

                    if (matches[0] != null) {
                        this.selectedChoice = matches[0];
                    } else {
                        this.selectedChoice = null;
                    }
                } else {
                    this.selectedChoice = null;
                }
                newValue = this.getValue();
                if (newValue !== oldValue) {
                    this.element.trigger('update', [newValue]);
                }
                this.display();
            };

            ImmyBox.prototype.revertOtherInstances = function() {
                var o, _i, _len;
                for (_i = 0, _len = objects.length; _i < _len; _i++) {
                    o = objects[_i];
                    if (o !== this) {
                        o.revert();
                    }
                }
            };

            ImmyBox.prototype.publicMethods = ['showResults', 'hideResults', 'getChoices', 'setChoices', 'getValue', 'setValue', 'destroy'];

            ImmyBox.prototype.showResults = function() {

                var thisw = this;
                $('body').append(this.queryResultArea);
                this.queryResultArea.hover(function() {
                    var el_this = $(this);
                    if (typeof settimes != "undefined" && settimes != "") {
                        window.clearTimeout(settimes);
                    }

                    return false;


                }, function() {

                    var el_this = $(this);
                    if (typeof settimes != "undefined" && settimes != "") {
                        window.clearTimeout(settimes);
                    }

                    settimes = setTimeout(function() {

                        el_this.remove();
                        thisw.revert();


                        thisw.reRetreatval();



                    }, 600);


                }).scroll(function() {

                    if (typeof settimes != "undefined" && settimes != "") {
                        window.clearTimeout(settimes);
                    }
                    return false;
                });

                this.queryResultAreaVisible = true;
                this.scroll();
                this.positionResultsArea();
            };

            //后退回填
            ImmyBox.prototype.reRetreatval = function() {

                var thisw = this;
                if (thisw.options.Retreatval && thisw.options.Retreatval == true) {

                    var datavalue = thisw.element.attr("data-value");
                    var itmeobj = thisw.returnitmeval(datavalue)[0];
                    if (itmeobj && itmeobj != null) {

                        var this_text = thisw.element.val();
                        if (itmeobj.text != this_text && this_text != "" && typeof this_text != "undefined") {
                            thisw.selectChoiceByValue(itmeobj.value);
                        }

                    }

                }


            };

            ImmyBox.prototype.hideResults = function() {
                this.queryResultArea.detach();
                this.queryResultAreaVisible = false;
            };

            ImmyBox.prototype.getChoices = function() {
                return this.choices;
            };

            ImmyBox.prototype.setChoices = function(newChoices) {
                this.choices = newChoices;
                if (this.selectedChoice != null) {
                    this.selectChoiceByValue(this.selectedChoice.value);
                }
                this.oldQuery = '';
                return newChoices;
            };

            ImmyBox.prototype.getValue = function() {
                if (this.selectedChoice != null) {
                    if (this.selectedChoice.value) {
                        return this.selectedChoice.value;
                    }

                    if (this.selectedChoice.VALUE) {
                        return this.selectedChoice.VALUE;
                    }

                } else {
                    return null;
                }
            };

            ImmyBox.prototype.setValue = function(newValue) {
                var currentValue;
                currentValue = this.getValue();
                if (currentValue !== newValue) {
                    this.selectChoiceByValue(newValue);
                    this.oldQuery = '';
                    return this.getValue();
                } else {
                    return currentValue;
                }
            };

            //卸载事件
            ImmyBox.prototype.destroy = function() {
                this.element.off('keyup change search', this.doQuery);
                this.element.off('keydown', this.doSelection);
                if (this.options.openOnClick) {
                    this.element.off('click', this.openResults);
                }
                this.element.removeClass(pluginName);
                this.queryResultArea.remove();
                $.removeData(this.element[0], "plugin_" + pluginName);
                objects = objects.filter((function(_this) {
                    return function(o) {
                        return o !== _this;
                    };
                })(this));
            };

            return ImmyBox;

        })();


        // $('html').on('click', function(e) {

        // ImmyBox.prototype.hmtlclock(e);

        // });



        // ImmyBox.prototype.hmtlclock = function(e) {


        //   var o, _i, _len;
        //   for (_i = 0, _len = objects.length; _i < _len; _i++) {
        //     o = objects[_i];        
        //     if(!o.reselects(e)){

        //       $("."+pluginName+ "_results").remove();
        //      return false;
        //     } 
        //     o.revert();
        //   }


        // }



        $(window).on('resize scroll', function() {
            var o, _i, _len;
            for (_i = 0, _len = objects.length; _i < _len; _i++) {
                o = objects[_i];
                if (o.queryResultAreaVisible) {
                    o.reposition();
                }
            }
        });


        function re_choices(options) {

            if (options.textname && options.valuename) {
                var srcchoices = options.choices;
                var newchoices = [];
                for (var keys in srcchoices) {
                    var itmes = srcchoices[keys];
                    if (itmes[options.textname] && itmes[options.valuename])
                        newchoices.push({
                            text: itmes[options.textname],
                            value: itmes[options.valuename]
                        });
                }

                options.choices = newchoices;

            }

            return options;


        }




        $.fn[pluginName] = function(options) {
            var args, outputs;
            args = Array.prototype.slice.call(arguments, 1);
            outputs = [];

            this.each(function() {
                var method, newObject, plugin;


                if ($.data(this, "plugin_" + pluginName)) {
                    if ((options != null) && typeof options === 'string') {
                        plugin = $.data(this, "plugin_" + pluginName);


                        method = re_choices(options);

                        if (__indexOf.call(plugin.publicMethods, method) >= 0) {
                            outputs.push(plugin[method].apply(plugin, args));
                        } else {
                            throw new Error("" + pluginName + " has no method '" + method + "'");
                        }
                    }
                } else {

                    options = re_choices(options);

                    newObject = new ImmyBox(this, options);

                    objects.push(newObject);
                    outputs.push($.data(this, "plugin_" + pluginName, newObject));

                    if (options.Defaultselect) {

                        newObject.selectChoiceByValue(options.Defaultselect);
                        newObject._val = options.Defaultselect;

                    }

                }




                $(this).focusout(function() {

                     var objdom = newObject.queryResultArea;
                    if(options.no_clearinputval && typeof options.no_clearinputval == "boolean"){
                          if(typeof settimes!="undefined" && settimes!=""){
                            window.clearTimeout(settimes);
                            objdom.remove();
                            return false;
                          }
                    }

                    settimes = setTimeout(function() {
                       
                        if (objdom.length > 0) {
                            objdom.remove();
                            newObject.revert();
                        }

                    }, 300);


                });


            });


            return outputs;
        };




    })(jQuery, window, document);

}).call(this);
