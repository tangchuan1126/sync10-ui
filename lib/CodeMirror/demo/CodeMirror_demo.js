define(function() {

//code


 	 $("body").show();

 	  if($("#texth100px").length > 0){

 	  	var texth100px = $("#texth100px")[0];

			 var est=CodeMirror.fromTextArea(texth100px, {
				lineNumbers: true,
				lineWrapping: true,
				extraKeys: {
					"Ctrl-Q": function(cm) {
						foldFunc_html(cm, cm.getCursor().line);
					}
				},
				matchBrackets: true,
				autoCloseBrackets: true,
				mode: "application/ld+json"
			});


			var esthlLine1 = est.addLineClass(0, "background", "activeline");
			est.on("cursorActivity", function() {

				var cur = est.getLineHandle(est.getCursor().line);

				if (cur != esthlLine1) {
					est.removeLineClass(esthlLine1, "background", "activeline");
					esthlLine1 = est.addLineClass(cur, "background", "activeline");
				}


			});


        }
			 //------------------    
       if($("#codereadOnlybox").length > 0){
       	
         $("#jsoninfo").removeClass('tab-pane');
          var codereadOnlybox=$("#codereadOnlybox")[0]; 
       var readOnly=CodeMirror.fromTextArea(codereadOnlybox, {
				lineNumbers: true,
				mode: "javascript",
				lineWrapping: true,
				readOnly: true
			});

     	var readOnlyhlLine = readOnly.addLineClass(0, "background", "activeline");
			readOnly.on("cursorActivity", function() {

				var cur = readOnly.getLineHandle(readOnly.getCursor().line);

				if (cur != readOnlyhlLine) {
					readOnly.removeLineClass(readOnlyhlLine, "background", "activeline");
					readOnlyhlLine = readOnly.addLineClass(cur, "background", "activeline");
				}

			});

		 $("#jsoninfo").addClass('tab-pane');	

       }


			var jsondata = $("#jsondata")[0];
			var editor1 = CodeMirror.fromTextArea(jsondata, {
				lineNumbers: true,
				lineWrapping: true,
				extraKeys: {
					"Ctrl-Q": function(cm) {
						foldFunc_html(cm, cm.getCursor().line);
					}
				},
				matchBrackets: true,
				autoCloseBrackets: true,
				mode: "application/ld+json"
			});


			var hlLine1 = editor1.addLineClass(0, "background", "activeline");
			editor1.on("cursorActivity", function() {

				var cur = editor1.getLineHandle(editor1.getCursor().line);

				if (cur != hlLine1) {
					editor1.removeLineClass(hlLine1, "background", "activeline");
					hlLine1 = editor1.addLineClass(cur, "background", "activeline");
				}


			});



			//-------------------

			

			var txtbox = $("#code")[0];
			var editor = CodeMirror.fromTextArea(txtbox, {
				lineNumbers: true,
				mode: "javascript",
				lineWrapping: true
			});

			var hlLine = editor.addLineClass(0, "background", "activeline");
			editor.on("cursorActivity", function() {

				var cur = editor.getLineHandle(editor.getCursor().line);

				if (cur != hlLine) {
					editor.removeLineClass(hlLine, "background", "activeline");
					hlLine = editor.addLineClass(cur, "background", "activeline");
				}


			});



			$("#run").click(function() {

				$("#Compond_box").empty();

				var jsondtas = editor1.getValue().replace(/^\s*/, "");

				var codesirng = editor.getValue().replace(/^\s*/, "");
				codesirng = codesirng.replace('$("#jsondata").val()', jsondtas);

				//var codes = '(function(){require(["jquery","lib/Navigation_Search/NavigationMenu","art_Dialog/dialog-plus"],function($,NavigationMenu,artdialog){$(function(){' + codesirng + '});})}())';

				window.eval(codesirng);

			});

			
});