// Expand --requirejs.config
requirejs.config({
    paths: {
        "select2.full": "/Sync10-ui/lib/select2.full/select2.full",
        "select2.mincss": "/Sync10-ui/lib/select2.full/select2.min",
        "prettifycss": "/Sync10-ui/lib/select2.full/prettify",
        "mCustomScrollbarcss": "/Sync10-ui/lib/mCustomScrollbar/css/mCustomScrollbar.min"
    }
});

//

require(['/Sync10-ui/requirejs_config.js'], function() {

    //Load css
    require([
        "require_css!Font-Awesome",
        "require_css!select2.mincss",
        "require_css!prettifycss",
        "require_css!mCustomScrollbarcss"
    ], function() {


        //Load js
        require(["jquery", "select2.full", "mCustomScrollbar"], function($) {


            $(function() {


            	//========1,======================

                //js-example-tags--select2
                //------------------------
                var _example_tags = $("#Model_tages")
                _example_tags.select2({
                    tags: true
                });

                _example_tags.on("change", function(e) {
                    var select = $(e.target);
                    chengeation(select);

                });



                //search_title--select2
                //------------------------
                var _search_title = $("#search_title")
                _search_title.select2({
                    tags: true
                });

                _search_title.on("change", function(e) {
                    var select = $(e.target);
                    chengeation(select);

                });

//========================end 1=============================


                function chengeation(select) {


                    var pbox = select.next(".select2-container--default").eq(0);
                    var ul = pbox.find(".select2-selection__rendered").eq(0);
                    //ul.height(34);
                    var liall = ul.find("li:not('.select2-search--inline')");
                    //

                    window.setTimeout(function() {

                        if (liall.length > 0) {
                            var liallwidth = 80;
                            for (var lival = 0; lival < liall.length; lival++) {

                                var liobj = liall.eq(lival);
                                var liw = liobj.width();
                                liallwidth = liallwidth + liw;
                            }
                            if (liallwidth > 100) {
                                ul.width(liallwidth);
                                $(".select2-selection__rendered").width("auto");

                            }

                        }



                    }, 300);
                }

                //----------------------------------------------




                var multiplestyle = $('<style type="text/css">.select2-container--default .select2-selection--multiple{border-bottom-left-radius: 0px;border-top-left-radius: 0px;}.multiplestyle{overflow: hidden;height: 34px;  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075); box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}</style>');
                $("body").prepend(multiplestyle);


                //----------------------select text Scrollbar demo 1 -------------
                var selct2_def0 = _example_tags.next(".select2-container--default");
                var selectbox1 = selct2_def0.find(".select2-selection--multiple").eq(0);
                selectbox1.addClass('multiplestyle');
                selectbox1.mCustomScrollbar({
                    axis: "x",
                    advanced: {
                        autoExpandHorizontalScroll: true
                    }
                });


                //----------------------select text Scrollbar demo 2 -------------
                var selct2_def1 = _search_title.next(".select2-container--default");
                var selectbox2 = selct2_def1.find(".select2-selection--multiple").eq(0);
                selectbox2.addClass('multiplestyle');
                selectbox2.mCustomScrollbar({
                    axis: "x",
                    advanced: {
                        autoExpandHorizontalScroll: true
                    }
                });


                $(".mCSB_horizontal>.mCSB_container").css("margin-bottom", "0px");
                $(".mCSB_draggerContainer").css({
                    "height": "10px",
                    "top": "25px"
                });




            });

        });


    });

});

