requirejs.config({
    paths: {
        "oso.lib": "/Sync10-ui/lib",
        "require_css": "/Sync10-ui/lib/require-css/css",
        "jquery1.9": "/Sync10-ui/lib/jquery1.9/jquery-1.9.0",
        "Font-Awesome": "/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min",
        "bootstrap.datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "bootstrap.datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "moment":"/Sync10-ui/bower_components/moment/min/moment.min"
    },
    shim: {
        'bootstrap.datetimepicker': {
            deps: ['jquery1.9', 'require_css!bootstrap.datetimepicker-css']
        }
    }
});


require(["oso.lib/nprogress/nprogress.min","jquery1.9","require_css!oso.lib/nprogress/nprogress", "require_css!bootstrap-css/bootstrap.min"],
    function(NProgress) {

        //jquery1.9--版本不要写callback方式

       // console.log($);

        $("body").show();

        NProgress.start();

        var cssfileall = [
            "require_css!Font-Awesome",
            "moment",           
            "oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
            "bootstrap.datetimepicker"
        ];


        // "bootstrap.datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN"


        require(cssfileall, function() {



            $(function() {

                var mydates = $('#datetimepicker12').Acars_datetimepicker({
                    inline: true,
                    icons: {
                        previous: 'fa fa-chevron-circle-left',
                        next: 'fa fa-chevron-circle-right'
                    },
                    toolbar: false,
                    format: 'YYYY-MM-DD',
                    tdcellclick: function(dates) {
                        //console.log(dates);
                    },
                    Initialization: function(setbuttondom) {

                        setbuttondom.show();
                        setbuttondom.click(function() {
                            //console.log(2233444);
                        });

                    }

                });



                //外部取得时间
                //var setdatas=mydates.data("DateTimePicker");
                // console.log(setdatas.date().toDate());


                //startView 3进入月，2日期，1小时，0分钟
                //到日期就关闭
                // $(".form_datetime").eq(0).datetimepicker({
                // format: "yyyy-mm-dd",
                //   autoclose:1,
                //   minView: 2,
                //   forceParse: 0,
                //   bgiconurl:"",//自定义时图片url|默认时bgicons.png|要空白时不要bgiconurl对象         
                // }).on('changeDate', function(ev){

                //       console.dir(ev.delegateTarget.value);

                //   });


                //到日期就关闭
                var formdoms = $(".form_datetime").eq(0);

                formdoms.datetimepicker({
                    format: "yyyy-mm-dd",
                    autoclose: 1,
                    minView: 2,
                    forceParse: 0,
                    bgiconurl: "", //,//自定义时图片url|默认时bgicons.png|要空白时不要bgiconurl对象
                    ValEmptyBtn: function() {
                        console.log(6666);
                    }
                });


              






                $(".datetimepicker_box .form_datetime").datetimepicker({
                    format: 'yyyy-mm-dd hh:00',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 1,
                    forceParse: 0,
                    bgiconurl: "../Aicons.png"
                });

                //选择日期后小时正00时就关闭
                $('.form_date').datetimepicker({
                    format: 'yyyy-mm-dd hh:00',
                    autoclose: 1,
                    minView: 1,
                    forceParse: 0
                });

                //给日期时间选择器设置一个新的起始日期
                //$('#datetimepicker').datetimepicker('setStartDate', '2012-01-01');
                //给日期时间选择器设置结束日期。
                //$('#datetimepicker').datetimepicker('setEndDate', '2012-01-01');

                $('#a0').datetimepicker({
                    format: 'yyyy-mm-dd hh:00',
                    autoclose: 1,
                    minView: 1,
                    forceParse: 0
                }).on('changeDate', function(ev) {

                    $('#b0').val("");



                    $('#b0').datetimepicker('setStartDate', ev.date);

                    //console.dir(ev.date);

                });


                $('#b0').datetimepicker({
                    format: 'yyyy-mm-dd hh:00',
                    autoclose: 1,
                    minView: 1,
                    forceParse: 0
                });



                NProgress.done();


            });
            // window lod end



        });
        // codemirror end

    });
// requirejs_config end
