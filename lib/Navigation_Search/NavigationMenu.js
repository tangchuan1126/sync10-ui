define(["jquery", "JSONTOHTML", "underscore", "mCustomScrollbar"], function($, JSONTOHTML, underscore, mCustomScrollbar) {

    var w_h_ = $(window).outerHeight();

    var transform = {
        li: {
            "tag": "li",
            "class": function(obj, index) {

                if (obj.listyle) {
                    return "li_itme " + obj.listyle;
                } else {
                    return "li_itme";
                }

            },
            "id": function(obj, index) {
                return "itme" + obj.id
            },
            "children": [{
                    "tag": "a",
                    "href": "javascript:void(0)",
                    "html": function(obj, index) {

                        var icos = obj.ico;
                        if (icos == "" || typeof icos == "undefined") {
                            icos = "fa-file-excel-o";
                        }

                        var i = '<i class="fa ' + icos + ' fa-fw"></i>';

                        var txt = " <span class='a_text'>" + obj.title + "</span>";
                        var span = '<span class="fa arrow"></span>';

                        return i + txt + span;
                    }
                }, {
                    "tag": "ul",
                    "class": function(obj) {

                        var claname = "nav nav-second-level collapse";


                        if (obj.listyle) {

                            claname = "nav nav-second-level";

                        }

                        if (!obj.open && obj.open != null && typeof obj.open != "undefined") {

                            claname = "nav nav-second-level in";

                        }

                        return claname;
                    },
                    "html": function(obj) {

                        return (JSONTOHTML.transform(obj.children, transform.ulli));
                    }

                }

            ]
        },
        ulli: {
            "tag": "li",
            "html": function(obj, index) {

                var icos = obj.ico;
                var i = "";
                if (icos != "" && typeof icos != "undefined" && /^fa-.*$/.test(icos)) {
                    i = '<i class="fa ' + icos + ' fa-fw"></i>';
                } else {
                    i = '<i class="fa fa-fw fa-file-o"></i>';
                }

                var txt = " <span class=''>" + obj.title + "</span";
                //var span = '<span class="fa arrow"></span>';

                var _ahtml = i + txt;

                return '<a title="' + obj.title + '" href="' + obj.url + '" onclick="return false">' + _ahtml + '</a>';
            }
        }


    };

    var _int = {
        jsondata: {},
        itme_sub_jsondata: {},
        _default: {},
        ui: function(dom, Search) {
            var uihtml = '<ul class="nav">';
            uihtml += '<li class="sidebar-search">';
            uihtml += '<div class="input-group custom-search-form">';
            uihtml += '<input type="text" class="form-control" placeholder="Search...">';
            uihtml += '<span class="input-group-btn">';
            uihtml += '<button class="btn btn-default" type="button">';
            uihtml += '<i class="fa fa-search"></i>';
            uihtml += '</button>';
            uihtml += '</span>';
            uihtml += '</div>';
            uihtml += '<a href="#" id="filter-clear" class="fa fa-times"></a>';
            uihtml += '<!-- /input-group -->';
            uihtml += '</li>';
            uihtml += '</ul>';
            if (!Search) {
                uihtml = "";
            }
            uihtml += '<div class="mCustomScrollbar side-menubox">';
            uihtml += '<div class="lodingicobox">';
            uihtml += '<i class="fa fa-3x fa-refresh fa-spin"></i>';
            uihtml += '</div>';
            uihtml += '</div>';

            dom.html(uihtml);

        },
        css: function(dom, Search) {

            var cssfileall = [
                "require_css!bootstrap-css/bootstrap.min",
                "require_css!oso.lib/bootstrap/css/plugins/timeline.css",
                "require_css!oso.lib/bootstrap/css/plugins/metisMenu/metisMenu.min.css",
                "require_css!oso.lib/bootstrap/css/sb-admin-2.css",
                "require_css!Font-Awesome",
                "require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css"
            ];

            require(cssfileall);

        },
        int: function(obj) {

            _int.css();
            var colidsring = obj.renderTo.replace(/#|\.|\s/g, "");
            _int._default[colidsring] = obj;
            _int.int.prototype._id_ = colidsring;
            _int.int.prototype.ss_mousewheel = false;
            return {
                render: _int.render,
                on: _int.events
            };
        },
        MenuSearch: function(componid) {

            var _def = _int._default[componid];
            var __dom = _def.__dom;
            //遍历一个节点下所有-查找
            var Rejsondata = [],
                nop_obj = {};

            function recursive(arrydata, _val, pobj) {

                var remenuall = {};
                _.map(arrydata, function(objval, key) {


                    var _searchval = new RegExp(_val, 'i');
                    var objtitle = objval.title;

                    var p_obj = {};
                    //一匹配上就进入
                    if (_searchval.test(objtitle)) {

                        remenuall = {
                            "id": objval.id,
                            "ico": objval.ico,
                            "title": objtitle,
                            "url": objval.url
                        };

                        //是否是子节点的递归进来
                        if (pobj) {

                            if (pobj.id) {
                                pobj["listyle"] = "active";

                                if (pobj.children) {
                                    pobj.children.push(remenuall);
                                } else {
                                    pobj["children"] = [];
                                    pobj.children.push(remenuall);
                                }

                                remenuall = pobj;

                            } else {


                                if (nop_obj.children) {
                                    nop_obj.children.push(remenuall);
                                } else {
                                    nop_obj["listyle"] = "active";
                                    nop_obj["children"] = [];
                                    nop_obj.children.push(remenuall);
                                }

                                remenuall = nop_obj;

                            }

                        } else {
                            //不是递归进来，保留匹配的级节点
                            // objval.opne=false;
                            objval["open"] = false;
                            Rejsondata.push(objval);

                        }


                    };

                    //匹配子节点--有子时节点时进入递归

                    if (objval.children) {

                        if (!remenuall.id) {
                            nop_obj = {
                                "id": objval.id,
                                "ico": objval.ico,
                                "title": objtitle,
                                "url": objval.url
                            }
                        }

                        recursive(objval.children, _val, remenuall);

                    }



                });


                //一个节点的结束
                if (remenuall.children) {

                    //存在时
                    if (Rejsondata.length > 0) {

                        var itmefnet = false;

                        //遍历存在的父节点
                        for (var allarrykey in Rejsondata) {
                            var itmedata = Rejsondata[allarrykey];

                            //如果存在就不写入
                            if (itmedata.id == remenuall.id) {
                                itmefnet = true;
                                break;
                            }
                        }

                        //不存在就创建新的
                        if (!itmefnet) {
                            Rejsondata.push(remenuall);
                        }


                    } else {
                        //父节点带子节点
                        Rejsondata.push(remenuall);
                    }


                    nop_obj = {};

                }

            }



            var _button = __dom.find(".input-group-btn");
            var _input = __dom.find(".form-control");

            var side_menu = __dom.find(".side-menu");
            var location = 0;

            _button.click(function() {
                openringpage();
                return false;
            });

            var delbtns = $("#filter-clear");

            delbtns.click(function() {

                var v_txt = _input.val();
                if (v_txt != "" && typeof v_txt != "undefined") {
                    _input.val("");
                    Rejsondata = [];
                    timeoutsring = setTimeout(function() {

                        side_menu.empty().html(JSONTOHTML.transform(_int.jsondata[componid], transform.li));
                        _int.setmeunevnts(componid, side_menu, false);

                        if (_def.MiniMenu)
                            _int._MiniMenu(componid);

                    }, 100);

                }
                delbtns.hide();
                return false;
            });


            _input.focus();

            var qurye_itme_titme = "";


            function quryeinput(_val) {


                Rejsondata = [];

                recursive(_int.jsondata[componid], _val);

                //本次搜索结果
                if (Rejsondata.length > 0) {

                    side_menu.empty().html(JSONTOHTML.transform(Rejsondata, transform.li));
                    location = 0;
                    //事件
                    _int.setmeunevnts(componid, side_menu, true);

                    //自动展开
                    if (qurye_itme_titme != "" && typeof qurye_itme_titme != "undefined") {
                        window.clearTimeout(qurye_itme_titme);
                    }

                    qurye_itme_titme = setTimeout(function() {

                        var __li_itme = side_menu.find('.li_itme');
                        if (__li_itme.length == 1 && !__li_itme.hasClass('active')) {
                            __li_itme.find("a").eq(0).click();
                            return false;
                        }
                    }, 200);


                } else {
                    //没匹配上清除
                    Rejsondata = [];
                    side_menu.empty();
                    return false;
                }




            }

            var timeoutsring = "";

            _input.on('input', function(e) {


                var _this = $(this);
                var _val = $.trim(_this.val());
                _val = _val.replace(/\/|\.|<|>/g, "");

                if (timeoutsring != "" && typeof timeoutsring != "undefined") {
                    window.clearTimeout(timeoutsring);
                }

                if (_val != "" && typeof _val != "undefined") {

                    timeoutsring = setTimeout(function() {
                        quryeinput(_val);
                        delbtns.show();

                    }, 200);


                } else {

                    Rejsondata = [];
                    timeoutsring = setTimeout(function() {

                        side_menu.empty().html(JSONTOHTML.transform(_int.jsondata[componid], transform.li));
                        _int.setmeunevnts(componid, side_menu, false);
                        delbtns.hide();

                    }, 200);

                }

                return false;

            }).keydown(function(e) {

                if (e.keyCode == 13) {
                    openringpage();
                    return false;
                }

                //后退问题
                var vals = $(this).val();
                if (e.keyCode == 8 && vals == "" || typeof vals == "undefined") {
                    e.preventDefault();
                    //window.event.returnValue = false;
                }

            }).keyup(function(e) {
                var _thisinput = $(this);
                var kcodes = e.keyCode;

                var ul_li = side_menu.find("ul li");

                var a_select_ = side_menu.find('.a_select');

                ul_li.find("a").removeClass("a_select");
                if (kcodes == 40 && location < (ul_li.length - 1)) {
                    location++;
                }

                var a_select_ = side_menu.find('.a_select');

                if (kcodes == 38 && location > 0) {
                    location--;
                }

                if (kcodes > -1 && location < ul_li.length) {


                    var __li = ul_li.eq(location);
                    var _aloction = __li.find("a");
                    _aloction.addClass("a_select");
                    // side_menu.focus();               

                }


                var ullens = side_menu.find('ul');
                if (a_select_.length == 0 && kcodes == 40 && ullens.length > 0) {

                    var a__ = ullens.find('li a').eq(0);
                    if (a__ > 0) {
                        a__.addClass("a_select");
                    }

                }

                return false;


            });

            function openringpage() {

                var ul = side_menu.find("ul");
                var _this_a = "";
                var ligt0 = "";

                if (ul.find("li").length > 1 || ul.length > 1) {

                    return false;


                }



                if (ul.length == 1 && ul.find("li").length == 1) {

                    _this_a = ul.find("li a").eq(0);
                    ligt0 = ul.find("li").eq(0);


                } else {
                    _this_a = ul.find("li .a_select").eq(0);
                    ligt0 = _this_a.parents("li").eq(0);
                }

                _int._showMenuItem(componid, ligt0);

                return false;

            }

            if (_def.MiniMenu)
                _int._MiniMenu(componid);

        },
        re_mCustomScrollbar_h: function(bootmh) {

            var mcusto_h = $(window).outerHeight();
            var Scrollbar_tbody_h = mcusto_h - bootmh;
            $("#Compond_navbar .mCustomScrollbar").height(Scrollbar_tbody_h);
            $("#Compond_navbar").mCustomScrollbar("scrollTo", "top");
        },
        _MiniMenu: function(componid) {
            //2015-06-19 12：14===增加迷你菜单
            var _def = _int._default[componid];
            var __dom = _def.__dom;

            var topFrame = window.top.window.frames["iframe"].frames["topFrame"];
            var top_doc = $(topFrame.document);
            var windowtop_win_doc = $(window.top.document);

            //if (top_doc.find(".top_menuicosbtn").length < 1) {

            if (top_doc.find(".hovers").length < 1) {

                //top.html----btn
                var leftFrame = window.top.window.frames["iframe"].frames["leftFrame"];
                var mainFrm = window.top.window.frames["iframe"].frames["mainFrm"];
                //index.html

                var _dombody = windowtop_win_doc.find("body");


                var topbody = top_doc.find("body");

                //原来创建的div--top_menuicosbtn弃用
               // $('<style id="new_mini_style" type="text/css">.top_menuicosbtn{width: 30px;height: 28px;cursor: pointer;float: left;margin: 12px 0px 0px 10px;overflow: hidden;text-align: center;vertical-align: middle;color:#E1E1E1}.topselect{color:#5E5E5E}.top_menuicosbtn i{font-size: 23px;}</style>').appendTo(topbody);

                //$('<div class="top_menuicosbtn" accesskey="m"><i class="fa fa-medium"></i></div>').prependTo(topbody.find(".navbar-static-top"));

               // var top_menuicosbtn = top_doc.find(".top_menuicosbtn");

            //用logo做按钮-----------
             $('<style>nav .hovers{cursor: pointer}</style>').prependTo(topbody.find(".navbar-static-top"));
              
               var top_menuicosbtn = top_doc.find(".navbar-header");
               top_menuicosbtn.addClass('hovers');

               //-----------------------


                var leftFramedoc = $(leftFrame.document);
                var _side_menu = leftFramedoc.find(".navbar-default");


                var bootmh = 88;
                _def.clicktmes = "";

                function clert_click_m(bootmh) {

                    if (_def.clicktmes != "" && typeof _def.clicktmes != "undefined") {
                        window.clearTimeout(_def.clicktmes);
                    }

                    _def.clicktmes = setTimeout(function() {

                        _int.re_mCustomScrollbar_h(bootmh);

                    }, 200);

                }


                top_menuicosbtn.click(function(event) {

                    var _thisbtn = $(this);
                    _def.jq_li_itme = "";

                    //var icons_i=_thisbtn.find("i");

                    var jqleft_document = $(leftFrame.document);
                    var Compond_navbar = jqleft_document.find("#Compond_navbar");


                    if (mainFrm && !Compond_navbar.hasClass('minileft') && mainFrm.cols == "240,*") {
                        mainFrm.cols = "60,*";
                        Compond_navbar.addClass('minileft');
                        //_thisbtn.addClass('topselect');
                        _def.Set_Mini = true;
                        var delbtns = jqleft_document.find("#filter-clear");
                        if (delbtns.val() != "" || typeof delbtns.val() != "undefined") {
                            //delbtns.val("");
                            delbtns.click();
                        }
                        //如果有打开的---关闭
                        var p_active = jqleft_document.find(".active");
                        if (p_active.length > 0) {

                            var openul = p_active.find("ul").eq(0);

                            if (openul.length > 0) {

                                openul.slideUp(300, function() {

                                    var _thisul = $(this);
                                    var _thispli = _thisul.parents("li").eq(0);
                                    _thispli.removeClass("active");

                                });

                            }

                        }

                        bootmh = 24;

                        clert_click_m(bootmh);

                       // icons_i.removeClass('fa-medium').addClass('fa-facebook');

                        return false;

                    }

                    if (mainFrm.cols == "60,*") {

                        Compond_navbar.removeClass('minileft');
                        mainFrm.cols = "0,*";
                        //_thisbtn.removeClass('topselect');                        
                        _def.Set_Mini = false;
                        bootmh = 88;
                        clert_click_m(bootmh);

                       // icons_i.removeClass('fa-facebook').addClass('fa-columns');

                        return false;

                    }

                    if (mainFrm.cols == "0,*") {

                        Compond_navbar.removeClass('minileft');
                        mainFrm.cols = "240,*";
                       // _thisbtn.removeClass('topselect');
                        _def.Set_Mini = false;
                        bootmh = 88;
                        clert_click_m(bootmh);

                        //icons_i.removeClass('fa-columns').addClass('fa-medium');
                        
                        return false;

                    }


                });

                //index.html
                //---------------------------------
                _def.minu_boxtimes = "", _def.mousertimes = "", _def.jq_li_itme = "";



                $('<style>.minu_box_div{z-index:9999;display: none;left:60px;position: absolute; border:1px solid #ccc;padding: 0px;border-left-width: 0px;background: #fff}.minu_box_div ul{list-style: none;padding: 0px 0px;width: 250px;background: #fff;margin-bottom: 0px}.minu_box_div ul li{width: 100%}.minu_box_div .mini_title_div{width:100%; height: 40px;overflow:hidden;background: #EEEEEE;border-bottom: 1px solid #e7e7e7;position: relative;left: -2px;border-right:2px #EEE solid}.mini_title_div span{height:40px;line-height: 40px;display:inline-block}.minu_box_div ul a:hover{background:#EEEEEE}.minu_box_div ul a:hover span{font-weight: bold;}.minu_box_div ul a,.minu_box_div ul a:hover,.minu_box_div ul a:active{font-size: 14px;outline: 0;line-height: 36px;border-bottom: dotted 1px #ccc;color: #5E5E5E;height:36px;display: inline-block;width: 100%;text-align: left;padding:0px 0px}.minu_box_div ul a i{margin-left: 5px;}.minu_box_div ul a span{margin-right: 5px}.mini_body_div{background: #fff}.mini_body_div .left_box{border-right:1px solid #EBEBEB}.mini_title_div i{top:3px;padding: 10px 8px;cursor: pointer;display:inline-block;position:absolute;right:0px;vertical-align:middle}.mini_title_div label{vertical-align:middle;width:89%;margin-left:5px;margin-top:4px;position:replace}.mini_title_div label input{height:32px;line-height:32px;width:100%;display:inline-block;border-radius: 4px;border: 1px solid #ccc;text-indent: 5px;}.mini_title_div label a{display:none;position:absolute;right: 35px;top: 7px;padding: 5px 6px;}</style>').appendTo(_dombody);

                $('<div class="minu_box_div"><div class="mini_title_div"><span></span><label class="hide"><input class="form-control" type="text" placeholder="Search..." /><a href="#" class="fa fa-times"></a></label><i class="fa fa-search"></i></div><div class="mini_body_div clearfix"></div></div>').appendTo(_dombody);


                _def.minubox = windowtop_win_doc.find(".minu_box_div");
                _def.mini_title = _def.minubox.find('.mini_title_div');
                _def.mini_body = _def.minubox.find('.mini_body_div');

                //mini搜索--ui
                var chek_i = _def.mini_title.find("i");
                chek_i.click(function(event) {
                    event.stopPropagation();
                    minishowinput();
                });

                _def.mini_title.dblclick(function(event) {
                    event.stopPropagation();
                    minishowinput();
                });

                function clerminuhide() {
                    if (_def.minu_boxtimes != "" && typeof _def.minu_boxtimes != "undefined") {
                        clearTimeout(_def.minu_boxtimes);
                    }
                }


                function minishowinput() {

                    if (_def.mini_title.find("label").css("display") == "inline-block") {
                        _def.mini_title.find("label").hide();
                        _def.mini_title.find("span").show();
                        _def.mini_body.find(".kaiqurybox").empty().hide();
                        _def.mini_body.find('ul').show();
                        return false
                    }

                    clerminuhide();

                    var p_w_ = _def.mini_title.outerWidth() - 42;

                    var click_i_this = $(this);
                    _def.mini_title.find("span").hide();
                    _def.mini_title.find("label").css({
                        width: p_w_ + "px",
                        display: "inline-block"
                    });
                    _def.mini_title.find("label input").val("").focus();

                    return false

                }

                //mini搜索--input
                var mini_input_stetime = "";
                _def.mini_title.find("label input").on("input", function(e) {


                    var minit_input_this = $(this);
                    var _val = $.trim(minit_input_this.val());
                    _val = _val.replace(/\/|\.|<|>/g, "");

                    if (mini_input_stetime != "" && typeof mini_input_stetime != "undefined") {
                        window.clearTimeout(mini_input_stetime);
                    }
                    if (_val != "" && typeof _val != "undefined") {

                        _def.mini_title.find("label a").css("display", "inline-block");
                        mini_input_stetime = setTimeout(function() {

                            _int.mini_input_query(_val, componid);

                        }, 300);

                    } else {
                        _def.mini_title.find("label a").css("display", "none");
                        _def.mini_body.find(".kaiqurybox").empty().hide();
                        _def.mini_body.find('ul').show();

                        var _ullen = _def.mini_body.find('ul').length * 250;
                        _def.mini_title.find("label").width(_ullen - 40);
                        // console.log(12);

                    }


                });

                _def.mini_title.find("label a").click(function(event) {
                    $(this).css("display", "none");
                    _def.mini_title.find("label input").val("").focus();
                    _def.mini_body.find(".kaiqurybox").empty().hide();
                    _def.mini_body.find('ul').show();
                    var _ullen = _def.mini_body.find('ul').length * 250;
                    _def.mini_title.find("label").width(_ullen - 40);
                    return false;
                });

                //右边触发
                _def.minubox.hover(function() {


                    clerminuhide();

                    $(this).show();

                    return false

                }, function() {

                    clerminuhide();

                    _def.minu_boxtimes = setTimeout(function() {

                        if (_def.jq_li_itme.length > 0) {
                            _def.jq_li_itme.removeClass('active1');
                        }

                        _def.minubox.hide();

                    }, 300);

                    return false

                });

            }

            _def.minu_hover_show = "";
            //左边触发
            var li_itme = __dom.find(".li_itme");
            li_itme.undelegate();
            li_itme.hover(function() {
                if (_def.minubox.length < 1)
                    _def.minubox = windowtop_win_doc.find(".minu_box_div");

                if (_def.mini_title.find("label").css("display") == "inline-block") {
                    _def.mini_title.find("span").show();
                    _def.mini_title.find("label").hide();
                }

                var _this_lis = $(this);

                if (_def.minu_hover_show != "" && typeof _def.minu_hover_show != "undefined")
                    window.clearTimeout(_def.minu_hover_show);

                _def.minu_hover_show = setTimeout(function() {


                    _int._hoverfshow(_this_lis, componid);

                    _def.jq_li_itme = _this_lis;

                }, 50);

                return false

            }, function() {

                if (_def.minubox.length < 1)
                    _def.minubox = windowtop_win_doc.find(".minu_box_div");

                var _this_lis = $(this);
                if (_this_lis.parents(".minileft").length > 0) {

                    //退出mini  
                    clerminuhide();

                    _def.minu_boxtimes = setTimeout(function() {

                        _def.jq_li_itme = "";
                        _this_lis.removeClass('active1');
                        _def.minubox.hide();

                    }, 300);

                }

                return false

            }).mouseover(function() {

                var _this = $(this);
                if (_this.parents(".minileft").length > 0) {
                    clerminuhide();
                }

            });

        },
        mini_input_query: function(_val, componid) {
            //查找subitme

            var datas = _int.itme_sub_jsondata[componid];
            var keyitmeall = [];
            datas.filter(function(itme) {
                var sokey = new RegExp(_val, 'i');

                if (sokey.test(itme.title)) {
                    keyitmeall.push(itme);
                }

            });



            if (keyitmeall.length > 0) {
                var _def = _int._default[componid];
                var mini_body = _def.mini_body;
                var mini_body_ul_li_len = mini_body.find('ul').eq(0).find("li").length;
                var mini_body_h = mini_body_ul_li_len * 37;
                mini_body.find("ul").hide();

                var kaiqurybox = mini_body.find(".kaiqurybox");

                var ullen = mini_body.find("ul").length - kaiqurybox.find('ul').length;
                //console.log(ullen);

                if (kaiqurybox.length < 1) {
                    $('<div class="kaiqurybox clearfix"></div>').appendTo(mini_body);
                    var kaiqurybox = mini_body.find(".kaiqurybox")
                    kaiqurybox.delegate("a", "click", function() {

                        var _new_a_this = $(this);

                        var itmedata = {
                            url: _new_a_this.attr("href"),
                            itme: _new_a_this.parents("li").eq(0)
                        };

                        if (_def.showMenuItem) {
                            _def.showMenuItem(itmedata);
                        }
                        _def.minubox.hide();

                        _def.jq_li_itme = "";

                        return false

                    });
                }

                kaiqurybox = mini_body.find(".kaiqurybox");
                kaiqurybox.empty();
                //==================================
                var _i = 0,
                    topulcss = "",
                    allul = "";

                //每列最多能放下几个li
                var sowr_ = window.parseInt(mini_body_h / 37);

                var cols = 0,
                    mvusom = parseInt(keyitmeall.length / sowr_);

                //补高度---li
                var ul_style_h = "",
                    min_w = 0,
                    kaiulmaiw = false;
                if (keyitmeall.length < sowr_ && keyitmeall.length > 0) {
                    var set_li_h = (sowr_ - keyitmeall.length) * 37;
                    //这里判断是否title在底部
                    if (!_def.add_top_and_boottom)
                        ul_style_h = '<li style="height:' + set_li_h + 'px"></li>';

                    min_w = (ullen * 250) + 1;
                } else {
                    kaiulmaiw = true;
                }


                for (var subkey in keyitmeall) {

                    var subitmeobj = keyitmeall[subkey];

                    var icos = subitmeobj.ico;
                    var isrinrs = "";
                    if (icos != "" && typeof icos != "undefined" && /^fa-.*$/.test(icos)) {
                        isrinrs = ' ' + icos;
                    } else {
                        isrinrs = ' fa-file-o';
                    }

                    var sorwr = parseInt(_i % sowr_);
                    var ecssr = "";

                    if (sorwr == 0 && _i > 0) {

                        topulcss = " left_box";

                        if (_i > 1 && cols < mvusom) {
                            ecssr = " " + topulcss;
                        }

                        allul += '</ul><ul class="a_new_itme_ul pull-left' + ecssr + '">' + '<li><a title="subitmeobj.title" href="' + subitmeobj.url + '" onclick="return false"><i class="fa' + isrinrs + ' fa-fw"></i> <span class="">' + subitmeobj.title + '</span></a></li>';

                    } else {
                        allul += '<li><a title="subitmeobj.title" href="' + subitmeobj.url + '" onclick="return false"><i class="fa' + isrinrs + ' fa-fw"></i> <span class="">' + subitmeobj.title + '</span></a></li>';
                    }

                    _i++;


                };

                if (min_w > 0) {
                    kaiqurybox.css("min-width", min_w + "px");
                }

                kaiqurybox.html('<ul class="a_new_itme_ul pull-left  left_box">' + ul_style_h + allul + '</ul>').show();

                if (kaiulmaiw)
                    _def.mini_title.find("label").width(kaiqurybox.outerWidth() - 40);


                //事件


            }


        },
        minu_offset: function(a_this, sub_ul_obj, componid) {
            //处理朝向问题
            var _def = _int._default[componid];
            var win_o_h = $(window).outerHeight();
            var minubox = _def.minubox;
            //头
            var mini_title = _def.mini_title;
            //身            
            var mini_body = _def.mini_body;

            //取出所有的a           
            var sub_li_all = sub_ul_obj.find("li");

            var _top_ = 0;

            //右边总高 
            var r_minu_box_h = sub_li_all.length * 37;
            //单列模式下的高度
            var r_minu_box0_h = r_minu_box_h - 50;


            var a_offset = a_this.offset();
            _top_ = a_offset.top + 50;


            //当前按钮为中心

            //按钮的屏幕上部空间高度---+ title_高40
            var top_box_h = a_offset.top + 40;
            var a_top_ = a_offset.top;

            //按钮的屏幕下部空间高度---+ 底部url预留高22
            var bottom_box_h = (win_o_h - top_box_h) - 22;

            //谁的空间小就把title放在谁那一边
            //minu_box_div == true上，false下
            var add_top_and_boottom = true;
            if (bottom_box_h > top_box_h) {
                //下半部大
                add_top_and_boottom = true;
            } else {
                //上不部大
                add_top_and_boottom = false;
            }

            _def.add_top_and_boottom = add_top_and_boottom;

            //默认单列
            var ul_html = "";
            if (sub_ul_obj.find('li').length > 0)
                ul_html = "<ul class='a_new_itme_ul pull-left'>" + sub_ul_obj.html() + "</ul>";

            //改变右边——top,位置
            if (!add_top_and_boottom) {
                //title在下
                _top_ = (a_top_ - r_minu_box0_h);
                mini_title.insertAfter(mini_body);
                //上部空间不够时--拆分多列
                if (r_minu_box_h > top_box_h) {

                    var rows = parseInt(a_top_ / 37);
                    ul_html = _int.Splitcolumns(sub_li_all, rows);

                    _top_ = (a_top_ + 50) - (rows * 37);

                }


            } else {
                //title在上
                mini_title.insertBefore(mini_body);
                //下部空间不够时--拆分多列
                if (r_minu_box_h > bottom_box_h) {

                    //能放下的高度个数
                    var rows = parseInt(bottom_box_h / 37);
                    ul_html = _int.Splitcolumns(sub_li_all, rows);

                }

            }


            return {
                css: {
                    "display": "block",
                    "top": _top_ + "px"
                },
                html: ul_html
            };

        },
        Splitcolumns: function(all_li, rows) {

            var allul = '';
            //all_li---所有Li
            //rows--每列能放几个
            var _i = 0,
                topulcss = "";
            var li_lens = all_li.length;
            var cols = 0,
                mvusom = parseInt(li_lens / rows);

            for (var likey in all_li) {

                var li_itmes = all_li[likey];

                if (_i == li_lens || typeof li_itmes == "number") {
                    break;
                }

                //在遍历中取模--产生列效果
                if (typeof li_itmes != "function" && typeof li_itmes.outerHTML != "undefined") {
                    var sorwr = parseInt(_i % rows);
                    var ecssr = "";

                    if (sorwr == 0 && _i > 0) {

                        cols++;

                        topulcss = " left_box";

                        if (_i > 1 && cols < mvusom) {
                            ecssr = " " + topulcss;
                        }

                        allul += '</ul><ul class="a_new_itme_ul pull-left' + ecssr + '">' + li_itmes.outerHTML;

                    } else {
                        allul += li_itmes.outerHTML;
                    }

                    _i++;
                }

            }

            return '<ul class="a_new_itme_ul pull-left' + topulcss + '">' + allul + '</ul>';
        },
        _hoverfshow: function(a_this, componid) {

            var _this = this;
            var _def = _int._default[componid];


            if (a_this == "" || typeof a_this == "undefined" || _def.minubox.length < 1)
                return false


            //show itme

            var __minileft = a_this.parents(".minileft");
            if (__minileft.length > 0 && !_int.int.prototype.ss_mousewheel) {

                var active1 = __minileft.find(".active1");
                if (active1.length > 0)
                    active1.removeClass('active1');

                var sub_ul_obj = a_this.find("ul");
                var this_a_span_html = a_this.find("a:first").find("span");
                var itme_id = a_this.attr("id");
                _def.minubox.attr("rel", itme_id);
                _def.mini_title.find("span").text(this_a_span_html.text());


                //处理朝向--------------------------                

                var sub_li_obj = _this.minu_offset(a_this, sub_ul_obj, componid);

                var div_css = sub_li_obj.css;
                var sub_html = sub_li_obj.html;
                //--------------------------------------

                _def.mini_body.html(sub_html);

                _def.minubox.css(div_css);
                a_this.addClass('active1');

                var _new_itme_ul = _def.mini_body.find(".a_new_itme_ul");

                if (_new_itme_ul.length > 0) {

                    var new_a_all = _def.mini_body.find("a");
                    new_a_all.click(function() {

                        var _new_a_this = $(this);

                        var itmedata = {
                            url: _new_a_this.attr("href"),
                            itme: _new_a_this.parents("li").eq(0)
                        };

                        if (_def.showMenuItem) {
                            _def.showMenuItem(itmedata);
                        }
                        _def.minubox.hide();

                        _def.jq_li_itme = "";
                        a_this.removeClass('active1');

                        return false;

                    });
                }

            }
            return false

        },
        _showMenuItem: function(componid, itmeobj) {

            var _def = _int._default[componid];

            if (_def.showMenuItem) {

                var a_btn = itmeobj.find("a");
                var selectitme = $(".selectitme");
                if (selectitme.length > 0) {
                    selectitme.removeClass('selectitme');
                }

                itmeobj.addClass('selectitme');


                var urltxt = a_btn.attr("href");

                if (urltxt == "#" || urltxt == "" || typeof urltxt == "undefined") {

                    if (_def.menuItemError) {
                        var errordata = {
                            itmedata: urltxt,
                            error: "路径错误"
                        };
                        _def.menuItemError(errordata);
                    }

                } else {

                    var itmedata = {
                        url: urltxt,
                        itme: itmeobj
                    };

                    if (_def.showMenuItem) {
                        _def.showMenuItem(itmedata);
                    }

                }

            }

        },
        setmeunevnts: function(componid, meunobj, open) {


            var li = meunobj.find("li").has("ul");

            var ul_a = li.find("ul a");

            //itme -li - a
            ul_a.click(function() {

                var _this_a = $(this);

                var p_li = _this_a.parents("li").eq(0);

                _int._showMenuItem(componid, p_li);

                return false;

            });

            if (!open) {
                li.find("ul").toggleClass("in").hide();
            } else {

                var notactive = li.not(".active");
                notactive.find(".in").hide();

                var notevtnli = li.not("li[class='']");
                notevtnli.removeClass("in");
            }

            _int.itmesliding(componid, li);


        },
        itmesliding: function(componid, li) {

            var _a = li.children("a");
            _a.off().unbind();

            var _def = _int._default[componid];


            _a.click(function() {


                var pretobjli = $(this).parents("li").eq(0);

                var __dom = _def.__dom;


                if (__dom.hasClass("minileft")) {
                    return false
                }



                var ul = pretobjli.find("ul").eq(0);
                ul.off().unbind();

                var showui = li.find("ul:visible");


                var p_active = __dom.find(".active");
                var openul = p_active.find("ul").eq(0);

                if (openul.length > 0) {

                    openul.slideUp(300, function() {

                        var _thisul = $(this);
                        var _thispli = _thisul.parents("li").eq(0);
                        _thispli.removeClass("active");

                    });

                }



                if (!ul.is(":hidden")) {

                    ul.slideUp(300, function() {

                        var active = pretobjli.hasClass("active");

                        if (!open && !active) {
                            pretobjli.toggleClass("active");
                        } else {
                            pretobjli.removeClass("active");
                        }

                    });

                } else {

                    if (ul.hasClass('collapse')) {
                        ul.css("visibility", "visible");
                    }

                    ul.slideDown(300, function() {

                        pretobjli.toggleClass("active");

                        var menubox = __dom.find(".side-menubox");
                        _int.mcu_srollto(menubox, pretobjli.index());

                    });


                }



                return false;

            });

            //清空后的问题
            //if (_def.MiniMenu)
            //  _int._MiniMenu(componid);

        },
        mcu_srollto: function(menubox, _index) {

            var topx = (_index * 41) + "px";

            menubox.mCustomScrollbar("scrollTo", topx);

        },
        render: function() {

            var componid = _int.int.prototype._id_;
            var obj = _int._default[componid];

            if (obj) {

                var container = obj.renderTo;
                obj.__dom = $('' + container + '');
                var resourcesurl = obj.dataUrl;
                var Menukeybox = true;

                if (obj.menuSearch) {
                    Menukeybox = obj.menuSearch;
                }

                var __dom = obj.__dom;

                _int.ui(__dom, Menukeybox);


                if (typeof resourcesurl == "object") {

                    _int.loadrun(resourcesurl, componid);

                } else {
                    $.getJSON(resourcesurl, function(_Data) {

                        _int.loadrun(_Data, componid);

                    }).error(function(e) {

                        if (obj.menuItemError) {
                            var errordata = {
                                itmedata: "报错是" + componid,
                                error: e.statusText
                            };
                            obj.menuItemError(e.statusText);
                        }

                    });

                }


            }

            var bootmh = 88;
            $(window).resize(function() {


                if (window.top && obj.MiniMenu) {

                    if (window.top.window.frames["iframe"])
                        var mainFrm = window.top.window.frames["iframe"].frames["mainFrm"];

                    if (mainFrm) {
                        if (mainFrm.cols == "240,14,*") {
                            bootmh = 88;
                        } else {
                            bootmh = 24;
                        }

                    }
                }

                obj.minubox.hide();

                _int.re_mCustomScrollbar_h(bootmh);


            });

            _int.re_mCustomScrollbar_h(bootmh);


        },
        loadrun: function(_Data, componid) {

            _int.jsondata[componid] = [];
            _int.jsondata[componid] = _Data;
            var obj = _int._default[componid];
            var __dom = obj.__dom;

            obj.mousertimes = "";

            var Menukeybox = true;

            if (obj.menuSearch) {
                Menukeybox = obj.menuSearch;
            }




            var menubox = __dom.find(".side-menubox");



            var scrollHeights = obj.scrollH;

            var menuhtml = ' <ul class="nav side-menu">' + JSONTOHTML.transform(_int.jsondata[componid], transform.li) + '</ul>';
            menubox.hide().empty().html(menuhtml);

            var side_menu = __dom.find(".side-menu");

            //side_menu.metisMenu();

            var li = side_menu.find("li").has("ul");
            _int.itmesliding(componid, li);

            $(".nav-second-level li").click(function() {

                var _lithis = $(this);

                _int._showMenuItem(componid, _lithis);


                return false;
            });

            //mCustomScrollbar --滚动条       


            menubox.fadeIn("slow");

            // end mCustomScrollbar

            //搜索
            if (Menukeybox) {
                _int.MenuSearch(componid);
            }

            if (obj.Initialize) {
                obj.Initialize(__dom);
            }


            var windowtop_win_doc = $(window.top.document);
            var minubox = "";

            window.setTimeout(function() {

                if (obj.scrollH) {

                    menubox.height(scrollHeights);
                    menubox.mCustomScrollbar({
                        axis: "y",
                        scrollInertia: scrollHeights,
                        scrollbarPosition: "outside",
                        callbacks: {
                            onScrollStart: function() {
                                //滚轮开始时

                                if (obj.MiniMenu && obj.Set_Mini) {
                                    if (windowtop_win_doc) {
                                        _int.int.prototype.ss_mousewheel = true;
                                        // minubox = windowtop_win_doc.find("#minu_box");

                                        if (obj.minubox.length > 0) {

                                            obj.minubox.hide();
                                            var active1 = menubox.find(".active1");
                                            if (active1.length > 0) {
                                                active1.removeClass('active1');
                                            }
                                        }

                                    }
                                }

                                return false

                            },
                            whileScrolling: function() {
                                //滚动中

                                if (obj.MiniMenu && obj.Set_Mini) {

                                    if (windowtop_win_doc) {
                                        if (obj.minu_boxtimes_show != "" && typeof obj.minu_boxtimes_show != "undefined")
                                            window.clearTimeout(obj.minu_boxtimes_show);

                                        _int.int.prototype.ss_mousewheel = true;

                                        obj.minu_boxtimes_show = window.setTimeout(function() {

                                            _int.int.prototype.ss_mousewheel = false
                                            _int._hoverfshow(obj.jq_li_itme, componid);

                                        }, 25);

                                    }

                                }

                                return false;


                            },
                            onScroll: function() {
                                //滚轮结束时
                                if (obj.MiniMenu && obj.Set_Mini)
                                    _int.int.prototype.ss_mousewheel = false;
                            }
                        }

                    });
                }

                //取出子节点所有
                if (obj.MiniMenu) {

                    var jsonsdatals = _int.jsondata[componid];
                    var sub_all_josn = _int.itme_sub_jsondata[componid];
                    var itmeall = new Array();
                    for (var pkey in jsonsdatals) {

                        if (jsonsdatals[pkey].children.length > 0) {
                            var itmesub = jsonsdatals[pkey].children;

                            itmesub.forEach(function(itmes) {
                                itmeall.push(itmes);
                            });

                        }

                    }

                    _int.itme_sub_jsondata[componid] = [];
                    _int.itme_sub_jsondata[componid] = itmeall;

                    return false;
                }




            }, 10);




            return false;

        },
        events: function(functionname, callback) {

            var _id = _int.int.prototype._id_;

            var _def = _int._default[_id];

            switch (functionname) {
                case "events.menuItemError":
                    _def.menuItemError = callback;
                    break;
                case "events.showMenuItem":
                    _def.showMenuItem = callback;
                    break;
                case "Initialize":
                    _def.Initialize = callback;
                    break;
            }

        }

    }

    return _int.int;

});
