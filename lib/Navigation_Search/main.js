require(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/CodeMirror/lib/codemirror.js'], function() {

	require(["oso.lib/nprogress/nprogress.min", "jquery", "require_css!oso.lib/nprogress/nprogress"],
		function(NProgress, $) {

			$("body").show();

			NProgress.start();

			var cssfileall = [
				"oso.lib/CodeMirror/mode/javascript/javascript",
				"oso.lib/CodeMirror/lib/util/foldcode",
				"require_css!oso.lib/CodeMirror/lib/codemirror.css",
				"bootstrap"
			];


			require(cssfileall, function() {

				require(["oso.lib/Navigation_Search/NavigationMenu", "art_Dialog/dialog-plus"], function(NavigationMenu, artdialog) {


					$(function() {



						//demo  

						//Menu,参数：容器，资源路径，点击事件,搜索按钮
						var _Menuset = {
							renderTo: "#Compond_box",
							dataUrl: "./d.json",
							scrollH: 400
						};

						var _left_meun = new NavigationMenu(_Menuset);

						

						_left_meun.on("events.showMenuItem", function(itemData) {

							console.log(itemData);

							//{itemData.url,itemData.itme}
							var d = new artdialog({
								content: itemData.url,
								icon: 'question',
								lock: true,
								width: 200,
								height: 70,
								title: '提示'
							});
							d.show();

							return false;

						});

						_left_meun.on("events.menuItemError", function(itemData) {


							//{itemData.error,itemData.itme}
							var d = new artdialog({
								content: itemData.error,
								icon: 'question',
								lock: true,
								width: 200,
								height: 70,
								title: '提示'
							});
							d.show();

						});


						_left_meun.on("Initialize", function() {

							var brotab = $("#tab_content");
							brotab.show();

							require(["require_css!oso.lib/CodeMirror/demo/CodeMirror_demo.css", "oso.lib/CodeMirror/demo/CodeMirror_demo"], function() {

								if (brotab.length > 0) {
									brotab.find("div[role='tabpanel']").addClass('tab-pane');
								}

							});

						});

						_left_meun.render();
						//$("body").show();
						NProgress.done();

					});

					// window end


				});

				// navigation_search

			});
			// codemirror end

		});
	// requirejs_config end
});
// nprogress end