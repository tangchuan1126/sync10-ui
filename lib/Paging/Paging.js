/**
 * 分页创建
 * @authors Your Name (you@example.org)
 * @date    2014-09-04 14:05:03
 * @version $Id$
 */

(function() {

    var languages={};

    languages["cn"]={
        Thispage:"当前",
        TotalPage:"总页",
        Previous:"上页",
        Next:"下页"
    };

    languages["en"]={
        Thispage:"Page",
        TotalPage:"Total",
        Previous:"prev",
        Next:"next"
    };



    var page=function($){


     var pagefn={
         language:"en",
        _default:{},    
     //翻页按钮事件
     events:function(father,callback){

    var keyname=father.replace("#","");

     var _def=pagefn._default[keyname];


     var btnli=$(father).find("li");
     
      btnli.click(function(){

      //  var jqbox=$("#"+_def.box+"") || $("."+_def.box+"") || $(_def.box);
      // _def.selectindex=jqbox.find(".Setpagesize select").val();

         var gotopage=false;   
         var _this=$(this);
         var classname=_this.attr("class");
         var gopage=1;

         //数字按钮
         if(typeof classname =="undefined" || classname==""){
           gopage=_this.text();           
           gotopage=true;
         }

         //下分页
         //当前页
         var selecttxt=parseInt($(father).find(".select").text());
         if(classname == "next"){
        //总页数
          var pageCount = _def.total_page;
           gopage=selecttxt + 1;
           if(gopage <= pageCount){
            gotopage=true;
           }

         }

      //上分页
        if(classname =="prev"){
          
          if(selecttxt > 1){
           gopage=selecttxt - 1;
           gotopage=true;
          }

        }


        //如果可以翻页时更新页号
         if(gotopage){        

           callback(gopage);           
            var total_page=_def.total_page;
            var box=_def.box;
            var _callback=_def.callback;
           pagefn.update_page(total_page,gopage,box,_callback,_def.select);
         }


      });

      //跳转页面

      var lastli=$(father).find(".Gotopage");
      var gobtn=lastli.find("span");
      var selectkye=lastli.find(":text");
      gobtn.click(function(){
        
         var inputval=selectkye.val();
         pagefn.topageing(inputval,_def);

      });

     selectkye.keydown(function(e){
      
      if (e.keyCode == 13) {
       var inputval=$(this).val();
       pagefn.topageing(inputval,_def);
      }
      //add by huhy
     }).keyup(function(){
        var inputval=parseInt(this.value);
        if(isNaN(inputval) || inputval < 1 || inputval > _def.total_page){
            this.value = "";
        }
     });

     var Setpagesize=$(father).find(".Setpagesize");
     var selectobjs=Setpagesize.find("select");
     if(_def.select && selectobjs.length > 0){

        selectobjs.change(function(){
            if(_def.select.callback){
            var __thisval=$(this).val();
            _def.select.callback(__thisval);
             return false;
            }
        });

     }


     },
     topageing:function(nosring,_def){
        //跳转页面
        var gopage=parseInt(nosring);

     if(!isNaN(gopage)){
        var total_page=_def.total_page;
        var box=_def.box;        
        var _callback=_def.callback;
        var page_list_Number=_def.page_list_Number;
        _callback(gopage);        
        pagefn.update_page(total_page,gopage,box,_callback,_def.select,page_list_Number);
       }

     },
     select:function(father){

       var _def=pagefn._default[father];

     },
    update_page:function(total_page,current_page,father,callback,selectjson,page_list_Number){
        
    var languagere=languages[pagefn.language];

    var _def="";
    if(!pagefn._default[father]){

      _def=pagefn._default[father]={}; 
    
    }else{
        
      //var selectindex=pagefn.default[father].selectindex;
      //console.log(dd);
     _def=pagefn._default[father]={};
    // _def.selectindex=selectindex;

    }

     

     _def.box=father;
     _def.total_page=total_page;
     _def.callback=callback;
     _def.page_list_Number=page_list_Number;

     if(!_def.select && selectjson && typeof selectjson!="undefined"){

     _def.select=selectjson;

     }

  
    

    var total_page = parseInt(total_page);
    var current_page = parseInt(current_page);
    var page_list_Number=parseInt(page_list_Number);

    var pager_length = 11;    //不包next 和 prev 必须为奇数
    var pager = new Array( pager_length );
    var header_length = 2; 
    var tailer_length = 2;
    //header_length + tailer_length 必须为偶数
    var main_length = pager_length - header_length - tailer_length; //必须为奇数
    
    var a_tag = 'li';
    var a_class = '';
    var a_id = '';
    var a_name = '';
    
    var disable_class = 'disable';
    var select_class = 'select';
                
    var i;
    var code = '';
    if( total_page < current_page || current_page < 1 || total_page < 1)
    {
       // alert('总页数不能小于当前页数');
        return false;    
    }    
    //判断总页数是不是小于 分页的长度，若小于则直接显示
    if( total_page < pager_length )
    {
        for(i = 0; i <     total_page; i++)
        {
            
            code += (i+1 != current_page) ? pagefn.fill_tag(a_tag, a_class, a_id, a_name, i+1) : pagefn.fill_tag(a_tag, select_class, a_id, a_name, i+1);
        }
    }
    //如果总页数大于分页长度，则为一下函数
    else
    {
        //先计算中心偏移量
        var offset = ( pager_length - 1) / 2;
        //分三种情况，第一种左边没有...
        if( current_page <= offset + 1)
        {
            var tailer = '';
            //前header_length + main_length 个直接输出之后加一个...然后输出倒数的    tailer_length 个
            for( i = 0; i < header_length + main_length; i ++)
                code += (i+1 != current_page) ? pagefn.fill_tag(a_tag, a_class, a_id, a_name, i+1) : pagefn.fill_tag(a_tag, select_class, a_id, a_name, i+1);
            code += pagefn.fill_tag(a_tag, a_class, a_id, a_name, '...');
            for(i = total_page; i > total_page - tailer_length; i --)
                tailer = pagefn.fill_tag(a_tag, a_class, a_id, a_name, i) + tailer;
                
            code += tailer;
        }
        //第二种情况是右边没有...
        else if( current_page >= total_page - offset )
        {
            var header = '';
            //后tailer_length + main_length 个直接输出之前加一个...然后拼接 最前面的 header_length 个
            for( i = total_page; i >= total_page-main_length - 1; i --)
                code = (( current_page != i ) ? pagefn.fill_tag(a_tag, a_class, a_id, a_name, i) : pagefn.fill_tag(a_tag, select_class, a_id, a_name, i)) + code;
            code = pagefn.fill_tag(a_tag, a_class, a_id, a_name, '...') + code;
            for( i = 0; i < header_length ; i++)
                header +=  pagefn.fill_tag(a_tag, a_class, a_id, a_name, i + 1);
            
            code = header + code;
        }
        //最后一种情况，两边都有...
        else
        {
            var header = '';
            var tailer = '';
            //首先处理头部
            for( i = 0; i < header_length; i ++)
                header += pagefn.fill_tag(a_tag, a_class, a_id, a_name, i + 1);
            header += pagefn.fill_tag(a_tag, a_class, a_id, a_name, '...');
            //处理尾巴
            for(i = total_page; i > total_page - tailer_length; i --)
                tailer = pagefn.fill_tag(a_tag, a_class, a_id, a_name, i) + tailer;
            tailer = pagefn.fill_tag(a_tag, a_class, a_id, a_name, '...') + tailer;
            //处理中间
            //计算main的中心点
            var offset_m = ( main_length - 1 ) / 2;
            var partA = '';
            var partB = '';
            var j;
            var counter = (parseInt(current_page) + parseInt(offset_m));
            for(i = j = current_page ; i <= counter; i ++, j --)
            {
                partA = (( i == j ) ? '' : pagefn.fill_tag(a_tag, a_class, a_id, a_name, j)) + partA;
                partB += ( i == j ) ? pagefn.fill_tag(a_tag, select_class, a_id, a_name, i) : pagefn.fill_tag(a_tag, a_class, a_id, a_name, i);
            }
            //拼接
            code = header + partA + partB + tailer;
            
            
        }
    }
    
    
    var prev = ( current_page == 1 ) ? pagefn.fill_tag(a_tag, disable_class, a_id, a_name, 'prev') : pagefn.fill_tag(a_tag, a_class, a_name, a_name, 'prev');
    var next = ( current_page == total_page ) ? pagefn.fill_tag(a_tag, disable_class, a_id, a_name, 'next') : pagefn.fill_tag(a_tag, a_class, a_name, a_name,'next');
    
    var chekin='<'+a_tag+' class="Gotopage"><label><input type="text" /></label><span>Go</span></'+a_tag+'>';
    
    var page_number=page_list_Number;
    var Totalnumber=total_page * page_number;

    var pageinfo="";


    
    if(page_list_Number > 0 && typeof page_list_Number!="undefined"){
     pageinfo='<'+a_tag+' class="pageinfo"><span>'+languagere.Thispage+':'+current_page+'</span>/<span class="mr_15">'+total_page+'</span><span>'+languagere.TotalPage+':'+Totalnumber+'</span></'+a_tag+'>';
    }
 
   var __select="";

   var jqbox=$("#"+_def.box+"") || $("."+_def.box+"") || $(_def.box);

   var _selectobjs=jqbox.find(".Setpagesize select");

   var selectindex="";
   if(_selectobjs.length)
   selectindex=_selectobjs.val();

    if(_def.select){

    var select_obj = _def.select;   


   // var selectindex=_def.selectindex;

     var __option="";
     var index=-1;
     if(select_obj.index){
       index=select_obj.index;
     }


     for(var optionkey in select_obj.option){
        var _optionobj=select_obj.option[optionkey]; 
        if(typeof _optionobj=="string" && typeof _optionobj!="undefined"){
         var selected="";
         if(index > -1 && index==optionkey || _optionobj == selectindex){
           selected =' selected="selected"';
         }
        __option+='<option'+selected+' value="'+_optionobj+'">'+_optionobj+'</option>';
       }
     }
    
     __select='<'+a_tag+' class="Setpagesize"><select>'+__option+'</select></'+a_tag+'>';

    }



    code =pageinfo + prev + code + next + __select + chekin;

    document.getElementById(father).innerHTML=(code);

  //点击时回调
    pagefn.events("#"+father+"",callback);


    },
     fill_tag:function(a_tag, a_class, a_id, a_name, a_html){ 
     var languagere=languages[pagefn.language];       
        a_class = (a_class == '') ? '' : ' class="'+a_class+'"';
        a_id = (a_id == '') ? '' : ' id="'+a_id+'"';
        a_name = (a_name == '') ? '' : ' name="'+a_name+'"';
        var btnstyle="";

        if(a_html == "..."){
         btnstyle=" class='dosring'";
        }

        if(a_html!="..." && /\D/g.test(a_html)){
         btnstyle=" class='"+a_html+"'";
        }

        if(a_html=="next"){
           a_html = languagere.Next;
        }

        if(a_html=="prev"){
           a_html = languagere.Previous;
        }


        var code = '<'+a_tag+a_class+a_id+a_name+''+btnstyle+'>'+a_html+'</'+a_tag+'>';
        return code;

        },
        abdss:function(){

            return "acs";

        }


    };


        return pagefn;
    }; 





  if (typeof define === 'function' && define.amd) {
    require(["require_css!oso.lib/Paging/css/pagestyle.css"]);
    define(["jquery"],page);
  }else{

    window.pageging=pageging = pagefn||{};

    //pageging.update_page(112,1,"pagebo",function(){console.log("回调"),每页几条});
  }


}).call(this);
