
"use strict";

(function(){
  define(["jquery", "jquery-ui", "jquery.multiselect"],function($){

  	return function(selector, options){

  		var opts = $.extend({}, {
	        noneSelectedText: "Entry Type",
	        checkAllText: "select all",
	        uncheckAllText: 'unselect all',
	        selectedList: 1,
	        minWidth: 100,
	        height:100		
		}, options);

  		$(selector).multiselect(opts);

  	};

  });
}).call(this);