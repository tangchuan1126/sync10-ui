
"use strict";

(function(){
  define(["jquery"],function($){

    var CheckBuild = function(options) {

    	if(!options.container){
    		throw new Error("Do not specify a container!");
    	}

    	var _this = this,
    		arrDom,
    		defaults = {
		    	data : []
		    	//container : "#container"
		    	//layout : "flow"
		    },
		    opts = $.extend({}, defaults, options),
	    	_build = function(data){ //构建dom

		    	var arrDom = [],
		    		html = [],
		    		fragment = document.createDocumentFragment();
		    	
		    	for(var i = 0, len = data.length; i < len; i++){

	  				var json = data[i],
	  					checkbox = document.createElement('input'),
	  					label =  document.createElement('label'),
	  					span = document.createTextNode(json.name),
	  					div = document.createElement('div');


	    			checkbox.type = 'checkbox'; 
	    			checkbox.checked = json.checked;
	    			checkbox.value = json.name;

	    			div.className = 'checkbox';

	    			label.appendChild(checkbox);
	    			label.appendChild(span);

	    			if(opts.layout === "flow"){
	    				//流式
	    				label.className = "checkbox-inline";
						fragment.appendChild(label);

	    			}else{

	    				div.appendChild(label);
	    				fragment.appendChild(div);
	    			}

	    			arrDom.push(checkbox); 
		    	}

		    	$(container).append(fragment);

		    	return arrDom;
		    };

	    //获取data
	    if(opts.url){
	    	$.ajax({
	    		url : opts.url,

	    	}).done(function(data){
	    		//请求成功
	    		arrDom = _build(data);

	    	}).fail(function(jqXHR, textStatus){
	    		alert("Request failed: " + textStatus);
	    	});
	    }else if(opts.data){
	    	arrDom = _build(data);
	    }
   
   		//绑定事件
	    _this.on = function(){

	    	var arr = arguments;

	    	var checkIsSuccess = function(){
	    		if(!arrDom){

		    		setTimeout(checkIsSuccess, 100);
		    	}else{

		    		for (var i = 0, len = arrDom.length; i < len; i++) {

			    		$.fn.on.apply($(arrDom[i]), arr);
			    	}
		    	}
	    	};

	    	setTimeout(checkIsSuccess, 100);

	    };

	    return _this;

    };

    return CheckBuild;

  });
}).call(this);