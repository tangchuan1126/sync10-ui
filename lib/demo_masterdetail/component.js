define(["jquery","backbone"],function($, Backbone){
	return Backbone.View.extend({
		constructor: function(options) {
			Backbone.View.apply( this, arguments ); //调用父类构建，可自动处理el等初始化属性
			this.template = options.template;
			var Model = Backbone.Model.extend({
				urlRoot: options.detailUrl,
				idAttribute: options.idAttribute || "id"
			});
			this.collection = new Backbone.Collection([],{model: Model});
			this.collection.url = options.masterUrl;
			var v = this;
			v.pageCtrl = { pageNo:1, pageSize: options.pageSize || 20};
			this.collection.parse = function(resp){ 
				var items = resp[options.itemsKey || "items"]; 
				v.pageCtrl= resp[options.pageCtrlKey || "pageCtrl"];
				return items;
			};
			
			this.detailView = new Backbone.View();
			this.detailView.template = options.detailTemplate;
			this.detailView.render = function(target){
				if(! options.isDetailViewShared) {
					this.setElement(v.findDetailEl(target,options.detailEl));
				}
				this.$el.html(this.template(this.model.toJSON()));
			};
			if(options.isDetailViewShared) this.detailView.setElement(options.detailEl);
		},
		findDetailEl:function(fromEl, detailEl){
			var el = $(fromEl).find(detailEl);  //优先在下级元素中找
			if(el.size()>0) return el.get();
			el = $(fromEl).parent().find(detailEl);  //找不到就从父级元素中往下找
			if(el.size()>0) return el.get();
			return null; //找不到DetailView的元素
		},
		render: function() {
			var v = this;
			this.collection.fetch({
				reset: true,
				data: { pageNo: v.pageCtrl.pageNo, pageSize: v.pageCtrl.pageSize },
				success: function(collection){
					v.$el.html(v.template({ items: collection.toJSON(), pageCtrl:v.pageCtrl }));
					$(".show-detail").css("cursor","pointer");
					v.trigger("events.renderMaster");
				},
				error:function(collection,resp){
					console.log("error:"+resp);
				}
			});
		},	
		events: {
			"click .show-detail": "showDetail",
			"click button.page-ctrl":"changePage"
		},
		showDetail: function(evt){
			var modelId = $(evt.currentTarget).data("id");
			var model = this.collection.get(modelId);
			var v = this;
			model.fetch({
				success: function(model){
					v.detailView.model = model;
					v.detailView.render($(evt.currentTarget));
					v.trigger("events.renderDetail",model.toJSON());
				},
				error:function(model,resp){
					console.log("error:"+resp);
				}
			});
		},
		changePage:function(evt){
            var btn = $(evt.currentTarget);
            if(btn.data("pageno")){
                this.pageCtrl.pageNo = parseInt(btn.data("pageno"));
            }
            else if(btn.data("pageplus")){
                this.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
            }
            else return;
            this.render();
        }
	});
});