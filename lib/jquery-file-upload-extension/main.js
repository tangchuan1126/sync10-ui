 require(['/Sync10-ui/requirejs_config.js'], function() {

 	require(["oso.lib/nprogress/nprogress.min", "jquery", "require_css!oso.lib/nprogress/nprogress","require_css!bootstrap-css/bootstrap.min"],
 		function(NProgress, $) {

 			$("body").show();
 			NProgress.start();

 			require(["oso.lib/jquery-file-upload-extension/upload-plugin"], function(uploadplugin) {
               
               $(function(){
               
                var jsonval={
                         renderTo: ".Express",
                         //nobotstrap:true,
                         fileposturl:"/Sync10/_fileserv/upload",
                         //FileSize:500000,
                         FileTypes:/(\.)/i,  //(\.|\/)(xlsx|xls|docx|gif|jpe?g|png)$/i,
                         acceptFileTypes:"The file type errors",
                         unknownError:"Error message",
                         FileSize:800000000,
                         maxNumberOfFiles:3,
                         Initialize:function(){}
                        // batch:false,//关闭批量操作功能
                         //Asinglefile:true//每次替换上次选择的文件
                         //或
                        // templateurl:"xx/xx/xx_tlp.html",
                     };

                var uploadfile = new uploadplugin(jsonval);
             
                //上传成功回调 
                 uploadfile.on("event.done",function(d){

                    //alert("上传成功！");
                    $('.fileupload-progress').removeClass('in').find('.progress-extended').empty().end().find('.progress-bar-success').width('0%');

                 });
                 

                 uploadfile.on("event.destroyed",function(e,d){

                      console.log(d);

                  });


                 NProgress.done();               

               
               });
               //document onload end

 			});

 			//upload-plugin end

 		});
 	  // css end

 });
 //requirejs config end