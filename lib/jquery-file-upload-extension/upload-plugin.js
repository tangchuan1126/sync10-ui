/**
 * gaowenzhen@msn.com
 * @authors //127.0.0.1/Sync10-ui/lib/jquery-file-upload-extension
 * @date    2014-12-04 09:42:50
 * @version 0.0.1
 */
(function(root, factory) {

	var fileupload = "jquery-file-upload/jquery.fileupload";
	var fileupload_ui = "jquery-file-upload/jquery.fileupload-ui";
	 libpat="/Sync10-ui/lib/jquery-file-upload-extension/";

	if (typeof define === 'function' && define.amd) {
		//add config
		var _paths = requirejs.s.contexts._.config.paths;
		var _mypat = _paths["jquery-file-upload"];
		libpat=_paths["oso.lib"]+"/jquery-file-upload-extension/";
		_paths["jquery.ui.widget"] = _mypat + "/vendor/jquery.ui.widget";
		_paths["tmpl"] = _paths["oso.lib"] + "/jquery-file-upload-extension/Relyon/tmpl.min";
		var __bower = _mypat.replace(/\/jquery-file-upload\/.*/, "/");
		var __ildat = __bower + "blueimp-load-image/js/";
		_paths["load-image"] = __ildat + "load-image";
		_paths["load-image-meta"] = __ildat + "load-image-meta";
		_paths["load-image-exif"] = __ildat + "load-image-exif";
		_paths["load-image-ios"] = __ildat + "load-image-ios";
		_paths["canvas-to-blob"] = __bower + "blueimp-canvas-to-blob/js/canvas-to-blob.min";
		//css
		var __uploadcss = _mypat.replace(/\/js.*/, "/css");
		_paths["fileupload-css"] = __uploadcss;
		//-----------------------

		// AMD. Register		
		define(['jquery', fileupload, fileupload_ui], factory);

	} else if (typeof uploadPlugin === 'object') {
		// CommonJS
		module.uploadPlugin = factory(
			require('jquery'),
			require(fileupload),
			require(fileupload_ui)
		);

	} else {
		// Browser globals
		root.uploadPlugin = factory(
			root.$,
			root.fileupload,
			root.fileupload_ui
		);
	}
}(this, function($, fileupload, fileupload_ui) {
	'use strict';

	var Componbox = {};
	//文件类型对应图标   add by huhy 
    var icons = {
    	'application/msword':'fa-file-word-o',
    	'application/pdf':'fa-file-pdf-o',
    	'application/vnd.ms-excel':'fa-file-excel-o',
    	'application/octet-stream':'fa-file-archive-o',
    	'application/zip':'fa-file-archive-o',
    	'application/vnd.ms-powerpoint':'fa-file-powerpoint-o',
    	'application/vnd.ms-works':'fa-file-word-o',
    	'application/javascript':'fa-file-code-o',
    	'application/x-msdownload':'fa-cogs'
    };

	Componbox.createcss = function(_opts) {
     
    

      	var uploadbox_style="require_css!oso.lib/jquery-file-upload-extension/css/uploadbox_style.css";

      

		var cssfileall = [
		    "require_css!bootstrap-css/bootstrap.min",
			"require_css!fileupload-css/jquery.fileupload",
			"require_css!fileupload-css/jquery.fileupload-ui",
			"require_css!Font-Awesome",
			"require_css!oso.lib/jquery-file-upload-extension/css/style.css"
		];

		if(_opts.nobotstrap){

			delete cssfileall[0];
			cssfileall.push(uploadbox_style);

		}

		require(cssfileall);

	};
    
     //扩展jq-ui
	Componbox.FromResponse = function(opts) {

		var serverpat="/Sync10/_fileserv/file/";
		if(opts.serverpat){
			serverpat=opts.serverpat;
		}



		var _events = Componbox.events[opts.Objectid];

		//console.dir(Componbox.events);
	

	     var _options=$.blueimp.fileupload.prototype.options;
	     
	     if(opts.acceptFileTypes){
	     _options.messages.acceptFileTypes=opts.acceptFileTypes;
	     }

	     if(opts.unknownError){
	     	_options.messages.unknownError=opts.unknownError;
	     }

	    // console.log(_options.prependFiles);

	  

			var _options1=$.blueimp.fileupload._proto.options;
			
			_options1.prependFiles=_options.prependFiles=true;


			

		if(!opts.nostop){
            _options1.stop = _options.stop=function(){return false};
	    }

	    //_options1=_options.destroy

	    _options1.destroy=_options.destroy=function (e, data) {
                if (e.isDefaultPrevented()) {
                    return false;
                }
                var that = $(this).data('blueimp-fileupload') ||
                        $(this).data('fileupload'),
                    removeNode = function () {
                        that._transition(data.context).done(
                            function () {
                                $(this).remove();
                                that._trigger('destroyed', e, data);

                            if(_events["event.destroyed"]){
                              _events["event.destroyed"](e,data);                    
                             }

                            }
                        );
                    };
                if (data.url) {
                    data.dataType = data.dataType || that.options.dataType;
                    $.ajax(data).done(removeNode).fail(function () {
                        that._trigger('destroyfailed', e, data);
                    });
                } else {
                    removeNode();
                }
            };



			_options1=_options.getFilesFromResponse=function(data) {
                var filesdata = [];
                if (data.result.files) {
                    filesdata = data.result.files;
                } else {
                    if (data.result) {
                        filesdata = data.result;
                    }

                    var arryname = [];

                    for (var file_i = 0; file_i < filesdata.length; file_i++) {
                        var fileitme = filesdata[file_i];  

                        arryname.push({
                            "url":serverpat+fileitme.file_id,
                            "thumbnailUrl": serverpat+fileitme.file_id,
                            "name": fileitme.original_file_name,
                            "type":fileitme.content_type,
                            "size":fileitme.file_size,
                            "deleteUrl":serverpat+ fileitme.file_id,
                            "deleteType": "DELETE",
                            "icons": fileitme.content_type.match('image') ? undefined : (icons[fileitme.content_type] || 'fa-file-o')
                        }); 
                    }

                 filesdata=arryname;

                }

                 if(_events["event.done"]){

                    _events["event.done"](data.result);
                    
                 }

                return filesdata;
            };

	};

	Componbox.createfileupload = function(opts) {

		$("input:checkbox").click(function(){
			alert("90909090");
		});

		var _Dom = opts._Dom;
		var tlpurl = opts.templateurl;
		if(opts.Initialize)
		var temploadfn=opts.Initialize;
		var fileposturl=opts.fileposturl;
		var FileSize=opts.FileSize;
		var maxNumberOfFiles=opts.maxNumberOfFiles;
		//var maxNumberOfFiles=3;
		var declareString = "每次最多只能上传"+maxNumberOfFiles+"个文件！";
		if(opts.FileSize){
          FileSize=opts.FileSize;
		}
		var FileTypes= /(\.|\/)(gif|jpe?g|png)$/i;
		if(opts.FileTypes){
		  FileTypes=opts.FileTypes;
		}
		Componbox.FromResponse(opts);

		var isAddCbxChange = false;

		_Dom.load(tlpurl, function() {
		
			_Dom.fadeIn("slow");

			var filesLength = 0;

			//选择文件类型的图标处理
	        $('.fileinput-button>input[type="file"]').change(function(e){
	        	console.log(this.files.length);
	            $.each(this.files, function(index, file){
	            	file['icon'] = file.type.match('image/') ? undefined : (icons[file.type] || 'fa-file-o');
	            });
	        });
			// Demo settings:
			$('#fileupload').fileupload({
				url: fileposturl,
				disableImageResize: /Android(?!.*Chrome)|Opera/
					.test(window.navigator.userAgent),
				maxFileSize: FileSize,
				maxNumberOfFiles:maxNumberOfFiles,
				acceptFileTypes:FileTypes,
			}).bind('fileuploadadd', function (e, data) {
				//console.log("fileuploadadd");
				if(filesLength == (maxNumberOfFiles-1)){
					$("#addBtn").attr("disabled","disbaled");
				}
				if (filesLength < maxNumberOfFiles) {
					filesLength++;
				}else{
					e.preventDefault();
				}
				if (opts.Asinglefile) {
					var $this = $(this),
						that = $this.data('blueimp-fileupload') ||
						$this.data('fileupload'),
						options = that.options;
					options.filesContainer.empty();
				}
				//console.log(filesLength);
				$("tr:last").attr("id",filesLength);
			})
		    .bind('fileuploadadded', function (e, data) {
		    	//console.log("fileuploadadded");
		    	$("#submitBtn").removeAttr("disabled");
		    	$("#resetBtn").removeAttr("disabled");
		    	//console.log(filesLength);
		    })
		    .bind('fileuploaddestroyed', function (e, data) {
		    	//console.log("fileuploaddestroyed");
		    	whenCancleFileOrDeleteFile();
		    	downloadDelCbxChange();
			})
		    .bind('fileuploadcompleted', function (e, data) {
		    	//console.log("fileuploadcompleted");
		    	if($(".template-upload").length == 0){
		    		$("#submitBtn").attr("disabled","disbaled");
			    	$("#resetBtn").attr("disabled","disbaled");
		    	}
		    })
		    .bind('fileuploadfailed', function (e, data) {
		    	//console.log("fileuploadfailed");
			    whenCancleFileOrDeleteFile();
			});
			
			$("#declareSpan").html(declareString);

			function whenCancleFileOrDeleteFile(){
				filesLength--;
			    //console.log(filesLength);
			    if(filesLength < maxNumberOfFiles){
			    	$("#addBtn").removeAttr("disabled");
			    }else{
			    	$("#addBtn").attr("disabled","disbaled");
			    }
			    if($(".template-upload").length == 0){
		    		$("#submitBtn").attr("disabled","disbaled");
			    	$("#resetBtn").attr("disabled","disbaled");
		    	}
			    /*if(filesLength == 0){
			    	$("#submitBtn").attr("disabled","disbaled");
			    	$("#resetBtn").attr("disabled","disbaled");
			    }else{
			    	$("#submitBtn").removeAttr("disabled");
			    	$("#resetBtn").removeAttr("disabled");
			    }*/
			}

			

		});

	};



	function uploadPlugin(opts) {

		if (!(this instanceof uploadPlugin))
			return new uploadPlugin(opts);

		if (typeof opts === 'string')
			opts = {
				renderTo: opts
			};


		var _this = this.constructor;
		_this.opts = opts;
		Componbox.createcss(_this.opts);
		_this.int();
		var replplugin = {
			on: _this.On,
			render: _this.Render
		};

		replplugin.constructor.opts = _this.opts;

		return replplugin;

	};

    Componbox.events={};
    Componbox.JSONDATA={};
	uploadPlugin.int = function() {

		var _this = this.opts;
		var __names = "up_" + $.now();
		_this.Objectid = __names;
		Componbox.events[__names]={};
        Componbox.JSONDATA[__names]={};
		if (_this.renderTo) {
			_this._Dom = $(_this.renderTo);
		} else {
			_this._Dom = $('<div id="' + __names + '"></div>');
		}

		if(_this.nobotstrap){			
           _this._Dom.hide().addClass("uploadbox_style upload_extensionbox");
		}else{
			_this._Dom.hide().addClass("upload_extensionbox");
		}		
	

		if(_this.Asinglefile && typeof _this.templateurl=="undefined" || _this.templateurl==""){
         _this.templateurl = libpat+"nobatch_tlp.html";	
		}

		//默认模板 
		if (!_this.templateurl) {
			_this.templateurl = libpat+"fileupload_tlp.html";
		}

		Componbox.createfileupload(_this);

	};

	uploadPlugin.On = function(event_,callback) {

		var _opts = this.constructor.opts;
		var _events = Componbox.events[_opts.Objectid];
		if(event_!="" && typeof event_ !="undefined"){
          _events[event_]=callback;
		}       

	};

	uploadPlugin.Render = function() {
		var _opts = this.constructor.opts;
	
	};


	return uploadPlugin;
}));

function downloadDelCbxChange(){
	var isEnabled = false;
	var deleteCbxArray = $("input[name='delete']");
	for(var i = 0; i < deleteCbxArray.length; i++){
		if(deleteCbxArray[i].checked){
			isEnabled = true;
			break;
		}
	}
	if(isEnabled){
		$("#deleteBtn").removeAttr("disabled");
	}else{
		$("#deleteBtn").attr("disabled","disbaled");
	}
}

function uploadDelCbxChange(){
	setTimeout("downloadDelCbxChange()", 10);
}