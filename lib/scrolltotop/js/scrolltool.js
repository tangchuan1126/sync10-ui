"use strict";

(function(){
	define(["jquery", "require_css!../css/style.css"], function($){

		var scrolltool = function(options){
			options = $.extend({		
				topline : 100,
				bottomline : 100,				
				scrollduration : 500,			
				fadeduration : [ 500, 100 ],
				offsetx : 5,
				offsety : 5,					
			}, options);
			
			var state = {
				top : {
					isvisible : false,
					shouldvisible : false
				},
				bottom : {
					isvisible : false,
					shouldvisible : false
				}
			}
			
			var $body,$top,$bottom;
			
			var init = function(){
				$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
				var $control = $('<div class="scrolltool"></div>').css({
					bottom : options.offsety,
					right : options.offsetx
				}).appendTo('body');

				$top = $('<div class="backtotop" title="返回顶部"></div>').click(function() {
					scrollup();
					return false;
				}).appendTo($control);

				$bottom = $('<div class="gotobottom" title="去到底部"></div>').click(function() {
					scrolldown();
					return false;
				}).appendTo($control);

				togglecontrol();

				$(window).bind('scroll resize', function(e) {
					togglecontrol();
				})
			};
			
			var scrollup = function() {
				$body.animate( {
					scrollTop : 0
				}, options.scrollduration);
			};

			var scrolldown = function(){
				$body.animate( {
					scrollTop : $(document).height()
				}, options.scrollduration);
			}

			var togglecontrol = function() {
				var scrollTop = $(window).scrollTop();
			　　var scrollHeight = $(document).height();
			　　var windowHeight = $(window).height();

			　　state.bottom.shouldvisible = (scrollHeight - (scrollTop + windowHeight) <= options.bottomline) ? false : true;
				if (state.bottom.shouldvisible && !state.bottom.isvisible) {
					$bottom.fadeIn(options.fadeduration[0]);
					state.bottom.isvisible = true;
				} else if (state.bottom.shouldvisible == false && state.bottom.isvisible) {
					$bottom.fadeOut(options.fadeduration[1])
					state.bottom.isvisible = false;
				}

				state.top.shouldvisible = (scrollTop >= options.topline) ? true : false;
				if (state.top.shouldvisible && !state.top.isvisible) {
					$top.fadeIn(options.fadeduration[0]);
					state.top.isvisible = true;
				} else if (state.top.shouldvisible == false && state.top.isvisible) {
					$top.fadeOut(options.fadeduration[1])
					state.top.isvisible = false;
				}
			};
			
			return init();
		};

		return scrolltool;
	});
})(this);