"use strict";

require([
     "../../requirejs_config", 
], function(){
    require(["jquery", "js/scrolltool.js"], function($, scrolltool){
        scrolltool({
            topline : 500,
            bottomline : 500, 
            fadeduration : [ 500, 500 ],          
            offsetx : 25,
            offsety : 25,    
        });
    });
});
