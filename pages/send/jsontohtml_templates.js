define(["JSONTOHTML","./key/TransportOrderKey","./key/TransportQualityInspectionKey","/Sync10-ui/bower_components/bootstrap/js/utility.js","./DateUtil"], function(JSONTOHTML,TransportOrderKey,TransportQualityInspectionKey,utility,DateUtil) {
	//第一次创建时带上表头
	var _table = {
		thead: 
		[{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index){
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th:
		{
			tag: "th",
			html: "${title}",
			style: function(obj, index) {
				if (index==3) {
					return "width:20%";
				} else {
					return "";
				}
			}
		}
	};
	var types = ["basicInfo","transportInfo","FLOWS","LOGS"];
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		//初始化，将json数据分类：分为对应的行和列
		AUTHS:null,
		int: function(jsons,View_control) {
			//数据转换成表格形式
			//["tr":"index","children":[{},{}]]
			_tbody.AUTHS = {};
			var headallobj = View_control.head;
			var footer = View_control.footer;
			var pageSize=jsons.PAGECTRL.pageSize;
			//按表头转换成行和列
			var __trhtml = "";
			//获得权限
			_.map(jsons.AUTHS,function(auth,key) {
				_tbody.AUTHS[auth.ID] = auth;
			});
			for (var tri = 0; tri < jsons.DATA.length; tri++) {
				var tritme = jsons.DATA[tri];
				//_.map(_tbody.AUTHS,function(obj,key){});
				var __td = [];
				for (var headkey in headallobj) {
					__td.push(tritme);
				}
				//显示行
				var __tr=[];
				__tr.push({"trid":tritme.TRANSPORT_ID,"children": __td});
				__trhtml += (JSONTOHTML.transform(__tr[0], _tbody.tr));
				//按钮行
				__tr=[];
				__tr.push({"trid":tritme.TRANSPORT_ID,"colspan":headallobj.length,"children": footer,"linedata":tritme});
				__trhtml += (JSONTOHTML.transform(__tr[0], _tbody.trfooter));
				if(tri >= pageSize) {
					break;
				}
			}
			return __trhtml;
		},
		tr:
		{
			"tag": "tr",
			"data-itmeid":function(obj){
				return "TRANSPORT_ID|"+obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				//自由列数console
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter:
		{
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+"TRANSPORT_ID|"+obj.trid+'" class="buttons-group">';
				var ahtml = "";
				var id = obj.trid;
				for (var akey in obj.children){
					var itmea = obj.children[akey];
					if(_tbody.isHasButton(itmea[0],obj.trid,_tbody.AUTHS[obj.trid])) {
						if ("events.FOLLOWUP"==itmea[0]) {
							itmea[1] = "跟进"+ TransportOrderKey[obj.linedata.TRANSPORT_STATUS][DateUtil.getLanguage()];
						}
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		isHasButton:function(itmea,trid,AUTHS)
		{
			var res = false;
			if(""+AUTHS.ID==trid+"")
			{
				if(itmea =="events.OUTBOUND")
				{
					res = AUTHS["BUTTON_OUTBOUND"];
				}
				else if(itmea =="events.FOLLOWUP")
				{
					res = AUTHS["BUTTON_FOLLOWUP"];
				}
				else if(itmea =="events.BOOKDOORORLOCATION")
				{
					res = AUTHS["BUTTON_BOOKDOORORLOCATION"];
				}
				else if(itmea =="events.BOOKDOORORLOCATIONUPDATEORVIEW")
				{
					res = AUTHS["BUTTON_BOOKDOORORLOCATIONUPDATEORVIEW"];
				}
				else
				{
					res = false;
				}
			}
			return res;
		},
		td:
		{
			tag: "td",
			class: function(obj, index) {
				//单元格样式名
				var classname = "";
				
				var keynames = types[index];
				if("basicInfo" == keynames) {
					classname = "td_" + "TRAILER";
				} else {
					classname = "td_" + keynames;
				}
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "top";
				}
				return valignsing;
			},
			html: function(obj, index) {
				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = types[index];
				if (keynames != "" && typeof keynames != "undefined") {
					if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
					}
				}
				return retuhtml;
			}
		},
		basicInfo:
		{
			"tag": "fieldset", 
			"class": "TRAILER", 
			"children": [
				{
					"tag": "span", 
					"class": "fieldset_tfoot", 
					"children": [
						{
							"tag": "em", 
							"html": "总容器：${CONTAINER_COUNT}"
						}
					]
				},{
					"tag": "legend",
					"class": "td_legend",
					"children": [
						{
							"tag": "span", "data-eventname": "transportDetail", "class": "valuestyle buttonallevnts", "html": "${TRANSPORT_ID}"
						},
						{
							"tag": "span",
							"class": function(obj,index) {
								if (obj.TRANSPORT_STATUS=="5") {
									return "status color-green";
								} else if (obj.TRANSPORT_STATUS=="2") {
									return "status color-blue";
								} else if (obj.TRANSPORT_STATUS=="3") {
									return "status color-red";
								} else if (obj.TRANSPORT_STATUS=="12") {
									return "status color-5B4B00";
								} else {
									return "status";
								}
							}, 
							"html": function (obj,index){ return TransportOrderKey[obj.TRANSPORT_STATUS][DateUtil.getLanguage()];}
						}
					]
				},
				{
					"tag": "div",
					"class": "Tractor_ulitme",
					"children": [
						{
							"tag": "ul",
							"children": [
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "创建人:"},
										{"tag": "span", "class": "valstyle", "html": "${CREATE_ACCOUNT}"}
									]
								}, 
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "TITLE:"}, 
										{"tag": "span", "class": "valstyle", "html": "${TITLE_NAME}"}
									]
								}, 
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "允许装箱:"},
										{"tag": "span", "class": "valstyle", "html": "${PACKING_ACCOUNT_NAME}"}
									]
								}, 
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "创建时间:"},
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.TRANSPORT_DATE);}}
									]
								}, 
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "更新时间:"}, 
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.UPDATEDATE);}}
									]
								}, 
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "ETA:"},
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.TRANSPORT_RECEIVE_DATE);}}
									]
								}
							]
						}
					]
				}
			]
		},
		transportInfo:
		{
			"tag":"div",
			"class":"Log_container",
			"children":[
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"收货仓库:"},{"tag":"span","class":"valstyle","html":"${RECEIVE_PSNAME}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"提货仓库:"},{"tag":"span","class":"valstyle","html":"${SEND_PSNAME}"}]}]},
				{"tag":"div","class":"box1px","html":""},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"运单号:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_WAYBILL_NUMBER}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"货运公司:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_WAYBILL_NAME}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"承运公司:"},{"tag":"span","class":"valstyle","html":"${CARRIERS}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"始发国/始发港:"},{"tag":"span","class":"valstyle","html":function(obj,index){return _tbody.emptyHandle(obj.TRANSPORT_SEND_COUNTRY_NAME,"无")+"/"+_tbody.emptyHandle(obj.TRANSPORT_SEND_PLACE,"无");}}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"目的国/目的港:"},{"tag":"span","class":"valstyle","html":function(obj,index){return _tbody.emptyHandle(obj.TRANSPORT_RECEIVE_COUNTRY_NAME,"无")+"/"+_tbody.emptyHandle(obj.TRANSPORT_RECEIVE_PLACE,"无");}}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"总体积:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_VOLUME} ${TRANSPORT_VOLUME_UNIT}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"总重量:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_WEIGHT} ${TRANSPORT_WEIGHT_UNIT}"}]}]}
			]
		},
		FLOWS:
		{
			tag: "div",
			class: "Log_container",
			html: function (obj, index) {
				//处理受理方
				var jsonFlows = obj.FLOWS;
				for (var idx=0;idx<jsonFlows.length;idx++){
					if (jsonFlows[idx].TYPE=="4") {
						jsonFlows[idx]["OVER"] = _tbody.emptyHandle(obj.ALL_OVER,"");
						jsonFlows[idx]["FLAG"] = "2";
						jsonFlows[idx]["TRANSPORT_DATE"] = _tbody.emptyHandle(obj.TRANSPORT_DATE,"0");
					} else if (jsonFlows[idx].TYPE=="11") {
						jsonFlows[idx]["OVER"] = _tbody.emptyHandle(obj.QUALITY_INSPECTION_OVER,"");
						jsonFlows[idx]["FLAG"] = _tbody.emptyHandle(obj.QUALITY_INSPECTION,"0");
						jsonFlows[idx]["RESPONSIBLE"] = _tbody.emptyHandle(obj.QUALITY_INSPECTION_RESPONSIBLE,"0");
						jsonFlows[idx]["TRANSPORT_DATE"] = _tbody.emptyHandle(obj.TRANSPORT_DATE,"0");
					}
				}
				return JSONTOHTML.transform(jsonFlows, _tbody._ulList);
			}
		},
		_ulList:
		{
			tag: "ul",
			children:[
				{
					"tag":"li",
					"class": function(obj, index) { if (index>0){return "box1px";} },
					"html":""
				},
				{
					"tag":"li",
					"children":[
						{"tag":"span","class":"namestyle","html":function(obj,index){ if ("4"==obj.TYPE) {return "货物状态:";} else if ("11"==obj.TYPE) {return "质检流程:";} else {return obj.TYPE;} }},
						{"tag":"span","class":"valstyle","html": function(obj,index){
								if (obj.TYPE) {
									if ("4"==obj.TYPE) {
										if (TransportOrderKey[_tbody.emptyHandle(obj.STATUS)])
											return TransportOrderKey[_tbody.emptyHandle(obj.STATUS)][DateUtil.getLanguage()];
									} else if ("11"==obj.TYPE){
										if (TransportOrderKey[_tbody.emptyHandle(obj.STATUS)])
											return ""+_tbody.TypeHandle(obj.RESPONSIBLE,"")+""+TransportQualityInspectionKey[_tbody.emptyHandle(obj.STATUS)][DateUtil.getLanguage()];
									} else {
										if (TransportOrderKey[_tbody.emptyHandle(obj.STATUS)])
											return TransportOrderKey[_tbody.emptyHandle(obj.STATUS)][DateUtil.getLanguage()];
									}
								}
								return "";
							}
						}
					]
				},
				{
					"tag":"li",
					"children":[
						{"tag":"span","class":"fuleft max-200px","html": "${EMPLOYE_NAMES}"},
						{"tag":"span","class":"furight text-right","html":function(obj,index){
								if (obj.FLAG==1) {
									return "";
								} else {
									if (obj.OVER!="") {
										return ""+DateUtil.changeOneDecimal(obj.OVER)+"天完成";
									} else {
										return ""+DateUtil.getDiffDate(obj.TRANSPORT_DATE,"dd")+"天";
									}
								}
								return "";
							}
						}
					]
				}
			]
		},
		LOGS:
		{
			tag: "div",
			class: "Log_container",
			html: function(obj, index) {
				var htmls = JSONTOHTML.transform(obj.LOGS, _tbody.LogItem);
				var ob = {};
				if (obj.LOGS.length>=3) {
					htmls += JSONTOHTML.transform(ob, _tbody.LogMore);
				}
				return htmls
			}
		},
		LogItem:
		{
			"tag":"ul",
			"children":[
				{
					"tag":"li",
					"class": function(obj, index) { if (index>0){return "box1px";} },
					"html":""
				},
				{"tag":"li","children":[
					{"tag":"span","class":"logvaluestyle","html":"${FOLLOWUPTYPE}:"},
					{"tag":"span","class":"logadmin","html":"${ADMINNAME}"},
					{"tag":"span","class":"logtime text-right","html":function(obj,index){ return DateUtil.shortFormatDate(obj.TRANSPORT_DATE);}}
				]},
				{"tag":"li","children":[{"tag":"span","class":"logcontent max-256px","html":"${TRANSPORT_CONTENT}"}]}
			]
		},
		LogMore:
		{
			"tag":"span","class":"valuestyle buttonallevnts","data-eventname":"events.moreLogs","children":[{"tag":"a","class":"more","html":"更多"}]
		},
		//====================================工具方法===================================//
		/**
		负责方
		**/
		TypeHandle: function(v, dv)
		{
			if(typeof v =="undefined") return dv;
			if ("1"==v) return "(发货方)";
			if ("2"==v) return "(收货方)";
			return "";
		},
		//处理空字符串
		emptyHandle:function(str,defaultValue)
		{
			if(typeof str !="undefined" && (""+str) !="")
			{
				return str;
			}
			else
			{
				if(typeof str =="undefined") return "";
				return defaultValue;
			}
		},
		_stateClass:function(state)
		{
			var tempclass="Sub_trailer";
			if(state == "0")
			{
				tempclass="Sub_trailer";
			}
			else if(state =="1")
			{
				tempclass="Sub_trailer";
			}
			else
			{
				tempclass="Sub_trailer";
			}
			return tempclass;
		},
		//====================================工具方法===================================//
		//=====================================布局工具==================================//
		//产生一个 ul 
		orderedULLayout:function(orderArr,dataObj,template)
		{
			for(var len=orderArr.length,i=0;i<len;i++)
			{
				//获取lable
				var lable = orderArr[i].lable;
				var valueKey = orderArr[i].valueKey;
				var value ="";
				if(typeof dataObj == "object")
				{
					value = dataObj[valueKey];
				}
				else if(typeof dataObj == "array")
				{
					for(var length=dataObj.length,j=0;j<length;j++)
					{
						if(dataObj[j].hasOwnProperty(valueKey))
						{
							//... 暂不支持数组排序
						}
					}
				}
				orderArr[i].value=value;
			}
			return JSONTOHTML.transform(orderArr, template);
		},
		_layLout:
		{
			tag: "ul",
			html: function(obj, index) 
			{
				return (JSONTOHTML.transform(obj, _tbody._layoutLi));
			}
		},
		_layoutLi:
		{
			tag: "li",
			html: function(obj, index) 
			{
				return '<span class="namestyle">'+obj.lable+':&nbsp;&nbsp;</span><span class="valstyle">' + _tbody.emptyHandle(obj.value,"") + '</span>';
			}
		},
		//====================================布局工具=========================================//
	}
	return {
		tbody: _tbody,
		table: _table
	}
});