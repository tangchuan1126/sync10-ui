define({
	Sync10UIPage: {
		url: "/Sync10-ui/pages"
	},
	Sync10Page: {
		url: "/Sync10/administrator"
	},
	keySearchURl: {
		url: "/Sync10/_b2b/transportOrderIndex/searchForSend"
	},
	searchURl: {
		url:"/Sync10/_b2b/transportOrder/filterDefaultForSend"
	},
	searchDefaultURl: {
		url:"/Sync10/_b2b/transportOrder/filterForSend"
	},
	autoComplete: {
		url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
	},
	storageTree: {
		url:"/Sync10/_b2b/storage/"
	},
	adminTree: {
		url:"/Sync10/_b2b/admin/"
	},
	filterModel: {
		url:"/Sync10-ui/pages/temp/filterTransport.action"
	}
	//"AsynLoadQueryTree":"/Sync10-ui/pages/Examples/js/AsynLoadQueryTree.js"
});