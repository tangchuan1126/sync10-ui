"use strict";
define(["jquery","bootstrap","metisMenu","CompondList/View","art_Dialog/dialog-plus","./view_config","./outbound"],
	function($,bootstrap,metisMenu,CompondList,Dialog,view_config,Outbound) {
		return function(options) {
			var baseUrL = "/Sync10/administrator";
			var ListView = {
				initialize: function(options) {
					var list = new CompondList(view_config, {
						renderTo: options.el,
						dataUrl: options.url,
						pageSize: 9,
						PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
						//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
					});
					//绑定事件
					list.render();
					list.on("events.OUTBOUND",function(e) {
						//发货图片
						//console.log(e);
						var obj = Outbound.showPictrueOnline('40',e.data.linedata.TRANSPORT_ID,"",0,"transport");
						if(!obj) {return;}
						if(window.top && window.top.openPictureOnlineShow){
							window.top.openPictureOnlineShow(obj);
						} else {
							var param = jQuery.param(obj);
							var uri = baseUrL +"/file/picture_online_show.html?"+param;
							var d = new Dialog({
								url: uri, title: 'photo', width: 1100, height: 600, lock: true, esc: true
							});
							d.showModal();
						}
					});
					//跟进备货
					list.on("events.FOLLOWUP",function(e) {
						//console.log(e);
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var d = new Dialog({
							url: baseUrL +"/transport/transport_follow_up.html?transport_id="+transport_id,
							title: "转运单["+transport_id+"]跟进",width:'600px',height:'300px', lock: true, esc: true
						});
						d.showModal();
					});
					list.on("events.BOOKDOORORLOCATION",function(e) {
						//司机签到
						//console.log(e);
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var d = new Dialog({
							url: baseUrL +"/transport/book_door_or_location.html?transport_id="+transport_id+"&rel_occupancy_use=1",
							//content:html,
							title: "转运单["+transport_id+"]司机签到",width:'800',height:'600', lock: true, esc: true
						});
						d.showModal();
					});
					list.on("events.BOOKDOORORLOCATIONUPDATEORVIEW",function(e) {
						//司机签到
						//console.log(e);
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var d = new Dialog({
							url: baseUrL +"/transport/book_door_or_location.html?transport_id="+transport_id+"&rel_occupancy_use=2",
							title: "转运单["+transport_id+"]司机已签到",width:'800',height:'600', lock: true, esc: true
						});
						d.showModal();
					});
					list.on("events.moreLogs",function(e) {
						//自定义事件没有linedata
						//console.log(e);
						var transport_id = e.data.TRANSPORT_ID;
						var d = new Dialog({
							url: baseUrL +"/transport/transport_logs.html?transport_id="+transport_id,
							//content:html,
							title: "日志 转运单号:["+transport_id+"]",width:'970px',height:'500px', lock: true, esc: true
						});
						d.showModal();
					});
					list.on("transportDetail",function(e) {
						window.open(baseUrL +"/transport/transport_order_out_detail.html?transport_id="+e.data.TRANSPORT_ID);
					});
					//构造一个视图
					this.view = list;
				},
				render:function(options) {
					//先清空之前的数据
					this.view.collection.remove();
					if(typeof options =="object") {
						//清空原来的页面
						$(options.el).empty();
						this.view.collection.el = options.el;
						this.view.collection.url = options.url;
						this.view.collection.PostData = (typeof options.queryCondition =="object" )?options.queryCondition:{};
					}
					this.view.render();
				},
			};
			ListView.initialize(options);
			return ListView.view;
		};
});
