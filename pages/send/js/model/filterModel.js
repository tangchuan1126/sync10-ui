define
(
["jquery","backbone","../../config"],
function($,Backbone,config)
{
	return Backbone.Model.extend(
	{
		url:config.filterModel.url,
		initialize:function()
		{

		}
	});
}
);