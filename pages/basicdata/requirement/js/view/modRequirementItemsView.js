"use strict";
define(["jquery", 
    "backbone", 
    "handlebars",
    "handlebars_ext", 
    "templates",
    "../view/listRequirementItemsView",
    "../model/requirementItemDetailsModel",
    "../model/requirementItemModel",
    "slidePanel",
    "../config",
    "assembleUtils",
    "artDialog",
    "art_Dialog/dialog-plus",
    "validate",
    "select2",
    "wrapToSelect",
    "showMessage"], 
function($, Backbone, HandleBars,HandlebarsExt, templates,ListView,models,riModels,SlidePanel,Config,assembleUtils,Dialog) {
  var editView =  Backbone.View.extend({
    template: templates.mod_view_requirement_item,
    model:new riModels.RequirementItem,
    initialize: function(options) {
      var v = this;
      v.setElement(options.el);
    },
    setView:function(views){
      this.requirementSearchResultView=views.requirementSearchResultView;
      this.requirementSearchView=views.requirementSearchView;
    },
    validator:function(){
      $("#eidt-requirement-form").validate({
          rules:{
              requirement_name:{
                  required:true,
                  maxlength:50,
                  remote: {
                    url: Config.checkName.url,
                    dataType: "json",
                    data:{
                        ri_id:$("#ri_id").val()
                    },
                    dataFilter: function (data) {
                        return $.parseJSON(data).success;
                    }
                }
              },
              display_type:{
                  required:true
              },
              description:{
                maxlength:300,
              }
          },
          messages:{
              requirement_name:{
                required:"Please input requirement name",
                remote:"Requirement name is exites",
                maxlength:"Requirement name maxlength 50"
              },
              display_type:"Please select one display type ",
              description:{
                maxlength:"Requirement description maxlength 300"
              }
          },
          errorPlacement: function(error, element) {
              if($(element).attr("id")=="description"){
                error.appendTo(element.parent()); 
              }else{
                error.appendTo(element.parent().parent()); 
              }
              
          }
      });
    },
    render: function(item,_title) {
      var v = this;
      if(typeof(item) != 'undefined' && item != "" && item!=null){
        v.model = item;        
      }else{
        v.model = new riModels.RequirementItem
      }
      v.model.set("displayTypes",v.requirementSearchView.display_type);
      var itemsDialog =art.dialog({
        title:_title
        ,lock: true
        ,width:'780px'
        ,height:'234px'
        ,opacity:0.3
        ,init:function(){
            var w1 = $(window).width(), H = $('html');
            H.css('overflow', 'hidden');
            var w2 = $(window).width();
            H.css('margin-right', (w2 - w1) + 'px');
            this.content(v.template({
                model:v.model.toJSON()
            }));
            $.unblockUI();
        }
        ,close:function(){
            document.body.parentNode.style.overflow="scroll";
            document.body.parentNode.style.marginRight="";
        },
        button: [{
            name: 'Submit',
            callback: function () {

                var dialog = this;

                var editReq = $("#eidt-requirement-form");

                if (editReq.valid()) {
                  $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
      
                    //禁用按钮
                    dialog.button({
                        name: 'Submit',
                        focus: true,
                        disabled: true
                    });
                      var hasValue = false;
                      //用JS 获取数据
                      var collection = new models.RIDetailsCollection;

                        if($("#requirement-value-list").children()){
                                
                          //获取数据
                          $("#requirement-value-list .search_result_list_content tbody").children().each(function(index){
                               hasValue = true;
                               if($(this).children().length==1){
                                
                               }else{
                                  var m = new models.RequirementItemDetails({
                                    "item_name":$(this).children("td:eq(0)").text().trim(),
                                     "item_value":$(this).children("td:eq(1)").text().trim(),
                                     "sort":$(this).children("td:eq(2)").text().trim()
                                  });
                                  if ($(this).attr("data-rid_id")!="") {
                                    m.set("rid_id",$(this).attr("data-rid_id"));
                                  };
                                  collection.add(m);
                               }
                               
                          });
                        }
                      var _item  = null; 
                      if($("#ri_id").val()!=""){
                        _item = new riModels.RequirementItem({
                            "ri_id":$("#ri_id").val()
                        });
                        // _item.set("ri_id",$("#ri_id").val());
                        _item.url+="/"+$("#ri_id").val();
                      }else{
                        _item = new riModels.RequirementItem;
                      }
                      if (collection.length>0) {
                        _item.set("children",collection.toJSON());
                      };
                      if(hasValue && collection.length==0){
                        //没有子项
                        showMessage("Please add item value,thanks","alert");
                        $("#item_name").focus();
                        dialog.button({
                            name: 'Submit',
                            focus: true,
                            disabled: false
                        });
                        $.unblockUI();
                        return false;
                      }
                       
                      
                      _item.save({
                        requirement_name:encodeURIComponent($("#requirement_name").val()),
                        display_type:encodeURIComponent($("#display_type").val()),
                        description :encodeURIComponent($("#description").val())
                      },{
                        success: function (e) {
                          v.model=null;
                          var result = e.changed.success;
                          var content = "";
                          var icon ="question";
                          if (result==0) {
                            content = "Update fail,please try again!";
                            icon ="error";

                          }else if(result==1){
                           //添加成功
                           content = "Update Success";
                            icon ="succeed";
                          }else if(result==2){
                           //已存在
                            content = "The Item Exites";
                            icon ="alert";
                          }
                          showMessage(e.changed.des,icon);

                          if(result==1){
                            setTimeout(function(){
                              v.requirementSearchResultView.render(v.requirementSearchView.searchModel);
                              dialog.close();
                            },1500);
                            //tmp.closeSelPl();
                            //tmp.requirementSearchResultView.render();
                          }else{
                                //开启按钮
                                dialog.button({
                                    name: 'Submit',
                                    focus: true,
                                    disabled: false
                                });
                            }
                            $.unblockUI();
                        },
                        error: function (e) {
                          showMessage("System Error","error");
                          $.unblockUI();
                          dialog.button({
                              name: 'Submit',
                              focus: true,
                              disabled: false
                          });
                        }
                      });

                } else {
                    editReq.validate().errorList[0].element.focus();
                    $.unblockUI();
                }
                return false;
            },
            focus: true
        }]
        ,cancel:true
        ,cancelVal:'Cancel'
    });

      $(".aui_content").css("padding","20px 0");
      // $.ajaxSettings.async = true
      $("#display_type").select2({
          placeholder: "Display Type...",
          allowClear: true,
      });

      $("#display_type").select2("val",this.model.toJSON().display_type);


      $("#display_type").bind("change",function(evt){
        var val = $("#display_type").val();
        if (val!=3&&val!=4&&val!=5) {
          $("#requirement-value-list").empty();
        }else{
          // v.listView = new ListView({el:"#requirement-value-list"});
          // v.listView.render();
          var searchConditions = new models.SearchModel({
            searchConditions:v.model.get("ri_id")
          });

          this.listView = new ListView({el:"#requirement-value-list"});
          this.listView.render(searchConditions);
        }
        $("#display_type").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end();
      });
      $("#display_type").val(v.model.toJSON().display_type);
      var display_type = parseInt(v.model.get("display_type"));
      if (display_type==3||display_type==4||display_type==5){
        var searchConditions = new models.SearchModel({
          searchConditions:v.model.get("ri_id")
        });

        this.listView = new ListView({el:"#requirement-value-list"});
        this.listView.render(searchConditions);
      }
      v.events();
      v.validator();
    },
    events:function(){
    },
    //获取url后面的参数
    GetRequest: function (name) {
      var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
      var r = window.location.search.substr(1).match(reg);  //匹配目标参数
      if (r != null) return unescape(r[2]);
      return null; //返回参数值
    }
  });

  return editView;
});