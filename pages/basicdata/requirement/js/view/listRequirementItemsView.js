"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/requirementItemDetailsModel",
    "slidePanel",
    "../config",
    "handlebars_ext",
    "artDialog",
    "showMessage",
    "mCustomScrollbar",
    "require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css"
],function($,Backbone,Handlebars,templates,models,SlidePanel,Config,HandlebarsExt){

    return Backbone.View.extend({

        template:templates.list_view_requirement_items,
        initialize:function(options){
            this.setElement(options.el);
            this.collection = new models.RIDetailsCollection;
        },
        setView:function(views){
            //this.delView = views.delView;
            // this.modView = views.modView;
        },
        render:function(searchParam){
            //搜索参数
            if(!searchParam){
               searchParam = new models.SearchModel();
                $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            }
            var tmp = this;
            this.collection.reset();
            // var model =  new models.RequirementItemDetails({item_name:"name",item_value:"1",sort:1});
            // console.log(this.collection.url);
            
            this.collection.fetch({
                data:{
                    searchConditions:searchParam.toJSON().searchConditions
                },
                success:function(collection){
                    // var model =  new models.RequirementItemDetails({item_name:"name",item_value:"1",sort:1});
                    //collection.add(model);
                    tmp.$el.html(tmp.template({
                        list:collection.toJSON()
                    }));
                    //处理
                    tmp.hiddenUpAndDown();
                    $.unblockUI();
                    //$("#list_content").mCustomScrollbar();
                }  
            });
        },
        events:{
            "click a[name='upSort']":"upSort",
            "click a[name='downSort']":"downSort",
            "click a[name='delRid']":"delRidetails",
            "click #btnNameAdd":"addRidetails", 
            "keydown #item_name":"key13Add", 
            "keydown #item_value":"key13Add",
            "keyup #item_name":"clearveVer",
            "keyup #item_value":"clearveVer"
        },
        upSort:function(evt){
            var tmp = this;
            var _tr = $(evt.target).parents("tr");
            var _upTr = $(_tr).prev();
            if(_upTr.length!=0){
                var name = $(_tr).children("td:eq(0)").text().trim();
                var upName = $(_upTr).children("td:eq(0)").text().trim();
                //console.log(name);
                var finobj={};
                finobj["item_name"]=name;
                var model = tmp.collection.findWhere(finobj);
                 finobj["item_name"]=upName;
                var upModel = tmp.collection.findWhere(finobj);
                var sort = model.get("sort");
                model.set({
                    "sort":upModel.get("sort")
                });
                upModel.set({
                    "sort":sort
                });
                //
                tmp.collection.sort();
                tmp.reloadTable();
            }
            
        },
        downSort:function(evt){

            var tmp = this;
            var _tr = $(evt.target).parents("tr");
            var _nTr = $(_tr).next();
            if(_nTr.length!=0){
                var name = $(_tr).children("td:eq(0)").text().trim();
                var upName = $(_nTr).children("td:eq(0)").text().trim();
                //console.log(name);
                var finobj={};
                finobj["item_name"]=name;
                var model = tmp.collection.findWhere(finobj);
                 finobj["item_name"]=upName;
                var nModel = tmp.collection.findWhere(finobj);
                var sort = model.get("sort");
                model.set({
                    "sort":nModel.get("sort")
                });
                nModel.set({
                    "sort":sort
                });
                //
                tmp.collection.sort();
                //console.log(model.cid);
                tmp.reloadTable();
            }
        },
        delRidetails:function(evt){
            var tmp = this;
            var rid_id = $(evt.target).parents("tr").data("rid_id");
            if (rid_id) {
                // 删除 直接删除 要有删除提示
                //console.log("删除rid_id"+rid_id);
                var model = new models.RequirementItemDetails({
                    rid_id:rid_id
                })
                model.url+="/"+rid_id;
                model.destroy({
                    success: function(model, response){

                        if(response.success == 'true'){
                            tmp.delReload(evt);
                        }else{

                            art.dialog({
                                title:'Message'
                                ,lock:true
                                ,opacity:0.3
                                ,content:response.error
                                ,icon:'warning'
                                ,ok:true
                                ,okVal:'Sure'
                            });
                        }
                    }
                });
            }else{
                tmp.delReload(evt);
            }
            
        },
        delReload:function(evt){
            var tmp = this;
            var name = $(evt.target).parents("tr").children("td:eq(0)").text().trim();
            //console.log(name);
            var finobj={};
            finobj["item_name"]=name;
            var model = tmp.collection.findWhere(finobj);
            tmp.collection.remove(model);
            //TODO 重新设置排序 Sort
            tmp.resetSort();
            tmp.reloadTable();
        },
        key13Add:function(evt){
            if(evt.keyCode==13){
               this.addRidetails();
            }
        },
        addRidetails:function(evt){
            var v = this;
            var name = $("#item_name").val();
            var val = $("#item_value").val();
            //TODO验证Value 输入如果不不是数字
            //console.log("addRidetails");
            if(!v.verification()){
                return;
            }
            if(""!=val){
                //验证value 
                if (v.isExists("item_value",val)) {
                    return;
                };
            }else{
                val = this.getMaxValue();
            }
            
            var order = this.getMaxOrder();
            
            if (v.isExists("item_name",name)) {
                return;
            };
            //添加 
            this.collection.add({
                item_name:name,
                item_value:parseInt(val),
                sort:order
            });
            var tmp = this;
            tmp.reloadTable();
             $("#item_name").focus();
        },
        // 获取本this.collection 中的排序最大值
        getMaxOrder:function(){
            //if(collection==null && )
            var collection = this.collection;
            var order =1+collection.length;
            return order;
        },
        getMaxValue:function(){
            //if(collection==null && )
            var val =1;
            var collection = this.collection;
            if (collection==null || collection.length==0) {
                return val;
            }else{
                var tmp = 1;
                for (var i = collection.length - 1; i >= 0; i--) {
                    tmp = parseInt(collection.at(i).toJSON().item_value);
                    if(val<tmp){
                        val = tmp;
                    }
                };
            }
            return val+1;
        },
        verification:function(){
            //验证
            var v = this;
            if(!v.verificationElment($("#item_name"))){
               return false;
            }
            // 验证该类型中是否有相同的名字
            return true;
        },
        verificationElment:function(elment){
          if ($(elment).val() == "" || $(elment).val() == undefined ||$(elment).val().length>50) {
                if (!$(elment).parent().hasClass("has-feedback")) {
                    $(elment).parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $(elment).focus();
                }
                return false;
            }else {
              $(elment).parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
               return true;
            }
           
        },
        clearveVer:function(evt){
            if(evt.keyCode!=13){
                $(evt.target).parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end();
            }
        },
        isExists:function(field,val){
            var finobj={};
            if(field=="item_value"){
                val = parseInt(val);
            }
            finobj[field]=val;
            var model = this.collection.findWhere(finobj);
            if(undefined!=model){
                //已存在
                var elment = $("#"+field);
                if (!$(elment).parent().hasClass("has-feedback")) {
                    $(elment).parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $(elment).focus();
                }
                return true;
            }
            return false;
        },
        resetSort:function(){
            var sort = 0;
            var collection = this.collection;
            if (collection!=null || collection.length>=0) {
                for (var i = 0,j= collection.length; i <j; i++) {
                    sort++;
                    //tmp = parseInt(collection.at(i).toJSON().item_value);
                    collection.at(i).set({
                        "sort":""+sort
                    });
                };
            }
        },
        hiddenUpAndDown:function(){
            $(".search_result_list_content tr:first-child").find("a[name='upSort']").css("display","none");
             $(".search_result_list_content tr:last-child").find("a[name='downSort']").css("display","none");
        },
        reloadTable:function(){
            var tmp = this;
            tmp.$el.html(tmp.template({
                list:tmp.collection.toJSON()
            }));
            tmp.hiddenUpAndDown();
        },
        checkIsInteger :function(str) { 
            //如果为空，则通过校验 
            if(str == "") 
               return true; 
               if(/^(\-?)(\d+)$/.test(str)) 
               return true; 
             else 
               return false; 
         } 
    });
});
