define([ 
	"jquery", 
	"artDialog" 
],
function($) {

	return function(title, content, width, saveCallback, saveCallbackParams, cancelCallback) {
		
		art.dialog({
			title : title,
			width : width,
			lock : true,
			opacity : 0.3,
			content : content,
			okVal : 'Submit',
			ok : function() {
				$.blockUI({message : '<div class="block_message">Please wait......</div>'});
				return saveCallback(saveCallbackParams);
			},
			cancelVal : 'Cancel',
			cancel : true
		});
	};

});