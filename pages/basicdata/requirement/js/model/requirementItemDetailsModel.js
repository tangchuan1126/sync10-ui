"use strict"
define([
	"../config",
    "jquery",
    "backbone"
],function(page_config, $, Backbone){
	var requirementItemDetails = Backbone.Model.extend({
		url:page_config.riDetailsUrl.url,
		idAttribute: "rid_id",
		defaults:{
			ri_id:"",
			item_name:"",
			item_value:"",
			sort:""	
		}
	});


	//requirementItemDetails  集合
	var rIDetailsCollection = Backbone.Collection.extend({
        url: page_config.riDetailsUrl.url,
        model: requirementItemDetails,
        parse: function (response) {
            return response.list;
        },
        comparator : function(m1, m2) {  
        var sort1 = m1.get('sort');  
        var sort2 = m2.get('sort');  
  
        if(sort1 > sort2) {  
            return 1;  
        } else {  
            return 0;  
        }  
    }  
    });
	 //搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            searchConditions: ""
        }
    });
	
	return {
    	RequirementItemDetails:requirementItemDetails,
   	 	RIDetailsCollection: rIDetailsCollection,
   	 	SearchModel:SearchModel
	}
});