"use strict"
define([
	"../config",
    "jquery",
    "backbone"
],function(page_config, $, Backbone){
	var requirementItem = Backbone.Model.extend({
		url:page_config.rItemUrl.url,
		idAttribute: "ri_id",
		defaults:{
			requirement_name:"",
			display_type:"",
			description:"",
			children:[]
		}
	});
	
	return {
    	RequirementItem:requirementItem
	}
});