"use strict";
define([
	"../config", 
	"jquery", 
	"backbone", 
	"handlebars", 
	"handlebars_ext" 
],
function(config, $, Backbone, Handlebars, HandlebarsExt) {

	var RequirementModel = Backbone.Model.extend({
		url : config.requirementUrl.url,
		idAttribute : "ri_id",
		defaults : {
			requirement_name : "",
			display_type : "",
			description : "",
			children : []
		},
	});

	var RequirementCollection = Backbone.Collection.extend({
		model : RequirementModel,
		url : config.requirementUrl.url,

		comparator : function(item) {

		},
		parse : function(response) {
			if (response.pagectrl) {
				RequirementCollection.pageCtrl = response.pagectrl;
			}
			return response.list;
		},
		initialize : function() {

		}
	}, 
	{
		pageCtrl : {
			pageNo : 1,
			pageSize : 10
		}
	});
	
	//搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            searchRname: "",
            searchDisplaytype: "",
            searchIsactive:"2",
            pageNo:"",
            cmd:""
        }
    });

	return {
		RequirementModel : RequirementModel,
		RequirementCollection : RequirementCollection,
		SearchModel : SearchModel
	};
});