(function(){
    var configObj = {
    	//默认配置
        requirementUrl:{
            url:"/Sync10/basicdata/requirement/handleRItems"
        },
        // 配置 requirementItemDetails 的URL
	 	riDetailsUrl:{
	 		url  :"/Sync10/basicdata/requirement/handleRiDetails"
	 	},
	 	// Display Type URL
	 	displayTypeUrl:{
	 		url  :"/Sync10-ui/pages/basicdata/json/DisplayType.json"
	 	},
	 	//
	 	rItemUrl:
	 	{
	 		url:"/Sync10/basicdata/requirement/handleRItems"
	 	},
	 	checkName:{
	 		url:"/Sync10/basicdata/requirement/check_name"
	 	}
    };
    
	define(configObj);
	
}).call(this);