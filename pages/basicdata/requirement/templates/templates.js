(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_view_requirement_item'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                        <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div id=\"addRequirementContainer\">\r\n    <div id=\"\" class=\"tab-pane\">\r\n        <form class=\"form-horizontal\" role=\"form\">\r\n          <div class=\"form-group\">\r\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\"><span class=\"red_star\">*&nbsp;</span>Requirement Name:</label>\r\n            <div class=\"col-sm-3\">\r\n                 <input id=\"requirement_name\" name=\"requirement_name\" type=\"text\" class=\"form-control \"  placeholder=\"Requirement Name\" />\r\n            </div>\r\n        </div> \r\n        <div class=\"form-group\">\r\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\"><span class=\"red_star\">*&nbsp;</span>Display Type:</label>\r\n            <div class=\"col-sm-3\">\r\n                <select id=\"display_type\" style=\"width:100%;\">\r\n                    <option value=\"\"></option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.displayTypes : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                </select>\r\n                <!--<input id=\"display_type\" name=\"display_type\" type=\"text\" class=\"form-control immybox immybox_witharrow\"  placeholder=\"Display Type\" />-->\r\n            </div>\r\n        </div>\r\n         <div class=\"form-group\" id=\"requirement-value-list\">\r\n            <!-- 添加值信息 表格形式 -->\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"packages_content2\" class=\"col-sm-2 control-label\"><span class=\"\"></span>Description :</label>\r\n            <div class=\"col-sm-6\">\r\n                <textarea class=\"form-control\" rows=\"5\" id=\"description\"></textarea>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\" style=\"margin-bottom:0\">\r\n            <div class=\"col-sm-8\">\r\n              <button type=\"button\" class=\"btn btn-info pull-right\" id=\"btnSave\">Submit</button>\r\n            </div>\r\n        </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n<div class=\"clear\"></div>";
},"useData":true});
templates['del_requirement'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div class=\"delete_requirement_dialog\">\r\n    Delete\r\n	<span>\r\n        "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + "\r\n    </span>\r\n    ?\r\n</div>\r\n";
},"useData":true});
templates['list_view_requirement_items'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.list : depth0)) != null ? stack1.length : stack1), depth0))
    + "\r\n                <tr data-rid_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.rid_id : depth0), depth0))
    + "\">\r\n                    <td valign=\"middle\" class=\"col-sm-5\">\r\n                        "
    + escapeExpression(lambda((depth0 != null ? depth0.item_name : depth0), depth0))
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" class=\"col-sm-2\">\r\n                        "
    + escapeExpression(lambda((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" class=\"col-sm-2\">\r\n                        "
    + escapeExpression(lambda((depth0 != null ? depth0.sort : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" class=\"col-sm-3\" style=\"text-align: center\">\r\n                        <a href=\"javascript:void(0)\" name=\"upSort\" class=\"btn btn-default fa fa-arrow-up \" style=\"margin-left: 1px;\" title=\"UP\"></a>\r\n                        <a href=\"javascript:void(0)\" name=\"downSort\" class=\"btn btn-default fa fa-arrow-down\" style=\"margin-left: 1px;\" title=\"UP\"></a>\r\n                        <a href=\"javascript:void(0)\" name=\"delRid\" class=\"btn btn-default fa fa-remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\r\n                    </td>\r\n                </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "                <tr>\r\n                    <td style=\"text-align:center;line-height:40px;height:40px;background:#FEFEFE;border:1px solid silver;\">No Data.</td>\r\n                </tr>\r\n                \r\n                 \r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<label for=\"packages_content\" class=\"col-sm-3 control-label\">\r\n<span class=\"red_star\">*&nbsp;</span>Instruction Items:</label>\r\n<div class=\"col-sm-9\">\r\n    <div class=\"form-group\" style=\" margin-top: 0px;\">\r\n        <div class=\"col-sm-6\">\r\n             <input id=\"item_name\" name=\"item_name\" type=\"text\" class=\"form-control \"  placeholder=\"Item Name\" />\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n             <input id=\"item_value\" name=\"item_value\" type=\"number\" class=\"form-control \"  placeholder=\"Item Value\" min=\"0\" max=\"999999999\" onkeyup=\"value=value.replace(/[^\\d]/g,'')\" />\r\n        </div>\r\n        <div class=\"col-sm-2\">\r\n          <button type=\"button\" class=\"btn btn-info pull-right\" id=\"btnNameAdd\">Add</button>\r\n        </div>\r\n    </div>\r\n    <div class=\"search_result_list\" style=\"max-height: 216px;\r\n  overflow-x: hidden;overflow-y: auto;\">\r\n        <div id=\"item_data_box\">\r\n            <table class=\"search_result_list_header\" cellspacing=\"0\">\r\n                <tr>\r\n                    <th class=\"col-sm-5\">Name</th>\r\n                    <th class=\"col-sm-2\">Value</th>\r\n                    <th class=\"col-sm-2\">Sort</th>\r\n                    <th class=\"col-sm-3\" style=\"border-right:0px;\">\r\n                        Operation\r\n                    </th>\r\n                </tr>\r\n            </table>\r\n\r\n            <table class=\"search_result_list_content\" cellspacing=\"0\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "              \r\n            </table>\r\n            \r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
templates['mod_view_requirement_item'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "                        <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.value : depth0), depth0))
    + "\"\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.value : depth0), ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1), {"name":"ifCond","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">\r\n                            "
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "\r\n                        </option>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "                                selected = \"true\"\r\n                            ";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = " <!-- 修改--> \r\n <div id=\"addRequirementContainer\" style=\"width:780px; max-height: 482px;\r\n  overflow-x: hidden;overflow-y: auto;\">\r\n    <div id=\"\" class=\"tab-pane\">\r\n        <form class=\"form-horizontal\" role=\"form\" id=\"eidt-requirement-form\">\r\n          <div class=\"form-group\">\r\n            <label for=\"packages_content\" class=\"col-sm-3 control-label\"><span class=\"red_star\">*&nbsp;</span>Instruction Name:</label>\r\n            <div class=\"col-sm-5\">\r\n                 <input id=\"requirement_name\" name=\"requirement_name\" type=\"text\" class=\"form-control \" style=\" width: 272px; \"  placeholder=\"Instruction Name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + "\" />\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"packages_content\" class=\"col-sm-3 control-label\"><span class=\"red_star\">*&nbsp;</span>Display Type:</label>\r\n            <div class=\"col-sm-5\">\r\n                 <select id=\"display_type\" style=\" width: 272px; \"  name=\"display_type\" >\r\n                 <option value=\"\"></option>\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.displayTypes : stack1), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                </select>\r\n            </div>\r\n        </div>\r\n         <div class=\"form-group\" id=\"requirement-value-list\">\r\n            <!-- 添加值信息 表格形式 -->\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"packages_content2\" class=\"col-sm-3 control-label\"><span class=\"\"></span>Description :</label>\r\n            <div class=\"col-sm-7\">\r\n                <textarea class=\"form-control\" rows=\"3\" id=\"description\" name=\"description\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.description : stack1), depth0))
    + "</textarea>\r\n            </div>\r\n        </div>\r\n        <input id=\"ri_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\"/>\r\n        </form>\r\n    </div>\r\n</div>\r\n<div class=\"clear\"></div>";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = " ";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.resultList : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " ";
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "\r\n                            <tr data-ri_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "\">\r\n                                <td style=\"text-align: center;\">"
    + escapeExpression(lambda((depth0 != null ? depth0.requirement_name : depth0), depth0))
    + "</td>\r\n\r\n                                <td style=\"text-align: center;\">"
    + escapeExpression(lambda((depth0 != null ? depth0.display_type_txt : depth0), depth0))
    + "</td>\r\n                                <td valign=\"middle\">\r\n\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.children : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "                                    </div>\r\n\r\n                                </td>\r\n                                <td valign=\"middle\">\r\n                                    <div class=\"_desc box\">"
    + escapeExpression(lambda((depth0 != null ? depth0.description : depth0), depth0))
    + "</div>\r\n                                </td>\r\n                                <td valign=\"middle\" style=\"text-align: center\">\r\n\r\n                                    <div class=\"btn-group\" role=\"group\">\r\n\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.active : depth0), 0, "=", {"name":"xifCond","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.active : depth0), 1, "=", {"name":"xifCond","hash":{},"fn":this.program(10, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n                                    </div>\r\n                                </td>\r\n                            </tr>\r\n                            ";
},"3":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "\r\n\r\n                                    <!--\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, (data && data.index), 2, "<", {"name":"xifCond","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, (data && data.index), 2, ">=", {"name":"xifCond","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        -->\r\n                                    <div style=\"\">\r\n\r\n\r\n\r\n                                        <div style=\"text-align:right;width:100px;display:inline-block\">\r\n                                            "
    + escapeExpression(lambda((depth0 != null ? depth0.item_name : depth0), depth0))
    + " :\r\n                                        </div>\r\n                                        <div style=\"text-align:left;display:inline-block\">\r\n                                            "
    + escapeExpression(lambda((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\r\n                                        </div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <div style=\"padding-top:10px;\">\r\n                                    <div style=\"text-align:right;width:100px;display:inline-block\">\r\n                                        "
    + escapeExpression(lambda((depth0 != null ? depth0.item_name : depth0), depth0))
    + ":\r\n                                    </div>\r\n                                    <div style=\"text-align:left;display:inline-block\">\r\n                                        "
    + escapeExpression(lambda((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\r\n                                    </div>\r\n                                <div>\r\n";
},"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "                                    ...\r\n                                "
    + escapeExpression(((helper = (helper = helpers['break'] || (depth0 != null ? depth0['break'] : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"break","hash":{},"data":data}) : helper)))
    + "\r\n";
},"8":function(depth0,helpers,partials,data) {
  return "\r\n                                        <a href=\"javascript:void(0)\" is_used=\"true\" name=\"modify\" class=\"btn btn-default disabled\" title=\"Edit\" style=\"font-size:12px;height: 28px;opacity:0;cursor: default;\"><i class=\"fa fa-pencil\" style=\"cursor: default;\"></i>  Edit</a>\r\n\r\n                                        <a href=\"javascript:void(0)\" name=\"active\" class=\"btn btn-default \" title=\"Active\" style=\"font-size:12px;height: 28px;\"><i class=\"fa fa-check-circle\"></i>  Active</a>\r\n";
  },"10":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.is_used : depth0), true, "=", {"name":"xifCond","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.is_used : depth0), true, "!=", {"name":"xifCond","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n                                        <a href=\"javascript:void(0)\" name=\"inactive\" class=\"btn btn-default\" title=\"Inactive\" style=\"font-size:12px;height: 28px;\"><i class=\"fa fa-minus-circle\"></i>  Inactive</a>\r\n";
},"11":function(depth0,helpers,partials,data) {
  return "                                        <a href=\"javascript:void(0)\" is_used=\"true\" name=\"modify\" class=\"btn btn-default disabled\" title=\"Edit\" style=\"font-size:12px;height: 28px;opacity:0;cursor: default;\"><i class=\"fa fa-pencil\" style=\"cursor: default;\"></i>  Edit</a>\r\n";
  },"13":function(depth0,helpers,partials,data) {
  return "                                        <a href=\"javascript:void(0)\" is_used=\"false\" name=\"modify\" class=\"btn btn-default\" title=\"Edit\" style=\"font-size:12px;height: 28px;\"><i class=\"fa fa-pencil\"></i>  Edit</a>\r\n";
  },"15":function(depth0,helpers,partials,data) {
  return "\r\n                            <tr>\r\n                                <td colspan=\"5\" style=\"text-align: center; line-height: 180px; height: 180px; background: #E6F3C5; border: 1px solid silver;\">No Data.</td>\r\n                            </tr>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<style>\r\n\r\n	div.box \r\n	{\r\n		height: 2.85em;\r\n		/*padding: 15px 20px 10px 20px;*/\r\n	}\r\n	div.box.opened\r\n	{\r\n		height: auto;\r\n	}\r\n	div.box .toggle .closed,\r\n	div.box.opened .toggle .open\r\n	{\r\n		display: none;\r\n	}\r\n	div.box .toggle .opened,\r\n	div.box.opened .toggle .closed \r\n	{\r\n		display: inline;\r\n	}\r\n\r\n	#search_result tbody ._desc{visibility:hidden}\r\n    .search_result_list .search_result_body { border:solid 1px #ddd;}\r\n\r\n</style>\r\n<div class=\"search_result_list\">\r\n            <div>\r\n                <div class=\"search_result_panel_title\"><span class=\"panel-heading\"><i class=\"fa fa-table\"></i>&nbsp;Instruction</span></div>\r\n                <div class=\"search_result_body\">\r\n                    <table class=\"table table-striped\" cellspacing=\"0\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th width=\"20%\">Instruction Name</th>\r\n\r\n                                <th width=\"15%\">Display Type</th>\r\n                                <th width=\"25%\">Instruction Items</th>\r\n                                <th width=\"20%\">Description</th>\r\n\r\n                                <th width=\"20%\">Operation</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.resultList : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(15, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n                <ul id=\"pagination\" class=\"clearfix pagebox\"></ul>\r\n            </div>\r\n\r\n        </div>\r\n";
},"useData":true});
templates['search_templet'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"advanced_search_tab_content\">\r\n\r\n\r\n    <form class=\"form-inline\">\r\n        <div class=\"form-group col-md-3\">\r\n            <input id=\"_r_name\" value=\"\" type=\"text\" class=\"form-control\" placeholder=\"Instruction Name\" style=\"width:100%;\" />\r\n        </div>\r\n\r\n        <div class=\"form-group col-md-2\"><select id=\"_display_type\" name=\"_display_type\" style=\"width:100%;\">\r\n            <option value=\"\"></option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.displayTypes : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </select></div>\r\n\r\n        <div class=\"form-group col-md-2\">\r\n            <select id=\"_is_active\" name=\"_is_active\" style=\"width:100%;\">\r\n                <option value=\"\"></option>\r\n                <option value=\"2\">All</option>\r\n                <option value=\"1\" selected>Active</option>\r\n                <option value=\"0\">Inactive</option>\r\n\r\n            </select>\r\n        </div>\r\n\r\n        <div class=\"form-group col-md-2\"><a id=\"advanced_search_btn\" name=\"advanced_search_btn\" href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i>   Search</a></div>\r\n\r\n        <a id=\"addRequirement\" href=\"javascript:void(0)\" class=\"btn btn-warning\" ><i class=\"fa fa-plus\"></i>   Add Instruction</a>\r\n    </form>\r\n\r\n	<!--<table>\r\n		<tr>\r\n			<td>Requirement Name:</br> \r\n				\r\n			</td>\r\n			\r\n			<td style=\"padding-left: 20px;\">Display Type:</br>\r\n				\r\n			</td>\r\n\r\n			<td style=\"padding-left: 20px;\">Is Active:</br>\r\n				\r\n			</td>\r\n			\r\n			<td style=\"padding-left: 20px;\">\r\n				 <br/>\r\n				\r\n			</td>\r\n		</tr>\r\n	</table>-->\r\n</div>\r\n\r\n\r\n";
},"useData":true});
})();