define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_view_requirement_item'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return " <div id=\"addRequirementContainer\">\n    <div id=\"\" class=\"tab-pane\">\n        <form class=\"form-horizontal\" role=\"form\">\n          <div class=\"form-group\">\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\"><span class=\"red_star\">*&nbsp;</span>Requirement Name:</label>\n            <div class=\"col-sm-3\">\n                 <input id=\"requirement_name\" name=\"requirement_name\" type=\"text\" class=\"form-control \"  placeholder=\"Requirement Name\" />\n            </div>\n        </div> \n        <div class=\"form-group\">\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\"><span class=\"red_star\">*&nbsp;</span>Display Type:</label>\n            <div class=\"col-sm-3\">\n                <select id=\"display_type\" style=\"width:100%;\">\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.displayTypes : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n                <!--<input id=\"display_type\" name=\"display_type\" type=\"text\" class=\"form-control immybox immybox_witharrow\"  placeholder=\"Display Type\" />-->\n            </div>\n        </div>\n         <div class=\"form-group\" id=\"requirement-value-list\">\n            <!-- 添加值信息 表格形式 -->\n        </div>\n        <div class=\"form-group\">\n            <label for=\"packages_content2\" class=\"col-sm-2 control-label\"><span class=\"\"></span>Description :</label>\n            <div class=\"col-sm-6\">\n                <textarea class=\"form-control\" rows=\"5\" id=\"description\"></textarea>\n            </div>\n        </div>\n        <div class=\"form-group\" style=\"margin-bottom:0\">\n            <div class=\"col-sm-8\">\n              <button type=\"button\" class=\"btn btn-info pull-right\" id=\"btnSave\">Submit</button>\n            </div>\n        </div>\n        </form>\n    </div>\n</div>\n<div class=\"clear\"></div>";
},"useData":true});
templates['del_requirement'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_requirement_dialog\">\n    Delete\n	<span>\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + "\n    </span>\n    ?\n</div>\n";
},"useData":true});
templates['list_view_requirement_items'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.list : depth0)) != null ? stack1.length : stack1), depth0))
    + "\n                <tr data-rid_id=\""
    + alias2(alias1((depth0 != null ? depth0.rid_id : depth0), depth0))
    + "\">\n                    <td valign=\"middle\" class=\"col-sm-5\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" class=\"col-sm-2\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" class=\"col-sm-2\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.sort : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" class=\"col-sm-3\" style=\"text-align: center\">\n                        <a href=\"javascript:void(0)\" name=\"upSort\" class=\"btn btn-default fa fa-arrow-up \" style=\"margin-left: 1px;\" title=\"UP\"></a>\n                        <a href=\"javascript:void(0)\" name=\"downSort\" class=\"btn btn-default fa fa-arrow-down\" style=\"margin-left: 1px;\" title=\"UP\"></a>\n                        <a href=\"javascript:void(0)\" name=\"delRid\" class=\"btn btn-default fa fa-remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\n                    </td>\n                </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "                <tr>\n                    <td style=\"text-align:center;line-height:40px;height:40px;background:#FEFEFE;border:1px solid silver;\">No Data.</td>\n                </tr>\n                \n                 \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<label for=\"packages_content\" class=\"col-sm-3 control-label\">\n<span class=\"red_star\">*&nbsp;</span>Instruction Items:</label>\n<div class=\"col-sm-9\">\n    <div class=\"form-group\" style=\" margin-top: 0px;\">\n        <div class=\"col-sm-6\">\n             <input id=\"item_name\" name=\"item_name\" type=\"text\" class=\"form-control \"  placeholder=\"Item Name\" />\n        </div>\n        <div class=\"col-sm-4\">\n             <input id=\"item_value\" name=\"item_value\" type=\"number\" class=\"form-control \"  placeholder=\"Item Value\" min=\"0\" max=\"999999999\" onkeyup=\"value=value.replace(/[^\\d]/g,'')\" />\n        </div>\n        <div class=\"col-sm-2\">\n          <button type=\"button\" class=\"btn btn-info pull-right\" id=\"btnNameAdd\">Add</button>\n        </div>\n    </div>\n    <div class=\"search_result_list\" style=\"max-height: 216px;\n  overflow-x: hidden;overflow-y: auto;\">\n        <div id=\"item_data_box\">\n            <table class=\"search_result_list_header\" cellspacing=\"0\">\n                <tr>\n                    <th class=\"col-sm-5\">Name</th>\n                    <th class=\"col-sm-2\">Value</th>\n                    <th class=\"col-sm-2\">Sort</th>\n                    <th class=\"col-sm-3\" style=\"border-right:0px;\">\n                        Operation\n                    </th>\n                </tr>\n            </table>\n\n            <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "              \n            </table>\n            \n        </div>\n    </div>\n</div>";
},"useData":true});
templates['mod_view_requirement_item'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\"\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.value : depth0),((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),{"name":"ifCond","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n                            "
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "\n                        </option>\n";
},"2":function(depth0,helpers,partials,data) {
    return "                                selected = \"true\"\n                            ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return " <!-- 修改--> \n <div id=\"addRequirementContainer\" style=\"width:780px; max-height: 482px;\n  overflow-x: hidden;overflow-y: auto;\">\n    <div id=\"\" class=\"tab-pane\">\n        <form class=\"form-horizontal\" role=\"form\" id=\"eidt-requirement-form\">\n          <div class=\"form-group\">\n            <label for=\"packages_content\" class=\"col-sm-3 control-label\"><span class=\"red_star\">*&nbsp;</span>Instruction Name:</label>\n            <div class=\"col-sm-5\">\n                 <input id=\"requirement_name\" name=\"requirement_name\" type=\"text\" class=\"form-control \" style=\" width: 272px; \"  placeholder=\"Instruction Name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + "\" />\n            </div>\n        </div>\n        <div class=\"form-group\">\n            <label for=\"packages_content\" class=\"col-sm-3 control-label\"><span class=\"red_star\">*&nbsp;</span>Display Type:</label>\n            <div class=\"col-sm-5\">\n                 <select id=\"display_type\" style=\" width: 272px; \"  name=\"display_type\" >\n                 <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.displayTypes : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n        </div>\n         <div class=\"form-group\" id=\"requirement-value-list\">\n            <!-- 添加值信息 表格形式 -->\n        </div>\n        <div class=\"form-group\">\n            <label for=\"packages_content2\" class=\"col-sm-3 control-label\"><span class=\"\"></span>Description :</label>\n            <div class=\"col-sm-7\">\n                <textarea class=\"form-control\" rows=\"3\" id=\"description\" name=\"description\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.description : stack1), depth0))
    + "</textarea>\n            </div>\n        </div>\n        <input id=\"ri_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\"/>\n        </form>\n    </div>\n</div>\n<div class=\"clear\"></div>";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return " "
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " ";
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "\n                            <tr data-ri_id=\""
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "\">\n                                <td style=\"text-align: center;\">"
    + alias2(alias1((depth0 != null ? depth0.requirement_name : depth0), depth0))
    + "</td>\n\n                                <td style=\"text-align: center;\">"
    + alias2(alias1((depth0 != null ? depth0.display_type_txt : depth0), depth0))
    + "</td>\n                                <td valign=\"middle\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                    </div>\n\n                                </td>\n                                <td valign=\"middle\">\n                                    <div class=\"_desc box\">"
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "</div>\n                                </td>\n                                <td valign=\"middle\" style=\"text-align: center\">\n\n                                    <div class=\"btn-group\" role=\"group\">\n\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),0,"=",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),1,"=",{"name":"xifCond","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                                    </div>\n                                </td>\n                            </tr>\n                            ";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing, alias2=this.lambda, alias3=this.escapeExpression;

  return "\n\n                                    <!--\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(data && data.index),2,"<",{"name":"xifCond","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(data && data.index),2,">=",{"name":"xifCond","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        -->\n                                    <div style=\"\">\n\n\n\n                                        <div style=\"text-align:right;width:100px;display:inline-block\">\n                                            "
    + alias3(alias2((depth0 != null ? depth0.item_name : depth0), depth0))
    + " :\n                                        </div>\n                                        <div style=\"text-align:left;display:inline-block\">\n                                            "
    + alias3(alias2((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\n                                        </div>\n";
},"4":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <div style=\"padding-top:10px;\">\n                                    <div style=\"text-align:right;width:100px;display:inline-block\">\n                                        "
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + ":\n                                    </div>\n                                    <div style=\"text-align:left;display:inline-block\">\n                                        "
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\n                                    </div>\n                                <div>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper;

  return "                                    ...\n                                "
    + this.escapeExpression(((helper = (helper = helpers['break'] || (depth0 != null ? depth0['break'] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"break","hash":{},"data":data}) : helper)))
    + "\n";
},"8":function(depth0,helpers,partials,data) {
    return "\n                                        <a href=\"javascript:void(0)\" is_used=\"true\" name=\"modify\" class=\"btn btn-default disabled\" title=\"Edit\" style=\"font-size:12px;height: 28px;opacity:0;cursor: default;\"><i class=\"fa fa-pencil\" style=\"cursor: default;\"></i>  Edit</a>\n\n                                        <a href=\"javascript:void(0)\" name=\"active\" class=\"btn btn-default \" title=\"Active\" style=\"font-size:12px;height: 28px;\"><i class=\"fa fa-check-circle\"></i>  Active</a>\n";
},"10":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing;

  return "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.is_used : depth0),true,"=",{"name":"xifCond","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.is_used : depth0),true,"!=",{"name":"xifCond","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                                        <a href=\"javascript:void(0)\" name=\"inactive\" class=\"btn btn-default\" title=\"Inactive\" style=\"font-size:12px;height: 28px;\"><i class=\"fa fa-minus-circle\"></i>  Inactive</a>\n";
},"11":function(depth0,helpers,partials,data) {
    return "                                        <a href=\"javascript:void(0)\" is_used=\"true\" name=\"modify\" class=\"btn btn-default disabled\" title=\"Edit\" style=\"font-size:12px;height: 28px;opacity:0;cursor: default;\"><i class=\"fa fa-pencil\" style=\"cursor: default;\"></i>  Edit</a>\n";
},"13":function(depth0,helpers,partials,data) {
    return "                                        <a href=\"javascript:void(0)\" is_used=\"false\" name=\"modify\" class=\"btn btn-default\" title=\"Edit\" style=\"font-size:12px;height: 28px;\"><i class=\"fa fa-pencil\"></i>  Edit</a>\n";
},"15":function(depth0,helpers,partials,data) {
    return "\n                            <tr>\n                                <td colspan=\"5\" style=\"text-align: center; line-height: 180px; height: 180px; background: #E6F3C5; border: 1px solid silver;\">No Data.</td>\n                            </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style>\n\n	div.box \n	{\n		height: 2.85em;\n		/*padding: 15px 20px 10px 20px;*/\n	}\n	div.box.opened\n	{\n		height: auto;\n	}\n	div.box .toggle .closed,\n	div.box.opened .toggle .open\n	{\n		display: none;\n	}\n	div.box .toggle .opened,\n	div.box.opened .toggle .closed \n	{\n		display: inline;\n	}\n\n	#search_result tbody ._desc{visibility:hidden}\n    .search_result_list .search_result_body { border:solid 1px #ddd;}\n\n</style>\n<div class=\"search_result_list\">\n            <div>\n                <div class=\"search_result_panel_title\"><span class=\"panel-heading\"><i class=\"fa fa-table\"></i>&nbsp;Instruction</span></div>\n                <div class=\"search_result_body\">\n                    <table class=\"table table-striped\" cellspacing=\"0\">\n                        <thead>\n                            <tr>\n                                <th width=\"20%\">Instruction Name</th>\n\n                                <th width=\"15%\">Display Type</th>\n                                <th width=\"25%\">Instruction Items</th>\n                                <th width=\"20%\">Description</th>\n\n                                <th width=\"20%\">Operation</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(15, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </tbody>\n                    </table>\n                </div>\n            </div>\n\n            <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n                <ul id=\"pagination\" class=\"clearfix pagebox\"></ul>\n            </div>\n\n        </div>\n";
},"useData":true});
templates['search_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"advanced_search_tab_content\">\n\n\n    <form class=\"form-inline\">\n        <div class=\"form-group col-md-3\">\n            <input id=\"_r_name\" value=\"\" type=\"text\" class=\"form-control\" placeholder=\"Instruction Name\" style=\"width:100%;\" />\n        </div>\n\n        <div class=\"form-group col-md-2\"><select id=\"_display_type\" name=\"_display_type\" style=\"width:100%;\">\n            <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.displayTypes : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </select></div>\n\n        <div class=\"form-group col-md-2\">\n            <select id=\"_is_active\" name=\"_is_active\" style=\"width:100%;\">\n                <option value=\"\"></option>\n                <option value=\"2\">All</option>\n                <option value=\"1\" selected>Active</option>\n                <option value=\"0\">Inactive</option>\n\n            </select>\n        </div>\n\n        <div class=\"form-group col-md-2\"><a id=\"advanced_search_btn\" name=\"advanced_search_btn\" href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i>   Search</a></div>\n\n        <a id=\"addRequirement\" href=\"javascript:void(0)\" class=\"btn btn-warning\" ><i class=\"fa fa-plus\"></i>   Add Instruction</a>\n    </form>\n\n	<!--<table>\n		<tr>\n			<td>Requirement Name:</br> \n				\n			</td>\n			\n			<td style=\"padding-left: 20px;\">Display Type:</br>\n				\n			</td>\n\n			<td style=\"padding-left: 20px;\">Is Active:</br>\n				\n			</td>\n			\n			<td style=\"padding-left: 20px;\">\n				 <br/>\n				\n			</td>\n		</tr>\n	</table>-->\n</div>\n\n\n";
},"useData":true});
return templates;
});