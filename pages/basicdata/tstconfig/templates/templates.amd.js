define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['list_requirement'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return " "
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " ";
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n                <tr data-ri_id=\""
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "\">\n                    <td style=\"text-align:center;\" width=\"20%\">"
    + alias2(alias1((depth0 != null ? depth0.requirement_name : depth0), depth0))
    + "</td>\n                    <td style=\"text-align:center;\" width=\"15%\">"
    + alias2(alias1((depth0 != null ? depth0.display_type_txt : depth0), depth0))
    + "</td>\n                    <td valign=\"middle\" width=\"35%\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"30\">\n                        <div class=\"_desclist box\">"
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "</div>\n                    </td>\n                </tr>\n                ";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <div style=\"padding-top:10px;\">\n                            <div style=\"text-align:right;width:100px;display:inline-block\">\n                                "
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + " :\n                            </div>\n                            <div style=\"text-align:left;display:inline-block\">\n                                "
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\n                            </div>\n                            <div>\n\n";
},"5":function(depth0,helpers,partials,data) {
    return "\n                <tr>\n                    <td style=\"text-align: center; line-height: 180px; height: 180px; background: #E6F3C5; border: 1px solid silver;\">No Data.</td>\n                </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style>\n\n	div.box \n	{\n        height: 2.85em;\n        font-size:12px;\n		/*height: 60px;*/\n		/*padding: 15px 20px 10px 20px;*/\n	}\n	div.box.opened\n	{\n		height: auto;\n	}\n	div.box .toggle .closed,\n	div.box.opened .toggle .open\n	{\n		display: none;\n	}\n	div.box .toggle .opened,\n	div.box.opened .toggle .closed \n	{\n		display: inline;\n	}\n	\n	#search_result_list_content tbody ._desclist{visibility:hidden}\n    \n\n</style> \n<div class=\"search_result_list\" style=\"width:860px;height:400px;overflow:auto;\">\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n				<th width=\"20%\">Instruction Name</th>\n				<th width=\"15%\">Display Type</th>\n				<th width=\"35%\">Instruction Items</th>\n				<th width=\"20\">Description</th>\n			</tr>\n        </table>\n        <div class=\"search_result_body\">\n            <table class=\"search_result_list_content\" cellspacing=\"0\" style=\"cursor:pointer;\">\n                "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>\n    </div>\n</div>\n<div class=\"pagebox_right\" style=\"margin-top:10px;height:10px;display: block;\">\n    <ul id=\"pagination\" class=\"clearfix pagebox\">\n    </ul>\n</div>  ";
},"useData":true});
templates['list_requirement_dialog'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"requirement_all_tabs\" style=\"width:860px;\">\n    <ul>\n        <li><a href=\"#requirement_search_advanced_tab\">Advanced Search</a></li>\n    </ul>\n    <div id=\"requirement_search_advanced_tab\">	\n		<div class=\"form-group\" style=\"clear:both;  margin-bottom: 0px;\">\n			<div class=\"col-sm-5\">\n				<label for=\"packages_content\" class=\"col-sm-5 control-label\">Instruction Name:</label>\n	            <div class=\"col-sm-7\">\n	                 <input id=\"_r_name\" name=\"_r_name\" type=\"text\" class=\"form-control \" placeholder=\"\" value=\"\">\n	            </div>\n			</div>\n			<div class=\"col-sm-5\">\n				<label for=\"packages_content\" class=\"col-sm-4 control-label\">Display Type:</label>\n	            <div class=\"col-sm-8\">\n	                 <select id=\"_display_type\" name=\"_display_type\" style=\"width:100%;height:26px;\">\n						<option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.displayTypes : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n	            </div>\n			</div>\n			<div class=\"col-sm-2\">\n				<a id=\"advanced_search_btn\" name=\"advanced_search_btn\" href=\"javascript:void(0)\" class=\"btn btn-default\" style=\"font-size:12px;\" style=\"height:26px;\"><i class=\"fa fa-search\"></i>   Search</a>\n			</div>\n		</div>\n    </div>\n</div>\n<div id=\"requirement-list-view\" style=\"height:430px;\">\n\n</div>\n";
},"useData":true});
templates['list_view_tstConfig'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing;

  return "                    <tr data-tstid=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.tsc_id : depth0), depth0))
    + "\">\n                        <td style=\"text-align: center;\" width=\"130px\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title_name : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </td>\n                         <td style=\"text-align: center;\" width=\"130px\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.customer_id : depth0),{"name":"if","hash":{},"fn":this.program(7, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </td>\n                        <td style=\"text-align: center;\" width=\"200px\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.ship_to_name : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </td>\n                        <td style=\"text-align: center;\" width=\"130px\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.process_step_name : depth0),{"name":"if","hash":{},"fn":this.program(11, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </td>\n                        <td style=\"text-align: center;\" width=\"160px\">\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.config_type : depth0),"0","=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.config_type : depth0),"1","=",{"name":"xifCond","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.config_type : depth0),"2","=",{"name":"xifCond","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.config_type : depth0),"3","=",{"name":"xifCond","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </td>\n                        <td style=\"text-align: center;\" width=\"160px\">\n\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.config : depth0),{"name":"if","hash":{},"fn":this.program(19, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "                        </td>\n                        <td valign=\"middle\" width=\"460px\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"each","hash":{},"fn":this.program(21, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n\n                        </td>\n\n\n                        </td>\n                        <td valign=\"middle\" width=\"310px\" style=\"text-align: center;\">\n                            <div class=\"btn-group\" role=\"group\">\n                                <a href=\"javascript:void(0)\" name=\"modRequirement\" class=\"btn btn-default\" title=\"Edit\" style=\"font-size:12px;\"><i class=\"fa fa-pencil\"></i>  Edit</a>\n                                <a href=\"javascript:void(0)\" name=\"delRequirement\" class=\"btn btn-default\" title=\"Delete\" style=\"font-size:12px;\"><i class=\"fa fa-remove\"></i>  Delete</a>\n                            </div>\n\n                        </td>\n                    </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "\n";
},"5":function(depth0,helpers,partials,data) {
    return "                            *\n";
},"7":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "\n";
},"9":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\n";
},"11":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.process_step_name : depth0), depth0))
    + "\n";
},"13":function(depth0,helpers,partials,data) {
    return "                            Product\n";
},"15":function(depth0,helpers,partials,data) {
    return "                            Product Category\n";
},"17":function(depth0,helpers,partials,data) {
    return "                            Product Line\n";
},"19":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.config : depth0), depth0))
    + "\n";
},"21":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <div style=\"\">\n                                <div style=\"text-align:right;width:100px;display:inline-block\">\n                                    "
    + alias2(alias1((depth0 != null ? depth0.requirement_name : depth0), depth0))
    + " :\n                                </div>\n                                <div style=\"text-align:left;display:inline-block\">\n                                    "
    + alias2(alias1((depth0 != null ? depth0.display_type_val : depth0), depth0))
    + "\n                                </div>\n                                <div>\n\n                                    <!--\n                                    <div style=\"line-height:25px;\" class=\"row\">\n                                        <div class=\"col-sm-6\" style=\"text-align:right;padding-right:5px\">\n                                            <label for=\"packages_content2\" class=\"control-label\">"
    + alias2(alias1((depth0 != null ? depth0.requirement_name : depth0), depth0))
    + " :</label>\n                                        </div>\n                                        <div class=\"col-sm-6\" style=\"text-align:left;padding-left:5px;\">\n                                            "
    + alias2(alias1((depth0 != null ? depth0.display_type_val : depth0), depth0))
    + "\n                                        </div>\n                                    </div>\n                                -->\n";
},"23":function(depth0,helpers,partials,data) {
    return "                    <tr>\n                        <td colspan=\"100%\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n                    </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\">\n    <div>\n        <div class=\"search_result_panel_title\"><span class=\"panel-heading\"><i class=\"fa fa-table\"></i>&nbsp;Handling SOP</span></div>\n        <div class=\"search_result_body\">\n            <table class=\"table table-striped\" cellspacing=\"0\" id=\"table-handling-requirement\">\n                <thead>\n                    <tr>\n                        <th width=\"130px\">Title</th>\n                        <th width=\"130px\">Customer Id</th>\n                        <th width=\"200px\">Ship To</th>\n                        <th width=\"130px\">Step</th>\n                        <th width=\"160px\">Level</th>\n                        <th width=\"160px\">Level Name</th>\n                        <th width=\"460px\">\n                            Instruction\n                        </th>\n                        <th width=\"310px\" style=\"border-right:0px;\">\n                            Operation\n                        </th>\n                    </tr>\n                </thead>\n                <tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(23, data, 0),"data":data})) != null ? stack1 : "")
    + "                </tbody>\n            </table>\n        </div>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\" ></ul>\n    </div>\n</div>\n";
},"useData":true});
templates['mod_view_tstconfig'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.title_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.customer_key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "</option>\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "</option>\n";
},"7":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.process_step : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.process_step_name : depth0), depth0))
    + "</option>\n";
},"9":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div id=\"addTstCContainer\" style=\"width:760px; max-height:430px;overflow-x: hidden;overflow-y: auto;\" >\n    <div  class=\"tab-pane\"> \n        <form class=\"form-horizontal\" role=\"form\" id=\"mod-tstconfig-form\">\n\n        <!--error container-->\n        <div class=\"alert alert-danger\" role=\"alert\" id=\"error_warp\" style=\"display:none;padding:10px;\">\n            \n        </div>\n    \n          <div class=\"form-group\">\n              <label for=\"packages_content\" class=\"col-sm-2 control-label\" style=\" line-height: 32px; padding-top: 0; margin-top:0;\">Title :</label>\n                <div class=\"col-sm-4\">\n                     <select id=\"selectTitle\" style=\"width:100%;\" name=\"title\">\n                        <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n                </div>\n                \n                <label for=\"packages_content\" class=\"col-sm-2 control-label\" style=\" line-height: 32px; padding-top: 0; margin-top:0;\">Customer : </label>\n                \n                <div class=\"col-sm-4\">\n                     <select id=\"selectCustomer\" style=\"width:100%;\" name=\"customer\">\n                        <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n                </div>\n          </div>\n        \n        <div class=\"form-group\">\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\" style=\" line-height: 32px; padding-top: 0; margin-top:0;\">Ship To :</label>\n            <div class=\"col-sm-4\">\n                <select id=\"selectShipTo\" style=\"width:100%;\" name=\"shipTo\">\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.shipTo : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\" style=\" line-height: 32px; padding-top: 0; margin-top:0;\">Step :</label>\n            <div class=\"col-sm-4\">\n                 <select id=\"selectStep\" style=\"width:100%;\" name=\"step\">\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.step : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n        </div>\n        \n        <div class=\"form-group\">\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\" style=\" line-height: 32px; padding-top: 0; margin-top:0;\">Level :</label>\n            <div class=\"col-sm-4\">\n                 <select id=\"selectConfigType\" style=\"width:100%;\" name=\"configType\" >\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.configType : depth0),{"name":"each","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n            <label for=\"packages_content\" class=\"col-sm-2 control-label\" style=\" line-height: 32px; padding-top: 0; margin-top:0;\">Level Name :</label>\n\n            <div class=\"col-sm-4\" id=\"config_value\" style=\"/*display:none;*/\">\n                <input type=\"text\" placeholder=\"Level Name...\" class=\"form-control\" name=\"config\" id=\"config\" style=\"height:32px;\" value="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.config : stack1), depth0))
    + ">\n                <input type=\"hidden\" id=\"config_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.config_id : stack1), depth0))
    + "\"/>\n                <input type=\"hidden\" id=\"tsc_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.tsc_id : stack1), depth0))
    + "\"/>\n            </div>\n        </div>\n       \n        <!--\n            <div class=\"requirement-box\" style=\"margin-bottom:5px;padding-left:50px;\">\n                 <strong>Requirement Config</strong>\n            </div>\n        -->\n        <div class=\"form-group\">\n              <div class=\"col-sm-12\">  \n                    <div class=\"line\"></div>\n                </div>\n        </div>\n         <div id=\"addBtnCtn\" class=\"form-group requirement-box\" style=\"margin-bottom:5px;\">\n             <div id=\"addRcDiv\" class=\"col-sm-11\">  \n                 <a href=\"javascript:void(0)\" name=\"addRC_1\" class=\"btn btn-default fa fa-plus pull-right\" style=\"margin-left: 1px;\" title=\"ADD\" id=\"addRC\"></a>\n            </div>\n        </div>\n        <div id=\"requirement-box\" class=\"requirement-box\" style=\"height: 150px;max-height: 150px; overflow-x: hidden;overflow-y: auto;\">\n            \n        </div>\n       \n        </form>\n    </div>\n</div>\n<div class=\"clear\"></div>";
},"useData":true});
templates['product_catagorys'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div style=\"padding: 10px;box-shadow: 0 0 5px #ccc;margin-bottom: 8px;\">\n	<input type=\"text\" id=\"product_catagorys_search\" style=\"padding: 6px;border-radius: 4px;border: 1px solid silver;font-family: inherit;font-size: inherit;line-height: inherit;width: 200px;\" placeholder=\"Search\">\n</div>\n\n<div class=\"permission_basic_layout\">\n	\n    <div>\n        <div id=\"product_catagorys_tree\"></div>\n    </div>\n</div>";
},"useData":true});
templates['product_catagorys_search'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.catagorys : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<div style=\"width:140px;float:left;margin-left:10px; height:20px\">\n								<a href=\"javascript:void(0)\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" data-pid=\""
    + alias2(alias1((depth0 != null ? depth0.parentid : depth0), depth0))
    + "\" class=\"search-item\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\n							</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "						No Product Category\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n\n<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n	<tbody>\n		<tr>\n			<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n				<span style=\"font-weight:bold;font-size:12px;\">Product <br /> Catagorys: </span>\n			</td>\n			<td width=\"80%\" valign=\"middle\">\n				<div style=\"overflow: auto; max-height: 60px;\" id=\"product_productcatagorys_condition\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.catagorys : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "				</div>\n			</td>\n			<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n				\n			</td>\n		</tr>\n	</tbody>\n</table>";
},"useData":true});
templates['product_line'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.proLines : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing;

  return ((stack1 = (helpers.calculate || (depth0 && depth0.calculate) || alias1).call(depth0,"(",(data && data.index),")%3 == 0",{"name":"calculate","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			 \n"
    + ((stack1 = (helpers.calculate || (depth0 && depth0.calculate) || alias1).call(depth0,"(",(data && data.index),")%3 != 0",{"name":"calculate","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			 \n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "				 </tr>\n				 <tr>\n					 <td>\n					 	<button data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" class=\"btn btn-default pl\" type=\"button\" value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" title = \""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "s\" style=\"text-overflow: ellipsis; overflow:hidden;\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</button>\n					 </td>	\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "			 <td>\n			 	<button data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" class=\"btn btn-default pl\" type=\"button\" value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" title = \""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "s\" style=\"text-overflow: ellipsis; overflow:hidden;\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</button>\n			 </td>\n";
},"7":function(depth0,helpers,partials,data) {
    return "		<td colspan=\"100%\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">\n			No Data.\n		</td>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"padding:0 10px 10px 10px;width:780px;height:400px;\" id=\"pl-box\" >\n	<table style=\"width: 100%;\">\n		<tr>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.proLines : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "		</tr>\n	</table>\n</div>\n<!--\n<div class=\"row-fluid\">\n  <div class=\"col-sm-12 btn-list\">\n    <button type=\"button\" class=\"btn btn-success pull-right\" id=\"select-config-ok\">OK</button>\n    <button type=\"button\" class=\"btn btn-Danger pull-right\" id=\"select-config-cancel\">Cancel</button>\n  </div>\n</div>-->";
},"useData":true});
templates['product_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-pc_id=\""
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\" style=\"cursor:pointer;\">\n                <td valign=\"middle\" width=\"140px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.p_name : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"140px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.p_code : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"120px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.upc : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"60px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.length : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"60px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.heigth : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"60px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.width : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"60px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.weight : depth0), depth0))
    + "\n                </td>\n            </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "            <tr>\n                <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n            </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\">\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n                <th width=\"140px\">Product Name</th>\n                <th width=\"140px\">Main Code</th>\n                <th width=\"100px\">UPC</th>\n                <th width=\"60px\">Length</th>\n                <th width=\"60px\">Heigth</th>\n                <th width=\"60px\">Width</th>\n                <th width=\"60px\" style=\"border-right:0px;\">Weight\n                </th>\n            </tr>\n        </table>\n\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n</div>";
},"useData":true});
templates['product_list_dialog'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id=\"product_all_tabs\" style=\"width:820px;\">\n    <ul>\n        \n        <li><a href=\"#product_search_advanced_tab\">Advanced Search</a></li>\n        <li><a href=\"#product_search_account_tab\">Search</a></li>\n    </ul>\n    <!--\n    \n    -->\n    <div id=\"product_search_advanced_tab\">\n        <div id=\"product_search_advanced\">\n            <!-- 条件 -->\n            <div id=\"search-condition\" style=\"display:none;\">\n                <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n                    <tbody>\n                        <tr>\n                            <td width=\"10%\" height=\"30px;\" align=\"right\">\n                                <span style=\"font-weight:bold;font-size:10px;\">Condition:</span>\n                            </td>\n                            <td width=\"80%\" valign=\"middle\">\n                                <div id=\"condition\">\n                                </div>\n                            </td>\n                            <td width=\"10%\">\n                                    <button  type=\"button\" class=\"btn btn-default\" id=\"product-condition-search-btn\"><i class=\"fa fa-search\"></i>   Search</button>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n                <div class=\"line\"></div>\n            </div>\n            <!--TITLE BOX -->\n            <div id=\"search-product-title\">\n\n            </div>\n            <!--Product Line -->\n            <div id=\"search-product-line\">\n            </div>\n            <!--Product Category -->\n            <div id=\"search-product-productCategory\">\n            </div>\n        </div>\n    </div>\n    <div id=\"product_search_account_tab\">\n        <div id=\"product-list-search-view\"></div>\n    </div>\n</div>\n<div id=\"product-list-view\" style=\"width:100%; height:260px;\"></div>\n";
},"useData":true});
templates['product_list_search'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "								<div style=\"width:140px;float:left;margin-left:10px; height:20px\">\n									<a href=\"javascript:void(0)\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.title_id : depth0), depth0))
    + "\" class=\"search-item\">"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</a>\n								</div>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.proLines : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "								<div style=\"width:140px;float:left;margin-left:10px; height:20px\">\n									<a href=\"javascript:void(0)\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" class=\"search-item\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\n								</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<!-- 条件 -->\n<div id=\"search-condition\">\n	<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n		<tbody>\n			<tr>\n				<td width=\"10%\" height=\"30px;\" align=\"right\">\n					<span style=\"font-weight:bold;font-size:10px;\">Condition:</span>\n				</td>\n				<td width=\"80%\" valign=\"middle\">\n					<div id=\"condition\">\n					</div>\n				</td>\n				<td width=\"10%\">\n						<button  type=\"button\" class=\"btn btn-default\" id=\"product-condition-search-btn\"><i class=\"fa fa-search\"></i>   Search</button>\n				</td>\n			</tr>\n		</tbody>\n	</table>\n	<div class=\"line\"></div>\n</div>\n<div id=\"search-product-title\">\n	<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n		<tbody>\n			<tr>\n				<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n					<span style=\"font-weight:bold;font-size:12px;\">TITLE: </span>\n				</td>\n				<td width=\"80%\" valign=\"middle\">\n					<div style=\"overflow: auto; height: 40px;\" id=\"product_title_condition\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n				</td>\n				<td width=\"10%\" height=\"30px;\" align=\"center\" valign=\"middle\">\n					<span style=\"font-weight:bold;font-size:12px;\">Multi </span>\n				</td>\n			</tr>\n		</tbody>\n	</table>\n	<div class=\"line\"></div>\n</div>\n\n<div id=\"search-product-productline\">\n	<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n		<tbody>\n			<tr>\n				<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n					<span style=\"font-weight:bold;font-size:12px;\">Product <br /> Line: </span>\n				</td>\n				<td width=\"80%\" valign=\"middle\">\n					<div style=\"overflow: auto; max-height: 40px;\" id=\"product_productline_condition\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.proLines : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n				</td>\n				<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n					\n				</td>\n			</tr>\n		</tbody>\n	</table>\n	<div class=\"line\"></div>\n</div>\n\n<!-- 产品分类 -->\n\n<div id=\"search-product-productCategory\">\n	<!-- 选择的产品分类 -->\n\n</div>";
},"useData":true});
templates['product_list_search_key'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id=\"search_command\" aria-labelledby=\"ui-id-1\"\n			class=\"ui-tabs-panel ui-widget-content ui-corner-bottom\"\n			role=\"tabpanel\" aria-hidden=\"false\">\n	<div>\n		<div>\n			<div class=\"eso_search_parent\">\n				<div class=\"eso_search_icon\">\n					<a> <img style=\"cursor: pointer;\" id=\"eso_search\"\n						src=\"image/easy_search.gif\">\n					</a>\n				</div>\n			</div>\n\n			<div class=\"eso_search_input\">\n\n				<input id=\"search_key\" name=\"search_key\" type=\"text\"\n					class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\"\n					aria-autocomplete=\"list\" aria-haspopup=\"true\" style=\"height: 30px;\">\n			</div>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['product_list_search_line'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.proLines : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<div style=\"width:140px;float:left;margin-left:10px; height:20px\">\n								<a href=\"javascript:void(0)\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" class=\"search-item\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\n							</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n	<tbody>\n		<tr>\n			<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n				<span style=\"font-weight:bold;font-size:12px;\">Product <br /> Line: </span>\n			</td>\n			<td width=\"80%\" valign=\"middle\">\n				<div style=\"overflow: auto; max-height: 60px;\" id=\"product_productline_condition\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.proLines : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\n			</td>\n			<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n				\n			</td>\n		</tr>\n	</tbody>\n</table>\n<div class=\"line\"></div>";
},"useData":true});
templates['product_list_search_title'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<div style=\"width:140px;float:left;margin-left:10px; height:20px\">\n								<a href=\"javascript:void(0)\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.title_id : depth0), depth0))
    + "\" class=\"search-item\">"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</a>\n							</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<!--Title  -->\n<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n	<tbody>\n		<tr>\n			<td width=\"10%\" height=\"30px;\" align=\"right\" valign=\"middle\">\n				<span style=\"font-weight:bold;font-size:12px;\">TITLE: </span>\n			</td>\n			<td width=\"80%\" valign=\"middle\">\n				<div style=\"overflow: auto; height: 60px;\" id=\"product_title_condition\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\n			</td>\n			<td width=\"10%\" height=\"30px;\" align=\"center\" valign=\"middle\">\n				<span style=\"font-weight:bold;font-size:12px;\"></span>\n			</td>\n		</tr>\n	</tbody>\n</table>\n<div class=\"line\"></div>";
},"useData":true});
templates['requirement_config'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return " <!--TextField-->\n    <label for=\"packages_content\" class=\"col-sm-2 rcitem-label\" style=\"padding-top: 0;font-weight: normal;line-height: 32px;\">\n    "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + " :</label>\n    <div class=\"col-sm-7\">\n        <div class=\"col-sm-7\" style=\" padding-left: 0px; \">\n            <input  name=\"ri_val_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\" type=\"text\" class=\"form-control \"   style=\"height: 32px;\" />\n         </div>\n    </div>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return " <!--TextArea-->\n    <label for=\"packages_content\" class=\"col-sm-2 rcitem-label\" style=\"padding-top: 0;font-weight: normal;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + " :</label>\n    <div class=\"col-sm-7\">\n        \n	   <textarea name=\"ri_val_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\" class=\"form-control\" rows=\"3\" id=\"ri_val_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\"></textarea>\n        \n    </div>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return " <!--Radio-->\n    <label for=\"packages_content\" class=\"col-sm-2 rcitem-label\" style=\"padding-top: 0;font-weight: normal;line-height: 32px;\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + " :</label>\n    <div class=\"col-sm-7\" style=\"line-height: 32px;\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.children : stack1),{"name":"each","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return "            <div class=\"col-sm-3\" style=\" padding-left: 0px; \">\n"
    + ((stack1 = helpers['if'].call(depth0,(data && data.first),{"name":"if","hash":{},"fn":this.program(7, data, 0),"inverse":this.program(9, data, 0),"data":data})) != null ? stack1 : "")
    + "           </div>\n";
},"7":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        		 <strong style=\"font-weight: normal;\"><input type=\"radio\" name=\"ri_val_"
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "\" value="
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + " checked=\"checked\" id=\"ri_val_"
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "_"
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\"> "
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + "</strong>\n";
},"9":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        	   	 <strong style=\"font-weight: normal;\"><input type=\"radio\" name=\"ri_val_"
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "\" value="
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + " id=\"ri_val_"
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "_"
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\"> "
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + "</strong>\n";
},"11":function(depth0,helpers,partials,data) {
    var stack1;

  return " <!--CHECK_BOX-->\n    <label for=\"packages_content\" class=\"col-sm-2 rcitem-label\" style=\"padding-top: 0;font-weight: normal;line-height: 32px;\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + " :</label>\n    <div class=\"col-sm-7\" style=\"line-height: 32px;\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.children : stack1),{"name":"each","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n";
},"12":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <div class=\"col-sm-3\" style=\" padding-left: 0px; \">\n    		 <strong style=\"font-weight: normal;\"><input type=\"checkbox\" name=\"ri_val_"
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "\" value="
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + " id=\"ri_val_"
    + alias2(alias1((depth0 != null ? depth0.ri_id : depth0), depth0))
    + "_"
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\" /> "
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + "</strong>\n             </div>\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return " <!--SELECT-->\n    <label for=\"packages_content\" class=\"col-sm-2 rcitem-label\" style=\"padding-top: 0;font-weight: normal;line-height: 32px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + " :</label>\n    <div class=\"col-sm-7\">\n        <div class=\"col-sm-7\" style=\" padding-left: 0px; \">\n        	<select  name=\"ri_val_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\" style=\"width:100%;\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.children : stack1),{"name":"each","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        	</select>\n        </div>\n    </div>\n";
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(data && data.first),{"name":"if","hash":{},"fn":this.program(16, data, 0),"inverse":this.program(18, data, 0),"data":data})) != null ? stack1 : "");
},"16":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        		<option value=\""
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\" selected=\"selected\">"
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + "</option>\n";
},"18":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        	    <option value=\""
    + alias2(alias1((depth0 != null ? depth0.item_value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.item_name : depth0), depth0))
    + "</option>\n";
},"20":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return " <!--SELECT-->\n    <label for=\"packages_content\" class=\"col-sm-2 rcitem-label\" style=\"padding-top: 0;font-weight: normal;line-height: 32px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.requirement_name : stack1), depth0))
    + " :</label>\n    <div class=\"col-sm-7\">\n        <div class=\"col-sm-7\" style=\" padding-left: 0px; \">\n            <input  name=\"ri_val_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\" type=\"text\" class=\"form_datetime form-control inconHand bgiconurl_\" style=\"height: 32px;\" readonly />\n        </div>\n    </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return " <!--隐藏域RequirementItem ID-->\n <div class=\"form-group row\" id=\"ri_id_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + "\" >\n <input type=\"hidden\"  value="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ri_id : stack1), depth0))
    + " name=\"ri_id\" data_type=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1), depth0))
    + "\"/>\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),"1","=",{"name":"xifCond","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),"2","=",{"name":"xifCond","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),"3","=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),"4","=",{"name":"xifCond","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),"5","=",{"name":"xifCond","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "<!--Date-->\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.display_type : stack1),"6","=",{"name":"xifCond","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    \n<div class=\"col-sm-2\" style=\"line-height: 32px;\">\n    <a href=\"javascript:void(0)\" name=\"delRC\" class=\"btn btn-default fa fa-remove\" style=\"margin-left: 1px;\" title=\"DELETE\"></a>\n    <a href=\"javascript:void(0)\" name=\"addRC\" class=\"btn btn-default fa fa-plus\" style=\"margin-left: 1px;\" title=\"ADD\"></a>\n</div>\n</div> ";
},"useData":true});
templates['requirement_item_values'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<span class=\"input-group-addon\">\n	"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n</span> \n<input id=\"searchRequirementValue\" type=\"text\"\nclass=\"form-control immybox immybox_witharrow\"\nplaceholder=\"Requirement Value\" data-value=\"-1\" />";
},"useData":true});
templates['search_condition_tsc'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <option value=\""
    + alias3(((helper = (helper = helpers.title_id || (depth0 != null ? depth0.title_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <option value=\""
    + alias3(((helper = (helper = helpers.customer_key || (depth0 != null ? depth0.customer_key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"customer_key","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.customer_id || (depth0 != null ? depth0.customer_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"customer_id","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <option value=\""
    + alias3(((helper = (helper = helpers.ship_to_id || (depth0 != null ? depth0.ship_to_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ship_to_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.ship_to_name || (depth0 != null ? depth0.ship_to_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ship_to_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"7":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <option value=\""
    + alias3(((helper = (helper = helpers.process_step || (depth0 != null ? depth0.process_step : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"process_step","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.process_step_name || (depth0 != null ? depth0.process_step_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"process_step_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"9":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                <option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\n";
},"11":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <option value=\""
    + alias3(((helper = (helper = helpers.ri_id || (depth0 != null ? depth0.ri_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ri_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.requirement_name || (depth0 != null ? depth0.requirement_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"requirement_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n\n<style>\n#select2-chosen-2 {text-align:left;}\n#select2-chosen-3 {text-align:left;}\n</style>\n\n<div id=\"advanced_search_tab_content\">\n    <form class=\"form-inline\">\n\n        <div class=\"form-group col-md-2 cus-style\">\n            <select id=\"_title\" name=\"_title\" style=\"width:100%;\">\n                <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div><!-- form-group -->\n        \n        <div class=\"form-group col-md-2 cus-style\">\n            <select id=\"_customer\" name=\"_customer\" style=\"width:100%;\">\n                <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div><!-- form-group -->\n\n        <div class=\"form-group col-md-2 cus-style\">\n            <select id=\"_ship_to\" name=\"_ship_to\" style=\"width:100%;\">\n                <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.shiptos : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div>\n        \n        \n        <div class=\"form-group col-md-2 cus-style\">\n            <select id=\"selectStep\" name=\"selectStep\" style=\" width:100%;\">\n                <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.processSteps : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div>\n        \n        <div class=\"form-group col-md-2 cus-style\" style=\"clear:left;\">\n            <select id=\"selectConfigType\" name=\"_config_type\" style=\" width:100%;\">\n                <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.configTypes : depth0),{"name":"each","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div>\n\n        <div class=\"form-group col-md-2 cus-style\">\n            <input type=\"text\" placeholder=\"Level Name...\" class=\"form-control\" name=\"config\" id=\"config\" style=\"width:100%;display:inline;\" value="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.config : stack1), depth0))
    + ">\n            <input type=\"hidden\" id=\"config_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.config_id : stack1), depth0))
    + "\" />\n            <input type=\"hidden\" id=\"tsc_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.tsc_id : stack1), depth0))
    + "\" />\n        </div>\n\n        <div class=\"form-group col-md-2 cus-style\">\n            <select id=\"_requirement\" name=\"_requirement\" style=\" width:100%;\">\n                <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.requirements : depth0),{"name":"each","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div>\n\n        <div class=\"form-group col-md-2 cus-style\">\n            <select id=\"_requirement_value\" name=\"_requirement_value\" style=\"width:100%;\">\n                <option value=\"\"></option>\n            </select>\n        </div>\n        <div class=\"form-group col-md-1 \">\n            <button id=\"btnLoadFilter\" type=\"button\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i>Search</button>\n        </div>\n            <button type=\"button\" id=\"addConfig\" class=\"btn btn-warning\"><i class=\"glyphicon glyphicon-plus\"></i>&nbsp;Add SOP</button>\n</form>\n\n	<!--<table>\n		<tr>\n			<td><span class=\"search_title\">Customer:</span></td>\n\n			<td><span class=\"search_title\">Title:</span></td>\n\n			<td style=\"display:none;\">\n				<label for=\"accurately\" style=\"font-weight:normal;margin:0;cursor: pointer;\"><span class=\"search_title\">Accurately:</span></label>\n				<input type=\"checkbox\" name=\"accurately\" id=\"accurately\" style=\"position: absolute;margin-top: 2px;margin-left: 5px;cursor: pointer;\" checked/>\n			</td>\n			\n			<td><span class=\"search_title\">:</span>\n            </td>\n		</tr>\n		\n		<tr>\n			<td><span class=\"search_title\">Ship To:</span></td>\n			<td><span class=\"search_title\">Requirement:</span></td>\n			<td><span class=\"search_title\">Config:</span></td>\n			<td id=\"_r_v\" name=\"_r_v\"></td>\n			<td></td>\n		</tr>\n	</table>-->\n</div>";
},"useData":true});
return templates;
});