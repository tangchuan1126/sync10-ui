"use strict"
define([
	"../config",
    "jquery",
    "backbone"
],function(page_config, $, Backbone){

	var Title = Backbone.Model.extend({

	});
	var ProductCatagorysCollection = Backbone.Collection.extend({
        url: page_config.loadProductCatagorysDataUrl.url,
        parse: function (response) {
            return response;
        }
    });

	//添加Title Ship To Config 所需要的参数
	var TstcParamsCollection = Backbone.Collection.extend({
		url: page_config.addTstConfigParamsUrl.url,
		parse: function (response) {
			TstcParamsCollection.titles = response.titles,
			TstcParamsCollection.shipTo = response.shipto,
			TstcParamsCollection.customers = response.customers,
			TstcParamsCollection.processSteps = response.processsteps
        }
	});
	/**
		ProductLine 
	**/
	var ProductLineConllection = Backbone.Collection.extend({
		url:page_config.loadProductLineUrl.url,
        parse:function(response){
            return response;
        }
	});
	/**
		产品Model
	**/
	var Product = Backbone.Model.extend({
		idAttribute: "pc_id",
		defaults:{
			p_name:"",
			p_code:"",
			orignal_pc_id:"",
			weight:"",
			heigth:"",
			width:"",
			alive:"",
			upc:"",
			price_uom:"",
			unit_price:"",
			weight_uom:"",
			length:"",
			union_flag:"",
			volume:"",
			length_uom:"",
			sn_size:"",
			unit_name:"",
			catalog_id:""
		}
	});
	var ProductConllection=Backbone.Collection.extend({
        url: page_config.loadProductDataUrl.url,
        model: Product,
        parse: function (response) {
            ProductConllection.pageCtrl = response.pagectrl;
            return response.list;
        }
    }, {
        pageCtrl: {
            pageNo: 1,
            pageSize: 5
        }
    });
	var ConfigSearchModel=Backbone.Model.extend({
        defaults: {
            title_id: ""
        }
    });
    /**
    	RequirementItemModelDetails
    **/
	var RequirementItemModelDetails = Backbone.Model.extend({
		url:page_config.riDetailsUrl.url,
		idAttribute: "rid_id",
		defaults:{
			ri_id:"",
			item_name:"",
			item_value:"",
			sort:""	
		}
	});
	var RequirementItemModelDetailses=Backbone.Collection.extend({
		url:page_config.riDetailsUrl.url,
		model:RequirementItemModelDetails
	}); 
    var RequirementItemModel=Backbone.Model.extend({
    	url:page_config.rItemUrl.url,
		idAttribute: "ri_id",
		defaults:{
			requirement_name:"",
			display_type:"",
			description:"",
			children:[]
		}
    });
    var  RequirementItemCollection = Backbone.Collection.extend({
		model : RequirementItemModel,
		url : page_config.requirementUrl.url,
		comparator : function(item) {
		},
		parse : function(response) {
			if (response.pagectrl) {
				RequirementItemCollection.pageCtrl = response.pagectrl;
			}
			return response.list;
		},
		initialize : function() {
		}
	}, 
	{
		pageCtrl : {
			pageNo : 1,
			pageSize : 10
		}
	});
    var TitleShipConfigDetail = Backbone.Model.extend({
		defaults:{
			tsc_id:"",
			ri_id:"",
			requirement_value_int:"",
			requirement_value_str:""
		}
    });
    var TitleShipConfigDetailes=Backbone.Collection.extend({
		model:TitleShipConfigDetail
	}); 
    var TitleShipConfig = Backbone.Model.extend({
    	url:page_config.handleTitleShipToUrl.url,
		idAttribute: "tsc_id",
		defaults:{
			title_id:"",
			ship_to_id:"",
			config_type:"",
			config_id:"",
			items:[]
		}
    });

    var ShowRequirementItem=Backbone.Collection.extend({
    	defaults:{
    		tsc_id:"",
    		ri_id:"",
    		requirement_value_int:"",
    		requirement_value_str:"",
			requirement_name:"",
			display_type:"",
			display_type_val:""
		}
    });
    var ShowTitleShipConfig = Backbone.Model.extend({
    	url:page_config.handleTitleShipToUrl.url,
		idAttribute: "tsc_id",
		defaults:{
			title_id:"",
			ship_to_id:"",
			config_type:"",
			config_id:"",
			config:"",
			title_name:"",
			ship_to_name:"",
			children:[]
		}
    });
    var SearchTstConfig = Backbone.Model.extend({
    	defaults:{
    		customer_key:"",
    		title_id:"",
    		ship_to_id:"",
    		config_id:"",
    		item_value:"",
    		accurately:true,
    		config:"",
    		tsc_id:"",
    		config_type:"",
    		process_step:""
    	}
    });
    var SearchProduct =  Backbone.Model.extend({
    	defaults:{
    		title_id:"",
    		product_line_id:"",
    		product_catagorys_id:"",
    		keys:""
    	}
    });

    //搜索参数模型
    var SearchRequirement = Backbone.Model.extend({
        defaults: {
            searchRname: "",
            searchDisplaytype: "",
            searchIsactive:"1",
            pageNo:"",
            cmd:"advance"
        }
    });	
    var ShowTitleShipConfigCollection =Backbone.Collection.extend({
        url: page_config.loadListUrl.url,
        model: ShowTitleShipConfig,
        parse: function (response) {
            ShowTitleShipConfigCollection.pageCtrl = response.pagectrl;
            return response.list;
        }
    }, {
        pageCtrl: {
            pageNo: 1,
            pageSize: 10
        }
    });
	return {
    	TstcParamsCollection:TstcParamsCollection,
    	ProductLineConllection:ProductLineConllection,
    	ConfigSearchModel:ConfigSearchModel,
    	ProductConllection:ProductConllection,
    	RequirementItemModel:RequirementItemModel,
    	RequirementItemModelDetails:RequirementItemModelDetails,
    	RequirementItemModelDetailses:RequirementItemModelDetailses,
    	TitleShipConfigDetailes:TitleShipConfigDetailes,
    	TitleShipConfigDetail:TitleShipConfigDetail,
    	TitleShipConfig:TitleShipConfig,
    	ShowTitleShipConfigCollection:ShowTitleShipConfigCollection,
    	RequirementItemCollection:RequirementItemCollection,
    	SearchTstConfig:SearchTstConfig,
    	ShowTitleShipConfig:ShowTitleShipConfig,
    	ProductCatagorysCollection:ProductCatagorysCollection,
    	SearchProduct:SearchProduct,
    	SearchRequirement:SearchRequirement
	}
});