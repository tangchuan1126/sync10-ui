"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../templates/templates.amd",
    "../model/TitleShipToConfigModel",
    "../config",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "jstree"
],function($,Backbone,Handlebars,templates,models,Config){

    return Backbone.View.extend({
        template:templates.product_catagorys,
        initialize:function(options){
            this.setElement(options.el);
            this.catagoryDialog = null;
            this.pcCollection  = new  models.ProductCatagorysCollection();
        },
        setView:function(views){
            this.modView = views.modView;
        },
        render:function(title_id,customerId){
            //搜索参数
            var tmp = this;
            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});

            var tmpUrl = this.pcCollection.url;
            if(!customerId){
            	customerId = 0;
            }            
            
            tmp.pcCollection.url += '?title_id='+title_id + "&customer_id=" + customerId;


            tmp.pcCollection.fetch({
                success:function(){
                    $.unblockUI();
                    tmp.catagoryDialog = art.dialog({
                        title:'SELECT Product Catagorys'
                        ,lock: true
                        ,opacity:0.3
                        ,Parentwindowscrooll:false
                        ,fixed: true
                        ,init:function(){
                            var w1 = $(window).width(), H = $('html');
                            H.css('overflow', 'hidden');
                            var w2 = $(window).width();
                            H.css('margin-right', (w2 - w1) + 'px');
                            this.content(tmp.template());
                        },close:function(){
                            document.body.parentNode.style.overflow="scroll";
                            document.body.parentNode.style.marginRight="";
                        }
                    });

                    var pcList = tmp.pcCollection.toJSON();

                    if(pcList.length > 0){
                        
                         $("#product_catagorys_tree").jstree(
                            {"core":{
                            "multiple" : false,
                            "data":pcList
                            },
                            "plugins" : ["types","wholerow","search"]
                        });
                    } else {

                        $(".permission_basic_layout").html('<div style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data.</div>');
                    }

                    $('#product_catagorys_tree').on('changed.jstree', function (e, data) {
                        tmp.closeDialog();
                        tmp.modView.setCatagory(data.node);

                    }).jstree();
                    var to = false;
                    $('#product_catagorys_search').keyup(function () {
                        if(to) { 
                            clearTimeout(to); }
                            to = setTimeout(function () {
                            var v = $('#product_catagorys_search').val();
                            $('#product_catagorys_tree').jstree(true).search(v);
                        }, 250);
                    });
                    tmp.pcCollection.url=tmpUrl;
                }
            })
        },
        closeDialog:function(){
            if(this.catagoryDialog){
               this.catagoryDialog.close(); 
            }
        }
        
    });
});
