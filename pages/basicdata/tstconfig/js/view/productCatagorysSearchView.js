"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
		"immybox",
		"slidePanel",
		"../config",
    "../model/TitleShipToConfigModel",
		"require_css!oso.lib/immybox-master/immybox.css"], 
function($, Backbone, HandleBars, templates,immybox,SlidePanel,Config,models) {
	var pcSearch =  Backbone.View.extend({
		template: templates.product_catagorys_search,
    collection:new models.ProductCatagorysCollection(),
    initialize: function(options) {
      this.setElement(options.el);

    },
    setView:function(views){
          this.pcSelectView = views.pcSelectView;
    }, 
		render: function(_tid,customer_id,_plineid,_pid) {
      //Title id product line id parentid 
      this.title_id = _tid;
      if(!customer_id){
    	  customer_id = 0;
      }
      this.product_line_id = _plineid;
      var tmp = this;
      
      var _url = tmp.collection.url;
      tmp.collection.url = Config.getCatagorysByPid.url
      //TODO  获取产品分类
      tmp.collection.fetch({
          data:{
            title_id:_tid,
            product_line_id:_plineid,
            pid:_pid,
            customer_id:customer_id
          },
          success:function(collection){
            tmp.catagorys=collection.toJSON();
            tmp.collection.url = _url;

            console.log(tmp.$el.length);

             tmp.$el.html(tmp.template({
               catagorys:collection.toJSON()
            }));
             if(collection.length==0){
                $("#search-product-productCategory").hide();
             }else{
                $("#search-product-productCategory").show();
             }
             
          }
      });
		},
    events:{
      "click #product_productcatagorys_condition a":"selCatagorys"
    },
    selCatagorys:function(evt){
      //处理 上面的信息
      //TODO 添加到条件
      var _a= $(evt.target).clone();
      $(evt.target).click(function(){
    	 return false;
      });
      this.createDiv(_a,"Category");
      var customerId = $("#selectCustomer").val();
      this.render(this.title_id,customerId,"",$(_a).data("id"));
      
    },
    createDiv:function(link, type){
      //先查找
      if(!$("#condition div[type='"+type+"']").length){
        var categoryDiv = $("<div></div>");
        categoryDiv.attr("type",type);
        categoryDiv.addClass('condition');
        categoryDiv.append("<label>"+type+":<label>");
        link.html(link.html()+"&nbsp;×");
        link.appendTo(categoryDiv);

        $("#condition").append(categoryDiv);
      }else{
        link.html(">>"+link.html()+"&nbsp;×");
        $("#condition div[type='"+type+"']").append(link);
      }
    }
	});
  return pcSearch; 
});