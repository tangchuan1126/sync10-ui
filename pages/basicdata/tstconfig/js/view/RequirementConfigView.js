"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
    "bootstrap.datetimepicker",
    "handlebars_ext",
    "jqueryui/dialog",
    "select2", "showMessage"], 
function($, Backbone, HandleBars, templates) {
    var addLastBtn= function(){
        if($("a[name='addRC']")){
          $("a[name='addRC']").remove();
        }
        var delRCs = $("a[name='delRC']");
        if(delRCs.length>0){
            $(delRCs[delRCs.length - 1]).parent().append('<a href="javascript:void(0)" name="addRC" class="btn btn-default fa fa-plus" style="margin-left: 1px;" title="ADD" id="addRcBtn"></a>');
        }else{
          // var addBtnCton = $('<div id="addBtnCtn" class="form-group requirement-box" style="margin-bottom:5px;"><div id="addRcDiv" class="col-sm-5"> <a href="javascript:void(0)"  name="addRC" class="buttons icon add pull-right" style="margin-left: 1px;" title="ADD"></a></div></div>');
          
          // $(addBtnCton).insertBefore($("#requirement-box"));
          $("#addBtnCtn").css("display","block");
        }
      }
  var keys = { riIdKey:'requirement_name'};
	return Backbone.View.extend({
		template: templates.requirement_config,
      initialize: function(options) {
          this.setElement(options.el);
      },
      setView:function(views){
            this.lrDialogView = views.lrDialogView;
      },
  		render: function(model,val) {
  			var v = this;
  			
  			var ri_id = model.attributes.ri_id;
  			if($("#ri_id_"+ri_id).length!=0){
  				showMessage("The repeat ["+model.attributes.requirement_name+"] already choosed", "alert");
          v.undelegateEvents();
          return;
  			}
  			
  			if($("a[name='addRC']")){
  				$("a[name='addRC']").remove();
  			}
  			

        //处理同类型的放在一起，如果没有放在最后没，
        //如果有同类型的，责插在最后一个后面

        var _type = model.toJSON().display_type;

        var sameItems = v.$el.find("input[data_type="+_type+"]");
        if(sameItems){
          if(sameItems.length!=0){
            var divId = "#ri_id_"+$(sameItems[sameItems.length-1]).val()

            $(divId).after(v.template({
                model:model.toJSON(),keys:keys
            }));
          }else{
             v.$el.append(v.template({
                model:model.toJSON(),keys:keys
            }));
          }
        }
        addLastBtn();
        $("#addBtnCtn").css("display","none");
        
         if(_type==5){
            $("select[name='ri_val_"+model.toJSON().ri_id+"']").select2({
                placeholder: "ConfigType",
                allowClear: true
              });
         }
        
        if(val){
          switch(_type){
            case 1:
                  $("input[name='ri_val_"+model.toJSON().ri_id+"']").val(val);
                  break;
            case 2:
                  $("textarea[name='ri_val_"+model.toJSON().ri_id+"']").val(val);
                  break;
            case 3:
                  $("input[name='ri_val_"+model.toJSON().ri_id+"'][value='"+val+"']").attr("checked","checked");
                  break;
            case 4:
                  var _vals = val.split(",");
                  for(var i = 0;i<_vals.length;i++){
                    $("input[name='ri_val_"+model.toJSON().ri_id+"'][value='"+_vals[i]+"']").attr("checked","checked");
                  }
                  break;
            case 5:
                  //$("select[name='ri_val_"+model.toJSON().ri_id+"'] option[value='"+val+"']").attr("selected","true");
                  $("select[name='ri_val_"+model.toJSON().ri_id+"']").select2("val",val);
                  break;
            case 6:
                  $("input[name='ri_val_"+model.toJSON().ri_id+"']").val(val);
                  break;
          }
         
          //$("input[name='ri_val_"+model.toJSON().ri_id+"'][value='2']").attr("checked","checked"); 
        }
         if(_type==6){
            $("input[name='ri_val_"+model.toJSON().ri_id+"']").datetimepicker({
              format: 'yyyy-mm-dd hh:mm:ss',
              weekStart: 1,
              todayBtn: 1,
              autoclose: 1,
              todayHighlight: 1,
              startView: 2,
              minView: 1,
              forceParse: 0,
            });
          }
         v.resetLableWidth();
  		},
      events:{
        "click a[name='delRC']":"delRC",
        "click a[name='addRC']":"addRC"
      },
      delRC:function(evt){
        var parent = $(evt.target).parent().parent();
       
        var ri_id =$(parent).find("input[name='ri_id']").val();
        if(ri_id !=""){
           this.lrDialogView.modView.delItmesId.push(ri_id);
        }
         
        parent.remove();
        addLastBtn();
      },
      addRC:function(evt){
        this.lrDialogView.render();
        //updateDialog("requirement-list-view", "create label template");
      },
      resetLableWidth:function(){
        $("#requirement-box .rcitem-label").each(function(){
          //console.log("width:"+$(this).width()) ;
          //console.log("width:"+$(this).height()) ;
        })
      }
	});
});