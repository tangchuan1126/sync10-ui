"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../templates/templates.amd",
    "../model/TitleShipToConfigModel",
    "Paging",
    "./productListSearchView",
    "../config",
    "handlebars_ext",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,models,Paging,PlistSearchView,Config){

    return Backbone.View.extend({
        template:templates.product_list,
        initialize:function(options){
            this.setElement(options.el);
            this.collection = new models.ProductConllection;
            // this.dialog = null;
        },
        setView:function(views){
            this.plistDialog = views.plistDialog;
        },
        render:function(searchParam){
            //搜索参数
            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            if(!searchParam){
               searchParam = new models.SearchProduct();   
            }

            var tmp = this;

            this.collection.reset();
            var title_id = searchParam.toJSON().title_id;
            var customer_id = searchParam.toJSON().customer_id;
            if(!title_id){
            	title_id = "0";
            }
            if(!customer_id){
            	customer_id = "0";
            }
            
            this.collection.fetch({
                data:{
                    pageNo:models.ProductConllection.pageCtrl.pageNo,
                    pageSize:models.ProductConllection.pageCtrl.pageSize,
                    title_id:title_id,
                    customer_id:customer_id,
                    keys:searchParam.toJSON().keys,
                    product_line_id:searchParam.toJSON().product_line_id,
                    product_catagorys_id:searchParam.toJSON().product_catagorys_id
                },
                success:function(collection){
                    $.unblockUI();
                    tmp.$el.html(tmp.template({
                        list:collection.toJSON(),
                        pageCtrl:models.ProductConllection.pageCtrl
                    }));
                    Paging.update_page(
                        models.ProductConllection.pageCtrl.pageCount
                        , models.ProductConllection.pageCtrl.pageNo
                        , "pagination"
                        , function(pageNo) {

                        models.ProductConllection.pageCtrl.pageNo = pageNo;
                        tmp.render(searchParam);
                    });
                    $.unblockUI();
                    // tmp.events();
                }   
            });
        },

        events:{
            //"click td":"setConfig"
            // var tmp = this;
            // $(".search_result_list_content tr").bind("click",function(evt){
            //     tmp.setConfig(evt);
            // });
            // $(".search_result_list_content tr").bind("dblclick",function(evt){
            //     // tmp.setConfig(evt);
            //     console.log("dblclick");
            //     tmp.setConfig(evt);
            // });

            "click .search_result_list_content tr":"setConfig"
        },
        setConfig:function(evt){
           var pc_id = $(evt.target).parent().data("pc_id");
            console.log(pc_id);
            if(!pc_id) return;
            var collection = this.collection;
            var model = this.collection.findWhere({pc_id:pc_id});
            var v = this;
            // $("#config").val(model.attributes.p_name);
            // $("#config_id").val(model.attributes.pc_id);
            v.plistDialog.setConfig(model);
        }
        
    });
});
