"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
		"immybox",
		"../config",
    "../model/TitleShipToConfigModel"], 
function($, Backbone, HandleBars, templates,immybox,Config,models) {
	var plSearch =  Backbone.View.extend({
		template: templates.product_list_search_line,
    collection:new models.ProductLineConllection(),
    initialize: function(options) {
      this.setElement(options.el);
      this.title_id =0;
    },
    setView:function(views){
          this.pcSearchView = views.pcSearchView;
    },
		render: function(_title_id,customer_id) {
      var tmp = this;
      this.title_id = _title_id;
      tmp.collection.fetch({
          data:{
              title_id:_title_id,
              customer_id : customer_id
          },
          success:function(collection){
            tmp.$el.html(tmp.template({
              proLines:collection.toJSON()
            }));
            $("#search-product-line").show();
            if(collection.length==0){
                $("#search-product-line").hide();
            }
          }
      }) ;
		},
    events:{
       "click #product_productline_condition a":"selProductLine"
    },
    selProductLine:function(evt){
      var _a= $(evt.target).clone();
      this.createDiv(_a,"Product Line");
      $("#search-product-line").hide();
      var customerId = $("#selectCustomer").val();
      this.pcSearchView.render(this.title_id ,customerId,$(_a).data("id"),"");
    },
    search:function(evt){
      console.log("search"+evt);
    },
    createDiv:function(link, type){
      //先查找
      if(!$("#condition div[type='"+type+"']").length){
        var categoryDiv = $("<div></div>");
        categoryDiv.attr("type",type);
        categoryDiv.addClass('condition');
        categoryDiv.append("<label>"+type+":<label>");
        link.html(link.html()+"&nbsp;×");
        link.appendTo(categoryDiv);

        $("#condition").append(categoryDiv);
      }else{
        $("#condition div[type='"+type+"']").append(">>").append(link);
      }
      
    },
    closeType:function(evt,tmp){
      console.log(evt);

      var type =$(evt.target).parent().attr("type");
      
      if("Title"==type){
        $(evt.target).parent().remove();
        $("#search-product-title").show();
      }else if("Product Line"==type){
        $(evt.target).parent().remove();
        $("#search-product-line").show();
      }else if("Category"==type){
        //TODO 判断个数，如果个数是1 则删除parent否是不删除
        //删除选择后的所有元素
        console.log($(evt.target).index());
        var index = $(evt.target).index();
        var length = $(evt.target).parent().find("a").length;
        if(index==1){
          $(evt.target).parent().remove();
        }else{
          for(var i=length-1;i>0;i--){
            $(evt.target).parent().find("a")[i].remove();
          }
        }
        // if($(evt.target).parent().find("a").length>1){
        //   $(evt.target).remove();
        // }else{
        //   $(evt.target).parent().remove();
        // }
        $("#search-product-productCategory").show();
        var customerId = $("#selectCustomer").val();
        tmp.pcSearchView.render($(evt.target).data("pid"),customerId);
      }
    }
	});
  return plSearch;
});