"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
		"immybox",
    "./productCatagorysSearchView",
		"../config",
    "../model/TitleShipToConfigModel",
		"require_css!oso.lib/immybox-master/immybox.css"], 
function($, Backbone, HandleBars, templates,immybox,pcSearchView,Config,models) {
	var plSearch =  Backbone.View.extend({
		template: templates.product_list_search,
    collection:new models.ProductLineConllection(),
    initialize: function(options) {
      this.setElement(options.el);
      this.pcSearchView =null;
    },
    setView:function(views){
          this.plistView = views.plistView;
          this.tscSearchCondition = views.tscSearchCondition;
          // this.pcSearchView = views.pcSearchView;
    },
		render: function() {
      var tmp = this;
      //TODO  获取产品线
      tmp.collection.fetch({
          success:function(collection){
            // tmp.proLines=collection.toJSON()
            tmp.$el.html(tmp.template({
              titles:tmp.tscSearchCondition.titles,
              proLines:collection.toJSON()
            }));
            tmp.pcSearchView = new pcSearchView();
            tmp.pcSearchView.setView({
              pcSelectView:tmp
            });
            var customerId = $("#selectCustomer").val();
            tmp.pcSearchView.render(0,customerId);
            // 初始化方法
             $('#condition').delegate('a','click',function(evt){
              tmp.closeType(evt,tmp)
             }); 
          }
      }) ;
      
      //tmp.initEvent();
		},
    events:{
       "click #product_title_condition a":"selTitle",
       "click #product_productline_condition a":"selProductLine",
       "click #product-condition-search-btn":"search",

    },
    selTitle:function(evt){
      var _a= $(evt.target).clone();
      this.createDiv(_a,"Title");
      $("#search-product-title").hide();
    },
    selProductLine:function(evt){
      var _a= $(evt.target).clone();
      this.createDiv(_a,"Product Line");
      $("#search-product-productline").hide();
    },
    search:function(evt){
      console.log("search"+evt);
      this.plistView.render();
    },
    createDiv:function(link, type){
      //先查找
      if(!$("#condition div[type='"+type+"']").length){
        var categoryDiv = $("<div></div>");
        categoryDiv.attr("type",type);
        categoryDiv.addClass('condition');
        categoryDiv.append("<label>"+type+":<label>");
        link.html(link.html()+"&nbsp;×");
        link.appendTo(categoryDiv);

        $("#condition").append(categoryDiv);
      }else{
        $("#condition div[type='"+type+"']").append(">>").append(link);
      }
      
    },
    closeType:function(evt,tmp){
      console.log(evt);

      var type =$(evt.target).parent().attr("type");
      
      if("Title"==type){
        $(evt.target).parent().remove();
        $("#search-product-title").show();
      }else if("Product Line"==type){
        $(evt.target).parent().remove();
        $("#search-product-productline").show();
      }else if("Category"==type){
        //TODO 判断个数，如果个数是1 则删除parent否是不删除
        //删除选择后的所有元素
        console.log($(evt.target).index());
        var index = $(evt.target).index();
        var length = $(evt.target).parent().find("a").length;
        if(index==1){
          $(evt.target).parent().remove();
        }else{
          for(var i=length-1;i>0;i--){
            $(evt.target).parent().find("a")[i].remove();
          }
        }
        $("#search-product-productCategory").show();
        var customerId = $("#selectCustomer").val();
        tmp.pcSearchView.render($(evt.target).data("pid"),customerId);
      }
    }
	});
  return plSearch;
});