"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../templates/templates.amd",
    "../model/TitleShipToConfigModel"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.requirement_item_values,
        initialize:function(options){
            this.setElement(options.el);
        },
        setView:function(views){
           
        },
        collection:new models.TstcParamsCollection(),
        render:function(ri_id){
            var tmp = this;
            
            var model = new models.RequirementItemModel({
                ri_id:ri_id
            });
            model.url+="/"+ri_id
            model.fetch({async:false,
                success: function(model, response){
                 
                  if(model && model.toJSON().display_type>2){
                    
                    var inputhtml=tmp.template({
                        name:model.toJSON().requirement_name
                    });

                    $("#requirement_item_values").html(inputhtml);

                    var choices =  model.toJSON().children;
                    console.log(choices);
                    $('#searchRequirement').immybox({
                          Pleaseselect:'<i>Select</i>',
                          textname:'item_name',
                          valuename:'item_value',
                          choices: model.toJSON().children
                    });

                  }else{
                    $("#requirement_item_values").html("");
                  }
                  
                }
            });
        }
    });
});
