/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "js/model/TitleShipToConfigModel"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.del_requirement,
        initialize:function(options){
        },
        setView:function(views){
        	
            this.requirementSearchResultView = views.requirementSearchResultView;
        },
        render:function(model){
            //TODO 删除
            // var tmp = this;

            // art.dialog({
            //     title:'Del Requirement » '+model.toJSON().requirement_name
            //     ,lock: true
            //     ,opacity:0.3
            //     ,init:function(){

            //         var w1 = $(window).width(), H = $('html');
            //         H.css('overflow', 'hidden');
            //         var w2 = $(window).width();
            //         H.css('margin-right', (w2 - w1) + 'px');

            //         this.content(tmp.template({
            //             model:model.toJSON()
            //         }));
            //     }
            //     ,close:function(){

            //         document.body.parentNode.style.overflow="scroll";
            //         document.body.parentNode.style.marginRight="";
            //     }
            //     ,icon:'warning'
            //     ,ok:function(){

            //         var requirementModel = new models.RequirementModel({
            //         	ri_id:model.toJSON().ri_id
            //         });

            //         requirementModel.url += "/"+model.toJSON().ri_id;

            //         requirementModel.destroy({
            //             success: function(model, response){

            //                 if(response.success == 'true'){

            //                     tmp.requirementSearchResultView.render();

            //                 }else{

            //                     art.dialog({
            //                         title:'Message'
            //                         ,lock:true
            //                         ,opacity:0.3
            //                         ,content:response.error
            //                         ,icon:'warning'
            //                         ,ok:true
            //                         ,okVal:'Sure'
            //                     });
            //                 }
            //             }
            //         });
            //     }
            //     ,okVal:'Sure'
            //     ,cancel:true
            //     ,cancelVal:'Cancel'
            // });
        }
    });
});
