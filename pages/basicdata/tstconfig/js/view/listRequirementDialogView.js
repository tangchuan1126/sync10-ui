"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
    "../model/TitleShipToConfigModel",
		"../config",
    "./listRequirementView",
    "artDialog"], 
function($, Backbone, HandleBars, templates,models,Config,lrView) {
  var lrDialog = null;
  var displayData =[
  {"value":1,"text":"TextField"},
  {"value":2,"text":"TextArea"},
  {"value":3,"text":"Radio"},
  {"value":4,"text":"CheckBox"},
  {"value":5,"text":"DropList"},
  {"value":6,"text":"Date"}
];
  return Backbone.View.extend({
		template: templates.list_requirement_dialog,
      initialize: function() {
        var tmp = this;
      },
  		render: function() {
        var tmp = this;
        if(lrDialog!=null){
          tmp.closeDialog();
        }
        lrDialog = art.dialog({
            title:'Instruction Items'
            ,lock: true
            ,opacity:0.3
            ,padding:'10px 5px 10px 5px'
            ,top:10
            ,init:function(){
                var w1 = $(window).width(), H = $('html');
                H.css('overflow', 'hidden');
                var w2 = $(window).width();
                H.css('margin-right', (w2 - w1) + 'px');
                this.content(tmp.template({
                  displayTypes:displayData
                }));
            }
        }); 
         $("#_display_type").select2({
            placeholder: "Select...",
            allowClear: true
          });

          $("#requirement_all_tabs").tabs();
          var setActiveTab = function(i){
              $("#requirement_all_tabs").tabs("option","active",i);
          };
          setActiveTab(0);
        //初始化 Requirement list
        tmp.lrView = new lrView({el:"#requirement-list-view"});
        tmp.lrView.render();
        tmp.lrView.setView({
          modView:tmp.modView,
          lrDialogView:tmp
        });

        $('#requirement_all_tabs').delegate('#advanced_search_btn','click',function(evt){
          tmp.search(evt,tmp)
        });

        $.unblockUI();

  		},
      setView:function(views){
          this.modView = views.modView;
      },
      search:function(evt,tmp){
        console.log("search");

        tmp.searchModel = new models.SearchRequirement({
            searchRname: $("#_r_name").val(),
            searchDisplaytype: $("#_display_type").select2("val")
        });

        tmp.lrView.render(tmp.searchModel);
      },
      closeDialog:function(){
        if(lrDialog && !lrDialog.closed){
          lrDialog.close();
          lrDialog = null;
        }
      }
	});
});