"use strict";

define(['jquery',
        'backbone',
        'handlebars',
        '../../templates/templates.amd',
        "../model/TitleShipToConfigModel",
        "slidePanel",
        "artDialog",
        "js/plugin/handlebars_ext.js",
        ], 
function($,
         Backbone,
         Handlebars,
         templates,
         models,SlidePanel){
    return Backbone.View.extend({
        template:templates.product_line,
        collection:new models.ProductLineConllection(),
        initialize:function(options){
            this.setElement(options.el);
            this.dialog = null;
        },
        setView:function(views){
            this.modView = views.modView;
        },
        
        render:function(searchParam){
            var tmp = this;
            if(!searchParam){
               searchParam= new models.ConfigSearchModel();
                $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            }
            var title_id = searchParam.toJSON().title_id;
            var customer_id = searchParam.toJSON().customer_id;
            if(!customer_id){
            	customer_id = 0;
            }            
            this.collection.fetch({
                data:{
                    title_id:title_id,
                    customer_id:customer_id
                },
                async:false,
                success:function(collection){
                    // tmp.$el.html(tmp.template({
                    //     proLines:collection.toJSON()
                    // }));
                    var html = tmp.template({
                        proLines:collection.toJSON()
                    });
                    tmp.dialog = art.dialog({
                        title:'SELECT Product Line'
                        ,lock: true
                        ,opacity:0.3
                        ,width:780
                        ,init:function(){
                            var w1 = $(window).width(), 
                            H = $('html');
                            H.css('overflow', 'hidden');
                            var w2 = $(window).width();
                            H.css('margin-right', (w2 - w1) + 'px');
                            this.content(html);
                        },close:function(){
                            document.body.parentNode.style.overflow="scroll";
                            document.body.parentNode.style.marginRight="";
                        }
                    });
                    tmp.init();
                }
            }) ;
                    
        },
        init:function(){
            var v = this;
            $("#pl-box button").bind("click",function(evt){
                v.itemSelected(evt);
            });

            // //OK 
            // $("#select-config-ok").bind("click",function(evt){
            //     v.configOk();
            // });
            // //OK 
            // $("#select-config-cancel").bind("click",function(evt){
            //     v.configCancel();
            // });
        },
        itemSelected:function(evt){
            var v = this;
            var _id = $(evt.target).data("id");
            var _val = $(evt.target).data("id");
            if(undefined!=_id){
                var collection = this.collection;
                var model = this.collection.findWhere({id:_id});
                $(evt.target).removeClass("btn-default");
                $(evt.target).addClass("btn-success btn-bordered");
                //修改兄弟节点样式
                $(evt.target).siblings().removeClass("btn-success btn-bordered");
                $(evt.target).siblings().addClass("btn-default");
                v.setConfig(model);
                v.closeSelPl();
            }

             
        },
        closeSelPl :function(){
            // $("#product-line-list-view").dialog("close");
            // $(".ui-widget-overlay").remove();
            if(this.dialog){
                this.dialog.close();
            }
        },
        setConfig:function(model){
            $("#config").val(model.attributes.name);
            $("#config_id").val(model.attributes.id);
        },
        onSelItem:function(_id){
            var selItem = $("#product_line-view button[data-id ='"+_id+"']");

            if(null!=selItem && undefined!=selItem){
                //TODO 修改对应样式
                $(selItem).removeClass("btn-default");
                $(selItem).addClass("btn-success btn-bordered");
            }
            this.closeSelPl();
        },
        configOk:function(){
            this.closeSelPl();
        },
        configCancel:function(){
            this.closeSelPl();
            $("#config").val("");
            $("#config_id").val("");
            $("#selectConfigType").val("");
        }
    });
});