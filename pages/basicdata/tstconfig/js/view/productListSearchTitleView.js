"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
		"immybox",
    "./productCatagorysSearchView",
		"../config",
    "../model/TitleShipToConfigModel"], 
function($, Backbone, HandleBars, templates,immybox,pcSearchView,Config,models) {
	//标示在添加页面是否 已选择Title
	var selectTitle = false;
	var plstSearch =  Backbone.View.extend({
		template: templates.product_list_search_title,
    collection:new models.ProductLineConllection(),
    initialize: function(options) {
      this.setElement(options.el);
      this.pcSearchView =null;
    },
    setView:function(views){
      this.tscSearchCondition = views.tscSearchCondition;
      this.plslView = views.plslView;
    },
		render: function() {
      var tmp = this;
      tmp.$el.html(tmp.template({
        titles:tmp.tscSearchCondition.titles
      }));
		},
    events:{
       "click #product_title_condition a":"selTitle"
    },
    selTitle:function(evt){
      selectTitle = ( $.trim($("#selectTitle").val()).length > 0 ) ? true : false;
      if(!selectTitle){
          var _a= $(evt.target).clone();
          this.createDiv(_a,"Title");
          $("#search-product-title").hide();
          var customerId = $("#selectCustomer").val();
          if($.trim(customerId).length == 0){
        	  customerId = 0;
          }          
          this.plslView.render($(_a).data("id"),customerId);
          $("#search-condition").show();    	  
      }
    },
    initTitleId:function(_tid){
      selectTitle = true;
      var _a = $("#product_title_condition a[data-id='"+_tid+"']").clone();
      this.createDiv(_a,"Title");
      $("#search-product-title").hide();
      var customerId = $("#selectCustomer").val();
      if($.trim(customerId).length == 0){
    	  customerId = 0;
      }
      
      this.plslView.render(_tid,customerId);
      $("#search-condition").show();
    },
    initCustomerId:function(customerId){
        $("#product_title_condition").append("<input type=\"hidden\" id=\"customerId\" value=\"" + customerId + "\" />");
    },    
    selProductLine:function(evt){
      var _a= $(evt.target).clone();
      this.createDiv(_a,"Product Line");
      $("#search-product-productline").hide();
    },
    search:function(evt){
      console.log("search"+evt);
    },
    createDiv:function(link, type){
      //先查找
      if(!$("#condition div[type='"+type+"']").length){
        var categoryDiv = $("<div></div>");
        categoryDiv.attr("type",type);
        if(selectTitle){
        	categoryDiv.attr("selectTitle","true");
        }else{
        	categoryDiv.attr("selectTitle","false");
        }
        categoryDiv.addClass('condition');
        categoryDiv.append("<label>"+type+":<label>");
        link.html(link.html()+"&nbsp;×");
        link.appendTo(categoryDiv);
        $("#condition").append(categoryDiv);
      }else{
        $("#condition div[type='"+type+"']").append(">>").append(link);
      }
      
    }
	});
  return plstSearch;
});