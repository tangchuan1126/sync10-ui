"use strict";
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "handlebars_ext",
    "../../templates/templates.amd",
    "../model/TitleShipToConfigModel",
    "./RequirementConfigView",
    "../config",
    "../step",
    "select2",
    "wrapToSelect",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,HandlebarsExt,templates,models,rcViews,Config,Step){
    var initialTstcModel = new models.ShowTitleShipConfig;
    var modTstc =  Backbone.View.extend({
        
        template:templates.mod_view_tstconfig,
        initialize:function(options){
            this.setElement(options.el);
            this.delItmesId = new Array();
            this.mTstcdialog = null;
            
        },
        setView:function(views){
            this.plineView = views.plineView;
            this.plistView = views.plistView;
            this.riListView = views.riListView;
            this.listTstc = views.listTstc;
            this.pcSelectView=views.pcSelectView;
            this.plDialog = views.plDialog;
            this.lrDialogView = views.lrDialogView;
        },
        collection:new models.TstcParamsCollection(),
        render:function(model,_title){
            var tmp = this;
            //当用“X”号 关闭弹出框时，好像有点问题，所以在此
            //把弹出框 设置为null
            var flag = false;
            if(typeof(model) != 'undefined' && model != "" && model!=null){
              tmp.model = model;
              flag = true;          
            }else{
              tmp.model = initialTstcModel;
            }  
             $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            var configType = [
                {"value":1,"text":"Product"},
                {"value":2,"text":"Product Category"},
                {"value":3,"text":"Product Line"}
            ];
            
            tmp.collection.fetch({
                async:false,
                success:function(collection){

                    $.unblockUI();

                    tmp.mTstcdialog = art.dialog({

                        title:_title
                        ,lock: true
                        ,opacity:0.3
                        ,width:'810px'
                        ,height:'300px'
                        ,Parentwindowscrooll:false
                        ,fixed: true
                        ,init:function(){
                            var w1 = $(window).width(), H = $('html');
                            H.css('overflow', 'hidden');
                            var w2 = $(window).width();
                            H.css('margin-right', (w2 - w1) + 'px');
                            this.content(tmp.template({
                                titles:models.TstcParamsCollection.titles,
                                shipTo:models.TstcParamsCollection.shipTo,
                                customers:models.TstcParamsCollection.customers,
                                configType:configType,
                                step:models.TstcParamsCollection.processSteps,
                                model:tmp.model.toJSON()
                            }));
                        }
                        ,close:function(){
                            document.body.parentNode.style.overflow="scroll";
                            document.body.parentNode.style.marginRight="";
                        },
                        button: [{
                            name: 'Submit',
                            callback: function () {

                                var dialog = this;

                                var addTstc = $("#mod-tstconfig-form");
                                if (tmp.verification()) {
                                    $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
                                    //禁用按钮
                                    dialog.button({
                                        name: 'Submit',
                                        focus: true,
                                        disabled: true,
                                    });

                                    var model = new models.TitleShipConfig({
                                        customer_key:$("#selectCustomer").select2("val")==""?0:$("#selectCustomer").select2("val"),
                                        title_id:$("#selectTitle").select2("val")==""?0:$("#selectTitle").select2("val"),
                                        ship_to_id:$("#selectShipTo").select2("val")==""?0:$("#selectShipTo").select2("val"),
                                        config_type:$("#selectConfigType").select2("val")==""?0:$("#selectConfigType").select2("val"),
                                        config_id:$("#config_id").val()==""?0:$("#config_id").val(),
                                        process_step:$("#selectStep").val()==""?0:$("#selectStep").val(),		
                                   });

                                    var tscdCollection = new models.TitleShipConfigDetailes;
                                    var emptyFlag  = false;
                                    $("input[name='ri_id']").each(function(){
                                        //TODO 通过val 获取值
                                        var type = $(this).attr("data_type");
                                        var val = new Array();
                                        var intFlag = false;
                                        
                                        switch(type){
                                            case "1":
                                                //TEXT_FIELD
                                                var _value = $("input[name='ri_val_"+$(this).val()+"']").val();
                                                if(_value==""){
                                                    showMessage("Please input value","alert");
                                                    $("input[name='ri_val_"+$(this).val()+"']").focus();
                                                    emptyFlag= true;
                                                    return;
                                                }
                                                val.push($("input[name='ri_val_"+$(this).val()+"']").val());
                                                break;
                                            case "2":
                                                //TEXT_AREA
                                                var _value = $("textarea[name='ri_val_"+$(this).val()+"']").val().trim();
                                                if(_value==""){
                                                    showMessage("Please input value","alert");
                                                    $("textarea[name='ri_val_"+$(this).val()+"']").focus();
                                                    emptyFlag= true;
                                                    return;
                                                }
                                                val.push($("textarea[name='ri_val_"+$(this).val()+"']").val());
                                                break;
                                            case "3":
                                                //RADIO
                                                intFlag = true;
                                                val.push($("input[name='ri_val_"+$(this).val()+"']:checked").val());
                                                break;
                                            case "4":
                                                //checkbox

                                                var _value = $("input[name='ri_val_"+$(this).val()+"']:checked").length;
                                                if(_value==0){
                                                    showMessage("Please select value","alert");
                                                    //$("textarea[name='ri_val_"+$(this).val()+"']").focus();
                                                    emptyFlag= true;
                                                    return;
                                                }
                                                $("input[name='ri_val_"+$(this).val()+"']:checked").each(function(){
                                                    val.push($(this).val());
                                                })
                                                break;
                                            case "5":
                                                //DROP_LIST
                                                var _value = $("select[name='ri_val_"+$(this).val()+"']").val().trim();
                                                if(_value==""){
                                                    showMessage("Please select value","alert");
                                                    emptyFlag= true;
                                                    return;
                                                }
                                                intFlag = true;
                                                val.push($("select[name='ri_val_"+$(this).val()+"']").val());
                                                break;
                                            case "6":
                                                //Date
                                                var _value = $("input[name='ri_val_"+$(this).val()+"']").val().trim();
                                                if(_value==""){
                                                    showMessage("Please input value","alert");
                                                    emptyFlag= true;
                                                    return;
                                                }
                                                val.push($("input[name='ri_val_"+$(this).val()+"']").val());
                                                break;
                                        }

                                        //console.log("val"+val.join(","));
                                        var m = new models.TitleShipConfigDetail({
                                            ri_id:$(this).val()
                                        });
                                        if(intFlag){
                                            m.set("requirement_value_int",val.join(","));
                                        }else{
                                            m.set("requirement_value_str",val.join(","));
                                        }
                                        tscdCollection.add(m);
                                    });
                                    if(emptyFlag){
                                        //有空值
                                         //开启按钮
                                        dialog.button({
                                            name: 'Submit',
                                            focus: true,
                                            disabled: false
                                        });
                                        $.unblockUI();
                                        return false;
                                    }
                                    var tsc_id = $("#tsc_id").val();
                                    if(tsc_id!=""){
                                        model.set("tsc_id",tsc_id);
                                        model.url+="/"+tsc_id;
                                    }

                                    model.set("items",tscdCollection.toJSON());
                                    model.save(null,{
                                        success: function (e) {
                                            var result = e.changed.success;
                                            var content = "";
                                            var icon ="succeed";
                                            if (result==0) {
                                              content = "Save fail,please try again!";
                                              icon ="error";
                                            }else if(result==1){
                                                //添加成功
                                                content = "Save Success";
                                                icon ="succeed";
                                            }else if(result==2){
                                               //已存在
                                                content = "The Item Exites";
                                                icon ="alert";
                                            }
                                            showMessage(e.changed.des,icon);
                                            //关闭 刷新
                                            if(result==1){
                                              // tmp.closeSelPl();
                                              //tmp.listTstc.render();
                                                setTimeout(function(){
                                                  tmp.listTstc.render();
                                                  dialog.close();
                                                },1500);
                                            }else{
                                                //开启按钮
                                                dialog.button({

                                                    name: 'Submit',
                                                    focus: true,
                                                    disabled: false
                                                });
                                            }
                                            $.unblockUI();
                                        },
                                        error: function (e) {
                                          showMessage("System error","error");
                                          dialog.button({
                                            name: 'Submit',
                                            focus: true,
                                            disabled: false
                                          });
                                          $.unblockUI();
                                        }

                                    });
                                } else {
                                    $.unblockUI();
                                }
                                return false;
                            },
                            focus: true
                        }]
                        ,cancel:true
                        ,cancelVal:'Cancel'
                    });
                    //TODO 开始子类
                    var children = tmp.model.toJSON().children;
                    //console.log(children);
                    if(children.length>0){
                        var riIds = new Array();
                        for(var i = 0;i<children.length;i++){
                            riIds.push(children[i].ri_id);
                        }
                        //console.log(riIds);
                        var itemsVal = new models.RequirementItemModelDetailses;
                        itemsVal.url+="/"+riIds.join(",");
                        itemsVal.fetch({
                            async:false,
                            success:function(collection){ 
                                //console.log(collection);
                                //TODO 增加
                                for(var i = 0;i<children.length;i++){
                                    var _ri_id = children[i].ri_id;
                                    var _itemVal = children[i].requirement_value_str;
                                    if(children[i].requirement_value_int!=undefined){
                                        _itemVal=children[i].requirement_value_int;
                                    }
                                    var itemModel = new models.RequirementItemModel({
                                        requirement_name:children[i].requirement_name,
                                        display_type:children[i].display_type,
                                        description:children[i].description,
                                        ri_id:children[i].ri_id
                                    });
                                    var _itemsVal = collection.where({ri_id:_ri_id});
                                    var _itemsValCollection = new models.RequirementItemModelDetailses;
                                    for(var j = 0;j<_itemsVal.length;j++){
                                        _itemsValCollection.add(_itemsVal[j]);
                                    }
                                    itemModel.set("children",_itemsValCollection.toJSON());
                                    tmp.addRequirementConfig(itemModel,_itemVal);
                                }
                                
                            }
                        });
                    }
                    
                    
                    $("#selectTitle").select2({
                        placeholder: "Title...",
                        allowClear: true
                    }).bind("change",tmp.changeTitle);
                    $("#selectCustomer").select2({
                        placeholder: "Customer...",
                        allowClear: true
                    });
                    $("#selectCustomer").select2("val", tmp.model.toJSON().customer_key);
                    $("#selectTitle").select2("val", tmp.model.toJSON().title_id);
                    $("#selectShipTo").select2({
                        placeholder: "Ship To...",
                        allowClear: true
                    });
                    $("#selectShipTo").select2("val", tmp.model.toJSON().ship_to_id);
                    
                    $("#selectStep").select2({
                        placeholder: "Step...",
                        allowClear: true
                    });
                    $("#selectStep").select2("val", tmp.model.toJSON().process_step);
                    
                    $("#selectConfigType").select2({
                        placeholder: "Level...",
                        allowClear: true
                    });
                    $("#selectConfigType").select2("val", tmp.model.toJSON().config_type);
                    // slide.open();
                    //
                    $("#selectConfigType").bind("change",function(evt){
                        tmp.removeError("#selectConfigType");
                        var val = $("#selectConfigType").val();
                        var customer_id = $("#selectCustomer").val();
                        if($.trim(customer_id).length == 0){
                        	customer_id = 0;
                        }
                        var params = new models.ConfigSearchModel({
                            title_id:$("#selectTitle").val(),
                            customer_id : customer_id
                        });
                        $("#config_value").css("display","block");
                        $(".treecontrolsbox").remove();
                        if(val==1){
                          // tmp.plistView.render(params);
                          tmp.plDialog.render($("#selectTitle").select2("val"),customer_id);
                          //updateDialog("product-list-view", "Selete Product");
                        }else if(val==2){
                           tmp.pcSelectView.render($("#selectTitle").select2("val"),customer_id);
                        }else if(val==3){
                            tmp.plineView.render(params);
                        }else{
                          //
                          //$("#config_value").css("display","none");
                          $("#config").val("");
                          $("#config_id").val("");
                        }
                    });
                    
                    $("#config").bind("focus",function(evt){
                        tmp.removeError();
                        var val = $("#selectConfigType").val();
                        var customer_id = $("#selectCustomer").val();
                        if(val==1){
                          // tmp.plistView.render(params);
                          tmp.plDialog.render($("#selectTitle").select2("val"),customer_id);
                        }else if(val==2){
                            tmp.pcSelectView.render($("#selectTitle").select2("val"),customer_id);
                        }else if(val==3){
                            var params = new models.ConfigSearchModel({
                                title_id:$("#selectTitle").val(),
                                customer_id : customer_id
                            });                        	
                            tmp.plineView.render(params);
                        }
                    });
                    $("#selectShipTo").bind("change",function(evt){
                        tmp.removeError("#selectShipTo");
                    });
                    $("#selectTitle").bind("change",function(evt){
                        tmp.removeError("#selectTitle");
                        var val = $("#selectTitle").val();
                        $("#config_value").css("display","none");
                        $("#config").val("");
                        $("#config_id").val("");
                        $("#selectConfigType").select2("val", "");
                    });
                    $("#selectCustomer").bind("change",function(evt){
                        tmp.removeError("#selectCustomer");
                        $("#config_value").css("display","none");
                        $("#config").val("");
                        $("#config_id").val("");
                        $("#selectConfigType").select2("val", "");
                    });                    
                    $("#selectCustomer").bind("change",function(evt){
                        tmp.removeError();
                    });
                    if(flag){
                        $("#config_value").css("display","block");
                    }
                    $("#addRC").click(function(evt){
                    	tmp.selRequirementConfig(evt);
                    });
                }
            });
        },
        setCatagory:function(treeNode){
            $("#config_id").val(treeNode.id); 
            $("#config").val(treeNode.text); 
            
        },
        selRequirementConfig:function(evt){
            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            this.lrDialogView.render();
        },
        changeTitle:function(event){
        	var titleId = $(this).val();
        	if($.trim(titleId).length == 0){
        		titleId = "0";
        	}
        	var url = Config.getCustomer.url.replace("{titleId}",titleId);
        	$.ajax({
	       		 type : "get",
	       		 url  : url ,
	       		 async : false,
	       		 success : function(data){
	       			var customerId = $("#selectCustomer").val();
	       			$("#selectCustomer option[value!='']").remove();
	       			 if(data && data.length == 0){
	       				 $("#selectCustomer").prev().children("a.select2-choice").addClass("select2-default").children("span:first").text("Customer...");
	       				 $("#selectCustomer").val("");
	       			 }else{
	       				 //判断现在新的下拉框选项中是否包含之前选择的Customer 
	       				 var exist = false;
		       			 for(var i=0;i<data.length;i++  ){
			       			$("#selectCustomer").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
			       			
			       			if(customerId == data[i].id){
			       				exist = true;
			       			}
			       		 }	
		       			 if(exist){
		       				$("#selectCustomer").val(customerId);
		       				 
		       			 }else{
		       				 $("#selectCustomer").prev().children("a.select2-choice").addClass("select2-default").children("span:first").text("Customer...");
		       				 $("#selectCustomer").val("");		       				 
		       			 }
	       			 }
	       		 }
        	});               	
        },
        addRequirementConfig:function(model,val){
          var rc= new rcViews({el:"#requirement-box"});
          rc.setView({lrDialogView:this.lrDialogView});
          rc.render(model,val);
        },
        verification:function(){
            //验证
            var tmp = this;
            var title = $("#selectTitle").select2("val");
            var shipTo = $("#selectShipTo").select2("val");
            var customer = $("#selectCustomer").select2("val");
            var configType = $("#selectConfigType").select2("val");
            

            var _tmp = title+shipTo+customer+configType;
            
            if(null==_tmp || _tmp=="" || _tmp.length==0 || _tmp.indexOf("undefined")>-1){
                //错误
                var _content ="Please choose Title 、Customer、 Ship To and Config Type";
                tmp.errorShow("block",_content);
                return false;
            }

            if(configType!=null && configType!="" && configType!=undefined){
                var config = $("#config").val();
                if(config==null ||config ==""){
                    var _content ="Please choose a config";
                    tmp.errorShow("block",_content);
                    return false;
                }
            }
            if($("input[name='ri_id']").size() == 0){
            	tmp.errorShow("block","Please add Instruction Item");
            	return false;
            }
            
            return true;
        },
        removeError:function(errId){
            this.errorShow("none","");
        },
        errorShow :function(show,content){
            $("#error_warp").html(content);
            $("#error_warp").css("display",show)
        }
    });

    return modTstc;
});
