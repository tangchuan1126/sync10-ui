/**
 * Yuanxinyu
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../templates/templates.amd"
    ,"../model/TitleShipToConfigModel"
    ,"auto"
    ,"showMessage"
],function($,Backbone,Handlebars,templates,models,auto){

    return Backbone.View.extend({

        template:templates.product_list_search_key,
        initialize:function(options){

            this.setElement(options.el);
        },
        events:{

        },
        setView:function(views){
            this.plistView = views.plistView;
        },
        render:function(){

            var tmp = this;

            this.$el.html(this.template());

            auto.addAutoComplete(
                $("#search_key"),
                "/Sync10/action/administrator/product/getSearchProductsJSON.action",
                "merge_info",
                "p_name"
            );

            $("#search_key").bind("keydown",function(evt){

                if(evt.keyCode==13){

                    tmp.search();
                }
            });

            $("#eso_search").bind("click",function(evt){

                tmp.search();
            });
        },
        search:function(){
            var tmp = this;
            var _keys = $("#search_key").val();
            // if(!_keys){
            //     showMessage("Please input keys","alert");
            //     return;
            // }
            var _title_id = $("#selectTitle").select2("val");
            var customer_id = $("#selectCustomer").val();
            var searchProduct = new models.SearchProduct({
              title_id:_title_id,
              customer_id:customer_id,
              keys:_keys
            })
            tmp.plistView.render(searchProduct);
        }
    });
});
