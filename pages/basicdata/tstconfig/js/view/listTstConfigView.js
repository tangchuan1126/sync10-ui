"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../templates/templates.amd",
    "../model/TitleShipToConfigModel",
    "Paging",
    "slidePanel",
    "../config",
    "art_Dialog/dialog-plus",
    "jqueryui/tabs",
    "handlebars_ext",
    "artDialog"
],function($,Backbone,Handlebars,templates,models,Paging,SlidePanel,Config,Dialog){

    return Backbone.View.extend({

        template:templates.list_view_tstConfig,
        initialize:function(options){

            this.setElement(options.el);
            this.collection = new models.ShowTitleShipConfigCollection;
        },
        setView:function(views){
            this.delView = views.delView;
            this.listTstc = views.listTstc;
            this.modView = views.modView;
        },
        render:function(searchParam){

            //搜索参数
            if(!searchParam){
               searchParam = new models.SearchTstConfig;
            }
            //console.log(searchParam.toJSON());
            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            // save search param to the views
            this.searchParam = searchParam;
            var tmp = this;

            this.collection.reset();
            
            this.collection.fetch({
                data:{
                    pageNo:models.ShowTitleShipConfigCollection.pageCtrl.pageNo,
                    pageSize:models.ShowTitleShipConfigCollection.pageCtrl.pageSize,
                    customer_key:searchParam.toJSON().customer_key,
                    title_id:searchParam.toJSON().title_id,
                    ship_to_id:searchParam.toJSON().ship_to_id,
                    config_id:searchParam.toJSON().config_id,
                    item_value:searchParam.toJSON().item_value,
                    accurately:searchParam.toJSON().accurately,
                    config:searchParam.toJSON().config,
                    tsc_id:searchParam.toJSON().tsc_id,
                    config_type:searchParam.toJSON().config_type,
                    process_step:searchParam.toJSON().process_step
                },
                success:function(collection){

                    tmp.$el.html(tmp.template({
                        resultList:collection.toJSON(),
                        pageCtrl:models.ShowTitleShipConfigCollection.pageCtrl
                    }));

                    Paging.update_page(
                        models.ShowTitleShipConfigCollection.pageCtrl.pageCount
                        ,models.ShowTitleShipConfigCollection.pageCtrl.pageNo
                        , "pagination"
                        , function(pageNo) {

                            models.ShowTitleShipConfigCollection.pageCtrl.pageNo = pageNo;

                            tmp.render(searchParam);
                    });

                    $.unblockUI();
                }   
            });
        },
        events:{
            "click a[name='modRequirement']":"modTst",
            "click a[name='delRequirement']":"delTst"
        },
        modTst:function(evt){

            var tstid = $(evt.target).parents("tr").data("tstid");
            var model = this.collection.findWhere({tsc_id:tstid});

            this.modView.render(model,"Edit Handling SOP");

        },
        delTst:function(evt){
            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            var configType = {"1":"Product","2":"Product Category","3":"Product Line"};
            
            var tmp = this;
            var tstid = $(evt.target).parents("tr").data("tstid");
            var model = this.collection.findWhere({tsc_id:tstid});

            var configTypeValue = configType[model.toJSON().config_type+''];

            var html = '<div style="width:300px;height:100%;text-align:left;">';

            if(model.toJSON().customer_id){
                html += '<label style="width:100px;text-align:right;">Customer Id&nbsp;:&nbsp;</label><label style="font-weight: normal;">'+model.toJSON().customer_id+'</label>';
            }
            if(model.toJSON().title_name){
                html += '<br /><label style="width:100px;text-align:right;">Title&nbsp;:&nbsp;</label><label style="font-weight: normal;">'+model.toJSON().title_name+'</label>';
            }
            if(model.toJSON().ship_to_name){
                html += '<br /><label style="width:100px;text-align:right;">Ship To&nbsp;:&nbsp;</label><label style="font-weight: normal;">'+model.toJSON().ship_to_name+'</label>';
            }
            if(model.toJSON().config && configTypeValue){
                html += '<br /><label style="width:100px;text-align:right;vertical-align: top;">Config&nbsp;:&nbsp;</label><label style="font-weight: normal;">'+configTypeValue+'<br />'+model.toJSON().config+'</label>';
            }

             $.unblockUI();
              art.dialog({
                    title:'Delete'
                    ,content: html
                    ,lock: true
                    ,opacity:0.3
                    ,fixed:true
                    ,init:function(){

                        var w1 = $(window).width(), H = $('html');
                        H.css('overflow', 'hidden');
                        var w2 = $(window).width();
                        H.css('margin-right', (w2 - w1) + 'px');
                    }
                    ,close:function(){

                        document.body.parentNode.style.overflow="scroll";
                        document.body.parentNode.style.marginRight="";
                    }
                    ,okVal:'Sure'
                    ,ok:function(event){

                        var dialog = this;

                       //禁用按钮
                        dialog.button({

                            name: 'Sure',
                            focus: true,
                            disabled: true
                        });

                        var config = new models.TitleShipConfig({
                            tsc_id:tstid
                        });

                        config.url += "?tstid="+tstid;

                        config.destroy({
                            success: function(){
                                    
                                showMessage("Success","succeed");
                                
                                setTimeout(function(){
                                    tmp.listTstc.render(tmp.searchParam);
                                    dialog.close();
                                },1500);
                                
                            }
                        });

                        return false;
                    }
                    ,cancel:true
                    ,cancelVal:'Cancel'
                });
        }
    });
});
