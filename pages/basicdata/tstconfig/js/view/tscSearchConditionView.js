"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
		"immybox",
		"slidePanel",
		"../config",
    "../model/TitleShipToConfigModel",
		"require_css!oso.lib/immybox-master/immybox.css"], 
function($, Backbone, HandleBars, templates,immybox,SlidePanel,Config,models) {
	return Backbone.View.extend({
		template: templates.search_condition_tsc,
      initialize: function() {
        var tmp = this;
      },
  		render: function() {
  			
        var tmp = this;
        
        var configTypes = [
			{"value":1,"text":"Product"},
			{"value":2,"text":"Product Category"},
			{"value":3,"text":"Product Line"}
		];

        $.getJSON(Config.loadTitleUrl.url, function (json) {
            tmp.$el.html(tmp.template({
                customers:json.customers,
                titles:json.titles,
                shiptos:json.shiptos,
                requirements:json.requirements,
                configTypes: configTypes,
                processSteps:json.processsteps
            }));
            tmp.titles =json.titles;
            $("#_customer").select2({
              placeholder: "Customer...",
              allowClear: true
            }).on('change',function(){tmp.search();});
            $("#_title").select2({
              placeholder: "Title...",
              allowClear: true
            }).on('change',function(){tmp.search();});
            $("#_ship_to").select2({
              placeholder: "Ship To...",
              allowClear: true
            }).on('change',function(){tmp.search();});
            $("#_requirement_value").select2({
              placeholder: "Instruction Item...",
              allowClear: true
            }).on('change',function(){tmp.search();});

            $("#_r_v").hide();

            $("#_requirement").select2({
              placeholder: "Instruction...",
              allowClear: true
            }).on('change',function(){
              var ri_id = $(this).select2("val");
              if(ri_id)
              {

                var model = new models.RequirementItemModel({
                  ri_id:ri_id
                });
                model.url+="/"+ri_id
                model.fetch({async:false,
                    success: function(model, response){
                     
                      if(model && model.toJSON().display_type>2){

                        var ridata = model.toJSON().children;

                        var str = "";
                        str = "<option value=''></option>";
                        //str = "";
                        for(var i = 0; i < ridata.length; i ++){
                            str += "<option value="+ridata[i].item_value+">"+ridata[i].item_name +"</option>";
                        }
                        
                        $("#_requirement_value").html(str);
                        $("#_requirement_value").select2("val","");

                        $("#_r_v").show();

                        tmp.search();

                      }else{
                        $("#_r_v").hide();
                        $("#_requirement_value").html("<option value=''></option>");
                        $("#_requirement_value").select2("val","");
                      }
                      
                    }
                });
                
              }
              else
              {
                $("#_r_v").hide();
                $("#_requirement_value").html("<option value=''></option>");
                $("#_requirement_value").select2("val","");
              }
            });
            
            $("#selectConfigType").select2({
                placeholder: "Level...",
                allowClear: true
            }).on('change',function(){tmp.search();});
            
            $("#selectConfigType").bind("change",function(evt){
                tmp.removeError("#selectConfigType");
                var val = $("#selectConfigType").val();
               
                var params = new models.ConfigSearchModel({
                    title_id:$("#_title").val()
                })
                $("#config_value").css("display","block");
                $(".treecontrolsbox").remove();
                if(val==1){
                  tmp.plDialog.render($("#_title").select2("val"));
                }else if(val==2){
                   tmp.pcSelectView.render($("#_title").select2("val"));
                }else if(val==3){
                    tmp.plineView.render(params);
                }else{
                  $("#config_value").css("display","none");
                  $("#config").val("");
                  $("#config_id").val("");
                }
                tmp.search();
            });
            
            $("#config").bind("focus",function(evt){
                tmp.removeError();
                var val = $("#selectConfigType").val();
                var params = new models.ConfigSearchModel({
                    title_id:$("#selectTitle").val()
                })
                if(val==1){
                    tmp.plDialog.render($("#_title").select2("val"));
                }else if(val==2){
                    tmp.pcSelectView.render($("#_title").select2("val"));
                }else if(val==3){
                    tmp.plineView.render(params);
                }
            });

            $("#selectStep").select2({
                placeholder: "Step...",
                allowClear: true
            }).on('change',function(){tmp.search();});
            
        });

  		},
      
      setView:function(views){
        this.listTstc = views.listTstc;
        this.itemView= views.itemView;
        this.modView= views.modView;
        this.plineView = views.plineView;
        this.pcSelectView=views.pcSelectView;
        this.plDialog = views.plDialog;
      },
  		events:{
        "click #addConfig":"AddTscConfig",
        "click #btnLoadFilter" :"search"
      },
      AddTscConfig:function(evt){
      //打开遮罩
      $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});

      	this.modView.render(null,"Add Handling SOP");
      //关闭遮罩 
        $.unblockUI();
      },
      search:function(evt){
		 var search = new models.SearchTstConfig({
			customer_key : $("#_customer").select2("val"),
			title_id : $("#_title").select2("val"),
			ship_to_id : $("#_ship_to").select2("val"),
			config_id : $("#_requirement").select2("val"),
			item_value : $("#_requirement_value").select2(
					"val"),
			accurately : true/*$("#accurately").prop('checked')*/,
			config : $("#config_id").val(),
			tsc_id : $("#tsc_id").val(),
			config_type : $("#selectConfigType").select2(
					"val"),
			process_step: $("#selectStep").select2("val")		
		 });
		 this.listTstc.render(search);
      },
      
      removeError:function(errId){
          this.errorShow("none","");
      },
      
      errorShow :function(show,content){
          $("#error_warp").html(content);
          $("#error_warp").css("display",show)
      }
      
	});
});