
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../templates/templates.amd",
    "slidePanel",
    "Paging",
    "../model/TitleShipToConfigModel",
    "jqueryui/dialog",
    "artDialog",
    "../jquery.dotdotdot.min"
],function($,Backbone,Handlebars,templates,SlidePanel,Paging,models){

    return Backbone.View.extend({
        template:templates.list_requirement,
        initialize:function(options){
            this.setElement(options.el);
            this.collection = new models.RequirementItemCollection;
            this.Listdialog = null;
        },
        events:function(){
            var tmp = this;
            $(".search_result_list_content tr").bind("click",function(evt){
                tmp.setConfig(evt);
            });
            $(".search_result_list_content div").bind("click",function(evt){
                tmp.setConfigDiv(evt);
            });
            
        },
        setView:function(views){
            this.modView = views.modView;
            this.lrDialogView = views.lrDialogView;
        },
        render:function(searchParam){
            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            
            if(!searchParam){
                searchParam = new models.SearchRequirement();
                searchParam.set("searchIsactive", 1);
            }
            var tmp = this;
            this.collection.fetch({
                data:{
                    //title_id:searchParam.toJSON().title_id
                    pageNo:models.RequirementItemCollection.pageCtrl.pageNo,
                    pageSize:models.RequirementItemCollection.pageCtrl.pageSize,
                    searchRname:searchParam.toJSON().searchRname,
                    searchDisplaytype:searchParam.toJSON().searchDisplaytype,
                    searchIsactive:searchParam.toJSON().searchIsactive,
                    cmd:searchParam.toJSON().cmd
                },
                async:false,
                success:function(collection){
                    
                    tmp.$el.html(
                        tmp.template({
                            resultList:collection.toJSON(),
                            pageCtrl:models.RequirementItemCollection.pageCtrl
                    }));

                    Paging.update_page(
                        models.RequirementItemCollection.pageCtrl.pageCount
                        ,models.RequirementItemCollection.pageCtrl.pageNo
                        , "pagination"
                        , function(pageNo) {

                        models.RequirementItemCollection.pageCtrl.pageNo = pageNo;
                        tmp.render(searchParam);
                    });


                    setTimeout(function(){

                        $("._desclist").css("visibility","visible").dotdotdot({
                            callback : function( isTruncated, orgContent ) {

                                if(isTruncated){

                                    var $desc = orgContent.prevObject;
                                    
                                   var createDots = function(){ 
                                        if(!$desc.has('a.toggle').length){
                                            $desc.append( ' <a class="toggle" href="#"><span class="open">[ + ]</span><span class="closed">[ - ]</span></a>' );
                                        }else{
                                            $desc.dotdotdot({
                                                after: 'a.toggle'
                                            });
                                        }
                                    };


                                    var destroyDots = function() {
                                        $desc.trigger( 'destroy' );

                                        if(!$desc.has('a.toggle').length){
                                            $desc.append( ' <a class="toggle" href="#"><span class="open">[ + ]</span><span class="closed">[ - ]</span></a>' );
                                        }
                                    };

                                     var initDots = function(){
                                        destroyDots();
                                        createDots();
                                    };
                                   // createDots();
                                    initDots();

                                    $desc.on(
                                        'click',
                                        'a.toggle',
                                        function() {
                                            $desc.toggleClass( 'opened' );

                                            if ( $desc.hasClass( 'opened' ) ) {
                                                destroyDots();
                                            } else {
                                                createDots();
                                            }
                                            return false;
                                        }
                                    );

                                }
                            }
                        });
                    },10);


                    tmp.events();
                    $.unblockUI();
                }
            }) ;

            
        },
        trdblclick:function(evt){
            var tmp = this;
            tmp.setConfig(evt);
            tmp.closeSelPl();
        },
        setConfigDiv:function(evt){
            var ri_id = $(evt.target).parents("tr").data("ri_id");
            if(!ri_id) return;
            var collection = this.collection;
            var model = this.collection.findWhere({ri_id:ri_id});
            // this.addTstConfig["riModel"] = model;
            this.closeSelPl();
            this.modView.addRequirementConfig(model);
        },
        setConfig:function(evt){
            var ri_id = $(evt.target).parent().data("ri_id");
            if(!ri_id) return;
            var collection = this.collection;
            var model = this.collection.findWhere({ri_id:ri_id});
            // this.addTstConfig["riModel"] = model;
            this.closeSelPl();
            this.modView.addRequirementConfig(model);
            
        },
        closeSelPl :function(){
            var tmp = this;
            tmp.lrDialogView.closeDialog();
        }
    });
});
