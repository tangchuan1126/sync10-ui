"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../../templates/templates.amd",
    "../model/TitleShipToConfigModel",
		"../config",
    "./productListSearchView",
    "./productListSearchTitleView",
    "./productListSearchLineView",
    "./productListView",
    "./productCatagorysSearchView",
    "./productListSearchKeyView",
    "artDialog"], 
function($, Backbone, HandleBars, templates,models,Config,PlistSearchView,plstView,plslView,plistView,pcSearchView,plKeySearch) {
	var plDialog = null;
  return Backbone.View.extend({
		template: templates.product_list_dialog,
      initialize: function() {
        var tmp = this;
      },
  		render: function(_tid,customer_id) {
        var tmp = this;
        plDialog = art.dialog({
            title:'SELECT Product'
            ,lock: true
            ,opacity:0.3
            ,width:780
            ,height:418
            ,padding:'1px 1px 1px 1px'
            ,init:function(){
                var w1 = $(window).width(), H = $('html');
                H.css('overflow', 'hidden');
                var w2 = $(window).width();
                H.css('margin-right', (w2 - w1) + 'px');
                this.content(tmp.template());
            },close:function(){
                document.body.parentNode.style.overflow="scroll";
                document.body.parentNode.style.marginRight="";
            }
        });
        $("#product_all_tabs").tabs();
          var setActiveTab = function(i){
              $("#product_all_tabs").tabs("option","active",i);
          };

          tmp.plKeySearch = new plKeySearch({el:"#product-list-search-view"});
          setActiveTab(0);
          //TITLE
          tmp.plstView = new plstView({el:"#search-product-title"}); 
          tmp.plslView = new plslView({el:"#search-product-line"})
          tmp.pcSearchView = new pcSearchView({el:"#search-product-productCategory"});
          tmp.plstView.setView({
            tscSearchCondition:tmp.tscSearchCondition,
            plslView:tmp.plslView
          });
          tmp.plslView.setView({
            pcSearchView:tmp.pcSearchView
          });

          tmp.plstView.render();
          if(_tid){
            //处理添加到
            tmp.plstView.initTitleId(_tid);
            tmp.plstView.initCustomerId(customer_id);
          }
          tmp.plKeySearch.render();
          // tmp.plistSearchView = new PlistSearchView({el:"#product_search_advanced"});

          tmp.plistView = new plistView({el:"#product-list-view"});
          tmp.plistView.setView({plistDialog:tmp});
          if(!_tid){
        	  _tid = "0";
          }
          if(!customer_id){
        	  customer_id = "0";
          }
          
          //设置view v
           var searchProduct = new models.SearchProduct({
            title_id:_tid,
            customer_id : customer_id
          });
          tmp.plistView.render(searchProduct);
          
          tmp.plKeySearch.setView({
            plistView:tmp.plistView
          });
          $('#condition').delegate('a','click',function(evt){
              tmp.closeType(evt,tmp)
          });
          $('#search-condition').delegate('#product-condition-search-btn','click',function(evt){
            tmp.searchProduct(evt,tmp)
          });
  		},
      setView:function(views){
        this.modView=views.modView;
        this.tscSearchCondition=views.tscSearchCondition;
      },
      setConfig:function(model){
        $("#config").val(model.toJSON().p_name);
        $("#config_id").val(model.toJSON().pc_id);
        this.closeDialog();
      },
      closeDialog:function(){
        if(plDialog && !plDialog.closed){
             plDialog.close();
        }
        plDialog=null;
      },
      
      searchProduct:function(evt,tmp){
        
        var _title_id = tmp.getTitleIds();
        var _line_id = tmp.getProductLineIds();
        var _Categoryid = tmp.getCategoryIds();
        var customer_id = 0;
        if($("#selectCustomer").size() > 0){
        	customer_id = $("#selectCustomer").val();
        }
        
        var searchProduct = new models.SearchProduct({
          title_id:_title_id,
          customer_id:customer_id,
          product_line_id:_line_id,
          product_catagorys_id:_Categoryid
        })
        tmp.plistView.render(searchProduct);
      },
      closeType:function(evt,tmp){

        var type =$(evt.target).parent().attr("type");
        var customerId = $("#selectCustomer").val();
        if($.trim(customerId).length == 0){
        	customerId = 0;
        }
        if("Title"==type){
          var titleId = $("#selectTitle").val();
          
          if($.trim(titleId).length == 0){
              $(evt.target).parent().parent().empty();
              $("#search-product-title").show();
              $("#search-product-line").empty();
              $("#search-product-productCategory").empty();
              var searchParam = new models.SearchProduct({customer_id:customerId,title_id:titleId});
              tmp.plistView.render(searchParam);
              $("#search-condition").hide();        	  
          }
        }else if("Product Line"==type){
          $(evt.target).parent().remove();
          var title_id = $("div[type='Title'] a").data("id");
          tmp.plslView.render(title_id,customerId);
          $("div[type='Category']").remove();
          $("#search-product-productCategory").empty();

        }else if("Category"==type){
          //TODO 判断个数，如果个数是1 则删除parent否是不删除
          //删除选择后的所有元素
          var index = $(evt.target).index();
          var length = $(evt.target).parent().find("a").length;
          
          var title_id = $("div[type='Title'] a").data("id");
          var _line_id = $("div[type='Product Line'] a").data("id");
          var customerId = $("#selectCustomer").val();
          if(index==1){
            $(evt.target).parent().remove();
            tmp.pcSearchView.render(title_id,customerId,_line_id,"");
          }else{
        	var test_pid = $(evt.target).data("pid");
            tmp.pcSearchView.render(title_id,customerId,"",test_pid);        	
            for(var i=length-1;i>0;i--){
              $(evt.target).nextAll().remove();
              $(evt.target).remove();
            }
          }
          $("#search-product-productCategory").show();
        }
      },
      getTitleIds:function(){
        // var ids  = new Array();
        // $("div[type='Title'] a").each(function(){
        //   ids.push($(this).data("id"));
        // })
        // return ids.join(",");
        return this.getSelectId("Title");
      },
      getProductLineIds:function(){
        return this.getSelectId("Product Line");
      },
      getCategoryIds:function(){
        return this.getSelectId("Category");
      },
      getSelectId:function(type){
        var ids  = new Array();
        if(type == "Category"){
            $("div[type='"+type+"'] a:last").each(function(){
                ids.push($(this).data("id"));
             }) ;       	
        }else{
            $("div[type='"+type+"'] a").each(function(){
                ids.push($(this).data("id"));
            }) ;      	
        }
        return ids.join(",");
      }
	});
});