/**
    create by zhaoyy 2015年4月2日 14:47:16
**/
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../view/tscSearchConditionView",
    "../view/listTstConfigView",
    "../view/RequirementItemValuesView",
    "../view/modTstConfigView",
    "../view/productLineView",
    "../view/productListView",
    "../view/productListSearchView",
    "../view/listRequirementView",
    "../view/productCatagorysView",
    "../view/productCatagorysSearchView",
    "../view/productListDialogView",
    "../view/listRequirementDialogView",
    "blockui",
    "jqueryui/tabs"
],function(
    $
    ,Backbone
    ,Handlebars
    ,TscSearchCondition
    ,listTstConfig
    ,ItemValuesView
    , ModView
    ,pLineView
    ,pListView
    ,pListSearchView
    ,riListView
    ,pcSelectView
    ,pcSearchView
    ,plDialogView
    ,lrDialogView
){
    
    (function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();
    $("#all_tabs").tabs();
    
    var modView = new ModView({el:"#mod_tstc_box"});
    var plineView = new pLineView({el:"#product-line-list-view"});
    //var plistView = new pListView({el:"#product-list-view"});
    //var plistSearchView = new pListSearchView({el:"#product-list-search-viewrender"});
    var riListView = new riListView({el:"#requirement-list-view"});
    var pcSelectView = new pcSelectView({el:"#product_catagorys-view"});
    var pcSearchView = new pcSearchView({el:"#search-product-productCategory-none"});
    var tscSearchCondition = new TscSearchCondition({el:"#search-warp"});

    //选择产品 的对话框
    var plDialog = new plDialogView();

    // 选择Requirement 的列表
    var lrDialogView = new lrDialogView();

    plDialog.setView({
        modView:modView,
        tscSearchCondition:tscSearchCondition
    });

    lrDialogView.setView({
         modView:modView
    }) ;
    plineView.setView({
        modView:modView
    });

    riListView.setView({
        modView:modView
    });
    
    pcSelectView.setView({
        modView:modView
    });
    
    
    var listTstc = new listTstConfig({el:"#list"});
    var itemView = new ItemValuesView({el:"#requirement_item_values"});
    tscSearchCondition.setView({
        listTstc:listTstc,
        itemView:itemView,
        modView:modView,
        plineView:plineView,
        pcSelectView:pcSelectView,
        plDialog:plDialog
    });
    listTstc.setView({
        tscSearchCondition:tscSearchCondition,
        listTstc:listTstc,
        modView:modView
    });
    modView.setView({
        plineView:plineView,
        riListView:riListView,
        listTstc:listTstc,
        pcSelectView:pcSelectView,
        plDialog:plDialog,
        lrDialogView:lrDialogView
    });
    pcSearchView.setView({
        pcSelectView:pcSelectView
    })
    listTstc.render();
    tscSearchCondition.render();
});