/**
*
* create by zhaoyy 
* @date 2015年4月7日 10:16:07
* @version  1.0
*/
"use strict";
require([
     "/Sync10-ui/requirejs_config.js"
],function(){
	require([
    "jquery",
    "backbone",
    "handlebars",
    "bootstrap",
    "nprogress",
    "blockui",
    "../view/addTstConfigView",
    "../view/productLineView",
    "../view/productListView",
    "../view/listRequirementView",
    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
    "../config"
], function ($, Backbone, handlebars, bootstrap, NProgress, blockUI, AddView,pLineView,pListView,riListView, AsynLoadQueryTree, datetimepicker ,config) {

    NProgress.start();
    //遮罩
    (function () {
        $.blockUI.defaults = {
            css: {
                padding: '8px',
                margin: 0,
                width: '170px',
                top: '45%',
                left: '40%',
                textAlign: 'center',
                color: '#000',
                border: '3px solid #999999',
                backgroundColor: '#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: '0.6'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut: 1000,
            showOverlay: true
        };
    })();

    
    $(document).ajaxStart(function () {
        $.blockUI({ message: '<img src="/Sync10-ui/pages/load/css/img/dy_loading.gif" class="loading" align="absmiddle"/> 正在处理，请稍后.' });   //遮罩打开 
    });

    $(document).ajaxError(function () {
        $.blockUI({ message: '出现错误，请刷新!<div><a class="closeBlock">关闭</a></div>' });   //遮罩打开 
        $(".closeBlock").click(function () {
            $.unblockUI();
        });
    });

    $(document).ajaxSuccess(function () {
        $.unblockUI();
    });

    var addView = new AddView({el:"#addTstcBox"}); 
    var plineView = new pLineView({el:"#plViewBox"});
    var plistView = new pListView({el:"#plistViewBox"});
    var riListView = new riListView({el:"riListViewBox"});
    plineView.setView({
        addTstConfig:addView
    });
    plistView.setView({
        addTstConfig:addView
    });
    riListView.setView({
        addTstConfig:addView
    });
    addView.setView({
        plineView:plineView,
        plistView:plistView,
        riListView:riListView
    });

    addView.render();

    NProgress.done();
});
})