"use strict";

require([
     "../../../requirejs_config.js"
], function(){

	var cssfileall = [
      "require_css!Font-Awesome"
    ];

    require(cssfileall);
    
    require(["index"]);
});
