(function(){

	 var configObj = {
	 	// 配置 requirementItemDetails 的URL
	 	riDetailsUrl:{
	 		url  :"/Sync10/basicdata/requirement/handleRiDetails"
	 	},
	 	// Display Type URL
	 	displayTypeUrl:{
	 		url  :"/Sync10-ui/pages/basicdata/json/DisplayType.json"
	 	},
	 	//添加Title Ship To Config 时的参数
	 	addTstConfigParamsUrl:{
	 		url  :"/Sync10/basicdata/tstc/params"
	 	},
	 	//默认配置
        requirementUrl:{
            url:"/Sync10/basicdata/requirement/handleRItems"
        },
        //
	 	rItemUrl:
	 	{
	 		url:"/Sync10/basicdata/requirement/handleRItems"
	 	},
	 	loadListUrl:{
	 		url  :"/Sync10/basicdata/tstc/handleTst"
	 	},
	 	delTstUrl:{
	 		url  :"/Sync10/basicdata/tstc/del/"
	 	},
	 	loadUCCLabelUrl:{
	 		url  :"/Sync10-ui/pages/basicdata/json/ucc_lable_data.json"
	 	},
	 	loadTitleUrl:{
	 		url  :"/Sync10/basicdata/title/getTitle"
	 	},
	 	loadShipToUrl:{
	 		url  :"/Sync10/basicdata/shipTo/all"
	 	},
	 	//获取全部的Requirement
	 	loadRequirementUrl:{
			url  :"/Sync10/basicdata/requirement/all"
	 	},
	 	loadProductLineUrl:{
	 		url  :"/Sync10/basicdata/productLine/getByTitleId"
	 	},
	 	loadProductLineViewUrl:{
	 		url  :"/Sync10-ui/pages/basicdata/product_line.html"
	 	},
	 	loadProductCatagorysViewUrl:{
	 		url  :"/Sync10-ui/pages/basicdata/product_catagorys.html"
	 	},
	 	loadProductCatagorysDataUrl:{
	 		url  :"/Sync10/basicdata/productCatagorys/getByTitleAndCustomer"
	 	},
	 	loadConfigTypeUrl:{
	 		url  :"/Sync10-ui/pages/basicdata/json/config_type_data.json"
	 	},
	 	loadProductDataUrl:{
	 		url  :"/Sync10/basicdata/product/getByTitleAndCustomer"
	 	},
	 	//Title Ship to Config 
	 	handleTitleShipToUrl:{
	 		url  :"/Sync10/basicdata/tstc/handleTst"
	 	},
	 	getCatagorysByPid:{
	 		url  :"/Sync10/basicdata/productCatagorys/getByLine"
	 	},
	 	getCustomer:{
	 		url  :"/Sync10/basicdata/customer/title/{titleId}"
	 	}	 	
	 };
	 define(configObj);
}).call(this);