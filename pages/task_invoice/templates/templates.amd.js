define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addDetail'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<tr>\n    <td class=\"text-center\"><span class=\"J-line-no\">"
    + alias3(((helper = (helper = helpers.LINE_NO || (depth0 != null ? depth0.LINE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"LINE_NO","hash":{},"data":data}) : helper)))
    + "</span></td>\n    <td><input class=\"form-control J-service-id\" placeholder=\"Service ID\" value=\""
    + alias3(((helper = (helper = helpers.SERVICE_ID || (depth0 != null ? depth0.SERVICE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SERVICE_ID","hash":{},"data":data}) : helper)))
    + "\"/></td>\n    <td><input class=\"form-control J-des\" placeholder=\"Description\" value=\""
    + alias3(((helper = (helper = helpers.DESCRIPTION || (depth0 != null ? depth0.DESCRIPTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"DESCRIPTION","hash":{},"data":data}) : helper)))
    + "\"/></td>\n    <td>\n    	<select class=\"form-control J-unit\">\n            <option value=\"\" selected></option>\n    		<option value=\"1\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 1",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Squara Feet</option>\n    		<option value=\"2\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 2",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Cubic Feet</option>\n            <option value=\"3\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 3",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">CWT</option>\n            <option value=\"4\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 4",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Long Ton</option>\n            <option value=\"5\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 5",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Short Ton</option>\n            <option value=\"6\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 6",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Metric Ton</option>\n            <option value=\"7\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 7",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Invoice</option>\n            <option value=\"8\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 8",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Package</option>\n            <option value=\"9\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 9",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Pallet</option>\n            <option value=\"10\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 10",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Piece</option>\n            <option value=\"11\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 11",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Receipt</option>\n            <option value=\"12\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 12",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Release</option>\n            <option value=\"13\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 13",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Period</option>\n            <option value=\"14\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 14",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Day</option>\n            <option value=\"15\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 15",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Hour</option>\n    	</select>\n    </td>\n    <td><input class=\"form-control J-quantity\" placeholder=\"0.0000\" value=\""
    + alias3(((helper = (helper = helpers.QUANTITY || (depth0 != null ? depth0.QUANTITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"QUANTITY","hash":{},"data":data}) : helper)))
    + "\"/></td>\n    <td><input class=\"form-control J-rate\" placeholder=\"0.0000\" value=\""
    + alias3(((helper = (helper = helpers.RATE || (depth0 != null ? depth0.RATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"RATE","hash":{},"data":data}) : helper)))
    + "\"/></td>\n    <td><span class=\"J-amount\">"
    + alias3(((helper = (helper = helpers.AMOUNT || (depth0 != null ? depth0.AMOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"AMOUNT","hash":{},"data":data}) : helper)))
    + "</span></td>\n    <td><i class=\"icon icon-remove\"></i></td>\n</tr>\n";
},"2":function(depth0,helpers,partials,data) {
    return "selected";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.detail : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['addInvoice'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"container-fluid task-invoice add-invoice\">\n    <div role=\"tabpanel\" class=\"panel panel-default\">\n        <div class=\"panel-body\">\n            <!-- Nav tabs -->\n            <ul class=\"nav nav-tabs\" role=\"tablist\">\n                <li role=\"presentation\" class=\"active\">\n                    <span class=\"check\"></span>\n                    <a href=\"#template\" aria-controls=\"template\" role=\"tab\" data-toggle=\"tab\">Customer Info</a>\n                </li>\n                <li role=\"presentation\">\n                    <span  id=\"check_detail\" class=\"check\"></span>\n                    <a href=\"#detail\" aria-controls=\"detail\" role=\"tab\" data-toggle=\"tab\">Bill Detail</a>\n                </li>\n                <!-- <li role=\"presentation\">\n                    <a href=\"#browse\" aria-controls=\"browse\" role=\"tab\" data-toggle=\"tab\">Preview</a>\n                </li> -->\n            </ul>\n\n            <!-- Tab panes -->\n            <div class=\"tab-content\">\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"template\"></div>\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"detail\"></div>\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"browse\"></div>\n                <div class=\"modal\"><div class=\"loading\"></div></div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['addSubTask'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"table table-bordered add-task\">\n    <tbody>\n        <tr> \n            <td>To</td>\n            <td>\n                <select style='width:350px;' multiple=\"multiple\" name=\"schedule_execute_id\" id=\"schedule_execute_id\"> </select>\n				<input type=\"hidden\" id=\"deleteusers\" name=\"deleteusers\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_EXECUTE_ID : stack1), depth0))
    + "'/>\n				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n				<input type=\"checkbox\" id=\"is_additional_Task\" name=\"is_additional_Task\" style=\"display:none;\"/>\n				\n				<input type=\"hidden\" id=\"assign_user_id\" name=\"assign_user_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ASSIGN_USER_ID : stack1), depth0))
    + "'/>\n	    		<input type=\"hidden\" id=\"parent_schedule_id\" name=\"parent_schedule_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.PARENT_SCHEDULE_ID : stack1), depth0))
    + "'/>\n	    		<input type=\"hidden\" id=\"schedule_id\" name=\"schedule_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_ID : stack1), depth0))
    + "'/>\n	    		<input type=\"hidden\" id=\"task_number\" name=\"task_number\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.TASK_NUMBER : stack1), depth0))
    + "'/>\n            </td>\n        </tr>\n        <tr>\n            <td>Task</td>\n            <td> \n                <textarea class=\"form-control\" name=\"schedule_detail\" id=\"schedule_detail\" placeholder=\"Description...\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_DETAIL : stack1), depth0))
    + "</textarea>\n            	<div class=\"submit\"><button class=\"btn btn-default\" type=\"button\" id=\"save_sub_task\">Submit</button></div>\n            </td>\n        </tr>\n    </tbody>\n</table>\n\n";
},"useData":true});
templates['addTask'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		                		<option value=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.LR_TYPE : stack1),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"2":function(depth0,helpers,partials,data) {
    return " selected='selected' ";
},"4":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        				<dd>\n	        				<span name=\"billId\">"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</span> <div class=\"invoice_del\"><i class=\"icon icon-remove\" name=\"delInvoice\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\"></i></div>\n	        			</dd>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		        			<dd>\n		        				<span name=\"fileId\">\n		        					<a href=\"/Sync10/_fileserv/file/"
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" download=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" data-gallery=\"\">\n		        						"
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\n		        					</a>\n		        				</span>\n		        				<div class=\"file_del\"><i class=\"icon icon-remove\" name=\"delFile\" data-filename=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" data-fileid=\""
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\"></i></div>\n	        				</dd>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"add-task-div\">\n<table class=\"table table-bordered add-task\">\n    <tbody>\n        <tr>\n            <td>From</td>\n            <td>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ASSEMPLOYNAME : stack1), depth0))
    + "\n	    	<input type=\"hidden\" id=\"assign_user_id\" name=\"assign_user_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ASSIGN_USER_ID : stack1), depth0))
    + "'/>\n	    	<input type=\"hidden\" id=\"schedule_id\" name=\"schedule_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_ID : stack1), depth0))
    + "'/>\n	    </td>\n        </tr> \n        <tr>\n            <td>To</td>\n            <td>\n				<input type=\"text\" class=\"form-control\" id=\"exe_employ_name\" name=\"exe_employ_name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.EXEEMPLOYNAME : stack1), depth0))
    + "\" placeholder=\"To...\" readonly=\"readonly\"/>\n				<input type=\"hidden\" id=\"schedule_execute_id\" name=\"schedule_execute_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_EXECUTE_ID : stack1), depth0))
    + "'/>\n				<input type=\"hidden\" id=\"deleteusers\" name=\"deleteusers\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_EXECUTE_ID : stack1), depth0))
    + "'/>\n				<font>*</font>\n            </td>\n        </tr>\n        <tr> \n            <td>CC</td>\n            <td>\n				<input type=\"text\" class=\"form-control\" id=\"join_employ_name\" name=\"join_employ_name\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.JOINEMPLOYNAME : stack1), depth0))
    + "' placeholder=\"CC...\" readonly=\"readonly\"/>\n				<input type=\"hidden\" id=\"schedule_join_execute_id\" name=\"schedule_join_execute_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_JOIN_EXECUTE_ID : stack1), depth0))
    + "'/>\n				<input type=\"hidden\" id=\"del_join_user_id\" name=\"del_join_user_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_JOIN_EXECUTE_ID : stack1), depth0))
    + "'/>\n            </td>\n        </tr>\n         <tr>\n            <td>Time</td>\n            <td class=\"js-date-task\">\n			  <div class=\"input-append form-group add-task_form-group1\">\n    			<input class=\"form_datetime form-control inconHand bgiconurl_\" type=\"text\" id=\"start_time\" name=\"start_time\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.START_TIME : stack1), depth0))
    + "\" size=\"20\" readonly=\"readonly\" placeholder=\"start time\">\n			  </div>\n			  <b>To</b>\n			  <div class=\"input-append form-group add-task_form-group2\">\n    			<input class=\"form_datetime form-control inconHand bgiconurl_\" type=\"text\" id=\"end_time\" name=\"end_time\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.END_TIME : stack1), depth0))
    + "\" size=\"20\" readonly=\"readonly\" placeholder=\"end time\">\n			  	<font>*</font>\n			  </div>\n			  \n            </td>\n        </tr>\n        <tr>\n            <td>Subject</td>\n            <td>\n                <input class=\"form-control\" type=\"text\"  name=\"schedule_overview\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_OVERVIEW : stack1), depth0))
    + "\" id=\"schedule_overview\" placeholder=\"Subject...\" />\n            	<font>*</font>\n            </td>\n        </tr>\n         <!--\n        <tr>\n            <td>LR No.</td>\n            <td>\n    			<dt>\n    			 	<i class=\"icon icon-add hid_lr\" name=\"addLR\" ></i>\n    			</dt>\n    			<dt name=\"lr_no\">\n				  	<div class=\"project_lr_no_left\">\n	        			<select class=\"form-control selectpicker\"\" id=\"lr_type\" name=\"lr_type\">\n		                	<option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.LRTypeKey : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		                </select>\n		             </div>\n		             <div class=\"project_lr_no_right\">\n						<div class=\"project_lr_no\"> <input class=\"form-control\" type=\"text\" id=\"lr_no\" name=\"lr_no\" placeholder=\"No.\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.LR_NO : stack1), depth0))
    + "\"/></div>\n						<div class=\"project_lr_no_del\"> <i class=\"icon icon-remove\" name=\"delLR\"></i></div>\n					</div>\n            	</dt>\n            </td>\n        </tr>\n         -->\n        <tr>\n            <td>Description</td>\n            <td> \n                <textarea class=\"form-control\" placeholder=\"Description...\" name=\"schedule_detail\" id=\"schedule_detail\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_DETAIL : stack1), depth0))
    + "</textarea>\n            </td>\n        </tr>\n        \n        <!--\n        <tr>\n            <td>Invoice</td>\n            <td>\n        		<dl id=\"showInvoice\">\n        			<dd><i class=\"icon icon-add\" name=\"addInvoice\" ></i></dd>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SUBINVOICE : stack1),{"name":"each","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        		</dl>\n            </td>\n        </tr>\n        -->\n        \n        <tr>\n            <td>Document</td>\n            <td>\n        		\n        			<div style=\"float:left;min-width:80%;\">\n        				<dl id=\"showFile\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SUBFILE : stack1),{"name":"each","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	        			</dl>	\n	        		</div>\n	        		\n	        		<div style=\"float:right;max-width:15%;\">\n	        			<i class=\"icon icon-upload\" name=\"uploadSOP\" ></i>\n	        		</div>\n        		\n            </td>\n        </tr>\n    </tbody>\n</table>\n</div>";
},"useData":true,"useDepths":true});
templates['addTaskLog'] = template({"1":function(depth0,helpers,partials,data) {
    return "							<option value=\"1\">Normal0</option>\n							<option value=\"0\" selected=\"selected\">Replay0</option>\n";
},"3":function(depth0,helpers,partials,data) {
    return "							<option value=\"1\" selected=\"selected\">Normal</option>\n							<option value=\"0\">Replay</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<!--<div class=\"add-replay\"> -->\n<!-- <fieldset> -->\n	<legend id=\"showAddReplayUserInfo\">"
    + alias3(((helper = (helper = helpers.ASSEMPLOYNAME || (depth0 != null ? depth0.ASSEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ASSEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "</legend>\n	<!-- <form id=\"addform\" action=\"\"> -->\n		<input type=\"hidden\" id=\"schedule_id\" name=\"schedule_id\"  value=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n		<input type=\"hidden\" id=\"sch_replay_id\" name=\"sch_replay_id\"  value=\""
    + alias3(((helper = (helper = helpers.SCH_REPLAY_ID || (depth0 != null ? depth0.SCH_REPLAY_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n		<input type=\"hidden\" name=\"employe_name\" id=\"employe_name\" value=\""
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "\"/>\n		<input type=\"hidden\" name=\"adid\" id=\"adid\" value=\""
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\"/>\n		<input type=\"hidden\" name=\"sch_state\" id=\"sch_state\" value=\""
    + alias3(((helper = (helper = helpers.SCH_STATE || (depth0 != null ? depth0.SCH_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_STATE","hash":{},"data":data}) : helper)))
    + "\"/>\n		<!-- <input type=\"hidden\" name=\"schedule_sub_id\" id=\"schedule_sub_id\" value=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_SUB_ID || (depth0 != null ? depth0.SCHEDULE_SUB_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_SUB_ID","hash":{},"data":data}) : helper)))
    + "\"/> -->\n		<!-- <input type=\"hidden\" name=\"total_state\" id=\"total_state\" value=\""
    + alias3(((helper = (helper = helpers.TOTAL_STATE || (depth0 != null ? depth0.TOTAL_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TOTAL_STATE","hash":{},"data":data}) : helper)))
    + "\"/> -->\n		<table class=\"table\">\n			<tr>\n				<td class=\"info\">Remark Type &nbsp;</td>\n				<td width=\"80%\">\n					<select class=\"form-control\" name=\"sch_replay_type\" id=\"sch_replay_type\" style=\"float:left;\">\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.SCH_REPLAY_TYPE == 0",{"name":"xif","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "					</select>\n				</td>\n			</tr>\n			<tr>\n				<td  class=\"info\"><span id=\"contentName\">Remark </span>&nbsp;</td>\n				<td>\n					<textarea  class=\"form-control\" style=\"height:232px;width:445px\" id=\"sch_replay_context\" name=\"sch_replay_context\">"
    + alias3(((helper = (helper = helpers.SCH_REPLAY_CONTEXT || (depth0 != null ? depth0.SCH_REPLAY_CONTEXT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_CONTEXT","hash":{},"data":data}) : helper)))
    + "</textarea>\n				</td>\n			</tr>\n			<tr>\n				<td  class=\"info\">Submitter &nbsp;</td>\n				<td><span style=\"color:green;\">"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "&nbsp;</span></td>\n			</tr>\n		</table>\n	<!-- </form> -->\n	<!-- </fieldset> -->\n<!-- </div>";
},"useData":true});
templates['downloadSOP'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<tr>\n				<td>\n					<span name=\"fileId\">\n	        			<a href=\"/Sync10/_fileserv/file/"
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" download=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" data-gallery=\"\">\n	        				"
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\n	        			</a> &nbsp;\n	        		</span>\n	        	</td>\n				<td class=\"text-center\">\n					<a href=\"/Sync10/_fileserv/file/"
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"glyphicon glyphicon-download-alt\"title=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" download=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\"></a>\n				</td>\n			</tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "		    <tr>\n		      <td class=\"no-data\" colspan=\"6\">NO Data</td>\n		    </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"download-content\">\n	<table class=\"table table-striped\">\n	<thead>\n		<tr>\n			<th>File Name</th>\n			<th>Operation</th>\n		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,depth0,{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,depth0,{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</tbody>\n	</table>\n</div>";
},"useData":true});
templates['initDeptInfo'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		<tr>\n			<td class=\"left\">\n				<span class=\"span_checkBox\" style=\"margin-top:-4px;\">\n					<input type=\"checkbox\" class=\"J-adgid_check_checked\" target=\"ul_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" id=\"adgid_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias3(((helper = (helper = helpers.deptId || (depth0 != null ? depth0.deptId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"deptId","hash":{},"data":data}) : helper)))
    + "\"/>\n				</span>&nbsp;\n				<label class=\"span_name\" for=\"adgid_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.deptName || (depth0 != null ? depth0.deptName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"deptName","hash":{},"data":data}) : helper)))
    + "</label>\n			</td>\n			<td>\n				<ul class=\"myul\" id=\"ul_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" target=\"adgid_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.values : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</ul>\n			</td>\n		</tr>\n";
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depths[2] != null ? depths[2].admin_id : depths[2]),(depth0 != null ? depth0.ADID : depth0),"!=",{"name":"xifCond","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"3":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<li><span class=\"span_checkBox\"><input type=\"checkbox\" class=\"J-user_check_checked\" name=\"user_"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "\" id=\"user_"
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\"  "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.checked : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  value=\""
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\"/></span>&nbsp;\n									<label class=\"span_name\" for=\"user_"
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "</label>\n								</li>\n";
},"4":function(depth0,helpers,partials,data) {
    return "checked=\"checked\" ";
},"6":function(depth0,helpers,partials,data) {
    return "		<tr>\n			<td class=\"no-data\" style=\"height: 180px;\" colspan=\"2\">NO Data</td>\n		</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<table class=\"mytable\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"unless","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</table>\n";
},"useData":true,"useDepths":true});
templates['invoiceBrowse'] = template({"1":function(depth0,helpers,partials,data) {
    return "  <button type=\"button\" class=\"btn btn-primary\">Save</button>\n";
},"3":function(depth0,helpers,partials,data) {
    return "  <button type=\"button\" class=\"btn btn-default J-print\">PDF</button>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return this.escapeExpression(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)));
},"7":function(depth0,helpers,partials,data) {
    return "XXXXXX";
},"9":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "      <tr>\n        <td style=\"border-right-width:0; border-left: 2px solid #333;\">"
    + alias1(this.lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</td>\n        <td style=\"border-left-width: 0; \">\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 1",{"name":"xif","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 2",{"name":"xif","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 3",{"name":"xif","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 4",{"name":"xif","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 5",{"name":"xif","hash":{},"fn":this.program(18, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 6",{"name":"xif","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 7",{"name":"xif","hash":{},"fn":this.program(22, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 8",{"name":"xif","hash":{},"fn":this.program(24, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 9",{"name":"xif","hash":{},"fn":this.program(26, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 10",{"name":"xif","hash":{},"fn":this.program(28, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 11",{"name":"xif","hash":{},"fn":this.program(30, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 12",{"name":"xif","hash":{},"fn":this.program(32, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 13",{"name":"xif","hash":{},"fn":this.program(34, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 14",{"name":"xif","hash":{},"fn":this.program(36, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n          "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.UNIT_PRICE == 15",{"name":"xif","hash":{},"fn":this.program(38, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n        </td>\n        <td><div style=\" max-width: 450px; word-break: break-all; text-align: left;\">"
    + alias1(((helper = (helper = helpers.DESCRIPTION || (depth0 != null ? depth0.DESCRIPTION : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"DESCRIPTION","hash":{},"data":data}) : helper)))
    + "</div></td>\n        <td>"
    + alias1(((helper = (helper = helpers.RATE || (depth0 != null ? depth0.RATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"RATE","hash":{},"data":data}) : helper)))
    + "</td>\n        <td>"
    + alias1(((helper = (helper = helpers.AMOUNT || (depth0 != null ? depth0.AMOUNT : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"AMOUNT","hash":{},"data":data}) : helper)))
    + "</td>\n      </tr>\n";
},"10":function(depth0,helpers,partials,data) {
    return "Squara Feet";
},"12":function(depth0,helpers,partials,data) {
    return "Cubic Feet";
},"14":function(depth0,helpers,partials,data) {
    return "CWT";
},"16":function(depth0,helpers,partials,data) {
    return "Long Ton";
},"18":function(depth0,helpers,partials,data) {
    return "Short Ton";
},"20":function(depth0,helpers,partials,data) {
    return "Metric Ton";
},"22":function(depth0,helpers,partials,data) {
    return "Invoice";
},"24":function(depth0,helpers,partials,data) {
    return "Package";
},"26":function(depth0,helpers,partials,data) {
    return "Pallet";
},"28":function(depth0,helpers,partials,data) {
    return "Piece";
},"30":function(depth0,helpers,partials,data) {
    return "Receipt";
},"32":function(depth0,helpers,partials,data) {
    return "Release";
},"34":function(depth0,helpers,partials,data) {
    return "Period";
},"36":function(depth0,helpers,partials,data) {
    return "Day";
},"38":function(depth0,helpers,partials,data) {
    return "Hour";
},"40":function(depth0,helpers,partials,data) {
    var helper;

  return this.escapeExpression(((helper = (helper = helpers.TOTAL || (depth0 != null ? depth0.TOTAL : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"TOTAL","hash":{},"data":data}) : helper)))
    + "\n";
},"42":function(depth0,helpers,partials,data) {
    var helper;

  return "            "
    + this.escapeExpression(((helper = (helper = helpers.SAVE || (depth0 != null ? depth0.SAVE : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"SAVE","hash":{},"data":data}) : helper)))
    + "\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div style=\"text-align: right; margin-top: 20px;margin-bottom: 20px;\">\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.noBtn : depth0),{"name":"unless","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.noBtn : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n<div id=\"browseTable_container\">\n  <table id=\"browseTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" style=\"width:740px; margin:10px auto;\">\n    <style type=\"text/css\" media=\"screen\">\n      * {\n        -webkit-box-sizing: border-box;\n        -moz-box-sizing: border-box;\n        box-sizing: border-box;\n      }\n      :before, :after{\n        -webkit-box-sizing: border-box;\n        -moz-box-sizing: border-box;\n        box-sizing: border-box;\n      }\n      .clearfix:before, .clearfix:after{\n        display: table;\n        content: \" \";\n      }\n      .clearfix:after{\n        clear: both;\n      }\n      .clearfix .col-md-5, .clearfix .col-md-7{\n        float: left;\n      }\n      .clearfix .col-md-5{\n        width: 42%;\n        font-weight: bold;\n      }\n      .clearfix .col-md-7{\n        width: 58%;\n        text-align: left;\n      }\n      label {\n        display: inline-block;\n        max-width: 100%;\n        margin-bottom: 5px;\n        font-weight: 700;\n      }\n      p {\n        line-height: 1.6em;\n        margin: 0;\n      }\n      .dl-horizontal dt {\n        float: left;\n        width: 160px;\n        overflow: hidden;\n        clear: left;\n        text-align: right;\n        text-overflow: ellipsis;\n        white-space: nowrap;\n      }\n      .dl-horizontal dd{\n        text-align: left;\n      }\n      #browseTable{\n        font-family: \"Helvetica Neue\",Helvetica,Arial,sans-serif;\n        font-size: 12px;\n        line-height: 1.6;\n        color: #333;\n        background-color: #fff;\n      }\n      #browseTable dd, #browseTable dt{\n        line-height: 1.6em;\n      }\n      #browseTable td, #browseTable th{\n        border-right: 2px solid #333;\n        border-bottom: 2px solid #333;\n      }\n      #browseTable thead td{\n        border-top: 2px solid #333;\n      }\n      #browseTable tbody td{\n        border-bottom-width: 0;\n      }\n      #browseTable .header{\n        font-weight: bold;\n      }\n      #browseTable th, #browseTable td{\n        padding: 5px;\n      }\n      #browseTable th{\n        text-align: center;\n      }\n      #browseTable td{\n        text-align: right;\n      }\n      #browseTable tfoot label{\n        padding-left: 10px;\n        margin-bottom: 0;\n        white-space: nowrap;\n      }\n      #browseTable .caption{\n        text-align: center;\n        text-shadow: #f3f3f3 0px 0px 1px, #D1D1D1 0px 0px 1px;\n        color: #333;\n        border-width: 0;\n      }\n      #browseTable .caption p{\n        font-weight: bold;\n        line-height: 1.6em;\n      }\n      #browseTable .caption label{\n        font-size: 1.2em;\n      }\n      #browseTable tfoot .col-md-5{\n        text-align: center;\n        letter-spacing: 1px;\n      }\n      #browseTable tfoot .col-md-5 p{\n        font-weight: bold;\n      }\n      #browseTable .bill-to .dl-horizontal.first, #browseTable .bill-to .dl-horizontal.last{\n        float: left;\n        padding: 0 15px;\n        margin: 0;\n      }\n      #browseTable .bill-to .dl-horizontal.first{\n        width: 58%;\n      }\n      #browseTable .bill-to .dl-horizontal.last{\n        width: 42%;\n      }\n      #browseTable .bill-to .dl-horizontal.first dt{\n        width: 70px;\n        font-weight:bold;\n      }\n      #browseTable .bill-to .dl-horizontal.first dd{\n        margin-left: 75px\n      }\n      #browseTable .bill-to .dl-horizontal.last dt{\n        width: 105px;\n        font-weight:bold;\n      }\n      #browseTable .bill-to .dl-horizontal.last dd{\n        margin-left: 110px\n      }\n    </style>\n    <thead>\n      <tr>\n        <td colspan=\"5\" class=\"caption\">\n          <label>Logistics Team</label>\n          <p>Location: (Valley) 15930 Valley Blvd. City of Industry CA 91744</p>\n          <p>TEL: 909-598-7268 FAX: 909-598-7216</p>\n          <p style=\"margin-top: 10px;\">INVOICE</p>\n        </td>\n      </tr>\n      <tr>\n        <td colspan=\"5\" class=\"bill-to\" style=\"border-left: 2px solid #333;\">\n          <div class=\"content clearfix\">\n            <dl class=\"dl-horizontal first\">\n              <dt>Bill To:</dt>\n              <dd>"
    + alias3(((helper = (helper = helpers.ADDRESS_NAME || (depth0 != null ? depth0.ADDRESS_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_NAME","hash":{},"data":data}) : helper)))
    + "</dd>\n              <dt>Address:</dt>\n              <dd>\n              "
    + alias3(((helper = (helper = helpers.ADDRESS_STREET || (depth0 != null ? depth0.ADDRESS_STREET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STREET","hash":{},"data":data}) : helper)))
    + "<br/>\n              "
    + alias3(((helper = (helper = helpers.ADDRESS_CITY || (depth0 != null ? depth0.ADDRESS_CITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_CITY","hash":{},"data":data}) : helper)))
    + ", "
    + alias3(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + " "
    + alias3(((helper = (helper = helpers.ADDRESS_ZIP || (depth0 != null ? depth0.ADDRESS_ZIP : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_ZIP","hash":{},"data":data}) : helper)))
    + "\n              </dd>\n              <dt>Attention:</dt>\n              <dd>"
    + alias3(((helper = (helper = helpers.ATTENTION || (depth0 != null ? depth0.ATTENTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ATTENTION","hash":{},"data":data}) : helper)))
    + "</dd>\n              <dt>Tel:</dt>\n              <dd>"
    + alias3(((helper = (helper = helpers.TEL || (depth0 != null ? depth0.TEL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TEL","hash":{},"data":data}) : helper)))
    + "</dd>\n              <dt>Fax:</dt>\n              <dd>"
    + alias3(((helper = (helper = helpers.FAX || (depth0 != null ? depth0.FAX : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FAX","hash":{},"data":data}) : helper)))
    + "</dd>\n              <dt>Note:</dt>\n              <dd></dd>\n            </dl><dl class=\"dl-horizontal last\">\n            <dt>Date:</dt>\n            <dd>"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\n            <dt>Invoice No.:</dt>\n            <dd>"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.BILL_ID : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "</dd>\n            <dt>Payment Term:</dt>\n            <dd style=\"margin-bottom: 40px;\">"
    + alias3(((helper = (helper = helpers.PAYMENT_TERM || (depth0 != null ? depth0.PAYMENT_TERM : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PAYMENT_TERM","hash":{},"data":data}) : helper)))
    + "</dd>\n            <dt>Reference No.:</dt>\n            <dd style=\"word-break: break-word;\">"
    + alias3(((helper = (helper = helpers.REFERENCE_NO || (depth0 != null ? depth0.REFERENCE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"REFERENCE_NO","hash":{},"data":data}) : helper)))
    + "</dd>\n          </dl>\n        </div></td>\n      </tr>\n      <tr class=\"header\">\n        <th colspan=\"2\" style=\"border-left: 2px solid #333; width: 25%;\">Quantity</th>\n        <th style=\"width:50%;\">Description</th>\n        <th>Rate</th>\n        <th>Amount</th>\n      </tr>\n    </thead>\n    <tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.BILL_DETAIL : depth0),{"name":"each","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </tbody>\n    <tfoot>\n      <tr>\n        <td colspan=\"2\" style=\"border-right-width:0; border-left: 2px solid #333; border-top: 2px solid #333;\"></td>\n        <td colspan=\"3\" style=\"border-left-width: 0; border-top: 2px solid #333;text-aglin:right;\">\n            <label>Total Amount Due =>&nbsp;&nbsp;</label>\n            \n            <span style=\"margin-left: 20px; display: inline-block; width:120px;\">$ \n            "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.TOTAL : depth0),{"name":"if","hash":{},"fn":this.program(40, data, 0),"inverse":this.program(42, data, 0),"data":data})) != null ? stack1 : "")
    + "            </span>\n        </td>\n      </tr>\n      <tr>\n        <td colspan=\"5\" style=\"border-left: 2px solid #333;\">\n          <div class=\"clearfix\">\n            <div class=\"col-md-5\">\n              <small>Please remit payment to</small>\n              <p>Logistics Team</p>\n              <p>218Machlin Ct.#A</p>\n              <p>Walnut,CA91789</p>\n              <p>Attn:Accounting Department</p>\n            </div>\n            <div class=\"col-md-7\">\n              5% late service fee per month will be added to amounts past due. if the account is turned over for collection, the prevailing party shall be entitled to obtain reasonable attorneys fees and costs.\n            </div>\n          </div>\n        </td>\n      </tr>\n    </tfoot>\n  </table>    \n</div>";
},"useData":true});
templates['invoiceDetail'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	    		<option value=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depths[1] != null ? depths[1].LR_TYPE : depths[1]),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"2":function(depth0,helpers,partials,data) {
    return " selected='selected' ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div style=\"min-height:450px;\">\n<div class=\"text-left clearfix\">\n	<div class=\"lr_no_left\">LR No.</div>\n	<div class=\"lr_no_left\">\n		<select class=\"form-control selectpicker\"\" id=\"lr_type\" name=\"lr_type\">\n	    	<option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.LRTypeKey : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	    </select>\n	 </div>\n	 <div class=\"lr_no_right\">\n		<div class=\"lr_no\"> <input class=\"form-control\" type=\"text\" id=\"lr_no\" name=\"lr_no\" placeholder=\"No.\" value=\""
    + alias3(((helper = (helper = helpers.LR_NO || (depth0 != null ? depth0.LR_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"LR_NO","hash":{},"data":data}) : helper)))
    + "\"/></div>\n	</div>\n</div>\n<table class=\"table table-bordered\">\n    <thead>\n        <tr class=\"success\">\n            <th>Line</th>\n            <th>Service ID</th>\n            <th>Description</th>\n            <th>Unit</th>\n            <th>Quantity</th>\n            <th>Rate</th>\n            <th>Amount</th>\n            <th><i class=\"icon icon-add\"></i></th>\n        </tr>\n    </thead>\n    <tbody>\n    </tbody>\n    <tfoot>\n        <tr>\n            <td colspan=\"8\" class=\"text-right\">\n                Total&nbsp;:<span class=\"text-left J-total\">"
    + alias3(((helper = (helper = helpers.SAVE || (depth0 != null ? depth0.SAVE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SAVE","hash":{},"data":data}) : helper)))
    + "</span>\n            </td>\n        </tr>\n    </tfoot>\n</table>\n</div>\n<div class=\"clearfix mt20\">\n    <div class=\"pull-left\">\n        <span>Currency&nbsp;:</span>USD\n        <!--<select class=\"form-control\" id=\"sel_cur\"></select>-->\n    </div>\n    <div class=\"pull-right\">\n        <!-- <button type=\"button\" class=\"btn btn-primary\" id=\"goToSave\">Save/Preview</button> --><button type=\"button\" class=\"btn btn-primary ml10\" id=\"goToBrowse\">Save</button>\n    </div>\n</div>";
},"useData":true,"useDepths":true});
templates['invoiceList'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "<tr>\n    <td>\n        <fieldset>\n            <legend>\n                <span class=\"fc-green J-bill-id\">"
    + alias1(this.lambda((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "&nbsp;&nbsp;"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\n            </legend>\n            <ul class=\"list-unstyled\" style=\"margin-bottom:0;\">\n                <li><span>Reference No&nbsp;:</span>\n"
    + ((stack1 = helpers.each.call(depth0,(depths[1] != null ? depths[1].LRTypeKey : depths[1]),{"name":"each","hash":{},"fn":this.program(9, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " &nbsp;\n              </li>\n              <li><span>Invoice Amount&nbsp;:</span>$&nbsp;"
    + alias1(((helper = (helper = helpers.SAVE || (depth0 != null ? depth0.SAVE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SAVE","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\n              <li><span>User&nbsp;:</span>"
    + alias1(((helper = (helper = helpers.ADMIN_NAME || (depth0 != null ? depth0.ADMIN_NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADMIN_NAME","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\n              <li><span>Create Time&nbsp;:</span>"
    + alias1(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\n          </ul>\n      </fieldset>\n  </td>\n  <td>\n    <dl class=\"dl-horizontal\">\n        <dt>Client ID&nbsp;:&nbsp;</dt>\n        <dd class=\"bold\">"
    + alias1(((helper = (helper = helpers.CUSTOMER_ID || (depth0 != null ? depth0.CUSTOMER_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"CUSTOMER_ID","hash":{},"data":data}) : helper)))
    + "</dd>\n        <dt>Customer Name&nbsp;:&nbsp;</dt>\n        <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_NAME || (depth0 != null ? depth0.ADDRESS_NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_NAME","hash":{},"data":data}) : helper)))
    + "</dd>\n        <dt>Payment Term&nbsp;:&nbsp;</dt>\n        <dd>"
    + alias1(((helper = (helper = helpers.PAYMENT_TERM || (depth0 != null ? depth0.PAYMENT_TERM : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"PAYMENT_TERM","hash":{},"data":data}) : helper)))
    + "</dd>\n        <dt>Street&nbsp;:&nbsp;</dt>\n        <dd>\n          "
    + alias1(((helper = (helper = helpers.ADDRESS_STREET || (depth0 != null ? depth0.ADDRESS_STREET : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_STREET","hash":{},"data":data}) : helper)))
    + "<br/>\n          "
    + alias1(((helper = (helper = helpers.ADDRESS_CITY || (depth0 != null ? depth0.ADDRESS_CITY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_CITY","hash":{},"data":data}) : helper)))
    + ", "
    + alias1(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + " "
    + alias1(((helper = (helper = helpers.ADDRESS_ZIP || (depth0 != null ? depth0.ADDRESS_ZIP : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_ZIP","hash":{},"data":data}) : helper)))
    + "\n      </dd>\n\n      <dt>Country&nbsp;:&nbsp;</dt>\n      <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_COUNTRY || (depth0 != null ? depth0.ADDRESS_COUNTRY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_COUNTRY","hash":{},"data":data}) : helper)))
    + "</dd>\n      <dt>Attention&nbsp;:&nbsp;</dt>\n      <dd>"
    + alias1(((helper = (helper = helpers.ATTENTION || (depth0 != null ? depth0.ATTENTION : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ATTENTION","hash":{},"data":data}) : helper)))
    + "</dd>\n      <dt>Payer Email&nbsp;:&nbsp;</dt>\n      <dd>"
    + alias1(((helper = (helper = helpers.PAYER_EMAIL || (depth0 != null ? depth0.PAYER_EMAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"PAYER_EMAIL","hash":{},"data":data}) : helper)))
    + "</dd>\n      \n      <dt>Tel&nbsp;:&nbsp;</dt>\n      <dd>"
    + alias1(((helper = (helper = helpers.TEL || (depth0 != null ? depth0.TEL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TEL","hash":{},"data":data}) : helper)))
    + "</dd>\n      <dt>Fax&nbsp;:&nbsp;</dt>\n      <dd>"
    + alias1(((helper = (helper = helpers.FAX || (depth0 != null ? depth0.FAX : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"FAX","hash":{},"data":data}) : helper)))
    + "</dd>\n\n  </dl>\n</td>\n<td>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.SCHEDULE : depth0),{"name":"unless","hash":{},"fn":this.program(20, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</td>\n<td>\n    <dl>\n        <dt>name&nbsp;\n            <a href=\"javascript:void(0);\" class=\"fc-green underline log-detail\">do something</a>\n        </dt>\n        <dd><small>06/09/15 18:04</small></dd>\n    </dl>\n</td>\n</tr>\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.noBtnGroup : depth0),{"name":"unless","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    return "IMPORTED";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    return "CLOSED";
},"7":function(depth0,helpers,partials,data) {
    return "OPEN";
},"9":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].LR_TYPE : depths[1]),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                  ";
},"10":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=this.escapeExpression;

  return "                  "
    + alias1(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "&nbsp;"
    + alias1(this.lambda((depths[2] != null ? depths[2].LR_NO : depths[2]), depth0))
    + "\n";
},"12":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <fieldset>\n        <legend>\n           <span>PROJECT</span> <a href=\"javascript:void(0);\" class=\"fc-green underline J-goto-task\" data-task=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "</a>\n       </legend>\n\n       <dl>\n        <div class=\"header\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBSCHEDULE : depth0)) != null ? stack1.length : stack1),4,">",{"name":"xifCond","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</dl>\n\n</fieldset>\n";
},"13":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),4,"<",{"name":"xifCond","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"14":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2=this.escapeExpression;

  return "      <dd>\n       "
    + alias2((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + ".&nbsp; "
    + alias2(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\n   </dd>\n";
},"16":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"17":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),3,">",{"name":"xifCond","hash":{},"fn":this.program(18, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"18":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2=this.escapeExpression;

  return "<dd>\n "
    + alias2((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + ".&nbsp; "
    + alias2(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\n</dd>\n";
},"20":function(depth0,helpers,partials,data) {
    return "<fieldset>\n    <legend>\n     <span>PROJECT</span>\n </legend>\n</fieldset>\n";
},"22":function(depth0,helpers,partials,data) {
    var stack1;

  return "<tr>\n    <td colspan=\"5\" class=\"text-right tr-btn\">\n        "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(23, data, 0),"inverse":this.program(25, data, 0),"data":data})) != null ? stack1 : "")
    + "    </td>\n</tr>\n";
},"23":function(depth0,helpers,partials,data) {
    return "";
},"25":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "\n        <div class=\"btn-group btn-group-sx\" role=\"group\">\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-edit\"><i class=\"glyphicon glyphicon-pencil\"> Edit</i></button>\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-del\"><i class=\"glyphicon glyphicon-trash\">  Del</i></button>\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-close\"><i class=\"glyphicon glyphicon-ban-circle\">  Close</i></button>\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-browse\"><i class=\"glyphicon glyphicon-eye-open\">  Preview</i></button>\n            <!--  <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-pdf\"><i class=\"glyphicon glyphicon-file\">  PDF</i></button> -->\n        </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['invoiceTemplate'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<fieldset>\n    <legend>\n        <span>Bill To</span>\n    </legend>\n    <dl class=\"dl-horizontal\">\n        <dt>Client ID&nbsp;:</dt>\n        <dd style=\"width: 196px;float: left;margin-left: 20px;\">\n            <div class=\"sel_client\">\n                <select id=\"sel_client\" class=\"form-control\"></select>\n                <input class=\"validate sel-input\" required id=\"client_id\" value=\""
    + alias3(((helper = (helper = helpers.CLIENT_ID || (depth0 != null ? depth0.CLIENT_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CLIENT_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n            </div>\n        </dd>\n        <dt style=\"clear: none;width: 138px;\">Payment Term&nbsp;:</dt>\n        <dd style=\"width: 100px;float: left;margin-left: 20px;\"><select id=\"paymentTerm\" class=\"form-control\">\n            <option value=\"COD\">COD</option>\n            <option value=\"N15\">N15</option>\n            <option value=\"NET30\">NET30</option>\n            <option value=\"NET40\">NET40</option>\n            <option value=\"NET45\">NET45</option>\n            <option value=\"NET60\">NET60</option>\n        </select></dd>\n        <dt>Customer Name&nbsp;:</dt>\n        <dd><input class=\"form-control\" id=\"name\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_NAME || (depth0 != null ? depth0.ADDRESS_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_NAME","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>Street&nbsp;: </dt>\n        <dd><input class=\"form-control\" id=\"street\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_STREET || (depth0 != null ? depth0.ADDRESS_STREET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STREET","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>City&nbsp;: </dt>\n        <dd><input class=\"form-control\" id=\"city\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_CITY || (depth0 != null ? depth0.ADDRESS_CITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_CITY","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>Zip&nbsp;: </dt>\n        <dd><input class=\"form-control\" id=\"zip\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_ZIP || (depth0 != null ? depth0.ADDRESS_ZIP : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_ZIP","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>Country&nbsp;:</dt>\n        <dd>\n            <div class=\"sel_country\">\n                <select id=\"sel_country\" class=\"form-control\"></select>\n                <input class=\"validate sel-input\" required id=\"sel_country_val\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_COUNTRY_CODE || (depth0 != null ? depth0.ADDRESS_COUNTRY_CODE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_COUNTRY_CODE","hash":{},"data":data}) : helper)))
    + "\"/>\n                <input type=\"hidden\" id=\"sel_country_ccid\" value=\""
    + alias3(((helper = (helper = helpers.CCID || (depth0 != null ? depth0.CCID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CCID","hash":{},"data":data}) : helper)))
    + "\" />\n                <input type=\"hidden\" id=\"sel_country_name\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_COUNTRY || (depth0 != null ? depth0.ADDRESS_COUNTRY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_COUNTRY","hash":{},"data":data}) : helper)))
    + "\" />\n            </div>\n        </dd>\n        <dt>State&nbsp;:</dt>\n        <dd>\n            <div class=\"sel_state\">\n                <select id=\"sel_state\" class=\"form-control\"></select>\n                <input class=\"validate sel-input\" required id=\"sel_state_val\" value=\""
    + alias3(((helper = (helper = helpers.PRO_ID || (depth0 != null ? depth0.PRO_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PRO_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n                <input type=\"hidden\" id=\"sel_state_name\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + "\" />\n            </div><input class=\"form-control validate\" id=\"state_input\" required value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + "\"/>\n        </dd>\n        <dt>Attention&nbsp;: </dt>\n        <dd><input class=\"form-control\" id=\"attention\" value=\""
    + alias3(((helper = (helper = helpers.ATTENTION || (depth0 != null ? depth0.ATTENTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ATTENTION","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>Payer Email&nbsp;: </dt>\n        <dd><input class=\"form-control validate\" validType=\"email\" id=\"email\" value=\""
    + alias3(((helper = (helper = helpers.PAYER_EMAIL || (depth0 != null ? depth0.PAYER_EMAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PAYER_EMAIL","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>Tel&nbsp;:</dt>\n        <dd><input class=\"form-control\" id=\"tel\" value=\""
    + alias3(((helper = (helper = helpers.TEL || (depth0 != null ? depth0.TEL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TEL","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n        <dt>Fax&nbsp;:</dt>\n        <dd><input class=\"form-control\" id=\"fax\" value=\""
    + alias3(((helper = (helper = helpers.FAX || (depth0 != null ? depth0.FAX : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FAX","hash":{},"data":data}) : helper)))
    + "\"/></dd>\n    </dl>\n</fieldset>\n<div class=\"text-right mt20\">\n    <button type=\"button\" class=\"btn btn-primary\" id=\"goToNext\">Next</button>\n</div>";
},"useData":true});
templates['invoice_addTask'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<fieldset>\n    <legend>\n        <a href=\"javascript:;\" class=\"fc-green underline\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "</a>\n        <i class=\"icon icon-remove J-remove-task\"></i>\n    </legend>\n    <dl>\n      <dt>"
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "</dt>\n      <dd class=\"ellipsis\">\n        <p>"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</p>\n        <div>"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</div>\n      </dd>\n    </dl>\n</fieldset>";
},"useData":true});
templates['logDetail'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<tr>\n				<td> "
    + alias3(((helper = (helper = helpers.POST_DATE || (depth0 != null ? depth0.POST_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"POST_DATE","hash":{},"data":data}) : helper)))
    + " </td>\n				<td> "
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + " </td>\n				<td> "
    + alias3(((helper = (helper = helpers.OPERATION_CONTENT || (depth0 != null ? depth0.OPERATION_CONTENT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"OPERATION_CONTENT","hash":{},"data":data}) : helper)))
    + " </td>\n				<td> "
    + alias3(((helper = (helper = helpers.TYPE || (depth0 != null ? depth0.TYPE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TYPE","hash":{},"data":data}) : helper)))
    + " </td>\n				<!-- <td> "
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + " </td> -->\n				<td>"
    + alias3(((helper = (helper = helpers.OPERATION_NOTE || (depth0 != null ? depth0.OPERATION_NOTE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"OPERATION_NOTE","hash":{},"data":data}) : helper)))
    + " </td>\n			</tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "		    <tr>\n		      <td class=\"no-data\" colspan=\"5\">NO Data</td>\n		    </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"download-content\">\n	<table class=\"table table-striped\">\n	<thead>\n		<tr>\n			<th>Date</th>\n			<th>Actor</th>\n			<th>Action</th>\n			<th>Object</th>\n			<!-- <th>ID</th> -->\n			<th>Name</th>\n		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,depth0,{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,depth0,{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</tbody>\n	</table>\n</div>";
},"useData":true});
templates['mainInvoice'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"task-invoice\">\n    <div role=\"tabpanel\" class=\"panel panel-default\">\n        <div class=\"panel-heading\"><span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\">&nbsp;Invoice</span></div>\n        <div class=\"panel-body\">\n            <!-- Nav tabs -->\n            <ul class=\"nav nav-tabs\" role=\"tablist\">\n                <li role=\"presentation\" class=\"active\"><a href=\"#common\" aria-controls=\"common\" role=\"tab\" data-toggle=\"tab\">Common Tools</a></li>\n                <li role=\"presentation\"><a href=\"#advanced\" aria-controls=\"advanced\" role=\"tab\" data-toggle=\"tab\">Advanced Search</a></li>\n            </ul>\n\n            <!-- Tab panes -->\n            <div class=\"tab-content\">\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"common\">\n                    <div class=\"col-md-4\">\n                        <div class=\"input-group\">\n                            <input type=\"text\" class=\"form-control J-search-common\" placeholder=\"Search for...\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.query || (depth0 != null ? depth0.query : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"query","hash":{},"data":data}) : helper)))
    + "\">\n                            <span class=\"input-group-btn\">\n                                <button class=\"btn btn-info J-btn-search\" type=\"button\"><span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span></button>\n                            </span>\n                        </div>\n                    </div>\n                    <div class=\"col-md-4\"></div>\n                    <div class=\"col-md-4 text-center\">\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_invoice\">Add Invoice</button>\n                    </div>\n                </div>\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\n                    <!-- todo -->\n                </div>\n            </div>\n        </div>\n        <div class=\"modal-container\">\n            <table class=\"table table-hover table-condensed\" id=\"invoice_table\">\n                <thead>\n                <tr>\n                    <th>Invoice Information</th>\n                    <th>Customer Information</th>\n                    <th>Relation</th>\n                    <th>Log</th>\n                </tr>\n                </thead>\n                <tbody>\n                </tbody>\n            </table>\n            <div class=\"modal\"><div class=\"loading\"></div></div>\n        </div>\n        <ul id=\"pagebox_invoice\" class='clearfix pagebox mb20 mt20'></ul>\n    </div>\n</div>";
},"useData":true});
templates['mainTask'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"task-invoice\">\n    <div role=\"tabpanel\" class=\"panel panel-default\">\n        <div class=\"panel-heading\"><span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\">&nbsp;Task</span></div>\n        <div class=\"panel-body\">\n            <!-- Nav tabs -->\n            <ul class=\"nav nav-tabs\" role=\"tablist\">\n                <li role=\"presentation\" class=\"active\"><a href=\"#common\" aria-controls=\"common\" role=\"tab\" data-toggle=\"tab\">Common Tools</a></li>\n                <li role=\"presentation\"><a href=\"#advanced\" aria-controls=\"advanced\" role=\"tab\" data-toggle=\"tab\">Advanced Search</a></li>\n            </ul>\n\n            <!-- Tab panes -->\n            <div class=\"tab-content\">\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"common\">\n                    <div class=\"col-md-4\">\n                        <div class=\"input-group\">\n                            <input type=\"text\" class=\"form-control J-search-common\" placeholder=\"Search for...\" value=\""
    + alias3(((helper = (helper = helpers.query || (depth0 != null ? depth0.query : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"query","hash":{},"data":data}) : helper)))
    + "\">\n                            <span class=\"input-group-btn\">\n                                <button class=\"btn btn-info J-btn-search\" type=\"button\"><span class=\"glyphicon glyphicon-search J-btn-search\" aria-hidden=\"true\"></span></button>\n                            </span>\n                        </div>\n                    </div>\n                    <div class=\"col-md-4\"></div>\n                    <div class=\"col-md-4 text-center\">\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_task\">Add Project</button>\n                    </div>\n                </div>\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\n                    <div class=\"col-md-9\">\n                    	 <div class=\"div-form-group\">\n							<span>Time:</span>	\n    						<input class=\"form_datetime form-control inconHand bgiconurl_\" type=\"text\" id=\"j_start_time\" value=\""
    + alias3(((helper = (helper = helpers.start_time || (depth0 != null ? depth0.start_time : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"start_time","hash":{},"data":data}) : helper)))
    + "\" size=\"20\" readonly=\"readonly\" placeholder=\"start time\">\n							<span>To</span>			  			\n						</div>\n			  			<div class= \"div-form-group\">\n    						<input class=\"form_datetime form-control inconHand bgiconurl_\" type=\"text\" id=\"j_end_time\" value=\""
    + alias3(((helper = (helper = helpers.end_time || (depth0 != null ? depth0.end_time : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"end_time","hash":{},"data":data}) : helper)))
    + "\" size=\"20\" readonly=\"readonly\" placeholder=\"end time\">\n			  			</div>\n						<div class=\"div-form-group\">\n							<span>Project Status:</span>\n    						<select class=\"form-control selectpicker\"\" id=\"j_task_status\">\n								<option value=\"-1\">ALL</option>\n		                		<option value=\"1\">Pending</option>\n		                		<option value=\"4\">Open</option>\n		                		<option value=\"2\">Processing</option>\n		                		<option value=\"3\">Closed</option>\n		                	</select>\n			  			</div>\n                    </div>\n					<div class=\"col-md-2\">\n						<button class=\"btn btn-primary J-advanced-search\"><i class=\"glyphicon glyphicon-search\"></i>&nbsp;&nbsp;Search</button>\n                   </div>\n				</div>\n            </div>\n        </div>\n        <div class=\"modal-container\">\n            <table class=\"table table-hover table-condensed\" id=\"task_table\">\n                <thead>\n                <tr>\n                    <th>Project Information</th>\n                    <th>Content</th>\n                    <th>Task</th>\n                    <th>Invoice</th>\n                    <th>Log</th>\n                </tr>\n                </thead>\n                <tbody>\n                </tbody>\n            </table>\n            <div class=\"modal\"><div class=\"loading\"></div></div>\n        </div>\n        <ul id=\"pagebox_task\" class='clearfix pagebox mb20 mt20'></ul>\n    </div>\n</div>";
},"useData":true});
templates['newAddSubTask'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "<tr "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isEdit : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " data-task="
    + alias1(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + " data-status=\""
    + alias1(((helper = (helper = helpers.TASK_STATUS || (depth0 != null ? depth0.TASK_STATUS : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TASK_STATUS","hash":{},"data":data}) : helper)))
    + "\" id=\"sub_task_"
    + alias1(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">\n	<td>	\n		<span class=\"J-sub-lineNo\">"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.lineNo : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.program(6, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\n		<input type=\"hidden\" class=\"J-sub-Id\" value=\""
    + alias1(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n		<input type=\"hidden\" class=\"J-sub_number\" value=\""
    + alias1(((helper = (helper = helpers.TASK_NUMBER || (depth0 != null ? depth0.TASK_NUMBER : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TASK_NUMBER","hash":{},"data":data}) : helper)))
    + "\"/>\n	</td>\n	<td>\n		<span class=\"info\">"
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</span>\n		<input class=\"form-control edit-form J-sub_detail\" value=\""
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\"  placeholder=\"Subtask...\"/>\n	</td>\n	<td>\n		<span class=\"info\">"
    + alias1(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "</span>\n		<input class=\"form-control edit-form J-select-user\" value=\""
    + alias1(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"To...\"/>\n		<input type=\"hidden\" class=\"J-sub_execute_id\" value='"
    + alias1(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\n		<input type=\"hidden\" class=\"J-sub_deleteusers\" value='"
    + alias1(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\n	</td>\n	<td class=\"J-sub_status "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Open : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Pending : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Processing : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(14, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</td>\n<td>\n	<span class=\"operate-btns\">\n		"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(16, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"!=",{"name":"xifCond","hash":{},"fn":this.program(18, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</span>\n	<span class=\"editing-btn\">\n		<i class=\"fa fa-check-circle fc-green J-sub-ok\"></i>\n		<i class=\"fa fa-times-circle fc-red J-sub-cancel\"></i>\n	</span>\n</td>\n</tr>\n";
},"2":function(depth0,helpers,partials,data) {
    return "class=\"editing\"";
},"4":function(depth0,helpers,partials,data) {
    return "\n		"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.lineNo : depth0), depth0))
    + "\n";
},"6":function(depth0,helpers,partials,data) {
    return "		"
    + this.escapeExpression((helpers.math || (depth0 && depth0.math) || helpers.helperMissing).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + "\n		";
},"8":function(depth0,helpers,partials,data) {
    return " \n	 status-open\"> Open&nbsp;";
},"10":function(depth0,helpers,partials,data) {
    return "	\"> Pending&nbsp;\n";
},"12":function(depth0,helpers,partials,data) {
    return "	\"> Processing&nbsp;\n";
},"14":function(depth0,helpers,partials,data) {
    return "	 status-closed\"> Closed&nbsp;\n";
},"16":function(depth0,helpers,partials,data) {
    return "&nbsp;";
},"18":function(depth0,helpers,partials,data) {
    return "		<i class=\"icon icon-edit J-sub-edit\" title=\"edit\"></i>\n		<i class=\"icon icon-remove J-sub-remove\" title=\"del\"></i>\n		<i class=\"icon icon-close J-sub-close\" title=\"close\"></i> \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['newAddTask'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "						<select class=\"form-control\">\n							<option value=\"\"> </option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.invoices : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</select>";
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel\" id=\"newAddTask\">\n	<div class=\"panel-heading\">\n		<a class=\"panel-title collapsed editing\" style=\"  background-color: #fcf8e3;\">\n			<div>\n			</div>\n			<div>\n				<div>\n					<input class=\"form-control edit-form J-schedule_detail\" value=\"\"  placeholder=\"Task...\"/>\n				</div>\n			</div>\n			<div>\n			<div>\n				<input class=\"form-control edit-form J-select-user\" name=\"exe_employ_name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.EXEEMPLOYNAME : stack1), depth0))
    + "\" placeholder=\"To...\" readonly/>\n				<input type=\"hidden\" class=\"J-schedule_execute_id\" name=\"schedule_execute_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_EXECUTE_ID : stack1), depth0))
    + "'/>\n			</div>\n			</div>\n			<div>\n			<div>\n				<input class=\"form_datetime start-time form-control edit-form J-start_time\"  size=\"16\" type=\"text\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.START_TIME : stack1), depth0))
    + "\" readonly>\n			</div>\n			</div>\n			<div><div>\n				<input class=\"form_datetime end-time form-control edit-form J-end_time\"  size=\"16\" type=\"text\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.END_TIME : stack1), depth0))
    + "\" readonly>\n			</div></div>\n			<div> </div>\n			<div><div>\n				<span class=\"edit-form edit-invoice editing\">\n					<span class=\"sel-invoice\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.invoices : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "<i class=\"icon icon-add J-add-invoice\" title=\"add invoice\"></i>\n					</span>\n					<span class=\"selcted-invoice\">\n							<span></span><!--\n						--><i class=\"icon icon-remove J-del-invoice\" title=\"del\"></i>\n					</span>\n				</span>\n			</div>\n			</div>\n			<div>\n			<div>\n				<span class=\"editing-btn\">\n					<i class=\"fa fa-check-circle fc-green J-newAddTask\"></i>\n					<i class=\"fa fa-times-circle fc-red J-newAddTask-cancel\"></i>\n				</span>\n			</div></div>\n		</a>\n	</div>\n</div>";
},"useData":true});
templates['newInvoiceInTaskList'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<dd>\n	<fieldset class=\"invoice\">\n		<legend>\n			<a href=\"javascript:void(0);\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"fc-green underline J-goto-invoice\">&nbsp;"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;</a>\n			<span class=\"J-invoice-status\">"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "</span>\n		</legend>\n		<dl>\n			<dt>&nbsp;"
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + "</dt>\n			<dd><span>Create Time&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\n		</dl>\n	</fieldset>\n</dd>\n\n";
},"3":function(depth0,helpers,partials,data) {
    return "IMPORTED";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(6, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"6":function(depth0,helpers,partials,data) {
    return "CLOSED";
},"8":function(depth0,helpers,partials,data) {
    return "OPEN";
},"10":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"11":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression, alias4=this.lambda;

  return "<dd>\n	<fieldset class=\"invoice\">\n		<legend>\n			<a href=\"javascript:void(0);\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"fc-green underline J-goto-invoice\">&nbsp;"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;</a>\n			<span class=\"J-invoice-status\">"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\n			<!-- <i class=\"icon icon-remove\" title=\"del invoice\" name=\"delInvoice\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" data-task="
    + alias3(alias4((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "></i> -->\n		</legend>\n		<dl>\n			<dt><span>Reference No&nbsp;:</span>&nbsp;\n"
    + ((stack1 = helpers.each.call(depth0,(depths[1] != null ? depths[1].LRTypeKey : depths[1]),{"name":"each","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				<button class=\"btn btn-default btn-xs J-pdf\" data-invoice=\""
    + alias3(alias4((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" data-task=\""
    + alias3(alias4((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" type=\"button\">Preview</button></dt>\n				<dd><span>Create Time&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\n			</dl>\n		</fieldset>\n	</dd>\n\n";
},"12":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].LR_TYPE : depths[1]),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(13, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"13":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=this.escapeExpression;

  return "				"
    + alias1(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "&nbsp;"
    + alias1(this.lambda((depths[1] != null ? depths[1].LR_NO : depths[1]), depth0))
    + "\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.program(10, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['selectOptions'] = template({"1":function(depth0,helpers,partials,data) {
    return "";
},"3":function(depth0,helpers,partials,data) {
    return "<option value=\"\"></option>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "<option value=\"-1\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isHand : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">by-hand input</option>\n";
},"6":function(depth0,helpers,partials,data) {
    return "selected";
},"8":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.TEXT || (depth0 != null ? depth0.TEXT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TEXT","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"10":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option data-id=\""
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.ACCOUNT_NAME || (depth0 != null ? depth0.ACCOUNT_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT_NAME","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"12":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.C_CODE || (depth0 != null ? depth0.C_CODE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"C_CODE","hash":{},"data":data}) : helper)))
    + "\" data-ccid=\""
    + alias3(((helper = (helper = helpers.CCID || (depth0 != null ? depth0.CCID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CCID","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.C_COUNTRY || (depth0 != null ? depth0.C_COUNTRY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"C_COUNTRY","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.PRO_ID || (depth0 != null ? depth0.PRO_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PRO_ID","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.PRO_NAME || (depth0 != null ? depth0.PRO_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PRO_NAME","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"16":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option data-target=\""
    + alias3(((helper = (helper = helpers.TARGET || (depth0 != null ? depth0.TARGET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TARGET","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias3(((helper = (helper = helpers.VALUE || (depth0 != null ? depth0.VALUE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"VALUE","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.TARGET || (depth0 != null ? depth0.TARGET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TARGET","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"18":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.CUSTOMER_KEY || (depth0 != null ? depth0.CUSTOMER_KEY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CUSTOMER_KEY","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.CUSTOMER_ID || (depth0 != null ? depth0.CUSTOMER_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CUSTOMER_ID","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"20":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isProto : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isCanEnter : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.f : depth0),{"name":"each","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.p : depth0),{"name":"each","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.country : depth0),{"name":"each","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.s : depth0),{"name":"each","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.cur : depth0),{"name":"each","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.client : depth0),{"name":"each","hash":{},"fn":this.program(18, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.invoices : depth0),{"name":"each","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['selectUser'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "						<option value='"
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "' "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].admin : depths[1])) != null ? stack1.ps_id : stack1),(depth0 != null ? depth0.ps_id : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.ps_name || (depth0 != null ? depth0.ps_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"2":function(depth0,helpers,partials,data) {
    return " selected='selected' ";
},"4":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<option value=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].admin : depths[1])) != null ? stack1.dept_id : stack1),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"5":function(depth0,helpers,partials,data) {
    return " selected=\"selected\" ";
},"7":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "				<option value=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].admin : depths[1])) != null ? stack1.post_id : stack1),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"select-user\">\n	<div class=\"select-condition\">\n		WareHouse:<select name=\"ps_id\" id=\"ps_id\">admin\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.productCatelog : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		 		</select>\n				\n		Group:<select name=\"group\" id=\"group\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.groupJson : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</select>\n		User:<select name=\"proJsId\" id=\"proJsId\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.userJson : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				 \n			</select>\n		<div class=\"div-btn text-center\">\n	        <button class=\"btn btn-info J-user-search\" type=\"button\"><span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span></button>\n	    </div> \n    </div>\n    <div class=\"user-info\"><div>\n</div>\n";
},"useData":true,"useDepths":true});
templates['sortSubtasks'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	  	<a href=\"javascript:void(0);\" class=\"list-group-item J-sort-item\" data-schedule=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"><span class=\"J-note\">"
    + alias3((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + "</span>.&nbsp;"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"clearfix sort-sub\">\n	<div class=\"list-group pull-left\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n	<div class=\"pull-right\">\n		<div class=\"sort-btn\">\n			<button class=\"btn btn-default J-btn-up\"><i class='glyphicon glyphicon-arrow-up'></i></button>\n			<button class=\"btn btn-default J-btn-down\"><i class='glyphicon glyphicon-arrow-down'></i></button>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['sortedSubTasks'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),4,"<",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        <dd>\n            <div class=\"header\">\n               <a href=\"javascript:void(0);\" data-sub=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\" data-task=\""
    + alias3(this.lambda((depths[2] != null ? depths[2].SCHEDULE_ID : depths[2]), depth0))
    + "\" class=\"J-sub-detail\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "           	\">\n                "
    + alias3((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + ".&nbsp; "
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\n               </a>\n            </div>\n            \n        </dd>\n";
},"3":function(depth0,helpers,partials,data) {
    return "         			 fc-green   \n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(6, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"6":function(depth0,helpers,partials,data) {
    return "         				 fc-gray \n";
},"8":function(depth0,helpers,partials,data) {
    return "         				fc-red\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['subTaskDetail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"subtask-list\" id=\"subtask-list\">\n	<div class=\"panel-group\" id=\"accordionSubTaskList\" role=\"tablist\" aria-multiselectable=\"true\">\n		<div class=\"panel panel-info\">\n			<div class=\"panel-heading\">\n				<div class=\"panel-title collapsed\">\n					<div>\n						<label class=\"checkbox J-checkbox-all checked\">\n							<input type=\"checkbox\" checked />\n							<span class=\"text\"></span>\n						</label>\n					</div>\n					<div>Task</div>\n					<div>To</div>\n					<div>Start</div>\n					<div>End</div>\n					<div>Status</div>\n					<div>Invoice</div>\n					<div><i class=\"icon icon-add J-addTask\" title=\"add\"></i></div>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n";
},"useData":true});
templates['subTaskList'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing, alias4="function";

  return "<div class=\"panel\">\n	<div class=\"panel-heading\" role=\"tab\" id=\"heading_"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\">\n		<span class=\"panel-title collapsed\" data-toggle=\"collapse\" data-number=\""
    + alias2(alias1((depth0 != null ? depth0.TASK_NUMBER : depth0), depth0))
    + "\" data-parent=\"#accordionSubTaskList\" data-schedule="
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + " href=\"#collapse_"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\" aria-expanded=\"true\" aria-controls=\"collapse_"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\">\n			<div>\n				<div>\n					<label class=\"checkbox "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"!=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\">\n						<input type=\"checkbox\" value=\""
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\" name=\"check-tack\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"!=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(6, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + ">\n						<span class=\"text\"></span>\n					</label>\n				</div>\n			</div>\n			<div>\n				<div>\n					<span class=\"info\">"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_DETAIL : depth0), depth0))
    + "</span>\n					<input class=\"form-control edit-form J-schedule_detail\" value=\""
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_DETAIL : depth0), depth0))
    + "\" />\n				</div>\n			</div>\n			<div>\n				<div>\n					<span class=\"info\">"
    + alias2(alias1((depth0 != null ? depth0.EXEEMPLOYNAME : depth0), depth0))
    + "</span>\n					<input class=\"form-control edit-form J-select-user\" name=\"exe_employ_name\" value=\""
    + alias2(alias1((depth0 != null ? depth0.EXEEMPLOYNAME : depth0), depth0))
    + "\" readonly/>\n					<input type=\"hidden\" class=\"J-schedule_execute_id\" name=\"schedule_execute_id\" value='"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0), depth0))
    + "'/>\n					<input type=\"hidden\" class=\"J-deleteusers\" name=\"deleteusers\" value='"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0), depth0))
    + "'/>\n				</div>\n			</div>\n			<div>\n				<div>\n					<span class=\"info\">"
    + alias2(alias1((depth0 != null ? depth0.START_TIME : depth0), depth0))
    + "</span>\n					<input class=\"form_datetime form-control edit-form J-start_time\"  size=\"16\" type=\"text\" value=\""
    + alias2(alias1((depth0 != null ? depth0.START_TIME : depth0), depth0))
    + "\" readonly>\n				</div>\n			</div>\n			<div>\n				<div>\n					<span class=\"info\">"
    + alias2(alias1((depth0 != null ? depth0.END_TIME : depth0), depth0))
    + "</span>\n					<input class=\"form_datetime form-control edit-form J-end_time\"  size=\"16\" type=\"text\" value=\""
    + alias2(alias1((depth0 != null ? depth0.END_TIME : depth0), depth0))
    + "\" readonly>\n				</div>\n			</div>\n			<div>\n				<div class=\"J-task_status\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Open : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Pending : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Processing : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(14, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\n			</div>\n			<div>\n				<div>\n					<span class=\"info info-invoice\" style=\"color: #269ABC; font-weight: bold;\">"
    + alias2(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</span>\n				<span class=\"edit-form edit-invoice\">\n					<span class=\"sel-invoice\">\n						<select class=\"form-control\">\n							<option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depths[1] != null ? depths[1].invoices : depths[1]),{"name":"each","hash":{},"fn":this.program(16, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</select><i class=\"icon icon-add J-add-invoice\" title=\"add invoice\"></i>\n					</span>\n					<san class=\"selcted-invoice\">\n							<span>"
    + alias2(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</span><!--\n						--><i class=\"icon icon-remove J-del-invoice\" title=\"del\"></i>\n					</span>\n				</span>\n				</san>\n				</div>\n			</div>\n			<div>\n				<div>\n					<span class=\"operate-btns\">\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(18, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"!=",{"name":"xifCond","hash":{},"fn":this.program(20, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</span>\n					<span class=\"editing-btn\">\n						<i class=\"fa fa-check-circle fc-green J-ok\" data-subtask=\""
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\" data-task="
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "></i>\n						<i class=\"fa fa-times-circle fc-red J-cancel\"></i>\n					</span>\n				</div>\n			</div>\n		</span>\n	</div>\n	<div id=\"collapse_"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading_"
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\">\n		<div class=\"panel-body\">\n			<table class=\"table table-bordered table-striped table-condensed table-hover\">\n				<thead>\n					<tr>\n						<th>Line No.</th>\n						<th>Subtask</th>\n						<th>To</th>\n						<th>Status</th>\n						<th>\n							"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"!=",{"name":"xifCond","hash":{},"fn":this.program(24, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</th>\n					</tr>\n				</thead>\n				<tbody class=\"J-sub-list\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(26, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"unless","hash":{},"fn":this.program(37, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</tbody>\n			</table>\n		</div>\n	</div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    return " checked ";
},"4":function(depth0,helpers,partials,data) {
    return " disabled ";
},"6":function(depth0,helpers,partials,data) {
    return " disabled=\"disabled\" ";
},"8":function(depth0,helpers,partials,data) {
    return "					status-open\"> Open&nbsp;\n";
},"10":function(depth0,helpers,partials,data) {
    return "					\"> Pending&nbsp;\n";
},"12":function(depth0,helpers,partials,data) {
    return "					\"> Processing&nbsp;\n";
},"14":function(depth0,helpers,partials,data) {
    return "					status-closed\"> Closed&nbsp;\n";
},"16":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"18":function(depth0,helpers,partials,data) {
    return "						&nbsp;\n";
},"20":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<i class=\"icon icon-edit J-task-edit\" title=\"edit\" name=\"editSubTask\" data-task=\""
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\"></i>\n						<i class=\"icon icon-remove J-task-del\" title=\"del\" name=\"delSubTask\" data-task="
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "></i>\n						<i class=\"icon icon-close J-task-close\" title=\"close\"  name=\"closeSubTask\" data-task="
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "></i> \n";
},"22":function(depth0,helpers,partials,data) {
    return "&nbsp;";
},"24":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<i class=\"icon icon-add J-subTask-add\" title=\"add\" data-task=\""
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\"></i>\n							<i class=\"icon icon-sort J-subTask-sort\" title=\"sort\" data-task=\""
    + alias2(alias1((depth0 != null ? depth0.SCHEDULE_ID : depth0), depth0))
    + "\"></i>\n";
},"26":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "					<tr data-task="
    + alias1(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + " data-status=\""
    + alias1(((helper = (helper = helpers.TASK_STATUS || (depth0 != null ? depth0.TASK_STATUS : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TASK_STATUS","hash":{},"data":data}) : helper)))
    + "\" id=\"sub_task_"
    + alias1(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">\n						<td>	\n							<span class=\"J-sub-lineNo\">"
    + alias1((helpers.math || (depth0 && depth0.math) || alias2).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + "</span>\n							<input type=\"hidden\" class=\"J-sub-Id\" value=\""
    + alias1(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n							<input type=\"hidden\" class=\"J-sub_number\" value=\""
    + alias1(((helper = (helper = helpers.TASK_NUMBER || (depth0 != null ? depth0.TASK_NUMBER : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TASK_NUMBER","hash":{},"data":data}) : helper)))
    + "\"/>\n						</td>\n						<td>\n							<span class=\"info\">"
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</span>\n							<input class=\"form-control edit-form J-sub_detail\" value=\""
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\"  placeholder=\"Subtask...\"/>\n						</td>\n						<td>\n							<span class=\"info\">"
    + alias1(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "</span>\n							<input class=\"form-control edit-form J-select-user\" value=\""
    + alias1(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"To...\"/>\n							<input type=\"hidden\" class=\"J-sub_execute_id\" value='"
    + alias1(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\n							<input type=\"hidden\" class=\"J-sub_deleteusers\" value='"
    + alias1(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\n						</td>\n						<td class=\"J-sub_status\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[2] != null ? depths[2].taskStateKey : depths[2])) != null ? stack1.Open : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(27, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[2] != null ? depths[2].taskStateKey : depths[2])) != null ? stack1.Pending : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(29, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[2] != null ? depths[2].taskStateKey : depths[2])) != null ? stack1.Processing : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(31, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[2] != null ? depths[2].taskStateKey : depths[2])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(33, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</td>\n						<td>\n							<span class=\"operate-btns\">\n								"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[2] != null ? depths[2].taskStateKey : depths[2])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias2).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[2] != null ? depths[2].taskStateKey : depths[2])) != null ? stack1.Closed : stack1),"!=",{"name":"xifCond","hash":{},"fn":this.program(35, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							</span>\n							<san class=\"editing-btn\">\n								<i class=\"fa fa-check-circle fc-green J-sub-ok\"></i>\n								<i class=\"fa fa-times-circle fc-red J-sub-cancel\"></i>\n							</span>\n						</td>\n					</tr>\n";
},"27":function(depth0,helpers,partials,data) {
    return "							 status-open\"> Open&nbsp;";
},"29":function(depth0,helpers,partials,data) {
    return "							\">Pending&nbsp;\n";
},"31":function(depth0,helpers,partials,data) {
    return "							\">Processing&nbsp;\n";
},"33":function(depth0,helpers,partials,data) {
    return "							 status-closed\"> Closed&nbsp;\n";
},"35":function(depth0,helpers,partials,data) {
    return "								<i class=\"icon icon-edit J-sub-edit\" title=\"edit\"></i>\n								<i class=\"icon icon-remove J-sub-remove\" title=\"del\"></i>\n								<i class=\"icon icon-close J-sub-close\" title=\"close\"></i> \n";
},"37":function(depth0,helpers,partials,data) {
    return "					<tr class=\"no-data\" style=\"height: 70px;\">\n						<td style=\"height: 70px; vertical-align: middle;\" colspan=\"5\">NO Data</td>\n					</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['taskList'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SCHEDULE_ID : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.SCHEDULE_ID : depth0),{"name":"unless","hash":{},"fn":this.program(52, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<tr>\n    <td>\n        <fieldset> \n            <legend>\n                <span class=\"blod\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;\n                "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 2",{"name":"xif","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 4",{"name":"xif","hash":{},"fn":this.program(9, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n               </span>\n            </legend>\n            <dl class=\"dl-horizontal\">\n                <dt>From&nbsp;:</dt>\n                <dd>"
    + alias3(((helper = (helper = helpers.ASSEMPLOYNAME || (depth0 != null ? depth0.ASSEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ASSEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "</dd>\n                <dt>To&nbsp;: </dt>\n                <dd class=\"ellipsis\">\n                    <p>\n                     "
    + alias3(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\n                    </p>\n                    <div>\n                      "
    + alias3(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\n                    </div>\n                </dd>\n                <dt>Start&nbsp;: </dt>\n                <dd>"
    + alias3(((helper = (helper = helpers.START_TIME || (depth0 != null ? depth0.START_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"START_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\n                <dt>Deadline&nbsp;: </dt>\n                <dd>"
    + alias3(((helper = (helper = helpers.END_TIME || (depth0 != null ? depth0.END_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"END_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\n                <dt>Create Time&nbsp;:</dt>\n                <dd>"
    + alias3(((helper = (helper = helpers.CREATE_TIME || (depth0 != null ? depth0.CREATE_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\n                <dt>Finish Time&nbsp;: </dt>\n                <dd>"
    + alias3(((helper = (helper = helpers.CLOSE_TIME || (depth0 != null ? depth0.CLOSE_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CLOSE_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\n            </dl>\n        </fieldset>\n    </td>\n    <td>\n        <dl>\n            <dt>Subject</dt>\n            <dd>"
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "</dd>\n            <dt>Description</dt>\n            <dd class=\"ellipsis\">\n                <p>\n                 "
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\n                </p>\n                <div>\n                  "
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\n                </div>\n            </dd>\n        </dl>\n    </td>\n    \n    <td>\n        <fieldset>\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.program(13, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            \n            <dl id=\"sub_task_list_"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">\n               \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(15, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </dl>\n            \n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBSCHEDULE : depth0)) != null ? stack1.length : stack1),4,">",{"name":"xifCond","hash":{},"fn":this.program(24, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </fieldset>\n    </td>\n    <td>\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(26, data, 0, blockParams, depths),"inverse":this.program(35, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "    </td>\n    <td>\n        <dl>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBLOG : depth0),{"name":"each","hash":{},"fn":this.program(40, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.SUBLOG : depth0),{"name":"unless","hash":{},"fn":this.program(43, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </dl>\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBLOG : depth0)) != null ? stack1.length : stack1),3,">",{"name":"xifCond","hash":{},"fn":this.program(45, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </td>\n</tr>\n<tr> \n    <td colspan=\"5\" class=\"text-right tr-btn\">\n        <div class=\"btn-group btn-group-sx\" role=\"group\">\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(47, data, 0, blockParams, depths),"inverse":this.program(49, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "        </div>\n    </td>\n</tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "Pending ";
},"5":function(depth0,helpers,partials,data) {
    return "Processing ";
},"7":function(depth0,helpers,partials,data) {
    return "Closed ";
},"9":function(depth0,helpers,partials,data) {
    return "Open ";
},"11":function(depth0,helpers,partials,data) {
    return "        		<legend>\n                	<span>Task</span>\n            	</legend>\n";
},"13":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        		<legend>\n	                <span>Add Task</span>\n	                <i class=\"icon icon-add\" name=\"addSubTask\" title=\"add\" data-task=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\" data-subject=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "\"></i>\n	                <i class=\"icon icon-sort J-sub-sort\" title=\"sort\" data-task=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"></i>\n	            </legend>\n";
},"15":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "               \n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),4,"<",{"name":"xifCond","hash":{},"fn":this.program(16, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"16":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <dd>\n                    <div class=\"header\">\n                       <a href=\"javascript:void(0);\" data-sub=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\" data-task=\""
    + alias3(this.lambda((depths[2] != null ? depths[2].SCHEDULE_ID : depths[2]), depth0))
    + "\" class=\"J-sub-detail\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(17, data, 0, blockParams, depths),"inverse":this.program(19, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                   	\">\n                        "
    + alias3((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + ".&nbsp; "
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\n                       </a>\n                    </div>\n                    \n                </dd>\n                \n";
},"17":function(depth0,helpers,partials,data) {
    return "                 			 fc-green   \n";
},"19":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(20, data, 0),"inverse":this.program(22, data, 0),"data":data})) != null ? stack1 : "");
},"20":function(depth0,helpers,partials,data) {
    return "                 				 fc-gray \n";
},"22":function(depth0,helpers,partials,data) {
    return "                 				fc-red\n";
},"24":function(depth0,helpers,partials,data,blockParams,depths) {
    return "        		<a href=\"javascript:void(0);\" data-task=\""
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" class=\"fc-orange fc-right underline J-sub-detail\">More...</a>\n";
},"26":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "	    		<fieldset>\n		            <legend>\n		                <span>Add Invoice</span>\n		            </legend>\n		             <dl id=\"invoice_list_"
    + this.escapeExpression(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">\n		               \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(27, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		            </dl>\n		        </fieldset>\n    \n";
},"27":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		                \n		                <dd>\n		                    <fieldset class=\"invoice\">\n		                        <legend>\n		                           <a href=\"javascript:void(0);\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"fc-green underline J-goto-invoice\">&nbsp;"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;</a>\n		                        	<span class=\"J-invoice-status\">"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(28, data, 0),"inverse":this.program(30, data, 0),"data":data})) != null ? stack1 : "")
    + "</span>\n		                        </legend>\n		                        <dl>\n		                            <dt>&nbsp;"
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + "</dt>\n		                            <dd><span>Create Time&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\n		                        </dl>\n		                    </fieldset>\n		                </dd>\n		                \n";
},"28":function(depth0,helpers,partials,data) {
    return "IMPORTED";
},"30":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(31, data, 0),"inverse":this.program(33, data, 0),"data":data})) != null ? stack1 : "");
},"31":function(depth0,helpers,partials,data) {
    return "CLOSED";
},"33":function(depth0,helpers,partials,data) {
    return "OPEN";
},"35":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper;

  return "	    	<fieldset>\n	        	 <legend>\n	                <span>Invoice</span>\n	            </legend>\n	            <dl id=\"invoice_list_"
    + this.escapeExpression(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(36, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	            </dl>\n	        </fieldset>\n";
},"36":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression, alias4=this.lambda;

  return "	                <dd>\n	                    <fieldset class=\"invoice\">\n	                        <legend>\n	                            <a href=\"javascript:void(0);\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"fc-green underline J-goto-invoice\">&nbsp;"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;</a>\n	                        	<span class=\"J-invoice-status\">"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(28, data, 0, blockParams, depths),"inverse":this.program(30, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\n	                        </legend>\n	                        <dl>\n	                            <dt><span>Reference No&nbsp;:</span>&nbsp;\n"
    + ((stack1 = helpers.each.call(depth0,(depths[4] != null ? depths[4].LRTypeKey : depths[4]),{"name":"each","hash":{},"fn":this.program(37, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                				<button class=\"btn btn-default btn-xs J-pdf\" data-invoice=\""
    + alias3(alias4((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" data-task=\""
    + alias3(alias4((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" type=\"button\">Preview</button></dt>\n	                            <dd><span>Create Time&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\n	                        </dl>\n	                    </fieldset>\n	                </dd>\n	                \n";
},"37":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].LR_TYPE : depths[1]),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(38, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"38":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=this.escapeExpression;

  return "                						"
    + alias1(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "&nbsp;"
    + alias1(this.lambda((depths[2] != null ? depths[2].LR_NO : depths[2]), depth0))
    + "\n";
},"40":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),3,"<",{"name":"xifCond","hash":{},"fn":this.program(41, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"41":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	            	<dt><span>"
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + " </span>\n	            		<a href=\"javascript:void(0);\" data-task=\""
    + alias3(this.lambda((depths[2] != null ? depths[2].SCHEDULE_ID : depths[2]), depth0))
    + "\" data-logid=\""
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\"  class=\"fc-green underline log-detail\">"
    + alias3(((helper = (helper = helpers.OPERATION_NOTE || (depth0 != null ? depth0.OPERATION_NOTE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"OPERATION_NOTE","hash":{},"data":data}) : helper)))
    + "</a>\n	            	</dt>\n	            	<dd><span>"
    + alias3(((helper = (helper = helpers.POST_DATE || (depth0 != null ? depth0.POST_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"POST_DATE","hash":{},"data":data}) : helper)))
    + "<span></dd>\n";
},"43":function(depth0,helpers,partials,data) {
    return "            	<dt>&nbsp;</dt> \n            	<dd></dd>\n";
},"45":function(depth0,helpers,partials,data,blockParams,depths) {
    return "        <a href=\"javascript:void(0);\" data-task=\""
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" class=\"fc-orange fc-right underline log-detail\">More...</a>\n";
},"47":function(depth0,helpers,partials,data) {
    return "        &nbsp;\n";
},"49":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "            <button type=\"button\" name=\"editTask\" class=\"btn btn-default\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + ">\n            	<i class=\"glyphicon glyphicon-pencil\"> Edit</i>\n            </button>\n            <button type=\"button\" name=\"delTask\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " class=\"btn btn-default\">\n            	<i class=\"glyphicon glyphicon-trash\">  Del</i>\n            </button>\n            <button type=\"button\" name=\"closeTask\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " class=\"btn btn-default\">\n            	<i class=\"glyphicon glyphicon-ban-circle\">  Close</i>\n           </button>\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBFILE : depth0)) != null ? stack1.length : stack1),0,">",{"name":"xifCond","hash":{},"fn":this.program(50, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "           <!--  <button type=\"button\" name=\"addLog\" class=\"btn btn-default\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + ">\n            	<i class=\"glyphicon glyphicon-bookmark\"> Log</i>\n            </button> -->\n";
},"50":function(depth0,helpers,partials,data) {
    var helper;

  return "            <button type=\"button\" name=\"downloadSOP\" data-task="
    + this.escapeExpression(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " class=\"btn btn-default\n             btn-warning\">\n            	<i class=\"glyphicon glyphicon-download-alt\"> SOP</i>\n            </button>\n";
},"52":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),0,"=",{"name":"xifCond","hash":{},"fn":this.program(53, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"53":function(depth0,helpers,partials,data) {
    return "	<tr>\n		<td class=\"no-data\" style=\"height: 180px;\" colspan=\"5\">NO Data</td>\n	</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['uploadSOP'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"upload-sop\">\n	<div id=\"upload_file\">\n	</div>\n</div>\n";
},"useData":true});
return templates;
});