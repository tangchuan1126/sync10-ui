(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addDetail'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<tr>\r\n    <td class=\"text-center\"><span class=\"J-line-no\">"
    + alias3(((helper = (helper = helpers.LINE_NO || (depth0 != null ? depth0.LINE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"LINE_NO","hash":{},"data":data}) : helper)))
    + "</span></td>\r\n    <td><input class=\"form-control J-service-id\" placeholder=\"Service ID\" value=\""
    + alias3(((helper = (helper = helpers.SERVICE_ID || (depth0 != null ? depth0.SERVICE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SERVICE_ID","hash":{},"data":data}) : helper)))
    + "\"/></td>\r\n    <td><input class=\"form-control J-des\" placeholder=\"Description\" value=\""
    + alias3(((helper = (helper = helpers.DESCRIPTION || (depth0 != null ? depth0.DESCRIPTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"DESCRIPTION","hash":{},"data":data}) : helper)))
    + "\"/></td>\r\n    <td>\r\n    	<select class=\"form-control J-unit\">\r\n            <option value=\"\" selected></option>\r\n    		<option value=\"1\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 1",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Squara Feet</option>\r\n    		<option value=\"2\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 2",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Cubic Feet</option>\r\n            <option value=\"3\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 3",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">CWT</option>\r\n            <option value=\"4\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 4",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Long Ton</option>\r\n            <option value=\"5\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 5",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Short Ton</option>\r\n            <option value=\"6\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 6",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Metric Ton</option>\r\n            <option value=\"7\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 7",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Invoice</option>\r\n            <option value=\"8\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 8",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Package</option>\r\n            <option value=\"9\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 9",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Pallet</option>\r\n            <option value=\"10\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 10",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Piece</option>\r\n            <option value=\"11\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 11",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Receipt</option>\r\n            <option value=\"12\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 12",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Release</option>\r\n            <option value=\"13\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 13",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Period</option>\r\n            <option value=\"14\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 14",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Day</option>\r\n            <option value=\"15\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 15",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">Hour</option>\r\n    	</select>\r\n    </td>\r\n    <td><input class=\"form-control J-rate\" placeholder=\"0.0000\" value=\""
    + alias3(((helper = (helper = helpers.RATE || (depth0 != null ? depth0.RATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"RATE","hash":{},"data":data}) : helper)))
    + "\"/></td>\r\n    <td><input class=\"form-control J-quantity\" placeholder=\"0.0000\" value=\""
    + alias3(((helper = (helper = helpers.QUANTITY || (depth0 != null ? depth0.QUANTITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"QUANTITY","hash":{},"data":data}) : helper)))
    + "\"/></td>\r\n    <td><span class=\"J-amount\">"
    + alias3(((helper = (helper = helpers.AMOUNT || (depth0 != null ? depth0.AMOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"AMOUNT","hash":{},"data":data}) : helper)))
    + "</span></td>\r\n    <td><i class=\"icon icon-remove\"></i></td>\r\n</tr>\r\n";
},"2":function(depth0,helpers,partials,data) {
    return "selected";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.detail : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['addInvoice'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"container-fluid task-invoice add-invoice\">\r\n    <div role=\"tabpanel\" class=\"panel panel-default\">\r\n        <div class=\"panel-body\">\r\n            <!-- Nav tabs -->\r\n            <ul class=\"nav nav-tabs\" role=\"tablist\">\r\n                <li role=\"presentation\" class=\"active\">\r\n                    <span class=\"check\"></span>\r\n                    <a href=\"#template\" aria-controls=\"template\" role=\"tab\" data-toggle=\"tab\">Bill Template</a>\r\n                </li>\r\n                <li role=\"presentation\">\r\n                    <span class=\"check\"></span>\r\n                    <a href=\"#detail\" aria-controls=\"detail\" role=\"tab\" data-toggle=\"tab\">Bill Detail</a>\r\n                </li>\r\n                <li role=\"presentation\">\r\n                    <a href=\"#browse\" aria-controls=\"browse\" role=\"tab\" data-toggle=\"tab\">Preview</a>\r\n                </li>\r\n            </ul>\r\n\r\n            <!-- Tab panes -->\r\n            <div class=\"tab-content\">\r\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"template\"></div>\r\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"detail\"></div>\r\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"browse\"></div>\r\n                <div class=\"modal\"><div class=\"loading\"></div></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
templates['addSubTask'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"table table-bordered add-task\">\r\n    <tbody>\r\n        <tr> \r\n            <td>To</td>\r\n            <td>\r\n                <select style='width:350px;' multiple=\"multiple\" name=\"schedule_execute_id\" id=\"schedule_execute_id\"> </select>\r\n				<input type=\"hidden\" id=\"deleteusers\" name=\"deleteusers\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_EXECUTE_ID : stack1), depth0))
    + "'/>\r\n				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n				<input type=\"checkbox\" id=\"is_additional_Task\" name=\"is_additional_Task\" style=\"display:none;\"/>\r\n				\r\n				<input type=\"hidden\" id=\"assign_user_id\" name=\"assign_user_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ASSIGN_USER_ID : stack1), depth0))
    + "'/>\r\n	    		<input type=\"hidden\" id=\"parent_schedule_id\" name=\"parent_schedule_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.PARENT_SCHEDULE_ID : stack1), depth0))
    + "'/>\r\n	    		<input type=\"hidden\" id=\"schedule_id\" name=\"schedule_id\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_ID : stack1), depth0))
    + "'/>\r\n	    		<input type=\"hidden\" id=\"task_number\" name=\"task_number\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.TASK_NUMBER : stack1), depth0))
    + "'/>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>Task</td>\r\n            <td> \r\n                <textarea class=\"form-control\" name=\"schedule_detail\" id=\"schedule_detail\" placeholder=\"Description...\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SCHEDULE_DETAIL : stack1), depth0))
    + "</textarea>\r\n            	<div class=\"submit\"><button class=\"btn btn-default\" type=\"button\" id=\"save_sub_task\">Submit</button></div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n";
},"useData":true});
templates['addTask'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        				<dd>\r\n	        				<span name=\"billId\">"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "</span> <div class=\"invoice_del\"><i class=\"icon icon-remove\" name=\"delInvoice\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\"></i></div>\r\n	        			</dd>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	        			<dd>\r\n	        				<span name=\"fileId\">\r\n	        					<a href=\"/Sync10/_fileserv/file/"
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" download=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" data-gallery=\"\">\r\n	        						"
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\r\n	        					</a>\r\n	        				</span>\r\n	        				<div class=\"file_del\"><i class=\"icon icon-remove\" name=\"delFile\" data-filename=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" data-fileid=\""
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\"></i></div>\r\n        				</dd>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"add-task-div\">\r\n<table class=\"table table-bordered add-task\">\r\n    <tbody>\r\n        <tr>\r\n            <td>From</td>\r\n            <td>"
    + alias3(((helper = (helper = helpers.ASSEMPLOYNAME || (depth0 != null ? depth0.ASSEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ASSEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\r\n	    	<input type=\"hidden\" id=\"assign_user_id\" name=\"assign_user_id\" value='"
    + alias3(((helper = (helper = helpers.ASSIGN_USER_ID || (depth0 != null ? depth0.ASSIGN_USER_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ASSIGN_USER_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n	    	<input type=\"hidden\" id=\"schedule_id\" name=\"schedule_id\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
<<<<<<< HEAD
    + "'/>\r\n	    </td>\r\n        </tr> \r\n        <tr>\r\n            <td>To</td>\r\n            <td>\r\n                <select class=\"multiple_select\" multiple=\"multiple\" name=\"schedule_execute_id\" id=\"schedule_execute_id\"></select>\r\n				<input type=\"hidden\" id=\"deleteusers\" name=\"deleteusers\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n				\r\n            </td>\r\n        </tr>\r\n        <tr> \r\n            <td>CC</td>\r\n            <td>\r\n           	 <select class=\"multiple_select\" multiple=\"multiple\" id=\"schedule_join_execute_id\">\r\n           	 </select>\r\n				<input type=\"hidden\" id=\"del_join_user_id\" name=\"del_join_user_id\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_JOIN_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_JOIN_EXECUTE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_JOIN_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n            </td>\r\n        </tr>\r\n         <tr>\r\n            <td>Time</td>\r\n            <td class=\"js-date-task\">\r\n               \r\n			  <div class=\"add-task_form-group1\">\r\n			   <label class=\"col-md-2 control-label\" for=\"dtp_input2\"> Start</label>\r\n			   <div id=\"startTime\" class=\"input-group date col-md-3\" data-format=\"mm/dd/yy hh:ii\" data-link-field=\"dtp_input2\" data-date-format=\"yyyy-mm-dd hh:ii\" data-date=\"\">\r\n					<input class=\"form-control\" type=\"text\" id=\"start_time\" name=\"start_time\" value=\""
    + alias3(((helper = (helper = helpers.START_TIME || (depth0 != null ? depth0.START_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"START_TIME","hash":{},"data":data}) : helper)))
    + "\" size=\"16\" readonly=\"readonly\" placeholder=\"start time\" >\r\n					<span class=\"input-group-addon\">\r\n						<span class=\"glyphicon glyphicon-calendar\"></span>\r\n					</span>\r\n			   </div>\r\n			  </div>\r\n			  <div class=\"add-task_form-group2\">\r\n			   	<label class=\"col-md-2 control-label\" for=\"dtp_input2\"> End</label>\r\n			   	<div id=\"endTime\" class=\"input-group date col-md-3\" data-link-format=\"mm/dd/yy hh:ii\" data-link-field=\"dtp_input2\" data-date-format=\"yyyy-mm-dd hh:ii\" data-date=\"\">\r\n					<input class=\"form-control\" type=\"text\" id=\"end_time\" name=\"end_time\" value=\""
=======
    + "'/>\r\n	    </td>\r\n        </tr> \r\n        <tr>\r\n            <td>To</td>\r\n            <td>\r\n               <!--\r\n                <select class=\"multiple_select\" multiple=\"multiple\" name=\"schedule_execute_id\" id=\"schedule_execute_id\"></select>\r\n				-->\r\n				<input type=\"text\" class=\"form-control\" id=\"exe_employ_name\" name=\"exe_employ_name\" value=\""
    + alias3(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"TO...\"/>\r\n				<input type=\"hidden\" id=\"schedule_execute_id\" name=\"schedule_execute_id\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n				<input type=\"hidden\" id=\"deleteusers\" name=\"deleteusers\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_EXECUTE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n            </td>\r\n        </tr>\r\n        <tr> \r\n            <td>CC</td>\r\n            <td>\r\n            <!--\r\n           	 <select class=\"multiple_select\" multiple=\"multiple\" id=\"schedule_join_execute_id\">\r\n           	 </select>\r\n           	 -->\r\n           	 \r\n				<input type=\"text\" class=\"form-control\" id=\"join_employ_name\" name=\"join_employ_name\" value='"
    + alias3(((helper = (helper = helpers.JOINEMPLOYNAME || (depth0 != null ? depth0.JOINEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"JOINEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "' placeholder=\"CC...\"/>\r\n				<input type=\"hidden\" id=\"schedule_join_execute_id\" name=\"schedule_join_execute_id\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_JOIN_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_JOIN_EXECUTE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_JOIN_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n				<input type=\"hidden\" id=\"del_join_user_id\" name=\"del_join_user_id\" value='"
    + alias3(((helper = (helper = helpers.SCHEDULE_JOIN_EXECUTE_ID || (depth0 != null ? depth0.SCHEDULE_JOIN_EXECUTE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_JOIN_EXECUTE_ID","hash":{},"data":data}) : helper)))
    + "'/>\r\n            </td>\r\n        </tr>\r\n         <tr>\r\n            <td>Time</td>\r\n            <td class=\"js-date-task\">\r\n			  <div class=\"add-task_form-group1\">\r\n			   <label class=\"col-md-2 control-label\" for=\"dtp_input2\"> Start</label>\r\n			   <div id=\"startTime\" class=\"input-group date col-md-3\" data-format=\"mm/dd/yy hh:ii\" data-link-field=\"dtp_input2\" data-date-format=\"yyyy-mm-dd hh:ii\" data-date=\"\">\r\n					<input class=\"form-control\" type=\"text\" id=\"start_time\" name=\"start_time\" value=\""
    + alias3(((helper = (helper = helpers.START_TIME || (depth0 != null ? depth0.START_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"START_TIME","hash":{},"data":data}) : helper)))
    + "\" size=\"16\" readonly=\"readonly\" placeholder=\"start time\" >\r\n					<span class=\"input-group-addon\">\r\n						<span class=\"glyphicon glyphicon-calendar\"></span>\r\n					</span>\r\n			   </div>\r\n			  </div>\r\n			  \r\n			  <div class=\"add-task_form-group2\">\r\n			   	<label class=\"col-md-2 control-label\" for=\"dtp_input2\"> End</label>\r\n			   	<div id=\"endTime\" class=\"input-group date col-md-3\" data-link-format=\"mm/dd/yy hh:ii\" data-link-field=\"dtp_input2\" data-date-format=\"yyyy-mm-dd hh:ii\" data-date=\"\">\r\n					<input class=\"form-control\" type=\"text\" id=\"end_time\" name=\"end_time\" value=\""
>>>>>>> task_invoice
    + alias3(((helper = (helper = helpers.END_TIME || (depth0 != null ? depth0.END_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"END_TIME","hash":{},"data":data}) : helper)))
    + "\" size=\"16\" readonly=\"readonly\" placeholder=\"end time\" >\r\n					<span class=\"input-group-addon\">\r\n						<span class=\"glyphicon glyphicon-calendar\"></span>\r\n					</span>\r\n			   	</div>\r\n			  </div>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>Subject</td>\r\n            <td>\r\n                <input class=\"form-control\"  name=\"schedule_overview\" value=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "\" id=\"schedule_overview\" placeholder=\"Subject...\" />\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>LR NO</td>\r\n            <td>\r\n    			<dt>\r\n    			 	<i class=\"icon icon-add\" name=\"addLR\" ></i>\r\n    			</dt>\r\n    			<dt class=\"hid_lr\">\r\n				  <div class=\"lr_no_left\">\r\n        			<select class=\"form-control\">\r\n	                	<option id=\"task_type\" name=\"task_type\" value=\"\">Type</option>\r\n	                	<option id=\"task_type\" name=\"task_type\" value=\"\">Load</option>\r\n	                	<option id=\"task_type\" name=\"task_type\" value=\"\">Order</option>\r\n	                </select>\r\n					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n					<input class=\"form-control\" type=\"text\" id=\"task_type_no\" name=\"task_type_no\" value=\""
    + alias3(((helper = (helper = helpers.TASK_TYPE_NO || (depth0 != null ? depth0.TASK_TYPE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TASK_TYPE_NO","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"NO.\"/>\r\n				  </div>\r\n				  <div class=\"lr_no_del\"> <i class=\"icon icon-remove\" name=\"delLR\"></i></div>\r\n            	</dt>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>Description</td>\r\n            <td> \r\n                <textarea class=\"form-control\" placeholder=\"Description\" name=\"schedule_detail\" id=\"schedule_detail\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
<<<<<<< HEAD
    + "</textarea>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>Invoice</td>\r\n            <td>\r\n        		<dl id=\"showInvoice\">\r\n        			<dd><i class=\"icon icon-add\" name=\"addInvoice\" ></i></dd>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        		</dl>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>Document</td>\r\n            <td>\r\n        		<dl id=\"showFile\">\r\n        			<dd><i class=\"icon icon-upload\" name=\"uploadSOP\" ></i></dd>\r\n"
=======
    + "</textarea>\r\n            </td>\r\n        </tr>\r\n        \r\n        <!--\r\n        <tr>\r\n            <td>Invoice</td>\r\n            <td>\r\n        		<dl id=\"showInvoice\">\r\n        			<dd><i class=\"icon icon-add\" name=\"addInvoice\" ></i></dd>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        		</dl>\r\n            </td>\r\n        </tr>\r\n        -->\r\n        \r\n        <tr>\r\n            <td>Document</td>\r\n            <td>\r\n        		<dl id=\"showFile\">\r\n        			<dd><i class=\"icon icon-upload\" name=\"uploadSOP\" ></i></dd>\r\n"
>>>>>>> task_invoice
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBFILE : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        			\r\n        		</dl>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</div>";
},"useData":true});
templates['addTaskLog'] = template({"1":function(depth0,helpers,partials,data) {
    return "							<option value=\"1\">Normal0</option>\r\n							<option value=\"0\" selected=\"selected\">Replay0</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "							<option value=\"1\" selected=\"selected\">Normal</option>\r\n							<option value=\"0\">Replay</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<!--<div class=\"add-replay\"> -->\r\n<!-- <fieldset> -->\r\n	<legend id=\"showAddReplayUserInfo\">"
    + alias3(((helper = (helper = helpers.ASSEMPLOYNAME || (depth0 != null ? depth0.ASSEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ASSEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "</legend>\r\n	<!-- <form id=\"addform\" action=\"\"> -->\r\n		<input type=\"hidden\" id=\"schedule_id\" name=\"schedule_id\"  value=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<input type=\"hidden\" id=\"sch_replay_id\" name=\"sch_replay_id\"  value=\""
    + alias3(((helper = (helper = helpers.SCH_REPLAY_ID || (depth0 != null ? depth0.SCH_REPLAY_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_ID","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<input type=\"hidden\" name=\"employe_name\" id=\"employe_name\" value=\""
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<input type=\"hidden\" name=\"adid\" id=\"adid\" value=\""
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<input type=\"hidden\" name=\"sch_state\" id=\"sch_state\" value=\""
    + alias3(((helper = (helper = helpers.SCH_STATE || (depth0 != null ? depth0.SCH_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_STATE","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<!-- <input type=\"hidden\" name=\"schedule_sub_id\" id=\"schedule_sub_id\" value=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_SUB_ID || (depth0 != null ? depth0.SCHEDULE_SUB_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_SUB_ID","hash":{},"data":data}) : helper)))
    + "\"/> -->\r\n		<!-- <input type=\"hidden\" name=\"total_state\" id=\"total_state\" value=\""
    + alias3(((helper = (helper = helpers.TOTAL_STATE || (depth0 != null ? depth0.TOTAL_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TOTAL_STATE","hash":{},"data":data}) : helper)))
    + "\"/> -->\r\n		<table class=\"table\">\r\n			<tr>\r\n				<td class=\"info\">Remark Type &nbsp;</td>\r\n				<td width=\"80%\">\r\n					<select class=\"form-control\" name=\"sch_replay_type\" id=\"sch_replay_type\" style=\"float:left;\">\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.SCH_REPLAY_TYPE == 0",{"name":"xif","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "					</select>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td  class=\"info\"><span id=\"contentName\">Remark </span>&nbsp;</td>\r\n				<td>\r\n					<textarea  class=\"form-control\" style=\"height:232px;width:445px\" id=\"sch_replay_context\" name=\"sch_replay_context\">"
    + alias3(((helper = (helper = helpers.SCH_REPLAY_CONTEXT || (depth0 != null ? depth0.SCH_REPLAY_CONTEXT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_CONTEXT","hash":{},"data":data}) : helper)))
    + "</textarea>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td  class=\"info\">Submitter &nbsp;</td>\r\n				<td><span style=\"color:green;\">"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "&nbsp;</span></td>\r\n			</tr>\r\n		</table>\r\n	<!-- </form> -->\r\n	<!-- </fieldset> -->\r\n<!-- </div>";
},"useData":true});
templates['downloadSOP'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<tr>\r\n				<td>\r\n					<span name=\"fileId\">\r\n	        			<a href=\"/Sync10/_fileserv/file/"
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" download=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" data-gallery=\"\">\r\n	        				"
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\r\n	        			</a> &nbsp;\r\n	        		</span>\r\n	        	</td>\r\n				<td class=\"text-center\">\r\n					<a href=\"/Sync10/_fileserv/file/"
    + alias3(((helper = (helper = helpers.FILE_ID || (depth0 != null ? depth0.FILE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FILE_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"glyphicon glyphicon-download-alt\"title=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\" download=\""
    + alias3(((helper = (helper = helpers.ORIGINAL_FILE_NAME || (depth0 != null ? depth0.ORIGINAL_FILE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ORIGINAL_FILE_NAME","hash":{},"data":data}) : helper)))
    + "\"></a>\r\n				</td>\r\n			</tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "		    <tr>\r\n		      <td class=\"no-data\" colspan=\"6\">NO Data</td>\r\n		    </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"download-content\">\r\n	<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>File Name</th>\r\n			<th>Operation</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n"
    + ((stack1 = helpers.each.call(depth0,depth0,{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,depth0,{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</tbody>\r\n	</table>\r\n</div>";
},"useData":true});
templates['initDeptInfo'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		<tr>\r\n			<td class=\"left\">\r\n				<span class=\"span_checkBox\" style=\"margin-top:-4px;\">\r\n					<input type=\"checkbox\" class=\"J-adgid_check_checked\" target=\"ul_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" id=\"adgid_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias3(((helper = (helper = helpers.deptId || (depth0 != null ? depth0.deptId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"deptId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n				</span>&nbsp;\r\n				<label class=\"span_name\" for=\"adgid_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.deptName || (depth0 != null ? depth0.deptName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"deptName","hash":{},"data":data}) : helper)))
    + "</label>\r\n			</td>\r\n			<td>\r\n				<ul class=\"myul\" id=\"ul_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" target=\"adgid_"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.values : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</ul>\r\n			</td>\r\n		</tr>\r\n";
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depths[2] != null ? depths[2].admin_id : depths[2]),(depth0 != null ? depth0.ADID : depth0),"!=",{"name":"xifCond","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"3":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<li><span class=\"span_checkBox\"><input type=\"checkbox\" class=\"J-user_check_checked\" name=\"user_"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "\" id=\"user_"
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\"  "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.checked : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  value=\""
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\"/></span>&nbsp;\r\n									<label class=\"span_name\" for=\"user_"
    + alias3(((helper = (helper = helpers.ADID || (depth0 != null ? depth0.ADID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + "</label>\r\n								</li>\r\n";
},"4":function(depth0,helpers,partials,data) {
    return "checked=\"checked\" ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<table class=\"mytable\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</table>\r\n";
},"useData":true,"useDepths":true});
templates['invoiceBrowse'] = template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return this.escapeExpression(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)));
},"3":function(depth0,helpers,partials,data) {
    return "XXXXXX";
},"5":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "            <tr>\r\n                <td>"
    + alias3(((helper = (helper = helpers.QUANTITY || (depth0 != null ? depth0.QUANTITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"QUANTITY","hash":{},"data":data}) : helper)))
    + "</td>\r\n                <td>"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 1",{"name":"xif","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 2",{"name":"xif","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 3",{"name":"xif","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 4",{"name":"xif","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 5",{"name":"xif","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 6",{"name":"xif","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 7",{"name":"xif","hash":{},"fn":this.program(18, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 8",{"name":"xif","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 9",{"name":"xif","hash":{},"fn":this.program(22, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 10",{"name":"xif","hash":{},"fn":this.program(24, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 11",{"name":"xif","hash":{},"fn":this.program(26, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 12",{"name":"xif","hash":{},"fn":this.program(28, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 13",{"name":"xif","hash":{},"fn":this.program(30, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 14",{"name":"xif","hash":{},"fn":this.program(32, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                  "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.UNIT_PRICE == 15",{"name":"xif","hash":{},"fn":this.program(34, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                </td>\r\n                <td><div>"
    + alias3(((helper = (helper = helpers.DESCRIPTION || (depth0 != null ? depth0.DESCRIPTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"DESCRIPTION","hash":{},"data":data}) : helper)))
    + "</div></td>\r\n                <td>"
    + alias3(((helper = (helper = helpers.RATE || (depth0 != null ? depth0.RATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"RATE","hash":{},"data":data}) : helper)))
    + "</td>\r\n                <td>"
    + alias3(((helper = (helper = helpers.AMOUNT || (depth0 != null ? depth0.AMOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"AMOUNT","hash":{},"data":data}) : helper)))
    + "</td>\r\n            </tr>\r\n";
},"6":function(depth0,helpers,partials,data) {
    return "Squara Feet";
},"8":function(depth0,helpers,partials,data) {
    return "Cubic Feet";
},"10":function(depth0,helpers,partials,data) {
    return "CWT";
},"12":function(depth0,helpers,partials,data) {
    return "Long Ton";
},"14":function(depth0,helpers,partials,data) {
    return "Short Ton";
},"16":function(depth0,helpers,partials,data) {
    return "Metric Ton";
},"18":function(depth0,helpers,partials,data) {
    return "Invoice";
},"20":function(depth0,helpers,partials,data) {
    return "Package";
},"22":function(depth0,helpers,partials,data) {
    return "Pallet";
},"24":function(depth0,helpers,partials,data) {
    return "Piece";
},"26":function(depth0,helpers,partials,data) {
    return "Receipt";
},"28":function(depth0,helpers,partials,data) {
    return "Release";
},"30":function(depth0,helpers,partials,data) {
    return "Period";
},"32":function(depth0,helpers,partials,data) {
    return "Day";
},"34":function(depth0,helpers,partials,data) {
    return "Hour";
},"36":function(depth0,helpers,partials,data) {
    return "<div class=\"text-center mt20\">\r\n    <button type=\"button\" class=\"btn btn-primary\">Save</button>\r\n</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <header>\r\n        <label>Logistics Team</label>\r\n        <p>Location:(Valley)15930 Valley Blvd. City of Industry CA 91744</p>\r\n        <p>TEL:909-598-7268 FAX:909-598-7216</p>\r\n        <p>INVOICE</p>\r\n    </header>\r\n    <section class=\"bill-to\">\r\n        <div class=\"content clearfix\">\r\n            <dl class=\"dl-horizontal\">\r\n              <dt>Bill To:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.ADDRESS_NAME || (depth0 != null ? depth0.ADDRESS_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_NAME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n              <dt>Address:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.ADDRESS_STREET || (depth0 != null ? depth0.ADDRESS_STREET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STREET","hash":{},"data":data}) : helper)))
    + " "
    + alias3(((helper = (helper = helpers.ADDRESS_CITY || (depth0 != null ? depth0.ADDRESS_CITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_CITY","hash":{},"data":data}) : helper)))
    + " "
    + alias3(((helper = (helper = helpers.ADDRESS_ZIP || (depth0 != null ? depth0.ADDRESS_ZIP : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_ZIP","hash":{},"data":data}) : helper)))
    + "</dd>\r\n              <dt>Attention:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.ATTENTION || (depth0 != null ? depth0.ATTENTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ATTENTION","hash":{},"data":data}) : helper)))
    + "</dd>\r\n              <dt>Tel:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.TEL || (depth0 != null ? depth0.TEL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TEL","hash":{},"data":data}) : helper)))
    + "</dd>\r\n              <dt>Fax:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.FAX || (depth0 != null ? depth0.FAX : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FAX","hash":{},"data":data}) : helper)))
    + "</dd>\r\n          </dl><dl class=\"dl-horizontal\">\r\n              <dt>Date:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\r\n              <dt>Invoice No:</dt>\r\n              <dd>"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.BILL_ID : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "</dd>\r\n              <dt>Payment Term:</dt>\r\n              <dd>"
    + alias3(((helper = (helper = helpers.PAYMENT_TERM || (depth0 != null ? depth0.PAYMENT_TERM : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PAYMENT_TERM","hash":{},"data":data}) : helper)))
    + "</dd>\r\n              <dt>Reference No:</dt>\r\n              <dd style=\"word-break: break-word;\">"
    + alias3(((helper = (helper = helpers.REFERENCE_NO || (depth0 != null ? depth0.REFERENCE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"REFERENCE_NO","hash":{},"data":data}) : helper)))
    + "</dd>\r\n          </dl>\r\n      </div>\r\n  </section>\r\n  <section>\r\n    <table class=\"table table-striped table-bordered\">\r\n        <thead>\r\n            <tr class=\"info\">\r\n                <th colspan=\"2\">Quantity</th>\r\n                <th>Description</th>\r\n                <th>Rate</th>\r\n                <th>Amount</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.BILL_DETAIL : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </tbody>\r\n        <tfoot>\r\n            <tr>\r\n                <td colspan=\"3\"></td>\r\n                <td colspan=\"2\">\r\n                    <ul class=\"list-unstyled\">\r\n                        <!-- <li class=\"info\"><label>Subtotal</label>"
    + alias3(((helper = (helper = helpers.SUBTOTAL || (depth0 != null ? depth0.SUBTOTAL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SUBTOTAL","hash":{},"data":data}) : helper)))
    + "</li> -->\r\n                        <!-- <li><label>Discount/Mark up</label>"
    + alias3(((helper = (helper = helpers.TOTAL_DISCOUNT || (depth0 != null ? depth0.TOTAL_DISCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TOTAL_DISCOUNT","hash":{},"data":data}) : helper)))
    + "%</li> -->\r\n                        <li class=\"info\"><label>Total</label>"
    + alias3(((helper = (helper = helpers.TOTAL || (depth0 != null ? depth0.TOTAL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TOTAL","hash":{},"data":data}) : helper)))
    + "</li>\r\n                    </ul>\r\n                </td>\r\n            </tr>\r\n        </tfoot>\r\n    </table>\r\n</section>\r\n<footer>\r\n    <div class=\"content clearfix\">\r\n        <div class=\"col-md-5\">\r\n            <small>Please remit payment to</small>\r\n            <p>Logistics Team</p>\r\n            <p>218Machlin Ct.#A</p>\r\n            <p>Walnut,CA91789</p>\r\n            <p>Attn:Accounting Department</p>\r\n        </div>\r\n        <div class=\"col-md-7\">\r\n            5% late service fee per month will be added to amounts past due. if the account is turned over for collection, the prevailing party shall be entitled to obtain reasonable attorneys fees and costs.\r\n        </div>\r\n    </div>\r\n</footer>\r\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.noBtn : depth0),{"name":"unless","hash":{},"fn":this.program(36, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['invoiceDetail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"text-left\">Reference No : <input class=\"form-control\" type=\"text\" id=\"reference_no\" value=\""
    + alias3(((helper = (helper = helpers.REFERENCE_NO || (depth0 != null ? depth0.REFERENCE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"REFERENCE_NO","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n<table class=\"table table-bordered\">\r\n    <thead>\r\n        <tr class=\"success\">\r\n            <th>Line</th>\r\n            <th>Service ID</th>\r\n            <th>Description</th>\r\n            <th>Unit</th>\r\n            <th>Rate</th>\r\n            <th>Quantity</th>\r\n            <th>Amount</th>\r\n            <th><i class=\"icon icon-add\"></i></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n    </tbody>\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"8\" class=\"text-right\">\r\n                Total&nbsp;:<span class=\"text-left J-total\">"
    + alias3(((helper = (helper = helpers.TOTAL || (depth0 != null ? depth0.TOTAL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TOTAL","hash":{},"data":data}) : helper)))
    + "</span>\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<!-- </div> -->\r\n<div class=\"clearfix mt20\">\r\n    <div class=\"pull-left\">\r\n        <span>Currency&nbsp;:</span>USD\r\n        <!--<select class=\"form-control\" id=\"sel_cur\"></select>-->\r\n    </div>\r\n    <div class=\"pull-right\">\r\n        <!-- <button type=\"button\" class=\"btn btn-primary\" id=\"goToSave\">Save/Preview</button> --><button type=\"button\" class=\"btn btn-primary ml10\" id=\"goToBrowse\">Preview</button>\r\n    </div>\r\n</div>";
},"useData":true});
templates['invoiceList'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "<tr>\r\n    <td>\r\n        <fieldset>\r\n            <legend>\r\n                <span class=\"fc-green J-bill-id\">"
    + alias1(this.lambda((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "&nbsp;&nbsp;"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(2, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</span>\r\n            </legend>\r\n            <ul class=\"list-unstyled\" style=\"margin-bottom:0;\">\r\n                <li><span>Reference No&nbsp;:</span>"
    + alias1(((helper = (helper = helpers.REFERENCE_NO || (depth0 != null ? depth0.REFERENCE_NO : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"REFERENCE_NO","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\r\n                <li><span>Invoice Amount&nbsp;:</span>"
    + alias1(((helper = (helper = helpers.SAVE || (depth0 != null ? depth0.SAVE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SAVE","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\r\n                <li><span>User&nbsp;:</span>"
    + alias1(((helper = (helper = helpers.ADMIN_NAME || (depth0 != null ? depth0.ADMIN_NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADMIN_NAME","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\r\n                <li><span>Create Time&nbsp;:</span>"
    + alias1(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "&nbsp;</li>\r\n            </ul>\r\n        </fieldset>\r\n    </td>\r\n    <td>\r\n        <dl class=\"dl-horizontal\">\r\n            <dt>Client ID&nbsp;:</dt>\r\n            <dd class=\"bold\">"
    + alias1(((helper = (helper = helpers.CUSTOMER_ID || (depth0 != null ? depth0.CUSTOMER_ID : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"CUSTOMER_ID","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>Customer Name&nbsp;: </dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_NAME || (depth0 != null ? depth0.ADDRESS_NAME : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_NAME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>Street&nbsp;:</dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_STREET || (depth0 != null ? depth0.ADDRESS_STREET : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_STREET","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>City&nbsp;: </dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_CITY || (depth0 != null ? depth0.ADDRESS_CITY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_CITY","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>State&nbsp;: </dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>Zip&nbsp;: </dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_ZIP || (depth0 != null ? depth0.ADDRESS_ZIP : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_ZIP","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>Tel&nbsp;:</dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.TEL || (depth0 != null ? depth0.TEL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TEL","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>Country&nbsp;:</dt>\r\n            <dd>"
    + alias1(((helper = (helper = helpers.ADDRESS_COUNTRY || (depth0 != null ? depth0.ADDRESS_COUNTRY : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"ADDRESS_COUNTRY","hash":{},"data":data}) : helper)))
    + "</dd>\r\n        </dl>\r\n    </td>\r\n    <td>\r\n        <fieldset>\r\n            <legend>\r\n                <span>Add TASK</span>\r\n                <i class=\"icon icon-add J-add-task\"></i>\r\n            </legend>\r\n            <dl>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </dl>\r\n        </fieldset>\r\n    </td>\r\n    <td>\r\n        <dl>\r\n            <dt>name&nbsp;\r\n                <a href=\"javascript:void(0);\" class=\"fc-green underline log-detail\">do something</a>\r\n            </dt>\r\n            <dd><small>06/09/15 18:04</small></dd>\r\n        </dl>\r\n    </td>\r\n</tr>\r\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.noBtnGroup : depth0),{"name":"unless","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    return "IMPORTED";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    return "CLOSED";
},"7":function(depth0,helpers,partials,data) {
    return "OPEN";
},"9":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                <dd>\r\n                    <fieldset>\r\n                        <legend>\r\n                            <a href=\"javascript:void(0);\" class=\"fc-green underline J-goto-task\" data-task=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "</a>\r\n                            <i class=\"icon icon-remove J-remove-task\"></i>\r\n                        </legend>\r\n                        <dl>\r\n                          <dt>"
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "</dt>\r\n                          <dd class=\"ellipsis\">\r\n                            <p>"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</p>\r\n                            <div>"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</div>\r\n                          </dd>\r\n                        </dl>\r\n                    </fieldset>\r\n                </dd>\r\n";
},"11":function(depth0,helpers,partials,data) {
    var stack1;

  return "<tr>\r\n    <td colspan=\"5\" class=\"text-right tr-btn\">\r\n    "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(12, data, 0),"inverse":this.program(14, data, 0),"data":data})) != null ? stack1 : "")
    + "    </td>\r\n</tr>\r\n";
},"12":function(depth0,helpers,partials,data) {
    return "";
},"14":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "\r\n        <div class=\"btn-group btn-group-sx\" role=\"group\">\r\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-edit\"><i class=\"glyphicon glyphicon-pencil\"> Edit</i></button>\r\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-del\"><i class=\"glyphicon glyphicon-trash\">  Del</i></button>\r\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-close\"><i class=\"glyphicon glyphicon-ban-circle\">  Close</i></button>\r\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-browse\"><i class=\"glyphicon glyphicon-eye-open\">  Preview</i></button>\r\n            <button type=\"button\" data-bill=\""
    + alias2(alias1((depth0 != null ? depth0.BILL_ID : depth0), depth0))
    + "\" class=\"btn btn-default btn-pdf\"><i class=\"glyphicon glyphicon-file\">  PDF</i></button>\r\n        </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['invoiceTemplate'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<fieldset>\r\n    <legend>\r\n        <span>Bill To</span>\r\n    </legend>\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>Client ID&nbsp;:</dt>\r\n        <dd style=\"width: 196px;float: left;margin-left: 20px;\">\r\n            <div class=\"sel_client\">\r\n                <select id=\"sel_client\" class=\"form-control\"></select>\r\n                <input class=\"validate sel-input\" required id=\"client_id\" value=\""
    + alias3(((helper = (helper = helpers.CLIENT_ID || (depth0 != null ? depth0.CLIENT_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CLIENT_ID","hash":{},"data":data}) : helper)))
    + "\"/>\r\n            </div>\r\n        </dd>\r\n        <dt style=\"clear: none;width: 138px;\">Payment Term&nbsp;:</dt>\r\n        <dd style=\"width: 100px;float: left;margin-left: 20px;\"><select id=\"paymentTerm\" class=\"form-control\">\r\n            <option value=\"COD\">COD</option>\r\n            <option value=\"N15\">N15</option>\r\n            <option value=\"NET30\">NET30</option>\r\n            <option value=\"NET40\">NET40</option>\r\n            <option value=\"NET45\">NET45</option>\r\n            <option value=\"NET60\">NET60</option>\r\n        </select></dd>\r\n        <dt>Customer Name&nbsp;:</dt>\r\n        <dd><input class=\"form-control\" id=\"name\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_NAME || (depth0 != null ? depth0.ADDRESS_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_NAME","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>Street&nbsp;: </dt>\r\n        <dd><input class=\"form-control\" id=\"street\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_STREET || (depth0 != null ? depth0.ADDRESS_STREET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STREET","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>City&nbsp;: </dt>\r\n        <dd><input class=\"form-control\" id=\"city\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_CITY || (depth0 != null ? depth0.ADDRESS_CITY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_CITY","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>Zip&nbsp;: </dt>\r\n        <dd><input class=\"form-control\" id=\"zip\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_ZIP || (depth0 != null ? depth0.ADDRESS_ZIP : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_ZIP","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>Country&nbsp;:</dt>\r\n        <dd>\r\n            <div class=\"sel_country\">\r\n                <select id=\"sel_country\" class=\"form-control\"></select>\r\n                <input class=\"validate sel-input\" required id=\"sel_country_val\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_COUNTRY_CODE || (depth0 != null ? depth0.ADDRESS_COUNTRY_CODE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_COUNTRY_CODE","hash":{},"data":data}) : helper)))
    + "\"/>\r\n                <input type=\"hidden\" id=\"sel_country_ccid\" value=\""
    + alias3(((helper = (helper = helpers.CCID || (depth0 != null ? depth0.CCID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CCID","hash":{},"data":data}) : helper)))
    + "\" />\r\n                <input type=\"hidden\" id=\"sel_country_name\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_COUNTRY || (depth0 != null ? depth0.ADDRESS_COUNTRY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_COUNTRY","hash":{},"data":data}) : helper)))
    + "\" />\r\n            </div>\r\n        </dd>\r\n        <dt>State&nbsp;:</dt>\r\n        <dd>\r\n            <div class=\"sel_state\">\r\n                <select id=\"sel_state\" class=\"form-control\"></select>\r\n                <input class=\"validate sel-input\" required id=\"sel_state_val\" value=\""
    + alias3(((helper = (helper = helpers.PRO_ID || (depth0 != null ? depth0.PRO_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PRO_ID","hash":{},"data":data}) : helper)))
    + "\"/>\r\n                <input type=\"hidden\" id=\"sel_state_name\" value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + "\" />\r\n            </div><input class=\"form-control validate\" id=\"state_input\" required value=\""
    + alias3(((helper = (helper = helpers.ADDRESS_STATE || (depth0 != null ? depth0.ADDRESS_STATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ADDRESS_STATE","hash":{},"data":data}) : helper)))
    + "\"/>\r\n        </dd>\r\n        <dt>Attention&nbsp;: </dt>\r\n        <dd><input class=\"form-control\" id=\"attention\" value=\""
    + alias3(((helper = (helper = helpers.ATTENTION || (depth0 != null ? depth0.ATTENTION : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ATTENTION","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>Payer Email&nbsp;: </dt>\r\n        <dd><input class=\"form-control validate\" validType=\"email\" id=\"email\" value=\""
    + alias3(((helper = (helper = helpers.PAYER_EMAIL || (depth0 != null ? depth0.PAYER_EMAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PAYER_EMAIL","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>Tel&nbsp;:</dt>\r\n        <dd><input class=\"form-control\" id=\"tel\" value=\""
    + alias3(((helper = (helper = helpers.TEL || (depth0 != null ? depth0.TEL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TEL","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n        <dt>Fax&nbsp;:</dt>\r\n        <dd><input class=\"form-control\" id=\"fax\" value=\""
    + alias3(((helper = (helper = helpers.FAX || (depth0 != null ? depth0.FAX : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"FAX","hash":{},"data":data}) : helper)))
    + "\"/></dd>\r\n    </dl>\r\n</fieldset>\r\n<div class=\"text-right mt20\">\r\n    <button type=\"button\" class=\"btn btn-primary\" id=\"goToNext\">Next</button>\r\n</div>";
},"useData":true});
templates['invoice_addTask'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<fieldset>\r\n    <legend>\r\n        <a href=\"javascript:;\" class=\"fc-green underline\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "</a>\r\n        <i class=\"icon icon-remove J-remove-task\"></i>\r\n    </legend>\r\n    <dl>\r\n      <dt>"
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "</dt>\r\n      <dd class=\"ellipsis\">\r\n        <p>"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</p>\r\n        <div>"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</div>\r\n      </dd>\r\n    </dl>\r\n</fieldset>";
},"useData":true});
templates['logDetail'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<tr>\r\n				<td> "
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + " </td>\r\n				<td> "
    + alias3(((helper = (helper = helpers.SCH_REPLAY_TIME || (depth0 != null ? depth0.SCH_REPLAY_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_TIME","hash":{},"data":data}) : helper)))
    + " </td>\r\n				<td>"
    + alias3(((helper = (helper = helpers.SCH_REPLAY_CONTEXT || (depth0 != null ? depth0.SCH_REPLAY_CONTEXT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_CONTEXT","hash":{},"data":data}) : helper)))
    + " </td>\r\n			</tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "		    <tr>\r\n		      <td class=\"no-data\" colspan=\"3\">NO Data</td>\r\n		    </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"download-content\">\r\n	<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Employe Name</th>\r\n			<th>Create Time</th>\r\n			<th>Log</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n"
    + ((stack1 = helpers.each.call(depth0,depth0,{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,depth0,{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</tbody>\r\n	</table>\r\n</div>";
},"useData":true});
templates['mainInvoice'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"task-invoice\">\r\n    <div role=\"tabpanel\" class=\"panel panel-default\">\r\n        <div class=\"panel-heading\"><span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\">&nbsp;Invoice</span></div>\r\n        <div class=\"panel-body\">\r\n            <!-- Nav tabs -->\r\n            <ul class=\"nav nav-tabs\" role=\"tablist\">\r\n                <li role=\"presentation\" class=\"active\"><a href=\"#common\" aria-controls=\"common\" role=\"tab\" data-toggle=\"tab\">Common Tools</a></li>\r\n                <li role=\"presentation\"><a href=\"#advanced\" aria-controls=\"advanced\" role=\"tab\" data-toggle=\"tab\">Advanced Search</a></li>\r\n            </ul>\r\n\r\n            <!-- Tab panes -->\r\n            <div class=\"tab-content\">\r\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"common\">\r\n                    <div class=\"col-md-4\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" class=\"form-control J-search-common\" placeholder=\"Search for...\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.query || (depth0 != null ? depth0.query : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"query","hash":{},"data":data}) : helper)))
    + "\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button class=\"btn btn-info J-btn-search\" type=\"button\"><span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span></button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4\"></div>\r\n                    <div class=\"col-md-4 text-center\">\r\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_invoice\">Add Invoice</button>\r\n                    </div>\r\n                </div>\r\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\r\n                    <!-- todo -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-container\">\r\n            <table class=\"table table-hover table-condensed\" id=\"invoice_table\">\r\n                <thead>\r\n                <tr>\r\n                    <th>Invoice Information</th>\r\n                    <th>Address</th>\r\n                    <th>Relation</th>\r\n                    <th>Log</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                </tbody>\r\n            </table>\r\n            <div class=\"modal\"><div class=\"loading\"></div></div>\r\n        </div>\r\n        <ul id=\"pagebox_invoice\" class='clearfix pagebox mb20 mt20'></ul>\r\n    </div>\r\n</div>";
},"useData":true});
templates['mainTask'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"task-invoice\">\r\n    <div role=\"tabpanel\" class=\"panel panel-default\">\r\n        <div class=\"panel-heading\"><span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\">&nbsp;Task</span></div>\r\n        <div class=\"panel-body\">\r\n            <!-- Nav tabs -->\r\n            <ul class=\"nav nav-tabs\" role=\"tablist\">\r\n                <li role=\"presentation\" class=\"active\"><a href=\"#common\" aria-controls=\"common\" role=\"tab\" data-toggle=\"tab\">Common Tools</a></li>\r\n                <li role=\"presentation\"><a href=\"#advanced\" aria-controls=\"advanced\" role=\"tab\" data-toggle=\"tab\">Advanced Search</a></li>\r\n            </ul>\r\n\r\n            <!-- Tab panes -->\r\n            <div class=\"tab-content\">\r\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"common\">\r\n                    <div class=\"col-md-4\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" class=\"form-control J-search-common\" placeholder=\"Search for...\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.query || (depth0 != null ? depth0.query : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"query","hash":{},"data":data}) : helper)))
<<<<<<< HEAD
    + "\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button class=\"btn btn-info J-btn-search\" type=\"button\"><span class=\"glyphicon glyphicon-search J-btn-search\" aria-hidden=\"true\"></span></button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4\"></div>\r\n                    <div class=\"col-md-4 text-center\">\r\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_task\">Add Task</button>\r\n                    </div>\r\n                </div>\r\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\r\n                    <!-- todo -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-container\">\r\n            <table class=\"table table-hover table-condensed\" id=\"task_table\">\r\n                <thead>\r\n                <tr>\r\n                    <th>Information</th>\r\n                    <th>Content</th>\r\n                    <th>Sub Task</th>\r\n                    <th>Invoice</th>\r\n                    <th>Log</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                </tbody>\r\n            </table>\r\n            <div class=\"modal\"><div class=\"loading\"></div></div>\r\n        </div>\r\n        <ul id=\"pagebox_task\" class='clearfix pagebox mb20 mt20'></ul>\r\n    </div>\r\n</div>";
=======
    + "\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button class=\"btn btn-info J-btn-search\" type=\"button\"><span class=\"glyphicon glyphicon-search J-btn-search\" aria-hidden=\"true\"></span></button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-4\"></div>\r\n                    <div class=\"col-md-4 text-center\">\r\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_task\">Add Project</button>\r\n                    </div>\r\n                </div>\r\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\r\n                    <!-- todo -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-container\">\r\n            <table class=\"table table-hover table-condensed\" id=\"task_table\">\r\n                <thead>\r\n                <tr>\r\n                    <th>Information</th>\r\n                    <th>Content</th>\r\n                    <th>Sub Task</th>\r\n                    <th>Invoice</th>\r\n                    <th>Log</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                </tbody>\r\n            </table>\r\n            <div class=\"modal\"><div class=\"loading\"></div></div>\r\n        </div>\r\n        <ul id=\"pagebox_task\" class='clearfix pagebox mb20 mt20'></ul>\r\n    </div>\r\n</div>";
>>>>>>> task_invoice
},"useData":true});
templates['selectOptions'] = template({"1":function(depth0,helpers,partials,data) {
    return "";
},"3":function(depth0,helpers,partials,data) {
    return "<option value=\"\"></option>\r\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "<option value=\"-1\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isHand : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">by-hand input</option>\r\n";
},"6":function(depth0,helpers,partials,data) {
    return "selected";
},"8":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.TEXT || (depth0 != null ? depth0.TEXT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TEXT","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"10":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option data-id=\""
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.ACCOUNT_NAME || (depth0 != null ? depth0.ACCOUNT_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT_NAME","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"12":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.C_CODE || (depth0 != null ? depth0.C_CODE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"C_CODE","hash":{},"data":data}) : helper)))
    + "\" data-ccid=\""
    + alias3(((helper = (helper = helpers.CCID || (depth0 != null ? depth0.CCID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CCID","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.C_COUNTRY || (depth0 != null ? depth0.C_COUNTRY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"C_COUNTRY","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.PRO_ID || (depth0 != null ? depth0.PRO_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PRO_ID","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.PRO_NAME || (depth0 != null ? depth0.PRO_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PRO_NAME","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"16":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option data-target=\""
    + alias3(((helper = (helper = helpers.TARGET || (depth0 != null ? depth0.TARGET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TARGET","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias3(((helper = (helper = helpers.VALUE || (depth0 != null ? depth0.VALUE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"VALUE","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.TARGET || (depth0 != null ? depth0.TARGET : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TARGET","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"18":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<option value=\""
    + alias3(((helper = (helper = helpers.CUSTOMER_KEY || (depth0 != null ? depth0.CUSTOMER_KEY : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CUSTOMER_KEY","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.sel : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.CUSTOMER_ID || (depth0 != null ? depth0.CUSTOMER_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CUSTOMER_ID","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isProto : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isCanEnter : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.f : depth0),{"name":"each","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.p : depth0),{"name":"each","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.country : depth0),{"name":"each","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.s : depth0),{"name":"each","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.cur : depth0),{"name":"each","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.client : depth0),{"name":"each","hash":{},"fn":this.program(18, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
<<<<<<< HEAD
templates['sortedSubTasks'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),4,"<",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "        <dd>\r\n            <div class=\"header\">\r\n               <a href=\"javascript:void(0);\" data-task=\""
    + alias1(this.lambda((depths[2] != null ? depths[2].SCHEDULE_ID : depths[2]), depth0))
    + "\" class=\"J-sub-detail\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "           	\">\r\n                "
    + alias1(((helper = (helper = helpers.TASK_NUMBER || (depth0 != null ? depth0.TASK_NUMBER : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TASK_NUMBER","hash":{},"data":data}) : helper)))
    + ".&nbsp; "
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\r\n               </a>\r\n            </div>\r\n            \r\n        </dd>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "         			 fc-green   \r\n";
},"5":function(depth0,helpers,partials,data) {
=======
templates['selectUser'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "						<option value='"
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "' "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].admin : depths[1])) != null ? stack1.ps_id : stack1),(depth0 != null ? depth0.ps_id : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.ps_name || (depth0 != null ? depth0.ps_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_name","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"2":function(depth0,helpers,partials,data) {
    return " selected='selected' ";
},"4":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<option value=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].admin : depths[1])) != null ? stack1.dept_id : stack1),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"5":function(depth0,helpers,partials,data) {
    return " selected=\"selected\" ";
},"7":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "				<option value=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depths[1] != null ? depths[1].admin : depths[1])) != null ? stack1.post_id : stack1),(depth0 != null ? depth0.key : depth0),"=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">"
    + alias3(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"select-user\">\r\n	<div class=\"select-condition\">\r\n		WareHouse:<select name=\"ps_id\" id=\"ps_id\">admin\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.productCatelog : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		 		</select>\r\n				\r\n		Group:<select name=\"group\" id=\"group\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.groupJson : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</select>\r\n		User:<select name=\"proJsId\" id=\"proJsId\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.userJson : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				 \r\n			</select>\r\n		<div class=\"div-btn text-center\">\r\n	        <button class=\"btn btn-info J-user-search\" type=\"button\"><span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span></button>\r\n	    </div> \r\n    </div>\r\n    <div class=\"user-info\"><div>\r\n</div>\r\n";
},"useData":true,"useDepths":true});
templates['sortedSubTasks'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),4,"<",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing, alias3="function";

  return "        <dd>\r\n            <div class=\"header\">\r\n               <a href=\"javascript:void(0);\" data-task=\""
    + alias1(this.lambda((depths[2] != null ? depths[2].SCHEDULE_ID : depths[2]), depth0))
    + "\" class=\"J-sub-detail\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "           	\">\r\n                "
    + alias1(((helper = (helper = helpers.TASK_NUMBER || (depth0 != null ? depth0.TASK_NUMBER : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"TASK_NUMBER","hash":{},"data":data}) : helper)))
    + ".&nbsp; "
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\r\n               </a>\r\n            </div>\r\n            \r\n        </dd>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "         			 fc-green   \r\n";
},"5":function(depth0,helpers,partials,data) {
>>>>>>> task_invoice
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(6, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"6":function(depth0,helpers,partials,data) {
    return "         				 fc-gray \r\n";
},"8":function(depth0,helpers,partials,data) {
    return "         				fc-red\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['sortSubtasks'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	  	<a href=\"javascript:void(0);\" class=\"list-group-item J-sort-item\" data-schedule=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"><span class=\"J-note\">"
    + alias3((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + "</span>.&nbsp;"
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "</a>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"clearfix sort-sub\">\r\n	<div class=\"list-group pull-left\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\r\n	<div class=\"pull-right\">\r\n		<div class=\"sort-btn\">\r\n			<button class=\"btn btn-default J-btn-up\"><i class='glyphicon glyphicon-arrow-up'></i></button>\r\n			<button class=\"btn btn-default J-btn-down\"><i class='glyphicon glyphicon-arrow-down'></i></button>\r\n		</div>\r\n	</div>\r\n</div>";
<<<<<<< HEAD
},"useData":true});
templates['subTaskDetail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"subtask-detail\">\r\n<!--	<div class=\"add-btn\"> \r\n		<button class=\"btn btn-default\" type=\"button\" name=\"show_sub_task\" id=\"show_sub_task\" >Add</button>\r\n	</div>\r\n-->	 \r\n		<div class=\"add-subtask\" id=\"add-subtask\"></div>\r\n	\r\n<!--	<div class=\"chevron-up\"> <i class=\"glyphicon glyphicon-chevron-up\" name=\"hid_sub_task\" id=\"hid_sub_task\"></i></div> -->\r\n	<div class=\"subtask-list\" id=\"subtask-list\"></div>\r\n</div>";
},"useData":true});
=======
},"useData":true});
templates['subTaskDetail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"subtask-detail\">\r\n<!--	<div class=\"add-btn\"> \r\n		<button class=\"btn btn-default\" type=\"button\" name=\"show_sub_task\" id=\"show_sub_task\" >Add</button>\r\n	</div>\r\n-->	 \r\n		<div class=\"add-subtask\" id=\"add-subtask\"></div>\r\n	\r\n<!--	<div class=\"chevron-up\"> <i class=\"glyphicon glyphicon-chevron-up\" name=\"hid_sub_task\" id=\"hid_sub_task\"></i></div> -->\r\n	<div class=\"subtask-list\" id=\"subtask-list\"></div>\r\n</div>";
},"useData":true});
>>>>>>> task_invoice
templates['subTaskList'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2=this.escapeExpression, alias3="function";

  return "				<tr>\r\n					<td class=\"w5\">"
    + alias2((helpers.math || (depth0 && depth0.math) || alias1).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + " </td>\r\n					<td class=\"w20\"> "
    + alias2(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + " </td>\r\n					<td class=\"w3\">"
    + alias2(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias3 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + " </td>\r\n					<td class=\"w12\"> \r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Pending : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Proccessing : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</td>\r\n					<td class=\"w12\">\r\n						<div class=\"opr-btns\">\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"=",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.TASK_STATUS : depth0),((stack1 = (depths[1] != null ? depths[1].taskStateKey : depths[1])) != null ? stack1.Closed : stack1),"<",{"name":"xifCond","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\r\n                     </td>\r\n				</tr>\r\n";
},"2":function(depth0,helpers,partials,data) {
    return "	                        	Pending&nbsp;\r\n";
},"4":function(depth0,helpers,partials,data) {
    return "	                        	Proccessing&nbsp;\r\n";
},"6":function(depth0,helpers,partials,data) {
    return "	                        	Closed&nbsp;\r\n";
},"8":function(depth0,helpers,partials,data) {
    return "	                        	&nbsp;\r\n";
},"10":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	                           <i class=\"icon icon-edit\" title=\"edit\" name=\"editSubTask\" data-subtask=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\" data-task="
    + alias3(((helper = (helper = helpers.PARENT_SCHEDULE_ID || (depth0 != null ? depth0.PARENT_SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PARENT_SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "></i>\r\n	                           <i class=\"icon icon-remove\" title=\"del\" name=\"delSubTask\" data-subtask=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\" data-task="
    + alias3(((helper = (helper = helpers.PARENT_SCHEDULE_ID || (depth0 != null ? depth0.PARENT_SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PARENT_SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "></i>\r\n	                           <i class=\"icon icon-close\" title=\"close\"  name=\"closeSubTask\" data-subject="
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + " data-subtask="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " data-task="
    + alias3(((helper = (helper = helpers.PARENT_SCHEDULE_ID || (depth0 != null ? depth0.PARENT_SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PARENT_SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "></i> \r\n";
},"12":function(depth0,helpers,partials,data) {
    return "			    <tr>\r\n			      <td class=\"no-data\" colspan=\"5\">NO Data</td>\r\n			    </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<table class=\"table table-striped sub-task-list\">\r\n		<thead>\r\n			<tr>\r\n				<th>Line</th>\r\n				<th>Task</th>\r\n				<th>To</th>\r\n				<th>State</th>\r\n				<th>Operate</th>\r\n			</tr>\r\n		</thead>\r\n		<tbody>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"unless","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</tbody>\r\n		</table>";
},"useData":true,"useDepths":true});
templates['taskList'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<tr>\r\n    <td>\r\n        <fieldset> \r\n            <legend>\r\n                <span class=\"blod\">"
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\r\n            </legend>\r\n            <dl class=\"dl-horizontal\">\r\n                <dt>From&nbsp;:</dt>\r\n                <dd>"
    + alias3(((helper = (helper = helpers.ASSEMPLOYNAME || (depth0 != null ? depth0.ASSEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ASSEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n                <dt>To&nbsp;: </dt>\r\n                <dd class=\"ellipsis\">\r\n                    <p>\r\n                     "
    + alias3(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\r\n                    </p>\r\n                    <div>\r\n                      "
    + alias3(((helper = (helper = helpers.EXEEMPLOYNAME || (depth0 != null ? depth0.EXEEMPLOYNAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EXEEMPLOYNAME","hash":{},"data":data}) : helper)))
    + "\r\n                    </div>\r\n                </dd>\r\n                <dt>Start&nbsp;: </dt>\r\n                <dd>"
    + alias3(((helper = (helper = helpers.START_TIME || (depth0 != null ? depth0.START_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"START_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n                <dt>Deadline&nbsp;: </dt>\r\n                <dd>"
    + alias3(((helper = (helper = helpers.END_TIME || (depth0 != null ? depth0.END_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"END_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n                <dt>Create Time&nbsp;:</dt>\r\n                <dd>"
    + alias3(((helper = (helper = helpers.CREATE_TIME || (depth0 != null ? depth0.CREATE_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n                <dt>Finish Time&nbsp;: </dt>\r\n                <dd>"
    + alias3(((helper = (helper = helpers.CLOSE_TIME || (depth0 != null ? depth0.CLOSE_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CLOSE_TIME","hash":{},"data":data}) : helper)))
    + "</dd>\r\n                <dt>Relation&nbsp;:</dt>\r\n                <dd></dd>\r\n            </dl>\r\n        </fieldset>\r\n    </td>\r\n    <td>\r\n        <dl>\r\n            <dt>Subject</dt>\r\n            <dd>"
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "</dd>\r\n            <dt>Description</dt>\r\n            <dd class=\"ellipsis\">\r\n                <p>\r\n                 "
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\r\n                </p>\r\n                <div>\r\n                  "
    + alias3(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\r\n                </div>\r\n            </dd>\r\n        </dl>\r\n    </td>\r\n    \r\n    <td>\r\n        <fieldset>\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(9, data, 0, blockParams, depths),"inverse":this.program(11, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            \r\n            <dl>\r\n               \r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBSCHEDULE : depth0),{"name":"each","hash":{},"fn":this.program(13, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </dl>\r\n            \r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBSCHEDULE : depth0)) != null ? stack1.length : stack1),4,">",{"name":"xifCond","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </fieldset>\r\n    </td>\r\n    <td>\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(24, data, 0, blockParams, depths),"inverse":this.program(33, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "    </td>\r\n    <td>\r\n        <dl>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBLOG : depth0),{"name":"each","hash":{},"fn":this.program(36, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.SUBLOG : depth0),{"name":"unless","hash":{},"fn":this.program(39, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </dl>\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBLOG : depth0)) != null ? stack1.length : stack1),2,">",{"name":"xifCond","hash":{},"fn":this.program(41, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </td>\r\n</tr>\r\n<tr> \r\n    <td colspan=\"5\" class=\"text-right tr-btn\">\r\n        <div class=\"btn-group btn-group-sx\" role=\"group\">\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(43, data, 0, blockParams, depths),"inverse":this.program(45, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "        </div>\r\n    </td>\r\n</tr>\r\n";
},"2":function(depth0,helpers,partials,data) {
    return "Proccessing";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    return "Closed";
},"7":function(depth0,helpers,partials,data) {
    return "Pending";
},"9":function(depth0,helpers,partials,data) {
    return "        		<legend>\r\n                	<span>Sub Task</span>\r\n            	</legend>\r\n";
},"11":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        		<legend>\r\n	                <span>Add Sub Task</span>\r\n	                <i class=\"icon icon-add\" name=\"addSubTask\" title=\"add\" data-task=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\" data-subject=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_OVERVIEW || (depth0 != null ? depth0.SCHEDULE_OVERVIEW : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_OVERVIEW","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n	                <i class=\"icon icon-sort J-sub-sort\" title=\"sort\" data-task=\""
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n	            </legend>\r\n";
},"13":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "               \r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),4,"<",{"name":"xifCond","hash":{},"fn":this.program(14, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"14":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.escapeExpression, alias2=helpers.helperMissing;

  return "                <dd>\r\n                    <div class=\"header\">\r\n                       <a href=\"javascript:void(0);\" data-task=\""
    + alias1(this.lambda((depths[2] != null ? depths[2].SCHEDULE_ID : depths[2]), depth0))
    + "\" class=\"J-sub-detail\r\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias2).call(depth0,"this.TASK_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(15, data, 0, blockParams, depths),"inverse":this.program(17, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                   	\">\r\n                        "
    + alias1((helpers.math || (depth0 && depth0.math) || alias2).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + ".&nbsp; "
    + alias1(((helper = (helper = helpers.SCHEDULE_DETAIL || (depth0 != null ? depth0.SCHEDULE_DETAIL : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_DETAIL","hash":{},"data":data}) : helper)))
    + "\r\n                       </a>\r\n                    </div>\r\n                    \r\n                </dd>\r\n                \r\n";
},"15":function(depth0,helpers,partials,data) {
    return "                 			 fc-green   \r\n";
},"17":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.TASK_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(18, data, 0),"inverse":this.program(20, data, 0),"data":data})) != null ? stack1 : "");
},"18":function(depth0,helpers,partials,data) {
    return "                 				 fc-gray \r\n";
},"20":function(depth0,helpers,partials,data) {
    return "                 				fc-red\r\n";
},"22":function(depth0,helpers,partials,data,blockParams,depths) {
    return "        		<a href=\"javascript:void(0);\" data-task=\""
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" class=\"fc-orange fc-right underline J-sub-detail\">More...</a>\r\n";
},"24":function(depth0,helpers,partials,data) {
    var stack1;

  return "	    		<fieldset>\r\n		            <legend>\r\n		                <span>Add Invoice</span>\r\n		            </legend>\r\n		            <dl>\r\n		               \r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(25, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		            </dl>\r\n		        </fieldset>\r\n    \r\n";
},"25":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		                \r\n		                <dd>\r\n		                    <fieldset class=\"invoice\">\r\n		                        <legend>\r\n		                           <a href=\"javascript:void(0);\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"fc-green underline J-goto-invoice\">&nbsp;"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(26, data, 0),"inverse":this.program(28, data, 0),"data":data})) != null ? stack1 : "")
    + "</a>\r\n		                        </legend>\r\n		                        <dl>\r\n		                            <dt>&nbsp;"
    + alias3(((helper = (helper = helpers.ACCOUNT || (depth0 != null ? depth0.ACCOUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ACCOUNT","hash":{},"data":data}) : helper)))
    + "</dt>\r\n		                            <dd><span>Create Time&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\r\n		                        </dl>\r\n		                    </fieldset>\r\n		                </dd>\r\n		                \r\n";
},"26":function(depth0,helpers,partials,data) {
    return "IMPORTED";
},"28":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.INVOICE_STATUS == 3",{"name":"xif","hash":{},"fn":this.program(29, data, 0),"inverse":this.program(31, data, 0),"data":data})) != null ? stack1 : "");
},"29":function(depth0,helpers,partials,data) {
    return "CLOSED";
},"31":function(depth0,helpers,partials,data) {
    return "OPEN";
},"33":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper;

  return "	    	<fieldset>\r\n	            <legend>\r\n	                <span>Add Invoice</span>\r\n	                <i class=\"icon icon-add\" title=\"add invoice\" name=\"addInvoice\" data-task=\""
    + this.escapeExpression(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n	            </legend>\r\n	            <dl>\r\n	               \r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.SUBINVOICE : depth0),{"name":"each","hash":{},"fn":this.program(34, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	            </dl>\r\n	        </fieldset>\r\n";
},"34":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	                \r\n	                <dd>\r\n	                    <fieldset class=\"invoice\">\r\n	                        <legend>\r\n	                            <a href=\"javascript:void(0);\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" class=\"fc-green underline J-goto-invoice\">&nbsp;"
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "&nbsp;&nbsp;"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias1).call(depth0,"this.INVOICE_STATUS == 1",{"name":"xif","hash":{},"fn":this.program(26, data, 0, blockParams, depths),"inverse":this.program(28, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</a>\r\n	                            <i class=\"icon icon-remove\" title=\"del invoice\" name=\"delInvoice\" data-invoice=\""
    + alias3(((helper = (helper = helpers.BILL_ID || (depth0 != null ? depth0.BILL_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"BILL_ID","hash":{},"data":data}) : helper)))
    + "\" data-task="
    + alias3(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "></i>\r\n	                        </legend>\r\n	                        <dl>\r\n	                            <dt><span>Reference No&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.REFERENCE_NO || (depth0 != null ? depth0.REFERENCE_NO : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"REFERENCE_NO","hash":{},"data":data}) : helper)))
    + "<button class=\"btn btn-default btn-xs\" type=\"button\">PDF</button></dt>\r\n	                            <dd><span>Create Time&nbsp;:</span>&nbsp;"
    + alias3(((helper = (helper = helpers.CREATE_DATE || (depth0 != null ? depth0.CREATE_DATE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CREATE_DATE","hash":{},"data":data}) : helper)))
    + "</dd>\r\n	                        </dl>\r\n	                    </fieldset>\r\n	                </dd>\r\n	                \r\n";
},"36":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),3,"<",{"name":"xifCond","hash":{},"fn":this.program(37, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"37":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	            	<dt>"
    + alias3(((helper = (helper = helpers.EMPLOYE_NAME || (depth0 != null ? depth0.EMPLOYE_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"EMPLOYE_NAME","hash":{},"data":data}) : helper)))
    + " &nbsp;\r\n	            		<a href=\"javascript:void(0);\" data-task=\""
    + alias3(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" class=\"fc-green underline log-detail\">"
    + alias3(((helper = (helper = helpers.SCH_REPLAY_CONTEXT || (depth0 != null ? depth0.SCH_REPLAY_CONTEXT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_CONTEXT","hash":{},"data":data}) : helper)))
    + "</a>\r\n	            	</dt>\r\n	            	<dd><span>"
    + alias3(((helper = (helper = helpers.SCH_REPLAY_TIME || (depth0 != null ? depth0.SCH_REPLAY_TIME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCH_REPLAY_TIME","hash":{},"data":data}) : helper)))
    + "<span></dd>\r\n";
},"39":function(depth0,helpers,partials,data) {
    return "            	<dt>&nbsp;</dt> \r\n            	<dd></dd>\r\n";
},"41":function(depth0,helpers,partials,data,blockParams,depths) {
    return "        <a href=\"javascript:void(0);\" data-task=\""
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].SCHEDULE_ID : depths[1]), depth0))
    + "\" class=\"fc-orange fc-right underline log-detail\">More...</a>\r\n";
},"43":function(depth0,helpers,partials,data) {
    return "        &nbsp;\r\n";
},"45":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "            <button type=\"button\" name=\"editTask\" class=\"btn btn-default\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + ">\r\n            	<i class=\"glyphicon glyphicon-pencil\"> Edit</i>\r\n            </button>\r\n            <button type=\"button\" name=\"delTask\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " class=\"btn btn-default\">\r\n            	<i class=\"glyphicon glyphicon-trash\">  Del</i>\r\n            </button>\r\n            <button type=\"button\" name=\"closeTask\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " class=\"btn btn-default\">\r\n            	<i class=\"glyphicon glyphicon-ban-circle\">  Close</i>\r\n           </button>\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.SUBFILE : depth0)) != null ? stack1.length : stack1),0,">",{"name":"xifCond","hash":{},"fn":this.program(46, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "           <!--  <button type=\"button\" name=\"addLog\" class=\"btn btn-default\" data-task="
    + alias3(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + ">\r\n            	<i class=\"glyphicon glyphicon-bookmark\"> Log</i>\r\n            </button> -->\r\n";
},"46":function(depth0,helpers,partials,data) {
    var helper;

  return "            <button type=\"button\" name=\"downloadSOP\" data-task="
    + this.escapeExpression(((helper = (helper = helpers.SCHEDULE_ID || (depth0 != null ? depth0.SCHEDULE_ID : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"SCHEDULE_ID","hash":{},"data":data}) : helper)))
    + " class=\"btn btn-default\r\n             btn-warning\">\r\n            	<i class=\"glyphicon glyphicon-download-alt\"> SOP</i>\r\n            </button>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['uploadSOP'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div style=\"width:770px;height:450px;padding: 10px;box-shadow: 0 0 5px #ccc;\">\r\n	<div id=\"upload_file\">\r\n	</div>\r\n</div>\r\n";
},"useData":true});
})();