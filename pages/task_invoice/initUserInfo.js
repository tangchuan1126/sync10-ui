/**
 * Created by gql on 2015-6-16
    user选中的
    data:[]
    defValue:""
 */
"use strict";
define([
    "jquery"
],function($){
	
    
	var initUserInfo = function(data, defValue, group){
		var result = [];
		if(data&&data.length>0){
			if(group==-1){
				result = setUserByGroup(data, defValue);
			}else{
				var objs = new Object();
				objs.deptName = data[0].NAME;
				objs.deptId = data[0].ADGID;
				objs.values = setDefValue(data, defValue);
				result.push(objs);
			}
			
			return result;
		}
	}
	
	function setUserByGroup(data, defValue){
		var newa = [];
		var userInfoDept = data;
		for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
			//分组
			var flag = true;
			for(var j = 0 , i = newa.length ; j < i ; j++ ){
				if(newa[j] && (newa[j].deptId === userInfoDept[index].ADGID)){
					flag = false;
					break;
				}
			}
			
			/*
			//set checked user
			if(defValue.indexOf(userInfoDept[index].ADID) >= 0){
				userInfoDept[index].checked="checked";
			}*/
			
			//dept add user
			if(flag){ //new dept
				var objs = new Object();
				objs.deptName = userInfoDept[index].NAME;
				objs.deptId = userInfoDept[index].ADGID;
				objs.values = [];
				objs.values.push(userInfoDept[index]); 
				newa.push(objs);
			} else{
				newa[newa.length - 1].values.push(userInfoDept[index]);	
			}
		} 
		
		return newa;
	}
	
	//set checked user
	function setDefValue(data, defValue){
		if(defValue && data && data.length>0 && defValue.length>0){
 			var newa = [];
 			for(var i = 0; i < data.length ; i++){
 				var temp = data[i];
 				if(defValue.indexOf(temp.ADID) >= 0){
 					temp.checked="checked";
 				}
 				newa.push(temp);
 			}
 			return newa;
		}else{
			return data;
		}
	}

    return initUserInfo;
});