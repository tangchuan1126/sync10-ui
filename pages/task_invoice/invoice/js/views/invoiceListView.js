/**
 * Created by huhy on 2015-3-30 10:27:16
 */
 "use strict";
 define([
    "jquery",
    "backbone",
    'handlebars.runtime',
    "handlebars_ext",
    "../../../templates/templates.amd.js",
    "./../models/invoiceCollection.js",
    "Paging",
    "./addInvoiceView.js",
    "/Sync10-ui/pages/task_invoice/task/js/views/addTaskView.js",
    "/Sync10-ui/pages/task_invoice/task/js/models/models.js",
    "../../../artDialog.js",
    "../../../config.js",
    " /Sync10-ui/pages/task_invoice/key/LRTypeKey.js",
    "require_css!/Sync10-ui/pages/task_invoice/css/add_task.css",
    "../../../jquery.dotdotdot.min"
    ],function($, Backbone, Handlebars, Handlebars_ext, templates, InvoiceCollection, Paging, AddInvoiceView,
      AddTaskView, TaskModel, Dialog, config, LRTypeKey){

        return Backbone.View.extend({
            template: templates.invoiceList,
            collection: new InvoiceCollection(),
            initialize: function(option){
                this.loadingModal = option.loadingModal;
                this.setElement(option.el);
                this.admin = option.admin;
                this.showErrorMessage = option.showErrorMessage;
            },
            render:function(){

                this.loadingModal.show();

            this.searchForList();       //查询数据，构造列表
            
        },
        searchForList: function(){

            var text = $.trim($('.J-search-common').val());

            this.buildList(text);
        },
        buildList: function(text){

            var _self = this;

            _self.collection.reset();

            _self.collection.fetch({
                data: $.extend({}, _self.collection.pageCtrl, {'BILL_ID' : text}),
                success: function(collection){

                    var data_arr = collection.toJSON();

                    if(!data_arr.length){
                        _self.$el.html('<tr><td colspan="4"><div class="mt20 mb20 text-center">No Data<div></td></tr>');
                        _self.loadingModal.hide();
                        return false;
                    }

                    _self.$el.html(_self.template({'list' : data_arr, 'LRTypeKey':LRTypeKey})).find('p').each(function(i, elem){
                        var $self = $(elem);
                        $self.dotdotdot({
                            callback : function( isTruncated, orgContent ) {
                                if(isTruncated){
                                    $self.parent().addClass("ellipsis-p");
                                }
                            }
                        });
                    });

                    //总页数，当前页号，容器id，回调
                    Paging.update_page(collection.pageCtrl.pageCount, collection.pageCtrl.pageNo, "pagebox_invoice", function(pageNo) {
                        collection.pageCtrl.pageNo = pageNo;
                        _self.loadingModal.show();
                        $('body').animate( {
                            scrollTop : 0
                        }, 400);
                        _self.render(); 
                    });

                    _self.loadingModal.hide();

                    //$('.art-popup-modal').remove();
                },
                error: function(){
                    _self.$el.html('<tr><td colspan="4"><div class="mt20 mb20 text-center">No Data<div></td></tr>');
                    _self.loadingModal.hide();
                    _self.showErrorMessage();
                },
                cache: false
            });
},
events: {
    'click .btn-edit' : 'editInvoice',
    'click .J-add-task' : 'addTask',
    'click .J-remove-task' : 'removeTask',
    'click .btn-del' : 'delInvoice',
    'click .btn-close' : 'closeInvoice',
    'click .btn-browse' : 'browseInvoice',
    'click .btn-pdf' : 'drawingPDF',
    'click .J-goto-task': 'gotoTask',
    "click .J-print" : "print"
},
editInvoice: function(e){
    var $tr = $(e.currentTarget).parents('tr:eq(0)').prev(),
    billId = $(e.currentTarget).data('bill') * 1,
    oldModel = this.collection.findWhere({'BILL_ID' : billId}),
    _self = this;

    if(!oldModel){
        throw new Error('Not find Model.');
    }

    new AddInvoiceView({'admin': this.admin, 'loadingModal': this.loadingModal, 'title': 'Edit Invoice', 'saveCallback': function(model, response, dialog){


        if(response.success){

            dialog.close();

                    //修改成功，更新tr里的内容
                    _self.collection.remove(oldModel);
                    _self.collection.add(model);    //更新collection里的当前model
                    model.set('noBtnGroup', true);   //不装载btn-tr

                    $tr.after(_self.template({'list' : [model.toJSON()], 'LRTypeKey':LRTypeKey})).remove();
                    
                }else{
                    $.unblockUI();
                    _self.showErrorMessage();
                }

            }}).render(oldModel);
},
delInvoice: function(e){
    var _self = this;

    var dialog = new Dialog({
        content: '<p style="margin-top:40px;text-align:center;">Are you sure you want to delete this invoice ?</p>',
        lock:true,
        width: 400,
        height: 100,
        title:'Delete Invoice',
        okValue:'Ok',
        ok: function (event) {
                    //var $target = $(e.currentTarget),
                       // billId = parseInt($target.parents('tr:eq(0)').prev().find('.J-bill-id').text()),
                       var billId = $(e.currentTarget).data('bill') * 1,
                       model = _self.collection.findWhere({'BILL_ID' : billId});

                       $.blockUI({message:'<img src="../image/loadingAnimation6.gif" align="absmiddle"/>'});

                       model.url = model.url + "?BILL_ID=" + billId;
                       model.destroy({
                        success: function(model, response) {
                            dialog.close();
                            $.unblockUI();
                            _self.render();     //刷新列表
                        },
                        error: function(){
                            dialog.close();
                            $.unblockUI();
                            _self.showErrorMessage();
                        }
                    });
                   },
                   cancelValue: 'Cancel',
                   cancel: function(){
                    dialog.close();
                }
            }).showModal();
},
closeInvoice: function(e){
    var _self = this;
    var billId = $(e.currentTarget).data('bill') * 1,
    model = _self.collection.findWhere({'BILL_ID' : billId}),
    tasks = model.toJSON()['SCHEDULE'];

            //判断是否可以被关闭
            for(var i = 0, len = !tasks ? 0 : tasks.length; i < len; i++){
                if(tasks[i]['TASK_STATUS'] != 3){
                    _self.showErrorMessage(' All the Tasks must be closed.');
                    return;
                }
            }
            var dialog = new Dialog({
                content: '<p style="margin-top:40px;text-align:center;">Are you sure you want to close this invoice ?</p>',
                lock:true,
                width: 400,
                height: 100,
                title:'Close Invoice',
                okValue:'Ok',
                ok: function (event) {
                    $.blockUI({message:'<img src="../image/loadingAnimation6.gif" align="absmiddle"/>'});

                    var url = config.selectUrl + '?key=closeInvoice&bill_id=' + billId;
                    var xhr = new XMLHttpRequest(); 
                    xhr.open('GET', url, true); 
                    xhr.responseType = "json";
                    xhr.onreadystatechange = function () { 
                        if (xhr.readyState == 4 && xhr.response.success) {
                            dialog.close();
                            //去掉操作按钮...
                            //$(e.currentTarget).parent().remove();
                            $.unblockUI();
                            _self.render();     //刷新列表
                        }
                    };
                    xhr.onerror = function(){
                        dialog.close();
                        $.unblockUI();
                        _self.showErrorMessage();
                    };

                    xhr.send();
                },
                cancelValue: 'Cancel',
                cancel: function(){
                    dialog.close();
                }
            }).showModal();
},
browseInvoice: function(e){
    var billId = $(e.currentTarget).data('bill') * 1,
    model = this.collection.findWhere({'BILL_ID' : billId});
    model = this.setRefernceNo(model);

    model.set('noBtn', true);

    var dialog = new Dialog({
        content: $('<div id="browse" class="task-invoice" style="padding:5px 10px;height:100%;overflow-y:auto;"></div>').append(templates.invoiceBrowse(model.toJSON())),
        lock:true,
        width: 804,
        height: 580,
        title:'Invoice Preview',
        cancel: function(){
            model.set('noBtn', false);
            dialog.close();
        },
        cancelDisplay: false
    }).showModal();


    var $container = $('#browseTable_container');
    var theadH = $container.find('thead').outerHeight();
    var tfootH = $container.find('tfoot').outerHeight();
    var tbodyH = $container.find('tbody').outerHeight();
    var unitH = 982 * 98 / 100 - 30 - theadH - tfootH;

    var mulriple = Math.ceil(tbodyH / unitH);

    var difference = unitH * mulriple - tbodyH;

    if(difference > 30){
        $container.find('tbody').append('<tr >'+
          '<td style="border-right-width:0; border-left: 2px solid #333;"></td>'+
          '<td style="border-left-width: 0; "></td>'+
          '<td><div style=" max-width: 450px; word-break: break-all; text-align: left;"></div></td>'+
          '<td><div style="height:'+ difference+'px; margin:0; padding:0;"></div></td><td></td></tr>');
    }
    $('#browse .J-print').off().on('click', function(){
        visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
        visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");
        visionariPrinter.ADD_PRINT_TABLE(10, 10, "100%", "100%", $('#browseTable_container').html());
        visionariPrinter.SET_PRINT_COPIES(1);
        // visionariPrinter.PREVIEW();

        visionariPrinter.SET_SAVE_MODE("SAVEAS_IMGFILE_EXENAME",".jpg");
        visionariPrinter.SAVE_TO_FILE("vvme.jpg"); 
    });

},
drawingPDF: function(e){
    var $target = $(e.currentTarget),
    _self = this,
                //billId = parseInt($target.parents('tr:eq(0)').prev().find('.J-bill-id').text()),
                billId = $(e.currentTarget).data('bill') * 1,
                model = this.collection.findWhere({'BILL_ID' : billId});
                model = this.setRefernceNo(model);


                var url = config.selectUrl + '?key=exportPdf&bill_id=' + billId;
                var xhr = new XMLHttpRequest(); 
                xhr.open('GET', url, true); 
                xhr.responseType = "blob";
                xhr.onreadystatechange = function () { 
                    if (xhr.readyState == 4) {
                        _self.downloadFile(billId + '.pdf', xhr.response);
                    }
                };

                xhr.send();
            },
            downloadFile: function(fileName, blob){

                var aLink = document.createElement('a');

                var evt = document.createEvent("MouseEvents");
                evt.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

                aLink.download = fileName;
                aLink.href = URL.createObjectURL(blob);
                aLink.dispatchEvent(evt);
            },
            addTask: function(e){
               var _self =this,
               billId = $(e.currentTarget).data('bill') * 1;

            //var billId = parseInt($(e.currentTarget).parents('tr:eq(0)').find('.J-bill-id').text());

            var addTaskView = new AddTaskView({'admin': this.admin, 'saveCallback': function(model, response, dialog){

                if(response.success){
                	_self.render();

                }else{
                    _self.showErrorMessage();
                }

            }});

            addTaskView.render("","Add Task", billId);
        },
        removeTask: function(e){
            var $target = $(e.currentTarget),
            $dd = $target.parents('dd:eq(0)'),
            scheduleId = $target.prev().text();


            var dialog = new Dialog({
                content: '<div style="margin-top:40px;text-align:center;">Are you sure you want to delete this Task(' + scheduleId + ') ?<div>',
                lock:true,
                width: 400,
                height: 100,
                title:'Det Task',
                okValue:'Ok',
                ok: function (event) {

                    //var billId = parseInt($(e.currentTarget).parents('tr:eq(0)').find('.J-bill-id').text());
                    var billId = $(e.currentTarget).data('bill') * 1;

                    var taskModel = new TaskModel.TaskModel();
                    taskModel.set('SCHEDULE_ID', scheduleId);
                    taskModel.url = taskModel.url + "?SCHEDULE_ID=" + scheduleId + '&BILL_ID=' + billId;
                    taskModel.destroy({
                        success: function(model, response) {
                            dialog.close();
                            $.unblockUI();
                            $dd.remove();
                        },
                        error: function(){
                            dialog.close();
                            $.unblockUI();
                            _self.showErrorMessage();
                        }
                    });
                    
                },
                cancelValue: 'Cancel',
                cancel: function(){
                    dialog.close();
                }
            }).showModal();
},
gotoTask: function(e){

    var scheduleId = $(e.currentTarget).data('task');

    if(window.top === window){
                //单独打开的页面

                // Save data to sessionStorage
                window.sessionStorage.setItem('SCHEDULE_ID', scheduleId);
                window.location='/Sync10-ui/pages/task_invoice/task/index.html';

            }else{
                //在框架里嵌入的页面

                var topIframe = window.top.window.frames["iframe"];

                topIframe.task_invoice ? topIframe.task_invoice['SCHEDULE_ID'] = scheduleId : topIframe.task_invoice = {'SCHEDULE_ID': scheduleId};

                
                var alink = topIframe.frames['leftFrame'].$('a[href$="task_invoice/task/index.html"]');

                alink.parent().trigger('click');

                topIframe.frames['main'].location='/Sync10-ui/pages/task_invoice/task/index.html';
                
            }

        },
        //账单预览、打印等操作页面使用
        setRefernceNo:function(model){
        	//by gql
            var lrType = model.toJSON().LR_TYPE;
            var lrNo = model.toJSON().LR_NO;
            var reference_no = "";
            if(lrType){
               for(var i=0;i<LRTypeKey.length;i++){
                  if(lrType==LRTypeKey[i].key){
                     reference_no=LRTypeKey[i].value+" ";
                 }
             }
         }
         reference_no += lrNo;
         model.set("REFERENCE_NO",reference_no);
         return model;
     }
 });
});