"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "/Sync10-ui/pages/task_invoice/key/LRTypeKey.js",
],function($, Backbone, Handlebars, templates, LRTypeKey){

    return Backbone.View.extend({
        template: templates.invoiceBrowse,
        initialize: function(option){
            this.loading = option.loading;
            this.setElement(option.el);
            this.model = option.model;
            this.saveCallback = option.saveCallback;    //保存成功之后的callback
            this.dialog = option.dialog;
            this.$error = option.$error;    //保存错误提示
        },
        render:function(){
        
            var _self = this;

            _self.loading.show();
            
            //by gql
            var lrType = _self.model.toJSON().LR_TYPE;
            var lrNo = _self.model.toJSON().LR_NO;
            var reference_no = "";
            if(lrType){
	            for(var i=0;i<LRTypeKey.length;i++){
	            	if(lrType==LRTypeKey[i].key){
	            		reference_no=LRTypeKey[i].value+" ";
	                }
	            }
            }
            reference_no += lrNo;
            _self.model.set("REFERENCE_NO",reference_no);
          //by gql end
            
            _self.$el.html(_self.template(_self.model.toJSON()));

            //页面构造完成
            _self.loading.hide();
        },
        events: {
            "click .btn-primary" : 'saveModel',
            "click .J-print" : "print"
        },
        saveModel: function(e){
            var _self = this;

            $.blockUI({message:'<img src="../image/loadingAnimation6.gif" align="absmiddle"/>'});

            this.model.save({},{
                success:function(model, response, options){
                    $.unblockUI();

                    if(_self.saveCallback){
                        _self.saveCallback(model, response, _self.dialog);
                    }
                    
                },
               error:function(){
                    console.error("error");
                    $.unblockUI();
                    _self.showErrorMessage();
                }
            });
        },
        print: function(e){
            var $container = this.$el.find('#browseTable_container');
            if(!$container.find('.fill').length){
                var theadH = $container.find('thead').outerHeight();
                var tfootH = $container.find('tfoot').outerHeight();
                var tbodyH = $container.find('tbody').outerHeight();
                var unitH = 982 * 98 / 100 - 30 - theadH - tfootH;

                var mulriple = Math.ceil(tbodyH / unitH);

                var difference = unitH * mulriple - tbodyH;

                if(difference > 30){
                    $container.find('tbody').append('<tr class="fill">'+
                      '<td style="border-right-width:0; border-left: 2px solid #333;"></td>'+
                      '<td style="border-left-width: 0; "></td>'+
                      '<td><div style=" max-width: 450px; word-break: break-all; text-align: left;"></div></td>'+
                      '<td><div style="height:'+ difference+'px; margin:0; padding:0;"></div></td><td></td></tr>');
                }
            }

            visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
            visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");
            visionariPrinter.ADD_PRINT_TABLE(10, 10, "100%", "100%", $('#browseTable_container').html());
            visionariPrinter.SET_PRINT_COPIES(1);
            // visionariPrinter.PREVIEW();

            visionariPrinter.SET_SAVE_MODE("SAVEAS_IMGFILE_EXENAME",".jpg");
            visionariPrinter.SAVE_TO_FILE("vvme.jpg"); 
        },
        showErrorMessage: function(){

            var _self = this;

            setTimeout(function(){
                _self.$error.fadeOut();

            }, 2800);

            _self.$error.css('left', (_self.$el.width() / 2 - 107) + 'px').fadeIn();

        }   
    });
});