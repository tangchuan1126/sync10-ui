"use strict";
define([
    "jquery",
    "../../../config.js",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "../../../artDialog.js",
    " /Sync10-ui/pages/task_invoice/key/LRTypeKey.js",
],function($, config, Backbone, Handlebars, templates, Dialog, LRTypeKey){

    return Backbone.View.extend({
        template: templates.invoiceDetail,
        initialize: function(option){
            this.loading = option.loading;
            this.setElement(option.el);
            this.model = option.model;
            this.saveCallback = option.saveCallback;
            this.dialog = option.dialog;
        },
        render:function(){
            var _self = this,
                data = _self.model.toJSON();
            
          //by gql reference no is edited lr no
           var self_data = {'TOTAL' : data.SAVE, "LR_TYPE" : data.LR_TYPE, "LR_NO" : data.LR_NO, "LRTypeKey":LRTypeKey};
            
           _self.loading.show();
            _self.$el.html(_self.template(self_data));

            _self.$tbody = _self.$el.find('tbody');

            _self.$tbody.html(templates.addDetail({"detail" : data.BILL_DETAIL}));      //列表数据

            _self.loading.hide();
            _self.isLoaded_flag = true;
        },
        
        events: {
            'click .icon-add' : 'addDetail',
            'click .icon-remove' : 'removeDetail',
            'blur .J-rate' : 'count',
            'blur .J-quantity' : 'count',
            'keyup .J-rate' : 'enterAddDetail',
            'blur .J-discount' : 'countTotal',
            'click #goToBrowse' : 'goToBrowse',
            'blur .form-control' : 'rangeToStart',
            'focus .form-control' : 'rangeToEnd'
        },
        rangeToStart: function(e){
            var self = this;
            var elem = e.currentTarget;
            elem.setSelectionRange && elem.setSelectionRange(0, 0);
        },
        rangeToEnd: function(e){
            var elem = e.currentTarget;
           var val = elem.value;
           var len = val ? val.length : 0;
            elem.setSelectionRange && elem.setSelectionRange(len, len);
        },
        enterAddDetail: function(e){
            if(e.keyCode == 13){
                this.addDetail(e);
            }
        },
        addDetail: function(e){

            var rate = this.$el.find('#sel_cur').val();

            var lineNo = this.$tbody.find('tr').length;

            this.$tbody.append(templates.addDetail({"detail" : [{
                "LINE_NO" : lineNo + 1, 
                "SERVICE_ID" : "SPECCHARGE", 
                "DESCRIPTION" : "Special Charge", 
                "UNIT_PRICE" : "12",
                "QUANTITY" : "1.00",
                //"RATE" : rate,
                // "AMOUNT" : "0.00"
            }]})).find('.J-service-id').last().focus();

        },
        removeDetail: function(e){
            var self = this;
            var dialog = new Dialog({
                content: '<p style="margin-top:40px;text-align:center;">Are you sure you want to delete this detail ?<p>',
                lock:true,
                width: 400,
                height: 100,
                title:'Del Detail',
                okValue:'Ok',
                ok: function (event) {
                    $(e.currentTarget).parent().parent('tr').remove();
                    self.$tbody.find('tr').each(function(i, elem){
                        $(elem).find('.J-line-no').text(i + 1);
                    });
                    self.countTotal();
                    dialog.close();
                },
                cancelValue: 'Cancel',
                cancel: function(){
                    dialog.close();
                }
            }).showModal();
        },  
        count: function(e){
            this.countAmountAndTotal($(e.currentTarget).parent().parent('tr'));
        },
        countAmountAndTotal: function($tr){
            var $rate = $tr.find('.J-rate'),
                $quantity = $tr.find('.J-quantity'),
                unit = parseFloat($rate.val()),
                quantity = parseFloat($quantity.val());

            if(!unit){
                unit = 0.00;
                $rate.val("");
            }else{
                unit = (Math.round(unit * 10000) / 10000).toFixed(4);
                $rate.val(unit + '');
            }

            if(!quantity){
                quantity = 0;
                $quantity.val("");
            }else{
                quantity = (Math.round(quantity * 100) / 100).toFixed(2);
                $quantity.val(quantity);
            }

            var amount = Math.round(unit * quantity * 100) / 100;

            if(amount){
                $tr.find('.J-amount').text(amount.toFixed(2))
            }else{
                $tr.find('.J-amount').text("")
            }
            
            this.countTotal();
        },
        countTotal: function(){

            var arr_amount = this.$el.find('.J-amount'),
                total = 0;

            $.each(arr_amount, function(i, obj){
                var amount = $(obj).text() || "0.00";
                total = (Math.round(parseFloat(amount) * 10000) + total * 10000) / 10000;
                total = (Math.round(total * 100) / 100).toFixed(2);
            });

            this.model.set('SUBTOTAL', total);

            if(isNaN(total)){
                throw new Error("total is NaN!");
            }

            this.$el.find('.J-total').text(total + '');

            this.model.set('TOTAL', total);
        },
        //验证是否通过
        isValidated: function(){

            if(!this.$tbody){
                this.render();
            }

            if(!this.$tbody.find('tr').length){
                return false;
            }

            //add by huhy 2015-6-30 14:38:28
            //LR No 选择了之后，后续input必填验证
            var $lr_no = $('#lr_no');
            if($('#lr_type').val() && !$lr_no.val()){
                $lr_no.focus();
                return false;
            }
            
            var forms = this.$el.find('.form-control:not(.J-discount):not(.J-service-id):not(#lr_no):not(#lr_type)');

            for(var i = 0, len = forms.length; i < len; i++){
                var $elem = $(forms[i]);
                var val = $elem.val();
                if(val == "" || val == 0){
                    if($elem.parents('tr:eq(0)').find('.J-service-id').val() == "" && $elem.get(0).id != "reference_no"){
                        continue;
                    }
                    $elem.focus();
                    return false;
                }
            }

            if(i == len){
                return true;
            }
        },
        //封装数据
        encapsulation: function(){

            this.countTotal();

            var detail = [],
                arr_detail = this.$tbody.find('tr');

            $.each(arr_detail, function(i, elem){
                detail[i] = {};
                var obj = detail[i],
                    $tr = $(elem);

                obj["LINE_NO"] = $tr.find('.J-line-no').text();
                obj["SERVICE_ID"] = $tr.find('.J-service-id').val();
                obj["DESCRIPTION"] = $tr.find('.J-des').val();
                obj["UNIT_PRICE"] = $tr.find('.J-unit').val();
                obj["RATE"] = $tr.find('.J-rate').val();
                obj["QUANTITY"] = $tr.find('.J-quantity').val();
                obj["AMOUNT"] = $tr.find('.J-amount').text();
            });
            this.model.set('BILL_DETAIL', detail);

//            this.model.set('REFERENCE_NO', this.$el.find('#reference_no').val());
            
            //by gql reference no is edited lr no
            this.model.set('LR_TYPE', this.$el.find('#lr_type').val());
            this.model.set('LR_NO', this.$el.find('#lr_no').val());

            // var lrType = _self.model.toJSON().LR_TYPE;
            // var lrNo = _self.model.toJSON().LR_NO;
            // var reference_no = "";
            // if(lrType){
            //     for(var i=0;i<LRTypeKey.length;i++){
            //         if(lrType==LRTypeKey[i].key){
            //             reference_no=LRTypeKey[i].value+" ";
            //         }
            //     }
            // }
            // reference_no += lrNo;
            // _self.model.set("REFERENCE_NO",reference_no);
        },
        goToBrowse: function(e){
            // $('a[href="#browse"]').trigger('click') ;
            var _self = this;
            if(this.isValidated()){
                this.encapsulation();
                $('#check_detail').removeClass('checkerror').addClass('checked');
            }else{
                $('#check_detail').removeClass('checked').addClass('checkerror');

                return false;
            }

            $.blockUI({message:'<img src="../image/loadingAnimation6.gif" align="absmiddle"/>'});

            this.model.save({},{
                success:function(model, response, options){
                    $.unblockUI();

                    if(_self.saveCallback){
                        _self.saveCallback(model, response, _self.dialog);
                    }
                    
                },
               error:function(){
                    console.error("error");
                    $.unblockUI();
                    _self.showErrorMessage();
                }
            });
        }
    });
});