/**
 * Created by huhy on 2015-3-30
    主页面 包括search部分， 列表的外框
 */
"use strict";
define([
    "jquery",
    "backbone",
    'handlebars.runtime',
    "../../../config.js",
    "../../../templates/templates.amd.js",
    "./invoiceListView.js",
    "./addInvoiceView.js",
    "../../../globalAdminModel",
    "../../../commonSearch.js",
    "bootstrap_tab",
    //2015-04-29临时修改
    "bootstrap.datetimepicker",
    "require_css!bootstrap.datetimepicker-css",
    /*"bootstrap.datetimepicker/js/bootstrap-datetimepicker",*/
    "require_css!bootstrap-css/bootstrap.min.css",
    "require_css!/Sync10-ui/pages/task_invoice/css/task_invoice.css",
    /*"require_css!bootstrap.datetimepicker/css/bootstrap-datetimepicker.min",*/
    "handlebars_ext",
    "domready!"
],function($, Backbone, Handlebars, config, templates, InvoiceListView, AddInvoiceView, GlobalAdminModel, commonSearch){

    return Backbone.View.extend({
        template: templates.mainInvoice,
        model: new GlobalAdminModel(),
        initialize: function(option){
            this.setElement(option.el);
        },
        render:function(query){

            var _self = this;

            _self.model.fetch({
                success:function(model){
                    _self.$el.html(_self.template({'query': query}));

                    if(!_self.listView){
                        _self.listView = new InvoiceListView({
                            'el': "#invoice_table tbody", 
                            'loadingModal': _self.$el.find('.modal'), 
                            'admin': model, 
                            'showErrorMessage' : _self.showErrorMessage
                        });
                    }
                    
                    _self.listView.render();

                    //search ...
                    commonSearch({
                        '$el' : _self.$el, 
                        'target' : _self.$el.find('.J-search-common'), 
                        'url' : config.selectUrl + '?key=getInvoiceResultByIndex',
                        'idName' : 'BILL_ID',
                        'textName' : 'MERGE_FIELD',
                        'callback': function(text){
                            _self.listView.render();
                        }
                    });
                }
            });
        } ,
        events: {
            "click #add_invoice" : "addInvoice",
            "click .J-btn-search" : "searchForList"
        },
        addInvoice: function(e){
            var _self = this;
            new AddInvoiceView({'admin': this.model, 'saveCallback': function(model, response, dialog){

                dialog.close();
                
                _self.listView.render();
                
            }}).render();
        },
        searchForList: function(e){

            this.listView.render();
        },
        showErrorMessage: function(msg){

            var _self = this;

            clearTimeout(_self.timer);

            if(!_self.$error){
                _self.$error = $('<div class="alert alert-danger" role="alert"><strong>Error ! </strong><span> The operation failure. </span></div>').appendTo(_self.$el);
            }
            msg && (_self.$error.find('span').html(msg)) && (_self.$error.flog = true);
            !msg && _self.$error.flog && (_self.$error.find('span').html(' The operation failure.')) && (_self.$error.flog = false);

            _self.timer = setTimeout(function(){
                _self.$error.fadeOut();
            }, 2800);

            _self.$error.fadeIn();

        }
    });
});