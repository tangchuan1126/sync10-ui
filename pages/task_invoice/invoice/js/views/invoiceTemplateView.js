"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "../../../config.js",
    "oso.lib/validate/js/validate",
    // "/Sync10-ui/lib/table/js/table.js",
    "select2",
    "require_css!/Sync10-ui/bower_components/select2/select2.css",
    "require_css!/Sync10-ui/bower_components/select2/select2-bootstrap.css"
],function($, Backbone, Handlebars, templates, config, validate){

    return Backbone.View.extend({
        template: templates.invoiceTemplate,
        initialize: function(option){
            this.loading = option.loading;
            this.setElement(option.el);
            this.isLoaded = {
                'country ' : false,
                // 'f' : false,
                'client' : false
            };
            this.model = option.model;
        },
        render:function(){
            var _self = this,
                data = _self.model.toJSON();

            _self.loading.show();
                
            _self.$el.html(_self.template(data));

            //payment Term
            _self.$el.find('#paymentTerm').val(data['PAYMENT_TERM'] || 'NET30');

            var $state_val = _self.$el.find('#state_input');

            //client_id 下拉
            _self.buildClientIdSelect(data['CLIENT_ID']);

            //country
            _self.buildCountrySelect(data['ADDRESS_COUNTRY_CODE']);

            //state
            //edit 时候页面上初始的state
            var address_state = data['PRO_ID'];
            if(address_state == '-1'){
                $state_val.addClass('validate').show();
            }else if(address_state){
                //有值，并且是选项中的
                _self.buildStateSelect(data['CCID'], address_state);
            }else{
                //state init
                _self.$el.find('#sel_state').html('<option></option>').select2({
                    placeholder: "Select a option",
                    allowClear: true
                });
            }

            //parse validate
            validate.parser({'container' : _self.el});

            //去除默认隐藏的input的验证
            $state_val.removeClass('validate');

        },
        //构建country的下拉
        buildCountrySelect: function(data, argName){
            var _self = this,
                $state_val = _self.$el.find('#state_input');
            //country
            _self.buildSelect('#sel_country', 'country', {'key' : 'GetAllCountryCode'}, data, argName ||'C_CODE', function(e){

                $('#sel_country_val').val(e.val).trigger('blur');
                $state_val.val('').removeClass('validate').trigger('blur').hide();
                $('#sel_state_val').removeClass('validate').val('').trigger('blur').hide();

                if(e.val === ""){
                    //清空了选项
                    $('#sel_state').html('<option></option>').select2({
                        placeholder: "Select a option",
                        allowClear: true
                    });
                    // $state_val.val('').removeClass('validate').trigger('blur').hide();
                    // $('#sel_state_val').removeClass('validate').val('').trigger('blur').hide();

                    return false;
                }

                //加载选项数据...
                _self.loading.show();

                var ccid = $(e.added.element[0]).data('ccid');

                _self.$el.find('#sel_country_ccid').val(ccid);
                _self.$el.find('#sel_country_name').val(e.added.text);

                _self.buildSelect('#sel_state', 's', {'key' : 'GetStorageProvinceByCcid', 'ccid' : ccid}, '', '', function(e){

                    $('#sel_state_val').show().addClass('validate').val(e.val).trigger('blur');

                    if(e.val === ""){
                        //清空了选项
                        $state_val.removeClass('validate').val('').trigger('blur').hide();
                    }else if(e.val === "-1"){
                        //手动输入
                        $state_val.addClass('validate').val('').show();
                    }else{
                        //填值
                        _self.$el.find('#sel_state_name').val(e.added.text);
                    }
                }, true, true);

            }, false);
        },
        //构建state的下拉
        buildStateSelect: function(data, address_state){
            var _self = this,
                $state_val = _self.$el.find('#state_input');

            _self.buildSelect('#sel_state', 's', {'key' : 'GetStorageProvinceByCcid', 'ccid' : data}, address_state, 'PRO_ID', function(e){

                $('#sel_state_val').show().addClass('validate').val(e.val).trigger('blur');

                if(e.val === ""){
                    //清空了选项
                    $state_val.removeClass('validate').val('').trigger('blur').hide();
                }else if(e.val === "-1"){
                    //手动输入
                    $state_val.addClass('validate').val('').show();
                }else{
                    //填值
                     _self.$el.find('#sel_state_name').val(e.added.text);
                }
            }, true, true);
        },
        //构建client_id的下拉
        buildClientIdSelect: function(data){
            var _self = this,
                $state_val = _self.$el.find('#state_input');

            _self.buildSelect('#sel_client', 'client', {'key' : 'getAddressInfoList'}, data, 'CUSTOMER_KEY', function(e){
                var clientID = e.val;
                $('#client_id').val(clientID).trigger('blur');
                if(clientID === ""){
                    //清空了选项
                    //TODO
                }else{
                    //填值
                    _self.loading.show();

                    $.ajax({
                        url: config.selectUrl + '?key=getAddressInfoListByClientId&client_id=' + clientID,
                        dataType: 'json',
                        success: function (data) {
                            
                            //回填
                            var client = data[0];

                            //email
                            $('#email').val(client['EMAIL']);

                            //name
                            $('#name').val(client['TITLE']);

                            //street
                            $('#street').val(client['SEND_HOUSE_NUMBER']);

                            //city
                            $('#city').val(client['CITY']);

                            //zip
                            $('#zip').val(client['SEND_ZIP_CODE']);

                            //tel
                            $('#tel').val(client['PHONE']);

                            //fax
                            $('#fax').val(client['FAX']);

                            //Attention
                            $('#attention').val(client['CONTACT']);

                            //country...
                            var country = client['NATIVE'];
                            if(country){
                                //_self.buildCountrySelect(country, 'CCID');
                                var $selected = $('#sel_country option[data-ccid="'+ country +'"]');

                                $('#sel_country_val').val($selected.val()).trigger('blur');
                                $('#sel_country_ccid').val(country);
                                $('#sel_country_name').val($selected.text());

                                $('#sel_country').val($selected.val()).select2({
                                    placeholder: "Select a option",
                                    allowClear: true
                                });
                            }else{
                                $('#sel_country').val("").select2({
                                    placeholder: "Select a option",
                                    allowClear: true
                                });
                            }
                            //state...
                            var address_state = client['PRO_ID'];
                            if(address_state == '-1'){
                                $state_val.addClass('validate').show();
                            }else if(address_state){
                                //有值，并且是选项中的
                                _self.buildStateSelect(country, address_state);//client['CCID'], address_state);
                            }else{
                                $('#sel_state').val("").select2({
                                    placeholder: "Select a option",
                                    allowClear: true
                                });
                            }
                            _self.loading.hide();
                        },
                        error:function(e){
                            console.log(arguments);
                            _self.loading.hide();
                        }
                    });
                }
            }, false);
        },
        hideInput: function($aName, $acount, obj){
            $aName.next().hide().removeClass('validate').trigger('blur').val('').end().text('').show().text(obj.text);;
            $acount.next().hide().removeClass('validate').trigger('blur').val('').end().text('').show().text(obj.id);;
        },
        showInput: function($aName, $acount){
            $aName.hide().text('').next().val('').addClass('validate').show();
            $acount.hide().text('').next().val('').addClass('validate').show();
        },
        buildSelect: function(id, selType, data, sel, option, changeFn, isCanEnter, flag){
            var _self = this;

            $.ajax({
                url: config.selectUrl,
                dataType: 'json',
                data: data,
                success: function (data) {
                    var obj = {};
                    obj[selType] = data;
                    obj['isCanEnter'] = isCanEnter;

                    if(sel == '-1'){
                        obj['isHand'] = true;
                    }else{
                        $.each(data, function(i, obj){
                            if(obj[option] == sel){
                                data[i].sel = true;
                                //隐藏域回填
                                if(id == "#sel_state"){
                                    $('#sel_state_val').show().addClass('validate').val(obj['PRO_ID']).trigger('blur');
                                    $('#sel_state_name').val(obj['PRO_NAME']);
                                }
                            }
                        });
                    }

                    _self.$el.find(id).html(templates.selectOptions(obj)).select2({
                        placeholder: "Select a option",
                        allowClear: true
                    }).on('change', changeFn);


                    //判断两个sel加载情况
                    _self.isLoaded[selType] = true;

                    if(_self.isLoaded['country'] && _self.isLoaded['client']){

                        _self.loading.hide();
                        _self.isLoaded_flag = true;
                    }

                    if(flag){
                        _self.loading.hide();
                    }
                },
                error:function(e){
                    console.error("request error!");
                    var obj = {};
                    obj['isCanEnter'] = isCanEnter;

                    _self.$el.find(id).html(templates.selectOptions(obj)).select2({
                        placeholder: "Select a option",
                        allowClear: true
                    }).on('change', changeFn);

                    //判断两个sel加载情况
                    _self.isLoaded[selType] = true;

                    if(_self.isLoaded['country'] && _self.isLoaded['client']){

                        _self.loading.hide();
                        _self.isLoaded_flag = true;
                    }

                    if(flag){
                        _self.loading.hide();
                    }
                }
            });
        },
        events: {
            'click #goToNext' : "goToNext"
        },
        //封装数据..
        encapsulation : function(){

            this.model.set('key_type', $('#sel_bill_f_val').val());
            // this.model.set('NOTE', $('#note').val());
            this.model.set('CLIENT_ID', $('#client_id').val());
            this.model.set('PAYER_EMAIL', $('#email').val());
            this.model.set('ADDRESS_NAME', $('#name').val());
            this.model.set('ADDRESS_STREET', $('#street').val());
            this.model.set('ADDRESS_CITY', $('#city').val());
            this.model.set('ADDRESS_ZIP', $('#zip').val());

            this.model.set('ADDRESS_COUNTRY_CODE', $('#sel_country_val').val());
            this.model.set('CCID', $('#sel_country_ccid').val());
            this.model.set('ADDRESS_COUNTRY', $('#sel_country_name').val());

            this.model.set('PRO_ID', $('#sel_state_val').val());
            if($('#sel_state_val').val() === '-1'){
                this.model.set('ADDRESS_STATE', $('#state_input').val());
            }else{
                this.model.set('ADDRESS_STATE', $('#sel_state_name').val());
            }
            
            this.model.set('TEL', $('#tel').val());
            this.model.set('MEMO_NOTE', $('#memo_note').val());

            this.model.set('FAX', $('#fax').val());
            this.model.set('ATTENTION', $('#attention').val());
            this.model.set('PAYMENT_TERM', $('#paymentTerm').val());
        },
        //是否通过验证
        isValidated: function(){
            return validate.isValidated('#template');
        },
        goToNext: function(e){
            $('a[href="#detail"]').trigger('click');
        }
    });
})