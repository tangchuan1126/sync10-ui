"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "./../models/invoiceModel.js",
    "./invoiceTemplateView.js",
    "./invoiceBrowseView.js",
    "./invoiceDetailView.js",
    "../../../artDialog.js",
    "../../../date.js",
],function($, Backbone, Handlebars, templates, InvoiceModel, InvoiceTemplateView, InvoiceBrowseView, InvoiceDetailView, Dialog, Date){

    return Backbone.View.extend({
        template: templates.addInvoice,
        initialize: function(options){
            this.admin = options.admin;  //登录人的信息
            this.saveCallback = options.saveCallback;
            this.title = options.title;
        },
        render:function(model, taskId){
            var _self = this;

            if(!model){
                _self.model = new InvoiceModel();
                _self.model.set('USER', _self.admin);
                _self.model.set('CREATE_ADID', _self.admin.id);     //admin信息
                _self.model.set('ADMIN_NAME', _self.admin.toJSON()['employe_name']);
                _self.model.set('SCHEDULE_ID', taskId);    //task id
                _self.model.set('CREATE_DATE', new Date().format("MM/dd/yy HH:mm")); 

            }else{
                _self.model = model.clone();
            }

            _self.$el.html(_self.template());
            _self.$warning = $('<div class="alert alert-warning" role="alert"><strong>Warning!</strong> Must first finish Template and Detail.</div>').appendTo(_self.$el);
            _self.$error = $('<div class="alert alert-danger" role="alert"><strong>Error ! </strong> The operation failure. </div>').appendTo(_self.$el);
            
            //dialog
            _self.dialog = new Dialog({
                lock:true,
                width: 804,
                height: 580,
                title: _self.title || 'Add Invoice',
                cancelDisplay: false,
                cancel: function(){
                    var dialog = new Dialog({
                        content: '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
                +'Your changes have not been saved, do you want to quit?</p>', 
                        lock:true,
                        width: 450,
                        height: 100,
                        title:'Warn » Cancel',
                        okValue:'OK',
                        ok: function (event) {
                            dialog.close();
                            _self.dialog.close();
                        },
                        cancelValue: 'Cancel',
                        cancel: function(){
                            dialog.close();
                        }
                    }).showModal();
                    return false;
                }
            });

            _self.dialog.onclose = function(){

                //清除view 防止backbone内部的保留导致的异常bug
                if(_self.invoiceTemplateView){
                    _self.invoiceTemplateView.remove();
                }
                if(_self.invoiceBrowseView){
                    _self.invoiceBrowseView.remove();
                }
                if(_self.invoiceDetailView){
                    _self.invoiceDetailView.remove();
                }
                _self.remove();

                if($('.art-dialog').length === 1){
                    $('html').css({'margin-right': 0, 'overflow-y': 'auto'});
                }

                _self.dialog.remove();
            };

            var loading = _self.$el.find('.modal');

            if(!_self.invoiceTemplateView){
                _self.invoiceTemplateView = new InvoiceTemplateView({'loading': loading, 'el': _self.$el.find('#template'), 'model' : _self.model});
            }

            if(!_self.invoiceBrowseView){
                _self.invoiceBrowseView = new InvoiceBrowseView({'loading': loading, 'el': _self.$el.find('#browse'), 'model' : _self.model, 'dialog': _self.dialog, 'saveCallback': _self.saveCallback, '$error' : _self.$error});
            }

            if(!_self.invoiceDetailView){
                _self.invoiceDetailView = new InvoiceDetailView({'loading': loading, 'el': _self.$el.find('#detail'), 'model' : _self.model, 'dialog': _self.dialog, 'saveCallback': _self.saveCallback, '$error' : _self.$error});
            }

            //template show ...
            _self.loadingTemplate();

            _self.dialog.content(_self.$el).showModal();
        },
        events: {
            'click a[href="#template"]' : 'loadingTemplate',
            'click a[href="#browse"]' : 'loadingBrowse',
            'click a[href="#detail"]' : 'loadingDetail',
            'hide.bs.tab a[href="#template"]' : 'hideTemplate',
            'hide.bs.tab a[href="#detail"]' : 'hideDetail'
        },
        loadingTemplate:function(e){
            if(e && e.currentTarget.parentNode.className.indexOf('active') !== -1){
                return false;
            }
            if(!this.$el.find('#template').get(0).hasChildNodes()){
                this.invoiceTemplateView.render();
            }else{

                if(this.invoiceTemplateView.isLoaded_flag){
                    this.invoiceTemplateView.loading.hide();
                }else{
                   this.invoiceTemplateView.loading.show(); 
                }
            }
        },
        hideTemplate: function(e){
            var $_self = $(e.currentTarget);

            if(this.invoiceTemplateView.isValidated()){
                $_self.prev('.check').removeClass('checkerror').addClass('checked');
                this.invoiceTemplateView.encapsulation();
            }else{
                $_self.prev('.check').removeClass('checked').addClass('checkerror');
            }
        },
        loadingDetail:function(e){
            if(e && e.currentTarget.parentNode.className.indexOf('active') !== -1){
                return false;
            }

            var curTarget = this.$el.find('.active a').attr('href');
            if(curTarget == '#template' && !this.invoiceTemplateView.isValidated()){
                this.$el.find('.active a').prev('.check').removeClass('checked').addClass('checkerror');
                return false;
            }

            if(!this.$el.find('#detail').get(0).hasChildNodes()){
                this.invoiceDetailView.render();

            }else{
                if(this.invoiceDetailView.isLoaded_flag){
                    this.invoiceDetailView.loading.hide();
                }else{
                    this.invoiceDetailView.loading.show();
                }
            }
        },
        hideDetail: function(e){
            var $_self = $(e.currentTarget);

            if(this.invoiceDetailView.isValidated()){
                this.invoiceDetailView.encapsulation();
                $_self.prev('.check').removeClass('checkerror').addClass('checked');
            }else{
                $_self.prev('.check').removeClass('checked').addClass('checkerror');
            }
        },
        loadingBrowse:function(e){
            if(e && e.currentTarget.parentNode.className.indexOf('active') !== -1){
                return false;
            }

            var curTarget = this.$el.find('.active a').attr('href');

            switch(curTarget){
                case '#template' : 
                    if(!this.invoiceTemplateView.isValidated()){
                        //不让跳转
                        e.preventDefault();
                        $(e.currentTarget).blur();
                        $('a[href="#template"]').prev('.check').removeClass('checked').addClass('checkerror').end().trigger('click');

                        //提示信息。。。
                        this.showMessage();

                        return false;
                    }else{
                        this.invoiceTemplateView.encapsulation();
                        $('a[href="#template"]').prev('.check').removeClass('checkerror').addClass('checked');
                    }

                case '#detail' : 
                    if(!this.invoiceDetailView.isValidated()){

                        e.preventDefault();
                        $(e.currentTarget).blur();
                        $('a[href="#detail"]').prev('.check').removeClass('checked').addClass('checkerror').end().trigger('click');

                        //提示信息。。。
                        this.showMessage();

                        return false;
                    }else{
                        this.invoiceDetailView.encapsulation();
                        $('a[href="#detail"]').prev('.check').removeClass('checkerror').addClass('checked');
                    }

                    var checkerror = this.$el.find('.checkerror');

                    if(checkerror.length){
                        e.preventDefault();
                        $(e.currentTarget).blur();
                        $(checkerror).next().trigger('click');
                        this.showMessage();
                        return false;
                    }

                default:
                   this.invoiceBrowseView.render(); 
            }           
        },
        showMessage: function(){

            var _self = this;

            setTimeout(function(){
                _self.$warning.fadeOut();

            }, 2800);

            _self.$warning.css('left', (_self.$el.width() / 2 - 175) + 'px').fadeIn();

        }
    });
});