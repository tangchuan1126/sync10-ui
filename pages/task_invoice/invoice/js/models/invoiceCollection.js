"use strict";
define([
  "../../../config.js",
  "./invoiceModel.js",
  "jquery",
  "backbone"
], function(config, InvoiceModel) {

    return Backbone.Collection.extend({
        model: InvoiceModel,
        url: config.invoiceCollectionUrl,
        comparator: function(item) {
            //
        },
        parse:function(response){
            if(response.pageCtrl){this.pageCtrl = response.pageCtrl;}
            return response.INVOICE;
        },
        initialize: function(options){
         
        }
    },{
        pageCtrl:{
            pageNo:1,
            pageSize:10
        }
        
    });
}); 