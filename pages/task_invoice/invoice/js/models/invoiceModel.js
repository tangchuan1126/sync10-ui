"use strict";
define([
  "../../../config.js",
  "jquery",
  "backbone",
], function(config) {

    return Backbone.Model.extend({
    	idAttribute: "BILL_ID",
    	url: config.invoiceCollectionUrl,
    	defaults: {
    		'TOTAL_DISCOUNT' : "0.00",
    		'SUBTOTAL' : 0,
			'TOTAL_QUANTITY' : 0,
			'SAVE' : "0.00",
            'BILL_DETAIL' : []
    	}
    });
}); 