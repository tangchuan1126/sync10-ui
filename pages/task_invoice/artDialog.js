/**
 * Created by huhy on 2015-3-30
 */
"use strict";
define([
    "jquery",
    "art_Dialog/dialog-plus"
],function($, Dialog){

    //解决dialog弹出之后背景层的滚动条滚动问题
    Dialog.prototype.onshow = function(){

        var H = $('html');

        if(H.css('overflow-y') === 'hidden'){
            return ;
        }

        var w1 = $(window).width();

        H.css('overflow-y', 'hidden');

        var w2 = $(window).width();

        H.css('overflow-y', 'auto');

        H.css({'margin-right': (w2 - w1) + 'px', 'overflow-y': 'hidden'});

        this.reset();   //重置dialog的位置，解决上面代码导致的window在resize时候dialog会往右边抖动几个像素的bug
    };
    Dialog.prototype.onclose = function(){
        if($('.art-dialog').length === 1){
            $('html').css({'margin-right': 0, 'overflow-y': 'auto'});
        }
        this.remove();
    };

    return Dialog;
});