/**
 * Created by gql on 2015-6-16
    LR No. type
 */
"use strict";
define([
    "jquery"
],function($){

	
	var LRTypeKey = [
	                   {"key": "1",  "value":"DN" },
	                   {"key": "2",  "value":"RN" },
	                   {"key": "3", "value":"RDN" },
	    			   {"key": "4", "value":"PO No." },
	     			   {"key": "5", "value":"BOL No." },
	     			   {"key": "6", "value":"Container No." },
	     			   {"key": "7", "value":"Pro No." },
	     			   {"key": "8", "value":"Load No." }
	                ];

    return LRTypeKey;
});