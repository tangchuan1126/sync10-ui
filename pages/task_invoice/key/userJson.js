/**
 * Created by gql on 2015-4-15
    userJson(role info)
 */
"use strict";
define([
    "jquery"
],function($){

	
	var userJson = [
	                   {"key": "-1", "value":"All" },
	                   {"key": "1",  "value":"Worker" },
	                   {"key": "5",  "value":"Lead" },
	                   {"key": "10", "value":"SuperVisor" },
	                   {"key": "15", "value":"Lead+SuperVisor" }
	                ];

    return userJson;
});