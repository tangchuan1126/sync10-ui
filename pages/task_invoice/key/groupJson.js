/**
 * Created by gql on 2015-4-15
    to、cc选择人员
 */
"use strict";
define([
    "jquery"
],function($){
	var groupJson = [
                     {"key":"-1","value":"All"},
                     {"key":"10000","value":"Manager"},
                     {"key":"1000001","value":"CheckIn"},
                     {"key":"1000002","value":"Warehouse"},
                     {"key":"1000003","value":"Window"},
                     {"key":"1000005","value":"Printer"},
                     {"key":"1000006","value":"Samsung"},
                     {"key":"1000008","value":"Routing"},
                     {"key":"1000009","value":"Schedule"},
                     {"key":"10000010","value":"Security"}
                     ];
    return groupJson;
});