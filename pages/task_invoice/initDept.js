/**
 * Created by gql on 2015-4-15
    to、cc选择人员
 */
"use strict";
define([
    "jquery"
],function($){

	var initDept = function($id, disable_val, value_str, data){
		if(data&&data.length>0){
 			var newa = [];
 			var userInfoDept = data;
 			for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
 				var flag = true;
 				for(var j = 0 , i = newa.length ; j < i ; j++ ){
 					if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
 						flag = false;
 						break;
 					}
 				}
 				if(flag){ //new dept
 					var objs = new Object();
 					objs.deptName = userInfoDept[index].name;
 					objs.deptId = userInfoDept[index].adgid;
 					objs.values = [];
 					objs.values.push(userInfoDept[index]); 
 					newa.push(objs);
 				} else{
 					newa[newa.length - 1].values.push(userInfoDept[index]);	
 				}
 			} 
 			 var selected = "";
 			 for(var index = 0 , count = newa.length ; index < count ; index++ ){
 				var optgroup = "<optgroup label='"+newa[index].deptName+"'>";
 				for(var j = 0 , length =  newa[index].values.length ; j < length ; j++ ){
 					var option = "<option value='"+newa[index].values[j].adid+"'>"+newa[index].values[j].employe_name+"</option>";
 					optgroup += option;
 				}
 				optgroup +="</optgroup>";
 				selected += optgroup;
 			 }
 			 
 			 var $selected = $(selected);
 			
// 			 $selected.find("option[value='"+disable_val+"']").attr("disabled",true);//该项不选
 			 $selected.find("option[value='"+disable_val+"']").remove();//当前登录人不显示在to和cc中
// 			 console.log(typeof(value_str));
 			 //默认选中的项
 			 if(value_str&&value_str!=""){
 				 var selected_arr = value_str;
 				 if(typeof(value_str)=="string"){
 					selected_arr = value_str.split(",");
 				 }
 				 for(var n=0;n<selected_arr.length;n++){
 					 $selected.find("option[value='"+selected_arr[n]+"']").attr("selected",true);
 				 }
 			 }
 			 var select_mult_id = $id.append($selected);
// 			 console.log($id.html());
 			//任务参与人
 			 select_mult_id.multiselect({
 	    			 enableFiltering: true,
 	    			 includeSelectAllOption: true,
 	    				selectAllValue: 0,
 	    				enableClickableOptGroups: true,
 	    				maxHeight: 300,
 	    				buttonWidth: '250px',
 	    				buttonText: function(options, select) {
 	    					if (options.length === 0) {
 	    						return 'No option selected ...';
 	    					}
 	    					else {
 	    						var labels = [];
 	    						
 	    						options.each(function() {
 	    							if ($(this).attr('label') !== undefined) {
 	    								labels.push($(this).attr('label'));
 	    							}
 	    							else {
 	    								labels.push($(this).html());
 	    							}
 	    							if(labels.length > 3){
 	    								labels.push("...");
 	    								return false;
 	    							}
 	    							
 	    						});
 	    						return labels.join(', ') + '';
 	    					}
 	    				}
 	    			});	
 		}else{
// 			showMessage("加载失败","error");
 		};
    };

    return initDept;
});