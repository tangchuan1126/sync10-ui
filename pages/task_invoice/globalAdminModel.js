"use strict";
define([
  "./config.js",
  "jquery",
  "backbone",
], function(config) {

    return Backbone.Model.extend({
    	idAttribute: "adid",
    	url: config.selectUrl + '?key=getAminInformation',
    	parse:function(response){
    	 	return response.adminBean;
    	},
    	initialize: function(){
    		
    	}
    });
}); 