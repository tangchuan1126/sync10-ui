﻿(function(){
    var configObj = {
		taskCollectionUrl : "/Sync10/action/administrator/schedule/handleScheduleAction.action",
		invoiceCollectionUrl : "/Sync10/action/administrator/invoice/handleInvoiceAction.action",
		selectUrl : "/Sync10/action/administrator/key/getNeededKey.action",
		closeTaskUrl : "/Sync10/action/administrator/key/getNeededKey.action?key=closeTask",
		addLogUrl : "/Sync10/action/administrator/key/getNeededKey.action?key=addLog",
		getSubTaskUrl : "/Sync10/action/administrator/key/getNeededKey.action?key=getSubTaskByMainId",
		selectUserUrl : "/Sync10/action/administrator/key/getNeededKey.action?key=getUserByParam",
		getProjectByParamUrl : "/Sync10/action/administrator/key/getNeededKey.action?key=getProjectBySearchParam",
//    	allUserUrl:"/Sync10/action/administrator/schedule/getAllUserInformation.action"
    };
	define(configObj);
	
}).call(this);



