/**
 * Created by huhy on 2015-4-21 09:27
    common tabs search
 */
"use strict";
define([
    "jquery"
],function($){

    var commonSearch = function(opts){

        var $target = $(opts.target),
            $el = opts.$el,
            url = opts.url,
            idName = opts.idName,
            textName = opts.textName,
            callback = opts.callback,
            $ul,            //list
            timer;

        if(!$target.length){
            throw new Error('No Element');
        }

        buildUL();

        //
        function buildUL(){

            //$wrap = $target.wrap('<div class="comsearch-wrap" tabindex="-1"></div>').parent();
            
            $ul = $('<div class="list-group J-search-list"></div>').appendTo($el);

            bindEvents($target, $ul);
        }

        // 绑定 事件
        function bindEvents($target, $ul){

            //target
            $target.on('keyup', function(e){

                switch(e.which || e.keyCode){
                    case 13:
                        //enter
                        assignment($target);

                        break;
                    case 40:
                        //↓
                        selectli($target, $ul, 0);

                        break;
                    case 38:
                        //↑
                        selectli($target, $ul, 1);

                        break;
                    
                    default:
                        buildlist($target, $ul);
                        
                }

            }).on('keydown', function(e){
                clearTimeout(timer);

                if((e.which || e.keyCode) === 38){
                e.preventDefault();
                $target.focus();
            }

            }).on('blur', function(){
                
                setTimeout(function(){ 
                    $ul.empty().data('curVal', ''); 
                }, 200);
            });

            $ul.on('click', '.list-group-item', function(e){
                assignment($target);
            });
        }

        //点击某一列，或者按下enter，赋值给input
        function assignment($target){

            if(callback){
                callback($.trim($target.val()));
            }

            $target.blur();
        };

        //上下选择列
        function selectli($target, $ul, isUp){
            var $cur = $ul.find('.hover');

            if(isUp){
                //↑

                if(!$cur.length || $cur.index() === 0){
                    findLast(); 
                }else{
                    findPrev();
                }
            }else{
                //↓
                if(!$cur.length || $cur.next().length === 0){
                    findFirst(); 
                }else{
                    findNext();
                }
            }

            function findLast(){
                var id = $ul.find('.list-group-item').removeClass('hover').last().addClass('hover').data('id');
                $target.val(id);
            } 

            function findFirst(){
                var id = $ul.find('.list-group-item').removeClass('hover').first().addClass('hover').data('id');
                $target.val(id);
            } 

            function findNext(){
                var id = $cur.removeClass('hover').next().addClass('hover').data('id');
                $target.val(id);
            } 

            function findPrev(){
                var id = $cur.removeClass('hover').prev().addClass('hover').data('id');
                $target.val(id);
            } 
            
        };

        //构造列表
        function buildlist($target, $ul){
            var text = $.trim($target.val()),
                offset = $target.offset(),
                height = $target.outerHeight();

            if(!text){
                $ul.empty().data('curVal', text);
                return false;
            }

            var preVal = $ul.data('curVal');
            if(preVal == text){
                return false;
            }

            timer = setTimeout(function(){
                $.ajax({
                    url: url,
                    dataType: 'json',
                    data: {'param' : text},
                    success: function (data) {

                        $ul.empty().css({
                            'top' : offset.top + height,
                            'left' : offset.left
                        });

                        $.each(data, function(i, obj){
                            var $li = $('<a href="javascript:void(0);" class="list-group-item"></li>')
                                .html(obj[textName].replace(new RegExp("("+text+")", "gi"),'<strong class="fc-red">$1</strong>'))
                                .data('id', obj[idName]).appendTo($ul).hover(function(){
                                    $ul.find('.list-group-item').removeClass('hover');
                                    $li.addClass('hover');
                                    $target.val(obj[idName]);
                                },function(){
                                    $li.removeClass('hover')
                                });
                        });

                        //记录当前input里的内容
                        $ul.data('curVal', text);
                    },
                    error:function(e){
                        
                    }
                });

            }, 600);
        };
    }

    return commonSearch;
});