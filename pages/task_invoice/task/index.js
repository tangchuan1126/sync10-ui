"use strict";
require([
     "../../../requirejs_config",
], function(){
    require(["jquery", "./js/views/mainView", "blockui"], function($, View){
    	$(function(){
    		$.blockUI.defaults = {
    	            css: {
    	                padding:        '8px',
    	                margin:         0,
    	                width:          '170px',
    	                top:            '45%',
    	                left:           '40%',
    	                textAlign:      'center',
    	                color:          '#000',
    	                border:         '3px solid #999999',
    	                backgroundColor:'#ffffff',
    	                '-webkit-border-radius': '10px',
    	                '-moz-border-radius':    '10px',
    	                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
    	                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
    	            },
    	            //设置遮罩层的样式
    	            overlayCSS:  {
    	                backgroundColor:'#000',
    	                opacity:        '0.3'
    	            },
    	            baseZ: 99999,
    	            centerX: true,
    	            centerY: true,
    	            fadeOut:  1000,
    	            showOverlay: true
    	        };

    		if(window.top === window){
                //单独打开的页面

                // Save data to sessionStorage
                var scheduleId = window.sessionStorage.getItem('SCHEDULE_ID');

                //清除数据
                window.sessionStorage.setItem('SCHEDULE_ID', '');
                
            }else{
                //在框架里嵌入的页面

                var data = window.top.window.frames["iframe"].task_invoice;

                var scheduleId = data ? data['SCHEDULE_ID'] : undefined;

                //清除数据
                if(scheduleId){
                    data['SCHEDULE_ID'] = undefined;
                }
            }

            new View({el:'#task_container'}).render(scheduleId);
    		
    	})
    	
    });

});
