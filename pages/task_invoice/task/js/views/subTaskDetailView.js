"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "../../../config.js",
  "../../../templates/templates.amd.js",
  "../../../initDept.js",
  "../../../artDialog.js",
//    "/Sync10-ui/pages/task_invoice/task/js/views/addSubTaskView.js",
"./../models/models.js",
"/Sync10-ui/pages/task_invoice/task/js/views/sortSubTaskView.js",
"/Sync10-ui/pages/task_invoice/task/js/views/subTaskListView.js",
" /Sync10-ui/pages/task_invoice/key/LRTypeKey.js",
"require_css!bootstrap-css/bootstrap.min",
"bootstrap",
//"require_css!/Sync10-ui/bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css",
//"multiselect",
"showMessage"
],function($, Backbone, Handlebars, Handlebars_ext, config, templates, initDept, Dialog, 
//      AddSubTaskView,
Models, SortSubTaskView, SubTaskListView, LRTypeKey){

  return Backbone.View.extend({
    template: templates.subTaskDetail,
    className: "subtask-detail",
    initialize: function(option){
     this.users = option.users;
     this.admin = option.admin;
     this.curOpened = option.curOpened;
   },
   render:function(model, isListPage){
    var _self = this;
    _self.isListPage = isListPage;
            _self.model = model;//主task信息
            var project_id = model.get("SCHEDULE_ID");//project_id
            var parent_subject = model.get("SCHEDULE_OVERVIEW");//主task主题
            var mainTask = model.get("SUBSCHEDULE");//所有main task
            
            _self.collection = new Models.SubTaskCollection(mainTask);
            _self.project_id = project_id;
            _self.parent_subject = parent_subject

            _self.$el.html(_self.template());


            _self.setSubTaskModel();

            if(!_self.subTaskListView){
              var subTaskListView = new SubTaskListView({'el': _self.$el.find('#subtask-list'), 'Project': _self.model});
              subTaskListView.parentView = _self;
              subTaskListView.admin = _self.admin;
              _self.subTaskListView = subTaskListView;
              _self.subTaskListView.render(project_id, _self.curOpened);
            }

            _self.dialog = new Dialog({
              content:  _self.$el,
              lock:true,
              width:$(window).innerWidth(),
              height: $(window).innerHeight() - 120,
              title: "Project ["+project_id+"] » Task",
              cancel: function(){
                if(_self.subTaskListView.$el.find('.panel-title.editing').length || !_self.subTaskListView.$el.find('.panel').not('.panel-info').length){
                //关闭的提示
                var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
                +'Your changes have not been saved, do you want to quit?</p>';

                var dialog = new Dialog({
                 content: html, 
                 lock:true,
                 width: 450,
                 height: 100,
                 title:'Warn » Cancel',
                 okValue:'OK',
                 ok: function (event) {
                   _self.closeDialog();
                 },
                 cancelValue: 'Cancel',
                 cancel: function(){
                   dialog.close();
                 }
               }).showModal();
              }else{
                _self.closeDialog();
              }
              return false;
            },         
            cancelDisplay: false,
            okValue:'Assign',
            ok: function(){
              if(_self.subTaskListView.isEditing()){
                return false;
              }

              console.log(_self.subTaskListView.getSelectedRow());

              _self.closeDialog();
            }
          }).showModal();

},
events: {

  "click #save_sub_task" : "saveSubTask",
            // "click i[name='editSubTask']" : "editSubTask",
            "click i[name='delSubTask']" : "delSubTask",
            "click i[name='closeSubTask']" : "closeSubTask",
          },
          closeDialog: function(){
            if(this.isListPage){
                //从list列表页面点击的弹框

                this.listView.loadingModal.show();

                var subTasks = this.subTaskListView.getTasks();
                var $dl = $('#sub_task_list_' + this.project_id);
                $dl.html(templates.sortedSubTasks({
                  'SUBSCHEDULE': subTasks, 
                  'SCHEDULE_ID' : this.project_id
                }));

                var $more = $dl.next();
                if(subTasks.length > 4 && !$more.length){
                  $dl.after('<a href="javascript:void(0);" data-task="'+ this.project_id +'" class="fc-orange fc-right underline J-sub-detail">More...</a>');
                }else if(subTasks.length <= 4 && $more.length){
                  $more.remove();
                }

            //更新model里的数据
            var taskModel = this.listView.collection.findWhere({
              SCHEDULE_ID: this.project_id
            });
            taskModel.set("SUBSCHEDULE", subTasks)

            //更新页面上的invoice的现实和数据
            var invoices = this.subTaskListView.getNewInvoices();
            var $idl = $('#invoice_list_' + this.project_id);
            if(invoices.length){
              $idl.append(templates.newInvoiceInTaskList({
                'SUBINVOICE': invoices,
                'SCHEDULE_ID': this.project_id,
                'TASK_STATUS': taskModel.get('TASK_STATUS'),
                'LRTypeKey': LRTypeKey,
              }));

              taskModel.set("SUBINVOICE", taskModel.get('SUBINVOICE').concat(invoices));
            }

            //更新页面上的invoice的状态
            //当task全部都关闭的时候，invoice的状态改变
            console.log(this.subTaskListView.isInvoiceOpen);
            if(this.subTaskListView.isInvoiceOpen){
              $idl.find('.J-invoice-status').text('OPEN');
            }


            this.dialog.close();
            this.listView.loadingModal.hide();
          }else{
            //不是从list列表页面点击的弹框
            this.listView.render();
            this.dialog.close();
          }
        },
        setSubTaskModel:function(){
         var _self = this;
         var subTask = _self.collection.toJSON();
            //设置新增的子任务的排在所有任务最后
            var task_number = 1;
            if(subTask.length==1){
            	task_number = subTask[0].TASK_NUMBER+1;
            }else if(subTask.length>1){
            	var subTaskFirstNumber = subTask[0].TASK_NUMBER;
            	var subTaskLastNumber = subTask[subTask.length-1].TASK_NUMBER;
            	if(subTaskFirstNumber>subTaskLastNumber){
            		task_number = subTaskFirstNumber+1;
            	}else{
            		task_number = subTaskLastNumber+1;
            	}
            }
//            console.log("排序后的 number="+task_number);
var taskModel = new Models.TaskModel();
taskModel.set("PARENT_SCHEDULE_ID",_self.project_id);
taskModel.set("PARENT_SUBJECT",_self.parent_subject);
taskModel.set("ASSIGN_USER_ID",_self.admin.toJSON().adid);
taskModel.set("ASSEMPLOYNAME", _self.admin.toJSON().employe_name);
            taskModel.set("IS_ADDITIONAL_TASK", 2);//默认不是additional task
            taskModel.set("TASK_NUMBER", task_number);//新增subtask时，默认为递增排序
            _self.subTaskModel = taskModel;
          },
          toggleSubTak: function(e){
           var title = "Add Sub Task";
           var _self = this;
        	 // _self.addSubTaskView.render(_self.subTaskModel,title);
           $('.add-subtask').toggle("fast");
           $('.add-btn').toggle();
           $('.chevron-up').toggle();
         },
        //刷新添加页面
        setAddSubTak: function(){
        	var title = "Add Sub Task";
        	var _self = this;
        	_self.setSubTaskModel();//设置subTask的模型
        	// _self.addSubTaskView.render(_self.subTaskModel,title);
        },
        //重置collection
        setTaskCollection:function(col){
        	this.collection = col;
//        	console.log(this.collection);
},
setSubTaskList:function(){
 var project_id = this.project_id;
 this.subTaskListView.render(project_id);
//        	console.log("refresh sub list");
},
editSubTask:function(e){
  alert(1);
  var _self = this;
  var btn = $(e.currentTarget);
  var schedule_id = btn.data("task") * 1;
  var sub_schedule_id = btn.data("subtask") * 1;
//            var taskModel = this.model;
var subTaskCollection = _self.collection;
var subTaskModel = subTaskCollection.findWhere({
 SCHEDULE_ID: sub_schedule_id
});
subTaskModel.set("PARENT_SUBJECT",_self.parent_subject);
var title = "Edit Sub Task ["+sub_schedule_id+"]";
            // this.addSubTaskView.render(subTaskModel,title);
            
            $('.add-subtask').css("display","block");
            $('.chevron-up').css("display","block");
            $('.add-btn').css("display","none");
            this.$el.find("div[class='subtask-detail']").scrollTop(0);
          },
          delSubTask:function(e){
           var btn = $(e.currentTarget),$parent=$(e.currentTarget).parent();
           var schedule_id = btn.data("task") * 1;
           var sub_schedule_id = btn.data("subtask") * 1;
           var _self = this;
           var subTaskCollection = _self.collection;
            var detSubTaskModel = subTaskCollection.findWhere({SCHEDULE_ID: sub_schedule_id});//要被删除的subTask的model
            
            var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            +'Delete Sub Task &nbsp;【'+sub_schedule_id+'】?</p>';
            
            var dialog = new Dialog({
             content: html, 
             lock:true,
             width: 400,
             height: 100,
             title:'Del Sub Task » '+schedule_id,
             okValue:'OK',
             ok: function (event) {
              subTaskCollection.remove(detSubTaskModel);
              detSubTaskModel.url = detSubTaskModel.url + "?SCHEDULE_ID=" + sub_schedule_id;

              detSubTaskModel.destroy({
                success: function(data, response) {
                 if(response.success){

                  showMessage("Success","succeed");

                  setTimeout(function(){
                   _self.setSubTaskList();
                 },1500);

                }else{
                  showMessage("Error","error");
                }
              },
              error: function() {
               showMessage("Error","error");
             }
           });
            },
            cancelValue: 'Cancel',
            cancel: function(){
             dialog.close();
           }
         }).showModal();

return false;
},
closeSubTask:function(e){
 var btn = $(e.currentTarget);
 var sub_schedule_id = btn.data("subtask") * 1;
 var sub_subject = btn.data("subject") * 1;
 if (!sub_schedule_id) return;
 var _self = this;
            var task_status = 3;//关闭close，task状态改变为
            var subTaskModel = new Models.SubTaskModel();
            subTaskModel.url = config.closeTaskUrl;
            var schedule_id = btn.data("task") * 1;
//            var subject = btn.data("subject");

var dialog = new Dialog({
  content: '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">Close Sub Task &nbsp;【'+sub_subject+'】?</p>', 
  lock:true,
  width: 400,
  height: 100,
  title:'Close Sub Task » '+sub_schedule_id,
  okValue:'OK',
  ok: function (event) {
    subTaskModel.save({
      SCHEDULE_ID: sub_schedule_id,
      TASK_STATUS: task_status,
                        IS_MAIN_SCHEDULE: 2//子任务
                      },{
                        success:function(model, response, options){
                          if(response.success){
                           showMessage("Success","succeed");

                           setTimeout(function(){
                             _self.setSubTaskList();
                           },1500);
                         }else{
                          showMessage("Error","error");
                        }
                        dialog.close();
                      },
                      error:function(model, response){
                        showMessage("Error","error");
                        dialog.close();
                      }
                    }); 
  },
  cancelValue: 'Cancel',
  cancel: function(){
    dialog.close();
  }
}).showModal();

return false;
}
});
});
