/**
 * Created by gql on 2015-6-9 16:18:16
 */
 "use strict";
 define([
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "../../../config.js",
  "../../../templates/templates.amd.js",
  "./../models/models.js",
  "/Sync10-ui/pages/task_invoice/task/js/views/selectUserView.js",
  "../../../artDialog.js",
  "/Sync10-ui/pages/task_invoice/invoice/js/views/addInvoiceView.js",
  "./sortSubTaskView.js",
  "./sortSubTaskView.js",
  "../../../date.js",
  "bootstrap.datetimepicker",
  "require_css!bootstrap.datetimepicker-css",
  "/Sync10-ui/bower_components/bootstrap/js/collapse.js",
  "require_css!Font-Awesome"
  ],function($, Backbone, Handlebars, Handlebars_ext, config, templates, Models, SelectUserView,
    Dialog, AddInvoiceView, SortSubTaskView){
    var taskStateKey = {Pending:1, Processing:2, Closed:3, Open:4};
    return Backbone.View.extend({
      template: templates.subTaskList,
      collection:new Models.SubTaskCollection(),
      initialize: function(options){
       this.setElement(options.el);
       this.Project = options.Project;
       this.invoices = [];
     },
     render:function(project_id, curSub_id){
      var _self = this;
      _self.PROJECT_ID = project_id;
      
      _self.collection.url = config.getSubTaskUrl+"&PARENT_SCHEDULE_ID="+project_id;
      _self.collection.reset();
      _self.collection.fetch({
        data:"",
        success: function(collection, res){
          var list = collection.toJSON();

                    //add by huhy on 2015-6-30 09:57:03
                    if(!list.length){
                        //一条数据都没有的时候，给添加按钮一个醒目提示
                        _self.$el.find('.J-addTask').addClass('marked');
                      }

                      var $list = _self.$el.find('#accordionSubTaskList');

//                      _self.invoices = collection.PROJECTINVOICE || [];
                      //edit by gql for invoices status==IMPORTED
                      _self.invoices = [];
                      $.each(collection.PROJECTINVOICE,function(i){
                    	  if(this.INVOICE_STATUS==1){
                    		  _self.invoices.push(this);
                    	  }
                      });
                      
                      $list.append(_self.template({'list':list,'taskStateKey':taskStateKey}));
                      _self.resetWidth(_self.$el);

                      _self.setDatetimepicker($('#accordionSubTaskList .form_datetime'));
                      
                      _self.parentView.setTaskCollection(collection);
                      _self.parentView.setAddSubTak();

                      //当有列表没有被选中，去掉全选按钮的选中状态
                      // if($list.find('.checkbox').length != $list.find('.checked').length)
                      //   var all_box = $list.find('.J-checkbox-all').removeClass('checked').addClass('disabled').find('input[type="checkbox"]').get(0);
                      // all_box.checked = false;
                      // all_box.disabled = "disabled";

                    //add by huhy in2015-6-26 14:54:42  
                    //默认打开列表之后，展开对应的项
                    //如果点击的是more，或者是点击的新增按钮，展开第一项
                    if($list.children().length > 1){
                      if(curSub_id){
                        var $curSub = _self.$el.find('#heading_' + curSub_id);

                        $curSub.length && ($curSub.find('.panel-title').trigger('click'));

                      }else{
                        _self.$el.find('#accordionSubTaskList').find('.panel:eq(1) .panel-title').trigger('click');
                      }
                    }
                  }
                });

},
getTasks: function(){
  return this.collection.toJSON();
},
getNewInvoices: function(){
  var invoices = [];
  
  $.each(this.invoices, function(i, obj){
    if(obj.isSel && obj.newInvoice){
      invoices.push(obj.newInvoice.toJSON());
    }
  });
  return invoices;
},
events: {
  "click .panel-title" : "openPanel",
  "click .checkbox:not(.disabled)" : "selectRow",
  "click .J-addTask" : "addTask",
  "click .J-task-edit" : "editTaskRow",
  "click .edit-form" : "defaultPrevented",
  "click .J-ok" : "modifyRow",
  "click .J-cancel" : "cancelEdit",
  "click .J-newAddTask": "newAddTask",
  "click .J-newAddTask-cancel": "cancelNewAddTask",
  "click .J-task-del" : "delTask",
  "click .J-task-close" : "closeTask",
  "click .J-subTask-add" : "addSubTask",
  "click .J-sub-cancel" : "cancelAddSubTask",
  "click .J-sub-ok" : "addSubTaskRow",
  "click .J-sub-remove" : "delSubTask",
  "click .J-sub-close" : "closeSubTask",
  "click .J-subTask-sort" : "sortSubTask",
  "click .J-sub-edit": "editSubTask",
//            "click .J-select-user": "selectUser",
"focus .J-select-user": "selectUser",
"click .J-del-invoice" : "delInvoice",
"click .J-add-invoice": "addInvoice",
"change .sel-invoice>.form-control": "selInvoice"
},
        //展开panel
        openPanel: function(e){
          if(e.currentTarget.className.indexOf('editing') != -1){
            return false;
          }

          if(e.currentTarget.className.indexOf('collapsed') === -1){
            return;
          }

            //处理滚动条
            var $panel = $(e.currentTarget).parents('.panel:eq(0)');
            if($panel.hasClass('.panel-info')){
              return false;
            }

            var $container = this.$el.parent();
            var $expanded = $container.find('.panel-collapse.in');

            var expandedH = $expanded.length && $expanded.parent().index() < $panel.index() ? $expanded.outerHeight() : 0;
       
            $container.animate({
              "scrollTop": $panel.position().top + $container.scrollTop() - expandedH
            });
          },
        //点击添加主Task按钮
        addTask: function(e){

          $(e.currentTarget).removeClass('marked');

            //判断是否已经存在一条还未完成的editing
            if(this.isEditing()){
              return false;
            }
            
            var $list = $('#accordionSubTaskList'),
            data = {};
            
            if($list.find('.panel').not('.panel-info').length){
            	var $lastPanel = $('#accordionSubTaskList .panel:last-child');
            	var exe_employ_name = $lastPanel.find(".J-select-user").val();
              var schedule_execute_id = $lastPanel.find(".J-schedule_execute_id").val();
              data.EXEEMPLOYNAME=exe_employ_name;
              data.SCHEDULE_EXECUTE_ID = schedule_execute_id;
            }else{
             data.EXEEMPLOYNAME=this.Project.get('EXEEMPLOYNAME');
             data.SCHEDULE_EXECUTE_ID = this.Project.get('SCHEDULE_EXECUTE_ID');
           }

          //modify by huhy in 2015-6-23 16:03:52
          data.START_TIME = this.Project.get('START_TIME');
          data.END_TIME = this.Project.get('END_TIME');
         //end

         $list.append(templates.newAddTask({'model':data, 'invoices': this.invoices.length? this.invoices : false}));
         $('#accordionSubTaskList .panel:last-child .form-control').first().focus();

         this.setDatetimepicker($('#accordionSubTaskList .panel:last-child .form_datetime'));

         this.closePanel();
         this.resetWidth($('#newAddTask'));

         return false;
       },
        //添加主Task
        newAddTask: function(e){
        	var _self = this;
        	var $panel = $(e.currentTarget).parents('.panel-title:eq(0)');

            //TODO... 一系列验证处理
            if(!this.validateTaskRow($panel))
              return false;



            //TODO... 处理一系列数据....
            var taskModel = new Models.TaskModel();
        	//页面上的信息
         var task_number = _self.setTaskNumber(_self.collection);
         var schedule_execute_id = $panel.find(".J-schedule_execute_id").val()==null?[]:$panel.find(".J-schedule_execute_id").val().split(",");
         var start_time = $panel.find(".J-start_time").val();
         var end_time = $panel.find(".J-end_time").val();
         var schedule_detail = $panel.find(".J-schedule_detail").val();
         var exe_employ_name = $panel.find(".J-select-user").val();
         var project_id = _self.PROJECT_ID;
         var invoice = $panel.find('.selcted-invoice span').text();

         taskModel.save({
          PROJECT_ID:project_id,
          TASK_STATUS: taskStateKey.Open,
          TASK_NUMBER:task_number,
          SCHEDULE_EXECUTE_ID: schedule_execute_id,
          START_TIME: start_time,
          END_TIME: end_time,
          EXEEMPLOYNAME:exe_employ_name,
          SCHEDULE_DETAIL:schedule_detail,
          SUBINVOICE:[invoice],
          ASSIGN_USER_ID: _self.admin.get('adid')
        }, 
        {
          success:function(model, response, options){
           if(response.success){
            model.set("SCHEDULE_ID",response.schedule_id);
            model.set("BILL_ID", invoice);
            console.log(model.toJSON());
                		//数据处理完之后的回调显示
                    $("#newAddTask").replaceWith(_self.template({'list':[model.toJSON()],'taskStateKey':taskStateKey}));

                    var $collapsed = $('#heading_' + response.schedule_id);

                    _self.resetWidth($collapsed);
                		  _self.setDatetimepicker($('#accordionSubTaskList .panel:last-child .form_datetime'));// 设置时间控件
                		  _self.collection.add(model);

                          //展开panel
                          $collapsed.find('.collapsed').trigger('click');
                        }else{
                          showMessage("Error","error");
                        }
                      },
                      error:function(model, response){
                        showMessage("Error","error");
                      }
                    }); 

return false;
},
delTask:function(e){
 var _self = this;
 var btn = $(e.currentTarget),
 $parent=$(e.currentTarget).parents('.panel:eq(0)');
 var schedule_id = btn.data("task") * 1;
 var detail = $parent.find(".J-schedule_detail").val();

 var taskCollection = _self.collection;
            var delTaskModel = taskCollection.findWhere({SCHEDULE_ID: schedule_id});//要被删除的subTask的model
            
            var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            +'Delete Task &nbsp;【'+detail+'】?</p>';
            
            var dialog = new Dialog({
             content: html, 
             lock:true,
             width: 400,
             height: 100,
             title:'Del Task » '+schedule_id,
             okValue:'OK',
             ok: function (event) {
               delTaskModel.url = delTaskModel.url + "?SCHEDULE_ID=" + schedule_id;
               delTaskModel.destroy({
                success: function(data, response) {
                 if(response.success){
                  showMessage("Success","succeed");
                  _self.collection.remove(delTaskModel);
                  $parent.remove();
                }else{
                  showMessage("Error","error");
                }
              },
              error: function() {
               showMessage("Error","error");
             }
           });
             },
             cancelValue: 'Cancel',
             cancel: function(){
               dialog.close();
             }
           }).showModal();

            return false;
          },
          closeTask:function(e){
           var _self = this;
           var btn = $(e.currentTarget),$parent=$(e.currentTarget).parents('.panel:eq(0)');
           var schedule_id = btn.data("task") * 1;
           var detail = $parent.find(".J-schedule_detail").val();
           if (!schedule_id) return false;
           
            //判断子任务是否都关闭
            var flag = true;
            var sub_subject = "";
            var status = $parent.find(".J-sub_status");
            for(var i=0;i<status.length;i++){
            	var _this = status[i];
            	var task_status =$(_this).html().trim();
              if(task_status.indexOf("Closed")==-1){
               sub_subject = $(_this).parents("tr:eq(0)").find(".J-sub_detail").val();
               flag = false;
               break;
             }
           }

            //子任务都关闭，关闭主任务
            if(flag){

              var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
              +'Close Task &nbsp;【'+detail+'】?</p>';

              var dialog = new Dialog({
               content: html, 
               lock:true,
               width: 400,
               height: 100,
               title:'Close Task » '+schedule_id,
               okValue:'OK',
               ok: function (event) {
                var task_status = taskStateKey.Closed;//关闭close，task状态改变为
                var taskModel = _self.collection.findWhere({SCHEDULE_ID: schedule_id});
                taskModel.url = config.closeTaskUrl;
                taskModel.save({
                 SCHEDULE_ID:schedule_id,
                 TASK_STATUS:task_status
               },
               {
                success:function(model, response, options){
                 if(response.success){
                  $parent.find(".J-task_status").addClass('status-closed').html("Closed");
                //不可选状态
                var checkbox = $parent.find('.checkbox').removeClass('checked').addClass('disabled').find('input[type="checkbox"]').get(0);
                checkbox.checked = false;
                checkbox.disabled = "disabled";

                $parent.find(".operate-btns").empty();
                showMessage("Success","succeed");
                taskModel.set('TASK_STATUS', task_status);

                      //判断是否都关闭了
                      if(response.is_invoice_open){
                        _self.isInvoiceOpen = true;
                      }

                      //子任务一条都没有的情况下
                      if(!status.length){
                        // $parent.find('.J-sub-list').remove();
                        $parent.find('th:last-child').empty();
                      }


                    }else{
                     showMessage("Error","error");
                   }
                 },
                 error:function(model, response){
                   showMessage("Error","error");
                 }
               });
                },
                cancelValue: 'Cancel',
                cancel: function(){
                 dialog.close();
                }
                }).showModal();

                }else{
                 showMessage("First close the sub task 【" +sub_subject+ "】","alert");
                }

return false;
},
        //删除invoice
        delInvoice: function(e){
          var $target = $(e.currentTarget);
            //提示？

            //数据删除操作？

            //页面效果
            $target.parents('.edit-invoice:eq(0)').addClass('editing').find('.form-control').val('');

            //数据更新
            this.modifyInvoiceInNewly($target.prev().text(), false);
          },
        //添加invoice
        addInvoice: function(e){
          var _self = this;
          var $target = $(e.currentTarget);

          new AddInvoiceView({'admin': this.admin, 'saveCallback': function(model, response, dialog){

            dialog.close();


            var invoice = response['BILL_ID'];

                //回填id
                _self.addInvoiceCallback(invoice, $target);
                
                //将新增的invoice的id添加进invoices集合中
                //将新增的invoice保存
                model.set('BILL_ID', invoice);
                model.set('INVOICE_STATUS', 1); //状态
                _self.invoices.push({
                  'BILL_ID': invoice, 
                  'newInvoice': model,
                  'isSel': true
                });

                //更新invoice的可选项
                $target.removeClass('error').prev().html(templates.selectOptions({'invoices': _self.invoices}));

              }}).render();
        },
        //选择invoice
        selInvoice: function(e){
          var $sel = $(e.currentTarget);
          var invoice = $sel.val();

          this.modifyInvoiceInNewly(invoice, true);

          this.addInvoiceCallback(invoice, $sel);

          return false;
        },
        //
        modifyInvoiceInNewly: function(BILL_ID, isSel){
          var invoices = this.invoices;
          for(var i = 0, len = invoices.length; i < len; i++){
            var model = invoices[i];
            if(model.BILL_ID == BILL_ID){
              model.isSel = isSel;
              break;
            }
          }
        },
        //添加invoice成功(或者选择了invoice)的回调
        addInvoiceCallback: function(invoice, $target){
          if(invoice){
            var $parent = $target.parents('.edit-invoice:eq(0)');
            $parent.removeClass('editing').find('.selcted-invoice span').text(invoice);
          }
        },
        //取消添加主Task
        cancelNewAddTask: function(e){
          $("#newAddTask").remove();

            //add by huhy on 2015-6-30 10:04:23
            //当一条数据都没有添加的时候，提醒
            if(!this.$el.find('.panel').not('.panel-info').length){
              this.$el.find('.J-addTask').addClass('marked');
            }

            return false;
          },
        //收缩展开的panel
        closePanel: function(){
          $('#accordionSubTaskList').find('.panel-title').not('.collapsed').trigger('click');
        },
        //点击checkbox
        selectRow: function(e){
          if($(e.currentTarget).parents('.panel-title:eq(0)').hasClass('editing')){
            return false;
          }

          var $target = $(e.currentTarget);

          var checkbox = $target.find('input[type="checkbox"]').get(0);

          $target.toggleClass('checked');

          if($target.hasClass('checked')){
            checkbox.checked = true;
          }else{
            checkbox.checked = false;
          }

          this.selectAll($target, checkbox);

          return false;
        },
        //编辑主Task行
        editTaskRow: function(e){
            //已经存在编辑状态的行
            if(this.isEditing()){
              return false;
            }

            var $parent =  $(e.currentTarget).parents(".panel-title:eq(0)");
            var $checkbox = $parent.find('.checkbox');

            //已经被选中
            if($checkbox.hasClass('checked')){
              $checkbox.trigger('click');
            }

            this.closePanel();

            $parent.addClass('editing');

            //更新invoice的可选项
            $parent.find('.edit-invoice .form-control').html(templates.selectOptions({'invoices': this.invoices}));



            return false;
          },
        //取消编辑
        cancelEdit: function(e){
        	var $parent = $(e.currentTarget).parents(".panel-title:eq(0)");
        	$parent.removeClass('editing').find('.edit-form').each(function(i, el){
            var $el = $(el);
            $el.val($el.prev().text());
          });

          $parent.find('.editing').removeClass('editing').end().parent().removeClass('error');

          var $id = $parent.find(".J-schedule_execute_id");
          $id.val($id.next().val());
          
          return false;
        },
        //验证task行
        validateTaskRow: function($parent){
          var arr = $parent.find('.edit-form').not('.edit-invoice').not('.J-select-user');

          for(var i = 0, len = arr.length; i < len; i++){
            var $el = $(arr[i]);
            if(!$el.val()){
              $el.focus().click();

              return false;
            }
          }

          var $invoice = $parent.find('.editing');
          if($invoice.length){
                //invoice属于编辑状态
                var $sel = $invoice.find('.form-control');
                if($sel.length){
                  $sel.focus();
                }else{
                  $invoice.find('.J-add-invoice').addClass('error');
                }

                return false;
              }

              return true;
            },
        //修改Task
        modifyRow: function(e){
          var $parent = $(e.currentTarget).parents(".panel-title:eq(0)");


            //一系列验证...
            if(!this.validateTaskRow($parent))
              return false;

            /* TODO... 数据后台处理？ */
            var taskModel = new Models.TaskModel();
            var _self = this;
        	//页面上的信息
         var schedule_id = $parent.data('schedule');
         var task_number = $parent.data("number");
         var schedule_execute_id = $parent.find(".J-schedule_execute_id").val()==null?[]:$parent.find(".J-schedule_execute_id").val().split(",");
         var start_time = $parent.find(".J-start_time").val();
         var end_time = $parent.find(".J-end_time").val();
         var schedule_detail = $parent.find(".J-schedule_detail").val();
         var deleteusers = $parent.find(".J-deleteusers").val();
         var project_id = this.PROJECT_ID;
         var invoice = $parent.find(".selcted-invoice span").text();
         var delsubinvoice = $parent.find(".info-invoice").text();

         taskModel.save({
          SCHEDULE_ID:schedule_id,
          DELETEUSERS:deleteusers,
          TASK_NUMBER:task_number,
          PROJECT_ID:project_id,
          START_TIME: start_time,
          END_TIME: end_time,
          SCHEDULE_DETAIL:schedule_detail,
          SCHEDULE_EXECUTE_ID: schedule_execute_id,
          SUBINVOICE:[invoice],
          DELSUBINVOICE:[delsubinvoice],
          ASSIGN_USER_ID: _self.admin.get('adid')
        }, 
        {
          success:function(model, response, options){
           if(response.success){
                		  //关闭修改，页面显示
                		  $parent.removeClass('editing')
                      .find('.info').not('.info-invoice').each(function(i, el){
                        var $el = $(el);
                        $el.text($el.next().val());
                      });
                          //invoice的回调处理
                          $parent.find(".info-invoice").text(invoice);

                          $parent.find('.editing').removeClass('editing');
                          $parent.find(".J-deleteusers").val($parent.find(".J-schedule_execute_id").val());
                        }else{
                          showMessage("Error","error");
                        }

                    //关闭error提示样式
                    $parent.parent().removeClass('error');
                  },
                  error:function(model, response){
                    showMessage("Error","error");
                  }
                }); 

return false;
},
        //阻止冒泡
        defaultPrevented: function(){
          return false;
        },
        //全选
        selectAll: function($target, checkbox){

          var rows = this.$el.find('.checkbox').not('.disabled');
          var selectedRows = this.$el.find('.checkbox.checked');

          if($target.hasClass('J-checkbox-all')){
            if(checkbox.checked){
                    //TODO... 全选按钮禁用的情况....处理
                    //没有内容行时, 只有一行且这行处于编辑状态时
                    // if($("#accordionSubTaskList").find('.panel').length <= 1 || $("#accordionSubTaskList").find('.panel').length === 2 && this.isEditing()){
                    //     $target.removeClass('checked').find('input[type="checkbox"]')[0].checked = false;
                    //     return;
                    // }

                    $.each(rows, function(i, el){
                      $(el).addClass('checked').find('input[type="checkbox"]')[0].checked = true;
                    });
                  }else{
                   $.each(selectedRows, function(i, el){
                    $(el).removeClass('checked').find('input[type="checkbox"]')[0].checked = false;
                  });
                 }
                 return;
               }

               if(!checkbox.checked){
                //取消全选
                $('#accordionSubTaskList .J-checkbox-all').removeClass('checked').find('input[type="checkbox"]').get(0).checked = false;
              }else if(selectedRows.length == rows.length - 1){
                $('#accordionSubTaskList .J-checkbox-all').trigger('click');
              }
            },
        //获取选中行的数据
        getSelectedRow: function(){
          var rows = this.$el.find('input[name="check-tack"]:checked');
          var result = [];
          $.each(rows, function(i, elem){
            result.push({"SCHEDULE_ID": elem.value});
          });

          return result;
        },
        //重置宽度
        resetWidth: function($el){
          var rows = $el.find('.panel-title');
          var rWidth = $(rows[0]).innerWidth();
          var arrW = [5, 25, 20, 10, 10, 10, 10, 10];

          $.each(rows, function(i, elem){
            $(elem).children().each(function(i, el){
              var $el = $(el);
              var $con = $(el).find('>div');
              if($con.length){
                $el = $con;
              }
              $el.outerWidth(rWidth * arrW[i] / 100);
            });
          });
        },
        //判断是否有处于编辑状态中
        isEditing: function(){
          var $editing = this.$el.find('.panel-title.editing');
          if($editing.length){
                //TODO... 一系列的提示?
                $editing.parent().addClass('error')
                return true; 
              }
              return false;
            },
            /*一下是sub task的相关操作*/
        //添加subtask
        addSubTask: function(e){
          var $table = $(e.currentTarget).parents('.table:eq(0)');
          var parent_schedule_id = $(e.currentTarget).data("task");
            //有处于编辑状态的行
            if(this.isEditing_Sub($table)){
                //TODO...

                return false;
              }
              
              var data = {};
              if($table.find('tbody tr').not(".no-data").length){
               var $lastTr = $table.find('tbody tr:last-child');
               var exe_employ_name = $lastTr.find(".J-select-user").val();
               var schedule_execute_id = $lastTr.find(".J-sub_execute_id").val();
               var task_number = $lastTr.find(".J-sub_number").val()*1+ 1;
               var lineNo = $table.find('.J-sub-list').find('tr').length + 1;
               data.lineNo = lineNo;
               data.TASK_NUMBER = task_number;
               data.EXEEMPLOYNAME=exe_employ_name;
               data.SCHEDULE_EXECUTE_ID = schedule_execute_id;
             }else{
               $table.find('tbody tr.no-data').remove();
               data.lineNo = 1;
               data.TASK_NUMBER = 1;
             }

             data.isEdit = true;
             data.TASK_STATUS = taskStateKey.Open;

             $table.find('.J-sub-list').append(templates.newAddSubTask({
              "SUBSCHEDULE":[data],
              "SCHEDULE_ID": parent_schedule_id, 
              "taskStateKey": taskStateKey
            }));
             $table.find('.J-sub-list tr:last-child').find('.form-control').first().focus();

             return false;
           },
        //取消添加subtask
        cancelAddSubTask: function(e){
          var $tr = $(e.currentTarget).parents("tr:eq(0)");
          if(!$tr.find('.J-sub-Id').val()){
            $(e.currentTarget).parents('tr:eq(0)').remove();
          }else{
            $tr.removeClass('editing').find('.edit-form').each(function(i, el){
              var $el = $(el);
              $el.val($el.prev().text());
            });
          }
        },
        //添加一行subtask数据
        addSubTaskRow: function(e){
            //TODO... 一系列验证处理


            //TODO... 处理一系列数据....
            var _self=this;
            var $parent = $(e.currentTarget).parents("tr:eq(0)");

            var taskModel = new Models.TaskModel();
        	//页面上的信息
         var schedule_id = $parent.find(".J-sub-Id").val();
         var task_number = $parent.find(".J-sub_number").val();
         var schedule_execute_id = $parent.find(".J-sub_execute_id").val()==null?[]:$parent.find(".J-sub_execute_id").val().split(",");
         var schedule_detail = $parent.find(".J-sub_detail").val();
         var parent_schedule_id = $parent.data("task");
         var project_id = _self.PROJECT_ID;

         if(schedule_id){
          var deleteusers = $parent.find(".J-sub_deleteusers").val();
        		 taskModel.set("SCHEDULE_ID",schedule_id);//修改时，给idAttribute赋值，表示是修改
        		 taskModel.set("DELETEUSERS",deleteusers);//修改之前，执行人的id
           }

           taskModel.save({
            PARENT_SCHEDULE_ID: parent_schedule_id,
            SCHEDULE_EXECUTE_ID: schedule_execute_id,
            PROJECT_ID:project_id,
            SCHEDULE_DETAIL:schedule_detail,
        		IS_MAIN_SCHEDULE: 2,//子任务
        		TASK_NUMBER:task_number,
            ASSIGN_USER_ID: _self.admin.get('adid')
          }, 
          {
            success:function(model, response, options){
             if(response.success){
              model.set("SCHEDULE_ID",response.schedule_id);
                		  //关闭修改，页面显示
                		  $parent.removeClass('editing').find('.info').each(function(i, el){
                        var $el = $(el);
                        $el.text($el.next().val());
                      });
                      $parent.find('.J-sub-Id').val(model.get("SCHEDULE_ID"));
                      $parent.find('.J-sub_deleteusers').val(model.get("SCHEDULE_EXECUTE_ID"));
                      $parent.get(0).id = "sub_task_" + response.schedule_id;
                    }else{
                      showMessage("Error","error");
                    }
                  },
                  error:function(model, response){
                    showMessage("Error","error");
                  }
                }); 
},
        //修改subtask
        editSubTask: function(e){
            //已经存在编辑状态的行
            if(this.isEditing_Sub($(e.currentTarget).parents('.table:eq(0)'))){
              return false;
            }

            var $parent =  $(e.currentTarget).parents("tr:eq(0)");

            $parent.addClass('editing');

          },
          closeSubTask:function(e){
           var _self = this;
           var $parent = $(e.currentTarget).parents("tr:eq(0)");
           var sub_schedule_id = $parent.find(".J-sub-Id").val();
           var sub_subject = $parent.find(".J-sub_detail").val();
           if (!sub_schedule_id) return;
            var task_status = taskStateKey.Closed;//关闭close，task状态改变为
            var subTaskModel = new Models.SubTaskModel();
            subTaskModel.url = config.closeTaskUrl;

            var dialog = new Dialog({
              content: '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">Close Sub Task &nbsp;【'+sub_subject+'】?</p>', 
              lock:true,
              width: 400,
              height: 100,
              title:'Close Sub Task »【'+sub_schedule_id+'】',
              okValue:'OK',
              ok: function (event) {
                subTaskModel.save({
                  SCHEDULE_ID: sub_schedule_id,
                  TASK_STATUS: task_status,
                        IS_MAIN_SCHEDULE: 2//子任务
                      },{
                        success:function(model, response, options){
                          if(response.success){
                           showMessage("Success","succeed");
                           $parent.find(".J-sub_status").addClass('status-closed').html("Closed");
                           $parent.find(".operate-btns").empty();

                                //更新状态
                                $('#sub_task_' + sub_schedule_id).data('status', task_status);

                                //如果所有的sub task 都关闭了，更新主task的状态
                                if(response.is_main_close){
                                  var schedule_id = $parent.data('task');
                                  var taskModel = _self.collection.findWhere({'SCHEDULE_ID': schedule_id});
                                  taskModel.set('TASK_STATUS', task_status);

                                    //页面显示
                                    var $panel = $parent.parents('.panel:eq(0)');
                                    $panel.find('.J-task_status').addClass('status-closed').html('Closed');
                                    //不可选状态
                                    var checkbox = $panel.find('.checkbox').removeClass('checked').addClass('disabled').find('input[type="checkbox"]').get(0);
                                    checkbox.checked = false;
                                    checkbox.disabled = "disabled";

                                    $panel.find('th:last-child').empty();
                                    $panel.find('.operate-btns').empty();
                                  }

                                 //判断是否都关闭了
                                 if(response.is_invoice_open){
                                  _self.isInvoiceOpen = true;
                                }

                              }else{
                                showMessage("Error","error");
                              }
                            },
                            error:function(model, response){
                              showMessage("Error","error");
                            }
                          }); 
},
cancelValue: 'Cancel',
}).showModal();

},
delSubTask:function(e){
  var _self = this;
  var $parent = $(e.currentTarget).parents("tr:eq(0)");
  var sub_schedule_id = $parent.find(".J-sub-Id").val();
  var sub_subject = $parent.find(".J-sub_detail").val();

  if (!sub_schedule_id) return;

  var detSubTaskModel = new Models.SubTaskModel({'SCHEDULE_ID':sub_schedule_id,"SCHEDULE_DETAIL":sub_subject});

  var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
  +'Delete Sub Task &nbsp;【'+sub_subject+'】?</p>';
  var dialog = new Dialog({
   content: html, 
   lock:true,
   width: 400,
   height: 100,
   title:'Del Sub Task » '+sub_subject,
   okValue:'OK',
   ok: function (event) {
     detSubTaskModel.url = detSubTaskModel.url + "?SCHEDULE_ID=" + sub_schedule_id;
     detSubTaskModel.destroy({
      success: function(data, response) {
       if(response.success){
        showMessage("Success","succeed");
		                    // setTimeout(function(){
                          var $next = $parent.next();
                          $parent.remove();
                                //更改序号
                                while($next.length){
                                  var $td = $next.find('.J-sub-lineNo');
                                  $td.text($td.text() * 1 - 1);
                                  $next = $next.next();
                                }

		                  // },1500);

   }else{
    showMessage("Error","error");
  }
},
error: function() {
 showMessage("Error","error");
}
});
   },
   cancelValue: 'Cancel',
 }).showModal();

return false;
},
         //subtask排序
         sortSubTask: function(e){
          var btn = $(e.currentTarget);
          var schedule_id = btn.data("task") * 1;
          var _self = this;
          var subTasks = [];
          $('#collapse_' + schedule_id).find('.J-sub-list tr').not('.no-data').each(function(i, el){
            var $el = $(el);
            var obj = {};
            obj['SCHEDULE_ID'] = $el.find('.J-sub-Id').val();
            obj['TASK_STATUS'] = $el.data('status');
            obj['TASK_NUMBER'] = $el.find('.J-sub_number').val();
            obj['SCHEDULE_DETAIL'] = $el.find('.J-sub_detail').val();
            obj['EXEEMPLOYNAME'] = $el.find('.J-select-user').val();
            obj['SCHEDULE_EXECUTE_ID'] = $el.find('.J-sub_execute_id').val();

            subTasks.push(obj);
          });

          if(subTasks.length>0){
            var sortSubTaskView = new SortSubTaskView();
            sortSubTaskView.render(subTasks);

            var dialog = new Dialog({
              content: sortSubTaskView.$el,
              lock:true,
              width: 500,
              height: 380,
              title:'Sort Subtasks',
              okValue:'Ok',
              ok: function (event) {
                        // _self.loadingModal.show();
                        var data = sortSubTaskView.getResult();
                        var subTasks = sortSubTaskView.getSortedSubTasks();

                        $.ajax({
                          url: config.selectUrl + '?key=sortSubScheduleNumber',
                          type:'post',
                          data: {'sortSubTask': JSON.stringify(data)},
                          dataType: 'json',
                          success: function (res) {

                                //排序之后的界面处理
                                btn.parents('.table:eq(0)').find('.J-sub-list').html(templates.newAddSubTask({
                                  'SUBSCHEDULE': subTasks, 
                                  'SCHEDULE_ID' : schedule_id,
                                  "taskStateKey": taskStateKey
                                }));

                                dialog.close();
                              },
                              error:function(e){
                              }
                            });
                        
                      },
                      cancelValue: 'Cancel',
                      cancel: function(){
                        dialog.close();
                      }
                    }).showModal();
}

return false;
},
        //判断是有有处于编辑状态的subtask
        isEditing_Sub: function($table){
          if($table.find('.editing').length){
                //TODO... 一系列的提示?
                return true; 
              }
              return false;
            },
            setTaskNumber:function(collection){
             var _self = this;
             var tasks = collection.toJSON();
            //设置新增的子任务的排在所有任务最后
            var task_number = 1;
            if(tasks.length==1){
            	task_number = tasks[0].TASK_NUMBER+1;
            }else if(tasks.length>1){
            	var taskFirstNumber = tasks[0].TASK_NUMBER;
            	var taskLastNumber = tasks[tasks.length-1].TASK_NUMBER;
            	if(taskFirstNumber>taskLastNumber){
            		task_number = taskFirstNumber+1;
            	}else{
            		task_number = taskLastNumber+1;
            	}
            }
            return task_number;
          },
        //分配人员，to
        selectUser:function(e){
        	var _self = this;
        	var $name = $(e.currentTarget);
        	var $id = $(e.currentTarget).next("input");
        	var value = $id.val();

          $(e.currentTarget).blur();
          
            //task to时
            var newAdmin = {};
            newAdmin.adgid = _self.admin.toJSON().adid; 
        	newAdmin.ps_id =  "1000005"; //Valley
         	newAdmin.dept_id = "1000002";//Warehouse
     		newAdmin.post_id = "-1";//All

        var selectUserView = new SelectUserView({'newAdmin':newAdmin,'loadingModal':_self.loadingModal,'admin':_self.admin,'value':value,'userName':$name,'userId':$id});
        selectUserView.render();
      },
        //设置时间控件
        setDatetimepicker:function($id){
        	$id.datetimepicker({
            format: "mm/dd/yy",
            minView: 2,
            autoclose:1,
            forceParse: 0,
            bgiconurl:"",
          }).on('changeDate', function(ev){
           var $self = $(this);
           if($self.hasClass('start-time')){
             $self.parent().parent().next().find('.form_datetime').datetimepicker('setStartDate', ev.date);
           }else if($self.hasClass('end-time')){
             $self.parent().parent().prev().find('.form_datetime').datetimepicker('setEndDate', ev.date);
           }
         });
        },

      });
});