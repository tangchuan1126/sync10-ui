/**
 * Created by huhy on 2015-3-30 10:27:16
 */
 "use strict";
 define([
    "jquery",
    "backbone",
    "handlebars",
    "handlebars_ext",
    "../../../config.js",
    "../../../templates/templates.amd.js",
    "./../models/models.js",
    "./addTaskView",
    "./sortSubTaskView",
//     "./addSubTaskView",
"./addTaskLogView",
"./logDetailView",
"./subTaskDetailView",
"/Sync10-ui/pages/task_invoice/invoice/js/views/addInvoiceView.js",
"/Sync10-ui/pages/task_invoice/invoice/js/models/invoiceModel.js",
"/Sync10-ui/pages/task_invoice/task/js/views/downloadSOPView.js",
"Paging",
"../../../globalAdminModel",
"../../../artDialog.js",
" /Sync10-ui/pages/task_invoice/key/LRTypeKey.js",
    // "../../../key/TaskStatusKey.js",
    "../../../jquery.dotdotdot.min",
    "showMessage"
    ],function($, Backbone, Handlebars, Handlebars_ext, config, templates, Models, AddTaskView, SortSubTaskView,
//		AddSubTaskView,
AddTaskLogView, LogDetailView, SubTaskDetailView, AddInvoiceView, InvoiceModel, DownloadSOPView, Paging, GlobalAdminModel,
Dialog, LRTypeKey){

       var taskCollection = new Models.TaskCollection();
//	var addSubTaskView = new AddSubTaskView({'el': 'div'});

	return Backbone.View.extend({
	    template: templates.taskList,
	    collection: taskCollection,
	    initialize: function(option){
	        this.setElement(option.el);
	        this.loadingModal = option.loadingModal;
	        this.admin = option.admin;
	        this.users = option.users;
	    },
	    render:function(){
	        this.loadingModal.show();
	        this.searchForList();
	    },
	    searchForList: function(){
	
	            //查询参数，目前就这一种，高级查询之后  TODO......
	            var text = $.trim($('.J-search-common').val());
	            var data = {'SCHEDULE_ID' : text};
	//            var url = config.taskCollectionUrl;
	            this.buildList(data);
	        },
	    buildList: function(data,url){
            var _self = this;
//            console.log(url);
            _self.collection.url = config.taskCollectionUrl;
            _self.collection.reset();
            if(url){
            	_self.collection.url=url;
            	console.log(data);
            }
            _self.collection.fetch({

                data: $.extend({}, _self.collection.pageCtrl, data),
                success: function(collection){
                    var list = collection.toJSON();
                    
                    
                    _self.$el.html(_self.template({
                        'list':list
                        ,'LRTypeKey':LRTypeKey
//                      ,'taskStatusKey':TaskStatusKey
                    	})).find('p').each(function(i, elem){
                        var $self = $(elem);
                        $self.dotdotdot({
                            callback : function( isTruncated, orgContent ) {
                                if(isTruncated){
                                    $self.parent().addClass("ellipsis-p");
                                }
                            }
                        });
                    });

                    //总页数，当前页号，容器id，回调
                    Paging.update_page(collection.pageCtrl.pageCount, collection.pageCtrl.pageNo, "pagebox_task", function(pageNo) {
                        collection.pageCtrl.pageNo = pageNo;
                        _self.loadingModal.show();
                        $('body').animate( {
                            scrollTop : 0
                        }, 400);
                        _self.render(); 
                    });

                    _self.loadingModal.hide();
                }
            });
},
events: {
   "click button[name='editTask']" : "editTask",
   "click button[name='delTask']" : "delTask",
   "click button[name='closeTask']" : "closeTask",
   "click button[name='downloadSOP']" : "downloadSOP",
//   "click button[name='addLog']" : "addLog",
//        	"click i[name='addSubTask']" : "addSubTask",
//        	"click i[name='editSubTask']" : "editSubTask",
//        	"click i[name='delSubTask']" : "delSubTask",
//        	"click i[name='closeSubTask']" : "closeSubTask",
"click i[name='addInvoice']" : "addInvoice",
"click i[name='delInvoice']" : "delInvoice",
"click .J-sub-sort" : "sortSubTask",
"click .J-goto-invoice" : "gotoInvoice",
"click .log-detail" : "gotoLogDetail",
"click i[name='addSubTask']" : "subTaskDetail",
"click .J-sub-detail" : "subTaskDetail",
"click .J-pdf": "invoiceBrowse"
},
subTaskDetail:function(e){
   var _self = this;
   var btn = $(e.currentTarget);
   var sub_id = btn.data('sub') * 1;
   var schedule_id = btn.data("task") * 1;
   var collection = _self.collection;
   var model = collection.findWhere({
       SCHEDULE_ID: schedule_id
   });

   var subTaskDetailView = new SubTaskDetailView({users:_self.users,admin:_self.admin, 'curOpened': sub_id});
   subTaskDetailView.parentView = _self;
   subTaskDetailView.listView = _self;
   subTaskDetailView.render(model, true);

},
gotoLogDetail:function(e){
   var _self = this;
   var btn = $(e.currentTarget);
   var schedule_id = btn.data("task") * 1;
//   var logId =  btn.data("logid");
//   console.log(logId);
   var collection = _self.collection;
   var model = collection.findWhere({
       SCHEDULE_ID: schedule_id
   });
   var logs = model.get("SUBLOG");
   var logCollection = new Models.TaskLogCollection(logs);

   new LogDetailView({'saveCallback': function(model, response, dialog){
       if(response.success){
         showMessage("Success","succeed");
         setTimeout(function(){
            _self.render();
            dialog.close();
        },1500);

     }else{
         showMessage("Error","error");
     }

 }}).render(logCollection);
},
addLog:function(e){
  var _self = this;
  var btn = $(e.currentTarget);
  var schedule_id = btn.data("task") * 1;
  var collection = _self.collection;
  var model = collection.findWhere({
     SCHEDULE_ID: schedule_id
 });
  var admin = _self.admin;
  var addTaskLogView = new AddTaskLogView({'admin':admin, 'saveCallback': function(model, response, dialog){
     if(response.success){
         showMessage("Success","succeed");
         _self.render();
         setTimeout(function(){
            dialog.close();
        },1500);
     }else{
         showMessage("Error","error");
     }

 }}).render(model,"Task 【"+schedule_id+"】 » Add Log");


},
downloadSOP:function(e){
   var _self = this;
   var btn = $(e.currentTarget);
   var schedule_id = btn.data("task") * 1;
   var collection = this.collection;
   var model = collection.findWhere({
       SCHEDULE_ID: schedule_id
   });
   var files = model.get("SUBFILE");
   var fileCollection = new Models.TaskFileCollection(files);

   new DownloadSOPView({'saveCallback': function(model, response, dialog){
       if(response.success){
         showMessage("Success","succeed");
         _self.render();
         setTimeout(function(){
            dialog.close();
        },1500);

     }else{
         showMessage("Error","error");
     }

 }}).render(fileCollection);

},
delInvoice:function(e){
   var btn = $(e.currentTarget);
   var schedule_id = btn.data("task") * 1;
   var bill_id = btn.data("invoice") * 1;
   var _self = this;
//            if(schedule_id||bill_id)
            var invoiceModel = new InvoiceModel();//删除task和invoice关系，调用invoice删除
            invoiceModel.set("SCHEDULE_ID",schedule_id);
            invoiceModel.set("BILL_ID",bill_id);
            invoiceModel.url = invoiceModel.url + "?SCHEDULE_ID=" + schedule_id+"&BILL_ID="+bill_id;
            
            var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            +'Delete &nbsp;【'+bill_id+'】?</p>';
            var dialog = new Dialog({
               content: html, 
               lock:true,
               width: 400,
               height: 100,
               title:'Del Invoice » '+bill_id,
               okValue:'OK',
               ok: function (event) {

                   invoiceModel.destroy({
                       success: function(data, response) {
                          if(response.success){
                             showMessage("Success","succeed");
                             setTimeout(function(){
                                _self.render();
                            },1500);

                         }else{
                             showMessage("Error","error");
                         }
                     },
                     error: function() {
                      showMessage("Error","error");
                  }
              });
               },
               cancelValue: 'Cancel',
               cancel: function(){
                   dialog.close();
               }
           }).showModal();



},
addInvoice:function(e){
   var btn = $(e.currentTarget);
   var schedule_id = btn.data("task") * 1;
   var _self = this;
   new AddInvoiceView({'admin': _self.admin, 'saveCallback': function(model, response, dialog){
       if(response.success){
         showMessage("Success","succeed");
         _self.render();
         setTimeout(function(){
            dialog.close();
        },1500);

     }else{
         showMessage("Error","error");
     }

            }}).render("",schedule_id);//添加相关联的任务id

},
       /* addSubTask:function(e){
        	var btn = $(e.currentTarget);
            var schedule_id = btn.data("task") * 1;
            var parent_subject = btn.data("subject");
            if (!schedule_id) return;
           
            var collection = this.collection;
            var model = collection.findWhere({
           	 SCHEDULE_ID: schedule_id
            });
            //设置新增的子任务的排在所有任务最后
            var subTask = model.get("SUBSCHEDULE");
            var task_number = 1;
            if(subTask.length==1){
            	task_number = subTask[0].TASK_NUMBER+1;
            }else if(subTask.length>1){
            	var subTaskFirstNumber = subTask[0].TASK_NUMBER;
            	var subTaskLastNumber = subTask[subTask.length-1].TASK_NUMBER;
            	if(subTaskFirstNumber>subTaskLastNumber){
            		task_number = subTaskFirstNumber+1;
            	}else{
            		task_number = subTaskLastNumber+1;
            	}
            }
//            console.log("task_number="+task_number);
            var taskModel = new Models.TaskModel();
            taskModel.set("PARENT_SCHEDULE_ID",schedule_id);
            taskModel.set("PARENT_SUBJECT",parent_subject);
            taskModel.set("ASSIGN_USER_ID",this.admin.toJSON().adid);
            taskModel.set("ASSEMPLOYNAME", this.admin.toJSON().employe_name);
            taskModel.set("IS_ADDITIONAL_TASK", 2);//默认不是additional task
            taskModel.set("TASK_NUMBER", task_number);//新增subtask时，默认为递增排序
            
            addSubTaskView.taskListView = this;
            addSubTaskView.admin = this.admin;
        	addSubTaskView.render(taskModel,"Add Sub Task");
        },
        editSubTask:function(e){
        	var btn = $(e.currentTarget);
            var schedule_id = btn.data("task") * 1;
            var sub_schedule_id = btn.data("subtask") * 1;
            
            var taskModel = this.collection.findWhere({
           	 SCHEDULE_ID: schedule_id
            });
            var subTaskCollection = new Models.SubTaskCollection(taskModel.get("SUBSCHEDULE"));
            var subTaskModel = subTaskCollection.findWhere({
            	SCHEDULE_ID: sub_schedule_id
            });
            subTaskModel.set("PARENT_SUBJECT",taskModel.get("SCHEDULE_OVERVIEW"));
            addSubTaskView.taskListView=this;
            addSubTaskView.admin = this.admin;
        	addSubTaskView.render(subTaskModel,"Edit SUB Task » "+sub_schedule_id);
        },
        delSubTask:function(e){
        	var btn = $(e.currentTarget);
        	var schedule_id = btn.data("task") * 1;
            var sub_schedule_id = btn.data("subtask") * 1;
            var _self = this;
            var taskModel = _self.collection.findWhere({SCHEDULE_ID: schedule_id});
            var subTaskCollection = new Models.SubTaskCollection(taskModel.get("SUBSCHEDULE"));
            var detSubTaskModel = subTaskCollection.findWhere({SCHEDULE_ID: sub_schedule_id});
            
            var subject = detSubTaskModel.get("SCHEDULE_OVERVIEW");//任务的subject
            
            
            var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            	+'Delete Sub Task &nbsp;【'+sub_schedule_id+'】?</p>';
            
	        var dialog = new Dialog({
	            content: html, 
	            lock:true,
	            width: 400,
	            height: 100,
	            title:'Del Sub Task » '+schedule_id,
	            okValue:'OK',
	            ok: function (event) {
	            	subTaskCollection.remove(detSubTaskModel);
	                taskModel.set("SUBSCHEDULE",subTaskCollection.toJSON());
	                detSubTaskModel.url = detSubTaskModel.url + "?SCHEDULE_ID=" + sub_schedule_id;
	                
	                detSubTaskModel.destroy({
	                	success: function(data, response) {
	                		if(response.success){
	                			showMessage("Success","succeed");
	                			setTimeout(function(){
	                				_self.render();
	                			},1500);
	                		
	                		}else{
	                			showMessage("Error","error");
	                		}
	                	},
	                	error: function() {
	                		showMessage("Error","error");
	                	}
	                });
	            },
	            cancelValue: 'Cancel',
	            cancel: function(){
	                dialog.close();
	            }
	        }).showModal();
        },
        closeSubTask:function(e){
        	var btn = $(e.currentTarget);
            var sub_schedule_id = btn.data("subtask") * 1;
            if (!sub_schedule_id) return;
            var _self = this;
            var task_status = 3;//关闭close，task状态改变为
            var subTaskModel = new Models.SubTaskModel();
            subTaskModel.url = config.closeTaskUrl;
            var schedule_id = btn.data("task") * 1;
//            var subject = btn.data("subject");

            var dialog = new Dialog({
                content: '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">Close Sub Task &nbsp;【'+sub_schedule_id+'】?</p>', 
                lock:true,
                width: 400,
                height: 100,
                title:'Close Sub Task » '+sub_schedule_id,
                okValue:'OK',
                ok: function (event) {
                    subTaskModel.save({
                        SCHEDULE_ID: sub_schedule_id,
                        TASK_STATUS: task_status,
                        IS_MAIN_SCHEDULE: 2//子任务
                      },{
                        success:function(model, response, options){
                            if(response.success){
                                
                                _self.render();
                                setTimeout(function(){
                                	showMessage("Success","succeed");
                                },1500);
                            }else{
                                showMessage("Error","error");
                            }
                            dialog.close();
                        },
                        error:function(model, response){
                            showMessage("Error","error");
                            dialog.close();
                        }
                    }); 
                },
                cancelValue: 'Cancel',
                cancel: function(){
                    dialog.close();
                }
            }).showModal();
            
            
},*/
editTask:function(e){
  var btn = $(e.currentTarget);
  var schedule_id = btn.data("task") * 1;
  if (!schedule_id) return;

  var _self = this;
  var collection = _self.collection;
  var model = collection.findWhere({
      SCHEDULE_ID: schedule_id
  });

  var addTaskView = new AddTaskView({'admin': _self.admin,'loadingModal':_self.loadingModal, 'saveCallback': function(model, response, dialog){
      if(response.success){
        showMessage("Success","succeed");
        _self.render();
        setTimeout(function(){
           dialog.close();
       },1500);

    }else{
        showMessage("Error","error");
    }

}});
  addTaskView.parentView = _self;
  addTaskView.users = _self.users;
  addTaskView.render(model,"Edit Project");
},
delTask:function(e){
   var btn = $(e.currentTarget);
   var schedule_id = btn.data("task") * 1;
   if (!schedule_id) return;

   var _self = this;
            //colletion获取要删除的model
            var model = _self.collection.findWhere({SCHEDULE_ID: schedule_id});
            var subject = model.get("SCHEDULE_OVERVIEW");//任务的subject
            var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            +'Delete &nbsp;【'+subject+'】?</p>';
            
            var dialog = new Dialog({
               content: html, 
               lock:true,
               width: 400,
               height: 100,
               title:'Del Project » '+schedule_id,
               okValue:'OK',
               ok: function (event) {
                  _self.collection.remove(model);
                  model.url = model.url + "?SCHEDULE_ID=" + schedule_id;

                  model.destroy({
                   success: function(data, response) {
                      if(response.success){
                         showMessage("Success","succeed");
                         setTimeout(function(){
                            _self.render();
                        },1500);

                     }else{
                         showMessage("Error","error");
                     }
                 },
                 error: function() {
                  showMessage("Error","error");
              }
          });
              },
              cancelValue: 'Cancel',
              cancel: function(){
               dialog.close();
           }
       }).showModal();

},
closeTask:function(e){
    var btn = $(e.currentTarget);
    var schedule_id = btn.data("task") * 1;
    if (!schedule_id) return;

    var _self = this;
           //判断子任务是否都关闭
           var flag = true;
           var openSubTask = "";
           var openSubTaskDetail = "";
           var taskModel = this.collection.findWhere({
            SCHEDULE_ID: schedule_id
        });
           var subTasks = taskModel.get("SUBSCHEDULE");
           for(var i=0;i<subTasks.length;i++){
             if(subTasks[i].TASK_STATUS!=3){
                openSubTask = subTasks[i].SCHEDULE_ID;
                openSubTaskDetail = subTasks[i].SCHEDULE_DETAIL;
                flag = false;
                break;
            }
        }

           //子任务都关闭，关闭主任务
           if(flag){
        	   var task_status = 3;//关闭close，task状态改变为
        	   var invoices = taskModel.get("SUBINVOICE");
        	   var invoice_ids = [];
        	   
        	   for(var i=0;i<invoices.length;i++){
               var invoice = invoices[i];
               invoice_ids.push(invoice.BILL_ID);
           }
//        	   console.log(invoice_ids);
var taskModel = new Models.TaskModel();
taskModel.url = config.closeTaskUrl;
taskModel.save({
    SCHEDULE_ID:schedule_id,
//            	   INVOICE_IDS:invoice_ids,
TASK_STATUS:task_status
},
{
   success:function(model, response, options){
    if(response.success){

      _self.render();
      setTimeout(function(){
         showMessage("Success","succeed");
     },1500);
  }else{
      showMessage("Error","error");
  }
},
error:function(model, response){
  showMessage("Error","error");
}
});
}else{
    showMessage("First close the task [ "+openSubTaskDetail+" ]","alert");
}

},
        //add by huhy
        sortSubTask: function(e){

            var btn = $(e.currentTarget);
            var schedule_id = btn.data("task") * 1;
            var _self = this;

            var taskModel = this.collection.findWhere({
                SCHEDULE_ID: schedule_id
            });

            var subTasks = taskModel.get("SUBSCHEDULE");
            if(subTasks.length>0){
            	var sortSubTaskView = new SortSubTaskView();
                sortSubTaskView.render(subTasks);

                var dialog = new Dialog({
                    content: sortSubTaskView.$el,
                    lock:true,
                    width: 500,
                    height: 380,
                    title:'Sort Subtasks',
                    okValue:'Ok',
                    ok: function (event) {
                        _self.loadingModal.show();
                        var data = sortSubTaskView.getResult();
                        var subTasks = sortSubTaskView.getSortedSubTasks();

                        $.ajax({
                            url: config.selectUrl + '?key=sortSubScheduleNumber',
                            type:'post',
                            data: {'sortSubTask': JSON.stringify(data)},
                            dataType: 'json',
                            success: function (res) {
                                //排序之后的界面处理
                                btn.parent().next().html(templates.sortedSubTasks({'SUBSCHEDULE': subTasks, 'SCHEDULE_ID' : schedule_id}));

                                dialog.close();
                                sortSubTaskView.remove();
                                _self.loadingModal.hide();
                            },
                            error:function(e){
                                showMessage("Error","error");
                                _self.loadingModal.hide();
                            }
                        });
                        
                    },
                    cancelValue: 'Cancel',
                    cancel: function(){
                        dialog.close();
                        sortSubTaskView.remove();
                    }
                }).showModal();
}else{
   showMessage("NO SubTask","alert");
}


},
        //切换页面
        gotoInvoice: function(e){
            var billId = $(e.currentTarget).data('invoice');

            if(window.top === window){
                //单独打开的页面

                // Save data to sessionStorage
                window.sessionStorage.setItem('BILL_ID', billId);
                window.location='/Sync10-ui/pages/task_invoice/invoice/index.html';

            }else{
                //在框架里嵌入的页面

                var topIframe = window.top.window.frames["iframe"];

                topIframe.task_invoice ? topIframe.task_invoice['BILL_ID'] = billId : topIframe.task_invoice = {'BILL_ID': billId};

                
                var alink = topIframe.frames['leftFrame'].$('a[href$="task_invoice/invoice/index.html"]');

                alink.parent().trigger('click');

                topIframe.frames['main'].location='/Sync10-ui/pages/task_invoice/invoice/index.html';
                
            }
        },
        invoiceBrowse: function(e){
            var $target = $(e.currentTarget);
            var billId = $target.data('invoice') * 1;
            var schedule_id = $target.data("task") * 1;
            var _self = this;

            var taskModel = this.collection.findWhere({
                SCHEDULE_ID: schedule_id
            });

            var invoices = taskModel.get("SUBINVOICE");
            
            for(var i = 0, len = invoices.length; i < len; i++){
                var invoice = invoices[i];

                if(invoice['BILL_ID'] == billId){
                    break;
                }
            }

            //没有数据 ---- 查找数据出错了
            if(i === len){
                return false;
            }

            invoice.noBtn = true;

            var lrType = invoice.LR_TYPE;
            var lrNo = invoice.LR_NO;
            var reference_no = "";
            if(lrType){
                for(var i=0;i<LRTypeKey.length;i++){
                    if(lrType==LRTypeKey[i].key){
                        reference_no=LRTypeKey[i].value+" ";
                    }
                }
            }
            reference_no += lrNo;
           invoice.REFERENCE_NO = reference_no;

            var dialog = new Dialog({
                content: $('<div id="browse" class="task-invoice" style="padding:5px 10px;height:100%;overflow-y:auto;"></div>').append(templates.invoiceBrowse(invoice)),
                lock:true,
                width: 804,
                height: 580,
                title:'Invoice Preview',
                cancel: function(){
                    dialog.close();
                },
                cancelDisplay: false
            }).showModal();


            var $container = $('#browseTable_container');
            var theadH = $container.find('thead').outerHeight();
            var tfootH = $container.find('tfoot').outerHeight();
            var tbodyH = $container.find('tbody').outerHeight();
            var unitH = 982 * 98 / 100 - 30 - theadH - tfootH;

            var mulriple = Math.ceil(tbodyH / unitH);

            var difference = unitH * mulriple - tbodyH;

            if(difference > 30){
                $container.find('tbody').append('<tr >'+
                  '<td style="border-right-width:0; border-left: 2px solid #333;"></td>'+
                  '<td style="border-left-width: 0; "></td>'+
                  '<td><div style=" max-width: 450px; word-break: break-all; text-align: left;"></div></td>'+
                  '<td><div style="height:'+ difference+'px; margin:0; padding:0;"></div></td><td></td></tr>');
            }
            $('#browse .J-print').off().on('click', function(){
                visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
                visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");
                visionariPrinter.ADD_PRINT_TABLE(10, 10, "100%", "100%", $('#browseTable_container').html());
                visionariPrinter.SET_PRINT_COPIES(1);
                // visionariPrinter.PREVIEW();

                visionariPrinter.SET_SAVE_MODE("SAVEAS_IMGFILE_EXENAME",".jpg");
                visionariPrinter.SAVE_TO_FILE("vvme.jpg"); 
            });
        }
    }); 

});