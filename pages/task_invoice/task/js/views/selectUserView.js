/**
 * Created by gql on 2015-6-9 16:18:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "handlebars_ext",
    "../../../config.js",
    "../../../templates/templates.amd.js",
//    "./../models/userModels.js",
    "../../../artDialog.js",
    "../../../initUserInfo.js",
    " /Sync10-ui/pages/task_invoice/key/psJson.js",
    " /Sync10-ui/pages/task_invoice/key/groupJson.js",
    " /Sync10-ui/pages/task_invoice/key/userJson.js",
],function($, Backbone, Handlebars, Handlebars_ext, config, templates,
//		UserCollection,
		Dialog, initUserInfo, productCatelog, groupJson, userJson){
	
	var UserCollection = Backbone.Collection.extend({
		url: config.selectUserUrl,
        parse: function(response) {
        	return response;
        }
    });
	
    return Backbone.View.extend({
        template: templates.selectUser,
        collection:new UserCollection(),
        initialize: function(options){
        	var _self = this;
        	this.admin = options.admin;
//        	this.loadingModal = options.loadingModal;
        	this.defValue = options.value;
        	this.userName = options.userName;
        	this.userId = options.userId;
        	this.newAdmin = options.newAdmin;
        },
        render:function(){
            var _self = this;
//            _self.loadingModal.show();
            var newAdmin = _self.newAdmin;
           /* newAdmin.ps_id =  _self.admin.toJSON().ps_id; 
        	newAdmin.dept_id = _self.admin.toJSON().department[0].DEPTID;
    		newAdmin.post_id = _self.admin.toJSON().department[0].POSTID;
			newAdmin.adgid = _self.admin.toJSON().adid; */
//			_self.admin = newAdmin;
            
//            _self.collection = 
            	
            _self.collection.reset();
            _self.collection.fetch({
            	data: {'warehouse_id': newAdmin.ps_id,'group_id':newAdmin.dept_id,'user_id':newAdmin.post_id},
                success: function(collection){
                    var list = collection.toJSON();
                    var userInfo = initUserInfo(list, _self.defValue, newAdmin.dept_id);
                    
                    _self.$el.html(_self.template({'productCatelog':productCatelog,'groupJson':groupJson,'userJson':userJson,'admin':newAdmin}));
                    _self.$el.find(".user-info").html(templates.initDeptInfo({'list':userInfo,'admin_id':_self.admin.toJSON().adid}));
//                    _self.loadingModal.hide();
                }
            });
            
            _self.dialog = new Dialog({
                content: _self.$el,
                lock:true,
                width: 750,
                height: 480,
                title:'Select User',
                okValue:'Ok',
                ok: function (event) {
                    _self.getResult();
                    _self.remove();
                    _self.dialog.close();
                },
                cancelValue: 'Cancel',
                cancel: function(){
                	 _self.dialog.close();
                	 _self.remove();
                }
            }).showModal();
        },
        events:{
        	"click .J-user-search":"getUserInfo",
        	"click .J-adgid_check_checked":"selectAll",
        	"click .J-user_check_checked": "selectUsers",
//        	"change #ps_id": "getUserInfo",
//        	"change #group": "getUserInfo",
        	"change #proJsId": "getUserInfo"
        },
        getResult:function(){
        	var _self = this;
        	var checked= $("input:checkbox[class='J-user_check_checked']");
        	var ids = "" ;
			var names = "";
			checked.each(function(){
				var _this = $(this);
				var liParent = _this.parent().parent();
				if(_this.is(':checked')){
					var tempIds = _this.attr("id").replace ("user_","");
					if(ids.indexOf(tempIds) < 0){
						ids += ","+tempIds;
						names += ","+ _this.attr("name").replace ("user_","");;
					}
				}
			});
			ids =  ids.length > 1 ? ids.substr(1):"";
			names = names.length > 1 ? names.substr(1):"";
			
			_self.userName.val(names);
			_self.userId.val(ids);
        },
        getUserInfo:function(e){
        	var _self = this;
//        	_self.loadingModal.show();
        	var ps_id = _self.$el.find("#ps_id").val();
        	var group = _self.$el.find("#group").val();
        	var proJsId =  _self.$el.find("#proJsId").val();
        	console.log("proJsId="+proJsId+"   group="+group);
        	$.ajax({
                url: config.selectUserUrl+"&warehouse_id="+ps_id+"&group_id="+group+"&user_id="+proJsId,
                type:'post',
                data: {'warehouse_id': ps_id,'group_id':group,'user_id':proJsId},
                dataType: 'json',
                success: function (res) {
                	 var userInfo = initUserInfo(res, _self.defValue, group);
                	 _self.$el.find(".user-info").html(templates.initDeptInfo({'list':userInfo,'admin_id':_self.admin.toJSON().adid}));
//                    _self.loadingModal.hide();
                },
                error:function(e){
                    showMessage("Error","error");
//                    _self.loadingModal.hide();
                }
            });
        },
        selectAll:function (e) {
        	var $obj = $(e.currentTarget);//仓库的checkbox
        	var $userChecked = $obj.parent().parent("td").next("td").find("input[class='J-user_check_checked']");//该仓库的用户checkbox
        	if($obj.is(':checked')){
        		$userChecked.prop("checked",true);
        	}else{
        		$userChecked.prop("checked",false);
        	}
        },
        selectUsers:function(e){
        	var flag = true;
        	var $obj = $(e.currentTarget);//用户的checkbox
        	var $psChecked = $obj.parent().parent().parent().parent("td").prev("td").find("input[class='J-adgid_check_checked']");//该仓库的checkbox
			var checks = $obj.parent().parent().parent().parent("td").find("input[class='J-user_check_checked']");//该仓库的用户checkbox
			//联动选择或取消选择仓库的checkbox
			checks.each(function(){
				var _this = $(this);
				if( !_this.is(':checked')){
					flag = false;
					$psChecked.prop("checked",false);
					return false;
				}
			});
			if(flag){
				$psChecked.prop("checked",true);
			}
			
		}
        
    });
});