/**
 * Created by gql on 2015-3-31 15:27:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "../../js/models/models.js",
    "../../../config.js",
    "../../../initDept.js",
    "oso.lib/validate/js/validate",
//    "../../js/models/userModels.js",
    "/Sync10-ui/pages/task_invoice/invoice/js/models/invoiceCollection.js",
    "/Sync10-ui/pages/task_invoice/invoice/js/views/addInvoiceView.js",
    "/Sync10-ui/pages/task_invoice/invoice/js/models/invoiceModel.js",
    "/Sync10-ui/pages/task_invoice/task/js/views/uploadSOPView.js",
    "/Sync10-ui/pages/task_invoice/task/js/views/selectUserView.js",
    "/Sync10-ui/pages/task_invoice/task/js/views/subTaskDetailView.js",
    "../../../artDialog.js",
    "../../../date.js",
//    " /Sync10-ui/pages/task_invoice/key/LRTypeKey.js",
    "require_css!bootstrap-css/bootstrap.min",
    "bootstrap",
    "bootstrap.datetimepicker",
    "require_css!bootstrap.datetimepicker-css",
//    "require_css!/Sync10-ui/bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css",
//    "multiselect",
    "showMessage",
    "select2",
    "domready!"
],function($, Backbone, Handlebars, templates, Models, config, initDept, validate,
//		userModels,
		InvoiceCollection, AddInvoiceView, InvoiceModel,
		UploadSOPView, SelectUserView, SubTaskDetailView, Dialog, Date){
//	var users = new userModels();//人员信息
	
    return Backbone.View.extend({
        template: templates.addTask,
        admin:null,
        delSubInvoice:[],
        delSubFile:[],
        initialize: function(options){
        	this.admin = options.admin;  //登录人的信息
            this.saveCallback = options.saveCallback;//保存成功的回调函数
            this.loadingModal = options.loadingModal;
//            this.falg = true;
        },
        taskListView:null,
        render:function(model, title, bill_id){
            var _self = this;
            
            //修改是传入
            if(model){
            	_self.model = model;
              }else{
            	  _self.model = new Models.TaskModel();
            	//默认任务安排人为系统登陆人
            	  _self.model.set("ASSIGN_USER_ID",_self.admin.toJSON().adid);
                  _self.model.set("ASSEMPLOYNAME",_self.admin.toJSON().employe_name);
              }
            _self.delSubInvoice = _self.model.get("SUBINVOICE");
            _self.delSubFile = _self.model.get("SUBFILE");
            _self.$el.html(_self.template({'model':_self.model.toJSON()}));
            
            _self.setDatetimepicker(_self.$el.find('#start_time,#end_time'));//添加时间控件
            
            _self.dialog = new Dialog({
                content:  _self.$el,
                lock:true,
                width: 650,
                height: 500,
                title:title,
                okValue:'Save',
                ok: function (event) {
                	_self.addTask(bill_id);//调用保存的方法
                	return false;
                },
                cancelValue: 'Cancel',
                cancel: function(){
                	_self.cancelAddTask();
                	return false;
                }
            }).showModal();
            
//            console.log(_self.model.toJSON());
        },
        events: {
        	"click i[name='addLR']" : "addLRNo",
        	"click i[name='delLR']" : "delLRNo",
        	/*"click i[name='addInvoice']" : "addInvoice",
        	"click i[name='delInvoice']" : "delInvoice",*/
        	"click i[name='uploadSOP']" : "uploadSOP",
        	"click i[name='delFile']" : "delFile",
        	"click #exe_employ_name" : "selectUser",
//        	"focus #exe_employ_name" : "selectUser",
        	"click #join_employ_name" : "selectUser",
        },
        selectUser:function(e){
        	var _self = this;
        	var $name = $(e.currentTarget);
        	var $id = $(e.currentTarget).next("input");
        	var value = $id.val();
        	
        	//设置人员部门等信息的默认值
        	var flag = true;//是否是to
        	var id = $id.attr("id");
        	if("schedule_join_execute_id"==id){
        		flag = false;
        	}
        	 var newAdmin = {};
        	 newAdmin.adgid = _self.admin.toJSON().adid; 
             if(flag){//to时
             	newAdmin.ps_id =  "1000005"; //Valley
              	newAdmin.dept_id = "1000002";//Warehouse
          		newAdmin.post_id = "15";//Lead+SuperVisor
             }else{//cc时
             	newAdmin.ps_id =  "1000005";//Valley
              	newAdmin.dept_id = "10000";//Manager
          		newAdmin.post_id = "-1";//All
             }
        	var selectUserView = new SelectUserView({'newAdmin':newAdmin,'loadingModal':_self.loadingModal,'admin':_self.admin,'value':value,'userName':$name,'userId':$id});
        	selectUserView.render();
        },
        delFile:function(e){
        	var _self = this;
        	var btn = $(e.target);
        	var file_id = btn.data("fileid") * 1;
        	var file_name = btn.data("filename");
        	if (!file_id) return;
        	var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            	+'Delete file&nbsp;【'+file_name+'】?</p>';
	        var dialog = new Dialog({
	            content: html, 
	            lock:true,
	            width: 400,
	            height: 100,
	            title:'Add Task » Del File',
	            okValue:'OK',
	            ok: function (event) {
	            	//删除服务器上的文件
	            	$.ajax("/Sync10/_fileserv/file/"+ file_id,  {
	                    type:'DELETE',
	                    dataType:'json',
	                    success: function(data){
	                   	 	var fileCollection = new Models.TaskFileCollection(_self.model.get("SUBFILE"));
	                        var fileModel = fileCollection.findWhere({
	                        	FILE_ID: file_id
	                        });
	                        fileCollection.remove(fileModel);
	                        _self.model.set("SUBFILE",fileCollection.toJSON());
//	                        console.log(_self.model.get("SUBFILE"));
	                        btn.parent("div").parent("dd").remove();
	                    },
	                    error:function(){
	                   	 showMessage("Error","error");
	                    }
	                });
	            	
	            },
	            cancelValue: 'Cancel',
	            cancel: function(){
	                dialog.close();
	            }
	        }).showModal();
        	
        },
        uploadSOP:function(e){
        	var _self = this;
        	var uploadSOPView = new UploadSOPView({'saveCallback': function(collection, dialog){
        		if(collection.length>0){
        			var data = collection.toJSON();
        			var subFile = _self.model.toJSON().SUBFILE;
//        			console.log(subFile);
        			var editSubFile = data.concat(subFile);
//        			var editSubFile = new Models.TaskFileCollection(data.concat(subFile));
//        			console.log(editSubFile);
        			var html = "";
        			_self.model.set("SUBFILE",editSubFile);//将主task相关的文件添加到task的model中，保存
//        			console.log(_self.model.get("SUBFILE"));
        			for(var i=0;i<data.length;i++){
        				var file_id = data[i].FILE_ID;
        				var file_name = data[i].ORIGINAL_FILE_NAME;
        				html+='	<dd>'
        					+'<span name="fileId">'
        					+'	<a href="/Sync10/_fileserv/file/'+file_id+'" title="'+file_name+'" download="'+file_name+'" data-gallery="">'+file_name+'</a></span> '
        					+'<div class="invoice_del"><i class="icon icon-remove" name="delFile" data-fileid="'+file_id+'"data-filename="'+file_name+'"></i></div>'
        					+'</dd>';
        			}
        			$(_self.el).find("#showFile").append(html);
        		}
        		
//        		console.log( _self.model.get("SUBFILE"));
        	}});
        	uploadSOPView.render();
        },
        /*addInvoice:function(e){
        	var _self = this;
        	var btn = $(e.target);
        	var bill_id = btn.data("invoice") * 1;
        	var invoiceCollection = new InvoiceCollection(_self.model.get("SUBINVOICE"));
//            console.log(invoiceCollection.toJSON());
        	var invoice_id=" ";
        	
        	var $showInvoice = _self.$el.find("#showInvoice");
            new AddInvoiceView({'admin': _self.admin, 'saveCallback': function(model, response, dialog){
	            	if(response.success){
	        			showMessage("Success","succeed");
	        			invoiceCollection.add(model);
	        			 _self.model.set("SUBINVOICE",invoiceCollection.toJSON());
	        			invoice_id = model.get("BILL_ID");
	        			setTimeout(function(){
	        				dialog.close();
	        			},1500);
	        			var html = "<dd>" +
			    					"<span name='billId'>"+invoice_id+"</span>" +
			    					"<div class='invoice_del'>" +
			    						"<i class='icon icon-remove' name='delInvoice' data-invoice='"+invoice_id+"'></i>" +
			    					"</div>" +
			    				"</dd>";
	        			$showInvoice.append(html);
	        			$.unblockUI();
	        		}else{
	        			showMessage("Error","error");
	        		}
                
            	}
            }).render();
        	
            
        },
        delInvoice:function(e){
        	var _self = this;
        	var btn = $(e.target);
            var bill_id = btn.data("invoice") * 1;
            if (!bill_id) return;
            var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            	+'Delete invoice&nbsp;【'+bill_id+'】?</p>';
	        var dialog = new Dialog({
	            content: html, 
	            lock:true,
	            width: 400,
	            height: 100,
	            title:'Del Invoice » '+bill_id,
	            okValue:'OK',
	            ok: function (event) {
	            	var invoiceCollection = new InvoiceCollection(_self.model.get("SUBINVOICE"));
	                var invoiceModel = invoiceCollection.findWhere({
	                	BILL_ID: bill_id
	                });
	                invoiceCollection.remove(invoiceModel);
	                _self.model.set("SUBINVOICE",invoiceCollection.toJSON());
	                
	                btn.parent("div").parent("dd").remove();
	                showMessage("Success","succeed");
	            },
	            cancelValue: 'Cancel',
	            cancel: function(){
	                dialog.close();
	            }
	        }).showModal();
            
        },*/
        addLRNo:function(e){
        	var $lr_td = $(e.target).parent("dt").parent("td");
        	 var $lr_dt = $(e.target).parent("dt").next("dt[name='lr_no']");
        	 $(e.target).hide();
        	 $lr_dt.show();
        	 this.$el.find('select[name="lr_type"]').select2({
                 placeholder: "Type...",
                 minimumResultsForSearch: -1,
             });
        },
        delLRNo:function(e){
        	var $lr_dt = $(e.target).parents("dt[name='lr_no']");
        	$(this.el).find('select[name="lr_type"]').select2('data', {id: null, text: null});
        	$(this.el).find("#lr_no").val("");
        	$lr_dt.hide();
        	$(this.el).find("i[name='addLR']").removeClass("hid_lr");
        	$(this.el).find("i[name='addLR']").show();
        },
        addTask:function(bill_id){
        	var _self = this;
        	if(_self.isValidated()){
//        		var subinvoice = new InvoiceCollection(_self.model.toJSON().SUBINVOICE);//编辑任务时，对账单添加修改操作完成后；model中存取的账单信息(即，保存任务时，该任务关联的账单信息)
//            	var delsubinvoice = _self.delSubInvoice;//编辑任务时，该任务未修改时，关联的账单信息
            	
            	var subfile =  new Models.TaskFileCollection(_self.model.toJSON().SUBFILE);//编辑任务时，对账单添加修改操作完成后；model中存取的文件信息(即，保存任务时，该任务关联的文件信息)
            	var delsubfile = _self.delSubFile;//编辑任务时，该任务未修改时，关联的文件信息
            	
            	/*invioce(账单)页面添加task时，该条invoice的id
                 * 将改账单信息，添加到改任务关联的账单信息中
                 */
            	if(bill_id&&bill_id!=""){
                	 var invoiceModel = new InvoiceModel();
                	 invoiceModel.set("BILL_ID",bill_id);
                	 subinvoice.add(invoiceModel);
                 }
                var taskModel = new Models.TaskModel();
                taskModel.set("DELSUBINVOICE", []);
                taskModel.set("SUBINVOICE", []); 
                taskModel.set("DELSUBFILE", delsubfile);
                taskModel.set("SUBFILE", subfile); 
            	
            	//页面上的信息
            	 var assign_user_id = _self.$el.find("#assign_user_id").val();
            	 var schedule_execute_id = _self.$el.find("#schedule_execute_id").val()==null?[]:_self.$el.find("#schedule_execute_id").val().split(",");
            	 var schedule_join_execute_id = _self.$el.find("#schedule_join_execute_id").val()==null?[]:_self.$el.find("#schedule_join_execute_id").val().split(",");
            	 var start_time = _self.$el.find("#start_time").val();
            	 var end_time = _self.$el.find("#end_time").val();
            	 var schedule_overview = _self.$el.find("#schedule_overview").val();
            	 var schedule_detail = _self.$el.find("#schedule_detail").val();
            	 var schedule_id = _self.$el.find("#schedule_id").val();
            	 var exe_employ_name = _self.$el.find("#exe_employ_name").val();
//            	 var lr_type =  _self.$el.find("#lr_type").val();
//                 var lr_no =  _self.$el.find("#lr_no").val();
            	 var isAdd = true;
            	 //判断是否是修改
            	 if(schedule_id&&schedule_id!=""){
            		 isAdd = false;
            		 taskModel.set("SCHEDULE_ID",schedule_id);//修改时，给idAttribute赋值，表示是修改
            		 var deleteusers = _self.$el.find("#deleteusers").val();
            		 var del_join_user_id = _self.$el.find("#del_join_user_id").val();
            		 taskModel.set("DELETEUSERS",deleteusers);//修改之前，执行人的id
            		 taskModel.set("DEL_JOIN_USER_ID",del_join_user_id);//修改之前，参与人的id
            	 }
            	 
            	 taskModel.save({
//            		LR_TYPE:lr_type,
//            		LR_NO:lr_no,
            		ASSIGN_USER_ID: assign_user_id,
            		SCHEDULE_EXECUTE_ID: schedule_execute_id,
            		SCHEDULE_JOIN_EXECUTE_ID: schedule_join_execute_id,
                    START_TIME: start_time,
                    END_TIME: end_time,
                    SCHEDULE_OVERVIEW: schedule_overview,
                    SCHEDULE_DETAIL:schedule_detail
                  }, 
                  {
                      success:function(model, response, options){
                    	  model.set("EXEEMPLOYNAME",exe_employ_name);
                    	  if(isAdd){
                    		  model.set("SCHEDULE_ID",response.schedule_id);
                    		  var subTaskDetailView = new SubTaskDetailView({users:_self.users,admin:_self.admin});
                    		  subTaskDetailView.listView = _self.parentView;
                    		  subTaskDetailView.render(model);
                    		  _self.dialog.close();
                    	  }else{
                    		  if(_self.saveCallback){
                    			  _self.saveCallback(model, response, _self.dialog);
                    		  }
                    	  }
                      },
                     error:function(model, response){
                    	 showMessage("Error","error");
                      }
                  }); 
        	}
        }
        ,cancelAddTask:function(){
        	var _self = this;
        	var subfile = _self.delSubFile;//编辑任务时，该任务未修改时，关联的文件信息
        	var subinvoice = _self.delSubInvoice;//编辑任务时，该任务未修改时，关联的账单信息
        	var html = '<p style="margin-top:40px;text-align:center;font-weight: bold;font-size:15px;">'
            	+'Your changes have not been saved, do you want to quit?</p>';
	        var dialog = new Dialog({
	            content: html, 
	            lock:true,
	            width: 450,
	            height: 100,
	            title:'Warn » Cancel',
	            okValue:'OK',
	            ok: function (event) {
	            	
	            	_self.model.set("SUBFILE", subfile); 
	            	_self.model.set("SUBINVOICE", subinvoice);
	            	dialog.close();
	            	_self.dialog.close();
	            },
	            cancelValue: 'Cancel',
	            cancel: function(){
	            }
	        }).showModal();
        	
        },
        setDatetimepicker:function($id){
        	var _self = this;
        	$id.datetimepicker({
                format: "mm/dd/yy",
                minView: 2,
                autoclose:1,
                forceParse: 0,
                bgiconurl:"",
        	}).on('changeDate', function(ev){
	            var $self = $(this);
	            var id = $self.attr('id');
	            if(id=="start_time"){
	            	_self.$el.find('#end_time').datetimepicker('setStartDate', ev.date);
	            }else if(id=="end_time"){
	            	_self.$el.find('#start_time').datetimepicker('setEndDate', ev.date);
	            }
	        });
        },
        isValidated:function(){
        	var _self = this;
        	var flag = true;
        	var forms = _self.$el.find("table.add-task").find("input[type='text']:not(#join_employ_name)");
        	for(var i=0;i<forms.length;i++){
        		var $elem = $(forms[i]);
        		var val = $elem.val();
        		if(val==""){
        			$elem.focus();
        			flag = false;
        			break;
        		}
        	}
        	
        	return flag;
        }
       
    });
});