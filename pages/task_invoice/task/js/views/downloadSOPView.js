/**
 * Created by gql on 2015-5-18 15:27:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "../../js/models/models.js",
    "../../../artDialog.js",
    "showMessage",
    "domready!"
],function($, Backbone, Handlebars, templates, Models, Dialog){
	
    return Backbone.View.extend({
        template: templates.downloadSOP,
        className: 'download-container',
        initialize: function(options){
        	this.saveCallback = options.saveCallback;//保存成功的回调函数
        },
        render:function(collection){
            var _self = this;
            _self.$el.html(_self.template(collection.toJSON()));
            
            _self.dialog = new Dialog({
                content:  _self.$el,
                lock:true,
                width: 550,
                height: 460,
                title:"Download SOP ",
//                okValue:'Save',
//                ok: function (event) {
//                	_self.saveFile();//调用保存的方法
//                },
                cancelValue: 'Cancel',
//                cancel: function(){
//                	_self.cancelUpload();
//                }
            }).showModal();
            
        },
        events: {
        	"click button[name='downloadSOP']" : "download"
        },
        download:function(){
        }
    });
});