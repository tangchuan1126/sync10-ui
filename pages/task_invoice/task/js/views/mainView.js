/**
 * Created by huhy on 2015-3-30
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
     "../../../config.js",
    "../../../templates/templates.amd.js",
    "./addTaskView",
    "./taskListView",
    "../../../globalAdminModel",
    "../../../commonSearch.js",
    "../../js/models/userModels",
    "bootstrap_tab",
    "require_css!bootstrap-css/bootstrap.min.css",
    "require_css!/Sync10-ui/pages/task_invoice/css/task_invoice.css",
    "require_css!/Sync10-ui/pages/task_invoice/css/add_task.css",
    //2015-04-29临时修改
    "require_css!bootstrap.datetimepicker-css",
    /*"require_css!bootstrap.datetimepicker/css/bootstrap-datetimepicker.min",*/
    "domready!"
],function($, Backbone, Handlebars, config, templates, AddTaskView, TaskListView, GlobalAdminModel, commonSearch, UserModels){

    return Backbone.View.extend({
        template: templates.mainTask,
        admin:new GlobalAdminModel(),
        users:new UserModels(),
        initialize: function(option){
            this.setElement(option.el);
            
        },
        taskListView:"",
        render:function(data){
            var _self = this;
            this.admin.fetch({success:function(admin, res){
            	_self.$el.html(_self.template({'query' : data}));
            	_self.setDatetimepicker(_self.$el.find('#j_start_time,#j_end_time'));//添加时间控件
            	
            	var loadingModal = _self.$el.find('.modal');
            	_self.taskListView = new TaskListView({el: "#task_table tbody", loadingModal: loadingModal, admin: admin, users:_self.users});
            	_self.taskListView.render({'SCHEDULE_ID' : data});

                //search ...
                commonSearch({
                    '$el' : _self.$el, 
                    'target' : _self.$el.find('.J-search-common'), 
                    'url' : config.selectUrl + '?key=getScheduleResultByIndex',
                    'idName' : 'SCHEDULE_ID',
                    'textName' : 'MERGE_FIELD',
                    'callback': function(text){
                        _self.taskListView.render({'SCHEDULE_ID' : text});
                    }
                });
            }});
        } ,
        events: {
            "click #add_task" : "addTask",
            "click .J-btn-search" : "searchForList",
            "click .J-advanced-search" : "advancedForList",
        },
        //add by gql for 'Advanced Search'
        advancedForList: function(e){
        	var _self = this;
        	var data = {};
        	var start_time = _self.$el.find("#j_start_time").val();
    		var end_time = _self.$el.find("#j_end_time").val();
			var task_status = _self.$el.find("#j_task_status").val();
			data.START_TIME = start_time;
        	data.END_TIME = end_time;
        	data.TASK_STATUS = task_status;
        	var url = config.getProjectByParamUrl; 
        	_self.taskListView.buildList(data,url);//刷新列表页面
        },
        searchForList: function(e){

            this.taskListView.render();
        },
        addTask: function(e){
        	
//        	$.blockUI({message:'<img src="../image/loadingAnimation6.gif" align="absmiddle"/>'});
        	var _self = this;
        	var addTaskView = new AddTaskView({'admin': _self.admin,'loadingModal':_self.loadingModal, 'saveCallback': function(model, response, dialog){
        		if(response.success){
         			showMessage("Success","succeed");
         			_self.taskListView.render();
         			setTimeout(function(){
         				dialog.close();
         			},1500);
         			
         		}else{
         			showMessage("Error","error");
         		}
                 
             }});
        	addTaskView.parentView = _self.taskListView;
        	addTaskView.users = _self.users;
        	addTaskView.render("","Add Project");
        	
        	
        	
        	
//            	$.unblockUI();
            	/*var dialog = new Dialog({
                    content: addTaskView.$el,
                    lock:true,
                    width: 600,
                    height: 450,
                    title:'Add Task',
                    okValue:'Save',
                    ok: function (event) {
                    	addTaskView.addTask("");
                    },
                    cancelValue: 'Cancel',
                    cancel: function(){
//                    	addTaskView.cancelAddTask();
                        dialog.close();
                    }
                }).showModal();*/
        	
        },
        setDatetimepicker:function($id){
        	var _self = this;
        	$id.datetimepicker({
                format: "mm/dd/yy",
                minView: 2,
                autoclose:1,
                forceParse: 0,
                bgiconurl:"",
        	}).on('changeDate', function(ev){
	            var $self = $(this);
	            var id = $self.attr('id');
	            if(id=="j_start_time"){
	            	_self.$el.find('#j_end_time').datetimepicker('setStartDate', ev.date);
	            }else if(id=="j_end_time"){
	            	_self.$el.find('#j_start_time').datetimepicker('setEndDate', ev.date);
	            }
	        });
        },
    });
});