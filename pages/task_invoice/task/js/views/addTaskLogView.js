/**
 * Created by gql on 2015-3-31 15:27:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "handlebars_ext",
    "../../../templates/templates.amd.js",
    "./../models/models.js",
    "../../../config.js",
    "../../../artDialog.js",
    "require_css!bootstrap-css/bootstrap.min",
    "bootstrap",
    "bootstrap.datetimepicker",
    "require_css!bootstrap.datetimepicker-css",
//    "require_css!/Sync10-ui/bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css",
//    "multiselect",
    "showMessage"
],function($, Backbone, Handlebars, Handlebars_ext, templates, Models, config, Dialog){
	
    return Backbone.View.extend({
        className:'add-replay',
        tagName: "fieldset",
        template: templates.addTaskLog,
        initialize: function(options){
//        	console.log("initialize addTaskLogView");
        	this.admin = options.admin;
        	this.saveCallback = options.saveCallback;//保存成功的回调函数
        },
        render:function(model,title){
            var _self = this;
            _self.model = model;
            model.set("ADID",_self.admin.toJSON().adid);
            model.set("EMPLOYE_NAME",_self.admin.toJSON().employe_name);
            _self.$el.html(_self.template(model.toJSON()));
            
            _self.dialog = new Dialog({
                content:_self.$el,
                lock:true,
                width: 600,
                height: 450,
                title:title,
                okValue:'Save',
                ok: function (event) {
                	_self.addTaskLog();//调用保存的方法
                },
                cancelValue: 'Cancel',
                cancel: function(){
//                	_self.cancelAddTask();
                	_self.dialog.close();
                }
            }).showModal();
        },
        events: {
//        	"click i[name='addInvoice_sub']" : "addInvoice",
//        	"click i[name='delInvoice_sub']" : "delInvoice",
        },
        addTaskLog:function(){
        	 var _self = this;
        	 var model = new Models.TaskLogModel();
        	 var sch_replay_id = _self.$el.find("#sch_replay_id").val();
        	 if(sch_replay_id){
        		 model.set("SCH_REPLAY_ID",sch_replay_id);
        	 }
        	
        	 var schedule_id = _self.$el.find("#schedule_id").val();
        	 var adid = _self.$el.find("#adid").val();
        	 var employe_name = _self.$el.find("#employe_name").val();
        	 var sch_replay_type = _self.$el.find("#sch_replay_type").val();
        	 var sch_replay_context = _self.$el.find("#sch_replay_context").val();
        	 var sch_state = _self.$el.find("#sch_state").val();
	       	 model.save({
	       		SCHEDULE_ID: schedule_id,
	       		SCH_REPLAY_CONTEXT:sch_replay_context,
	       		SCH_REPLAY_TYPE:sch_replay_type,
	       		ADID:adid,
	       		EMPLOYE_NAME:employe_name,
	       		SCH_STATE:sch_state
             }, 
             {
                 success:function(model, response, options){
//               	  console.log("Success:"+response.success);
//                	  console.log(response);
                     if(_self.saveCallback){
                         _self.saveCallback(model, response, _self.dialog);
                     }
                     
                 },
                error:function(model, response){
                	showMessage("Error","error");
                }
             });
        }
        
    });
});