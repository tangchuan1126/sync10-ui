/**
 * Created by gql on 2015-6-1 17:27:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "../../js/models/models.js",
    "../../../artDialog.js",
    "showMessage",
    "domready!"
],function($, Backbone, Handlebars, templates, Models, Dialog){
	
    return Backbone.View.extend({
        template: templates.logDetail,
        className: 'download-container',
        initialize: function(options){
        	this.saveCallback = options.saveCallback;//保存成功的回调函数
        },
        render:function(collection){
            var _self = this;
            _self.$el.html(_self.template(collection.toJSON()));
//            console.log(collection.toJSON());
            _self.dialog = new Dialog({
                content:  _self.$el,
                lock:true,
                width: 700,
                height: 460,
                title:"Log",
//                okValue:'Save',
//                ok: function (event) {
//                	_self.saveFile();//调用保存的方法
//                },
                cancelValue: 'Cancel',
//                cancel: function(){
//                	_self.cancelUpload();
//                }
            }).showModal();
            
        }
    });
});