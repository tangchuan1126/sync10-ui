"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js"
],function($, Backbone, Handlebars, templates){

    return Backbone.View.extend({
        template: templates.sortSubtasks,
        initialize: function(option){

        },
        render:function(subTasks){
            var _self = this;

            _self.subTasks = subTasks;

            _self.$el.html(_self.template({'list': subTasks}));
        },
        events: {
            "click .J-sort-item" : "selectSub",
            "click .J-btn-up" : "upItem",
            "click .J-btn-down" : "downItem"
        },
        selectSub: function(e){
            var $self = $(e.currentTarget),
                $parent = $self.parent();

            if($self.hasClass('active')){
                $parent.find('.active').removeClass('active');
            }else{

                $parent.find('.active').removeClass('active');
                $self.addClass('active');
            }
        },
        upItem:function(e){
            var $self = this.$el.find('.active'),
                $prev = $self.prev(),
                $note = $self.find('.J-note'),
                curNote = Number($note.text());

            if(!curNote || curNote === 1){
                return false;
            }

            $note.text(curNote - 1);
            $prev.find('.J-note').text(curNote);
            $prev.before($self);

            //在数组里的处理
            this.exchange(curNote, (curNote  - 1));

        },
        downItem: function(e){
            var $self = this.$el.find('.active'),
                $next = $self.next(),
                $note = $self.find('.J-note'),
                curNote = Number($note.text()),
                len = this.$el.find('.J-sort-item').length;

            if(!curNote || curNote === len){
                return false;
            }

            $note.text(curNote + 1);
            $next.find('.J-note').text(curNote);
            $next.after($self);

            //在数组里的处理
            this.exchange(curNote, (curNote  + 1));
        },
        exchange: function(arg1, arg2){
            var arr = this.subTasks,
                index1 = arg1 - 1,
                index2 = arg2 - 1;

            var temp = arr[index1];
            temp['TASK_NUMBER'] = arg2;

            
            arr[index1] = arr[index2];
            arr[index1]['TASK_NUMBER'] = arg1;

            arr[index2] = temp;
        },
        getResult: function(){
            var arr = [];

            this.$el.find('.J-sort-item').each(function(index, elem){
                var $elem = $(elem),
                    obj = {};

                obj['SCHEDULE_ID'] = $elem.data('schedule'),
                obj['TASK_NUMBER'] = $elem.find('.J-note').text();

                arr.push(obj);
            });

            return arr;
        },
        getSortedSubTasks: function(){
            return this.subTasks;
        }
    });
});