/**
 * Created by gql on 2015-5-13 15:27:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "../../../templates/templates.amd.js",
    "../../js/models/models.js",
    "../../../artDialog.js",
    "oso.lib/jquery-file-upload-extension/upload-plugin",
    "showMessage",
    "domready!"
],function($, Backbone, Handlebars, templates, Models, Dialog, uploadplugin){
	var uploadfileurl = "/Sync10/_fileserv/upload";//上传文件服务器地址
	var delfileurl = "/Sync10/_fileserv/file/";//上传文件服务器地址
	
    return Backbone.View.extend({
        template: templates.uploadSOP,
        initialize: function(options){
        	this.files=[];
        	this.saveCallback = options.saveCallback;//保存成功的回调函数
        },
        files:[],
        render:function(){
            var _self = this;
            _self.$el.html(_self.template());
            
            _self.dialog = new Dialog({
                content:  _self.$el,
                lock:true,
//                width: $('body').width() * 0.8 < 850 ? $('body').width() * 0.8 : 850,
                height: 460,
                title:"Upload SOP ",
                okValue:'Save',
                ok: function (event) {
                	_self.saveFile();//调用保存的方法
                },
                cancelValue: 'Cancel',
                cancel: function(){
                	_self.cancelUpload();
                }
            }).showModal();
            
        	this.uploadFile();  
        },
        uploadFile:function(){
        	var _self = this;
            var jsonval={
                renderTo: "#upload_file",
                fileposturl:"/Sync10/_fileserv/upload",
                nobotstrap:true,
                FileTypes:/(\.)/i,
                acceptFileTypes:"File type error.",
                unknownError:"Error",
                Asinglefile:false,//关闭批量操作功能
                batch:true,//关闭批量操作功能
                nostop:true
            };
            
            var uploadfile = new uploadplugin(jsonval);
            //上传成功的回调函数
            uploadfile.on("event.done",function(data){
            	var file = {};
            	if(data!=null&&data.length>0){
            		file.FILE_ID = data[0].file_id;
            		file.ORIGINAL_FILE_NAME = data[0].original_file_name;
            		_self.files.push(file);//将文件添加到文件数组
            	}
            });
            //删除文件的回调函数
            uploadfile.on("event.destroyed",function(e,d){
            	var url = d.url;
            	var url_server = "/Sync10/_fileserv/file/";
            	var file_id = url.substring(url_server.length);
            	//从数组中删除该文件信息
            	for(var i=0;i<_self.files.length;i++){
            		if(_self.files[i].FILE_ID==file_id){
            			_self.files.splice(i,1);
            			break;
            		}
            	}
            });

        },
        saveFile:function(){
        	var _self = this;
        	var data =  new Models.TaskFileCollection(_self.files);
        	
//        	var html = $(".files").parent("table").html();
//        	console.log(html);
    		if(_self.saveCallback){
                _self.saveCallback(data, _self.dialog);
            }
        },
        cancelUpload:function(){
        	var _self = this;
        	var fileIds = "";
        	
        	if(_self.files.length>0){
	    		for(var i=0;i<_self.files.length;i++){
	        		fileIds += ","+_self.files[i].FILE_ID;
	        	}
	    		_self.delFile(fileIds.substring(1));//删除上传到文件服务器上的文件
    		}else{
    			_self.dialog.close();
    		}
        },
        delFile:function(file_arr){
        	var _self = this;
        	//删除服务器上的文件
        	$.ajax("/Sync10/_fileserv/file/"+ file_arr,  {
                type:'DELETE',
                dataType:'json',
                success: function(data){
                	_self.dialog.close();
                },
                error:function(){
               	 showMessage("Error","error");
                }
            });
        }
    });
});