/**
 * Created by gql on 2015-3-31 15:27:16
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "handlebars_ext",
    "../../../templates/templates.amd.js",
    "./../models/models.js",
    "../../../config.js",
    "../../js/models/userModels.js",
    "/Sync10-ui/pages/task_invoice/invoice/js/models/invoiceCollection.js",
    "/Sync10-ui/pages/task_invoice/invoice/js/views/addInvoiceView.js",
    "/Sync10-ui/pages/task_invoice/invoice/js/models/invoiceModel.js",
    "../../../initDept.js",
    "../../../artDialog.js",
    "require_css!bootstrap-css/bootstrap.min",
    "bootstrap",
    "bootstrap.datetimepicker",
    "require_css!bootstrap.datetimepicker-css",
//    "require_css!/Sync10-ui/bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css",
//    "multiselect",
    "showMessage"
],function($, Backbone, Handlebars, Handlebars_ext, templates, Models, config, userModels, 
		InvoiceCollection, AddInvoiceView, InvoiceModel, initDept, Dialog){
	var users = new userModels();
	
    return Backbone.View.extend({
        template: templates.addSubTask,
        initialize: function(options){
        	this.setElement(options.el);
        	console.log("initialize addSubTaskView");
        },
        parentView:null,
        render:function(model,title){
            var _self = this;
            _self.model = model;
            _self.$el.html(_self.template({'title':title, 'model':model.toJSON()}));
            var $to = _self.$el.find("#schedule_execute_id");
            var adid = model.toJSON().ASSIGN_USER_ID;
            var to_value = model.toJSON().SCHEDULE_EXECUTE_ID;
            initDept($to, adid, to_value, users.toJSON());
        },
        events: {
        	"click i[name='addInvoice_sub']" : "addInvoice",
        	"click i[name='delInvoice_sub']" : "delInvoice",
        	"click #save_sub_task" : "saveSubTask",
        },
        addInvoice:function(e){
        	var _self = this;
        	var btn = $(e.target);
        	var bill_id = btn.data("invoice") * 1;
        	var parent_schedule_id = _self.$el.find("#parent_schedule_id").val();//主任务id
        	var invoiceCollection = new InvoiceCollection(_self.model.get("SUBINVOICE"));
//            console.log(invoiceCollection.toJSON());
        	var invoice_id=" ";
        	
        	var $showInvoice = _self.$el.find("#showInvoice");
            new AddInvoiceView({'admin': _self.admin, 'saveCallback': function(model, response, dialog){
	            	if(response.success){
	        			showMessage("Success","succeed");
	        			invoiceCollection.add(model);
	        			 _self.model.set("SUBINVOICE",invoiceCollection.toJSON());
	        			invoice_id = model.get("BILL_ID");
	        			setTimeout(function(){
	        				dialog.close();
	        			},1500);
	        			var html = "<dd>" +
			    					"<span name='billId'>"+invoice_id+"</span>" +
			    					"<div class='invoice_del'>" +
			    						"<i class='icon icon-remove' name='delInvoice' data-invoice='"+invoice_id+"'></i>" +
			    					"</div>" +
			    				"</dd>";
	        			$showInvoice.append(html);
	        			$.unblockUI();
	        		}else{
	        			showMessage("Error","error");
	        		}
                
            	}
            }).render();
        	
            
        },
        delInvoice:function(e){
        	var _self = this;
        	var btn = $(e.target);
            var bill_id = btn.data("invoice") * 1;
        	var invoiceCollection = new InvoiceCollection(_self.model.get("SUBINVOICE"));
            var invoiceModel = invoiceCollection.findWhere({
            	BILL_ID: bill_id
            });
            invoiceCollection.remove(invoiceModel);
            _self.model.set("SUBINVOICE",invoiceCollection.toJSON());
            
            btn.parent("div").parent("dd").remove();
            
        },
        saveSubTask:function(){
        	 var _self = this;
            var schedule_execute_id = _self.$el.find("#schedule_execute_id").val()==null?[]:_self.$el.find("#schedule_execute_id").val();
        	var schedule_detail = _self.$el.find("#schedule_detail").val();

            if(!schedule_execute_id.length){
                _self.$el.find("#schedule_execute_id").next().addClass('open');
                return false;
            }

            if(!schedule_detail){
                _self.$el.find("#schedule_detail").focus();
                return;
            }

            var subinvoice = [];
        	 if(_self.model.get("SUBINVOICE")){
        		 subinvoice =  _self.model.get("SUBINVOICE");//获取子任务添加的账单
        	 }
//        	 console.log(subinvoice);
        	 _self.model = new Models.SubTaskModel();
        	 var parent_schedule_id = _self.$el.find("#parent_schedule_id").val();
	       	 var assign_user_id = _self.$el.find("#assign_user_id").val();
	       	 
//	       	 var schedule_join_execute_id = _self.$el.find("#schedule_join_execute_id").val()==null?[]:_self.$el.find("#schedule_join_execute_id").val();
	       	 var is_additional_Task = _self.$el.find("#is_additional_Task").is(":checked")?1:2;
	       	 var task_number = _self.$el.find("#task_number").val();//获取子任务的排序
	       	 var schedule_id = _self.$el.find("#schedule_id").val();
	       	 if(schedule_id&&schedule_id!=""){
	       		_self.model.set("SCHEDULE_ID",schedule_id);//修改时，给idAttribute赋值，表示是修改
	       		 var deleteusers = _self.$el.find("#deleteusers").val();
	       		 var del_join_user_id = _self.$el.find("#del_join_user_id").val();
	       		_self.model.set("DELETEUSERS",deleteusers);//修改之前，执行人的id
	       		_self.model.set("DEL_JOIN_USER_ID",del_join_user_id);//修改之前，参与人的id
	       	 }
	       	 console.log("task_number="+task_number);
	       	_self.model.save({
	    	   PARENT_SCHEDULE_ID: parent_schedule_id,
	    	   SUBINVOICE: subinvoice,
	    	   TASK_NUMBER:task_number,
	       	   ASSIGN_USER_ID: assign_user_id,
	       	   IS_MAIN_SCHEDULE: 2,//子任务
	       	   SCHEDULE_EXECUTE_ID: schedule_execute_id,
	       	   SCHEDULE_JOIN_EXECUTE_ID: [],
//               TASK_TYPE: task_type,
               IS_ADDITIONAL_TASK: is_additional_Task,
               SCHEDULE_OVERVIEW:"",
               SCHEDULE_DETAIL:schedule_detail
             }, {
               success: function(model, response, options) {
            	   if(response.success){
               		showMessage("Success","succeed");
               		setTimeout(function(){
               			_self.parentView.setSubTaskList();
                       },1500);
	               	}else{
	               		showMessage("Error","error");
	               	}
             },error:function(){
            	 showMessage("Error","error");
             }
           });
        }
    });
});