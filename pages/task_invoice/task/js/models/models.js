"use strict";
define([
  "../../../config.js",
  "jquery",
  "backbone"
], function(config) {
	var SubTaskModel = Backbone.Model.extend({
		url: config.taskCollectionUrl,
        idAttribute: "SCHEDULE_ID",
        defaults: {
        	SUBINVOICE:[]
        }
	});
	var SubTaskCollection = Backbone.Collection.extend({
        model: SubTaskModel,
        url: config.taskCollectionUrl,
        parse:function(response){
            if(response.PROJECTINVOICE){this.PROJECTINVOICE = response.PROJECTINVOICE;}
            return response.MAINTASKS;
        },
    });
	
	var TaskLogModel = Backbone.Model.extend({
		url: config.addLogUrl,
        idAttribute: "SCH_REPLAY_ID"
	});
	var TaskLogCollection = Backbone.Model.extend({
		 model: TaskLogModel,
	});
	
	var TaskModel = Backbone.Model.extend({
		url: config.taskCollectionUrl,
        idAttribute: "SCHEDULE_ID",
        defaults: {
        	SUBSCHEDULE:[],
        	SUBINVOICE:[],
        	SUBFILE:[],
//        	SUBLOG:[],
        }
	});
	
	var TaskCollection = Backbone.Collection.extend({
        model: TaskModel,
        url: config.taskCollectionUrl,
        comparator: function(item) {
            //
        },
        parse:function(response){
            if(response.pageCtrl){this.pageCtrl = response.pageCtrl;}
            return response.SCHEDULE;
        },
        initialize: function(options){
         
        }
    },{
        pageCtrl:{
            pageNo:1,
            pageSize:10
        }
        
    });
	
	var TaskFileModel = Backbone.Model.extend({
		idAttribute: "FILE_ID",
	});
	
	var TaskFileCollection = Backbone.Collection.extend({
        model: TaskFileModel,
    });
	
	 return {
		 SubTaskModel:SubTaskModel,
		 SubTaskCollection:SubTaskCollection,
		 TaskModel:TaskModel,
		 TaskCollection:TaskCollection,
		 TaskFileModel:TaskFileModel,
		 TaskFileCollection:TaskFileCollection,
		 TaskLogModel:TaskLogModel,
		 TaskLogCollection:TaskLogCollection
	 };
}); 