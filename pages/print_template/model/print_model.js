"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt) {

	var PrintCollection = Backbone.Collection.extend({
		url:page_config.printCollection.url,
		parse: function(response) {
	        return response.datas;
	    }
    });
	
     return {
    	 PrintCollection:PrintCollection
    };

  });