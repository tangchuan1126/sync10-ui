"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/print_model",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models) {
    
    var A4printPackingListView = Backbone.View.extend({
    	  initialize:function(options){
            this.setElement(options.el);
          },
          collection:new Models.PrintCollection(),
	      template:templates.a4print_packing_list,
	      render: function(param) {
	    	  var v = this;
              v.collection.url = page_config.printCollection.url+param;
              v.collection.fetch({
                success:function(collection){
                	v.$el.html(v.template({
                		datas: collection.toJSON()
                	}));
                }
              });
	      },
	      events:{
	    	  "click #printInfo": "print"
	      },
	      print:function(){
	    	  var printAll = $('div[name="printPackingListHtml"]');
	    	  for(var i=0;i<printAll.length;i++){
	    			visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
	    			visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
	    			visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$(printAll[i]).html());	
	    			visionariPrinter.SET_PRINT_COPIES(1);
	    			//visionariPrinter.PREVIEW();
	    		 	visionariPrinter.PRINT();
	    	 }
	      }
	      
	    });
    
    return A4printPackingListView;

});