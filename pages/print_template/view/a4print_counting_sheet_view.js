"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/print_model",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models) {
    
    var A4CountingSheetView = Backbone.View.extend({
    	  initialize:function(options){
            this.setElement(options.el);
          },
          collection:new Models.PrintCollection(),
	      template:templates.a4print_counting_sheet,
	      render: function(param) {
	    	  var v = this;
              v.collection.url = page_config.printCollection.url+param;
              v.collection.fetch({
                success:function(collection){
                	v.$el.html(v.template({
                		datas: collection.toJSON()
                	}));
                }
              });
	      },
	      events:{
	    	  "click #printInfo": "print"
	      },
	      print:function(){
	    	  $(".tally_sheet tr:even").css("background-color","#FFF");  //改变偶数行背景色
	    	  $(".tally_sheet tr:odd").css("background-color","#999999");  //改变奇数行背景色
	    	  var printHtml=$('div[name="printHtml"]');
			     for(var i=0;i<printHtml.length;i++){
			    	 var a1=$('#a1',printHtml[i]);
			    	 var a2=$('#a2',printHtml[i]);
			    	 var a3=$('#a3',printHtml[i]);		
			    	 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");			// 设置页码字体
			    	 visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");					
			    	 visionariPrinter.ADD_PRINT_HTM(0, 0, "100%", "275mm", a1.html());
			    	
			    	 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());
			    	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
			    	 
			    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",a3.html());
			    	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
				 
			    	 visionariPrinter.SET_PRINT_COPIES(1);
			    	 visionariPrinter.PREVIEW();
//				     visionariPrinter.PRINT(); 
			     }		
		     
	      }
	      
	      
	      
	    });
    
    return A4CountingSheetView;

});