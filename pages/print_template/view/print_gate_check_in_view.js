"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/print_model",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models) {
    
    var GateCheckInView = Backbone.View.extend({
	    initialize:function(options){
		this.setElement(options.el);
	    },
	    collection:new Models.PrintCollection(),
		template:templates.print_gate_check_in,
		render: function(param) {
		    var v = this;
		    v.$el.html(v.template({
			  datas: v.collection.datas
		  }));
		v.collection.url = page_config.printCollection.url+param;
		v.collection.fetch({
		    success:function(collection){
			v.$el.html(v.template({
			    datas: collection.toJSON()
			}));
		    }
		});
	    },
	    events:{
	    	  "click #printInfo": "print"
	    },
	    print:function(){
	    	//alert("打印");
	    	//获取打印机名字列表
    		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	   //判断是否有该名字的打印机
    		var printer = "LabelPrinter";
    		var printerExist = "false";
    		var containPrinter = printer;
    		for(var i = 0;i<printer_count;i++){
    			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
    				printerExist = "true";
    				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
    				break;
    			}
    		}
    		if(printerExist=="true"){
   			     var printHtml=$('div[name="avg"]');
   			     for(var i=0;i<printHtml.length;i++){
		    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
    	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
    	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
    	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
    	  			     visionariPrinter.SET_PRINT_COPIES(1);
    	  				 visionariPrinter.PREVIEW();
    	  			     //visionariPrinter.PRINT();        		
   			     }
    		}else{
    			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
    			if(op!=-1){ //判断是否点了取消
    				 var printHtml=$('div[name="avg"]');
    			     for(var i=0;i<printHtml.length;i++){
			    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
	    	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	    	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	    	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
	    	  			     visionariPrinter.SET_PRINT_COPIES(1);
	    	  				 visionariPrinter.PREVIEW();
	    	  			     //visionariPrinter.PRINT();        		
    			     }
    			}	
    		}
	    }
	      
    });
    
    return GateCheckInView;

});