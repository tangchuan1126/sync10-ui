"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/print_model",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models) {
    
    var A4PrintView = Backbone.View.extend({
    	  initialize:function(options){
            this.setElement(options.el);
          },
          collection:new Models.PrintCollection(),
	      template:templates.a4print_generic,
	      render: function(param) {
	    	  var v = this;
              v.collection.url = page_config.printCollection.url+param;
              v.collection.fetch({
                success:function(collection){
                	v.$el.html(v.template({
                		datas: collection.toJSON()
                	}));
                }
              });
	      },
	      events:{
	    	  "click #printInfo": "print"
	      },
	      print:function(){
	    	  var printAll = $('div[name="printAll"]');
				for(var i=0;i<printAll.length;i++){
				
				//分页字体
				 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
				 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
		 
		         visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",$('#a1',printAll[i]).html());
		         visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);                //设置次页偏移把区域向下扩
		       
		        
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a2',printAll[i]).html());
		         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);                //关联
		        // visionariPrinter.SET_PRINT_STYLEA(0,"LinkNewPage",true); //新起一页
		                     
		         
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a3',printAll[i]).html());	
		         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
		         
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a4',printAll[i]).html());	
		         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
		        
		         var orderCount = $('#orderLength',printAll[i]).val()*1; //customer order 数据条数
		         var orderItemCount =$('#itemLength',printAll[i]).val()*1;  //Carrier 数据条数
		         
		         if(orderCount>=10){//order超了
		        	 visionariPrinter.NewPageA();
		        	 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a5',printAll[i]).html());
		        	 if(orderItemCount>=11){//items超了
			             visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a6',printAll[i]).html());	
			             visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
		        	 }
		 		}else if(orderItemCount>=11){//order未超，items超了
		 			 visionariPrinter.NewPageA();
		 			 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a6',printAll[i]).html());	
		        	 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
		 		}
				visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printAll[i]).html());
			    visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
				visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
				 
				visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
				visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
			 
				visionariPrinter.SET_PRINT_COPIES(1);
				visionariPrinter.PREVIEW();
//				visionariPrinter.PRINT();
		
				}
	      }
	      
	      
	      
	    });
    
    return A4PrintView;

});