"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/print_model",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models) {
    
    var ReceiptsWmsByBolContainerView = Backbone.View.extend({
    	  initialize:function(options){
            this.setElement(options.el);
          },
          collection:new Models.PrintCollection(),
	      template:templates.print_receipts_wms_by_bol_container,
	      render: function(param) {
	    	  var v = this;
              v.collection.url = page_config.printCollection.url+param;
              v.collection.fetch({
                success:function(collection){
                	v.$el.html(v.template({
                		datas: collection.toJSON()
                	}));
                }
              });
	      },
	      events:{
	    	  "click #printInfo": "print"
	      },
	      print:function(){
	    	    var printHtml=$('div[name="printWmsReceipt"]');
		  		for(var i=0;i<printHtml.length;i++){
		  			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Receipts Label");
		  	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
		  	      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
		  	         visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		  			 visionariPrinter.ADD_PRINT_TABLE("0.4cm",7,"100%","100mm",$(printHtml[i]).html());
		  			 visionariPrinter.SET_PRINT_COPIES(1);
		  			 visionariPrinter.ADD_PRINT_HTM("14cm",160,"100%","100%",addStyleForText("CONTINUED..."));
		  			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
		  			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","Last");
//		  			visionariPrinter.PREVIEW();
		  			visionariPrinter.PRINT(); 
		  		}
	      }
	      
	    });
    
    function addStyleForText(text)
	{
		return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
	}
    
    return ReceiptsWmsByBolContainerView;

});