"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/print_model",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models) {
    
    var Vics2View = Backbone.View.extend({
    	  initialize:function(options){
            this.setElement(options.el);
          },
          collection:new Models.PrintCollection(),
	      template:templates.a4print_vics2,
	      render: function(param) {
	    	  var v = this;
	    	  v.$el.html(v.template({
          		datas: v.collection.datas
          	}));
              v.collection.url = page_config.printCollection.url+param;
              v.collection.fetch({
                success:function(collection){
                	v.$el.html(v.template({
                		datas: collection.toJSON()
                	}));
                }
              });
	      },
	      events:{
	    	  "click #printInfo": "print"
	      },
	      print:function(){
	    	  var printHtml=$('div[name="printHtml"]');
	    		for(var i=0;i<printHtml.length;i++) {
	    			var a1=$('#a1',printHtml[i]);
	    		 	var a2=$('#a2',printHtml[i]);
	    		 	var a3=$('#a3',printHtml[i]);
	    		 	var a4=$('#a4',printHtml[i]);
	    		 	
	    		 	visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");			// 设置页码字体
	    		 	visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"A4");
	    		 	visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());
	    		 	visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);
	    		
	    		 	visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());
	    		 	visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
	    		 
	    		 	visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());
	    		 	visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
	    		 	
	    		 	var orderItemLength =$('#orderItemLength',printHtml[i]).val()*1; 
	    		 	if(orderItemLength>6){
	    				visionariPrinter.NewPageA();
	    				visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",a4.html());	
	    	       	 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
	    			}
	    		 	
	    		 	visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top').html());
	    			visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
	    			visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
	    			 
	    			visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
	    			visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
	    			
	    		 	visionariPrinter.SET_PRINT_COPIES(1);
	    		 	//visionariPrinter.PREVIEW();
	    	        visionariPrinter.PRINT(); 
	    		}		 
	      }
	      
	      
	      
	    });
    
    return Vics2View;

});