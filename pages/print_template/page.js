"use strict";
define([
         "config", 
         "jquery",
         "backbone",
         "handlebars",
         "handlebars_ext",
         "templates",
         "./view/a4print_generic_vizio_view",
         "./view/a4print_vics2_view",
         "./view/a4print_bol_view",
         "./view/a4print_vics2_order_view",
         "./view/print_order_master_wms_view",
         "./view/print_order_no_master_wms_view",
         "./view/print_order_no_master_wms_order_view",
         "./view/a4print_counting_sheet_view",
         "./view/a4print_counting_sheet_order_view",
         "./view/print_gate_check_in_view",
//         "auto",
//         "blockui",
//         "jqueryui/tabs",
//         "jqueryui/dialog",
         "domready"
    ],
 function(page_config,$,Backbone,Handlebars,handlebars_ext,templates,V1,V2,V3,V4,V5,V6,V7,V8,V9,V10){
        //v1
	var parsm='?printType=a4print&jsonString=[{"checkDataType":"PICKUP","number":"155980133","door_name":"11","companyId":"W12","customerId":"VIZIO","master_bol_nos":"26062","order_nos":"","checkLen":1}]&entry_id=100939&out_seal=NA';
//	var parsm='?printType=a4_counting_sheet&entryId=100939&isPrint=1&jsonString=[{"checkDataType":"PICKUP","number":"155980133","door_name":"11","companyId":"W12","customerId":"VIZIO","number_type":"10","order":"","checkLen":1}]';
//	var parsm='?printType=a4_counting_sheet_order&entryId=100939&isPrint=1&jsonString=[{"checkDataType":"PICKUP","number":"156905","door_name":"11","companyId":"W12","customerId":"VIZIO","number_type":"11","order":"","checkLen":1}]';
//	var parsm='?printType=print_order_no_master_wms_order&orderNo=156905&entryId=100939&window_check_in_time=2014-08-25 10:29:03.0&DockID=11&company_name=WERWE&gate_container_no=WERWERW&seal=NA&CompanyID=W12&CustomerID=VIZIO';
//	var parsm='?printType=print_order_no_master_wms&loadNo=155980133&entryId=100939&window_check_in_time=2014-08-25 10:29:03.0&DockID=11&company_name=WERWE&gate_container_no=WERWERW&seal=NA&CompanyID=W12&CustomerID=VIZIO';
//	var parsm='?printType=print_order_master_wms&loadNo=155980133&entryId=100939&window_check_in_time=2014-08-25 10:29:03.0&DockID=11&company_name=WERWE&gate_container_no=WERWERW&seal=NA&CompanyID=W12&CustomerID=VIZIO';
//	var parsm='?printType=bol&jsonString=[{"checkDataType":"PICKUP","number":"155980133","door_name":"11","companyId":"W12","customerId":"VIZIO","master_bol_nos":"26062","order_nos":"","checkLen":1}]&entry_id=100939&out_seal=';
//	var parsm='?printType=bol&jsonString=[{"checkDataType":"PICKUP","number":"155980133","door_name":"11","companyId":"W12","customerId":"VIZIO","master_bol_nos":"26062","order_nos":"","checkLen":1}]&entry_id=100939&out_seal=';
//	var parsm='?printType=a4print_generic_vizio&jsonString=[{"checkDataType":"PICKUP","number":"155980133","door_name":"11","companyId":"W12","customerId":"VIZIO","master_bol_nos":"26062","order_nos":"","checkLen":1}]&entry_id=100939&out_seal=';
      //var parsm='?printType=check_in_open_print&main_id=100939';
    var a4PrintVizioView = new V1({el:"#datas"});
    a4PrintVizioView.render(parsm);
//    var vics2View = new V2({el:"#datas"});
//    vics2View.render(parsm);
//    var a4PrintBolView = new V3({el:"#datas"});
//    a4PrintBolView.render(parsm);
//    var vics2OrderView = new V4({el:"#datas"});
//    vics2OrderView.render(parsm);
//    var orderMasterWmsView = new V5({el:"#datas"});
//    orderMasterWmsView.render(parsm);
//    var orderNoMasterWmsView = new V6({el:"#datas"});
//    orderNoMasterWmsView.render(parsm);
//    var orderNoMasterOderWmsView = new V7({el:"#datas"});
//    orderNoMasterOderWmsView.render(parsm);
//    var countingSheetView = new V8({el:"#datas"});
//    countingSheetView.render(parsm);
//    var countingSheetOrderView = new V9({el:"#datas"});
//    countingSheetOrderView.render(parsm);
       
     //  var gateCheckInView = new V10({el:"#datas"});
     //  gateCheckInView.render(parsm);	 
    
});
