"use strict";
define(["jquery",
	"backbone",
	'handlebars',
	'../../MsgHelper',
	"../../templates",
	"../../config",
	"oso.lib/upload/js/upload",
	"./resultView",
	"jquery-labelauty"
	],
	function($,Backbone,HandleBars,MsgHelper,templates,config,upload,ResultView)
	{
		return Backbone.View.extend(
		{
			el:"#import",
			template:templates.importOrder,
			initialize:function(opts)
			{

			},
			render:function() 
			{
			
				this.$el.html(this.template({"url":config.templateURL.url}));
				this.initFileUpload();
				return this;
			},
			events:
			{
				//"click .label-group :input":"comfirm",
			},
			initFileUpload:function()
			{
				upload({
		            elem:'#updateLoad',
		           // width:undefined,
					//height:undefined,
		            fileType:'xls,xlsx',
		            url:config.fileserv.upload,
		            success : function(data){
		                console.log(data);
		                var file = data[0];

		                var param ={
		                	importType:'2',
		                	getProgessMethod:'diff',
							fileId:file.file_id,
							fileName:file.original_file_name,
							url:config.importData.url// importDN URL
						};
						new ResultView(param).render();
		            }
	     	   });
			},
	
		});
	}
);