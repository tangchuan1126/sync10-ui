define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['errorInfo'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <span><i>For details, please download the result file: </span><span ><a href=\""
    + alias2(alias1((depth0 != null ? depth0.download : depth0), depth0))
    + "/"
    + alias2(alias1((depth0 != null ? depth0.fileId : depth0), depth0))
    + "\" class=\"\">"
    + alias2(alias1((depth0 != null ? depth0.filename : depth0), depth0))
    + "</a></i></span>\n";
},"3":function(depth0,helpers,partials,data) {
    return "            <span>Download template file: </span><span ><a href=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.downloadtemplte : depth0), depth0))
    + "\" class=\"\">template</a></span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\">\n   <div class=\"panel-body\">\n       <div class=\"text-center\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.fileId : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.downloadtemplte : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </div>\n   </div>\n</div>";
},"useData":true});
templates['importOrder'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div  class=\"panel panel-default\">  \n 	<div class=\"panel-body\">\n 		<div>\n 			<button type=\"button\" class=\"btn btn-info\"  id='updateLoad'><span class='glyphicon glyphicon-plus'/> Update Load</button>\n 			 &nbsp; &nbsp; &nbsp; &nbsp;<span class=\"download\"><i> download template here :&nbsp;&nbsp;</i><a id=\"downloadtemplate\" href=\""
    + this.escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"url","hash":{},"data":data}) : helper)))
    + "\"><i class=\"glyphicon glyphicon-download-alt\"></i></a><span> \n 		</div>\n	    <div class=\"line\">\n	    </div>\n	    <div class=\"importAre\">\n	    </div>\n	</div>    \n</div>  \n";
},"useData":true});
templates['result'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  class=\"panel panel-default\" style=\"margin-bottom: 0\">  \n	<div class=\"panel-body\">\n		<div class=\"row\">\n			<div class=\"text-center\" id=\"loading\">\n				 <img src=\"./imgs/loading.gif\"><br/>\n				<label class=\"loading import-success\">loading...</label>\n			</div>\n		</div>\n		<div class=\"text-center hidden\" id=\"success\">\n			<img src=\"./imgs/success.png\"><br>\n			<label class=\"import-success\">Import Successed</label>\n		</div>\n		<div class=\"text-center hidden\" id=\"warning\">\n			<img src=\"./imgs/warning.png\" class=\"breathing\"><br/>\n			<label class=\"import-finish\">Import Finisned</label>\n		</div>\n		<div id=\"restultDetail\" class='hide'>\n\n		</div>\n	</div>\n</div>	\n<div class=\"opbar hide\">\n	<button type=\"button\" id=\"continueImport\" class=\"btn btn-info\" style=\"display:none\" disabled>Import</button>\n	<button type=\"button\" id=\"close\" class=\"btn btn-info pull-right\">Close</button>\n</div>";
},"useData":true});
templates['resultInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<br>\n<table class=\"table table-bordered\">\n  	<th class=\"text-center\">Total DNs</th>\n    <th class=\"text-center\">Imported Count</th>\n    <th class=\"text-center\">Fail Count</th>\n    <th class=\"text-center\">Result File</th>\n    <tr>\n    	<td class=\"text-center\">"
    + alias2(alias1((depth0 != null ? depth0.total : depth0), depth0))
    + "</td>\n    	<td class=\"text-center\">"
    + alias2(alias1((depth0 != null ? depth0.successCount : depth0), depth0))
    + "</td>\n    	<td class=\"text-center invalidateCount\">"
    + alias2(alias1((depth0 != null ? depth0.failCount : depth0), depth0))
    + "</td>\n    	<td class=\"text-center\">\n    		<button type=\"button\" id=\"downloadbtn\" class=\"btn btn-success \" >download</button>\n    	</td>\n    </tr>\n</table>";
},"useData":true});
return templates;
});