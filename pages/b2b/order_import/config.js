define(
{
    updateSO:
    {
      url:"/Sync10/action/administrator/order/updSO1Action.action",
      url2:"/Sync10/action/administrator/order/updSO2Action.action"
    },
    customer: 
    {
         url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllCustomerId"
         //url:"/Sync10-ui/pages/b2b/order_import/json/country.json"
    },
    country: 
    {
         url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action"
         //url:"/Sync10-ui/pages/b2b/order_import/json/country.json"
    },
    createOrder: 
    {
         //url:"/Sync10-ui/pages/bol/bol_add/index.html"
         url:"/Sync10-ui/pages/b2b/b2b_create/index.html"
    },
    importProgess:
    {
          url:"/Sync10/action/administrator/order/excelImportProcess.action"
    },
    templateURL:
    { 
          action:"/Sync10/action/administrator/order/excelModelAction.action",    
          url:"/Sync10/_fileserv/download/"
    },
    fileserv:
    {
        upload:"/Sync10/_fileserv/upload",
        download:"/Sync10/_fileserv/download"
    },
    checkData:
    {
      url:"/Sync10/action/administrator/order/excelImportStep1.action"
    },
    importData:
    {
       url:"/Sync10/action/administrator/order/excelImportStep2.action"
    },
    session:
    {
        url:"/Sync10/_b2b/admin/session"
    }
});