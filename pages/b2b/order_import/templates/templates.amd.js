define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['errorInfo'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <span><i>For details, please download the result file: </span><span ><a href=\""
    + alias2(alias1((depth0 != null ? depth0.download : depth0), depth0))
    + "/"
    + alias2(alias1((depth0 != null ? depth0.fileId : depth0), depth0))
    + "\" class=\"\">"
    + alias2(alias1((depth0 != null ? depth0.filename : depth0), depth0))
    + "</a></i></span>\n";
},"3":function(depth0,helpers,partials,data) {
    return "            <span>Download template file: </span><span ><a href=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.downloadtemplte : depth0), depth0))
    + "\" class=\"\">template</a></span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\">\n   <div class=\"panel-body\">\n       <div class=\"text-center\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.fileId : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.downloadtemplte : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </div>\n   </div>\n</div>";
},"useData":true});
templates['importOrder'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  class=\"panel panel-default\">  \n	 	<div class=\"panel-body\">\n		    <div class=\"form-horizontal\">\n		        <div class=\"form-group\">\n		            <label for=\"customer\" class=\"control-label col-sm-3 required hide \">Customer </label>\n		            <div class=\"col-sm-6 \">\n		                  <select id=\"customerid\"  class=\"form-control vr hide \">\n			             </select>\n		            </div>\n		            <div class=\"col-sm-3 error hidden\" >\n		                <span class=\"error\" title=\"Customer  is reuqired.\"></span>\n		            </div>\n		            <div class=\"col-sm-3 hide\" id=\"validating\">\n		                 <svg version=\"1.1\" id=\"loader-1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n		                   width=\"25px\" height=\"25px\" viewBox=\"0 0 40 40\" enable-background=\"new 0 0 40 40\" xml:space=\"preserve\">\n		                  <path opacity=\"0.2\" fill=\"#000\" d=\"M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946\n		                    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634\n		                    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z\"/>\n		                  <path fill=\"#000\" d=\"M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0\n		                    C22.32,8.481,24.301,9.057,26.013,10.047z\">\n		                    <animateTransform attributeType=\"xml\"\n		                      attributeName=\"transform\"\n		                      type=\"rotate\"\n		                      from=\"0 20 20\"\n		                      to=\"360 20 20\"\n		                      dur=\"0.5s\"\n		                      repeatCount=\"indefinite\"/>\n		                    </path>\n		                  </svg>\n		            </div>\n		        </div>\n		    </div>\n	 <!--  	    \n		    <div class=\"form-horizontal\">\n		        <div class=\"form-group\">\n		            <label for=\"carrier\" class=\"control-label col-sm-3\">Carrier</label>\n		            <div class=\"col-sm-6\" >\n		                <select id=\"carrier\"  class=\"form-control vr\">\n		                	<option value=\"\" ></option>\n			             	<option value=\"CC\">CC</option>\n			             	<option value=\"CA\">CA</option>\n			            </select>\n		            </div>\n		            <div class=\"col-sm-3 error hidden\" >\n               		 	<span class=\"error\">Carrier is reuqired.</span>\n            		</div>\n		        </div>\n		    </div>\n		  \n	 		<div class=\"form-horizontal\">\n		        <div class=\"form-group\">\n		            <label for=\"receiveCountry\" class=\"control-label col-sm-3\">Receive Country</label>\n		            <div class=\"col-sm-6\" >\n		            \n		             <select id=\"receiveCountry\"  class=\"form-control vr\">\n		             	<option value=\"11036\" selected>America</option>\n		             	<option value=\"11036\">China</option>\n		             </select>\n\n		            </div>\n		            <div class=\"col-sm-3 error hidden\" >\n               		 	<span class=\"error\">Please choose country.</span>\n            		</div>\n		        </div>\n		    </div> \n		 -->\n\n		    <div class=\"line\">\n		    </div>\n		    <div class=\"importAre\">\n		    </div>\n		</div>    \n</div>  \n<div class=\"opbar\">\n	<ul class=\"label-group pull-right\">\n		<li><input type=\"radio\" name=\"radio\" data-labelauty=\"Update SO\" class=\"confirm\" value=\"1\"></li>\n		<li><input type=\"radio\" name=\"radio\" data-labelauty=\"Import DN\" class=\"confirm\" value=\"2\" checked></li>\n	</ul>\n	<!--<button type=\"button\" id=\"comfirm\" class=\"btn btn-info\">Confirm</button> -->\n</div>\n";
},"useData":true});
templates['importResult'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<br>\n<table class=\"table table-bordered\">\n  	<th class=\"text-center\">Total DNs</th>\n    <th class=\"text-center\">Imported Count</th>\n    <th class=\"text-center\">Fail Count</th>\n    <th class=\"text-center\">Result File</th>\n    <tr>\n    	<td class=\"text-center\">"
    + alias2(alias1((depth0 != null ? depth0.total : depth0), depth0))
    + "</td>\n    	<td class=\"text-center\">"
    + alias2(alias1((depth0 != null ? depth0.successCount : depth0), depth0))
    + "</td>\n    	<td class=\"text-center invalidateCount\">"
    + alias2(alias1((depth0 != null ? depth0.failCount : depth0), depth0))
    + "</td>\n    	<td class=\"text-center\">\n    		<button type=\"button\" id=\"downloadbtn\" class=\"btn btn-success \" >download</button>\n    	</td>\n    </tr>\n</table>";
},"useData":true});
templates['result'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  class=\"panel panel-default\" style=\"margin-bottom: 0\">  \n	<div class=\"panel-body\">\n		<div class=\"row\">\n			<div class=\"text-center\" id=\"loading\">\n				 <img src=\"./imgs/loading.gif\"><br/>\n				<label class=\"import-success\">loading...</label>\n			</div>\n		</div>\n		<div class=\"text-center hidden\" id=\"success\">\n			<img src=\"./imgs/success.png\"><br>\n			<label class=\"import-success\">Import Successed</label>\n		</div>\n		<div class=\"status hidden\" id=\"warning\">\n			<img src=\"./imgs/warning.png\" class=\"breathing\"><br/>\n			<label class=\"import-finish\">Import Finisned</label>\n		</div>\n		<div id=\"restultDetail\">\n		</div>\n	</div>\n</div>	\n<div class=\"opbar\">\n	<button type=\"button\" id=\"continueImport\" class=\"btn btn-info\" style=\"display:none\" disabled>Import</button>\n	<button type=\"button\" id=\"close\" class=\"btn btn-info\">Close</button>\n</div>";
},"useData":true});
return templates;
});