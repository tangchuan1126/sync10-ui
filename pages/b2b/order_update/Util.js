define(["jquery"],function($)
	{
		return {
			isEmpty:function(str)
			{
				return str =="" || str == undefined || str == null || str =="undefined" || str=="null";
			},
			handleEmpty:function(str,defaultValue)
			{
				if(this.isEmpty(str))
				{
					return defaultValue;
				}
				else
				{
					return $.trim(str);
				}
			},
			isNumberStr:function(str)
			{
				return /^\d+$/.test(str);
			}
		}
	});