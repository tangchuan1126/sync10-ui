"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery", 
			"./js/view/importView",
			"bootstrap",
			"domready"
			],
			function($,ImportView)
			{	
				$.ajax({
				url: config.template.action,
				type: 'GET',
				dataType: 'text',
				})
				.done(function(data) {
					config.template.url = config.template.url+eval(data);
					new ImportView().render();
				})
				.fail(function() {
					console.log("error");
				});
			});
	});