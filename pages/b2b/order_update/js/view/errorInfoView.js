define(["jquery","backbone","../../config","../../templates","../../KeyUtil","../../json/errorTypeKey"],
function($,Backbone,config,templates,KeyUtil,TypeKey) {
	return Backbone.View.extend(
	{
			el:"#restultDetail",
			template:templates.errorInfo,
			//model:new FilterModel(),
			fileserv:{"upload":config.fileserv.upload,"download":config.fileserv.download},
			initialize:function(options)
			{
	
			},
			events:
			{
				"click #filter" : "errorInfo",
			},
			render:function(errorInfo)
			{	
				var param = $.extend({},this.fileserv, errorInfo);
				var v = this;
				$("#restultDetail").html(this.template(param));
				//this.$el.html(this.template(errorInfo));
				return this;
			}
	});

}
);