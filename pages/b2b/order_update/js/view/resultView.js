"use strict";
define(["jquery",
	"backbone",
	'handlebars',
	'../../Util',
	'../../MsgHelper',
	"../../templates",
	"../../config",
	"./errorInfoView",
	"./importResultView",
	"artDialog"
	],
	function($,Backbone,HandleBars,Util,MsgHelper,templates,config,ErrorInfoView,ImportResultView)
	{
		return Backbone.View.extend(
		{
			el:"#update",
			template:templates.result,
			errroInfoView : new ErrorInfoView(),
			checkResult:true,
			initialize:function(opts)
			{
			//	this.resultView = opts.resultView
			},
			render:function(data) {
				var that = this;
				this.$el.html(this.template({}));

				var formData = {
					"customerid":data.customerid,
					"carrier":data.carrier,"receiveCountry":data.receiveCountry,
					"fileId":data.fileId,"fileName":data.fileName
				};
				this.formData = formData;

				$.ajax({
					url:config.checkData.url,
					//url: '/Sync10/action/administrator/order/excelImportStep1.action',
					type: 'GET',
					dataType: 'text',
					timeout : 600000, 
					cache:false,
					data:formData,
				})
				.done(function(data) {
					// that.handleCheckResult(data);
					//提交数据成功，开始查询完成进度
					that.getProgess();
				})
				.fail(function(e) {
					that.handleException(e);
					console.log(e);
				});
			},
			handleCheckResult:function(data)
			{
				//console.log(data);
				var that = this;
				$("#loading").hide();
				var warning = data.warning;
				if(warning != undefined && warning !='')
				{
					this.checkResult = parseInt(warning) == 0;
				}
				//console.log(data);
				if(data.code=="200")
				{
					var text  = this.checkResult?"Check Successed":data.msg;
					if(this.checkResult)
					{
						$("#success").removeClass('hidden').addClass('show');
						$("#success").find("label").html(text);
						$("#continueImport").attr('disabled', false);
						$("#continueImport").show();
					}
					else
					{
						$("#warning").removeClass('hidden').addClass('show');
						$("#warning").find("label").html(text);
						$("#continueImport").attr('disabled', false);
						$("#continueImport").show();
					}
					if(data.failCount > 0)
					{
						(new ImportResultView()).render(data);
					}
					else
					{
						that.startImport();
					}
				}
				else 
				{
					$("#warning").find("label").html(data.msg);
					$("#warning").removeClass('hidden').addClass('show');
					data.downloadtemplte = config.template.url;
					$("#continueImport").hide();
					this.errroInfoView.render(data);
				}
			},
			handleException:function(e)
			{
				$("#loading").hide();
				if(e.status=="504" || e.status=="502")
				{
					//请求超时
					$("#warning").find("label").text("Network Errors");
					$("#warning").removeClass('hidden').addClass('show');
				}else if(e.status=="404" || e.status=="500")
				{
					//系统异常
					$("#warning").find("label").text("System Errors！Please contact system administrator.");
					$("#warning").removeClass('hidden').addClass('show');
				}else
				{
					$("#warning").find("label").text("Unknown Errors");
					$("#warning").removeClass('hidden').addClass('show');
				}
			},
			startImport:function()
			{
				var that = this;
				if(!this.checkResult)
				{
					artDialog.confirm("未校验成功的数据将不会被导入，确定要继续导入？",function()
					{
						that.importData();
					},function(){

					});
				}else
				{
					this.importData();
				}
			},
			importData:function()
			{
				//第一阶段完成
				$("#steps li:eq(1)").removeClass().addClass('default');
				//第二阶段开始
				$("#steps li:eq(2)").removeClass().addClass('info');

				$("#success").removeClass('show').addClass('hidden');
				$("#warning").removeClass('show').addClass('hidden');
				
				$("#restultDetail").empty();
				$("#continueImport").addClass('hidden');
				$("#loading").show();
				var that = this;
				var param = that.formData;
				$.ajax({
					url: config.updateData.url,
					type: 'POST',
					dataType: 'json',
					data: param ,
				})
				.done(function(data) {
					$("#loading").hide();
					that.handleImport(data);
					artDialog.opener.refreshWindow();
				})
				.fail(function(e) {
					$("#loading").hide();
					that.handleException(e);
				});
			},
			handleImport:function(data)
			{
				//console.log(data);
				if(data.code=="200")
				{
					//that.handleImport("success");
					$("#success").removeClass("hidden")
						.addClass('show').find('label').text('Import Finshed');
						new ImportResultView().render(data);
				}
				else
				{
					$("#warning").removeClass('hidden').addClass('show');
					$("#warning").find('img').addClass('picBreathing');
					//$("#warning").find("label").text(data.msg);
					
					$("#warning").find("label").addClass('text-danger');
					$("#warning").find("label").html(data.msg);
					//this.errroInfoView.render(data);
				}
				
				this.errroInfoView.render(data);
			},
			getProgess:function()
			{
				var that = this;
				setTimeout(
					function(){
						$.ajax({
								url: config.importProgess.url+"?a="+new Date().getTime(),
								type: 'GET',
								dataType: 'JSON',
								data:{action:"get",m:"diff"}
							})
							.done(function(data) {
								//console.log(data);
								if(data != null && data.finish== "1")
								{
									that.handleCheckResult(data);
									//请求删除缓存
									that.removeCache();
								}
								else if(data != null && data.finish== "0")
								{
									that.getProgess();
								}
								else
								{
									//出现异常，未返回合法的数据
									that.handleException({});
								}
							}).fail(function(e)
							{
								console.log(e);
								that.handleException(e);
							});

					},500);
			},
			removeCache:function()
			{
				$.ajax({
					url:  config.importProgess.url,
					type: 'GET'
				});
			},
			events:
			{
				"click #close":"close",
				"click #continueImport":"startImport"
			},
			close:function()
			{
				artDialog.close();
			}
		
		});
	}
);