"use strict";
define(["jquery",
	"backbone",
	'handlebars',
	'../../Util',
	'../../MsgHelper',
	"../../templates",
	"../../config",
	"immybox",
	//"../../upload",
	"oso.lib/jquery-file-upload-extension/upload-plugin",
	"./resultView",
	"require_css!oso.lib/immybox-master/immybox",
	//"oso.lib/AjaxFileUploaderV2.1/ajaxfileupload"
	],
	function($,Backbone,HandleBars,Util,MsgHelper,templates,config,immybox,uploadplugin,ResultView)
	{
		return Backbone.View.extend(
		{
			el:"#update",
			template:templates.importOrder,
			file:{},
			initialize:function(opts)
			{
			//	this.resultView = opts.resultView

			},
			preRender:function()
			{
				this.$el.html(this.template({}));
			},
			render:function() 
			{
				this.preRender();
				this.initCustomer("");
				//this.initCarrier();
				
				//this.initCountry("11036");

	 			this.initFileUpload();
	 			$("#global").show(500, function() {
	 			});
				return this;
			},
			events:
			{
				"click #comfirm":"comfirm",
				"blur .vr":"valiSingle"
			},
			initFileUpload:function()
			{
				var that = this;
				var jsonval={
                     renderTo: ".importAre",
                     //nobotstrap:true,
                     //fileposturl:"/Sync10/action/administrator/file_up/JqueryFileUpSubmitFormAction.action",
                     fileposturl:config.fileserv.upload,
                    // fileposturl:"/Sync10/action/administrator/order/excelImport.action",
                     FileSize:500000,
                     FileTypes:/(\.|\/)(xls|xlsx)$/i,
                     acceptFileTypes:"文件类型错误",
                     unknownError:"未知错误",
                     batch:false,
                     templateurl:"./import_tlp.html",
                     Asinglefile:true,
					 Initialize:function(){
						$("#downloadtemplate").attr("href",config.template.url);
					 }
                 };
               var uploadfile = new uploadplugin(jsonval);
			   //console.log($("#downloadtemplate").length);
                //上传成功回调 
                uploadfile.on("event.done",function(result){
                 	//console.log(files);
                 	that.file = result[0];
                });
               
			},
			initCustomer:function(selectedId)
			{
				$.ajax({
					url: config.customer.url,
					type: 'GET',
					dataType: 'json',
					data: {"adgid": '1000007'},
				})
				.done(function(customers) 
				{
				 	$("#customerid").empty();
					var options = $("#customerid")[0].options;
					options.add(new Option('',''));
					$.each(customers,function(i){
						var option = new Option(customers[i].TEXT,customers[i].VALUE); 
						if(selectedId == customers[i].VALUE)
						{
							option.selected=true;
						}
						options.add(option);
					});
				})
				.fail(function(e) {
					console.log(e);
				});
			},
			initCarrier:function()
			{
				//carrier已经隐藏
				return;
				$.ajax({
					url: config.country.url,
					type: 'GET',
					dataType: 'json',
					data: {"action": 'getAllCountry'},
				})
				.done(function(carriers) 
				{
				 	$("#carrier").empty();
					var options = $("#carrier")[0].options;
					var selectedId = "";
					$.each(customers,function(i){
						var option = new Option(carriers[i].c_country,carriers[i].ccid); 
						if(selectedId == carriers[i].ccid)
						{
							option.selected=true;
						}
						options.add(option);
					});
				})
				.fail(function(e) {
					console.log(e);
				});
			},
			initCountry:function(selectedId)
			{
				$.ajax({
					url: config.country.url,
					type: 'GET',
					dataType: 'json',
					data: {"action": 'getAllCountry'},
				})
				.done(function(countrys) 
				{
				 	$("#receiveCountry").empty();
					var options = $("#receiveCountry")[0].options;
					$.each(countrys,function(i){
						var option = new Option(countrys[i].C_COUNTRY,countrys[i].CCID); 
						if(selectedId == countrys[i].CCID)
						{
							option.selected=true;
						}
						options.add(option);
					});
				})
				.fail(function(e) {
					console.log(e);
				});
			},
			valiSingle:function()
			{
				if(this.needVali)
				{
					this.validate();
				}
			},
			_errorInfo:function($obj,result,errorInfo)
			{
				$obj.next().attr('title', '')
				if(result)
				{
					$obj.parent().next().removeClass('show').addClass('hidden');
					$obj.parent().removeClass('has-error');//.addClass('has-success');
				}
				else
				{
					$obj.parent().next().removeClass('hidden').addClass('show');
					$obj.parent().removeClass('has-success').addClass('has-error');
				}
			},
			getObject:function()
			{
				return {
					customid: $("#customerid"),
					carrier: $("#carrier"),
					receiveCountry:$("#receiveCountry")
				}
			},
			valiResult:function()
			{
				return $("#update .has-error").length < 1;
			},
			validate:function()
			{
				this.needVali=true;
				var customid = $("#customerid").val();
				var carrier = $("#carrier").val();
				var receiveCountry = $("#receiveCountry").val();
				var fileName =this.file;

			//	this._errorInfo($("#customerid"),!Util.isEmpty(customid),"请输入Customer");
				//this._errorInfo($("#carrier"),!Util.isEmpty(carrier),"请输入carrier");
				this._errorInfo($("#receiveCountry"),!Util.isEmpty(receiveCountry),"请输入receiveCountry");
			},
			comfirm:function()
			{
				this.validate();
				if(this.valiResult())
				{

					if($(".files .delete").length < 1)
					{
						MsgHelper.showMessage("Please upload a file !","error");
						return false;
					}

						var param ={
						customerid:$("#customerid").val(),
						//carrier:$("#carrier").val(),
						//receiveCountry:$("#receiveCountry").val(),
						receiveCountry:'11036',
						fileId:this.file.file_id,
						fileName:this.file.original_file_name
					};

					//第一阶段完成
					$("#steps li:eq(0)").removeClass().addClass('default');
					//第二阶段开始
					$("#steps li:eq(1)").removeClass().addClass('info');

					new ResultView().render(param);
					
				}
			}
		});
	}
);