define(["jquery","backbone","../../config","../../templates","../../KeyUtil","../../json/errorTypeKey"],
function($,Backbone,config,templates,KeyUtil,TypeKey) {
	return Backbone.View.extend(
	{
			el:"#restultDetail",
			template:templates.importResult,
			//model:new FilterModel(),
			initialize:function(options)
			{
	
			},
			render:function(result)
			{
				var v = this;
				//$("#restultDetail").html(this.template({}));
				this.$el.html(this.template(result));
				$("#downloadbtn").click(function(event) {
					window.location.href=config.fileserv.download+"/"+result.fileId;
				});
			//	console.log(result);
				if(result.invalidateCount != "0")
				{
					$("#restultDetail .invalidateCount").addClass('danger');
					$("#downloadbtn").addClass("buttonBreathing");
				}
				if(result.moreCount != "0")
				{
					$("#restultDetail .moreCount").addClass('danger');
					$("#downloadbtn").addClass('buttonBreathing');
				}
				
				return this;
			}
	});

}
);