define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['errorInfo'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <span>For details, please download the result file: </span><span ><a href=\""
    + alias2(alias1((depth0 != null ? depth0.download : depth0), depth0))
    + "/"
    + alias2(alias1((depth0 != null ? depth0.fileId : depth0), depth0))
    + "\" class=\"\">"
    + alias2(alias1((depth0 != null ? depth0.filename : depth0), depth0))
    + "</a></span>\n";
},"3":function(depth0,helpers,partials,data) {
    return "            <span>Download template file: </span><span ><a href=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.downloadtemplte : depth0), depth0))
    + "\" class=\"\">template</a></span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\">\n   <div class=\"panel-body\">\n       <div class=\"text-center\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.fileId : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.downloadtemplte : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </div>\n   </div>\n</div>";
},"useData":true});
templates['importOrder'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  class=\"panel panel-default\">  \n	 	<div class=\"panel-body\">\n		    <div class=\"form-horizontal\">\n		        <div class=\"form-group\">\n		            <label for=\"customer\" class=\"control-label col-sm-3 required hide \">Customer </label>\n		            <div class=\"col-sm-6 \">\n		                  <select id=\"customerid\"  class=\"form-control vr hide\">\n			             </select>\n		            </div>\n		            <div class=\"col-sm-3 error hidden\" >\n		                <span class=\"error\" title=\"Customer  is reuqired.\"></span>\n		            </div>\n		        </div>\n		    </div>\n	 <!--  	    \n		    <div class=\"form-horizontal\">\n		        <div class=\"form-group\">\n		            <label for=\"carrier\" class=\"control-label col-sm-3\">Carrier</label>\n		            <div class=\"col-sm-6\" >\n		                <select id=\"carrier\"  class=\"form-control vr\">\n		                	<option value=\"\" ></option>\n			             	<option value=\"CC\">CC</option>\n			             	<option value=\"CA\">CA</option>\n			            </select>\n		            </div>\n		            <div class=\"col-sm-3 error hidden\" >\n               		 	<span class=\"error\">Carrier is reuqired.</span>\n            		</div>\n		        </div>\n		    </div>\n		  \n	 		<div class=\"form-horizontal\">\n		        <div class=\"form-group\">\n		            <label for=\"receiveCountry\" class=\"control-label col-sm-3\">Receive Country</label>\n		            <div class=\"col-sm-6\" >\n		            \n		             <select id=\"receiveCountry\"  class=\"form-control vr\">\n		             	<option value=\"11036\" selected>America</option>\n		             	<option value=\"11036\">China</option>\n		             </select>\n\n		            </div>\n		            <div class=\"col-sm-3 error hidden\" >\n               		 	<span class=\"error\">Please choose country.</span>\n            		</div>\n		        </div>\n		    </div> \n		 -->\n		    <div class=\"line\">\n		    </div>\n		    <div class=\"importAre\">\n		    </div>\n		</div>    \n</div>  \n<div class=\"opbar\">\n	<button type=\"button\" id=\"comfirm\" class=\"btn btn-info\">Confirm</button>\n</div>\n";
},"useData":true});
templates['importResult'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<br>\n<table class=\"table table-bordered\">\n	<th class=\"text-center\">Total DNs</th>\n    <th class=\"text-center\">Imported Count</th>\n    <!-- th class=\"text-center\">Hanging Count</th -->\n    <th class=\"text-center\">Fail Count</th>\n    <th class=\"text-center\">Result File</th>\n    <tr>\n    	<td class=\"text-center\">"
    + alias2(alias1((depth0 != null ? depth0.total : depth0), depth0))
    + "</td>\n    	<td class=\"text-center\">"
    + alias2(alias1((depth0 != null ? depth0.successCount : depth0), depth0))
    + "</td>\n    	<!-- td class=\"text-center invalidateCount\">"
    + alias2(alias1((depth0 != null ? depth0.invalidateCount : depth0), depth0))
    + "</td -->\n		<td class=\"text-center invalidateCount\">"
    + alias2(alias1((depth0 != null ? depth0.failCount : depth0), depth0))
    + "</td>\n    	<!-- td class=\"text-center moreCount\">"
    + alias2(alias1((depth0 != null ? depth0.moreCount : depth0), depth0))
    + "</td -->\n    	<td class=\"text-center\">\n    		<button type=\"button\" id=\"downloadbtn\" class=\"btn btn-success \" >download</button>\n    	</td>\n    </tr>\n</table>";
},"useData":true});
templates['result'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  class=\"panel panel-default\" style=\"margin-bottom: 0\">  \n	<div class=\"panel-body\">\n		<div class=\"row\">\n			<div class=\"text-center\" id=\"loading\">\n				 <img src=\"./imgs/loading.gif\"><br/>\n				<label class=\"import-success\">loading...</label>\n			</div>\n		</div>\n		<div class=\"text-center hidden\" id=\"success\">\n			<img src=\"./imgs/success.png\"><br>\n			<label class=\"import-success\">Import Successed</label>\n		</div>\n		<div class=\"status hidden\" id=\"warning\">\n			<img src=\"./imgs/warning.png\" class=\"breathing\"><br/>\n			<label class=\"import-finish\">Import Finisned</label>\n		</div>\n		<div id=\"restultDetail\">\n		</div>\n	</div>\n</div>	\n<div class=\"opbar\">\n	<button type=\"button\" id=\"continueImport\" class=\"btn btn-info\" style=\"display:none\" disabled>Update</button>\n	<button type=\"button\" id=\"close\" class=\"btn btn-info\">Close</button>\n</div>";
},"useData":true});
return templates;
});