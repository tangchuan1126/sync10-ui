(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['errorInfo'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <span>For details, please download the result file: </span><span ><a href=\""
    + escapeExpression(lambda((depth0 != null ? depth0.download : depth0), depth0))
    + "/"
    + escapeExpression(lambda((depth0 != null ? depth0.fileId : depth0), depth0))
    + "\" class=\"\">"
    + escapeExpression(lambda((depth0 != null ? depth0.filename : depth0), depth0))
    + "</a></span>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <span>Download template file: </span><span ><a href=\""
    + escapeExpression(lambda((depth0 != null ? depth0.downloadtemplte : depth0), depth0))
    + "\" class=\"\">template</a></span>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"panel panel-default\">\r\n   <div class=\"panel-body\">\r\n       <div class=\"text-center\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.fileId : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.downloadtemplte : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "      </div>\r\n   </div>\r\n</div>";
},"useData":true});
templates['importOrder'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div  class=\"panel panel-default\">  \r\n	 	<div class=\"panel-body\">\r\n		    <div class=\"form-horizontal\">\r\n		        <div class=\"form-group\">\r\n		            <label for=\"customer\" class=\"control-label col-sm-3 required hide \">Customer </label>\r\n		            <div class=\"col-sm-6 \">\r\n		                  <select id=\"customerid\"  class=\"form-control vr hide\">\r\n			             </select>\r\n		            </div>\r\n		            <div class=\"col-sm-3 error hidden\" >\r\n		                <span class=\"error\" title=\"Customer  is reuqired.\"></span>\r\n		            </div>\r\n		        </div>\r\n		    </div>\r\n	 <!--  	    \r\n		    <div class=\"form-horizontal\">\r\n		        <div class=\"form-group\">\r\n		            <label for=\"carrier\" class=\"control-label col-sm-3\">Carrier</label>\r\n		            <div class=\"col-sm-6\" >\r\n		                <select id=\"carrier\"  class=\"form-control vr\">\r\n		                	<option value=\"\" ></option>\r\n			             	<option value=\"CC\">CC</option>\r\n			             	<option value=\"CA\">CA</option>\r\n			            </select>\r\n		            </div>\r\n		            <div class=\"col-sm-3 error hidden\" >\r\n               		 	<span class=\"error\">Carrier is reuqired.</span>\r\n            		</div>\r\n		        </div>\r\n		    </div>\r\n		  \r\n	 		<div class=\"form-horizontal\">\r\n		        <div class=\"form-group\">\r\n		            <label for=\"receiveCountry\" class=\"control-label col-sm-3\">Receive Country</label>\r\n		            <div class=\"col-sm-6\" >\r\n		            \r\n		             <select id=\"receiveCountry\"  class=\"form-control vr\">\r\n		             	<option value=\"11036\" selected>America</option>\r\n		             	<option value=\"11036\">China</option>\r\n		             </select>\r\n\r\n		            </div>\r\n		            <div class=\"col-sm-3 error hidden\" >\r\n               		 	<span class=\"error\">Please choose country.</span>\r\n            		</div>\r\n		        </div>\r\n		    </div> \r\n		 -->\r\n		    <div class=\"line\">\r\n		    </div>\r\n		    <div class=\"importAre\">\r\n		    </div>\r\n		</div>    \r\n</div>  \r\n<div class=\"opbar\">\r\n	<button type=\"button\" id=\"comfirm\" class=\"btn btn-info\">Confirm</button>\r\n</div>\r\n";
  },"useData":true});
templates['importResult'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<br>\r\n<table class=\"table table-bordered\">\r\n	<th class=\"text-center\">Total DNs</th>\r\n    <th class=\"text-center\">Imported Count</th>\r\n    <!-- th class=\"text-center\">Hanging Count</th -->\r\n    <th class=\"text-center\">Fail Count</th>\r\n    <th class=\"text-center\">Result File</th>\r\n    <tr>\r\n    	<td class=\"text-center\">"
    + escapeExpression(lambda((depth0 != null ? depth0.total : depth0), depth0))
    + "</td>\r\n    	<td class=\"text-center\">"
    + escapeExpression(lambda((depth0 != null ? depth0.successCount : depth0), depth0))
    + "</td>\r\n    	<!-- td class=\"text-center invalidateCount\">"
    + escapeExpression(lambda((depth0 != null ? depth0.invalidateCount : depth0), depth0))
    + "</td -->\r\n		<td class=\"text-center invalidateCount\">"
    + escapeExpression(lambda((depth0 != null ? depth0.failCount : depth0), depth0))
    + "</td>\r\n    	<!-- td class=\"text-center moreCount\">"
    + escapeExpression(lambda((depth0 != null ? depth0.moreCount : depth0), depth0))
    + "</td -->\r\n    	<td class=\"text-center\">\r\n    		<button type=\"button\" id=\"downloadbtn\" class=\"btn btn-success \" >download</button>\r\n    	</td>\r\n    </tr>\r\n</table>";
},"useData":true});
templates['result'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div  class=\"panel panel-default\" style=\"margin-bottom: 0\">  \r\n	<div class=\"panel-body\">\r\n		<div class=\"row\">\r\n			<div class=\"text-center\" id=\"loading\">\r\n				 <img src=\"./imgs/loading.gif\"><br/>\r\n				<label class=\"import-success\">loading...</label>\r\n			</div>\r\n		</div>\r\n		<div class=\"text-center hidden\" id=\"success\">\r\n			<img src=\"./imgs/success.png\"><br>\r\n			<label class=\"import-success\">Import Successed</label>\r\n		</div>\r\n		<div class=\"status hidden\" id=\"warning\">\r\n			<img src=\"./imgs/warning.png\" class=\"breathing\"><br/>\r\n			<label class=\"import-finish\">Import Finisned</label>\r\n		</div>\r\n		<div id=\"restultDetail\">\r\n		</div>\r\n	</div>\r\n</div>	\r\n<div class=\"opbar\">\r\n	<button type=\"button\" id=\"continueImport\" class=\"btn btn-info\" style=\"display:none\" disabled>Update</button>\r\n	<button type=\"button\" id=\"close\" class=\"btn btn-info\">Close</button>\r\n</div>";
  },"useData":true});
})();