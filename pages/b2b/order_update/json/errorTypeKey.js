define({
    "1": {
        "en": "excel 原始数据错误,excel列对数据库字段类型校验",
        "zh": "excel 原始数据错误,excel列对数据库字段类型校验"
    },
    "2": {
        "en": "nes_tmp临时表里数据有不合理数据",
        "zh": "nes_tmp临时表里数据有不合理数据"
    },
    "3": {
        "en": "order表里有的dn在excel里没有",
        "zh": "order表里有的dn在excel里没有"
    },
    "4": {
        "en": "excel里有的dn在order表里没有",
        "zh": "excel里有的dn在order表里没有"
    },
    "5": {
        "en": "相同的dn号但是lines不同",
        "zh": "相同的dn号但是lines不同"
    },
     "6": {
        "en": "国籍和州没找到的",
        "zh": "国籍和州没找到的"
    },
     "7": {
        "en": "shipTo 地址有变化的",
        "zh": "shipTo 地址有变化的"
    },
     "8": {
        "en": "发货仓库映射没有找到",
        "zh": "发货仓库映射没有找到"
    },
     "9": {
        "en": "相同的dn在数据库里的order已经关闭不能操作",
        "zh": "相同的dn在数据库里的order已经关闭不能操作"
    },
     "10": {
        "en": "相同的dn在item相同但是生成运单不能操作",
        "zh": "相同的dn在item相同但是生成运单不能操作"
    }
});