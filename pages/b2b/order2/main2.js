"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config){
	require(["jquery",
		"./js/view/advanceSearchView",
		"oso.lib/table/js/table",
		"auto",
		"artDialog",
		"nprogress",
		"bootstrap",
		"domready!",
		"require_css!nprogress"
	],
	function($,SearchView/*,CommonToolsView*/,Table,auto,artDialog,NProgress) {
		NProgress.start();
		var psId="0";
		var queryOptions = {el: "#data", url:config.searchDefaultURL.url,queryCondition:{"send_psid":0,pageSize:10}};
		/*
		var resultView  = new OrderListView(queryOptions);
		//resultView.collection.pageCtrl.pageSize= 10;
		resultView.on("events.Error", function(e) {
			$(".loding,.LodingModal").hide();
			alert("无法从服务器获取数据,请稍后在试。");
		});*/
		/*
		jQuery.download = function (url, data, method) {
			// 获取url和data
			if (url && data) {
				// data 是 string 或者 array/object
				data = typeof data == 'string' ? data : jQuery.param(data);
				var inputs = '';
				jQuery.each(data.split('&'), function () {
					var pair = this.split('=');
					inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
				});
				console.log(inputs);
				//jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
			};
		};
		*/
		var sv = null;
		var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
		var resultView = new Table({
			container : '#data',
			//url: config.searchDefaultURL.url,
			title : 'Order List',
			height: (height-290),
			rownumbers : false,
			checkbox: true,
			singleSelection : false,
			pagination: false,
			datasName: 'DATA',
			pageSizeName: 'pageSize',
			pageNumberName: 'pageNo',
			pageSize:500,
			//totalName: 'PAGECTRL.allCount',
			pageList: ["50","100","200","500","1000"],
			isEnable:function(row){
				if (row.B2B_ORDER_STATUS==5) {
					return false;
				} else {
					return true;
				}
			},
			columns:[
				{field:'B2B_OID',title:'Order NO.',width:60,formatter : function(data, row){
					return '<a href="'+config.order.url+'?B2B_OID='+data+'" target="_new">' + data + '</a>';
				}},
				{field:'CUSTOMER_DN',title:'DN',width:50},
				{field:'RETAIL_PO',title:'PO',width:50},
				{field:'SEND_PSNAME',title:'Hub',width:100},
				{field:'MABD',title:'MABD',width:100},
				{field:'SHIP_TO_PARTY_NAME',title:'Ship To',width:100,formatter : function(data, row){
					if (row.RECEIVE_PSID==0) {
						return "["+data+"]";
					} else {
						return data;
					}
				}},
				{field:'CUSTOMER_NAME',title:'Customer',width:100}
				/*,{field:'LOAD_NO',title:'Load#',width:100}*/
			],
			toolbar: [{
					text : 'Commit',
					btnClass: "btn-warning",
					id: 'check',
					iconCls : 'add',
					handler : function() {
						var v = this;
						var wid = "";
						var sid = "";
						var rows = resultView.getSelected();
						var ids = [];
						var bids = [];
						if (rows) {
							var sendid = ""
							for (var i=0;i<rows.length;i++) {
								if (i==0) {
									//sendid = rows[i].SEND_PSNAME;
									wid = rows[i].COMPANY_ID;
									sid = rows[i].CUSTOMER_ID
								}
								if (rows[i].CUSTOMER_DN) {
									ids.push(''+rows[i].CUSTOMER_DN);
									bids.push(rows[i].B2B_OID);
								}
								/*
								if (rows[i].COMPANY_ID!=wid) {
									artDialog.alert("Please select [Ship From] for search.");
									return;
								}
								if (rows[i].CUSTOMER_ID!=sid) {
									artDialog.alert("Please select only one customer.");
									return;
								}*/
							}
							if (ids.length>0) {
								//if (!$("#customer").attr("data-value")||$("#customer").attr("data-value")=="") {
								/*
								if (!$("#customer").val()||$("#customer").val()=="") {
									artDialog.alert("Please select [customer] for search.");
									return;
								}
								*/
								var params = {
									"companyId": wid,
									"customerId": sid,
									"dnList": ids
								};
								//console.log(params);
								//$.download(config.commitDN.url, params, 'post');
								NProgress.start();
								$(v).attr("disabled",true);
								$.ajax({
									type: "POST",
									url: config.commitDN.url,
									data: params,
									timeout: 60000,
									datatype: "json",//"xml", "html", "script", "json", "jsonp", "text".
									beforeSend:function(){
										//在请求之前调用的函数
									},
									success: function(data,textStatus) {
										//成功返回之后调用的函数
										if (textStatus=="success") {
											//console.log(data);
											if (data.length == 2) {
												var path= data[0].split("/");
												if (path.length>0) {
													artDialog.alert("Please download:<a href='/Sync10/"+data[0]+"' download='"+path[path.length-1]+"'>"+path[path.length-1]+"</a>");
												} else {
													artDialog.alert("Please download:<a href='/Sync10/"+data[0]+"' download='"+path[path.length-1]+"'>"+data[0]+"</a>");
												}
												//console.log(data);
												/*
												if(data[1]==='Y') {
													
												}*/
												//sv.filter();
												artDialog.opener.refreshWindow();
												/*
												取消生成运单
												$.ajax({
													type: "POST",
													url: config.addTransport.url,
													data: {data: JSON.stringify(bids), action:"addTransport"},
													timeout: 30000,
													datatype: "json",
													success: function(data,textStatus) {
														//成功返回之后调用的函数
														//artDialog.alert("Create BOL OK");
														//console.log(data);
														sv.filter();
														artDialog.opener.refreshWindow();
													},
													error: function(){
														alert("Can't Create BOL!");
													},
													headers: {
														"X-HTTP-Method-Override": "GET"
													}
												});
												*/
											} else {
												artDialog.alert("Bed Data!");
											}
										} else {
											artDialog.alert("Can't get Data!");
										}
										NProgress.done();
										$(v).attr("disabled",false);
									},
									error: function(){
										//请求出错处理
										NProgress.done();
										artDialog.alert("Network Error,try again!");
										$(v).attr("disabled",false);
									},
									headers: {
										"X-HTTP-Method-Override": "GET"
									}
								});
							} else {
								artDialog.alert("Please check orders.");
								return;
							}
						}
					}
				}
			]
		});
		sv = new SearchView({send_psid:psId,resultView:resultView}).render();
		/*new CommonToolsView({"send_psid":psId,resultView:resultView}).render();
		$("#createOrder").bind('click', {}, function(event) {
			artDialog.open(config.createOrder.url, {title:"Create Order", width:'950px',height:'550px', lock:true, opacity:0.3, fixed:true});
		});
		$("#importOrder").bind('click', {}, function(event) {
			artDialog.open(config.importOrder.url, {title: "Import Order",width:'850px',height:'450px', lock: true,opacity: 0.3,fixed: true});
		});
		*/
		window.refreshWindow = function() {
			sv.filter();
		};
		NProgress.done();
	});
});