define(["jquery"], function($) 
{
		var ReadOnlyMap = function(idAttribute, dataArray)
		{
			this._map = {};
			var temp = $.extend(true,{},dataArray);
			this.initialize(idAttribute, temp);
		}

		ReadOnlyMap.prototype.initialize = function(idAttribute, array)
		{
			this._map = {}
			var temp = this;
			if(typeof array =="object" )
			{
				$.each(array, function(index, lineData) 
				{
					if(lineData && lineData[idAttribute])
					{
						temp._map[lineData[idAttribute]] = lineData;
					}
				});
			}
			return this;
		}
		ReadOnlyMap.prototype.get = function(id)
		{
			return this._map[id];
		},
		ReadOnlyMap.prototype.removeAll = function()
		{
			this._map = null;
			return this;
		}

		return ReadOnlyMap;
});