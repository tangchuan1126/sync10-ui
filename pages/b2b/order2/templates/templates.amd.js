define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['advanceSearch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"condition\">\n	<div class=\"row\" style=\"margin:2px\">\n		<div class=\"form-group col-sm-4\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Hub <font color=\"red\">*</font></div>\n				<input id=\"shipFrom\" type=\"text\" class=\"form-control min-100 immybox immybox_witharrow\" placeholder=\"Hub\" title=\"Please select From\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3 hide\"> \n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Ship To</div>\n				<input id=\"shipTo\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Ship To\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Status</div>\n				<input id=\"status\" type=\"text\" class=\"form-control min-200\" placeholder=\"Select Status\">\n			</div>\n		</div>\n		<div class=\"form-group col-md-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">ETD</div>\n				<input id=\"ETDMin\" type=\"text\" class=\"form-control min-80\" placeholder=\"Begin Date\">\n				<div class=\"input-group-addon\">~</div>\n				<input id=\"ETDMax\" type=\"text\" class=\"form-control min-80\" placeholder=\"End Date\">\n			</div>\n		</div>\n		<!--\n		<div class=\"form-group col-md-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">ETA</div>\n				<input id=\"ETAMin\" type=\"text\" class=\"form-control min-80\" placeholder=\"Begin Date\">\n				<div class=\"input-group-addon\">~</div>\n				<input id=\"ETAMax\" type=\"text\" class=\"form-control min-80\" placeholder=\"End Date\">\n			</div>\n		</div>\n		-->\n		<div class=\"form-group col-sm-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">FT</div>\n				<input id=\"payment\" type=\"text\" class=\"form-control min-200\" placeholder=\"Select Freight Term\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Creater</div>\n				<input id=\"creater\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select Creater\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Source</div>\n				<input id=\"source\" type=\"text\" class=\"form-control min-200\" placeholder=\"Select Source\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-4\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Customer <font color=\"red\">*</font></div>\n				<input id=\"customer\" type=\"text\" class=\"form-control min-100\" placeholder=\"Select Customer\" title=\"Please select customer\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Load Flag</div>\n				<input id=\"load_flag\" type=\"text\" class=\"form-control min-200\" placeholder=\"Select Load Flag\">\n			</div>\n		</div>\n		<div id=\"div_load_no\" class=\"form-group col-sm-3 hide\" style=\"display:none\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Load #</div>\n				<input id=\"load_no\" type=\"text\" class=\"form-control min-200\" value=\"\" placeholder=\"Input Load No.\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-4\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Retailer</div>\n				<input id=\"retailer\" type=\"text\" class=\"form-control min-100 immybox immybox_witharrow\" placeholder=\"Select Retailer\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-2\">\n			<button id=\"SearchTabBtn\" class=\"btn btn-default btn-info\" style=\"width:120px\">Search</button>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['commonTools'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"row\">\n	<div class=\"form-group col-md-6\">\n		<div class=\"input-group\">\n			<input id=\"Search_Key\" type=\"text\" class=\"form-control min-200\" data-ui-autocomplete=\"\">\n			<div class=\"input-group-btn\">\n				<button id=\"eso_search\" class=\"btn btn-info\" type=\"button\" style=\"width:120px\">Go!</button>\n			</div>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['self_work'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "					<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=helpers.helperMissing, alias2="function", alias3=helpers.blockHelperMissing, buffer = 
  "<table id=\"selfworkTable\" class=\"filter\">\n	<tr>\n		<td>\n			<select id=\"self_status\" class=\"form-control\">\n				<option value=\"0\">货物状态</option>\n";
  stack1 = ((helper = (helper = helpers.product_state || (depth0 != null ? depth0.product_state : depth0)) != null ? helper : alias1),(options={"name":"product_state","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.product_state) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_stock_in_set\" class=\"form-control\">\n				<option value=\"0\">运费流程</option>\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : alias1),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.stock_in_set) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td colspan=\"2\">\n			<div id=\"self_create_account_id\" style=\"width:200px\"></div>\n		</td>\n		<td>\n		</td>\n		<td>\n		</td>\n	</tr>\n	<tr>\n		<td>\n			<select id=\"self_declaration\" class=\"form-control\">\n				<option value=\"0\">出口报关</option>\n";
  stack1 = ((helper = (helper = helpers.declaration || (depth0 != null ? depth0.declaration : depth0)) != null ? helper : alias1),(options={"name":"declaration","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.declaration) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_clearance\" class=\"form-control\">\n				<option value=\"0\">进口清关</option>\n";
  stack1 = ((helper = (helper = helpers.clearance || (depth0 != null ? depth0.clearance : depth0)) != null ? helper : alias1),(options={"name":"clearance","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.clearance) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_invoice\" class=\"form-control\">\n				<option value=\"0\">发票流程</option>\n";
  stack1 = ((helper = (helper = helpers.invoice || (depth0 != null ? depth0.invoice : depth0)) != null ? helper : alias1),(options={"name":"invoice","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.invoice) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_drawback\" class=\"form-control\">\n				<option value=\"0\">退税流程</option>\n";
  stack1 = ((helper = (helper = helpers.drawback || (depth0 != null ? depth0.drawback : depth0)) != null ? helper : alias1),(options={"name":"drawback","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.drawback) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</select>\n		</td>\n		<td>\n			<button id=\"filter\" class=\"buttons big\">filter</button>\n		</td>\n	</tr>\n</table>";
},"useData":true});
return templates;
});