"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config){
	require(["jquery",
		"./js/view/advanceSearchView",
		"./js/view/commonToolsView",
		"oso.lib/table/js/table",
		"auto",
		"artDialog",
		"bootstrap",
		"domready!"
	],
	function($,SearchView,CommonToolsView,Table,auto,artDialog) {
		var psId="1000005";
		var queryOptions = {el: "#data", url:config.searchDefaultURL.url,queryCondition:{"send_psid":psId,pageSize:10}};
		
		var resultView = new Table({
			container : '#data',
			//url: config.searchDefaultURL.url,
			title : 'Order List',
			height: auto,
			rownumbers : false,
			checkbox: true,
			singleSelection : false,
			pagination: true,
			datasName: 'DATA',
			pageSizeName: 'pageSize',
			pageNumberName: 'pageNo',
			totalName: 'PAGECTRL.allCount',
			pageList: ["10","20","30","40","50","100","200","500","1000"],
			columns:[
				{field:'B2B_OID',title:'Order ID',width:50,hidden:true},
				{field:'CUSTOMER_DN',title:'CustomerDN',width:50},
				{field:'RETAIL_PO',title:'Retail PO',width:50},
				{field:'SEND_PSNAME',title:'Ship From',width:100},
				{field:'MABD',title:'MABD',width:100},
				{field:'RECEIVE_PSNAME',title:'Ship To',width:100},
				{field:'CUSTOMER_NAME',title:'customer',width:100}
			],
			toolbar: [{
					text : '验证库存',
					id: 'check',
					iconCls : 'add',
					handler : function() {
						var wid = "";
						var rows = resultView.getSelected();
						var ids = [];
						var bids = [];
						if (rows) {
							for (var i=0;i<rows.length;i++) {
								if (i==0) wid = rows[i].COMPANY_ID;
								if (rows[i].CUSTOMER_DN) {
									ids.push(''+rows[i].CUSTOMER_DN);
									bids.push(rows[i].B2B_OID);
								}
								if (rows[i].COMPANY_ID!=wid) {
									artDialog.alert("please select Ship From for search.");
									return;
								}
							}
							if (bids.length>0) {
								$.ajax({
									type: "POST",
									url: config.addTransport.url,
									data: {data: JSON.stringify(bids), action:"addTransport"},
									timeout: 30000,
									datatype: "json",
									success: function(data,textStatus) {
										//成功返回之后调用的函数
									},
									error: function(){
										artDialog.alert("Can't Create BOL!");
									},
									headers: {
										"X-HTTP-Method-Override": "GET"
									}
								});
							} else {
								artDialog.alert("please checked Order List.");
								return;
							}
						}
					}
				}
			]
		});
		var sv = new SearchView({"send_psid":psId,resultView:resultView}).render();
		new CommonToolsView({"send_psid":psId,resultView:resultView}).render();
		$("#createOrder").bind('click', {}, function(event) {
			artDialog.open(config.createOrder.url, {title:"Create Order", width:'950px',height:'550px', lock:true, opacity:0.3, fixed:true});
		});
		$("#importOrder").bind('click', {}, function(event) {
			artDialog.open(config.importOrder.url, {title: "Import Order",width:'850px',height:'450px', lock: true,opacity: 0.3,fixed: true});
		});
	});
});