define(["JSONTOHTML","dateUtil","oso.lib/fieldsetControls/fieldset","./KeyUtil","../key/TransportOrderKey","../key/FundStatusKey"], 
	function(JSONTOHTML,DateUtil,fieldset,Key,TransportOrderKey,FundStatusKey) {
	//第一次创建时带上表头
	var types = ["basicInfo","transportInfo","assert","LOGS"];
	var _table = {
		thead:
		[
			{
				tag: "table",
				class: "checkin_box",
				children: [
					{
						tag: "thead",
						children: [{
							tag: "tr",
							html: function(obj, index){
								var thdata = obj.children[0].children;
								return (JSONTOHTML.transform(thdata.children, _table.th));
							}
						}]
					},
					{
						tag: "tbody",
						html: ""
					}
				]
			},
			{
				tag: "div",
				class: "tfootpage",
				html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
			},
			{
				tag: "div",
				class: "SelectMorebox"
			}
		],
		th: {
			tag: "th",
			html: "${title}",
			style: function(obj, index) {
				if (index==3) {
					return "width:22%";
				} else if (index==2) {
					return "width:22%";
				} else{
					return "";
				}
			}
		}
	};
	
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var tdID = "B2B_OID";
	var fsid = [];
	var _tbody = {
		//初始化，将json数据分类：分为对应的行和列
		AUTHS:[],
		fieldsets:[],
		int: function(jsons,View_control) {
			//数据转换成表格形式
			//["tr":"index","children":[{},{}]]
			_tbody.AUTHS = {};
			var headallobj = View_control.head;
			var footer = View_control.footer;
			var pageSize=jsons.PAGECTRL.pageSize;
			//按表头转换成行和列
			var __trhtml = "";
			//获得权限
			_.map(jsons.AUTHS,function(auth,key) {
				//
				_tbody.AUTHS[auth.B2B_OID] = auth;
			});
			for (var tri = 0; tri < jsons.DATA.length; tri++) {
				var tritme = jsons.DATA[tri];
				//_.map(_tbody.AUTHS,function(obj,key){});
				var __td = [];
				for (var headkey in headallobj) {
					__td.push(tritme);
				}
				//显示行
				var __tr=[];
				__tr.push({"trid":tritme.B2B_OID,"children": __td});
				__trhtml += (JSONTOHTML.transform(__tr[0], _tbody.tr));
				//按钮行
				__tr=[];
				__tr.push({"trid":tritme.B2B_OID,"colspan":headallobj.length,"children": footer,"linedata":tritme});
				__trhtml += (JSONTOHTML.transform(__tr[0], _tbody.trfooter));
				if(tri >= pageSize) {
					break;
				}
			}
			return __trhtml;
		},
		tr: {
			"tag": "tr",
			"data-itmeid":function(obj){
				return tdID+"|"+obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				//自由列数console
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tdID+"|"+obj.trid+'" class="buttons-group">';
				var ahtml = "";
				var id = obj.trid;
				for (var akey in obj.children){
					var itmea = obj.children[akey];
					if(_tbody.isHasButton(itmea[0],obj.trid,_tbody.AUTHS[obj.trid])) {
						ahtml += '<a href="javascript:void(0)" data-eventname="'+ itmea[0] +'" class="buttons">' + itmea[1] + '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		isHasButton:function(itema,trid,AUTHS) {
			var res = false;
			if (AUTHS) {
				if(!AUTHS[tdID]) return false;
				if(trid==AUTHS[tdID]){
					if (itema=="events.CLOSE") {
						res = AUTHS["BTN_CLOSE"];
					} else if (itema=="events.ADDDETAIL") {
						res = AUTHS["BTN_ADDDETAIL"];
					} else if (itema=="events.CREATEAPPOINTMENT") {
						res = AUTHS["BTN_CREATEAPPMT"];
					} else if (itema=="events.CREATEBOL") {
						res = AUTHS["BTN_CREATEBOL"];
					} else {
						res = false;
					}
				}
			}
			return res;
		},
		basicInfo:
		{
			"tag": "fieldset", 
			"class": "TRAILER", 
			"children": [
				{
					"tag": "span", 
					"class": "fieldset_tfoot", 
					"children": [
						{
							"tag": "em", 
							"html": "总容器：${CONTAINER_COUNT}"
						}
					]
				},{
					"tag": "legend",
					"class": "td_legend",
					"children": [
						{
							"tag": "span", "data-eventname": "transportDetail", "class": function (obj,index) {
								if (_tbody.emptyHandle(obj.PURCHASE_ID,"0") > 0) {
									return "valuestyle2 buttonallevnts";
								}
								return "valuestyle buttonallevnts";
							}, "html": "T${TRANSPORT_ID}"
						},
						{
							"tag": "span",
							"class": function(obj,index) {
								if (obj.TRANSPORT_STATUS=="5") {
									return "status color-green";
								} else if (obj.TRANSPORT_STATUS=="2") {
									return "status color-blue";
								} else if (obj.TRANSPORT_STATUS=="3") {
									return "status color-red";
								} else if (obj.TRANSPORT_STATUS=="12") {
									return "status color-5B4B00";
								} else {
									return "status";
								}
							}, 
							"html": function (obj,index){ return TransportOrderKey[obj.TRANSPORT_STATUS][DateUtil.getLanguage()];}
						}
					]
				},
				{
					"tag": "div",
					"class": "Tractor_ulitme",
					"children": [
						{
							"tag": "ul",
							"children": [
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "创建人:"},
										{"tag": "span", "class": "valstyle", "html": "${CREATE_ACCOUNT}"}
									]
								},
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "允许装箱:"},
										{"tag": "span", "class": "valstyle", "html": "${PACKING_ACCOUNT_NAME}"}
									]
								},
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "创建时间:"},
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.TRANSPORT_DATE);}}
									]
								},
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "更新时间:"}, 
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.UPDATEDATE);}}
									]
								},
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "ETD:"},
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.TRANSPORT_OUT_DATE);}}
									]
								},
								{
									"tag": "li",
									"children": [
										{"tag": "span", "class": "namestyle", "html": "ETA:"},
										{"tag": "span", "class": "valstyle", "html": function(obj,index){ return DateUtil.formatDate(obj.TRANSPORT_OUT_DATE);}}
									]
								}
							]
						}
					]
				}
			]
		},
		transportInfo:
		{
			"tag":"div",
			"class":"Log_container",
			"children":[
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"收货仓库:"},{"tag":"span","class":"valstyle","html":"${RECEIVE_PSNAME}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"提货仓库:"},{"tag":"span","class":"valstyle","html":"${SEND_PSNAME}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"收货人:"},{"tag":"span","class":"valstyle","html":"${A}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"收货时间:"},{"tag":"span","class":"valstyle","html":"${DD}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"入库人:"},{"tag":"span","class":"valstyle","html":"${A}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"入库时间:"},{"tag":"span","class":"valstyle","html":"${A}"}]}]},
				{"tag":"div","class":"box1px","html":""},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"运单号:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_WAYBILL_NUMBER}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"货运公司:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_WAYBILL_NAME}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"承运公司:"},{"tag":"span","class":"valstyle","html":"${CARRIERS}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"始发国/始发港:"},{"tag":"span","class":"valstyle","html":function(obj,index){return _tbody.emptyHandle(obj.TRANSPORT_SEND_COUNTRY_NAME,"无")+"/"+_tbody.emptyHandle(obj.TRANSPORT_SEND_PLACE,"无");}}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"目的国/目的港:"},{"tag":"span","class":"valstyle","html":function(obj,index){return _tbody.emptyHandle(obj.TRANSPORT_RECEIVE_COUNTRY_NAME,"无")+"/"+_tbody.emptyHandle(obj.TRANSPORT_RECEIVE_PLACE,"无");}}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"总体积:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_VOLUME} ${TRANSPORT_VOLUME_UNIT}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"总重量:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_WEIGHT} ${TRANSPORT_WEIGHT_UNIT}"}]}]},
				{"tag":"ul","children":[{"tag":"li","children":[{"tag":"span","class":"namestyle","html":"总金额:"},{"tag":"span","class":"valstyle","html":"${TRANSPORT_SUM_PRICE} ${TRANSPORT_SUM_PRICE_UNIT}"}]}]}
			]
		},
		_applyMoneyLi:
		{
			tag:"li",children:
			[
				{"tag":"span","class":"",children:[
					{"tag":"a","class":" buttonallevnts","data-eventname":"events.goApplyFunds","data-val":"${APPLY_ID}","html":"T${APPLY_ID}"},
				]},
				{"tag":"span","class":"","html":function(obj,index){return "&nbsp;"+Key.get(FundStatusKey,obj.STATUS)+"&nbsp;";}},
				{"tag":"span","class":"","html":function(obj,index){
					var freightCostStr ="";
					if(obj.CURRENCY !== "RMB")
					{
						freightCostStr ="/"+obj.STANDARD_MONEY+"RMB";
					}
					return obj.AMOUNT+obj.CURRENCY+freightCostStr}},
			]
		},
		_applyMoneyUl:
		{
			tag:"ul",html:function(obj,index)
			{
				var html ="";
				if(obj.APPLYMONEYS && obj.APPLYMONEYS.length > 0)
				{
					var applyMoneys = obj.APPLYMONEYS;
					html = JSONTOHTML.transform(applyMoneys,_tbody._applyMoneyLi);
					if(applyMoneys.APPLYTRANSFERROWS && applyMoneys.APPLYTRANSFERROWS.length > 0) {
						html += JSONTOHTML.transform(applyMoneys.APPLYTRANSFERROWS,_tbody._transferMoneyLi);
					}
				}
				return html;
			}
		},
		assert:
		{
			tag: "fieldset",
			class: "stockInSet",
			children:
			[
				{tag:"legend",children:
				[
					{"tag":"span","class":"","html":"运费:"},
 					{"tag":"span","class":"valstyle","html":"${TRANSPORT_SUM_PRICE} ${TRANSPORT_SUM_PRICE_UNIT}"}
				]},
				{tag:"div",class:"applyMoney Log_container","html":function(obj,index){return JSONTOHTML.transform(obj,_tbody._applyMoneyUl);}}
			]
		},
		LOGS:
		{
			tag: "div",
			class: "Log_container",
			html: function(obj, index) {
				var htmls = JSONTOHTML.transform(obj.LOGS, _tbody.LogItem);
				var ob = {};
				if (obj.LOGS.length>=3) {
					htmls += JSONTOHTML.transform(ob, _tbody.LogMore);
				}
				return htmls
			}
		},
		LogItem:
		{
			"tag":"ul",
			"children":[
				{
					"tag":"li",
					"class": function(obj, index) { if (index>0){return "box1px";} },
					"html":""
				},
				{"tag":"li","children":[
					{"tag":"span","class":"namestyle color-orange","html":"${FOLLOWUPTYPE}:"},
					{"tag":"span","class":"valstyle","html":"${ADMINNAME}"},
					{"tag":"span","class":"logtime text-right","html":function(obj,index){ return DateUtil.shortFormatDate(obj.TRANSPORT_DATE);}}
				]},
				{"tag":"li","children":[{"tag":"span","class":"logcontent max-p99","html":"${TRANSPORT_CONTENT}"}]}
			]
		},
		LogMore:
		{
			"tag":"span","class":"valuestyle buttonallevnts","data-eventname":"events.moreLogs","children":[{"tag":"a","class":"more","html":"更多"}]
		},
		td: {
			tag: "td",
			class: function(obj, index) {
				//单元格样式名
				var classname = "";
				var keynames = types[index];
				if("basicInfo" == keynames) {
					classname = "td_" + "TRAILER";
				} else {
					classname = "td_" + keynames;
				}
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "inherit";
				}
				return valignsing;
			},
			html: function(obj, index) {
				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = types[index];
				if (keynames != "" && typeof keynames != "undefined") {
					if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
					}
				}
				return retuhtml;
			}
		},
		//====================================工具方法===================================//
		//处理空字符串
		emptyHandle:function(str,defaultValue) {
			if(typeof str !="undefined" && (""+str) !="")
			{
				return str;
			} else {
				if(typeof str =="undefined") return "";
				return defaultValue;
			}
		},
		//更新一条数据
		updatatr: function(jsondata, trid, View_control) {
			var __td = [];
			var headallobj = View_control.head;
			for (var headkey in headallobj) {
				__td.push(jsondata.DATA[0]);
			}
			var strid = "'"+tdID+"|"+trid+"'";
			//TR Data
			$("tr[data-itmeid="+strid+"]").html(JSONTOHTML.transform(__td, _tbody.td));
			var ahtml = "";
			var footer = View_control.footer;
			for (var akey in footer){
				var itmea = footer[akey];
				if(_tbody.isHasButton(itmea[0],trid,jsondata.AUTHS[0])) {
					ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
				}
			}
			$("div[data-itmeid="+strid+"]").html(ahtml);
		}
	}
	return {
		tbody: _tbody,
		table: _table
	}
});