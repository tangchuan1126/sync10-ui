define(["JSONTOHTML",
	"dateUtil",
	"./Map",
	"./KeyUtil",
	"../key/TransportOrderKey",
	"../key/ClearanceKey",
	"../key/DeclarationKey",
	"../key/TransportCertificateKey",
	"../key/TransportStockInSetKey",
	"../key/FundStatusKey",
	"../key/B2BOrderLogTypeKey"
	], function(JSONTOHTML,
		DateUtil,
		Map,
		Key,
		TransportOrderKey,
		ClearanceKey,
		DeclarationKey,
		TransportCertificateKey,
		TransportStockInSetKey,
		FundStatusKey,
		B2BOrderLogTypeKey
		) {

	//第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index)
					{
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}",
			style: function(obj, index) {
				if (index==3) {
					return "width:22%";
				} else if (index==2) {
					return "width:22%";
				} else{
					return "";
				}
			}
		}

	};
	
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		int: function(jsons,View_control) 
		{
			//try {
				var start = new Date().getTime();
				//初始化全局配置
				_tbody.idAttribute = View_control.idAttribute;
				_tbody.authIdAttribute = View_control.authIdAttribute;
				_tbody.headallobj = View_control.head;
				_tbody.footer = View_control.footer;
				var data = new Map(_tbody.idAttribute,jsons.DATA);
				var auths = new Map(_tbody.authIdAttribute,jsons.AUTHS);
				var pageSize=jsons.PAGECTRL.pageSize;
				//按表头转换成行和列
				var __tr=[];
				for (var tri = 0; tri < jsons.DATA.length; tri++) 
				{
					var rowData = jsons.DATA[tri];
	                var __td = [];
					for (var headkey in _tbody.headallobj) 
					{
						var column = _tbody.headallobj[headkey];
						var columnFiled = column.field;
						var columnData = eval("({" + columnFiled + ":rowData})");
						__td.push(columnData);
					}
					__tr.push({"rowId":rowData[_tbody.idAttribute],"children": __td});
					if(tri >= pageSize)
					{
						break;
					}
				}
				//拼装tr,trfooter，
				var __rowsHtml = "";
				for (var trkey in __tr)
				{
					var _row = __tr[trkey];
					var  rowId = _row.rowId;
					__rowsHtml += (JSONTOHTML.transform(_row, _tbody.tr));
					var trfooterdata = {
						"colspan": _tbody.headallobj.length,
						"children": _tbody.footer,
						"rowId":rowId,
						"rowData":data.get(rowId),
						"auth":auths.get(rowId)
						};
					//将行值传给按钮行，用于判断按钮是否需要显示
					__rowsHtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
				}
				data = null;
				auths = null;
				var end = new Date().getTime();
				//console.log(end-start);
				return __rowsHtml;
			/*}catch(e)
			{
				alert(e);
				alert("数据解析异常，请联系管理员");
			}*/
		},
		keyname: function(obj) 
		{
			//列对象，只有一个key,那就是列名对应的字段
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id":function(obj)
			{
				return obj.rowId;
			},
			//行对象ID
			"data-itmeid":function(obj){
				return _tbody.idAttribute+"|"+obj.rowId;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				//chuldren :列数组[]
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			"id":function(obj)
			{
				return "foot_"+obj.rowData[_tbody.idAttribute];
			},
			class: "TFOOTbutton",
			html: function(obj, index) {
				var rowData = obj.rowData,
					auth = obj.auth,
					tfootData = _tbody.idAttribute+"|"+obj.rowId,
					trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tfootData+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) 
				{
					var itmea = obj.children[akey];
					if(_utils.isHasButton(itmea[0],auth))
					{
						var name = itmea[1];
						var calss="";
						if(itmea[0] =="events.followup")
						{
							name = itmea[1]+Key.get(TransportOrderKey,rowData.TRANSPORT_STATUS);
						}
						if(itmea[0].indexOf("delete"))
						{
							calss = " icon remove";
						}
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons'+calss+'">' + name+ '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		td: {
			tag: "td",
			class: function(obj, index) {
				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				if("basicInfo" == keynames)
				{
					classname = "td_" + "TRAILER";
				}
				else
				{
					classname = "td_" + keynames;
				}
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "inherit";
				}
				return valignsing;
			},
			html: function(obj, index) {
				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);
				if (keynames != "" && typeof keynames != "undefined")
				{
					if(typeof keynames == "string")
					{
						if (typeof cellTemplate[keynames] != "undefined" && cellTemplate[keynames] != "") 
						{
							retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
						}
					}
					else if(typeof keynames == "object")
					{
						retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
					}
				}
				return retuhtml;
			}
		},
		updateOneRow:function(parentId,rowData,auths)
		{
			var __td = [];
			for (var headkey in this.headallobj) 
			{
				var column = this.headallobj[headkey];
				var columnFiled = column.field;
				var columnData = eval("({" + columnFiled + ":rowData})");
				__td.push(columnData);
			}
			var trfooterdata = {
					"colspan": this.headallobj.length,
					"children": this.footer,
					"rowId":parentId,
					"rowData":rowData,
					"auth":auths
					};
			$("#foot_"+parentId).remove();
			$("#"+parentId).empty().html(JSONTOHTML.transform(__td,this.td)).after(JSONTOHTML.transform(trfooterdata,this.trfooter));
		}
	}
	var cellTemplate =
	{
		basicInfo:
		{
			tag: "fieldset",
			class: "TRAILER",
			children: [
			{
				tag:"span",
				class:"fieldset_tfoot",
				html:function(obj,index)
				{
					return "";//"<em>总容器："+obj.CONTAINER_COUNT+"</em>";
				}
			},
			{
				tag: 'legend',
				class: "td_legend",
				children:
				[
					{"tag":"span","class":"lightlink buttonallevnts","data-eventname":"transportDetail","html":"B${B2B_OID}"},
					{
						"tag":"span",
						"class":function(obj,index){return _utils._stateClass( obj.B2B_ORDER_STATUS)},
						"html":function(obj,index){return Key.get(TransportOrderKey,obj.B2B_ORDER_STATUS);}
					}
				]
			}, 
			{
				 tag: "div",
				 class: "Tractor_ulitme",
				 children: 
				 [
					{tag: "ul",children:
					[
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Customer:"},
									{"tag":"span","class":"valstyle","html":"${CUSTOMER_ID}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Customer DN:"},
									{"tag":"span","class":"valstyle","html":"${CUSTOMER_DN}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Retail PO:"},
									{"tag":"span","class":"valstyle","html":"${RETAIL_PO}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Freight:"},
									{"tag":"span","class":"valstyle","html":"${FREIGHT_TERM}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Appointment:"},
									{"tag":"span","class":"valstyle","html":"${CUSTOMER_ORDER_DATE}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"ETD:"},
									{"tag":"span","class":"valstyle","html":"${ETD}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Creater:"},
									{"tag":"span","class":"valstyle","html":"${CREATE_ACCOUNT}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"允许装箱:"},
									{"tag":"span","class":"valstyle","html":"${PACKING_ACCOUNT}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Create Date:"},
									{"tag":"span","class":"valstyle","html":"${B2B_ORDER_DATE}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"更新时间:"},
									{"tag":"span","class":"valstyle","html":"${UPDATEDATE}"}
								]
							}
					]}	
				 ]
			}
			]
		},
		transportInfo: 
		{
				 tag: "div",
				 class: "Log_container",
				 children:
				 [
				 	{tag:"ul",children:
				 	[
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"提货仓库:"},
 							{"tag":"span","class":"valstyle","html":"${SEND_PSNAME}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"收货仓库:"},
 							{"tag":"span","class":"valstyle","html":"${RECEIVE_PSNAME}"}
				 		]},
						/*
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"收货人:"},
 							{"tag":"span","class":"valstyle","html":"${DELIVERYER_ID}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"收货时间:"},
 							{"tag":"span","class":"valstyle","html":"${DELIVERYED_DATE}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"入库人:"},
 							{"tag":"span","class":"valstyle","html":"${WAREHOUSINGER_ID}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"入库时间:"},
 							{"tag":"span","class":"valstyle","html":"${WAREHOUSING_DATE}"}
				 		]},
						*/
				 		{tag:"div",class:"box1px",html:""},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"运单号:"},
 							{"tag":"span","class":"valstyle","html":"${B2B_ORDER_WAYBILL_NUMBER}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"货运公司:"},
 							{"tag":"span","class":"valstyle","html":"${B2B_ORDER_WAYBILL_NAME}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"承运公司:"},
 							{"tag":"span","class":"valstyle","html":"${CARRIERS}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"始发 City/State:"},
 							{"tag":"span","class":"valstyle","html":function(obj,index){
								return _utils.handleEmpty(obj.SEND_CITY,"无")+"/"+_utils.handleEmpty(obj.ADDRESS_STATE_SEND,"无");}}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"目的 City/State:"},
 							{"tag":"span","class":"valstyle","html":function(obj,index){
								return _utils.handleEmpty(obj.DELIVER_CITY,"无")+"/"+_utils.handleEmpty(obj.ADDRESS_STATE_DELIVER,"无");}}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"总体积:"},
 							{"tag":"span","class":"valstyle","html":"${B2B_VOLUME} ${B2B_VOLUME_UNIT}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"总重量:"},
 							{"tag":"span","class":"valstyle","html":"${B2B_WEIGHT} ${B2B_WEIGHT_UNIT}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"总金额:"},
 							{"tag":"span","class":"valstyle","html":"${B2B_SUM_PRICE} ${B2B_SUM_PRICE_UNIT}"}
				 		]},
				 	]}
				 ]
		},
		assert:
		{
			tag: "fieldset",
			class: "stockInSet",
			children:
			[
				{tag:"legend",children:
				[
					{"tag":"span","class":"","html":"运费:"},
 					{"tag":"span","class":"valstyle","html":"${TRANSPORT_SUM_PRICE} ${TRANSPORT_SUM_PRICE_UNIT}"}
				]},
				{tag:"div",class:"applyMoney Log_container","html":function(obj,index){return JSONTOHTML.transform(obj,cellTemplate._applyMoneyUl);}}
			]
		},
		_applyMoneyUl:
		{
			tag:"ul",html:function(obj,index)
			{
				var html ="";
				if(obj.APPLYMONEYS && obj.APPLYMONEYS.length > 0)
				{
					var applyMoneys = obj.APPLYMONEYS;
					html = JSONTOHTML.transform(applyMoneys,cellTemplate._applyMoneyLi);
					if(applyMoneys.APPLYTRANSFERROWS && applyMoneys.APPLYTRANSFERROWS.length > 0)
					{
						html += JSONTOHTML.transform(applyMoneys.APPLYTRANSFERROWS,cellTemplate._transferMoneyLi);
					}
				}
				return html;
			}

		},
		_applyMoneyLi:
		{
			tag:"li",children:
			[
				{"tag":"span","class":"",children:[
					{"tag":"a","class":" buttonallevnts","data-eventname":"events.goApplyFunds","data-val":"${APPLY_ID}","html":"T${APPLY_ID}"},
				]},
				{"tag":"span","class":"","html":function(obj,index){return "&nbsp;"+Key.get(FundStatusKey,obj.STATUS)+"&nbsp;";}},
				{"tag":"span","class":"","html":function(obj,index){
					var freightCostStr ="";
					if(obj.CURRENCY !== "RMB")
					{
						freightCostStr ="/"+obj.STANDARD_MONEY+"RMB";
					}

					return obj.AMOUNT+obj.CURRENCY+freightCostStr}},
			]
		},
		_transferMoneyLi:
		{
			tag:"li",children:
			[
				{"tag":"span","class":"",children:[
					{"tag":"a","class":" buttonallevnts","data-eventname":"events.goFundsTransferListPage","data-val":"${TRANSFER_ID}","html":"W${TRANSFER_ID}"},
				]},
				{"tag":"span","class":"","html":function(obj,index){return "&nbsp;"+Key.get(FundStatusKey,obj.STATUS)+"&nbsp;";}},
				{"tag":"span","class":"","html":function(obj,index){
					var freightCostStr ="";
					if(obj.CURRENCY !== "RMB")
					{
						freightCostStr ="/"+obj.STANDARD_MONEY+"RMB";
					}

					return obj.AMOUNT+obj.CURRENCY+freightCostStr}},
			]
		},
		FLOWS: 
		{
			tag: "div",
			class: "Log_container flows",
			html:function(obj,index)
			{
				var html = "" ;
				var box1px = "<div class='box1px'></div>";
				var length = obj.FLOWS.length;
				_.map(obj.FLOWS,function(flow,index)
					{
						var fontClass = _utils._flowClass(flow,obj);
						var  res = _utils._flowState(flow,obj);

						//设置显示状态
						flow.STATUS = "<font class='"+fontClass+"'  >"+res.STATUS+"</font>";
						flow.OVER=res.OVER;
						//设置完成天数
						html+= JSONTOHTML.transform(flow,cellTemplate._ulFlows);
						if(index != length - 1)
						{
							html+= box1px;
						}
					});
				return html;
			}
		},
		_ulFlows:
		{
			tag:"ul",
			class:"flows",
			children:
			[
				{tag:"li", children:
				[
					{tag:"span",class:"namestyle",html:"${TYPENAME}:"},
					{tag:"span",class:"valstyle",html:"${STATUS}"},
					{tag:"span",class:"rightstyle",html:"${OVER}"}
				]},
				{tag:"li", children:
				[
					{tag:"span",class:"flowname",html:"${EMPLOYE_NAMES}"}
				]}
			]
		},
		_liList:
		{
			tag: "li",
			html: function(obj, index) 
			{
				return '<span class="namestyle">'+obj.TYPE+':</span><span class="valstyle">' + obj.EMPLOYE_NAMES + '</span>';
			}
		},
		LOGS: 
		{
			tag: "div",
			class: "Log_container",
			html: function(obj, index) 
			{
				var ulsring = "";
				var arryjson = obj.LOGS;
				var box1px = "<div class='box1px'></div>";
				if (arryjson) {
					for (var _key = 0; _key < arryjson.length; _key++)
					{
						var arryitmes = arryjson[_key];
						var ul = JSONTOHTML.transform(arryitmes,cellTemplate._ulLogs);
						if (ulsring != "" && typeof ulsring != "undefined") 
						{
							ulsring = ulsring + box1px + ul;
						} else {
							ulsring = ul;
						}
						if (_key == 2) 
						{
							ulsring = ulsring +'<span class=" buttonallevnts" data-eventname="events.moreLogs"><a class="more">更多</a></span>';
							break;
						}
					}
				}
				return ulsring;
			}
		},
		_ulLogs:
		{
			tag:"ul",
			children:
			[
				{tag:"li",children:[
					{tag:"span",class:"logvaluestyle",html: function(obj,index) {
							return B2BOrderLogTypeKey[obj.B2B_TYPE][DateUtil.getLanguage()]+":";
							//"${B2B_TYPE}:&nbsp;&nbsp;"
						}
					},
					{tag:"span",class:"",html:"${B2BER}"},
					{tag:"span",class:"rightstyle ",html:"${B2B_DATE}"},
				]},
				{tag:"li",children:[
					{tag:"span",class:"",html:"${B2B_CONTENT}"}
				]}
			]
		},
	}
	var _utils = 
	{
		handleEmpty:function(str,defaultValue)
		{
			if(str == "" || str == undefined || str.length == 0 || str == "null" || str == null)
			{
				return defaultValue;
			}
			else
			{
				return str;
			}
		},
		isHasButton:function(itmea,auth)
		{
			var res = false;
			if(itmea =="events.followup")
			{
				res = auth["BUTTON_FOLLOWUP"];
			}
			else if(itmea =="events.STOP")
			{
				res = auth["BUTTON_STOP"];
			}
			else if(itmea =="events.DECLARATION")
			{
				res = auth["BUTTON_DECLARATION"];
			}
			else if(itmea =="events.CLEARANCE")
			{
				res = auth["BUTTON_CLEARANCE"];
			}
			else if(itmea =="events.STOCK_IN_SET")
			{
				res = auth["BUTTON_STOCK_IN_SET"];
			}
			else if(itmea =="events.CERTIFICATE")
			{
				res = auth["BUTTON_CERTIFICATE"];
			}
			else
			{
				res = false;
			}
			res=true;
			return res;
		},
		_stateClass:function(state)
		{
			//运输流程
			var tempclass="";
			if(state == "2")
			{
				tempclass=" colorbule";
			}
			else if(state =="3")
			{
				tempclass=" colorred";
			}
			else if (state =="5")
			{
				tempclass=" colorgreen";
			}
			else
			{
				tempclass="";
			}
			tempclass = tempclass +" status";
			return tempclass;
		},
		//获取显示颜色
		_flowClass:function(flow,obj)
		{
			var type = flow.TYPE;
			var state = flow.STATUS
			var cls = "fontBold ";
			if(type == "5")
			{
				//运费 流程，判断 STOCK_IN_SET_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.STOCK_IN_SET_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "5")//运费完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "6")
			{
				//进口清关 CLEARANCE_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.CLEARANCE_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//清关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需清关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type =="7")
			{
				//出口报关DECLARATION_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.DECLARATION_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//报关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需报关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "9")
			{
				//单证 CERTIFICATE_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.CERTIFICATE_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//报关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需报关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "4")
			{
				cls =_utils._stateClass(state);
			}
			return cls;
		},
		_calTime:function(over,obj)
		{
				var res = "";
				if(over !="" && over != undefined && over != "undefined")
				{
					res = over+"天完成";
				}
				else
				{
					res = DateUtil.getDiffDate(obj.TRANSPORT_DATE,"dd")+"天";
				}
				
				return res;
		},
		//获取责任方
		_flowState:function(flow,obj)
		{
			var result = {};
			var calTime = _utils._calTime;
			var res ={"1":"(发货方)","2":"(收货方)"};
			switch(parseInt(flow.TYPE))
			{
				case 4://货物状态
					  result.OVER = calTime(obj.ALL_OVER,obj);
					  result.STATUS = Key.get(TransportOrderKey,flow.STATUS);
					   break;
				case 5://运费
					 result.OVER = calTime(obj.STOCK_IN_SET_OVER,obj); 
					 result.STATUS = res[obj.STOCK_IN_SET_RESPONSIBLE]+Key.get(TransportStockInSetKey,flow.STATUS);
					 break;
				case 6://清关
					  result.OVER = calTime(obj.CLEARANCE_OVER,obj);
					  result.STATUS= res[obj.CLEARANCE_RESPONSIBLE]+Key.get(ClearanceKey,flow.STATUS);
					 break;
				case 7://报关
					  result.OVER = calTime(obj.DECLARATION_OVER,obj);
					  result.STATUS= res[obj.DECLARATION_RESPONSIBLE]+Key.get(DeclarationKey,flow.STATUS);
					 break;
				case 9://单证
					  result.OVER = calTime(obj.CERTIFICATE_OVER,obj);
					  result.STATUS= res[obj.CERTIFICATE_RESPONSIBLE]+Key.get(TransportCertificateKey,flow.STATUS);
					 break;
				default:
					 result.OVER="0.0";
					 result.STATUS="";
			}
			return result;
		},

	}
	
	return {
		tbody: _tbody,
		table: _table
	}

});