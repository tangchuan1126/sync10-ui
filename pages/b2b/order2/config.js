define({
	addTransport:
	{
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action"
	},
	createOrder:
	{
		//url:"/Sync10-ui/pages/bol/bol_add/index.html"
		url:"/Sync10-ui/pages/b2b/b2b_create/index.html"
	},
	order:
	{
		url:"/Sync10-ui/pages/b2b/b2b_detail/index.html"
	},
	loadType:
	{
		url: "./json/load.json"
	},
	commitDN:
	{
		url:"/Sync10/action/administrator/product/inventoryCommitmentAction.action"
	},
	importOrder:
	{
		url:"/Sync10-ui/pages/b2b/order_import/importIndex.html"
	},
	//-------------------------------------------------//
	customer:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllCustomerId"
	},
	retailer:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getRetailers"
	},
	source:
	{
		url:"./json/source.json"
	},
	payment:
	{
		url:"./json/payment.json"
	},
	shipFrom:
	{
		//url:"./json/shipFrom.json"
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	shipTo:
	{
		//url:"./json/shipTo.json"
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	filterTransportion:
	{
		 url:"/Sync10-ui/pages/temp/bol.json"
	},
	keySearchURL:
	{
		url: "/Sync10-ui/pages/b2b/order/json/do.json?action=fillterB2BOrder"
	},
	searchDefaultURL:
	{
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getWmsOrder"
	},
	filterSelfWork:
	{
		url:"/Sync10-ui/pages/b2b/order/json/do.json"
	},
	autoComplete:
	{
		url: "/Sync10/action/administrator/b2b_order/B2BOrderGetSearchJSONAction.action"
	},
	adminTree:
	{
		//url:"/Sync10/_b2b/admin/"
		url: "./json/admin.json"
	},
	session:
	{
		url:"/Sync10/_b2b/admin/session"
	}
});