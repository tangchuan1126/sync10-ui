define(["./jsontohtml_templates"], function(tmp) {
	return {
		"View_control": {
			"head": [
				{
					"title": "订单基本信息",
					"field": "basicInfo"
				}, {
					"title": "库房及运输信息",
					"field": "transportInfo"
				}, {
					"title": "资金情况",
					"field": "assert"
				},
				//{
				//	"title": "流程信息",
				//	"field": "FLOWS"
				//},
				{
					"title": "跟进",
					"field": "LOGS"
				}
			],
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"idAttribute":"B2B_OID",
			"authIdAttribute":"ID",
			"footer": [
				[
					"events.delete", "删除"
				]
			],
			"templates": tmp
		}
	}
});