"use strict";
define(
	["jquery","bootstrap","metisMenu","CompondList/View","./view_config","./jsontohtml_templates","artDialog"],
	function($,bootstrap,metisMenu,CompondList,view_config,Template)
	{
		return  function(options)
		{
			var list  = new CompondList(view_config, {
				renderTo: options.el,
				dataUrl: options.url,
				PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
				//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
			});
			//绑定事件
			list.render();
			list.on("transportDetail",function(e) {
				var B2B_OID = e.data.B2B_OID;
				var uri = "/Sync10-ui/pages/b2b/b2b_detail/index.html?B2B_OID="+B2B_OID;
				window.open(uri);
				/*artDialog.open(uri , {title: "B2B Order B"+B2B_OID,width:'900px',height:'550px', lock: true,opacity: 0.3,fixed: true,
					close:function() {
						//renderOneRow(list,transport_id);
					}
				});*/
			});
			//更多日志
			list.on("events.moreLogs",function(e) {
				var B2B_OID = e.data.B2B_OID;
				var uri ="/Sync10/administrator/b2b_order/b2b_order_logs.html?b2b_oid="+B2B_OID;
				artDialog.open(uri, {title: 'Log Order:B'+B2B_OID,width:'970px',height:'500px',lock:true,opacity:0.3});
			});
			list.on("events.delete",function(e) {
				artDialog.confirm("确定删除订单B"+e.data.linedata.B2B_OID+"?",function(){
					$.ajax({
						url: '/Sync10/action/administrator/b2b_order/B2BOrderDelAction.action',
						type: 'post',
						dataType: 'json',
						timeout: 10000,
						cache: false,
						data: "b2b_oid=" + e.data.linedata.B2B_OID,
						beforeSend:function(request) {},
						error: function(e) {
							alert("提交失败，请重试！");
						},
						success: function(data) {
							if(data.rel) {
								var jqtr = $("tr[id='"+e.data.linedata.B2B_OID+"']");
								var set = list.deletetr(jqtr,"B2B_OID='"+e.data.linedata.B2B_OID+"'");
								//console.log(set);
								//TODO 删除页面数据行
							}
						}
					});
				}, function(){});
			});
			return list;
		};
});
