define(["jquery","backbone","../../config","../../templates",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree","immybox","dateUtil",
	"../../../key/B2BOrderKey",
		'jqueryui/datepicker',
		"require_css!jqueryui-css",
		"jqueryui/i18n/datepicker-zh-CN",
		"jqueryui/i18n/datepicker-en-AU"],
	function($,Backbone,config,templates,AsynLoadQueryTree,immybox,DateUitl,B2BOrderKey) {
	return Backbone.View.extend({
			el:"#advanceSearch",
			template:templates.advanceSearch,
			//model:new FilterModel(),
			initialize: function(options) {
				this.resultView = options.resultView;
				if (options.send_psid!='0') {
					this.shipFrom = options.send_psid;
				} else {
				}
				this.param=options;
			},
			events: {
				"click #createBol" : "createBol"
			},
			render: function() {
				var v = this;
				v.$el.html(v.template({}));
				//console.log(v.shipFrom);
				/*
				var fromTree = new AsynLoadQueryTree({
					renderTo: "#shipFrom",
					//selectid: {value: v.shipFrom},
					PostData: {rootType:"Ship From"},
					dataUrl: config.shipFrom.url,
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder: "Ship From"
				});
				fromTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				fromTree.render();
				/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType&type=1&lv=0&Reqnumr=1426668648423&rootType=Ship+From
				*/
				$.getJSON(config.shipFrom.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].data,"text":json[i].name});
					}
					$('#shipFrom').immybox({
						Pleaseselect:'Clean Select',
						choices: rls
					});
				});
				/*
				var toTree = new AsynLoadQueryTree({
					renderTo: "#shipTo",
					selectid: {value: v.shipTo},
					PostData: {rootType:"Ship To"},
					dataUrl: config.shipTo.url,
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder:"Ship To"
				});
				toTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				toTree.render();
				*/
				$('#status').immybox({
					Pleaseselect:'Clean Select',
					choices: v.getSelect(B2BOrderKey,DateUitl.getLanguage(),[1,2,3,4,5,6,7,8])
				});
				
				var adminTree = new AsynLoadQueryTree({
					renderTo: "#creater",
					dataUrl: config.adminTree.url,
					scrollH: 400,
					PostData: {rootType:"Creater"},
					multiselect: false,
					Async: true,
					Parentclick: false,
					Pleaseselect: "Clean Select",
					placeholder: "Creater"
				});
				adminTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				adminTree.render();
				$.getJSON(config.payment.url, function (json) {
					$('#payment').immybox({
						Pleaseselect:'Clean Select',
						choices: json
					});
				});
				var nowString = DateUitl.formatDate(new Date());
				if (DateUitl.getLanguage()=="zh") {
					$.datepicker.setDefaults($.datepicker.regional[ "zh-CN" ]);
					nowString = nowString.substring(0,10);
				} else {
					$.datepicker.setDefaults($.datepicker.regional[ "en" ]);
					nowString = nowString.substring(0,10);
				}
				$.getJSON(config.source.url, function (json) {
					$('#source').immybox({
						Pleaseselect:'Clean Select',
						choices: json
					});
				});
				$.getJSON(config.customer.url, function (json) {
					$('#customer').immybox({
						Pleaseselect:'Clean Select',
						choices: json
					});
				});
				$.getJSON(config.retailer.url, function (json) {
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].SHIP_TO_ID,"text":json[i].SHIP_TO_NAME});
					}
					$('#retailer').immybox({
						Pleaseselect:'Clean Select',
						choices: rls
					});
				});
				function changeLoad(e,d) {
					console.log(d);
					if (d.value==='1') {
						$("#div_load_no").show();
					} else {
						$("#div_load_no").hide();
					}
				}
				$.getJSON(config.loadType.url, function (json) {
					$('#load_flag').immybox({
						Pleaseselect:'Clean Select',
						Pleaseselectclick: function(e) {
							//console.log(e);
							$("#div_load_no").hide();
							$("#load_no").val("");
						},
						change: function(e,d) {
							//console.log(d);
							if (d.value==='1') {
								$("#div_load_no").show();
							} else {
								$("#div_load_no").hide();
								$("#load_no").val("");
							}
						},
						choices: json
					});
				});
				$("#SearchTabBtn").bind('click', {}, function(event) {
					v.filter();
				});
				//$("#ETAMin").datepicker({defaultDate: "+0"});
				//$("#ETAMax").datepicker({defaultDate: "+0"});
				$("#ETDMin").datepicker({defaultDate: "+0"});
				$("#ETDMax").datepicker({defaultDate: "+0"});
				//this.filter(v.shipFrom);
				return this;
			},
			getSelect: function(objlist,language,ul)
			{
				var list = [];
				if(ul) {
					for(var i=0;i<ul.length;i++) {
						var key = ul[i];
						var li = {};
						li.value = key;
						li.text = objlist[key][language];
						list.push(li);
					}
					return list;
				}
				_.map(objlist,function(item,key) {
					var li = {};
					li.value = key;
					li.text = item[language];
					list.push(li);
				});
				return list;
			},
			filter: function(pid) {
				//this.resultView.render(this.queryOptions);
				var f = true;
				var data = {
					//url: config.searchDefaultURL.url
				};
				/*
				if (pid) {
					data.send_psid = pid;
				} else {
					if ($("#shipFrom").attr("data-value")>0) {
						data.send_psid = $("#shipFrom").attr("data-value");
					}
				}*/
				if ($("#shipFrom").attr("data-value") && $("#shipFrom").val().trim()!="") {
					data.send_psid = $("#shipFrom").attr("data-value");
					$("#shipFrom").parent().removeClass("has-error");
				} else {
					$("#shipFrom").parent().addClass("has-error");
					f = false;
				}
				data.b2b_order_status = "11";
				/*
				if ($("#shipTo").attr("data-val")>0) {
					data.receive_psid = $("#shipTo").attr("data-val");
				}
				
				
				if ($("#status").attr("data-value")!=""&& $("#status").attr("data-value")>0) { data.b2b_order_status = $("#status").attr("data-value"); }
				if ($("#payment").attr("data-value")!="") { data.freight_term = $("#payment").attr("data-value"); }
				if ($("#source").attr("data-value")!="") { data.source_type= $("#source").attr("data-value"); }
				if ($("#creater").attr("data-val")!="") { data.create_account_id = $("#creater").attr("data-val"); }
				//if ($("#ETAMin").val()!="") { data.eta_begin = ($("#ETAMin").datepicker('getDate')).getTime(); }
				//if ($("#ETAMax").val()!="") { data.eta_end = ($("#ETAMax").datepicker('getDate')).getTime(); }
				if ($("#ETDMin").val()!="") { data.etd_begin = ($("#ETDMin").datepicker('getDate')).getTime(); }
				if ($("#ETDMax").val()!="") { data.etd_end = ($("#ETDMax").datepicker('getDate')).getTime(); }
				*/
				if ($("#customer").attr("data-value") && $("#customer").val().trim()!="") {
					data.customer_id=$("#customer").attr("data-value");
					$("#customer").attr("data-text",$("#customer").val());
					$("#customer").parent().removeClass("has-error");
				} else {
					$("#customer").parent().addClass("has-error");
					f = false;
				}
				if (!f) return;
				if ($("#retailer").attr("data-value") && $("#retailer").val().trim()!="") {
					data.retailer = $("#retailer").attr("data-value");
				}
				/*
				if ($("#load_flag").attr("data-value")==='1') {
					data.load_no_flag = 1;
					if ($("#load_no").val()!='') {
						data.load_no = $("#load_no").val();
					}
				} else if ($("#load_flag").attr("data-value")==='0') {
					data.load_no_flag = 0;
				}
				*/
				
				//CUSTOMER_ID
				//console.log(data);
				this.resultView.reload({url:config.searchDefaultURL.url,queryParams:data});
				//new TransportList(queryOptionsa);
			}
	});

}
);