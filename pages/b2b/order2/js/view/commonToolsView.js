"use strict";
define(["jquery","backbone",'handlebars',"../../templates","../../config","auto"],
	function($,Backbone,HandleBars,templates,config,auto)
	{
		return Backbone.View.extend(
		{
			el:"#commonTools",
			template:templates.commonTools,
			initialize:function(opts)
			{
				this.resultView = opts.resultView
				
			},
			render:function()
			{
				var temp = this;
				this.$el.html(this.template({}));
				auto.addAutoComplete($("#Search_Key"),config.autoComplete.url,"merge_field","b2b_oid");
				return this;
			},
			events:
			{
				"click #eso_search":"searchLeft"
			},
			searchLeft:function(evt)
			{
				var val = $("#Search_Key").val().replace(/\'/g,'');
				if (val=="") {
					alert("你好像忘记填写关键词了？");
					return;
				} else {
					val = "\'"+val.toLowerCase()+"\'";
					$("#Search_Key").val(val);
					this.resultView.setQuery({"key":val,"search_mode":1,"dataUrl":config.keySearchURL.url});
				}
			}
		});
	}
);