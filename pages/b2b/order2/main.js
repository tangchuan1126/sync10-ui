"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config){
	require(["jquery",
		"./js/view/advanceSearchView",
		"./js/view/commonToolsView",
		"oso.lib/table/js/table",
		"auto",
		"artDialog",
		"bootstrap",
		"domready!"
	],
	function($,SearchView,CommonToolsView,Table,auto,artDialog) {
		var psId="1000005";
		var queryOptions = {el: "#data", url:config.searchDefaultURL.url,queryCondition:{"send_psid":psId,pageSize:10}};
		/*
		var resultView  = new OrderListView(queryOptions);
		//resultView.collection.pageCtrl.pageSize= 10;
		resultView.on("events.Error", function(e) {
			$(".loding,.LodingModal").hide();
			alert("无法从服务器获取数据,请稍后在试。");
		});*/
		/*
		jQuery.download = function (url, data, method) {
			// 获取url和data
			if (url && data) {
				// data 是 string 或者 array/object
				data = typeof data == 'string' ? data : jQuery.param(data);
				var inputs = '';
				jQuery.each(data.split('&'), function () {
					var pair = this.split('=');
					inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
				});
				console.log(inputs);
				//jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
			};
		};
		*/
		var sv = null;
		var resultView = new Table({
			container : '#data',
			//url: config.searchDefaultURL.url,
			title : 'Order List',
			height: auto,
			rownumbers : false,
			checkbox: true,
			singleSelection : false,
			pagination: true,
			datasName: 'DATA',
			pageSizeName: 'pageSize',
			pageNumberName: 'pageNo',
			pageSize:50,
			totalName: 'PAGECTRL.allCount',
			pageList: ["50","100","200","500","1000"],
			isEnable:function(row){
				if (row.B2B_ORDER_STATUS==5|| row.B2B_ORDER_STATUS==7) {
					return false;
				} else {
					return true;
				}
			},
			columns:[
				{field:'B2B_OID',title:'Order ID',width:50,formatter : function(data, row){
					return '<a href="'+config.order.url+'?B2B_OID='+data+'" target="_new">' + data + '</a>';
				}},
				{field:'CUSTOMER_DN',title:'CustomerDN',width:50},
				{field:'RETAIL_PO',title:'Retail PO',width:50},
				{field:'SEND_PSNAME',title:'Ship From',width:100},
				{field:'MABD',title:'MABD',width:100},
				{field:'RECEIVE_PSNAME',title:'Ship To',width:100},
				{field:'CUSTOMER_NAME',title:'customer',width:100},
				{field:'LOAD_NO',title:'Load No.',width:100}
			],
			toolbar: [{
					text : '验证库存',
					id: 'check',
					iconCls : 'add',
					handler : function() {
						var wid = "";
						var sid = "";
						var rows = resultView.getSelected();
						var ids = [];
						var bids = [];
						if (rows) {
							for (var i=0;i<rows.length;i++) {
								if (i==0) {
									wid = rows[i].COMPANY_ID;
									sid = rows[i].CUSTOMER_ID
								}
								if (rows[i].CUSTOMER_DN) {
									ids.push(''+rows[i].CUSTOMER_DN);
									bids.push(rows[i].B2B_OID);
								}
								if (rows[i].COMPANY_ID!=wid) {
									artDialog.alert("please select Ship From for search.");
									return;
								}
								if (rows[i].CUSTOMER_ID!=sid) {
									artDialog.alert("please select only one customer.");
									return;
								}
							}
							if (ids.length>0) {
								//if (!$("#customer").attr("data-value")||$("#customer").attr("data-value")=="") {
								if (!$("#customer").val()||$("#customer").val()=="") {
									artDialog.alert("please select customer.");
									return;
								}
								var params = {
									"companyId": wid,
									"customerId": $("#customer").val(),
									"dnList": ids
								};
								//$.download(config.commitDN.url, params, 'post');
								$.ajax({
									type: "POST",
									url: config.commitDN.url,
									data: params,
									timeout: 30000,
									datatype: "json",//"xml", "html", "script", "json", "jsonp", "text".
									beforeSend:function(){
										//在请求之前调用的函数
									},
									success: function(data,textStatus) {
										//成功返回之后调用的函数
										if (textStatus=="success") {
											//console.log(data);
											if (data.length == 2) {
												var path= data[0].split("/");
												if (path.length>0) {
													artDialog.alert("please download:<a href='/Sync10/"+data[0]+"'>"+path[path.length-1]+"</a>");
												} else {
													artDialog.alert("please download:<a href='/Sync10/"+data[0]+"'>"+data[0]+"</a>");
												}
												if(data[1]==='Y') {
													$.ajax({
														type: "POST",
														url: config.addTransport.url,
														data: {data: JSON.stringify(bids), action:"addTransport"},
														timeout: 30000,
														datatype: "json",
														success: function(data,textStatus) {
															//成功返回之后调用的函数
															sv.filter(0);
															artDialog.alert("Create BOL OK");
														},
														error: function(){
															artDialog.alert("Can't Create BOL!");
														},
														headers: {
															"X-HTTP-Method-Override": "GET"
														}
													});
												}
											}
										}
									},
									error: function(){
										//请求出错处理
										artDialog.alert("Can't get Data!");
									},
									headers: {
										"X-HTTP-Method-Override": "GET"
									}
								});
							} else {
								artDialog.alert("please checked Order List.");
								return;
							}
						}
					}
				}
			]
		});
		sv = new SearchView({"send_psid":psId,resultView:resultView}).render();
		new CommonToolsView({"send_psid":psId,resultView:resultView}).render();
		$("#createOrder").bind('click', {}, function(event) {
			artDialog.open(config.createOrder.url, {title:"Create Order", width:'950px',height:'550px', lock:true, opacity:0.3, fixed:true});
		});
		$("#importOrder").bind('click', {}, function(event) {
			artDialog.open(config.importOrder.url, {title: "Import Order",width:'850px',height:'450px', lock: true,opacity: 0.3,fixed: true});
		});
		window.refreshWindow = function() {
			sv.filter();
		};
	});
});