define(
	{"1": {"en": "Create","zh": "创建记录"},
	"10": {"en": "Picture","zh": "实物图片流程"},
	"11": {"en": "Check","zh": "质检流程"},
	"12": {"en": "Inner label","zh": "内部标签"},
	"13": {"en": "Sending","zh": "到货派送"},
	"14": {"en": "third party label","zh": "第三方标签"},
	"15": {"en": "Unloading","zh": "缷货任务"},
	"16": {"en": "Loading","zh": "装货任务"},
	"2": {"en": "Finance","zh": "财务记录"},
	"3": {"en": "Update","zh": "修改记录"},
	"4": {"en": "status","zh": "货物状态"},
	"5": {"en": "Freight","zh": "运费流程"},
	"6": {"en": "Customs clearance","zh": "进口清关"},
	"7": {"en": "Customs entry","zh": "出口报关"},
	"8": {"en": "Entity label","zh": "实物标签"},
	"9": {"en": "Document","zh": "单证流程"}
});