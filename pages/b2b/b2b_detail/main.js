"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery",
			"./js/view/briefInfoView",
			"./js/view/addressInfoView",
			"./js/view/appointmentView",
			"./js/view/paymentView",
			"./js/view/detailView",
			"./js/view/OrderInfoView",
			"artDialog",
			/*
			"jquery.browser",
			"/Sync10-ui/bower_components/jqgrid/js/jquery.jqGrid.js",
			"/Sync10-ui/bower_components/jqgrid/js/i18n/grid.locale-en.js",
			*/
			"bootstrap"], function($,BriefInfoView,AddressInfoView,AppointmentInfoView,PaymentView,DetailView,OrderInfoView,artDialog) {
				function getQueryString(name) {
					var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
					var r = window.location.search.substr(1).match(reg);
					if (r != null) return unescape(r[2]); return null;
				}
				var B2B_OID = getQueryString("B2B_OID");
				var B2B_Data = {};
				var isModifyable = false;
				var eventAcrossView =  _.extend({}, Backbone.Events);

			

				var  detailView = null;

				var showMdfBtn = function()
				{
					$("#saveBtn").hide();
					$("#cancelBtn").hide();
					if(isModifyable)
					{
						$("#mdfBtn").show();
					}
					if($("#appointmentTab").parent().hasClass('active'))
					{
						$("#mdfBtn").show();
					}
				}
				showOrder(B2B_OID,"ALL");
				$("#mdfBtn").on("click", function(){
					$("#data>div.tab-pane").each(function(){
						if ($(this).hasClass("active")) {
							var tid =$(this).attr("id");
							$(this).data("editing",true);
							var param2 = {
								"eventControl":eventAcrossView,
								"B2B_OID": B2B_OID,
								"edit":true
							};
							if (tid=='detail') {
								detailView.render(true,false);
							} else if(tid=='orderInfo') {
								new BriefInfoView(param2).render(B2B_Data.DATA);
								new OrderInfoView(param2).render(B2B_Data.DATA);
							} else if(tid=='addressInfo') {
								new AddressInfoView(param2).render(B2B_Data.DATA);
							} else if (tid=='payment') {
								new PaymentView(param2).render(B2B_Data.DATA);
							} else if (tid=='appointment') {
								new AppointmentInfoView(param2).render(B2B_Data.DATA);
							}
						}
					});
					$("#mdfBtn").hide();
					$("#saveBtn").show();
					$("#cancelBtn").show();
				});
				function saveOrderform() {
					var data = {};
					if ($("#ipt_customer_dn").val().trim()=="") {
						$("#ipt_customer_dn").parent().addClass("has-warning");
						return null;
					} else {
						data.customer_dn = $("#ipt_customer_dn").val();
						$("#ipt_customer_dn").parent().removeClass("has-warning");
					}
					/**  去掉了 po 和so
					if ($("#ipt_retail_po").val().trim()=="") {
						$("#ipt_retail_po").parent().addClass("has-warning");
						return null;
					} else {
						data.retail_po = $("#ipt_retail_po").val();
						$("#ipt_retail_po").parent().removeClass("has-warning");
					} 
					if ($("#order_number").val().trim()=="") {
						//$("#order_number").parent().addClass("has-warning");
						//return null;
						data.order_number = $("#order_number").val();
					} else {
						data.order_number = $("#order_number").val();
						$("#order_number").parent().removeClass("has-warning");
					} **/
					if ($("#ipt_customer_id").attr("data-value")=="") {
						$("#ipt_customer_id").parent().addClass("has-warning");
						return null;
					} else {
						data.customer_id = $("#ipt_customer_id").attr("data-value");
						$("#ipt_customer_id").parent().removeClass("has-warning");
					}
					if ($("#ipt_mabd").val().trim()=="") {
						$("#ipt_mabd").parent().addClass("has-warning");
						return null;
					} else {
						data.mabd = $("#ipt_mabd").val();
						$("#ipt_mabd").parent().removeClass("has-warning");
					}

					if ($("#ipt_shipNotBefore").val().trim()=="") {
						$("#ipt_shipNotBefore").parent().addClass("has-warning");
						return null;
					} else {
						data.ship_not_before = $("#ipt_shipNotBefore").val();
						$("#ipt_shipNotBefore").parent().removeClass("has-warning");
					}
					
					if ($("#ipt_shipNotLater").val().trim()=="") {
						$("#ipt_shipNotLater").parent().addClass("has-warning");
						return null;
					} else {
						data.ship_not_later = $("#ipt_shipNotLater").val();
						$("#ipt_shipNotLater").parent().removeClass("has-warning");
					}
					if ($("#ipt_reqdate").val()!="") data.requested_date = $("#ipt_reqdate").val();
					return data;
				}
				function saveAddressform() {
					var data = {};
					var ok = true;
					if ($("#ipt_send_psid").attr("data-value")=="") {
						$("#ipt_send_psid").parent().addClass("has-warning");
						ok = false;
					} else {
						data.send_psid = $("#ipt_send_psid").attr("data-value");
						$("#ipt_send_psid").parent().removeClass("has-warning");
					}
					data.send_house_number = $("#ipt_send_house_number").val();
					data.send_city = $("#ipt_send_city").val();
					data.address_state_send = $("#ipt_address_state_send").val();
					data.send_zip_code = $("#ipt_send_zip_code").val();
					data.send_name = $("#ipt_send_name").val();
					data.send_linkman_phone = $("#ipt_send_linkman_phone").val();
					
					/**
					if ($("#ipt_receive_psid").attr("data-val")=="") {
						$("#ipt_receive_psid").parent().addClass("has-warning");
						ok = false;
					} else {
						data.receive_psid = $("#ipt_receive_psid").attr("data-val");
						$("#ipt_receive_psid").parent().removeClass("has-warning");
					}
					 **/
					 //处理手工输入
					if($("#ipt_receive_psid").val() != "")
					{
						var oldName = $("#old_name").val();
						var inputShipToName =  $("#ipt_receive_psid").val();
					  	var selectShipToName =  $("#ipt_receive_psid").attr("data-name");
					  	var receive_psid = $("#ipt_receive_psid").attr("data-val");
					  	var ship_to_party_name = "";
					  	var  account_id = "";
					  	if(inputShipToName != selectShipToName)
					  	{
					  		//不一致，肯定是手工输入
					  		data.receive_psid = $("#ipt_send_psid").attr("data-value");
					  		data.ship_to_party_name =  inputShipToName;
					  		data.account_id = "-1";
					  	} 
					  	else
					  	{
					  		//选择的
					  		data.receive_psid = receive_psid;
					  		data.ship_to_party_name = selectShipToName;
					  		data.account_id = "0";
					  	}
					  	$("#ipt_receive_psid").parent().removeClass("has-warning");
					}
					else
					{
						$("#ipt_receive_psid").parent().addClass("has-warning");
						ok = false;
					}

					if ($("#ipt_deliver_house_number").val()=="") {
						$("#ipt_deliver_house_number").parent().addClass("has-warning");
						ok = false;
					} else {
						data.deliver_house_number = $("#ipt_deliver_house_number").val();
						$("#ipt_deliver_house_number").parent().removeClass("has-warning");
					}
					if ($("#ipt_deliver_street").val()=="") {
						data.deliver_street = $("#ipt_deliver_street").val();
						//$("#ipt_deliver_street").parent().addClass("has-warning");
						//ok = false;
					} else {
						data.deliver_street = $("#ipt_deliver_street").val();
						//$("#ipt_deliver_street").parent().removeClass("has-warning");
					}
					if ($("#ipt_deliver_city").val()=="") {
						$("#ipt_deliver_city").parent().addClass("has-warning");
						ok = false;
					} else {
						data.deliver_city = $("#ipt_deliver_city").val();
						$("#ipt_deliver_city").parent().removeClass("has-warning");
					}

					if ($("#ipt_address_state_deliver").attr("data-value")=="") {
						$("#ipt_address_state_deliver").parent().addClass("has-warning");
						ok = false;
					} else {
						data.address_state_deliver = $("#ipt_address_state_deliver").val();
						data.deliver_pro_id = $("#ipt_address_state_deliver").attr("data-value");
						$("#ipt_address_state_deliver").parent().removeClass("has-warning");
					}


					if ($("#ipt_deliver_zip_code").val()=="") {
						$("#ipt_deliver_zip_code").parent().addClass("has-warning");
						ok = false;
					} else {
						data.deliver_zip_code = $("#ipt_deliver_zip_code").val();
						$("#ipt_deliver_zip_code").parent().removeClass("has-warning");
					}
				
					data.deliver_ccid = $("#ipt_country").attr("data-value");
					data.b2b_order_linkman = $("#ipt_b2b_order_linkman").val();
					var phone = $("#ipt_b2b_order_linkman_phone").val().trim();
					if( phone !="")
					{
						var  pattern =/^([\d-+\s]*)$/;
						if(pattern.test(phone) && phone.length < 50)
						{
							data.b2b_order_linkman_phone = $("#ipt_b2b_order_linkman_phone").val();
							$("#ipt_b2b_order_linkman_phone").removeClass("has-warning");
							$("#ipt_b2b_order_linkman_phone").attr('title', '');
						}
						else
						{
							$("#ipt_b2b_order_linkman_phone").parent().addClass("has-warning");
							$("#ipt_b2b_order_linkman_phone").attr('title', 'Please input a correct phone number');
							ok = false;
						}
						
					}
					
					if (!ok) return null;
					return data;
				}
				function saveAppointmentform() {
					var data = {};
					
					if ($("#ipt_pickup_appointment").val()!="") data.pickup_appointment = $("#ipt_pickup_appointment").val();
					if ($("#ipt_carrier").val()!="") {
						data.carriers = $("#ipt_carrier").attr("data-value");
						//data.b2b_order_waybill_name = $("#ipt_carrier").val();
					} else {
						data.carriers = "";
						//data.b2b_order_waybill_name = "";
					}
					if ($("#ipt_freight_type").val()!="") {
						data.freight_carrier = $("#ipt_freight_type").attr("data-value");
					} else {
						data.freight_carrier = "";
					}
					if ($("#ipt_load_no").val()!="") {
						data.load_no = $("#ipt_load_no").val();
					} else {
						data.load_no = "";
					}
					if ($("#ipt_waybill_no").val()!="") {
						data.b2b_order_waybill_number = $("#ipt_waybill_no").val();
					} else {
						data.b2b_order_waybill_number = "";
					}
					//if ($("#ipt_etd").val()!="") data.etd = $("#ipt_etd").val();
					//if ($("#ipt_eta").val()!="") data.eta = $("#ipt_eta").val();
					return data;
				}
				function savePaymentform() {
					var data = {};
					
					var freight_term = $("input[name='optionsRadios']:checked").val();
					if (freight_term=="Third Party") {
						var f = true;
						if ($("#ipt_bill_to_name").val().trim()=="") {
							$("#ipt_bill_to_name").parent().addClass("has-warning");
							f = false;
						} else {
							data.bill_to_name = $("#ipt_bill_to_name").val();
							$("#ipt_bill_to_name").parent().removeClass("has-warning");
						}
						if ($("#ipt_bill_to_address1").val().trim()=="") {
							$("#ipt_bill_to_address1").parent().addClass("has-warning");
							f = false;
						} else {
							var address =  $("#ipt_bill_to_address1").val();
							if(address.replace(/[^\x00-\xff]/g,"  ").length >= 50)
							{
								$("#ipt_bill_to_address1").parent().addClass("has-warning");
								$("#ipt_bill_to_address1").attr("title","Address is too long!");
								f = false;
							}
							else
							{	
								$("#ipt_bill_to_address1").parent().removeClass("has-warning");
							}
						}
						if ($("#ipt_bill_to_city").val().trim()=="") {
							$("#ipt_bill_to_city").parent().addClass("has-warning");
							f = false;
						} else {
							$("#ipt_bill_to_city").parent().removeClass("has-warning");
						}
						if ($("#ipt_bill_to_state").val().trim()=="") {
							$("#ipt_bill_to_state").parent().addClass("has-warning");
							f = false;
						} else {
							$("#ipt_bill_to_state").parent().removeClass("has-warning");
						}
						if ($("#ipt_bill_to_zip_code").val().trim()=="") {
							$("#ipt_bill_to_zip_code").parent().addClass("has-warning");
							f = false;
						} else {
							$("#ipt_bill_to_zip_code").parent().removeClass("has-warning");
						}
						if (!f) return null;
					}
					data.freight_term = freight_term;
					data.bill_to_id = $("#ipt_bill_to_name").attr("data-val");
					data.bill_to_name = $("#ipt_bill_to_name").attr("data-name");
					data.bill_to_address1 = $("#ipt_bill_to_address1").val();
					data.bill_to_city = $("#ipt_bill_to_city").val();
					data.bill_to_state = $("#ipt_bill_to_state").val();
					data.bill_to_zip_code = $("#ipt_bill_to_zip_code").val();
					//data.bill_to_contact = $("#ipt_bill_to_contact").val();
					//data.bill_to_extension = $("#ipt_bill_to_extension").val();
					//data.bill_to_fax = $("#ipt_bill_to_fax").val();
					return data;
				}
				function showOrder(OID,tid) {
					var param = {
						//"eventAcrossView": _.extend({}, Backbone.Events),
						"B2B_OID": OID,
						"edit": 0
					};
					$.ajax({
						url: config.order.url,//将来改为session信息
						type: 'GET',
						dataType: 'json',
						timeout: 5000,
						data:{action:"getB2BOrder",data: OID},
						success: function(data) {
							B2B_Data = data;
							if (B2B_Data.DATA.CUSTOMER_DN) {
								$("title").text("DN"+B2B_Data.DATA.CUSTOMER_DN);
							}
							if(tid=='addressInfo') {
								new AddressInfoView(param).render(B2B_Data.DATA);
							} else if(tid=='orderInfo') {
								new OrderInfoView(param).render(B2B_Data.DATA);
								new BriefInfoView(param).render(B2B_Data.DATA);
							} else if (tid=='payment') {
								new PaymentView(param).render(B2B_Data.DATA);
							} else if (tid=='appointment') {

								new AppointmentInfoView(param).render(B2B_Data.DATA);
							} else {
								new BriefInfoView(param).render(data.DATA);
								new OrderInfoView(param).render(data.DATA);
								new AddressInfoView(param).render(data.DATA);
								new AppointmentInfoView(param).render(data.DATA);
								new PaymentView(param).render(data.DATA);


								var initData = {
								"eventControl":eventAcrossView,
								"B2B_OID": B2B_OID,
								"B2B_DATA":data.DATA,
								"edit":true
								};
								detailView = new DetailView(initData);
								detailView.render(false,true);
								if (data.DATA.B2B_ORDER_STATUS=='1' ||data.DATA.B2B_ORDER_STATUS=='10') {
									isModifyable = true;
									$("#mdfBtn").show();
								} else {
									$("#mdfBtn").hide();
								}
							}
							
						},
						error:function(e) {
							$("#saveBtn").hide();
							$("#cancelBtn").hide();
							$("#mdfBtn").hide();
							console.log(e);
							alert("error");
						}
					});
				}
				function saveMainInfo(data, OID, tid) {
					data.B2B_OID = OID;
					$.ajax({
						url: config.order.url,
						type: 'POST',
						dataType: 'json',
						timeout: 5000,
						data:{data: JSON.stringify(data), action:"updateB2BOrder"},
						success: function(rdata) {
							//console.log(rdata);
							if(rdata.CODE=="200") {
								showOrder(OID,tid)
								showMdfBtn();
								detailView.showMessage("Save success !",'succeed');
							} else {
							detailView.showMessage("Save error !",'error');
							}
						},
						error:function(e) {
							alert("System error");
							//console.log(e);
						}
					});
				}
				function showDetail(OID) {
					
				}
				function saveDetailInfo(OID) {
					eventAcrossView.trigger('saveDetail');
					//$("#cancelBtn").trigger("click","#detail");
				}
				eventAcrossView.on('saveSuccess',function(){
						$("#saveBtn").hide();
						$("#cancelBtn").hide();
						$("#mdfBtn").show();
				});
				$("#saveBtn").on("click", function(e,d){
					$("#data>div.tab-pane").each(function()
					{
						if ($(this).hasClass("active")) 
						{
							var data = null;
							var tid =$(this).attr("id");
							if (tid=='detail') 
							{
								saveDetailInfo(B2B_OID);
							} 
							else if(tid=='orderInfo') 
							{
								 data = saveOrderform();
								if (data!=null) saveMainInfo(data,B2B_OID,tid);
							} 
							else if(tid=='addressInfo') 
							{
								 data = saveAddressform();
								if (data!=null) saveMainInfo(data,B2B_OID,tid);
							} 
							else if (tid=='appointment') 
							{
								 data = saveAppointmentform();
								if (data!=null) saveMainInfo(data,B2B_OID,tid);
							} 
							else if (tid=='payment') 
							{
								 data = savePaymentform();
								if (data!=null) saveMainInfo(data,B2B_OID,tid);
							}

							if(data!=null ) {
								$('#tabs a[href="'+d+'"]').tab('show');
								$("#"+tid).data("editing",false);
							}
						}
					});
				});
				$("#cancelBtn").on("click", function(e,d){
					$("#data>div.tab-pane").each(function(){
						if ($(this).hasClass("active")) {
							var tid =$(this).attr("id");
							var param = {
								"B2B_OID": B2B_OID,
								"edit": 0
							};
							if(tid=='addressInfo') {
								
								new AddressInfoView(param).render(B2B_Data.DATA);
							} else if(tid=='orderInfo') {
								new BriefInfoView(param).render(B2B_Data.DATA);
								new OrderInfoView(param).render(B2B_Data.DATA);
							} else if (tid=='payment') {
								new PaymentView(param).render(B2B_Data.DATA);
							} else if (tid=='appointment') {
								new AppointmentInfoView(param).render(B2B_Data.DATA);
							} else if (tid=='detail') {
								detailView.render(false,true);
							}
							showMdfBtn();
							$("#"+tid).data("editing",false);
							if(d) {
								$('#tabs a[href="'+d+'"]').tab('show');
							}
						}
					});
					
				});
				$('#tabs a[data-toggle="tab"]').on('show.bs.tab', function (e) {
					if ($(e.relatedTarget.hash).data("editing")===true) {
						artDialog.confirm('Save Change?',function(){
							$("#saveBtn").trigger("click",e.target.hash);
						},function(){
							$("#cancelBtn").trigger("click",e.target.hash);
						});
						return e.preventDefault();
					}

					if(!isModifyable)
					{
						if(e.target.id =="appointmentTab")
						{
							$("#mdfBtn").show();
						}
						else
						{
							$("#mdfBtn").hide();
						}
					}
					
					if(e.target.id =="detailTab")
					{
						//解决表头变形问题
						window.resizeTo($(window).width(),$(window).height())
					}	
					
				});

				
       			  window.onunload =function()
       			  {
       			  	if(window.opener)window.opener.refreshWindow();
       			  }
			

		});
	});