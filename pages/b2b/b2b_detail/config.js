define({
	getClpType:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getClpTypes",
	},
	saveLines:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=modLines",
	},
	title:
	{
		url:"/Sync10/action/administrator/getAllTitle.action"
	},
   shipTo:
    {
       url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        //url:"/Sync10-ui/lib/AsynLoadQueryTree/d.json"
    },
	cityInfo:
    {
       // http://localhost:8080/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=11004
        url:"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action"
    },
	products:
	{
		url:"/Sync10/action/administrator/product/getSearchProductsJSON.action"
	},
	addDetail:
    {
        url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=addB2BOrderItem"
    },
    editDetail:
    {
    	url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=updateB2BOrderItem"
    },
    delDetail:
    {
    	url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=deleteB2BOrderItem"
    },
    accumulate:
    {
    	url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=sumCount"
    },
	orderDetail: {
		//url:"/Sync10-ui/pages/b2b/b2b_detail/data/detail.json?1=1"
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getB2BOrderItems"
	},
	orderDetail2: {
		//url:"/Sync10-ui/pages/b2b/b2b_detail/data/detail2.json"
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getB2BOrderItems2"
	},
	order: {
		//url:"/Sync10-ui/pages/b2b/b2b_detail/data/order.json"
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action"
	},
	carrier: {
		//url:"/Sync10-ui/pages/b2b/b2b_detail/data/carrier.json"
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllFreight"
	},
	freightType: {
		url:"/Sync10-ui/pages/b2b/b2b_detail/data/freightType.json"
	},
	customer:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllCustomerId"
	},
	addressInfo: {
		url: "/Sync10/action/administrator/product/getProductStorageCatalogJson.action?1=1"
	},
	shipFrom: {
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
		//url:"/Sync10/_b2b/storage/"
		//url: "./data/shipFrom.json"
	},
	shipTo: {
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
		//url:"/Sync10/_b2b/storage/"
		//url: "./data/shipTo.json"
	},
	autoComplete: {
		url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
	},
	storageTree: {
		url:"/Sync10/_b2b/storage/"
	},
	adminTree: {
		url:"/Sync10/_b2b/admin/"
	},
	session: {
		url: "/Sync10/_fileserv/redis_session"
	}
});