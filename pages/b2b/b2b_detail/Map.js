define(["jquery"], function($) 
{
		var LyMap = function(idAttribute, dataArray)
		{

			this._map = {};
			var temp = $.extend(true,{},dataArray);
			if(arguments.length == 0){
				this.initialize2();
			}else{
				this.initialize(idAttribute, temp);
			}
		}

		LyMap.prototype.initialize = function(idAttribute, array)
		{
			this._map = {}
			var temp = this;
			if(typeof array =="object" )
			{
				$.each(array, function(index, lineData) 
				{
					if(lineData && lineData[idAttribute])
					{
						temp._map[lineData[idAttribute]] = lineData;
					}
				});
			}
			return this;
		}
		LyMap.prototype.initialize2 = function()
		{
			this._map = {}
			return this;
		}
		LyMap.prototype.get = function(id)
		{
			return this._map[id];
		}
		LyMap.prototype.put = function(key,obj)
		{
			return this._map[key]=obj;
		}
		LyMap.prototype.remove = function(key)
		{
			this._map[key] = null;
			delete this._map[key];
			return this;
		}
		LyMap.prototype.removeAll = function()
		{
			this._map = null;
			return this;
		}
		LyMap.prototype.getValues = function()
		{
			var array = [];
			for(key in this._map)
			{
				array.push(this.get(key));
			}	
			return array;
		}
		return LyMap;
});