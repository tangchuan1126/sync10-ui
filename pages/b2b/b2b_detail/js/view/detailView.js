define(["jquery","backbone","../../config","../../templates",
"oso.lib/table/js/table",
"artDialog",
"art_Dialog/dialog",
"./ProductEditView",
"auto",
"../../Map",
"jquery.browser"],
function($,Backbone,config,templates,Table,artDialog,Dialog,ProductEditView,auto,Map) {
	return Backbone.View.extend({
			el:"#detail",
			template:templates.detail,
			deleteData : [],
			initialize:function(param) {
				var that =this;
				this.param = param;
				this.param.eventControl.on('saveDetail', function(event) {
					that.saveData();
				});
			},
			events: {
				//"click #addDetail" : "addDetail",
				//"click #deleteDetail" : "deleteDetail"
			},
			init:function(edit,reload)
			{
				var that =this;
				//查看页面，重新加载数据
				if(reload)
				{
					//每次初始化 ,先清空集合
					this.collection = null;
					this.deleteData = [];
					$.getJSON(config.orderDetail.url, {data:this.param.B2B_OID}, function(json, textStatus) 
					{
						//存放 新增和修改的数据
						that.collection = new Map();
						$.each(json.DATA, function(index, obj) {
							that.collection.put(obj.TITLE+"-"+obj.B2B_P_NAME,obj);
						});
						that.initDetailTable(edit,that.collection.getValues());
					});
				}
				else
				{
					that.initDetailTable(edit,that.collection.getValues());
				}
			},
			initDetailTable:function(edit,localData)
			{
				var that = this;
				//存放删除的数据(已经持久化过了的)
				$("#detail").empty();
				var table = new Table({
				container : '#detail',
				//url : config.orderDetail.url+'&data='+this.param.B2B_OID,
				localData :localData,
				//title : 'DATA LIST',
				height: 255,
				rownumbers : false,
				singleSelection : false,
				pagination: false,
				checkbox : edit,
				datasName: 'DATA',
				onLoadSuccess:function(){
					table.refreshWidth();
				},
				columns:[
					//{field:'B2B_DETAIL_ID',title:'ID',width:10,hidden:true},
					//name:'B2B_P_NAME',index:'Model#/Lot#'
					{field:'TITLE',title:'Title',width:100},
					{field:'B2B_P_NAME',title:'Model',width:120},
					{field:'RETAIL_PO',title:'PO',width:100},
					{field:'SO',title:'SO',width:100},
					{field:'P_CODE',title:'UPC NO.',width:90},
					{field:'Config',title:'Configuration',width:100,formatter : function(data, row){
							if(row.LENGTH && row.WIDTH && row.HEIGTH){
								return row.LENGTH+"x"+row.WIDTH+"x"+row.HEIGTH;
							}
							else{
								return "";
							}
						}
					},
					{field:'LP_NAME',title:'CLP Type',width:120},
					{field:'B2B_COUNT',title:'Order Qty',width:65},
					{field:'BOXES',title:'Carton Qty',width:70},
					{field:'PALLET_SPACES',title:'Pallet Qty',width:70},
					
					{field:'TOTAL_WEIGHT',title:'Weight(LBS)',width:70},
				],
				toolbar : !edit? null :[
                {
                    text : 'add',
                    iconCls : 'add',
                    handler : function(){
                    	var so = null;
                    	var isElement = null;
                    	var cusromer = that.param.B2B_DATA.CUSTOMER_ID;
                    	if(cusromer=="1"||cusromer=="2"||cusromer=="3")
                    	{
                    		var orderLines = table.getLocalData();
	                    	if(orderLines.length > 0){
	                    		so = orderLines[0].SO;
	                    		isElement = true;
	                    	}
                    	}
                    	var view = new ProductEditView({"B2B_OID":that.param.B2B_OID}).render({"SO":so,"isElement":isElement});
						Dialog({
							title: 'Add Product',
							width: 450,
							content:view.el,
							okValue:"Confirm",
							onshow:function()
							{
								view.autoComplete();
							},
							ok:function(){
								if(view.validate())
								{
									var newData = view.getData();
									//基本数据校验完成,进行深度校验
									newData.B2B_OID = that.param.B2B_OID;
									var local = table.getLocalData();
									var oldData = null;
									$.each(local, function(index, row) {
										if(row.B2B_P_NAME == newData.B2B_P_NAME && row.TITLE == newData.TITLE)
										{
											oldData = row;
										}
									});
										//该订单下已经存在该商品,提示累加还是更新
									if(oldData != null)
									{
										Dialog({
											  title: 'Attention',
											  content:'This product already exists in this order !Accumulate button will accumulate Quantity,Carton,Total Weight and Pallet Spaces !',
											  button: [{
															value: 'Accumulate',
															callback: function () {
																var megerData = that.accumulate(oldData,newData);
																megerData._update = true;
																that.collection.put(megerData.TITLE+"-"+megerData.B2B_P_NAME,megerData);
																table.deleteLocalData(megerData,function(){
															    	table.addLocalData(megerData, function(row){
										                          		 table.reloadCurrent(true);
										                          		 table.refreshWidth();
										                        		});
																});
															},
															autofocus: true
														},
														{
															value: 'Cancel',
														}]
										}).showModal();
									}
									else
									{
										//新增
										that.collection.put(newData.TITLE+"-"+newData.B2B_P_NAME,newData);
										table.addLocalData(newData,function(row){
				                            table.reloadCurrent(true);
				                            table.refreshWidth();
				                        });
									}
								}
								else
								{
									return false;
								}
	                    	},
	                    	cancel:function()
							{
								this.close().remove();
							}
						}).showModal();
					}
                },
                {
                    text : 'update',
                    iconCls : 'update',
                    handler : function(){
                   		var isElement = null;
                    	var cusromer = that.param.B2B_DATA.CUSTOMER_ID;
                    	if(cusromer=="1"||cusromer=="2"||cusromer=="3")
                    	{
                    		var orderLines = table.getLocalData();
	                    	if(orderLines.length > 1){//只有一条数据的时候允许修改
	                    		isElement = true;
	                    	}
                    	}
                    	var data  = table.getSelected();
						if(data.length > 0)
						{
							if(data.length == 1)
							{
								var oldData = data[0];
								oldData.isElement = isElement;
								var view = new ProductEditView({"B2B_OID":that.param.B2B_OID}).render(oldData);
								Dialog({
								title: 'Edit Product',
								width: 450,
								content:view.el,
								okValue:"Confirm",
								onshow: function () 
								{
									view.autoComplete();
								},
								ok:function(){
									if(view.validate())
									{
										var row = view.getData();

										var local = table.getLocalData();
										var repeat = false;
										$.each(local, function(index, obj) {
											if(that.isSameOrderLine(obj,row))
											{
												if(obj._index != row._index)
												{
													repeat = true;
												}
											}
										});

										if(!repeat)
										{
											//没有跟其他商品有重复，正常添加
											row._update = true;
											that.collection.remove(oldData.TITLE+"-"+oldData.B2B_P_NAME);
											that.collection.put(row.TITLE+"-"+row.B2B_P_NAME,row);
											table.deleteLocalData(oldData,function(){
											   table.addLocalData(row, function(row){
					                          	 table.reloadCurrent(true);
					                          	 table.refreshWidth();
					                        	});
											});
										}
										else
										{
											//修改后的商品与现有商品重复
											Dialog({
											  title: 'Attention',
											  content:'This product already exists in this order !',
											  button: [{
															value: 'Confirm',
															autofocus: true
														}]
												}).showModal();
										}
									}
									else
									{
										return false;
									}
								},
								cancel:function()
								{
									this.close().remove();
								}
								}).showModal();
							}
							else
							{
								alert("please choose one data");
							}
						}
						else
						{
							alert("please choose a data");
						}
                    	
                    }
                },
                {
                    text : 'delete',
                    iconCls : 'remove',
                    handler : function(){
                        var rows = table.getSelected();
                        if(rows&& rows.length >0)
                        {
                        	$.each(rows, function(index, row) {
                        		that.collection.remove(row.TITLE+"-"+row.B2B_P_NAME);
                        		if(row.B2B_DETAIL_ID){
                        			//将数据库中的数据单独存放
                        			that.deleteData.push(row);
                        		}
                        	});
                        	 table.deleteLocalData(rows, function(row){
                            	 table.reloadCurrent(true);
                       		 });
                        }
                    }
                }]
				});
			},
			render:function(edit,reload)
			{
				this.init(edit,reload);
			},
			saveData:function()
			{
				var that = this;
				var so = null;
				var isElement =  false;
				var cusromer = that.param.B2B_DATA.CUSTOMER_ID;
				var validateResult = true;
            	if(cusromer=="1"||cusromer=="2"||cusromer=="3")
            	{
            		isElement  = true;
            	}
				var updates=[],adds=[],dels= this.deleteData;
				$.each(this.collection.getValues(), function(index, row) {
					if(isElement)
					{
						if(index==0)so = row.SO;
						//比较so是否相同
						if(row.SO!=so){
							validateResult = false;
							return false;
						}
					}
					if(row&&row.B2B_DETAIL_ID){
						if(row._update)
						{
							updates.push(that.getUpdateData(row));
						}
					}else{
						adds.push(that.getAddData(row));
					}
				});

				if(!validateResult){
					that.showMessage("All the lines should has the same SO!",'error');
					return;
				}
				var delIds  = "";
				if(dels.length >0){
					$.each(dels, function(index, obj) {
						delIds = delIds + obj.B2B_DETAIL_ID+",";
					});
					delIds = delIds.substring(0,delIds.length-1);
				}
				var postData = {
					"b2b_oid":that.param.B2B_OID,
					"updates":updates,
					"adds":adds,
					"dels":delIds
				};
				//console.log(postData);
				if(updates.length ==0 && adds.length ==0 && delIds == "")
				{
					$("#detail").data("editing",false);
					that.param.eventControl.trigger('saveSuccess');
					that.showMessage("Save success !",'succeed');
					that.render(false,false);
					return false;
				}
				$("#saveBtn").addClass('disabled');
				$.ajax({
					url: config.saveLines.url,
					type: 'POST',
					dataType: 'json',
					data: {data:JSON.stringify(postData)},
				})
				.done(function(data) {
					if(data.code == "200")
					{
						$("#detail").data("editing",false);
						that.showMessage("Save success !",'succeed');
						that.param.eventControl.trigger('saveSuccess');
						that.render(false,true);
					}else{
						that.showMessage("Save failed ! Please try again later!",'error');
					}
				})
				.fail(function(error) {
					that.showMessage("Save failed ! Please try again later!",'error');
				}).always(function(){
					$("#saveBtn").removeClass('disabled');
				});
				
			},
			getAddData:function(param)
			{
				var data = {};
				data.TITLE= param.TITLE;
				data.B2B_P_NAME=param.B2B_P_NAME;
				data.B2B_COUNT=param.B2B_COUNT;
				data.BOXES=param.BOXES;
				data.RETAIL_PO = param.RETAIL_PO;
				data.SO = param.SO;
				data.PALLET_SPACES=param.PALLET_SPACES;
				data.TOTAL_WEIGHT=param.TOTAL_WEIGHT;
				data.B2B_OID = this.param.B2B_OID;
				data.CLP_TYPE_ID = param.CLP_TYPE_ID;
				return data;
			},
			getUpdateData:function(param)
			{
				var data = {};
				data.B2B_DETAIL_ID = param.B2B_DETAIL_ID;
				data.TITLE= param.TITLE;
				data.B2B_P_NAME=param.B2B_P_NAME;
				data.B2B_COUNT=param.B2B_COUNT;
				data.BOXES=param.BOXES;
				data.RETAIL_PO = param.RETAIL_PO;
				data.SO = param.SO;
				data.CLP_TYPE_ID =param.CLP_TYPE_ID;
				if(!this.isEmpty(param.PALLET_SPACES)){
					data.PALLET_SPACES=param.PALLET_SPACES;
				}else{
					data.PALLET_SPACES="0";
				}
				if(!this.isEmpty(param.TOTAL_WEIGHT)){
					data.TOTAL_WEIGHT=param.TOTAL_WEIGHT;
				}else{
					data.TOTAL_WEIGHT="0";
				}
				
				return data;
			},
			editDetail:function(param)
			{
				
			},
			deleteDetail:function(data)
			{
			
			},
			accumulate:function(oldData,data)
			{
				oldData.B2B_COUNT = parseInt(oldData.B2B_COUNT) + parseInt(data.B2B_COUNT);
				oldData.BOXES = parseInt(oldData.BOXES) + parseInt(data.BOXES);
				if(!this.isEmpty(data.PALLET_SPACES))
				{
					if(!this.isEmpty(oldData.PALLET_SPACES))
					{
						oldData.PALLET_SPACES = parseFloat(oldData.PALLET_SPACES) + parseFloat(data.PALLET_SPACES);
					}
					else
					{
						oldData.PALLET_SPACES = parseFloat(data.PALLET_SPACES);
					}
				}
				else
				{
					oldData.PALLET_SPACES ="";
				}
				if(!this.isEmpty(data.TOTAL_WEIGHT))
				{
					if(!this.isEmpty(oldData.TOTAL_WEIGHT))
					{
						oldData.TOTAL_WEIGHT = parseFloat(oldData.TOTAL_WEIGHT) + parseFloat(data.TOTAL_WEIGHT);
					}
					else
					{
						oldData.TOTAL_WEIGHT = parseFloat(data.TOTAL_WEIGHT);
					}
				}
				else
				{
					oldData.TOTAL_WEIGHT ="";
				}
				return oldData;
			},
			isEmpty:function(str)
			{
				return str=="" ||str==undefined||str==null||str=="null";
			},
			isSameOrderLine:function(old_line,new_line){
				if(this.isEmpty(old_line.RETAIL_PO))old_line.RETAIL_PO="";
				if(this.isEmpty(old_line.SO))old_line.SO="";
				if(this.isEmpty(new_line.RETAIL_PO))new_line.RETAIL_PO="";
				if(this.isEmpty(new_line.SO))new_line.SO="";
				return old_line.B2B_P_NAME == new_line.B2B_P_NAME 
					&& old_line.TITLE == new_line.TITLE
					&& old_line.RETAIL_PO == new_line.RETAIL_PO
					&& old_line.SO == new_line.SO;
			},
			autoComplete:function()
			{
				auto.addAutoComplete($("#B2B_P_NAME"),config.products.url,"merge_info","p_name");
			},
			//stateBox 信息提示框
			showMessage:function(_content,_state){
				var o =  {
					state:_state || "succeed" ,
					content:_content,
					corner: true
				};
				var _self = $("body"),
				_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
				_self.append(_stateBox);
				 
				if(o.corner){
					_stateBox.addClass("ui-corner-all");
				}
				if(o.state === "succeed"){
					_stateBox.addClass("ui-stateBox-succeed");
					setTimeout(removeBox,1500);
				}else if(o.state === "alert"){
					_stateBox.addClass("ui-stateBox-alert");
					setTimeout(removeBox,2000);
				}else if(o.state === "error"){
					_stateBox.addClass("ui-stateBox-error");
					setTimeout(removeBox,2800);
				}
				_stateBox.fadeIn("fast");
				function removeBox(){
					_stateBox.fadeOut("fast").remove();
			}
		}
	});
}
);