define(["jquery","backbone","../../config","../../templates",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"],
function($,Backbone,config,templates,AsynLoadQueryTree) {
	return Backbone.View.extend({
		el:"#payment",
		template:templates.paymentInfo,
		initialize:function(param) {
			this.B2B_OID = param.B2B_OID;
			this.param = param;
			if (param.edit) {
				this.template = templates.paymentInfoMdf;
			} else {
				this.template = templates.paymentInfo;
			}
		},
		render:function(data) {
			var v = this;
			if(data.FREIGHT_TERM.toLowerCase() == "prepaid") {
				data.FREIGHT_TERM = "Prepaid";
			} else if (data.FREIGHT_TERM.toLowerCase() == "collect") {
				data.FREIGHT_TERM = "Collect";
			} else {
				data.FREIGHT_TERM = "3rd Party";
			}
			v.$el.html(v.template(data));
			//console.log(data);
			if (v.param.edit) 
			{
				if (data.FREIGHT_TERM) 
				{
					if(data.FREIGHT_TERM.toLowerCase() == "prepaid")
					{
						$("#v_Prepaid").attr('checked', 'checked');
					}
					else if (data.FREIGHT_TERM.toLowerCase() == "collect")
					{
						$("#v_Collect").attr('checked', 'checked');
					}
					else
					{
						$("#v_ThirdParty").attr('checked', 'checked');
					}
				}

				// init billName
				var paymentTree = new AsynLoadQueryTree({
				renderTo: "#ipt_bill_to_name",
				selectid: {value: data.BILL_TO_ID},
				//PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 280,
				multiselect: false,
				Async: true,
				placeholder:"Payment"
				});
				paymentTree.render();
				
	             paymentTree.on("events.Itemclick",function(treeId,treeNode){
	                  $.ajax({
	                   	url: config.addressInfo.url,
	                   	type: 'GET',
	                   	dataType: 'JSON',
	                   	data: {ps_id: treeNode.data},
	                   })
	                   .done(function(data) {
	                   		v.autocomplete(data);
	                   })
	                   .fail(function() {
	                   	console.log("error");
	                   })
	                   .always(function() {
	                   });
	             });
			}
		},
		autocomplete:function(data)
		{

			//$("#billToCountry").val(data.COUNTRY);
			$("#ipt_bill_to_state").val(data.STATE);
			$("#ipt_bill_to_city").val(data.deliver_city);
			$("#ipt_bill_to_address1").val(data.deliver_street);
			$("#ipt_bill_to_zip_code").val(data.deliver_zip_code);	
		}, 
		saveform:function() {
			var data = {};
			data.freight_term = $("#ipt_freight_term").val();
			data.bill_to_name = $("#ipt_bill_to_name").val();
			data.bill_to_address1 = $("#ipt_bill_to_address1").val();
			data.bill_to_city = $("#ipt_bill_to_city").val();
			data.bill_to_state = $("#ipt_bill_to_state").val();
			data.bill_to_zip_code = $("#ipt_bill_to_zip_code").val();
			data.bill_to_contact = $("#ipt_bill_to_contact").val();
			data.bill_to_extension = $("#ipt_bill_to_extension").val();
			data.bill_to_fax = $("#ipt_bill_to_fax").val();
			return data;
		}
	});
});