define(["jquery","backbone","../../config","../../templates","immybox","artDialog","handlebars_ext"],
function($,Backbone,config,templates,immybox) {
	return Backbone.View.extend({
		el:"#briefInfo",
		template: templates.briefInfo,
		model:new (Backbone.Model.extend({url:config.orderDetail.url}))(),
		initialize:function(param) {
			this.param = param;
			if (param.edit) {
				this.template = templates.briefInfoMdf;
			} else {
				this.template = templates.briefInfo;
			}
		},
		render: function(data) {
			var v = this;
			v.$el.html(v.template(data));
			if (v.param.edit) {
				$.getJSON(config.customer.url, function (json) {
					$('#ipt_customer_id').immybox({
						Pleaseselect:'Clean Select',
						Defaultselect:data.CUSTOMER_ID,
						choices: json
					});
				});
			}
			return v;
		},
		saveform: function() {
			var data = {};
			data.customer_dn = $("#ipt_customer_dn").val();
			data.retail_po = $("#ipt_retail_po").val();
			data.customer_id = $("#ipt_customer_id").attr("data-value");
			return data;
		}
	});
});