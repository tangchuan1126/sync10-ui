define(
["jquery",
 "backbone",
 "auto",
 "../../config",
"../../templates",
"immybox"
],
function($,Backbone,auto,config,templates,immybox)
{
	return Backbone.View.extend({
		template:templates.productEdit,
		initialize:function(options){
			this.B2B_OID = options.B2B_OID;
		},
		render:function(data)
		{
			this.data = data;
			var that = this;
			$(this.el).html(this.template(data));
			$.getJSON(config.title.url, {}, function(json, textStatus) {
				var defaultValue = "";
				if(data && data.TITLE)
				{
					var defaultTitle = "";
					$.each(json.data, function(index, val) {
						 if(data&&data.TITLE.toLowerCase() == val.TITLE_NAME.toLowerCase())
						 {
						 	defaultTitle = val.TITLE_ID;
						 }
					});

					$('#TITLE').immybox({
					Pleaseselect:'Clean Select',
					textname:"TITLE_NAME",
					valuename:"TITLE_ID",
					Defaultselect: defaultTitle,
					choices: json.data,
					change:function(e,d){
						that.compute();
					}
					});
				}
				else
				{
					$('#TITLE').immybox({
					Pleaseselect:'Clean Select',
					textname:"TITLE_NAME",
					valuename:"TITLE_ID",
					choices: json.data,
					change:function(e,d){
						that.compute();
					}
					});
				}

				//初始化clp
				if(data && data.CLP_TYPE_ID )
				{
					/**
					var isVali = true;
					if(data.B2B_DETAIL_ID){
						isVali = false;
					} **/
					var param = {"TITLE_ID":defaultTitle,"TITLE":data.TITLE,"B2B_P_NAME":data.B2B_P_NAME,"B2B_OID":data.B2B_OID};
					that.getClpType(param,data.CLP_TYPE_ID,false);
				}

			});

			return this;
		},
		events:{
			"blur .vr" : "vlt",
			"blur .compute" : "compute",
			"blur .weight" : "getAllWeight"
		},
		getClpType:function(param,selectId,isVali){
			var that = this;
			if(isVali){
				param.isVali="0";
			}else{
				param.isVali="1";
			}
			that.loading = true;
			that.computeParam = param;
			that.getOkButton().attr('disabled','true');
			that.getOkButton().html('loading...');
			that.clear();
			$.ajax({
				url: config.getClpType.url,
				type: 'GET',
				dataType: 'json',
				data: {data: JSON.stringify(param)},
			})
			.done(function(data) {
				var clpst = data.clptypes;
				if(data.product)
				{
					if(data && Array.isArray(clpst) &&  clpst.length > 0 )
					{
						var clps = clpst;
						that.initClps(clps,selectId);//that.clps = clps;
						that.product = data.product;
						that.getAllWeight();
						that._errorInfo($("#B2B_P_NAME"),true,'');
					}
					else
					{
						//有商品 ，但是没有clp
						that.product = data.product;
						that.getAllWeight();
						that._errorInfo($("#B2B_P_NAME"),false,'This product has no clps !');
					}
				}
				else
				{
					if(data.CODE=="201")
					{
						that._errorInfo($("#B2B_P_NAME"),false,'This order has this product already!');
					}
					else
					{
						that._errorInfo($("#B2B_P_NAME"),false,'Thers not exist this product!');
					}
					
				}
				
			})
			.fail(function(e) {
				that.showMessage("Get product info failed,please try again later!",'error');
			}).always(function()
			{
				that.loading = false;
				that.getOkButton().removeAttr('disabled');
				that.getOkButton().html('Confirm');
			}); 
		},
		vlt:function()
		{
			if(this.needVlt)
			{
				this.validate();
			}
		},
		html:function()
		{
			return this.el;
		},
		autoComplete:function()
		{
			auto.addAutoComplete($("#B2B_P_NAME"),config.products.url,"merge_info","p_name");
		},
		compute:function()
		{
			var that = this;
			var title = $("#TITLE").val();
			var title_id = $("#TITLE").attr("data-value");
			var p_name = $("#B2B_P_NAME").val();
			var orderQty = $("#B2B_COUNT").val();
			if(!this.isEmpty(title) && !this.isEmpty(p_name))
			{
				var param = {"TITLE_ID":title_id,"TITLE":title,"B2B_P_NAME":p_name,"B2B_OID":that.B2B_OID,"COUNT":orderQty};

				if(!that.loading && this.computeParam && this.isSame(this.computeParam ,param))
				{
					//商品没变，只是数量有改变，重新计算总重量
					if(orderQty != this.computeParam.COUNT){
						that.getAllWeight();
						 this.computeParam.COUNT = orderQty;
					}
				}
				else
				{
					//商品有变，重新查询商品
					that.product = null;
					that.getClpType(param,null);
				}

			}
		},
		initClps:function(clps,selectId)
		{
			var that = this;
			that.clps = clps;
			var choices =[];
			var defaultId = null;
			$.each(clps, function(index, obj) {
				if(index == 0)
				{
					if(selectId==null)
					{
						defaultId = obj.LPT_ID;
					}
					else
					{
						defaultId = selectId;
					}
				}
				 choices.push({text:obj.LP_NAME,value:obj.LPT_ID});
			});
			if(this.clpEle)
			{
				this.clpEle[0].Rechoicesdata(choices,defaultId);
			}
			else
			{
				this.clpEle = $('#clp').immybox({
				Defaultselect:defaultId,
				choices: choices,
				change:function(e,d){
					that.getAllWeight();
				}
				});
			}

		},
		getAllWeight:function()
		{
			var that = this;
			var qty = $("#B2B_COUNT").val();
			if(that.isEmpty(qty)){
				return;
			}
			if(this.product )
			{
				var weight =  this.product.WEIGHT;//单位？
				var clpWeight = 0;
				var clpQty = 0;
				var selectedClp = $("#clp").attr("data-value");
				var cantons = 0;
				$.each(that.clps, function(index, obj) {
					 if(obj.LPT_ID == selectedClp)
					 {
					 	clpWeight = obj.TOTAL_WEIGHT;
					 	clpQty = obj.INNER_TOTAL_PC;
					 	cantons = obj.INNER_TOTAL_LP;
					 }
				});
				//console.log("cantons",cantons);
			
				var totalWeight= 0;
				if(!that.isEmpty(weight))
				{
					//console.log("weight",weight);
					//console.log("qty",qty);
					//console.log("clpWeight",clpWeight);
					//console.log("clpQty",clpQty);
					if(clpWeight && parseInt(clpWeight) != 0 && clpQty && parseInt(clpQty) != 0)
					{
						//按照clp计算重量
						var box = Math.round(qty/clpQty);
						var surs = qty % clpQty;
						totalWeight = parseInt(box) * parseFloat(clpWeight) + surs*parseFloat(weight);

					}
					else
					{
						//数量*单个重量
						totalWeight = parseInt(qty) * parseFloat(weight);
					}
				}
				totalWeight = Math.round(totalWeight*100)/100;
				$("#BOXES").val(cantons);
				$("#TOTAL_WEIGHT").val(totalWeight);
			}
		},
		clear:function(){
			this.initClps([],null);
			$("#clp").attr('data-value', '');
			$("#clp").val('');
			$("#BOXES").val('');
			$("#TOTAL_WEIGHT").val('');
		},
		isSame:function(old,newObj)
		{
			return old.TITLE == newObj.TITLE && old.B2B_P_NAME == newObj.B2B_P_NAME ;
		},
		isEmpty:function(str){
			return  str==undefined || str ==null || str=='null' || $.trim(str) =='';
		},
		getOkButton:function(){
		 	return $(".ui-dialog-autofocus");
		},
		validate:function()
		{
			this.needVlt = true;

			if(!this.loading)
			{
				var TITLE = $("#TITLE");
				var B2B_P_NAME = $("#B2B_P_NAME");
				var B2B_COUNT = $("#B2B_COUNT");
				var BOXES = $("#BOXES");
				var TOTAL_WEIGHT = $("#TOTAL_WEIGHT");
				var PALLET_SPACES = $("#PALLET_SPACES");
				var TRAILER_FEET = $("#TRAILER_FEET");
				var CLP_TYPE_ID = $("#clp").attr('data-value');
				var LP_NAME = $("#clp").val();
				this._errorInfo($("#clp"),LP_NAME && $.trim(CLP_TYPE_ID)!='','CLP Type should not be empty!');
				this._errorInfo(TITLE,$.trim(TITLE.val())!='','Title should not be empty!');
				//this._errorInfo(B2B_P_NAME,this.findP_Name(B2B_P_NAME.val()),'Product name is null or not exist this product');
				this._errorInfo(B2B_COUNT,/^\d+$/.test(B2B_COUNT.val()),'Quantity should be integer number');
				this._errorInfo(BOXES,/^\d+$/.test(BOXES.val()),'Carton Qty should be integer number');

				if(TOTAL_WEIGHT.val() != '')
				{
					this._errorInfo(TOTAL_WEIGHT,/^([1-9]\d{0,7}|0)(\.\d{1,5})?$/.test(TOTAL_WEIGHT.val()),'Please input a number 0~99999999.99999');
				}
				else
				{
					this._errorInfo(TOTAL_WEIGHT,true,'');
				}

				if(PALLET_SPACES.val() != '')
				{
					this._errorInfo(PALLET_SPACES,/^([1-9]\d{0,7}|0)(\.\d{1,5})?$/.test(PALLET_SPACES.val()),'Please input a number 0~99999999.99999');
				}
				else
				{
					this._errorInfo(PALLET_SPACES,true,'')
				}

				return this.valiResult();
			}
			else
			{
				this.showMessage("loadig...","alert");
				return false;
			}
		},
		vltPName:function(param,callback)
		{
			//param ,B2B_P_NAME ,B2B_OID
			if(param.B2B_P_NAME != '')
			{
				$.ajax({
					url: '/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getDetailProductByPname',
					type: 'GET',
					dataType: 'json',
					data: {data: JSON.stringify(param)},
				})
				.done(function(data) {
					callback(data);
				})
				.fail(function(e) {
					console.log(e);
				})
				.always(function() {
					//console.log("complete");
				});
			}
		},
		findP_Name:function(name)
		{
			var result = false;
			$.each($(".ui-autocomplete > .ui-menu-item"), function(index, obj) {
				 if($(obj).attr("data-value") == name)
				 {
				 	result = true;
				 	return false;
				 }
			});
			if(this.data && this.data.B2B_P_NAME==name)
			{
				result = true;
			}
			return result;
		},

		_errorInfo:function($obj,isSuccess,msg)
		{
			if(isSuccess)
			{
				$obj.next().attr('title', '');
				$obj.parent().removeClass('has-error');
				$obj.parent().addClass('has-success');
				$obj.next().removeClass('glyphicon-remove');
				$obj.next().addClass('glyphicon-ok');
			}
			else
			{
				$obj.parent().removeClass('has-success');
				$obj.parent().addClass('has-error');
				$obj.next().removeClass('glyphicon-ok');
				$obj.next().addClass('glyphicon-remove');
				if(msg != '')
				{
					$(".form-control-feedback").css({"pointer-events":"auto","cursor":"help"});
					$obj.next().attr('title', msg);
				}
			}
		},
		valiResult:function()
		{
			return $(".art-dialog .has-error").length < 1;
		},
		getData:function()
		{
			return this.filterEmpty($.extend({}, this.data,this.filterEmpty({
				"TITLE": $("#TITLE").val(),
				"B2B_P_NAME":$("#B2B_P_NAME").val(),
				"RETAIL_PO":$("#RETAIL_PO").val(),
				"SO":$("#SO").val(), /*SO*/
				"MATERIAL_NUMBER":$("#MATERIAL_NUMBER").val(),
				"B2B_COUNT":$("#B2B_COUNT").val(),
				"BOXES":$("#BOXES").val(),
				"TOTAL_WEIGHT":$("#TOTAL_WEIGHT").val()==''?'0.00':$("#TOTAL_WEIGHT").val(),
				"PALLET_SPACES":$("#PALLET_SPACES").val()==''?'0.00':$("#PALLET_SPACES").val(),
				"TRAILER_FEET":$("#TRAILER_FEET").val(),
				"CLP_TYPE_ID":$("#clp").attr("data-value"),
				"LP_NAME":$("#clp").val(),
				"P_CODE":this.product.P_CODE_UPC,
				"HEIGTH":this.product.HEIGTH,
				"LENGTH":this.product.LENGTH,
				"WIDTH":this.product.WIDTH
			})));
		},
		filterEmpty:function(data)
		{
			var temp = {};
			for(var key in data)
			{
				//if(key=='LENGTH'||key=='WIDTH'||key=='HEIGTH'||key=='P_CODE') {
				//} else if(data[key]!='') {
					temp[key]=data[key];
				//}
			}
			return temp;
		},
		//stateBox 信息提示框
		showMessage:function(_content,_state){
			var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			};
			var _self = $("body"),
			_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);
			 
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
			}
		}
	});

});