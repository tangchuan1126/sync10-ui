define(["jquery","backbone","../../config","../../templates",
"oso.lib/table/js/table","artDialog",
"art_Dialog/dialog",
"./ProductEditView",
"auto",
"jquery.browser",
"/Sync10-ui/bower_components/jqgrid/js/jquery.jqGrid.js",
"/Sync10-ui/bower_components/jqgrid/js/i18n/grid.locale-en.js"],
function($,Backbone,config,templates,Table,artDialog,Dialog,ProductEditView,auto,jqGrid) {
	return Backbone.View.extend({
			el:"#detail",
			template:templates.detail,
			dataCollection:null,
			initialize:function(param) {
				var that =this;
				this.param = param;
			},
			events: {
				//"click #addDetail" : "addDetail",
				//"click #deleteDetail" : "deleteDetail"
			},
			initJqGrid:function()
			{
				var editable = true;
				var mygrid = $("#jqGrid01").jqGrid({
					sortable: true,
					url: config.orderDetail2.url,
					datatype: "json",
					width: parseInt(document.body.scrollWidth*0.98),
					height: 255,
					autowidth: false,
					shrinkToFit: true,
					loadonce:true,
					//postData: {data:B2B_OID},
					cellsubmit:'clientArray',
					jsonReader:{
						id:'B2B_OID',
						repeatitems : false
					},
					colNames:['id','Model#','Lot#','Order qty','Title','# of Boxs','Weight','Pallets'],
					colModel:[
						{name:'B2B_DETAIL_ID',index:'id',hidden:true,sortable:false},
						{name:'B2B_P_NAME',index:'Model#/Lot#',editable:editable,align:'left',editrules:{required:true},sortable:false},
						/*{name:'MATERIAL_NUMBER',index:'Lot#',editable:editable,editrules:{required:true},sortable:false},*/
						{name:'B2B_COUNT',index:'Order qty',editable:editable,editrules:{required:true},sortable:false},
						{name:'TITLE',index:'Title',editable:editable,sortable:false},
						{name:'BOXES',index:'# of Boxes',editable:editable,sortable:false},
						{name:'TOTAL_WEIGHT',index:'Weight',editable:editable,sortable:false},
						{name:'PALLET_SPACES',index:'Pallets',editable:editable,sortable:false}
						//{name:'TRAILER_FEET',index:'TRAILER_FEET',editable:editable,sortable:false}
					],
					rowNum:-1,//-1显示全部
					pgtext:false,
					pgbuttons:false,
					gridview: true,
					rownumbers:true,
					pager:'#jqGridPager01',
					sortname: 'B2B_DETAIL_ID',
					viewrecords: true, 
					sortorder: "asc",
					multiselect: true,
					errorCell: function(serverresponse, status) {
						alert(serverresponse.responseText);
					},
					//,editurl:'/Sync10/action/administrator/transport/gridEditTransportDetail.action'
					caption: "Detail List"
				});
				var canEdit = true;
				var canAdd = true;
				var canDel = true;
				var canSearch = true;
				//jQuery("#jqGrid01").jqGrid('navGrid','#jqGridPager01',{edit:canEdit,add:canAdd,del:canDel,search:canSearch},{height:200,reloadAfterSubmit:true});
				jQuery("#jqGrid01").jqGrid('saveRow',"rowid",{url:'clientArray'});
				var that = this;
				mygrid.jqGrid('navGrid','#jqGridPager01',{
					edit:canEdit,
					add:canAdd,
					del:canDel,
					search:canSearch,
					addfunc:function(){
						mygrid.jqGrid('editGridRow', "new",{
							//height:320,
							reloadAfterSubmit: false,
							beforeSubmit: function(postdata)
							{
								console.log(postdata);
								mygrid.addRowData(postdata.id, postdata);
								return false;
							}
						});
						that.autoComplete();
					}, 
					editfunc:function(rowid){
						mygrid.jqGrid('editGridRow', rowid,{
							//height:320,
							reloadAfterSubmit: false,
							beforeSubmit: function(postdata) 
							{
								console.log(postdata);
								mygrid.addRowData(postdata.id, postdata);
								return false;
							}
						});
					},
					delfunc:function(rowid){
						console.log(rowid);
					},  
					 
				});
			},
			initDetailTable:function(edit)
			{
				var opbar='<div id="opbar" style="height:22px"><button style="background-color: #f5f5f5;border:0" id="addDetail" ><span class="glyphicon glyphicon-plus"></span></button>'
				+'<button style="background-color: #f5f5f5;border:0" id="editDetail" ><span class="glyphicon glyphicon-pencil"></span></button>'
				+'<button style="background-color: #f5f5f5;border:0" id="delDetail" ><span class="glyphicon glyphicon-trash"></span></button></div>';
				var table = new Table({
				container : '#detail',
				url : config.orderDetail.url+'&data='+this.param.B2B_OID,
				title : opbar,
				//title : 'DATA LIST',
				height: 255,
				rownumbers : !edit,
				singleSelection : false,
				pagination: false,
				checkbox : edit,
				datasName: 'DATA',
				columns:[
					//{field:'B2B_DETAIL_ID',title:'ID',width:10,hidden:true},
					//name:'B2B_P_NAME',index:'Model#/Lot#'
					{field:'TITLE',title:'Title',width:100},
					{field:'B2B_P_NAME',title:'Model',width:120},
					/*{field:'MATERIAL_NUMBER',title:'Lot#',width:100},*/
					{field:'P_CODE',title:'UPC NO.',width:100},
					{field:'B2B_COUNT',title:'Order Qty',width:80},
					{field:'BOXES',title:'Carton Qty',width:70},
					{field:'PALLET_SPACES',title:'Pallet Qty',width:70},
					{field:'Config',title:'Configuration',width:150,formatter : function(data, row){
							return row.LENGTH+"x"+row.WIDTH+"x"+row.HEIGTH
						}
					},
					{field:'TOTAL_WEIGHT',title:'Weight(LBS)',width:70},
				]
				});
				$('#tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
					if(e.target.hash=="#detail") {
						table.refreshWidth();
					}
				});
				var that = this;
				$("#addDetail").click(function(event) {
					var view = new ProductEditView().render();
					Dialog({
					title: 'Add Product',
					width: 450,
					content:view.el,
					okValue:"Confirm",
					onshow:function()
					{
						view.autoComplete();
					},
					ok:function(){
						if(view.validate())
						{
							var newData = view.getData();
							//基本数据校验完成,进行深度校验
							view.vltPName({B2B_OID:that.param.B2B_OID,B2B_P_NAME:newData.B2B_P_NAME},function(data){
								
								newData.B2B_OID = that.param.B2B_OID;
								if(data.CODE=="500")
								{
									//不存在此商品
									that.showMessage("The goods does not exist, please update the index.","error");
								}else if(data.CODE=="200")
								{
									//该订单下没有该商品,可以入库
									that.addDetail(newData);
								}else if(data.CODE=="201")
								{
									//该订单下已经存在该商品,提示累加还是更新
									console.log(data);
									Dialog({
										  title: 'Attention',
										  content:'This product already exists in this order !Accumulate button will accumulate Quantity,Carton,Total Weight and Pallet Spaces !',
										  button: [{
														value: 'Accumulate',
														callback: function () {
															//累加
															console.log(newData)
															that.accumulate(newData);
														},
														autofocus: true
													},
													{
														value: 'Cancel',
													}]
									}).showModal();
								}
							});
							
							//that.addDetail(newData);
						}
						else
						{
							return false;
						}
					},
					cancel:function()
					{
						this.close().remove();
					}
					}).showModal();
				});

				$("#editDetail").click(function(event) {
					var data  = table.getSelected();
					if(data.length > 0)
					{
						if(data.length == 1)
						{
							var view = new ProductEditView().render(data[0]);
							Dialog({
							title: 'Edit Product',
							width: 450,
							content:view.el,
							okValue:"Confirm",
							onshow: function () 
							{
								view.autoComplete();
							},
							ok:function(){
								if(view.validate())
								{
									var data = view.getData();
									that.editDetail(data);
								}
								else
								{
									return false;
								}
							},
							cancel:function()
							{
								this.close().remove();
							}
							}).showModal();
						}
						else
						{
							alert("please chose one data");
						}
					}
					else
					{
						alert("please choose a data");
					}
					
				});

				$("#delDetail").click(function(event) {
					var data  = table.getSelected();
					if(data.length > 0)
					{
						Dialog({
							content:'Do you want to delete the selected records?',
							ok:function(){
								that.deleteDetail(data);
							},
							cancel:function()
							{
								this.close().remove();
							}
						}).showModal();
					}
				});
			},
			preRender:function() 
			{
				var v = this;
				if (v.param.edit)
				{
					$("#detail").empty();
					this.initDetailTable(true);
					$("#opbar").show(1000);
					// v.$el.html('<table id="jqGrid01"></table><div id="jqGridPager01"></div>');
					// this.initJqGrid();
				} else 
				{
					$("#detail").empty();
					v.$el.html('');//v.template({})
					this.initDetailTable(false);
					$("#opbar").hide();
				}
			},
			render:function()
			{
				var v = this;
				this.preRender();
				/*
					{field:'PROGRESS',title:'Progress',width:100,align:'center',formatter:
						function(data, row){
							return (data*100) + '%';
						}
					},
					*/
			},
			addDetail:function(data)
			{
				var that = this;
				var arr  = [data];
				console.log(JSON.stringify(arr));
				$.ajax({
					url: config.addDetail.url,
					type: 'POST',
					dataType: 'json',
					data: {data:JSON.stringify(arr)},
				})
				.done(function(data) {
					console.log(data);
					if(data.CODE=='200')
					{
						that.render({edit:true});
					}
					else 
					{
						that.showMessage("Save failed:"+data.INFO,"error");
					}
					
				})
				.fail(function(e) {
					console.log(e);
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			},
			editDetail:function(param)
			{
				var that = this;
				$.ajax({
					url:config.editDetail.url,
					//url: "/Sync10/action/administrator/order/updateB2BOrderItem.action",
					type: 'POST',
					dataType: 'json',
					data: {data:JSON.stringify(param)},
				})
				.done(function(data) {
					if(data.CODE=='200')
					{
						that.render({edit:true});
					}
					else 
					{
						that.showMessage("Save failed,please contact the administrator!","error");
					}
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			},
			deleteDetail:function(data)
			{
				var ids  = "";
				$.each(data, function(index, obj) {
					ids = ids + obj.B2B_DETAIL_ID+",";
				});
				ids = ids.substring(0,ids.length-1);
				var that = this;
				$.ajax({
					url:config.delDetail.url,
					type: 'POST',
					dataType: 'json',
					data: {data:ids},
				})
				.done(function() {
					that.render({edit:true});
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			},
			accumulate:function(data)
			{
				var that = this;
				$.ajax({
					url: config.accumulate.url,
					type: 'POST',
					dataType: 'json',
					data: {data:JSON.stringify(data)},
				})
				.done(function(data) {
					if(data.CODE=='200')
					{
						that.render({edit:true});
					}
					else 
					{
						that.showMessage("Save failed:"+data.INFO,"error");
					}
					
				})
				.fail(function(e) {
					console.log(e);
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			},
			_findP_Name_Local:function()
			{
				
				
			},
			autoComplete:function()
			{
				auto.addAutoComplete($("#B2B_P_NAME"),config.products.url,"merge_info","p_name");
			},
			//stateBox 信息提示框
			showMessage:function(_content,_state){
				var o =  {
					state:_state || "succeed" ,
					content:_content,
					corner: true
				};
				var _self = $("body"),
				_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
				_self.append(_stateBox);
				 
				if(o.corner){
					_stateBox.addClass("ui-corner-all");
				}
				if(o.state === "succeed"){
					_stateBox.addClass("ui-stateBox-succeed");
					setTimeout(removeBox,1500);
				}else if(o.state === "alert"){
					_stateBox.addClass("ui-stateBox-alert");
					setTimeout(removeBox,2000);
				}else if(o.state === "error"){
					_stateBox.addClass("ui-stateBox-error");
					setTimeout(removeBox,2800);
				}
				_stateBox.fadeIn("fast");
				function removeBox(){
					_stateBox.fadeOut("fast").remove();
			}
		}
	});
}
);