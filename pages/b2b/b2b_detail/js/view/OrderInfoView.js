define(["jquery","backbone","../../config","../../templates","../../../key/B2BOrderKey","dateUtil",
	"oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
	"jqueryui/datepicker",
	"jqueryui/i18n/datepicker-zh-CN",
	"jqueryui/i18n/datepicker-en-AU",
	"bootstrap.datetimepicker",
	"require_css!bootstrap.datetimepicker-css"
	],
	function($,Backbone,config,templates,B2BOrderKey,DateUitl,datetimepicker) {
		return Backbone.View.extend({
			el: "#orderInfo",
			template: templates.orderInfo,
			initialize: function(param) {
				this.B2B_OID = param.B2B_OID;
				this.param = param;
				if (param.edit) {
					this.template = templates.orderInfoMdf;
				} else {
					this.template = templates.orderInfo;
				}
			},
			render: function(data) {
				var v = this;
				data['B2B_ORDER_STATUS_NAME'] = B2BOrderKey[data.B2B_ORDER_STATUS].en;
				//console.log(data);
				v.$el.html(v.template(data));
			//	$.datepicker.setDefaults($.datepicker.regional[ "en" ]);
			//	$("#ipt_mabd").datepicker({});
			//	$("#ipt_reqdate").datepicker({});
			
			    $("#ipt_mabd").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 })
					.on('changeDate', function(ev){
						$('#ipt_reqdate').datetimepicker('setEndDate', ev.delegateTarget.value);
				});


			    $("#ipt_reqdate").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 });
				

			    $("#ipt_shipNotBefore").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 })
					.on('changeDate', function(ev){
						$('#ipt_shipNotLater').datetimepicker('setStartDate', ev.delegateTarget.value);
				});

				$("#ipt_shipNotLater").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 });
				
				/*
				.datetimepicker('setEndDate',($("#ipt_mabd").val()?$("#ipt_reqdate").val():""));
			    	.on('changeDate', function(ev){
				    	$('#ipt_mabd').datetimepicker('setStartDate', ev.delegateTarget.value);
						
			            //startTime.datetimepicker('setEndDate', ev.date);
		            });
					*/
			}
	});
});
