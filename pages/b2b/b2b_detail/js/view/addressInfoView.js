define(["jquery","backbone","../../config","../../templates","auto",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"immybox"],
function($,Backbone,config,templates,auto,AsynLoadQueryTree,immybox) {
	return Backbone.View.extend({
		el:"#addressInfo",
		template:templates.addressInfo,
		//model:new FilterModel(),
		initialize:function(param)
		{
			var v =this;
			this.B2B_OID = param.B2B_OID;
			this.param = param;
			if (param.edit) {
				this.template = templates.addressInfoMdf;
			} else {
				this.template = templates.addressInfo;
			}
		},
		render:function(data){
			var v = this;
			
			//var odata = data
			v.$el.html(v.template(data));
			if (v.param.edit) {
				/*
				$('#ipt_send_psid').immybox({
					Pleaseselect:'Clean Select',
					choices: []
				});
				*/
				/**
				var fromTree = new AsynLoadQueryTree({
					renderTo: "#ipt_send_psid",
					selectid: {value: data.SEND_PSID},
					PostData: {rootType:"Ship From"},
					dataUrl: config.shipFrom.url+"",
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder: "Ship From"
				});
				fromTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				fromTree.on("events.Itemclick",function(treeId,treeNode){
					//v.sendPsInfo(treeNode.data);
				});
				fromTree.render();

				**/

				$.getJSON(config.shipFrom.url,{"type":"1"}, function (json) {
						var rls=[];
						for (var i=0;i<json.length;i++) {
							rls.push({"value":json[i].data,"text":json[i].name});
						}
						$('#ipt_send_psid').immybox({
							Pleaseselect:'Clean Select',
							choices: rls,
							Defaultselect:data.SEND_PSID,
						});
					});

				var toTree = {};	
				if(data.ACCOUNT_ID == "-1")
				{
					//手工输入的，不设默认值
					toTree = new AsynLoadQueryTree({
					renderTo: "#ipt_receive_psid",
					//selectid: {value: data.RECEIVE_PSID},
					PostData: {rootType:"Ship To"},
					dataUrl: config.shipTo.url,
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder:"Ship To"
					});
				}
				else
				{
					toTree = new AsynLoadQueryTree({
					renderTo: "#ipt_receive_psid",
					selectid: {value: data.RECEIVE_PSID},
					PostData: {rootType:"Ship To"},
					dataUrl: config.shipTo.url,
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder:"Ship To"
					});
				}
				
				toTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				toTree.on("events.Itemclick",function(treeId,treeNode){
					v.receivePsInfo(treeNode.data);
				});
				
				toTree.render();
				v.rjson = [];
				$.ajax({
					url: config.order.url,
					type: 'GET',
					dataType: 'json',
					data: {"action": 'getAllCountry'},
				}).done(function(countrys) {
					//$("#country").empty();
					$.each(countrys, function(i){
						v.rjson.push({"text":countrys[i].C_COUNTRY,"value":countrys[i].CCID});
					});
					//console.log(v.rjson);
					$('#ipt_country').immybox({
						Pleaseselect:'Clean Select',
						Defaultselect: data.DELIVER_CCID,
						choices: v.rjson
					});
				});
				this.initState(data.DELIVER_CCID,data.DELIVER_PRO_ID);

			}
			return this;
		},
		sendPsInfo:function(id) {
			//console.log(config.addressInfo.url);
			$.getJSON(config.addressInfo.url+"&ps_id="+id, function (data) {
				if (!data.send_address1) data.send_address1="";
				if (!data.send_address2) data.send_address2="";
				$("#ipt_send_house_number").val((data.send_address1+" "+data.send_address2).trim());
				if (data.send_city) {
					$("#ipt_send_city").val(data.send_city);
				} else {
					$("#ipt_send_city").val("");
				}
				if (data.send_pro_input) {
					$("#ipt_address_state_send").val(data.send_pro_input);
				} else {
					$("#ipt_address_state_send").val("");
				}
				if (data.send_zip_code) {
					$("#ipt_send_zip_code").val(data.send_zip_code);
				} else {
					$("#ipt_send_zip_code").val("");
				}
				if (data.contact) {
					$("#ipt_send_name").val(data.contact);
				} else {
					$("#ipt_send_name").val("");
				}
				if (data.phone) {
					$("#ipt_send_linkman_phone").val(data.phone);
				} else {
					$("#ipt_send_linkman_phone").val("");
				}
			});
		},
		receivePsInfo:function(id) {
			var v = this;
			$.getJSON(config.addressInfo.url+"&ps_id="+id, function (data) 
			{
				console.log(data);
				if (data.deliver_house_number) 
				{
					$("#ipt_deliver_house_number").val(data.deliver_house_number);
				} else {
					$("#ipt_deliver_house_number").val("");
				}

				if (data.deliver_street) 
				{
					$("#ipt_deliver_street").val(data.deliver_street);
				} else {
					$("#ipt_deliver_street").val("");
				}
				
				//$("#ipt_deliver_house_number").val((data.deliver_address1+" "+data.deliver_address2).trim());
				//$("#ipt_deliver_house_number").val((data.deliver_address1+" "+data.deliver_address2).trim());
				
				if (data.deliver_city) 
				{
					$("#ipt_deliver_city").val(data.deliver_city);
				} else {
					$("#ipt_deliver_city").val("");
				}

				if (data.deliver_pro_input) {
					$("#ipt_address_state_deliver").val(data.deliver_pro_input);
				} else {
					$("#ipt_address_state_deliver").val("");
				}

				if (data.deliver_zip_code) 
				{
					$("#ipt_deliver_zip_code").val(data.deliver_zip_code);
				} else {
					$("#ipt_deliver_zip_code").val("");
				}

				if (data.deliver_contact) 
				{
					$("#ipt_b2b_order_linkman").val(data.deliver_contact);
				} else {
					$("#ipt_b2b_order_linkman").val("");
				}

				if (data.deliver_nation) 
				{
				
					v.initState(data.deliver_nation,data.deliver_pro_id);	
					$("#ipt_country").attr("data-value",data.deliver_nation);
					
					$.each(v.rjson, function(i){
						if (v.rjson[i].value == data.deliver_nation) {
							$("#ipt_country").val(v.rjson[i].text);
						}
					});

				} else {
					
					$("#ipt_country").attr("data-value","");
					$("#ipt_country").val("");
				}
				
				if (data.deliver_phone) {
					$("#ipt_b2b_order_linkman_phone").val(data.deliver_phone);
				} else {
					$("#ipt_b2b_order_linkman_phone").val("");
				}
			});
		},
		initState:function(countryId,selectid)
		{
			var that =  this;
			$.getJSON(config.cityInfo.url,{ccid:countryId},function(json)
			{
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].pro_id,"text":json[i].p_code});
				}
				if(that.states)
				{
					that.states[0].Rechoicesdata(rls,selectid);
				}
				else
				{
						var states = $('#ipt_address_state_deliver').immybox({
						Pleaseselect:'Clean Select',
						Pleaseselectclick:function(e)
						{
							$('#ipt_address_state_deliver').attr("data-value","");
						},
						choices: rls,
						Defaultselect:selectid,
						change:function(e,d)
						{
							 	//console.log(e);
									//console.log(d);
						}
						});
					that.states = states;
				}
			

			});
		},
		saveform:function() {
			var data = {};
			data.send_psid = $("#ipt_send_psid").attr("data-value");
			data.send_house_number = $("#ipt_send_house_number").val();
			data.send_city = $("ipt_send_city").val();
			data.address_state_send = $("ipt_address_state_send").val();
			data.send_zip_code = $("ipt_send_zip_code").val();
			data.send_name = $("#ipt_send_name").val();
			data.send_linkman_phone = $("#ipt_send_linkman_phone").val();
			data.receive_psid = $("#ipt_receive_psid").attr("data-val");
			data.deliver_house_number = $("#ipt_deliver_house_number").val();
			data.deliver_street = $("#ipt_deliver_street").val();
			data.deliver_city = $("#ipt_deliver_city").val();
			data.address_state_deliver = $("#ipt_address_state_deliver").val();
			data.deliver_zip_code = $("#ipt_deliver_zip_code").val();
			data.b2b_order_linkman = $("#ipt_b2b_order_linkman").val();
			data.b2b_order_linkman_phone = $("#ipt_b2b_order_linkman_phone").val();
			console.log(data);
			return data;
		}
	});
});