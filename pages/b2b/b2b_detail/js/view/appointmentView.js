define(["jquery","backbone","../../config","../../templates","dateUtil",
	"bootstrap.datetimepicker",
	"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,templates,DateUitl,datetimepicker) {
	var lang = "en";
	var format = "";
	return Backbone.View.extend({
		el:"#appointment",
		template:templates.appointmentInfo,
		initialize:function(param) {
			this.B2B_OID = param.B2B_OID;
			this.param = param;
			
			$.datepicker.setDefaults($.datepicker.regional[ "en" ]);
			format = 'mm/dd/yyyy hh:00';
			lang = "en";
			//$.datepicker.setDefaults($.datepicker.regional[ "en" ]);
			if (param.edit) {
				this.template = templates.appointmentInfoMdf;
			} else {
				this.template = templates.appointmentInfo;
			}
		},
		render: function(data) {
			var v = this;
			v.$el.html(v.template(data));
			if (v.param.edit) {
				//alert(format);
				//$("#ipt_pickup_appointment").datepicker({});//.datetimepicker({ format: 'mm/dd/yyyy HH' });
				$('#ipt_pickup_appointment').datetimepicker({
					startDate: new Date(), autoclose: 1, minView: 1,forceParse: 0, format:'mm/dd/yyyy hh:00' 
				});
				//$("#ipt_etd").datepicker({});
				//$("#ipt_eta").datepicker({});
				//$("#ipt_mabd").datepicker({altFormat:'MM/dd/yy'});
				//$("#ipt_req").datepicker({});
				$.getJSON(config.freightType.url, function (json) {
					$('#ipt_freight_type').immybox({
						Pleaseselect:'Clean Select',
						Defaultselect: data.FREIGHT_CARRIER,
						choices: json
					});
				});
				$.getJSON(config.carrier.url,{from_ccid:11036, to_ccid: data.DELIVER_CCID}, function (json) {
					var t = [];
					/*$.each(json, function(i){
						if (json[i].FR_ID==26) t.push({"value":json[i].FR_ID,"text":json[i].FR_COMPANY});
					});
					*/
					$.each(json, function(index, obj) {
						obj.text = obj.value + " - "+ obj.text;
					});
					$('#ipt_carrier').immybox({
						Pleaseselect:'<i>Clean Select</i>',
						textname:"text",
						valuename:"value",
						Defaultselect: data.CARRIERS,
						choices: json
					});
				});
			}
		},
		saveform:function() {
			var data = {};
			data.pickup_appointment = $("#ipt_pickup_appointment").val();
			data.etd = $("#ipt_etd").val();
			data.eta = $("#ipt_eta").val();
			data.mabd = $("#ipt_mabd").val();
			data.requested_date = $("#ipt_req").val();
			return data;
		}
	});
});