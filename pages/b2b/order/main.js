"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config){
	require(["jquery",
		"./js/view/advanceSearchView",
		"./js/view/commonToolsView",
		"./orderListMain",
		"./js/view/slideView.js",
		"auto",
		"bootstrap",
		],
	function($,SearchView,CommonToolsView,OrderListView,SlideView,auto) {
		function showDetail(url) {
			window.onbeforeunload = function() {parent.document.getElementById('mainFrm').setAttribute("cols","240,14,*");}
			parent.document.getElementById('mainFrm').setAttribute("cols","0,0,*");
			var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
			var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
			artDialog.open(url, {title: "Order detail", width:width, height:(height-60), lock: true,opacity: 0.3,fixed: true, Parentwindowscrooll: false, close: function(){
					parent.document.getElementById('mainFrm').setAttribute("cols","240,14,*");
					window.refreshWindow();
					window.onbeforeunload = function() {}
				}
			});
		}
		$.ajax({
			url: config.session.url,
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) 
		{ 
				function getQueryString(name)
				{
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var oid = getQueryString("oid");
			    var psId = data.attributes.adminSesion.ps_id;
			    var queryOptions = {el: "#data", url:config.keySearchURL.url,queryCondition:{"send_psid":psId,pageSize:10, "action":"fillterB2BOrder"}};
			    
			    if(oid != null && oid != "")
			    {
			    	queryOptions ={el: "#data", url:config.keySearchURL.url ,queryCondition:{"search_key":oid,"search_mode":1,"action":"fastSearch"}};
			  		$("#tabs li:first-child a").tab('show');
			    }
				
				var resultView  = new OrderListView(queryOptions);
				//resultView.collection.pageCtrl.pageSize= 10;
				resultView.on("events.Error", function(e) {
					$(".loding,.LodingModal").hide();
					alert("无法从服务器获取数据,请稍后在试。");
				});
				var sv = new SearchView({"send_psid":psId,resultView:resultView}).render();
				new CommonToolsView({"send_psid":psId,resultView:resultView}).render({"oid":oid});
				//new SlideView().render();
				$("#createOrder").bind('click', {}, function(event) {
					artDialog.open(config.createOrder.url, {title:"Create Order", width:'850px',height:'500px', lock:true, opacity:0.3, fixed:true,
						close:function() {
							var orderId = artDialog.data("orderId");
								//console.log(orderId);
							if(orderId != undefined && orderId != null  && orderId !="") {
								var url = config.order.url+"?B2B_OID="+orderId;
								showDetail(url);
							}
							artDialog.data("orderId","");
						}
					});
				});
				$("#importOrder").bind('click', {}, function(event) {
					var uri = config.importOrder.url;
					artDialog.open(uri , {title: "Import Order",width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true,
					
					});
				});
				$("#updateOrder").bind('click', {}, function(event) {
					var uri = config.updateOrder.url;
					artDialog.open(uri , {title: "Load&APPT",width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true,
					});
				});
				window.refreshWindow = function() {
					sv.filter();
				};
				$('#inventoryControl').click(function(){
					var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
					var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
					artDialog.open("../order2/index2.html" , {title: "Inventory Commitment", width:width, height:(height-60), lock: true,opacity: 0.3,fixed: true, Parentwindowscrooll: true});
					//slide.open();
				});
		})
		.fail(function() 
		{
			alert("please login in !");
		});
		
	});
});