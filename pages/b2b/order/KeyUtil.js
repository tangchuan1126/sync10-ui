define(["dateUtil"], function(DateUtil) 
{
	var key =
	{
		locallanguage: DateUtil.getLanguage(),
		get:function(key,keyId)
		{
			var result ="No KEY";
			if(key[keyId] )
			{
				result = key[keyId][this.locallanguage];
				if(result == undefined || result =="" )
				{
					result="No KEY";
				}
			}
			return result;
		}
	}
	return key;
});