define(["JSONTOHTML",
	"dateUtil",
	"./Map",
	"./KeyUtil",
	"../key/B2BOrderKey",
	"../key/B2BOrderLogTypeKey"
	], function(JSONTOHTML,
		DateUtil,
		Map,
		Key,
		B2BOrderKey,
		B2BOrderLogTypeKey
	) {
	console.info( 'quiet' );
	//第一次创建时带上表头
	var _table = 
	{
		thead:[
			{
				tag: "table",
				class: "checkin_box",
				children: [{
					tag: "thead",
					children: [{
						tag: "tr",
						html: function(obj, index)
						{
							var thdata = obj.children[0].children;
							return (JSONTOHTML.transform(thdata.children, _table.th));
						}
					}]
				}, {
					tag: "tbody",
					html: ""
				}]
			},
			{
				tag: "div",
				class: "tfootpage",
				html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
			},
			{
				tag: "div",
				class: "SelectMorebox"
			}
		],
		th:
		{
			tag: "th",
			html: "${title}",
			style: function(obj, index) {
				if (index==0) {
					return "width:25%";
				} else if (index==2) {
					return "width:26%";
				} else if (index==3) {
					return "width:19%";
				} else{
					return "";
				}
			}
		}
	};
	
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = 
	{
		int: function(jsons,View_control) 
		{
			//try {
				var start = new Date().getTime();
				//初始化全局配置
				_tbody.idAttribute = View_control.idAttribute;
				_tbody.authIdAttribute = View_control.authIdAttribute;
				_tbody.headallobj = View_control.head;
				_tbody.footer = View_control.footer;
				var data = new Map(_tbody.idAttribute,jsons.DATA);
				var auths = new Map(_tbody.authIdAttribute,jsons.AUTHS);
				var pageSize=jsons.PAGECTRL.pageSize;
				//按表头转换成行和列
				var __tr=[];
				for (var tri = 0; tri < jsons.DATA.length; tri++) 
				{
					var rowData = jsons.DATA[tri];
					var __td = [];
					for (var headkey in _tbody.headallobj) 
					{
						var column = _tbody.headallobj[headkey];
						var columnFiled = column.field;
						var columnData = eval("({" + columnFiled + ":rowData})");
						__td.push(columnData);
					}
					__tr.push({"rowId":rowData[_tbody.idAttribute],"children": __td});
					if(tri >= pageSize)
					{
						break;
					}
				}
				//拼装tr,trfooter，
				var __rowsHtml = "";
				for (var trkey in __tr)
				{
					var _row = __tr[trkey];
					var rowId = _row.rowId;
					__rowsHtml += (JSONTOHTML.transform(_row, _tbody.tr));
					var authCon = auths.get(rowId);
					if (data.get(rowId).B2B_ORDER_STATUS=="2") {
						authCon.BUTTON_ROLLBACK = true;
					} else if (data.get(rowId).B2B_ORDER_STATUS=="1" || data.get(rowId).B2B_ORDER_STATUS=="10") {
						authCon.BUTTON_COMMIT = true;
						authCon.BUTTON_CANCEL = false;
						authCon.BUTTON_DELETE = true;
					}
					var trfooterdata = {
						"colspan": _tbody.headallobj.length,
						"children": _tbody.footer,
						"rowId": rowId,
						"rowData": data.get(rowId),
						"auth": authCon
					};
					//将行值传给按钮行，用于判断按钮是否需要显示
					__rowsHtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
				}
				data = null;
				auths = null;
				var end = new Date().getTime();
				//console.log(end-start);
				return __rowsHtml;
			/*}catch(e)
			{
				alert(e);
				alert("数据解析异常，请联系管理员");
			}*/
		},
		keyname: function(obj) 
		{
			//列对象，只有一个key,那就是列名对应的字段
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr:
		{
			"tag": "tr",
			"id":function(obj)
			{
				return obj.rowId;
			},
			//行对象ID
			"data-itmeid":function(obj){
				return _tbody.idAttribute+"|"+obj.rowId;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				//chuldren :列数组[]
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter:
		{
			tag: "tr",
			"id":function(obj) {
				return "foot_"+obj.rowData[_tbody.idAttribute];
			},
			class: "TFOOTbutton",
			html: function(obj, index) {
				var rowData = obj.rowData,
					auth = obj.auth,
					tfootData = _tbody.idAttribute+"|"+obj.rowId,
					trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tfootData+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) {
					var itmea = obj.children[akey];
					if(_utils.isHasButton(itmea[0],auth)) {
						var name = itmea[1];
						var calss="";
						/*
						if(itmea[0].indexOf("cancel")>=0) {
							calss = " icon remove";
						}
						if(itmea[0].indexOf("delete")>=0) {
							calss = " icon remove";
						}
						*/
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons'+calss+'">' + name+ '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		td:
		{
			tag: "td",
			class: function(obj, index) {
				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				if("basicInfo" == keynames)
				{
					classname = "td_" + "TRAILER";
				}
				else
				{
					classname = "td_" + keynames;
				}
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "inherit";
				}
				return valignsing;
			},
			html: function(obj, index) {
				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);
				if (keynames != "" && typeof keynames != "undefined")
				{
					if(typeof keynames == "string")
					{
						if (typeof cellTemplate[keynames] != "undefined" && cellTemplate[keynames] != "") 
						{
							retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
						}
					}
					else if(typeof keynames == "object")
					{
						retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
					}
				}
				return retuhtml;
			}
		},
		updateOneRow:function(parentId,rowData,auths)
		{
			var __td = [];
			for (var headkey in this.headallobj) {
				var column = this.headallobj[headkey];
				var columnFiled = column.field;
				var columnData = eval("({" + columnFiled + ":rowData.DATA})");
				__td.push(columnData);
			}
			var trfooterdata = {
				"colspan": this.headallobj.length,
				"children": this.footer,
				"rowId": parentId,
				"rowData": rowData.DATA,
				"auth": rowData.AUTHS
			};
			$("#"+("foot_"+parentId)).remove()
			$("#"+parentId).empty().html(JSONTOHTML.transform(__td,this.td)).after(JSONTOHTML.transform(trfooterdata,this.trfooter));
		}
	};
	var cellTemplate =
	{
		basicInfo: {
		tag: "div",
			class: "TRAILER",
			children:
			[{
			tag: "fieldset",
			class: "TRAILER",
			children: [
			{
				tag:"span",
				class:"fieldset_tfoot",
				html:function(obj,index)
				{
					return "";//"<em>总容器："+obj.CONTAINER_COUNT+"</em>";
				}
			},
			{
				tag: 'legend',
				class: "td_legend",
				children:
				[
					{"tag":"span","class":"lightlink buttonallevnts","data-eventname":"orderDetail","html":"B${B2B_OID}"},
					{
						"tag":"span",
						"class":function(obj,index){return _utils._stateClass( obj.B2B_ORDER_STATUS);},
						"html":function(obj,index){
							return Key.get(B2BOrderKey,obj.B2B_ORDER_STATUS);
						}
					},
					{
						"tag": "span",
						"style": function(obj,index) {
							if (obj.B2B_ORDER_STATUS=='5'|| obj.B2B_ORDER_STATUS=='6') {
								return "";
							}
							return "display:none";
						},
						"html": "Stah Location:${STAG_AREA}"
					}
				]
			}, 
			{
				 tag: "div",
				 class: "Tractor_ulitme",
				 children: 
				 [
					{tag: "ul",children:
						[
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Customer:"},
									{"tag":"span","class":"valstyle","html":"${CUSTOMER_NAME}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"DN :"},
									{"tag":"span","class":"valstyle","html":"${CUSTOMER_DN}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"PO :"},
									{"tag":"span","class":"valstyle mutiValStyle","html":function(obj,index){
										if(Object.prototype.toString.call(obj.PO) == '[object Array]')
										{	
											var title = "";
											$.each(obj.PO, function(index, po) {
												if(po!="")title+=po+",";
											});
											if(title.length>0)title=title.substring(0,title.length-1);
											obj.POtitle=title;
											return title;
										}
										else
										{
											return "" ;
										}

									},title:"${POtitle}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"SO :"},
									{"tag":"span","class":"valstyle mutiValStyle","html":function(obj,index){
										if(Object.prototype.toString.call(obj.SO) == '[object Array]')
										{	
											var title = "";
											$.each(obj.SO, function(index, so) {
												if(so!="")title+=so+",";
											});
											if(title.length>0)title=title.substring(0,title.length-1);
											obj.SOtitle=title;
											return title;
										}
										else
										{
											return "" ;
										}
									},title:"${SOtitle}"}
								]
							},
							{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":"Del.Window :"},
								{"tag":"span","class":"valstyle ","html":"${SHIP_NOT_BEFORE} ~ ${SHIP_NOT_LATER}"}
								//function(obj,index)
								//{	
								//	return _utils.handleEmpty(obj.SHIP_NOT_BEFORE,"") +" ~ "+_utils.handleEmpty(obj.SHIP_NOT_LATER,"");

									//"${SHIP_NOT_BEFORE} ~ ${SHIP_NOT_LATER}"
								//}}
							]
							},
							{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":"MABD :"},
								{"tag":"span","class":"valstyle","html":"${MABD}"}
							]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Req.Ship Date :"},
									{"tag":"span","class":"valstyle","html":"${REQUESTED_DATE}"}
								]
							},
							
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Order Date :"},
									{"tag":"span","class":"valstyle","html":"${CUSTOMER_ORDER_DATE}"}
								]
							},
							{tag: "li",children: [
							{"tag":"span","class":"namestyle","html":"Ship Date :"},
							{"tag":"span","class":"valstyle","html":"${SHIPPED_DATE}"}
							]},
							/*
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Creator :"},
									{"tag":"span","class":"valstyle","html":"${CREATE_ACCOUNT}"}
								]
							}, **/
					]}
				 ]
			}
			]
		},
		{tag:"div",style:"height:5px",html:""},
		{
			tag: "ul",
			id: "note_${B2B_OID}",
			style: function(obj,index) {
				if (obj.REMARK) {
					return "";
				} else {
					return "display:none"
				}
			},
			class: "NOTES",
			//html: "${REMARK}",
			children: [
				{
					tag:"li",children:
					[
						{"tag":"span","class":"namestyle red","html":"Note:"},
						{"tag":"span","class":"valstyle","style":"width:65%","html":"${REMARK}"}
					]
				}
			]
		}
			]
		},
		shipInfo:
		{
			tag: "div",
			class: "lineContainer",
			html:function(obj,index)
			{
				var html = "" ;
				var box1px = "";//<div class='box1px'></div>";
				if(obj.LINES && obj.LINES.length >0)
				{
					html = '<div class="lineItem"><div class="col-xs-3"><strong>Title</strong></div><div class="col-xs-6"><strong>Model</strong></div><div class="col-xs-3 no-padding"><strong>Order Qty</strong></div></div>';
					for(var i = 0 , len = obj.LINES.length ;i < len;i++)
					{
						if(i < 3)
						{
							html+= JSONTOHTML.transform(obj.LINES[i],cellTemplate._linesInfo);
						}
						else
						{
							html += '<div class="moreIitem"><span class=" buttonallevnts" data-eventname="events.moreOrderLines"><a class="more">See Details &gt;&gt;</a></span></div>';
							break;
						}
						
						//if(i != length - 1)
						//{
							//html+= box1px;//
						//}
						
					}

					//html+='<span class="total"><div class="col-xs-6">Total Boxs : '+obj.TOTAL_BOXES+'</div><div class="col-xs-6">Total Pallets : '+obj.TOTAL_PALLETS+'</div></span>';
					//html+= '<span class="total"><div class="col-xs-12">Total Weight : '+obj.TOTAL_WEIGHT+' '+obj.B2B_WEIGHT_UNIT+'</div></span>';
					html+=JSONTOHTML.transform(obj,cellTemplate._lineTotal);
				}
				else
				{
					html = "<div class='noData'>No Data</div>";
				}
				
				return html;
			}
			/*
			children:
			[
				tag:"ul",
				class:"lines",
				children:
				[
					{tag:"li",children:
					[
						{"tag":"span","class":"namestyle","html":"Hub:"},
						{"tag":"span","class":"valstyle","html":"${SEND_PSNAME}"}
					]},
					{tag:"li",children:
					[
						{"tag":"span","class":"namestyle","html":"Ship-to Name:"},
						{"tag":"span","class":"valstyle","html":"${RECEIVE_PSNAME}"}
					]},
					
					{tag:"li",children:
					[
						{"tag":"span","class":"namestyle","html":"Ship-to Address:"},
						{"tag":"span","style":"max-width:62%","class":"valstyle","html":"${DELIVER_HOUSE_NUMBER} ${DELIVER_STREET}"}
					]},
					{tag:"li",children:
					[
						{"tag":"span","class":"namestyle","html":"City,State Zip:"},
						{"tag":"span","class":"valstyle","html":function(obj,index){
							return _utils.handleEmpty(obj.DELIVER_CITY," ")+","+_utils.handleEmpty(obj.ADDRESS_STATE_DELIVER," ")+" "+_utils.handleEmpty(obj.DELIVER_ZIP_CODE,"");}}
					]},
					{tag:"li",children:
					[
						{"tag":"span","class":"namestyle","html":"Country:"},
						{"tag":"span","class":"valstyle","html":function(obj,index){
							return _utils.handleEmpty(obj.DELIVER_CCID_NAME,"");}}
					]},
					
					{tag:"div",class:"box1px",html:""},
					{tag: "li",children:
						[
							{"tag":"span","class":"namestyle","html":"Freight Term:"},
							{"tag":"span","class":"valstyle","html":"${FREIGHT_TERM}"}
						]
					},
					{
						tag:"li",
						"style": function(obj,index) 
						{
							if (obj.FREIGHT_TERM!='ThirdParty' && obj.FREIGHT_TERM!='Third Party') {
								return "display:none;";
							}
							return "";
						},
						children: [
							{"tag":"span","class":"namestyle","html":"Bill To Name:"},
	 						{"tag":"span","class":"valstyle","html":"${BILL_TO_NAME}"}
						]
					},
					{
						tag:"li",
						"style": function(obj,index)
						{
							if (obj.FREIGHT_TERM!='ThirdParty' && obj.FREIGHT_TERM!='Third Party') {
								return "display:none;";
							}
							return "";
						},
						children: 
						[
							{"tag":"span","class":"namestyle","html":"Bill-To Address:"},
	 						{"tag":"span","style":"max-width:62%","class":"valstyle","html":"${BILL_TO_ADDRESS1}"}
						]
					},
					{
						tag:"li",
						"style": function(obj,index) {
							if (obj.FREIGHT_TERM!='ThirdParty' && obj.FREIGHT_TERM!='Third Party') {
								return "display:none;";
							}
							return "";
						},
						children: 
						[
							{"tag":"span","class":"namestyle","html":"Bill-To City,State Zip:"},
							{
								"tag":"span",
								"class":"valstyle",
								"html":function(obj,index)
								{
									return _utils.handleEmpty(obj.BILL_TO_CITY," ")+","+_utils.handleEmpty(obj.BILL_TO_STATE," ")+" "+_utils.handleEmpty(obj.BILL_TO_ZIP_CODE,"");
								}
							}
						]
					}
				
				]}
			] */
		},
		_linesInfo:
		{
			tag:"div",
			class:"lineItem",
			children:[
			  {
			  	tag:"div",
			  	class:"col-xs-3 titleString",
			  	title:function(obj,index)
			  	{
			  		return _utils.handleEmpty(obj.TITLE,"");
			  	},
			  	html:"${TITLE}"
			  },
			  {
			  	tag:"div",
			  	class:"col-xs-6 nameString",
			  	title:function(obj,index)
			  	{
			  		return _utils.handleEmpty(obj.B2B_P_NAME,"");
			  	},
			  	html:"${B2B_P_NAME}",
			  //	html:function(obj,index)
			  //	{
			  		
			  		//return _utils.handleEmpty(obj.B2B_P_NAME,"13");
			  //	}
			  },
			 
			  {
			  	tag:"div",
			  	class:"col-xs-3 no-padding",
			  	title:function(obj,index)
			  	{
			  		return _utils.handleEmpty(obj.B2B_COUNT,"");
			  	},
			  	html:"${B2B_COUNT}"
			  }
			]
		},
		_lineTotal:
		{
			tag:"span",
			class:"total col-xs-12",
			children:[
				{
				tag:"p",
				style:"font-style:normal",
				//html:"Total : ${TOTAL_BOXES} carton(${TOTAL_PALLETS} pallets) ,  ${TOTAL_WEIGHT} ${B2B_WEIGHT_UNIT} LBS"
					html:"<span class='totalItem'>Total Carton</span> : ${TOTAL_BOXES} "
				},
				{
				tag:"p",
				style:"font-style:normal",
				html:"<span class='totalItem'>Total Pallet</span> : ${TOTAL_PALLETS}"
				},
				{
				tag:"p",
				style:"font-style:normal",
				html:"<span class='totalItem'>Total Weight</span> : ${TOTAL_WEIGHT} LBS"
				}
			]
		},
		transportInfo: 
		{
				 tag: "div",
				 class: "Log_container transportion",
				 children:
				 [
				 	{tag:"ul",children:
				 	[
				 		{tag:"li",children:
						[
							{"tag":"span","class":"namestyle","html":"Hub :"},
							{"tag":"span","class":"valstyle","html":"${SEND_PSNAME}"}
						]},
						{tag:"li",children:
						[
							{"tag":"span","class":"namestyle","html":"Ship-to Name :"},
							{"tag":"span","class":"valstyle longString","title":"${SHIP_TO_PARTY_NAME}","html":"${SHIP_TO_PARTY_NAME}"}
						]},
						{tag: "li",children:
						[
							{"tag":"span","class":"namestyle","html":"Freight Term :"},
							{"tag":"span","class":"valstyle textUp","html":function(obj,index){
								//"${FREIGHT_TERM}"
								var ft = obj.FREIGHT_TERM;
								if(ft && ft.toLowerCase() !="prepaid" && ft.toLowerCase() !="collect")
								{
									return "3rd Party";
								}
								else
								{
									return ft?ft.toLowerCase():'';
								}
							}}
						]
						},
						{tag:"li",
						"style": function(obj,index) 
						{
						 	var ft = obj.FREIGHT_TERM;
							if(ft && ft.toLowerCase() =="prepaid" || ft.toLowerCase() =="collect")
							{
								return "display:none;";
							}
							return "";
						},
						children: [
							{"tag":"span","class":"namestyle","html":"Bill To Name:"},
	 						{"tag":"span","class":"valstyle longString","title":"${BILL_TO_NAME}","html":"${BILL_TO_NAME}"}
						]
					},
						{tag:"div",class:"box1px",html:""},
						
						{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"SCAC :"},
 							{"tag":"span","class":"valstyle longString","title":"${CARRIER}",html:function(obj,index){
 								if(!_utils.isEmpty(obj.CARRIERS))
 								{
 									if(!_utils.isEmpty(obj.CARRIER))
 									{	
 										return obj.CARRIERS+" - "+obj.CARRIER;
 									}
 									else
 									{
 										return obj.CARRIERS;
 									}
 									
 								}
 								else
 								{
 									return "";
 								}
 							}}
 							//"${CARRIERS} - ${CARRIER}"
				 		]},
						{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"Freight Type :"},
 							{"tag":"span","class":"valstyle","html":"${FREIGHT_CARRIER}"}
				 		]},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":"MBOL :"},
								{"tag":"span","class":"valstyle","html":"${MBOL}"}
							]
						},
						{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"Load NO. :"},
 							{"tag":"span","class":"valstyle","html":"${LOAD_NO}"}
				 		]},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":"Appointment :"},
								{
									"tag":"span",
									"class":"valstyle",
									"html": function(obj,index) {
										return obj.PICKUP_APPOINTMENT?obj.PICKUP_APPOINTMENT:(obj.DELIVERY_APPOINTMENT?obj.DELIVERY_APPOINTMENT:"");
									}
								}
							]
						},
						{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"Pro NO. :"},
 							{"tag":"span","class":"valstyle","html":"${B2B_ORDER_WAYBILL_NUMBER}"}
				 		]},
						{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"Seal :"},
 							{"tag":"span","class":"valstyle","html":"${SEAL_NO}"}
				 		]}
				 	]}
				 ]
		},
	
		FLOWS: 
		{
			tag: "div",
			class: "Log_container flows",
			html:function(obj,index)
			{
				var html = "" ;
				var box1px = "<div class='box1px'></div>";
				var length = obj.FLOWS.length;
				_.map(obj.FLOWS,function(flow,index)
					{
						var fontClass = _utils._flowClass(flow,obj);
						var  res = _utils._flowState(flow,obj);

						//设置显示状态
						flow.STATUS = "<font class='"+fontClass+"'  >"+res.STATUS+"</font>";
						flow.OVER=res.OVER;
						//设置完成天数
						html+= JSONTOHTML.transform(flow,cellTemplate._ulFlows);
						if(index != length - 1)
						{
							html+= box1px;
						}
					});
				return html;
			}
		},
		_ulFlows:
		{
			tag:"ul",
			class:"flows",
			children:
			[
				{tag:"li", children:
				[
					{tag:"span",class:"namestyle",html:"${TYPENAME}:"},
					{tag:"span",class:"valstyle",html:"${STATUS}"},
					{tag:"span",class:"rightstyle",html:"${OVER}"}
				]},
				{tag:"li", children:
				[
					{tag:"span",class:"flowname",html:"${EMPLOYE_NAMES}"}
				]}
			]
		},
		_liList:
		{
			tag: "li",
			html: function(obj, index) 
			{
				return '<span class="namestyle">'+obj.TYPE+':</span><span class="valstyle">' + obj.EMPLOYE_NAMES + '</span>';
			}
		},
		LOGS: 
		{
			tag: "div",
			class: "Log_container",
			html: function(obj, index) 
			{
				var ulsring = "";
				var arryjson = obj.LOGS;
				var box1px = "<div class='box1px'></div>";
				if (arryjson) {
					for (var _key = 0; _key < arryjson.length; _key++)
					{
						if (_key == 3) 
						{
							ulsring = ulsring +'<span class=" buttonallevnts logMore" data-eventname="events.moreLogs"><a class="more">More Logs &gt;&gt;</a></span>';
							break;
						}
						var arryitmes = arryjson[_key];
						var ul = JSONTOHTML.transform(arryitmes,cellTemplate._ulLogs);
						if (ulsring != "" && typeof ulsring != "undefined") 
						{
							ulsring = ulsring + box1px + ul;
						} else {
							ulsring = ul;
						}
					}
				}
				return ulsring;
			}
		},
		_ulLogs:
		{
			tag:"ul",
			children:
			[
				{tag:"li",children:[
					{tag:"span",class:"opreationName",html:"${B2BER}"},
					{tag:"span",class:"logvaluestyle spanToA","data-eventname":"events.moreLogs",html: function(obj,index) {
							//return B2BOrderLogTypeKey[obj.B2B_TYPE][DateUtil.getLanguage()]+" ";
							return B2BOrderLogTypeKey[obj.B2B_TYPE]['en']+" ";
						}
					}
				]},
				{tag:"li",children:[
					{tag:"span",class:"rightstyle opreationTime",html:"${B2B_DATE}"}
					//{tag:"span",class:"",html:"${B2B_CONTENT}"}
				]} 
			]
		},
	};
	var _utils =
	{
		handLongStr:function(str,len)
		{
			if(str == undefined || str ==null)
			{
			   return "";
			}
			if(str.length > len)
			{
			  return str.substr(0,len-1)+"...";
			}
			else
			{
				return str;
			}
		},
		isEmpty:function(str)
		{
			return str == "" || str == undefined || str.length == 0 || str == "null" || str == null;
		},
		handleEmpty: function(str,defaultValue) {
			if(str == "" || str == undefined || str.length == 0 || str == "null" || str == null) {
				return defaultValue;
			} else {
				return str;
			}
		},
		isHasButton:function(itmea,auth) {
			var res = false;
			if (auth) {
			
				if(itmea =="events.cancel") {
					if (auth["BUTTON_CANCEL"]) {
						res = true;
					}
				} else if(itmea == "events.document") {
					res = true;
				} else if(itmea =="events.photo") {
					res = true;
				} else if(itmea =="events.note") {
					res = true;
				} else if(itmea =="events.commit") {
					if (auth["BUTTON_COMMIT"]) res = true;
				} else if(itmea =="events.rollback") {
					if (auth["BUTTON_ROLLBACK"]) res = true;
				} else if(itmea =="events.delete") {
					if (auth["BUTTON_DELETE"]) res = true;
				} else if(itmea =="events.app") {
					if (auth["BUTTON_APP"]) res = true;
				} else {
					res = false;
				}
			}
			//res = true;
			return res;
		},
		_stateClass:function(state)
		{
			//运输流程
			var tempclass="";
			if(state == "1")
			{
				tempclass=" colorbule";
			}
			else if(state =="3")
			{
				tempclass=" colorred";
			}
			else if (state =="2")
			{
				tempclass=" colorgreen";
			}
			else
			{
				tempclass="";
			}
			tempclass = tempclass +" status";
			return tempclass;
		}
	};
	return {
		tbody: _tbody,
		table: _table
	};
});