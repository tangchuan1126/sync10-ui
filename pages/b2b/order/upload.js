/**
 * gaowenzhen@msn.com
 * @authors //127.0.0.1/Sync10-ui/lib/jquery-file-upload-extension
 * @date    2014-12-04 09:42:50
 * @version 0.0.1
 */
;
(function(root, factory) {

	var fileupload = "jquery-file-upload/jquery.fileupload";
	var fileupload_ui = "jquery-file-upload/jquery.fileupload-ui";
	 libpat="/Sync10-ui/lib/jquery-file-upload-extension/";

	if (typeof define === 'function' && define.amd) {
		//add config
		var _paths = requirejs.s.contexts._.config.paths;
		var _mypat = _paths["jquery-file-upload"];
		libpat=_paths["oso.lib"]+"/jquery-file-upload-extension/";
		_paths["jquery.ui.widget"] = _mypat + "/vendor/jquery.ui.widget";
		_paths["tmpl"] = _paths["oso.lib"] + "/jquery-file-upload-extension/Relyon/tmpl.min";
		var __bower = _mypat.replace(/\/jquery-file-upload\/.*/, "/");
		var __ildat = __bower + "blueimp-load-image/js/";
		_paths["load-image"] = __ildat + "load-image";
		_paths["load-image-meta"] = __ildat + "load-image-meta";
		_paths["load-image-exif"] = __ildat + "load-image-exif";
		_paths["load-image-ios"] = __ildat + "load-image-ios";
		_paths["canvas-to-blob"] = __bower + "blueimp-canvas-to-blob/js/canvas-to-blob.min";
		//css
		var __uploadcss = _mypat.replace(/\/js.*/, "/css");
		_paths["fileupload-css"] = __uploadcss;
		//---------------------

		// AMD. Register		
		define(['jquery', fileupload, fileupload_ui], factory);

	} else if (typeof uploadPlugin === 'object') {
		// CommonJS
		module.uploadPlugin = factory(
			require('jquery'),
			require(fileupload),
			require(fileupload_ui)
		);

	} else {
		// Browser globals
		root.uploadPlugin = factory(
			root.$,
			root.fileupload,
			root.fileupload_ui
		);
	}
}(this, function($, fileupload, fileupload_ui) {
	'use strict';

	

	var Componbox = {};

	Componbox.createcss = function(_opts) {
     
    

      	var uploadbox_style="require_css!oso.lib/jquery-file-upload-extension/css/uploadbox_style.css";

      

		var cssfileall = [
		    "require_css!bootstrap-css/bootstrap.min",
			"require_css!fileupload-css/jquery.fileupload",
			"require_css!fileupload-css/jquery.fileupload-ui",
			"require_css!Font-Awesome",
			"require_css!oso.lib/jquery-file-upload-extension/css/style.css"
		];

		if(_opts.nobotstrap){

			delete cssfileall[0];
			cssfileall.push(uploadbox_style);

		}

		require(cssfileall);

	};
    
     //扩展jq-ui
	Componbox.FromResponse = function(opts) {

		var serverpat="/Sync10/_fileserv/file/";
		if(opts.serverpat){
			serverpat=opts.serverpat;
		}

		var _events = Componbox.events[opts.Objectid];
	

	     var _options=$.blueimp.fileupload.prototype.options;
	     
	     if(opts.acceptFileTypes){
	     _options.messages.acceptFileTypes=opts.acceptFileTypes;
	     }

	     if(opts.unknownError){
	     	_options.messages.unknownError=opts.unknownError;
	     }


			var _options1=$.blueimp.fileupload._proto.options;
			//console.log(_options1);
			_options1=_options.getFilesFromResponse=function(data) {
				console.log(data);
                var filesdata = [];
                if (data.result.files) {
                    filesdata = data.result.files;
                } else {
                    if (data.result) {
                        filesdata = data.result;
                    }

                    var arryname = [];

                    for (var file_i = 0; file_i < filesdata.length; file_i++) {
                        var fileitme = filesdata[file_i];

                        arryname.push({
                            "url":serverpat+fileitme.file_id,
                            "thumbnailUrl": serverpat+fileitme.file_id,
                            "name": fileitme.original_file_name,
                            "type":fileitme.content_type,
                            "size":fileitme.file_size,
                            "deleteUrl":serverpat+ fileitme.file_id,
                            "deleteType": "DELETE",
                            "icons":"fa-th"
                        });

                    }

                 filesdata=arryname;

                }

                 if(_events["event.done"]){

                    _events["event.done"](filesdata);
                    
                 }

                return filesdata;
            };

	};

	Componbox.createfileupload = function(opts) {

		var _Dom = opts._Dom;
		var tlpurl = opts.templateurl;
		var fileposturl=opts.fileposturl;
		var FileSize=5000000;
		if(opts.FileSize){
          FileSize=opts.FileSize;
		}
		var FileTypes= /(\.|\/)(gif|jpe?g|png)$/i;
		if(opts.FileTypes){
		  FileTypes=opts.FileTypes;
		}
		Componbox.FromResponse(opts);

		_Dom.load(tlpurl, function() {
		
		_Dom.fadeIn("slow");

			// Demo settings:
			$('#fileupload').fileupload({
				url: fileposturl,
				disableImageResize: /Android(?!.*Chrome)|Opera/
					.test(window.navigator.userAgent),
				maxFileSize: FileSize,
				acceptFileTypes:FileTypes
			});

		});

	};


	function uploadPlugin(opts) {

		if (!(this instanceof uploadPlugin))
			return new uploadPlugin(opts);

		if (typeof opts === 'string')
			opts = {
				renderTo: opts
			};


		var _this = this.constructor;
		_this.opts = opts;
		Componbox.createcss(_this.opts);
		_this.int();
		var replplugin = {
			on: _this.On,
			render: _this.Render
		};

		replplugin.constructor.opts = _this.opts;

		return replplugin;

	};

    Componbox.events={};
    Componbox.JSONDATA={};
	uploadPlugin.int = function() {

		var _this = this.opts;
		var __names = "up_" + $.now();
		_this.Objectid = __names;
		Componbox.events[__names]={};
        Componbox.JSONDATA[__names]={};
		if (_this.renderTo) {
			_this._Dom = $(_this.renderTo);
		} else {
			_this._Dom = $('<div id="' + __names + '"></div>');
		}

		if(_this.nobotstrap){			
           _this._Dom.hide().addClass("uploadbox_style upload_extensionbox");
		}else{
			_this._Dom.hide().addClass("upload_extensionbox");
		}
		
		//console.log(_this.templateurl);
		//默认模板 
		// if (!_this.templateurl) {
		// 	_this.templateurl = libpat+"fileupload_tlp.html";
		// }

		// if(!_this.batch){
  //        _this.templateurl = libpat+"nobatch_tlp.html";	
		// }

		Componbox.createfileupload(_this);

	};

	uploadPlugin.On = function(event_,callback) {

		var _opts = this.constructor.opts;
		var _events = Componbox.events[_opts.Objectid];
		if(event_!="" && typeof event_ !="undefined"){
          _events[event_]=callback;
		}       

	};

	uploadPlugin.Render = function() {
		var _opts = this.constructor.opts;
	
	};


	return uploadPlugin;
}));