define({
	commitDN:
	{
		url:"/Sync10/action/administrator/product/inventoryCommitmentAction.action"
	},
	exportToExcel:
	{
		 url:"/Sync10/action/administrator/order/ordersExpAction.action"
	},
	createOrder:
	{
		 //url:"/Sync10-ui/pages/bol/bol_add/index.html"
		 url:"/Sync10-ui/pages/b2b/b2b_create/index.html"
	},
	updateOrder:
	{
		 //url:"/Sync10-ui/pages/b2b/order_update/importIndex.html"
		 url:"/Sync10-ui/pages/b2b/update_derect/importIndex.html"
		 
	},
	importOrder:
	{
		 url:"/Sync10-ui/pages/b2b/import_derect/importIndex.html"
	},
	order: {
		url:"/Sync10-ui/pages/b2b/b2b_detail/index.html"
	},
	orderItem: {
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action"
	},
	retailer:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getRetailers"
	},
	//-------------------------------------------------//
	customer:
	{
		url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllCustomerId"
	},
	importOrder2:
	{
		 url:"/Sync10-ui/pages/b2b/order_import2/importIndex.html"
	},
	//-------------------------------------------------//
	source:
	{
		url: "./json/source.json"
	},
	payment:
	{
		url: "./json/payment.json"
	},
	shipFrom:
	{
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	shipTo:
	{
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"

	},
	filterTransportion:
	{
		 url:"/Sync10-ui/pages/temp/bol.json"
	},
	keySearchURL:
	{
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action"
		//url: "/Sync10-ui/pages/b2b/order/json/do.json"
	},
	searchDefaultURL:
	{
		//url: "/Sync10-ui/pages/b2b/order/json/do.json"
		url : "/Sync10/action/administrator/order/b2BOrderJsonAction.action"
	},
	filterSelfWork:
	{
		url:"/Sync10-ui/pages/b2b/order/json/do.json"
	},
	autoComplete:
	{
		url: "/Sync10/action/administrator/b2b_order/B2BOrderGetSearchJSONAction.action"
	},
	adminTree:
	{
		
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAdmin"

	},
	session:
	{
		url:"/Sync10/_fileserv/redis_session"
	}
});