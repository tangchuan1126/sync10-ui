"use strict";
define(
	["jquery","bootstrap","metisMenu","CompondList/View","./view_config","./jsontohtml_templates","artDialog","./config","./js/view/OrderLineDetailView"],
	function($,bootstrap,metisMenu,CompondList,view_config,Template,artDialog,config,OrderLineDetailView)
	{
		return  function(options)
		{
			artDialog.defaults.title= 'Alert';
			artDialog.defaults.okVal= 'Confirm';
			artDialog.defaults.cancelVal= 'Cancel';
			var list  = new CompondList(view_config, {
				renderTo: options.el,
				dataUrl: options.url,
				PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
				//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
			});
			//绑定事件
			list.render();
			list.on("orderDetail",function(e) {
				var B2B_OID = e.data.B2B_OID;
				if(window[B2B_OID] && window[B2B_OID].top)
				{
					window[B2B_OID].focus();
				}
				else
				{
					var uri = config.order.url+"?B2B_OID="+B2B_OID;
					window[B2B_OID] = window.open(uri,B2B_OID);
				}
				/*artDialog.open(uri , {title: "B2B Order B"+B2B_OID,width:'900px',height:'550px', lock: true,opacity: 0.3,fixed: true,
					close:function() {
						//renderOneRow(list,transport_id);
					}
				});*/
			});
			//更多日志
			list.on("events.moreLogs",function(e) {
				//console.log(e);
				var B2B_OID = e.data.B2B_OID;
				var uri ="/Sync10-ui/pages/b2b/order_log/index.html?b2b_oid="+B2B_OID;
				artDialog.open(uri, {title: 'Log Order:B'+B2B_OID,width:'970px',height:'500px',lock:true,opacity:0.3});
			});
			// 更多orderLine
			list.on("events.moreOrderLines",function(e) {
				//console.log(e);
				var B2B_OID = e.data.B2B_OID;

				if(window[B2B_OID] && window[B2B_OID].top)
				{
					window[B2B_OID].focus();
				}
				else
				{
					var uri = config.order.url+"?B2B_OID="+B2B_OID;
					window[B2B_OID] = window.open(uri,B2B_OID);
				}

		
				/**
				var orderView = new OrderLineDetailView().render(order);
				artDialog({
					content:orderView.el,
					title: 'OrderlineDetail:B'+order.B2B_OID,
					//width:'600px',
					//height:'500px',
					lock:true,
					opacity:0.3,
					close:function()
					{
						orderView = null;
					}
				});  **/
				//artDialog.open(uri, {title: 'Log Order:B'+B2B_OID,width:'970px',height:'500px',lock:true,opacity:0.3});
			});
			list.on("events.delete",function(e) {
				artDialog.confirm("Delete Order# B"+e.data.linedata.B2B_OID+"?",function(){
					$.ajax({
						url: config.orderItem.url,
						type: 'post',
						dataType: 'json',
						timeout: 10000,
						cache: false,
						data: {data: e.data.linedata.B2B_OID, action: "deleteB2Border"},//&b2b_oid=" + e.data.linedata.B2B_OID,
						beforeSend:function(request) {},
						error: function(e) {
							alert("submit with error，try again！");
						},
						success: function(data) {
							if(data.CODE=="200") {
								var jqtr = $("tr[id='"+e.data.linedata.B2B_OID+"']");
								var set = list.deletetr(jqtr,"B2B_OID='"+e.data.linedata.B2B_OID+"'");
								//console.log(set);
								//TODO 删除页面数据行
							} else if(data.CODE=="500") {
								alert(""+ data.INFO);
							}
						}
					});
				}, function(){});
			});
			list.on("events.cancel",function(e) {
				artDialog.confirm("Cancel Order# B"+e.data.linedata.B2B_OID+"?",function(){
					$.ajax({
						url: config.orderItem.url,
						type: 'post',
						dataType: 'json',
						timeout: 10000,
						cache: false,
						data: {data: e.data.linedata.B2B_OID,action:"cancelB2Border"},
						beforeSend:function(request) {},
						error: function(e) {
							alert("Cancel with error，try again！");
						},
						success: function(data) {
							if(data.CODE=="200") {
								updateRow(e.data.linedata.B2B_OID,list,Template);
							}
							if(data.CODE=="500") {
								alert(""+ data.INFO);
							}
						}
					});
				}, function(){});
			});
			list.on("events.rollback",function(e) {
				artDialog.confirm("Rollback Commited Order# B"+e.data.linedata.B2B_OID+"?",function(){
					$.ajax({
						url: config.orderItem.url,
						type: 'post',
						dataType: 'json',
						timeout: 10000,
						cache: false,
						data: {data: e.data.linedata.B2B_OID,action:"reCommit"},
						beforeSend:function(request) {},
						error: function(e) {
							alert("Cancel with error，try again！");
						},
						success: function(data) {
							if(data.CODE=="200") {
								updateRow(e.data.linedata.B2B_OID,list,Template);
							}
							if(data.CODE=="500") {
								alert(""+ data.INFO);
							}
						}
					});
				}, function(){});
			});
			list.on("events.note",function(e) {
				var id = e.data.linedata.B2B_OID;
				
				$("#note_"+id).show();
				var remark = "";
				if (e.data.linedata.REMARK) {remark = e.data.linedata.REMARK;}
				$("#note_"+id+ " span.valstyle").html("<textarea style='width:100%;height:100px'>"+remark+"</textarea><button class='save'>save</button><button class='cancel'>cancel</button>");
				$("#note_"+id+" textarea").focus();
				$("#note_"+id+" button.save").on( "click", function(e1){
					var text = $("#note_"+id+" textarea").val();
					artDialog.confirm("Save Note of Order# B"+e.data.linedata.B2B_OID+"?",function(){
						var param = {b2b_oid:e.data.linedata.B2B_OID,remark:text};
						$.ajax({
							url: config.orderItem.url,
							type: 'post',
							dataType: 'json',
							timeout: 10000,
							cache: false,
							data: {data:JSON.stringify(param),action:"updateB2BOrder"},
							beforeSend:function(request) {},
							error: function(e) {
								alert("Cancel with error，try again！");
							},
							success: function(data) {
								if(data.CODE=="200") {
									updateRow(e.data.linedata.B2B_OID,list,Template);
								}
							}
						});
					}, function(){});
				});
				$("#note_"+id+" button.cancel").on( "click", function(e1){
				//	alert("button.cancel");
					updateRow(e.data.linedata.B2B_OID,list,Template);
				});
			});
			list.on("events.photo",function(e){
				var B2B_OID = e.data.linedata.B2B_OID;
				var obj = {
					file_with_type : 51,
					file_with_id : B2B_OID,
					current_name : "",
					product_file_type: 0,
					cmd: "multiFile",
					table:'file',
					base_path:"/Sync10/upload/b2b_order"
				}
				var uri ="/Sync10/administrator/file/picture_online_show.html?"+jQuery.param(obj);
				artDialog.open(uri, {title: 'Photo for B'+B2B_OID,width:'970px',height:'500px',lock:true,opacity:0.3});
			});
			
			list.on("events.commit",function(e){
				//console.log(e);
				artDialog.confirm("Commit Order# B"+e.data.linedata.B2B_OID+"?",
					function(){
					var B2B_OID = e.data.linedata.B2B_OID;
					var ids = [];
					ids.push(e.data.linedata.CUSTOMER_DN);
					var params = {
						"companyId": e.data.linedata.COMPANY_ID,
						"customerId": e.data.linedata.CUSTOMER_ID,
						"dnList": ids
					};
					$(e.button).siblings().addClass('disabled noclick');
					$(e.button).addClass('commitContariner active noclick').find('span').html('Commiting...');
					$(e.button).find('.fa').addClass('fa-spinner fa-spin');
					
					
					$.ajax({
						type: "POST",
						url: config.commitDN.url,
						data: params,
						timeout: 30000,
						datatype: "json",
						success: function(data,textStatus) {
							//成功返回之后调用的函数
							updateRow(e.data.linedata.B2B_OID,list,Template);
							if (data.length == 2 && data[1]==='N') {
								artDialog.alert("Cannot Commit!");
							}
						},
						error: function(){
							//请求出错处理
							updateRow(e.data.linedata.B2B_OID,list,Template);
							artDialog.alert("Can't get Data!");
							
						},
						headers: {
							"X-HTTP-Method-Override": "GET"
						} 
					});
				}, function(){});
			});
			function updateRow(_id,list,templates) {
				$.getJSON(config.orderItem.url,{action:"getB2BOrder",data:_id},function(d){
					var itemlist = [];
					var item1 = {};
					item1.Root = "DATA";
					item1.itmeid = _id;
					item1.itmekey = view_config.View_control.idAttribute;
					itemlist.push(item1);
					var item2 = {};
					item2.Root = view_config.View_control.authIdAttribute;
					item2.itmeid = _id;
					item2.itmekey = "ID";
					itemlist.push(item2);
					var updataObj = new Object();
					updataObj.upitme = itemlist;
					updataObj.DATA = d.DATA;
					var authCon = d.AUTHS[0];
					if (d.DATA.B2B_ORDER_STATUS=="2") {
						authCon.BUTTON_ROLLBACK = true;
					} else if (d.DATA.B2B_ORDER_STATUS=="1"|| d.DATA.B2B_ORDER_STATUS=="10") {
						authCon.BUTTON_COMMIT = true;
						authCon.BUTTON_CANCEL = false;
						authCon.BUTTON_DELETE = true;
					}
					updataObj.AUTHS = authCon;
					list.upSubitmeData(updataObj);
					templates.tbody.updateOneRow(_id,updataObj,view_config.View_control);
					list.Settopnvaheigth();
				}).error(function(e){
					//alert(e);
				});
			}
			return list;
		};
});
