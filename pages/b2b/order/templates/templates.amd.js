define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['advanceSearch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"condition\">\n	<div class=\"row\">\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\" >Hub</div>\n				<input id=\"shipFrom\" type=\"text\" class=\"form-control  immybox immybox_witharrow\" placeholder=\"Select Hub\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3\"> \n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Retailer</div>\n				<input id=\"retailer\" type=\"text\" class=\"form-control immybox immybox_witharrow\" placeholder=\"Select Retailer\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Freight Term</div>\n				<input id=\"payment\" type=\"text\" class=\"form-control immybox immybox_witharrow\" placeholder=\"Select Freight Term\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Status</div>\n				<input id=\"status\" type=\"text\" class=\"form-control immybox immybox_witharrow\" placeholder=\"Select Status\">\n			</div>\n		</div>\n		<!--\n		<div class=\"form-group col-md-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">ETD</div>\n				<input id=\"ETDMin\" type=\"text\" class=\"form-control \" placeholder=\"Begin Date\">\n				<div class=\"input-group-addon\">~</div>\n				<input id=\"ETDMax\" type=\"text\" class=\"form-control \" placeholder=\"End Date\">\n			</div>\n		</div>\n		<div class=\"form-group col-md-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">ETA</div>\n				<input id=\"ETAMin\" type=\"text\" class=\"form-control \" placeholder=\"Begin Date\">\n				<div class=\"input-group-addon\">~</div>\n				<input id=\"ETAMax\" type=\"text\" class=\"form-control \" placeholder=\"End Date\">\n			</div>\n		</div>\n		-->\n		<!--\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Source</div>\n				<input id=\"source\" type=\"text\" class=\"form-control\" placeholder=\"Select Source\">\n			</div>\n		</div>\n		-->\n	</div>\n	<div class=\"row\">\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Customer</div>\n				<input id=\"customer\" type=\"text\" class=\"form-control immybox immybox_witharrow\" placeholder=\"Select Customer\">\n			</div>\n		</div>\n<!-- 		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">DN</div>\n				<input id=\"customer_dn\" type=\"text\" class=\"form-control \" value=\"\" placeholder=\"Input Reference No.\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">PO</div>\n				<input id=\"retail_po\" type=\"text\" class=\"form-control \" value=\"\" placeholder=\"Input P.O No.\">\n			</div>\n		</div>\n		<div class=\"form-group col-sm-3\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Load#</div>\n				<input id=\"load_no\" type=\"text\" class=\"form-control\" value=\"\" placeholder=\"Input Load No.\">\n			</div>\n		</div> -->\n		<div class=\"form-group col-sm-3 hide\">\n			<div class=\"input-group\">\n				<div class=\"input-group-addon\">Creater</div>\n				<input id=\"creater\" type=\"text\" class=\"form-control immybox immybox_witharrow\" placeholder=\"Select Creater\">\n			</div>\n		</div>\n		\n		<div class=\"form-group col-sm-5\">\n			<div class=\"input-group\">\n				<div class=\"input-group-btn\">\n			        <button type=\"button\" class=\"btn btn-default dropdown-toggle longButton\" data-toggle=\"dropdown\" aria-expanded=\"false\"><span id=\"timeType\" data-value=\"orderDate\">Order Date</span> <span class=\"caret\"></span></button>\n			        <ul class=\"dropdown-menu\" role=\"menu\">\n			          <li><a href=\"javascript:void(0)\" class=\"timeTyleItem\" data-value=\"orderDate\">Order Date</a></li>\n			          <li class=\"divider\"></li>\n			          <li><a href=\"javascript:void(0)\" class=\"timeTyleItem\" data-value=\"mabd\">MABD</a></li>\n			          <li class=\"divider\"></li>\n			          <li><a href=\"javascript:void(0)\" class=\"timeTyleItem\" data-value=\"reqShipDate\">Req.Ship Date</a></li>\n			        </ul>\n			     </div>\n				<input id=\"startTime\" type=\"text\" class=\"form-control \" placeholder=\"Start Time\" style=\"border-right:0;background-color: #fff !important;cursor: pointer;\" value=\""
    + alias3(((helper = (helper = helpers.startTime || (depth0 != null ? depth0.startTime : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"startTime","hash":{},"data":data}) : helper)))
    + "\" readonly/>\n				<div class=\"input-group-addon\">~</div>\n				<input id=\"endTime\" type=\"text\" class=\"form-control \" placeholder=\"End Time\" style=\"border-left:0;background-color: #fff !important;cursor: pointer;\" value=\""
    + alias3(((helper = (helper = helpers.endTime || (depth0 != null ? depth0.endTime : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"endTime","hash":{},"data":data}) : helper)))
    + "\" readonly>\n			</div>\n		</div>  \n		<div class=\"form-group col-sm-4\">\n			<button id=\"SearchTabBtn\" type=\"button\" class=\"btn btn-default btn-info\" >Search</button>\n			<button id=\"exportBtn\"  type=\"button\" class=\"btn btn-default btn-info\" >Export</button>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['commonTools'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"row\">\n	<div class=\"form-group col-md-6\">\n		<div class=\"input-group\">\n			<input id=\"Search_Key\" type=\"text\" class=\"form-control min-200\" data-ui-autocomplete=\"\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.oid || (depth0 != null ? depth0.oid : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"oid","hash":{},"data":data}) : helper)))
    + "\">\n			<div class=\"input-group-btn\">\n				<button id=\"eso_search\" class=\"btn btn-info\" type=\"button\" style=\"width:120px\">Go!</button>\n			</div>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['orderLineDetail'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "      		<tr>\n      			<td>"
    + alias3(((helper = (helper = helpers.TITLE || (depth0 != null ? depth0.TITLE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"TITLE","hash":{},"data":data}) : helper)))
    + "</td>\n      			<td>"
    + alias3(((helper = (helper = helpers.B2B_P_NAME || (depth0 != null ? depth0.B2B_P_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"B2B_P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\n      			<td>"
    + alias3(((helper = (helper = helpers.B2B_COUNT || (depth0 != null ? depth0.B2B_COUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"B2B_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\n      		</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default dialogLineContainer\" >\n	<table class=\"table\">\n		<th>Title</th><th>Model</th><th>Order Qty</th>\n"
    + ((stack1 = helpers.blockHelperMissing.call(depth0,this.lambda((depth0 != null ? depth0.LINES : depth0), depth0),{"name":"this.LINES","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</table>\n</div>\n";
},"useData":true});
templates['self_work'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "					<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=helpers.helperMissing, alias2="function", alias3=helpers.blockHelperMissing, buffer = 
  "<table id=\"selfworkTable\" class=\"filter\">\n	<tr>\n		<td>\n			<select id=\"self_status\" class=\"form-control\">\n				<option value=\"0\">货物状态</option>\n";
  stack1 = ((helper = (helper = helpers.product_state || (depth0 != null ? depth0.product_state : depth0)) != null ? helper : alias1),(options={"name":"product_state","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.product_state) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_stock_in_set\" class=\"form-control\">\n				<option value=\"0\">运费流程</option>\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : alias1),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.stock_in_set) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td colspan=\"2\">\n			<div id=\"self_create_account_id\" style=\"width:200px\"></div>\n		</td>\n		<td>\n		</td>\n		<td>\n		</td>\n	</tr>\n	<tr>\n		<td>\n			<select id=\"self_declaration\" class=\"form-control\">\n				<option value=\"0\">出口报关</option>\n";
  stack1 = ((helper = (helper = helpers.declaration || (depth0 != null ? depth0.declaration : depth0)) != null ? helper : alias1),(options={"name":"declaration","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.declaration) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_clearance\" class=\"form-control\">\n				<option value=\"0\">进口清关</option>\n";
  stack1 = ((helper = (helper = helpers.clearance || (depth0 != null ? depth0.clearance : depth0)) != null ? helper : alias1),(options={"name":"clearance","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.clearance) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_invoice\" class=\"form-control\">\n				<option value=\"0\">发票流程</option>\n";
  stack1 = ((helper = (helper = helpers.invoice || (depth0 != null ? depth0.invoice : depth0)) != null ? helper : alias1),(options={"name":"invoice","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.invoice) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"self_drawback\" class=\"form-control\">\n				<option value=\"0\">退税流程</option>\n";
  stack1 = ((helper = (helper = helpers.drawback || (depth0 != null ? depth0.drawback : depth0)) != null ? helper : alias1),(options={"name":"drawback","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.drawback) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</select>\n		</td>\n		<td>\n			<button id=\"filter\" class=\"buttons big\">filter</button>\n		</td>\n	</tr>\n</table>";
},"useData":true});
return templates;
});