define(["./jsontohtml_templates"], function(tmp) {
	return {
		"View_control": {
			"head": [
				{
					"title": "Order Info",
					"field": "basicInfo"
				}, {
					"title": "Order Details",
					"field": "shipInfo"
				}, {
					"title": "Transportation  Info",
					"field": "transportInfo"
				},
				//{
				//	"title": "资金情况",
				//	"field": "assert"
				//}, -->
				//{
				//	"title": "流程信息",
				//	"field": "FLOWS"
				//},
				{
					"title": "Log",
					"field": "LOGS"
				}
			],
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"idAttribute":"B2B_OID",
			"authIdAttribute":"ID",
			"footer": [
				[
					"events.commit", "<i class='fa'></i><span>Commit</span>"
				],
				[
					"events.rollback", "Rollback"
				],
				[
					"events.note", "Note"
				],
				[
					"events.document", "Document"
				],
				[
					"events.photo", "Photo"
				],
				[
					"events.cancel", "Cancel"
				],
				[
					"events.delete", "Delete"
				]
			],
			"templates": tmp
		}
	}
});