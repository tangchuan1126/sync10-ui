define(["jquery","backbone","../../config"],
	function($,Backbone,config)
	{
		return Backbone.Model.extend(
		{
			 url:config.transportModel.url,
			 idAttribute:"transport_id",
			 initialize:function()
			 {

			 }
		}
		);
	}
	);