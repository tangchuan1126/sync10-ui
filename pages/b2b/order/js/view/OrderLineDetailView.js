define(
["jquery",
 "backbone",
 "../../config",
"../../templates"
],
function($,Backbone,config,templates)
{
	return Backbone.View.extend({
		template:templates.orderLineDetail,
		render:function(data)
		{
			this.data = data;
			$(this.el).html(this.template(data));
			
			return this;
		},
		
		html:function()
		{
			return this.el;
		}
	})	

});