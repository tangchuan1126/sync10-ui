"use strict";
define(["jquery","backbone",'handlebars',"../../templates","../../config","auto"],
	function($,Backbone,HandleBars,templates,config,auto)
	{
		return Backbone.View.extend(
		{
			el:"#commonTools",
			template:templates.commonTools,
			initialize:function(opts)
			{
				this.resultView = opts.resultView
				
			},
			render:function(data)
			{
				var temp = this;
				this.$el.html(this.template(data));
				auto.addAutoComplete($("#Search_Key"),config.autoComplete.url,"merge_field","b2b_oid");
				$(document).bind('contextmenu', function(e) {
					//console.log(e);
					//console.log(e.target.id);
					if (e.target.className && (e.target.className=='loding'|| e.target.className=='LodingModal')) {
						return false;
					}
					if (e.target.id && e.target.id=='eso_search') {
						return false;
					}
				});
				return this;
			},
			events:
			{
				"click #eso_search": "searchLeft",
				"mouseup #eso_search": "searchRight"
			},
			contextmenu: function(event) {
				if (document.all) window.event.returnValue = false;// for IE
				else event.preventDefault();
			},
			searchLeft:function(evt)
			{
				var val = $("#Search_Key").val().replace(/\'/g,'');
				if (val=="") {
					alert("Please,Input Search word!");
					return;
				} else {
					val = "\'"+val.toLowerCase()+"\'";
					$("#Search_Key").val(val);
					this.resultView.setQuery({"search_key":val,"search_mode":1,"action":"fastSearch","dataUrl":config.keySearchURL.url});
				}
			},
			searchRight:function(evt)
			{
				if (!evt) evt=window.event;
				if (evt.button==2) {
					var val = $("#Search_Key").val().replace(/\'/g,'');
					if (val=="") {
						alert("Please,Input Search word!");
						return;
					} else {
						$("#Search_Key").val(val);
						this.resultView.setQuery({"search_key":val,"search_mode":2,"action":"fastSearch","dataUrl":config.keySearchURL.url});
					}
				}
			}
		});
	}
);