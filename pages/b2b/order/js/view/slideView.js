"use strict";
define(["jquery", "backbone", "slidePanel"], function ($, Backbone, SlidePanel) {
	return Backbone.View.extend({
		render:function()
		{
			$('#inventoryControl').click(function(){
				var slide = new SlidePanel({
					url: "../order2/index.html",
					title: 'Inventory Control444',
					duration: 500,
					topLine: 0,
					slideLine: "90%"
				});
				slide.open();
			});
		}
	});
});