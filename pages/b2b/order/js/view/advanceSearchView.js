define(["jquery","backbone","../../config","../../templates",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree","immybox","dateUtil",
	"../../../key/B2BOrderKey",
	"bootstrap.datetimepicker",
	"require_css!bootstrap.datetimepicker-css",
	"moment"],
	function($,Backbone,config,templates,AsynLoadQueryTree,immybox,DateUitl,TransportOrderKey) {
	return Backbone.View.extend({
		el:"#advanceSearchA",
		template:templates.advanceSearch,
		//model:new FilterModel(),
		initialize: function(options) {
			this.resultView = options.resultView;
			this.param=options;
		},
		changeTimeType:function(e)
		{
			var text = $(e.target).html();
			var value = $(e.target).attr("data-value");
			$("#timeType").attr("data-value",value).html(text);
		},
		events: {
			"click #createBol" : "createBol",
			"click .timeTyleItem":"changeTimeType",
			"click #exportBtn":"exportToExcel"
		},
		exportToExcel:function(event)
		{
			var data = this.getExportConditon();
			if(data)
			{
				window.location.href = config.exportToExcel.url+"?"+$.param(data);
				var url = config.exportToExcel.url+"?"+$.param(data);
				$(event.target).html('Loading...').attr('disabled','true');
				window.URL = window.URL || window.webkitURL;
				var xhr = new XMLHttpRequest();
				xhr.open('GET', url, true);
				xhr.responseType = 'blob';
				xhr.onload = function(e) {
					$(event.target).html('Export').attr('disabled',false);
				  if (this.status == 200) {
				    var blob = this.response;
				    var link = document.createElement('a');
				    link.href = window.URL.createObjectURL(blob);
				    link.type = 'application/vnd.ms-excel';
				    document.body.appendChild(link);
				    link.download = new Date().getTime()+".xlsx";
				  }
				};
				xhr.send();
			}
		},
		render: function() {
		   // var now  = new Date().format('MM/dd/yyyy');
			//var  data ={ "startTime":now, "endTime":now,};
			var v = this;
			v.$el.html(v.template(data));
			/**
			var fromTree = new AsynLoadQueryTree({
				renderTo: "#shipFrom",
				//selectid: {value: v.shipFrom},
				PostData: {rootType:"Ship From"},
				dataUrl: config.shipFrom.url,
				scrollH: 400,
				multiselect: false,
				Async: true,
				Pleaseselect: "Clean Select",
				placeholder:"Ship From"
			});
			fromTree.on("Pleaseselect",function(jqinput){
				jqinput.val("");
			});
			fromTree.render();  **/
			
			$.getJSON(config.shipFrom.url,{"type":"1"}, function (json) {
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].data,"text":json[i].name});
				}
				$('#shipFrom').immybox({
					Pleaseselect:'Clean Select',
					Defaultselect:v.param.send_psid,
					choices: rls
				});
			});
			$('#status').immybox({
				Pleaseselect:'Clean Select',
				choices: v.getSelect(TransportOrderKey,DateUitl.getLanguage(),[1,2,3,4,5,6,7,8,9,10])
			});
			var adminTree = new AsynLoadQueryTree({
				renderTo: "#creater",
				dataUrl: config.adminTree.url,
				scrollH: 400,
				PostData: {rootType:"Creater"},
				multiselect: false,
				Async: true,
				Parentclick: false,
				Pleaseselect: "Clean Select",
				placeholder: "Creator"
			});
			adminTree.on("Pleaseselect",function(jqinput){
				jqinput.val("");
			});
			adminTree.render();
			
			$.getJSON(config.payment.url, function (json) {
				$('#payment').immybox({
					Pleaseselect:'Clean Select',
					choices: json
				});
			});

		
			/*
			$.getJSON(config.source.url, function (json) {
				$('#source').immybox({
					Pleaseselect:'Clean Select',
					choices: json
				});
			});
			*/
			$.getJSON(config.customer.url, function (json) {
				$('#customer').immybox({
					Pleaseselect:'Clean Select',
					choices: json
				});
			});
			$("#SearchTabBtn").bind('click', {}, function(event) {
				v.filter();
			});

			$.getJSON(config.retailer.url, function (json) {
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].SHIP_TO_ID,"text":json[i].SHIP_TO_NAME});
				}
				$('#retailer').immybox({
					Pleaseselect:'Clean Select',
					choices: rls
				});
			});

 			$("#startTime").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 })
					.on('changeDate', function(ev){
						$('#endTime').datetimepicker('setStartDate', ev.delegateTarget.value);
				});

			 $("#endTime").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 });
			/*
			$("#ETAMin").datepicker({defaultDate: "+0"});
			$("#ETAMax").datepicker({defaultDate: "+0"});
			$("#ETDMin").datepicker({defaultDate: "+0"});
			$("#ETDMax").datepicker({defaultDate: "+0"});
			*/
			return this;
		},
	
		getSelect: function(objlist,language,ul) {
			var list = [];
			if(ul) {
				for(var i=0;i<ul.length;i++) {
					var key = ul[i];
					var li = {};
					li.value = key;
					li.text = objlist[key][language];
					list.push(li);
				}
				return list;
			}
			_.map(objlist,function(item,key) {
				var li = {};
				li.value = key;
				li.text = item[language];
				list.push(li);
			});
			return list;
		},
		clearErrorMsg:function()
		{
			$("#shipFrom").parent().removeClass('has-error').removeClass('shake').attr('title', '');
			$("#customer").parent().removeClass('has-error').removeClass('shake').attr('title', '');
		},
		getExportConditon:function()
		{
			this.clearErrorMsg();
			var data = {};

			var result = true;

			var hub = $("#shipFrom").attr("data-value");
			var customer =  $("#customer").attr("data-value");
			if(this.isEmpty(hub))
			{
				$("#shipFrom").parent().addClass('has-error').addClass('shake').attr('title', 'Please select when export!');;
				result =   false;
			}
			
			if(this.isEmpty(customer))
			{
				$("#customer").parent().addClass('has-error').addClass('shake').attr('title', 'Please select when export!');
				result =   false;
			}

			var timeType = $("#timeType").attr("data-value");
			var startTime = $("#startTime").val().trim();
			var endTime = $("#endTime").val().trim();
			if(startTime != "" || endTime !="")
			{
				var shipFrom = $("#shipFrom").attr("data-value");
				if(shipFrom && shipFrom > 0 )
				{	
					data.timeType = timeType;
					if(startTime != "")
					{	
						var startMoment  = new moment(startTime,'MM/DD/YYYY');
						data.startTime = startMoment.format('YYYY-MM-DD');
					}
					if(endTime != "")
					{
						var m2  = new moment(endTime,'MM/DD/YYYY');
						data.endTime = m2.format('YYYY-MM-DD');
					}
					
				}
			}

			if (!this.isEmpty($("#shipFrom").attr("data-value"))) { data.send_psid = $("#shipFrom").attr("data-value"); }
			if (!this.isEmpty($("#retailer").attr("data-value"))) { data.retailer = $("#retailer").attr("data-value"); }
			if (!this.isEmpty($("#status").attr("data-value"))) { data.b2b_order_status = $("#status").attr("data-value"); }
			if (!this.isEmpty($("#payment").attr("data-value"))) { data.freight_term = $("#payment").attr("data-value"); }
			if (!this.isEmpty($("#creater").attr("data-val"))) { data.create_account_id = $("#creater").attr("data-val"); }
			if (!this.isEmpty($("#customer").attr("data-value")))
			 {
				data.customer_id= $("#customer").attr("data-value"); 
			}
		
			return result ? data : false;
		},
		isEmpty:function(str)
		{
			return str == undefined || $.trim(str) =="" || str==null || str =="null"; 
		},
		filter: function() {
			//this.resultView.render(this.queryOptions);
			var data = {};
			this.clearErrorMsg();
			var timeType = $("#timeType").attr("data-value");
			var startTime = $("#startTime").val().trim();
			var endTime = $("#endTime").val().trim();
			if(startTime != "" || endTime !="")
			{
				var shipFrom = $("#shipFrom").attr("data-value");
				if(shipFrom && shipFrom > 0 )
				{	
					data.timeType = timeType;
					if(startTime != "")
					{	
						var startMoment  = new moment(startTime,'MM/DD/YYYY');
						data.startTime = startMoment.format('YYYY-MM-DD');
					}
					if(endTime != "")
					{
						var m2  = new moment(endTime,'MM/DD/YYYY');
						data.endTime = m2.format('YYYY-MM-DD');
					}
				}
				else
				{
				   //should  select hub
				   $("#shipFrom").parent().addClass('has-error').addClass('shake').attr('title', 'please select Hub!');;
				   return false;
				}
			}

			if ($("#shipFrom").attr("data-value")>0) { data.send_psid = $("#shipFrom").attr("data-value"); }
			if ($("#retailer").attr("data-value")>0) { data.retailer = $("#retailer").attr("data-value"); }
			if ($("#status").attr("data-value")!="") { data.b2b_order_status = $("#status").attr("data-value"); }
			if ($("#payment").attr("data-value")!="") { data.freight_term = $("#payment").attr("data-value"); }
			//if ($("#source").attr("data-value")!="") { data.source_type= $("#source").attr("data-value"); }
			if ($("#creater").attr("data-val")!="") { data.create_account_id = $("#creater").attr("data-val"); }
			//if ($("#ETAMin").val()!="") { data.eta_begin = ($("#ETAMin").datepicker('getDate')).getTime(); }
			//if ($("#ETAMax").val()!="") { data.eta_end = ($("#ETAMax").datepicker('getDate')).getTime(); }
			//if ($("#ETDMin").val()!="") { data.etd_begin = ($("#ETDMin").datepicker('getDate')).getTime(); }
			//if ($("#ETDMax").val()!="") { data.etd_end = ($("#ETDMax").datepicker('getDate')).getTime(); }
			if ($("#customer").attr("data-value")!="") {
				data.customer_id= $("#customer").attr("data-value"); 
			}
			if ($("#customer_dn").val()!="") {
				data.customer_dn = $("#customer_dn").val();
			}
			if ($("#retail_po").val()!="") {
				data.retail_po = $("#retail_po").val();
			}
			if ($("#load_no").val()!="") {
				data.load_no = $("#load_no").val();
			}
			data.action ="fillterB2BOrder";
			this.resultView.setQuery(data);
			//new TransportList(queryOptionsa);
		}
	});

}
);