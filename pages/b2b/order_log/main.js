"use strict";
require(["../../../requirejs_config","./config"
	
], function(con,config){
	require(["jquery","bootstrap",
	"backbone","./config","./templates",
	"oso.bower/jqueryui/jquery-ui.min",
	"/Sync10/administrator/js/art/plugins/jquery.artDialog.source.js",
	"/Sync10/administrator/js/art/plugins/iframeTools.js",
	"domready!"], function($,bootstrap,Backbone,config,templates) {
		//$(function(){
		function getQueryString(name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
			var r = window.location.search.substr(1).match(reg);
			if (r != null) return unescape(r[2]); return null;
		}
		var id = getQueryString("b2b_oid");
		var view = Backbone.View.extend({
			el:"#tabs",
			template:templates.uitab,
			initialize: function(options) {},
			events: {
				"click #createBol" : "createBol"
			},
			render: function() {
				var v = this;
				v.$el.html(v.template({"id":id}));
				$("#tabs").tabs({
					cache: true,
					spinner: '',
					cookie: { expires: 30000 } ,
					load: function(event, ui) {
						/*onLoadInitZebraTable();*/
					}
				});
				/*
				$("#btnclose").click(function(){
					console.log(111);
					$.artDialog.close();
				});
				*/
			}
		});
		(new view()).render();
	});
});