"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery", 
			"./js/view/importView",
			"MsgHelper",
			"bootstrap",
			"domready"
			],
			function($,ImportView,MsgHelper)
			{
				$.ajax({
					url: config.templateURL.action,
					type: 'GET',
					dataType: 'JSON',
				})
				.done(function(data) {
					config.templateURL.url = config.templateURL.url+data.mid;
					new ImportView().render();
				})
				.fail(function(e) {
					console.log(e);
					MsgHelper.showMessage("System error!","error");
				});
			});
	});