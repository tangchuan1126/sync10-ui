"use strict";
define(["jquery",
	"backbone",
	'handlebars',
	'../../MsgHelper',
	"../../templates",
	"../../config",
	"artDialog"
	],
	function($,Backbone,HandleBars,MsgHelper,templates,config)
	{
		return Backbone.View.extend(
		{
			el:"#import",
			template:templates.result,
			errorInfoTemplate:templates.errorInfo,
			resultInfoTemplate:templates.resultInfo,
			initialize:function(options)
			{
				this.param = options;
			},
			render:function() {
				var that = this;
				this.$el.html(this.template({}));

				var formData = {
					"fileId":this.param.fileId,
					"fileName":this.param.fileName
				};
				$.ajax({
					url:this.param.url,
					type: 'GET',
					dataType: 'JSON',
					timeout : 600000, 
					cache:false,
					data:formData,
				})
				.done(function(data) {
					//提交数据成功，开始查询完成进度
					//that.getProgess();
					that.handle(data);
				})
				.fail(function(e) {
					that.handleException(e);
					console.log(e);
				});
			},
			//直接处理方式
			handle:function(data)
			{
				var that = this;
				if(data != null)
				{
					that.handleCheckResult(data);
				}
				else
				{
					//出现异常，未返回合法的数据
					that.handleException({});
				}
			},
			getProgess:function()
			{
				var that = this;
				setTimeout(
					function(){
						$.ajax({
								url: config.importProgess.url+"?a="+new Date().getTime(),
								type: 'GET',
								dataType: 'JSON',
								data:{action:"get",m:that.param.getProgessMethod}
							})
							.done(function(data) {
								//console.log(data);
								if(data != null && data.finish == "1")
								{
									that.handleCheckResult(data);
									//请求删除缓存
									that.removeCache();
								}
								else if(data != null && data.finish== "0")
								{
									that.getProgess();
								}
								else
								{
									//出现异常，未返回合法的数据
									that.handleException({});
								}
							}).fail(function(e)
							{
								console.log(e);
								that.handleException(e);
							});

					},1000);
			},
			removeCache:function()
			{
				$.ajax({
					url:  config.importProgess.url,
					type: 'GET'
				});
			},
			handleCheckResult:function(data)
			{
				var that =this;
				$("#loading").hide();
				var warning = data.warning;
				data.download = config.fileserv.download;
				var checkResult = parseInt(warning) == 0;
				if(data.code && parseInt(data.code)==200)
				{
					var text  = checkResult?"Import Successed":data.msg;
					if(checkResult)
					{
						$("#success").removeClass('hidden').addClass('show');
						$("#success").find("label").html(text);
					}
					else
					{
						$("#warning").removeClass('hidden').addClass('show');
						$("#warning").find("label").html(text);
					}
					this.displayResultInfo(data);
				}
				else 
				{
					$("#warning").find("label").html(data.msg);
					$("#warning").removeClass('hidden').addClass('show');
					//设置模板文件下载地址
					data.downloadtemplte=config.templateURL.url;
					this.displayResultInfo(data);
				}
				artDialog.opener.refreshWindow();
			},
			handleException:function(e)
			{
				$("#loading").hide();
				if(e.status=="504" || e.status=="502")
				{
					//请求超时
					$("#warning").find("label").text("Network Errors");
					$("#warning").removeClass('hidden').addClass('show');
				}else if(e.status=="404" || e.status=="500")
				{
					//系统异常
					$("#warning").find("label").text("System Errors！Please contact system administrator.");
					$("#warning").removeClass('hidden').addClass('show');
				}else
				{
					$("#warning").find("label").text("Unknown Errors");
					$("#warning").removeClass('hidden').addClass('show');
				}
				artDialog.opener.refreshWindow();
			},
			displayErrorInfo:function(data)
			{
				$("#restultDetail").html(this.errorInfoTemplate(data)).removeClass('hide');
			},
			displayResultInfo:function(data)
			{
				$("#restultDetail").html(this.resultInfoTemplate(data)).removeClass('hide');
				$("#downloadbtn").click(function(event) {
					window.location.href=config.fileserv.download+"/"+data.fileId;
				});
				if(data.failCount != 0)
				{
					$("#restultDetail .invalidateCount").addClass('danger');
					$("#downloadbtn").addClass("buttonBreathing");
				}
			},
			close:function()
			{
				artDialog.close();
			}
		
		});
	}
);