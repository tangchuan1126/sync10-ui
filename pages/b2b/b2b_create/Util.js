define(["jquery"],function($)
	{
		return {
			isEmpty:function(str)
			{
				return str =="" || str == undefined || str == null || str =="undefined" || str=="null";
			},
			handleEmpty:function(str,defaultValue)
			{
				if(this.isEmpty(str))
				{
					return defaultValue;
				}
				else
				{
					return $.trim(str);
				}
			},
			vltNumberStr:function(str)
			{
				return /^\d+$/.test(str);
			},
			vltEmpty:function(str)
			{
				return !this.isEmpty(str);
			}
		}
	});