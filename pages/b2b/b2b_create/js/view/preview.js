define(["jquery","backbone","../../config","../../templates","../../MsgHelper","art_Dialog/dialog","artDialog"],
function($,Backbone,config,templates,MsgHelper,Dialog) {
	return Backbone.View.extend(
	{
			el:"#preview",
			template:templates.preview,
			//model:new FilterModel(),

			initialize:function()
			{
				
			},
			events:
			{
				"click #save" : "save",
				
				"click #preview_pre":"preview_pre"
			},
			render:function(options)
			{
				//console.log(options);
				if(options != null)
				{
					this.briefData = options.briefData;
					this.addressData = options.addressData;
					this.timesData = options.timesData;
					this.paymentData = options.paymentData;
					//console.log(options);
				} else {
					this.briefData = {};
					this.addressData = {};
					this.timesData = {};
					this.paymentData = {};
				}
				var v = this;
				v.$el.html(v.template({
					briefData : this.briefData,
					addressData:this.addressData,
					timesData:this.timesData,
					paymentData:this.paymentData
				}));
				this.initScroll();
				return this;
			},
			save:function(e)
			{
				$(e.target).prop('disabled', 'true');
				this.dialog = new Dialog({
					content: 'Saving...'
				}),
				this.dialog.showModal();
				var that = this;
				//字段转换
				var briefData = {
					"customer_id":this.briefData.customer,
					"customer_dn":this.briefData.customerDN,
					"retail_po":this.briefData.retailPO,
					"mabd":this.briefData.mabd,
					"requested_date":this.briefData.etd,
					"order_number":this.briefData.order_number,
					"ship_not_before":this.briefData.ship_not_before,
					"ship_not_later":this.briefData.ship_not_later,
					"customer_order_date":this.briefData.customer_order_date
				};
				var toAddressData ={
					"send_psid":this.addressData.shipFrom,
					"receive_psid":this.addressData.shipTo,
					"ship_to_party_name":this.addressData.ship_to_party_name,
					"account_id":this.addressData.account_id,
					"deliver_ccid":this.addressData.country,
					"deliver_pro_id":this.addressData.state,
					"address_state_deliver":this.addressData.stateName,
					"deliver_city":this.addressData.city,
					"deliver_house_number":this.addressData.address,
					"deliver_street":this.addressData.address2,
					"deliver_zip_code":this.addressData.zipCode,
					"b2b_order_linkman":this.addressData.contact,
					"b2b_order_linkman_phone":this.addressData.phone
				};
				
				var payment = {
					"freight_term":this.paymentData.paymentType,
					"bill_to_id":this.paymentData.billToId,
					"bill_to_name":this.paymentData.billToName,
					"bill_to_country":this.paymentData.billToCountry,
					"bill_to_state":this.paymentData.billToState,
					"bill_to_city":this.paymentData.billToCity,
					"bill_to_zip_code":this.paymentData.billToZipCode,
					"bill_to_address1":this.paymentData.billToAddress,
					//"bill_to_contact":this.paymentData.billToContact,
					//"bill_to_extension":this.paymentData.billToPhone,
					//"bill_to_fax":this.paymentData.billToFax
				};
				var extraFiled ={
					"b2b_order_status":"1",
					"source_type":"0",
					"b2b_order_address":this.addressData.address,
				};
				var param = $.extend({}, briefData,toAddressData, this.timesData,payment,extraFiled);
				console.log(param);
				$.ajax({
					url: config.saveOrder.url,
					type: 'POST',
					dataType: 'json',
					data:{data:JSON.stringify(param),action:"addB2BOrder"} ,
				})
				.done(function(data) {
					if(data.CODE=="200")
					{
						//MsgHelper.showMessage("Create order success","succeed");
						artDialog.opener.refreshWindow();
						artDialog.data("orderId",data.DATA);
						artDialog.close();
					}
					else
					{
						alert("System error");
						artDialog.close();
					}
				})
				.fail(function(e) {
					$(e.target).prop('disabled', 'false');
					alert("System error");
					artDialog.close();
				})
				.always(function() {
					that.dialog.close().remove();
				});
				
				
			},
			initScroll:function(){
				$("#previewContainer").on('mousewheel DOMMouseScroll', function(e) {
					var $this  =$(this);
					var up = 1;
   			 		if(e.originalEvent){
			 			if(e.originalEvent.wheelDelta){
			 				up = e.originalEvent.wheelDelta;
			 			}else if(e.originalEvent.detail)
			 			{
			 				up = 0 - e.originalEvent.detail;
			 			}
			 		}
			 		if(up > 0 && $this.scrollTop() < 1)
   			 		{
   			 			//上滚
   			 			e.preventDefault();
   			 		}
   			 		if(up < 0 )
   			 		{//下滚
   			 			var divH = $this.outerHeight();

   			 			var h = $this[0].scrollHeight;
   			 			if(divH+ $this.scrollTop() > h-20)
   			 			{
   			 				e.preventDefault();
   			 			}
   			 		}
				});
			},
			preview_pre:function()
			{
				$('#tabs li:eq(2) a').tab('show') ;
			}
	

	});

}
);