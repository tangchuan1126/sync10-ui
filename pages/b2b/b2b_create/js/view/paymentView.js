define(["jquery",
	"backbone",
	"../../config",
	"../../templates",
	"../../Util",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"
	],
function($,Backbone,config,templates,Util,AsynLoadQueryTree) {
	return Backbone.View.extend(
	{
			el:"#payment",
			template:templates.payment,
			//model:new FilterModel(),
			initialize:function(options)
			{
	
			},
			events:
			{
				"click #payment_pre" : "payment_pre",
				"click [name='optionsRadios']" : "inputPaymentAddress",
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({
					
				}));

				var paymentTree = new AsynLoadQueryTree({
				renderTo: "#billToName",
				//selectid: {value: v.shipFrom},
				//PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 280,
				multiselect: false,
				Async: true,
				placeholder:"Payment"
				});
				paymentTree.render();
				  //点击节点时监听
	             paymentTree.on("events.Itemclick",function(treeId,treeNode){
	                  $.ajax({
	                   	url: config.addressInfo.url,
	                   	type: 'GET',
	                   	dataType: 'JSON',
	                   	data: {ps_id: treeNode.data},
	                   })
	                   .done(function(data) {
	                   		v.autocomplete(data);
	                   })
	                   .fail(function() {
	                   	console.log("error");
	                   })
	                   .always(function() {
	                   });
	             });
				return this;
			},
			inputPaymentAddress:function(e)
			{
				if($("#thirdParty").prop("checked"))
				{
					$(".payment .paymentAddress").slideDown(1000);
				}
				else
				{
					$("#billToAddress").val("");
					$(".payment .paymentAddress").slideUp(1000);
				}
				this.valiPaymenType();
			},
			valiPaymenType:function()
			{
				if(this.needVlt)
				{
					this.validate();
					this.validateResult();
				}
			},
			validate:function()
			{
				this.needVlt=true;
				var paymentType = $(".payment [name='optionsRadios']:checked").val();
				this._errorInfo($("#freightTerms"),Util.isEmpty(paymentType),"");

				var address = $("#billToAddress").val();
				if(address.replace(/[^\x00-\xff]/g,"  ").length >= 50)
				{
					this._errorInfo($("#billToAddress"),true,"");
				}
				else
				{
					this._errorInfo($("#billToAddress"),false,"");
					
				}
				return this.validateResult();
			},
			validateResult:function()
			{
				if( $(".payment .has-error").length > 0  )
				{
					$('#tabs li:eq(2)').find(".check").removeClass("checked").addClass('checkerror');
					return false;
				}
				else
				{
					$('#tabs li:eq(2)').find(".check").removeClass("checkerror").addClass('checked');
					return true;
				}
			},
			_errorInfo:function($errorObj,display,message)
			{
				if(display)
				{
					$errorObj.parent().addClass('has-error');
					$errorObj.parent().next().removeClass('hidden').addClass('show');
					if(!Util.isEmpty(message))
					{
						$errorObj.parent().next().find("span").html(message);
					}
				}
				else
				{
					$errorObj.parent().removeClass('has-error');
					$errorObj.parent().next().removeClass('show').addClass('hidden');
				}
				return display;
			},
			autocomplete:function(data)
			{
			    this.initCountry(data.ccid);
			    this.initStates(data.ccid,data.pro_id);
				//$("#billToCountry").val(data.c_country);
				//$("#billToState").val(data.pro_id);
				$("#billToCity").val(data.city);
				$("#billToAddress").val(data.deliver_street);
				$("#billToZipCode").val(data.deliver_zip_code);	
				//$("#billToContact").val(data.CONTACT);
				//$("#billToPhone").val(data.PHONE);
				
				// if(this.needValidate)
				// {
				// 	this.validate();
				// }
			}, 
			initCountry:function(selectedId)
			{
				$.ajax({
					url: config.saveOrder.url,
					type: 'GET',
					dataType: 'json',
					data: {"action": 'getAllCountry'},
				})
				.done(function(countrys) 
				{
				 	$("#billToCountry").empty();
					var options = $("#billToCountry")[0].options;
					$.each(countrys,function(i){
						var option = new Option(countrys[i].C_COUNTRY,countrys[i].CCID); 
						if(selectedId == countrys[i].CCID)
						{
							option.selected=true;
						}
						options.add(option);
					});
				})
				.fail(function(e) {
					console.log(e);
				})
				.always(function() {
					//console.log("complete");
				});
			},
			initStates:function(ccid,selectedId)
			{	

			 	$.getJSON(config.cityInfo.url, {ccid: ccid}, function(states, textStatus)
			 	 {
			 	 	$("#billToState").empty();
					var options = $("#billToState")[0].options;
					$.each(states,function(i){
						var option = new Option(states[i].pro_name,states[i].pro_id); 
						if(selectedId == states[i].pro_id)
						{
							option.selected=true;
						}
						options.add(option);
					});
			 	});
			},
			getObject:function()
			{
				return {
					"paymentType":$(".payment :checked"),
					"billToName":$("#billToName"),
					"billToCountry":$("#billToCountry"),
					"billToState":$("#billToState"),
					"billToCity":$("#billToCity"),
					"billToAddress":$("#billToAddress"),
					"billToZipCode":$("#billToZipCode")
					};
					/*,	
					"billToContact":$("#billToContact"),
					"billToPhone":$("#billToPhone")
					};
					*/
			},
			getData:function()
			{
				if($("#thirdParty").prop("checked"))
				{
					return {
					"paymentType":$(".payment :checked").val(),
					"billToId":$("#billToName").attr("data-val"),
					"billToName":$("#billToName").attr("data-name"),
					//"billToCountry":$("#billToCountry").val(),
					//"billToState":$("#billToState").val(),
					"billToCountry":$("#billToCountry").find("option:selected").text(),
					"billToState":$("#billToState").find("option:selected").text(),
					"billToCity":$("#billToCity").val(),
					"billToAddress":$("#billToAddress").val(),
					"billToZipCode":$("#billToZipCode").val()
					};
/*					
					"billToContact":$("#billToContact").val(),
					"billToPhone":$("#billToPhone").val(),
					"billToFax":$("#billToFax").val()
					}
					*/
				}
				else
				{
					return {
					"paymentType":$(".payment :checked").val(),
					};
				}
				//return data;
			},
			payment_pre:function()
			{
				$('#tabs li:eq(1) a').tab('show') ;
			}
	});
}
);