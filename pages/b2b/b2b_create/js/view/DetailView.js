define(["jquery","backbone","../../Util"],
function($,Backbone,config,,Util) {
	return Backbone.View.extend(
	{
			//model:new FilterModel(),
			initialize:function(options)
			{
	
			},
			preRender:function(data)
			{
				var v = this;
				v.$el.html(v.template(data));
				return this;
			},
			_errorInfo:function($errorObj,display,message)
			{
				if(display)
				{
					$errorObj.parent().addClass('has-error');
					$errorObj.parent().next().removeClass('hidden').addClass('show');
					if(!Util.isEmpty(message))
					{
						$errorObj.parent().next().find("span").html(message);
					}
				}
				else
				{
					$errorObj.parent().removeClass('has-error');
					$errorObj.parent().next().removeClass('show').addClass('hidden');
				}
				return display;
			},
		
	});

}
);