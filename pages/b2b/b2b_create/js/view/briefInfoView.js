define(["jquery","backbone","../../config","../../templates",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
	"../../MsgHelper",
	"../../Util",
	"immybox",
	"bootstrap.datetimepicker",
	"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,templates,AsynLoadQueryTree,MsgHelper,Util,immybox) {
	return Backbone.View.extend(
	{
			el:"#briefInfo",
			template:templates.briefInfo,
			//model:new FilterModel(),
			rightData:"",
			loading:false,
			initialize:function(options)
			{
	
			},
			events:
			{
				"click #brief_next" : "nextStep",
				"blur .briefInfo .vr":"valiSingle"
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({
				}));
				this.initCustomer("");
			//	this.initShipFrom();

				$("#mabd").datetimepicker({
					format:'mm/dd/yyyy',
				 	autoclose:1,
         	 		minView: 2,
         	 		bgiconurl:"",
        	 		forceParse: 0,
        	 		startDate:new Date()
        	 	}).on('changeDate', function(ev){
			    		var endTime= $('#etd');
			            endTime.datetimepicker('setEndDate', ev.delegateTarget.value);
		            }).on('keydown', function(event) {
		            	return false;
		            });
				$("#etd").datetimepicker(
				{
					format:'mm/dd/yyyy',
					language:'en',
					autoclose:1,
         	 		minView: 2,
         	 		bgiconurl:"",
        	 		forceParse: 0,
        	 		startDate:new Date()
				}).on('keydown', function(event) {
		            	return false;
		            });


			    $("#ipt_shipNotBefore").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 ,startDate:new Date()})
					.on('changeDate', function(ev){
						$('#ipt_shipNotLater').datetimepicker('setStartDate', ev.delegateTarget.value);
				});

				$("#ipt_shipNotLater").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0,startDate:new Date() });

				$("#customer_order_date").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 });

				return this;
			},
			initCustomer:function(selectedId)
			{
				$.ajax({
					url: config.customer.url,
					type: 'GET',
					dataType: 'json',
					data:{"type":"1"}
				})
				.done(function(customers) {
					$('#customer').immybox({
						Pleaseselect:'Clean Select',
						textname:"TEXT",
          				valuename:"VALUE",
          				choices: customers,
					});
				})
				.fail(function() {
					MsgHelper.showMessage("loading data failed,please try again later! ","error");
				});
			},
			initShipFrom:function()
			{
				var shipFrom = new AsynLoadQueryTree({
				renderTo: "#shipFrom",
				//selectid: {value: v.shipFrom},
				//PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 280,
				multiselect: false,
				Async: true,
				placeholder:"Delivery Warehouse"
				});
				shipFrom.render();
				$("#shipFrom").keydown(function(){ return false;});
			    // shipFrom.on("events.Itemclick",function(treeId,treeNode){
			    // });
			},
			nextStep:function()
			{
				$('#tabs li:eq(1) a').tab('show');
			},
			getData:function()
			{
				return {
				 	customer : $("#customer").attr("data-value"),
				 	customerName : $("#customer").val(),
					customerDN: $("#customerDN").val(),
				 	/**retailPO :$("#retailPO").val(),
				 	order_number:$("#order_number").val(), **/
				 	shipFrom:$("#shipFrom").data("val"),
					shipFromName:$("#shipFrom").val(),
					mabd:$("#mabd").val(),
					etd:$("#etd").val(),
					ship_not_before:$("#ipt_shipNotBefore").val(),
					ship_not_later:$("#ipt_shipNotLater").val(),
					customer_order_date:$("#customer_order_date").val()
				};
				
			},
			valiSingle:function(e)
			{
				if(this.needVali)
				{
					// var method =$(e.target).attr("data-vr");
					// this[method]();
					this.validate();
				}
			},
			validate:function()
			{
				this.needVali =true;
				this.valiCustomer();
				this.valiCustomerDN();
				//this.valiRetailPO();

				//Mabd
				this.valiMabd();
				this.valiShipDate();
				this.valiDeliWindow();
				return this.validateResult();
			},
			valiCustomerDN:function()
			{
				var customerDN = $("#customerDN").val();
				if(Util.isEmpty(customerDN))
				{
					this._errorInfo($("#customerDN"),true,"");
				}
				else
				{
					this.depthVali();
				}
			},
			depthVali:function()
			{
				var param ={
					customer_id : $("#customer").attr("data-value"),
					customer_dn : $("#customerDN").val()
				}
				var that =this;
				if(param.customer_id != this.rightData.customer_id || param.customer_dn != this.rightData.customer_dn )
				{
					that.loading = true;
					$.ajax({
					url: config.dnCount.url,
					type: 'GET',
					dataType: 'json',
					timeout:10000,
					data: {data: JSON.stringify(param)},
					})
					.done(function(data) {
						//console.log(data);
						if(data.CODE=="200")
						{
							//validate success
							if(data.DATA=="0")
							{
								that._errorInfo($("#customerDN"),false,"");
								that.rightData = param;
							}
							else
							{
								that._errorInfo($("#customerDN"),true,"This DN has already exist.");
							}
						}
						that.loading = false;
						that.validateResult();
					})
					.fail(function(e) {
						that.loading = false;
						MsgHelper.showMessage("loading data failed,please try again later! ","error");
					});
				}
			},
			valiRetailPO:function()
			{
				var retailPO  = $("#retailPO").val();
				this._errorInfo($("#retailPO"),Util.isEmpty(retailPO),"");
			},
			valiCustomer:function()
			{
				 var customer  = $("#customer").attr("data-value");
				 this._errorInfo($("#customer"),Util.isEmpty(customer),"");
			},
			valiShipFrom:function()
			{
				var shipFrom = $("#shipFrom").data("val");
				 this._errorInfo($("#shipFrom"),Util.isEmpty(shipFrom),"");
			},

			//Validate MABD
			valiMabd:function()
			{
				var mabd = $("#mabd").val();
				 this._errorInfo($("#mabd"),Util.isEmpty(mabd),"");
			},
			valiShipDate:function()
			{
				var etd = $("#etd").val();
				var mabd = $("#mabd").val();
				if(!Util.isEmpty(etd)&&!Util.isEmpty(mabd))
				{
					var etdDate = new Date(etd);
					var mabdDate = new Date(mabd);
					if(etdDate > mabdDate)
					{
						 this._errorInfo($("#etd"),true,"Req.Ship date should  earlier than MABD!");
					}
					else
					{
						this._errorInfo($("#etd"),false,"");
					}
				}
			},
			valiDeliWindow:function()
			{
				var ship_not_before = $("#ipt_shipNotBefore").val();
				var ship_not_later = $("#ipt_shipNotLater").val();
				if(!Util.isEmpty(ship_not_before)&&!Util.isEmpty(ship_not_later))
				{
					var before = new Date(ship_not_before);
					var later = new Date(ship_not_later);
					if(before > later)
					{
						this._errorInfo($("#deliWindow"),true,"Invalid  Time");
					}
					else
					{
						this._errorInfo($("#deliWindow"),false,"");
					}
				}
				else
				{
					this._errorInfo($("#deliWindow"),true,"");
				}
			},
			_errorInfo:function($errorObj,display,message)
			{
				if(display)
				{
					$errorObj.parent().addClass('has-error');
					$errorObj.parent().next().removeClass('hidden').addClass('show');
					if(!Util.isEmpty(message))
					{
						$errorObj.parent().next().find("span").html(message);
					}
				}
				else
				{
					$errorObj.parent().removeClass('has-error');
					$errorObj.parent().next().removeClass('show').addClass('hidden');
				}
				return display;
			},
			validateResult:function()
			{
				if(this.loading)
				{
					return false;
				}
				if( $(".briefInfo .has-error").length > 0  )
				{
					$('#tabs li:eq(0)').find(".check").removeClass("checked").addClass('checkerror');
					return false;
				}
				else
				{
					$('#tabs li:eq(0)').find(".check").removeClass("checkerror").addClass('checked');
					return true;
				}
			}
	});

}
);