define(["jquery","backbone","../../config","../../templates",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
	"../../Util",
	"../../MsgHelper",
	"art_Dialog/dialog","immybox"],
function($,Backbone,config,templates,AsynLoadQueryTree,Util,MsgHelper,Dialog) {
	return Backbone.View.extend(
	{
			//el:"#toAddress",
			el:"#address",
			template:templates.toAddress,
			//model:new FilterModel(),
			initialize:function(options)
			{
			},
			events:
			{
				"click #filter" : "filter",
				"click #toAddress_pre":"toAddress_pre",
				"click #toAddress_next":"nextStep",
				"change #country":"countryChange",
				"blur  .addressInfo .form-control:input":"valiSingle"
			},
			valiSingle:function()
			{
				if(this.needValidate)
				{
					this.validate();
				}
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({}));
				var selectedId = "11036";//默认选择的的国家
				this.initCountry(selectedId);
				this.initStates(selectedId,"");
				var shiToTree = new AsynLoadQueryTree({
				renderTo: "#shipTo",
				//selectid: {value: v.shipFrom},
				//PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 280,
				multiselect: false,
				Async: true,
				Pleaseselect: "Clean Select",
				placeholder:"Ship To"
				});
				shiToTree.render();
				//$("#shipTo").keydown(function(){ return false;});
	            //点击节点时监听
	             shiToTree.on("events.Itemclick",function(treeId,treeNode){
	                  //treeNode.data="100006";
	                  $.ajax({
	                   	url: config.addressInfo.url,
	                   	type: 'POST',
	                   	dataType: 'JSON',
	                   	data: {ps_id: treeNode.data},
	                   })
	                   .done(function(data) {
		                   	if(data != null)
		                   	{
								v.autocomplete(data);
		                   	}
	                   })
	                   .fail(function() {
	                   	MsgHelper.showMessage("System error , please login in ! ","error");
	                   })
	                   .always(function() {
	                   });
	             });

	            //initShipFrom 异步zTree加载
				this.initShipFrom();
				return this;
			},
			toAddress_pre:function()
			{
				$('#tabs li:eq(0) a').tab('show') ;
			},
			nextStep:function()
			{
				$('#tabs li:eq(2) a').tab('show') ;
			},
			autocomplete:function(data)
			{
				var that = this;
				//设置国家
				that.country[0].selectChoiceByValue(data.ccid);
				//通过国家查询该国家下所有的城市
				$.ajax({
					url: config.cityInfo.url,
					type: 'GET',
					dataType: 'json',
					data: {ccid: data.ccid},
				})
				.done(function(states) {

					var rls=[];
					for (var i=0;i<states.length;i++) {
						rls.push({"value":states[i].pro_id,"text":states[i].p_code});
					}
					if(that.state)
					{
						that.state[0].Rechoicesdata(rls,data.pro_id);
					}
				/**
					$("#state").empty();
					var options = $("#state")[0].options;
					$.each(states,function(i){
						var option = new Option(states[i].pro_name,states[i].pro_id); 
						if(data.pro_id == states[i].pro_id)
						{
							option.selected=true;
						}
						options.add(option);
					}); **/

					$("#city").val(data.deliver_city);
					$("#addressA").val(data.deliver_house_number);
					$("#address2").val(data.deliver_street);
					$("#zipCode").val(data.deliver_zip_code);	
					$("#contact").val(data.contact);
					$("#phone").val(data.phone);
					if(that.needValidate)
					{
						that.validate();
					}
				})
				.fail(function(error) {
					MsgHelper.showMessage("System error , please login in. ","error");
				})
				.always(function() {
				});
			}, 
			initCountry:function(selectedId)
			{
				var that = this;
				$.ajax({
					url: config.saveOrder.url,
					type: 'GET',
					dataType: 'json',
					data: {"action": 'getAllCountry'},
				})
				.done(function(countrys) 
				{
					that.country = $("#country").immybox({
						Pleaseselect:'Clean Select',
						textname:"C_COUNTRY",
          				valuename:"CCID",
          				Defaultselect:selectedId,
						choices: countrys,
						change:function(e,data)
						{
     						if(data.value)
     						{
     							that.initStates(data.value,"");
     							$("#state").attr("data-value","");
     							$("#state").val("");
     						}
     						
						}
					});

					/**
				 	$("#country").empty();
					var options = $("#country")[0].options;
					var selectedId = "11036";
					$.each(countrys,function(i){
						var option = new Option(countrys[i].C_COUNTRY,countrys[i].CCID); 
						if(selectedId == countrys[i].CCID)
						{
							option.selected=true;
						}
						options.add(option);
					});

					that.initStates(selectedId,-1);  **/
				})
				.fail(function(e) {
					console.log(e);
				})
				.always(function() {
					//console.log("complete");
				});
			},
			countryChange:function()
			{
				var country = $("#country").val();
				this.initStates(country,-1);
			},
			initStates:function(countryId,selectedId)
			{	
			   var that = this;
				$.ajax({
					url: config.cityInfo.url,
					type: 'GET',
					dataType: 'json',
					data: {ccid: countryId},
				}).done(function(json) 
				{	
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].pro_id,"text":json[i].p_code});
					}
					if(that.state)
					{
						that.state[0].Rechoicesdata(rls,selectedId);
					}
					else
					{	
						var states = $("#state").immybox({
							Pleaseselect:'Clean Select',
	          				Defaultselect:selectedId,
							choices: rls
							});
						that.state = states;
					}	
					
				/**
					$("#state").empty();
					var options = $("#state")[0].options;
					options.add(new Option('',''));
					$.each(states,function(i){
						var option = new Option(states[i].pro_name,states[i].pro_id); 
						if(selectedId == states[i].pro_id)
						{
							option.selected=true;
						}
						options.add(option);
					});  **/
				});
			},

			//加载hub异步zTree
			initShipFrom:function()
			{
					$.getJSON(config.shipTo.url,{"type":"1"}, function (json) {
						var rls=[];
						for (var i=0;i<json.length;i++) {
							rls.push({"value":json[i].data,"text":json[i].name});
						}
						$('#shipFrom').immybox({
							Pleaseselect:'Clean Select',
							choices: rls,
							change:function(e,d)
							{
								 	//console.log(e);
	         						//console.log(d);
							}
						});
					});
			   /**
				var shipFrom = new AsynLoadQueryTree({
				renderTo: "#shipFrom",
				//selectid: {value: v.shipFrom},
				//PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 280,
				multiselect: false,
				Async: true,
				placeholder:"Delivery Warehouse"
				});
				shipFrom.render();
				$("#shipFrom").keydown(function(){ return false;});
			    // shipFrom.on("events.Itemclick",function(treeId,treeNode){
			    // });  **/
			},
			_errorInfo:function($errorObj,display,message)
			{
				if(display)
				{
					$errorObj.parent().addClass('has-error');
					$errorObj.parent().next().removeClass('hidden').addClass('show');
					if(!Util.isEmpty(message))
					{
						$errorObj.parent().next().find("span").html(message);
					}
				}
				else
				{
					$errorObj.parent().removeClass('has-error');
					$errorObj.parent().next().removeClass('show').addClass('hidden');
				}
				return display;
			},
			getObject:function()
			{
				return {
					"customerDN":$("#customerDN"),
					"shipTo":$("#shipTo"),
					"country":$("#country"),
					"state":$("#state"),
					"city":$("#city"),
					"address":$("#addressA"),
					"address2":$("#address2"),
					"zipCode":$("#zipCode"),	
					"contact":$("#contact"),
					"phone":$("#phone"),
					"shipFrom":$("#shipFrom")
					};
			},
			getData:function()
			{

				var inputShipToName =  $("#shipTo").val();
			  	var selectShipToName =  $("#shipTo").attr("data-name");
			  	var receive_psid = $("#shipTo").attr("data-val");
			  	var ship_to_party_name = "";
			  	var  account_id = "";
			  	if(inputShipToName != selectShipToName)
			  	{
			  		receive_psid = $("#shipFrom").attr("data-value");
			  		ship_to_party_name =  inputShipToName;
			  		account_id = "-1";
			  	} 
			  	else
			  	{
			  		ship_to_party_name = selectShipToName;
			  	}
				return {
					"shipTo":receive_psid,
					"ship_to_party_name":ship_to_party_name,
					"account_id":account_id,
					"country":$("#country").attr("data-value"),
					"countryName":$("#country").val(), 
					"state":$("#state").attr("data-value"),
					"stateName":$("#state").val(), 
					"city":$("#city").val(),
					"address":$("#addressA").val(),
					"address2":$("#address2").val(),
					"zipCode":$("#zipCode").val(),	
					"contact":$("#contact").val(),
					"phone":$("#phone").val(),
					"shipFrom":$("#shipFrom").attr("data-value"),
					"shipFromName":$("#shipFrom").val()
					};
			},
			validate:function(alert)
			{

				this.needValidate = true;
				var data = this.getObject();
				this._errorInfo(data.shipTo,Util.isEmpty(data.shipTo.val()),"");
				this._errorInfo(data.country,Util.isEmpty(data.country.val()),"");
				this._errorInfo(data.state,Util.isEmpty(data.state.val()),"");
				this._errorInfo(data.city,Util.isEmpty(data.city.val()),"");
				this._errorInfo(data.address,Util.isEmpty(data.address.val()),"");
				//this._errorInfo(data.address2,Util.isEmpty(data.address2.val()),"");
				this._errorInfo(data.zipCode,Util.isEmpty(data.zipCode.val()),"");
			//	this._errorInfo(data.contact,Util.isEmpty(data.contact.val()),"");
			//	this._errorInfo(data.phone,Util.isEmpty(data.phone.val()),"");
				this._errorInfo(data.shipFrom,Util.isEmpty(data.shipFrom.val()),"");
				return this.validateResult();
			},
			validateResult:function()
			{
				if( $(".addressInfo .has-error").length > 0  )
				{
					$('#tabs li:eq(1)').find(".check").removeClass("checked").addClass('checkerror');
					return false;
				}
				else
				{
					$('#tabs li:eq(1)').find(".check").removeClass("checkerror").addClass('checked');
					return true;
				}
			}

	});

}
);