define(["jquery",
	"backbone",
	"../../config",
	"../../templates",
	"../../Vlt",
	"../../Util",
	//"jqueryui/datepicker"
	],
function($,Backbone,config,templates,Vlt,Util) {
	return Backbone.View.extend(
	{
			el:"#others",
			template:templates.others,
			//model:new FilterModel(),
			queryCondition:{},
			initialize:function(options)
			{
				
			},
			events:
			{
				"click #filter" : "filter",
				"click #others_pre" : "preStep",
				"click #others_next" : "nextStep",
				"blur  .others .vr" : "vlt"
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({
					
				}));

            	// $("#eta ").datepicker({
             //    dateFormat: "yy-mm-dd",
             //    changeMonth: true,
             //    changeYear: true
            	// });
            	// $("#etd").datepicker({
             //    dateFormat: "yy-mm-dd",
             //    changeMonth: true,
             //    changeYear: true
            	// });
            	// $("#mabd").datepicker({
             //    dateFormat: "yy-mm-dd",
             //    changeMonth: true,
             //    changeYear: true
            	// })
		//	$("#eta").datetimepicker({format:'yyyy-mm-dd',language:'en'});
		//	$("#etd").datetimepicker({format:'yyyy-mm-dd',language:'en'});
		//	$("#mabd").datetimepicker({format:'yyyy-mm-dd',language:'en'});

				return this;
			},
			vlt:function()
			{
				if(this.needVlt)
				{
					this.validate();
				}
			},
			validateHandler:function($obj,vltItem,result)
			{
				//$obj,校验项，vltItem 校验的项目参见Vlt 对应列表，result 校验结果
				var msg = "";
				if(vltItem == "0")
				{
					msg ="This filed is required";
				}
				else
				{
					msg ="Invalid Data";
				}
				this._errorInfo($obj,!result,msg);
			},
			_errorInfo:function($errorObj,display,message)
			{
				if(display)
				{
					$errorObj.parent().addClass('has-error');
					$errorObj.parent().next().removeClass('hidden').addClass('show');
					if(!Util.isEmpty(message))
					{
						$errorObj.parent().next().find("span").html(message);
					}
				}
				else
				{
					$errorObj.parent().removeClass('has-error');
					$errorObj.parent().next().removeClass('show').addClass('hidden');
				}
				return display;
			},
			validate:function()
			{
				this.needVlt = true;
				Vlt.validate($(".others .vr"),this.validateHandler,this);
				return this.validateResult();
			},
			validateResult:function()
			{
				if( $(".others .has-error").length > 0  )
				{
					$('#tabs li:eq(2)').find(".check").removeClass("checked").addClass('checkerror');
					return false;
				}
				else
				{
					$('#tabs li:eq(2)').find(".check").removeClass("checkerror").addClass('checked');
					return true;
				}
			},
			nextStep:function()
			{
				$('#tabs li:eq(3) a').tab('show') ;
			},
			preStep:function()
			{
				$('#tabs li:eq(1) a').tab('show') ;
			},
			getData:function()
			{
				return {
					"etd":$("#etd").val(),
					"eta":$("#eta").val(),
				//	"mabd":$("#mabd").val()
					}
			}
	});
}
);