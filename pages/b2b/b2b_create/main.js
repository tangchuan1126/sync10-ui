"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery",
			"./js/view/briefInfoView",
			"./js/view/toAddressView",
			"./js/view/paymentView",
			"./js/view/preview",
			"art_Dialog/dialog",
			"./MsgHelper.js",
			"bootstrap"],
			function($,BriefInfoView,ToAddressView,PaymentView,Preview,Dialog,MsgHelper)
			{
				function getQueryString(name)
				{
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var soId = getQueryString("soId");
				soId="so1255151548";
				// var addressInfo = new AddressInfoView().render();
				 var briefInfo =new BriefInfoView().render();
				 var addressInfo = new ToAddressView().render();
			//	 var othersInfo = new OthersView().render();
				 var paymentInfo = new PaymentView().render();

				 var preview = new Preview();
				// new AdditionalView().render();

				
				$('a[data-toggle="tab"]').on('show.bs.tab', function (e)
				{
					var sourceId = e.relatedTarget.id;
					if(sourceId =="briefInfoLink")
					{
						briefInfo.validate();
					}
					else if(sourceId =="addressLink")
					{
						addressInfo.validate();
					}
					else if(sourceId =="othersLink")
					{
					//	othersInfo.validate();
					}
					else if(sourceId =="paymentLink")
					{
						paymentInfo.validate();
					}

					//预览前获取数据
					if(e.target.id =="previewLink")
					{
						var param={
							briefData:briefInfo.getData(),
							addressData:addressInfo.getData(),
					//		timesData:othersInfo.getData(),
							paymentData:paymentInfo.getData()
						};
						preview.render(param);
					}
					else
					{
						$("#previewLink").css({display: 'none'});
					}
				});

				var that = this;
				$("#previewBtn").on('click',  function(event) 
				{

					if(briefInfo.loading)
					{
						MsgHelper.showMessage("validating data,please try later!","alert");
						return;
					}
					var vltAll = addressInfo.validate();
				//	vltAll =  othersInfo.validate() && vltAll ;
					vltAll =  briefInfo.validate() && vltAll ;
					vltAll = paymentInfo.validate() && vltAll;
					if(vltAll)
					{
						//所有页签校验成功，展示预览页面
						$('#previewLink').css({display: 'block'}).tab('show');
					}
					else
					{
						MsgHelper.showMessage("Please modify the error !","error");
					}
				});


				$(".addressInfo").on('mousewheel DOMMouseScroll', function(e) {
					var $this  =$(this);
					var up = 1;
   			 		if(e.originalEvent){
			 			if(e.originalEvent.wheelDelta){
			 				up = e.originalEvent.wheelDelta;
			 			}else if(e.originalEvent.detail)
			 			{
			 				up = 0 - e.originalEvent.detail;
			 			}
			 		}
			 		if(up > 0 && $this.scrollTop() < 1)
   			 		{
   			 			//上滚
   			 			e.preventDefault();
   			 		}
   			 		if(up < 0 )
   			 		{//下滚
   			 			var divH = $this.outerHeight();

   			 			var h = $this[0].scrollHeight;
   			 			if(divH+ $this.scrollTop() > h-20)
   			 			{
   			 				e.preventDefault();
   			 			}
   			 		}
				});
			});
	});