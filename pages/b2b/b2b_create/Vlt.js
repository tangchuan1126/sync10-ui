define(["jquery","./Util"],function($,Util)
	{
		var vlt = {
			"0": "vltEmpty",
			"1": "vltNumberStr"
			};
		return {
			validate:function($array,handler,context)
			{
				$.each($array, function(index, obj) {
					try
					{
						var vltItem = $(obj).data("vr")+"";
						var func = {};
						var result = false;
						if(vltItem.indexOf("&") != -1)
						{
							var items = vltItem.split("&");
							for(var key in items)
							{
								//校验项
								var item = items[key];
								//开始校验
								func =eval(Util[vlt[item]]);
								result = func.call(Util, $(obj).val());
							 	handler.call(context,$(obj),item,result);
							 	if(!result)
							 	{
							 		break;
							 	}
							}
						}
						else if(!Util.isEmpty(vltItem))
						{
							 func = eval(Util[vlt[vltItem]]);
							 result = func.call(Util, $(obj).val());
							 handler.call(context,$(obj),vltItem,result);
						}
					}catch(e)
					{
						console.log(e);
					}
					
				});
			},
		}
	});