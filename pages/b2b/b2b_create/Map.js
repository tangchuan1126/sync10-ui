define(["jquery"], function($) 
{
		var CustomMap = function(idAttribute, dataArray)
		{
			this._map = {};
			var temp = $.extend(true,{},dataArray);
			this.initialize(idAttribute, temp);
		}

		CustomMap.prototype.initialize = function(idAttribute, array)
		{
			this._map = {}
			var temp = this;
			if(typeof array =="object" )
			{
				$.each(array, function(index, lineData) 
				{
					if(lineData && lineData[idAttribute])
					{
						temp._map[lineData[idAttribute]] = lineData;
					}
				});
			}
			return this;
		}
		CustomMap.prototype.get = function(id)
		{
			return this._map[id];
		},
		CustomMap.prototype.put = function(id,obj)
		{
			return this._map[id]=obj;
		},
		CustomMap.prototype.remove = function(id)
		{
			return delete this._map[id];
		},
		CustomMap.prototype.keys = function()
		{
			var arr = [];
			for(var id  in this._map)
			{
				arr.push(id);
			}
			return arr;
		},
		CustomMap.prototype.values = function()
		{
			var arr = [];
			for(var id  in this._map)
			{
				arr.push(this._map[id]);
			}
			return arr;
		},
		CustomMap.prototype.removeAll = function()
		{
			this._map = null;
			return this;
		}

		return CustomMap;
});