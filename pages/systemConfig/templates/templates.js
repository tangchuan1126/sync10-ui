(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_config_option'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"content_add_config\">\r\n<form id=\"addClassifyForm\">\r\n	<table>\r\n		<tr>\r\n			<td>\r\n				<span>Location</span>\r\n			</td>\r\n			<td>\r\n				"
    + escapeExpression(((helper = (helper = helpers.classify || (depth0 != null ? depth0.classify : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classify","hash":{},"data":data}) : helper)))
    + "\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<b class=\"require_property\">*</b>\r\n				<span>Option Id</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"classifyId\" name=\"classifyId\" maxlength=\"30\" />\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<b class=\"require_property\">*</b>\r\n				<span>Option Name</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"classifyName\" name=\"classifyName\" maxlength=\"200\"/>\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n\r\n		<tr>\r\n			<td>\r\n				<b class=\"require_property\">*</b>\r\n				<span>Option Type</span>\r\n			</td>\r\n			<td>\r\n				<select class=\"input_text_password_select\" id=\"classifyType\">\r\n					<option value=\"textbox\">\r\n						文本框\r\n					</option>\r\n					<option value=\"textfield\">\r\n						文本域\r\n					</option>\r\n					<option value=\"text\">\r\n						只读文本\r\n					</option>\r\n					<option value=\"radio\">\r\n						单选框\r\n					</option>\r\n					<option value=\"checkbox\">\r\n						复选框\r\n					</option>\r\n					<option value=\"select\">\r\n						下拉列表\r\n					</option>\r\n				</select>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n\r\n		<tr>\r\n			<td>\r\n				<div id=\"changeTypeTitle\">\r\n					Option Value\r\n				</div>\r\n			</td>\r\n			<td colspan=\"2\">\r\n				<div id=\"changeTypeContent\">\r\n					<input id=\"classifyValue\" type=\"text\" class=\"input_text_password_select\"/>\r\n				</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<span>Pre Description</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"preDescription\" name=\"preDescription\" maxlength=\"200\"/>\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<span>Post Description</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"postDescription\" name=\"postDescription\" maxlength=\"200\"/>\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n	</table>\r\n</form>\r\n</div>";
},"useData":true});
templates['add_config_tab'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div style=\"width:500px;\">\r\n	<div>\r\n		<form id=\"createClassifyForm\">\r\n			<table>\r\n				<tr>\r\n					<td>\r\n						<b class=\"require_property\">*</b>\r\n						Classify Name\r\n					</td>\r\n					<td>\r\n						<input type=\"text\" id=\"classifyName\" name=\"classifyName\" class=\"input_text_password_select\" style=\"margin-left: 10px;\">\r\n					</td>\r\n					<td class=\"status validator_style\">&nbsp;</td>\r\n				</tr>\r\n				<tr>\r\n					<td colspan=\"3\">\r\n						&nbsp;\r\n					</td>\r\n				</tr>\r\n				<tr>\r\n					<td>\r\n						<b class=\"require_property\">*</b>\r\n						Belong Classify\r\n					</td>\r\n					<td>\r\n						<div class=\"systemconfig_classify_tree_small\">\r\n							<div id=\"configClassifyTree\"></div>\r\n						</div>\r\n						<input type=\"hidden\" id=\"parentClassifyId\" name=\"parentClassifyId\">\r\n					</td>\r\n					<td class=\"status validator_style\">&nbsp;</td>\r\n				</tr>\r\n			</table>\r\n		</form>\r\n	</div>\r\n</div>";
  },"useData":true});
templates['create_choice_item'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "		<tr style=\"line-height:20px;background-color:#eeeeee;\">\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				"
    + escapeExpression(lambda((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\r\n			</td>\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				"
    + escapeExpression(lambda((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\r\n			</td>\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				<input class=\"normal\" type=\"button\" name=\"cci_del\" data-key=\""
    + escapeExpression(lambda((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\" value=\"del\"/>\r\n			</td>\r\n		</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"cci_tmpMultipleContent\" style=\"border:1px dotted #bbbbbb;width:400px;\">\r\n	<table style=\"width:100%;border:1px;\">\r\n		<tr style=\"line-height:20px;background-color:#eeeeee;\">\r\n			<td style=\"text-align: center;\">\r\n				KEY\r\n			</td>\r\n			<td style=\"text-align: center;\">\r\n				VALUE\r\n			</td>\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				<input class=\"normal\" type=\"button\" id=\"cci_complete\" value=\"Complete\"/>\r\n			</td>\r\n		</tr>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.choiseCollection : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		<tr style=\"line-height:20px;background-color:#eeeeee;\">\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				<input type=\"text\" id=\"cci_key\"/>\r\n			</td>\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				<input type=\"text\" id=\"cci_value\"/>\r\n			</td>\r\n			<td style=\"text-align:left;padding-left:2px;\">\r\n				<input class=\"normal\" type=\"button\" id=\"cci_add\" value=\"Add\"/>\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</div>";
},"useData":true});
templates['edit_config_option'] = template({"1":function(depth0,helpers,partials,data) {
  return " selected=\"selected\" ";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "<div class=\"content_add_config\">\r\n<form id=\"editItemForm\">\r\n	<table>\r\n		<tr>\r\n			<td>\r\n				<span>Location</span>\r\n			</td>\r\n			<td>\r\n				"
    + escapeExpression(((helper = (helper = helpers.classify || (depth0 != null ? depth0.classify : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"classify","hash":{},"data":data}) : helper)))
    + "\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<span>Option Id</span>\r\n			</td>\r\n			<td>\r\n				"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFNAME : stack1), depth0))
    + "\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<b class=\"require_property\">*</b>\r\n				<span>Option Name</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"classifyName\" name=\"classifyName\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.DESCRIPTION : stack1), depth0))
    + "\" maxlength=\"200\"/>\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<b class=\"require_property\">*</b>\r\n				<span>Option Type</span>\r\n			</td>\r\n			<td>\r\n				<select class=\"input_text_password_select\" id=\"classifyType\">\r\n					<option value=\"textbox\" ";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1), "textbox", {"name":"sif","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">\r\n						文本框\r\n					</option>\r\n					<option value=\"textfield\" ";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1), "textfield", {"name":"sif","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">\r\n						文本域\r\n					</option>\r\n					<option value=\"text\" ";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1), "text", {"name":"sif","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">\r\n						只读文本\r\n					</option>\r\n					<option value=\"radio\" ";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1), "radio", {"name":"sif","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">\r\n						单选框\r\n					</option>\r\n					<option value=\"checkbox\" ";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1), "checkbox", {"name":"sif","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += ">\r\n						复选框\r\n					</option>\r\n					<option value=\"select\" ";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1), "select", {"name":"sif","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">\r\n						下拉列表\r\n					</option>\r\n				</select>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n\r\n		<tr>\r\n			<td>\r\n				<div id=\"changeTypeTitle\">\r\n					Option Value\r\n				</div>\r\n			</td>\r\n			<td colspan=\"2\">\r\n				<div id=\"changeTypeContent\">\r\n					<input id=\"classifyValue\" type=\"text\" class=\"input_text_password_select\"/>\r\n				</div>\r\n			</td>\r\n		</tr>\r\n\r\n		<tr>\r\n			<td>\r\n				<span>Pre Description</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"preDescription\" name=\"preDescription\" maxlength=\"200\" \r\n				value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.PRE_DESCRIPTION : stack1), depth0))
    + "\"/>\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<span>Post Description</span>\r\n			</td>\r\n			<td>\r\n				<input type=\"text\" class=\"input_text_password_select\" id=\"postDescription\" name=\"postDescription\" maxlength=\"200\"\r\n				value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.POST_DESCRIPTION : stack1), depth0))
    + "\"/>\r\n			</td>\r\n			<td class=\"status validator_style\">&nbsp;</td>\r\n		</tr>\r\n	</table>\r\n</form>\r\n</div>";
},"useData":true});
templates['move_config_option'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "							<tr>\r\n								<td style=\"height:24px;\">\r\n									"
    + escapeExpression(lambda((depth0 != null ? depth0.DESCRIPTION : depth0), depth0))
    + "\r\n								</td>\r\n							</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div>\r\n	<table>\r\n		<tr>\r\n			<td valign=\"top\">\r\n				<div>\r\n					<div></div>\r\n					<div class=\"systemconfig_classify_tree\">\r\n						<table>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.configClassify : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "						</table>\r\n					</div>\r\n				</div>\r\n			</td>\r\n			<td>\r\n				<div>\r\n					MOVE TO\r\n				</div>\r\n			</td>\r\n			<td>\r\n				<div>\r\n					<div></div>\r\n					<div class=\"systemconfig_classify_tree\">\r\n						<div id=\"configClassifyTree\"></div>\r\n					</div>\r\n				</div>\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</div>";
},"useData":true});
templates['output_checkbox'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "	<input type=\"checkbox\" name=\"choice_box\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\" \r\n		";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.checked : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "/>\r\n	"
    + escapeExpression(lambda((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\r\n&nbsp;&nbsp;\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "checked";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"width:400px;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.choiceBox : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n<input id=\"cci_edit\" type=\"button\" class=\"normal\" value=\"Edit\"/>\r\n</div>";
},"useData":true});
templates['output_radio'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<input type=\"radio\" name=\"choice_box\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\"\r\n	";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.checked : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "/>\r\n"
    + escapeExpression(lambda((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\r\n&nbsp;&nbsp;\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "checked=\"checked\"";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"width:400px;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.choiceBox : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n<input id=\"cci_edit\" type=\"button\" class=\"normal\" value=\"Edit\"/>\r\n</div>";
},"useData":true});
templates['output_select'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "	<option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\" ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.checked : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">\r\n		"
    + escapeExpression(lambda((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\r\n	</option>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "selected";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"width:400px;\">\r\n<select name=\"choice_box\" class=\"input_text_password_select\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.choiceBox : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</select>\r\n\r\n<input id=\"cci_edit\" type=\"button\" class=\"normal\" value=\"Edit\"/>\r\n</div>";
},"useData":true});
templates['system_config'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <li>\r\n                <a href=\"#tab_"
    + escapeExpression(lambda((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\">\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.TAB_NAME : depth0), depth0))
    + "\r\n                </a>\r\n            </li>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "    <div id=\"tab_"
    + escapeExpression(lambda((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\" data-classifyid=\""
    + escapeExpression(lambda((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\" data-name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.TAB_NAME : depth0), depth0))
    + "\">\r\n        <div>\r\n            <div id=\"id_"
    + escapeExpression(lambda((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\"></div>\r\n\r\n            <div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            </div>\r\n        </div>\r\n    </div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "                    ";
  stack1 = ((helpers.tree || (depth0 && depth0.tree) || helperMissing).call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"tree","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div name=\"all_tabs\">\r\n\r\n    <ul>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.configMenu : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "    </ul>\r\n\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.configMenu : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\r\n\r\n            ";
},"useData":true});
templates['system_config_content'] = template({"1":function(depth0,helpers,partials,data,depths) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "        <tr name=\"configRow\" data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\r\n            <td>\r\n                <input name=\"checkbox_"
    + escapeExpression(lambda((depths[1] != null ? depths[1].tabId : depths[1]), depth0))
    + "\" type=\"checkbox\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\"/>\r\n            </td>\r\n            <td>\r\n                <div class=\"both_sides\">\r\n                    <span>\r\n                        "
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_NAME : depth0), depth0))
    + "\r\n                    </span>\r\n                    <span id=\"move_"
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\r\n";
  stack1 = helpers['if'].call(depth0, (data && data.first), {"name":"if","hash":{},"fn":this.program(2, data, depths),"inverse":this.program(4, data, depths),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n                        &nbsp;\r\n                        <input type=\"button\" name=\"moveConfigOption\" class=\"normal\" value=\"Move\">\r\n                    </span>\r\n                </div>\r\n            </td>\r\n            <td>\r\n                <span>\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.PRE_DESCRIPTION : depth0), depth0))
    + "\r\n                </span>\r\n\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "text", {"name":"sif","hash":{},"fn":this.program(9, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "textbox", {"name":"sif","hash":{},"fn":this.program(11, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "textfield", {"name":"sif","hash":{},"fn":this.program(13, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "radio", {"name":"sif","hash":{},"fn":this.program(15, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "checkbox", {"name":"sif","hash":{},"fn":this.program(21, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "select", {"name":"sif","hash":{},"fn":this.program(24, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n                <span>\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.POST_DESCRIPTION : depth0), depth0))
    + "\r\n                </span>\r\n            </td>\r\n            <td>\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.CONFIG_TYPE : depth0), "text", {"name":"sif","hash":{},"fn":this.program(30, data, depths),"inverse":this.program(32, data, depths),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            </td>\r\n        </tr>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "                            <input type=\"button\" name=\"downConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"D\"/>\r\n";
  },"4":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (data && data.last), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"5":function(depth0,helpers,partials,data) {
  return "                                <input type=\"button\" name=\"upConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"U\"/>\r\n";
  },"7":function(depth0,helpers,partials,data) {
  return "                                <input type=\"button\" name=\"upConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"U\"/>\r\n                                <input type=\"button\" name=\"downConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"D\"/>\r\n";
  },"9":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                    "
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_VALUE : depth0), depth0))
    + "\r\n";
},"11":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                    <input name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_ID : depth0), depth0))
    + "\" style=\"width:362px;height:20px;\" type=\"text\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_VALUE : depth0), depth0))
    + "\"/>\r\n";
},"13":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                    <textarea name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_ID : depth0), depth0))
    + "\" rows=\"5\" cols=\"50\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_VALUE : depth0), depth0))
    + "</textarea>\r\n";
},"15":function(depth0,helpers,partials,data,depths) {
  var stack1, buffer = "\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.OPTION : depth0), {"name":"each","hash":{},"fn":this.program(16, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n";
},"16":function(depth0,helpers,partials,data,depths) {
  var stack1, helperMissing=helpers.helperMissing, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depths[1] != null ? depths[1].CONFIG_VALUE : depths[1]), (depth0 != null ? depth0.OPTION_KEY : depth0), {"name":"sif","hash":{},"fn":this.program(17, data, depths),"inverse":this.program(19, data, depths),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        "
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "\r\n                        &nbsp;&nbsp;&nbsp;\r\n";
},"17":function(depth0,helpers,partials,data,depths) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                            <input type=\"radio\" name=\""
    + escapeExpression(lambda((depths[2] != null ? depths[2].CONFIG_ID : depths[2]), depth0))
    + "\" checked=\"checked\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">\r\n";
},"19":function(depth0,helpers,partials,data,depths) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                            <input type=\"radio\" name=\""
    + escapeExpression(lambda((depths[2] != null ? depths[2].CONFIG_ID : depths[2]), depth0))
    + "\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">\r\n";
},"21":function(depth0,helpers,partials,data,depths) {
  var stack1, buffer = "\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.OPTION : depth0), {"name":"each","hash":{},"fn":this.program(22, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"22":function(depth0,helpers,partials,data,depths) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "\r\n                        <input name=\""
    + escapeExpression(lambda((depths[2] != null ? depths[2].CONFIG_ID : depths[2]), depth0))
    + "\" type=\"checkbox\" "
    + escapeExpression(lambda((depth0 != null ? depth0.CHECKED : depth0), depth0))
    + " value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">\r\n                        "
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "\r\n                        &nbsp;&nbsp;&nbsp;\r\n";
},"24":function(depth0,helpers,partials,data,depths) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                    \r\n                    <select style=\"width:220px;\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONFIG_ID : depth0), depth0))
    + "\" id=\"option_"
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.OPTION : depth0), {"name":"each","hash":{},"fn":this.program(25, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                    </select>\r\n\r\n";
},"25":function(depth0,helpers,partials,data,depths) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depths[1] != null ? depths[1].CONFIG_VALUE : depths[1]), (depth0 != null ? depth0.OPTION_KEY : depth0), {"name":"sif","hash":{},"fn":this.program(26, data, depths),"inverse":this.program(28, data, depths),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n";
},"26":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <option selected value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "</option>\r\n";
},"28":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "</option>\r\n";
},"30":function(depth0,helpers,partials,data) {
  return "                    &nbsp;\r\n";
  },"32":function(depth0,helpers,partials,data) {
  return "                    <input type=\"button\" name=\"editConfigOption\" class=\"normal\" value=\"Edit\"/>\r\n";
  },"34":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div style=\"margin-top:10px;\">\r\n        <input type=\"button\" name=\"saveConfig\" class=\"normal\" data-tabid=\""
    + escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"tabId","hash":{},"data":data}) : helper)))
    + "\" value=\"Save\">\r\n    </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,depths) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"config_content\" data-tabid=\""
    + escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"tabId","hash":{},"data":data}) : helper)))
    + "\">\r\n    <table class=\"systemconfig_title\" cellspacing=\"0\">\r\n        <tr>\r\n            <td>\r\n                <input name=\"checkbox_"
    + escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"tabId","hash":{},"data":data}) : helper)))
    + "_all\" type=\"checkbox\" />\r\n            </td>\r\n            <td>\r\n                <button name=\"moveConfigAll\" id=\"moveConfigAll_"
    + escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"tabId","hash":{},"data":data}) : helper)))
    + "\" class=\"normal\" style=\"display:none\">\r\n                    Move\r\n                </button>\r\n                Option Name\r\n            </td>\r\n            <td>\r\n                Option Value\r\n            </td>\r\n            <td>\r\n                Operation\r\n            </td>\r\n        </tr>\r\n    </table>\r\n    <form id=\"form_"
    + escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"tabId","hash":{},"data":data}) : helper)))
    + "\" data-tabid=\""
    + escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"tabId","hash":{},"data":data}) : helper)))
    + "\">\r\n    <table class=\"systemconfig_description\" cellspacing=\"0\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.configContent : depth0), {"name":"each","hash":{},"fn":this.program(1, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "    </table>\r\n    </form>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.configContent : depth0), {"name":"if","hash":{},"fn":this.program(34, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true,"useDepths":true});
templates['system_config_toolbar'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"toolbar\">\r\n\r\n    <button class=\"normal\" id=\"addConfigTab\">\r\n    	Add Tab\r\n    </button>\r\n\r\n    <button class=\"normal\" id=\"addConfigOption\" style=\"width:115px;\">\r\n    	Add Tab Item\r\n    </button>\r\n\r\n    &nbsp;&nbsp;&nbsp;&nbsp;\r\n\r\n    <button class=\"normal\" id=\"folloWaybill\">\r\n    	运单追踪\r\n    </button>\r\n</div>";
  },"useData":true});
})();