define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_config_option'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"content_add_config\">\n<form id=\"addClassifyForm\">\n	<table>\n		<tr>\n			<td>\n				<span>Location</span>\n			</td>\n			<td>\n				"
    + this.escapeExpression(((helper = (helper = helpers.classify || (depth0 != null ? depth0.classify : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"classify","hash":{},"data":data}) : helper)))
    + "\n			</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Option Id</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"classifyId\" name=\"classifyId\" maxlength=\"30\" />\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Option Name</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"classifyName\" name=\"classifyName\" maxlength=\"200\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Option Type</span>\n			</td>\n			<td>\n				<select class=\"input_text_password_select\" id=\"classifyType\">\n					<option value=\"textbox\">\n						文本框\n					</option>\n					<option value=\"textfield\">\n						文本域\n					</option>\n					<option value=\"text\">\n						只读文本\n					</option>\n					<option value=\"radio\">\n						单选框\n					</option>\n					<option value=\"checkbox\">\n						复选框\n					</option>\n					<option value=\"select\">\n						下拉列表\n					</option>\n				</select>\n			</td>\n			<td>&nbsp;</td>\n		</tr>\n\n		<tr>\n			<td>\n				<div id=\"changeTypeTitle\">\n					Option Value\n				</div>\n			</td>\n			<td colspan=\"2\">\n				<div id=\"changeTypeContent\">\n					<input id=\"classifyValue\" type=\"text\" class=\"input_text_password_select\"/>\n				</div>\n			</td>\n		</tr>\n		<tr>\n			<td>\n				<span>Pre Description</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"preDescription\" name=\"preDescription\" maxlength=\"200\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<span>Post Description</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"postDescription\" name=\"postDescription\" maxlength=\"200\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n</form>\n</div>";
},"useData":true});
templates['add_config_tab'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div style=\"width:500px;\">\n	<div>\n		<form id=\"createClassifyForm\">\n			<table>\n				<tr>\n					<td>\n						<b class=\"require_property\">*</b>\n						Classify Name\n					</td>\n					<td>\n						<input type=\"text\" id=\"classifyName\" name=\"classifyName\" class=\"input_text_password_select\" style=\"margin-left: 10px;\">\n					</td>\n					<td class=\"status validator_style\">&nbsp;</td>\n				</tr>\n				<tr>\n					<td colspan=\"3\">\n						&nbsp;\n					</td>\n				</tr>\n				<tr>\n					<td>\n						<b class=\"require_property\">*</b>\n						Belong Classify\n					</td>\n					<td>\n						<div class=\"systemconfig_classify_tree_small\">\n							<div id=\"configClassifyTree\"></div>\n						</div>\n						<input type=\"hidden\" id=\"parentClassifyId\" name=\"parentClassifyId\">\n					</td>\n					<td class=\"status validator_style\">&nbsp;</td>\n				</tr>\n			</table>\n		</form>\n	</div>\n</div>";
},"useData":true});
templates['create_choice_item'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "		<tr style=\"line-height:20px;background-color:#eeeeee;\">\n			<td style=\"text-align:left;padding-left:2px;\">\n				"
    + alias2(alias1((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\n			</td>\n			<td style=\"text-align:left;padding-left:2px;\">\n				"
    + alias2(alias1((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\n			</td>\n			<td style=\"text-align:left;padding-left:2px;\">\n				<input class=\"normal\" type=\"button\" name=\"cci_del\" data-key=\""
    + alias2(alias1((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\" value=\"del\"/>\n			</td>\n		</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"cci_tmpMultipleContent\" style=\"border:1px dotted #bbbbbb;width:400px;\">\n	<table style=\"width:100%;border:1px;\">\n		<tr style=\"line-height:20px;background-color:#eeeeee;\">\n			<td style=\"text-align: center;\">\n				KEY\n			</td>\n			<td style=\"text-align: center;\">\n				VALUE\n			</td>\n			<td style=\"text-align:left;padding-left:2px;\">\n				<input class=\"normal\" type=\"button\" id=\"cci_complete\" value=\"Complete\"/>\n			</td>\n		</tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.choiseCollection : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		<tr style=\"line-height:20px;background-color:#eeeeee;\">\n			<td style=\"text-align:left;padding-left:2px;\">\n				<input type=\"text\" id=\"cci_key\"/>\n			</td>\n			<td style=\"text-align:left;padding-left:2px;\">\n				<input type=\"text\" id=\"cci_value\"/>\n			</td>\n			<td style=\"text-align:left;padding-left:2px;\">\n				<input class=\"normal\" type=\"button\" id=\"cci_add\" value=\"Add\"/>\n			</td>\n		</tr>\n	</table>\n</div>";
},"useData":true});
templates['edit_config_option'] = template({"1":function(depth0,helpers,partials,data) {
    return " selected=\"selected\" ";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2=this.escapeExpression, alias3=this.lambda;

  return "<div class=\"content_add_config\">\n<form id=\"editItemForm\">\n	<table>\n		<tr>\n			<td>\n				<span>Location</span>\n			</td>\n			<td>\n				"
    + alias2(((helper = (helper = helpers.classify || (depth0 != null ? depth0.classify : depth0)) != null ? helper : alias1),(typeof helper === "function" ? helper.call(depth0,{"name":"classify","hash":{},"data":data}) : helper)))
    + "\n			</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<span>Option Id</span>\n			</td>\n			<td>\n				"
    + alias2(alias3(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFNAME : stack1), depth0))
    + "\n			</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Option Name</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"classifyName\" name=\"classifyName\" value=\""
    + alias2(alias3(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.DESCRIPTION : stack1), depth0))
    + "\" maxlength=\"200\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Option Type</span>\n			</td>\n			<td>\n				<select class=\"input_text_password_select\" id=\"classifyType\">\n					<option value=\"textbox\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1),"textbox",{"name":"sif","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n						文本框\n					</option>\n					<option value=\"textfield\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1),"textfield",{"name":"sif","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n						文本域\n					</option>\n					<option value=\"text\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1),"text",{"name":"sif","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n						只读文本\n					</option>\n					<option value=\"radio\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1),"radio",{"name":"sif","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n						单选框\n					</option>\n					<option value=\"checkbox\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1),"checkbox",{"name":"sif","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n						复选框\n					</option>\n					<option value=\"select\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONFIG_TYPE : stack1),"select",{"name":"sif","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n						下拉列表\n					</option>\n				</select>\n			</td>\n			<td>&nbsp;</td>\n		</tr>\n\n		<tr>\n			<td>\n				<div id=\"changeTypeTitle\">\n					Option Value\n				</div>\n			</td>\n			<td colspan=\"2\">\n				<div id=\"changeTypeContent\">\n					<input id=\"classifyValue\" type=\"text\" class=\"input_text_password_select\"/>\n				</div>\n			</td>\n		</tr>\n\n		<tr>\n			<td>\n				<span>Pre Description</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"preDescription\" name=\"preDescription\" maxlength=\"200\" \n				value=\""
    + alias2(alias3(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.PRE_DESCRIPTION : stack1), depth0))
    + "\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<span>Post Description</span>\n			</td>\n			<td>\n				<input type=\"text\" class=\"input_text_password_select\" id=\"postDescription\" name=\"postDescription\" maxlength=\"200\"\n				value=\""
    + alias2(alias3(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.POST_DESCRIPTION : stack1), depth0))
    + "\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n</form>\n</div>";
},"useData":true});
templates['move_config_option'] = template({"1":function(depth0,helpers,partials,data) {
    return "							<tr>\n								<td style=\"height:24px;\">\n									"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.DESCRIPTION : depth0), depth0))
    + "\n								</td>\n							</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div>\n	<table>\n		<tr>\n			<td valign=\"top\">\n				<div>\n					<div></div>\n					<div class=\"systemconfig_classify_tree\">\n						<table>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.configClassify : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</table>\n					</div>\n				</div>\n			</td>\n			<td>\n				<div>\n					MOVE TO\n				</div>\n			</td>\n			<td>\n				<div>\n					<div></div>\n					<div class=\"systemconfig_classify_tree\">\n						<div id=\"configClassifyTree\"></div>\n					</div>\n				</div>\n			</td>\n		</tr>\n	</table>\n</div>";
},"useData":true});
templates['output_checkbox'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "	<input type=\"checkbox\" name=\"choice_box\" value=\""
    + alias2(alias1((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\" \n		"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.checked : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "/>\n	"
    + alias2(alias1((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\n&nbsp;&nbsp;\n";
},"2":function(depth0,helpers,partials,data) {
    return "checked";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:400px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.choiceBox : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n<input id=\"cci_edit\" type=\"button\" class=\"normal\" value=\"Edit\"/>\n</div>";
},"useData":true});
templates['output_radio'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<input type=\"radio\" name=\"choice_box\" value=\""
    + alias2(alias1((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\"\n	"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.checked : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "/>\n"
    + alias2(alias1((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\n&nbsp;&nbsp;\n";
},"2":function(depth0,helpers,partials,data) {
    return "checked=\"checked\"";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:400px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.choiceBox : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n<input id=\"cci_edit\" type=\"button\" class=\"normal\" value=\"Edit\"/>\n</div>";
},"useData":true});
templates['output_select'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "	<option value=\""
    + alias2(alias1((depth0 != null ? depth0.choiseKey : depth0), depth0))
    + "\" "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.checked : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n		"
    + alias2(alias1((depth0 != null ? depth0.choiseValue : depth0), depth0))
    + "\n	</option>\n";
},"2":function(depth0,helpers,partials,data) {
    return "selected";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:400px;\">\n<select name=\"choice_box\" class=\"input_text_password_select\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.choiceBox : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</select>\n\n<input id=\"cci_edit\" type=\"button\" class=\"normal\" value=\"Edit\"/>\n</div>";
},"useData":true});
templates['system_config'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <li>\n                <a href=\"#tab_"
    + alias2(alias1((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.TAB_NAME : depth0), depth0))
    + "\n                </a>\n            </li>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "    <div id=\"tab_"
    + alias2(alias1((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\" data-classifyid=\""
    + alias2(alias1((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\" data-name=\""
    + alias2(alias1((depth0 != null ? depth0.TAB_NAME : depth0), depth0))
    + "\">\n        <div>\n            <div id=\"id_"
    + alias2(alias1((depth0 != null ? depth0.TAB_ID : depth0), depth0))
    + "\"></div>\n\n            <div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return "                    "
    + ((stack1 = (helpers.tree || (depth0 && depth0.tree) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"tree","hash":{},"data":data})) != null ? stack1 : "")
    + "\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div name=\"all_tabs\">\n\n    <ul>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.configMenu : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </ul>\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.configMenu : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n\n            ";
},"useData":true});
templates['system_config_content'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "        <tr name=\"configRow\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\n            <td>\n                <input name=\"checkbox_"
    + alias2(alias1((depths[1] != null ? depths[1].tabId : depths[1]), depth0))
    + "\" type=\"checkbox\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\"/>\n            </td>\n            <td>\n                <div class=\"both_sides\">\n                    <span>\n                        "
    + alias2(alias1((depth0 != null ? depth0.CONFIG_NAME : depth0), depth0))
    + "\n                    </span>\n                    <span id=\"move_"
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\n"
    + ((stack1 = helpers['if'].call(depth0,(data && data.first),{"name":"if","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\n                        &nbsp;\n                        <input type=\"button\" name=\"moveConfigOption\" class=\"normal\" value=\"Move\">\n                    </span>\n                </div>\n            </td>\n            <td>\n                <span>\n                    "
    + alias2(alias1((depth0 != null ? depth0.PRE_DESCRIPTION : depth0), depth0))
    + "\n                </span>\n\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"text",{"name":"sif","hash":{},"fn":this.program(9, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"textbox",{"name":"sif","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"textfield",{"name":"sif","hash":{},"fn":this.program(13, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"radio",{"name":"sif","hash":{},"fn":this.program(15, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"checkbox",{"name":"sif","hash":{},"fn":this.program(21, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"select",{"name":"sif","hash":{},"fn":this.program(24, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                <span>\n                    "
    + alias2(alias1((depth0 != null ? depth0.POST_DESCRIPTION : depth0), depth0))
    + "\n                </span>\n            </td>\n            <td>\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.CONFIG_TYPE : depth0),"text",{"name":"sif","hash":{},"fn":this.program(30, data, 0, blockParams, depths),"inverse":this.program(32, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "            </td>\n        </tr>\n";
},"2":function(depth0,helpers,partials,data) {
    return "                            <input type=\"button\" name=\"downConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"D\"/>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(data && data.last),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    return "                                <input type=\"button\" name=\"upConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"U\"/>\n";
},"7":function(depth0,helpers,partials,data) {
    return "                                <input type=\"button\" name=\"upConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"U\"/>\n                                <input type=\"button\" name=\"downConfigOption\" style=\"width:20px;\" class=\"normal\" value=\"D\"/>\n";
},"9":function(depth0,helpers,partials,data) {
    return "                    "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONFIG_VALUE : depth0), depth0))
    + "\n";
},"11":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <input name=\""
    + alias2(alias1((depth0 != null ? depth0.CONFIG_ID : depth0), depth0))
    + "\" style=\"width:362px;height:20px;\" type=\"text\" value=\""
    + alias2(alias1((depth0 != null ? depth0.CONFIG_VALUE : depth0), depth0))
    + "\"/>\n";
},"13":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <textarea name=\""
    + alias2(alias1((depth0 != null ? depth0.CONFIG_ID : depth0), depth0))
    + "\" rows=\"5\" cols=\"50\">"
    + alias2(alias1((depth0 != null ? depth0.CONFIG_VALUE : depth0), depth0))
    + "</textarea>\n";
},"15":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.OPTION : depth0),{"name":"each","hash":{},"fn":this.program(16, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n";
},"16":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].CONFIG_VALUE : depths[1]),(depth0 != null ? depth0.OPTION_KEY : depth0),{"name":"sif","hash":{},"fn":this.program(17, data, 0, blockParams, depths),"inverse":this.program(19, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                        "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "\n                        &nbsp;&nbsp;&nbsp;\n";
},"17":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <input type=\"radio\" name=\""
    + alias2(alias1((depths[2] != null ? depths[2].CONFIG_ID : depths[2]), depth0))
    + "\" checked=\"checked\" value=\""
    + alias2(alias1((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">\n";
},"19":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <input type=\"radio\" name=\""
    + alias2(alias1((depths[2] != null ? depths[2].CONFIG_ID : depths[2]), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">\n";
},"21":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.OPTION : depth0),{"name":"each","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"22":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "\n                        <input name=\""
    + alias2(alias1((depths[2] != null ? depths[2].CONFIG_ID : depths[2]), depth0))
    + "\" type=\"checkbox\" "
    + alias2(alias1((depth0 != null ? depth0.CHECKED : depth0), depth0))
    + " value=\""
    + alias2(alias1((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "\n                        &nbsp;&nbsp;&nbsp;\n";
},"24":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                    \n                    <select style=\"width:220px;\" name=\""
    + alias2(alias1((depth0 != null ? depth0.CONFIG_ID : depth0), depth0))
    + "\" id=\"option_"
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.OPTION : depth0),{"name":"each","hash":{},"fn":this.program(25, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n\n";
},"25":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].CONFIG_VALUE : depths[1]),(depth0 != null ? depth0.OPTION_KEY : depth0),{"name":"sif","hash":{},"fn":this.program(26, data, 0, blockParams, depths),"inverse":this.program(28, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\n";
},"26":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <option selected value=\""
    + alias2(alias1((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "</option>\n";
},"28":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <option value=\""
    + alias2(alias1((depth0 != null ? depth0.OPTION_KEY : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.OPTION_VALUE : depth0), depth0))
    + "</option>\n";
},"30":function(depth0,helpers,partials,data) {
    return "                    &nbsp;\n";
},"32":function(depth0,helpers,partials,data) {
    return "                    <input type=\"button\" name=\"editConfigOption\" class=\"normal\" value=\"Edit\"/>\n";
},"34":function(depth0,helpers,partials,data) {
    var helper;

  return "    <div style=\"margin-top:10px;\">\n        <input type=\"button\" name=\"saveConfig\" class=\"normal\" data-tabid=\""
    + this.escapeExpression(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"tabId","hash":{},"data":data}) : helper)))
    + "\" value=\"Save\">\n    </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"config_content\" data-tabid=\""
    + alias3(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"tabId","hash":{},"data":data}) : helper)))
    + "\">\n    <table class=\"systemconfig_title\" cellspacing=\"0\">\n        <tr>\n            <td>\n                <input name=\"checkbox_"
    + alias3(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"tabId","hash":{},"data":data}) : helper)))
    + "_all\" type=\"checkbox\" />\n            </td>\n            <td>\n                <button name=\"moveConfigAll\" id=\"moveConfigAll_"
    + alias3(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"tabId","hash":{},"data":data}) : helper)))
    + "\" class=\"normal\" style=\"display:none\">\n                    Move\n                </button>\n                Option Name\n            </td>\n            <td>\n                Option Value\n            </td>\n            <td>\n                Operation\n            </td>\n        </tr>\n    </table>\n    <form id=\"form_"
    + alias3(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"tabId","hash":{},"data":data}) : helper)))
    + "\" data-tabid=\""
    + alias3(((helper = (helper = helpers.tabId || (depth0 != null ? depth0.tabId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"tabId","hash":{},"data":data}) : helper)))
    + "\">\n    <table class=\"systemconfig_description\" cellspacing=\"0\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.configContent : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </table>\n    </form>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.configContent : depth0),{"name":"if","hash":{},"fn":this.program(34, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true,"useDepths":true});
templates['system_config_toolbar'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"toolbar\">\n\n    <button class=\"normal\" id=\"addConfigTab\">\n    	Add Tab\n    </button>\n\n    <button class=\"normal\" id=\"addConfigOption\" style=\"width:115px;\">\n    	Add Tab Item\n    </button>\n\n    &nbsp;&nbsp;&nbsp;&nbsp;\n\n    <button class=\"normal\" id=\"folloWaybill\">\n    	运单追踪\n    </button>\n</div>";
},"useData":true});
return templates;
});