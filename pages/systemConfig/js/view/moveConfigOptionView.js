/**
 * Created by subin on 2014.11.3.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "js/model/systemConfigModel",
    "jqueryui/dialog",
    "jstree"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.move_config_option,
        initialize:function(options){

            this.setElement(options.el);
            this.collection = new models.MoveConfigInfoCollection;
            this.treeCollection = new models.ConfigClassifyTreeCollection;
        },
        setView:function(views){

            this.systemConfigContentV = views.systemConfigContentV;
        },
        moveToConfigItemId:"",
        render:function(moveId){

            var tmp = this;

            //清空
            tmp.moveToConfigItemId='';
            
            this.collection.fetch({
                data:{
                    moveItemId:moveId,
                },
                success:function(collection){

                    art.dialog({
                        title:'MOVE OPTIONS'
                        ,lock: true
                        ,resize:false
                        ,init:function(){

                            this.content(tmp.template({
                                configClassify:collection.toJSON()
                            }));
                        }
                        ,okVal:'Submit'
                        ,ok:function(){

                            //判断是否选中
                            if(tmp.moveToConfigItemId!=''){
                                var tmpDialog = this;
                                $.ajax({
                                    type:'POST',
                                    url:'/Sync10/_microservice/systemConfigItemMove',
                                    dataType:'json',
                                    data:"classifyId="+tmp.moveToConfigItemId+"&configItemId="+moveId,
                                    success:function(msg){
                                        //刷新
                                        tmp.systemConfigContentV.render();
                                    }
                                });
                            }else{
                                //待完善
                                alert("请选择移动到的位置!")
                                return false;
                            }
                        }
                        ,cancel:true
                        ,cancelVal:'Cancel'
                    });

                    tmp.treeCollection.fetch({
                        success:function(treeCollection){

                            $('#configClassifyTree').jstree({
                                "animation" : 0,
                                'core' : {
                                    "themes" : {
                                        "stripes" : true
                                    },
                                    'data':treeCollection.toJSON()
                                },
                                "plugins" : ["types","wholerow"]
                            }).on('changed.jstree', function (e, data) {

                                if(data.node.children.length > 0){

                                    $('#configClassifyTree').jstree(true).deselect_node(data.node.id,true);
                                    $('#configClassifyTree').jstree(true).select_node(data.node.children[0]);
                                }

                                tmp.moveToConfigItemId=data.selected;
                            });
                        }
                    });
                }
            });
        }
    });
});