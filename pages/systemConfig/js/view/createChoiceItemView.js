/**
 * Created by subin on 2014.11.6
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "js/model/systemConfigModel"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.create_choice_item,
        initialize:function(options){

            this.setElement(options.el);
            this.collection = new models.ChoiceBoxCollection();
        },
        resetCollection:function(){

            this.collection = new models.ChoiceBoxCollection();
        },
        setCollection:function(collection){

            this.collection = collection;
        },
        render:function(params){

            //保存参数
            this.params = params;

            var tmp = this;

            $("#"+params.id).html(tmp.template({
                choiseCollection:tmp.collection.toJSON()
            }));

            $("#cci_add").bind("click",function(evt){
                
                tmp.addChoiceBox(evt);
            });
            $("input[name='cci_del']").bind("click",function(evt){
                
                tmp.delChoiceBox(evt);
            });
            $("#cci_complete").bind("click",function(evt){
                
                tmp.saveChoiceBox(evt);
            });
        },
        addChoiceBox:function(evt){

            if($("#cci_key").val()!='' && $("#cci_value").val() != ''){

                this.collection.add(new models.ChoiceBoxModel({
                    choiseKey:$("#cci_key").val(),
                    choiseValue:$("#cci_value").val()
                }));

                this.render(this.params);
            }
        },
        delChoiceBox:function(evt){

            //转字符串
            var key = $(evt.target).data("key") + "";

            var model = this.collection.findWhere({choiseKey:key});

            this.collection.remove(model);

            this.render(this.params);
        },
        saveChoiceBox:function(evt){

            var tmp = this;

            if(this.params.type == 'radio'){
                $("#"+this.params.id).html(templates.output_radio({
                    choiceBox:this.collection.toJSON()
                }));

                $("[name='choice_box']").bind("change",function(evt){

                    for(var i in tmp.collection.models){

                        if(tmp.collection.models[i].get("choiseKey") == $(evt.target).prop("value")){

                            tmp.collection.models[i].set("checked","checked");
                        }else{
                            tmp.collection.models[i].set("checked","");
                        }
                    }
                });

            }else if(this.params.type == 'checkbox'){
                $("#"+this.params.id).html(templates.output_checkbox({
                    choiceBox:this.collection.toJSON()
                }));

                $("[name='choice_box']").bind("change",function(evt){

                    var model = tmp.collection.findWhere({choiseKey:$(evt.target).val()});

                    if($(evt.target).prop("checked")){

                        model.set("checked","checked");
                    }else{
                        model.set("checked","");
                    }
                });

            }else if(this.params.type == 'select'){
                $("#"+this.params.id).html(templates.output_select({
                    choiceBox:this.collection.toJSON()
                }));

                $("[name='choice_box']").bind("change",function(evt){

                    for(var i in tmp.collection.models){

                        if(tmp.collection.models[i].get("choiseKey") == $(evt.target).prop("value")){

                            tmp.collection.models[i].set("checked","checked");
                        }else{
                            tmp.collection.models[i].set("checked","");
                        }
                    }
                });
            }

            $("#cci_edit").bind("click",function(evt){
                
                tmp.editChoiceBox(evt);
            });
        },
        editChoiceBox:function(evt){

            this.render(this.params);
        }
    });
});