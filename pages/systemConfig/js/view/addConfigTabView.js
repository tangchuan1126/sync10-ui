/**
 * Created by subin on 2014.11.10
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "js/model/systemConfigModel",
    "jstree",
    "artDialog",
    "validate",
    "js/plugin/jquery.validatePlugin"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.add_config_tab,
        initialize:function(options){

            this.setElement(options.el);
            this.treeCollection = new models.ConfigClassifyAllTreeCollection;
        },
        setView:function(views){

            this.systemConfigV = views.systemConfigV;
        },
        parentClassifyId:""
        ,render:function(){

            var tmp = this;

            tmp.parentClassifyId = "";

            art.dialog({
                title:'CREATE CLASSIFY'
                ,lock: true
                ,resize:false
                ,init:function(){
                    this.content(tmp.template());
                }
                ,okVal:'Submit'
                ,ok:function(){
                    
                    var form = $("#createClassifyForm");

                    if (form.valid()) {

                        var model = new models.ConfigClassifyModel();
                    
                        model.save({

                            "classifyName": form.find("input[id='classifyName']").val().replace(/(^\s*)|(\s*$)/g, ""),
                            "parentClassifyId":tmp.parentClassifyId
                        }, {
                            success: function () {

                                tmp.systemConfigV.render();
                            }
                        });

                        return true;
                    }else{

                        form.validate().errorList[0].element.focus();
                        return false;
                    }
                }
                ,cancel:true
                ,cancelVal:'Cancel'
            });

            tmp.treeCollection.fetch({
                success:function(treeCollection){

                    $('#configClassifyTree').jstree({
                        "animation" : 0,
                        'core' : {
                            "themes" : {
                                "stripes" : true
                            },
                            'data':treeCollection.toJSON()
                        },
                        "plugins" : ["types","wholerow"]
                    }).on('changed.jstree', function (e, data) {

                        tmp.parentClassifyId=data.selected[0];
                        $("#parentClassifyId").val(tmp.parentClassifyId);
                    });
                }
            });

            tmp.validator();
        },
        validator: function () {

            $("#createClassifyForm").validate({
                rules: {
                    classifyName:{
                        required: true
                    },
                    parentClassifyId:{
                        required: true
                    }
                },
                messages: {
                    classifyName:{
                        required: "请输入Classify Name"
                    },parentClassifyId:{
                        required: "请选择Belong Classify"
                    }
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        }
    });
});