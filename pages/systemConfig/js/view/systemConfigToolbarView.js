/**
 * Created by subin on 2014.10.29.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates"
],function($,Backbone,Handlebars,templates){

    return Backbone.View.extend({

        template:templates.system_config_toolbar,
        initialize:function(options){

            this.setElement(options.el);
        },
        setView:function(views){

            this.addConfigOptionV = views.addConfigOptionV;
            this.addConfigTabV = views.addConfigTabV;
        },
        events:{

            "click #addConfigTab":"addConfigTab"
            ,"click #addConfigOption":"addConfigOption"
            ,"click #folloWaybill":"folloWaybill"
        },
        folloWaybill:function(evt){
            
            alert('等待微服务接口!');
        },
        addConfigOption:function(evt){
            
            var classifyId = this.getClassifyId();

            this.addConfigOptionV.render(classifyId);
        },
        addConfigTab:function(evt){
            
            this.addConfigTabV.render();
        },
        getClassifyId:function(){

            var result = '';
            //获取当前最底层活动tabID
            $("[data-classifyid][aria-hidden='false']").each(function(){

                var parentArray = $(this).parents("[data-classifyid]");

                if(parentArray.length == 0){

                    if($(this).find("div[name='all_tabs']").length == 0){

                        result = $(this).data("classifyid");
                    }
                }else{

                    var isHidden = false;
                    $(parentArray).each(function(){

                        if($(this).attr("aria-hidden") == 'true'){

                            isHidden = true;
                        }
                    });

                    if(!isHidden){

                        if($(this).find("div[name='all_tabs']").length == 0){

                            result = $(this).data("classifyid");
                        }
                    }
                }
            });

            return result;
        },
        render:function(){

            var tmp = this;

            tmp.$el.html(tmp.template({
                
            }));
        }
    });
});
