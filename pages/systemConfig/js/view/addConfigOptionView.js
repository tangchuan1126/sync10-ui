/**
 * Created by subin on 2014.11.6
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "js/model/systemConfigModel",
    "jstree",
    "artDialog",
    "validate",
    "js/plugin/jquery.validatePlugin"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.add_config_option,
        initialize:function(options){

            this.setElement(options.el);
        },
        setView:function(views){

            this.systemConfigContentV = views.systemConfigContentV;
            this.createChoiceItemV = views.createChoiceItemV;
        },
        render:function(classifyId){

            var tmp = this;

            art.dialog({
                title:'CREATE ITEM'
                ,lock: true
                ,resize:false
                ,init:function(){
                    this.content(tmp.template({
                        classify:$("#tab_"+classifyId).data("name")
                    }));
                }
                ,okVal:'Submit'
                ,ok:function(){

                    var classifyForm = $("#addClassifyForm");

                    if (classifyForm.valid()) {

                        var classifyValue = tmp.getClassifyValue();
                        
                        var model = new models.ClassifyModel();
                        
                        model.save({

                            "classifyId": classifyForm.find("input[id='classifyId']").val(),
                            "classifyName": classifyForm.find("input[id='classifyName']").val().replace(/(^\s*)|(\s*$)/g, ""),
                            "classifyType": $("#classifyType").val(),
                            "classifyValue": classifyValue,
                            "classifyTypeContent": tmp.createChoiceItemV.collection,
                            "belongClassifyId":classifyId,
                            "preDescription":classifyForm.find("input[id='preDescription']").val().replace(/(^\s*)|(\s*$)/g, ""),
                            "postDescription":classifyForm.find("input[id='postDescription']").val().replace(/(^\s*)|(\s*$)/g, "")
                        }, {
                            success: function () {

                                tmp.systemConfigContentV.render();
                            }
                        });

                        return true;

                    }else{

                        classifyForm.validate().errorList[0].element.focus();
                        return false;
                    }
                }
                ,cancel:true
                ,cancelVal:'Cancel'
            });

            $("#classifyType").bind("change",function(evt){

                var type = $("#classifyType").val();
                
                tmp.changeClassifyType(type);
            });

            tmp.createChoiceItemV.resetCollection();

            tmp.validator();
        },
        //获取选项值
        getClassifyValue:function(){

            var classifyForm = $("#addClassifyForm");
            
            var classifyValue = "";
            //文本
            if(classifyForm.find("[id='classifyValue']").length>0){

                classifyValue = classifyForm.find("[id='classifyValue']").val().replace(/(^\s*)|(\s*$)/g, "");
            }else{
                //单选
                if($("#classifyType").val()=='radio'){

                    classifyValue = classifyForm.find("[name='choice_box']:checked").val();
                //多选
                }else if($("#classifyType").val()=='checkbox'){

                    var tmpCheckboxVal = ",";
                    classifyForm.find("[name='choice_box']").each(function(){

                        if($(this).prop("checked") ==true) {
                            
                            tmpCheckboxVal += $(this).prop("value") + ',';
                        }
                    });

                    if(tmpCheckboxVal != ','){

                       classifyValue = tmpCheckboxVal.substr(1,(tmpCheckboxVal.length-2));
                    }
                //下拉
                }else if($("#classifyType").val()=='select'){

                    classifyValue = classifyForm.find("[name='choice_box'] option:selected").val();
                }
            }

            return classifyValue;
        },
        //改变选项类型
        changeClassifyType:function(type){

            var tmp = this;
            switch(type){

                case 'textbox':
                    $("#changeTypeTitle").html('Option Value');
                    $("#changeTypeContent").html('<input id="classifyValue" type="text" class="input_text_password_select"/>');
                    break;
                case 'textfield':
                    $("#changeTypeTitle").html('Option Value');
                    $("#changeTypeContent").html('<textarea id="classifyValue" rows="5" cols="50"></textarea>');
                    break;
                case 'text':
                    $("#changeTypeTitle").html('');
                    $("#changeTypeContent").html('');
                    break;
                case 'radio':
                    $("#changeTypeTitle").html('Option Value');
                    tmp.createChoiceItemV.render({
                        id:'changeTypeContent'
                        ,type:'radio'
                    });
                    break;
                case 'checkbox':
                    $("#changeTypeTitle").html('Option Value');
                    tmp.createChoiceItemV.render({
                        id:'changeTypeContent'
                        ,type:'checkbox'
                    });
                    break;
                case 'select':
                    $("#changeTypeTitle").html('Option Value');
                    tmp.createChoiceItemV.render({
                        id:'changeTypeContent'
                        ,type:'select'
                    });
                    break;
                default:
                    $("#changeTypeTitle").html('');
                    $("#changeTypeContent").html('');
            }
        },
        validator: function () {

            $("#addClassifyForm").validate({
                rules: {
                    classifyId: {
                       required: true
                       ,commonId: true
                       ,remote: {
                            url: "/Sync10/_microservice/existSystemConfigItemIdValidator",
                            dataType: "json",
                            dataFilter: function (data) {
                                return $.parseJSON(data).success;
                            }
                        }
                    },
                    classifyName:{
                        required: true
                    }
                },
                messages: {
                    classifyId: {
                        required: "请输入Option Id",
                        commonId: "Option Id只能包含数字字母下划线",
                        remote: "此Option Id已存在"
                    },
                    classifyName:{
                        required: "请输入Option Name"
                    }
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        }
    });
});