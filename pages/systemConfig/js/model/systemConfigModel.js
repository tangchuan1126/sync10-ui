/**
 * Created by subin on 2014.10.28.
 */
"use strict";
define([
    "../config",
    "jquery",
    "backbone"
],function(page_config,$,Backbone){

    var SystemConfigMenuCollection = Backbone.Collection.extend({
        url: page_config.systemConfigMenuUrl.url,
        parse: function (response) {
            return response.configClassify;
        }
    });

    var SystemConfigContentCollection = Backbone.Collection.extend({
        url: page_config.systemConfigContentUrl.url,
        parse: function (response) {
            return response.configInfo;
        }
    });

    var MoveConfigInfoCollection = Backbone.Collection.extend({
        url: page_config.configItemMoveUrl.url,
        parse: function (response) {
            return response.configInfo;
        }
    });

    var ConfigClassifyTreeCollection = Backbone.Collection.extend({
        url: page_config.configClassifyTreeUrl.url,
        parse: function (response) {
            return response.configClassify;
        }
    });

    var ConfigClassifyAllTreeCollection = Backbone.Collection.extend({
        url: page_config.configClassifyAllTreeUrl.url,
        parse: function (response) {
            return response.configClassify;
        }
    });

    //Classify Model
    var ConfigClassifyModel = Backbone.Model.extend({
        url: page_config.configClassifyUrl.url,
        idAttribute: "id",
        defaults : {
            classifyName:"",
            parentClassifyId:""
        },
    });

    //Classify item Model
    var ClassifyModel = Backbone.Model.extend({
        url: page_config.configItemUrl.url,
        idAttribute: "id",
        defaults: {
            classifyId:"",
            classifyName: "",
            classifyType: "",
            classifyValue: "",
            classifyTypeContent:[],
            belongClassifyId:"",
            preDescription:"",
            postDescription:""
        }
    });

    //Classify Collection
    var ClassifyCollection = Backbone.Collection.extend({
        url:page_config.configItemUrl.url,
        parse: function (response) {
            return response.configItem;
        }
    });

    //ChoiceBox
    var ChoiceBoxModel = Backbone.Model.extend({
        idAttribute: "choiseKey",
        defaults: {
            choiseValue: "",
            checked:""
        }
    });

    var ChoiceBoxCollection = Backbone.Collection.extend({
        model:ChoiceBoxModel
    });

    return {
        SystemConfigMenuCollection:SystemConfigMenuCollection
        ,SystemConfigContentCollection:SystemConfigContentCollection
        ,MoveConfigInfoCollection:MoveConfigInfoCollection
        ,ConfigClassifyTreeCollection:ConfigClassifyTreeCollection
        ,ConfigClassifyModel:ConfigClassifyModel
        ,ClassifyModel:ClassifyModel
        ,ClassifyCollection:ClassifyCollection
        ,ChoiceBoxModel:ChoiceBoxModel
        ,ChoiceBoxCollection:ChoiceBoxCollection
        ,ConfigClassifyAllTreeCollection:ConfigClassifyAllTreeCollection
    };
});