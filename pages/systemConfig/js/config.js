﻿(function(){
    var configObj = {
        systemConfigMenuUrl:{
            url:"/Sync10/_microservice/systemConfigClassify"
        },
        systemConfigContentUrl:{
            url:"/Sync10/_microservice/systemConfigInfo"
        },
        configItemMoveUrl:{
            url:"/Sync10/_microservice/systemConfigItemMove"
        },
        configClassifyTreeUrl:{
            url:"/Sync10/_microservice/systemConfigClassifyTree"
        },
        configClassifyAllTreeUrl:{
            url:"/Sync10/_microservice/systemConfigClassifyAllTree"
        },
        configClassifyUrl:{
            url:"/Sync10/_microservice/systemConfigClassify"
        },
        configItemUrl:{
            url:"/Sync10/_microservice/systemConfigItem"
        }
    };
	define(configObj);
    
}).call(this);