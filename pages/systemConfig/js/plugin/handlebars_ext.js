define(['handlebars.runtime','templates'],function(Handlebars,templates){
    
	Handlebars = Handlebars.default;

	Handlebars.registerHelper('tree', function(children) {

		return templates.system_config({configMenu:children});
	});

  	return Handlebars;
});