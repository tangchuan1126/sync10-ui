define(['jquery'],function($){
    
    //表单转json
	$.fn.serializeObject = function(){

        var result = {};  
        var params = this.serializeArray();

        $.each(params, function() {

            //如果有重复内容
            if(result[this.name]){

                //以逗号分隔
                result[this.name] += ("," + (this.value || ''));

            }else{
                //直接添加
                result[this.name] = this.value || '';
            }  
        });

        return result;
    };
});