define(
	["jquery","validate"]
	,function($) {

    //自定义jquery validate 验证规则
    $.validator.addMethod(
        "commonId"
        ,function(value, element) {
            var length = value.length;

            var regex = /^[a-zA-Z0-9_]+$/;
            return (regex.exec(value))? true:false;
        }
        ,"Option Id只能包含数字字母下划线"
    );
});