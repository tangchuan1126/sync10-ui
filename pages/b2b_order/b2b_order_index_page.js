"use strict";
 define(["jquery","backbone","handlebars","templates","auto",'jqueryui/tabs'],function($,Backbone,Handlebars,templates,auto){
		$(function(){
			$("#tabs").tabs();			
			var searchView = Backbone.View.extend({
			 el:"#search",
           	 template:templates.search_autocomplte,
           	 render:function(){
	           	  var v = this;
	              v.$el.html(v.template());
	            }
			});

			var search_View = new searchView();

			search_View.render();

			auto.addAutoComplete($("#search_key"),"/Sync10/action/administrator/order/getSearchOrdersJson.action","merge_field","oid");
		});
});