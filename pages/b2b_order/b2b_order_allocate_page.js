/**
 * 封箱拆分计算
 * @authors WenZhen (gaowenzhen@msn.com)
 * @date    2014-08-13 16:27:39
 * @version 0.1
 */

(function($) {

	Array.prototype.delarry = function() {
		var res__ = [],
			hash__ = {};
		for (var p = 0, elem_;
			(elem_ = this[p]) != null; p++) {
			if (!hash__[elem_]) {
				res__.push({
					data: elem_,
					index: p
				});
				hash__[elem_] = true;
			}
		}
		return res__;
	}

	Sealingcalculation = {
		global: {
			setTimesr: "",
			postTimes: ""
		},
		_int: function() {

			this.eacheall();
			this.tableallwidthmax();

		},
		selectcheckbox: function() {

         

			//匹配右边自动勾选

			//-----右边------
			var pre_calcubox = $("#pre_calcu_order_page");
			var fieldset = pre_calcubox.find("fieldset");
			if (fieldset.length > 0) {

				var checkval = [];
				for (var tr = 0; tr < fieldset.length; tr++) {
					var trobj = fieldset.eq(tr);
					var trlast = trobj.find("tr:gt(0)");
					for(var gti=0;gti < trlast.length;gti++){
					var itmetr=trlast.eq(gti);
					var td = itmetr.find("td:first");
					var tdtxt = $.trim(td.text());
					if(typeof tdtxt!="undefined" && tdtxt!=""){
					checkval.push(tdtxt);
				    }
				   }
				}

				var d = checkval.delarry();
				var vararrl = [];
				for (var key in d) {
					var chekvals = d[key];
					var valsring = chekvals.data;
					if (typeof valsring == "string" && valsring != "" && valsring!="undefined") {
						vararrl.push(valsring);
					}
				}

				//------左边--------

				if (vararrl.length > 0) {
					var checkboxobj = [];
					var receive_allocatebox = $("#receive_allocate_view");
					var setbox = receive_allocatebox.find(".set");
					for (var vals in vararrl) {
						var dval = vararrl[vals];
                        if(typeof dval == "string" && dval != "" && dval!="undefined"){
						for (var s = 0; s < setbox.length; s++) {
							var setobj = setbox.eq(s).find("legend :checkbox:first");
							var checkval = setobj.attr("data-pname");							
							var chesk=setobj.prop("checked");
							if (dval == checkval && !chesk) {
								setobj.prop("checked", true);
								break;
							}
						}

                        }
					}

				}
			}
		

		},
		tableallwidthmax: function() {

			//统一最大宽度
			var fieldset = $("#receive_allocate_view fieldset");
			var allwidth = [];
			if (fieldset.length > 1) {
				for (var i = 0; i < fieldset.length; i++) {
					var itmes = fieldset.eq(i);
					var width = itmes.width();
					allwidth.push(width);
				}
				var maxpx = Math.max.apply(null, allwidth);
				fieldset.width(maxpx);
			}

		},
		eacheall: function() {

			var receive_allocate = $("#receive_allocate_view fieldset");
			for (var i = 0; i < receive_allocate.length; i++) {
				var fieldset = receive_allocate.eq(i);
				this.Eventrun(fieldset);
				this.tablemargin(fieldset);
			}

		},
		tablemargin: function(fieldset) {
			var table = fieldset.find("table:first");
			table.css("margin", "0px auto 0px 0px");
		},
		Eventrun: function(fieldset) {
			var b2bthis = Sealingcalculation;
			//input对象
			var all = fieldset.find("input[rel='all']");
			var clp = fieldset.find("input[rel='clp']");
			var blp = fieldset.find("input[rel='blp']");
			var oviq = fieldset.find("input[rel='Orig']");


			//被隐藏对象
			var clpli = clp.parents("tr").eq(0);
			var blpli = blp.parents("tr").eq(0);
			var oviqli = oviq.parents("tr").eq(0);

			blpli.find(".blue").text(all.attr("data-blpname"));

			var clprel = parseInt(all.attr("data-clptotalpiece"));

			var blprel = parseInt(all.attr("data-blptotalpiece"));



			var allval = parseInt(all.val());



			all.keyup(function() {

				var _this = $(this);
				allfun(_this);
				if (!isNaN(parseInt(_this.val()))) {
					b2bthis.postvies(_this);
				}

			});

			function nablr() {
				all.val(allval);
				allfun(all);
				return false;
			}

			function allfun(_this) {

				var _val = parseInt(_this.val());

				if (_val > allval) {
					return nablr();
				}

				if (!isNaN(_val) && _val <= allval) {
					var clpnun = parseInt(_val / clprel);
					var blpnun = _val / blprel;


					if (clpnun > 0 && allval >= clprel) {
						clpli.show();
						clp.val(clpnun);
					} else {
						clpli.hide();
						clp.val("");
					}


					//clp剩下
					var sr = 0,
						blpall = 0,
						vipoall = 0;
					if (clpnun > 0) {

						sr = clpnun * clprel;
						blpall = _val - sr;

					}

					if (blpall == 0 && _val >= blprel) {

						blpall = _val;

					}


					var blpval = 0;
					if (!isNaN(parseInt(blpall / blprel)))
						blpval = parseInt(blpall / blprel);

					if (blpval > 0) {
						blpli.show();
						blp.val(blpval);
					} else {
						blpli.hide();
						blp.val("");
					}

					vipoall = _val - (sr + (blpval * blprel));

					if (vipoall > 0) {

						oviqli.show();
						oviq.val(vipoall);

					} else {

						oviqli.hide();
						oviq.val(0);

					}

					var clpval = parseInt(clp.val());
					var val_clpnun = clpval * clprel;

					if (_val == clprel || val_clpnun == _val) {
						blpli.hide();
						blp.val("");
						oviqli.hide();
						oviq.val("");
					}


				}

				if (isNaN(_val)) {
					clereli();
				}


			}

			//第一次默认运行
			allfun(all);

			function clereli() {

				oviqli.show();
				clpli.show();
				blpli.show();
				clp.val("");
				blp.val("");
				oviq.val("");
				all.val("");

			}

			//clp操作
			clp.keyup(function() {
				var _this = $(this);
				var _val = parseInt(_this.val());
				if (allval > clprel && !isNaN(_val)) {

					//clp总数
					var clprunall = _val * clprel;

					//blp总数
					var c_blpval = parseInt(blp.val());
					var blprunall = 0;
					if (!isNaN(c_blpval))
						blprunall = c_blpval * blprel;

					//oviq数
					var c_oviqval = 0;
					if (!isNaN(parseInt(oviq.val())))
						c_oviqval = parseInt(oviq.val());

					//状态总数
					var c_valall = clprunall + blprunall + c_oviqval;

					if (c_valall < allval) {
						all.val(c_valall);
						b2bthis.postvies(all);

					} else {
						if (c_valall > allval)
							b2bthis.alertinfo(_this);
						return nablr();
					}

				} else {
					_this.val("");
				}

			});

			//blp操作
			blp.keyup(function() {
				var _this = $(this);
				var _val = _this.val();
				if (allval > blprel && !isNaN(_val)) {

					var b_clpval = 0;
					if (!isNaN(parseInt(clp.val())))
						b_clpval = parseInt(clp.val());

					var b_blpallval = _val * blprel;
					var b_clpallval = b_clpval * clprel;

					var b_oviqval = 0;
					if (!isNaN(parseInt(oviq.val())))
						b_oviqval = parseInt(oviq.val());

					var b_allval = b_blpallval + b_clpallval + b_oviqval;
					if (b_allval < allval) {
						all.val(b_allval);
						b2bthis.postvies(all);
					} else {
						if (b_allval > allval)
							b2bthis.alertinfo(_this);
						return nablr();
					}

				} else {
					_this.val("");
				}

			});

			//ovip操作
			var ovipsettime = "";
			oviq.keyup(function() {
				var _this = $(this);
				var _val = parseInt(_this.val());
				if (!isNaN(_val)) {
					var o_clpval = 0;
					if (!isNaN(parseInt(clp.val())))
						o_clpval = parseInt(clp.val());

					var o_clpallval = o_clpval * clprel;

					var o_blpval = 0;
					if (!isNaN(parseInt(blp.val())))
						o_blpval = parseInt(blp.val());

					var o_blpallval = o_blpval * blprel;
					var o_ovipallval = o_clpallval + o_blpallval + _val;
					if (o_ovipallval < allval) {
						all.val(o_ovipallval);
						b2bthis.postvies(all);
					} else {
						if (o_ovipallval > allval)
							b2bthis.alertinfo(_this);
						if (typeof ovipsettime != "undefined" && ovipsettime != "") {
							clearTimeout(ovipsettime);
						}
						ovipsettime = setTimeout(function() {
							return nablr();
						}, 600);
					}


				} else {
					_this.val("");
				}


			});



		},
		postvies: function(obj) {
			var _this = this;
			var postTimes = _this.global.postTimes;
			if (typeof postTimes != "undefined" && postTimes != "") {
				clearTimeout(_this.global.postTimes);
			}
			_this.global.postTimes = setTimeout(function() {


				var allocate_view = $("#receive_allocate_view");
				var fieldsetall = allocate_view.find(":checkbox");
				for (var i = 0; i < fieldsetall.length; i++) {
					var checkboxitme = fieldsetall.eq(i);
					checkboxitme.prop("checked", false);
				}

				var checkbox = obj.parents("fieldset").eq(0).find(":checkbox:first");
				putToB2BWaybillForItem(checkbox.attr("id"));
				checkbox.prop("checked", true);

			}, 200);
		},
		alertinfo: function(obj) {
			var span = $("<span style='color:red;font-size:12px' class='spaninfo'>超出总默认数</span>");
			var spaninfo = $(".spaninfo");
			var pli = obj.parents("td").eq(0);

			if (spaninfo.length < 1) {
				span.css({
					"display": "inline-block"
				}).appendTo(pli);
			} else {
				spaninfo.css({
					"display": "inline-block"
				});
			}
			obj.css("border", "1px solid red");
			if (typeof this.global.setTimesr != "undefined" && this.global.setTimesr != "") {
				clearTimeout(this.global.setTimesr);
			}
			this.global.setTimesr = setTimeout(function() {
				obj.css("border", "1px solid #ccc");
				$(".spaninfo").remove();
			}, 800);

		}


	};

	window.Sealingcalculation = Sealingcalculation || {};


})(jQuery);

// $(function() {
// 	console.log(123);
// 	Sealingcalculation._int();
// })