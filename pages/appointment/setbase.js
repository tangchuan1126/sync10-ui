requirejs(['/Sync10-ui/requirejs_config.js'], function() {
	requirejs(["oso.lib/nprogress/nprogress.min", "jquery", "require_css!oso.lib/nprogress/nprogress",
	"require_css!bootstrap-css/bootstrap.min", 
	//"oso.lib/fullcalendar/calendar_push",
	"oso.bower/fullcalendar/dist/fullcalendar"],
	function(NProgress, $) {
		$("body").show();
		NProgress.start();
		//#####==========calendar -set
		var cssfileall = [
			"require_css!Font-Awesome",
			"require_css!oso.bower/fullcalendar/dist/fullcalendar",
			"require_css!oso.lib/fullcalendar/style.calendar"//
			//,"oso.bower/fullcalendar/dist/lang/zh-cn"
		];
		requirejs(cssfileall, function(fullCalendar) {
			$(function() {
				//$('#calendar').fullCalendar("redata",{storage_id:1});
					$('#calendar').fullCalendar({
						header: {
							left: 'prev,today,next',
							center:'title',
							right: ''
						},
						//height:410,
						//width:590,
						defaultDate: new Date(),
						editable: true,
						eventLimit: true,
						tdcellclick:function(todate){
							//每单击td事件回调反回todate--年月日
							console.log(this);
							console.log(todate);
						},
						events: {
							url: '/Sync10/_load/sumWorkByStartEnd',
							type: 'GET',
							data: {
								storage_id: '1000005'
							},
							error: function() {
								alert('there was an error while fetching events!');
							}
						}
					});
					NProgress.done();
				});
		});
	//####==========calendar end
	});
	//requirejs_config end
});
// nprogress end