﻿"use strict";
define([
    "config",
    "jquery",
    "backbone"
], function (page_config, $, Backbone) {
    var AppointmentModel = Backbone.Model.extend({
        url: page_config.addAppointment.url,
        idattribute: "id",
        contentType: "charset=utf-8",
        defaults: {
			key: "",
            storage_id: "",
            storage_name: "",
            storage_linkman: "",
            storage_linkman_tel: "",
            carrier_id: "",
            carrier_name: "",
            appointment_time: "",
            carrier_linkman: "",
            carrier_linkman_tel: "",
			carrier_email: "",
            etd: "",
            eta: "",
            licenseplate: "",
            driver_license: "",
            driver_name: "",
            invoices: [],
            status: "open",
        }
    });

    return {
        AppointmentModel: AppointmentModel

    }
});