﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"], function ($, Backbone, handlebars, templates, CompondBox, AsynLoadQueryTree) {
    return Backbone.View.extend({
        el: "#containerAddWarehouse",
        template: templates.v3AddWarehouse,
        render: function () {
            var v = this;
            var temp = new CompondBox({
                content: v.template().trim(),
                title: "Hub & Contacts",
                container: v.el,
                isExpand: true
            });
            v.init();
            
        },
        init: function () {
            var fromTree = new AsynLoadQueryTree({
                renderTo: "#inputAddWareHourse",
                dataUrl: "./json/shipTo.json",
                selectid: 100199,//非异步时默认选中
                scrollH: 400,//多高后出现滚动条
                Async: false,//非异步获取树节点，（一次加载完成）
                multiselect: false,//多选
                PostData: { "a": "1", "b": "2" },//异步时请求每个节点时往服务端传值,
                placeholder: "Please input Hub",//默认input框值
                Parentclick: true//开启父节点可以选中
            });

            fromTree.render();


            fromTree.on("Initialize", function (e) {
                $("#inputAddWareHourseLinkman").val(e.getSelectedNodes()[0].linkman);
                $("#inputAddWareHourseTel").val(e.getSelectedNodes()[0].tel);
            });

            fromTree.on("events.Itemclick", function (treeId, treeNode, treeNodeAll) {
                $("#inputAddWareHourseLinkman").val(treeNodeAll.linkman);
                $("#inputAddWareHourseTel").val(treeNodeAll.tel);
                //console.log(treeNode);
                //console.log(treeNodeAll);
            });
        }
    });
});