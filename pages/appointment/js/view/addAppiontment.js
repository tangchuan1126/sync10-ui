﻿"use strict";
define(["jquery", "backbone", "handlebars", "slidePanel"], function ($, Backbone, handlebars, SlidePanel) {
    return Backbone.View.extend({
        //template: templates.addAppiontment,
        render: function () {

            $('#addAppiontment').click(function () {
                var slide = new SlidePanel({
                    url: "/sync10-ui/pages/WMSAppointment/addAppiontment.html",
                    title: 'Create Appoiontment',
                    duration: 500,
                    topLine: 0,
                    slideLine: "85%"
                });
                slide.open();
            });
        }
    });
});