"use strict";
var all="";
define(["jquery",
    "backbone",
    "handlebars",
    "templates",
    "bootstrap.datetimepicker",
    "immybox",
    "CompondList/View",
    "art_Dialog/dialog-plus",
    "../config/appiontmentConfig",
    "slidePanel",
    "config",
    "../model/appiontment",
    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
    "dateUtil",
	"oso.lib/table/js/table",
    "oso.bower/moment/min/moment-with-locales.min",
    "require_css!bootstrap.datetimepicker-css"

    ],
    function ($, Backbone, handlebars, templates, datetimepicker, immybox, CompondList, Dialog, list_config, SlidePanel, Config, Model, AsynLoadQueryTree,dateUtil,Table,moment) {
        return Backbone.View.extend({
            defaultStore : "1000005",
			key:"",
            chooseHour:"",
            chooseType:"",
            chooseDay:"",
			myStores:[],
			boundType: "",
            initialize: function (options) {
                var v = this;
                /*window.onbeforeunload =function(){   //点击左边菜单关掉预约页面占车位减一
                   var params = {
                        "storageId": $("#searchCustomer").attr("data-value"),
                        "date": moment(v.chooseDay,"MM/DD/YYYY").format("YYYY-MM-DD"),
                        "hour": v.chooseHour,
                        "type": v.chooseType,
                        "key":v.key
                    };
                    if(!v.flags){
                        //调用减占位服务
                        $.ajax({
                            url: "/Sync10/_load/cancelAppointment?"+ jQuery.param(params),
                            type: 'GET',
                            dataType: 'json',
                            async:false,
                            timeout: 30000,
                            error: function(XHR,textStatus,errorThrown) {
                            }
                        }).done(function(data){
                            if (data.status=="200") {
                                v.flags = true;
                                v.initTime({"storage_id": $("#searchCustomer").attr("data-value") , "date":moment(v.chooseDay,"MM/DD/YYYY").format("YYYY-MM-DD")});
                            }
                        });
                    }
                }*/
                if (options.psid) v.defaultStore = options.psid;
				v.myStores = options.myStores || v.myStores;
                $(function () {
                    v.appiontmentCalendarInit(v.defaultStore);
                    v.advancedSearchInit();
                    v.luceneSearchInit();
                    v.quickCreateInit();
					//hyq has add code as follows(2015-3-27)
					$(".calendar-time tbody tr:odd").css("background","#f5f6f5");
                });
				window.MyApi={};
            },
            listView : function (param) {
                var hourDate="";
                //hyq has add code about hour(2015-5-22)
                if($("#inputAppointmentTime").val()){  //选择了日期
                    if($("#inputAppointmentTimeHour").val()){  //选择了小时
                        var rqsj = $.trim($("#inputAppointmentTime").val())+" "+$.trim($("#inputAppointmentTimeHour").val());
                        //$("#inputAppointmentTime").val(rqsj);
                        hourDate = rqsj
                    }
                }else{
                    if($("#inputAppointmentTimeHour").val()){  //选择了小时
                        var today = (new Date()).format('MM/dd/yyyy');
                        //$("#inputAppointmentTime").val(today);
                        //var rqsj = $.trim($("#inputAppointmentTime").val())+" "+$.trim($("#inputAppointmentTimeHour").val());
                        //$("#inputAppointmentTime").val(rqsj);
                        var rqsj = $.trim(today)+" "+$.trim($("#inputAppointmentTimeHour").val());
                        hourDate = rqsj
                    }
                }
                var obj = {
                    //"selectOrderType": $("#selectOrderType").attr("data-value"),
                    "orderNum": $("#inputOrder").val(),
                    //"inputAppointmentTime": $("#inputAppointmentTime").val(),
                    "inputAppointmentTime": hourDate,
                    "searchFrom": $("#searchFrom").attr("data-value")
                };
				if ($("#inputCarrier").val()!="") {
					obj.inputCarrier = $("#inputCarrier").attr("data-value")
				}
                if (!$("#searchFrom").attr("data-value")) {
                    if (!param) {
                        obj.searchFrom = this.defaultStore;
                    }
                }
                var list = new CompondList(list_config, {
                    renderTo: "#appiontmentList",
                    dataUrl: Config.appiontmentList.url,
                    PostData: $.extend(obj , param )
                });
                /*list.render();
                return list;*/
                list.render();
                return list;

            },
            render: function () {
                var v = this;
                var list = this.listView();
                //列表.
                var listView = list;
                this.initCal();
                //搜索按钮.
                $("#btnListSearch").on("click", function () {
                    v.listView();
                });
                $("#btnReset").on("click", function () {
                    $("#selectOrderType").attr("data-value", "all").val("All Type");
                    $("#inputOrder").val("");
                    $("#inputAppointmentTime").val("");
                    //hyq has a clear about hour
                    $("#inputAppointmentTimeHour").val("");
                    $("#inputCarrier").attr("data-value", "").attr("data-name", "").val("");
                    $("#searchFrom").attr("data-value", "").attr("data-name", "").val("");
                    v.listView();
                });
                //快速创建.
                $("#btnQuickSave").on("click", function () {
                    var f = true;
                    if ($("#inputQuickCarrier").val() == "" | $("#inputQuickCarrier").val() == undefined) {
                        if (!$("#inputQuickCarrier").parent().hasClass("has-feedback")) {
                            $("#inputQuickCarrier").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                        }
                        f = false;
                    } else {
                        $("#inputQuickCarrier").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                    }
                    if ($("#inputQuickAppointmentTime").val() == "" | $("#inputQuickAppointmentTime").val() == undefined) {
                        if (!$("#inputQuickAppointmentTime").parent().hasClass("has-feedback")) {
                            $("#inputQuickAppointmentTime").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                        }
                        f = false;
                    } else {
                        $("#inputQuickAppointmentTime").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                    }

                    if (f) {
                        var model = new Model.AppointmentModel();
						v.defaultStore = $("#searchCustomer").attr("data-value");
                        var defaultStoreName = $("#searchCustomer").val();
						var yyyymmddhh = v.selecDate;
                        model.save({
							storage_id: v.defaultStore,
							storage_name: defaultStoreName,
                            carrier_id: $("#inputQuickCarrier").attr("data-value"),
                            carrier_name: $("#inputQuickCarrier").val(),
							appointment_type: v.boundType?v.boundType:'outbound',
                            appointment_time: $("#inputQuickAppointmentTime").val()
                        },{
                            success: function (e,r) {
								if (e.changed.result != 0) {
                                    var d = new Dialog({
                                        content: e.changed.error,
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Save fail',
                                        cancelVal: 'close',
                                        cancel: function () { }
                                    }).show();
                                } else {
                                    $("#inputQuickCarrier").val("").attr("data-value", "").attr("data-name", "");
                                    v.listView({
										"inputAppointmentTime":$("#inputQuickAppointmentTime").val(),
										"searchFrom": v.defaultStore
									});
									$("#inputQuickAppointmentTime").val("");
									if (yyyymmddhh) {
										v.initTime({
											"storage_id": v.defaultStore,
											"date": yyyymmddhh
										});
									}
                                }
                            },
                            faile: function (e) {
                                var d = new Dialog({
                                    content: "Save Fail, Please try again!",
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    title: 'save fail',
                                    cancelVal: 'close',
                                    cancel: function () { }
                                }).show();
                            }
                        });
                    }
                });
                //创建.
                /*$('#addAppiontment').click(function (a,b,c) {  //a指当前对象，b触发该事件传过来的参数
                    //console.log(a);
                    //console.log(b);
                    //console.log(c);  //后台传过来的key值，有key值表示时间是可预约，否则不能预约
                    var inputQuickAppointmentTime = b;
                    //console.log("时间"+inputQuickAppointmentTime);
                    if(v.key){   //有key值传过来
                        inputQuickAppointmentTime = inputQuickAppointmentTime ? "?name="+inputQuickAppointmentTime:"";
                        if (inputQuickAppointmentTime) {
                            inputQuickAppointmentTime += "&bound_Type="+ v.boundType;
                        } else {
                        }
                        inputQuickAppointmentTime += "?key="+ v.key;
                        MyApi.opensdiag = function(data, vf, orderNo) {
                            var d = Dialog(data);
                            d.reset();
                            d.showModal();
                            vf.loadList(orderNo);
                        }
                        var slide = new SlidePanel({
                            url: Config.createAppiontement.url + inputQuickAppointmentTime,
                            title: 'Create Appointment',
                            duration: 500,
                            topLine: 0,
                            zindex: 100,
                            slideLine: "85%",
                            closebefore:function(){   //关闭约车页面之前
                                if(!slide.flag)
                                {
                                    var d = new Dialog({
                                    content: 'Are you sure to cancel this time about the appointment?',
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 70,
                                    title: 'Cancel Appointment',
                                    okValue: 'Yes',
                                    ok: function () {
                                        slide.flag = true;
                                        //调用减占位服务
                                        slide.close();  //关闭当前约车页面
                                    },
                                    cancel: function () {
                                    }
                                    }).showModal();
                                    return false;
                                }
                            }
                        });
                        slide.open();
                    }else{
                        var d = new Dialog({
                            content: "The time you have selected has elasped the currect time",
                            icon: 'question',
                            lock: true,
                            width: 200,
                            height: 40,
                            //title: 'Prompt',
                            title:'Warning!',
                            cancelValue: 'Close',
                            cancel: function () { },
                            close:function(){
                                d.remove();
                            }
                        }).show();
                        return;
                    }
                });*/
                list.on("events.UpdateAppiontmentDate", function (e) {
                    var btntxt = e.button.text();
                    var btntxt = e.button.text();
                    var d = new Dialog({
                        content: 'Are you sure to cancel this time about the appointment?',
                        icon: 'question',
                        lock: true,
                        width: 200,
                        height: 70,
                        title: 'Cancel appointment',
                        okVal: 'Yes',
                        ok: function () {
                            $.ajax({
                                type: 'post',
                                url: Config.delAppiontment.url,
                                data: 'aid=' + e.data.linedata.trid,
                                dataType: 'json',
                                success: function (data) {
									v.defaultStore = $("#searchCustomer").attr("data-value");
                                    //console.log((new Date()).format('MM/dd/yyyy'));
                                    v.listView({
										"searchFrom": v.defaultStore,
										"inputAppointmentTime":(new Date()).format('MM/dd/yyyy')
									});
									//$('#calendar').fullCalendar("restorage_id",v.defaultStore);
									v.initTime({
										"storage_id": v.defaultStore,
										"date": v.getCurrentDate()
									});
                                },
                                error: function () {
                                }
                            });
                        },
                        cancelVal: 'No',
                        cancel: function () {
                        }
                    }).showModal();
                });
                list.on("events.Edit", function (e) {
                    var btntxt = e.button.text();
                    //console.log(e.data.linedata.trid);
                    var slide = new SlidePanel({
                        title: "Edit Appointment",
                        url: Config.createAppiontement.url + "?appid=" + e.data.linedata.trid + "&type=Edit", 
                        duration: 500,
                        topLine: 0,
                        slideLine: "85%"
                    });
                    slide.open();
                });
                list.on("events.more1", function (e) {
                    //console.log(e);
                    var btnobj = e.button;
                    var table = btnobj.parents(".tbInnerInvoice").find(".toggle");
                    table.toggle(800);
                    if($.trim(btnobj.text()) == "More..."){  //是隐藏
                        btnobj.text("collapse");
                    }else{
                        btnobj.text("More...");
                    }
                    //btnobj.html("open");
                        /*table.show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeInDown");
                            e.button.html("Hide");
                        });*/
                    
                    /* else {
                        table.addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeOutUp").hide();
                            e.button.html("More...");
                        });
                    }*/
                });

				list.on("events.more", function (e) {
                    var btnobj = e.button;
                    var table = btnobj.parents(".Log_container").find(".trHidden");
                    if (table.is(":hidden")) {
                        table.show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeInDown");
                            e.button.html("Hide");
                        });
                    } else {
                        table.addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeOutUp").hide();
                            e.button.html("More...");
                        });
                    }
                });
                //hyq has add code(2015-5-21)
                list.on("events.moreOption1", function (e) {
                    var btnobj = e.button;
                    var table = btnobj.parents(".td_Invoice").find(".trHidden");
                    if (table.is(":hidden")) {
                        table.show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeInDown");
                            e.button.html("Hide");
                        });
                    } else {
                        table.addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeOutUp").hide();
                            e.button.html("More...");
                        });
                    }
                });



					//点击“更多”触发
					list.on("events.LoadEvent", function (e) {
						window.location.href = "/Sync10-ui/pages/load/index.html?loadno="+ e.button.attr("data-no");
						//console.log(e.button.attr("data-no"));
					});
			        list.on("events.moreOption", function (e) {
					//	console.log(e.data.jet_lag);    //打印后台返回的数据
			        	var d = new Dialog({
			        	    content: "<div id='logsTableList'></div>",
			        	    width: 600,
			        	    height: 300,
			        	    title: 'Logs',
			        	    cancelValue: 'Close',
			        	    cancel: function () { },
			        	    onshow:function(){
    		        				var table2 = new Table({
    		        	                container: '#logsTableList',
    		        	                height: 230,
    		        	         		localData : e.data.Log.LOGDETAILS,   //后台返回数据
    		        	            //    groupKey: "connect",
    		        	                rownumbers: false,
    		        	            //    checkbox: true,
    		        	                singleSelection: true,
    		        	                pagination: true,
    		        	                pageSize: 10,
    		        	                columns: [
    		        	                    { field: 'USER', title: 'USER', width: 50 },
    		        	                    { 
    		        	                    	field: 'REMARK', title: 'REMARK', width: 50 ,
	    		        	                    formatter : function (data, row){
													return "<font color='#FF6600'>" + data  + "</font>";
												}
											},
    		        	                    { 
    		        	                    	field: 'DATETIME', title: 'DATETIME', width: 80 ,
    		        	                    	formatter :function (data, row){

                                                    var dfff = moment(  data ,"MM/DD/YYYY HH:mm").format("x")

                                        var newP = parseInt(dfff);
                                         var p = parseInt( e.data.jet_lag || 0);

                                                    var doneTime = moment(  newP + p     ).format("MM/DD/YYYY HH:mm");

                                                    return doneTime;
												}
											}
    		        	                ]
    		        	            });
									//设置table的高度不变
									$(".table-content").css("height",230);

			        	    }
			        	}).showModal();
			        });
                //创建保存和修改监听.
                window.addEventListener('message', function (e) {
                    $(".slide-header-close").click();
					if (v.selecDate) {
						var param = {"storage_id":v.defaultStore, "date":v.selecDate};
						v.initTime(param);
                        var selectParam = {
                             "inputAppointmentTime": moment(v.selecDate).format("MM/DD/YYYY"),
                             "searchFrom": $("#searchCustomer").attr("data-value"),
                             "boundType" : ""//入库
                        };
						v.listView(selectParam);
					} else {
						var param = {"storage_id":v.defaultStore, "date":v.getCurrentDate()};
						v.initTime(param);
                        var selectParam ={
                             "inputAppointmentTime": new Date().format("MM/dd/yyyy"),
                             "searchFrom": $("#searchCustomer").attr("data-value"),
                             "boundType" : ""//入库
                        };
						v.listView(selectParam);
					}
                }, false);

                /*$("#inputAppointmentTime").datetimepicker(
                    { 
                        autoclose: 1,
                        minView: 1,
                        forceParse: 0, 
                        format:'mm/dd/yyyy hh:00',
                        bgiconurl:"" 
                    }
                );*/
                //布局更改需要的
                $("#inputAppointmentTime").datetimepicker(
                    { 
                        autoclose: 1,
                        minView: 2,
                        forceParse: 0, 
                        format:'mm/dd/yyyy',
                        bgiconurl:"" 
                    }
                );
                $("#inputAppointmentTimeHour").datetimepicker(
                    { 
                        autoclose: 1,
                        startView: 1,
                        minView:1,
                        forceParse: 0, 
                        format:'hh:00',
                        bgiconurl:"" 
                    }
                );
                //下面两行样式控制时分插件头部删除
                $("div[rel='picker-inputAppointmentTimeHour'] table").css({
                    "width":"190px"
                });
                $("div[rel='picker-inputAppointmentTimeHour'] table thead").css({
                    "display":"none"
                });
                //console.log($("#inputAppointmentTimeHour").datetimepicker());
                
            },
            appiontmentCalendarInit:function(psid){
                var v = this;
                $("#appiontmentCalendarTab").html(templates.appiontmentCalendar().trim());
				$.getJSON(Config.shipTo.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].data,"text":json[i].name});
					}
					$('#searchCustomer').immybox({
						//Pleaseselect:'<i>Clean Select</i>',
						choices: rls,
						Defaultselect: v.defaultStore,
						change:function(e,d){
                           if (d.value) {
							//设置选择的参看仓库 ID
							v.defaultStore = d.value;
							//触发calendar的查询方法
							ckSearch();
							v.showConfigButton(v.myStores,v.defaultStore);
							}
						}
					});
				});
				//选择仓库触发查询
				function ckSearch(){
				var mydates=$('#calendar');
					var setdatas=mydates.data("DateTimePicker");
					var dates = setdatas.date().toDate().format("yyyy-MM-dd");
					var todate = {"dates" : dates};
					 v.selecDate = todate.dates;
					 var param = {"storage_id":v.defaultStore , "date":todate.dates};
					 v.initTime(param);
					 var _tdate = moment(todate.dates).format("MM/DD/YYYY");
					 $("#appiontmentInfo").html(_tdate);
					 
					 $('#calendar').data("appointmentDate",_tdate);
					 todate.storage_id = $("#searchCustomer").attr("data-value");
					 v.todate = todate;
					 v.listView({
						 "inputAppointmentTime": _tdate,
						 "searchFrom": $("#searchCustomer").attr("data-value")
					 });
				}
                var list;
                //列表.
                var listView = function (param) {
                    list = new CompondList(list_config, {
                        renderTo: "#appiontmentList",
                        dataUrl: Config.appiontmentList.url,
                        PostData: param
                    });
                    list.render();
                }
             //直接点击时间的事件 不区分boundType
             var initTimeParam = {"storage_id":v.defaultStore, "date":v.getCurrentDate()  };
             v.initTime(initTimeParam);
             //鼠标浮在all圆角上
             $(".timeBadgeAllbound").hover(function(){ 
                $(this).css("cursor","pointer"); 
                if($(this).prev("a").hasClass("time")){  //有time表示不是过期时间圆角
                    $(this).css({
                        "background":"#539E07"
                    });
                }
             },
             function(){
                if($(this).prev("a").hasClass("time")){  //有time表示不是过期时间圆角
                    $(this).css({
                        "background":"rgb(75,211,100)"
                    })
                }
             }
             )
             //鼠标浮在inbound圆角上
             $(".timeBadgeInbound").hover(function(){ 
                $(this).css("cursor","pointer"); 
                if($(this).prev("a").hasClass("time")){  //有time表示不是过期时间圆角
                    $(this).css({
                        "background":"#539E07"
                    });
                }
             },function(){
                 if($(this).prev("a").hasClass("time")){  //有time表示不是过期时间圆角
                    $(this).css({
                        "background":"rgb(75,211,100)"
                    })
                }
             })
             //鼠标浮在outbound圆角上
             $(".timeBadgeOutbound").hover(function(){ 
                $(this).css("cursor","pointer"); 
                if($(this).prev("a").hasClass("time")){  //有time表示不是过期时间圆角
                    $(this).css({
                        "background":"#539E07"
                    });
                }
             },function(){
                if($(this).prev("a").hasClass("time")){  //有time表示不是过期时间圆角
                    $(this).css({
                        "background":"rgb(75,211,100)"
                    })
                }
             })

            $(".timeBadgeAllbound").click(function(evt){  //all圆角点击事件
                 var dataId = $(this).prev("a").data("id");
                 var appointmentDate = $('#calendar').data("appointmentDate");
                 if (appointmentDate) {
                 } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');   //没有选择日期则默认获取当天日期
                 }
                 v.boundType =  dataId>19?"inbound":"outbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }
                v.listView({
                    "inputAppointmentTime":inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value")
                });
             });
            $(".timeBadgeInbound").click(function(evt){  //inbound圆角点击事件
                var dataId = $(this).prev("a").data("id");
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                   appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
                v.boundType = "inbound";
                //v.boundType = "Inbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }
                v.listView({
                    "inputAppointmentTime": inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value"),
                    "boundType" : "inbound"//入库
                });
            });
            $(".timeBadgeOutbound").click(function(evt){  //outbound圆角点击事件
                var dataId = $(this).prev("a").data("id");
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
                v.boundType = "outbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }
                v.listView({
                    "inputAppointmentTime": inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value"),
                    "boundType" : "outbound"  //出库
                });
            });
				$(".timeTextAllbound").click(function(evt){  //all整点时间点击事件
					var tid = $(this).attr("data-id");
					if ($(this).hasClass("enable")) {
						if ($(this).hasClass("time")) {
							var d = new Dialog({
								//content:"<span style='font-size:16px'>Select Type</span>",
                                content:"",
								icon: 'question',
								lock: true,
								width: 200,
								//height: 30,
								title: 'Select Type',
							    button:[
                                {
                                    value: 'Inbound',
                                    callback: function () 
                                    {
                                    v.boundType = "inbound";
                                      v.showAdd("inbound",tid);
                                    },
                                    autofocus: false
                                },
                                {
                                    value: 'Outbound',
                                    callback: function () 
                                    {
                                        v.boundType = "outbound";
                                      v.showAdd("outbound",tid);
                                    },
                                    autofocus: false
                                }]
							}).show();
						} else {
							var d = new Dialog({
								content:"The time you have selected has elasped the currect time",
								icon: 'question',
								lock: true,
								width: 200,
								height: 40,
								//title: 'Prompt',
								title: 'Warning!',
								cancelValue: 'Close',
								cancel: function () {}
							}).show();
						}
					}
				});
				$(".timeTextInbound").click(function(evt){  //inbound整点时间点击事件
					var tid = $(this).attr("data-id");
					if ($(this).hasClass("enable")) {
						if ($(this).hasClass("time")) {
							v.showAdd("inbound",tid);
						} else {
							var d = new Dialog({
								content:"The time you have selected has elasped the currect time",
								icon: 'question',
								lock: true,
								width: 200,
								height: 40,
								//title: 'Prompt',
								title: 'Warning!',
								cancelValue: 'Close',
								cancel: function () {}
							}).show();
						}
					}
				});
				$(".timeTextOutbound").click(function(evt){  //outbound整点时间点击事件
					var tid = $(this).attr("data-id");
					if ($(this).hasClass("enable")) {
						if ($(this).hasClass("time")) {
							v.showAdd("outbound",tid);
						} else {
							var d = new Dialog({
								content:"The time you have selected has elasped the currect time",
								icon: 'question',
								lock: true,
								width: 200,
								height: 40,
								title: 'Warning!',
								cancelValue: 'Close',
								cancel: function () {}
							}).show();
						}
					}
				});
            },
            clickTD: function (dataId) {
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
                var inputAppointmentTime = appointmentDate;
                if (dataId) {
                    if (dataId<10) {
                        inputAppointmentTime = appointmentDate + "";
                    } else {
                        inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                    }
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= moment(new Date()).format("MM/DD/YYYY HH:00")) ) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }
                v.listView({
                    "inputAppointmentTime":inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value")
                });
            },
            appointmentCar:function(b,c){  //约车页面，第一个参数表示时间，第二个参数表示key，有key时间可约车
                var inputQuickAppointmentTime = b;
                var v = this;
                v.chooseDay = b;
                if(v.key){   //有key值传过来
                    inputQuickAppointmentTime = inputQuickAppointmentTime ? "?name="+inputQuickAppointmentTime:"";
                    if (inputQuickAppointmentTime) {
                        inputQuickAppointmentTime += "&bound_Type="+ v.boundType;
                    } else {
                    }
                    inputQuickAppointmentTime += "&key="+ v.key;
                    MyApi.opensdiag = function(data, vf, orderNo) {
                        var d = Dialog(data);
                        d.reset();
                        d.showModal();
                        vf.loadList(orderNo);
                    }
					window.onbeforeunload = function() {
						var params = {
							"storageId": $("#searchCustomer").attr("data-value"),
							"date": moment(b,"MM/DD/YYYY").format("YYYY-MM-DD"),
							"hour": v.chooseHour,
							"type": v.chooseType,
							"key":v.key
						};
						$.ajax({
							url: "/Sync10/_load/cancelAppointment?"+ jQuery.param(params),
							type: 'GET',
							dataType: 'json',
							timeout: 30000
						});
					}; 
                    var slide = new SlidePanel({
                        url: Config.createAppiontement.url + inputQuickAppointmentTime,
                        title: 'Create Appointment',
                        duration: 500,
                        topLine: 0,
                        zindex: 100,
                        slideLine: "85%",
                        closebefore:function(){   //关闭约车页面之前
                            var isAlert =sessionStorage.saveStatus;
                            if(!slide.flag && isAlert!=1)
                            {
                                var d = new Dialog({
                                content: 'Are you sure to cancel this time about the appointment?',
                                icon: 'question',
                                lock: true,
                                width: 200,
                                height: 70,
                                title: 'Cancel Appointment',
                                okValue: 'Yes',
                                ok: function () {
                                    slide.flag = true;
                                    var params = {
                                        "storageId": $("#searchCustomer").attr("data-value"),
                                        "date": moment(b,"MM/DD/YYYY").format("YYYY-MM-DD"),
                                        "hour": v.chooseHour,
                                        "type": v.chooseType,
                                        "key":v.key
                                    };
                                    v.boundType = v.chooseType;
									window.onbeforeunload = function() {};
                                    //调用减占位服务
                                    $.ajax({
                                        url: "/Sync10/_load/cancelAppointment?"+ jQuery.param(params),
                                        type: 'GET',
                                        dataType: 'json',
                                        timeout: 30000,
                                        error: function(XHR,textStatus,errorThrown) {
                                            
                                        }
                                    }).done(function(data){
                                        if (data.status=="200") {
                                            v.initTime({"storage_id": $("#searchCustomer").attr("data-value") , "date":moment(b,"MM/DD/YYYY").format("YYYY-MM-DD")});
                                        }
                                    });
                                    slide.close();  //关闭当前约车页面
                                },
                                cancel: function () {
                                }
                                }).showModal();
                                return false;
                            }
                            sessionStorage.saveStatus = 0;
                        }
                    });
                    slide.open();
                }else{
                    var d = new Dialog({
                        content: "The time you have selected has elasped the currect time",
                        icon: 'question',
                        lock: true,
                        width: 200,
                        height: 40,
                        title:'Warning!',
                        cancelValue: 'Close',
                        cancel: function () { },
                        close:function(){
                            d.remove();
                        }
                    }).show();
                    return;
                }
            },
			showAdd: function(type,hour) {
				var v = this;
				v.chooseHour = hour;
                v.chooseType = type;
				var appointmentDate = $('#calendar').data("appointmentDate");
				if (!appointmentDate) {
					appointmentDate = moment(new Date()).format("MM/DD/YYYY")
				}
				//makeAppointment
				var params = {
					"storageId": $("#searchCustomer").attr("data-value"),
					"date": moment(appointmentDate,"MM/DD/YYYY").format("YYYY-MM-DD"),
					"hour": hour,
					"type": type
				};
				v.boundType = type;
				$.ajax({
					url: "/Sync10/_load/makeAppointment?"+ jQuery.param(params),
					type: 'GET',
					dataType: 'json',
					timeout: 30000,
					error: function(XHR,textStatus,errorThrown) {
						//console.log(XHR);
						v.initTime({"storage_id": $("#searchCustomer").attr("data-value") , "date":moment(appointmentDate,"MM/DD/YYYY").format("YYYY-MM-DD")});
						new Dialog({
							content: XHR.responseJSON.message,
							icon: 'question',
							lock: true,
							width: 200,
							height: 40,
							title: 'Warning!',
							cancelVal: 'Cancel',
							cancel: function () { }
						}).show();
					}
				}).done(function(data){
					if (data.status=="200") {
						//console.log(data.key);
						v.key = data.key||"";
						var vhour = (hour>9?""+hour:"0"+hour);
						//$('#addAppiontment').trigger("click",appointmentDate+" "+vhour+":00",data.key);
                        v.appointmentCar(appointmentDate+" "+vhour+":00",data.key);
						v.initTime({"storage_id": $("#searchCustomer").attr("data-value") , "date":moment(appointmentDate,"MM/DD/YYYY").format("YYYY-MM-DD")});
					}
				});
				
			},
			initTime: function(param) {
				$.getJSON(Config.appiontmentCalendarTimeUrl.url, param , function (result) {
					$(".time").find("span.timeBadge").html(null);
					var data = result.DATA;
					$.each(data, function(i, e){
						var inboundbadge = (e.CNTIN+e.SUMIN)>0 ? e.CNTIN+'/'+ e.SUMIN : "";
						var outboundbadge = (e.CNTOUT+e.SUMOUT)>0 ? e.CNTOUT+'/'+ e.SUMOUT : "";
						var totalboundbadge = (e.TOTAL+e.ALL)>0 ? e.ALL +'/'+ e.TOTAL : "";
						var _id = parseInt(e.ID);
						if(e.TIME) {
							if (totalboundbadge=="") {
								$("#timeBadge"+ _id ).html(totalboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey;cursor:default;").removeClass("enable").addClass("time");
							} else {
								var totalboundbadgeCss = e.ALL>=e.TOTAL?"background-color:grey":"";
								$("#timeBadge"+ _id ).html(totalboundbadge).attr("style",totalboundbadgeCss).prev("a").attr("style","").addClass("enable").addClass("time");
							}
							if (inboundbadge=="") {
								$("#timeBadgeInbound"+ _id ).html(inboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey;cursor:default;").removeClass("enable").addClass("time");
							} else {
								var inboundbadgeCss = e.CNTIN>=e.SUMIN?"background-color:grey":"";
								$("#timeBadgeInbound"+ _id ).html(inboundbadge).attr("style",inboundbadgeCss).prev("a").attr("style","").addClass("enable").addClass("time");
							}
							if (outboundbadge=="") {
								$("#timeBadgeOutbound"+ _id ).html(outboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey;cursor:default;").removeClass("enable").addClass("time");
							} else {
								var outboundbadgeCss = e.CNTOUT>=e.SUMOUT?"background-color:grey":"";
								$("#timeBadgeOutbound"+ _id ).html(outboundbadge).attr("style",outboundbadgeCss).prev("a").attr("style","").addClass("enable").addClass("time");
							}
						} else {
							if (totalboundbadge=="") {
								$("#timeBadge"+ _id ).html(totalboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey;cursor:default;").removeClass("enable").removeClass("time");
							} else {
								$("#timeBadge"+ _id ).html(totalboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey").addClass("enable").removeClass("time");
							}
							if (inboundbadge=="") {
								$("#timeBadgeInbound"+ _id ).html(inboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey;cursor:default;").removeClass("enable").removeClass("time");
							} else {
								$("#timeBadgeInbound"+ _id ).html(inboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey").addClass("enable").removeClass("time");
							}
							if (outboundbadge=="") {
								$("#timeBadgeOutbound"+ _id ).html(outboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey;cursor:default;").removeClass("enable").removeClass("time");
							} else {
								$("#timeBadgeOutbound"+ _id ).html(outboundbadge).attr("style","background-color:grey").prev("a").attr("style","color:grey").addClass("enable").removeClass("time");
							}
						}
					});
				});
			},
            initCal : function(){
                var v = this;
                var cssfileall = [
					"require_css!Font-Awesome",
					"require_css!oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker.css",
					"require_css!oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker-plus.css",
					"oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
					"require_css!bootstrap.datetimepicker-css",
					"bootstrap.datetimepicker"
                ];
                require(cssfileall, function() {
                  $(function() {
                    var mydates=$('#calendar').Acars_datetimepicker({     //点击日历上日期触发
                     inline: true,
                     icons:{
                     previous: 'fa fa-chevron-circle-left',
                     next: 'fa fa-chevron-circle-right'
                     },
                     toolbar:false,
                     format: 'YYYY-MM-DD',
                     tdcellclick:function(dates){
                             var todate = {"dates" : dates};
                             v.selecDate = todate.dates;
                             var param = {"storage_id":v.defaultStore , "date":todate.dates};
                             v.initTime(param);
                             var _tdate = moment(todate.dates).format("MM/DD/YYYY");
                             $("#appiontmentInfo").html(_tdate);
                             $('#calendar').data("appointmentDate",_tdate);
                             todate.storage_id = $("#searchCustomer").attr("data-value");
                             v.todate = todate;
                             v.listView({
                                 "inputAppointmentTime": _tdate,
                                 "searchFrom": $("#searchCustomer").attr("data-value")
                             });
                     },
                    Initialization:function(setbuttondom){
                        v.showConfigButton( v.myStores , v.defaultStore , setbuttondom );
                        setbuttondom.click(function(){
                            v.defaultStore = $("#searchCustomer").attr("data-value");
							var para = "?storage_id="+ v.defaultStore; //jQuery.param(v.todate);
							var slide = new SlidePanel({
								url: Config.appiontementLimit.url + para,
								title: 'Appoiontment Config',
								duration: 500,
								topLine: 0,
								slideLine: "1020"
							});
							slide.open();
							//TODO
							window.addEventListener('message', function (e) {
								$(".slide-header-close").click();
								v.listView();
							}, false);
                        });
                    }
                });
                //外部取得时间
               var setdatas=mydates.data("DateTimePicker");
               var initDate = setdatas.date().toDate().format("yyyy-MM-dd");
            });
                  // window lod end
        });
        },
			showConfigButton: function(ls,defaultStore,setbuttondom) {
				if (ls.length>0) {
					for (var idx=0;idx<ls.length;idx++) {
						var sitem = ls[idx];
						if (sitem && sitem.POSTID==10 && sitem.DEPTID==1000008) {
                            setbuttondom.show();
						}
					}
				}
			},
            advancedSearchInit: function () {
                var v = this;
                $("#searchAdvancedTab").html(templates.advancedSearch().trim());
				$.getJSON(Config.shipTo.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].data,"text":json[i].name});
					}
					$('#searchFrom').immybox({
						Pleaseselect:'<i>Clean Select</i>',
						choices: rls
			
					});
				});
				$.getJSON(Config.carrierJson.url, function (result) {     //advancesearch中scac值获取
                    //hyq has add code（2015-4-3）(简称和全称连接)
                    $.each(result,function(index,obj){
                        obj.text = obj.value + " - "+ obj.text;    //简称和全称连接
                    })
                    $('#inputCarrier').immybox({
                        Pleaseselect:'<i>Clean Select</i>',
                        choices: result,
                        textname:"text",
                        valuename:"value"
                    });
                });

                $.getJSON(Config.orderType.url, function (result) {
                    $('#selectOrderType').immybox({
						//Pleaseselect:'<i>Clean Select</i>',
                        choices: result,
						change: function(e,d){
							if (d.value=="order") {
								$("#inputOrder").removeAttr("readonly");
								$("#inputOrder").attr("placeholder","Order NO.");
							} else if (d.value=="load") {
								$("#inputOrder").removeAttr("readonly");
								$("#inputOrder").attr("placeholder","Load NO.");
							} else {
								$("#inputOrder").attr("readonly","true");
								$("#inputOrder").attr("placeholder","");
							}
						}
                    });
                    $("#selectOrderType").attr("data-value", "all").val("All Type");
                });
            },   //高级搜索.
            luceneSearchInit:function(){
                $("#searchAppiontmentTab").html(templates.luceneSearch().trim());
            },
            quickCreateInit: function () {
				$.getJSON(Config.carrierJson.url, function (result) {
                    $.each(result,function(index,obj){
                        obj.text = obj.value + " - "+ obj.text;    //简称和全称连接
                    })
                    $('#inputQuickCarrier').immybox({
                        Pleaseselect:'<i>Clean Select</i>',
                        textname:"text",
                        valuename:"value",
                        choices: result
                    });
				});
                $("#inputQuickAppointmentTime").css("cursor","pointer");
                $("#inputQuickAppointmentTime").datetimepicker({ startDate: new Date(), autoclose: 1, minView: 1,forceParse: 0, format: 'mm/dd/yyyy hh:00', bgiconurl:""});
                $('.quick-create').on('click', function (e) {
                    e.stopPropagation();
                    return false;
                });
            },
            //获取当前日期 2015-02-13
            getCurrentDate:function (){
                var myDate = new Date();
                //myDate.getYear();        //获取当前年份(2位)
                var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
                var month = myDate.getMonth();       //获取当前月份(0-11,0代表1月)
                var date = myDate.getDate();        //获取当前日(1-31)
                //myDate.getDay();         //获取当前星期X(0-6,0代表星期天)
                //myDate.getTime();        //获取当前时间(从1970.1.1开始的毫秒数)
                var hours =myDate.getHours();       //获取当前小时数(0-23)
                var minutes = myDate.getMinutes();     //获取当前分钟数(0-59)
                var seconds = myDate.getSeconds();     //获取当前秒数(0-59)
                month++;
                if(month<=9)month="0"+month;
                if(date<=9)date="0"+date;
                return year + "-" +month+ "-" +date;// +" "+hours+ ":" +minutes+ ":" +seconds
            }
        });
    });
