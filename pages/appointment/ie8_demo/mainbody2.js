"use strict";
//css和js路径配置
requirejs.config({
    paths: {
        "oso.bower": "/Sync10-ui/bower_components",
        "oso.lib": "/Sync10-ui/lib",
        "jquery": "/Sync10-ui/bower_components/jquery/dist/jquery.min",
        "jquery1.9": "/Sync10-ui/lib/jquery1.9/jquery-1.9.0",
        "underscore": "/Sync10-ui/bower_components/underscore/underscore.min",
        "moment": "/Sync10-ui/bower_components/moment/min/moment-with-locales.min",
        "Backbone": "/Sync10-ui/bower_components/backbone/backbone.min",
        "Font-Awesome": "/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min",
        "config": "/Sync10-ui/pages/appointment/ie8_demo/config",
        "immybox": "/Sync10-ui/lib/immybox-master/jquery.immybox",
        "CompondBox": "/Sync10-ui/lib/compondBox/js/compondBox",
        "handlebars.runtime": "/Sync10-ui/bower_components/handlebars/handlebars.runtime.amd.min",
        "handlebars": "/Sync10-ui/bower_components/handlebars/handlebars.amd.min",
        "templates": "/Sync10-ui/pages/appointment/templates/templates.amd",
        "SlidePanel": "/Sync10-ui/lib/slidePanel/js/slidePanel",
        "require_css": "/Sync10-ui/lib/require-css/css",
        "datetimepicker-css": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "datetimepicker-css-plus": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker-plus",
        "bootstrap.datetimepicker": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "bootstrap-css": "/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min",
        "bootstrap": "/Sync10-ui/bower_components/bootstrap/dist/js/bootstrap.min",
        "smalot.datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "smalot.datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "compondbox-css": "/Sync10-ui/lib/compondBox/css/compondBox",
        "nprogress": "/Sync10-ui/lib/nprogress/nprogress",
        "nprogress_css": "/Sync10-ui/lib/nprogress/nprogress",
        "appointment_css_main": "/Sync10-ui/pages/appointment/ie8_demo/main",
        "index2_css": "/Sync10-ui/pages/appointment/ie8_demo/index2",
        "appointment_style_css": "/Sync10-ui/pages/appointment/ie8_demo/style",
        "immybox_css": "/Sync10-ui/lib/immybox-master/immybox"
    }
});
//css

var isie = /msie/.test(navigator.userAgent.toLowerCase());
var jquerysring = ["jquery", "nprogress", "require_css!nprogress_css", "require_css!bootstrap-css", "require_css!appointment_css_main", "require_css!index2_css", "require_css!immybox_css"];
if (isie) {
    jquerysring = [];
    jquerysring = ["jquery1.9", "nprogress"];
}


require(jquerysring, function(jq_, NProgress) {

    $("body").show(); 
   
    var requcss = ["require_css!appointment_style_css", "require_css!Font-Awesome", "require_css!datetimepicker-css", "require_css!datetimepicker-css-plus", "require_css!smalot.datetimepicker-css", "require_css!compondbox-css"];
    if (isie) {

        requcss = [];
    }

    require(requcss, function() {

        NProgress.start();

        //js

        require(["moment", "Backbone", "config", "immybox", "CompondBox", "handlebars", "templates", "SlidePanel", "bootstrap.datetimepicker"], function(moment, backbone, Config, immbox, CompondBox, handlebars, templates, SlidePanel, Acars_datetimepicker) {
            $(function() {
                //console.log(backbone);
                var viewmainbody = Backbone.View.extend({
                    defaultStore: "1000005",
                    myStores: [],
                    boundType: "",
                    chooseDate: "", //定义一个全局变量用来保存选择的日期
                    inoutbound: "", //选择的入库还是出货
                    createNum: "", //定义一个全局的变量用来保存创建预约时需要的号码
                    initialize: function(options) {
                        //var a = moment(new Date()).format("MM/DD/YYYY HH:00");   //获取整点日期

                        var v = this; //全局当前对象
                        $("#buttonDiv a").click(function(){
                            v.inoutbound = $(this).attr("id");
                        })
                        /*
                        //点击整点时间事件
                        $(".timeText").click(function() {
                            var chooseDate = v.chooseDate; //选择的日期
                            var storeId = $("#searchCustomer").attr("data-value");
                            var hour = $(this).attr("data-id"); //当前点击的整小时
                            //返回200时应走程序
                            //var time=v.chooseDate+" "+$(this).text();
                            var appointmentTime = moment(v.chooseDate).format("MM/DD/YYYY");
                            appointmentTime = appointmentTime + " " + $(this).text(); //月日年 整点格式
                            appointmentTime = appointmentTime ? "?name=" + appointmentTime : "";
                            
                            if (appointmentTime) {
                                appointmentTime += "&bound_Type="+ v.inoutbound+"&inoutbound="+v.inoutbound+"&hubVal="+$("#searchCustomer").val();
                            } 
                            var slide = new SlidePanel({
                                url: Config.createAppiontement1.url + appointmentTime,
                                title: 'Create Appointment',
                                duration: 500,
                                topLine: 0,
                                zindex: 100,
                                slideLine: "85%"
                            });
                            slide.open();
                        });
                        */
                        $(".timeText").click(function() {
                            if($(this).attr("data-value") == "1"){   //等于1表面该时间点可预约
                                var chooseDate = v.chooseDate; //选择的日期
                                var storeId = $("#searchCustomer").attr("data-value");
                                var hour = $(this).attr("data-id"); //当前点击的整小时
                                //返回200时应走程序
                                //var time=v.chooseDate+" "+$(this).text();
                                var appointmentTime = moment(v.chooseDate).format("MM/DD/YYYY");
                                appointmentTime = appointmentTime + " " + $(this).text(); //月日年 整点格式
                                appointmentTime = appointmentTime ? "?name=" + appointmentTime : "";
                                
                                if (appointmentTime) {
                                    appointmentTime += "&bound_Type="+ v.inoutbound+"&inoutbound="+v.inoutbound+"&hubVal="+$("#searchCustomer").val();
                                } 
                                var slide = new SlidePanel({
                                    url: Config.createAppiontement1.url + appointmentTime,
                                    title: 'Create Appointment',
                                    duration: 500,
                                    topLine: 0,
                                    zindex: 100,
                                    slideLine: "85%"
                                });
                                slide.open();
                            }else{

                            }
                        })
                        var v = this;
                        //调用加载仓库方法
                        $.getJSON(Config.shipTo.url + "&type=1&lv=0&rootType=Ship+From&Reqnumr=" + Date.parse(new Date()), function(json) {
                            //console.log(json);   //ie8不支持这种打印
                            //alert(json);
                            var rls = [];
                            for (var i = 0; i < json.length; i++) {
                                rls.push({
                                    "value": json[i].data,
                                    "text": json[i].name
                                });
                            }
                            //点击仓库输入框调用
                            $('#searchCustomer').immybox({
                                //Pleaseselect:'<i>Clean Select</i>',
                                choices: rls,
                                //Defaultselect: v.defaultStore,
                                change: function(e, d) {
                                    if (d.value) {
                                        //设置选择的参看仓库 ID
                                        v.defaultStore = d.value;
                                        $("#buttonDiv").removeClass("hide"); //按钮组显示
                                        //inbound,outbound点击事件
                                        $(".aStyle").click(function() {
                                            $(this).removeClass('active');
                                            $(this).siblings().removeClass('active');
                                            $(this).addClass('active');

                                            $(this).css({
                                                "display": "inline-block",
                                                "text-decoration": "none",
                                                "width": "80px",
                                                "line-height": "35px",
                                                "height": "35px",
                                                "margin": "0px 10px",
                                                "border": "1px solid #ccc",
                                                "color": "white"

                                            })
                                            $(this).siblings().css({
                                                    "color": "#337ab7"
                                                })
                                                /*$(this).css("color","white");
													$(this).siblings({"color":"black","background":"#e6e6e6"});*/
                                                //return false;
                                            $("#calendarDiv").removeClass("hide");
                                            $(".calendar-time tbody tr:odd").css("background", "#f5f6f5");
                                            $(".time").css("padding", "13px 0px 4px 40px");
                                            showCalendar();
                                        });
                                    }
                                }
                            });
                        });
                        //日历显示
                        function showCalendar() {
                            var mydates = $('#calendar').Acars_datetimepicker({ //日期块
                                inline: true,
                                icons: {
                                    previous: 'fa fa-chevron-circle-left',
                                    next: 'fa fa-chevron-circle-right'
                                },
                                toolbar: false,
                                format: 'YYYY-MM-DD',
                                tdcellclick: function(dates) {
                                    $("#calendarTime").removeClass("hide");
                                    //chooseDate = dates;
                                    v.chooseDate = dates;
                                    //console.log(dates);   //打印点击的日期
                                    //点击日期调用服务返回哪些时间点可以预约
                                    $.getJSON(Config.enableAppointment.url+"?storage_id="+$("#searchCustomer").attr("data-value")+"&date="+v.chooseDate,function(data){
                                        $(".timeText").css("color","grey");
                                        if(data.DATA.length){
                                            for(var i=0;i<data.DATA.length;i++){
                                                if(v.inoutbound == "in"){
                                                    $("#time"+data.DATA[i].ID).children("a").attr("data-value",data.DATA[i].CNTIN);
                                                    if(data.DATA[i].CNTIN == 1){   //表示选择的inbound中有时间点可以预约
                                                        $("#time"+data.DATA[i].ID).children("a").css("color","#1479fb");
                                                    }else if(data.DATA[i].CNTIN == 0){
                                                        $("#time"+data.DATA[i].ID).children("a").css("color","grey");
                                                    }
                                                }else if(v.inoutbound == "out"){   //表示选择的outbound中有时间点可以预约
                                                    $("#time"+data.DATA[i].ID).children("a").attr("data-value",data.DATA[i].CNTOUT);
                                                    if(data.DATA[i].CNTOUT == 1){
                                                        $("#time"+data.DATA[i].ID).children("a").css("color","#1479fb");
                                    
                                                    }else if(data.DATA[i].CNTOUT == 0){
                                                        $("#time"+data.DATA[i].ID).children("a").css("color","grey");
                                                    }
                                                }
                                            }
                                        }else{
										//alert("没有数据可显示");
										}
                                    });
                                },
                                Initialization: function(setbuttondom) { //日期初始化入口
                                }
                            });
                            //$(".Acars_picke").css("width","auto");
                            /*日期样式控制*/
                            $(".Acars_picke .table-condensed thead tr:nth-child(1) th").css({
                                "background": "#628DB9",
                                "height": "32px",
                                "line-height": "32px",
                                "border-radius": "0px",
                                "color": "#fff",
                                "font-size": "22px"
                            })
                            $(".Acars_picke .bootstrap-datetimepicker-widget table thead tr:nth-child(1) th").hover(function() {
                                $(this).css("background", "#628DB9");
                            })
                            $(".Acars_picke .table-condensed thead tr:nth-child(2) th").css({
                                "border-bottom": "1px solid #E9E9E9",
                                "color": "#147AFB",
                                "border-radius": "0px"
                            })
                            $(".Acars_picke td.active").css({
                                "border": "2px solid red",
                            });
                        }
                    },
                    render: function() {
                        //console.log(1);
                    },
                    //主管权限判断方法
                    showConfigButton: function(ls, defaultStore, setbuttondom) {
                        if (ls.length > 0) {
                            for (var idx = 0; idx < ls.length; idx++) {
                                var sitem = ls[idx];
                                if (sitem && sitem.POSTID == 10 && sitem.DEPTID == 1000008) {
                                    //$("#calendar .fc-config-button").show();
                                    setbuttondom.show();
                                    //console.log("show");
                                }
                            }
                        }
                    }
                });

                var view01 = new viewmainbody(); //创建backbone对象
                view01.render(); //对象的渲染

                NProgress.done();
            });
        });
    });

});
