(function () {
    var configObj = {
        appiontmentList: {  //列表
            url: "/Sync10/_load/getAppointmentList"//url: "/Sync10/ui/pages/WMSAppointment/json/appiontmentList.json"
        },
        createAppiontement: {   //创建
            url: "/Sync10-ui/pages/appointment/add.html"
        },
        /*hyq has add code aboute createAppiontement1(2015-4-20) */
        createAppiontement1: {   //创建
            url: "/Sync10-ui/pages/appointment/ie8_demo/add1.html"
        },
        /*哪个时间点可以预约服务*/
        enableAppointment:{
            url:"/Sync10/_load/sumWorkEnable"
        },
        testenableAppointment:{
            url:"/Sync10-ui/pages/appointment/json/enableTime.json"
        },
		appiontementLimit: {
			url: "/Sync10-ui/pages/appointmentLimit/index.html"
		},
        addAppointment: {
            url: "/Sync10/_load/addAppointment"
        },
        /*hyq has a makeAppointmentUrl(2015-4-17)*/
        makeAppointment:{
            url:"/Sync10/_load/makeAppointment"
        },

        delAppiontment: {   //取消约车
            url: "/Sync10/_load/delAppointment"
        },
        getAppointmentInfo: {//获取约车信息.
            url: "/Sync10/_load/editAppointment"
        },
        getLoginStorage: {
            url: "/Sync10/_load/getLoginStorageCatalog"
        },
        getSingleInvoiceInfo: { //根据单据ID获取单据相关信息，返回json数据和loadlist、orderlist等信息格式一样.
            url: "/Sync10/_load/getInviceByTypeId",
        },
        getOrderLRList:{
            url:"/Sync10/_load/getAppLRList"
        },
        getLoadList: {  //load列表
            url: "/Sync10/_load/getAppLoadList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        getOrderList: { //order列表
            url: "/Sync10/_load/getAppOrderList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        getPoList: {    //po列表
            url: "/Sync10/_load/getAppPoList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        shipTo: {   //仓库地址json
//            url: "/Sync10-ui/pages/appointment/json/shipTo.json"
            url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        },
        carrierJson: {  //卡车信息json
            url: "/Sync10-ui/pages/appointment/json/carrier.json"
        },
        orderType: {    //单据类型.
            url: "/Sync10-ui/pages/appointment/json/orderType.json"
        },


        orderpoType: {    //单据类型.
            url: "/Sync10-ui/pages/appointment/json/orderpoType.json"
        },

        appiontmentCalendarTimeUrl: {    //calendar-time 入库出库数据
          // url: "/Sync10-ui/pages/appointment/json/appiontmentCalendar.json"
           url: "/Sync10/_load/sumWorkByDate"
        },
        calendarSumAppointmentUrl:{
           url: "/Sync10/_load/sumWorkByStartEnd"
        },
		adminSession: {
			url: "/Sync10/_fileserv/redis_session"
		}
    };
    define(configObj);
}).call(this);