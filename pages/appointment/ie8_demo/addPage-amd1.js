"use strict";

requirejs.config({
    paths: {
        "oso.bower": "/Sync10-ui/bower_components",
        "oso.lib": "/Sync10-ui/lib",
        "jquery": "/Sync10-ui/bower_components/jquery/dist/jquery.min",
        "jquery1.9": "/Sync10-ui/lib/jquery1.9/jquery-1.9.0",
        "underscore": "/Sync10-ui/bower_components/underscore/underscore.min",
        "require_css": "/Sync10-ui/lib/require-css/css",
        "backbone": "/Sync10-ui/bower_components/backbone/backbone.min",
        "handlebars.runtime": "/Sync10-ui/bower_components/handlebars/handlebars.runtime.amd.min",
        "Paging": "/Sync10-ui/lib/Paging/Paging",
        "bootstrap": "/Sync10-ui/bower_components/bootstrap/dist/js/bootstrap.min",
        "bootstrap-css": "/Sync10-ui/bower_components/bootstrap/dist/css",
        "handlebars": "/Sync10-ui/bower_components/handlebars/handlebars.amd.min",
        "bootstrap.datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "bootstrap.datetimepicker-css":"/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "compondBox": "/Sync10-ui/lib/compondBox/js/compondBox",
        "createBoxPanel": "/Sync10-ui/lib/slidePanel/js/createPanel",
        "art_Dialog": "/Sync10-ui/lib/artDialog/src",
        "immybox": "/Sync10-ui/lib/immybox-master/jquery.immybox",
        "dateUtil": "/Sync10-ui/lib/DateUtil",
        "nprogress": "/Sync10-ui/lib/nprogress/nprogress",
        "buttons_css":"/Sync10-ui/css/buttons/buttons",
        "compondBox_css":"/Sync10-ui/lib/compondBox/css/compondBox"
    }
});

var isie = /msie/.test(navigator.userAgent.toLowerCase());

var addpageloadval = ["nprogress", "jquery","require_css!./main","require_css!compondBox_css","require_css!buttons_css","require_css!bootstrap.datetimepicker-css","require_css!oso.lib/immybox-master/immybox"];

if (isie) {
    addpageloadval = [];
    addpageloadval = ["nprogress", "jquery1.9"];
}

require(addpageloadval, function(NProgress) {

    NProgress.start();

    var defval = [
        "/Sync10-ui/lib/blockui/jquery.blockUI.233.js",
        "js/v4Create1",
        "require_css!oso.bower/jqueryui/themes/base/all"
    ]
    if (isie) {
        defval = [];
        defval = [
            "/Sync10-ui/lib/blockui/jquery.blockUI.233.js",
            "js/v4Create1"
        ]
    }


    require(defval, function(blockui, Create) {



        $(function() {

            window.setTimeout(function() {


                $.blockUI._defaults = {
                    css: {
                        padding: '8px',
                        margin: 0,
                        width: '170px',
                        top: '45%',
                        left: '40%',
                        textAlign: 'center',
                        color: '#000',
                        border: '3px solid #999999',
                        backgroundColor: '#ffffff',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                        '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
                    },
                    overlayCSS: {
                        backgroundColor: '#000',
                        opacity: '0.6'
                    },
                    baseZ: 99999,
                    centerX: true,
                    centerY: true,
                    fadeOut: 1000,
                    showOverlay: true
                };



                $.ajaxSetup({
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                });


                var createView = new Create();
                createView.render();

                //var v3AddRelWarehouseView = new v3AddRelWarehouse();
                //v3AddRelWarehouseView.render();

                //var v3AddRelOrderView = new v3AddRelOrder();
                //v3AddRelOrderView.render();

                //var v3AddCNTRView = new v3AddCNTR();
                //v3AddCNTRView.render();

                //var v3SaveInfoView = new v3SaveInfo();
                //v3SaveInfoView.render();

                NProgress.done();

            }, 100);

        });




    });



});
