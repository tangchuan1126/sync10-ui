"use strict";

var msieindexof = /msie/.test(navigator.userAgent.toLowerCase());

var jquerysring = "jquery";

if (msieindexof) {
    jquerysring = "jquery1.9";
}

var defineval = [
    jquerysring,
    "backbone",
    "handlebars",
    "../../templates/templates.amd",
    "bootstrap.datetimepicker",
    "compondBox",
    "createBoxPanel",
    "art_Dialog/dialog-plus",
    "oso.lib/table/js/table",
    "config",
    "immybox",
    "js/appiontment",
    "dateUtil"
];

define(defineval, function(jq_, Backbone, handlebars, templates, datetimepicker, CompondBox, createBoxPanel, Dialog, Table, Config, immybox, Model, DateUtil) {


    //, createBoxPanel, Dialog, Table, Config,immybox, Model,DateUtil
  



        return Backbone.View.extend({
            inout:"",
            initialize: function() {
                var v = this;
               //alert(v.getQueryString("hubVal"));
            //alert(v.getQueryString("inoutbound"));
                /*if($.trim(v.getQueryString("inoutbound")) == "Inbound"){
                    v.inout = "in";
                }else{
                    v.inout = "out";
                }*/
                v.inout = v.getQueryString("inoutbound");
                $(function() {$("#inputAddAppointmentTime").attr("disabled","disabled");
                    // var v = this;
                    $.getJSON(Config.carrierJson.url, function(result) {
                        v.carrierJson = result;
                    });


                    if (v.GetRequest("appid") == undefined || v.GetRequest("appid") == "undefined") {
                        v.id = "";
                    } else {
                        v.id = v.GetRequest("appid");
                    }



                    //if (v.GetRequest("type") == undefined || v.GetRequest("type") == "undefined") {
                    //    v.type = "";
                    //} else {
                    //    v.type = v.GetRequest("type");
                    //}
                    $.getJSON("js/appiontmentType.json", function(json) {
                        v.AType = json;
                    });
                    v.addRelOderInit();
                    v.warehouseInit();
                    v.addCNTRInit();
                    //v.addRelOderInit();
                    v.saveInfoInit();

                    if (v.id) {
                        $(".createTitle").html("Edit Appoiontment");
                        $.ajax({
                            type: 'post',
                            url: Config.getAppointmentInfo.url,
                            data: 'aid=' + v.id,
                            dataType: 'json',
                            success: function(data) {
                                try {
                                    $("#inputAddWareHourse").attr("data-value", data.storage_id).attr("data-name", data.storage_name).val(data.storage_name);
                                    $("#inputAddWareHourseLinkman").val(data.storage_linkman);
                                    $("#inputAddWareHourseTel").val(data.storage_linkman_tel);
                                    //$("#inputAddCarrier").attr("data-value", data.carrier_id).attr("data-name", data.carrier_name).val(data.carrier_name);

                                    $.getJSON(Config.carrierJson.url, function(result) {
                                        v.carrierJson = result;


                                        $.each(result, function(index, obj) {
                                            obj.text = obj.value + " - " + obj.text; //简称和全称连接
                                        })
                                        $('#inputAddCarrier').immybox({
                                            Pleaseselect: '<i>Clean Select</i>',
                                            Defaultselect: data.carrier_id,
                                            textname: "text",
                                            valuename: "value",
                                            choices: result
                                        });
                                    });
                                    if (data.appointment_time) {
                                        $("#inputAddAppointmentTime").val(DateUtil.getFormartDateStringForPs(data.appointment_time, "MM/dd/yyyy hh:mm", data.jet_lag));
                                    }
                                    $("#inputAddCarrierLinkman").val(data.carrier_linkman);
                                    $("#inputAddCarrierTel").val(data.carrier_linkman_tel);
                                    if (data.etd) {
                                        $("#inputAddETD").val(DateUtil.getFormartDateStringForPs(data.etd, "MM/dd/yyyy hh:mm", data.jet_lag));
                                    }
                                    if (data.eta) {
                                        $("#inputAddETA").val(DateUtil.getFormartDateStringForPs(data.eta, "MM/dd/yyyy hh:mm", data.jet_lag));
                                    }
                                    $("#inputAddLicensePlate").val(data.licenseplate);
                                    $("#inputAddDriverLicense").val(data.driver_license);
                                    $("#inputAddDriverName").val(data.driver_name);
                                    $("#inputAddType").attr("data-value", data.inputAddType);
                                    $("#inputAddType").attr("value", data.inputAddType)
                                    v.status = data.status;

                                    v.select_data_rows = data.invoices;
                                    //v.Deduplication(data.invoices);

                                    if (data.invoices && data.invoices != undefined) {
                                        for (var i = 0; i < data.invoices.length; i++) {
                                            var con = $(templates.singleBoxBol()).find(".bol-title h3").html(v.replaceFirst(data.invoices[i].type) + "<a href='#'>" + data.invoices[i].num + "</a>").end()
                                                .find(".to").html("&nbsp;" + v.formartStr(data.invoices[i].customer)).end()
                                                .find(".plateNum").html("&nbsp;" + v.formartStr(data.invoices[i].platenum)).end()
                                                .find(".volume").html("&nbsp;" + v.formartStr(data.invoices[i].volume)).end()
                                                .find(".weight").html("&nbsp;" + v.formartStr(Math.ceil(((data.invoices[i].weight || 0) * 100)) / 100)).end();

                                            var createPanelView = new createBoxPanel({
                                                content: con,
                                                width: '',
                                                container: '#ContainerAddBol',
                                                id: data.invoices[i].id,
                                                type: data.invoices[i].type,
                                                close: function() {
                                                    //hyq has changed code as follows(2015-4-8)
                                                    //return v.delOrder(this.id, this.type);
                                                    return v.delOrder(this);
                                                }
                                            });
                                        }
                                    }
                                } catch (err) {}
                            },
                            error: function() {}
                        });
                    } else {
                        $(".createTitle").html("Create Appointment");
                        $.getJSON(Config.carrierJson.url, function(result) {
                            $.each(result, function(index, obj) {
                                obj.text = obj.value + " - " + obj.text; //简称和全称连接
                            })
                            v.carrierJson = result;
                            $.ajax({
                                type: 'post',
                                url: Config.getLoginStorage.url,
                                dataType: 'json',
                                success: function(data) {
                                    try {
                                        $("#inputAddWareHourse").attr("data-value", data.id).attr("data-name", data.title).val(data.title);
                                        $("#inputAddWareHourseLinkman").val(data.send_contact);
                                        $("#inputAddWareHourseTel").val(data.send_phone);

                                    } catch (err) {}
                                },
                                error: function() {}
                            });
                            var orderid = v.GetRequest("orderid");
                            var ordertype = v.GetRequest("ordertype");
                            var orderno = v.GetRequest("orderno");
                            if (orderid && ordertype) { //load\order中约车.
                                $.ajax({
                                    type: 'post',
                                    url: Config.getSingleInvoiceInfo.url,
                                    data: {
                                        id: orderid,
                                        type: ordertype
                                    },
                                    dataType: 'json',
                                    success: function(data) {
                                        //hyq has add code(2015-4-3)
                                        $.each(data, function(index, obj) {
                                            obj.text = obj.value + " - " + obj.text; //简称和全称连接
                                        })
                                        try {
                                            if (data && data != undefined) {
                                                v.select_data_rows = data;
                                                //console.log(data[0].carrier_id);
                                                var carrierJsonObject = {};
                                                $('#inputAddCarrier').immybox({
                                                    Pleaseselect: '<i>Clean Select</i>',
                                                    Defaultselect: "" + data[0].carrier_id,
                                                    //textname:"value",
                                                    textname: "text",
                                                    valuename: "value",
                                                    choices: v.carrierJson
                                                });
                                                for (var i = 0; i < v.carrierJson.length; i++) {
                                                    var value = v.carrierJson[i].value;
                                                    carrierJsonObject[value] = v.carrierJson[i].text;
                                                }
                                                //console.log(carrierJsonObject);
                                                for (var i = 0; i < data.length; i++) {
                                                    var con = null;
                                                    if (data[i].type == "load") {
                                                        if (carrierJsonObject && data[i].carrier_id) {
                                                            con = $(templates.singleBoxBol()).find(".bol-title h3").html(v.replaceFirst(data[i].type) + "<a href='#'>" + data[i].num + "</a>").end()
                                                                .find(".to").html("&nbsp;" + carrierJsonObject[data[i].carrier_id] ? carrierJsonObject[data[i].carrier_id] : v.formartStr(data[i].carrier_id)).end()
                                                                .find(".plateNum").html("&nbsp;" + v.formartStr(data[i].platenum)).end()
                                                                .find(".volume").html("&nbsp;" + v.formartStr(data[i].volume)).end()
                                                                .find(".weight").html("&nbsp;" + v.formartStr(Math.ceil(((data[i].weight || 0) * 100)) / 100)).end();
                                                        } else {
                                                            con = $(templates.singleBoxBol()).find(".bol-title h3").html(v.replaceFirst(data[i].type) + "<a href='#'>" + data[i].num + "</a>").end()
                                                                .find(".to").html("&nbsp;" + v.formartStr(data[i].carrier_id)).end()
                                                                .find(".plateNum").html("&nbsp;" + v.formartStr(data[i].platenum)).end()
                                                                .find(".volume").html("&nbsp;" + v.formartStr(data[i].volume)).end()
                                                                .find(".weight").html("&nbsp;" + v.formartStr(Math.ceil(((data[i].weight || 0) * 100)) / 100)).end();
                                                        }
                                                    }
                                                    var createPanelView = new createBoxPanel({
                                                        content: con,
                                                        width: '',
                                                        container: '#ContainerAddBol',
                                                        id: data[i].id,
                                                        type: data[i].type,
                                                        close: function() {
                                                            //hyq has changed code as follows(2015-4-8)
                                                            //return v.delOrder(this.id, this.type);
                                                            return v.delOrder(this);
                                                        }
                                                    });
                                                }
                                            } else {
                                                $('#inputAddCarrier').immybox({
                                                    Pleaseselect: '<i>Clean Select</i>',
                                                    choices: v.carrierJson,
                                                    //hyq has add code as follows(2015-4-1)
                                                    //textname:"value", 
                                                    textname: "text",
                                                    valuename: "value"
                                                });
                                            }
                                        } catch (err) {}
                                    },
                                    error: function() {}
                                });
                            } else {
                                $('#inputAddCarrier').immybox({
                                    //Defaultselect:"data.carrier_id",
                                    Pleaseselect: '<i>Clean Select</i>',
                                    //textname:"value",
                                    textname: "text",
                                    valuename: "value",
                                    choices: v.carrierJson
                                });
                            }
                        });
                    }



                });

            },
            render: function() {
                var v = this;
            },
            warehouseInit: function() {
                var v = this;
                var temp = new CompondBox({
                    //content: templates.v3AddWarehouse(),
                    content: templates.v3AddWarehouse1(),
                    title: "Hub & Contacts",
                    container: "#containerAddWarehouse",
                    isExpand: true
                });
                //alert(v.getQueryString("hubVal"));
                $("#inputAddWareHourse").val(v.getQueryString("hubVal")); 
                $("#inputAddWareHourse").attr("disabled","disabled");



                $.getJSON(Config.shipTo.url + "&type=1&lv=0&rootType=Ship+From&Reqnumr=" + Date.parse(new Date()), function(json) {
                    var rls = [];
                    for (var i = 0; i < json.length; i++) {
                        rls.push({
                            "value": json[i].data,
                            "text": json[i].name
                        });
                    }
                    /*$('#inputAddWareHourse').immybox({
                        Pleaseselect: '<i>Clean Select</i>',
                        choices: rls,
                        change: function(e, d) {
                            //设置选择的参看仓库 ID
                            v.defaultStore = d.value;
                            //alert($("#inputAddWareHourse").val());
                            //  v.showConfigButton(v.myStores,v.defaultStore);

                            for (var i = 0; i < json.length; i++) {
                                if ($.trim(json[i].name) == $.trim($("#inputAddWareHourse").val())) { //选择的仓库和所有仓库比较
                                    //$("#inputAddWareHourseLinkman").val(json[i].contact);  //仓库选择带出联系人和电话
                                    //$("#inputAddWareHourseTel").val(json[i].phone);
                                    $("#inputAddWareHourseLinkman").val(json[i].send_contact); //发货仓库选择带出发货联系人和电话
                                    $("#inputAddWareHourseTel").val(json[i].send_phone);
                                }

                            }
                        }

                    });*/
                });



            }, //仓库信息初始化.
            addRelOderInit: function() {
                var v = this;
                var quickSearch = "Relevance Order" + templates.v3QuickSearchOrder();
                // console.log(templates.v3AddRelOrder());
                var temp = new CompondBox({
                    content: templates.v3AddRelOrder(),
                    title: quickSearch,
                    container: "#containerAddRelOder",
                    isExpand: true
                });
                $(".quick-search").on("click", function(e) {
                    e.stopPropagation();
                    return false;
                })
                $("#btnAddOrder").on("click", function() {
                    v.addOrder();
                });
                $("#inputQuickLoadNo").on('keypress', function(event) {
                    if (event.keyCode == "13") {
                        v.addOrder($("#inputQuickLoadNo").val());
                    }
                });


            },
            addCNTRInit: function() {
                var v = this;
                var temp = new CompondBox({
                    //content: templates.v3AddCNTR(),
                    content: templates.v3AddCNTR1(),
                    title: "Appointment",
                    container: "#containerAddCar",
                    isExpand: true
                });
                //hyq has add code as follows(2015-4-1)
                //$("#inputAddAppointmentTime").after('<span class=""><span class="glyphicon glyphicon-calendar" style="float:right;display:inline-block;position:relative;top:-22px"></span></span>');
                // $("#inputAddAppointmentTime").css("cursor","pointer");

                //hyq has changed code as follows(2015-4-1)

                
                //$("#inputAddType").val(v.getQueryString("inoutbound"));
                if(v.getQueryString("inoutbound") == "in"){
                    $("#inputAddType").val("Inbound");
                }else{
                    $("#inputAddType").val("Outbound");
                }
                $("#inputAddType").attr("disabled","disabled");
                $("#inputAddAppointmentTime").attr("disabled","disabled");
                /*if (v.getQueryString("name")) {
                    $("#inputAddAppointmentTime").datetimepicker({
                        format: 'mm/dd/yyyy hh:00',
                        autoclose: 1,
                        minView: 1,
                        forceParse: 0,
                        startDate: new Date(v.getQueryString("name")),
                        bgiconurl: ""
                    });
                } else {
                    $("#inputAddAppointmentTime").datetimepicker({
                        format: 'mm/dd/yyyy hh:00',
                        autoclose: 1,
                        minView: 1,
                        forceParse: 0,
                        startDate: new Date(),
                        bgiconurl: ""
                    });
                }*/

                //.datetimepicker({ startDate: new Date(), autoclose: 1, minView: 2,forceParse: 0, format: 'mm/dd/yyyy hh:00' });
                //hyq has add a row code as follows(cleanval)(2015-4-2)
                //获取到点击的预约时间之前先清空值
                $("#inputAddAppointmentTime").val("");
                $("#inputAddAppointmentTime").val(v.getQueryString("name")); //获取v4Creater.js中传过来参数值
                //hyq has changed code as follows(2015-3-27)
                //$("#inputAddETD").datetimepicker({ startDate: new Date(), autoclose: 1, minView: 2,forceParse: 0, format: 'mm/dd/yyyy hh:ii' });
                $("#inputAddETD").css("cursor", "pointer");
                $("#inputAddETD").datetimepicker({
                    format: 'mm/dd/yyyy hh:ii',
                    autoclose: 1,
                    minView: 0,
                    forceParse: 0,
                    bgiconurl: ""
                });
                //hyq has changed code as follows(2015-3-27)
                //$("#inputAddETA").datetimepicker({ startDate: new Date(),  autoclose: 1, minView: 2,forceParse: 0, format: 'mm/dd/yyyy hh:ii' });
                $("#inputAddETA").css("cursor", "pointer");
                $("#inputAddETA").datetimepicker({
                    format: 'mm/dd/yyyy hh:ii',
                    autoclose: 1,
                    minView: 0,
                    forceParse: 0,
                    bgiconurl: ""
                });

                /*$.getJSON("js/appiontmentType.json", function(json) {
                    var ls = $('#inputAddType').immybox({
                        Pleaseselect: '<i>Clean Select</i>',
                        choices: json,
                        textname: "text",
                        valuename: "value",
                        //动态设置默认值
                        Defaultselect: v.getQueryString("bound_Type") || "outbound"
                            //Defaultselect: v.getQueryString("bound_Type") || "Outbound"

                    });
                });*/




                //fromTree.on("events.Itemclick", function (treeId, treeNode, treeNodeAll) {
                //    $("#inputAddCarrierLinkman").val(treeNodeAll.linkman);
                //    $("#inputAddCarrierTel").val(treeNodeAll.linkman);
                //});
            },
            saveInfoInit: function() {
                var v = this;
                $("#saveAppiontment").on("click", function() {
                    if (v.verification()) {
                        var h = $("<div class='bol-body form-horizontal' role='form'>" + "<div class='form-group'>" + "<label class='col-sm-5 control-label'>Appiontment Hub</label>" + "<div class='col-sm-7'>" + "    <p class='form-control-static'>" + $("#inputAddWareHourse").val() + "</p>" + "</div></div>" + "<div class='form-group'>" + "<label class='col-sm-5 control-label'>SCAC</label>" + "<div class='col-sm-7'>" + "    <p class='form-control-static'>" + $("#inputAddCarrier").val() + "</p>" + "</div></div>" + "<div class='form-group'>"
                            //+ "<label class='col-sm-5 control-label'>Appointment Time</label>"
                            + "<label class='col-sm-5 control-label'>Appointment</label>" + "<div class='col-sm-7'>" + "    <p class='form-control-static'>" + $("#inputAddAppointmentTime").val() + "</p>" + "</div></div>" + "<div class='form-group'>" + "<label class='col-sm-5 control-label'>Appointment Type</label>" + "<div class='col-sm-7'>" + "    <p class='form-control-static'>" + $("#inputAddType").val() + "</p>" + "</div></div>" + "</div>");

                        var d = Dialog({
                            title: "Confirm save",
                            width: 400,
                            height: 180,
                            lock: true,
                            opacity: 0.6,
                            content: h,
                            okValue: "Save",
                            ok: function() {
                                v.save();
                            },
                            cancelValue: 'Cancel',
                            cancel: function() {}
                        });
                        d.reset();
                        d.showModal();
                    }
                });
            },


            getQueryString: function(name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                var r = window.location.search.substr(1).match(reg);
                if (r != null) return unescape(r[2]);
                return null;
            },
            addOrder: function(orderNo) {
                var v = this;
                //var h = $(templates.v3SelectOrder());
                var h = $(templates.v3SelectOrder1());

                var d = Dialog({
                    title: "Relevance Order",
                    width: 730,
                    height: 410,
                    lock: true,
                    opacity: 0.6,
                    Dragset: false,
                    content: h,
                    cancelValue: 'Confirm',
                    cancel: function() {}
                });
                if($.trim(v.getQueryString("inoutbound")) == "Inbound"){
                    $("#searchNo").attr("placeholder","Load/Order/RN");
                }else{
                    $("#searchNo").attr("placeholder","Load/Order/PO");
                }
                d.reset();
                d.showModal();

                v.loadList(orderNo);
            },
            loadList: function(orderNo) {
                var v = this;

                var tableSelectLoad;
                var tableSelectOrder;
                var tableSelectPO;


                $("#tabSelectLoadList").append(templates.v3SelectLoadToolbars());
                $("#tabSelectOrderList").append(templates.v3SelectOrderToolbars());
                $("#tabPOList").append(templates.v3SelectPOToolbars());

                $("#searchLoadNo").val(orderNo);
                $("#searchOrderNo").val(orderNo);
                $("#searchPoNo").val(orderNo);

                $("#searchBeginLoadMABD").css("cursor", "pointer");
                $("#searchBeginLoadMABD").datetimepicker({
                        autoclose: 1,
                        minView: 2,
                        forceParse: 0,
                        format: 'mm/dd/yyyy',
                        bgiconurl: ""
                    })
                    .on('changeDate', function(ev) {
                        var startTime = $('#searchEndLoadMABD');
                        startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
                    });
                $("#searchEndLoadMABD").css("cursor", "pointer");
                $("#searchEndLoadMABD").datetimepicker({
                        autoclose: 1,
                        minView: 2,
                        forceParse: 0,
                        format: 'mm/dd/yyyy',
                        bgiconurl: ""
                    })
                    .on('changeDate', function(ev) {
                        var startTime = $('#searchBeginLoadMABD');
                        startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
                    });

                $("#searchBeginOrderMABD").css("cursor", "pointer");
                $("#searchBeginOrderMABD").datetimepicker({
                        autoclose: 1,
                        minView: 2,
                        forceParse: 0,
                        format: 'mm/dd/yyyy',
                        bgiconurl: ""
                    })
                    .on('changeDate', function(ev) {
                        var startTime = $('#searchEndOrderMABD');
                        startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
                    });
                $("#searchEndOrderMABD").css("cursor", "pointer");
                $("#searchEndOrderMABD").datetimepicker({
                        autoclose: 1,
                        minView: 2,
                        forceParse: 0,
                        format: 'mm/dd/yyyy',
                        bgiconurl: ""
                    })
                    .on('changeDate', function(ev) {
                        var startTime = $('#searchBeginOrderMABD');
                        startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
                    });


                function formatterDateStringForPs(data, row) {
                        //    console.log(data);
                        return data ? DateUtil.getFormartDateStringForPs(data, "MM/dd/yyyy", row.jet_lag || 0) : "";
                    }
                    //console.log($("#inputAddWareHourse").attr("data-value"));


                tableSelectOrder = new Table({
                        container: '#tabSelectOrderList',
                        /*url: Config.getOrderLRList.url,
                        queryParams: [{
                            checkLIds: v.GetSelectID("load"),
                            checkOIds: v.GetSelectID("order"),
                            otype: $(".btn-group").find("button.active").attr("data-value"),
                            rfno: $("#searchNo").val()
                        }],
                        method: "post",*/
                        rownumbers: false,
                        pagination: true,
                        checkbox: true,
                        pageSize: 20,
                        height: 150,
                        groupKey: "con",
                        columns: [{
                                field: 'num',
                                title: 'NO.',
                                width: 100,
                                formatter: function(data, row) {
                                    return row.type + ":" + data;
                                }
                            }, {
                                field: 'mabd',
                                title: 'MABD',
                                width: 80,
                                formatter: formatterDateStringForPs
                            }, {
                                field: 'pallets',
                                title: 'Pallet Qty',
                                width: 70
                            },
                            //{ field: 'volume', title: 'Total Volume', width: 90 },
                            {
                                field: 'weights',
                                title: 'Total Weight(LBS)',
                                width: 100
                            }, {
                                field: 'con',
                                title: 'Customer & ShipToAddress',
                                width: 80
                            }
                        ],
                        toolbar: [{
                            text: 'Relevance',
                            iconCls: 'add',
                            handler: function() {
                                var rows = tableSelectOrder.getSelected();
                                if (!rows[0]) {
                                    return;
                                }
                                var d = new Dialog({
                                    content: "Determine the association order?",
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    title: 'Prompt',
                                    //okVal: 'Yes',
                                    okValue: 'Yes',
                                    ok: function() {
                                        v.RelTableHandler(tableSelectOrder, rows);
                                        v.reloadOrder(tableSelectOrder);
                                    },
                                    cancelValue: 'No',
                                    cancel: function() {}
                                }).show();
                            }
                        }]
                    });

                    tableSelectOrder.on("events.loadSuccess", function() {
                        $(".orderBadge").html(tableSelectOrder.getTotal());
                    });



                //hyq has add code as follows(2015-4-14)
                //点击查询
                $("#btn-group1").find("button").click(function() {
                    $("#btn-group1").find("button").removeClass("active");
                    $(this).addClass("active");
                });
                $("#typeSearch").on("click", function() {

                    //alert(v.inout);

                    //alert($("input[type='radio']:checked").val());
                    //alert($("#searchNo").val());
                    $(".table-panel").remove(); //查询按钮每点击一次表格列表就得先清除一次
                    $(".pagebox").remove(); //查询按钮没点击一次表格列表的翻页栏也得先清除一次
                    //当填写了必填项点击查询时才出现下面的列表
                    //hyq has add code (2015-4-13)
                    tableSelectOrder = new Table({
                        container: '#tabSelectOrderList',
                        url: Config.getOrderLRList.url,
                        queryParams: [{
                            checkLIds: v.GetSelectID("load"),
                            checkOIds: v.GetSelectID("order"),
                            //otype: $(".btn-group").find("button.active").attr("data-value"),
                            otype: v.inout,
                            rfno: $("#searchNo").val()
                        }],
                        method: "post",
                        rownumbers: false,
                        pagination: true,
                        checkbox: true,
                        pageSize: 20,
                        height: 150,
                        groupKey: "con",
                        columns: [{
                                field: 'num',
                                title: 'NO.',
                                width: 100,
                                formatter: function(data, row) {
                                    return row.type + ":" + data;
                                }
                            }, {
                                field: 'mabd',
                                title: 'MABD',
                                width: 80,
                                formatter: formatterDateStringForPs
                            }, {
                                field: 'pallets',
                                title: 'Pallet Qty',
                                width: 70
                            },
                            //{ field: 'volume', title: 'Total Volume', width: 90 },
                            {
                                field: 'weights',
                                title: 'Total Weight(LBS)',
                                width: 100
                            }, {
                                field: 'con',
                                title: 'Customer & ShipToAddress',
                                width: 80
                            }
                        ],
                        toolbar: [{
                            text: 'Relevance',
                            iconCls: 'add',
                            handler: function() {
                                var rows = tableSelectOrder.getSelected();
                                if (!rows[0]) {
                                    return;
                                }
                                var d = new Dialog({
                                    content: "Determine the association order?",
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    title: 'Prompt',
                                    //okVal: 'Yes',
                                    okValue: 'Yes',
                                    ok: function() {
                                        v.RelTableHandler(tableSelectOrder, rows);
                                        v.reloadOrder(tableSelectOrder);
                                    },
                                    cancelValue: 'No',
                                    cancel: function() {}
                                }).show();
                            }
                        }]
                    });

                    tableSelectOrder.on("events.loadSuccess", function() {
                        $(".orderBadge").html(tableSelectOrder.getTotal());
                    });
                    //v.reloadOrder(tableSelectOrder);
                });





                $("#btnSearchLoad").on("click", function() {
                    $(".table-panel").remove(); //查询按钮每点击一次表格列表就得先清除一次
                    $(".pagebox").remove(); //查询按钮没点击一次表格列表的翻页栏也得先清除一次
                    //当填写了必填项点击查询时才出现下面的列表
                    //hyq has add code (2015-4-13)
                    tableSelectLoad = new Table({
                        container: '#tabSelectLoadList',
                        url: Config.getLoadList.url,
                        queryParams: [{
                            no: $("#searchLoadNo").val(),
                            mabd: $("#searchLoadMABD").val(),
                            checkIds: v.GetSelectID("load"),
                            //storageId: $("#inputAddWareHourse").attr("data-value"),
                            storageId: $("#inputAddWareHourse").attr("data-value") || "",
                            type: "load"
                        }],
                        method: "post",
                        rownumbers: false,
                        pagination: true,
                        checkbox: true,
                        pageSize: 20,
                        height: 150,
                        groupKey: "customer",
                        columns: [{
                                field: 'num',
                                title: 'Load NO.',
                                width: 70
                            }, {
                                field: 'mabd',
                                title: 'MABD',
                                width: 50,
                                formatter: formatterDateStringForPs
                            }, {
                                field: 'platenum',
                                title: 'Pallet Qty',
                                width: 70
                            },
                            //{ field: 'volume', title: 'Total Volume', width: 90 },
                            {
                                field: 'weight',
                                title: 'Total Weight(LBS)',
                                width: 90
                            }, {
                                field: 'customer',
                                title: 'Customer',
                                width: 120
                            }
                        ],
                        toolbar: [{
                            text: 'Relevance',
                            iconCls: 'add',
                            handler: function() {
                                var rows = tableSelectLoad.getSelected();
                                if (!rows[0]) {
                                    return;
                                }
                                //hyq has changed code about popWindow as follows(2015-4-8)
                                if (confirm("确定关联Load,关联Load后信息将暂不显示.")) {
                                    v.RelTableHandler(tableSelectLoad, rows, "load");
                                    v.reloadLoad(tableSelectLoad);

                                }
                                var d = new Dialog({
                                    content: "Determine the association load?",
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    title: 'Prompt',
                                    //okVal: 'Yes',
                                    okValue: 'Yes',
                                    ok: function() {
                                        v.RelTableHandler(tableSelectLoad, rows, "load");
                                        v.reloadLoad(tableSelectLoad);
                                    },
                                    cancelValue: 'No',
                                    cancel: function() {}
                                }).show();
                            }
                        }]
                    })

                    tableSelectLoad.on("events.loadSuccess", function() {
                        $(".loadBadge").html(tableSelectLoad.getTotal());
                    });


                    v.reloadLoad(tableSelectLoad);
                });

                //hyq has add code as follows(2015-4-1)
                //$("#tabSelectOrderList .table-th-div").css("WORD-BREAK","break-word");
                //   $("#tabSelectLoadList .table-th-div").css("WORD-BREAK","break-word");
                //hyq has canceled code(2015-4-13)


                $("#btnSearchOrder").on("click", function() {
                    $(".table-panel").remove(); //查询按钮每点击一次表格列表就得先清除一次
                    $(".pagebox").remove(); //查询按钮每点击一次表格列表翻页栏就得先清除一次
                    //hyq has add code(2015-4-13)
                    tableSelectOrder = new Table({
                        container: '#tabSelectOrderList',
                        url: Config.getOrderList.url,
                        queryParams: [{
                            no: $("#searchOrderNo").val(),
                            from: $("#searchOrderFrom").val(),
                            to: $("#searchOrderTo").val(),
                            mabd: $("#searchOrderMABD").val(),
                            checkIds: v.GetSelectID("order"),
                            //storageId: $("#inputAddWareHourse").attr("data-value"),
                            storageId: $("#inputAddWareHourse").attr("data-value") || "",
                            type: "order"
                        }],
                        method: "post",
                        rownumbers: false,
                        pagination: true,
                        checkbox: true,
                        pageSize: 20,
                        height: 150,
                        groupKey: "con",
                        columns: [{
                                field: 'num',
                                title: 'Order NO.',
                                width: 100
                            }, {
                                field: 'mabd',
                                title: 'MABD',
                                width: 80,
                                formatter: formatterDateStringForPs
                            }, {
                                field: 'platenum',
                                title: 'Pallet Qty',
                                width: 70
                            },
                            //{ field: 'volume', title: 'Total Volume', width: 90 },
                            {
                                field: 'weight',
                                title: 'Total Weight(LBS)',
                                width: 100
                            }, {
                                field: 'con',
                                title: 'Customer & ShipToAddress',
                                width: 80
                            }
                        ],
                        toolbar: [{
                            text: 'Relevance',
                            iconCls: 'add',
                            handler: function() {
                                var rows = tableSelectOrder.getSelected();
                                if (!rows[0]) {
                                    return;
                                }

                                //hyq has changed code about popWindow as follows(2015-4-8)

                                var d = new Dialog({
                                    content: "Determine the association order?",
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    title: 'Prompt',
                                    //okVal: 'Yes',
                                    okValue: 'Yes',
                                    ok: function() {
                                        v.RelTableHandler(tableSelectOrder, rows, "order");
                                        v.reloadOrder(tableSelectOrder);
                                    },
                                    cancelValue: 'No',
                                    cancel: function() {}
                                }).show();


                            }
                        }]
                    });

                    tableSelectOrder.on("events.loadSuccess", function() {
                        $(".orderBadge").html(tableSelectOrder.getTotal());
                    });



                    v.reloadOrder(tableSelectOrder);
                });

                //hyq has changed code (2015-4-13)
                //hyq has add code as follows(2015-4-10)
                $.getJSON(Config.orderpoType.url, function(result) {
                    $('#selectType').immybox({
                        //Pleaseselect:'<i>Clean Select</i>',
                        choices: result,
                        change: function(e, d) {
                            if (d.value == "order") {
                                $("#searchOrderNo").removeAttr("readonly");
                                $("#searchOrderNo").attr("placeholder", "Order NO.");
                            } else if (d.value == "po") {
                                $("#searchOrderNo").removeAttr("readonly");
                                $("#searchOrderNo").attr("placeholder", "Po NO.");
                            }
                            //console.log(d.value);
                        }
                    });
                    $("#selectType").attr("data-value", "Order NO.").val("Order NO.");
                });

                //hyq has canceled code as follows(2015-4-14)

                // tableSelectPO = new Table({
                //     container: '#tabPOList',
                //     url: Config.getPoList.url,
                //     queryParams: [{
                //         no: $("#searchPoNo").val(),
                //         from: $("#searchPOFrom").val(),
                //         to: $("#searchPOTo").val(),
                //         checkIds: v.GetSelectID("po"),
                //         //storageId: $("#inputAddWareHourse").attr("data-value"),
                //         storageId: $("#inputAddWareHourse").attr("data-value")||"",
                //         type:"po"
                //     }],
                //     method: "post",
                //     rownumbers: false,
                //     pagination: true,
                //     checkbox: true,
                //     pageSize: 20,
                //     height: 150,
                //     groupKey: "con",
                //     columns: [
                //         { field: 'num', title: 'PO', width: 80 },
                //         { field: 'mabd', title: 'MABD', width: 80 ,formatter : formatterDateStringForPs },
                //         { field: 'platenum', title: 'Pallet Qty', width: 70 },
                //         //{ field: 'volume', title: 'Total Volume', width: 90 },
                //         { field: 'weight', title: 'Total Weight(LBS)', width: 90 }
                //     ],
                //     toolbar: [
                //         {
                //             text: 'Relevance',
                //             iconCls: 'add',
                //             handler: function () {
                //                 var rows = tableSelectPO.getSelected();
                //                 if (!rows[0]) {
                //                     return;
                //                 }
                //                 //hyq has changed code about popWindow as follows(2015-4-8)




                // $("#btnSearchPO").on("click", function () {
                //     v.reloadPo(tableSelectPO);
                // });


                // $('#tabsSelectOrder a').click(function (e) {
                //     e.preventDefault();
                //     $(this).tab('show');
                //     if (tableSelectLoad.refreshWidth)
                //         tableSelectLoad.refreshWidth();
                //     if (tableSelectOrder.refreshWidth)
                //     tableSelectOrder.refreshWidth();
                //     if (tableSelectPO.refreshWidth)
                //     tableSelectPO.refreshWidth();
                // });
                // $('#tabsSelectOrder a:first').tab('show');


            },
            GetRequest: function(name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = window.location.search.substr(1).match(reg); //匹配目标参数
                if (r != null) return unescape(r[2]);
                return null; //返回参数值
            },
            save: function() {
                var v = this;
                var model = new Model.AppointmentModel();
                model.save({
                    id: v.id,
                    storage_id: $("#inputAddWareHourse").attr("data-value"),
                    storage_name: encodeURI($("#inputAddWareHourse").attr("data-name")),
                    storage_linkman: encodeURI($("#inputAddWareHourseLinkman").val()),
                    storage_linkman_tel: $("#inputAddWareHourseTel").val(),
                    carrier_id: $("#inputAddCarrier").attr("data-value"),
                    carrier_name: encodeURI($("#inputAddCarrier").val()),
                    appointment_time: $("#inputAddAppointmentTime").val(),
                    appointment_type: $("#inputAddType").attr("data-value"),
                    carrier_linkman: encodeURI($("#inputAddCarrierLinkman").val()),
                    carrier_linkman_tel: $("#inputAddCarrierTel").val(),
                    etd: $("#inputAddETD").val(),
                    eta: $("#inputAddETA").val(),
                    licenseplate: encodeURI($("#inputAddLicensePlate").val()),
                    driver_license: encodeURI($("#inputAddDriverLicense").val()),
                    driver_name: encodeURI($("#inputAddDriverName").val()),
                    invoices: v.select_data_rows,
                    status: v.status
                }, {
                    error: function(model, response, options) {},
                    success: function(e, response) {
                        if (response.message) {
                            var d = new Dialog({
                                content: response.message,
                                icon: 'question',
                                lock: true,
                                width: 200,
                                height: 40,
                                //title: 'Save Error',  //hyq has changed titleValue(2015-4-10)
                                title: 'Warning!',
                                cancelVal: 'Close',
                                cancel: function() {}
                            }).show();
                        } else {
                            if (e.changed.result != 0) {
                                var d = new Dialog({
                                    content: e.changed.error,
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    //title: 'Save Error',  //hyq has changed titleValue(2015-4-10)
                                    title: 'Warning!',
                                    cancelVal: 'Close',
                                    cancel: function() {}
                                }).show();
                            } else {
                                window.parent.postMessage("", '*');
                            }
                        }
                    },
                    faile: function(e) {
                        var d = new Dialog({
                            content: e.message,
                            icon: 'question',
                            lock: true,
                            width: 200,
                            height: 40,
                            title: 'Save Error',
                            cancelVal: 'Close',
                            cancel: function() {}
                        }).show();
                    }
                });
            },
            verification: function() {
                var f = true;
                if ($("#inputAddWareHourse").val() == "" | $("#inputAddWareHourse").val() == undefined) {
                    if (!$("#inputAddWareHourse").parent().hasClass("has-feedback")) {
                        $("#inputAddWareHourse").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    }
                    f = false;
                } else {
                    $("#inputAddWareHourse").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                }

                if ($("#inputAddCarrier").val() == "" | $("#inputAddCarrier").val() == undefined) {
                    if (!$("#inputAddCarrier").parent().hasClass("has-feedback")) {
                        $("#inputAddCarrier").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    }
                    f = false;
                } else {
                    $("#inputAddCarrier").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                }


                if ($("#inputAddAppointmentTime").val() == "" | $("#inputAddAppointmentTime").val() == undefined) {
                    if (!$("#inputAddAppointmentTime").parent().hasClass("has-feedback")) {
                        $("#inputAddAppointmentTime").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    }
                    f = false;
                } else {
                    $("#inputAddAppointmentTime").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                }
                if ($("#inputAddType").val() == "" | $("#inputAddType").val() == undefined) {
                    if (!$("#inputAddType").parent().hasClass("has-feedback")) {
                        $("#inputAddType").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    }
                    f = false;
                } else {
                    $("#inputAddType").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                }
                return f;
            },
            GetSelectID: function(type) {
                var v = this;
                var ids = new Array();
                //console.log(v.select_data_rows);
                if (v.select_data_rows != null && v.select_data_rows != undefined && v.select_data_rows != "") {
                    for (var j = 0; j < v.select_data_rows.length; j++) {
                        //console.log(v.select_data_rows[j].type + "=====" + type);
                        if (v.select_data_rows[j].type == type) {
                            ids.push(v.select_data_rows[j].id);
                        }
                    }
                }
                return ids.toString();
            },
            Deduplication: function(rows) {
                var v = this;
                var flag = true;
                var obj = [];

                for (var i = 0; i < v.select_data_rows.length; i++) {
                    obj.push(v.select_data_rows[i]);
                }

                for (var i = 0; i < rows.length; i++) {
                    for (var j = 0; j < v.select_data_rows.length; j++) {
                        if (v.select_data_rows[j] == rows[i]) {
                            flag = false;
                        }
                    }
                    if (flag) {
                        obj.push(rows[i]);
                    }
                }
                v.select_data_rows = null;
                v.select_data_rows = obj;
            },
            RelTableHandler: function(table, rows) { //字符串拼写
                //console.log(table);
                console.log(rows); //rows[i].send_psid
                var v = this;

                if (v.select_data_rows == undefined || v.select_data_rows == "") {
                    v.select_data_rows = "";
                }
                v.Deduplication(rows);
                var carrierJsonObject = {};
                for (var i = 0; i < v.carrierJson.length; i++) {
                    var value = v.carrierJson[i].value;
                    carrierJsonObject[value] = v.carrierJson[i].text;
                }
                for (var i = 0; i < rows.length; i++) {
                    var con = null;
                    if (carrierJsonObject && rows[i].scac) {
                        con = $(templates.singleBoxBol()).find(".bol-title h3").html(v.replaceFirst(rows[i].type) + "<a href='#'>" + rows[i].num + "</a>").end()
                            .find(".to").html("&nbsp;" + (carrierJsonObject["" + rows[i].scac] ? carrierJsonObject["" + rows[i].scac] : v.formartStr(rows[i].scac))).end()
                            .find(".plateNum").html("&nbsp;" + v.formartStr(rows[i].pallets)).end()
                            .find(".volume").html("&nbsp;" + v.formartStr(rows[i].volumes)).end()
                            .find(".weight").html("&nbsp;" + v.formartStr(Math.ceil(((rows[i].weights || 0) * 100)) / 100)).end();


                    } else {
                        con = $(templates.singleBoxBol()).find(".bol-title h3").html(v.replaceFirst(rows[i].type) + "<a href='#'>" + rows[i].num + "</a>").end()
                            .find(".to").html("&nbsp;" + v.formartStr(rows[i].scac)).end()
                            .find(".plateNum").html("&nbsp;" + v.formartStr(rows[i].pallets)).end()
                            .find(".volume").html("&nbsp;" + v.formartStr(rows[i].volumes)).end()
                            .find(".weight").html("&nbsp;" + v.formartStr(Math.ceil(((rows[i].weights || 0) * 100)) / 100)).end();


                    }
                    var createPanelView = new createBoxPanel({
                        content: con,
                        width: '',
                        container: '#ContainerAddBol',
                        id: rows[i].id,
                        type: rows[i].type,
                        close: function() {
                            //hyq has changed code as follows(2015-4-8)
                            //return v.delOrder(this.id,this.type);
                            return v.delOrder(this);
                        }
                    });
                }

            },
            //hyq has changed delOrder way as follows(2015-4-8);


            delOrder: function(obj) {
                //console.log(obj);   //createBoxPanel弹窗对象
                //console.log(obj.content);  //obj.content内容是一个jQuery对象
                //obj.content.remove();  //调用jquery对象中的remove()方法
                var id = obj.id
                var type = obj.type;

                var v = this;
                var obj1 = obj;

                var d = new Dialog({
                    content: "Delete the associated documents to determine it?",
                    icon: 'question',
                    lock: false,
                    width: 200,
                    height: 40,
                    title: 'Prompt',
                    okValue: 'Yes',
                    ok: function() {
                        var obj = [];
                        for (var j = 0; j < v.select_data_rows.length; j++) {
                            if (v.select_data_rows[j].id == id && v.select_data_rows[j].type == type) {

                            } else {
                                obj.push(v.select_data_rows[j]);
                            }
                        }
                        v.select_data_rows = null
                        v.select_data_rows = obj;
                        obj1.content.remove();
                        return true;
                    },
                    cancelValue: 'No',
                    cancel: function() {

                    }
                }).showModal();

                //return false;
            },

            reloadLoad: function(table) {
                var v = this;
                table.reload({
                    queryParams: [{
                        no: $("#searchLoadNo").val(),
                        startDate: $("#searchBeginLoadMABD").val(),
                        endDate: $("#searchEndLoadMABD").val(),
                        checkIds: v.GetSelectID("load"),
                        storageId: $("#inputAddWareHourse").attr("data-value"),
                        type: "load"
                    }]
                });
            },
            reloadOrder: function(table) {
                var v = this;
                table.reload({
                    queryParams: [{
                        checkLIds: v.GetSelectID("load"),
                        checkOIds: v.GetSelectID("order"),
                        //checkIds: v.GetSelectID("order"),
                        //otype: $(".btn-group").find("button.active").attr("data-value"),
                        otype: v.inout,
                        rfno: $("#searchNo").val()
                    }]
                });
            },
            reloadPo: function(table) {
                var v = this;
                table.reload({
                    queryParams: [{
                        no: $("#searchPoNo").val(),
                        from: $("#searchPOFrom").val(),
                        to: $("#searchPOTo").val(),
                        checkIds: v.GetSelectID("po"),
                        storageId: $("#inputAddWareHourse").attr("data-value"),
                        type: "po"
                    }]
                });
            },
            replaceFirst: function(word) {
                return word.replace("load", "Load").replace("po", "PO").replace("order", "Order");
            },
            formartStr: function(word) {
                if (word == "" || word == undefined || word == null) {
                    return "";
                }
                return word;
            }
        });

    


});
