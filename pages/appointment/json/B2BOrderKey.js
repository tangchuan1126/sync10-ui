"use strict";
define({
"Open":{"zh":"Open","en":"Open"},
"Close":{"zh":"Close","en":"Close"},
"1":{"zh":"Imported","en":"Imported"},
"2":{"zh":"Committed","en":"Committed"},
"3":{"zh":"Picking","en":"Picking"},
"4":{"zh":"Packing","en":"Packing"},
"5":{"zh":"Staging","en":"Staging"},
"6":{"zh":"Staged","en":"Staged"},
"7":{"zh":"Loading","en":"Loading"},
"8":{"zh":"Closed","en":"Closed"},
"9":{"zh":"Cancel","en":"Cancel"}
});