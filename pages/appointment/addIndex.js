﻿/**
    *create by cuixiang 20141202
*/
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "bootstrap",
    "nprogress",

    "js/view/v4Create",
    "blockui"
    

], function ($, Backbone, handlebars, bootstrap, NProgress,Create) {
    NProgress.start();

    (function () {
        $.blockUI.defaults = {
            css: {
                padding: '8px',
                margin: 0,
                width: '170px',
                top: '45%',
                left: '40%',
                textAlign: 'center',
                color: '#000',
                border: '3px solid #999999',
                backgroundColor: '#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: '0.6'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut: 1000,
            showOverlay: true
        };
    })();
    $.ajaxSetup({
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    });
	/*
    $(document).ajaxStart(function () {
        $.blockUI({ message: '<img src="/Sync10-ui/pages/load/css/img/dy_loading.gif" class="loading" align="absmiddle"/> 正在处理，请稍后.' });   //遮罩打开 
    });
    $(document).ajaxError(function () {
        $.blockUI({ message: '出现错误，请刷新!<div><a class="closeBlock">关闭</a></div>' });   //遮罩打开 
        $(".closeBlock").click(function () {
            $.unblockUI();
        });
    });
    $(document).ajaxSuccess(function () {
        $.unblockUI();
    });
	*/

    var createView = new Create();
    createView.render();

    //var v3AddRelWarehouseView = new v3AddRelWarehouse();
    //v3AddRelWarehouseView.render();

    //var v3AddRelOrderView = new v3AddRelOrder();
    //v3AddRelOrderView.render();

    //var v3AddCNTRView = new v3AddCNTR();
    //v3AddCNTRView.render();

    //var v3SaveInfoView = new v3SaveInfo();
    //v3SaveInfoView.render();

    

    NProgress.done();
}
);