﻿(function () {
    var configObj = {
        appiontmentList: {  //列表
            url: "/Sync10/_load/getAppointmentList"//url: "/Sync10/ui/pages/WMSAppointment/json/appiontmentList.json"
        },
        createAppiontement: {   //创建
            url: "/Sync10-ui/pages/appointment/add.html"
        },
		appiontementLimit: {
			url: "/Sync10-ui/pages/appointmentLimit4/index.html"
			//url: "/Sync10-ui/pages/appointmentLimit/index.html"
		},
        addAppointment: {  //创建时提交
            url: "/Sync10/_load/saveAppointment"
        },
        EditSaveAppointment: {  //编辑完提交
            url: "/Sync10/_load/addAppointment"
        },
        delAppiontment: {   //取消约车
            url: "/Sync10/_load/delAppointment"
        },
        getAppointmentInfo: {//获取约车信息.
            url: "/Sync10/_load/editAppointment"
        },
        getLoginStorage: {
            url: "/Sync10/_load/getLoginStorageCatalog"
        },
        getSingleInvoiceInfo: { //根据单据ID获取单据相关信息，返回json数据和loadlist、orderlist等信息格式一样.
            url: "/Sync10/_load/getInviceByTypeId",
        },
        getOrderLRList:{
            url:"/Sync10/_load/getAppLRList"
        },
        getLoadList: {  //load列表
            url: "/Sync10/_load/getAppLoadList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        getOrderList: { //order列表
            url: "/Sync10/_load/getAppOrderList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        getPoList: {    //po列表
            url: "/Sync10/_load/getAppPoList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        shipTo: {   //仓库地址json
//            url: "/Sync10-ui/pages/appointment/json/shipTo.json"
            url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        },
        carrierJson: {  //卡车信息json
            //url: "/Sync10-ui/pages/appointment/json/carrier.json"
            url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllFreight"
        },
        orderType: {    //单据类型.
            url: "/Sync10-ui/pages/appointment/json/orderType.json"
        },


        orderpoType: {    //单据类型.
            url: "/Sync10-ui/pages/appointment/json/orderpoType.json"
        },

        appiontmentCalendarTimeUrl: {    //calendar-time 入库出库数据
          // url: "/Sync10-ui/pages/appointment/json/appiontmentCalendar.json"
           url: "/Sync10/_load/sumWorkByDate"
        },
        calendarSumAppointmentUrl:{
           url: "/Sync10/_load/sumWorkByStartEnd"
        },
		adminSession: {
			url: "/Sync10/_load/session"
		}
    };
    define(configObj);
}).call(this);