require(['/Sync10-ui/requirejs_config.js'], function() {
	require([
		"jquery",
		"artDialog",
		"bootstrap.datetimepicker",
		"require_css!bootstrap.datetimepicker-css",
		"bootstrap"
	], function($,artDialog,datetimepicker){
		var timeCheckedCount = 0;
		function loadData(psid,week) {
			//console.log({"storage_id":psid,"week_id":week});
			$.getJSON("/Sync10/_load/getDefaultValue", {"storage_id":psid,"week_id":week} , function (result) {
				$(result.DATA).each(function(index,obj) {
					//console.log(obj);
					if (obj.ID<10) {
						$("#initHour input[name='0"+obj.ID+"'].inboundInput").val(obj.VALUE_IN);
						$("#initHour input[name='0"+obj.ID+"'].outboundInput").val(obj.VALUE_OUT);
					} else {
						$("#initHour input[name='"+obj.ID+"'].inboundInput").val(obj.VALUE_IN);
						$("#initHour input[name='"+obj.ID+"'].outboundInput").val(obj.VALUE_OUT);
					}
				});
			});
		}
		$(function(){
			$("#setStartTime").trigger("focus");   //一开始进来触发设置时间区间一触发获取焦点
			$("#setStartTime").focus(function(){});
			$("#setEndTime").focus(function(){});
			//点击复选框按钮
			$(".checkboxButton .btn").click(function(){
				if(!$(this).hasClass("active")){
					$(this).css({"background":"#59E076","color":"white"});
				}else{
					$(this).css({"background":"#eee","color":"#333"});
				}
			})
			$("input[type='number']").attr("min","0");    //控制减数时最小值为0
			//获取头部开关值
			$("#selectStartDate").attr("disabled",true);
			$("#selectEndDate").attr("disabled",true);
			$("#selectStartDate").datetimepicker({
				format: 'mm/dd/yyyy',
				autoclose: 1,
				minView: 2,
				forceParse: 0,
				bgiconurl: ""
			}).on('changeDate', function(ev){
				var startTime = $('#selectEndDate');
				startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
			});
			$("#selectEndDate").datetimepicker({
				format: 'mm/dd/yyyy',
				autoclose: 1,
				minView: 2,
				forceParse: 0,
				bgiconurl: ""
			}).on('changeDate', function(ev){
				var startTime = $('#selectStartDate');
				startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
			});
			//头部按钮切换
			$("#lastGroup .onoffswitch-checkbox").click(function(){
				if($.trim($("#lastGroup .onoffswitch-checkbox:checked").val()) == "on"){
					$("#selectStartDate").attr("disabled",true).val("").parent().removeClass("has-error");
					$("#selectEndDate").attr("disabled",true).val("").parent().removeClass("has-error");
				}else{
					$("#selectStartDate").attr("disabled",false).parent().removeClass("has-error");
					$("#selectEndDate").attr("disabled",false).parent().removeClass("has-error");
					$("ul.dates").removeClass("has-error");
				}
			})
			$(".dates li:not(.all)").click(function(){
				//星期框点击事件
				$(this).addClass("active").css("background","#E3F9E3");
				$(this).siblings().removeClass("active").css("background","#EFEFEF");
				//loadData("1000005",$(this).attr("data-value"));
			});
			//点击星期all按钮
			$(".dates #all").click(function(){
				if(this.checked){ 
					$("ul input[type='checkbox']:not('#all')").each(function(){
						this.checked="checked";
					})
				}else{
					$("ul input[type='checkbox']:not('#all')").attr("checked",false);
				}
			})
			var checkedCount = 0;
			//星期的点击事件不包括星期全选框
			$("ul input[type='checkbox']").click(function(){
				$("ul.dates").removeClass("has-error");
			});
			$("ul input[type='checkbox']:not('#all')").click(function(){
				if(this.checked){
					checkedCount = checkedCount+1;
					if(checkedCount == 7){   //星期是否全部勾选中判断
						$(".dates #all")[0].checked = 'checked';
					}
				}else{
					checkedCount = checkedCount-1;
					if(checkedCount <7){
						$(".dates #all").attr("checked",false);
					}
					$(this).next("label").css("color","#1479fb");
					$(this).parent("li").css({"background":"#efefef"});
				}
			})
			//点击整点时间全选与反选
			var num = /^[0-9]{1,}$/
			$("#allvalue").blur(function(){
				if(!num.test($(this).val())){
					$(this).val("");
				}
			});
			$("#setAll").click(function(){
				//先清空所有时间点的文本inbound和outbound值
				if($("#allvalue").val()){
					if(!num.test($("#allvalue").val())){
						$(".setallinputgroup").addClass("has-error");
					}else{
						$(".setallinputgroup").removeClass("has-error");  //剔除必填项警示类样式
						if($("#initHour input[type='checkbox']:checked").length>0){  //判断时间点复选框是否至少有一个勾选中
							//$(this).attr("data-value",$(this).text());
							var inorout = "";
							if ($("#checkboxButtons label.active").length==0) {
								$("#checkboxButtons label.btn-primary1").css("border","1px solid #a94442");
							} else {
								$("#checkboxButtons label").css("border","1px solid #ccc");
								$("#checkboxButtons label.active").each(function(){
									if ($(this).attr("name")==="inbound") {
										$("#initHour input[type='checkbox']:checked").each(function(i,e){  //默认选中的所有文本框inbound值为set输入的值
											$(this).parent("div").next("div").children("input.inboundInput").val($("#allvalue").val());
										});
									}
									if ($(this).attr("name")==="outbound") {
										$("#initHour input[type='checkbox']:checked").each(function(i,e){  //默认选中的所有文本框outbound值为set输入的值
											$(this).parent("div").next("div").children("input.outboundInput").val($("#allvalue").val());
										});
									}
								});
							}
						}
					}
				} else {  //没输入值
					$(".setallinputgroup").addClass("has-error");
				}
			})
			$("input[type='number']").blur(function(){  //所有列表时间点文本框失去焦点
				if($(this).val() && (parseInt($(this).val())<0)){  //输入的时间值小于0则默认设置0
					$(this).val(0);
				}
			})
			$("#setEndTime").blur(function(){  //设置时间区间2失去焦点事件
				if($(this).val() && (parseInt($(this).val())<0)){  //输入的时间值小于0则默认设置0
					$(this).val(0);
				}else if($(this).val() && (parseInt($(this).val())>23)){  //输入的值大于23则默认设置为23
					$(this).val(23);
				}
				if($(this).val() && $("#setStartTime").val()){
					if(parseInt($(this).val()) < $("#setStartTime").val()){
						$(this).val($("#setStartTime").val());
						$("#setEndTime").trigger("blur");
					}else{
						$(".hourCondition .setTimeTip").hide();
						$(".initHour input[type='checkbox']").each(function(){
							$(this)[0].checked = "";
						})
						$(".setTimes div").removeClass("has-error");  //剔除警示样式
						var num1 = parseInt($("#setStartTime").val());
						var num2 = parseInt($("#setEndTime").val());
						$(".hourCondition .setTimeTip").hide();
						for(var i=num1;i<=num2;i++){   //整点时间区间
							$("#time"+i)[0].checked = "checked";   //设置区间内的整点时间都选中 
						}
					}
				}else if(($(this).val() && (!$("#setStartTime").val())) || ((!$(this).val()) && $("#setStartTime").val()) || ((!$(this).val()) && (!$("#setStartTime").val()))){
					$(".setTimes div").addClass("has-error");
				}
			})
			//时间区中前面时间失去焦点
			$("#setStartTime").blur(function(){
				if($(this).val() && (parseInt($(this).val())<0)){
					$(this).val(0);
				}else if($(this).val() && (parseInt($(this).val())>23)){
					$(this).val(23);
				}
				if($(this).val() && $("#setEndTime").val()){
					if(parseInt($(this).val()) > parseInt($("#setEndTime").val())){
						$(this).val($("#setEndTime").val());
						$("#setStartTime").trigger("blur");
					}else{
						$(".hourCondition .setTimeTip").hide();
						$(".initHour input[type='checkbox']").each(function(){
							$(this)[0].checked = "";
						})
						$(".setTimes div").removeClass("has-error");  //剔除警示样式
						var num1 = parseInt($("#setStartTime").val());
						var num2 = parseInt($("#setEndTime").val());
						$(".hourCondition .setTimeTip").hide();
						for(var i=num1;i<=num2;i++){   //整点时间区间
							$("#time"+i)[0].checked = "checked";   //设置区间内的整点时间都选中 
						}
					}
				}else if(($(this).val() && (!$("#setEndTime").val())) || ((!$(this).val()) && $("#setEndTime").val()) || ((!$(this).val()) && (!$("#setEndTime").val()))){
					$(".setTimes div").addClass("has-error");
				}
			});
			$("#save").click(function(){
				if ($("#myonoffswitch")[0].checked) {
					//重复
					var hasempty = false;
					$("#initHour").find("input[type='number']").each(function(index,item) {
						if (""===item.value) {
							$(this).addClass("has-error").attr("title","Please input value");
							hasempty = true;
						} else {
							$(this).removeClass("has-error")
						}
					});
					//dates
					if ($(".dates input:gt(0):checked").length==0) {
						console.log("has-error");
						$("ul.dates").addClass("has-error");
						return;
					} else {
						$("ul.dates").removeClass("has-error");
					}
					if(!hasempty) {
						var POSTDATA = {};
						var data = [];
						for (var i=0;i<24;i++) {
							var item = {}
							item.hour = i;
							if (i<10) {
								item.value_in = ($("#initHour input[name='0"+i+"'].inboundInput").val());
								item.value_out = ($("#initHour input[name='0"+i+"'].outboundInput").val());
							} else {
								item.value_in = ($("#initHour input[name='"+i+"'].inboundInput").val());
								item.value_out = ($("#initHour input[name='"+i+"'].outboundInput").val());
							}
							data.push(item);
						}
						//setBaseWork
						
						POSTDATA.storage_id = 1000005;
						POSTDATA.weeks = [];
						$(".dates input:gt(0):checked").each(function(){
							POSTDATA.weeks.push($(this).attr("data-value"));
						});
						POSTDATA.data = JSON.stringify(data);
						$(this).attr("disabled","disabled");
						$.ajax({
							type: 'POST',
							url: "/Sync10/_load/setBaseWork",
							//url: config.setBaseWorkUrl.url,
							data: POSTDATA,
							dataType: 'json',
							async:false,
							success: function (data) {
								$("#save").removeAttr("disabled");
								artDialog.tips("Save ok",3);
							},
							error: function (data) {
								$("#save").removeAttr("disabled");
								//console.log(data.responseJSON);
								artDialog.alert("Setup Error.");
							}
						});
					}
				} else {
					//特例
					var flag = true;
					if ($("#selectStartDate").val()==="") {
						flag = false;
						$("#selectStartDate").parent().addClass("has-error");
					} else {
						$("#selectStartDate").parent().removeClass("has-error");
					}
					if ($("#selectEndDate").val()==="") {
						flag = false;
						$("#selectEndDate").parent().addClass("has-error");
					} else {
						$("#selectEndDate").parent().removeClass("has-error");
					}
					var hasempty = false;
					$("#initHour input[type='number']").removeClass("has-error");
					if($("#initHour input[type='checkbox']:checked").length==0){
						$("#initHour input[type='checkbox']").parent().parent().addClass("has-error");
					} else {
						$("#initHour input[type='checkbox']").parent().parent().removeClass("has-error");
						$("#initHour input[type='checkbox']:checked").parent().next().find("input[type='number']").each(function(){
							if ($(this).val()=="") {
								$(this).addClass("has-error").attr("title","Please input value");
								hasempty = true;
							} else {
								$(this).removeClass("has-error").removeAttr("title");
							}
						});
					}
					if(!hasempty) {
						var POSTDATA = {};
						var data = [];
						for (var i=0;i<24;i++) {
							var item = {}
							item.hour = i;
							$("#initHour input[type='checkbox']:checked").parent().next()
							if (i<10) {
								if ($("#initHour input[name='0"+i+"']").parent().prev("div.ckbox").find("input[type='checkbox']:checked").length==1) {
									item.value_in = ($("#initHour input[name='0"+i+"'].inboundInput").val());
									item.value_out = ($("#initHour input[name='0"+i+"'].outboundInput").val());
									data.push(item);
								}
							} else {
								if ($("#initHour input[name='"+i+"']").parent().prev("div.ckbox").find("input[type='checkbox']:checked").length==1) {
									item.value_in = ($("#initHour input[name='"+i+"'].inboundInput").val());
									item.value_out = ($("#initHour input[name='"+i+"'].outboundInput").val());
									data.push(item);
								}
							}
							
						}
						//setBaseWork
						POSTDATA.start = $("#selectStartDate").val();
						POSTDATA.end = $("#selectEndDate").val();
						POSTDATA.storage_id = 1000005;
						POSTDATA.weeks = [];
						$(".dates input:gt(0):checked").each(function(){
							POSTDATA.weeks.push($(this).attr("data-value"));
						});
						POSTDATA.data = JSON.stringify(data);
						console.log(POSTDATA);
						$(this).attr("disabled","disabled");
						$.ajax({
							type: 'POST',
							url: "/Sync10/_load/setDefaultValueByDate",
							//url: config.setBaseWorkUrl.url,
							data: POSTDATA,
							dataType: 'json',
							async:false,
							success: function (data) {
								$("#save").removeAttr("disabled");
								artDialog.tips("Save ok",3);
							},
							error: function (data) {
								$("#save").removeAttr("disabled");
								//console.log(data.responseJSON);
								artDialog.alert("Setup Error.");
							}
						});
					}
				}
			});
		});
	});
});