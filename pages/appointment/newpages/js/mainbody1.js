require(['/Sync10-ui/requirejs_config.js',"../config"], function(_r,config) {
	require(["jquery","moment","artDialog","immybox",
		"bootstrap.datetimepicker",
		"require_css!bootstrap.datetimepicker-css","bootstrap"],function($,moment,artDialog,immybox,datetimepicker){
			function loadWeekData(psid,start,end) {
				$.getJSON("/Sync10/_load/getBaseWorkByWeek", {"storage_id":psid,"start":start,"end":end} , function (result) {
					var DayMap = result.DATES;
					$(result.DATA).each(function(index,obj){
						var day =DayMap[obj.work_date];
						if (obj.hour<10) {
							$("#"+day+"0"+obj.hour+"IN").text(obj.sumin);
							$("#"+day+"0"+obj.hour+"OUT").text(obj.sumout);
						} else {
							$("#"+day+""+obj.hour+"IN").text(obj.sumin);
							$("#"+day+""+obj.hour+"OUT").text(obj.sumout);
						}
					});
				});
			}
			$.getJSON(config.shipFrom.url,{"type":"1"}, function (json) {
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].data,"text":json[i].name});
				}
				$('#shipFrom').immybox({
					Pleaseselect:'Clean Select',
					Defaultselect:1000005,
					choices: rls
				});
				$(function(){
					var w0 =  moment().startOf('week').format("YYYY-MM-DD");
					var w6 =  moment().endOf('week').format("YYYY-MM-DD");
					$("#home table tbody tr:odd").css("background","#f5f6f5");
					$("#home table tr").each(function(){
						$(this).children(":first").css({"color":"#007AFF","font-weight":"bold"});
					});
					
					$("#home tbody tr:first").children("td:gt(0)").css({"color":"black","font-weight":"bold"});
					//表头点击事件
					$("#home table th").click(function(){  
						$(this).removeClass("clickStyle");
						$(this).siblings() .removeClass("clickStyle");
						$(this).addClass("clickStyle");
						$("tbody td").removeClass();
						$("tbody tr:first").children("td").css("background","white");  //tbody中第一行所有列样式
					})
					//表格列点击事件
					$("#home tbody tr:first").children("td:gt(0)").each(function(i,e){
						$(this).click(function(){
							$("table td").css("border-collapse","collapse");
							$(this).siblings().css("background","white");   //tbody中第一行所有列样式
							$(this).css("background","#478dcd");
							var cols = i+1;
							//alert($(this).parent("tr").siblings("tr").length);   //24行
							$("tbody tr td").removeClass();
							$()
							$(this).parent("tr").siblings("tr").each(function(){
								$(this).children("td:eq("+cols+")").addClass("selectCols");
							})
						})
						//$(this).parent("tr").next("tr").children("td:gt("+i+")").addClass("selectCols");
					})
					//表格行点击事件
					$("#home tbody tr:gt(0)").each(function(){
						$(this).children(":eq(0)").click(function(){
							$(this).removeClass();
							$("tbody td").removeClass();
							$("tbody tr:first").children("td").css("background","white");  //tbody中第一行所有列样式
							$(this) .parent().siblings("tr").children("td:nth-child(1)").removeClass();  //所在行兄弟行的第一个子元素剔除所有类
							$(this).parent().siblings().children("td").removeClass("selectRows");     //所在行对应列剔除所有样式
							//$(this).addClass("selectTr").css("color","white");           //所在行对应列添加行样式 
							$(this).parent().children("td").addClass("selectRows");     //所在行对应列添加行样式
						})
					})
					//单元格即使某一列点击事件
					$("#home tbody tr:gt(0)").each(function(i,e){
						//alert(i);
						$(this).children("td:gt(0)").each(function(j,e){
							//alert(j)
							$(this).click(function(){
								var count = j+1;
								//alert(i);
								//alert(j);
								$("table th").removeClass("clickStyle");
								$("tbody tr:first").children("td").css("background","white");   //tbody中第一行所有列样式
								$(this).removeClass();  //剔除当前点击列的所有类样式
								$("tbody td").removeClass(); //剔除tbody中所有列的类样式
								$(this).addClass("clickTd"); //为当前点击具体行列添加样式
								$("tbody tr td:eq(0)").removeClass();   //第一行中所有列剔除所有样式
								$("tbody tr:eq(0)").children("td").removeClass();
								$(this).parent("tr").children("td:eq(0)").addClass("clickTd"); //当前列所在行第一列添加样式
								$("tbody tr:eq(0)").children("td:eq("+count+")").addClass("clickTd");    //当前列在第一行中列添加样式
							});
						});
					});
					$("#tprv").click(function(){
						//console.log("tprv");
						w0 = moment(w0,"YYYY-MM-DD").add(-2,"day").startOf('week').format("YYYY-MM-DD");
						w6 = moment(w0,"YYYY-MM-DD").endOf('week').format("YYYY-MM-DD");
						$("#home thead th:gt(0)").each(function(idx,obj){
							$(this).html(moment(w0,"YYYY-MM-DD").add(idx,"day").format("YYYY-MM-DD"));
						});
						loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
					});
					$("#tnext").click(function(){
						//console.log("tnext");
						w6 = moment(w6,"YYYY-MM-DD").add(2,"day").endOf('week').format("YYYY-MM-DD");
						w0 = moment(w6,"YYYY-MM-DD").startOf('week').format("YYYY-MM-DD");
						$("#home thead th:gt(0)").each(function(idx,obj){
							$(this).html(moment(w0,"YYYY-MM-DD").add(idx,"day").format("YYYY-MM-DD"));
						});
						loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
						//monment(w6)
					});
					$("#tsetup").click(function(){
					});
					$("#home thead th:gt(0)").each(function(idx,obj){
						$(this).html(moment(w0,"YYYY-MM-DD").add(idx,"day").format("YYYY-MM-DD"));
					});
					
					loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
				});
				$(function(){
					function loadData(psid,week) {
						//console.log({"storage_id":psid,"week_id":week});
						$.getJSON("/Sync10/_load/getDefaultValue", {"storage_id":psid,"week_id":week} , function (result) {
							$(result.DATA).each(function(index,obj) {
								//console.log(obj);
								if (obj.ID<10) {
									$("#initHour input[name='0"+obj.ID+"'].inboundInput").val(obj.VALUE_IN);
									$("#initHour input[name='0"+obj.ID+"'].outboundInput").val(obj.VALUE_OUT);
								} else {
									$("#initHour input[name='"+obj.ID+"'].inboundInput").val(obj.VALUE_IN);
									$("#initHour input[name='"+obj.ID+"'].outboundInput").val(obj.VALUE_OUT);
								}
							});
						});
					}
					$(function(){
						$("#setStartTime").trigger("focus");   //一开始进来触发设置时间区间一触发获取焦点
						$("#setStartTime").focus(function(){});
						$("#setEndTime").focus(function(){});
						//点击复选框按钮
						$(".checkboxButton .btn").click(function(){
							if(!$(this).hasClass("active")){
								$(this).css({"background":"#59E076","color":"white"});
							}else{
								$(this).css({"background":"#eee","color":"#333"});
							}
						})
						$("input[type='number']").attr("min","0");    //控制减数时最小值为0
						//获取头部开关值
						$("#selectStartDate").attr("disabled",true);
						$("#selectEndDate").attr("disabled",true);
						$("#selectStartDate").datetimepicker({
							format: 'mm/dd/yyyy',
							autoclose: 1,
							minView: 2,
							forceParse: 0,
							bgiconurl: ""
						}).on('changeDate', function(ev){
							var startTime = $('#selectEndDate');
							startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
						});
						$("#selectEndDate").datetimepicker({
							format: 'mm/dd/yyyy',
							autoclose: 1,
							minView: 2,
							forceParse: 0,
							bgiconurl: ""
						}).on('changeDate', function(ev){
							var startTime = $('#selectStartDate');
							startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
						});
						//头部按钮切换
						$("#lastGroup .onoffswitch-checkbox").click(function(){
							if($.trim($("#lastGroup .onoffswitch-checkbox:checked").val()) == "on"){
								$("#selectStartDate").attr("disabled",true).val("").parent().removeClass("has-error");
								$("#selectEndDate").attr("disabled",true).val("").parent().removeClass("has-error");
							}else{
								$("#selectStartDate").attr("disabled",false).parent().removeClass("has-error");
								$("#selectEndDate").attr("disabled",false).parent().removeClass("has-error");
								$("ul.dates").removeClass("has-error");
							}
						})
						$(".dates li:not(.all)").click(function(){
							//星期框点击事件
							$(this).addClass("active").css("background","#E3F9E3");
							$(this).siblings().removeClass("active").css("background","#EFEFEF");
							//loadData("1000005",$(this).attr("data-value"));
						});
						//点击星期all按钮
						$(".dates #all").click(function(){
							if(this.checked){ 
								$("ul input[type='checkbox']:not('#all')").each(function(){
									this.checked="checked";
								})
							}else{
								$("ul input[type='checkbox']:not('#all')").attr("checked",false);
							}
						})
						var checkedCount = 0;
						//星期的点击事件不包括星期全选框
						$("ul input[type='checkbox']").click(function(){
							$("ul.dates").removeClass("has-error");
						});
						$("ul input[type='checkbox']:not('#all')").click(function(){
							if(this.checked){
								checkedCount = checkedCount+1;
								if(checkedCount == 7){   //星期是否全部勾选中判断
									$(".dates #all")[0].checked = 'checked';
								}
							}else{
								checkedCount = checkedCount-1;
								if(checkedCount <7){
									$(".dates #all").attr("checked",false);
								}
								$(this).next("label").css("color","#1479fb");
								$(this).parent("li").css({"background":"#efefef"});
							}
						})
						//点击整点时间全选与反选
						var num = /^[0-9]{1,}$/
						$("#allvalue").blur(function(){
							if(!num.test($(this).val())){
								$(this).val("");
							}
						});
						$("#setAll").click(function(){
							//先清空所有时间点的文本inbound和outbound值
							if($("#allvalue").val()){
								if(!num.test($("#allvalue").val())){
									$(".setallinputgroup").addClass("has-error");
								}else{
									$(".setallinputgroup").removeClass("has-error");  //剔除必填项警示类样式
									if($("#initHour input[type='checkbox']:checked").length>0){  //判断时间点复选框是否至少有一个勾选中
										//$(this).attr("data-value",$(this).text());
										var inorout = "";
										if ($("#checkboxButtons label.active").length==0) {
											$("#checkboxButtons label.btn-primary1").css("border","1px solid #a94442");
										} else {
											$("#checkboxButtons label").css("border","1px solid #ccc");
											$("#checkboxButtons label.active").each(function(){
												if ($(this).attr("name")==="inbound") {
													$("#initHour input[type='checkbox']:checked").each(function(i,e){  //默认选中的所有文本框inbound值为set输入的值
														$(this).parent("div").next("div").children("input.inboundInput").val($("#allvalue").val());
													});
												}
												if ($(this).attr("name")==="outbound") {
													$("#initHour input[type='checkbox']:checked").each(function(i,e){  //默认选中的所有文本框outbound值为set输入的值
														$(this).parent("div").next("div").children("input.outboundInput").val($("#allvalue").val());
													});
												}
											});
										}
									}
								}
							} else {  //没输入值
								$(".setallinputgroup").addClass("has-error");
							}
						})
						$("input[type='number']").blur(function(){  //所有列表时间点文本框失去焦点
							if($(this).val() && (parseInt($(this).val())<0)){  //输入的时间值小于0则默认设置0
								$(this).val(0);
							}
						})
						$("#setEndTime").blur(function(){  //设置时间区间2失去焦点事件
							if($(this).val() && (parseInt($(this).val())<0)){  //输入的时间值小于0则默认设置0
								$(this).val(0);
							}else if($(this).val() && (parseInt($(this).val())>23)){  //输入的值大于23则默认设置为23
								$(this).val(23);
							}
							if($(this).val() && $("#setStartTime").val()){
								if(parseInt($(this).val()) < $("#setStartTime").val()){
									$(this).val($("#setStartTime").val());
									$("#setEndTime").trigger("blur");
								}else{
									$(".hourCondition .setTimeTip").hide();
									$(".initHour input[type='checkbox']").each(function(){
										$(this)[0].checked = "";
									})
									$(".setTimes div").removeClass("has-error");  //剔除警示样式
									var num1 = parseInt($("#setStartTime").val());
									var num2 = parseInt($("#setEndTime").val());
									$(".hourCondition .setTimeTip").hide();
									for(var i=num1;i<=num2;i++){   //整点时间区间
										$("#time"+i)[0].checked = "checked";   //设置区间内的整点时间都选中 
									}
								}
							}else if(($(this).val() && (!$("#setStartTime").val())) || ((!$(this).val()) && $("#setStartTime").val()) || ((!$(this).val()) && (!$("#setStartTime").val()))){
								$(".setTimes div").addClass("has-error");
							}
						})
						//时间区中前面时间失去焦点
						$("#setStartTime").blur(function(){
							if($(this).val() && (parseInt($(this).val())<0)){
								$(this).val(0);
							}else if($(this).val() && (parseInt($(this).val())>23)){
								$(this).val(23);
							}
							if($(this).val() && $("#setEndTime").val()){
								if(parseInt($(this).val()) > parseInt($("#setEndTime").val())){
									$(this).val($("#setEndTime").val());
									$("#setStartTime").trigger("blur");
								}else{
									$(".hourCondition .setTimeTip").hide();
									$(".initHour input[type='checkbox']").each(function(){
										$(this)[0].checked = "";
									})
									$(".setTimes div").removeClass("has-error");  //剔除警示样式
									var num1 = parseInt($("#setStartTime").val());
									var num2 = parseInt($("#setEndTime").val());
									$(".hourCondition .setTimeTip").hide();
									for(var i=num1;i<=num2;i++){   //整点时间区间
										$("#time"+i)[0].checked = "checked";   //设置区间内的整点时间都选中 
									}
								}
							}else if(($(this).val() && (!$("#setEndTime").val())) || ((!$(this).val()) && $("#setEndTime").val()) || ((!$(this).val()) && (!$("#setEndTime").val()))){
								$(".setTimes div").addClass("has-error");
							}
						});
						$("#save").click(function(){
							if ($("#myonoffswitch")[0].checked) {
								//重复
								var hasempty = false;
								$("#initHour").find("input[type='number']").each(function(index,item) {
									if (""===item.value) {
										$(this).addClass("has-error").attr("title","Please input value");
										hasempty = true;
									} else {
										$(this).removeClass("has-error")
									}
								});
								//dates
								if ($(".dates input:gt(0):checked").length==0) {
									//console.log("has-error");
									$("ul.dates").addClass("has-error");
									return;
								} else {
									$("ul.dates").removeClass("has-error");
								}
								if(!hasempty) {
									var POSTDATA = {};
									var data = [];
									for (var i=0;i<24;i++) {
										var item = {}
										item.hour = i;
										if (i<10) {
											item.value_in = ($("#initHour input[name='0"+i+"'].inboundInput").val());
											item.value_out = ($("#initHour input[name='0"+i+"'].outboundInput").val());
										} else {
											item.value_in = ($("#initHour input[name='"+i+"'].inboundInput").val());
											item.value_out = ($("#initHour input[name='"+i+"'].outboundInput").val());
										}
										data.push(item);
									}
									//setBaseWork
									
									POSTDATA.storage_id = $('#shipFrom').attr("data-value");
									POSTDATA.weeks = [];
									$(".dates input:gt(0):checked").each(function(){
										POSTDATA.weeks.push($(this).attr("data-value"));
									});
									POSTDATA.data = JSON.stringify(data);
									$(this).attr("disabled","disabled");
									$.ajax({
										type: 'POST',
										url: "/Sync10/_load/setBaseWork",
										//url: config.setBaseWorkUrl.url,
										data: POSTDATA,
										dataType: 'json',
										async:false,
										success: function (data) {
											$("#save").removeAttr("disabled");
											//artDialog.tips("Save ok",3);
											var w0 = moment().startOf('week').format("YYYY-MM-DD");
											var w6 = moment().endOf('week').format("YYYY-MM-DD");
											loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
											$('#tabbody a[href="#home"]').tab('show');
										},
										error: function (data) {
											$("#save").removeAttr("disabled");
											//console.log(data.responseJSON);
											artDialog.alert("Setup Error.");
										}
									});
								}
							} else {
								//特例
								var flag = true;
								if ($("#selectStartDate").val()==="") {
									flag = false;
									$("#selectStartDate").parent().addClass("has-error");
								} else {
									$("#selectStartDate").parent().removeClass("has-error");
								}
								if ($("#selectEndDate").val()==="") {
									flag = false;
									$("#selectEndDate").parent().addClass("has-error");
								} else {
									$("#selectEndDate").parent().removeClass("has-error");
								}
								var hasempty = false;
								$("#initHour input[type='number']").removeClass("has-error");
								if($("#initHour input[type='checkbox']:checked").length==0){
									$("#initHour input[type='checkbox']").parent().parent().addClass("has-error");
								} else {
									$("#initHour input[type='checkbox']").parent().parent().removeClass("has-error");
									$("#initHour input[type='checkbox']:checked").parent().next().find("input[type='number']").each(function(){
										if ($(this).val()=="") {
											$(this).addClass("has-error").attr("title","Please input value");
											hasempty = true;
										} else {
											$(this).removeClass("has-error").removeAttr("title");
										}
									});
								}
								if(!hasempty) {
									var POSTDATA = {};
									var data = [];
									for (var i=0;i<24;i++) {
										var item = {}
										item.hour = i;
										$("#initHour input[type='checkbox']:checked").parent().next()
										if (i<10) {
											if ($("#initHour input[name='0"+i+"']").parent().prev("div.ckbox").find("input[type='checkbox']:checked").length==1) {
												item.value_in = ($("#initHour input[name='0"+i+"'].inboundInput").val());
												item.value_out = ($("#initHour input[name='0"+i+"'].outboundInput").val());
												data.push(item);
											}
										} else {
											if ($("#initHour input[name='"+i+"']").parent().prev("div.ckbox").find("input[type='checkbox']:checked").length==1) {
												item.value_in = ($("#initHour input[name='"+i+"'].inboundInput").val());
												item.value_out = ($("#initHour input[name='"+i+"'].outboundInput").val());
												data.push(item);
											}
										}
										
									}
									//setBaseWork
									POSTDATA.start = $("#selectStartDate").val();
									POSTDATA.end = $("#selectEndDate").val();
									POSTDATA.storage_id = $('#shipFrom').attr("data-value");
									POSTDATA.weeks = [];
									$(".dates input:gt(0):checked").each(function(){
										POSTDATA.weeks.push($(this).attr("data-value"));
									});
									POSTDATA.data = JSON.stringify(data);
									//console.log(POSTDATA);
									$(this).attr("disabled","disabled");
									$.ajax({
										type: 'POST',
										url: "/Sync10/_load/setDefaultValueByDate",
										//url: config.setBaseWorkUrl.url,
										data: POSTDATA,
										dataType: 'json',
										async:false,
										success: function (data) {
											$("#save").removeAttr("disabled");
											var w0 = moment($("#selectStartDate").val(),"MM/DD/YYYY").startOf('week').format("YYYY-MM-DD");
											var w6 = moment($("#selectStartDate").val(),"MM/DD/YYYY").endOf('week').format("YYYY-MM-DD");
											loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
											$('#tabbody a[href="#home"]').tab('show');
										},
										error: function (data) {
											$("#save").removeAttr("disabled");
											//console.log(data.responseJSON);
											artDialog.alert("Setup Error.");
											
										}
									});
								}
							}
						});
					});
				});
			});
	});
});