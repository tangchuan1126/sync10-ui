(function () {
	var configObj = {
		shipFrom: {
			//url : "/Sync10-ui/pages/appointment/json/shipTo.json"
			url : "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
		},
		adminSession: {
			url : "/Sync10/_fileserv/redis_session"
		}
	};
	define(configObj);
}).call(this);