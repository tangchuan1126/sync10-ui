(function(){
    var configObj = {
    	//国家ur信息
        countryUrl:{
            addUrl:"/Sync10/basicdata/countrys/addCountry",
            modUrl:"/Sync10/basicdata/countrys/updateCountry",
            getUrl:"/Sync10/basicdata/countrys/getCountry",
            delUrl:"/Sync10/basicdata/countrys/deleteCountry",
            checkUrl:"/Sync10/basicdata/countrys/checkCountry",
            getAllUrl:"/Sync10/basicdata/countrys/countryList"
        },
        //省份url State
        provinceUrl:{
            addUrl:"/Sync10/basicdata/provinces/addProvince",
            getAllUrl:"/Sync10/basicdata/provinces/provinceList",
            modUrl:"/Sync10/basicdata/provinces/updateProvince",
            delUrl:"/Sync10/basicdata/provinces/deleteProvince",
            checkUrl:"/Sync10/basicdata/provinces/checkProvince",
        }
        
    };
	define(configObj);
	
}).call(this);
