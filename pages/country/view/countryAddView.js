/**
 * Created by liyi on 2015.4.20.
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/countryModel",
    "artDialog"
],function(config,$,Backbone,Handlebars,templates,models){

    /**页面:修改账号*/
    return Backbone.View.extend({

        template:templates.add_country,
        initialize:function(options){

        },
        setView:function(views){
            this.countrySearchResultView = views.countrySearchResultView;
        },
        shoeErrow:function(error,element) {  
            element.parent().next().text(error); 
        },
        validator:function(v,dialog){
            var c_code = $("#c_code");
            var c_code2 = $("#c_code2");
            var c_country = $("#c_country");

            if(c_code.val()=="") {
              v.shoeErrow("Please enter alpha-2",c_code);
              return;
            } else if(c_code.val().length<2) {
              v.shoeErrow("Please enter 2 character",c_code);
              return;
            } else {
               v.shoeErrow("",c_code); 
            }
            if(c_code2.val()=="") {
              v.shoeErrow("Please enter alpha-3",c_code2);
              return;
            } else if(c_code2.val().length<3) {
              v.shoeErrow("Please enter 3 character",c_code2);
              return;
            }else {
              v.shoeErrow("",c_code2);
            }
            if(c_country.val()=="") {
              v.shoeErrow("Please enter country",c_country);
              return;
            } else {
              v.shoeErrow("",c_country);
            }
            var url = config.countryUrl.checkUrl;
            $.post(url,{c_code:c_code.val(),c_code2:c_code2.val(),c_country:c_country.val()},function(data){
                if(data.success) {
                    $.blockUI({message:'<div class="block_message">Please wait......</div>'});
                    dialog.button({
                        name: 'Submit',
                        focus: true,
                        disabled: true
                    });
                    var model = new models.CountryModel;
                    model.url=config.countryUrl.addUrl;
                    model.save({
                        "c_country":$("#c_country").val().replace(/(^\s*)|(\s*$)/g, ""),
                        "c_code":$("#c_code").val().replace(/(^\s*)|(\s*$)/g, ""),
                        "c_code2":$("#c_code2").val().replace(/(^\s*)|(\s*$)/g, "")
                    },{
                        success: function (data) {
                            if(data.get("success")){
                                
                                v.countrySearchResultView.render();
                                dialog.close();
                               
                            }else{
                                dialog.button({
                                    name: 'Submit',
                                    focus: true,
                                    disabled: false
                                });
                            }
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.unblockUI();
                        }
                    });
                } else {
                    if(data.c_code) {
                        v.shoeErrow("Alpha-2 already exists",c_code);
                    } else {
                        v.shoeErrow("",c_code);
                    }
                    if(data.c_code2) {
                        v.shoeErrow("Alpha-3 already exists",c_code2);
                    } else {
                        v.shoeErrow("",c_code2);
                    }
                    if(data.c_country) {
                        v.shoeErrow("Country already exists",c_country);
                    } else {
                        v.shoeErrow("",c_country);
                    }
                }
            });
        },
        render:function(){
            var tmp = this;
            art.dialog({
                title:'Add Country'
                ,lock: true
                ,opacity:0.3
                ,init:function(){
                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(tmp.template({
                    }));
                }
                ,close:function(){
                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";
                },
                button: [{
                    name: 'Submit',
                    callback: function () {

                        var dialog = this;
                        tmp.validator(tmp,dialog);
                       

                        return false;
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
        }
    });
});
