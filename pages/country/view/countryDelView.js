/**
 * Created by liyi on 2015.4.18.
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/countryModel"
],function(config,$,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.del_country,
        initialize:function(options){
        },
        setView:function(views){
            this.countrySearchResultView = views.countrySearchResultView;
        },
        render:function(model){
            var tmp = this;
            art.dialog({
                title:'Del Country » '+model.toJSON().c_country
                ,lock: true
                ,opacity:0.3
                ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(tmp.template({
                        model:model.toJSON()
                    }));
                }
                ,close:function(){
                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";
                }
                ,icon:'warning'
                ,ok:function(){
                    var countryModel = new models.CountryModel({
                        ccid:model.toJSON().ccid
                    });
                    countryModel.url =config.countryUrl.delUrl+"?ccid="+model.get("ccid");
                    countryModel.destroy({
                        success: function(model, response){
                            if(response.success){
                                tmp.countrySearchResultView.render();
                            }else{

                                art.dialog({
                                    title:'Message'
                                    ,lock:true
                                    ,opacity:0.3
                                    ,content:"Delete Failure"
                                    ,icon:'warning'
                                    ,ok:true
                                    ,okVal:'Sure'
                                });
                            }
                        }
                    });
                }
                ,okVal:'Sure'
                ,cancel:true
                ,cancelVal:'Cancel'
            });
        }
    });
});
