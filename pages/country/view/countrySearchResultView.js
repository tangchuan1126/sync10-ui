/**
 * Created by liyi on 2015.4.18
 http://zh.wikipedia.org/wiki/ISO_3166-1
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/countryModel",
    "Paging",
    "jqueryui/tabs"
],function(config,$,Backbone,Handlebars,templates,models,Paging){

    return Backbone.View.extend({

        template:templates.search_result,
        initialize:function(options){
            this.setElement(options.el);
            this.collection = new models.CountryCollection;
        },
        setView:function(views){
            this.countryModView = views.countryModView;
            this.countryDelView = views.countryDelView;
            this.provinceListView = views.provinceListView;
        },
        render:function(searchParam){
            //搜索参数
            if(!searchParam){
                searchParam = new models.SearchModel();
                $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            }
            var tmp = this;
            this.collection.reset();
            this.collection.url=config.countryUrl.getAllUrl;
            this.collection.fetch({
                data:{
                    pageNo:models.CountryCollection.pageCtrl.pageNo,
                    pageSize:models.CountryCollection.pageCtrl.pageSize,
                    searchConditions:searchParam.toJSON().searchConditions
                },
                success:function(collection){
                    tmp.$el.html(tmp.template({
                        resultList:collection.toJSON(),
                        pageCtrl:models.CountryCollection.pageCtrl
                    }));

                    Paging.update_page(
                        models.CountryCollection.pageCtrl.pageCount
                        ,models.CountryCollection.pageCtrl.pageNo
                        , "pagination"
                        , function(pageNo) {
                            models.CountryCollection.pageCtrl.pageNo = pageNo;
                            tmp.render(searchParam);
                    });

                    $.unblockUI();
                }   
            });
        },
        events:{
            "click a[name='modCountry']":"modCountry",
            "click a[name='delCountry']":"delCountry",
            "click a[name='provinceList']":"provinceList"
        },
        modCountry:function(evt){
            var ccid = $(evt.target).parents("tr").data("ccid");
            var model = this.collection.findWhere({ccid:ccid});
            this.countryModView.render(model);
        },
        delCountry:function(evt){
            var ccid = $(evt.target).parents("tr").data("ccid");
            var model = this.collection.findWhere({ccid:ccid});
            this.countryDelView.render(model);
        },
        provinceList:function(evt){
            var ccid = $(evt.target).parents("tr").data("ccid");
            var model = this.collection.findWhere({ccid:ccid});
            this.provinceListView.render(model);
        }
    });
});
