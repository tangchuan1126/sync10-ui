/**
 * Created by liyi on 2015.4.18.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/countryModel"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.search,
        initialize:function(options){

            this.setElement(options.el);
        },
        events:{
            "click #addCountry":"addCountry"
        },
        setView:function(views){

            this.countrySearchResultView = views.countrySearchResultView;
            this.countryAddView = views.countryAddView;
        },
        addCountry:function(){
            this.countryAddView.render();
        },
        render:function(){
            var tmp = this;
            this.$el.html(this.template({

            }));

            $("#search_key").bind("keydown",function(evt){
                if(evt.keyCode==13){
                    tmp.search();
                }
            });

            $("#eso_search").bind("click",function(evt){
                tmp.search();
            });
        },
        search:function(){
            var searchModel = new models.SearchModel({
                searchConditions:$("#search_key").val()
            });
            this.countrySearchResultView.render(searchModel);
        }
    });
});
