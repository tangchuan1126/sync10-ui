/**
*created by liyi on 2015.4.18.
*/
"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/countryModel",
  "art_Dialog/dialog-plus",
  "Paging"
], function(config,$, Backbone, Handlebars, templates,models,Dialog,Paging) {
    return Backbone.View.extend({
      template:templates.province_result,
      collection: new models.ProvinceCollection(),
      model:new models.ProvinceModel(),
      initialize: function(){
      },
      setView:function(views){
          this.provinceDelView = views.provinceDelView;
      },
      shoeErrow:function(error,element) {
          var nextTr = $(element.parentNode.parentNode).next("tr")[0];
          var index = $(element.parentNode).index();
          $(nextTr.children[index]).text(error);
      },
      validator:function(evt,v){
        var ccid = $(evt.target).data("ccid");
        var pro_id =$(evt.target).parent().parent('tr').data("pro_id");
        var pro_name = $("#pro_name")[0];
        var p_code = $("#p_code")[0];
        if(p_code.value=="") {
          v.shoeErrow("Please enter code",p_code);
          return;
        } else {
          v.shoeErrow("",p_code);
        }
        if(pro_name.value=="") {
          v.shoeErrow("Please enter name",pro_name);
          return;
        } else {
          v.shoeErrow("",pro_name);
        }
        
        var url = config.provinceUrl.checkUrl;

        $.post(url,{p_code:p_code.value,pro_id:pro_id,pro_name:pro_name.value,ccid:ccid},function(data){
                if(data.success) {
                  $.blockUI({message:'<div class="block_message">Please wait......</div>'});
                  var proModel = new models.ProvinceModel();
                  proModel.set({pro_name:$("#pro_name").val(),p_code:$("#p_code").val(),nation_id:ccid});
                  proModel.url=config.provinceUrl.addUrl;
                  if(pro_id!="") {
                     proModel.url=config.provinceUrl.modUrl;
                     proModel.set("pro_id",pro_id);
                  }
                  proModel.save({},{
                      success: function (data) {
                        if(data.get("success")) {
                          v.shoeErrow("",pro_name);
                          v.shoeErrow("",p_code);
                          v.ad.close();
                          v.render(v.model);
                        }
                        $.unblockUI();
                      },
                      error: function (data) {
                        $.unblockUI();
                      }
                  });
                }
                if(data.pro_name) {
                  v.shoeErrow("Name already exists",pro_name);
                } else {
                  v.shoeErrow("",pro_name);
                } 

                if(data.p_code) {
                  v.shoeErrow("Code already exists",p_code);
                } else {
                  v.shoeErrow("",p_code);
                }
            }
        );
      },
      modProvince:function(evt,v){
        var pro_id = $(evt.target).parent().parent('tr').data("pro_id");
        var pModel = v.collection.findWhere({pro_id:pro_id});
        $("#p_code").val(pModel.get("p_code"));
        $("#pro_name").val(pModel.get("pro_name"));
        $("#pro_name").parent().parent('tr').data("pro_id",pModel.get("pro_id"));
        $("#addProvince").attr("title","EditState");
        $("#addProvince").text("EditState");
        $('.validator_style').text('');
        $("#cancel").show();
      },
      delProvince:function(evt,v){
        var pro_id = $(evt.target).parent().parent('tr').data("pro_id");
        var pModel = v.collection.findWhere({pro_id:pro_id});
        this.provinceDelView.setView({provinceResultView:v});
        this.provinceDelView.render(pModel,v.model);
      },
      addProvince:function(evt,v){
        var ccid = $(evt.target).data("ccid");
        var trNode = $(evt.target).parent().parent('tr');
        v.validator(evt,v);
      },
      cancel:function(evt,v){
        $("#p_code").val("");
        $("#pro_name").val("");
        $("#pro_name").parent().parent('tr').data("pro_id","");
        $("#addProvince").attr("title","AddState");
        $("#addProvince").text("AddState");
        $("#cancel").hide();
        $('.validator_style').text('');

      },
      render:function(model){
          this.model = model;
          var v = this;
          $.blockUI({message:'<div class="block_message">Please wait......</div>'});
          if(model!=null){
              v.collection.url = config.provinceUrl.getAllUrl;
              v.collection.reset();
              v.collection.fetch({
                async: false,
                data:{
                    ccid:model.toJSON().ccid,
                    pageNo:models.ProvinceCollection.pageCtrl.pageNo,
                    pageSize:models.ProvinceCollection.pageCtrl.pageSize
                }
              });
              var html =v.template({resultList: v.collection.toJSON(),country:model.toJSON(), pageCtrl:models.ProvinceCollection.pageCtrl});
              var ad = art.dialog({title:model.get("c_country")+' for State List'
                  ,width:680
                  ,lock: true
                  ,opacity:0.3
                  ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(html);
                    $("a[name='modProvince']").click(function(evt){
                        v.modProvince(evt,v);
                    });
                      
                      
                    $("a[name='delProvince']").click(function(evt){
                        v.delProvince(evt,v,ad);
                    });
                    var addProvince=$("#addProvince").click(function(evt){
                        v.addProvince(evt,v);
                    });
                    
                    $("#cancel").click(function(evt){
                      v.cancel(evt,v);
                    });
                    $("#p_code").keydown(function() {
                      if (event.keyCode == "13") {
                        $('#addProvince').click();
                      }
                    });
                    $("#pro_name").keydown(function() {
                      if (event.keyCode == "13") {
                        $('#addProvince').click();
                      }
                    });
                },close:function(){
                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";
                }
                
                  
                });
                
                v.ad=ad;
                Paging.update_page(
                        models.ProvinceCollection.pageCtrl.pageCount
                        ,models.ProvinceCollection.pageCtrl.pageNo
                        , "pagination"
                        , function(pageNo) {
                            models.ProvinceCollection.pageCtrl.pageNo = pageNo;
                            ad.close();  
                            v.render(model);
                    });
          }
         
        $.unblockUI(); 
      }
      
 });
}); 