/**
 * Created by liyi on 2015.4.18.
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/countryModel"
],function(config,$,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.del_province,
        initialize:function(options){
        },
        setView:function(views){
            this.provinceResultView = views.provinceResultView;
        },
        render:function(pModel,cModel){
            var tmp = this;
            art.dialog({
                title:'Del Province » '+pModel.get("pro_name")
                ,lock: true
                ,opacity:0.3
                ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(tmp.template({
                        model:pModel.toJSON()
                    }));
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";
                }
                ,icon:'warning'
                ,ok:function(){

                    var provinceModel = new models.ProvinceModel({
                        pro_id:pModel.get("pro_id")
                    });

                    provinceModel.url =config.provinceUrl.delUrl+"?pro_id="+pModel.get("pro_id");

                    provinceModel.destroy({
                        success: function(model, response){

                            if(response.success){
                                tmp.provinceResultView.ad.close();
                                tmp.provinceResultView.render(cModel);
                            }else{

                                art.dialog({
                                    title:'Message'
                                    ,lock:true
                                    ,opacity:0.3
                                    ,content:"Delete Failure"
                                    ,icon:'warning'
                                    ,ok:true
                                    ,okVal:'Sure'
                                });
                            }
                        }
                    });
                }
                ,okVal:'Sure'
                ,cancel:true
                ,cancelVal:'Cancel'
            });
        }
    });
});
