/**
 * Created by liyi on 2015.04.18.
 */

"use strict";
define([
    "../config",
    "jquery",
    "backbone"
],function(config,$,Backbone){

     //国家
    var CountryModel = Backbone.Model.extend({
        
        idAttribute: "ccid",
        defaults: {
            c_country: "",
            c_code: "",
            c_code2: ""
        }
    });


    //国家集合
    var CountryCollection = Backbone.Collection.extend({
        
        model: CountryModel,
        parse: function (response) {
            CountryCollection.pageCtrl = response.pagectrl;
            return response.countrys;
        }
    }, {
        pageCtrl: {
            pageNo: 1,
            pageSize:10
        }
    });
     //省份
    var ProvinceModel = Backbone.Model.extend({
        idAttribute: "pro_id"
        
    });
   //省份集合
    var ProvinceCollection = Backbone.Collection.extend({  
        model: ProvinceModel,
        parse: function (response) {
            ProvinceCollection.pageCtrl = response.pagectrl;
            return response.provinces;
        }
    }, {
        pageCtrl: {
            pageNo: 1,
            pageSize:10
        }
    });
    //搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            searchConditions: ""
        }
    });

    return {
        CountryModel:CountryModel
        ,CountryCollection:CountryCollection
        ,SearchModel:SearchModel
        ,ProvinceModel:ProvinceModel
        ,ProvinceCollection:ProvinceCollection
    };

});