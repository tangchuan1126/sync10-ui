(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_country'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\r\n    <form id=\"addCountryForm\" >\r\n        <table>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Alpha-2 Code</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" maxlength='2' onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\"  name='c_code' id='c_code' type=\"text\" />\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Alpha-3 Code</span>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" maxlength='3'  onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\"  name='c_code2' id='c_code2' type='text' />\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Country Name</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='c_country' id='c_country' onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\"type='text' maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>";
},"useData":true});
templates['del_country'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_country_dialog\">\r\n    Delete\r\n	<span>\r\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.c_country : stack1), depth0))
    + "\r\n    </span>\r\n    ?\r\n</div>\r\n";
},"useData":true});
templates['del_province'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_country_dialog\">\r\n    Delete\r\n	<span>\r\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pro_name : stack1), depth0))
    + "\r\n    </span>\r\n    ?\r\n</div>\r\n";
},"useData":true});
templates['mod_country'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"dialog_update_content\">\r\n    <form id=\"modCountryForm\" >\r\n        <table>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Alpha-2 Code</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input oldc_code='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code : stack1), depth0))
    + "' class=\"input_text_password_select\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" name='c_code' value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code : stack1), depth0))
    + "' id='c_code' type=\"text\" maxlength=\"2\">\r\n                    <input type='hidden' name='ccid' id=\"ccid\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.ccid : stack1), depth0))
    + "'\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Alpha-3 Code</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input oldc_code2='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code2 : stack1), depth0))
    + "' class=\"input_text_password_select\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" name='c_code2' id='c_code2' value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code2 : stack1), depth0))
    + "' type='text' maxlength=\"3\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Country Name</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input oldc_country='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_country : stack1), depth0))
    + "' class=\"input_text_password_select\"  name='c_country' id='c_country' onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_country : stack1), depth0))
    + "' type='text' maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>";
},"useData":true});
templates['province_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-pro_id=\""
    + alias2(alias1((depth0 != null ? depth0.pro_id : depth0), depth0))
    + "\">\r\n                <td valign=\"middle\" width=\"330px\">\r\n                    "
    + alias2(alias1((depth0 != null ? depth0.p_code : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"330px\">\r\n                    "
    + alias2(alias1((depth0 != null ? depth0.pro_name : depth0), depth0))
    + "\r\n                </td>\r\n                \r\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\r\n                      <a href=\"javascript:void(0)\" name=\"modProvince\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\r\n                    <a href=\"javascript:void(0)\" name=\"delProvince\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\r\n                </td>\r\n            </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
    return "            <tr>\r\n                <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\r\n            </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div align=\"left\" style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;;margin-bottom:10px;\">\r\n     \r\n     <table width=\"100%\">\r\n        <tr data-pro_id=\"\">\r\n            <td width=\"200px\"><b class=\"require_property\">*</b>Code:&nbsp;&nbsp;<input type=\"input\" style=\"width:150px\" id=\"p_code\" name=\"p_code\" class=\"input_text_password_select\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" maxlength='3'></td>\r\n            <td width=\"200px\" minlength='1'> \r\n                <b class=\"require_property\">*</b>Name:&nbsp;&nbsp;<input type=\"input\" name=\"pro_name\" id=\"pro_name\" class=\"input_text_password_select\" style=\"width:150px;\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" >\r\n            </td>\r\n            <td  valign=\"middle\" width=\"200px\" style=\"text-align: center\">\r\n           <a href=\"javascript:void(0)\"  class=\"buttons\" style=\"margin-left: 1px;\" data-ccid=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.ccid : stack1), depth0))
    + "\" id=\"addProvince\" title=\"Add State\">Add State</a>\r\n           <a href=\"javascript:void(0)\"  class=\"buttons\" style=\"margin-left: 1px;display:none\"  id=\"cancel\" title=\"Cancel\">Cancel</a>\r\n            </td>\r\n\r\n        </tr>\r\n        <tr>\r\n            <td class=\"status validator_style\"></td>\r\n            <td class=\"status validator_style\"></td>\r\n            <td class=\"status validator_style\"></td>\r\n            \r\n        </tr>\r\n     </table>\r\n   \r\n</div>\r\n<div class=\"search_result_list\">\r\n    <div>\r\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\r\n            <tr>\r\n                <th width=\"330px\" style=\"text-align: center\">Code</th>\r\n                <th width=\"330px\" style=\"text-align: center\">Name</th>\r\n                <th width=\"200px\" style=\"text-align: center\">\r\n                    Operation\r\n                </th>\r\n            </tr>\r\n        </table>\r\n\r\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\r\n    </div>\r\n\r\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\r\n        </ul>\r\n    </div>\r\n</div>";
},"useData":true});
templates['search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\r\n    <div>\r\n        <div class=\"eso_search_parent\">\r\n            <div class=\"eso_search_icon\">\r\n                <a>\r\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search.gif\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n\r\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\r\n            <a id=\"addCountry\" href=\"javascript:void(0)\" title='AddCountry' class=\"buttons icon add button_add_country\">Add Country</a>\r\n        </div>\r\n\r\n        <div class=\"eso_search_input\">\r\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
templates['search_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-ccid=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\">\r\n                <td valign=\"middle\" width=\"330px\">\r\n                    "
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"330px\">\r\n                    "
    + alias2(alias1((depth0 != null ? depth0.c_code2 : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"460px\">\r\n                    "
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\r\n                    <a href=\"javascript:void(0)\" name=\"modCountry\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\r\n                    <a href=\"javascript:void(0)\" name=\"delCountry\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\r\n                    <a href=\"javascript:void(0)\" name=\"provinceList\" class=\"buttons icon add\" style=\"margin-left: 1px;\" title=\"ProvinceList\"></a>\r\n                    \r\n                </td>\r\n            </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
    return "	        <tr>\r\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\r\n	        </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\">\r\n    <div>\r\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\r\n            <tr>\r\n                <th width=\"330px\">Alpha-2 Code</th>\r\n                <th width=\"330px\">Alpha-3 Code</th>\r\n                <th width=\"460px\">Country Name</th>\r\n                <th width=\"200px\" style=\"border-right:0px;\">\r\n                    Operation\r\n                </th>\r\n            </tr>\r\n        </table>\r\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\r\n    </div>\r\n\r\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\r\n        </ul>\r\n    </div>\r\n</div>";
},"useData":true});
})();