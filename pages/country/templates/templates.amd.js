define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_country'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\n    <form id=\"addCountryForm\" >\n        <table>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Alpha-2 Code</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" maxlength='2' onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\"  name='c_code' id='c_code' type=\"text\" />\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Alpha-3 Code</span>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" maxlength='3'  onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\"  name='c_code2' id='c_code2' type='text' />\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Country Name</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='c_country' id='c_country' onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\"type='text' maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n        </table>\n    </form>\n</div>";
},"useData":true});
templates['del_country'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_country_dialog\">\n    Delete\n	<span>\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.c_country : stack1), depth0))
    + "\n    </span>\n    ?\n</div>\n";
},"useData":true});
templates['del_province'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_country_dialog\">\n    Delete\n	<span>\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pro_name : stack1), depth0))
    + "\n    </span>\n    ?\n</div>\n";
},"useData":true});
templates['mod_country'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"dialog_update_content\">\n    <form id=\"modCountryForm\" >\n        <table>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Alpha-2 Code</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input oldc_code='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code : stack1), depth0))
    + "' class=\"input_text_password_select\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" name='c_code' value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code : stack1), depth0))
    + "' id='c_code' type=\"text\" maxlength=\"2\">\n                    <input type='hidden' name='ccid' id=\"ccid\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.ccid : stack1), depth0))
    + "'\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Alpha-3 Code</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input oldc_code2='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code2 : stack1), depth0))
    + "' class=\"input_text_password_select\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" name='c_code2' id='c_code2' value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_code2 : stack1), depth0))
    + "' type='text' maxlength=\"3\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Country Name</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input oldc_country='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_country : stack1), depth0))
    + "' class=\"input_text_password_select\"  name='c_country' id='c_country' onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" value='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.c_country : stack1), depth0))
    + "' type='text' maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n        </table>\n    </form>\n</div>";
},"useData":true});
templates['province_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-pro_id=\""
    + alias2(alias1((depth0 != null ? depth0.pro_id : depth0), depth0))
    + "\">\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.p_code : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.pro_name : depth0), depth0))
    + "\n                </td>\n                \n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\n                      <a href=\"javascript:void(0)\" name=\"modProvince\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\n                    <a href=\"javascript:void(0)\" name=\"delProvince\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\n                </td>\n            </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "            <tr>\n                <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n            </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div align=\"left\" style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;;margin-bottom:10px;\">\n     \n     <table width=\"100%\">\n        <tr data-pro_id=\"\">\n            <td width=\"200px\"><b class=\"require_property\">*</b>Code:&nbsp;&nbsp;<input type=\"input\" style=\"width:150px\" id=\"p_code\" name=\"p_code\" class=\"input_text_password_select\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" maxlength='3'></td>\n            <td width=\"200px\" minlength='1'> \n                <b class=\"require_property\">*</b>Name:&nbsp;&nbsp;<input type=\"input\" name=\"pro_name\" id=\"pro_name\" class=\"input_text_password_select\" style=\"width:150px;\" onkeyup=\"value=value.replace(/[\\d]/g,'')\" onbeforepaste=\"clipboardData.setData('text',clipboardData.getData('text').replace(/[\\d]/g,'))\" >\n            </td>\n            <td  valign=\"middle\" width=\"200px\" style=\"text-align: center\">\n           <a href=\"javascript:void(0)\"  class=\"buttons\" style=\"margin-left: 1px;\" data-ccid=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.country : depth0)) != null ? stack1.ccid : stack1), depth0))
    + "\" id=\"addProvince\" title=\"Add State\">Add State</a>\n           <a href=\"javascript:void(0)\"  class=\"buttons\" style=\"margin-left: 1px;display:none\"  id=\"cancel\" title=\"Cancel\">Cancel</a>\n            </td>\n\n        </tr>\n        <tr>\n            <td class=\"status validator_style\"></td>\n            <td class=\"status validator_style\"></td>\n            <td class=\"status validator_style\"></td>\n            \n        </tr>\n     </table>\n   \n</div>\n<div class=\"search_result_list\">\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n                <th width=\"330px\" style=\"text-align: center\">Code</th>\n                <th width=\"330px\" style=\"text-align: center\">Name</th>\n                <th width=\"200px\" style=\"text-align: center\">\n                    Operation\n                </th>\n            </tr>\n        </table>\n\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n</div>";
},"useData":true});
templates['search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n    <div>\n        <div class=\"eso_search_parent\">\n            <div class=\"eso_search_icon\">\n                <a>\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search.gif\">\n                </a>\n            </div>\n        </div>\n\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\n            <a id=\"addCountry\" href=\"javascript:void(0)\" title='AddCountry' class=\"buttons icon add button_add_country\">Add Country</a>\n        </div>\n\n        <div class=\"eso_search_input\">\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['search_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-ccid=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\">\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.c_code2 : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"460px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\n                    <a href=\"javascript:void(0)\" name=\"modCountry\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\n                    <a href=\"javascript:void(0)\" name=\"delCountry\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\n                    <a href=\"javascript:void(0)\" name=\"provinceList\" class=\"buttons icon add\" style=\"margin-left: 1px;\" title=\"ProvinceList\"></a>\n                    \n                </td>\n            </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "	        <tr>\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n	        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\">\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n                <th width=\"330px\">Alpha-2 Code</th>\n                <th width=\"330px\">Alpha-3 Code</th>\n                <th width=\"460px\">Country Name</th>\n                <th width=\"200px\" style=\"border-right:0px;\">\n                    Operation\n                </th>\n            </tr>\n        </table>\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n</div>";
},"useData":true});
return templates;
});