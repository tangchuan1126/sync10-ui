/**
 * Created by liyi on 2015.4.18.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "./view/countrySearchView",
    "./view/countrySearchResultView",
    "./view/countryAddView",
    "./view/countryModView",
    "./view/countryDelView",
    "./view/provinceResultView",
    "./view/provinceDelView",
    "blockui",
    "jqueryui/tabs"
],function(
    $
    ,Backbone
    ,Handlebars
    ,CountrySearch
    ,CountrySearchResult
    ,CountryAdd
    ,CountryMod
    ,CountryDel
    ,ProvinceResult
    ,ProvinceDel
){

    (function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();
    $("#all_tabs").tabs();
    var setActiveTab = function(i){
        $("#all_tabs").tabs("option","active",i);
    };
    setActiveTab(0);
    var countrySearchView = new CountrySearch({el:"#search_country"});

    var countrySearchResultView = new CountrySearchResult({el:"#search_result"});

    var countryAddView = new CountryAdd();

    var countryModView = new CountryMod();

    var countryDelView = new CountryDel();

    var provinceDelView = new ProvinceDel();

    var provinceListView = new ProvinceResult({el:"#province_result"});

    provinceListView.setView({
        provinceDelView:provinceDelView
    });

    countrySearchResultView.setView({
        countryModView:countryModView,
        countryDelView:countryDelView,
        provinceListView:provinceListView
    });

    countryAddView.setView({
        countrySearchResultView:countrySearchResultView
    });

    countrySearchView.setView({
        countrySearchResultView:countrySearchResultView,
        countryAddView:countryAddView
    });

    countryModView.setView({
        countrySearchResultView :countrySearchResultView
    });

    countryDelView.setView({
        countrySearchResultView :countrySearchResultView
    });

    countrySearchView.render();
    countrySearchResultView.render();
});