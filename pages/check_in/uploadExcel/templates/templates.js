(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['excelList'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <tr data-fileid=\""
    + escapeExpression(lambda((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\r\n            <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.original_file_name : depth0), depth0))
    + "</td>\r\n            <td><button class=\"btn btn-primary\" type=\"button\" name=\"delBtn\">Delete</button></td>\r\n        </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<table id=\"uploadExcel\" class=\"table table-hover\">\r\n    <thead>\r\n      <tr>\r\n         <th>ReportExcel</th>\r\n         <th>Edit</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.excels : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </tbody>\r\n</table>";
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
})();