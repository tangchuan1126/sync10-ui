define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['excelList'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        <tr data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n            <td>"
    + alias2(alias1((depth0 != null ? depth0.original_file_name : depth0), depth0))
    + "</td>\n            <td><button class=\"btn btn-primary\" type=\"button\" name=\"delBtn\">Delete</button></td>\n        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<table id=\"uploadExcel\" class=\"table table-hover\">\n    <thead>\n      <tr>\n         <th>ReportExcel</th>\n         <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.excels : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </tbody>\n</table>";
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "";
},"useData":true});
return templates;
});