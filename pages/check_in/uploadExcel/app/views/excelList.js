define(['jquery',
        'backbone',
        'handlebars',
        'templates',
        '../models/excel_collection',
        '../models/excel'], 
    function ($, Backbone, Handlebars, templates, ExcelCollection,ExcelModel) { 
        
    var ExcelView = Backbone.View.extend({
            el: "#excelList",
            template: templates.excelList,
            collection: new ExcelCollection(),
            render: function () {
                var that = this;
         
                this.collection.fetch({
                    success: function (collection, data) {
                        that.$el.html(that.template({
                            excels: collection.toJSON()
                        }));
                    } ,errror:function(){
                    }
                })	
                                 
            },
            events:{
                "click button[name=delBtn]":"delExcel"
            },
            delExcel: function (evt) {
                
                var that = this;
                var fileid= $(evt.target).parent().parent().data("fileid");
                if(!fileid) {
                    return;
                }

                $.ajax({
                        type:'detete',
                        url:"/Sync10/_fileserv/file/"+fileid,
                        success:function(){
                            that.render();    
                            if($("#uploadExcel tr").length == 2) {
                                window.location.reload();
                            }
                        }

                }) ;                                       
            }
    });

    return new ExcelView();
});
