define(['backbone', '../../config', './excel'], function (Backbone, page_config, ExcelModel) {
    return Backbone.Collection.extend({
        model: ExcelModel,
        url: page_config.ExcelCollection.url
    
    });
});
