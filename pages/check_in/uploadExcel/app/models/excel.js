define(['jquery',
        'backbone',
        '../../config',
        'jqueryui/dialog'], 
    function ($, Backbone, page_config) {
        return Backbone.Model.extend({
            url: page_config.ExcelModel.url,
            idAttribute: "file_id"
        });
});