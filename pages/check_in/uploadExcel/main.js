 require(['/Sync10-ui/requirejs_config.js'], function() {

 	require(["oso.lib/nprogress/nprogress.min", 
                            "jquery", 
                            "require_css!oso.lib/nprogress/nprogress",
                            "require_css!bootstrap-css/bootstrap.min"],
 		function(NProgress, $) {

 			$("body").show();

 			NProgress.start();

 			require(["oso.lib/jquery-file-upload-extension/upload-plugin",  "app/views/excelList"], function(uploadplugin,excelList) {
               
               $(function(){
               excelList.render();
                var jsonval={
                         renderTo: ".uploadExcel",
                         //nobotstrap:true,
                         fileposturl:"/Sync10/_fileserv/upload/",
                         //FileSize:500000,
                         FileTypes:/(\.|\/)(xlsm|xlsx|xls|png)$/i,
                         acceptFileTypes:"The file type errors",
                         unknownError:"Error message",
                        //batch:false,//关闭批量操作功能
                         //Asinglefile:true//单文件每次替换上次选择的文件
                         //或
                        // templateurl:"xx/xx/xx_tlp.html"
                     };

                var uploadfile = new uploadplugin(jsonval);
             
                //上传成功回调 
                 uploadfile.on("event.done",function(d){
                            if(d==null) {
                                alert("upload Error!");
                                throw new Error("upload Error!");
                                return false;
                            }
                            var file_id = d[0].file_id;
                            if($("#file_ids").val() !="") {
                                    var arr = $("#file_ids").val().split(",");
                                    arr.push(file_id);
                                    arr.join(",");
                                    $("#file_ids").val(arr)
                            } else {
                                    $("#file_ids").val(file_id);
                            }

                 });

        
                 NProgress.done();               

                 $("#save").click(function(){

     
                        $.ajax({
                                type:'post',
                                dataType : 'json',
                                data:$("#uploadForm").serialize(),
                                url:"/Sync10/action/administrator/checkin/upLoadReport.action",
                                success:function(){
                                    window.location.reload();
                                },
                                error:function() {
                                    window.location.reload();
                                }

                        }) ;  

 //                  excelList.render();
               });
                
               });
               //document onload end

 			});

 			//upload-plugin end

 		});
 	  // css end

 });
 //requirejs config end