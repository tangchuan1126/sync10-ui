﻿(function(){
    var configObj = {
    	//账号信息
        accountUrl:{
            url:"/Sync10/action/administrator/proprietary/AccountMgrAction.action"
        },
        //账号上下文信息
        accountContextUrl:{
        	url:"/Sync10/action/administrator/proprietary/AccountContextMgrAction.action"
        },
        //权限信息
        accountPermissionUrl: {
            url:"/Sync10/action/administrator/proprietary/AccountPermissionMgrAction.action"
        },
        //title信息
        accountTitleUrl: {
        	url:"/Sync10/action/administrator/proprietary/AccountTitleMgrAction.action"
        },
        //图片信息
        accountPhotoUrl:{
            url:"/Sync10/action/administrator/proprietary/AccountPhotoMgrAction.action"
        }
    };
	define(configObj);
	
}).call(this);



