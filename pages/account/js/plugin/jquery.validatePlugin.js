define(
	["jquery","validate"]
	,function($) {

    //自定义jquery validate 验证规则
    $.validator.addMethod(
        "mobilephone"
        ,function(value, element) {
            var length = value.length;
            var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
            return (length == 11 && mobile.exec(value))? true:false;
        }
        ,""
    );

    $.validator.addMethod(
        "account"
        ,function(value, element) {
            var length = value.length;

            var regex = /^[a-zA-Z0-9_]+$/;
            return (length >= 5 && regex.exec(value))? true:false;
        }
        ,"eg.  1、_、a, at least five"
    );

    $.validator.addMethod(
        "password"
        ,function(value, element) {
            var length = value.length;

            var regex = /^[a-zA-Z0-9_.!@#$%^&*-]+$/;
            return (length >= 5 && regex.exec(value))? true:false;
        }
        ,"eg.  1、_、a, at least five"
    );
});