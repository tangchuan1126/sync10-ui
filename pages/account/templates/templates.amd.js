define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_admin'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.customer_key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.carrier : depth0), depth0))
    + "</option>\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"content_add_account\" style=\"/*height:508px;*/height:100%;overflow-y:auto;\">\n<form id=\"addAdminForm\"> \n	<table>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Account Number</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input value=\"\" class=\"input_text_password_select\" name=\"account\" id='account' type='text' maxlength=\"18\" title=\"\" placeholder=\"eg.  1、_、a, at least five\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Password</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input value=\"\" class=\"input_text_password_select\" name='pwd' id='pwd' type=\"password\" maxlength=\"12\" title=\"\" placeholder=\"eg.  1、_、a, at least five\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n 				<span>Confirm Password</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name='confirmPwd' id='confirmPwd' type='password' maxlength=\"12\" title=\"\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n	<table style=\"margin-top: 20px;\">\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Full Name</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name='employeName' id='employeName' type='text' size=\"20\" maxlength=\"30\" />\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<span>Telephone</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name=\"mobilePhone\" id=\"mobilePhone\" type=\"text\" maxlength=\"30\" />\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		\n		<tr>\n			<td>\n 				<span>E-Mail</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name=\"email\" id=\"email\" type=\"text\" maxlength=\"320\" />\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<!-- <b class=\"require_property\">*</b> -->\n 				Hiredate\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name=\"entryDate\" id=\"entryDate\" type=\"text\" size=\"20\" maxlength=\"10\" title=\"\" placeholder=\"eg. 2014-08-12\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n	<table style=\"margin-top: 20px;\">\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n 				Department/Post\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" id=\"department\" name=\"department\" type=\"text\" readonly=\"readonly\" title=\"\" >\n				<input id=\"department_id\" name=\"department_id\" type=\"hidden\">\n				\n		   		<div id=\"department_menuContent\" class=\"content_add_account_select\">\n					<ul id=\"department_tree\" class=\"ztree\"></ul>\n				</div>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n		<tr id=\"customer_tr\" name=\"customer_tr\">\n			<td>\n			<b class=\"require_property\">*</b>\n 				Customer\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"customer_id\" name=\"customer_id\" style=\"width:242px;\" >\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n		<tr id=\"carrier_tr\" name=\"carrier_tr\">\n			<td>\n			<b class=\"require_property\">*</b>\n 				Carrier\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"carrier_id\" name=\"carrier_id\" style=\"width:242px;\" >\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.carriers : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n		<tr>\n			<td>\n 				Office Location\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" id=\"officeLocation\" name=\"officeLocation\" type=\"text\" readonly=\"readonly\" >\n				<input id=\"officeLocation_id\" name=\"officeLocation_id\" type=\"hidden\">\n				\n		   		<div id=\"officeLocation_menuContent\" class=\"content_add_account_select\">\n					<ul id=\"officeLocation_tree\" class=\"ztree\"></ul>\n				</div>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n			<b class=\"require_property\">*</b>\n 				Warehouse Type\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"warehouseType\" name=\"warehouseType\" style=\"width:242px;\" >\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.waretype : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				Warehouse\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"warehouse\" name=\"warehouse\" style=\"width:242px;\" >\n					<option value=\"\"></option>\n                </select>\n\n				<input id=\"warehouse_id\" name=\"warehouse_id\" type=\"hidden\">\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n		<tr style=\"line-height:30px;\">\n			<td>\n 				Area\n			</td>\n			<td>&nbsp;</td>\n			<td>\n                <select id=\"area\" style=\"width:242px;\" multiple=\"multiple\">\n                	<option value=\"\"></option>\n            	</select>\n				<input id=\"area_id\" name=\"area_id\" type=\"hidden\">\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n</form>\n</div>";
},"useData":true});
templates['del_admin'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_account_dialog\">\n    Are you sure you want to delete\n	<span>\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.employe_name : stack1), depth0))
    + "\n	</span>\n	?\n</div> \n";
},"useData":true});
templates['import_title'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div style=\"width:770px;height:450px;padding: 10px;box-shadow: 0 0 5px #ccc;\">\n	<div id=\"importAccountTitle\">\n	</div>\n</div> ";
},"useData":true});
templates['manage_permission'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "\n<div style=\"padding: 10px;box-shadow: 0 0 5px #ccc;margin-bottom: 8px;\">\n	<input type=\"text\" id=\"permissionTreeSearch\" style=\"padding: 6px;border-radius: 4px;border: 1px solid silver;font-family: inherit;font-size: inherit;line-height: inherit;width: 200px;\" placeholder=\"Search\">\n</div>\n\n<div class=\"permission_basic_layout\">\n	<div>\n		<div id=\"permission_tree\"></div>\n	</div>\n</div>\n\n<!-- margin: 0em auto 1em auto; --> ";
},"useData":true});
templates['manager_photo'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.calculate || (depth0 && depth0.calculate) || helpers.helperMissing).call(depth0,"(",(data && data.index),"+1)%3 == 1",{"name":"calculate","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(13, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "                </tr>\n                <tr>\n                <td data-id=\""
    + alias2(alias1((depths[1] != null ? depths[1].id : depths[1]), depth0))
    + "\">\n<div style=\"height:220px;width:210px;\">\n<div style=\"width:200px;height:200px;border:1px #bdbdbd solid;position:relative;border-radius:5px;\">\n    \n    <img style=\"max-width:100%;max-height:100%;position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto\" src=\""
    + alias2(alias1((depths[1] != null ? depths[1].file_path : depths[1]), depth0))
    + "\"/>\n   \n    <div style=\"position:absolute; width:200px; height:200px;box-shadow:0 0 5px #ccc;\">\n        \n        <div style=\"height:25px;width:100%;\">\n            <div style=\"position:relative;\">\n                <span class=\"align-width-span-right\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depths[1] != null ? depths[1].activity : depths[1]),{"name":"if","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                </span>\n            </div>\n        </div>\n\n        <a class=\"fancybox-thumb\" rel=\"fancybox-thumb\" href=\""
    + alias2(alias1((depths[1] != null ? depths[1].file_path : depths[1]), depth0))
    + "\" title=\""
    + alias2(alias1((depth0 != null ? depth0.file_flag : depth0), depth0))
    + "\">\n            <div style=\"height:150px;width:100%\"></div>\n        </a>\n\n        <div style=\"background-color:rgba(208,208,208,0.9);height:25px;width:100%;\">\n        <div style=\"position:relative;\">\n            <span class=\"align-width-span-left\">\n                <b>\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.file_flag : depth0),"portrait",{"name":"sif","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.file_flag : depth0),"finger",{"name":"sif","hash":{},"fn":this.program(9, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.file_flag : depth0),"token",{"name":"sif","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </b>\n            </span>\n            <span class=\"align-width-span-right\">\n                <img src=\"image/delete.png\" title=\"DELETE\" name=\"delPhoto\">\n            </span>\n        </div>\n        </div>\n    </div>\n</div>\n</div>\n                </td>\n";
},"3":function(depth0,helpers,partials,data) {
    return "                        <img src=\"image/default.png\" title=\"DEFAULT\" name=\"cancelDefaultPhoto\">\n";
},"5":function(depth0,helpers,partials,data) {
    return "                        <img src=\"image/select.png\" title=\"SET DEFAULT\" name=\"setDefaultPhoto\">\n";
},"7":function(depth0,helpers,partials,data) {
    return "\n                        Head Portrait\n\n";
},"9":function(depth0,helpers,partials,data) {
    return "\n                        Fingerprint\n\n";
},"11":function(depth0,helpers,partials,data) {
    return "\n                        Token\n\n";
},"13":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "                <td data-id=\""
    + alias2(alias1((depths[1] != null ? depths[1].id : depths[1]), depth0))
    + "\">\n<div style=\"height:220px;width:210px;\">\n<div style=\"width:200px;height:200px;border:1px #bdbdbd solid;position:relative;border-radius:5px;\">\n    \n    <img style=\"max-width:100%;max-height:100%;position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto\" src=\""
    + alias2(alias1((depths[1] != null ? depths[1].file_path : depths[1]), depth0))
    + "\"/>\n\n    <div style=\"position:absolute; width:200px; height:200px;box-shadow:0 0 5px #ccc;\">\n        <div style=\"height:25px;width:100%;\">\n            <div style=\"position:relative;\">\n                <span class=\"align-width-span-right\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depths[1] != null ? depths[1].activity : depths[1]),{"name":"if","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                </span>\n            </div>\n        </div>\n\n        <a class=\"fancybox-thumb\" rel=\"fancybox-thumb\" href=\""
    + alias2(alias1((depths[1] != null ? depths[1].file_path : depths[1]), depth0))
    + "\" title=\""
    + alias2(alias1((depth0 != null ? depth0.file_flag : depth0), depth0))
    + "\">\n            <div style=\"height:150px;width:100%\"></div>\n        </a>\n\n        <div style=\"background-color:rgba(208,208,208,0.9);height:25px;width:100%;\">\n        <div style=\"position:relative;\">\n            <span class=\"align-width-span-left\">\n                <b>\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.file_flag : depth0),"portrait",{"name":"sif","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.file_flag : depth0),"finger",{"name":"sif","hash":{},"fn":this.program(9, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || alias3).call(depth0,(depth0 != null ? depth0.file_flag : depth0),"token",{"name":"sif","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </b>\n            </span>\n            <span class=\"align-width-span-right\">\n                <img src=\"image/delete.png\" title=\"DELETE\" name=\"delPhoto\">\n            </span>\n        </div>\n        </div>\n    </div>\n</div>\n</div>\n                </td>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"manage_photo\">\n\n    <div style=\"padding: 10px;box-shadow: 0 0 5px #ccc;margin-bottom: 8px;\">\n        <span>\n            <select id=\"image_flag\" style=\"width:120px;height:24px\">\n            <option value=\"finger\">\n                Fingerprint\n            </option>\n            <option value=\"portrait\">\n                Head Portrait\n            </option>\n            <option value=\"token\">\n                Token \n            </option>\n            </select>\n        </span>\n        <span style=\"margin-left: 4px;\">\n            <button class=\"normal\" id=\"upload_image\">Upload</button>\n        </span>\n    </div>\n    \n\n    \n    <div class=\"manage_photo_content\">\n        <table cellspacing=\"0\">\n            <tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.photoList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </tr>\n        </table>\n    </div>\n</div>";
},"useData":true,"useDepths":true});
templates['manager_title'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<tr>\n								<td name=\"_hasTitle\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.title_id : depth0), depth0))
    + "\" data-sort=\""
    + alias2(alias1((depth0 != null ? depth0.title_admin_sort : depth0), depth0))
    + "\">\n							 		"
    + alias2(alias1((depth0 != null ? depth0.title_admin_sort : depth0), depth0))
    + "."
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "\n							 	</td>\n							</tr>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<tr>\n								<td name=\"_unhasTitle\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.title_id : depth0), depth0))
    + "\">\n									"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "\n								</td>\n							</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"title_basic_layout\">\n	<div class=\"title_basic_layout_header\">\n		<span>\n			Linked TITLE&nbsp;&nbsp;(&nbsp;<em style=\"font-size: 14px;color: red;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.context : depth0)) != null ? stack1.hasTitle : stack1)) != null ? stack1.length : stack1), depth0))
    + "</em>&nbsp;)\n		</span>	    		\n		<label>\n			No Linked TITLE&nbsp;&nbsp;(&nbsp;<em style=\"font-size: 14px;color: red;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.context : depth0)) != null ? stack1.unhasTitle : stack1)) != null ? stack1.length : stack1), depth0))
    + "</em>&nbsp;)\n		</label>\n	</div>\n	<div class=\"title_basic_layout_content\">\n		<table> \n			<tr>\n			    <td>\n				    <div class=\"double_title_list\">\n		        		<table>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.context : depth0)) != null ? stack1.hasTitle : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						 </table>\n						 <input type=\"hidden\" id=\"has_title\" value=\",\"/>\n		      		</div>\n			    </td>\n			    <td valign=\"top\">\n                    <div>\n                        <a id=\"moveTitleLeft\" class=\"buttons icon arrowleft disabled\"  title=\"Linked\"></a>\n                    </div>\n                    <div style=\"margin-top: 6px;\">\n                        <a id=\"moveTitleRight\" class=\"buttons icon arrowright disabled\" title=\"Cancel Linked\"></a>\n                    </div>\n\n                    <div style=\"margin-top: 100px;\">\n                        <a id=\"moveTitleUp\" class=\"buttons icon arrowup disabled\" title=\"Up Priority\"></a>\n                    </div>\n                    <div style=\"margin-top: 6px;\">\n                        <a id=\"moveTitleDown\" class=\"buttons icon arrowdown disabled\" title=\"Down Priority\"></a>\n                    </div>\n			    </td>\n			    <td>\n			   	    <div class=\"double_title_list\">\n		        		<table>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.context : depth0)) != null ? stack1.unhasTitle : stack1),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</table>\n						<input type=\"hidden\" id=\"unhas_title\" value=\",\"/>\n		    		</div>\n				</td>\n			</tr>\n		</table>\n	</div>\n</div>";
},"useData":true});
templates['mod_admin'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.customer_key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.carrier : depth0), depth0))
    + "</option>\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"content_mod_account\">\n<form id=\"modAdminForm\">\n	<table>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n				<span>Full Name</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name='employeName' id='employeName' type='text' size=\"20\" maxlength=\"30\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.employe_name : stack1), depth0))
    + "\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr> \n\n		<tr>\n			<td>\n				<span>Telephone</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name=\"mobilePhone\" id=\"mobilePhone\" type=\"text\" maxlength=\"30\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.mobilephone : stack1), depth0))
    + "\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		\n		<tr>\n			<td>\n 				<span>E-Mail</span>\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name=\"email\" id=\"email\" type=\"text\" maxlength=\"320\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.email : stack1), depth0))
    + "\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<!-- <b class=\"require_property\">*</b> -->\n 				Hiredate\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" name=\"entryDate\" id=\"entryDate\" type=\"text\" size=\"20\" maxlength=\"10\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.entrytime : stack1), depth0))
    + "\" title=\"\" placeholder=\"eg. 2014-08-12\"/>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n\n	<table style=\"margin-top: 20px;\">\n\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n 				Department/Post\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" id=\"department\" name=\"department\" type=\"text\" readonly=\"readonly\" title=\"\">\n				<input id=\"department_id\" name=\"department_id\" type=\"hidden\">\n				\n		   		<div id=\"department_menuContent\" class=\"content_add_account_select\">\n					<ul id=\"department_tree\" class=\"ztree\"></ul>\n				</div>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		\n		<tr id=\"customer_tr\" name=\"customer_tr\">\n			<td>\n			<b class=\"require_property\">*</b>\n 				Customer\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"customer_id\" name=\"customer_id\" style=\"width:242px;\" >\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		\n		<tr id=\"carrier_tr\" name=\"carrier_tr\">\n			<td>\n			<b class=\"require_property\">*</b>\n 				Carrier\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"carrier_id\" name=\"carrier_id\" style=\"width:242px;\" >\n                    <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.carriers : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n\n		<tr>\n			<td>\n 				Office Location\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<input class=\"input_text_password_select\" id=\"officeLocation\" name=\"officeLocation\" type=\"text\" readonly=\"readonly\">\n				<input id=\"officeLocation_id\" name=\"officeLocation_id\" type=\"hidden\">\n				\n		   		<div id=\"officeLocation_menuContent\" class=\"content_add_account_select\">\n					<ul id=\"officeLocation_tree\" class=\"ztree\"></ul>\n				</div>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n 				Warehouse Type\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"warehouseType\" name=\"warehouseType\" style=\"width:242px;\" >\n					<option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.waretype : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		<tr>\n			<td>\n				<b class=\"require_property\">*</b>\n 				Warehouse\n			</td>\n			<td>&nbsp;</td>\n			<td>\n				<select id=\"warehouse\" name=\"warehouse\" style=\"width:242px;\" >\n					<option value=\"\"></option>\n                </select>\n\n				<input id=\"warehouse_id\" name=\"warehouse_id\" type=\"hidden\">\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n		\n		<tr style=\"line-height:30px;\">\n			<td>\n 				Area\n			</td>\n			<td>&nbsp;</td>\n			<td>\n                <select id=\"area\" style=\"width:242px;\" multiple=\"multiple\">\n                	<option value=\"\"></option>\n            	</select>\n				<input id=\"area_id\" name=\"area_id\" type=\"hidden\">\n			</td>\n			<td class=\"status validator_style\">&nbsp;</td>\n		</tr>\n	</table>\n</form>\n</div>";
},"useData":true});
templates['mod_fingerprint'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div style=\"width:180px;text-align:center;\">\n	<div style=\"margin-bottom:10px;\">\n		Please enter your fingerprints within 10 seconds\n	</div>\n    <form id=\"fingerprintFrom\">\n        <object id=\"ZAZFingerActivex\" type=\"application/x-itst-activex\"\n            clsid=\"{87772C8D-3C8C-4E55-A886-5BA5DA384424}\">\n        </object>\n        <object id=\"Splugin\" type=\"application/x-text-plugin-demo\" width=0 height=0></object>\n    </form>\n</div> \n";
},"useData":true});
templates['mod_password'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\n<form id=\"modifyPasswordForm\" >\n<table>\n	<tr>\n		<td>\n			<b class=\"require_property\">*</b>\n			<span>Password</span>\n		</td>\n		<td>&nbsp;</td>\n	</tr> \n	<tr>\n		<td>\n			<input class=\"input_text_password_select\" name='pwd' id='pwd' type=\"password\" maxlength=\"12\"/>\n		</td>\n		<td class=\"status validator_style\">&nbsp;</td>\n	</tr>\n	<tr>\n		<td>\n			<b class=\"require_property\">*</b>\n			<span>Confirm Password</span>\n		</td>\n		<td>&nbsp;</td>\n	</tr>\n	<tr>\n		<td>\n			<input class=\"input_text_password_select\" name='confirmPwd' id='confirmPwd' type='password' maxlength=\"12\"/>\n		</td>\n		<td class=\"status validator_style\">&nbsp;</td>\n	</tr>\n</table>\n</form>\n</div>";
},"useData":true});
templates['mod_token'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\n<form id=\"modifyTokenForm\" >\n<table>\n	<tr>\n		<td>\n			<b class=\"require_property\">*</b>\n			<span>SN</span>\n		</td>\n		<td>&nbsp;</td>\n	</tr>\n	<tr>\n		<td> \n			<input class=\"input_text_password_select\" name='tokenId' id='tokenId' type='text'/>\n		</td>\n		<td class=\"status validator_style\">&nbsp;</td>\n	</tr>\n	<tr>\n		<td>\n			<b class=\"require_property\">*</b>\n			<span>Key</span>\n		</td>\n		<td>&nbsp;</td>\n	</tr>\n	<tr>\n		<td>\n			<input class=\"input_text_password_select\" name='tokenKey' id='tokenKey' type='text'>\n		</td>\n		<td class=\"status validator_style\">&nbsp;</td>\n	</tr>\n</table>\n</form>\n</div>";
},"useData":true});
templates['search_advanced_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <option value=\""
    + alias2(alias1((depth0 != null ? depth0.adgid : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div>\n    <table style=\"width:85%\">\n    \n        <tr>\n            <td valign=\"top\">\n                <div>\n                    <div>\n                        <select id=\"selectDeptment\" style=\"width:260px;\">\n                            <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.deptments : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </select>\n                    </div> \n                    <div style=\"margin-top: 10px;\">\n                        <select id=\"selectPost\" style=\"width:260px;\">\n                            <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.posts : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </select>\n                    </div>\n                </div>\n            </td>\n            <td valign=\"top\">\n                <div>\n                    <div>\n                        <select id=\"selectWarehouse\" style=\"width:260px;\">\n                            <option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.warehouses : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </select>\n                    </div>\n                    <div style=\"margin-top: 10px;\">\n                        <select id=\"selectArea\" style=\"width:260px;\">\n                            <option value=\"\"></option>\n                        </select>\n                    </div>\n                </div>\n            </td>\n            <td valign=\"top\">\n                <div>\n                    <div STYLE=\"position: relative\">\n\n                        <input class=\"input_text_password_select\" id=\"selectOfficeLocation\" name=\"officeLocation\" type=\"text\" readonly=\"readonly\">\n                        <input id=\"selectLocation\" name=\"selectLocation\" type=\"hidden\">\n\n                        <div id=\"select_officeLocation_menuContent\" class=\"content_add_account_select\">\n                            <ul id=\"select_officeLocation_tree\" class=\"ztree\"></ul>\n                        </div>\n\n                    </div>\n                </div>\n            </td>\n            <td valign=\"top\">\n                <div class=\"advanced_search_checked\">\n                    <div>\n                        <input id=\"unFingerprintFlag\" type=\"checkbox\">\n                        <span id=\"unFingerprintFlagTxt\">\n                            No Fingerprint\n                        </span>\n                    </div>\n                    <div>\n                        <input id=\"unTokenFlag\" type=\"checkbox\">\n                        <span id=\"unTokenFlagTxt\">\n                            No Token\n                        </span>\n                    </div>\n                    <div>\n                        <input id=\"unShieldFlag\" type=\"checkbox\">\n                        <span id=\"unShieldFlagTxt\">\n                            Locked\n                        </span>\n                    </div>\n                </div>\n            </td>\n            <td valign=\"top\">\n                <div class=\"advanced_search_checked\">\n                    <div>\n                        <input id=\"unPermissionFlag\" type=\"checkbox\">\n                        <span id=\"unPermissionFlagTxt\">\n                            No Permissions\n                        </span>\n                    </div>\n                    <!-- <div>\n                        <input id=\"unTitleFlag\" type=\"checkbox\">\n                        <span id=\"unTitleFlagTxt\">\n                            No Title\n                        </span>\n                    </div> -->\n                    <div>\n                        <input id=\"unUploadPhotoFlag\" type=\"checkbox\">\n                        <span id=\"unUploadPhotoFlagTxt\">\n                            No Images\n                        </span>\n                    </div>\n                </div>\n            </td>\n        </tr>\n    </table>\n</div>\n";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "        <tr data-adid=\""
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "\" class=\"content_account_text\">\n        \n            <td class=\"content_account_text_td1\">\n                <fieldset class=\"set\" style=\"border: 2px solid green;\">\n                <legend>\n                    <span class=\"title\" style=\"font-weight: bold;\">"
    + alias2(alias1((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "</span>\n                </legend>\n\n                    <table style=\"width:100%;height:100%\">\n                            <tr>\n                                <td style=\"width:100%;\">\n\n                    <table style=\"border-spacing: 0px;\">\n                        <tr>\n                            <td>\n                                Account:&nbsp;\n                            </td>\n                            <td style=\"padding-left:5px;position: relative;height:20px;\">\n                                "
    + alias2(alias1((depth0 != null ? depth0.account : depth0), depth0))
    + "\n                                 <span style=\"display: inline-block;right: 0px;position: absolute\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.llock : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                                </span>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Token:&nbsp;\n                            </td>\n                            <td style=\"padding-left:5px;height:20px;\">\n                                "
    + alias2(alias1((depth0 != null ? depth0.register_token : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Phone:&nbsp;\n                            </td>\n                            <td style=\"padding-left:5px;height:20px;\">\n                                "
    + alias2(alias1((depth0 != null ? depth0.mobilephone : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                E-Mail:&nbsp;\n                            </td>\n                            <td style=\"padding-left:5px;height:20px;\">\n                                "
    + alias2(alias1((depth0 != null ? depth0.email : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                    </table>\n                                </td>\n                            </tr>\n                        </table>\n                    </fieldset>\n            </td>\n            <td class=\"content_account_text_td1\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.warearea : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </td>\n            <td class=\"content_account_text_td1\">\n\n                <fieldset class=\"set\" style=\"border: 1px #ff6600 solid;\">\n                    <legend>\n                        <span>\n                            Department / Post\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.deptpost : depth0)) != null ? stack1.length : stack1),"4",">",{"name":"xifCond","hash":{},"fn":this.program(18, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </span>\n                    </legend>\n\n                    <table style=\"width:100%;height:100%\">\n                        <tr>\n                        <td style=\"width:95%;\" valign=\"center\">\n                        <table style=\"border-spacing: 0px;\">\n                        \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.deptpost : depth0),{"name":"each","hash":{},"fn":this.program(20, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.deptpost : depth0)) != null ? stack1.length : stack1),"2","<",{"name":"xifCond","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                        </table>\n                        </td>\n                        </tr>\n                    </table>\n                </fieldset>\n            </td>\n            <!-- <td class=\"content_account_text_td1\">\n\n                <fieldset class=\"set\" style=\"border: 1px #999 solid;\">\n                    <legend>\n                        <span name=\"displayTitle\" >\n                            \n                            <span style=\"text-decoration:underline;cursor:pointer;\">\n                                Linked Title\n                            </span>\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.titles : depth0)) != null ? stack1.length : stack1),"4",">",{"name":"xifCond","hash":{},"fn":this.program(25, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </span>\n                    </legend>\n\n                    <table style=\"width:100%;height:100%\">\n                        <tr>\n                        <td style=\"width:85%;padding-left: 5px;\" valign=\"center\">\n                        <table style=\"border-spacing: 0px;\">\n                        \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(27, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.titles : depth0)) != null ? stack1.length : stack1),"2","<",{"name":"xifCond","hash":{},"fn":this.program(32, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                        </table>\n                        </td>\n                        </tr>\n                    </table>\n\n                </fieldset>\n            </td> -->\n            <td class=\"content_account_text_td1\">\n\n                <fieldset class=\"set\" style=\"border: 1px #ff6600 solid;\">\n\n                    <legend>\n                        <span>\n                            Office Location\n                        </span>\n                    </legend>\n\n                    <table class=\"detailed_location\">\n                        <tr>\n                            <td>\n                                Office:\n                            </td>\n                            <td>\n                                &nbsp;\n                                "
    + alias2(alias1((depth0 != null ? depth0.office_all_name : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>Area:</td>\n                            <td>\n                                &nbsp;\n                                "
    + alias2(alias1((depth0 != null ? depth0.nation_name : depth0), depth0))
    + "\n                                "
    + alias2(alias1((depth0 != null ? depth0.provinces_name : depth0), depth0))
    + "\n                                "
    + alias2(alias1((depth0 != null ? depth0.city : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>Address:</td>\n                            <td>\n                                &nbsp;\n                                "
    + alias2(alias1((depth0 != null ? depth0.house_number : depth0), depth0))
    + "\n                                "
    + alias2(alias1((depth0 != null ? depth0.street : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>Zip Code:</td>\n                            <td>\n                                &nbsp;\n                                "
    + alias2(alias1((depth0 != null ? depth0.zip_code : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                    </table>\n                </fieldset>\n            </td>\n            <!-- <td class=\"content_account_text_td2\">\n                <table>\n                    <tr>\n                        <td>\n                            Hiredate:\n                        </td>\n                        <td>\n                            &nbsp;"
    + alias2(alias1((depth0 != null ? depth0.entrytime : depth0), depth0))
    + "\n                        </td>\n                    </tr>\n                    <tr>\n                        <td>\n                            Last Login Date:\n                        </td>\n                        <td>\n                            &nbsp;"
    + alias2(alias1((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "\n                        </td>\n                    </tr>\n                </table>\n            </td> -->\n        </tr>\n\n        <tr data-adid=\""
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "\" class=\"content_account_button\">\n            <td colspan=\"5\">\n\n            <!--    <input name=\"manageTitle\" type=\"button\" class=\"lighter\" value=\"Title\" /> -->\n                <input name=\"managePermission\" type=\"button\" class=\"lighter\" value=\"Authority\" />\n                <input name=\"modifyAdmin\" type=\"button\" class=\"lighter\" value=\"Edit\" />\n                <input name=\"managePhoto\" type=\"button\" class=\"lighter\" value=\"Images\" />\n\n                <!-- <input name=\"modifyFingerprint\" type=\"button\" class=\"lighter\" value=\"Fingerprint\" /> -->\n                <input name=\"modifyPassword\" type=\"button\" class=\"lighter\" value=\"Password\" />\n                <input name=\"modifyToken\" type=\"button\" class=\"lighter\" value=\"Token\" />\n                <input name=\"shieldAdmin\" type=\"button\" class=\"lighter\" value=\""
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.llock : depth0),{"name":"if","hash":{},"fn":this.program(34, data, 0, blockParams, depths),"inverse":this.program(36, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\" />\n                <!-- <input name=\"deleteAdmin\" type=\"button\" class=\"lighter\" value=\"Delete\" /> -->\n            </td>\n        </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "                                        <img src=\"image/lockadmin.gif\">&nbsp;&nbsp;\n";
},"5":function(depth0,helpers,partials,data) {
    return "                                        &nbsp;\n";
},"7":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=helpers.helperMissing;

  return "                    <fieldset class=\"set\" style=\"border: 1px #999 solid;\">\n                    <legend>\n                        <span>\n                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.warename : depth0), depth0))
    + " \n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.area_name_array : depth0)) != null ? stack1.length : stack1),"4",">",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </span>\n                    </legend>\n                        <table style=\"width:100%;height:100%\">\n                            <tr>\n                                <td style=\"width:100%;padding-left:10px;\" valign=\"top\">\n                                    <table style=\"border-spacing: 0px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.area_name_array : depth0),{"name":"each","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.area_name_array : depth0)) != null ? stack1.length : stack1),"2","<",{"name":"xifCond","hash":{},"fn":this.program(15, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                    </table>\n                                </td>\n                            </tr>\n                        </table>\n                    </fieldset>\n";
},"8":function(depth0,helpers,partials,data) {
    return "                            &nbsp;|&nbsp;\n                                <span name=\"displayArea\" data-flag=\"more\" style=\"text-decoration:underline;cursor:pointer;color:#00F\">More...\n                                </span>\n                            \n";
},"10":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),"3",">",{"name":"xifCond","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.program(13, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"11":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "\n                                                <tr name=\"displayArea_"
    + alias2(alias1((depths[3] != null ? depths[3].adid : depths[3]), depth0))
    + "\" style=\"display:none\">\n                                                    <td style=\"text-align:left;height:20px;\">\n                                                        "
    + alias2(alias1((depth0 != null ? depth0.area : depth0), depth0))
    + " \n                                                    </td>\n                                                </tr>\n";
},"13":function(depth0,helpers,partials,data) {
    return "                                                <tr>\n                                                    <td style=\"text-align:left;height:20px;\">\n                                                        "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.area : depth0), depth0))
    + "\n                                                    </td>\n                                                </tr>\n";
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.times || (depth0 && depth0.times) || helpers.helperMissing).call(depth0,"2-this.area_name_array.length",{"name":"times","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"16":function(depth0,helpers,partials,data) {
    return "                                            <tr>\n                                                <td style=\"text-align:left;height:20px;\">\n                                                    &nbsp;\n                                                </td>\n                                            </tr>\n";
},"18":function(depth0,helpers,partials,data) {
    return "                                &nbsp;|&nbsp;\n                                <span name=\"displayDept\" data-flag=\"more\" style=\"text-decoration:underline;cursor:pointer;color:#00F\">More...</span>\n";
},"20":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <tr>\n                                <td style=\"text-align:left;\">\n                                <table style=\"border-spacing: 0px;\">\n                                    <tr>\n                                    <td style=\"text-align:right;height:20px;\">"
    + alias2(alias1((depth0 != null ? depth0.deptname : depth0), depth0))
    + "</td>\n                                    <td style=\"text-align:left;height:20px;\">&nbsp;»&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.postname_str : depth0), depth0))
    + "</td>\n                                    </tr>\n                                </table>\n                                </td>\n                            </tr>\n";
},"22":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.times || (depth0 && depth0.times) || helpers.helperMissing).call(depth0,"2-this.deptpost.length",{"name":"times","hash":{},"fn":this.program(23, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"23":function(depth0,helpers,partials,data) {
    return "                            <tr>\n                                <td style=\"text-align:left;height:20px;\">\n                                    &nbsp;\n                                </td>\n                            </tr>\n";
},"25":function(depth0,helpers,partials,data) {
    return "                                &nbsp;|&nbsp;\n                                <span style=\"text-decoration:underline;cursor:pointer;color:#00F\">      More...\n                                </span>\n";
},"27":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),"3",">",{"name":"xifCond","hash":{},"fn":this.program(28, data, 0, blockParams, depths),"inverse":this.program(30, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "                                <td style=\"text-align:left;\">\n                                <table style=\"border-spacing: 0px;\">\n                                    <tr>\n                                    <td style=\"text-align:left;height:20px;\">&nbsp;"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</td>\n                                    </tr>\n                                </table>\n                                </td>\n                            </tr>\n";
},"28":function(depth0,helpers,partials,data,blockParams,depths) {
    return "                                <tr name=\"displayTitle_"
    + this.escapeExpression(this.lambda((depths[2] != null ? depths[2].adid : depths[2]), depth0))
    + "\" style=\"display:none\">\n";
},"30":function(depth0,helpers,partials,data) {
    return "                                <tr>\n";
},"32":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.times || (depth0 && depth0.times) || helpers.helperMissing).call(depth0,"2-this.titles.length",{"name":"times","hash":{},"fn":this.program(23, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"34":function(depth0,helpers,partials,data) {
    return "UnLock";
},"36":function(depth0,helpers,partials,data) {
    return "Lock";
},"38":function(depth0,helpers,partials,data) {
    return "        <tr>\n            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"title_account\">\n    <table>\n        <tr>\n            <th>\n                <div>\n                    <span>\n                        Account Information\n                    </span>\n                    <span> \n                        <!-- <button class=\"normal\" id=\"exportTitle\">Export Title</button> -->\n                        &nbsp;&nbsp;\n                        <button class=\"normal\" id=\"addAdmin\">Add Account</button>\n                    </span>\n                </div> \n            </th>\n        </tr>\n    </table>\n</div>\n<div style=\"margin-left: 1px;\">\n    <table cellspacing=\"0\" width=\"100%\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.program(38, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "    </table>\n</div>\n<div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n    <ul id=\"pagination\" class=\"clearfix pagebox\">\n    </ul>\n</div>";
},"useData":true,"useDepths":true});
templates['search_result_templet1'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        <tr name=\"basicInfo\" data-adid=\""
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "\">\n            <td>\n                "
    + alias2((helpers.math || (depth0 && depth0.math) || helpers.helperMissing).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + "\n            </td>\n            <td>\n                "
    + alias2(alias1((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "\n            </td>\n            <td>\n                "
    + alias2(alias1((depth0 != null ? depth0.account : depth0), depth0))
    + "\n            </td>\n            <td>\n                "
    + alias2(alias1((depth0 != null ? depth0.mobilephone : depth0), depth0))
    + "\n            </td>\n            <td>\n                "
    + alias2(alias1((depth0 != null ? depth0.email : depth0), depth0))
    + "\n            </td>\n            <td>\n                "
    + alias2(alias1((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "\n            </td>\n        </tr>\n        <tr>\n            <td colspan=\"6\">\n                <div style=\"margin: 10px;display:none;\" name=\"detailInfo_"
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "\">\n                    <table style=\"width:100%\">\n                        <tr>\n                            <td>\n                                Full Name\n                            </td>\n                            <td>\n                                "
    + alias2(alias1((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Account\n                            </td>\n                            <td>\n                                "
    + alias2(alias1((depth0 != null ? depth0.account : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Mobilephone\n                            </td>\n                            <td>\n                                "
    + alias2(alias1((depth0 != null ? depth0.mobilephone : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Email\n                            </td>\n                            <td>\n                                "
    + alias2(alias1((depth0 != null ? depth0.email : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Token ID\n                            </td>\n                            <td>\n                                "
    + alias2(alias1((depth0 != null ? depth0.register_token : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                Last Login Date\n                            </td>\n                            <td>\n                                "
    + alias2(alias1((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "\n                            </td>\n                        </tr>\n                    </table>\n                </div>\n            </td>\n        </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div style=\"margin-left: 1px;margin-top: 10px;\">\n\n<style type=\"text/css\">\n\n.table {\n    width: 100%;\n    max-width: 100%;\n    margin-bottom: 20px;\n    border-spacing: 0;\n    border-collapse: collapse;\n    border: 1px solid #ddd; \n}\n\n.table thead tr{\n\n    height: 35px;\n}\n\n.table tbody tr{\n\n    cursor: pointer;\n    height: 35px;\n}\n\n.table tr:nth-child(2n) {\n\n    background: #f9f9f9;\n}\n\n.table tbody tr:hover{\n\n    background: #E6F3C5;\n}\n\n.table td{\n\n    border-top: 1px solid #ddd;\n    border: 1px solid #ddd;\n    padding:5px;\n}\n\n.table th{\n\n    border-top: 1px solid #ddd;\n    border: 1px solid #ddd;\n    padding:5px;\n}\n\n.bg-info {\n    background-color: #e5e5e5;\n}\n</style>\n\n    <table class=\"table\">\n        <thead>\n            <tr class=\"bg-info\">\n                <th>\n                    Index\n                </th>\n                <th>\n                    Full Name\n                </th>\n                <th>\n                    Account\n                </th>\n                <th>\n                    Mobilephone\n                </th>\n                <th>\n                    Email\n                </th>\n                <th>\n                    Last Login Date\n                </th>\n            </tr>\n        </thead>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </table>\n    \n</div>\n\n<div class=\"pagination\">\n<table>\n	<tr>\n        <th>\n            Pages："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\n            &nbsp;&nbsp;\n           	Total："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "\n            &nbsp;&nbsp;\n            <button class=\"normal\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >First</button> \n            <button class=\"normal\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >Previous</button>\n            <button class=\"normal\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >Next</button>\n            <button class=\"normal\" data-pageno=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >Last</button>\n        </th>\n    </tr>\n</table>\n</div>";
},"useData":true});
templates['search_templet'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n    <div>\n        <div class=\"eso_search_parent\">\n            <div class=\"eso_search_icon\">\n                <a>\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\n                </a>\n            </div>\n        </div>\n        <div class=\"eso_search_input\">\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\n        </div> \n    </div>\n</div>";
},"useData":true});
templates['upload_photo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"photo_basic_layout\">\n	<div class=\"photo_basic_layout_select\">\n		<div id=\"selectFile\" style=\"position:relative;height:20px;width:238px;border: 1px solid #999999;display: inline-block;padding: 2px;background-color: #fff;\">\n			<input id=\"_upload_photo\" title=\"No Choose File\" name=\"_upload_photo\" type=\"file\" accept=\"image/*\" /  style=\"position:absolute;opacity: 0;height:20px;width:238px;top:-1px;left:-1px;\" hidefocus>\n			<button>Select File...&nbsp;</button>\n			<input id=\"selectFile_text\" readonly style=\"color:#3e3e3e;border:none\" placeholder=\"No Choose File\"/>\n		</div>\n	</div> \n\n	<div class=\"photo_basic_layout_context\">\n	\n		<img src=\"image/upload_image_t.gif\" id=\"jcrop_target\" style=\"width:541px;height:472px;\">\n		 \n		<input type=\"hidden\" id=\"jcrop_has_img_flag\" value=\"0\"/>\n	</div>\n\n    <form id=\"crop_image_coordinate\">\n\n        <input type=\"hidden\" id=\"adid\" name=\"adid\" />\n        <input type=\"hidden\" id=\"flag\" name=\"flag\" />\n\n        <input type=\"hidden\" id=\"x\" name=\"x\" />\n        <input type=\"hidden\" id=\"y\" name=\"y\" />\n        <input type=\"hidden\" id=\"w\" name=\"w\" />\n        <input type=\"hidden\" id=\"h\" name=\"h\" />\n\n        <input type=\"hidden\" id=\"bigImage\" name=\"bigImage\" value=\"\"/>\n\n    </form>\n</div>";
},"useData":true});
templates['validate_title'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.escapeExpression, alias2=this.lambda;

  return "		<tr>\n			<td style=\"width:5%\">\n				"
    + alias1((helpers.math || (depth0 && depth0.math) || helpers.helperMissing).call(depth0,(data && data.index),"+","1",{"name":"math","hash":{},"data":data}))
    + "\n			</td>\n			<td style=\"width:25%\">\n				"
    + alias1(alias2((depth0 != null ? depth0.account : depth0), depth0))
    + "\n			</td>\n			<td style=\"width:25%\">\n				"
    + alias1(alias2((depth0 != null ? depth0.title : depth0), depth0))
    + "\n			</td>\n			<td style=\"width:10%\">\n				<img style=\"width:20px;height:20px;\" src=\"image/"
    + alias1(alias2((depth0 != null ? depth0.status : depth0), depth0))
    + ".png\">\n			</td>\n			<td style=\"width:35%\">\n				"
    + alias1(alias2((depth0 != null ? depth0.error : depth0), depth0))
    + "\n			</td>\n		</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"show-import-title\">\n\n	<table>\n		<tr>\n			<td style=\"width:5%\">\n				No\n			</td>\n			<td style=\"width:25%\">\n				Account\n			</td>\n			<td style=\"width:25%\">\n				Title\n			</td>\n			<td style=\"width:10%\">\n				Status\n			</td>\n			<td style=\"width:35%\">\n				Error Info\n			</td>\n		</tr>\n	</table> \n\n	<table>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titleList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</table>\n</div>\n";
},"useData":true});
return templates;
});