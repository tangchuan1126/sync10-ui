﻿"use strict";
define([
    "../config",
    "jquery",
    "backbone"
], function (page_config, $, Backbone) {
    var OrderModel = Backbone.Model.extend({
        idattribute: "id",
        defaults: {
            company_id: "",
            ordered_date: "",
            status: "",
            ship_to_address1: "",
            date_created: "",
            order_no: "",
            carrier_id: "",
            user_created: "",
            ship_to_state: "",
            ship_to_name: "",
            po_no: "",
            user_updated: "",
            date_updated: "",
            ship_to_city: "",
            load_no: "",
            reference_no: "",
            customer_id:""
        }
    });

    var LoadModel = Backbone.Model.extend({
        url: page_config.addLoadUrl.url,
        idattribute: "id",
        defaults: {
            loadNo: "",
            orderList: []
        }
    });

    var OrderCollection = Backbone.Collection.extend({
        url: page_config.orderUrl.url,
        model: OrderModel
    });

    return {
        OrderModel: OrderModel,
        OrderCollection: OrderCollection,
        LoadModel: LoadModel

    }
});