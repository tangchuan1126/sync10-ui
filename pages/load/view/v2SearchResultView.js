﻿"use strict";
define(["jquery",
    "backbone",
    "bootstrap.datetimepicker",
    'handlebars',
    "../templates/templates.amd",
    "compondBox",
    "oso.lib/table/js/table",
    "art_Dialog/dialog-plus",
    "../model/loadModel",
    "../config",
	"immybox",
    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
	"dateUtil",
    "require_css!bootstrap-css/bootstrap.min",
    "require_css!bootstrap.datetimepicker-css",
    "require_css!oso.lib/immybox-master/immybox.css",,
    "require_css!Font-Awesome"
    ],
    function ($, Backbone, datetimepicker, HandleBars, templates, CompondBox, Table, Dialog, Model, config,immybox, AsynLoadQueryTree,DateUtil) {
    return Backbone.View.extend({
     //   defaultStore : "1000005",
        el: null,
        template: templates.loadDetail,
        initialize: function (options) {
            var v = this;
            if (options && options.adminSesion) v.defaultStore = options.adminSesion.ps_id;
         //   console.log(v.defaultStore);
            $(function () {
                var loadid = v.GetRequest("loadid");
                var loadtype = v.GetRequest("loadtype");
                var orderno = v.GetRequest("orderno");

                var LoadCompondBox = new CompondBox({
                    content: templates.loadDetail().trim(),
                    title: "Load Info",
                    container: "#loadInfoContainer",
                    isExpand: false
                });
				$("#btnSaveInfo").on("click", function () { v.SaveInfo(); });
               

/*                var CarrierTree = new AsynLoadQueryTree({
                    renderTo: "#addCarrier",
                    dataUrl: config.carrierUrl.url,
                    scrollH: 400,//多高后出现滚动条
                    Async: false,//非异步获取树节点，（一次加载完成）
                    multiselect: false,//多选
                    placeholder: "Carrier",//默认input框值
                    Parentclick: true,//开启父节点可以选中
                    ztreebox_Align:"right"
                });
                CarrierTree.render();*/

                

                /*
                    判断添加load还是编辑load
                    如果是添加load则不能关联order
                */
                var addEdit = v.GetRequest("addEdit");
                if(addEdit){

                    $.getJSON(config.carrierUrl.url, function (result) {

                        $.each(result, function(index, obj) {
                            obj.text = obj.value + " - "+ obj.text;
                        });

                        $('#addCarrier').immybox({
                            Pleaseselect:'<i>Clean Select</i>',
                            textname:"text",
                            valuename:"value",
                            choices: result
                        });
                    });

                    v.disabledLoadButton();

                }else{
                    v.removeDisabledLoadButton();
                }
             //   setTimeout(function(){v.initTable()}, 3000 )
             //   v.initTable();

                if (loadid && loadtype) {
                    v.loadid = loadid;
                    //$(".createTitle").html("Edit Load");
                    v.reload(loadid, true);
                } else {
                    //$(".createTitle").html("Create Load");
                }
            });
        },
        initTable : function(){
            var v = this;
            var quickSearch = "Order " + templates.quickSelectOrder().trim();
            var orderCompondBox = new CompondBox({
                content: templates.addLoadTable().trim(),
                title: quickSearch,
                container: "#loadTableContainer",
                isExpand: false,
                isMerger: false
            });
            function formatterDateStringForPs (data, row){
                //console.log(data);
                return data ? DateUtil.getFormartDateStringForPs(data,"MM/dd/yyyy", row.jet_lag || 0) : "" ;
            }
            function poShow(data,row){  //多个po处理
                /*var datas = "";
                var v = this;
                if(data.length>1){  //有1个及以上po
                    for(var i=0;i<data.length;i++){
                        if(i==0){
                            //datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' id='morePo'>Collapse...</span><br/>";
                            datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' class='morePo'>Collapse...</span><br/>";
                        }else{
                            datas +=  "<div class='pohide' style='display:none;'>"+data[i]+"</div>";
                        }
                    }
                }else if(data.length == 1){
                    datas +=  "<span title="+data+">"+data[0]+"</span>";
                }
                //点击缩展更多po
                $("#addTableList table").on("click",".morePo",function(e){  //点击一次执行四次
                    var v = this;
                    if(!this.count0){
                        this.count0 = true;
                        if(!this.count){
                            this.count = true;
                            $(this).siblings(".pohide").slideToggle(800);
                        }else{
                            this.count = false;
                        }
                    }else{
                        this.count0 = false;
                    }
                })
                return datas;*/
                /*
                var datas = "";
                var v = this;
                //console.log(data);
                if(data.length>1){  //有1个及以上po
                    for(var i=0;i<data.length;i++){
                        if(i==0){
                            //datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' id='morePo'>Collapse...</span><br/>";
                            datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' class='morePo'>Collapse...</span><br/>";
                        }else{
                            datas +=  "<div class='pohide' style='display:none;'>"+data[i]+"</div>";
                        }
                    }
                }else if(data.length == 1){
                    datas +=  "<span title="+data+">"+data[0]+"</span>";
                }
                $("td").on("click",".morePo",function(){
                    $(this).siblings(".pohide").slideToggle(800);
                    this.way();
                })*/

                //console.log(data);
                var datas = "";
                var v = this;
                for(var i=0;i<data.length;i++){
                    if(i==0){
                        //datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' id='morePo'>Collapse...</span><br/>";
                        datas +=  "<span title="+data+">"+data[i]+"</span>";
                    }else{
                        datas +=  "<div class='pohide' style='display:none;'>"+data[i]+"</div>";
                    }
                }
                return datas;
            }
            window.top.parentTable = v.parentTable = new Table({
                container: '#addTableList',
                height: 800,
                rownumbers: false,
                localData: [],
                checkbox: true,
                noDataMsg: "No Data",
                groupKey: "connect",
                pagination: false,
                singleSelection: false,
                columns: [
                    { field: 'order_id', title: 'Order NO.', width: 100 },
                    //{ field: 'po', title: 'PO', width: 100 },
                    { field: 'po', title: 'PO', width: 100,formatter:poShow },
                    //{ field: 'company_id', title: 'Hub', width: 90 },  //hyq has changed code (2015-6-10)
                    { field: 'company_name', title: 'Hub', width: 90 },
                    { field: 'total_pallets', title: 'Pallet Qty', width: 120 },
                    { field: 'mabd', title: 'MABD', width: 120, formatter : formatterDateStringForPs},
                    { field: 'freight_carrier', title: 'Freight Type', width: 120 },
                    { field: 'connect', title: 'Ship To' }

                ],
                toolbar: [
                    {
                        text: 'Remove',
                        iconCls: 'glyphicon glyphicon-trash',
                        handler: function () {
                            var rows = v.parentTable.getSelected();
                            if (!rows[0]) {
                                return;
                            }
                            if (confirm("Do you want to delete relevance?")) {
                                //console.log(rows);
                                v.parentTable.deleteLocalData(rows, function (row) {
                                    var obj = [];
                                    var flag = true;
                                    for (var j = 0; j < v.select_data_rows.length; j++) {
                                        for (var i = 0; i < rows.length; i++) {
                                            if (row[i].order_id == v.select_data_rows[j].order_id) {
                                                flag = false;
                                            }
                                        }
                                        if (flag) {
                                            obj.push(v.select_data_rows[j]);
                                        }
                                        else {
                                            flag = true;
                                        }
                                    }
                                    v.select_data_rows = obj;
                                    v.parentTable.reloadCurrent(true);
                                   var orderIds = [];
                                   for(var i=0 ; i<rows.length ; i++){
                                       var row = rows[i];
                                       var order_id= row.order_id;
                                       //console.log(order_id);
                                       orderIds.push(order_id);
                                   }
                                   var loadid = v.GetRequest("loadid");
                                   var param = {
                                       id: loadid||$("#loadId","#details").val(),
                                       orderList: orderIds
                                   }
                                   $.ajax({
                                       type: 'post',dataType : "json", cache : false,  traditional :true,
                                       url: config.delLoadOrder.url,
                                       data : param,
                                       success: function (data) {
                                           v.parentTable.reloadCurrent(true);
                                           v.addsetclose();
                                       },
                                       error: function (e) {
                                           //console.log(e);
                                       }
                                   });
                                 //   window.location.href=window.location.href;

                                });
                            }
                        }
                    }
                ]
            });



v.addLoadEvents();
        },
        changeState:function(countryId,selectedId){   //国家州改变
            var params = {"ccid":countryId};
            var that = this;
            $.ajax({
                type:"get",
                url:config.shipState.url+"?"+$.param(params),
                dataType:"json",
                success:function(json){
                    var rls=[];
                    if(json){
                        for (var i=0;i<json.length;i++) {
                            rls.push({"value":json[i].pro_id,"text":json[i].p_code});
                            
                        }
                    }
                   /* $('#shipState').immybox({   //单独的这个对象无法清空当没有数据时原来的数据被空值覆盖
                        Pleaseselect:'<i>Clean Select</i>',
                        Defaultselect: selectedId,
                        choices: rls
                    });*/
                    if(that.state)
                    {
                        //console.log(that.state[0].Rechoicesdata);
                        that.state[0].Rechoicesdata(rls,selectedId);  //给新对象重现填充数据
                    }else
                    {
                        var stateVal = $('#shipState').immybox({
                            Pleaseselect:'<i>Clean Select</i>',
                            //Defaultselect: selectedId,
                            choices: rls
                        });
                        that.state = stateVal;
                    }
                }
            }) 
        },
		reload: function(loadid,fl) {
			var v = this;
            if(!loadid){
                loadid = 0;
            }
			$.ajax({
                        type: 'post',
                        url: config.getMasterBolByLoadId.url,   //叶天龙要求换掉 new
                    //    url: config.getLoadUrl.url,   //叶天龙要求换掉 old
                        data: 'load_id=' + loadid,
                        success: function (data) {
							if (fl) {
								/*var CarrierTree = new AsynLoadQueryTree({
									renderTo: "#addCarrier",
									dataUrl: config.carrierUrl.url,
									scrollH: 400,//多高后出现滚动条
									Async: false,//非异步获取树节点，（一次加载完成）
									multiselect: false,//多选
								//    selectid:{value:data.CarrierId},
									selectid:data.CarrierId,
									//PostData: { "a": "1", "b": "2" },//异步时请求每个节点时往服务端传值,
									placeholder: "Carrier",//默认input框值
									Parentclick: true,//开启父节点可以选中
									ztreebox_Align:"right"
								});
								CarrierTree.render();*/
                                $.getJSON(config.carrierUrl.url, function (result) {
                                    $.each(result, function(index, obj) {
                                        obj.text = obj.value + " - "+ obj.text;
                                    });
                                    $('#addCarrier').immybox({
                                        Defaultselect: data.CarrierId ,
                                        Pleaseselect:'<i>Clean Select</i>',
                                        textname:"text",
                                        valuename:"value",
                                        choices: result
                                    });
                                });

								try {
									$("#addLoadNo").val(data.LoadNo);
									//console.log(CarrierTree);
									//CarrierTree.val("w");
									v.parentTable.loadData(data.OrderList);
									v.parentTable.reload(true);
									v.select_data_rows = data.OrderList;
								} catch (err) { }
							} else {
								v.parentTable.loadData(data.OrderList);
								v.parentTable.reload(true);
								v.select_data_rows = data.OrderList;
							}
                        },
                        error: function (e) {
                            var d = new Dialog({
                                content: "Load fail!",
                                icon: 'question',
                                lock: true,
                                width: 200,
                                height: 40,
                                title: 'Load fail',
                                cancelValue: 'Close',
                                cancel: function () { }
                            }).show();
                        }
                    });
		},
        render: function () {

            
        },
        addLoadEvents: function () {
            var v = this;
            $(".quick-search").on("click", function (e) { e.stopPropagation(); return false; });
            $("#inputQuickOrder").on('keypress', function (event) {
                if (event.keyCode == "13") {
                    v.addOrder($("#inputQuickOrder").val());
                }
            });

            $("#btnConsol").on("click", function () { v.ConsolOrder(); });  //合并
            $("#btnAddOrder").on("click", function () { v.addOrder(); });
        },
        SaveInfo: function () {
            var v = this;
            if (v.verification()) {
                var count = 0;
                if (v.select_data_rows) {
                    count = v.select_data_rows.length;
                }

                var h = $("<div class='bol-body form-horizontal' role='form'>"
                    + "<div class='form-group'>"
                    + "<label class='col-sm-5 '>Load NO. : </label>"  + ( $("#addLoadNo").val()||"" ) 
                 //   + "<div class='col-sm-5'>"
                 //   + "    <p class='form-control-static'>" + ( $("#addLoadNo").val()||"" ) + "</p>"
                  //  + "</div>"
                    + "</div>"
                    + "<div class='form-group'>"
                    + "<label class='col-sm-5 '>SCAC : </label>" +( $("#addCarrier").val()||"" )
               //     + "<div class='col-sm-5'>"
                //    + "    <p class='form-control-static'>" +( $("#addCarrier").val()||"" ) + "</p>"
                //    + "</div>"
                    + "</div>"
                    + "</div>");

                var d = Dialog({
                    title: "Confirm",
                    width: 300,
                    height: 120,
                    lock: true,
                    opacity: 0.6,
                    content: h,
                    okValue: "Save",
                    ok: function () {
                        v.save();
                    },
                    cancelValue: 'Cancel',
                    cancel: function () {
                    }
                });
                d.reset();
                d.showModal();
            }
        },
        addOrder: function (poNo) {
            var v = this;
            var h = $(templates.addAllOrderLoadTable().trim());
            var d = Dialog({
                title: "Relevance Order",
                width: 700,
                height: 400,
                lock: true,
                opacity: 0.6,
				Dragset: false,
                content: h,
                cancelValue: 'Confirm',
                cancel: function () {
                }
            });
            d.reset();
            d.showModal();

            $("#searchOrderPoNo").val(poNo);
            v.init();
            v.getOrderList({ "company_id" : v.defaultStore });

            $("#btnOrderFilter").on("click", function () {
                v.getOrderList();
            });

        },

        /*
            合并order
        */
        ConsolOrder: function (poNo) {
            this.state = null;  //清空immybox对象
            var v = this;
            var rows = v.parentTable.getSelected();
          //  console.log(rows);
            if(rows.length<1){
                alert("Please select order!");
                return;
            }
            //console.log(rows);
            //console.log(rows.length);
            for(var i = 0 ; i< rows.length ;i++)
            {
                var row0 = rows[0];
                var row = rows[i];


                var row0_freight_carrier = row0.freight_carrier;
                var row_freight_carrier = row.freight_carrier;

                var row0_customer_id = row0.customer_id;
                var row_customer_id = row.customer_id;

                if(!row_freight_carrier ){
                    alert("Freight Type not empty!");

                    i=rows.length+4;
                    return;
                }


                if(row_freight_carrier.toLowerCase() != "consol"){
                    alert("Freight Type is \"consol\" to create!");

                    i=rows.length+4;
                    return;
                }


                
                if( (row0_freight_carrier != row_freight_carrier) || (row0_customer_id != row_customer_id) ){
                    alert("Selected freight type difference!");

                    i=rows.length+4;
                    return;
                }

            }
            var oids = [];

            for(var i = 0 ; i< rows.length ;i++)
            {
                var row = rows[i];
                oids.push(row.order_id);
                v.customerId = row.customer_id;   
            }

            var loadid = v.GetRequest("loadid");
			if (!loadid) {
				loadid = $("#loadId").val();
			}
            var h = oids.length>1 ? $(templates.consolMultiOrderDialog().trim()) : $(templates.consolSingleOrderDialog().trim());
            


            var v = this;
            var defaultCountryId = "11036" ; //美国
            v.changeState(defaultCountryId,"");  //州改变方法
            $.ajax({  //调用国家服务
                type:"get",
                url:config.country.url,
                dataType:"json",
                success:function(json){
                    var rls=[];
                    for (var i=0;i<json.length;i++) {
                        rls.push({"value":json[i].CCID,"text":json[i].C_COUNTRY});
                    }
                    $('#country').immybox({
                        Pleaseselect:'<i>Clean Select</i>',
                        Defaultselect:"11036",
                        choices: rls,
                        change:function(e,data){   //切换国家
                           if(data.value){
                              // $("#shipState").attr("data-value","");
                               $("#shipState").val("");
                               v.changeState(data.value,"");
                           }
                        }
                    });
                }
            })

            
            /*
            var defaultCountryId = "";
            if(rows.length == 1){  //选择了一个Order
                if(rows[0].ship_to_county){
                    console.log($.trim(sessionStorage.country));
                    console.log($.trim(sessionStorage.state));
                    defaultCountryId = $.trim(sessionStorage.country);
                }else{
                    defaultCountryId = "11036";
                }
                if(rows[0].ship_to_state){  //有州
                    if(!sessionStorage.state){
                        console.log(1);
                        v.changeState(defaultCountryId,"");
                    }else{
                        //console.log(sessionStorage.state);
                        console.log(2);
                        v.changeState(defaultCountryId,$.trim(sessionStorage.state));
                    }
                }else{console.log(234);
                    v.changeState(defaultCountryId,"");
                }
            }
            $.ajax({  //调用国家服务
                type:"get",
                url:config.country.url,
                dataType:"json",
                success:function(json){
                    var rls=[];
                    for (var i=0;i<json.length;i++) {
                        rls.push({"value":json[i].CCID,"text":json[i].C_COUNTRY});
                    }
                    $('#country').immybox({
                        Pleaseselect:'<i>Clean Select</i>',
                        Defaultselect:defaultCountryId,
                        choices: rls,
                        change:function(e,data){   //切换国家
                           if(data.value){
                               //$("#shipState").attr("data-value","");
                               $("#shipState").val("");
                               v.changeState(data.value,"");
                           }
                        }
                    });
                }
            })
            sessionStorage.country = $("#country").attr("data-value");  //保存选择了国家的ccid
                   sessionStorage.state = $("#shipState").attr("data-value"); //保存选择了州的id*/
            
    
            var d = Dialog({
                title: "Create MBOL",
                width: 600,
                /*height: 90,*/
                height: 255,
                lock: true,
                opacity: 0.6,
                content: h,
                okValue: "Create",
                ok : function(){

                    /*
                        地址address和zipcode不能为穿
                    */
                   if( !v.verificationConsolOrder() ){
                        return false;
                   }

                    $.ajax({
                       type: 'post',async:false,
                       //url: "/Sync10/_load/modMasterBol" ,
                       url: config.modMasterBol.url,
                       data:{
                            cid : v.customerId ,
                            oids:oids, 
                            loadId:loadid, 
                            hub:row0.company_id,   //新增仓库id（2015-06-09）
                            zipCode : $("#zipCode").val(), 
                            address : $("#address").val(),
                            country:$("#country").val(),  //国家
                            state:$("#shipState").val(),  //国家州
                            city:$("#shipCity").val()  //城市
                        },
                       success: function (data) {
                        if(data.code=="200"){
                           v.addsetclose();
                            //刷新
                            //window.location.href=window.location.href;
                           // v.parentTable.reload();
						   v.reload(loadid,false);
                        }
                       },
                       error: function (data) {
                            //console.log(data);
                       }
                    });

                },
                cancelValue: 'Close',
                cancel : function () {
                }
            });
            d.reset();
            d.showModal();
            var $zipCode = $("#zipCode").val(null);
            var $address = $("#address").val(null);

            
         $("#newAddress").click(function(){
             $zipCode.val(null);
             $address.val(null);
             $("#country").val("");
            $("#shipState").val("");
            $("#shipCity").val("");
         });
         

         $("#oldAddress").click(function(){
            $("#country").val(rows[0].ship_to_county);  //国家
            $("#shipState").val(rows[0].ship_to_state); //州
            $("#shipCity").val(rows[0].ship_to_city);  //城市
            //hyq has changed code as follows(2015-6-4)
            //$zipCode.val( rows[0].ship_to_zipcode || "" );
            //$address.val( rows[0].ship_to_address || "" );
            $zipCode.val( rows[0].ship_to_zipcode || "" );
            $address.val( rows[0].ship_to_address || "" );
         });

        },
        getOrderList: function (param) {
            //console.log(1);
            var v = this;
            $("#allTableList").html("");
            function formatterDateStringForPs (data, row){
					//console.log(data);
				return data ? DateUtil.getFormartDateStringForPs(data,"MM/dd/yyyy", row.jet_lag || 0) : "" ;
			}
            //hyq has add code as follows(2015-6-9)
            function poShow(data,row){  //多个po处理
                var datas = "";
                var v = this;
                for(var i=0;i<data.length;i++){
                    if(i==0){
                        //datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' id='morePo'>Collapse...</span><br/>";
                        datas +=  "<span title="+data+">"+data[i]+"</span>";
                    }else{
                        datas +=  "<div class='pohide' style='display:none;'>"+data[i]+"</div>";
                    }
                }
                return datas;
            }

            /*function poShow(data,row){  //多个po处理
                var datas = "";
                var v = this;
                console.log(data);
                if(data.length>1){
                    for(var i=0;i<data.length;i++){
                        if(i==0){
                            datas +=  "<span>"+data[i]+"</span>"+"<span style='color:#008000;Font-weight:bold;padding:10px;cursor:pointer;' id='morePo'>Collapse...</span><br/>";
                            //datas +=  "<span title="+data+">"+data[i]+"</span>";
                        }else{
                            datas +=  "<div class='pohide' style='display:none;'>"+data[i]+"</div>";
                        }
                    }
                }else{
                    datas +=  "<span>"+data[i]+"</span>";
                }
                $("table").on("click",".morePo",function(){alert(23);})
                return datas;
            }*/

            var vaildFreightType=null;
			var table2 = new Table({
                container: '#allTableList',
                height: 150,
                url: config.orderUrl.url,
                queryParams: [$.extend({
                    company_id: $("#searchOrderCompany").attr("data-value") || v.defaultStore,
                    customer_id: $("#searchOrderCustomer").attr("data-value"),
                    req_start: $("#searchOrderBeginRequest").val(),
                    req_end: $("#searchOrderEndRequest").val(),
                    order_no: $("#searchOrderPoNo").val(),
                    checkIds: v.GetSelectID()
                },param) ],
                method: "post",
                groupKey: "connect",
                rownumbers: false,
                checkbox: true,
                singleSelection: false,
                pagination: true,
                pageSize: 50,
                onClickRow:function(rowIndex , rowData , checkbox){
                    //已经关联order的tableList的已经加载数据
                    var upList = v.parentTable.getLocalData();
                //    console.dir(   checkbox.checked    );

                    //console.log(upList);

                    //当前点击行数据的freight_carrier属性
                    var currFreightT = rowData.freight_carrier;



                //   var upFreightT = upList.length>0 ? upList[0].freight_carrier : null;
     //           if(checkbox.checked){
                    if(upList.length>0){

                        //取出一条已经关联的order,然后确保后关联的数据的freight_carrier和之前添加的一致
                       var upFreightT = upList[0].freight_carrier;
                      
                        if(upFreightT != currFreightT){
                        
                            checkbox.checked = false
                            //不允许选择复选框;

                            //去除选择后的样式,模板未被选中
                            var tr_obj=$(checkbox).parents("tr").eq(0);
                            tr_obj.removeClass("info");
                            tr_obj.find(".bg-d9edf7").removeClass("bg-d9edf7");
                        }

                     
                    }else{


                     //   如果vaildFreightType为空说明重新选择
                        vaildFreightType = vaildFreightType || currFreightT;
                        if(  vaildFreightType  !=  currFreightT){
                            //不允许选择复选框
                            checkbox.checked = false;

                            //去除选择后的样式,模板未被选中
                            var tr_obj=$(checkbox).parents("tr").eq(0);
                            tr_obj.removeClass("info");
                            tr_obj.find(".bg-d9edf7").removeClass("bg-d9edf7");
                        }


                        /*
                            如果rows.length为0,说明有可能重新选择,需要重置变量 vaildFreightType
                        */
                        var rows = table2.getSelected();
                        if(!rows.length){
                            vaildFreightType = null;
                        }
                    }
           //     }
                //    console.dir(    currFreightT   );
                      
                //      console.dir(v.parentTable.getLocalData());
                },
                columns: [
                    { field: 'order_id', title: 'Order NO.', width: 100 },
                    //{ field: 'po', title: 'PO', width: 70 },
                    { field: 'po', title: 'PO', width: 100,formatter : poShow },
                    { field: 'company_name', title: 'Hub', width: 50 },
                    { field: 'total_pallets', title: 'Pallet Qty', width: 90 },
                    { field: 'mabd', title: 'MABD', width: 90,formatter : formatterDateStringForPs },
                    { field: 'freight_carrier', title: 'Freight Type', width: 90 },
                    { field: 'connect', title: 'Ship To', width: 110 }
                ],
                toolbar: [
                    {
                        text: 'Add To Load',
                        iconCls: 'add',
                        handler: function () {
                            var rows = table2.getSelected();
                            if (!rows[0]) {
                                return;
                            }

                            if (confirm("Are you sure relevance order?")) {
                                if (v.select_data_rows == undefined || v.select_data_rows == "") {
                                    v.select_data_rows = "";
                                }
                                v.Deduplication(rows);

                                var orderIds = [];
                                for(var i=0 ; i<rows.length ; i++){
                                    var row = rows[i];
                                    var order_id= row.order_id;
                                    orderIds.push(order_id);
                                }
                                var loadid = v.GetRequest("loadid");
                                var param = {
                                    id: loadid||$("#loadId","#details").val(),
                                    orderList: orderIds
                                }
                          //     v.parentTable.reload(true);

                                $.ajax({
                                    type: 'post',dataType : "json", cache : false,  traditional :true,
                                //    url: config.getMasterBolByLoadId.url,
                                    url: "/Sync10/_load/addLoadOrder" ,
                                    data : param,
                                    success: function (data) {
                                        //console.log(v.select_data_rows);
                                        v.parentTable.reload(true);
                                        v.addsetclose();
                                        //console.log(table2);
                                        table2.reload({
                                            queryParams: [{
                                                company_id: $("#searchOrderCompany").attr("data-value"),
                                                customer_id: $("#searchOrderCustomer").attr("data-value"),
                                                req_start: $("#searchOrderBeginRequest").val(),
                                                req_end: $("#searchOrderEndRequest").val(),
                                                order_no: $("#searchOrderPoNo").val(),
                                                checkIds: v.GetSelectID()
                                            }]
                                        });
                                        v.parentTable.loadData(v.select_data_rows);
                                    },
                                    error: function (e) {
                                        //console.log(table2);
                                        //console.log(e);
                                        //两人并发关联同一订单处理（hyq has add code(2015-6-10)）
                                        var obj = [];
                                        var flag = true;
                                        v.parentTable.deleteLocalData(rows, function (row) {
                                            for (var j = 0; j < v.select_data_rows.length; j++) {
                                                //console.log(v.select_data_rows[j].order_id); //原来的
                                                for (var i = 0; i < rows.length; i++) {
                                                    //console.log(row[i].order_id);  //新增的
                                                    if (row[i].order_id == v.select_data_rows[j].order_id) {
                                                        flag = false;
                                                        //obj.pop(v.select_data_rows[j]);
                                                    }
                                                }
                                                if (flag) {
                                                    //obj.pop(v.select_data_rows[j]);  //清除父窗口所有的单子
                                                    obj.push(v.select_data_rows[j]);  //入栈父窗口原有的数据
                                                }
                                                else {
                                                    flag = true;
                                                }
                                            }
                                            v.select_data_rows = obj;
                                            v.parentTable.reloadCurrent(true);
                                        });
                                    }
                                });
                            }
                        }
                    }
                ]
            });
        },
        init: function () {
            var v = this;

            $.getJSON(config.shipFrom.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
                var rls=[];
                for (var i=0;i<json.length;i++) {
                    rls.push({"value":json[i].data,"text":json[i].name});
                }
                $('#searchOrderCompany').immybox({
                    Pleaseselect:'<i>Clean Select</i>',
                    Defaultselect: v.defaultStore,
                    choices: rls
                });
            });
          /*  var fromTree = new AsynLoadQueryTree({
                renderTo: "#searchOrderCompany",
                dataUrl: config.companyUrl.url,
                scrollH: 400,
                multiselect: false,
                Async: true
            });
            fromTree.render();*/



            
			/*
            var fromTree2 = new AsynLoadQueryTree({
                renderTo: "#searchOrderCustomer",
                dataUrl: "./json/shipTo.json",
                scrollH: 400,
                multiselect: false,
                Async: true
            });
            fromTree2.render();
			*/
			$.getJSON(config.customerUrl.url, function (json) {
				$('#searchOrderCustomer').immybox({
					Pleaseselect:'<i>Clean Select</i>',
					choices: json
				});
			});
            $("#searchOrderBeginRequest").datetimepicker({
                format: 'mm/dd/yyyy' ,
                autoclose:1,
                minView: 2,
                bgiconurl:"",
                forceParse: 0 
            }).on('changeDate', function(ev){
                    var startTime = $('#searchOrderEndRequest');
                    startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
                });
            $("#searchOrderEndRequest").datetimepicker({
                format: 'mm/dd/yyyy' , 
                autoclose:1,
                minView: 2,
                bgiconurl:"",
                forceParse: 0 
            }).on('changeDate', function(ev){
                
                    var startTime = $('#searchOrderBeginRequest');
                    startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
                });
        },
        Deduplication: function (rows) {
            var v = this;
            var flag = true;
            var obj = [];

            for (var i = 0; i < v.select_data_rows.length; i++) {
                obj.push(v.select_data_rows[i]);
            }

            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    if (v.select_data_rows[j] == rows[i]) {
                        flag = false;
                    }
                }
                if (flag) {
                    obj.push(rows[i]);
                }
            }
            v.select_data_rows = null;
            v.select_data_rows = obj;
        },
        RemoveRows: function (rows) {
            var v = this;
            var flag = true;
            var obj = [];

            for (var j = 0; j < v.select_data_rows.length; j++) {
                obj.push(v.select_data_rows[j]);
            }

            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    if (v.select_data_rows[j].contains(rows[i])) {
                        flag = false;
                    }
                }
                if (flag) {
                    obj.push(rows[i]);
                }
            }
            v.select_data_rows = null;
            v.select_data_rows = obj;


            //console.log(obj);
        },
        GetSelectID: function () {
            var v = this;
            var ids = new Array();
            if (v.select_data_rows != null && v.select_data_rows != undefined && v.select_data_rows != "") {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    ids.push(v.select_data_rows[j].order_id);
                }
            }
            
            return ids.toString();
        },
        save: function () {
            var v = this;
            var model = new Model.LoadModel();

            model.save({
                id: v.loadid,
                loadNo: encodeURI(  $("#addLoadNo").val() ),
              //  carrierId: $("#addCarrier").attr("data-val"),
				carrierId: $("#addCarrier").attr("data-value"),
                orderList: v.select_data_rows
            },{
                success: function (e) {

                    if (e.changed.result != 0) {


                        var d = new Dialog({
                            content: e.changed.error,
                            icon: 'question',
                            lock: true,
                            width: 200,
                            height: 40,
                            title: 'Save fail',
                            cancelValue: 'Close',
                            cancel: function () { }
                        }).show();
                    } else {
                        $("#loadId","#details").val(e.changed.loadId);
                        var addEdit = v.GetRequest("addEdit");
                        if(addEdit){
                            v.removeDisabledLoadButton();
                        }
                        v.addsetclose();
                        //window.parent.postMessage("", '*');
                    }
                },
                faile: function (e) {
                    var d = new Dialog({
                        content: "Save fail,please try again!",
                        icon: 'question',
                        lock: true,
                        width: 200,
                        height: 40,
                        title: 'Save Fail',
                        cancelValuel: 'Close',
                        cancel: function () { }
                    }).show();
                }
            });

            //console.log(model);

        },
        verification: function () {
            var f = true;
            var $addLoadNo = $("#addLoadNo");
            if ($addLoadNo.val() == "" | $addLoadNo.val() == undefined) {
                if (!$addLoadNo.parent().hasClass("has-feedback")) {
                    $addLoadNo.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $addLoadNo.focus();
                }
                f = false;
            } else {
                $addLoadNo.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            var $addCarrier = $("#addCarrier");
            if ($("#addCarrier").val() == "" | $addCarrier.val() == undefined) {
                if (!$addCarrier.parent().hasClass("has-feedback")) {
                    $addCarrier.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $addCarrier.focus();
                }
                f = false;
            } else {
                $addCarrier.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            return f;
        },

        /*
            地址address和zipcode不能为穿
        */
        verificationConsolOrder: function () {
            var f = true;
            var $addLoadNo = $("#address");
            if ($addLoadNo.val() == "" | $addLoadNo.val() == undefined) {
                if (!$addLoadNo.parent().hasClass("has-feedback")) {
                    $addLoadNo.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $addLoadNo.focus();
                }
                f = false;
            } else {
                $addLoadNo.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }
            var $addCarrier = $("#zipCode");
            if ($addCarrier.val() == "" | $addCarrier.val() == undefined) {
                if (!$addCarrier.parent().hasClass("has-feedback")) {
                    $addCarrier.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $addCarrier.focus();
                }
                f = false;
            } else {
                $addCarrier.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            return f;
        },
        GetRequest: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = window.location.search.substr(1).match(reg);  //匹配目标参数
            if (r != null) return unescape(r[2]);
            return null; //返回参数值
        },

        //当load点击EDIT后提交保存或create mbol后,主页面刷新
        addsetclose:function(){
            var pifrme=$(window.parent.document);
            var slide_header_close=pifrme.find(".slide-header-close");
            slide_header_close.attr("pifrmereset","true");
         //  console.dir(slide_header_close);
        },
        disabledLoadButton :function(isdisabled){
            $("#btnConsol").hide(); 
            $("#btnAddOrder").attr("disabled","disabled"); 
            $("#inputQuickOrder").attr("disabled","disabled"); 
        },
        removeDisabledLoadButton:function(){
            var v = this;
            $("#btnConsol").show(); 
            $("#btnAddOrder").removeAttr("disabled"); 
            $("#inputQuickOrder").removeAttr("disabled"); 
            v.initTable();
        }
    });
});