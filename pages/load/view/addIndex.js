﻿
/**
    *create by cuixiang 20141202
*/
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "bootstrap",
    "nprogress",
    "blockui",
    "../view/v2SearchResultView",
    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
    "../config",


], function ($, Backbone, handlebars, bootstrap, NProgress, blockUI, SearchResultView, AsynLoadQueryTree ,config) {

    NProgress.start();
    //遮罩
    (function () {
        $.blockUI.defaults = {
            css: {
                padding: '8px',
                margin: 0,
                width: '170px',
                top: '45%',
                left: '40%',
                textAlign: 'center',
                color: '#000',
                border: '3px solid #999999',
                backgroundColor: '#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: '0.6'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut: 1000,
            showOverlay: true
        };
    })();

    
    $(document).ajaxStart(function () {
        $.blockUI({ message: '<img src="/Sync10-ui/pages/load/css/img/dy_loading.gif" class="loading" align="absmiddle"/> please wait.' });   //遮罩打开 
    });

    $(document).ajaxError(function () {
        $.blockUI({ message: 'Error，Please refresh!<div><a class="closeBlock">Close</a></div>' });   //遮罩打开 
        $(".closeBlock").click(function () {
            $.unblockUI();
        });
    });

    $(document).ajaxSuccess(function () {
        $.unblockUI();
    });
    $.ajax({
        type: "GET",
        url: config.adminSession.url,
        dataType: "json",
        timeout: 30000,
        error: function(XHR,textStatus,errorThrown) {
            if (textStatus==401) {
                showError('Please login！');
            } else {
                showError('error！');
            }
          NProgress.done();
        },
        success: function(data,textStatus) {
            if (textStatus=="success") {
             //   console.log(data.attributes.adminSesion);
                var SearchResultView1 = new SearchResultView({"adminSesion":data.attributes.adminSesion}).render();
            }
          NProgress.done();
        },
        headers: {
            "X-HTTP-Method-Override": "GET"
        }
    });
  //  var SearchResultView1 = new SearchResultView().render();

    NProgress.done();
}
);