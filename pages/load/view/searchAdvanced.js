"use strict";
define(["jquery", "backbone", "handlebars" , "../templates/templates.amd","../config",

	"require_css!Font-Awesome", "require_css!bootstrap-css/bootstrap.min", "require_css!oso.lib/bootstrap/css/bootstrap_input_icos.css"], 
	function($, Backbone, HandleBars, templates , Config) {
	return Backbone.View.extend({
		template: templates.searchAdvanced,
		events:
		{
			"click #btnLoadFilter": "btnLoadFilter",
			"click #btnReset": "btnReset",
			"blur #searchLoadCompany": "searchLoadCompany"
		},
        initialize: function(opts) {
            //this.listenTo(this, "update-model", this.updateModel);
            var v = this;
            $.extend(v, opts);


         //   console.log(v.psId)
            v.$el.html(v.template);
        },
		render: function() {
			var v = this;

			//	当页面url带有参数 loadno 时,将查询该参数.至少需要在查询之前执行该方法
			v.firstInitParamSearch();
			


			v.initDatetimepicker();
			v.initSearchLoadCompany();
			v.initsearchLoadCustomer();

			v.listView();
		},

		initSearchLoadCompany : function(){
			var v = this;

			$.ajax({
			    type: "GET",
			    url: Config.shipFrom.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()) ,
			    dataType: "json", 
				async:false,
			    error: function(data,textStatus,errorThrown) {
			    	console.log("load/view/searchAdvanced.js:  "+data)
					$('#searchLoadCompany').immybox({
						Pleaseselect:'<i>Clean Select</i>'
					});
			    },
			    success: function(json,textStatus) {

			      	var rls=[];
      				for (var i=0;i<json.length;i++) {
      					rls.push({"value":json[i].data,"text":json[i].name});
      				}
      				$('#searchLoadCompany').immybox({
      					Pleaseselect:'<i>Clean Select</i>',
      					Defaultselect: v.psId ,
      					choices: rls
      				});

			    },
			    headers: {
			        "X-HTTP-Method-Override": "GET"
			    }
			});

/*
			$.getJSON(Config.shipFrom.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].data,"text":json[i].name});
				}
				$('#searchLoadCompany').immybox({
					Pleaseselect:'<i>Clean Select</i>',
					Defaultselect: v.psId ,
					choices: rls
				});
			});
*/
		},
		initsearchLoadCustomer : function(){
			var v = this;
			$.getJSON(Config.customerUrl.url, function (json) {
				$('#searchLoadCustomer').immybox({
					Pleaseselect:'<i>Clean Select</i>',
					choices: json
				});

			});
		},
		btnLoadFilter:function(){
			var v = this;
        	var searchLoadEndMABD = $("#searchLoadEndMABD").val();
        	var searchLoadBeginMABD = $("#searchLoadBeginMABD").val();

			if(searchLoadEndMABD || searchLoadBeginMABD){
				var $searchLoadCompany = $("#searchLoadCompany");
				if(! v.vaildSearch($searchLoadCompany)    ){
					return;
				}
			}

            v.listView();
		},
		btnReset: function(){
			var v = this;
			v.vaildSearch( $("#searchLoadCompany")  , true);

			$("#searchLoadPoNo").val(null);
			$("#searchLoadCustomer").removeAttr("data-value").removeAttr("data-name").val(null);
			$("#searchLoadCompany").removeAttr("data-value").removeAttr("data-name").val(null);
			$("#searchLoadEndMABD").val(null);
			$("#searchLoadBeginMABD").val(null);

			v.listView();
		},

		vaildSearch : function($selectObj , flag){
		//	console.log(  $selectObj );
			if( flag){
				$selectObj.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
				return true;
			}

		//	var $selectObj = $("#address");
			if (  $selectObj.val() == "" | $selectObj.val() == undefined) {
			    if (!$selectObj.parent().hasClass("has-feedback")) {
			        $selectObj.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
			        $selectObj.css({  'width':196,  'border-radius': '0px 4px 4px 0px'   }   );
			        $selectObj.focus();
			    }
			    return false;
			} else {
			   $selectObj.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
			   return true;
			}
		},
		searchLoadCompany:function(){
			var v = this;
	        setTimeout(function(){

	        	var searchLoadEndMABD = $("#searchLoadEndMABD").val();
	        	var searchLoadBeginMABD = $("#searchLoadBeginMABD").val();
	        	var $searchLoadCompany = $("#searchLoadCompany");
				if(searchLoadEndMABD || searchLoadBeginMABD){
					v.vaildSearch( $searchLoadCompany );
				}else{
					v.vaildSearch( $searchLoadCompany , true);
				}

	        },300);

		},
		initDatetimepicker : function(){

			var option = {
		    	format: 'mm/dd/yyyy',
		    	autoclose:1,
		        minView: 2,
       			bgiconurl:"",
		        forceParse: 0 
		    };

		    $("#searchLoadBeginMABD").datetimepicker(option)
		    	.on('changeDate', function(ev){
			    	var startTime = $('#searchLoadEndMABD');
		            startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
	            });

		    $("#searchLoadEndMABD").datetimepicker(option)
		    	.on('changeDate', function(ev){
			    	var startTime = $('#searchLoadBeginMABD');
		            startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
	            });
		},


		//	当页面url带有参数 loadno 时,将查询该参数.至少需要在查询之前执行该方法
		firstInitParamSearch : function(){
			var v = this;
			if(v.urlParam.loadno){
				var loadno = v.urlParam.loadno;
				$("#searchLoadPoNo").val(loadno);
			//	console.log( loadno );
			}
		}






















	});
});