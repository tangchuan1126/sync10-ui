"use strict";
define(["jquery", "backbone", 'handlebars', "../templates/templates.amd"], function($, Backbone, HandleBars, templates) {
	return Backbone.View.extend({
		template: templates.searchSample,
        initialize: function() {
            //this.listenTo(this, "update-model", this.updateModel);
            var v = this;
            v.$el.html(v.template().trim());
        },
		render: function() {
			
		}
	});
});