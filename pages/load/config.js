﻿(function () {
    var configObj = {
        //order信息
        orderUrl: {
            url: "/Sync10/_load/getWmsOrderByComCusPoReq" //"/sync10-ui/pages/load/loaddata.js" //
        },
        //save load
        addLoadUrl: {   
            url: "/Sync10/_load/addLoad"
        },
        //删除load
        delLoadUrl: {
            url: "/Sync10/_load/delLoad"
        },
        loadListUrl: {
           url: "/Sync10/_load/getWmsLoadList"      //"/sync10-ui/pages/load/loaddata.json"// "/Sync10/load/getWmsLoadList?load_no&status&start_time&end_time&page=1&page_size=1"// 
            // url: "/Sync10-ui/pages/load/json/loads.json"
        },
        addAppoiontmentUrl: {   //约车    
            url: "/Sync10-ui/pages/appointment/add.html"
        },
        editLoadUrl: {  //编辑界面
            url: "/Sync10-ui/pages/load/add.html"
        },
        getLoadUrl: {
            url: "/Sync10/_load/getWmsLoadById"  //http://192.168.1.8/Sync10/load/getWmsLoadById?load_id=
        },
        getMasterBolByLoadId : {
            url: "/Sync10/_load/getMasterBolByLoadId",  //getLoadUrl : "/Sync10/_load/getWmsLoadById"
        },
        delLoadOrder : {
            url: "/Sync10/_load/delLoadOrder",  //getLoadUrl : "/Sync10/_load/getWmsLoadById"
        },
        modMasterBol: {
            url: "/Sync10/_load/modMasterBol"
        },
		carrierUrl: {
			//url: "/Sync10-ui/pages/load/json/carrier.json"
            url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllFreight"
        },
        customerUrl: {  //customer json
//            url: "./json/carrier.json"
            url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllCustomerId"
        },
        companyUrl: {   //company json
//            url: "./json/carrier.json"
            url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        },
        shipFrom:
        {
            //url:"./json/shipFrom.json"
            url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        },
        adminSession: {
            url: "/Sync10/_fileserv/redis_session"
        },
        country:{  //addCountry
            url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getAllCountry"
        },
        shipState:{  //addState
            url:"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action"
        },
        megerMload:{
            url:"/Sync10/_load/mergeLoad"
        },
        deleteMload:{
            url:"/Sync10/_load/delMasterLoad"
        }

        //load列表:/Sync10/load/getWmsLoadList?load_no&status&start_time&end_time&page=1&page_size=1
        //添加Load:http://192.168.1.8/Sync10/load/addLoad?data=json
        //查询单个load：http://192.168.1.8/Sync10/load/getWmsLoadById?load_id=
        //编辑Load:http://192.168.1.8/Sync10/load/editLoad?data=json
        //删除Load:http://192.168.1.8/Sync10/load/delLoad?data=json
        //关联Order列表:/Sync10/load/getWmsOrderByComCusPoReq
        //customer json:http://192.168.1.8/Sync10/load/customerList
        //company json：http://192.168.1.8/Sync10/load/companyList
    };
    define(configObj);
}).call(this);