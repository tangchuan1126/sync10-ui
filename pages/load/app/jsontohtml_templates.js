define(["JSONTOHTML","dateUtil"],
function(JSONTOHTML,DateUtil) {
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index) {
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			},
			{
				tag: "tbody",
				html: ""
			}]
		},
		{
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		},
		{
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}"
		}
	};
	var _tbody = {
		head: null,
		int: function(jsons, View_control) {
			var headallobj = View_control.head;
			var footer = View_control.footer;
			_tbody.head = headallobj;
			var trfooterdata = {
				"colspan": headallobj.length,
				"children": footer,
				"trid": ""
			};
			var pageSize = jsons.PAGECTRL.pageSize;
			//console.log(jsons.DATA);
			var __tr = _tbody.TotrJson(jsons.DATA, pageSize);
			var __trhtml = "";
			//console.log(__tr);
			for (var trkey in __tr) {
                
			    var _tritmes = __tr[trkey];


				__trhtml += (JSONTOHTML.transform(_tritmes, _tbody.tr));
				trfooterdata["trid"] = _tritmes.trid;
				__trhtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
			}
			return __trhtml;
		},
		TotrJson: function(DATA, pageSize) {
			var headallobj = _tbody.head;
			var __tr = [];
			for (var tri = 0; tri < DATA.length; tri++) {
				var tritme = DATA[tri];
				var __td = [];
				for (var headkey in headallobj) {
					var tritmes = headallobj[headkey];
					var Rowname = tritmes.field;
					var Rowobj = {};
					if (typeof Rowname == "object") {
						__td.push({
							Options: Rowname
						});
					} else {
						Rowobj = eval("tritme." + Rowname);
						var setobjs = eval("({" + Rowname + ":Rowobj})");
						__td.push(setobjs);
					}
				}
				__tr.push({
					"trid": tritme.trid,
					"children": __td
				});
				if (tri >= pageSize) {
					break;
				}
			}
			return __tr;
		},
		keyname: function(obj) {
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id": function (obj) {
			    //console.log(obj);
				return obj.trid;
			},
			"data-itmeid": function(obj) {
				return "trid|" + obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="trid|' + obj.trid + '" class="buttons-group">';
				var ahtml = "";
				//console.log(obj);
				for (var akey in obj.children) {
					var itmea = obj.children[akey];
					ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
				}
				trhtmls += ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		td: {
		    tag: "td",
		    width: function (obj, index) {
		        var width = "20%";
		        switch (index) {
		            case 0:
		                width = "30%";
		                break;
		            case 1:
		                width = "50%";
		                break;
		            case 2:
		                width = "20%";
		                break;
		        }
		        return width;
		    },
			class: function(obj, index) {
				var classname = "";
				var keynames = _tbody.keyname(obj);
				classname = "td_" + keynames;
				return classname;
			},
			valign: function (obj, index) {
			    return "center";
				//if(index == 0){
				//	return "center";
				//} else {
				//	return "top";
				//}
			},
			html: function(obj, index) {
				var retuhtml = "Not find template";
				var keynames = _tbody.keyname(obj);
				if (keynames != "" && typeof keynames != "undefined") {
					if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
					}
				}
				return retuhtml;
			}
		},
		LOAD: {
			tag: "fieldset",
			class: "ORDERGreen",
			children: [
                {
				tag: "legend",
				html: function(obj, index) {
					var legendhtml = "";
					legendhtml = '<span data-eventname="events.Edit_legend" style="text-decoration: underline;cursor:pointer; padding:0 5px;" class="Sub_order">' + obj.LOAD.LOADNO + '</span>';
					return legendhtml;
				}
			},
			{
				tag: "div",
				class: "Tractor_ulitme",
				children: [{
					tag: "ul",
					html: function(obj, index) {
						return (JSONTOHTML.transform(obj.LOAD.CHILDREN, _tbody.fieldset1divulli));
					}
				}]
			}]
		},
		fieldset1divulli: {
			tag: "li",
			html: function(obj, index) {
				if(obj.LABLE == '特殊配送')
					return '<span class="namestyle" style="color:#E00">' + obj.LABLE + ':</span><span class="valstyle" style="color:#E00">' + obj.VALUE + '</span>';
				else
					return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		"ORDERLIST": {
		    tag: "div",
		    children: [{
		        tag: "div",
		        html: function (obj, index) {
                    console.log()
		            return (JSONTOHTML.transform(obj.ORDERLIST, _tbody.ORDER2li));
		        }
		    }]
		},
		ORDER2li: {
		    tag: "fieldset",
		    class: "tableBlue",
		    children: [{
		        tag: "legend",
		        html: function(obj, index) {
		            var legendhtml = "";
		            legendhtml += '<span class="Sub_order" style="padding-left:5px;">Master Order: ' + obj.MASTERORDER + '</span>';
		            legendhtml += '<i>|</i>';
		            legendhtml += '<span class="Sub_Customer longString" title="' + obj.CONN + '">' + obj.CONN + '</span>';
		            return legendhtml;
		        }
		    }, 
			{
			    tag: "div",
			    html: function (obj, index) {
			        var ulsring = "<table class='table table-bordered'>";
			        var arryjson = obj.CHILDREN;

			        for (var _key = 0; _key < arryjson.length; _key++) {
			            var arryitmes = arryjson[_key];
			            var th = "";
			            var td = "";

			            if (_key == 0) {
			                th += "<tr><th>Order NO.</th>";
			                for (var _key2 = 0; _key2 < arryitmes.CHILDREN.length; _key2++) {
			                    th += "<th>" + arryitmes.CHILDREN[_key2].LABLE + "</th>";
			                }
			                th += "</tr>";

			                td += "<tr><td>" + arryjson[_key].ORDERNO + "</td>";
			                for (var _key2 = 0; _key2 < arryitmes.CHILDREN.length; _key2++) {
			                    td += "<td>" + arryitmes.CHILDREN[_key2].VALUE + "</td>";
			                }
			                td += "</tr>";
			            }
			            else {
			                if (_key > 1) {
			                    td += "<tr class='animated trHidden'>"; //fadeInDown
			                }
			                else {
			                    td += "<tr>";
			                }
			                td += "<td>" + arryjson[_key].ORDERNO + "</td>";
			                for (var _key2 = 0; _key2 < arryitmes.CHILDREN.length; _key2++) {
			                    td += "<td>" + arryitmes.CHILDREN[_key2].VALUE + "</td>";
			                }
			                td += "</tr>";
			            }

			            ulsring += th + td;
			        }
			        ulsring += "</table>";

			        if (arryjson.length > 2) {
			            ulsring += '<span data-eventname="events.more" class="Morestyle">More...</span>';
			        }
			        return ulsring;
			    }
			}]
		},
		fieldset2divulli: {
			tag: "fieldset",
			class: "ORDER",
			children: [{
				tag: "legend",
				html: function(obj, index) {
					var legendhtml = "";
					legendhtml = '<span class="Sub_order">' + obj.ORDERID + '</span>';
					return legendhtml;
				}
			},
			{
				tag: "div",
				class: "Tractor_ulitme",
				children: [{
					tag: "ul",
					html: function(obj, index) {
						return (JSONTOHTML.transform(obj.CHILDREN, _tbody.fieldset3divulli));
					}
				}]
			}]
		},
		fieldset3divulli: {
			tag: "li",
			html: function(obj, index) {
					return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		OPTION: {
		    tag: "div",
		    class: "Log_container",
		    html: function (obj, index) {
		        var ulsring = "";
		        if (obj.OPTION == null || obj.OPTION == "") {
		            return ulsring;
		        }
		        var arryjson = obj.OPTION.LOGDETAILS;
		        var box1px = "<span class='box1px'></span>";

		        for (var _key = 0; _key < arryjson.length; _key++) {

		            var arryitmes = arryjson[_key];
                     
		            var ul = '<ul><li><span>' + arryitmes.USER + '</span><span data-eventname="events.moreOption" class="valuestyle">' + arryitmes.OPTION + arryitmes.VALUE + '</span></li><li><span>' + DateUtil.getFormartDateStringForPs(arryitmes.DATETIME,'MM/dd/yyyy hh:mm',arryitmes.JET_LAG||0); + '</span></li></ul>';
		            
		            if (ulsring != "" && typeof ulsring != "undefined") {
		                if (_key > 1) {
		                    ulsring = ulsring + '<div class="animated trHidden">' + box1px + ul + "</div>";
		                } else {
		                    ulsring = ulsring + box1px + ul;
		                }
		            } else {
		                ulsring = ul;
		            }
		        }

		        if (arryjson.length > 2) {
		            ulsring += '<label>';
		            ulsring += '<span data-eventname="events.moreOption" class="Morestyle">More...</span>';
		            ulsring += '</label>';
		        }
		        return ulsring;
		    }
		},
		Options: {
			tag: "li",
			html: function(obj, index) {
				return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
		}
		},
		updatatr: function(jsondata, trid) {
			var tritmeobj = $(trid);
			var tritmedata = [];
			tritmedata.push(jsondata);
			var uptrdata = _tbody.TotrJson(tritmedata, 1);
			//console.log(uptrdata[0].children);
			tritmeobj.empty().html(JSONTOHTML.transform(uptrdata[0].children, _tbody.td));
		}
	}
	return {
		tbody: _tbody,
		table: _table
	}
});