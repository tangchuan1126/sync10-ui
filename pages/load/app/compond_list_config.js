define(["./jsontohtml_templates"], function(tmp) {
	return {
		"View_control": {
			"head": [{
				"title": "Load",
				"field": "LOAD"
			}, {
				"title": "Order",
				"field": "ORDERLIST"
			}, {
				"title": "Log",
				"field": "OPTION"
			}],
			"idAttribute":"load_id",
			"authIdAttribute":"ID",
			"footer": [
				 /*[
					"events.addToBatch", "Add To Master Load"
				],
               [
					"events.CreateAppoiontment", "Appointment"
                ],*/
				[
					"events.Edit", "Edit"
				],
				[
					"events.Delete", "Delete"
				]
			],
			"templates": tmp
		}
	}

});