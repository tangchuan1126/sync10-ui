"use strict";
require(['/Sync10-ui/requirejs_config.js'], function() {

	    require(["jquery",
            "bootstrap",
            "bootstrap.datetimepicker",
            "metisMenu",
            "CompondList/View",
            "art_Dialog/dialog-plus",
            "compond_list_config",
            "jsontohtml_templates",
            'backbone',
            'handlebars',
            "../view/searchSample",
            "../view/searchAdvanced",
            "slidePanel",
            "../config",
            "../model/loadModel",
        //    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
            "immybox",
            "oso.lib/table/js/table",
            "dateUtil",
            "blockui",
            "require_css!bootstrap-css/bootstrap.min",
            "require_css!bootstrap.datetimepicker-css",
            "require_css!oso.lib/immybox-master/immybox.css"
            ],
			function ($, bootstrap, datetimepicker, metisMenu, CompondList, Dialog, list_config, templates, backbone, Handlebars, searchSample, searchAdvanced, SlidePanel, Config, Model,immybox, Table , DateUtil) {
			    (function () {



          $("body").show();

                NProgress.start();

			        $.blockUI.defaults = {
			            css: {
			                padding: '8px',
			                margin: 0,
			                width: '170px',
			                top: '45%',
			                left: '40%',
			                textAlign: 'center',
			                color: '#000',
			                border: '3px solid #999999',
			                backgroundColor: '#ffffff',
			                '-webkit-border-radius': '10px',
			                '-moz-border-radius': '10px',
			                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			                '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
			            },
			            overlayCSS: {
			                backgroundColor: '#000',
			                opacity: '0.6'
			            },
			            baseZ: 99999,
			            centerX: true,
			            centerY: true,
			            fadeOut: 1000,
			            showOverlay: true
			        };
			    })();
				/*
				
			    $(document).ajaxStart(function () {
			        $.blockUI({ message: '<img src="/Sync10-ui/pages/load/css/img/dy_loading.gif" class="loading" align="absmiddle"/> 正在处理，请稍后.' });   //遮罩打开 
			    });
				$(document).ajaxSuccess(function () {
			        $.unblockUI();
			    });
				*/
			function GetRequest (name) {
			    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
			    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
			    if (r != null) return unescape(r[2]);
			    return null; //返回参数值
			};

			$.ajax({
				url: Config.adminSession.url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(data) 
			{

				var adminSesion = data.attributes.adminSesion;
				var psId = adminSesion.ps_id;


			    $(document).ajaxError(function () {
			        $.blockUI({ message: 'Error，Please refresh!<div><a class="closeBlock">Close</a></div>' });   //遮罩打开 
			        $(".closeBlock").click(function () {
			            $.unblockUI();
			        });
			    });
			    window.Parentpush = {};
			    //$('#myTab a:first').tab('show');
			    $('#myTab a:last-child').tab('show');
			    
			    $('.quick-create').on('click', function (e) {
			        e.stopPropagation();
			        return false;
			    });




				$.getJSON(Config.carrierUrl.url, function (result) {

					$.each(result, function(index, obj) {
						obj.text = obj.value + " - "+ obj.text;
					});

				    $('#addCarrier').immybox({
				    	Pleaseselect:'<i>Clean Select</i>',
				        textname:"text",
				        valuename:"value",
				        choices: result
				    });
				});

		



				delete window.top.parentTable;
	            var SlidePanelCloseBefore = function( param ){
	            	
	            //	console.log( window.top.parentTable );
	            	if(window.top.parentTable){
	            		
		            	var parentTable =  window.top.parentTable;
		            	var upList = parentTable.getLocalData();
		            //	console.log(upList);
		            	if(!upList || upList.length<1){
		            		alert("Order not empty. Please add order!");
		            		return false;
		            	}
		            	delete window.top.parentTable;
					}
					
				};

				var SlidePanelShutdownCallback = function(addset){

	           //	console.log(addset);

	            	//如果为true,要父页面刷新
					if(addset){
						window.top.listView();
                   //	window.parent.postMessage(addset, '*');
					}
				}

			    $("#addPackage").on('click', function () {
			        var slide = new SlidePanel({
			            title: "Create Load",
			            url: Config.editLoadUrl.url + "?addEdit=add",
			            duration: 500,
			            topLine: 0,
			            slideLine: "85%",
			            closebefore : SlidePanelCloseBefore,
			            ShutdownCallback : SlidePanelShutdownCallback
			        });
	                slide.open();
			    });


			    /*创建load*/
			    $("#btnQuickCreateLoad").on("click", function () {
			        var f = true;
			        var $quickInputAddLoadNo = $("#inputAddLoadNo");
			        if ($quickInputAddLoadNo.val() == "" | $quickInputAddLoadNo.val() == undefined) {
			            if (!$quickInputAddLoadNo.parent().hasClass("has-feedback")) {
			                $quickInputAddLoadNo.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
			                $quickInputAddLoadNo.focus();
			            }
			            f = false;
			        } else {
			            $quickInputAddLoadNo.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
			        }
			        
			        var $quickAddCarrier = $("#addCarrier");
			        if ($quickAddCarrier.val() == "" | $quickAddCarrier.val() == undefined) {
			            if (!$quickAddCarrier.parent().hasClass("has-feedback")) {
			                $quickAddCarrier.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
			            //    $quickAddCarrier.focus();
			            }
			            f = false;
			        } else {
			            $quickAddCarrier.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
			        }

			        if (f) {
			            var v = this;
			            var model = new Model.LoadModel({
			                loadNo: $quickInputAddLoadNo.val(),
			                carrierId : $quickAddCarrier.attr("data-value")
			            });

			            model.save({
			                id: v.loadid,
			                loadNo: encodeURI($quickInputAddLoadNo.val()),
			                carrierId : $quickAddCarrier.attr("data-value"),
			                orderList: []
			            }, {
			                success: function (e) {
			                    if (e.changed.result !=0) {
			                        var d = new Dialog({
			                            content: e.changed.error || "Save is fail",
			                            icon: 'question',
			                            lock: true,
			                            width: 200,
			                            height: 40,
			                            title: 'Save Fail',
			                            cancelValue: 'Close',
			                            cancel: function () { }
			                        }).show();
			                    } else {
			                        $("#inputAddLoadNo").val("");
			                        $quickAddCarrier.val(null).removeAttr('data-value');
			                        listView();
			                    }
			                },
			                faile: function (e) {
			                    var d = new Dialog({
			                        content: "Save fail,please try again!",
			                        icon: 'question',
			                        lock: true,
			                        width: 200,
			                        height: 40,
			                        title: 'Save Fail',
			                        cancelValue: 'Close',
			                        cancel: function () {}
			                    }).show();
			                }
			            });
			        }

			    });

			        var list;
			        var listView = function () {

			        	var PostData = { //load_no&status&start_time&end_time&page=1&page_size=1
			        	    "load_no": $("#searchLoadPoNo").val(),
			        	    "customer": $("#searchLoadCustomer").attr("data-value"),
			        	    "company": $("#searchLoadCompany").attr("data-value"),
			        	    "end_time": $("#searchLoadEndMABD").val(),
			        	    "begin_time": $("#searchLoadBeginMABD").val()
			        	}
			            list = new CompondList(list_config, {
			                renderTo: "#main_div",
			                dataUrl: Config.loadListUrl.url,
			                PostData: PostData
                            //fetchtype:"POST"
			            });
			            //console.log(PostData);
			            list.render();
			            Parentpush.RestwintopMeun = list.Settopnvaheigth;
			            Parentpush.Dialog_api = Dialog;
			        }
			     //   listView();
			        window.top.listView = listView;
			        
		            var sample = new searchSample({
		        	    el: "#searchSampleTab"
		            }).render();
		        

		        	//当页面url带有参数 loadno 时,将查询该参数
			        var urlParam = { };
			        var loadno = GetRequest("loadno");
			        if(loadno){
			        	urlParam.loadno = loadno;
			        }
		            var advanced = new searchAdvanced({
		            	"psId":psId,
		            	"listView":listView ,
		            	urlParam:urlParam,	//于用查询
		        	    el: "#searchAdvancedTab"
		            }).render();
				    
			        list.on("events.more", function (e) {
			            var btnobj = e.button;
			            var table = btnobj.prev("table").eq(0);

			            if (table.find(".trHidden").is(":hidden")) {
			                table.find(".trHidden").show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			                    table.find(".trHidden").removeClass("fadeInDown");
			                    btnobj.html("Hide");
			                });
			            } else {
			                table.find(".trHidden").addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			                    table.find(".trHidden").removeClass("fadeOutUp").hide();
			                    btnobj.html("More...");
			                });
			            }
			        });

			        list.on("events.moreOption", function (e) {

			        	var d = new Dialog({
			        	    content: "<div id='logsTableList'></div>",
			        	 //   icon: 'question',
			        	//    lock: true,
			        	    width: 600,
			        	    height: 300,
			        	    title: 'Logs',
			        	    cancelValue: 'Close',
			        	    cancel: function () { },
			        	    onshow:function(){
    		        				var table2 = new Table({
    		        	                container: '#logsTableList',
    		        	                height: 230,
    		        	         		localData : e.data.OPTION.LOGDETAILS,
    		        	            //    groupKey: "connect",
    		        	                rownumbers: false,
    		        	            //    checkbox: true,
    		        	                singleSelection: true,
    		        	                pagination: true,
    		        	                pageSize: 10,
    		        	                columns: [
    		        	                    { field: 'USER', title: 'USER', width: 50 },
    		        	                    { 
    		        	                    	field: 'OPTION', title: 'OPTION', width: 50 ,
	    		        	                    formatter : function (data, row){
													return "<font color='#FF6600'>" + data + row.VALUE + "</font>";
												}
											},
    		        	                    { 
    		        	                    	field: 'DATETIME', title: 'DATETIME', width: 80 ,
    		        	                    	formatter :function (data, row){
													return data ? DateUtil.getFormartDateStringForPs(data,"MM/dd/yyyy   hh:mm:ss", row.jet_lag || 0) : "" ;
												}
											}
    		        	                ]
    		        	            });
									//设置table的高度不变
									$(".table-content").css("height",230);

			        	    }
			        	}).showModal();

			        });

			        list.on("events.CreateAppoiontment", function (e) {
			            var btntxt = e.button.text();
			         //   console.log(e);
			            var loadChildren = e.data.linedata.LOAD.CHILDREN;
			            var title = 'Create Appoiontment';
			            for(var i=0; i<loadChildren.length; i++){
			            	var dat = loadChildren[i];
			            	var flag = dat.LABLE.toLowerCase() === "appointment time";
			            //	console.log(flag + "   " + dat.LABLE.toLowerCase() );
			            	if(flag){
			            		title = 'Edit Appoiontment';
			            		break;
			            	}
			            	
			            }
			        
				        
				        var slide = new SlidePanel({
				            url: Config.addAppoiontmentUrl.url + "?orderid=" + e.data.linedata.trid + "&ordertype=load&appid=" + e.data.linedata.LOAD.aid,
				            title: title,
				            duration: 500,
				            topLine: 0,
				            slideLine: "85%",
				            dynamicParam: '{"orderid":' + e.data.linedata.trid + ',"ordertype":"load"}'
				        });
				        slide.open();

				    });
			        /*
						Load legend
			        */
	                list.on("events.Edit_legend", function (e) {
	                		//console.log(e);

    					        var slide = new SlidePanel({
    					            title: "Edit Load",
    					            url: Config.editLoadUrl.url+ "?loadid=" + e.data.trid + "&loadtype=Edit",//&orderno=" + e.data.linedata.LOAD.LOADNO,
    					            duration: 500,
    					            topLine: 0,
    					            slideLine: "85%",
    					           	closebefore : SlidePanelCloseBefore,
    					            ShutdownCallback : SlidePanelShutdownCallback

    					        });
    					        slide.open();
	        	    });
				    list.on("events.Edit", function (e) {
				        var btntxt = e.button.text();

				        
				        var slide = new SlidePanel({
				            title: "Edit Load",
				            url: Config.editLoadUrl.url+ "?loadid=" + e.data.linedata.trid + "&loadtype=Edit",//&orderno=" + e.data.linedata.LOAD.LOADNO,
				            duration: 500,
				            topLine: 0,
				            slideLine: "85%",
				            closebefore : SlidePanelCloseBefore,
				            ShutdownCallback: SlidePanelShutdownCallback

				        });
				        slide.open();
				   
				    });

				    list.on("events.Delete", function (e) {
					    var btntxt = e.button.text();
					    var d = new Dialog({
						    content: 'Are you sure you want to delete this load?',
						    icon: 'question',
						    lock: true,
						    width: 200,
						    height: 70,
						    title: 'Delete Load',
						    okValue: 'Yes',
						    ok: function () {
						        $.ajax({
						            type: 'post',
						            url: Config.delLoadUrl.url,
						            data: 'load_id=' + e.data.linedata.trid,
						            dataType: 'json',
						            success: function (data) {
						            	if(data!=null){
						            		if(data.result == "-2") {
						            			alert(data.error);
						            		}
						            	}
						                listView();
						            },
						            error: function () {}
						        });
						    },
						    cancelValue: 'No',
						    cancel: function() {
							    //console.log("No");
						    }
					    }).showModal();
				    });



				    window.addEventListener('message', function (e) {
				        $(".slide-header-close").click();
				        listView();
				    }, false);

				    
			    
			
			NProgress.done();
		})
		.fail(function(){
			alert("please login !");
		});
	});
});