﻿/**
    *create by cuixiang 20141202
*/
"use strict";
define([
	"jquery",
	"backbone",
	"handlebars",
	"bootstrap",
	"./config",
	"nprogress",
	"immybox",
	"js/view/addAppiontment",
	"js/view/v4List",
	"blockui",
	"jqueryui/tabs"
], function ($, Backbone, handlebars, bootstrap, config, NProgress, immybox, addAppiontment,List) {
		NProgress.start();
        //Tab切换.
        $('#listTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        //$('#listTab a:first-child').tab('show');

        //当点击ADVANCED SEARCH高级查询时候,清空 "快速创建预约"内的预约时间,同时也清空列表title的时间.
        //  原因是当点击Calendar内时间的同时也会把自己的时间赋值给快速创建预约的时间
        $('#listTab a:last-child').click(function(){
            $("#inputQuickAppointmentTime").val(null);
            $("#appiontmentInfo").html(null);
        });
		
		
        //遮罩
        (function () {
            $.blockUI.defaults = {
                css: {
                    padding: '8px',
                    margin: 0,
                    width: '170px',
                    top: '45%',
                    left: '40%',
                    textAlign: 'center',
                    color: '#000',
                    border: '3px solid #999999',
                    backgroundColor: '#ffffff',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                    '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
                },
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: '0.6'
                },
                baseZ: 99999,
                centerX: true,
                centerY: true,
                fadeOut: 1000,
                showOverlay: true
            };
        })();
		/*
        $(document).ajaxStart(function () {
            $.blockUI({ message: '<img src="/Sync10-ui/pages/load/css/img/dy_loading.gif" class="loading" align="absmiddle"/> 正在处理，请稍后.' });   //遮罩打开 
        });
        $(document).ajaxSuccess(function () {
            $.unblockUI();
        });
		*/
		$(document).ajaxError(function () {
			showError();//遮罩打开
			$.blockUI({ message: '出现错误，请刷新!<div><a class="closeBlock">关闭</a></div>' });   //遮罩打开 
			$(".closeBlock").click(function () {
				$.unblockUI();
			});
		});
		$.ajax({
			type: "GET",
			url: config.adminSession.url,
			dataType: "json",
			timeout: 30000,
			error: function(XHR,textStatus,errorThrown) {
				if (textStatus==401) {
					showError('请确认已经登陆系统！');
				} else {
					showError('请求出错！');
				}
				NProgress.done();
			},
			success: function(data,textStatus) {
				if (textStatus=="success") {
					renderMe(data.attributes.adminSesion);
				}
				NProgress.done();
			},
			headers: {
				"X-HTTP-Method-Override": "GET"
			}
		});
		function showError(error) {
			$.blockUI({ message: ""+error+'<div><a class="closeBlock">关闭</a></div>' });   //遮罩打开 
			$(".closeBlock").click(function () {
				$.unblockUI();
			});
		}
		function renderMe(data) {
			//console.log(data);
			//console.log(data.department);
			var listView = new List({psid:data.psid, myStores:data.department});
			listView.render();
		}
		//renderMe({});
});