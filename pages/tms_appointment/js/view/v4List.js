﻿"use strict";
var all="";
define(["jquery",
    "backbone",
    "handlebars",
    "templates",
    "bootstrap.datetimepicker",
    "immybox",
    "CompondList/View",
    "art_Dialog/dialog-plus",
    "../config/appiontmentConfig",
    "slidePanel",
    "config",
    "../model/appiontment",
    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
    "dateUtil",
	"oso.lib/table/js/table",
    "oso.bower/moment/min/moment-with-locales.min",
    "require_css!bootstrap.datetimepicker-css"
    ],
    function ($, Backbone, handlebars, templates, datetimepicker, immybox, CompondList, Dialog, list_config, SlidePanel, Config, Model, AsynLoadQueryTree,dateUtil,Table,moment) {
        console.log(datetimepicker);
        return Backbone.View.extend({
            defaultStore : "1000005",
			myStores:[],
			boundType: "",
            initialize: function (options) {
                var v = this;
                if (options.psid) v.defaultStore = options.psid;
				v.myStores = options.myStores || v.myStores;
                //console.log(options.psid);
                $(function () {
                    v.appiontmentCalendarInit(v.defaultStore);
                    v.advancedSearchInit();
                    v.luceneSearchInit();
                    v.quickCreateInit();

                
					//hyq has add code as follows(2015-3-27)
					$(".calendar-time tbody tr:odd").css("background","#f5f6f5");
                });
				window.MyApi={};
            },
            listView : function (param) {
             //   console.log(param);
                //var v = this;
                var obj = {
                    "selectOrderType": $("#selectOrderType").attr("data-value"),
                    "orderNum": $("#inputOrder").val(),
                    "inputAppointmentTime": $("#inputAppointmentTime").val(),
                    
                    "searchFrom": $("#searchFrom").attr("data-val")
                };
				if ($("#inputCarrier").val()!="") {
					obj.inputCarrier = $("#inputCarrier").attr("data-value")
				}
                if (!$("#searchFrom").attr("data-val")) {
                    if (!param) {
                        obj.searchFrom = this.defaultStore;
                    }
                }
                var list = new CompondList(list_config, {
                    renderTo: "#appiontmentList",
                    dataUrl: Config.appiontmentList.url,
                    PostData: $.extend(obj , param )
                });
                list.render();
                return list;
            },
            render: function () {
                var v = this;
                var list = this.listView();
                //列表.
                var listView = list;
                this.initCal();
                //搜索按钮.
                $("#btnListSearch").on("click", function () {
                    v.listView();
                });
                $("#btnReset").on("click", function () {
                    $("#selectOrderType").attr("data-value", "all").val("All Type");
                    $("#inputOrder").val("");
                    $("#inputAppointmentTime").val("");
                    $("#inputCarrier").attr("data-value", "").attr("data-name", "").val("");
                    $("#searchFrom").attr("data-val", "").attr("data-name", "").val("");
                    v.listView();
                });
                //快速创建.
                $("#btnQuickSave").on("click", function () {
                    var f = true;
                    if ($("#inputQuickCarrier").val() == "" | $("#inputQuickCarrier").val() == undefined) {
                        if (!$("#inputQuickCarrier").parent().hasClass("has-feedback")) {
                            $("#inputQuickCarrier").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                        }
                        f = false;
                    } else {
                        $("#inputQuickCarrier").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                    }

                    if ($("#inputQuickAppointmentTime").val() == "" | $("#inputQuickAppointmentTime").val() == undefined) {
                        if (!$("#inputQuickAppointmentTime").parent().hasClass("has-feedback")) {
                            $("#inputQuickAppointmentTime").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                        }
                        f = false;
                    } else {
                        $("#inputQuickAppointmentTime").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
                    }

                    if (f) {
                        var model = new Model.AppointmentModel();
						v.defaultStore = $("#searchCustomer").attr("data-value");
                        //hyq has changed code as follows(2015-4-1)
						//var defaultStoreName = $("#searchCustomer").attr("data-name");
                        var defaultStoreName = $("#searchCustomer").val();
						var yyyymmddhh = v.selecDate;
                        model.save({
							storage_id: v.defaultStore,
							storage_name: defaultStoreName,
                            carrier_id: $("#inputQuickCarrier").attr("data-value"),
                            carrier_name: $("#inputQuickCarrier").val(),
							appointment_type: v.boundType?v.boundType:'outbound',
                            //appointment_type: v.boundType?v.boundType:'Outbound',
                            appointment_time: $("#inputQuickAppointmentTime").val()
                        },{
                            success: function (e,r) {
								if (e.changed.result != 0) {
                                    var d = new Dialog({
                                        content: e.changed.error,
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Save fail',
                                        cancelVal: 'close',
                                        cancel: function () { }
                                    }).show();
                                } else {
                                    $("#inputQuickCarrier").val("").attr("data-value", "").attr("data-name", "");
                                    v.listView({
										"inputAppointmentTime":$("#inputQuickAppointmentTime").val(),
										"searchFrom": v.defaultStore
									});
									$("#inputQuickAppointmentTime").val("");
									if (yyyymmddhh) {
										v.initTime({
											"storage_id": v.defaultStore,
											"date": yyyymmddhh
										});
									}
									//$('#calendar').fullCalendar("restorage_id",v.defaultStore);
                                }
                            },
                            faile: function (e) {
                                var d = new Dialog({
                                    content: "Save fail, please try again!",
                                    icon: 'question',
                                    lock: true,
                                    width: 200,
                                    height: 40,
                                    title: 'save fail',
                                    cancelVal: 'close',
                                    cancel: function () { }
                                }).show();
                            }
                        });

                        
                    }
                });
                //创建.
                //hyq has changed clickevent code as follows(2015-4-1)
                //$('#addAppiontment').click(function () {
                $('#addAppiontment').click(function (a,b) {  //a指当前对象，b触发该事件传过来的参数
                    //从快速约车页面内取得预约时间,然后作为参数传过去..
                    var inputQuickAppointmentTime = $("#inputQuickAppointmentTime").val();
                    //hyq has add if(b) code as follows(2015-4-1)
					if(b){
                        //hyq has changed code as follows(2015-4-2)
                        /*//hyq has add compare between time and nowtime
                        if (b && (new Date(b)> new Date()) ) {
                            inputQuickAppointmentTime = b;
                        } else {
                            $("#inputQuickAppointmentTime").val("");
                        }*/

                        /*//hyq has add timetransfer as follows(2015-4-2)
                        var nowtime = new Date();  //获取当前时间
                        var year = nowtime.getFullYear();  //获取年份
                        var month = nowtime.getMonth()+1;  //获取月份
                        var day = nowtime.getDate();       //获取日
                        var hours = nowtime.getHours();    //获取小时
                        var newtime = month+"/"+day+"/"+year+" "+hours+":00";   //整合时间(当前时间取到整小时)*/
                        //newtime = new Date(newtime);   //当前时间取到准小时
                        //选中时间(预约时间)必须大于或等于当前时间准小时
                        if (b && (new Date(b)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                            inputQuickAppointmentTime = b;
                        } else {
                            var d = new Dialog({
                                //content: "Time can not be less than the current time!",
                                content: "The time you have selected has elasped the currect time",
                                icon: 'question',
                                lock: true,
                                width: 200,
                                height: 40,
                                //title: 'Prompt',
                                title:'Warning!',
                                cancelValue: 'Close',
                                cancel: function () { }
                            }).show();
                            d.undelegateEvents();   //实例化清除
                            var d = new Dialog({
                                //content: "Time can not be less than the current time!",
                                content: "The time you have selected has elasped the currect time",
                                icon: 'question',
                                lock: true,
                                width: 200,
                                height: 40,
                                //title: 'Prompt',
                                title:'Warning!',
                                cancelValue: 'Close',
                                cancel: function () { }
                            }).show();
                            //$("#inputQuickAppointmentTime").val("");
                            //alert("预约时间不能小于当前时间的准小时");
                        }
                    }
                    inputQuickAppointmentTime = inputQuickAppointmentTime ? "?name="+inputQuickAppointmentTime:"";
					if (inputQuickAppointmentTime) {
						inputQuickAppointmentTime += "&bound_Type="+ v.boundType;
					} else {
					}

					MyApi.opensdiag = function(data, vf, orderNo) {
						var d = Dialog(data);
						d.reset();
						d.showModal();
						vf.loadList(orderNo);
					}
                    var slide = new SlidePanel({
                        url: Config.createAppiontement.url + inputQuickAppointmentTime,
                        title: 'Create Appointment',
                        duration: 500,
                        topLine: 0,
						zindex: 100,
                        slideLine: "85%"
                    });
                    slide.open();
                });

                list.on("events.UpdateAppiontmentDate", function (e) {
                    var btntxt = e.button.text();

                    var btntxt = e.button.text();
                    var d = new Dialog({
                        content: 'Are you sure to cancel this time about the appointment?',
                        icon: 'question',
                        lock: true,
                        width: 200,
                        height: 70,
                        title: 'Cancel appointment',
                        okVal: 'Yes',
                        ok: function () {
                            $.ajax({
                                type: 'post',
                                url: Config.delAppiontment.url,
                                data: 'aid=' + e.data.linedata.trid,
                                dataType: 'json',
                                success: function (data) {
									v.defaultStore = $("#searchCustomer").attr("data-value");
                                    //console.log((new Date()).format('MM/dd/yyyy'));
                                    v.listView({
										"searchFrom": v.defaultStore,
										"inputAppointmentTime":(new Date()).format('MM/dd/yyyy')
									});
									//$('#calendar').fullCalendar("restorage_id",v.defaultStore);
									v.initTime({
										"storage_id": v.defaultStore,
										"date": v.getCurrentDate()
									});
                                },
                                error: function () {
                                }
                            });
                        },
                        cancelVal: 'No',
                        cancel: function () {
                            //console.log("No");
                        }
                    }).showModal();

                });
                
                list.on("events.Edit", function (e) {
                    var btntxt = e.button.text();
                    //console.log(e.data.linedata.trid);
                    var slide = new SlidePanel({
                        title: "Edit Appointment",
                        url: Config.createAppiontement.url + "?appid=" + e.data.linedata.trid + "&type=Edit",
                        duration: 500,
                        topLine: 0,
                        slideLine: "85%"
                    });
                    slide.open();

                });
				
				//hyq has changed code as follows(2015-3-27)
                // list.on("events.moreOption", function (e) {
                    // var btnobj = e.button;
                    // var table = btnobj.parents(".Log_container").find(".trHidden");

                    // if (table.is(":hidden")) {
                        // table.show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            // table.removeClass("fadeInDown");
                            // e.button.html("Hide");
                        // });
                    // } else {
                        // table.addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            // table.removeClass("fadeOutUp").hide();
                            // e.button.html("More...");
                        // });
                    // }
                // });
				list.on("events.more", function (e) {
                    var btnobj = e.button;
                    var table = btnobj.parents(".Log_container").find(".trHidden");

                    if (table.is(":hidden")) {
                        table.show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeInDown");
                            e.button.html("Hide");
                        });
                    } else {
                        table.addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                            table.removeClass("fadeOutUp").hide();
                            e.button.html("More...");
                        });
                    }
                });
					//点击“更多”触发
			        list.on("events.moreOption", function (e) {
					//	console.log(e.data.jet_lag);    //打印后台返回的数据
			        	var d = new Dialog({
			        	    content: "<div id='logsTableList'></div>",
			        	 //   icon: 'question',
			        	//    lock: true,
			        	    width: 600,
			        	    height: 300,
			        	    title: 'Logs',
			        	    cancelValue: 'Close',
			        	    cancel: function () { },
			        	    onshow:function(){
    		        				var table2 = new Table({
    		        	                container: '#logsTableList',
    		        	                height: 230,
    		        	         		localData : e.data.Log.LOGDETAILS,   //后台返回数据
    		        	            //    groupKey: "connect",
    		        	                rownumbers: false,
    		        	            //    checkbox: true,
    		        	                singleSelection: true,
    		        	                pagination: true,
    		        	                pageSize: 10,
    		        	                columns: [
    		        	                    { field: 'USER', title: 'USER', width: 50 },
    		        	                    { 
    		        	                    	field: 'REMARK', title: 'REMARK', width: 50 ,
	    		        	                    formatter : function (data, row){
													return "<font color='#FF6600'>" + data  + "</font>";
												}
											},
    		        	                    { 
    		        	                    	field: 'DATETIME', title: 'DATETIME', width: 80 ,
    		        	                    	formatter :function (data, row){

                                                    var dfff = moment(  data ,"MM/DD/YYYY HH:mm").format("x")

                                        var newP = parseInt(dfff);
                                         var p = parseInt( e.data.jet_lag || 0);

                                                    var doneTime = moment(  newP + p     ).format("MM/DD/YYYY HH:mm");

                                                    return doneTime;
												}
											}
    		        	                ]
    		        	            });
									//设置table的高度不变
									$(".table-content").css("height",230);

			        	    }
			        	}).showModal();
			        });
				
				
				

                //创建保存和修改监听.
                window.addEventListener('message', function (e) {
                    //console.log("dfdd");
                    $(".slide-header-close").click();
					//$('#calendar').fullCalendar("restorage_id",v.defaultStore);
                    //v.listView();
					if (v.selecDate) {
						var param = {"storage_id":v.defaultStore, "date":v.selecDate};
						v.initTime(param);
                        var selectParam = {
                             "inputAppointmentTime": moment(v.selecDate).format("MM/DD/YYYY"),
                             "searchFrom": $("#searchCustomer").attr("data-value"),
                             "boundType" : ""//入库
                        };
                    //    console.log(selectParam);
						v.listView(selectParam);
					} else {

						var param = {"storage_id":v.defaultStore, "date":v.getCurrentDate()};
						v.initTime(param);
                        var selectParam ={
                             "inputAppointmentTime": new Date().format("MM/dd/yyyy"),
                             "searchFrom": $("#searchCustomer").attr("data-value"),
                             "boundType" : ""//入库
                        };
                    //    console.log(selectParam);
						v.listView(selectParam);
					}
                }, false);

                $("#inputAppointmentTime").datetimepicker({ autoclose: 1, minView: 1,forceParse: 0, format:'mm/dd/yyyy hh:00',bgiconurl:"" });
            },
            appiontmentCalendarInit:function(psid){
                var v = this;
            //    $("#appiontmentCalendarTab").html(templates.appiontmentCalendar().trim());
                $("#appiontmentCalendarTab").html(templates.appiontmentCalendar().trim());
                // var searchCustomer = new AsynLoadQueryTree({
                    // renderTo: "#searchCustomer",
                    // selectid: {value: v.defaultStore},
                    // PostData: {rootType:"Ship From"},
                    // dataUrl: Config.shipTo.url,
                    // scrollH: 400,
                    // multiselect: false,
                    // Async: true,
                    // Pleaseselect: "Clean Select",
                    // placeholder:"Ship From"
                // });
                // searchCustomer.on("Pleaseselect",function(jqinput){
                    // jqinput.val("");
                // });
                // searchCustomer.on("events.Itemclick",function(treeId,treeNode){
                // //    console.log(treeNode.data);
                    // //设置选择的参看仓库 ID
                    // v.defaultStore = treeNode.data;
                    // //触发calendar的查询方法
                    // //$('#calendar').fullCalendar("restorage_id",treeNode.data);
					// //v.showConfigButton(v.myStores,v.defaultStore);
                // });
                //searchCustomer.render();
				
				$.getJSON(Config.shipTo.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
                    //console.log(json);
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].data,"text":json[i].name});
					}
					$('#searchCustomer').immybox({
						//Pleaseselect:'<i>Clean Select</i>',
						choices: rls,
						Defaultselect: v.defaultStore,
						change:function(e,d){
                           if (d.value) {

						 //  console.log(d);
							//设置选择的参看仓库 ID
							v.defaultStore = d.value;
							//触发calendar的查询方法
						//	$('#calendar').fullCalendar("restorage_id",d.value);
							ckSearch();
							v.showConfigButton(v.myStores,v.defaultStore);
							}
						}
			
					});
				});
				//选择仓库触发查询
				function ckSearch(){
				var mydates=$('#calendar');
					var setdatas=mydates.data("DateTimePicker");
					var dates = setdatas.date().toDate().format("yyyy-MM-dd");
					var todate = {"dates" : dates};
					 v.selecDate = todate.dates;
					 var param = {"storage_id":v.defaultStore , "date":todate.dates};
					 v.initTime(param);
					 var _tdate = moment(todate.dates).format("MM/DD/YYYY");
					 //console.log(_tdate);
					 $("#appiontmentInfo").html(_tdate);
					 
					 $('#calendar').data("appointmentDate",_tdate);
					 todate.storage_id = $("#searchCustomer").attr("data-value");
					 v.todate = todate;
                //     console.log(_tdate);
					 v.listView({
						 "inputAppointmentTime": _tdate,
						 "searchFrom": $("#searchCustomer").attr("data-value")
					 });
				}


                var list;
                //列表.
                var listView = function (param) {
                    list = new CompondList(list_config, {
                        renderTo: "#appiontmentList",
                        dataUrl: Config.appiontmentList.url,
                        PostData: param
                    });
                    list.render();
                }
                
                


             //   $.getJSON(Config.appiontmentCalendarDataUrl.url, function (result) {})
             //直接点击时间的事件 不区分boundType
             var initTimeParam = {"storage_id":v.defaultStore, "date":v.getCurrentDate()  };
             this.initTime(initTimeParam);


             /*
                total Tab内时间的点击事件
             */
             //hyq has changed code as follows(2015-4-7)（整点时间与圆角事件互换）
             /*$(".timeTextAllbound").click(function(evt){
				
                 var dataId = $(this).data("id");
                 var appointmentDate = $('#calendar').data("appointmentDate");
                 if (appointmentDate) {
                 } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');   //没有选择日期则默认获取当天日期
                 }
				 
				 v.boundType =  dataId>19?"inbound":"outbound";
                 //var inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);

                //hyq has changed code as follows(2015-4-2)
                //选中时间(预约时间)必须大于当前时间
				if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
					$("#inputQuickAppointmentTime").val(inputAppointmentTime);
				} else {
					$("#inputQuickAppointmentTime").val("");
				}
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    //alert(33);
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    /*var d = new Dialog({
                            content: "Time can not be less than the current time!",
                            icon: 'question',
                            lock: true,
                            width: 200,
                            height: 40,
                            title: 'prompt',
                            cancelVal: 'close',
                            cancel: function () { }
                        }).show();

                    $("#inputQuickAppointmentTime").val("");
                    //alert("预约时间不能小于当前时间的准小时");
                }

                v.listView({
                    "inputAppointmentTime":inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value")
                });
             });*/
            $(".timeBadgeAllbound").click(function(evt){
                 var dataId = $(this).prev("a").data("id");
                 var appointmentDate = $('#calendar').data("appointmentDate");
                 if (appointmentDate) {
                 } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');   //没有选择日期则默认获取当天日期
                 }
                 
                 //hyq has changed code as follows(2015-4-7)(inbound\outboudn captial)
                 v.boundType =  dataId>19?"inbound":"outbound";
                 //v.boundType =  dataId>19?"Inbound":"Outbound";
                 //var inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);

                //hyq has changed code as follows(2015-4-2)
                /*//选中时间(预约时间)必须大于当前时间
                if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }*/
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    //alert(33);
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }

                v.listView({
                    "inputAppointmentTime":inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value")
                });
             });




             /*
                 Inbound Tab内时间的点击事件
                 html来自模板appiontmentCalendar.handlebars
              */
             /*$(".timeTextInbound").click(function(evt){
                var dataId = $(this).data("id");
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                   appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
				v.boundType = "inbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //hyq has changed code as follows(2015-4-2)
				/*if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
					$("#inputQuickAppointmentTime").val(inputAppointmentTime);
				} else {
					$("#inputQuickAppointmentTime").val("");
				}
                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }

				v.listView({
					"inputAppointmentTime": inputAppointmentTime,
					"searchFrom": $("#searchCustomer").attr("data-value"),
					"boundType" : "inbound"//入库
				});
            });*/
            $(".timeBadgeInbound").click(function(evt){
                var dataId = $(this).prev("a").data("id");
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                   appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
                v.boundType = "inbound";
                //v.boundType = "Inbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //hyq has changed code as follows(2015-4-2)
                /*if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }*/
                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }

                v.listView({
                    "inputAppointmentTime": inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value"),
                    "boundType" : "inbound"//入库
                    //"boundType" : "Inbound"//入库
                });
            });





            /*
                Outbound Tab内时间的点击事件
                html来自模板appiontmentCalendar.handlebars
            */
            /*$(".timeTextOutbound").click(function(evt){
                var dataId = $(this).data("id");
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
				
				v.boundType = "outbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //hyq has changed code as follows(2015-4-2)
				if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
					$("#inputQuickAppointmentTime").val(inputAppointmentTime);
				} else {
					$("#inputQuickAppointmentTime").val("");
				}

                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }

                v.listView({
                    "inputAppointmentTime": inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value"),
                    "boundType" : "outbound"  //出库
                });
            });*/
            $(".timeBadgeOutbound").click(function(evt){
                var dataId = $(this).prev("a").data("id");
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
                
                v.boundType = "outbound";
                //v.boundType = "Outbound";
                var inputAppointmentTime = appointmentDate;
                if (dataId<10) {
                    inputAppointmentTime = appointmentDate + " 0" + dataId + ":00";
                } else {
                    inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                }
                $("#appiontmentInfo").html(inputAppointmentTime);
                //hyq has changed code as follows(2015-4-2)
                /*if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }*/

                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    $("#inputQuickAppointmentTime").val("");
                }

                v.listView({
                    "inputAppointmentTime": inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value"),
                    "boundType" : "outbound"  //出库
                    //"boundType" : "Outbound"  //出库
                });
            });

            
            },
            clickTD: function (dataId) {
                var appointmentDate = $('#calendar').data("appointmentDate");
                if (appointmentDate) {
                } else {
                    appointmentDate = (new Date()).format('MM/dd/yyyy');
                }
                var inputAppointmentTime = appointmentDate;
                if (dataId) {
                    if (dataId<10) {
                        inputAppointmentTime = appointmentDate + "";
                    } else {
                        inputAppointmentTime = appointmentDate + " " + dataId + ":00";
                    }
                }
                //$("#inputQuickAppointmentTime").val(inputAppointmentTime);
                $("#appiontmentInfo").html(inputAppointmentTime);

                //hyq has changed code as follows(2015-4-2)
				/*if (inputAppointmentTime && (new Date(inputAppointmentTime)> new Date()) ) {
					$("#inputQuickAppointmentTime").val(inputAppointmentTime);
				} else {
					$("#inputQuickAppointmentTime").val("");
				}*/
                /*//hyq has add timetransfer as follows(2015-4-2)
                var nowtime = new Date();  //获取当前时间
                var year = nowtime.getFullYear();  //获取年份
                var month = nowtime.getMonth()+1;  //获取月份
                var day = nowtime.getDate();       //获取日
                var hours = nowtime.getHours();    //获取小时
                var newtime = month+"/"+day+"/"+year+" "+hours+":00";   //整合时间(当前时间取到整小时)
                //newtime = new Date(newtime);   //当前时间取到准小时*/
                //选中时间(预约时间)必须大于或等于当前时间准小时
                if (inputAppointmentTime && (new Date(inputAppointmentTime)>= moment(new Date()).format("MM/DD/YYYY HH:00")) ) {  //选择的时间大于等于当前时间准小时可以预约
                    $("#inputQuickAppointmentTime").val(inputAppointmentTime);
                } else {
                    /*var d = new Dialog({
                            content: "Time can not be less than the current time!",
                            icon: 'question',
                            lock: true,
                            width: 200,
                            height: 40,
                            title: 'prompt',
                            cancelVal: 'close',
                            cancel: function () { }
                        }).show();*/
                    $("#inputQuickAppointmentTime").val("");
                    //alert("预约时间不能小于当前时间的准小时");
                }

                v.listView({
                    "inputAppointmentTime":inputAppointmentTime,
                    "searchFrom": $("#searchCustomer").attr("data-value")
                });
            },
           initTime: function (param){

           // console.log(param);
                var v = this;

            //  param = {"storage_id":"1000005", "date":"2015-02-12"  };
                $.getJSON(Config.appiontmentCalendarTimeUrl.url, param , function (result) {


                    var data = result.DATA;
					var datat = result.TOTAL;
					//console.log(datat);
                    //alert(data);
                   // alert(datat);
					var ibound =0;
					var obound =0;
					for (var i=0;i<datat.length&& i<3;i++) {
						var ito = datat[i];
						if (ito) {
							if (ito.type=='inbound') {
								ibound = parseInt(ito.lentxt);
							} else if (ito.type=='outbound') {
								obound = parseInt(ito.lentxt);
							}
						}
					}
					$("#appiontmentCalendarTab").find("a[role='tab']").each(function(){
						if ($(this).hasClass("Total")) {
							$(this).html("Total <i style='color: rgb(75,211,100);font-style: normal;'>(" +(ibound+obound)+ ")</i>");
						}
						if ($(this).hasClass("InBound")) {
							$(this).html("InBound <i style='color: rgb(75,211,100);font-style: normal;'>("+ibound+")</i>");
						}
						if ($(this).hasClass("OutBound")) {
							$(this).html("OutBound <i style='color: rgb(75,211,100);font-style: normal;'>("+obound+")</i>");
						}
					});
                    /*
                    如果长度为0 说明没有返回值, 需要将角标清除...
                    */
                    $(".time").find("span").html(null);


                    //inbound鼠标浮在圆角数字上时鼠标样式控制
                    $(".timeBadgeAllbound").hover(function(){
                        //$(this).css("cursor","pointer");
                        if($(this).text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                $(this).css("cursor","pointer");
                            }
                        }else{     //圆角里是一个数字
                            $(this).css("cursor","pointer");
                        }
                    })
                    //inbound鼠标浮在圆角数字上时鼠标样式控制
                    $(".timeBadgeInbound").hover(function(){
                        //$(this).css("cursor","pointer");
                        if($(this).text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                $(this).css("cursor","pointer");
                            }
                        }else{     //圆角里是一个数字
                            $(this).css("cursor","pointer");
                        }
                    })
                    //Outbound鼠标浮在圆角数字上时鼠标样式控制
                    $(".timeBadgeOutbound").hover(function(){
                        //$(this).css("cursor","pointer");
                        if($(this).text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                $(this).css("cursor","pointer");
                            }
                        }else{     //圆角里是一个数字
                            $(this).css("cursor","pointer");
                        }
                    })




                    //hyq has add code(2015-4-3)
                    var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期
                    if (appointmentDate) {   //点击选择了日期
                    } else {   //没有选择日期默认是当前日期
                        appointmentDate = (new Date()).format('MM/dd/yyyy');  //此时时间是月日年
                    }
                    //alert(zgqx);

                    
                    $(".timeTextAllbound").each(function(){
                        //moment(new Date()).format("MM/DD/YYYY HH:00")
                        var chooseDate = moment(new Date(appointmentDate+" "+$(this).text())).format("MM/DD/YYYY HH:00");
                        //var date = new Date(appointmentDate+" "+$(this).text());

                        var currentDate = moment(new Date()).format("MM/DD/YYYY HH:00");
                        
                        if(chooseDate<currentDate){
                            $(this).css("color","grey");
                        }else{
                            $(this).css("color","#1479fb");
                        }
                    })
                    $(".timeTextInbound").each(function(){
                        //moment(new Date()).format("MM/DD/YYYY HH:00")
                        var chooseDate = moment(new Date(appointmentDate+" "+$(this).text())).format("MM/DD/YYYY HH:00");
                        //var date = new Date(appointmentDate+" "+$(this).text());

                        var currentDate = moment(new Date()).format("MM/DD/YYYY HH:00");
                        //alert(date);
                       // alert(chooseDate);alert(currentDate);
                        if(chooseDate<currentDate){
                            $(this).css("color","grey");

                        }else{
                            $(this).css("color","#1479fb");
                        }
                    })
                    $(".timeTextOutbound").each(function(){
                        //moment(new Date()).format("MM/DD/YYYY HH:00")
                        var chooseDate = moment(new Date(appointmentDate+" "+$(this).text())).format("MM/DD/YYYY HH:00");
                        //var date = new Date(appointmentDate+" "+$(this).text());

                        var currentDate = moment(new Date()).format("MM/DD/YYYY HH:00");
                        //alert(date);
                       // alert(chooseDate);alert(currentDate);
                        if(chooseDate<currentDate){
                            $(this).css("color","grey");
                        }else{
                            $(this).css("color","#1479fb");
                        }
                    })



                    //hyq has changed code as follows(2015-4-7)(整点时间事件与圆角事件互换)
                    //total圆角数字点击事件
                    /*$(".timeBadgeAllbound").click(function(){
                        //获取当前点击的整小时
                        var nowHour = $(this).prev("a").text();   
                        var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期

                        var rq="";
                        if (appointmentDate) {   //点击选择了日期
                        } else {   //没有选择日期默认是当前日期
                            appointmentDate = (new Date()).format('MM/dd/yyyy');
                        }
                        rq = appointmentDate;
                        var allTimeString = rq+" "+nowHour;
                        //var timeObj = new Date(allTimeString);   //时间格式化为标准时间
                        if($(this).text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                //hyq has chnaged code as follows(2015-4-2)
                                //$('#addAppiontment').trigger("click",allTimeString);
                                if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                                    //alert("预约时间不能小于当前时间的准小时");
                                }
                            }
                        }else{     //圆角里是一个数字
                            //$('#addAppiontment').trigger("click",allTimeString);
                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                            }
                        }
                    })*/
                    $(".timeTextAllbound").click(function(){
                        //获取当前点击的整小时
                        var nowHour = $(this).text();   
                        var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期

                        var rq="";
                        if (appointmentDate) {   //点击选择了日期
                        } else {   //没有选择日期默认是当前日期
                            appointmentDate = (new Date()).format('MM/dd/yyyy');
                        }
                        rq = appointmentDate;
                        var allTimeString = rq+" "+nowHour;
                        //var timeObj = new Date(allTimeString);   //时间格式化为标准时间
                        if($(this).next("span").text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).next("span").text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                //hyq has chnaged code as follows(2015-4-2)
                                //$('#addAppiontment').trigger("click",allTimeString);
                                if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content:"The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title: 'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title: 'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                                    //alert("预约时间不能小于当前时间的准小时");
                                }
                            }
                            //hyq has add code as follows(2015-4-10)(前面的数字等于后面的数字且是主管权限可以约车)
                            else if(preNum >= nextNum){
                                //主管权限判断
                                if (v.myStores.length>0) {  //有仓库
                                    var ls = v.myStores;
                                    //console.log(v.myStores);
                                    for (var idx=0;idx<ls.length;idx++) {
                                        var sitem = ls[idx];
                                        //if (sitem && sitem.POSTID==10 && sitem.DEPTID==1000008) {
                                        if (sitem && sitem.POSTID==10 && sitem.DEPTID==10000) {
                                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                        $('#addAppiontment').trigger("click",allTimeString);
                                    } else {
                                        var d = new Dialog({
                                            //content: "Time can not be less than the current time!",
                                            content:"The time you have selected has elasped the currect time",
                                            icon: 'question',
                                            lock: true,
                                            width: 200,
                                            height: 40,
                                            //title: 'Prompt',
                                            title: 'Warning!',
                                            cancelValue: 'Close',
                                            cancel: function () { }
                                        }).show();
                                        d.undelegateEvents();
                                        var d = new Dialog({
                                            //content: "Time can not be less than the current time!",
                                            content: "The time you have selected has elasped the currect time",
                                            icon: 'question',
                                            lock: true,
                                            width: 200,
                                            height: 40,
                                            //title: 'Prompt',
                                            title: 'Warning!',
                                            cancelValue: 'Close',
                                            cancel: function () { }
                                        }).show();
                                        //$("#inputQuickAppointmentTime").val("");
                                        //alert("预约时间不能小于当前时间的准小时");
                                    }
                                        }
                                    }
                                }
                            }

                        }
                        else{     //圆角里是一个数字
                            //$('#addAppiontment').trigger("click",allTimeString);
                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00")))) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title:'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title:'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                            }
                        }
                    })


                    //inbound圆角数字点击事件
                    /*$(".timeBadgeInbound").click(function(){
                        //获取当前点击的整小时
                        var nowHour = $(this).prev("a").text();   
                        var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期
                        var rq="";
                        if (appointmentDate) {   //点击选择了日期
                        } else {   //没有选择日期默认是当前日期
                            appointmentDate = (new Date()).format('MM/dd/yyyy');
                        }
                        rq = appointmentDate;
                        var allTimeString = rq+" "+nowHour;
                        //var timeObj = new Date(allTimeString);   //时间格式化为标准时间
                        if($(this).text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                //$('#addAppiontment').trigger("click",allTimeString);
                                if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                   // $("#inputQuickAppointmentTime").val("");
                            }
                        }
                        }else{     //圆角里是一个数字
                            //$('#addAppiontment').trigger("click",allTimeString);
                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                            }
                        }
                })*/
                $(".timeTextInbound").click(function(){
                        //获取当前点击的整小时
                        var nowHour = $(this).text();   
                        var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期
                        var rq="";
                        if (appointmentDate) {   //点击选择了日期
                        } else {   //没有选择日期默认是当前日期
                            appointmentDate = (new Date()).format('MM/dd/yyyy');
                        }
                        rq = appointmentDate;
                        var allTimeString = rq+" "+nowHour;
                        //var timeObj = new Date(allTimeString);   //时间格式化为标准时间
                        if($(this).next("span").text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).next("span").text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                //$('#addAppiontment').trigger("click",allTimeString);
                                if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title:'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title:'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                   // $("#inputQuickAppointmentTime").val("");
                            }
                        }
                        }else{     //圆角里是一个数字
                            //$('#addAppiontment').trigger("click",allTimeString);
                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title:'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title:'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                            }
                        }
                })

                
                    //outbound圆角数字点击事件
                    /*$(".timeBadgeOutbound").click(function(){
                        //获取当前点击的整小时
                        var nowHour = $(this).prev("a").text();   
                        var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期
                        var rq="";
                        if (appointmentDate) {   //点击选择了日期
                        } else {   //没有选择日期默认是当前日期
                            appointmentDate = (new Date()).format('MM/dd/yyyy');
                        }
                        rq = appointmentDate;
                        var allTimeString = rq+" "+nowHour;
                        //var timeObj = new Date(allTimeString);   //时间格式化为标准时间
                        if($(this).text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                //$('#addAppiontment').trigger("click",allTimeString);
                                if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                            }
                        }
                        }else{     //圆角里是一个数字
                            //$('#addAppiontment').trigger("click",allTimeString);
                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        content: "Time can not be less than the current time!",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        title: 'Prompt',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                   // $("#inputQuickAppointmentTime").val("");
                            }
                        }
                    })*/
                    $(".timeTextOutbound").click(function(){
                        //获取当前点击的整小时
                        var nowHour = $(this).text();   
                        var appointmentDate = $('#calendar').data("appointmentDate"); //获取选择好的日期
                        var rq="";
                        if (appointmentDate) {   //点击选择了日期
                        } else {   //没有选择日期默认是当前日期
                            appointmentDate = (new Date()).format('MM/dd/yyyy');
                        }
                        rq = appointmentDate;
                        var allTimeString = rq+" "+nowHour;
                        //var timeObj = new Date(allTimeString);   //时间格式化为标准时间
                        if($(this).next("span").text().match("/")){  //圆角里是两个数字
                            var appointmentCount = $.trim($(this).next("span").text()).split("/");
                            var preNum = parseInt(appointmentCount[0]);
                            var nextNum = parseInt(appointmentCount[1]);
                            if(preNum<nextNum){  //前面的数字小于后面的数字
                                //$('#addAppiontment').trigger("click",allTimeString);
                                if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title: 'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title: 'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    //$("#inputQuickAppointmentTime").val("");
                            }
                        }
                        }else{     //圆角里是一个数字
                            //$('#addAppiontment').trigger("click",allTimeString);
                            if (allTimeString && (new Date(allTimeString)>= new Date(moment(new Date()).format("MM/DD/YYYY HH:00"))) ) {
                                    $('#addAppiontment').trigger("click",allTimeString);
                                } else {
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title: 'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                    d.undelegateEvents();   //实例化清除
                                    var d = new Dialog({
                                        //content: "Time can not be less than the current time!",
                                        content: "The time you have selected has elasped the currect time",
                                        icon: 'question',
                                        lock: true,
                                        width: 200,
                                        height: 40,
                                        //title: 'Prompt',
                                        title: 'Warning!',
                                        cancelValue: 'Close',
                                        cancel: function () { }
                                    }).show();
                                   // $("#inputQuickAppointmentTime").val("");
                            }
                        }
                    })


                    //console.log(v.myStores);
                    var HH = moment(moment(new Date()).format("MM/DD/YYYY HH:00"),'MM/DD/YYYY HH:00').format("X");
                    $.each(data, function(i , e){
                      //  var badge = '<ul class="pull-right liitem" style="list-style-type:none;padding:0px;margin-right:20px;">';
                        //console.log(v.myStores);
                        var inboundBgColor = ' style="background-color: #FFF"; ';
                        var inboundHtml = ' &nbsp; ';
                        var inboundAvailable = '';
                        //console.log(e.SUMIN);
                        if((0+e.SUMIN)>0){
                            inboundBgColor = '';
                            inboundHtml = e.CNTIN //+ '/' + e.SUMIN;
                            inboundAvailable = 'inboundAvailable';
                        }
                        //   var inboundbadge ='<a data-id="'+ parseInt(e.ID,10) +'" href="#" '+inboundBgColor+' class="badge inbound pull-right '+inboundAvailable+'">' + inboundHtml + '</a>';
                        var inboundbadge = e.CNTIN || "";


                        var outboundBgColor = ' style="background-color: #FFF"; ';
                        var outboundHtml = ' &nbsp; ';
                        var outboundAvailable = '';
                        if( (0+e.SUMOUT)>0){
                            outboundBgColor = '';
                            outboundHtml = e.CNTOUT //+ '/' + e.SUMOUT;
                            outboundAvailable = 'outboundAvailable';
                        }
                      //  var outboundbadge = badge + '<li class="'+outboundAvailable+'" data-id="'+ parseInt(e.ID,10) +'"><a data-id="'+ parseInt(e.ID,10) +'" href="#" '+outboundBgColor+' class="badge outbound pull-right '+outboundAvailable+'">' + outboundHtml + '</a></li></ul>';
                        var outboundbadge = e.CNTOUT || "";


						
						
                        var totalboundBgColor = ' style="background-color: #FFF"; ';
                        var totalboundHtml = ' &nbsp; ';
                        var totalboundAvailable = '';
                        if( (0+e.SUMOUT)>0){
                            totalboundBgColor = '';
                            totalboundHtml = e.ALL + '/' + e.TOTAL;
                            totalboundAvailable = 'outboundAvailable';
                        }

                        //   var totalboundbadge = badge + '<li class="'+totalboundAvailable+'" data-id="'+ parseInt(e.ID,10) +'"><a data-id="'+ parseInt(e.ID,10) +'" href="#" '+totalboundBgColor+' class="badge totalbound pull-right '+totalboundAvailable+'">' + totalboundHtml + '</a></li></ul>';
                        var totalboundbadge =  e.TOTAL ? e.ALL + '/' + e.TOTAL : "";
                        //var totalboundbadge =  e.ALL + '/' + e.TOTAL;
                        var _id = parseInt(e.ID);
						
						var nday = moment(appointmentDate+" "+_id,"MM/DD/YYYY H").format("X");
                        
                        //hyq has changed code as follows(2015-4-7)
						/*if (nday>=HH) {
							if(totalboundbadge){   //整个圆角框里有值
								if(e.ALL >= e.TOTAL){  //前面的数字大于等于后面的数字
									$("#timeBadge"+ _id ).css("background","grey");
									$("#timeBadge"+ _id ).prev("a").css("color","grey");
									$("#timeBadgeInbound"+ _id ).css("background","grey");
									$("#timeBadgeInbound"+ _id ).prev("a").css("color","grey");
									$("#timeBadgeOutbound"+ _id ).css("background","grey");
									$("#timeBadgeOutbound"+ _id ).prev("a").css("color","grey");
								} else {
									//$("#timeBadge"+ _id ).css("background",null);
									$("#timeBadge"+ _id ).attr("style","cursor: pointer;");
									//$("#timeBadge"+ _id ).prev("a").css("color","#7C7C7C");
								}
							}else{   //整点时间后面没有圆角
								$("#timeBadge"+ _id ).prev("a").css("color","#7C7C7C");
							}
						} else {
							$("#timeBadge"+ _id ).css("background","grey");
							$("#timeBadge"+ _id ).prev("a").css("color","grey");
						}*/

                        if (nday>=HH) {
                            if(totalboundbadge){   //整个圆角框里有值
                                if(e.ALL >= e.TOTAL){  //前面的数字大于等于后面的数字
                                    //主管权限判断
                                    if (v.myStores.length>0) {  //有仓库
										var ls = v.myStores;
                                        //console.log(v.myStores);
                                        for (var idx=0;idx<ls.length;idx++) {
                                            var sitem = ls[idx];
                                            //if (sitem && sitem.POSTID==10 && sitem.DEPTID==1000008) {
                                            if (sitem && sitem.POSTID==10 && sitem.DEPTID==10000) {
                                                //hyq has changed code as follows(color)(2015-4-10)
                                                //$("#timeBadge"+ _id ).prev("a").css("color","#7C7C7C");
                                                //$("#timeBadgeInbound"+ _id ).prev("a").css("color","#7C7C7C");
                                                //$("#timeBadgeOutbound"+ _id ).prev("a").css("color","#7C7C7C");
                                                $("#timeBadge"+ _id ).prev("a").css("color","rgb(20, 121, 251)");
                                                $("#timeBadgeInbound"+ _id ).prev("a").css("color","rgb(20, 121, 251)");
                                                $("#timeBadgeOutbound"+ _id ).prev("a").css("color","rgb(20, 121, 251)");
                                            }else{
                                                $("#timeBadge"+ _id ).prev("a").css("color","grey");
                                                $("#timeBadgeInbound"+ _id ).prev("a").css("color","grey");
                                                $("#timeBadgeOutbound"+ _id ).prev("a").css("color","grey");
                                            }
                                        }
                                    }else{
                                        $("#timeBadge"+ _id ).prev("a").css("color","grey");
                                        $("#timeBadgeInbound"+ _id ).prev("a").css("color","grey");
                                        $("#timeBadgeOutbound"+ _id ).prev("a").css("color","grey");
                                    }
                                    $("#timeBadge"+ _id ).css("background","grey");
                                    $("#timeBadgeInbound"+ _id ).css("background","grey");
                                    $("#timeBadgeOutbound"+ _id ).css("background","grey");
                                } 
                                else {
                                    //$("#timeBadge"+ _id ).css("background",null);
                                    $("#timeBadge"+ _id ).attr("style","cursor: pointer;");
                                    //$("#timeBadge"+ _id ).prev("a").css("color","#7C7C7C");
                                }
                            }else{   //整点时间后面没有圆角
                                $("#timeBadge"+ _id ).prev("a").css("color","#7C7C7C");
                            }
                        }else {
                            $("#timeBadge"+ _id ).css("background","grey");
                            $("#timeBadge"+ _id ).prev("a").css("color","grey");
                            //hyq has add code as follows(2015-4-8)
                            $("#timeBadgeInbound"+ _id ).css("background","grey");
                            $("#timeBadgeInbound"+ _id ).prev("a").css("color","grey");
                            $("#timeBadgeOutbound"+ _id ).css("background","grey");
                            $("#timeBadgeOutbound"+ _id ).prev("a").css("color","grey");
                        }






                       /* var chooseDate = moment(new Date(appointmentDate+" "+$("#timeBadge"+ _id ).prev("a").text())).format("MM/DD/YYYY HH:00");
                        //var date = new Date(appointmentDate+" "+$(this).text());

                        var currentDate = moment(new Date()).format("MM/DD/YYYY HH:00");
                        //alert(date);
                       //alert(chooseDate);alert(currentDate);
                        if(chooseDate<currentDate){
                            $("#timeBadge"+ _id ).css("background","grey");
                            $("#timeBadge"+ _id ).prev("a").css("color","grey");

                        }else{
                            //hyq has add code about compare e.ALL with e.TOTAL
                            if(totalboundbadge){   //整个圆角框里有值
                                if(e.ALL >= e.TOTAL){  //前面的数字大于等于后面的数字
                                    alert(33);
                                    $("#timeBadge"+ _id ).css("background","grey");
                                    $("#timeBadge"+ _id ).prev("a").css("color","grey");
                                    $("#timeBadgeInbound"+ _id ).css("background","grey");
                                    $("#timeBadgeInbound"+ _id ).prev("a").css("color","grey");
                                    $("#timeBadgeOutbound"+ _id ).css("background","grey");
                                    $("#timeBadgeOutbound"+ _id ).prev("a").css("color","grey");

                                }
                            }else{   //整点时间后面没有圆角
                                $("#timeBadge"+ _id ).prev("a").css("color","grey");

                            }
                        }*/





                        $("#timeBadge"+ _id ).html(totalboundbadge);  //Total标题中表格列带的数字
                        $("#timeBadgeInbound"+ _id ).html(inboundbadge);  //InBound标题中表格列带的数字
                        $("#timeBadgeOutbound"+ _id ).html(outboundbadge);  //OutBound标题中表格列带的数字

                    });


                    
                });
            },

            initCal : function(){
                var v = this;

                var cssfileall = [
					"require_css!Font-Awesome",
					"require_css!oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker.css",
					"require_css!oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker-plus.css",
					"oso.lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
					"require_css!bootstrap.datetimepicker-css",
					"bootstrap.datetimepicker"
                ];
                require(cssfileall, function() {
                  $(function() {
                    var mydates=$('#calendar').Acars_datetimepicker({
                     inline: true,
                     icons:{
                     previous: 'fa fa-chevron-circle-left',
                     next: 'fa fa-chevron-circle-right'
                     },
                     toolbar:false,
                     format: 'YYYY-MM-DD',
                     tdcellclick:function(dates){
					  all = dates;
                             var todate = {"dates" : dates};
                             v.selecDate = todate.dates;
                             var param = {"storage_id":v.defaultStore , "date":todate.dates};
                             v.initTime(param);
                            // console.log(param);
                             var _tdate = moment(todate.dates).format("MM/DD/YYYY");
                          //   console.log(_tdate);
                             $("#appiontmentInfo").html(_tdate);
                             
                             $('#calendar').data("appointmentDate",_tdate);
                             todate.storage_id = $("#searchCustomer").attr("data-value");
                             v.todate = todate;
                             v.listView({
                                 "inputAppointmentTime": _tdate,
                                 "searchFrom": $("#searchCustomer").attr("data-value")
                             });
                     },
                    Initialization:function(setbuttondom){
                        //  setbuttondom.show();
                        v.showConfigButton( v.myStores , v.defaultStore , setbuttondom );
                        setbuttondom.click(function(){
                            v.defaultStore = $("#searchCustomer").attr("data-value");
							var para = "?storage_id="+ v.defaultStore; //jQuery.param(v.todate);
							var slide = new SlidePanel({
								url: Config.appiontementLimit.url + para,
								title: 'Appoiontment Config',
								duration: 500,
								topLine: 0,
								slideLine: "1000"
							});
							slide.open();
							//TODO
							window.addEventListener('message', function (e) {
								//console.log("dfdd");
								$(".slide-header-close").click();
								//$('#calendar').fullCalendar("restorage_id",v.defaultStore);
								v.listView();
							}, false);
                        });

                    }
                    
                   });

                

                    //外部取得时间
                   var setdatas=mydates.data("DateTimePicker");
                   var initDate = setdatas.date().toDate().format("yyyy-MM-dd");
              //    console.log(initDate);
              //    v.todate = initDate;


                  });
                  // window lod end



                });


                /*
                     var cssfileall = [ 
                                  "require_css!Font-Awesome",
                                   "require_css!oso.lib/fullcalendar/fullcalendar",
                                   "require_css!oso.lib/fullcalendar/style.calendar"
                                   //,"oso.bower/fullcalendar/dist/lang/zh-cn"
                                 ];

                                 requirejs(cssfileall, function(fullCalendar) {


                              $(function() {


                                    $('#calendar').fullCalendar({
                                       header: {
                                           left: 'prev,next today',
                                           center: '',
                                           right: 'config'
                                       },
                                       defaultDate: v.getCurrentDate(),
                                       editable: true,
                                       eventLimit: true,
                                        ringhtconfigclick:function() {
												v.defaultStore = $("#searchCustomer").attr("data-val");
												var para = "?storage_id="+ v.defaultStore; //jQuery.param(v.todate);
                                                var slide = new SlidePanel({
                                                    url: Config.appiontementLimit.url + para,
                                                    title: 'Appoiontment Config',
                                                    duration: 500,
                                                    topLine: 0,
                                                    slideLine: "1000"
                                                });
                                                slide.open();
                                                //TODO
                                                window.addEventListener('message', function (e) {
                                                    //console.log("dfdd");
                                                    $(".slide-header-close").click();
													//$('#calendar').fullCalendar("restorage_id",v.defaultStore);
                                                    v.listView();
                                                }, false);
												
                                            if (v.todate) {
                                                
                                            } else {
                                                var d = new Dialog({
                                                    content: "please select date!",
                                                    icon: 'question',
                                                    lock: true,
                                                    width: 200,
                                                    height: 40,
                                                    title: 'alert',
                                                    cancelVal: 'close',
                                                    cancel: function () { }
                                                }).show();
                                            }
                                        },
                                        tdcellclick : function(todate){
                                            //每单击td事件回调反回todate--年月日   2015-02-12
                                              console.log(todate.dates);
											v.selecDate = todate.dates;
                                            var param = {"storage_id":v.defaultStore , "date":todate.dates};
                                            v.initTime(param);
                                            var _tdate = (new Date(todate.dates)).format("MM/dd/yyyy");
                                            //console.log(_tdate);
                                            $("#appiontmentInfo").html(_tdate);
                                            
                                            $('#calendar').data("appointmentDate",_tdate);
                                            todate.storage_id = $("#searchCustomer").attr("data-val");
                                            v.todate = todate;
                                            v.listView({
                                                "inputAppointmentTime": _tdate,
                                                "searchFrom": $("#searchCustomer").attr("data-val")
                                            });
                                       },
										events:[],
										selectTdbgcolor: "#cccccc",
										Initialization:function(){
											$("#calendar .fc-config-button").hide();
											v.showConfigButton(v.myStores,v.defaultStore);
										}
                                   });
									//v.showConfigButton(v.myStores,v.defaultStore);
                            

                            //    NProgress.done();  
                        


                                      });
                                    


                              });*/


            },
			showConfigButton: function(ls,defaultStore,setbuttondom) {
			//	$("#calendar .fc-config-button").hide();
        //    setbuttondom.show();
				if (ls.length>0) {
					for (var idx=0;idx<ls.length;idx++) {
						var sitem = ls[idx];
						if (sitem && sitem.POSTID==10 && sitem.DEPTID==1000008) {
							//$("#calendar .fc-config-button").show();
                            setbuttondom.show();
							//console.log("show");
						}
					}
				}
			},
            advancedSearchInit: function () {
                var v = this;
                $("#searchAdvancedTab").html(templates.advancedSearch().trim());
				
				$.getJSON(Config.shipTo.url+"&type=1&lv=0&rootType=Ship+From&Reqnumr="+Date.parse(new Date()), function (json) {
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].data,"text":json[i].name});
					}
					$('#searchFrom').immybox({
						Pleaseselect:'<i>Clean Select</i>',
						choices: rls
			
					});
				});
				
/*
                var fromTree = new AsynLoadQueryTree({
                    renderTo: "#searchFrom",
                    PostData: { rootType: "Ship From" },
                    dataUrl: Config.shipTo.url,
                    scrollH: 400,
                    multiselect: false,
                    Async: true,
                    placeholder: "Ship From"
                });
                fromTree.render();
*/	
				/*
                var CarrierTree = new AsynLoadQueryTree({
                    renderTo: "#inputCarrier",
                    PostData: { rootType: "Ship From" },
                    dataUrl: Config.carrierJson.url,
                    scrollH: 400,
                    multiselect: false,
                    Async: true,
                    placeholder: "Ship From"
                });
                CarrierTree.render();
				*/
				$.getJSON(Config.carrierJson.url, function (result) {     //advancesearch中scac值获取
                    //hyq has add code（2015-4-3）(简称和全称连接)
                    $.each(result,function(index,obj){
                        obj.text = obj.value + " - "+ obj.text;    //简称和全称连接
                    })
                    //hyq has changed code as follows(2015-4-3)
                    /*$('#inputCarrier').immybox({
						Pleaseselect:'<i>Clean Select</i>',
                        choices: result,
						textname:"value",
						valuename:"value"
                    });*/
                    $('#inputCarrier').immybox({
                        Pleaseselect:'<i>Clean Select</i>',
                        choices: result,
                        textname:"text",
                        valuename:"value"
                    });

                    //$("#inputCarrier").attr("data-value", "all").val("All Type");
                });

                $.getJSON(Config.orderType.url, function (result) {
                    $('#selectOrderType').immybox({
						//Pleaseselect:'<i>Clean Select</i>',
                        choices: result,
						change: function(e,d){
							if (d.value=="order") {
								$("#inputOrder").removeAttr("readonly");
								$("#inputOrder").attr("placeholder","Order NO.");
							} else if (d.value=="load") {
								$("#inputOrder").removeAttr("readonly");
								$("#inputOrder").attr("placeholder","Load NO.");
							} else {
								$("#inputOrder").attr("readonly","true");
								$("#inputOrder").attr("placeholder","");
								//$("#inputOrder");
							}
							//console.log(d.value);
						}
                    });
                    $("#selectOrderType").attr("data-value", "all").val("All Type");
                });
                

                
            },   //高级搜索.
            luceneSearchInit:function(){
                $("#searchAppiontmentTab").html(templates.luceneSearch().trim());
            },
            quickCreateInit: function () {
                /*var CarrierTree = new AsynLoadQueryTree({
                    renderTo: "#inputQuickCarrier",
                    PostData: { rootType: "Ship From" },
                    dataUrl: Config.carrierJson.url,
                    scrollH: 400,
                    multiselect: false,
                    Async: true,
                    placeholder: "Ship From"
                });
                CarrierTree.render();
				*/
				$.getJSON(Config.carrierJson.url, function (result) {
                    //hyq has change code as follows(2015-4-3)
					/*$('#inputQuickCarrier').immybox({
						Pleaseselect:'<i>Clean Select</i>',
						textname:"value",
                        valuename:"value",
						choices: result
					});*/
                    $.each(result,function(index,obj){
                        obj.text = obj.value + " - "+ obj.text;    //简称和全称连接
                    })
                    $('#inputQuickCarrier').immybox({
                        Pleaseselect:'<i>Clean Select</i>',
                        textname:"text",
                        valuename:"value",
                        choices: result
                    });

				});
                
                $("#inputQuickAppointmentTime").css("cursor","pointer");
                $("#inputQuickAppointmentTime").datetimepicker({ startDate: new Date(), autoclose: 1, minView: 1,forceParse: 0, format: 'mm/dd/yyyy hh:00', bgiconurl:""});

                $('.quick-create').on('click', function (e) {
                    e.stopPropagation();
                    return false;
                });
            },
            //获取当前日期 2015-02-13
            getCurrentDate:function (){
                var myDate = new Date();
                //myDate.getYear();        //获取当前年份(2位)
                var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
                var month = myDate.getMonth();       //获取当前月份(0-11,0代表1月)
                var date = myDate.getDate();        //获取当前日(1-31)
                //myDate.getDay();         //获取当前星期X(0-6,0代表星期天)
                //myDate.getTime();        //获取当前时间(从1970.1.1开始的毫秒数)
                var hours =myDate.getHours();       //获取当前小时数(0-23)
                var minutes = myDate.getMinutes();     //获取当前分钟数(0-59)
                var seconds = myDate.getSeconds();     //获取当前秒数(0-59)
                //myDate.getMilliseconds();    //获取当前毫秒数(0-999)
                //myDate.toLocaleDateString();     //获取当前日期
                //var mytime=myDate.toLocaleTimeString();     //获取当前时间
                //myDate.toLocaleString( );        //获取日期与时间
                month++;
                if(month<=9)month="0"+month;
                if(date<=9)date="0"+date;
                return year + "-" +month+ "-" +date;// +" "+hours+ ":" +minutes+ ":" +seconds
            }
        });
    });
