﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "immybox", "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree", "bootstrap.datetimepicker","require_css!bootstrap.datetimepicker-css"], function ($, Backbone, handlebars, templates, CompondBox, immybox, AsynLoadQueryTree, datetimepicker) {
    return Backbone.View.extend({
        el: "#containerAddCar",
        template: templates.v3AddCNTR,
        render: function () {
            var v = this;
            var temp = new CompondBox({
                content: v.template().trim(),
                title: "Appiontment",
                container: v.el,
                isExpand: true
            });

            $("#inputAddAppointmentTime").datetimepicker({
						autoclose:1,
				        minView: 2,
				        forceParse: 0,
						format: 'mm-dd-yyyy hh:ii'
					});
            $("#inputAddETD").datetimepicker({ 
						autoclose:1,
				        minView: 2,
				        forceParse: 0,
						format: 'mm-dd-yyyy hh:ii'
					});
            $("#inputAddETA").datetimepicker({ 
						autoclose:1,
				        minView: 2,
				        forceParse: 0,
						format: 'mm-dd-yyyy hh:ii'
					});
            v.init();
			
			$.getJSON("./json/appiontmentType.json", function (json) {
				console.log(json);
				$('#inputAddType').immybox({
					//Pleaseselect:'Clean Select',
					choices: json
				});
			});

        },
        init: function () {
            var fromTree = new AsynLoadQueryTree({
                renderTo: "#inputAddCarrier",
                dataUrl: "./json/carrier.json",
                scrollH: 400,//多高后出现滚动条
                Async: false,//非异步获取树节点，（一次加载完成）
                multiselect: false,//多选
                PostData: { "a": "1", "b": "2" },//异步时请求每个节点时往服务端传值,
                placeholder: "Please input appointment HUB",//默认input框值
                Parentclick: true//开启父节点可以选中
            });
            fromTree.render();
            fromTree.on("events.Itemclick", function (treeId, treeNode, treeNodeAll) {
                //console.log(treeNodeAll);
                $("#inputAddCarrierLinkman").val(treeNodeAll.linkman);
                $("#inputAddCarrierTel").val(treeNodeAll.linkman);
            });
        }
    });
});