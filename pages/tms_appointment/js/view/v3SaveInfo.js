﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "art_Dialog/dialog-plus"], 
function ($, Backbone, handlebars, templates, Dialog) {
    return Backbone.View.extend({
        render: function () {
            var v = this;
            $("#saveAppiontment").on("click", function () {
                if (v.verification()) {
                    var h = $("<div class='bol-body form-horizontal' role='form'>"
                        + "<div class='form-group'>"
                        + "<label class='col-sm-5 control-label'>Appointment HUB</label>"
                        + "<div class='col-sm-7'>"
                        + "    <p class='form-control-static'>" + $("#inputAddWareHourse").val() + "</p>"
                        + "</div></div>"
                        + "<div class='form-group'>"
                        + "<label class='col-sm-5 control-label'>SCAC</label>"
                        + "<div class='col-sm-7'>"
                        + "    <p class='form-control-static'>" + $("#inputAddCarrier").val() + "</p>"
                        + "</div></div>"
                        + "<div class='form-group'>"
                        + "<label class='col-sm-5 control-label'>Appointment</label>"
                        + "<div class='col-sm-7'>"
                        + "    <p class='form-control-static'>" + $("#inputAddAppointmentTime").val() + "</p>"
                        + "</div></div>"
                        + "</div>");

                    var d = Dialog({
                        title: "Confirm save",
                        width: 400,
                        height: 180,
                        lock: true,
                        opacity: 0.6,
                        content: h,
                        okValue: "Save",
                        ok: function () {
                            //console.log("保存");
                            v.save();
                        },
                        cancelValue: 'Cancel',
                        cancel: function () {
                        }
                    });
                    d.reset();
                    d.showModal();
                }
            });
        },
        save: function () {
            console.log("save");
        },
        verification: function () {
            var f = true;
            //console.log($("#inputAddWareHourse").val());
            if ($("#inputAddWareHourse").val() == "" | $("#inputAddWareHourse").val() == undefined) {
                if (!$("#inputAddWareHourse").parent().hasClass("has-feedback")) {
                    $("#inputAddWareHourse").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                }
                f = false;
            } else {
                $("#inputAddWareHourse").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            if ($("#inputAddCarrier").val() == "" | $("#inputAddCarrier").val() == undefined) {
                if (!$("#inputAddCarrier").parent().hasClass("has-feedback")) {
                    $("#inputAddCarrier").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                }
                f = false;
            } else {
                $("#inputAddCarrier").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            //console.log($("#inputAddAppointmentTime").val());
            if ($("#inputAddAppointmentTime").val() == "" | $("#inputAddAppointmentTime").val() == undefined) {
                if (!$("#inputAddAppointmentTime").parent().hasClass("has-feedback")) {
                    $("#inputAddAppointmentTime").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                }
                f = false;
            } else {
                $("#inputAddAppointmentTime").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }
            return f;
        }
    });
});