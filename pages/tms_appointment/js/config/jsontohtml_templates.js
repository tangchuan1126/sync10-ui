﻿define(["JSONTOHTML","../../json/B2BOrderKey"],
function (JSONTOHTML,B2BOrderKey) {
    var _table = {
        thead: [{
            tag: "table",
            class: "checkin_box",
            children: [{
                tag: "thead",
                children: [{
                    tag: "tr",
                    html: function (obj, index) {
                        var thdata = obj.children[0].children;
                        return (JSONTOHTML.transform(thdata.children, _table.th));
                    }
                }]
            },
			{
			    tag: "tbody",
			    html: ""
			}]
        },
		{
		    tag: "div",
		    class: "tfootpage",
		    html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		},
		{
		    tag: "div",
		    class: "SelectMorebox"
		}],
        th: {
            tag: "th",
            html: "${title}"
        }
    };
    var _tbody = {
        head: null,
        int: function (jsons, View_control) {
            var headallobj = View_control.head;
            var footer = View_control.footer;
            _tbody.head = headallobj;
            var trfooterdata = {
                "colspan": headallobj.length,
                "children": footer,
                "trid": ""
            };
            var pageSize = jsons.PAGECTRL.pageSize;
            var __tr = _tbody.TotrJson(jsons.DATA, pageSize);
            var __trhtml = "";
            for (var trkey in __tr) {

                var _tritmes = __tr[trkey];


                __trhtml += (JSONTOHTML.transform(_tritmes, _tbody.tr));
                trfooterdata["trid"] = _tritmes.trid;
                __trhtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
            }
            return __trhtml;
        },
        TotrJson: function (DATA, pageSize) {
            var headallobj = _tbody.head;
            var __tr = [];
            for (var tri = 0; tri < DATA.length; tri++) {
                var tritme = DATA[tri];
                var __td = [];
                for (var headkey in headallobj) {
                    var tritmes = headallobj[headkey];
                    var Rowname = tritmes.field;
                    var Rowobj = {};
                    if (typeof Rowname == "object") {
                        __td.push({
                            Options: Rowname
                        });
                    } else {
                        Rowobj = eval("tritme." + Rowname);
                        var setobjs = eval("({" + Rowname + ":Rowobj})");
                        __td.push(setobjs);
                    }
                }
                __tr.push({
                    "trid": tritme.trid,
                    "children": __td
                });
                if (tri >= pageSize) {
                    break;
                }
            }
            return __tr;
        },
        keyname: function (obj) {
            var key = "";
            for (var rowkey in obj) {
                key = rowkey;
            }
            return key;
        },
        tr: {
            "tag": "tr",
            "id": function (obj) {
                //console.log(obj);
                return obj.trid;
            },
            "data-itmeid": function (obj) {
                return "trid|" + obj.trid;
            },
            "class": "tr_itme_style",
            "html": function (obj, index) {
                return (JSONTOHTML.transform(obj.children, _tbody.td));
            }
        },
        trfooter: {
            tag: "tr",
            class: "TFOOTbutton",
            html: function (obj, index) {
                var trhtmls = "";
                trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
                trhtmls += '<div data-itmeid="trid|' + obj.trid + '" class="buttons-group">';
                var ahtml = "";
                //console.log(obj);
                for (var akey in obj.children) {
                    var itmea = obj.children[akey];
                    ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
                }
                trhtmls += ahtml;
                trhtmls += '</div></td>';
                return trhtmls;
            }
        },
        td: {
            tag: "td",
            width: function (obj, index) {
                var width = "20%";
                switch (index) {
                    case 0:
                        width = "25%";
                        break;
                    case 1:
                        width = "30%";
                        break;
                    case 2:
                        width = "25%";
                        break;
                    case 3:
                        width = "20%";
                        break;
                }
                return width;
            },
            class: function (obj, index) {
                var classname = "";
                var keynames = _tbody.keyname(obj);
                classname = "td_" + keynames;
                return classname;
            },
            valign: function (obj, index) {
                return "center";
            },
            html: function (obj, index) {
                var retuhtml = "没找到模版";
                var keynames = _tbody.keyname(obj);
                
                if (keynames != "" && typeof keynames != "undefined") {
                    if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
                        retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
                    }
                }
                return retuhtml;
            }
        },
        Appiontment: {
            tag: "fieldset",
            children: [{
                tag: "legend",
                html: function (obj, index) {
                    var legendhtml = "";
                    legendhtml = '<span class="Sub_trailer"></span>';
                    /*if (obj.Appiontment.STATE != "有效") {
                        legendhtml += '<i>|</i><span class="deliverystyle">状态:' + obj.Appiontment.STATE + '</span>';
                    }*/
                    return legendhtml;
                }
            },
			{
			    tag: "div",
			    class: "Tractor_ulitme",
			    children: [{
			        tag: "ul",
			        html: function (obj, index) {
			            return (JSONTOHTML.transform(obj.Appiontment.CHILDREN, _tbody.AppiontmentInfoLi));
			        }
			    }]
			}]
        },
        AppiontmentInfoLi: {
            tag: "li",
            html: function (obj, index) {
                return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle logText" title='+obj.VALUE+'>' + obj.VALUE + '</span>';
            }
        },
        Truck: {
            tag: "fieldset",
            class: "Time",
            children: [{
                tag: "legend",
                class:"td_legend",
                html: function (obj, index) {
                    var legendhtml = "";
                    legendhtml = '<span class="E_DLO_ID">SCAC:' + obj.Truck.CARRIER + '</span>';
                 //   return legendhtml;
                }
            }, {
                tag: "div",
                class: "Tractor_ulitme",
                children: [{
                    tag: "ul",
                    html: function (obj, index) {
                        //console.log(obj);
                        obj.Truck.CHILDREN.push({"LABLE":"SCAC","VALUE" : obj.Truck.CARRIER});
                        return (JSONTOHTML.transform(obj.Truck.CHILDREN, _tbody.TruckInfoLi));
                    }
                }]
            }
            ]
        },
        TruckInfoLi: {
            tag: "li",
            html: function (obj, index) {
                return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle logText" title='+obj.VALUE+'>' + obj.VALUE + '</span>';
            }
        },
        Invoice: {
            tag: "div",
            class:"tbInnerInvoice",
            html: function (obj, index) {
                return (JSONTOHTML.transform(obj.Invoice, _tbody.Invoice2Field));
            }
        },
        Invoice2Field: {
            tag: "fieldset",
            children: [{
                tag: "legend",
                class: "td_legend",
                html: function (obj, index) {
                    var legendhtml = "";
                    // if (obj.InvoiceTYPE=='Order') {
					if (obj.InvoiceTYPE=='Bol') {
                        // legendhtml = '<span class="E_DLO_ID">Order:' + obj.InvoiceID + '</span>';
						legendhtml = '<span class="E_DLO_ID">Bol:' + obj.InvoiceID + '</span>';
					} else {
						legendhtml = '<span class="E_DLO_ID">' + obj.InvoiceTYPE + ':' + obj.InvoiceID + '</span>';
                    }
					if(obj.EntryID){
                        legendhtml += '<span class="ring_title"><em>EntryID:' + obj.EntryID + '</em></span>';
                    }
                    return legendhtml;
                }
            }, {
                tag: "div",
                class: "Tractor_ulitme",
                children: [{
                    tag: "ul",
                    html: function (obj, index) {
                        return (JSONTOHTML.transform(obj.CHILDREN, _tbody.Invoice3Li));
                    }
                }]
            }]
        },
        Invoice3Li: {
            tag: "li",
            html: function (obj, index) {
                /*
                    将 状态 字段的key值转为方便识别的词
                    
                    eval("B2BOrderKey[" + obj.VALUE +"]['en']")
                */
                if(obj.LABLE.toLowerCase() == "status") {
                    obj.VALUE = eval("B2BOrderKey['" + obj.VALUE +"']['en']");
                }
                //return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
                return '<span><span class="namestyle" style="width:90px">' + obj.LABLE + ':</span><span class="valstyle logText" style="width:58px;" title='+obj.VALUE+'>' + obj.VALUE + '</span></span>';
            }
        },
        Log: {
            tag: "div",
            class:"Log_container",
            html: function (obj, index) {
                var ulsring = "";
                var arryjson = obj.Log.LOGDETAILS;
                var box1px = "<span class='box1px'></span>";
                if (arryjson != "" && typeof arryjson != "undefined") {

                    for (var _key = 0; _key < arryjson.length; _key++) {

                        var arryitmes = arryjson[_key];

                        var ul = '<ul><li><span>' + arryitmes.USER + '</span><span class="valuestyle" data-eventname="events.moreOption">' + (arryitmes.InvoiceType=="Order"?"Order":arryitmes.InvoiceType) + ':' + arryitmes.InvoiceID + '</span></li>'+
						'<li><span class="remarkStyle" style="width:124px;" title="'+arryitmes.REMARK+'">'+arryitmes.REMARK+'</span></li>'+
						'<li><span>' + arryitmes.DATETIME + '</span></li>'+
						'</ul>';
                        if (ulsring != "" && typeof ulsring != "undefined") {
                            if (_key > 1) {
                                ulsring = ulsring + '<div class="animated trHidden">' + box1px + ul + "</div>";
                            } else {
                                ulsring = ulsring + box1px + ul;
                            }
                        } else {
                            ulsring = ul;
                        }
                        
                    }

                    if (arryjson.length > 2) {    //如果条数大于2则显示出More
                        ulsring += '<label>';
                        ulsring += '<span data-eventname="events.moreOption" class="Morestyle">More...</span>';
                        ulsring += '</label>';
                    }
                }
                return ulsring;

            }
            
        },
        "ORDER": {
            tag: "div",
            class: "Tractor_ulitme",
            children: [{
                tag: "div",
                class: "Tractor_ulitme",
                html: function (obj, index) {
                    return (JSONTOHTML.transform(obj.ORDER.CHILDREN, _tbody.fieldset2divulli));
                }
            }]
        },
        fieldset2divulli: {
            tag: "fieldset",
            class: "ORDER",
            children: [{
                tag: "legend",
                html: function (obj, index) {
                    var legendhtml = "";
                    legendhtml = '<span class="Sub_order">' + obj.ORDERID + '</span>';
                    return legendhtml;
                }
            },
			{
			    tag: "div",
			    class: "Tractor_ulitme",
			    children: [{
			        tag: "ul",
			        html: function (obj, index) {
			            return (JSONTOHTML.transform(obj.CHILDREN, _tbody.fieldset3divulli));
			        }
			    }]
			}]
        },
        fieldset3divulli: {
            tag: "li",
            html: function (obj, index) {
                return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
            }
        },
        OPTION: {
            tag: "div",
            class: "Tractor_ulitme",
            children: [{
                tag: "ul",
                html: function (obj, index) {
                    return (JSONTOHTML.transform(obj.OPTION.CHILDREN, _tbody.Options));
                }
            }]
        },
        Options: {
            tag: "li",
            html: function (obj, index) {
                return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
            }
        },
        updatatr: function (jsondata, trid) {
            var tritmeobj = $(trid);
            var tritmedata = [];
            tritmedata.push(jsondata);
            var uptrdata = _tbody.TotrJson(tritmedata, 1);
            //console.log(uptrdata[0].children);
            tritmeobj.empty().html(JSONTOHTML.transform(uptrdata[0].children, _tbody.td));
        }
    }
    return {
        tbody: _tbody,
        table: _table
    }
});