﻿define(["../config/jsontohtml_templates"], function (tmp) {

    return {
        "View_control": {
            "head": [{
                "title": "Appointment",
                "field": "Appiontment"
            }, {
                //"title": "Truck",   //hyq hs changed code(2015-4-13)
                "title": "Carrier",
                "field": "Truck"
            }, {
                "title": "Invoice",
                "field": "Invoice"
            }, {
                "title": "Log",
                "field": "Log"
            }],
            "footer": [
				[
					"events.Edit", "Edit"
				], [
					"events.UpdateAppiontmentDate", "Delete"
				]
            ],
            "templates": tmp
        }
    }

});