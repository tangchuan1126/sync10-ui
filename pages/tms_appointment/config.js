﻿(function () {
    var configObj = {
        appiontmentList: {  //列表
            url: "/Sync10/_tms/getAppointmentList"//url: "/Sync10/ui/pages/WMSAppointment/json/appiontmentList.json"
        },
        createAppiontement: {   //创建
//          url: "/Sync10-ui/pages/appointment/add.html"
            url: "/Sync10-ui/pages/tms_appointment/add.html"
        },
		appiontementLimit: {
			url: "/Sync10-ui/pages/appointmentLimit/index.html"
		},
        addAppointment: {
            url: "/Sync10/_tms/addAppointment"
        },
        delAppiontment: {   //取消约车
            url: "/Sync10/_tms/delAppointment"
        },
        getAppointmentInfo: {//获取约车信息.
            url: "/Sync10/_tms/editAppointment"
        },
        getLoginStorage: {
            url: "/Sync10/_tms/getLoginStorageCatalog"
        },
        getSingleInvoiceInfo: { //根据单据ID获取单据相关信息，返回json数据和loadlist、orderlist等信息格式一样.
            url: "/Sync10/_tms/getInviceByTypeId",
        },
        getLoadList: {  //load列表
            url: "/Sync10/_tms/getAppLoadList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        getOrderList: { //order列表
                url: "/Sync10/_tms/getAppOrderList"//url: "/Sync10-ui/pages/TMSAppiontment/json/a.json"
        },
        getPoList: {    //po列表
            url: "/Sync10/_tms/getAppPoList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },
        getBOLList: {    //bol列表
            url: "/Sync10/_tms/getBolList" //url: "/Sync10/ui/pages/WMSAppointment/json/a.json"
        },

        shipTo: {   //仓库地址json
//            url: "/Sync10-ui/pages/appointment/json/shipTo.json"
            url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        },
        carrierJson: {  //卡车信息json
            url: "/Sync10-ui/pages/appointment/json/carrier.json"
        },
        orderType: {    //单据类型.
            url: "/Sync10-ui/pages/appointment/json/orderType.json"
        },


        orderpoType: {    //单据类型.
            url: "/Sync10-ui/pages/appointment/json/orderpoType.json"
        },

        appiontmentCalendarTimeUrl: {    //calendar-time 入库出库数据
          // url: "/Sync10-ui/pages/appointment/json/appiontmentCalendar.json"
           url: "/Sync10/_tms/sumWorkByDate"
        },
        calendarSumAppointmentUrl:{
           url: "/Sync10/_tms/sumWorkByStartEnd"
        },
		adminSession: {
			url: "/Sync10/_fileserv/redis_session"
		}
    };
    define(configObj);
}).call(this);