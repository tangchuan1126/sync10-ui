/*!
 * 
 * author: lixf
 * date: 2015-6-16
 */
"use strict"
define([
	"../config",
    "jquery",
    "backbone"
],function(config, $, Backbone){
	var extAttrValues = Backbone.Model.extend({
		url:config.productDefine.getExtAttrValuesUrl,
		idAttribute: "v_id",
		defaults:{
			attr_id:"",
			item_name:"",
			item_value:"",
			sort:""	
		}
	});

	//extAttrValues  集合
	var extAttrValuesCollection = Backbone.Collection.extend({
        url: config.productDefine.getExtAttrValuesUrl,
        model: extAttrValues,
        parse: function (response) {
            return response.attrvalues;
        },
        comparator : function(m1, m2) {  
	        var sort1 = m1.get('sort');  
	        var sort2 = m2.get('sort');  
	  
	        if(sort1 > sort2) {  
	            return 1;  
	        } else {  
	            return 0;  
	        }
        }  
    });
	
	//搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            searchConditions: ""
        }
    });
	
	return {
		ExtAttrValues:extAttrValues,
		ExtAttrValuesCollection: extAttrValuesCollection,
   	 	SearchModel:SearchModel
	}
});