/*!
 * 
 * author: lixf
 * date: 2015-6-16
 */
"use strict"
define([
	"../config",
    "jquery",
    "backbone"
],function(config, $, Backbone){
	var extAttr = Backbone.Model.extend({
		url : config.productDefine.addExtAttrUrl,
		idAttribute: "attr_id",
		defaults: {
			attr_name: "",
			display_type: "",
			description: "",
			attrValues: []
		}
	});
	
	return {
		ExtAttr:extAttr
	}
});