/*!
 * 
 * author: lixf
 * date: 2015-6-16
 */
"use strict";
define([
	"../config", 
	"jquery", 
	"backbone", 
	"handlebars", 
	"handlebars_ext" 
],
function(config, $, Backbone, Handlebars, HandlebarsExt) {

	var ExtAttrModel = Backbone.Model.extend({
		url: config.productDefine.getExtAttrUrl,
		idAttribute: "attr_id",
		defaults: {
			attr_id: "",
			attr_name: "",
			display_type: "",
			description: "",
			attrValues: []
		},
	});

	var ExtAttrCollection = Backbone.Collection.extend({
		model: ExtAttrModel,
		url: config.productDefine.getPagingExtAttrsUrl,

		comparator: function(item) {

		},
		parse: function(response) {
			if (response.pagectrl) {
				ExtAttrCollection.pageCtrl = response.pagectrl;
			}
			return response.extattrs;
		},
		initialize: function() {

		}
	}, 
	{
		pageCtrl: {
			pageNo: 1,
			pageSize: 10
		}
	});
	
	//搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            attr_name: "",
            display_type: "",
            status:"0",
            pageNo:"",
            cmd:""
        }
    });

	return {
		ExtAttrModel: ExtAttrModel,
		ExtAttrCollection: ExtAttrCollection,
		SearchModel: SearchModel
	};
});