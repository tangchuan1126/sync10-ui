/*!
 * author: lixf
 * date: 2015-6-11
 */
"use strict"
define([
	"jquery",
	"backbone",
	"handlebars",
	"view/extAttrSearchView",
	"view/extAttrSearchResultView",
	"view/modExtAttrValuesView",
	"view/delExtAttrView",
	"blockui",
	"jqueryui/tabs"
],function(
	$
    ,Backbone
    ,Handlebars
    ,ExtAttrSearch
    ,extAttrSearchResult
    ,ModExtAttr
    ,DelExtAttr
){
	(function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();

    $("#all_tabs").tabs();

    //搜索
    var extAttrSearchView = new ExtAttrSearch({el:"#search_extattr"});
    //搜索结果
    var extAttrSearchResultView = new extAttrSearchResult({el:"#search_result"});
    //删除
    var delExtAttrView = new DelExtAttr();
    //修改
    var modExtAttrView = new ModExtAttr({el:"#modBasicBox"});

    extAttrSearchResultView.setView({
       delExtAttrView:delExtAttrView,
       modExtAttrView:modExtAttrView,
       extAttrSearchResultView:extAttrSearchResultView
    });
    modExtAttrView.setView({
        extAttrSearchResultView:extAttrSearchResultView,
        extAttrSearchView:extAttrSearchView
    });

    extAttrSearchView.setView({
        extAttrSearchResultView:extAttrSearchResultView,
        modExtAttrView:modExtAttrView
    });
    
    delExtAttrView.setView({
        extAttrSearchResultView :extAttrSearchResultView
    });

    extAttrSearchView.render();
    extAttrSearchResultView.render();
});