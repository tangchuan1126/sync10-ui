/*!
 * config product define urls
 * author: lixf
 * date: 2015-6-11
 */
(function(){
	var extattr = "/Sync10/basicdata/extattr"
	var restfulUrls = {
		productDefine:{
			displayTypeUrl: extattr + "/displaytype",
			addExtAttrUrl: extattr + "/extattr",
			getPagingExtAttrsUrl: extattr + "/pagingextattrs",
			getExtAttrsUrl: extattr + "/extattrs",
			getExtAttrValuesUrl: extattr + "/extattrvalues",
			changeStatusUrl: extattr + "/extattr",
			chackNameUrl:"",
			basicRuleUrl: extattr + "/basicrule"
		}	
	};
	define(restfulUrls);
}).call(this);