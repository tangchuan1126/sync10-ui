"use strict";
define(["jquery", 
    "backbone", 
    "handlebars",
    "handlebars_ext", 
    "templates",
    "../view/listExtAttrView",
    "../model/extAttrValuesModel",
    "../model/extAttrOModel",
    "slidePanel",
    "../config",
    "assembleUtils",
    "artDialog",
    "art_Dialog/dialog-plus",
    "validate",
    "select2",
    "wrapToSelect",
    "showMessage"], 
function($, Backbone, HandleBars,HandlebarsExt, templates,ListView,models,extAttrModel,SlidePanel,Config,assembleUtils,Dialog) {
	var editView =  Backbone.View.extend({
		template: templates.mod_ext_attr,
		model:new extAttrModel.ExtAttr,
		initialize: function(options) {
			var v = this;
			v.setElement(options.el);
		},
		setView:function(views){
			this.extAttrSearchResultView = views.extAttrSearchResultView;
			this.extAttrSearchView = views.extAttrSearchView;
		},
		validator:function(){
			$("#eidt-extattr-form").validate({
			    rules:{
			    	attr_name:{
			            required:true,
			            maxlength:50,
			            remote: {
			              url: Config.productDefine.chackNameUrl,
			              dataType: "json",
			              data:{
			                  attr_id:$("#attr_id").val()
			              },
			              dataFilter: function(data) {
			                  return $.parseJSON(data).success;
			              }
			            }
			        },
			        display_type:{
			            required:true
			        },
			        description:{
			        	maxlength:300,
			        }
			    },
			    messages:{
			        attr_name:{
				      	required:"Please input attribute name",
				      	remote:"Attribute name is exites",
				      	maxlength:"Attribute name maxlength 50"
			        },
			        display_type:"Please select one display type ",
			        description:{
			        	maxlength:"Attribute description maxlength 300"
			        }
			    },
			    errorPlacement: function(error, element) {
			        if($(element).attr("id")=="description"){
			        	error.appendTo(element.parent()); 
			        }else{
			        	error.appendTo(element.parent().parent()); 
			        }
			    }
			});
		},
		render: function(item,_title) {
			var v = this;
			if(typeof(item) != 'undefined' && item != "" && item!=null){
				v.model = item;        
			}else{
				v.model = new extAttrModel.ExtAttr;
			}
			v.model.set("displayTypes",v.extAttrSearchView.display_type);
			v.model.set("basicRules", v.extAttrSearchResultView.basicrules);
			var itemsDialog =art.dialog({
				title:_title
			    ,lock: true
			    ,width:'780px'
			    //,height:'234px'
			    ,opacity:0.3
			    ,init:function(){
			        var w1 = $(window).width(), H = $('html');
			        H.css('overflow', 'hidden');
			        var w2 = $(window).width();
			        H.css('margin-right', (w2 - w1) + 'px');
			        this.content(v.template({
			            model:v.model.toJSON()
			        }));
			        $.unblockUI();
			    }
			    ,close:function(){
			        document.body.parentNode.style.overflow="scroll";
			        document.body.parentNode.style.marginRight="";
			    },
			    button: [{
			        name: 'Submit',
			        callback: function () {
			
			            var dialog = this;
			
			            var editExtAttr = $("#eidt-extattr-form");
			
			            if (editExtAttr.valid()) {
			            	$.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
			    
			                //禁用按钮
			                dialog.button({
			                    name: 'Submit',
			                    focus: true,
			                    disabled: true
			                });
			                var hasValue = false;
			                //用JS 获取数据
			                var attrValuesCollection = new models.ExtAttrValuesCollection;
			
			                if($("#extattr-value-list").children()){
								//获取数据
								$("#extattr-value-list .search_result_list_content tbody").children().each(function(index){
									hasValue = true;
									if($(this).children().length==1){
									 
									}else{
									   var m = new models.ExtAttrValues({
										   "attr_id":$("#attr_id").val() || "",
										   "item_name":$(this).children("td:eq(0)").text().trim(),
										   "item_value":$(this).children("td:eq(1)").text().trim(),
										   "sort":$(this).children("td:eq(2)").text().trim()
									   });
									   if ($(this).attr("data-v_id") && $(this).attr("data-v_id")!="") {
										   m.set("v_id",$(this).attr("data-v_id"));
									   };
									   attrValuesCollection.add(m);
									}
								});
			                }
			                
			                var _item  = null; 
			                if($("#attr_id").val()!=""){
				                _item = new extAttrModel.ExtAttr({
				                    "attr_id":$("#attr_id").val()
				                });
				                _item.url+="/"+$("#attr_id").val();
			                }else{
			                	_item = new extAttrModel.ExtAttr;
			                }
			                if (attrValuesCollection.length>0) {
			                	_item.set("attrValues",attrValuesCollection.toJSON());
			                };
			                if(hasValue && attrValuesCollection.length==0){
				                //没有子项
				                showMessage("Please add item value,thanks","alert");
				                $("#item_name").focus();
				                dialog.button({
				                    name: 'Submit',
				                    focus: true,
				                    disabled: false
				                });
				                $.unblockUI();
				                return false;
			                }
			                
			                _item.save({
				                attr_name:$("#attr_name").val(),
				                display_type:$("#display_type").val(),
				                description :$("#description").val()
			                },{
				                success: function (e) {
					                v.model=null;
					                var result = e.changed.success;
					                var content = "";
					                var icon ="question";
					                if (result==0) {
					                	//添加成功
						                content = "Update Success";
						                icon ="succeed";
					                }else if(result==1){
					                	content = "Update fail,please try again!";
						                icon ="error";
					                }else if(result==2){
						                //已存在
						                content = "The Item Exites";
						                icon ="alert";
					                }
					                showMessage(content,icon);
				
					                if(result==0){
						                setTimeout(function(){
							                v.extAttrSearchResultView.render(v.extAttrSearchView.searchModel);
							                dialog.close();
						                },1500);
					                }else{
					                    //开启按钮
					                    dialog.button({
					                        name: 'Submit',
					                        focus: true,
					                        disabled: false
					                    });
					                }
					                $.unblockUI();
				                },
				                error: function (e) {
					                showMessage("System Error","error");
					                $.unblockUI();
					                dialog.button({
					                    name: 'Submit',
					                    focus: true,
					                    disabled: false
					                });
				                }
			                });
			
			            } else {
			                editExtAttr.validate().errorList[0].element.focus();
			                $.unblockUI();
			            }
			            return false;
			        },
			        focus: true
			    }]
			    ,cancel:true
			    ,cancelVal:'Cancel'
			});
			
			$(".aui_content").css("padding","20px 0");
			$("#display_type").select2({
			    placeholder: "Display Type...",
			    allowClear: true,
			});
			
			$("#display_type").select2("val",this.model.toJSON().display_type);
			
			$("#display_type").bind("change",function(evt){
			    var val = $("#display_type").val();
			    if (val!=3 && val!=4 && val!=5) {
			    	$("#extattr-value-list").empty();
			    }else{
					var searchConditions = new models.SearchModel({
						searchConditions:v.model.get("attr_id")
					});
			
				    this.listView = new ListView({el:"#extattr-value-list"});
				    this.listView.render(searchConditions);
			    }
			    $("#display_type").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end();
			});
			$("#display_type").val(v.model.toJSON().display_type);
			var display_type = parseInt(v.model.get("display_type"));
			if (display_type==3 || display_type==4 || display_type==5){
			    var searchConditions = new models.SearchModel({
			    	searchConditions:v.model.get("attr_id")
			    });
			
			    this.listView = new ListView({el:"#extattr-value-list"});
			    this.listView.render(searchConditions);
			}
			v.events();
			v.validator();
			
			itemsDialog.position();
		},
		events:function(){
		},
		//获取url后面的参数
		GetRequest: function (name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
			var r = window.location.search.substr(1).match(reg);  //匹配目标参数
			if (r != null) return unescape(r[2]);
			return null; //返回参数值
		}
	});
	
	return editView;
});