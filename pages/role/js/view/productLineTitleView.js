/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "js/model/postModel",
    "blockui"
],function($,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.product_line_title,
        initialize:function(options){
            this.post = new models();
        },
        refresh:function(model){
            var tmp = this;
            tmp.refreshOrNot = true;
            tmp.postDialog.content(tmp.template({model:model}));
            
        },
        render:function(model){

            $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            var tmp = this;
            
            //获取数据
           
            this.post.fetch({
              success:function(model){
                var postDialog = art.dialog({
                  title:'Post'
                  ,lock: true
                  ,opacity:0.3
                  ,init:function(){
                      $("#unhas_title").val();
                      $("#has_title").val();
                      var w1 = $(window).width(), H = $('html');
                      H.css('overflow', 'hidden');
                      var w2 = $(window).width();
                      H.css('margin-right', (w2 - w1) + 'px');

                      this.content(tmp.template({model:model.toJSON()}));
                      tmp.setElement(this.DOM.content.get(0));
                  }
                  ,close:function(){
                      document.body.parentNode.style.overflow="auto";
                      document.body.parentNode.style.marginRight="";
                      tmp.delegateEvents(); 
                  }
                });
                tmp.postDialog = postDialog;
                $.unblockUI();
              },
              data:{
                id:model.toJSON().adgid
              }
            });
           
        },
        events:{
          "click .add_jt":"add",
          "click .det_jt":"del",
          "click #_lefts":"choiceLeft",
          "click #_rights":"choiceRight"
        },
        add:function(evt){
         
          var tmp = this;

          var unhasTitle = $("#unhas_title").val();

          if(unhasTitle == ''){
            showMessage("Please select post to add","alert");
            return false;
          }
           $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
           this.post.set("unhasTitle",unhasTitle);
           this.post.save({},
           {
              success:function(model){
                  tmp.refresh(model.toJSON());
                  $.unblockUI();
                }
           });  
        },
        del:function(evt){
             
              var tmp = this;

              var hasTitle = $("#has_title").val();

              if(hasTitle == ''){
                showMessage("Please select post to delete","alert");
                return false;
              }
              $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
               var post = new models({
                        _id:this.post.toJSON().role.ADGID
                    });
              post.url += "?adg_id="+post.get("_id")+"&hasTitle="+hasTitle;
              post.destroy(
              {
                  success: function(model,response)
                  {
                      tmp.refresh(response);
                      $.unblockUI();
                  }
                  
              });
        },
        choiceLeft:function(evt){
          var id = $(evt.target).attr("id");
          if((','+$("#has_title").val()+',').indexOf(','+id+',')==-1){
            $("#"+id).css("background-color","#E6F3C5");
            if($("#has_title").val()==''){
              $("#has_title").val(id);
            }else{
              $("#has_title").val($("#has_title").val()+','+id);
            }
          }else{
            $("#"+id).css("background-color","white");
            $("#has_title").val($("#has_title").val().replace(","+id+",",","));
            $("#has_title").val($("#has_title").val().replace(","+id,""));
            $("#has_title").val($("#has_title").val().replace(id+",",""));
            $("#has_title").val($("#has_title").val().replace(id,""));
          }

        },
        choiceRight:function(evt){
          var id = $(evt.target).attr("id");
          if((','+$("#unhas_title").val()+',').indexOf(','+id+',')==-1){
            $("#"+id).css("background-color","#E6F3C5");
            if($("#unhas_title").val()==''){
              $("#unhas_title").val(id);
            }else{
              $("#unhas_title").val($("#unhas_title").val()+','+id);
            }
          }else{
            $("#"+id).css("background-color","white");
            $("#unhas_title").val($("#unhas_title").val().replace(","+id+",",","));
            $("#unhas_title").val($("#unhas_title").val().replace(","+id,""));
            $("#unhas_title").val($("#unhas_title").val().replace(id+",",""));
            $("#unhas_title").val($("#unhas_title").val().replace(id,""));
          } 
        }
    });
});
