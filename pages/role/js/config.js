(function(){
    var configObj = {
    	//账号信息
        roleUrl:{
            url:"/Sync10/action/administrator/proprietary/RoleMgrAction.action"
        },
        //权限信息
        accountPermissionUrl: {
            url:"/Sync10/action/administrator/proprietary/RolePermissionMgrAction.action"
        },
        //部门和职务关联
        groupPostUrl:{
            url:"/Sync10/action/administrator/proprietary/RoleGroupPostAction.action"
        }
    };
	define(configObj);
	
}).call(this);
