(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"dialog_update_content\">\r\n    <form id=\"addRoleForm\" >\r\n        <table>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Role Name</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='roleName' id='roleName' type=\"text\" maxlength=\"30\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <span>Default Home</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='defaultHome' id='defaultHome' type='text' maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <span>Description</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>";
  },"useData":true});
templates['del_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div class=\"delete_account_dialog\">\r\n    Delete\r\n	<span>\r\n        "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\r\n    </span>\r\n    ?\r\n</div>\r\n";
},"useData":true});
templates['mod_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div class=\"dialog_update_content\">\r\n    <form id=\"modRoleForm\" >\r\n        <table>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Role Name</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='roleName' id='roleName' type=\"text\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" maxlength=\"30\"/>\r\n                    <input name='adgid' id='adgid' type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.adgid : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <span>Default Home</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='defaultHome' id='defaultHome' type='text' value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.index_page : stack1), depth0))
    + "\" maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <span>Description</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.description : stack1), depth0))
    + "\" maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>";
},"useData":true});
templates['permission_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div style=\"padding: 10px;box-shadow: 0 0 5px #ccc;margin-bottom: 8px;\">\r	<input type=\"text\" id=\"permissionTreeSearch\" style=\"padding: 6px;border-radius: 4px;border: 1px solid silver;font-family: inherit;font-size: inherit;line-height: inherit;width: 200px;\" placeholder=\"Search\">\r</div>\r\r<div class=\"permission_basic_layout\">\r    <div>\r        <div id=\"permission_tree\"></div>\r    </div>\r</div>";
  },"useData":true});
templates['product_line_title'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "									<tr>\r\n										<td   id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\" style=\"cursor:pointer;\" width=\"20%\" height=\"30px\" align=\"center\">\r\n									 		"
    + escapeExpression(lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "\r\n									 	</td>\r\n									</tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "									<tr>\r\n										<td   id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\" style=\"cursor:pointer;\" width=\"20%\" height=\"30px\" align=\"center\">\r\n											"
    + escapeExpression(lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "\r\n										</td>\r\n									</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div align=\"left\" style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;margin-bottom:10px;\">\r\n     <font style=\"font-weight:bold;\">Role Name :</font>&nbsp;<font  style=\"font-weight:bold;size:16px;color:#00F\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.role : stack1)) != null ? stack1.NAME : stack1), depth0))
    + "</font>\r\n</div>\r\n	<div id=\"update\">\r\n	     <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	     	<tr>\r\n	     		<td width=\"305px\" height=\"30px\" bgcolor=\"#dddddd\" style=\"padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold\">Linked Post</td>\r\n	     		<td bgcolor=\"#dddddd\" style=\"padding-left:5px;font-weight:bold\" align=\"center\">No Linked Post</td>\r\n	     	</tr>\r\n	     </table>\r\n         <div align=\"left\" style=\" margin-top:2px; height:395px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;\">		\r\n			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n				<tr>\r\n				    <td>\r\n					    <div style=\"background-color:#FFF;width:250px; border:2px #dddddd solid; height:390px; float:left; overflow:auto\">\r\n			        		 <table id=\"_lefts\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"search_result_list_content\" style=\"border:none;\">\r\n				        		 	<input type=\"hidden\" id=\"has_title\" value=\"\"/>\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lefts : stack1), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "							 </table>\r\n			      		</div>\r\n				    </td>\r\n				    <td align=\"center\" >\r\n				    	<div style=\"width:155px\">\r\n				    	<a class=\"add_jt\" style=\"cursor:pointer\">\r\n				    		<img alt=\"\" src=\"./image/add_jt.gif\"/>\r\n				    	</a>\r\n				    	<br><br><br>\r\n				    	<a class=\"det_jt\" style=\"cursor:pointer\">\r\n				    		<img alt=\"\" src=\"./image/det_jt.gif\"/>\r\n				    	</a>\r\n				    	</div>\r\n				    </td>\r\n				    <td>\r\n				   	    <div style=\"background-color:#FFF;width:250px; border:2px #dddddd solid; height:390px; float:left; overflow:auto\">\r\n			        		 <table id=\"_rights\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"search_result_list_content\" style=\"border:none;\">\r\n				        		 	<input type=\"hidden\" id=\"unhas_title\" value=\"\"/>\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.rights : stack1), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "							 </table>\r\n			      		</div>\r\n				    </td>\r\n				</tr>\r\n			</table>\r\n		</div>         \r\n    </div>";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.resultList : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <tr data-adgid=\""
    + escapeExpression(lambda((depth0 != null ? depth0.adgid : depth0), depth0))
    + "\">\r\n                <td valign=\"middle\" width=\"330px\">\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"330px\">\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.index_page : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"460px\">\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.description : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\r\n                    <a href=\"javascript:void(0)\" name=\"setPermission\" class=\"buttons icon settings\" style=\"margin-left: 1px;\" title=\"Config\"></a>\r\n                    <a href=\"javascript:void(0)\" name=\"modRole\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\r\n                    <a href=\"javascript:void(0)\" name=\"delRole\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\r\n                    <a href=\"javascript:void(0)\" name=\"relationRole\" class=\"buttons icon user\" style=\"margin-left: 1px;\" title=\"Relation\"></a>\r\n                </td>\r\n            </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "	        <tr>\r\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\r\n	        </tr>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"search_result_list\">\r\n    <div>\r\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\r\n            <tr>\r\n                <th width=\"330px\">Role Name</th>\r\n                <th width=\"330px\">Default Home</th>\r\n                <th width=\"460px\">Description</th>\r\n                <th width=\"200px\" style=\"border-right:0px;\">\r\n                    Operation\r\n                </th>\r\n            </tr>\r\n        </table>\r\n\r\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.resultList : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </table>\r\n    </div>\r\n\r\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\r\n        </ul>\r\n    </div>\r\n</div>";
},"useData":true});
templates['search_templet'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div>\r\n    <div>\r\n        <div class=\"eso_search_parent\">\r\n            <div class=\"eso_search_icon\">\r\n                <a>\r\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n\r\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\r\n            <a id=\"addRole\" href=\"javascript:void(0)\" class=\"buttons icon add button_add_role\">Add Role</a>\r\n        </div>\r\n\r\n        <div class=\"eso_search_input\">\r\n\r\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\r\n        </div>\r\n    </div>\r\n</div>";
  },"useData":true});
})();