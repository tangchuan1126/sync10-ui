define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\n    <form id=\"addRoleForm\" >\n        <table>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Role Name</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='roleName' id='roleName' type=\"text\" maxlength=\"30\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Default Home</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='defaultHome' id='defaultHome' type='text' maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Description</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n        </table>\n    </form>\n</div>";
},"useData":true});
templates['del_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"delete_account_dialog\">\n    Delete\n	<span>\n        "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\n    </span>\n    ?\n</div>\n";
},"useData":true});
templates['mod_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"dialog_update_content\">\n    <form id=\"modRoleForm\" >\n        <table>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Role Name</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='roleName' id='roleName' type=\"text\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" maxlength=\"30\"/>\n                    <input name='adgid' id='adgid' type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.adgid : stack1), depth0))
    + "\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Default Home</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='defaultHome' id='defaultHome' type='text' value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.index_page : stack1), depth0))
    + "\" maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Description</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.description : stack1), depth0))
    + "\" maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n        </table>\n    </form>\n</div>";
},"useData":true});
templates['permission_role'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div style=\"padding: 10px;box-shadow: 0 0 5px #ccc;margin-bottom: 8px;\">\r	<input type=\"text\" id=\"permissionTreeSearch\" style=\"padding: 6px;border-radius: 4px;border: 1px solid silver;font-family: inherit;font-size: inherit;line-height: inherit;width: 200px;\" placeholder=\"Search\">\r</div>\r\r<div class=\"permission_basic_layout\">\r    <div>\r        <div id=\"permission_tree\"></div>\r    </div>\r</div>";
},"useData":true});
templates['product_line_title'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "									<tr>\n										<td   id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" style=\"cursor:pointer;\" width=\"20%\" height=\"30px\" align=\"center\">\n									 		"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n									 	</td>\n									</tr>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "									<tr>\n										<td   id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" style=\"cursor:pointer;\" width=\"20%\" height=\"30px\" align=\"center\">\n											"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n										</td>\n									</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div align=\"left\" style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;margin-bottom:10px;\">\n     <font style=\"font-weight:bold;\">Role Name :</font>&nbsp;<font  style=\"font-weight:bold;size:16px;color:#00F\">"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.role : stack1)) != null ? stack1.NAME : stack1), depth0))
    + "</font>\n</div>\n	<div id=\"update\">\n	     <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n	     	<tr>\n	     		<td width=\"305px\" height=\"30px\" bgcolor=\"#dddddd\" style=\"padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold\">Linked Post</td>\n	     		<td bgcolor=\"#dddddd\" style=\"padding-left:5px;font-weight:bold\" align=\"center\">No Linked Post</td>\n	     	</tr>\n	     </table>\n         <div align=\"left\" style=\" margin-top:2px; height:395px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;\">		\n			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n				<tr>\n				    <td>\n					    <div style=\"background-color:#FFF;width:250px; border:2px #dddddd solid; height:390px; float:left; overflow:auto\">\n			        		 <table id=\"_lefts\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"search_result_list_content\" style=\"border:none;\">\n				        		 	<input type=\"hidden\" id=\"has_title\" value=\"\"/>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lefts : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							 </table>\n			      		</div>\n				    </td>\n				    <td align=\"center\" >\n				    	<div style=\"width:155px\">\n				    	<a class=\"add_jt\" style=\"cursor:pointer\">\n				    		<img alt=\"\" src=\"./image/add_jt.gif\"/>\n				    	</a>\n				    	<br><br><br>\n				    	<a class=\"det_jt\" style=\"cursor:pointer\">\n				    		<img alt=\"\" src=\"./image/det_jt.gif\"/>\n				    	</a>\n				    	</div>\n				    </td>\n				    <td>\n				   	    <div style=\"background-color:#FFF;width:250px; border:2px #dddddd solid; height:390px; float:left; overflow:auto\">\n			        		 <table id=\"_rights\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"search_result_list_content\" style=\"border:none;\">\n				        		 	<input type=\"hidden\" id=\"unhas_title\" value=\"\"/>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.rights : stack1),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							 </table>\n			      		</div>\n				    </td>\n				</tr>\n			</table>\n		</div>         \n    </div>";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-adgid=\""
    + alias2(alias1((depth0 != null ? depth0.adgid : depth0), depth0))
    + "\">\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.index_page : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"460px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\n                    <a href=\"javascript:void(0)\" name=\"setPermission\" class=\"buttons icon settings\" style=\"margin-left: 1px;\" title=\"Config\"></a>\n                    <a href=\"javascript:void(0)\" name=\"modRole\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\n                    <a href=\"javascript:void(0)\" name=\"delRole\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\n                    <a href=\"javascript:void(0)\" name=\"relationRole\" class=\"buttons icon user\" style=\"margin-left: 1px;\" title=\"Relation\"></a>\n                </td>\n            </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "	        <tr>\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n	        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\">\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n                <th width=\"330px\">Role Name</th>\n                <th width=\"330px\">Default Home</th>\n                <th width=\"460px\">Description</th>\n                <th width=\"200px\" style=\"border-right:0px;\">\n                    Operation\n                </th>\n            </tr>\n        </table>\n\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n</div>";
},"useData":true});
templates['search_templet'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n    <div>\n        <div class=\"eso_search_parent\">\n            <div class=\"eso_search_icon\">\n                <a>\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\n                </a>\n            </div>\n        </div>\n\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\n            <a id=\"addRole\" href=\"javascript:void(0)\" class=\"buttons icon add button_add_role\">Add Role</a>\n        </div>\n\n        <div class=\"eso_search_input\">\n\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\n        </div>\n    </div>\n</div>";
},"useData":true});
return templates;
});