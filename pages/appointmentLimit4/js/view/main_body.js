"use strict";
define([
	"jquery",
	"backbone", 
	"config",
	"templates",
	"bootstrap",
	"moment",
	"artDialog",
	"bootstrap.datetimepicker",
	"require_css!bootstrap.datetimepicker-css"
    ],
	function($,Backbone,config,templates,bootstrap,moment,artDialog,datetimepicker) { 
		return Backbone.View.extend({
			el : "#main_body",
			template : templates.main_body,
			w0:"",
			w6:"",
			checkedCount:0,
			events:{ 
				"click #home table th":"thClick", //th点击事件
				"click #tprv":"prevWeek",    //上一周点击事件
				"click #tnext":"nextWeek",    //下一周点击事件
				//"click #all":"selectWeek",   //周中所有复选框点击事件
				"click .dates input":"dayClick", //单天点击事件
				"change #myonoffswitch" : "changeDefault",  //头部开关切换 
				"click .dates li":"liClick", //li点击事件
				"click .checkboxButton .btn":"chooseBound",   //inbound，outbound点击事件
				"blur #allvalue":"valueBlur",   //设置值失去焦点
				"click #setAll":"setValue",  //值设置点击
				"blur input[type='number']":"numberBlur",  //数字文本框失去焦点
				"blur #setStartTime":"setTime1Blur",  //筛选时设置时间一失去焦点
				"blur #setEndTime":"setTime2Blur", //筛选时设置时间二失去焦点
				"click #save":"saveClick"  //保存点击事件
			},
			initialize : function(param) {
				var v =this;
				this.param = param;
			},
			render : function() {
				var v = this;
				v.$el.html(v.template({}));   
				v.addTimeTable();
				v.addSetTime();
				v.w0 =  moment().startOf('week').format("YYYY-MM-DD");
				v.w6 =  moment().endOf('week').format("YYYY-MM-DD");
				$("#selectStartDate").attr("disabled",true);
				$("#selectEndDate").attr("disabled",true);
				$("input[type='number']").attr("min","0");    //控制减数时最小值为0
				$("#selectStartDate").datetimepicker({  //点击日期文本框
					format: 'mm/dd/yyyy',
					autoclose: 1,
					minView: 2,
					forceParse: 0,
					bgiconurl: ""
				}).on('changeDate', function(ev){
					var startTime = $('#selectEndDate');
					startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
				});
				$("#selectEndDate").datetimepicker({
					format: 'mm/dd/yyyy',
					autoclose: 1,
					minView: 2,
					forceParse: 0,
					bgiconurl: ""
				}).on('changeDate', function(ev){
					var startTime = $('#selectStartDate');
					startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
				});
				$("#home table tbody tr:odd").css("background","#f5f6f5");
				$("#home table tr").each(function(){
					$(this).children(":first").css({"color":"#007AFF","font-weight":"bold"});
				});
				$("#home tbody tr:first").children("td:gt(0)").css({"color":"black","font-weight":"bold"});
				$("#home thead th:gt(0)").each(function(idx,obj){
					$(this).html(moment(v.w0,"YYYY-MM-DD").add(idx,"day").format("YYYY-MM-DD"));
				});
				//表格列点击事件
				$("#home tbody tr:first").children("td:gt(0)").each(function(i,e){
					$(this).click(function(){
						$("table td").css("border-collapse","collapse");
						$(this).siblings().css("background","white");   //tbody中第一行所有列样式
						$(this).css("background","#478dcd");
						var cols = i+1;
						//alert($(this).parent("tr").siblings("tr").length);   //24行
						$("tbody tr td").removeClass();
						$(this).parent("tr").siblings("tr").each(function(){
							$(this).children("td:eq("+cols+")").addClass("selectCols");
						})
					})
				})
				//表格行点击事件
				$("#home tbody tr:gt(0)").each(function(){
					$(this).children(":eq(0)").click(function(){
						$(this).removeClass();
						$("tbody td").removeClass();
						$("tbody tr:first").children("td").css("background","white");  //tbody中第一行所有列样式
						$(this) .parent().siblings("tr").children("td:nth-child(1)").removeClass();  //所在行兄弟行的第一个子元素剔除所有类
						$(this).parent().siblings().children("td").removeClass("selectRows");     //所在行对应列剔除所有样式
						//$(this).addClass("selectTr").css("color","white");           //所在行对应列添加行样式 
						$(this).parent().children("td").addClass("selectRows");     //所在行对应列添加行样式
					})
				})
				//单元格即使某一列点击事件
				$("#home tbody tr:gt(0)").each(function(i,e){
					$(this).children("td:gt(0)").each(function(j,e){
						$(this).click(function(){
							var count = j+1;
							$("table th").removeClass("clickStyle");
							$("tbody tr:first").children("td").css("background","white");   //tbody中第一行所有列样式
							$(this).removeClass();  //剔除当前点击列的所有类样式
							$("tbody td").removeClass(); //剔除tbody中所有列的类样式
							$(this).addClass("clickTd"); //为当前点击具体行列添加样式
							$("tbody tr td:eq(0)").removeClass();   //第一行中所有列剔除所有样式
							$("tbody tr:eq(0)").children("td").removeClass();
							$(this).parent("tr").children("td:eq(0)").addClass("clickTd"); //当前列所在行第一列添加样式
							$("tbody tr:eq(0)").children("td:eq("+count+")").addClass("clickTd");    //当前列在第一行中列添加样式
						});
					});
				});
				v.loadWeekData($('#shipFrom').attr("data-value"),v.w0,v.w6);
				return this;
			},
			addTimeTable:function(){
				$("#home").append(templates.timeTable().trim());  //week标签对应内容添加
			},
			addSetTime:function(){
				$("#setup1").append(templates.setTimes().trim());  //setup标签对应内容添加
			},
			loadWeekData:function(psid,start,end){
				$.getJSON("/Sync10/_load/getBaseWorkByWeek", {"storage_id":psid,"start":start,"end":end} , function (result) {
					var DayMap = result.DATES;
					$(result.DATA).each(function(index,obj){
						var day =DayMap[obj.work_date];
						if (obj.hour<10) {
							$("#"+day+"0"+obj.hour+"IN").text(obj.sumin);
							$("#"+day+"0"+obj.hour+"OUT").text(obj.sumout);
						} else {
							$("#"+day+""+obj.hour+"IN").text(obj.sumin);
							$("#"+day+""+obj.hour+"OUT").text(obj.sumout);
						}
					});
				});
			},
			thClick:function(e){   //表格头点击事件方法
				$(e.target).removeClass("clickStyle");
				$(e.target).siblings() .removeClass("clickStyle");
				$(e.target).addClass("clickStyle");
				$("tbody td").removeClass();
				$("tbody tr:first").children("td").css("background","white");  //tbody中第一行所有列样式
			},
			prevWeek:function(e){  //点击上一周事件方法
				var v = this;
				$("td[id$='OUT'],td[id$='IN']","#home tbody").empty();
				v.w0 = moment(v.w0,"YYYY-MM-DD").add(-2,"day").startOf('week').format("YYYY-MM-DD");
				v.w6 = moment(v.w0,"YYYY-MM-DD").endOf('week').format("YYYY-MM-DD");
				$("#home thead th:gt(0)").each(function(idx,obj){
					$(this).html(moment(v.w0,"YYYY-MM-DD").add(idx,"day").format("YYYY-MM-DD"));
				});
				v.loadWeekData($('#shipFrom').attr("data-value"),v.w0,v.w6);
			}, 
			nextWeek:function(e){  //点击下一周事件方法
				var v = this;
				$("td[id$='OUT'],td[id$='IN']","#home tbody").empty();
				v.w6 = moment(v.w6,"YYYY-MM-DD").add(2,"day").endOf('week').format("YYYY-MM-DD");
				v.w0 = moment(v.w6,"YYYY-MM-DD").startOf('week').format("YYYY-MM-DD");
				$("#home thead th:gt(0)").each(function(idx,obj){
					$(this).html(moment(v.w0,"YYYY-MM-DD").add(idx,"day").format("YYYY-MM-DD"));
				});
				v.loadWeekData($('#shipFrom').attr("data-value"),v.w0,v.w6);
			},
			/*selectWeek:function(e){   //周全选复选框方法
				//console.log(e);   //这是一个对象
				//console.log(e.target);   //获取到复选框本身对象
				//console.log(e.target.checked);
				if(e.target.checked){ 
					$("ul input[type='checkbox']:not('#all')").each(function(){
						this.checked="checked";
					})
				}else{
					$("ul input[type='checkbox']:not('#all')").attr("checked",false);
				}
			},*/
			dayClick:function(e){  //单天单击事件
				var v = this;
				/*if(!$(e.target).is("#all")){
					if(e.target.checked){
						v.checkedCount = v.checkedCount+1;
						if(v.checkedCount == 7){   //星期是否全部勾选中判断
							$(".dates #all")[0].checked = 'checked';
						}
					}else{
						v.checkedCount = v.checkedCount-1;
						if(v.checkedCount <7){
							$(".dates #all").attr("checked",false);
						}
						$(e.target).next("label").css("color","#1479fb");
						$(e.target).parent("li").css({"background":"#efefef"});
					}
				}else if($(e.target).is("#all")){
					v.checkedCount = 7;
				}*/
				/*if(!$(e.target).is("#all")){
					if($(".dates input:not('#all'):checked").length == 7){
						$(".dates #all")[0].checked = 'checked';
					}else{
						$(".dates #all")[0].checked = '';
					}
				}*/
				if($(e.target).is("#all"))
				{
					$("ul input[type='checkbox']:not('#all')").prop('checked',e.target.checked);
				}
				else
				{
					$("#all").prop('checked',$("ul input[type='checkbox']:not('#all')").filter(":checked").length==7);
				}
			},
			changeDefault : function() {   //头部按钮切换事件处理函数
				if($.trim($("#lastGroup .onoffswitch-checkbox:checked").val()) == "on"){
					$("#clearSp .header .form-control").css("background","none");
					$("#selectStartDate").attr("disabled",true).val("").parent().removeClass("has-error");
					$("#selectEndDate").attr("disabled",true).val("").parent().removeClass("has-error");
					$("#main_header .form-control").removeClass("dateBg");
				}else{
					$("#clearSp .header .form-control").css("background","white");
					$("#selectStartDate").attr("disabled",false).parent().removeClass("has-error");
					$("#selectEndDate").attr("disabled",false).parent().removeClass("has-error");
					$("ul.dates").removeClass("has-error");
				}
			},
			liClick:function(e){  //li点击事件方法
				//console.log($(e.target));
				if(!$(e.target).is(".all") && (!$(e.target).parent().is(".all"))){
					if($(e.target).is(".ckbox")){
						$(e.target).addClass("active").css("background","#E3F9E3");
						$(e.target).siblings().removeClass("active").css("background","#EFEFEF");
					}else{
						$(e.target).parent().addClass("active").css("background","#E3F9E3");
						$(e.target).parent().siblings().removeClass("active").css("background","#EFEFEF");
					}
				}else{
					$(e.target).siblings().css("background","none");
					$(e.target).parent().siblings().css("background","none");
				}
			},
			chooseBound:function(e){  //inbound,outbound的点击
				if(!$(e.target).hasClass("active")){
					$(e.target).css({"background":"#59E076","color":"white"});
				}else{
					$(e.target).css({"background":"#eee","color":"#333"});
				}
			},
			valueBlur:function(e){
				var num = /^[0-9]{1,}$/;
				if(!num.test($(e.target).val())){
					$(this).val("");
				}
			},
			setValue:function(e){
				var num = /^[0-9]{1,}$/;
				if($("#allvalue").val()){
					if(!num.test($("#allvalue").val())){
						$(".setallinputgroup").addClass("has-error");
					}else{
						$(".setallinputgroup").removeClass("has-error");  //剔除必填项警示类样式
						if($("#initHour input[type='checkbox']:checked").length>0){  //判断时间点复选框是否至少有一个勾选中
							var inorout = "";
							if ($("#checkboxButtons label.active").length==0) {
								$("#checkboxButtons label.btn-primary1").css("border","1px solid #a94442");
							} else {
								$("#checkboxButtons label").css("border","1px solid #ccc");
								$("#checkboxButtons label.active").each(function(){
									if ($(this).attr("name")==="inbound") {
										$("#initHour input[type='checkbox']:checked").each(function(i,e){  //默认选中的所有文本框inbound值为set输入的值
											$(this).parent("div").next("div").children("input.inboundInput").val($("#allvalue").val());
										});
									}
									if ($(this).attr("name")==="outbound") {
										$("#initHour input[type='checkbox']:checked").each(function(i,e){  //默认选中的所有文本框outbound值为set输入的值
											$(this).parent("div").next("div").children("input.outboundInput").val($("#allvalue").val());
										});
									}
								});
							}
						}
					}
				} else {  //没输入值
					$(".setallinputgroup").addClass("has-error");
				}
			},
			numberBlur:function(e){  //时间列表文本框失去焦点
				if($(e.target).val() && (parseInt($(e.target).val())<0)){  //输入的时间值小于0则默认设置0
					$(e.target).val(0);
				}
			},
			setTime1Blur:function(e){  //时间列表筛选时间一失去焦点
				if($(e.target).val() && (parseInt($(e.target).val())<0)){
					$(e.target).val(0);
				}else if($(e.target).val() && (parseInt($(e.target).val())>23)){
					$(e.target).val(23);
				}
				if($(e.target).val() && $("#setEndTime").val()){ //设置时间一存在且时间二也有值
					if(parseInt($(e.target).val()) > parseInt($("#setEndTime").val())){  //时间一大于时间二
						$(e.target).val($("#setEndTime").val());
						$("#setStartTime").trigger("blur");
					}else{   //时间一小于或等于时间二
						$(".hourCondition .setTimeTip").hide();
						$(".initHour input[type='checkbox']").each(function(){
							$(this)[0].checked = "";
						})
						$(".setTimes div").removeClass("has-error");  //剔除警示样式
						var num1 = parseInt($("#setStartTime").val());
						var num2 = parseInt($("#setEndTime").val());
						$(".hourCondition .setTimeTip").hide();
						for(var i=num1;i<=num2;i++){   //整点时间区间
							$("#time"+i)[0].checked = "checked";   //设置区间内的整点时间都选中 
						}
					}
				}else if(($(e.target).val() && (!$("#setEndTime").val())) || ((!$(e.target).val()) && $("#setEndTime").val()) || ((!$(e.target).val()) && (!$("#setEndTime").val()))){
					$(".setTimes div").addClass("has-error");
				}
			},
			setTime2Blur:function(e){  //时间列表筛选时间二失去焦点
				if($(e.target).val() && (parseInt($(e.target).val())<0)){  //输入的时间值小于0则默认设置0
					$(e.target).val(0);
				}else if($(e.target).val() && (parseInt($(e.target).val())>23)){  //输入的值大于23则默认设置为23
					$(e.target).val(23);
				}
				if($(e.target).val() && $("#setStartTime").val()){
					if(parseInt($(e.target).val()) < $("#setStartTime").val()){
						$(e.target).val($("#setStartTime").val());
						$("#setEndTime").trigger("blur");
					}else{
						$(".hourCondition .setTimeTip").hide();
						$(".initHour input[type='checkbox']").each(function(){
							$(this)[0].checked = "";
						})
						$(".setTimes div").removeClass("has-error");  //剔除警示样式
						var num1 = parseInt($("#setStartTime").val());
						var num2 = parseInt($("#setEndTime").val());
						$(".hourCondition .setTimeTip").hide();
						for(var i=num1;i<=num2;i++){   //整点时间区间
							$("#time"+i)[0].checked = "checked";   //设置区间内的整点时间都选中 
						}
					}
				}else if(($(e.target).val() && (!$("#setStartTime").val())) || ((!$(e.target).val()) && $("#setStartTime").val()) || ((!$(e.target).val()) && (!$("#setStartTime").val()))){
					$(".setTimes div").addClass("has-error");
				}
			},
			setTime1Focus:function(e){

			},
			saveClick:function(e){  //点击保存事件方法
				var v = this;
				if ($("#myonoffswitch")[0].checked) {  //开关on
					//重复
					var hasempty = false;
					$("#initHour").find("input[type='number']").each(function(index,item) {
						if (""===item.value) {
							$(this).addClass("has-error").attr("title","Please input value");
							hasempty = true;
						} else {
							$(this).removeClass("has-error")
						}
					});
					//dates
					if ($(".dates input:gt(0):checked").length==0) {
						//console.log("has-error");
						$("ul.dates").addClass("has-error");
						return;
					} else {
						$("ul.dates").removeClass("has-error");
					}
					if(!hasempty) {
						var POSTDATA = {};
						var data = [];
						for (var i=0;i<24;i++) {
							var item = {}
							item.hour = i;
							if (i<10) {
								item.value_in = ($("#initHour input[name='0"+i+"'].inboundInput").val());
								item.value_out = ($("#initHour input[name='0"+i+"'].outboundInput").val());
							} else {
								item.value_in = ($("#initHour input[name='"+i+"'].inboundInput").val());
								item.value_out = ($("#initHour input[name='"+i+"'].outboundInput").val());
							}
							data.push(item);
						}
						//setBaseWork
						
						POSTDATA.storage_id = $('#shipFrom').attr("data-value");
						POSTDATA.weeks = [];
						$(".dates input:gt(0):checked").each(function(){
							POSTDATA.weeks.push($(this).attr("data-value"));
						});
						POSTDATA.data = JSON.stringify(data);
						$(this).attr("disabled","disabled");
						$.ajax({
							type: 'POST',
							url: "/Sync10/_load/setBaseWork",
							//url: config.setBaseWorkUrl.url,
							data: POSTDATA,
							dataType: 'json',
							async:false,
							success: function (data) {
								$("#save").removeAttr("disabled");
								//artDialog.tips("Save ok",3);
								var w0 = moment().startOf('week').format("YYYY-MM-DD");
								var w6 = moment().endOf('week').format("YYYY-MM-DD");
								v.loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
								$('#tabbody a[href="#home"]').tab('show');
							},
							error: function (data) {
								$("#save").removeAttr("disabled");
								//console.log(data.responseJSON);
								artDialog.alert("Setup Error.");
							}
						});
					}
				} else {   //开关off
					//特例
					var flag = true;
					if ($("#selectStartDate").val()==="") {
						flag = false;
						$("#selectStartDate").parent().addClass("has-error");
					} else {
						$("#selectStartDate").parent().removeClass("has-error");
					}
					if ($("#selectEndDate").val()==="") {
						flag = false;
						$("#selectEndDate").parent().addClass("has-error");
					} else {
						$("#selectEndDate").parent().removeClass("has-error");
					}
					var hasempty = false;
					$("#initHour input[type='number']").removeClass("has-error");
					if($("#initHour input[type='checkbox']:checked").length==0){
						$("#initHour input[type='checkbox']").parent().parent().addClass("has-error");
					} else {
						$("#initHour input[type='checkbox']").parent().parent().removeClass("has-error");
						$("#initHour input[type='checkbox']:checked").parent().next().find("input[type='number']").each(function(){
							if ($(this).val()=="" && $(this).siblings().val() == "") {
								$(this).addClass("has-error").attr("title","Please input value");
								hasempty = true;
							} else {
								$(this).removeClass("has-error").removeAttr("title");
							}
						});
					}
					if(!hasempty) {
						var POSTDATA = {};
						var data = [];
						for (var i=0;i<24;i++) {
							var item = {}
							item.hour = i;
							$("#initHour input[type='checkbox']:checked").parent().next()
							if (i<10) {
								if ($("#initHour input[name='0"+i+"']").parent().prev("div.ckbox").find("input[type='checkbox']:checked").length==1) {
									if($("#initHour input[name='0"+i+"'].inboundInput").val()){
										item.value_in = ($("#initHour input[name='0"+i+"'].inboundInput").val());
									}
									if($("#initHour input[name='0"+i+"'].outboundInput").val()){
										item.value_out = ($("#initHour input[name='0"+i+"'].outboundInput").val());
									}
									data.push(item);
								}
							} else {
								if ($("#initHour input[name='"+i+"']").parent().prev("div.ckbox").find("input[type='checkbox']:checked").length==1) {
									if($("#initHour input[name='"+i+"'].inboundInput").val()){
										item.value_in = ($("#initHour input[name='"+i+"'].inboundInput").val());
									}
									if($("#initHour input[name='"+i+"'].outboundInput").val()){
										item.value_out = ($("#initHour input[name='"+i+"'].outboundInput").val());
									}
									data.push(item);
								}
							}
						}
						//setBaseWork
						POSTDATA.start = $("#selectStartDate").val();
						POSTDATA.end = $("#selectEndDate").val();
						POSTDATA.storage_id = $('#shipFrom').attr("data-value");
						POSTDATA.weeks = [];
						$(".dates input:gt(0):checked").each(function(){
							POSTDATA.weeks.push($(this).attr("data-value"));
						});
						POSTDATA.data = JSON.stringify(data);
						//console.log(POSTDATA);
						$(this).attr("disabled","disabled");
						$.ajax({
							type: 'POST',
							url: "/Sync10/_load/setDefaultValueByDate",
							//url: config.setBaseWorkUrl.url,
							data: POSTDATA,
							dataType: 'json',
							async:false,
							success: function (data) {
								$("#save").removeAttr("disabled");
								var w0 = moment($("#selectStartDate").val(),"MM/DD/YYYY").startOf('week').format("YYYY-MM-DD");
								var w6 = moment($("#selectStartDate").val(),"MM/DD/YYYY").endOf('week').format("YYYY-MM-DD");
								v.loadWeekData($('#shipFrom').attr("data-value"),w0,w6);
								$('#tabbody a[href="#home"]').tab('show');
							},
							error: function (data) {
								$("#save").removeAttr("disabled");
								//console.log(data.responseJSON);
								artDialog.alert("Setup Error.");
								
							}
						});
					}
				}
			}
		});
	}
);