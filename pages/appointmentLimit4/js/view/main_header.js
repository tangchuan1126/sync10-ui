"use strict";
define([
	"jquery",
	"backbone", 
	"config",
	"templates",
	"immybox",
	"bootstrap.datetimepicker",
	"require_css!bootstrap.datetimepicker-css"
	  ],
	function($,Backbone,config,templates,immybox,datetimepicker) { 
		return Backbone.View.extend({
			el : "#main_header",
			template : templates.main_header,
			initialize : function(param) {
				var v =this;
				this.param = param;
				//获取仓库服务
				$.getJSON(config.shipFrom.url,{"type":"1"}, function (json) {
					var rls=[];
					for (var i=0;i<json.length;i++) {
						rls.push({"value":json[i].data,"text":json[i].name});
					}
					//仓库文本框点击
					$('#shipFrom').immybox({
						Pleaseselect:'Clean Select',
						Defaultselect:1000005,
						choices: rls
					});
				})
			},
			render : function() {
				var v = this;
				//console.log(v.template({}));
				v.$el.html(v.template({}));
				return this;
			}
		});
	}
);