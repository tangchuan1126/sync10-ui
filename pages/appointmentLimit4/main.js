"use strict";
requirejs.config({   //自个样式路径配置
	paths : {
		"basePath" : "/Sync10-ui/pages/appointmentLimit4/",
		"main_css" : "./css/main",   //./表示当前目录
		"index_css" : "./css/index",
		"general-form":"/Sync10-ui/lib/webpage/chain/css/general-forms1"
	},
	shim : {}
});
require(["../../requirejs_config.js","./config"],function(requirejs_,config){  
	//依赖需要的js和css，require_css!是css依赖进来标志，用来和js区分
	require(["jquery","nprogress","./js/view/main_header","./js/view/main_body","require_css!nprogress","require_css!bootstrap-css/bootstrap","require_css!main_css","require_css!index_css","require_css!general-form"], 
		function($,NProgress,Main_header,Main_body) {
		NProgress.start();  //进度开始
		new Main_header().render();  //页面渲染
		new Main_body().render();  //页面渲染,重复new多少次会有多少次事件重复
		$('#tsetup').on('shown.bs.tab', function (e) {   //一进来时就让设置时间一获取焦点
		  $("#setStartTime")[0].focus();   //原生态获取
		})
		NProgress.done();  //进度结束
	})
})