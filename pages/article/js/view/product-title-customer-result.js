/**
 * Created by lujintao on 2015.07.01
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/title_customer",
    "../config",
    "../role",
    "jqueryui/tooltip",
    "artDialog",
    "showMessage",
    "select2",
    "../plugin/handlebars_ext"
],function($,Backbone,Handlebars,templates,models,Config,Role){
	var temp;
	var urlConfig = Config.productTitle.url;
	//var customerCountPerRow = Config.productTitle.customerSizePerRow;
	

	
    return Backbone.View.extend({
    	template : templates.product_title_customer_result,
    	setFormView : function(formView){
    		this.formView = formView;
    	},
        setRole : function(role){
        	this.role = role;
        },    	
        setTitleId : function(id){
        	this.titleId = id;
        },
        setCustomerId : function(id){
        	this.customerId = id;
        },
        setProductId : function(id){
        	this.productId = id;
        },          
        initialize:function(options){
            this.setElement(options.el);
            this.customerId = options.customerId;
            this.role = options.role;
            this.titleId = options.titleId;
            this.collection = new models.CustomerCollection;
            temp = this;

        },
        initData:function(){
            var url = urlConfig.base + "/" + this.productId;
            if(temp.role == Role.Customer){
            	url = urlConfig.base + "/" + this.productId + "/customer/" + this.customerId; 
            }else if(temp.role == Role.Title){
            	url = urlConfig.base + "/" + this.productId + "/title/" + this.titleId; 
            }
            temp.collection.reset();
        	$.ajax({
	       		 type : "get",
	       		 url  : url ,
	       		 async : false,
	       		 success : function(data){
	       			 
	       			 for(var i=0;i<data.length;i++  ){
	       				 var customerTitle = new models.CustomerTitle({title:data[i].title,customers:data[i].customers,productId:temp.productId});
	       				 temp.collection.add(customerTitle);
	       				 
	       			 }
	       		 }
        	});           	
        },
        events:{
        	"click a.edit":"edit",
        	"click a.remove":"remove"
        }, 
        render : function(){
        	this.initData();
        	
        	temp.$el.find("div.result").height($(window).height() - 150);
        	window.onresize = function(){
        		temp.$el.find("div.result").height($(window).height() - 150);
        	};
        	
        	temp.$el.find("div.result").html(temp.template({title_customers:temp.collection.toJSON()}));
        	
        	
        	
        	if(temp.role == Role.Customer){
        		$("a.edit").remove();
        	}
        	$("td.customer").each(function(){
        		var content = $.trim($(this).text());
        		$(this).text(content.substring(0,content.length-1));
        	});
        	//$("table.zebraTable  tr:even").addClass("highlight");
        	$("table.zebraTable  tr").each(function(data){
        		var index = parseInt($(this).data("index"));
        		if(index % 2 == 0){
        			$(this).addClass("highlight");
        		}
        	});
        	
        	
        	$("tr.record").hover(
        			function(){
        				//$(this).addClass("over");
        				var index = $(this).data("index");
        				$("table.zebraTable  tr[data-index='" + index + "']").addClass("over");
	        		},function(){
	        			//$(this).removeClass("over");
        				var index = $(this).data("index");
        				$("table.zebraTable  tr[data-index='" + index + "']").removeClass("over");	        			
	        		}
        	);
       
        },
        "edit" :function(event){
        	var titleId = $(event.target).data("titleid");
        	$.ajax({
	       		 type : "get",
	       		 url  : urlConfig.base + "/"+ temp.productId + "/" + titleId + "/customer",
	       		 async : false,
	       		 success : function(customers){
	       			temp.formView.render();
	       			$("#title").val(titleId);
	       			$("#title").select2();
	       			$("#title").attr("disabled","true");
	       			
	       			$("#customer option").removeAttr("selected");
	       			for(var i = 0; i < customers.length; i++){
	       	        	$("#customer option[value='" + customers[i].id + "']").attr("selected","true");
	       			}
	       			$("#customer").select2();
	       			temp.formView.setTitleId(titleId);
	       			//$("#addButton").text("Update"); 
	       			$("#addButton").unbind();
	       			$("#addButton").attr("title","Update").removeClass("add").addClass("approve").bind("click",temp.formView.update);
	       			$("a.cancel").show();
	       			/*$(event.target).unbind();
	       			$(event.target).next("a").unbind();
	       			$(event.target).bind("click",function(){
	       				return false;
	       			});
	       			$(event.target).next("a").bind("click",function(){
	       				return false;
	       			});	
	       			*/
	       			//$("a.edit,a.remove").unbind();
	       			$("a.edit,a.remove").attr("edit","true");
	       			
	       			$("a.edit,a.remove").unbind().bind("click",function(){
	       				if($(this).attr("edit") == "true"){
	       					showMessage("Editing","alert");
	       				}
	       				return false;
	       			});   
	       			
	       			$("tr.record").unbind();
	       			
	       			$(this).parent().parent().addClass("over");
	       		 }
        	});	        	
        },
        "remove" :function(event){
        	var title = $(event.target).parent().parent().find("td:first").text();
		    art.dialog({
		        title:'Remove Relation With ' + title
		        ,lock: true
		        ,opacity:0.3
		        ,init:function(){
		            this.content("<div style='font-size:14px;width:300px;'>Are you sure to delete this data?</div>");
		        }
		        ,close:function(){
		            document.body.parentNode.style.marginRight="";
		        }
		        ,icon:'warning'
		        ,button: [{
		            name: 'Sure',
		            callback: function () {
		            	var titleId = $(event.target).data("titleid");
		            	var url = urlConfig.base + "/" + temp.productId + "/" + titleId; 
		                if(temp.role == Role.Customer){
		                	url = urlConfig.base + "/" + temp.productId + "/" + temp.customerId + "/" + titleId; 
		                }	            	
		            	$.ajax({
		    	       		 type : "delete",
		    	       		 url  : url,
		    	       		 async : false,
		    	       		 success : function(data){
		    	                    if(data.success == true){
		                            	$.ajax({
		                           			 type : "get",
		                           			 url  : urlConfig.updateIndex.replace("{pid}",temp.productId),
		                           			 success : function(data){
		                           			 }                        			
		                            	});       	
		    	                    	showMessage("Success","succeed");
		    	                    	if($.trim($("#title").val()).length == 0){
			    	                    	var item = temp.collection.findWhere({title:{id:titleId,name:"AMTRAN"}});
			    	                    	
			    	                    	var model_array = temp.collection.models;
			    	                    	for(var i=0;i<model_array.length; i++){
			    	                    		if(model_array[i].toJSON().title.id == titleId){
			    	                    			temp.collection.remove(model_array[i]);
			    	                    		}
			    	                    	}		    	                    		
		    	                    	}else{
		    	                    		temp.initData();
		    	                    	}
		    	                    	temp.render();
		    	                    }else{
		    	                    	if(data.errors.data_error && data.errors.data_error == "NOT_ALLOWED"){
		    	                    		var content = "<div style='width:300px;font-size:14px;'>Can't delete,please delete SOP and CLP Type Configuration with this Title and Customer first.</div>";
		                    	            art.dialog({
		                    	                title:''
		                    	                ,lock: true
		                    	                ,opacity:0.3
		                    	                ,init:function(){
		                    	                    this.content(content);
		                    	                }
		                    	                ,icon:'warning'
		                    	                ,okVal:'Sure'
		                    	            });  		    	                    	
		    	                    	}else{
		    	                    		showMessage("Failure","error");
		    	                    	}
		    	                    }	       			 
		    	       			 
		    	       		 },
		    	       		 error : function (XMLHttpRequest, textStatus, errorThrown) {
		    	       			showMessage("Failure","error");
		    	       		 }

		            	});   
		            },
		            focus: true
		        }]
		        ,cancel:function(){
		        	        			        	
		        }
		        ,cancelVal:'Cancel'            			        
		    });	            	
        	
       	
        	
        }

    });
});
