"use strict";
define([
    "../config",
    "jquery",
    "backbone"
],function(page_config,$,Backbone){
    
    var ClpModel = Backbone.Model.extend({
        url: page_config.clpListUrl.url,
        idAttribute: "lpt_id"
    });

    var ClpColl = Backbone.Collection.extend({
        url: page_config.clpListUrl.url,
        parse: function (response) {
            ClpColl.containers = response.containers;
            ClpColl.clps = response.clps;
            ClpColl.titles = response.titles;
            ClpColl.customers = response.customers;
            ClpColl.shipTos = response.shipTos;
            ClpColl.actives = response.actives;
            return ClpColl;
        }
    });
    //搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            pc_id: "",
            basic_container_type:0,
            cmd:"",
            title_value:0,
            ship_to_value:0,
            customer_value:0,
            active:1
        }
    });
    return {
        ClpModel:ClpModel
        ,ClpColl:ClpColl
        ,SearchModel:SearchModel
    };
});