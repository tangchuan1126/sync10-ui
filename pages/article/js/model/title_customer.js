define(['backbone','../config'], function (Backbone,Config) {
	var urlConfig = Config.productTitle.url;
    //生产商
    var Title = Backbone.Model.extend({
    	default : {
			id : 0,
			name : ""
    	}
    });  
    
    //品牌商
    var Customer = Backbone.Model.extend({
    	default : {
			id : 0,
			name : ""
    	}
    });        

    //生产商集合
    var CustomerCollection = Backbone.Collection.extend({model:Customer});       
    
    //品牌商生产商关系模型
    var CustomerTitle = Backbone.Model.extend({
    	url : urlConfig.base,
    	default : {
    		productId : 0,
    		title : {
    			"id" : 0,
    			"name" : ""
    		},
    		customers : []
    	}
    });


    
    //品牌商集合
    var TitleCollection = Backbone.Collection.extend(
    	{
	        url: urlConfig.base,
	        model: CustomerTitle
    	}
    );


    return {
    	Customer:Customer,
    	Title:Title,
    	TitleCollection:TitleCollection,   	
    	CustomerTitle:CustomerTitle,
    	CustomerCollection:CustomerCollection
    };    
});