﻿"use strict";
define([
    "js/view/product-table",
    "js/view/product-search",
    "js/view/product-picture",
    "js/view/product-update",
    "js/view/product-clptype",
    "js/view/product-title-customer",
    "js/view/product-title-customer-result",
    "js/view/product-info"
],function(
    ProductTable,
    ProductSearch,
    ProductPicture,
    ProductUpdate,
    ProductClptype,
    ProductTitle,
    ProductTitleResult,
    ProductInfo
){

    return {

        get:function(option){

            if(!this[option.name]){

                this.create(option);

            }else{

                this.update(option);
            }

            return this[option.name];
        },
        create:function(option){

            switch(option.name){

                case "ProductTable": this[option.name] = new ProductTable({el:option.el, view:this});break;
                case "ProductSearch": this[option.name] = new ProductSearch({el:option.el, view:this});break;
                case "ProductPicture": this[option.name] = new ProductPicture({el:option.el, view:this});break;
                case "ProductUpdate": this[option.name] = new ProductUpdate({el:option.el, view:this});break;
                case "ProductClptype": this[option.name] = new ProductClptype({el:option.el, view:this});break;
                case "ProductTitle": this[option.name] = new ProductTitle({el:option.el, view:this});break;
                case "ProductInfo": this[option.name] = new ProductInfo({el:option.el, view:this});break;
                case "ProductTitleResult": this[option.name] = new ProductTitleResult({el:option.el, view:this});break;
            }
        },
        update:function(option){

            this[option.name].setElement(option.el);
        }
    }
});