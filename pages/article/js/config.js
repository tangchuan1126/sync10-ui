﻿(function(){
    var configObj = {

        //商品列表
        productUrl:{
            url:"/Sync10/basicdata/product"
        },
        //商品基本信息
        productInfoUrl:{
            url:"/Sync10/basicdata/productInfo"
        },
        fileUrl:{
            url:"/Sync10/basicdata/product/file"
        },
        clpListUrl:{
            url:"/Sync10/basicdata/clpList"
        },
        productTitle : {
        	url : {
            	base : "/Sync10/basicdata/productCustomer",
            	updateIndex : "/Sync10/action/administrator/product/updateProductCustomerTitleIndex.action?pid={pid}",
            	getAllTitle : "/Sync10/basicdata/title/getAll",
            	getAllCustomer : "/Sync10/basicdata/customer/all"
        	},
        	customerSizePerRow : 4         //在Title/Customer 列表页，每一行 最多允许显示的customer 的个数
        },
        productContextUrl:{
            url:"/Sync10/basicdata/product/context/web"
        }
    };
    
	define(configObj);
	
}).call(this);

