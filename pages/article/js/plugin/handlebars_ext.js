define(['handlebars.runtime', "templates",'../config'],function(Handlebars,template,Config){
	var customerCountPerRow = Config.productTitle.customerSizePerRow;
	
    function getHtml(customers){
    	var content = "";
    	for(var i = 0; i < customerCountPerRow; i++){
    		content += "<td width=\"18%\">";
    		if( i < customers.length ){
    			content += customers[i].name;
    		}
    		
    		content += "</td>";
    	}
    	return content;
    }
	
	
	Handlebars = Handlebars.default;
    //定义一个输出title,customer 页面展示内容的函数
	Handlebars.registerHelper('content', function(title,customers,index,options) {
    	var rowCount = (customers.length % customerCountPerRow == 0) ? (customers.length / customerCountPerRow) : (Math.floor(customers.length/customerCountPerRow) + 1) ;
    	
    	var content = "";
    	content += "<tr class=\"record\" data-index=\"" + index + "\"><td rowspan=" + rowCount + " width=\"15%\">" + title.name + "</td>" + getHtml(customers.slice(0,customerCountPerRow)) + template.product_title_customer_operation({title:title,count:rowCount})  +  "</tr>";
    
    	if(rowCount > 1){
    		for(var i = 0; i < rowCount-1; i++){
    			content += "<tr class=\"record\" data-index=\"" + index + "\">" + getHtml(customers.slice((i+1)*customerCountPerRow,(i+2)*customerCountPerRow   )) + "</tr>";
    			
    		}
    		content += ""
    	}
        return content;
    });	
	
	return Handlebars;
});