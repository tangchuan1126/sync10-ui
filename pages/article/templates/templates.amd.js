define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['product_clptype'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.type_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.type_name : depth0), depth0))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\r\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "</option>\r\n";
},"7":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.active : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.active_name : depth0), depth0))
    + "</option>\r\n";
},"9":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.clps : depth0),{"name":"each","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"10":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "                <tr>\r\n                    <!-- clp type -->\r\n                    <td>\r\n                        <ul class=\"rb-grid clearfix\">\r\n                            <li class=\"td_TRAILER\">\r\n                                <div class=\"fieldset_itme\">\r\n                                    <div class=\"fieldsetconte clptype\">\r\n                                        <ul>\r\n                                            <li class=\"clearfix\">\r\n                                                <span class=\"NUMBERTYPE\">CLP Type : </span>\r\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.lp_name : depth0), depth0))
    + "</span>\r\n                                            </li>\r\n                                            <li class=\"clearfix\">\r\n                                                <span class=\"NUMBERTYPE\">Packaging Type : </span>\r\n                                                <a id=\"packaging_type_"
    + alias2(alias1((depth0 != null ? depth0.basic_type_id : depth0), depth0))
    + "\" _type_id=\""
    + alias2(alias1((depth0 != null ? depth0.basic_type_id : depth0), depth0))
    + "\" class=\"packaging_type\" href=\"javascript:void(0);\"><span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.type_name : depth0), depth0))
    + "</span></a>\r\n                                            </li>\r\n                                            <li class=\"clearfix\">\r\n                                                <span class=\"NUMBERTYPE\">Serialized Product : </span>\r\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.is_has_sn_en : depth0), depth0))
    + "</span>\r\n                                            </li>\r\n\r\n                                            <li class=\"clearfix\">\r\n                                                <span class=\"NUMBERTYPE\">Weight : </span>\r\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.total_weight : depth0), depth0))
    + " "
    + alias2(alias1((depth0 != null ? depth0.weight_uom_en : depth0), depth0))
    + "</span>\r\n                                            </li>\r\n                                        </ul>\r\n                                    </div>\r\n                                </div>\r\n                            </li>\r\n                        </ul>\r\n                    </td>\r\n                    <!-- Configuration -->\r\n                    <td>\r\n                        <ul class=\"rb-grid clearfix\">\r\n                            <li class=\"td_TRAILER\">\r\n                                <div class=\"fieldset_itme\">\r\n                                    <div class=\"fieldsetconte configuration\">\r\n                                        <ul>\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.inner_pc_or_lp : depth0),"1","<",{"name":"xifCond","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                            <li class=\"clearfix\">\r\n                                                <span class=\"NUMBERTYPE\">Stack : </span>\r\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.stack_length_qty : depth0), depth0))
    + " * "
    + alias2(alias1((depth0 != null ? depth0.stack_width_qty : depth0), depth0))
    + " * "
    + alias2(alias1((depth0 != null ? depth0.stack_height_qty : depth0), depth0))
    + "</span>\r\n                                            </li>\r\n                                            <li class=\"clearfix\">\r\n                                                <span class=\"NUMBERTYPE\">Product Qty : </span>\r\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.inner_total_pc : depth0), depth0))
    + "</span>\r\n                                            </li>\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.inner_pc_or_lp : depth0),"0",">",{"name":"xifCond","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                        </ul>\r\n                                    </div>\r\n                                </div>\r\n                            </li>\r\n                        </ul>\r\n                    </td>\r\n                    <!-- Title / Ship To -->\r\n                    <td>\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title_customer_ship : depth0),{"name":"if","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </td>\r\n                    <!-- Log -->\r\n                    <td>\r\n                    </td>\r\n                </tr>\r\n                <!-- button group -->\r\n                <tr>\r\n                    <td colspan=\"4\" style=\" text-align: right; background-color: white;\">\r\n                        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\r\n                          <button type=\"button\" class=\"btn btn-default\">CLP Lookup</button>\r\n                          <button type=\"button\" class=\"btn btn-default\">Print CLP Label</button>\r\n                          <button type=\"button\" class=\"btn btn-default\">Title/Customer/ShipTo</button>\r\n                          <button type=\"button\" class=\"btn btn-default\">Inactive</button>\r\n                        </div>\r\n                    </td>\r\n                </tr>\r\n";
},"11":function(depth0,helpers,partials,data) {
    return "                                                <li class=\"clearfix\">\r\n                                                    <span class=\"NUMBERTYPE\"></span>\r\n                                                    <span class=\"NUMBER\"></span>\r\n                                                </li>\r\n";
},"13":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda;

  return "                                                <li class=\"clearfix\">\r\n                                                    <span class=\"NUMBERTYPE\">Inner CLP Type : </span>\r\n                                                    <span class=\"NUMBER\">"
    + ((stack1 = alias1((depth0 != null ? depth0.inner_clp_html : depth0), depth0)) != null ? stack1 : "")
    + "</span>\r\n                                                </li>\r\n                                                <li class=\"clearfix\">\r\n                                                    <span class=\"NUMBERTYPE\">Inner CLP Qty : </span>\r\n                                                    <span class=\"NUMBER\">"
    + this.escapeExpression(alias1((depth0 != null ? depth0.inner_total_lp : depth0), depth0))
    + "</span>\r\n                                                </li>\r\n";
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return "                            <table class=\"table table-bordered table-striped table-hover product-title-shipto\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th style=\"width:30%;\">Title</th>\r\n                                        <th style=\"width:30%;\">Customer</th>\r\n                                        <th style=\"width:30%;\">ShipTo</th>\r\n                                        <th style=\"width:10%;\">Operation</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.title_customer_ship : depth0),{"name":"each","hash":{},"fn":this.program(16, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                </tbody>\r\n                            </table>\r\n                            <div style=\"float:right;\"><span style=\"text-decoration:underline;cursor:pointer;color:#00F\" class=\"more_or_less\" _lpt_id=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.lpt_id : depth0), depth0))
    + "\">More...</span></div>\r\n";
},"16":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(data && data.index),"1",">",{"name":"xifCond","hash":{},"fn":this.program(17, data, 0),"inverse":this.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "                                                <td><a id=\"_title\" href=\"javascript:void(0);\" class=\"underline\"><span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</span></a></td>\r\n                                                <td><a id=\"_customer\" href=\"javascript:void(0);\" class=\"underline\"><span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.customer_name : depth0), depth0))
    + "</span></a></td>\r\n                                                <td><a id=\"_ship_to\" href=\"javascript:void(0);\" class=\"underline\"><span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "</span></a></td>\r\n                                                <td><a id=\"_configuration\" href=\"javascript:void(0);\" class=\"underline\"><span class=\"NUMBER\">Configuration</span></a></td>\r\n                                            </tr>\r\n";
},"17":function(depth0,helpers,partials,data) {
    return "                                            <tr name=\"displayTitleAndShipTo_"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.sub_lpt_id : depth0), depth0))
    + "\" style=\"display:none\">\r\n";
},"19":function(depth0,helpers,partials,data) {
    return "                                            <tr>\r\n";
},"21":function(depth0,helpers,partials,data) {
    return "            <tr>\r\n                <td colspan=\"3\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\r\n            </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style type=\"text/css\">\r\n    .rb-grid li{\r\n        cursor: auto;\r\n    }\r\n    .rb-grid > li{\r\n        background: rgba(229, 229, 229, 0);\r\n        color: #000;\r\n        height: auto;\r\n        width: 100%;\r\n        padding: 0;\r\n        padding-top: 10px;\r\n    }\r\n    .rb-grid{\r\n        padding-top: 10px;\r\n    }\r\n    .clptype .NUMBERTYPE{\r\n        width: 130px;\r\n        font-size: 12px;\r\n    }\r\n    .configuration .NUMBERTYPE{\r\n        width: 100px;\r\n        font-size: 12px;\r\n    }\r\n    .fieldsetconte .NUMBER{\r\n        font-size: 12px;\r\n        color: #0066FF;\r\n    }\r\n    .packaging_type{\r\n        text-decoration: underline;\r\n        color: green;\r\n    }\r\n    .product-clptype{\r\n        width: 99%;\r\n        margin-left: 8px;\r\n    }\r\n    .underline{\r\n        text-decoration: underline;\r\n        color: green;\r\n    }\r\n    .underline .NUMBER{\r\n        font-size: 12px;\r\n        color: #0066FF;\r\n    }\r\n    .product-title-shipto{\r\n        margin-top: 10px;\r\n        margin-bottom: 5px;\r\n    }\r\n    .rb-product-info{\r\n        overflow-y: auto;\r\n        height: 100%;\r\n    }\r\n    .table-responsive{\r\n        display: inline;\r\n    }\r\n    \r\n</style>\r\n \r\n<!-- product info -->\r\n<div class=\"product_info\">\r\n\r\n</div>\r\n<!-- search & add button -->\r\n<div class=\"col-sm-12\" style=\"padding-bottom: 15px;margin-top: 0;\">\r\n  <div class=\"form-group\">\r\n    <div class=\"col-sm-2\">\r\n      <select class=\"form-control\" id=\"container_type_id\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.containers : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </select>\r\n    </div>\r\n    <div class=\"col-sm-2\">\r\n      <select class=\"form-control\" id=\"sku_lp_title_id\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </select>\r\n    </div>\r\n    <div class=\"col-sm-2\">\r\n      <select class=\"form-control\" id=\"sku_lp_customer_id\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </select>\r\n    </div>\r\n    <div class=\"col-sm-2\">\r\n      <select class=\"form-control\" id=\"sku_lp_to_ps_id\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.shipTos : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </select>\r\n    </div>\r\n    <div class=\"col-sm-2\">\r\n      <select class=\"form-control\" id=\"sku_lp_active\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.actives : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </select>\r\n    </div>\r\n    <div class=\"col-sm-2\">\r\n      <button class=\"btn btn-default\" type=\"button\" id=\"add_clp_type\"><i class=\"icon-plus\"></i>&nbsp;&nbsp;Add CLP Type</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"table-responsive\">\r\n    <table class=\"table table-striped table-bordered table-hover product-clptype\">\r\n        <thead>\r\n            <tr>\r\n                <th style=\"width:30%;\">CLP Type</th>\r\n                <th style=\"width:30%;\">Configuration</th>\r\n                <th style=\"width:30%;\">Title / Ship To</th>\r\n                <th style=\"width:10%;\">Log</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <!-- Start -->\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.clps : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.program(21, data, 0),"data":data})) != null ? stack1 : "")
    + "            <!-- End -->\r\n\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<div>\r\n    <br />\r\n    <br />\r\n</div>";
},"useData":true});
templates['product_info'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<style>\r\n/*\r\n.form-group .control-label{\r\n	padding: 0;\r\n}\r\n.col-sm-3 , .col-sm-1{\r\n	padding: 0;\r\n}\r\n.form-control-static{\r\n	padding: 3px;\r\n}\r\n*/\r\n.col-sm-12{\r\n	/*line-height:25px;*/\r\n    padding-bottom: 5px;\r\n	background-color: #E5E5E5;\r\n	border: 1px solid #BBB;\r\n	border-radius: 4px;\r\n	margin-bottom: 25px;\r\n  	margin-top: 25px;\r\n    width: 99%;\r\n    margin-left: 8px;\r\n}\r\n.form-group:first-child{\r\n	margin-top: 15px;\r\n}\r\n.info-text{\r\n    line-height: 9px;\r\n    padding-left: 0;\r\n    padding-bottom: 11px;\r\n}\r\n.info-title{\r\n    /*padding-right: 0;*/\r\n    padding-left: 0;\r\n}\r\n</style>\r\n\r\n<div class=\"col-sm-12 product-info\">\r\n    <div class=\"form-group\">\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Product Name : </label>\r\n    	<div class=\"col-sm-3 info-text\">\r\n    		<p class=\"form-control-static\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.p_name : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Length : </label>\r\n    	<div class=\"col-sm-1 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.length : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.length_uom_name : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Width : </label>\r\n    	<div class=\"col-sm-1 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.width : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.length_uom_name : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Height : </label>\r\n    	<div class=\"col-sm-1 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.heigth : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.length_uom_name : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Volume : </label>\r\n    	<div class=\"col-sm-1 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.volume : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Main Code : </label>\r\n    	<div class=\"col-sm-3 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.main_code : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Category : </label>\r\n    	<div class=\"col-sm-3 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.category_name : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    	<label class=\"col-sm-1 control-label text-right info-title\">Weight : </label>\r\n    	<div class=\"col-sm-3 info-text\">\r\n    		<p class=\"form-control-static \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.weight : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.productInfo : depth0)) != null ? stack1.weight_uom_name : stack1), depth0))
    + "</p>\r\n    	</div>\r\n    </div>\r\n</div>";
},"useData":true});
templates['product_picture'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n                        <div class=\"col-md-2\" style=\"margin-top: 20px;\">\n                            \n                            <div class=\"thumbnail\" >\n                                <div style=\"position: relative;\">\n\n                                    <div class=\"img-rel-block\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n\n                                        <a class=\"various\" rel=\"fancybox-thumb-sku\" href=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                            <img class=\"img-responsive\" style=\"max-width:99%;max-height:99%;display:inline-block;\" src=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\" /><span style=\"display:inline-block;vertical-align: middle;height: 100%;width: 0;\"></span>\n                                        </a>\n\n                                        <div style=\"position: absolute;top:0;right: 2px;\">\n                                            <button name=\"deletePicture\" type=\"button\" class=\"btn btn-default btn-sm\" style=\"display: none;\" aria-label=\"Left Align\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                                <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Delete\n                                            </button>\n                                        </div>\n                                        <div style=\"position: absolute;top:0;\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.file_cover : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "                                        </div>\n                                    </div>\n\n                                    <!--confirm dialog-->\n                                </div>\n                            </div>\n                        </div>\n";
},"2":function(depth0,helpers,partials,data) {
    return "                                            <span class=\"label label-success\">Cover</span>\n";
},"4":function(depth0,helpers,partials,data) {
    return "                                            <button name=\"coverPicture\" type=\"button\" class=\"btn btn-default btn-sm\" style=\"display: none;\" aria-label=\"Left Align\" data-fileid=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\" data-loading-text=\"Cover...\">\n                                                <span class=\"glyphicon glyphicon-picture\" aria-hidden=\"true\"></span> Make Cover\n                                            </button>\n";
},"6":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <div class=\"col-md-2\" style=\"margin-top: 20px;\">\n                            \n                        <div class=\"thumbnail\" >\n                            <div style=\"position: relative;\">\n\n                                <div class=\"img-rel-block\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n\n                                    <a class=\"various\" rel=\"fancybox-thumb-pkg\" href=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                        <img class=\"img-responsive\" style=\"max-width:99%;max-height:99%;display:inline-block;\" src=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\" /><span style=\"display:inline-block;vertical-align: middle;height: 100%;width: 0;\"></span>\n                                    </a>\n\n                                    <div style=\"position: absolute;top:0;right: 2px;\">\n                                        <button name=\"deletePicture\" type=\"button\" class=\"btn btn-default btn-sm\" style=\"display: none;\" aria-label=\"Left Align\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                            <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Delete\n                                        </button>\n                                    </div>\n                                </div>\n\n                                <!--confirm dialog-->\n                            </div>\n                        </div>\n                    </div>\n";
},"8":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <div class=\"col-md-2\" style=\"margin-top: 20px;\">\n                            \n                        <div class=\"thumbnail\" >\n                            <div style=\"position: relative;\">\n\n                                <div class=\"img-rel-block\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n\n                                    <a class=\"various\" rel=\"fancybox-thumb-btm\" href=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                        <img class=\"img-responsive\" style=\"max-width:99%;max-height:99%;display:inline-block;\" src=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\" /><span style=\"display:inline-block;vertical-align: middle;height: 100%;width: 0;\"></span>\n                                    </a>\n\n                                    <div style=\"position: absolute;top:0;right: 2px;\">\n                                        <button name=\"deletePicture\" type=\"button\" class=\"btn btn-default btn-sm\" style=\"display: none;\" aria-label=\"Left Align\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                            <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Delete\n                                        </button>\n                                    </div>\n                                </div>\n\n                                <!--confirm dialog-->\n                            </div>\n                        </div>\n                    </div>\n";
},"10":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <div class=\"col-md-2\" style=\"margin-top: 20px;\">\n                            \n                        <div class=\"thumbnail\" >\n                            <div style=\"position: relative;\">\n\n                                <div class=\"img-rel-block\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n\n                                    <a class=\"various\" rel=\"fancybox-thumb-lbl\" href=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                        <img class=\"img-responsive\" style=\"max-width:99%;max-height:99%;display:inline-block;\" src=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\" /><span style=\"display:inline-block;vertical-align: middle;height: 100%;width: 0;\"></span>\n                                    </a>\n\n                                    <div style=\"position: absolute;top:0;right: 2px;\">\n                                        <button name=\"deletePicture\" type=\"button\" class=\"btn btn-default btn-sm\" style=\"display: none;\" aria-label=\"Left Align\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                            <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Delete\n                                        </button>\n                                    </div>\n                                </div>\n\n                                <!--confirm dialog-->\n                            </div>\n                        </div>\n                    </div>\n";
},"12":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <div class=\"col-md-2\" style=\"margin-top: 20px;\">\n                            \n                        <div class=\"thumbnail\" >\n                            <div style=\"position: relative;\">\n\n                                <div class=\"img-rel-block\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n\n                                    <a class=\"various\" rel=\"fancybox-thumb-wgt\" href=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                        <img class=\"img-responsive\" style=\"max-width:99%;max-height:99%;display:inline-block;\" src=\"/Sync10/_fileserv/file/"
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\" /><span style=\"display:inline-block;vertical-align: middle;height: 100%;width: 0;\"></span>\n                                    </a>\n\n                                    <div style=\"position: absolute;top:0;right: 2px;\">\n                                        <button name=\"deletePicture\" type=\"button\" class=\"btn btn-default btn-sm\" style=\"display: none;\" aria-label=\"Left Align\" data-fileid=\""
    + alias2(alias1((depth0 != null ? depth0.file_id : depth0), depth0))
    + "\">\n                                            <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Delete\n                                        </button>\n                                    </div>\n                                </div>\n\n                                <!--confirm dialog-->\n                            </div>\n                        </div>\n                    </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<!-- upload photo modal -->\n<div class=\"modal fade bs-example-modal-lg\" id=\"upload_picture_modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\n    \n    <div class=\"modal-dialog modal-lg\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n                <h4 class=\"modal-title\" id=\"myModalLabel\">Upload File</h4>\n            </div>\n            <div class=\"modal-body\">\n            </div>\n        </div>\n    </div>\n</div>\n\n<!--delete photo modal-->\n<div style=\"display:none;\" id=\"alert_confirm_content\">\n    \n    <div style=\"position: absolute;width:100%;height:100%;top:0;cursor:default;background-color: #000;opacity: .5;-webkit-border-radius: 5px;\">\n    </div>\n\n    <div class=\"alert alert-dismissible fade in\" role=\"alert\" style=\"position: absolute;width:100%;top: 30%;;cursor:default;text-align: center;\">\n        \n        <h4 style=\"color: white;\">Confirm Delete?</h4>\n\n            <div class=\"col-md-12\">\n                <button type=\"button\" class=\"btn btn-warning btn-sm\" name=\"yes\" data-loading-text=\"Deleting...\" style=\"margin-right: 5px;\">YES</button>\n                <button type=\"button\" class=\"btn btn-default btn-sm\" name=\"no\" data-loading-text=\"NO\">NO</button>\n            </div>\n    </div>\n</div>\n\n<div class=\"container\">\n    <div class=\"row\" style=\"margin-top: 20px;\">\n        <div class=\"col-md-12\">\n\n            <!-- Nav tabs -->\n            <ul class=\"nav nav-tabs\" name=\"picture_tabs\">\n\n                <li class=\"active\" data-class=\"2\">\n                    <a href=\"#tab_pic_sku\" data-toggle=\"tab\">SKU</a>\n                </li>\n                <li data-class=\"1\"><a href=\"#tab_pic_pkg\" data-toggle=\"tab\">PKG</a></li>\n                <li data-class=\"3\"><a href=\"#tab_pic_btm\" data-toggle=\"tab\">BTM</a></li>\n                <li data-class=\"5\"><a href=\"#tab_pic_lbl\" data-toggle=\"tab\">LBL</a></li>\n                <li data-class=\"4\"><a href=\"#tab_pic_wgt\" data-toggle=\"tab\">WGT</a></li>\n            </ul>\n\n            <!-- Tab panes -->\n            <div class=\"tab-content mb30\">\n\n                <div class=\"tab-pane active\" id=\"tab_pic_sku\">\n\n                    <div class=\"row\" style=\"margin-top: 10px;\"> \n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-info\" name=\"upload_picture_btn\">\n                                <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span> Upload Photo\n                            </button>\n                        </div>\n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-default\" name=\"capture_picture_btn\">\n                                <span class=\"glyphicon glyphicon-camera\" aria-hidden=\"true\"></span> Capture Online\n                            </button>\n                        </div>\n                        <div class=\"col-md-10\" style=\"text-align:right;\">\n                            <h4 class=\"nomargin\" style=\"color: #636E7B;\">Note:The photos where has words or jack on the product, 3~6</h4>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.file_sku : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab_pic_pkg\">\n\n                    <div class=\"row\" style=\"margin-top: 10px;\"> \n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-info\" name=\"upload_picture_btn\">\n                                <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span> Upload Photo\n                            </button>\n                        </div>\n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-default\" name=\"capture_picture_btn\">\n                                <span class=\"glyphicon glyphicon-camera\" aria-hidden=\"true\"></span> Capture Online\n                            </button>\n                        </div>\n                        <div class=\"col-md-10\" style=\"text-align:right;\">\n                            <h4 class=\"nomargin\" style=\"color: #636E7B;\">Note:The photos where has words on the product package surface, 3~6</h4>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.file_pkg : depth0),{"name":"each","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    \n                    </div>\n\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab_pic_btm\">\n\n                    <div class=\"row\" style=\"margin-top: 10px;\"> \n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-info\" name=\"upload_picture_btn\">\n                                <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span> Upload Photo\n                            </button>\n                        </div>\n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-default\" name=\"capture_picture_btn\">\n                                <span class=\"glyphicon glyphicon-camera\" aria-hidden=\"true\"></span> Capture Online\n                            </button>\n                        </div>\n                        <div class=\"col-md-10\" style=\"text-align:right;\">\n                            <h4 class=\"nomargin\" style=\"color: #636E7B;\">Note:The photos where has words,icons,logo,model that pastes on the bottom of product, 3~6</h4>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.file_btm : depth0),{"name":"each","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    \n                    </div>\n                </div>\n\n                <div class=\"tab-pane\" id=\"tab_pic_lbl\">\n\n                    <div class=\"row\" style=\"margin-top: 10px;\"> \n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-info\" name=\"upload_picture_btn\">\n                                <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span> Upload Photo\n                            </button>\n                        </div>\n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-default\" name=\"capture_picture_btn\">\n                                <span class=\"glyphicon glyphicon-camera\" aria-hidden=\"true\"></span> Capture Online\n                            </button>\n                        </div>\n                        <div class=\"col-md-10\" style=\"text-align:right;\">\n                            <h4 class=\"nomargin\" style=\"color: #636E7B;\">Note:The photos where pastes the labels of your company's code, 3~6</h4>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.file_lbl : depth0),{"name":"each","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    \n                    </div>\n\n                </div>\n                <div class=\"tab-pane\" id=\"tab_pic_wgt\">\n\n                    <div class=\"row\" style=\"margin-top: 10px;\"> \n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-info\" name=\"upload_picture_btn\">\n                                <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span> Upload Photo\n                            </button>\n                        </div>\n                        <div class=\"col-md-1\">\n                            <button type=\"button\" class=\"btn btn-default\" name=\"capture_picture_btn\">\n                                <span class=\"glyphicon glyphicon-camera\" aria-hidden=\"true\"></span> Capture Online\n                            </button>\n                        </div>\n                        <div class=\"col-md-10\" style=\"text-align:right;\">\n                            <h4 class=\"nomargin\" style=\"color: #636E7B;\">Note:The photos of the weight when put the product on the electronic scale, 3~6</h4>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.file_wgt : depth0),{"name":"each","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['product_search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<ul class=\"nav nav-tabs\">\n    <li class=\"active\"><a href=\"#advanced_search\" data-toggle=\"tab\">Advanced Search</a></li>\n    <li><a href=\"#common_tools\" data-toggle=\"tab\">Common Tools</a></li>\n</ul>\n\n<!-- Tab panes -->\n<div class=\"tab-content\">\n    \n    <div class=\"tab-pane active\" id=\"advanced_search\">\n\n        <form class=\"form-horizontal\">\n\n            <div class=\"col-sm-3\">\n                <input type=\"email\" class=\"form-control\" id=\"search_product\" placeholder=\"\">\n            </div>\n\n            <button type=\"button\" class=\"btn btn-primary\">\n                <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span>\n                Search\n            </button>\n            \n            <div class=\"btn-group\"> \n                <button type=\"button\" class=\"btn btn-warning\">\n                   <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\n                    Add\n                </button>\n                \n                <button type=\"button\" class=\"btn btn-warning dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n                    <span class=\"caret\"></span>\n                    <span class=\"sr-only\">Toggle Dropdown</span>\n                </button>\n                \n                <ul class=\"dropdown-menu\" role=\"menu\">\n                    <li><a href=\"#\"><i class=\"icon-download-alt\"></i>&nbsp;Download Template</a></li>\n                </ul>\n            </div>\n        </form>\n    </div>\n\n    <div class=\"tab-pane active\" id=\"common_tools\">\n\n    </div>\n    \n</div>\n";
},"useData":true});
templates['product_table'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr style=\"height: 200px;\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n            \n                <td class=\"grid_items\" id=\"a_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                    <ul name=\"product_picture\">\n                        <li>\n\n                            <div class=\"ch-item ch-img-1\" \n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.cover : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n                            \n                                <div class=\"ch-info\">\n                                    <!--查询结构-->\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.filecnt : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                </div>\n                            </div>\n                        </li>\n                    </ul>\n\n                    <div class=\"rb-overlay\" style=\"z-index: 10;\">\n                        <span class=\"rb-close\">close</span>\n                        <div class=\"rb-title\"></div> \n                        <div style=\"overflow: auto;\" class=\"rb-product-info rb-week\" id=\"product_picture_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n\n                            <!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!商品图片-->\n                        </div>\n                    </div>\n                </td>\n                <td class=\"grid_items\" id=\"b_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                    <ul name=\"product_update\">\n                        <li class=\"td_TRAILER\" >\n                            <fieldset class=\"TRAILER\">\n                                <legend class=\"td_legend\">\n                                    <span class=\"E_DLO_ID\">"
    + alias2(alias1((depth0 != null ? depth0.maincode : depth0), depth0))
    + "</span>\n                                </legend>\n                                <div class=\"fieldset_itme\">\n                                    <div class=\"fieldsetconte\">\n                                        <ul>\n                                            <li>\n                                                <span class=\"NUMBERTYPE\">PName : </span>\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.p_name : depth0), depth0))
    + "</span>\n                                            </li>\n                                            <li>\n                                                <span class=\"NUMBERTYPE\">Dimension : </span>\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.length : depth0), depth0))
    + " * "
    + alias2(alias1((depth0 != null ? depth0.width : depth0), depth0))
    + " * "
    + alias2(alias1((depth0 != null ? depth0.heigth : depth0), depth0))
    + " ("
    + alias2(alias1((depth0 != null ? depth0.length_uom_name : depth0), depth0))
    + ")</span>\n                                            </li>\n                                            <li>\n                                                <span class=\"NUMBERTYPE\">Weight : </span>\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.weight : depth0), depth0))
    + " ("
    + alias2(alias1((depth0 != null ? depth0.weight_uom_name : depth0), depth0))
    + ")</span>\n                                            </li>\n       \n                                            <li>\n                                                <span class=\"NUMBERTYPE\">Category : </span>\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.category_name : depth0), depth0))
    + "</span>\n                                            </li>\n                                        </ul>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </li>\n                    </ul>\n\n                    <div class=\"rb-overlay\">\n                        <span class=\"rb-close\">close</span>\n                        <div class=\"rb-title\"></div>\n                        <div class=\"rb-product-info rb-week\" id=\"product_update_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                              <!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!更新商品-->\n                        </div>\n                    </div>\n                </td>\n                <td class=\"grid_items\" id=\"c_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                    <ul name=\"product_title\">\n                        <li class=\"td_TRACTOR\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.ptc : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </li>\n                    </ul>\n\n                    <div class=\"rb-overlay\">\n                        <div class=\"rb-title\"></div>\n                        <span class=\"rb-close\">close</span>\n                        <div class=\"rb-product-info rb-week\" id=\"product_title_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                            <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!更新customer / title -->\n                        </div>\n                    </div>\n                </td>\n                <td class=\"grid_items\" id=\"d_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                    <ul name=\"product_clptype\">\n                        <li class=\"td_TRAILER\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.clptype : depth0),{"name":"each","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </li>\n                    </ul>\n\n                    <div class=\"rb-overlay\">\n                        <span class=\"rb-close\">close</span>\n                        <div class=\"rb-title\"></div>\n                        <div class=\"rb-product-info rb-week\" id=\"product_clptype_"
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\n                            <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! clp type -->\n                        </div>\n                    </div>\n                </td>\n            </tr>\n";
},"2":function(depth0,helpers,partials,data) {
    return "                                style=\"background-image: url(/Sync10/_fileserv/file/"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.cover : depth0), depth0))
    + ");\"\n                            ";
},"4":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                    <p>"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + ":"
    + alias2(alias1((depth0 != null ? depth0.cnt : depth0), depth0))
    + "</p>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return "                            <fieldset class=\"\">\n                                <legend>\n                                    <span class=\"Sub_trailer\" data-toggle=\"popover\" data-trigger=\"click\" title=\"Title\" \n                                        data-placement=\"bottom\"\n                                        data-container=\"body\"\n                                        data-content=\""
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\">\n\n                                        Title / Customer\n                                    </span>\n                                </legend>\n                                \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.ptc : depth0),{"name":"each","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                            </fieldset>\n";
},"7":function(depth0,helpers,partials,data) {
    return " "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + " ";
},"9":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                <div class=\"Tractor_ulitme row\">\n                                    \n                                    <div class=\"col-md-5\" style=\"text-align: right;\">\n                                        "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "\n                                    </div>\n                                    <div class=\"col-md-7\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customer : depth0),{"name":"each","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                    </div>\n                                    \n                                </div>\n";
},"10":function(depth0,helpers,partials,data) {
    return "                                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.customer_name : depth0), depth0))
    + "<br>\n";
},"12":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <fieldset class=\"TRAILER\">\n                                <legend class=\"td_legend\"><span class=\"E_DLO_ID\">"
    + alias2(alias1((depth0 != null ? depth0.lp_name : depth0), depth0))
    + "</span></legend>\n                                <div class=\"fieldset_itme\">\n                                    <div class=\"fieldsetconte\">\n                                        <ul>\n                                            <li><span class=\"NUMBERTYPE\">Inner Type: </span><span class=\"NUMBER\">Product</span></li>\n                                            <li>\n                                                <span class=\"NUMBERTYPE\">Product Qty: </span>\n                                                <span class=\"NUMBER\">"
    + alias2(alias1((depth0 != null ? depth0.inner_total_pc : depth0), depth0))
    + "</span>\n                                            </li>\n                                        </ul>\n                                    </div>\n                                    <div class=\"fieldset_itme\">\n                                        <fieldset>\n                                            <legend class=\"itme_legend\" align=\"left\">\n                                                <span class=\"DOOR\">Customer / Title / Shipto</span>\n                                            </legend>\n                                            <div class=\"fieldsetconte\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.tcs : depth0),{"name":"each","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                                            </div>\n                                        </fieldset>\n                                    </div>\n                                </div>\n                            </fieldset>\n";
},"13":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                                <div class=\"field_customer_title_shipto\">\n                                                    <span>"
    + alias2(alias1((depth0 != null ? depth0.customer_name : depth0), depth0))
    + "</span>\n                                                    <span>"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</span>\n                                                    <span>"
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "</span>\n                                                </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div data-example-id=\"bordered-table\">\n\n    <table class=\"table table-bordered table-striped table-hover product-list\">\n        <thead>\n            <tr>\n                <th style=\"width:15%;\">Picture</th>\n                <th style=\"width:25%;\">Product</th>\n                <th style=\"width:25%;\">Title / Customer</th>\n                <th style=\"width:35%;\">CLP Type</th>\n            </tr>\n        </thead>\n        <tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.product : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </tbody>\n    </table>\n</div>\n\n<div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n    <ul id=\"pagination\" class=\"pagebox\">\n    </ul>\n</div>";
},"useData":true});
templates['product_title_customer'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value=\""
    + alias3(((helper = (helper = helpers.title_id || (depth0 != null ? depth0.title_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:80%;margin-left:10%;margin-top:22px;\">\n\n	<form id=\"customerTitleForm\" method=\"post\">\n		<table width=\"100%\" style=\"margin:5px 5px 5px 5px;\" align=\"center\">\n			<tr>\n				<td align=\"left\" width=\"24%\">\n					<select id=\"title\" name=\"title\" style=\"width:100%;\" data-placeholder=\"Title...\"> \n						<option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titleList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n				</td>\n				<td width=\"10%\" align=\"right\">Customer:</td>\n				<td  align=\"left\" width=\"48%\">\n					<select id=\"customer\" name=\"customer\" style=\"width:350px;\" multiple=\"multiple\"> \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customerList : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>			\n				</td>				\n				<td width=\"6.4%\" align=\"right\" valign=\"center\">\n					<div style=\"position:relative;left:2px;padding-bottom:3px;\"> \n						<a id=\"addButton\" href=\"javascript:void(0)\" style=\"margin-top:6px;\"  class=\"buttons icon add\" style=\"background-image:none;\"></a> \n					</div>\n				</td>\n				\n				<td width=\"7%\" align=\"right\" valign=\"center\">\n                    <a href=\"javascript:void(0)\" class=\"buttons cancel\" style=\"display:none;height:25px;width:36px;\" title=\"Cancel\"><i class=\"fa fa-minus-circle\" style=\"font-size:12px;\"></i></a>\n				</td>\n				<td width=\"18\"></td>\n			</tr>\n		</table>\n	</form>\n\n	<div id=\"customer_title_head\" style=\"margin-top:50px;width:98%;border: 0px solid #BBB;border-top-left-radius:5px;border-top-right-radius:5px;padding: 0px;margin:0 0.9%;;\">\n		<table width=\"100%\"  cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n			<tr class=\"alt\" style=\"background-color: #E5E5E5;\" height=\"40px\"> \n		        <th width=\"15%\" style=\"vertical-align: center;text-align: center;\" class=\"left-title\">Title</th>\n		        <th width=\"72%\" style=\"vertical-align: center;text-align: center;\" class=\"left-title\">Customer</th>\n		        <th class=\"left-title\" style=\"vertical-align: center;text-align: center;\">Operation</th>\n		    </tr>\n		</table>\n	</div>	\n					\n	<div  class=\"result\" style=\"width:98%;height:349px;border: 0px solid #BBB;border-bottom-left-radius:5px;border-bottom-right-radius:5px;padding: 0px;margin:0 0.9%;overflow-y:scroll;overflow-x:hide;\">\n	</div>	\n	\n</div>\n	";
},"useData":true});
templates['product_title_customer_operation'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=this.lambda;

  return "<td align=\"center\" valign=\"center\" rowspan=\""
    + alias1(((helper = (helper = helpers.count || (depth0 != null ? depth0.count : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"count","hash":{},"data":data}) : helper)))
    + "\"> \n    <a title=\"Edit\"  class=\"buttons icon edit\" name=\"mod\" href=\"javascript:void(0)\"  data-titleId=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"></a>\n    <a title=\"Delete\"  class=\"buttons icon remove\" name=\"del\" href=\"javascript:void(0)\"   data-titleId=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"></a>					\n</td>	";
},"useData":true});
templates['product_title_customer_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "				"
    + ((stack1 = (helpers.content || (depth0 && depth0.content) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.title : depth0),(depth0 != null ? depth0.customers : depth0),(data && data.index),{"name":"content","hash":{},"data":data})) != null ? stack1 : "")
    + "	\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "		<table width=\"100%\" class=\"zebraTable\"  cellspacing=\"0\" cellpadding=\"0\"  align=\"left\" style=\"margin-top:20px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.title_customers : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</table>";
},"useData":true});
templates['product_update'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n			            <form class=\"form-horizontal\" name=\"update_product_from\">\n						  \n						  <div class=\"form-group\">\n						    <label for=\"p_name\" class=\"col-sm-2 control-label\">Product Name</label>\n\n						    <div class=\"col-sm-7\">\n						      	<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.p_name : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"p_name\" name=\"p_name\" placeholder=\"PHILIPS32PHF3056/T3\">\n						      	<div style=\"display:none\">\n									<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n									<span class=\"sr-only\">(error)</span>\n									<span class=\"help-block alert-error\"></span>\n								</div>\n						    </div>\n						  </div>\n  \n						  <div class=\"form-group\">\n						    <label for=\"maincode\" class=\"col-sm-2 control-label\">Main Code</label>\n\n						    <div class=\"col-sm-7\">\n						 		<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.maincode : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"maincode\" name=\"maincode\" placeholder=\"\">\n						 		<div style=\"display:none\">\n									<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n									<span class=\"sr-only\">(error)</span>\n									<span class=\"help-block alert-error\">Error the main code has required</span>\n								</div>\n						    </div>\n						  </div>\n\n						  <div class=\"form-group\">\n						    <label for=\"nmfc_code\" class=\"col-sm-2 control-label\">NMFC Code</label>\n						    <div class=\"col-sm-7\">\n						      <input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.nmfc_code : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"nmfc_code\" name=\"nmfc_code\" placeholder=\"\">\n						    </div>\n						  </div>\n\n						  <div class=\"form-group\">\n						    <label for=\"freight_class\" class=\"col-sm-2 control-label\">Freight Class</label>\n						    <div class=\"col-sm-7\">\n						      <input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.freight_class : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"freight_class\" name=\"freight_class\" placeholder=\"\">\n						    </div>\n						  </div>\n\n						<div class=\"form-group\">\n						    <label for=\"length\" class=\"col-sm-2 control-label\">L*W*H</label>\n\n						    <div class=\"col-sm-7\">\n						    	\n								<div class=\"row\">\n							    	<div class=\"col-sm-3\">\n							    		<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.length : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"length\" name=\"size\" placeholder=\"731.0\">\n\n							    		<div style=\"display:none\">\n											<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n											<span class=\"sr-only\">(error)</span>\n										</div>\n							    	</div>\n							    	<div class=\"col-sm-3\">\n							    		<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.width : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"width\" name=\"size\" placeholder=\"465.5\">\n\n							    		<div style=\"display:none\">\n											<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n											<span class=\"sr-only\">(error)</span>\n										</div>\n							    	</div>\n							    	<div class=\"col-sm-3\">\n							    		<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.heigth : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"height\" name=\"size\" placeholder=\"169.7\">\n							    		\n							    		<div style=\"display:none\">\n											<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n											<span class=\"sr-only\">(error)</span>\n										</div>\n							    	</div>\n							    	<div class=\"col-sm-3\">\n\n							    		<select class=\"form-control input-length\" id=\"length_uom\" name=\"length_uom\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.lengthUom : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "										</select>\n							    	</div>\n						    	</div>\n\n						    	<span class=\"help-block alert-error\" style=\"color:#a94442; display:none;\">Error the main code has required</span>\n						    </div>\n						</div>\n\n						<div class=\"form-group\">\n						    <label for=\"weight\" class=\"col-sm-2 control-label\">Weight</label>\n						    \n						    <div class=\"col-sm-7\">\n\n						    	<div class=\"row\">\n\n							    	<div class=\"col-sm-9\">\n										<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.weight : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"weight\" name=\"weight\" placeholder=\"5.1\">\n										\n										<div style=\"display:none\">\n											<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n											<span class=\"sr-only\">(error)</span>\n											<span class=\"help-block alert-error\">Error the weight has required</span>\n										</div>\n									</div>\n								    <div class=\"col-sm-3\">\n								   		<select class=\"form-control input-length\" id=\"weight_uom\" name=\"weight_uom\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.weightUom : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "										</select>\n								    </div>\n						    	</div>\n						    </div>\n						</div>\n\n						<div class=\"form-group\">\n						    <label for=\"price\" class=\"col-sm-2 control-label\">Price</label>\n\n						    <div class=\"col-sm-7\">\n\n						    	<div class=\"row\">\n\n							    	<div class=\"col-sm-9\">\n								 		<div class=\"input-group\">\n								      		<div class=\"input-group-addon\">$</div>\n								      		<input value=\""
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.price : stack1), depth0))
    + "\" type=\"text\" class=\"form-control input-length\" id=\"price\" name=\"price\" placeholder=\"1399.00\">\n								      	</div>\n								      	<div style=\"display:none\">\n											<span class=\"glyphicon glyphicon-remove form-control-feedback\" aria-hidden=\"true\"></span>\n											<span class=\"sr-only\">(error)</span>\n											<span class=\"help-block alert-error\">Error the price has required</span>\n										</div>\n								    </div>\n								    <div class=\"col-sm-3\">\n								      	<select class=\"form-control input-length\" id=\"price_uom\" name=\"price_uom\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.priceUom : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "										</select>\n								    </div>\n						    	</div>\n						    </div>\n						</div>\n\n						<div class=\"form-group\" style=\"margin-bottom: 7px !important;\">\n						    <label for=\"sn_size\" class=\"col-sm-2 control-label\">SN Length</label>\n						    <div class=\"col-sm-7\">\n						      	<div class=\"row\">\n\n							    	<div class=\"col-sm-4\" style=\"margin-bottom: 10px;\">\n							    		<div class=\"input-group\">\n									   	  <input value=\"\" type=\"text\" class=\"form-control input-length\" name=\"sn_size\" placeholder=\"\">\n									      <span class=\"input-group-btn\">\n									        <button class=\"btn btn-default input-length\" name=\"del_sn_size\" type=\"button\">\n									        	<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span>\n									        </button>\n									      </span>\n									    </div>\n							    	</div>\n\n							    	<div class=\"col-sm-4\" style=\"margin-bottom: 10px;\">\n							    		<button class=\"btn btn-default btn-sm\" type=\"button\" id=\"add_sn_size\" name=\"add_sn_size\" style=\"margin-top: 8px;\">\n								        	<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\n								        </button>\n							    	</div>\n\n							    	<div style=\"display:none\" id=\"sn_size_templates\">\n							    		<div class=\"col-sm-4\" style=\"margin-bottom: 10px;\">\n								    		<div class=\"input-group\">\n										   	  <input value=\"\" type=\"text\" class=\"form-control input-length\" name=\"sn_size\" placeholder=\"\">\n										      <span class=\"input-group-btn\">\n										        <button class=\"btn btn-default input-length\" name=\"del_sn_size\"  type=\"button\">\n										        	<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span>\n										        </button>\n										      </span>\n										    </div>\n								    	</div>\n							    	</div>		    	\n						    	</div>\n						    </div>\n						</div>\n\n						<div class=\"form-group\">\n						    <label for=\"description\" class=\"col-sm-2 control-label\">Description</label>\n						    <div class=\"col-sm-7\">\n						      <textarea class=\"form-control\" rows=\"3\" id=\"description\" name=\"description\">"
    + alias2(alias1(((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.description : stack1), depth0))
    + "</textarea>\n						    </div>\n						</div>\n\n						<div class=\"form-group\">\n							<div class=\"col-sm-2\"></div>\n						    <div class=\"col-sm-1\">\n						      <button type=\"button\" class=\"btn btn-primary\" name=\"submit\" > Submit </button>\n						    </div>\n						    <div class=\"col-sm-1\">\n						      <button type=\"button\" class=\"btn btn-default\" name=\"cancel\" > Cancel </button>\n						    </div>\n						</div>\n						</form>\n";
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "												<option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.id : depth0),((stack1 = (depths[2] != null ? depths[2].model : depths[2])) != null ? stack1.length_uom : stack1),{"name":"sif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n													"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n												</option>\n";
},"3":function(depth0,helpers,partials,data) {
    return "selected";
},"5":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "											<option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.id : depth0),((stack1 = (depths[2] != null ? depths[2].model : depths[2])) != null ? stack1.weight_uom : stack1),{"name":"sif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n												"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n											</option>\n";
},"7":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "											<option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" "
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.id : depth0),((stack1 = (depths[2] != null ? depths[2].model : depths[2])) != null ? stack1.price_uom : stack1),{"name":"sif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ">\n												"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n											</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "\n<div class=\"container\">\n\n    <div class=\"row row-dashboard\" style=\"margin-top: 20px;\">\n\n    	<ul class=\"nav nav-tabs\" name=\"product_step_tab\">\n            <li class=\"active\"><a href=\"#update_product\" data-toggle=\"tab\"><strong>Step 1:</strong> Basic Info</a></li>\n            <li><a href=\"#product_code\" data-toggle=\"tab\"><strong>Step 2:</strong> Product Codes</a></li>\n            <li><a href=\"#product_category\" data-toggle=\"tab\"><strong>Step 3:</strong> Product Category</a></li>\n        </ul>\n        \n        <div class=\"tab-content\">\n	        <div role=\"tabpanel\" class=\"tab-pane active panel panel-default\" id=\"update_product\">\n			  <div class=\"panel-body\">\n\n			    	<div class=\"col-md-2\"></div>\n			        <div class=\"col-md-7\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.context : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			        </div>\n\n			        <div class=\"col-md-3\">\n			    	</div>\n			  </div>\n			  <div class=\"panel-footer\">Panel footer</div>\n			</div>\n\n			<div role=\"tabpanel\" class=\"tab-pane\" id=\"product_code\"></div>\n\n			<div role=\"tabpanel\" class=\"tab-pane\" id=\"product_category\">\n			</div>\n		</div>\n    </div>\n</div>";
},"useData":true,"useDepths":true});
return templates;
});