define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
return templates['search_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression;

  return "    <tr>\n      <th scope=\"row\">"
    + alias1(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "</th>\n      <td>"
    + alias1(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</td>\n      <td>\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customer : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        <ul>\n      </td>\n      <td>\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customer : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </ul>\n      </td>\n      <td>\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.category : depth0),{"name":"each","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n      </td>\n      <td>\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.line : depth0),{"name":"each","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n      </td>\n    </tr>\n";
},"2":function(depth0,helpers,partials,data) {
    return "\n          <p>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "</p>\n\n";
},"4":function(depth0,helpers,partials,data) {
    return "\n          <p>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.p_name : depth0), depth0))
    + "</p>\n\n";
},"6":function(depth0,helpers,partials,data) {
    return "\n          <p>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title : depth0), depth0))
    + "</p>\n\n\n";
},"8":function(depth0,helpers,partials,data) {
    return "\n          <p>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "</p>\n \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "\n<div class=\"panel panel-default\">\n  \n  <div class=\"panel-heading\">Title Customer Product Category Line Relation</div>\n  <div class=\"panel-body\">\n\n    <div class=\"row\">\n      <div class=\"col-lg-2\">\n\n          <select id=\"searchType\" class=\"form-control\">\n            <option value=\"title\">Title</option>\n            <option value=\"customer\">Customer</option>\n            <option value=\"product\">Product</option>\n            <option value=\"category\">Category</option>\n            <option value=\"line\">Line</option>\n          </select>\n      </div>\n      <div class=\"col-lg-6\">\n        <div class=\"input-group\">\n          <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\" id=\"searchKey\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.searchKey || (depth0 != null ? depth0.searchKey : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"searchKey","hash":{},"data":data}) : helper)))
    + "\">\n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-default\" type=\"button\" id=\"search\">Go!</button>\n          </span>\n        </div>\n      </div>\n    </div>\n\n  </div>\n<table class=\"table table-striped\">\n  \n  <caption></caption>\n  <thead>\n    <tr>\n      <th>#</th>\n      <th>Title</th>\n      <th>Customer</th>\n      <th>Product</th>\n      <th>Category</th>\n      <th>Line</th>\n    </tr>\n  </thead>\n  <tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n  </tbody>\n</table>\n\n</div>";
},"useData":true});
});