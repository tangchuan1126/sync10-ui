define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addMenu'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "	  		<div id=\"menu\" class=\"dialog_update_content\">\n		  		<form id=\"menuForm\" method=\"post\" >\n		  			<table>\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Title</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"title\" id=\"title\" maxlength=\"50\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <span>Uri</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"link\" id=\"link\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>				            \n			            <tr>\n			                <td>\n			                    <span>Type</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n									<input type=\"radio\" name=\"application_type\" value=\"1\" checked=\"true\">Web &nbsp;\n					  	  		    <input type=\"radio\" name=\"application_type\" value=\"2\" >App\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>				            					  	  				  	  \n		  			</table>\n				</form>   \n			</div> ";
},"useData":true});
templates['cancle_auth'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<font size=\"2\" color=\"#000\"> Are you sure to cancle access restrictions of all resources under this menu? </font>";
},"useData":true});
templates['confirm_auth'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<font size=\"2\" color=\"#000\">Are you sure to add access restrictions of all resources under this menu?</font>";
},"useData":true});
templates['menuMod'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	  		<div id=\"menu\" class=\"dialog_update_content\">\n		  		<form id=\"menuForm\" method=\"post\" >\n		  			<input type=\"hidden\" name=\"id\" id=\"id\" value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"/>\n		  			<table>\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Title</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"title\" id=\"title\" value=\""
    + alias3(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"text","hash":{},"data":data}) : helper)))
    + "\" maxlength=\"50\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <span>Uri</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"link\" id=\"link\" value=\""
    + alias3(((helper = (helper = helpers.link || (depth0 != null ? depth0.link : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"link","hash":{},"data":data}) : helper)))
    + "\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>				            \n		  			</table>\n				</form>   \n			</div> 			";
},"useData":true});
templates['resourceAdd'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "	  	<div id=\"resource\" class=\"dialog_update_content\">\n	  		<form id=\"resourceForm\" method=\"post\" >\n		  			<table>\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Description</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"description\" id=\"description\" maxlength=\"50\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <span>Action</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"action\" id=\"action\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>	\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Uri</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"url\" id=\"url\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>	\n			            		            \n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Method</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n								<select name=\"method\" id=\"method\">\n									<option value=\"\">Please Select</option>\n									<option value=\"GET\">GET</option>\n									<option value=\"POST\">POST</option>\n									<option value=\"PUT\">PUT</option>\n									<option value=\"DELETE\">DELETE</option>	\n								</select>		\n			                </td>\n			                <td class=\"status validator_style\"></td>\n			            </tr>	\n\n			            <tr>\n			                <td>\n			                    <span>Authorization</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n								<input type=\"radio\" name=\"auth\"  value=\"1\">Required  &nbsp;\n								<input type=\"radio\" name=\"auth\" value=\"0\" checked=\"true\"/> Not Required\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>	\n		  			</table>				  \n			</form>   \n		 </div> ";
},"useData":true});
templates['resourceMod'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	  	<div id=\"resource\" class=\"dialog_update_content\">\n	  		<form id=\"resourceForm\" method=\"post\" >\n	  				<input type=\"hidden\" name=\"page\" id=\"page\" value=\""
    + alias3(((helper = (helper = helpers.page || (depth0 != null ? depth0.page : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"page","hash":{},"data":data}) : helper)))
    + "\" />\n		  			<table>\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Description</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"description\" id=\"description\" value=\""
    + alias3(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"description","hash":{},"data":data}) : helper)))
    + "\" maxlength=\"50\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <span>Action</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\"  name=\"action\" id=\"action\" value=\""
    + alias3(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>	\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Uri</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"url\" id=\"url\" value=\""
    + alias3(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"url","hash":{},"data":data}) : helper)))
    + "\"/>\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>	\n			            		            \n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Method</span>\n			                </td>\n			                <td></td>\n			            </tr>\n			            <tr>\n			                <td>\n								<select name=\"method\" id=\"method\">\n									<option value=\"\">Please Select</option>\n									<option value=\"GET\">GET</option>\n									<option value=\"POST\">POST</option>\n									<option value=\"PUT\">PUT</option>\n									<option value=\"DELETE\">DELETE</option>	\n								</select>		\n			                </td>\n			                <td class=\"status validator_style\"></td>\n			            </tr>	\n		  			</table>				  \n			</form>   \n		 </div> 		 \n		 ";
},"useData":true});
templates['resourceSearch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n    <div>\n        <div class=\"eso_search_parent\">\n            <div class=\"eso_search_icon\">\n                <a>\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search.gif\">\n                </a>\n            </div>\n        </div>\n\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\n            <a id=\"addRole\" href=\"javascript:void(0)\" class=\"buttons icon add button_add_role\">Add Role</a>\n        </div>\n\n        <div class=\"eso_search_input\">\n\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['resourceSearchResult'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-adgid=\""
    + alias2(alias1((depth0 != null ? depth0.adgid : depth0), depth0))
    + "\">\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"330px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.index_page : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"460px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\n                    <a href=\"javascript:void(0)\" name=\"setPermission\" class=\"buttons icon settings\" style=\"margin-left: 1px;\" title=\"Config\"></a>\n                    <a href=\"javascript:void(0)\" name=\"modRole\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\n                    <a href=\"javascript:void(0)\" name=\"delRole\" class=\"buttons icon remove\" style=\"margin-left: 1px;\" title=\"Delete\"></a>\n                    <a href=\"javascript:void(0)\" name=\"relationRole\" class=\"buttons icon user\" style=\"margin-left: 1px;\" title=\"Relation\"></a>\n                </td>\n            </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "	        <tr>\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n	        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\">\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n                <th width=\"330px\">Role Name</th>\n                <th width=\"330px\">Default Home</th>\n                <th width=\"460px\">Description</th>\n                <th width=\"200px\" style=\"border-right:0px;\">\n                    Operation\n                </th>\n            </tr>\n        </table>\n\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n</div>";
},"useData":true});
templates['tree'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <li><span data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </li>\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                "
    + ((stack1 = (helpers.tree || (depth0 && depth0.tree) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"tree","hash":{},"data":data})) != null ? stack1 : "")
    + "\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "               <ul>\n"
    + ((stack1 = helpers.each.call(depth0,depth0,{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "               </ul>";
},"useData":true});
return templates;
});