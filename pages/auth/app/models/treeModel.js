define(['backbone','../config'], function (Backbone,page_config) {

    
    //菜单模型
    var MenuModel = Backbone.Model.extend({
    	url:page_config.menuUrl,
		idAttribute : "id",    	
    	default : {
    		id : 5,
    		text : "",
    		link : "",
    		controlType : 1,
    		parentId : 0
    	}
    });

    //菜单集合
    var MenuCollection = Backbone.Collection.extend({
        url: page_config.menuUrl,
        model: MenuModel,
    }, {
        pageCtrl: {
            pageNo: 1,
            pageSize: 15
        }
    });

    //资源模型
    var ResourceModel = Backbone.Model.extend({
        url: page_config.resourceUrl
    });

    var ResourceCollection = Backbone.Collection.extend({
        url: page_config.resourceUrl
    });

    //搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            searchConditions: ""
        }
    });

    return {
    	MenuModel:MenuModel
        ,MenuCollection:MenuCollection
        ,ResourceCollection:ResourceCollection
        ,ResourceModel:ResourceModel
        ,SearchModel:SearchModel
    };    
});