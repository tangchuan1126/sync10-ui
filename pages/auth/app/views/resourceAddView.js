/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/treeModel",
    "../common",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,models,Common){
    /**页面:修改账号*/
    return Backbone.View.extend({

        template:templates.resourceAdd,
        setMenuId:function(menuId){
        	this.menuId = menuId;
        },
        setSelectNode : function(node){
        	this.node = node;
        },
        getUrl : function(){
        	return $("#url").val();
        },
        validator:function(){
        	var tmp = this;
        	$.validator.addMethod("validateUrl",function(value,element){
        		return tmp.validateUrl(value,element);
        	});
        	
            $("#resourceForm").validate({
                rules: {
                	description: {
                        required: true,
                        remote: {
                            url: "/Sync10/accountpermission/resource/exist/menu/" + tmp.menuId,
                            dataType: "json",
                            dataFilter: function (data) {
                                return $.parseJSON(data).success;
                            }
                        }

                    },
                    url : {
                        required: true
                    },
                	method: {
                        required: true 
                    }                    
                },
                onkeyup : false,
                messages: {
                	description: {
                        required: "Enter description",
                        remote: "Resource  already exists"
                    },
                    url : {
                        required: "Enter uri"
                    },
                	method: {
                        required: "Please select method"
                    }                  
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        },
        render:function(){

            var tmp = this;
            art.dialog({
                title:'Add resource'
                ,lock: true
                ,opacity:0.3
                ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(tmp.template({
                    }));
                    $("#description").focus();
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";

                },
                button: [{
                    name: 'Submit',
                    callback: function () {
                        var dialog = this;

                        var resourceForm = $("#resourceForm");
                        if (resourceForm.valid()) {
                            //禁用按钮
                            dialog.button({

                                name: 'Submit',
                                focus: true,
                                disabled: true
                            });

                            var model = new models.ResourceModel;
                            
                            model.save({
                            	"page": tmp.menuId,
                                "action": $.trim( resourceForm.find("#action").val() ),
                                "description": $.trim( resourceForm.find("#description").val() ),
                                "url": $.trim( resourceForm.find("#url").val() ),
                                "method": $.trim( resourceForm.find("#method").val() ),
                                "auth":(($.trim( resourceForm.find(":radio:checked").val()) == "1") ? true : false )
                            },{
                                success: function (data) {
                                    if(data.get("success")){
                                        showMessage("Success","succeed");
                                        setTimeout(function(){
                                            dialog.close();
                                            var ref = $('#view-panel-2').jstree(true);
                                            
                                    		var node_data = {
                                    				id : "role_" + data.attributes.id,
                                    				text : model.get("description"),
                                    				level : 3,
                                    				icon : "img/auth.png" ,
                                    				others : {
                                    					auth : 	model.get("auth")
                                    				},
                                    				state : {
                                    					selected : model.get("auth"),
                                    					disabled : tmp.node.state.disabled
                                    				},
                                    				type : ((tmp.node.original.others.controlType==1) ? "web_resource" : "app_resource")
                                    		};
                                    		var select = tmp.node.state.selected;
                                    		if(select && !model.get("auth")){
                                    			tmp.node.state.selected = false;
                                    		}
					        		    	ref.create_node(tmp.node,node_data);
					        		    	
                                    		if( select && !model.get("auth")){
                                    			tmp.node.state.selected = false;
                                    			$("#" + tmp.node.id).children("a:first").removeClass("jstree-clicked");
                                    			if(ref.is_open(tmp.node)){
                                        			ref.get_node($("#" + node_data.id)).state.selected = false;
                                        			$("#" + node_data.id).children("a:first").removeClass("jstree-clicked");                                    				
                                    			}
                                    		}else{
                                    			
                                    		}
                                    		
                                    		//改变所属菜单的统计数字
				        		    		var enabled_increase = (model.get("auth")) ? 1 : 0;
				        		    		Common.changeCount(tmp.node,1,enabled_increase);
                                        },1500);
                                    }else{
                                    	var errors = data.get("errors");
                                		if(errors.description){
                                			$("#description").parent().next().text(errors.description);
                                		}
                                		if(errors.url){
                                			$("#url").parent().next().text(errors.url);
                                		}
                                		if(errors.method){
                                			$("#method").parent().next().text(errors.method);
                                			$("#method").removeClass("valid").addClass("error");
                                		}                                		
                                		if(errors.server){
                                			showMessage("Failure","error");
                                		}
                                    	
                                        //开启按钮
                                        dialog.button({

                                            name: 'Submit',
                                            focus: true,
                                            disabled: false
                                        });
                                    }
                                },
                                error: function(model, response, options){
                                	showMessage("Failure","error");
                                    dialog.button({
                                        name: 'Submit',
                                        focus: true,
                                        disabled: false
                                    });                                	
                                }                                
                            });
                        } else {
                            resourceForm.validate().errorList[0].element.focus();
                        }

                        return false;
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
            
           
            tmp.validator();
        }
    });
});
