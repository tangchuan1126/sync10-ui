/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/treeModel",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,models){
    /**页面:修改账号*/
    return Backbone.View.extend({

        template:templates.resourceMod,
        setSelectNode : function(node){
        	this.node = node;
        },
        getUrl : function(){
        	return $("#url").val();
        },
        validator:function(model){
        	
        	var tmp = this;
            $("#resourceForm").validate({
                rules: {
                	description: {
                        required: true,
                        remote: {
                            url: "/Sync10/accountpermission/resource/exist/menu/" + model.get("page"),
                            dataType: "json",
                            data :{id : model.get("id")},
                            dataFilter: function (data) {
                                return $.parseJSON(data).success;
                            }
                        }

                    },
                    url : {
                        required: true                       
                    },
                	method: {
                        required: true
                    }                    
                },
                onkeyup:false,
                messages: {
                	description: {
                        required: "Enter description",
                        remote: "Resource  already exists"
                    },
                    url : {
                        required: "Enter uri"
                    },
                	method: {
                        required: "Please select method"
                    }                  
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        },
        render:function(model){

            var tmp = this;
            art.dialog({
                title:'Edit resource'
                ,lock: true
                ,opacity:0.3
                ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(tmp.template(model.toJSON()));
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";

                },
                button: [{
                    name: 'Submit',
                    callback: function () {

                        var dialog = this;

                        var resourceForm = $("#resourceForm");

                        if (resourceForm.valid()) {
                            //禁用按钮
                            dialog.button({

                                name: 'Submit',
                                focus: true,
                                disabled: true
                            });

                            var data = {
                                	"page": model.get("page"),
                                    "action": $.trim( resourceForm.find("#action").val() ),
                                    "description": $.trim( resourceForm.find("#description").val() ),
                                    "url": $.trim( resourceForm.find("#url").val() ),
                                    "method": $.trim( resourceForm.find("#method").val() ),
                                    "auth":model.get("auth")                       		
                            };
			        		 $.ajax({
				        		   type: "PUT",
				        		   url: "/Sync10/accountpermission/resource/" + model.get("id"),  
				        		   data:JSON.stringify(data),
				        		   contentType:"application/json",
				        		   success: function(result){
	                                    if(result.success){
	                                        showMessage("Success","succeed");
	                                        
	                                        setTimeout(function(){
	                                            dialog.close();
						      		    	    var ref = $('#view-panel-2').jstree(true);
						      		    	    var id = model.get("id");
						    		    	    ref.rename_node($("#role_" + id),data.description);
						    		    	    tmp.node.state.selected = data.auth;
						    		    	    /*
						    		    	    if(data.auth){
						    		    	    	$("#role_" + id).children("a:first").addClass("jstree-clicked");
						    		    	    }else{
						    		    	    	$("#role_" + id).children("a:first").removeClass("jstree-clicked");
						    		    	    }*/
	                                        },1500);
	                                        
	                                    }else{
	                                    	var errors = result.errors;
	                                		if(errors.description){
	                                			$("#description").parent().next().text(errors.description);
	                                		}
	                                		if(errors.url){
	                                			$("#url").parent().next().text(errors.url);
	                                		}
	                                		if(errors.method){
	                                			$("#method").parent().next().text(errors.method);
	                                			$("#method").removeClass("valid").addClass("error");	                                			
	                                		}                                		
	                                		if(errors.server){
	                                			showMessage("Failure","error");
	                                		}

	                                        //开启按钮
	                                        dialog.button({

	                                            name: 'Submit',
	                                            focus: true,
	                                            disabled: false
	                                        });
	                                    }
				        		   },
	                                error: function(model, response, options){
	                                	showMessage("Failure","error");
	                                    dialog.button({
	                                        name: 'Submit',
	                                        focus: true,
	                                        disabled: false
	                                    });                                	
	                                }				        		   
				        	 });    
                        } else {
                            resourceForm.validate().errorList[0].element.focus();
                        }

                        return false;
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
            
           
            $("#resourceForm").find(":radio[value='" + (model.get("auth") ? "1" : "0") +"']").click();
            $("#resourceForm").find("#method").val(model.get("method"));
            
            tmp.validator(model);
        }
    });
});
