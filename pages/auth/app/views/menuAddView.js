/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/treeModel",
    "../common",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,models,Common){
    /**页面:修改账号*/
    return Backbone.View.extend({

        template:templates.addMenu,
        setParentId:function(parentId){
        	this.parentId = parentId;
        },
        setSelectNode : function(node){
        	this.node = node;
        },
        validateUrl:function(value,element){
        	if(this.parentId > 0){
        		if($.trim(value).length == 0){
        			return false;
        		}
        	}
        	return true;
        },
        validator:function(){
        	var tmp = this;
        	$.validator.addMethod("validateUrl",function(value,element){
        		return tmp.validateUrl(value,element);
        	});
        	
            $("#menuForm").validate({
                rules: {
                    title: {
                        required: true

                    },
                    link : {
                    	validateUrl : true
                    }
                },
                onkeyup:false,
                messages: {
                    title: {
                        required: "Enter Title"
                    },
                    link: {
                    	validateUrl: "Enter Uri",
                    }                    
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        },
        render:function(){

            var tmp = this;
            art.dialog({
                title:'Add Menu'
                ,lock: true
                ,opacity:0.3
                ,init:function(){
                	
                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');
					
                    this.content(tmp.template({
                    }));
                    $("#title").focus();
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";

                },
                button: [{
                    name: 'Submit',
                    callback: function () {

                        var dialog = this;

                        var menuForm = $("#menuForm");

                        if (menuForm.valid()) {

                            //禁用按钮
                            dialog.button({

                                name: 'Submit',
                                focus: true,
                                disabled: true
                            });

                            var model = new models.MenuModel;

                            model.save({
                                "text": $.trim( menuForm.find("#title").val() ),
                                "link": $.trim( menuForm.find("#link").val() ),
                                "parentId": tmp.parentId,
                                "controlType":$.trim( menuForm.find(":radio:checked").val() ),
                                "enabled" : tmp.node.original.others.enabled
                            },{
                                success: function (data) {
                                    if(data.get("success")){
                                        showMessage("Success","succeed");
                                        setTimeout(function(){
                                        	dialog.close();
                                            var ref = $('#view-panel-2').jstree(true);
                                            data.attributes.text = data.attributes.text + "  (<font class=\"enabled\">0</font>/<font class=\"total\">0</font>)";
    				        		    	if(tmp.node.state.disabled){
    				        		    		data.attributes.state = {"disabled":true};
    				        		    	}
                                            
                                            ref.create_node(tmp.node, data.attributes);
    				        		    	if(tmp.parentId > 0){
    				        		    		var enabled_increase = (model.get("enabled")) ? 1 : 0;
    				        		    		Common.changeCount(tmp.node,1,enabled_increase);
    				        		    	}
    				        		    	$("#-1 a").children("i:first").css({"display":"none"});
                                        }, 1500);
                                                                                   
                                    }else{
                                    	var errors = data.get("errors");
                                		if(errors.title){
                                			$("#title").parent().next().text(errors.title);
                                		}
                                		if(errors.link){
                                			$("#link").parent().next().text(errors.link);
                                		}
                                		if(errors.server){
                                			showMessage("Failure","error");
                                		}

                                        //开启按钮
                                        dialog.button({

                                            name: 'Submit',
                                            focus: true,
                                            disabled: false
                                        });
                                    }
                                },
                                error: function(model, response, options){
                                	showMessage("Failure","error");
                                    dialog.button({
                                        name: 'Submit',
                                        focus: true,
                                        disabled: false
                                    });                                	
                                }
                            });
                        } else {
                            menuForm.validate().errorList[0].element.focus();
                        }

                        return false;
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
            
           
            $("#menuForm").find(":radio[value='" + this.node.original.others.controlType +"']").click();
            if(this.node.original.level == 1){
            	 $("#menuForm :radio").attr("disabled","true");
            }
            
            tmp.validator();
        }
    });
});
