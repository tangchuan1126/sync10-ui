/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/treeModel",
    "../common",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage",
    
],function($,Backbone,Handlebars,templates,models,Common){
    /**页面:修改账号*/
    return Backbone.View.extend({

        template:templates.menuMod,
        setModel : function(model){
        	this.model = model;
        },
        setParentId:function(parentId){
        	this.parentId = parentId;
        },
        setSelectNode : function(node){
        	this.node = node;
        },
        validateUrl:function(value,element){
        	if(this.node.original.parentId > 0){
        		if($.trim(value).length == 0){
        			return false;
        		}
        	}
        	return true;
        },
        validator:function(model){
        	var tmp = this;
        	$.validator.addMethod("validateUrl",function(value,element){
        		return tmp.validateUrl(value,element);
        	});
        	
            $("#menuForm").validate({
                rules: {
                    title: {
                        required: true,
                        remote: {
                            url: "/Sync10/accountpermission/menu/exist/" + model.get("parentId"),
                            dataType: "json",
                            data:{id:model.get("id")},
                            dataFilter: function (data) {
                                return $.parseJSON(data).success;
                            }
                        }

                    },
                    link : {
                    	validateUrl : true
                    }
                },
                onkeyup:false,
                messages: {
                    title: {
                        required: "Enter Title",
                        remote: "Menu  already exists"
                    },
                    link: {
                    	validateUrl: "Enter Uri",
                    }                    
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        },
        render:function(model){

            var tmp = this;
            art.dialog({
                title:'Edit Menu'
                ,lock: true
                ,opacity:0.3
                ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');
                    this.content(tmp.template(model.toJSON()));
                    $("#menuForm").find(":radio[value='" + model.get("controlType") + "']").click();
                    
                    if(model.get("parentId") == 0){
                    	$("#menu_url").hide();
                    }else{
                    	$("#menu_type").hide();
                    }
                    
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";

                },
                button: [{
                    name: 'Submit',
                    callback: function () {

                        var dialog = this;

                        var menuForm = $("#menuForm");

                        if (menuForm.valid()) {

                            //禁用按钮
                            dialog.button({

                                name: 'Submit',
                                focus: true,
                                disabled: true
                            });

                            var data = {
                                    "text": $.trim( menuForm.find("#title").val()),
                                    "link": $.trim( menuForm.find("#link").val() ),
                                    "controlType": model.get("controlType"),
                                    "sort" : model.get("sort"),
                                     "parentId" : model.get("parentId"),
                                     "enabled" : model.get("enabled")                            		
                            };
                            
			        		 $.ajax({
				        		   type: "PUT",
				        		   url: "/Sync10/accountpermission/menu/" + model.get("id"),  
				        		   data:JSON.stringify(data),
				        		   contentType:"application/json",
				        		   success: function(result){
	                                    if(result.success){
	                                        showMessage("Success","succeed");
	                                        
	                                        setTimeout(function(){
	                                           
	                                            dialog.close();
						      		    	    var ref = $('#view-panel-2').jstree(true);
						    		    	    ref.rename_node($("#" + model.get("id")),data.text + "  (<font class='enabled'>" + Common.getEnabledCount(tmp.node.id)  + "</font>/<font class='total'>" + Common.getTotal(tmp.node.id) + "</font>)");
						    		    	    $("#-1 a").children("i:first").css({"display":"none"});
						    		    	    
						    		    	    tmp.node.original.others.controlType = parseInt(data.controlType);
						    		    	   // tmp.node.original = data;
						    		    	    if(tmp.node.original.parentId == 0){
							    			    	  var children = ref.get_children_dom($("#" + tmp.node.original.id));
							    			    	  children.each(function(){
							    			    		  ref.get_node($(this)).original.others.controlType = tmp.node.original.others.controlType;
							    			    	  });						    		    	    	
						    		    	    }
						    			    	  
	                                        },1500);
	                                        
	                                    }else{
	                                    	var errors = result.errors;
	                                		if(errors.title){
	                                			$("#title").parent().next().text(errors.title);
	                                		}
	                                		if(errors.link){
	                                			$("#link").parent().next().text(errors.link);
	                                		}
	                                		if(errors.server){
	                                			showMessage("Failure","error");
	                                		}
	                                        //开启按钮
	                                        dialog.button({

	                                            name: 'Submit',
	                                            focus: true,
	                                            disabled: false
	                                        });
	                                    }
				        		   },
	                                error: function(model, response, options){
	                                	showMessage("Failure","error");
	                                    dialog.button({
	                                        name: 'Submit',
	                                        focus: true,
	                                        disabled: false
	                                    });                                	
	                                }				        		   
				        	 });                            
                        } else {
                        	
                            menuForm.validate().errorList[0].element.focus();
                        }

                        return false;
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
            tmp.validator(model);
        }
    });
});
