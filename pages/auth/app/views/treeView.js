define(['jquery','backbone','handlebars','templates','../models/treeModel','../common','jstree','jqueryui/dialog','showMessage', 'blockui','artDialog'], 
function ($,Backbone,Handlebars,templates,models,Common) {
	
    (function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();
	
    Handlebars = Handlebars.default;
    var treeTemplate = templates.tree;

    Handlebars.registerHelper('tree', function(children) {
        return treeTemplate(children);
    });

var select_node;
var parentId = 0;
var tree;
var tree_view;
//记录改变了状态，但子节点状态没有同步变化的菜单ID，将它们用,连接
var change_menu_ids = ",";
var finish_change = false;
var finish_initialize = false;
//表示现在是否点击了取消按钮
var is_cancel = false;

function cleanMenuForm(){
	$("#menuForm :text").val("");
	$("#menuForm :radio:first").click();
	$("#menuForm span").html("");
	$("#menuForm *").show();
};



function changeStatus(node){
    var data = {"id":node.original.id,"enabled":node.original.others.enabled};
    $.blockUI({message:''});
    var flag = false;
	 $.ajax({
		   type: "put",
		   url: "/Sync10/accountpermission/menu/status", 
		   timeout:2000,
		   data:JSON.stringify(data),
		   contentType:"application/json",
		   success: function(result){
		      if(result.success){
		    	  flag = true;
		    	  showMessage("Success","succeed");
		    	  node.original.others.enabled = !node.original.others.enabled;
		    	  node.state.disabled = !node.state.disabled;
		    	  
		    	  if(node.original.others.enabled){
		    		  $("#" + node.original.id).find("a").removeClass("jstree-disabled");
		    	  }else{
		    		  $("#" + node.original.id).find("a").addClass("jstree-disabled");	    		  
		    	  }
		    	  var children = tree.get_children_dom($("#" + node.original.id));
		    	  //if(node.original.level == 1){
			    	  
			    	  if(children.length == 0){
			    		  change_menu_ids += node.original.id + ",";
			    	  }
			    	  children.each(function(){
			    		  var child_node = tree.get_node($(this));
			    		  if(node.original.level == 1){
			    			  tree.get_node($(this)).original.others.enabled = node.original.others.enabled;
			    		  }
			    		  tree.get_node($(this)).state.disabled = node.state.disabled;
			    		  if(tree.is_open($(this))){
			    			  var  resource_nodes = tree.get_children_dom($("#" + child_node.id));
			    			  resource_nodes.each(function(){
			    				  tree.get_node($(this)).original.others.enabled = node.original.others.enabled;
			    				  tree.get_node($(this)).state.disabled = node.state.disabled;
			    			  });
			    			  
			    		  }
			    	  });		    		  
		    	 // }
			    	  
			      if(node.parent > 0){
			    	  //改变父菜单的统计值
			    	  var parentNode = tree.get_node($("#" + node.parent));	
			    	  Common.changeCount(parentNode,0,node.original.others.enabled ? 1 : -1);				    	  
			      }else{
			    	  //改变当前菜单的统计值
			    	  if(node.original.others.enabled){
			    		  Common.enableAll(node);
			    	  }else{
			    		  Common.disableAll(node);
			    	  }
			      } 
				 setTimeout(function(){
					 $.unblockUI();
				 },1500);				      
		      }else{
		    	 showMessage("Failed","error");
		 		 setTimeout(function(){
					 $.unblockUI();
				 },2300);			    	  
		      }
		   },
		   "error":function(XMLHttpRequest, status, error){
			   showMessage("Failed","error");
				 setTimeout(function(){
					 $.unblockUI();
				 },2300);				   
		   }
	 });
	 

};

function updateResourceAuth(node){
	 
	 var id = (node.original.id + "").substring(5);
     var data = {"id":id,"auth":node.original.others.auth};
     $.blockUI({message:''});
     var flag = false;
	 $.ajax({
		   type: "put",
		   url: "/Sync10/accountpermission/resource/status",  
		   data:JSON.stringify(data),
		   timeout:2000,
		   contentType:"application/json",
		   success: function(result){
			  flag = result.success;
		      if(result.success){
		    	  showMessage("Success","succeed");
		    	  node.original.others.auth = !node.original.others.auth;
		    	  //改变父菜单的统计值
		    	  var parentNode = tree.get_node($("#" + node.parent));	
		    	  Common.changeCount(parentNode,0,node.original.others.auth ? 1 : -1);
		 		  setTimeout(function(){
					  $.unblockUI();
				  },1500);		    		    	  
		      }else{
		    	  showMessage("Failure","error");
		    	  stop = false;
		    	  $("#" + node.id + " div:first").click();	
		 		  setTimeout(function(){
					  $.unblockUI();
				  },2300);		    	  
		      }
		   },
		   "error":function(XMLHttpRequest, status, error){
			   showMessage("Failed","error");
				 setTimeout(function(){
					 $.unblockUI();
				 },2300);				   
		   }	 
	 });
};



function updateResourceAuthWithMenu(node,auth){
	var id = node.original.id ;
    var data = {"page":id,"auth":auth};
    $.blockUI({message:''});
    var flag = false;
	 $.ajax({
		   type: "put",
		   url: "/Sync10/accountpermission/resource/status",  
		   data:JSON.stringify(data),
		   timeout:2000,
		   contentType:"application/json",
		   success: function(result){
		      if(result.success){
		    	  flag = true;
		    	  showMessage("Success","succeed");
		    	  stop = false;
		    	  node.state.selected = auth;
		    	  $("#" + node.id + " div:first").click();
		    	  
		    	  if(!auth){
		    		  node.state.selectAll = true;
		    		  node.state.cancleAll = false;
		    		  node.state.selected = true;
		    		  if(node.original.level > 1){
		    			  Common.enableAll(node);
		    		  }
		    		  
		    	  }else{
		    		  node.state.cancleAll = true;
		    		  node.state.selectAll = false;
		    		  node.state.selected = false;
		    		  if(node.original.level > 1){
		    			  Common.disableAll(node);
		    		  }		    		  
		    	  }		
		    	  $("#" + id + " a>i:first").removeClass("jstree-undetermined");
		    	  node.state.undetermined = false;
		    	  var children = tree.get_children_dom($("#" + id)); 
		    	  if(children.length == 0){
		    		  change_menu_ids += node.original.id + ",";
		    	  }else{
		    		  //点击的是二级菜单
			    	  if(node.original.level == 2){
				    	  children.each(function(){
				    		  tree.get_node($(this)).original.others.auth = !auth; 
				    		  tree.get_node($(this)).state.selected = !auth; 
				    	  });
			    	  }else{
				    	  children.each(function(){
				    		  var child_node = tree.get_node($(this));
				    		  child_node.state.selected = !auth;
				    		  if(!auth){
				    			  if(Common.getTotal(child_node.id) > 0){
					    			  Common.enableAll(child_node);
				    			  }

				    		  }else{
				    			  Common.disableAll(child_node);
				    		  }
				    		  $(this).children("a").children("i:first").removeClass("jstree-undetermined");
				    		  tree.get_node($(this)).state.undetermined = false;
				    		  //判断二级菜单是否展开
				    		  if(tree.is_open(child_node)){
				    			  var  resource_nodes = tree.get_children_dom($("#" + child_node.id));
				    			  if(resource_nodes && resource_nodes.length > 0){
				    				  resource_nodes.each(function(){
				    					 tree.get_node($(this)).original.others.auth = !auth;
				    					 tree.get_node($(this)).state.selected = !auth; 
							    		  $(this).children("a").children("i:first").removeClass("jstree-undetermined");
							    		  tree.get_node($(this)).state.undetermined = false;				    					 
				    				  });
				    				  
				    			  }
				    		  }else{
					    		  if(!auth){
					    			  child_node.state.selectAll = true; 
					    			  child_node.state.cancleAll = false;
					    			  child_node.state.selected = true;
					    		  }else{
					    			  child_node.state.cancleAll = true;
					    			  child_node.state.selectAll = false;
					    			  child_node.state.selected = false;
					    		  }				    			  
				    		  }
				    		  
				    	  });	
			    	  }		    		  
		    	  }
		 		  setTimeout(function(){
					 $.unblockUI();
				  },1500);			    	  
		      }else{
		    	  showMessage("Failure","error");
		    	  if(!auth){
			        	node.state.selected = false;
			        	$("#" + node.id).children("a:first").removeClass("jstree-clicked");	
			        	alert($("#" + node.id).find("a.jstree-clicked").size());
			        	if($("#" + node.id).find("a.jstree-clicked").size() > 0 ){
			        		alert($("#" + node.id).children("a:first").children("i:first").size());
			        		$("#" + node.id).children("a:first").children("i:first").addClass("jstree-undetermined");
			        		node.state.undetermined = true;
			        	}
		    	  }else{
			        	node.state.selected = true;
			        	$("#" + node.id).children("a:first").addClass("jstree-clicked");			    		  
		    	  }
		 		  setTimeout(function(){
					 $.unblockUI();
				  },2300);			    	  
		      }
		   },
		   "error":function(XMLHttpRequest, status, error){
			   showMessage("Failed","error");
				 setTimeout(function(){
					 $.unblockUI();
				 },2300);				   
		   }		   
	 });
	
};

//删除节点
function deleteNode(node,isMenu){
	var msg = (isMenu) ? "<h2>Are you sure to delete this menu?</h2>" : "<h2>Are you sure to delete this resource?</h2>";
	var title = (isMenu) ? "Delete Menu" : "Delete Resource"  ;
    art.dialog({
        title:title
        ,lock: true
        ,opacity:0.3
        ,width:400
        ,height:70
        ,init:function(){
        	
            var w1 = $(window).width(), H = $('html');
            H.css('overflow', 'hidden');
            var w2 = $(window).width();
            H.css('margin-right', (w2 - w1) + 'px');
			
            this.content(msg);
        }
        ,close:function(){

            document.body.parentNode.style.overflow="scroll";
            document.body.parentNode.style.marginRight="";

        }
        ,icon:'warning'
        ,button: [{
            name: 'Sure',
            callback: function () {
           	 var url = (isMenu) ? "/Sync10/accountpermission/menu/" + node.id : "/Sync10/accountpermission/resource/" + (node.id + "").substring(5) ;
        	 $.blockUI({message:''});
        	 var flag = false;
        	 $.ajax({
        		   type: "delete",
        		   url: url,  
        		   timeout:2000,
        		   contentType:"application/json",
        		   success: function(result){
        		      if(result.success){
        		    	  flag = true;
        		    	  showMessage("Success","succeed");
        		    	  if(node.parent > 0){
        		    		  	 var parentNode = tree.get_node($("#" + node.parent));	
        		    		  	 var enabled_change_count = 0;
        			    		 if(isMenu){
        			    			 enabled_change_count = (node.original.others.enabled) ? -1 : 0 ;
        			    		 }else{
        			    			 enabled_change_count = (node.original.others.auth) ? -1 : 0 ;
        			    		 }	
        		    			 Common.changeCount(parentNode,-1, enabled_change_count);  
        		    	  }
        		    	  tree.delete_node(node);
        		    	  $("#-1 a").children("i:first").css({"display":"none"});
    	        		  setTimeout(function(){
    	        			 $.unblockUI();
    	        		  },1500);			    	  
        		      }else{
        		    	  showMessage("Failed","error");
    	        		  setTimeout(function(){
    	        			 $.unblockUI();
    	        		  },2300);        		    	  
        		      }
        		   },
		  		   "error":function(XMLHttpRequest, status, error){
					   showMessage("Failed","error");
						 setTimeout(function(){
							 $.unblockUI();
						 },2300);				   
				   }	        	 
        	 });	
            },
            focus: true
        }]
        ,cancel:true
        ,cancelVal:'Cancle'
    });	 	
	

}

var cancle_flag = false;
//标识复选框的选择是否需要传播
var stop = true;
function changeMenuOrder(e,data){
	if(data.node.original.level < 3 ){
		 if(!cancle_flag){
	    	 
			 if(data.parent != data.old_parent || data.position != data.old_position){
				 var params = "parentId=" + ((data.parent == '-1')?0:data.parent) + "&oldParentId=" + ((data.old_parent == '-1')?0:data.old_parent) + "&position=" + data.position + "&oldPosition=" + data.old_position;
				 $.blockUI({message:''});
				 var flag = false;
				 $.ajax({
					   type: "put",
					   timeout:2000,
					   url: "/Sync10/accountpermission/menu/order/" + data.node.id + "?" + params,  
					   success: function(result){
						   
					      if(result.success){
					    	  flag = true;
					    	  showMessage("Success","succeed");
					    	  if(data.parent != data.old_parent){
						    	  
						    	  var enabled_increase = 0;
						    	  var new_parent_node = tree.get_node($("#" + data.parent));
						    	  if(new_parent_node.state.disabled){
						    		  if(!data.node.state.disabled){
						    			  data.node.state.disabled = true;
						    			  data.node.original.others.enabled = false;
						    			  $("#" + data.node.id).children("a:first").addClass("jstree-disabled");
						    		  }
						    	  }else if(!data.node.state.disabled){
						    		  enabled_increase = 1;
						    	  }
						    	  Common.changeCount( tree.get_node($("#" + data.old_parent)),-1,(data.node.state.disabled) ? 0 : -1 );
						    	  Common.changeCount(new_parent_node,1,enabled_increase);				    		  
					    	  }
							  setTimeout(function(){
								 $.unblockUI();
							  },1500);		    	  
					      }else{
					    	  showMessage("Failed","error");
					    	  cancle_flag = true;
					    	  tree.open_node($("#" + data.parent));
					    	  if(data.parent != data.old_parent || data.position > data.old_position){
					    		  tree.move_node($("#" + data.node.id),$("#" + data.old_parent),data.old_position);
					    	  }else{
					    		  tree.move_node($("#" + data.node.id),$("#" + data.old_parent),data.old_position + 1);
					    	  }					    		  

							  setTimeout(function(){
								 $.unblockUI();
							  },2300);						    	  
					      }
					   },
			  		   "error":function(XMLHttpRequest, status, error){
						   showMessage("Failed","error");
							 setTimeout(function(){
								 $.unblockUI();
							 },2300);				   
					   }	   					   
				 });	
				 $("#-1 a").children("i:first").css({"display":"none"});
			 }		 
		 }else{
			 cancle_flag = false;
		 }		
	}
}

function changeSelectStyle(node){
	  $("div.right_select").removeClass("right_select").css("background-color","");
	  $("a.right_select").removeClass("right_select").css("background-color","");
	  $("#" + node.id + " div:first").css("background-color","#d3eb9a").addClass("right_select");
	  $("#" + node.id + " a:first").css("background-color","#d3eb9a").addClass("right_select");	
};

    
	var treeData;
	$.ajax({
				 type : "get",
				 url  : "/Sync10/accountpermission/menu",
				 async : false,
				 success : function(data){
					 treeData = data;
				 }
			}
	);
	
    var treeView = Backbone.View.extend({
        initialize:function(options){
            this.setElement(options.el);
            tree_view = this;
        },
        setView:function(options){
        	this.menuAddView = options.menuAddView;
        	this.menuModView = options.menuModView;
        	this.resourceAddView = options.resourceAddView;
        	this.resourceModView = options.resourceModView;
        },
        model: new models.MenuModel(treeData),
        events:{
            "select_node.jstree":"selectTreeNode",
            "deselect_node.jstree":"deSelectTreeNode",            
			"show_contextmenu.jstree":"showMenu"
        },
        render:function(){
        	
            this.$el.jstree({"core":{"multiple" : true ,"data":[this.model.toJSON()],"check_callback":true,"animation" : 0,"themes" : { "stripes" : true }},
                  "checkbox" : {
                    "keep_selected_style" : false
                  },
                  "dnd" : {
                      "drag_selection" : function(){
                    	  return false;
                      },
                      "check_while_dragging": function(){
                    	  return false;
                      },
                      "is_draggable":function(array){
                    	  if(array[0].original.level == 3){
                    		  return false;
                    	  }else{
                    		  return true;
                    	  }
                      }
                    },
                    
            	  "types" : {
            		    "#" : {
            		      "max_children" : 1, 
            		      "max_depth" : 4, 
            		      "valid_children" : ["root"]
            		    },
            		    "root" : {
              		      "valid_children" : ["first_web_menu","first_app_menu"]
              		    },            		    
            		    "first_web_menu" : {
            		      "valid_children" : ["second_web_menu"]
            		    },
            		    "second_web_menu" : {
            		      "valid_children" : ["web_resource"]
            		    },
            		    "first_app_menu" : {
              		      "valid_children" : ["second_app_menu"]
              		    },
              		    "second_app_menu" : {
              		      "valid_children" : ["app_resource"]
              		    },            		    
            		    "web_resource" : {
                		      "valid_children" : []
                		 },
             		    "app_resource" : {
              		      "valid_children" : []
             		    }                 		 
            		  },          	
            	 
            	"plugins": ["contextmenu","checkbox", "dnd", "search",  "types", "wholerow"],
            	"contextmenu":{"select_node":false,"items":this.rightMenu}}).bind("move_node.jstree", function (e, data){
            		changeMenuOrder(e,data);
            	}).on('ready.jstree', function (e, data){
            		$("#-1 a").children("i:first").css({"display":"none"});
                    finish_initialize = true;
                    
            	}).on("after_open.jstree",function(e,data){
  		    	    $("#-1 a").children("i:first").css({"display":"none"});
            	}).on("open_node.jstree",function(e,data){
            		
            		
            		var id = data.node.original.id;
            		var children = tree.get_children_dom($("#" + id)); 
            		var close = false;
            		if(!finish_initialize){
                		if(data.node.original.level > 0){
                			tree.close_node(data.node);
                			close = true;
                		}            			
            		}else{
			    		var total = Common.getTotal(id);
			    		var enabledCount = Common.getEnabledCount(id);
                		if(change_menu_ids.indexOf("," +id + ",") >= 0){
    				    	  children.each(function(){
    				    		  tree.get_node($(this)).original.others.enabled = data.node.original.others.enabled;
    				    		  tree.get_node($(this)).state.disabled = data.node.state.disabled;
    					    	  if(!data.node.state.disabled){
    					    		  $(this).children("a:first").removeClass("jstree-disabled");
    					    	  }else{
    					    		  $(this).children("a:first").addClass("jstree-disabled");	    		  
    					    	  }	
    					    	  
    					    	  if(data.node.original.level == 2){
        					    	  if(total == enabledCount){
        					    		  tree.get_node($(this)).original.others.auth = true;
        					    		  tree.get_node($(this)).state.selected = true;
        					    		  $(this).children("a:first").addClass("jstree-clicked");
        					    	  }else if(enabledCount == 0){
        					    		  tree.get_node($(this)).original.others.auth = false;
        					    		  tree.get_node($(this)).state.selected = false;
        					    		  $(this).children("a:first").removeClass("jstree-clicked");
        					    	  }    					    		  
    					    	  }else if(data.node.original.level == 1){
    					    		  if(data.node.state.selectAll){
    					    			  tree.get_node($(this)).state.selected = true;
    					    			  tree.get_node($(this)).state.selectAll = true;
    					    			  tree.get_node($(this)).state.cancleAll = false;
    					    			  $(this).children("a:first").addClass("jstree-clicked");
    					    			  Common.enableAll(tree.get_node($(this)));
    					    			  
    					    		  }else if(data.node.state.cancleAll){
    					    			  tree.get_node($(this)).state.selected = false;
    					    			  tree.get_node($(this)).state.selectAll = false;
    					    			  tree.get_node($(this)).state.cancleAll = true;    					    			  
    					    			  $(this).children("a:first").removeClass("jstree-clicked");
    					    			  Common.disableAll(tree.get_node($(this)));
    					    			  
    					    		  }    					    		  
    					    	  }
    					    	     					    	  
    				    	    });	            		
                			  change_menu_ids = change_menu_ids.replace(id + ",","");

                		 }else{
                			 
            				children.each(function(){
            					var node = tree.get_node($(this));
            					if(data.node.original.level == 2){
	      				    		  tree.get_node($(this)).original.others.enabled = data.node.original.others.enabled;
	    				    		  tree.get_node($(this)).state.disabled = data.node.state.disabled;
	    					    	  if(!data.node.state.disabled){
	    					    		  $(this).children("a:first").removeClass("jstree-disabled");
	    					    	  }else{
	    					    		  $(this).children("a:first").addClass("jstree-disabled");	    		  
	    					    	  }	
            					}else if(data.node.original.level == 1){
            						if(data.node.state.selected){
          			    	    		$(this).children("a:first").addClass("jstree-clicked");
          			    	    		node.state.selected = true;            							
            						}
            					}
            				});   
                		} 
                		
            		}

      			    data.node.state.selectAll = false;
    			    data.node.state.cancleAll = false;

            	});
            tree =  $('#view-panel-2').jstree(true);
            this.getNodeId = function(node){
                return node.id;
            };
        },
        selectTreeNode:function(e,data){
        	if(finish_initialize){
        		if(data.node.original.level == 3){
        			if(stop){
        				updateResourceAuth(data.node); 
        			}
        			stop = false;
        		}else if(data.node.original.level > 0){
        			if(data.node.children.length > 0){
        				if(stop){
            				var title =  templates.confirm_auth();
            			    art.dialog({
            			        title:'Add Access Restriction'
            			        ,lock: true
            			        ,opacity:0.3
            			        ,init:function(){
            			        	
            			            var w1 = $(window).width(), H = $('html');
            			            H.css('overflow', 'hidden');
            			            var w2 = $(window).width();
            			            H.css('margin-right', (w2 - w1) + 'px');
            						
            			            this.content(title);
            			        }
            			        ,close:function(){

            			            document.body.parentNode.style.overflow="scroll";
            			            document.body.parentNode.style.marginRight="";

            			        },
            			        button: [{
            			            name: 'Sure',
            			            callback: function () {
            			            	 updateResourceAuthWithMenu(data.node,false); 
            			            },
            			            focus: true
            			        }]
            			        ,cancel:function(){
            			        	stop = false;
            			        	is_cancel = true;
            			        	$("#" + data.node.id).children("div:first").click();
            			        	
            			        }
            			        ,cancelVal:'Cancel'
            			    });	         					
        				}
        			}else{
        				stop = false;
        			}
        		}

        		changeSelectStyle(data.node);
        	}
    		if(data.node.original.level == 0 || stop || is_cancel){
    			is_cancel = false;
            	e.stopImmediatePropagation();
    		}
    		stop = true;
        },
        deSelectTreeNode : function(e,data){
        	if(finish_initialize){
        		if(data.node.original.level == 3){
        			if(stop){
        				updateResourceAuth(data.node); 
        			}
        			stop = false;
        		}else if(data.node.original.level > 0 ){
        			if(data.node.children.length > 0){
        				if(stop){
            				var title =  templates.cancle_auth();
            			    art.dialog({
            			        title:'Cancle Access Restriction'
            			        ,lock: true
            			        ,opacity:0.3
            			        ,init:function(){
            			        	
            			            var w1 = $(window).width(), H = $('html');
            			            H.css('overflow', 'hidden');
            			            var w2 = $(window).width();
            			            H.css('margin-right', (w2 - w1) + 'px');
            						
            			            this.content(title);
            			        }
            			        ,close:function(){

            			            document.body.parentNode.style.overflow="scroll";
            			            document.body.parentNode.style.marginRight="";

            			        },
            			        button: [{
            			            name: 'Sure',
            			            callback: function () {
            			            	 updateResourceAuthWithMenu(data.node,true); 
            			            },
            			            focus: true
            			        }]
            			        ,cancel:function(){
            			        	stop = false;
            			        	is_cancel = true;
            			        	$("#" + data.node.id).children("div:first").click();            			        	
            			        	        			        	
            			        }
            			        ,cancelVal:'Cancel'            			        
            			    });	         					
        				}      				
        			}else{
        				stop = false;
        			}
        		}
        		changeSelectStyle(data.node);
        	}
    		if(data.node.original.level == 0 || stop || is_cancel){
    			is_cancel = false;
            	e.stopImmediatePropagation();
    		}
    		stop = true;
        },
		showMenu:function(node,position){
			var width =  $("#" + select_node.original.id).find("a:first").width();
			$("ul.jstree-default-contextmenu").css({"left": (position.x + width) + "px"});
		},
		rightMenu:function(node){
			  select_node = node;
			  changeSelectStyle(node);
			  parentId = (node.id == -1) ? 0 : node.id;
			  var items = {};
			  

			  if(node.original.level == 3){
				  items.updateItem = { 
						   label: "Update",
						   action: function () {

							   tree_view.resourceModView.setSelectNode(select_node);
								 var json_data;
								 $.ajax({
											 type : "get",
											 url  : "/Sync10/accountpermission/resource/" + node.id.substring(5),
											 cache:false,
											 async : false,
											 success : function(data){
												 json_data = data;
											 }
								 		}
								 );							   
							   var model = new models.ResourceModel(json_data);
							   tree_view.resourceModView.render(model);						   
						   }
				  };
				  
			  }else{
				  
				  
				  if(node.original.level == 1 || node.original.level == 0){
				      items.addItem = { 
						   label: "Add",
						   action: function () {
							   tree_view.menuAddView.setParentId(parentId);
							   tree_view.menuAddView.setSelectNode(select_node);
							   tree_view.menuAddView.render();
						   }
				      };
				  }else if(node.original.level == 2){
				      items.addItem = { 
						   label: "Add Resource",
						   action: function () {
							   tree_view.resourceAddView.setMenuId(parentId);
							   tree_view.resourceAddView.setSelectNode(select_node);
							   tree_view.resourceAddView.render();						   
						   }
				      };
				  }	
				  if(node.original.level > 0){
					  items.updateItem = { 
							   label: "Update",
							   action: function () {
								   tree_view.menuModView.setParentId(parentId);
								   tree_view.menuModView.setSelectNode(select_node);
								   
									 var json_data;
									 $.ajax({
												 type : "get",
												 url  : "/Sync10/accountpermission/menu/" + node.id,
												 async : false,
												 cache:false,
												 success : function(data){
													 json_data = data;
												 }
									 		}
									 );
								   
								   var model = new models.MenuModel(json_data);
								   tree_view.menuModView.render(model);						   
							   }
					  };
					  if(node.original.others.enabled ){
						  items.unuseItem = { 
								   label: "Forbid",
								   action: function(){
									   changeStatus(node);
								   }
						  };						  
					  }else{
						  if(tree.get_node($("#" + node.parent)).original.others.enabled){
							  items.useItem = { 
									   label: "Allow",
									   action: function(){
										  changeStatus(node);
									   }
							  };							  
						  }
					  }						  
				  }

			  }
			  
			  if(node.original.level > 0 && node.children.length == 0){
				  items.deleteItem = { 
						   label: "Delete",
						   action: function(){
							   deleteNode(node, (node.original.level < 3) );
						   }
				  };					  
			  }			  
			  
			 return items;			
		}
    });

    return treeView;
});



