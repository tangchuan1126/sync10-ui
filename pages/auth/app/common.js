define(["jquery"],function($){
		var changeCount = function(node,total_change_count,enabled_change_count){
	    	if(node.id > 0){
		    	var total = parseInt($.trim($("#" + node.id).children("a:first").children("font.total").text()));
		    	total = total + total_change_count;
		    	var dom_a = $("#" + node.id).children("a:first");
		    	var enabled_count = parseInt($.trim(dom_a.children("font.enabled").text()));
		    	enabled_count += enabled_change_count;
		    	
		    	dom_a.children("font.total").text((total) + "");
		    	dom_a.children("font.enabled").text((enabled_count) + "");
		    	node.text = node.text.replace(/\(.+\)/g,"") + "  (<font class=\"enabled\">" + enabled_count + "</font>/<font class=\"total\">" + total + "</font>)";
	    	}			
		};
		var getTotal = function(nodeId){
			return parseInt($.trim($("#" + nodeId).children("a:first").children("font.total").text()));
		};
		var getEnabledCount = function(nodeId){
			return parseInt($.trim($("#" + nodeId).children("a:first").children("font.enabled").text()));
		};
		var disableAll = function(node){
			changeCount(node,0,0 - getEnabledCount(node.id));
		};	
		var enableAll = function(node){
			changeCount(node,0, getTotal(node.id) - getEnabledCount(node.id));
		};				
		return {
			"changeCount": changeCount,
			"getTotal" : getTotal,
			"getEnabledCount" : getEnabledCount,
			"disableAll" : disableAll,
			"enableAll" : enableAll
		};
	}
);