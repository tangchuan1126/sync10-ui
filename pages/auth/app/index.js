/**
 * Created by lujintao on 2015.3.25.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "./views/treeView",
    "./views/menuAddView",
    "./views/menuModView", 
    "./views/resourceAddView",  
    "./views/resourceModView",       
    "blockui",
    "jqueryui/tabs"
],function($ ,Backbone,Handlebars,TreeView,MenuAddView,MenuModView,ResourceAddView,ResourceModView){

    $(function(){
    	$("#view-panel-2").height($(window).height() - 50);       	
    });
    window.onresize = function(){
    	$("#view-panel-2").height($(window).height() - 50);   
    };	
	$("#main").show();
    var treeView = new TreeView({el:"#view-panel-2"});


    var menuAddView = new MenuAddView({el:"#menuForm"});
    var menuModView = new MenuModView();
    var resourceAddView = new ResourceAddView({el:"#resourceForm"});
    var resourceModView = new ResourceModView();    
    treeView.setView({
    	menuAddView: menuAddView,
    	menuModView: menuModView,
    	resourceAddView: resourceAddView,
    	resourceModView: resourceModView    
    });

    treeView.render();
    $.unblockUI();

 
});