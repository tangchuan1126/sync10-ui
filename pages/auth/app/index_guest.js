/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "./views/treeView_guest",
    "blockui",
    "jqueryui/tabs"
],function($ ,Backbone,Handlebars,TreeView,MenuAddView,MenuModView,ResourceAddView,ResourceModView){

    $(function(){
    	$("#view-panel-2").height($(window).height() - 50);       	
    });
    window.onresize = function(){
    	$("#view-panel-2").height($(window).height() - 50);   
    };   
	$("#main").show();    
    var treeView = new TreeView({el:"#view-panel-2"});
    treeView.render();
    $.unblockUI();
});