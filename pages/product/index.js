/**
 * Created by liyi on 2015.4.23
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "./view/productCodeResultView",
    "./view/productCodeDelView",
    "blockui",
    "jqueryui/tabs"
],function(
    $
    ,Backbone
    ,Handlebars
    ,PcodeView
    ,PcodeDelView
){

    (function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();
    
    
    function getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
    var pc_id = getQueryString("pc_id");
    var pcodeView = new PcodeView();

    var pcodeDelView = new PcodeDelView();
    pcodeView.setView({
        productCodeDelView:pcodeDelView
    });
    pcodeView.render(pc_id);
    
    $(window.parent.document).find("div.blockUI").remove();
    $("#loading").hide();
});