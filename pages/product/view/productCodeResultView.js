/**
 * Created by liyi on 2015.4.23
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars_ext",
    "templates",
    "../model/productCodeModel",
    "art_Dialog/dialog-plus",
    "select2",
    "showMessage"
],function(config,$,Backbone,Handlebars,templates,models){
    var editing = false;
    return Backbone.View.extend({
        template:templates.productcode_result,
        el:"#search_result",
        collection: new models.ProductCodeCollection(),
        initialize:function(){
        },
        events:{
           "click #addCode":"addCode",
           "click a[name='delCode']":"delCode",
           "click a[name='modCode']":"editCode",
           "click a.cancel" : "cancel"
        },
        validator:function(evt,v){
            var ignore = $("input[type='radio'][id='retaile_type']:checked").length==0?true:false;
            
            var addCode = $("#addCodeForm");
            var pcode_id = 0;
             //pcode_id = $(evt.target).parent().parent('tr')[0].dataset.pcode_id;
            if(editing){
            	pcode_id = $("tr.over").data("pcode_id");
            }
            var pc_id = $(evt.target).data("pc_id");
            var codeType = addCode.find("input[type='radio']:checked").val();
            var p_code = addCode.find("#p_code")[0];
            var oldp_code = addCode.find("#p_code").attr("oldp_code");

            var retailer_id = addCode.find("#retailer_id")[0];
            var oldRetailer_id = addCode.find("#retailer_id").attr("oldretailer_id");
            if(p_code.value=="") {
            	showMessage("Please enter code","alert");
             // v.shoeErrow("Please enter code",p_code);
              return;
            } else {
              v.shoeErrow("",p_code);
            }
            if(!ignore) {
                if(retailer_id.value=="") {
                	showMessage("Please select retailer","alert");
                    //v.shoeErrow("Please select retailer",retailer_id);
                    
                    return;
                } else {
                    v.shoeErrow("",retailer_id);
                }
            }
            if(oldp_code!=undefined&&oldp_code!="") {
                if((oldp_code+""+oldRetailer_id)==(p_code.value+""+retailer_id.value)){
                    v.render(pc_id);
                    return;
                }
            }
            
            var regexp = new RegExp("^[A-Za-z0-9$+/-]+$");
            if(p_code.value!="" && !regexp.test($.trim(p_code.value))){
            	showMessage("Code only support numbers, letters and $ - + /","alert");
            	return;
            }
            
            $.blockUI({message:''});
            var url = config.productCodeUrl.checkUrl;
            $.post(url,{p_code:p_code.value,pcode_id:pcode_id,code_type:codeType,retailer_id:retailer_id.value,pc_id:pc_id},function(data){
            	if(data.success) {
                  var proModel = new models.ProductCodeModel();
                  proModel.set({p_code:addCode.find("#p_code").val(),code_type:codeType,retailer_id:addCode.find("#retailer_id").val(),pc_id:pc_id});
                  proModel.url=config.productCodeUrl.addUrl;
                  if(editing) {
                    proModel.url=config.productCodeUrl.modUrl;
                    proModel.set("pcode_id",pcode_id);
                  }  
                  proModel.save({},{
                      success: function (data) {
                        if(data.get("success")) {
                          if($("tr.no-data").size() > 0){
                        	  $("tr.no-data").remove();
                          }	
                        	
                          showMessage("Success","succeed");
                          $("#upc_type,#retaile_type").removeAttr("disabled");
                          v.shoeErrow("",retailer_id);
                          v.shoeErrow("",p_code );
                          v.render(pc_id);
                          
                          $("tr.over").removeClass("over");
                        }
                    }
                  });
                } else {
                    if(data.p_code){
                    	showMessage("Code already exists","alert");
                       // v.shoeErrow("Code already exists",p_code);
                    } else {
                        v.shoeErrow("",p_code);
                    }
                    if(!ignore){
                        if(data.retailer_id) {
                        	showMessage("Retailer or code already exists","alert");
                           // v.shoeErrow("Retailer or code already exists",retailer_id);
                        } else {
                            v.shoeErrow("",retailer_id);
                        }
                    }
                }
                setTimeout(function(){
                	$.unblockUI();
                },1500);
            }
        );
        },
        shoeErrow:function(error,element) {  
          var nextTr = $(element.parentNode.parentNode).next("tr")[0];
          var index = $(element.parentNode).index();
          $(nextTr.children[index]).text(error);
        },
        setView:function(views){
            this.productCodeDelView = views.productCodeDelView;
        },
        addCode:function(evt){
            var v = this;
            v.validator(evt,v);
        },
        delCode:function(evt){
            var pcode_id = $(evt.target).parent().parent('tr').data("pcode_id");
            var pModel = this.collection.findWhere({pcode_id:pcode_id});
            this.productCodeDelView.setView({productCodeResultView:this});
            this.productCodeDelView.render(pModel,this.pc_id);
        },
        editCode:function(evt){
        	if(editing){
        		showMessage("Editing","alert");
        		return ;
        	}
        	
            var pcode_id = $(evt.target).parent().parent('tr').data("pcode_id");
            var pModel = this.collection.findWhere({pcode_id:pcode_id});
            var type = pModel.get("code_type");
            if(type == 2){
            	$("#upc_type").click();
            }else{
            	$("#retaile_type").click();
                $("#retailer_id").val(pModel.get("retailer_id"));
                $("#retailer_id").attr("oldretailer_id",pModel.get("retailer_id"));

                $("#retailer_id").select2("val",pModel.get("retailer_id"));
                $("#retaileTd").parent('tr').data("pcode_id",pcode_id);
                //$("#retaileTd").css("opacity",1);            	
            }
            $("#upc_type,#retaile_type").attr("disabled","true");
            $("#p_code").val(pModel.get("p_code"));
            $("#p_code").attr("oldp_code",pModel.get("p_code"));           

            //$("#addCode").text("EditCode");
            $("#addCode").removeClass("add").addClass("approve").attr("title","Update");
            $('.validator_style').text('');
            $("a.cancel").show();
            editing = true;
        },
        cancel : function(event){
        	$("#upc_type,#retaile_type").removeAttr("disabled");
        	$("#p_code").val("");
        	$("#upc_type").click();
        	$("a.cancel").hide();
        	$("#addCode").removeClass("approve").addClass("add"); 
        	editing = false;
        	$("tr.over").removeClass("over");
        },
        render:function(pc_id){
        	
            var v = this;
            v.pc_id = pc_id;
            editing = false;
            //$.blockUI({message:''});
           
            if(pc_id!=null){
                v.collection.url = config.productCodeUrl.getAllUrl;
                v.collection.reset();
                v.collection.fetch({
                    async: false,
                    data:{
                        pc_id:pc_id                        
                    }
                });
                v.retailerCollection = new models.ShipToCollection();
                v.$el.html(v.template({
                        result:v.collection.toJSON(),
                        retailers : v.retailerCollection.toJSON(),
                        pc_id:pc_id  
                }));
                
                
                $("#retailer_id").attr("disabled","true");
                $("#retailer_id").select2({
                placeholder: "Select...",
                allowClear: true
            }); 
            }
            $("td.type[data-type='1']").parent().remove();
            $("table.search_result_list_content  tr").hover(
            	function(){
	            	if(!editing){
	            		$(this).addClass("over");
	            	}
            	},function(){
	            	if(!editing){
	            		$(this).removeClass("over");
	            	}            	
            	}
            );
            if($("table.search_result_list_content  tr").size() == 0){
            	$("table.search_result_list_content").append("<tr class=\"no-data over\"><td style=\"text-align:center;height:314px;\" >No Data.</td></tr>");
            }
        }
    });
});
