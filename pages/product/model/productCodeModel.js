/**
 * Created by liyi on 2015.04.23.
 */

"use strict";
define([
    "../config",
    "jquery",
    "backbone"
],function(config,$,Backbone){

     //产品code
    var productCodeModel = Backbone.Model.extend({
        idAttribute: "pcode_id"
    });

    var shipToModel = Backbone.Model.extend({
        idAttribute: "ship_to_id"
    });

    //产品code集合
    var productCodeCollection = Backbone.Collection.extend({  
        model: productCodeModel,
        parse: function (response) {
            return response;
        }
    });

    var shipToCollection = Backbone.Collection.extend({  
        model: shipToModel,
        parse: function (response) {
            return response;
        }
    });


    var shipToCollection = Backbone.Collection.extend({
           model: shipToModel,
           url: config.productCodeUrl.getAllRetailer,
            parse:function(response){
                
                return response;
            },
           initialize: function(){
            
              this.fetch({dataType: "json",async: false});
               
            }
       });

    return {
        ProductCodeModel:productCodeModel
        ,ProductCodeCollection:productCodeCollection
        ,ShipToModel:shipToModel
        ,ShipToCollection:shipToCollection
        
    };

});