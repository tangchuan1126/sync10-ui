define([
    './config',
    'jquery',
    'backbone',
    'handlebars',
    'templates',
    /*"./model/task_request_model",
    "./model/edit_task_model",
    "./view/storage_list_box",
    "./view/title_list_box",
     "art_Dialog/dialog",*/
     "./view/new_task_view",
    "bootstrap",

],
function(config,$,Backbone,Handlebars,templates,newTaskView){
    
    $(function(){
        /*var locations = new Array();
        locations.push(1006261);
        locations.push(1006262);
        locations.push(1006263);*/
        
        /*var task = new editTaskModel.TaskModel();
        task.fetch({dataType: "json",async: false});
        task.set({locations:locations});*/

    	/*var storageListBox = new storageListBoxView({});
    	storageListBox.render();*/
        var ntv = new newTaskView().render();
        
    }); 
    
});
