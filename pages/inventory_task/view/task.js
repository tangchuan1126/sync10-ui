"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "bootstrap",
  "../js/common"

], function( $, Backbone, Handlebars, templates) {
    
    return Backbone.View.extend({
      el:"",
      template:templates.task,


      render:function(data){
        var dis = this;
       
        var html=dis.template({area: data.model});
        $(data.el).html(html);
        
        

        $("#task_box_"+data.model.area_id+" .loc  input[type='checkbox']").change(function(evt){
            var checkbox = $(evt.target);
            var box = checkbox.parent().parent().parent();
            if(this.checked){
              dis.select_box(box);
            }else{
              var checkboxes = box.find(":checkbox");
              dis.unselect_box(box);
              $.each(checkboxes, function(i,item){
                  if(item.checked){
                    dis.select_box(box);
                    return;
                  }
              });
            }
            checkEnableCreateButton();
        });


        $("#task_box_"+data.model.area_id+" .select_all  input[type='checkbox']").change(function(evt){
            var checkbox = $(evt.target);
            var box = checkbox.parent().parent();
            
            if(this.checked){
              dis.select_box(box);
            }else{
              dis.unselect_box(box);
            }
            box.find(":checkbox").prop('checked',this.checked);
            checkEnableCreateButton();
        });

      },
      
      select_box:function(box){
        box.removeClass("task_box_unselected");
        box.addClass("task_box_selected");
      },
      unselect_box:function(box){
        box.addClass("task_box_unselected");
        box.removeClass("task_box_selected");
      },

      /*check_condition_for_assign_user_btn:function(){
        $("#assign_user_btn").addClass("disabled");
        var flag = false;
        $(".task_box").each(function(i,item){
          if($(item).hasClass("task_box_selected")){
            $("#assign_user_btn").removeClass("disabled");
            flag = true;
            return;
          }
        });
        if(flag==false){
          $(".employee_name").each(function(i,item){
           
            $(item).removeClass("selected_employee");
          });
        }
      },

      check_condition_for_add_deadline_btn:function(){
        console.log($("#assign_user_btn").hasClass("disabled"))
        if($("#assign_user_btn").hasClass("disabled")){
          $("#add_deadline_btn").addClass("disabled");
        }
      },*/
    });

}); 