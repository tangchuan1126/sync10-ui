"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/user_model.js",
  "./deadline_view",
  "../js/common",
  "immybox",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, userModel, deadlineView, common) {
    
    return Backbone.View.extend({
      el:"#users_body",
      template:templates.user_view,
      collection:null,
      userId:null,

      initialize:function(data){
        var dis = this;
        dis.collection.fetch({dataType: "json",async: false});
        dis.userId = data.userId;
        
      },
      selectDefaultValue:function() {
        var dis = this;
        if(dis.userId){
          $("#"+dis.userId).parent().addClass("selected_employee");
          dis.checkConditionToEnableLinks();
        }
      },
      checkConditionToEnableLinks:function(){
        if($(".selected_employee").length>0){
            $("#next_date_selection_div").removeClass("disabled");
           }else{
            $("#next_date_selection_div").addClass("disabled");
           }
      },
      render:function(){
        var dis = this;
        var html=dis.template({users: dis.collection.toJSON()});

        dis.$el.html(html);
        //dis.$el.hide();   


        $(".employee_box").click(function(evt){
           
           $(".employee_box").each(function(i,item){
                  $(item).removeClass("selected_employee");
           });
           $(this).addClass("selected_employee");
           dis.checkConditionToEnableLinks();
           
        });

        $("#back_area_selection").click(function(){common.show_areas();});
        $("#next_date_selection").click(function(){common.show_date();});

        dis.selectDefaultValue();
      },
      
    });

}); 