"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/area_model",
  "../model/task_request_model",
  "./task",
  "oso.lib/InventoryControls/Inventory",
    "../js/common",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models, models, Task, InventoryControl,common) {
    
    return Backbone.View.extend({
      el:"#toc_area",
      //template:templates.area_list_box,
      collection:null,
      task:null,

      initialize:function(data){
        var dis = this;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
      },
      selectDefaultValue:function(){
        var dis=this;
        
       if(dis.task.locations){
          $.each(dis.task.locations, function(index,item){
            $("#inven_"+item+" input").trigger("click");
          })
        }
        
      },
      render:function(){
        var dis = this;
       // var html=dis.template({list: dis.collection.toJSON()});
        dis.$el.html("");
        $("#toc_area").show();
        $("#no_area").hide();
        // $("#users_body").hide();
         //$("#deadline_body").hide();
        

        // populate InventoryControl component for resultant areas
        var invdata = {
          renderTo: "#toc_area",//容器
          dataUrl: dis.collection.sort().toJSON(),//数据或[{},{}...]
          //postData:{a:"123",b:"345"},//往服务端传数据
          searchfieldkey:"AREA_NAME",//被匹配字段名
          scrollH:250,//高度，出滚动条
          itmepanelw:200,//宽度
          filterbuttonstxt: "clear",
          inventorytitle:"Select All",
          placeholder:"Filter Areas / Locations", 
          title_in_input:true
         // configurl:{"buttons":[]}
        };

        var Inventoryliste = new InventoryControl(invdata);
        
        Inventoryliste.on("events.titleallclick",function(jsondata){
          common.checkEnableCreateButton();
        });
        Inventoryliste.on("events.filter",function(jsondata){
          console.log("clear:",$(".Inventory_filter input").val(""));
          $(".Inventory_filter input").trigger("input");
        });
        Inventoryliste.on("events.itmeclick",function(jsondata){
          common.checkEnableCreateButton();
        });
        Inventoryliste.on("events.all-areas-checkbox",function(jsondata){
          common.checkEnableCreateButton();
        });
        Inventoryliste.on("events.on-render-complete",function(){
           dis.selectDefaultValue();
        });
        Inventoryliste.on("Initialize",function(){
        
          $("#toc_area .ptopfoot5px .titleinputlable").before("<button type='button' id='show_selected_areas_only' class='buttons buttons-default'>Show Only Selected Areas</button>");
          $("#toc_area .ptopfoot5px .titleinputlable").before("<button type='button' id='show_all_areas_only' class='buttons buttons-default'>Show All Areas</button>");
          $("#toc_area .ptopfoot5px").append("<div id='next_assign_user_div' class='disabled'> <a href='#' class='next' id='next_assign_user'>Assign User</a></div>");
          $("#toc_area .ptopfoot5px").append("<div class='clear'></div>");
          $("#next_assign_user").click(function(){common.show_users("left");});
          $("#show_selected_areas_only").click(function(){common.show_selected_areas();});
          $("#show_all_areas_only").hide();
          $("#show_all_areas_only").click(function(){common.show_all_areas();});
         // dis.selectDefaultValue();
        });
        Inventoryliste.render();
        
       

      },
      showOnlySelectedAreas:function(){
          $(".task_box_unselected").each(function(i,item){
             $(item).parent().hide(); 
          });
      },

      find_in_attributes:function(searchString,model){
          var attributesList = ["name"];
          for (var i = attributesList.length - 1; i >= 0; i--) {
            if(model.get(attributesList[i])){
              if(model.get(attributesList[i]).toLowerCase().indexOf(searchString)>-1){
                return true;
              }
            }
          }
          return false;
      },

      filterAreas: function(searchString){
       
        var dis = this;
        $("#no_area").show();
        var result = dis.collection.select(function (model) {
           if(dis.find_in_attributes(searchString,model)){
              $("#area_"+model.area_id).show();
              $("#no_area").hide();
           }else{
              $("#area_"+model.area_id).hide();
           }
        });
        
      },
      show_all:function(){
        var dis = this;
        $.each(dis.collection.toJSON(), function(i, item) {
           $("#area_"+item.area_id).show();
        
        }); 
      },

      
    
      
    });

}); 