"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/title_model.js",
  "../model/area_model.js",
    "../model/user_model.js",
  "./area_list_box.js",
    "oso.lib/InventoryControls/Inventory",
    "../js/common",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,title_models,area_models,userModel,areaListBoxView, InventoryControl, common ) {
    
    return Backbone.View.extend({
      el:"#toc_title",
      template:templates.title_list_box,
      collection: new title_models.TitleImmyCollection(),
      task:null,
      
      selectDefaultValue:function(immyBoxIns){
        var dis= this;
        if(dis.task.TITLE_ID){
          immyBoxIns.selectChoiceByValue(dis.task.TITLE_ID);
          $("#selected_title_id").val(dis.task.TITLE_ID);
        }else{
          immyBoxIns.selectChoiceByValue("0");
          $("#selected_title_id").val("0");
        }
        
        dis.pull_areas();
      },
      render:function(data){
        
        var dis = this;
        dis.collection.url = config.getAllTitlesJSON+ "?ps_id="+data.warehouse_id;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");
        dis.$el.html(dis.template());

        var place_holder = new title_models.TitleImmyModel();
        place_holder.text = "All";
        place_holder.value = "0";
        dis.collection.add(place_holder,{at:0})
        var ib = $('#title_list_box').immybox({
          choices: dis.collection,
          maxResults: 10000,
        });
        

        $("#title_list_box").focus({box:ib},function(evt){
          var box = evt.data.box;
          
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_title_id").val()=="0"){

           box[0].selectChoiceByValue(null);
           
          }
          if(box[0].selectedChoice==null && $("#selected_title_id").val()!="0"){
            
            $("#selected_title_id").val("0");
            box[0].selectChoiceByValue(null);
            dis.pull_areas();
          }
          else if(box[0].selectedChoice && $("#selected_title_id").val()!=box[0].selectedChoice.value){

            $("#selected_title_id").val(box[0].selectedChoice.value);
            dis.pull_areas();
          }
          
          
        });

        $("#title_list_box").blur({box:ib},function(evt){
          var box = evt.data.box;
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_title_id").val()=="0"){

           box[0].selectChoiceByValue("0");
           
          }
          if((box[0].selectedChoice==null && $("#selected_title_id").val()!="0")
            || ( $("#selected_title_id").val()!="0" && $("#title_list_box").val()=="")
            ){
            
            $("#selected_title_id").val("0");
            box[0].selectChoiceByValue("0");
            dis.pull_areas();
          
          }
          else if(box[0].selectedChoice && $("#selected_title_id").val()!=box[0].selectedChoice.value){

            $("#selected_title_id").val(box[0].selectedChoice.value);
            dis.pull_areas();
          }
          
          
        });
        dis.selectDefaultValue(ib[0]);
      },

      pull_areas:function(){
        var dis = this;
        var title = $("#selected_title_id").val();
        var warehouse = $("#selected_storage_id").val();

          common.hideError();
        
          $("#toc_title").show();
          $("#toc_area").empty();
          var col = new area_models.AreaImmyCollection(warehouse,title);
          var areas = new areaListBoxView({collection:col,task:dis.task});
          if(areas.collection.length > 0){
                common.show_other_fields();
                areas.render();

            }else{
              common.hide_other_fields();
              common.showError("Selected Title has no areas");
              $("#toc_title").show();
            }

      }
      
    });

}); 