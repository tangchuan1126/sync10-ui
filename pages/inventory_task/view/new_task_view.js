"use strict";
define([
    'config',
    'jquery',
    'backbone',
    'handlebars',
    'templates',
    "../model/task_request_model",
    "../model/edit_task_model",
    "./storage_list_box",
    "./title_list_box",
     "art_Dialog/dialog",
    "bootstrap",

], function(config,$,Backbone,Handlebars,templates,models, editTaskModel, storageListBoxView, titleView, buttonView, dialog){
    
    return Backbone.View.extend({
      el:"#task_order_content",
      template:templates.new_task_view,
      

      render:function(){
        var dis = this;
        /*var locations = new Array();
        locations.push(1006261);
        locations.push(1006262);
        locations.push(1006263);*/
        
        /*var task = new editTaskModel.TaskModel();
        task.fetch({dataType: "json",async: false});
        task.set({locations:locations});*/
        var html=dis.template({heading:"Create Cycle Count Task"});
        dis.$el.html(html);
        var storageListBox = new storageListBoxView({});
        storageListBox.render();
      }
     
      
    });

}); 