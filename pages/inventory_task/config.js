define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getAreaForStorageJSON: "/Sync10/action/administrator/storage_location/GetLocationAreasJSON.action?ps_id=",
    getAreaLocationUserJSON: "/Sync10/app/search/detailedAreaLocations?ps_id=",
    getUsersOfWarehouseJSON: "/Sync10/app/search/usersOfWarehouse?ps_id=",
    getAreaDifferencesJSON: "/Sync10/app/search/filterStorageApproveAreas",
    getAllTitlesJSON: "/Sync10/app/search/titles",
    getLocationDifferencesJSON: "/Sync10/app/search/getStorageApproveLocations?saa_id=",
    //getContainerDifferencesJSON: "/Sync10/action/administrator/location/GetContainerDifferencesJSONAction.action",
    getContainerDifferencesJSON: "/Sync10/app/search/getStorageApproveContainers",
    getCountDifferenceOfContainerJSON: "/Sync10/app/search/getStorageApproveDifferents?sac_id=",
    saveApproveDifference: "/Sync10/app/update/approveDifferents",
    createTaskDifference: "/Sync10/app/add/taskDefinition",

    getTaskInfo: "/Sync10-ui/pages/inventory_task/task.json"
   
});
