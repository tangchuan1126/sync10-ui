"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var AreaImmyModel = Backbone.Model.extend({
          idAttribute: "area_id",

          /*parse: function(data){
            this.name = data.TITLE +data.AREA_NAME;
            this.area_id = data.AREA_ID;
            this.locations = data.LOCATIONS;
            return this;
            
          }*/
        
       });
       
      var AreaImmyCollection =  Backbone.Collection.extend({
           model: AreaImmyModel,
           url: config.getAreaLocationUserJSON,

           initialize: function(ps_id,title_id){
              this.url = this.url+ps_id;
              
              if(title_id){
                this.url = this.url+"&title_id="+title_id;
              }
          },
          comparator: function(item) {
                return item.get('AREA_NAME');
            },

        
       });

      return {
	      AreaImmyModel:AreaImmyModel,
	      AreaImmyCollection:AreaImmyCollection,

	    };




}); //page_init