"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      var ListModel = Backbone.Model.extend({
          idAttribute: "location_id",
          initialize:function(data){
            this.locationId = data.locationId;
            this.locationName = data.locationName;
          }
          

       });
      var ListCollection =  Backbone.Collection.extend({
           model: ListModel,

      });
      var AreaListModel = Backbone.Model.extend({
          idAttribute: "areaId",
          initialize:function(data){
            this.areaId = data.areaId;
            this.areaName = data.areaName;
            this.locations = data.locations;
          }
          

       });
      var AreaListCollection =  Backbone.Collection.extend({
           model: AreaListModel,

      });
      var TaskModel = Backbone.Model.extend({
        url:config.createTaskDifference,
        idAttribute: "id",
        defaults:{
           user:"",
           warehouse:"",
          },
        initialize:function(data){
          this.user = data.user;
          this.warehouse = data.warehouse;
          this.title = data.title;
          this.areas = data.areas;
         
          this.isRepeat = data.isRepeat;
      
          this.endsOn = data.endsOn;
          this.startsOn = data.startsOn;
          
          this.type = data.type;
          //this.scheduled = data.scheduled;
          this.duration = data.duration;
          this.repeatBy = data.repeatBy;
          this.repeatEvery = data.repeatEvery;
          
          this.dayOfWeek = data.dayOfWeek;
          this.occurance = data.occurance;
          return this;
        }

       });
       
       
      
      

      return {
        TaskModel:TaskModel,
        ListModel:ListModel,
        ListCollection:ListCollection,
        AreaListModel:AreaListModel,
        AreaListCollection:AreaListCollection,
      };




}); //page_init