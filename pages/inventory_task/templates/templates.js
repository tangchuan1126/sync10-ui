(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_list_box'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "\r\n		<div class=\"panel panel-default\">\r\n			<div class=\"panel-heading header\">\r\n				<div>"
    + escapeExpression(lambda((depth0 != null ? depth0.areaName : depth0), depth0))
    + "</div>\r\n			</div>\r\n			<div class=\"body panel-body\">\r\n				<div class=\"locations_block\">	\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.locations : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "				</div>\r\n\r\n			</div>\r\n		</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "					\r\n						<div>"
    + escapeExpression(lambda((depth0 != null ? depth0.locationName : depth0), depth0))
    + "</div>\r\n					\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"areas_in_selection\">\r\n\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});
templates['create_task'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Title:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>User:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"user","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Duration:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.duration || (depth0 != null ? depth0.duration : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"duration","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Repeats:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.repeatOptions || (depth0 != null ? depth0.repeatOptions : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"repeatOptions","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Repeat every:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.repeatEvery || (depth0 != null ? depth0.repeatEvery : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"repeatEvery","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.dayOfWeek : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Starts on:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.startsOn || (depth0 != null ? depth0.startsOn : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"startsOn","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Ends:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.endsOn || (depth0 != null ? depth0.endsOn : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"endsOn","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
},"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Repeat on:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.dayOfWeek || (depth0 != null ? depth0.dayOfWeek : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"dayOfWeek","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
},"8":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Start Date:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.start_date || (depth0 != null ? depth0.start_date : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"start_date","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>End Date:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.end_date || (depth0 != null ? depth0.end_date : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"end_date","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "\r\n<div class=\"panel panel-default\">\r\n	<div class=\"panel-heading header\">\r\n		<a class=\"previous\" id=\"back_date_selection\">Back</a>\r\n		<div class=\"clear\"></div>\r\n	</div>\r\n\r\n	<div class=\"body panel-body\">\r\n		<div class=\"div-table\" >\r\n			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell\">\r\n					<div class=\"div-table\">\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Warehouse:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.warehouse || (depth0 != null ? depth0.warehouse : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"warehouse","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.title : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.user : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>is Repeating:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.isRepeat || (depth0 != null ? depth0.isRepeat : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"isRepeat","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell right\"><label>Type:</label></div>\r\n							<div class=\"div-table-cell\">"
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + "</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class=\"div-table-cell\">\r\n					<div class=\"div-table\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.scheduled : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(8, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "					</div>\r\n				</div>\r\n				<div class=\"div-table-cell\" >\r\n					&nbsp;\r\n				</div>\r\n				<div class=\"div-table-cell\" >\r\n					<div class=\"div-table\" >\r\n						<div class=\"div-table-row\">\r\n							<div class=\"div-table-cell\" >&nbsp;</div>\r\n							<div class=\"div-table-cell\" id=\"create_btn_div\">\r\n								<a href=\"#\" id=\"create_task_btn\" class=\"buttons big\">Create Task</a>\r\n							</div>\r\n						</div>\r\n					</div>			\r\n				</div>\r\n			</div>\r\n		</div>\r\n		\r\n		<div id=\"selected_areas_create\"></div>\r\n		\r\n	</div>\r\n</div>\r\n";
},"useData":true});
templates['deadline'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"panel panel-default\">\r\n		<div class=\"panel-heading header\">\r\n			<a class=\"previous\" id=\"back_user_selection\">User Selection </a>\r\n			\r\n			<div class=\"disabled\" id=\"next_date_selection_div\"><a class=\"next\" id=\"create_task_link\">Task Preview</a></div>\r\n			<div class=\"clear\"></div>\r\n		</div>\r\n		\r\n		<div class=\"body panel-body\" >\r\n			<div class=\"div-table\">\r\n				<div class=\"div-table-row\" id=\"is_repeating_checkbox_div\">\r\n					<div class=\"div-table-cell right\"><label for=\"is_repeating\">Is Repeating?</label></div>\r\n					<div class=\"div-table-cell\">\r\n						<input type=\"checkbox\" name=\"is_repeating\" id=\"is_repeating\" style=\"width:100px\" data-label-on=\"   Yes   \" data-label-off=\"   No   \" class=\"iToggle\"/>\r\n					</div>\r\n				</div>\r\n				<div class=\"div-table-row\" >\r\n					<div class=\"div-table-cell right\"><label >Task Type</label></div>\r\n					<div class=\"div-table-cell\" id=\"is_blind_checkbox_div\">\r\n					<input type=\"checkbox\" name=\"is_blind\" id=\"is_blind\" data-label-on=\"Blind\" data-label-off=\"Verify\" class=\"iToggle\"/>\r\n						\r\n					</div>\r\n				</div>\r\n				\r\n			</div>\r\n			<div class=\"div-table\" id=\"non-repeating-div\">\r\n				<div class=\"div-table-row\" id=\"start_date_box\">\r\n					<div class=\"div-table-cell right\"><label for=\"start_date\">Start Date</label></div>\r\n					<div class=\"div-table-cell\"><input type=\"text\" id=\"start_date\" value=\"\" readonly=\"readonly\"/></div>\r\n				</div>\r\n				<div class=\"div-table-row\" id=\"end_date_box\">\r\n					<div class=\"div-table-cell right\"><label for=\"end_date\">End Date</label></div>\r\n					<div class=\"div-table-cell\"><input type=\"text\" id=\"end_date\" value=\"\" readonly=\"readonly\"/></div>\r\n				</div>\r\n			</div>\r\n			<div id=\"repeating_div\">\r\n				<div class=\"div-table\">\r\n					<div class=\"div-table-row\" id=\"task_duration_div\">\r\n						<div class=\"div-table-cell right\"><label for=\"task_duration\">Duration</label></div>\r\n						<div class=\"div-table-cell\">\r\n							<select id=\"task_duration\" name=\"task_duration\">\r\n								<option value=\"1\">1</option>\r\n								<option value=\"2\">2</option>\r\n								<option value=\"3\">3</option>\r\n								<option value=\"4\">4</option>\r\n								<option value=\"5\">5</option>\r\n								<option value=\"6\">6</option>\r\n								<option value=\"7\">7</option>\r\n								\r\n							</select>\r\n							Days\r\n						</div>\r\n					</div>\r\n					<div class=\"div-table-row\">\r\n						<div class=\"div-table-cell right\"><label for=\"repeat_options\">Repeats</label></div>\r\n						<div class=\"div-table-cell\">\r\n							<select id=\"repeat_options\" name=\"repeat_options\">\r\n								<option value=\"Daily\">Daily</option>\r\n								<option value=\"Weekly\">Weekly</option>\r\n								<option value=\"Monthly\">Monthly</option>\r\n							</select>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div id=\"Weekly_table\" class=\"div-table\">\r\n					<div class=\"div-table-row\">\r\n						<div class=\"div-table-cell right\"><label for=\"repeat_options_weekly\">Repeat every</label></div>\r\n						<div class=\"div-table-cell\">\r\n							<select id=\"repeat_options_weekly\" name=\"repeat_options_weekly\">\r\n								<option value=\"1\">1</option>\r\n								<option value=\"2\">2</option>\r\n								<option value=\"3\">3</option>\r\n								<option value=\"4\">4</option>\r\n								<option value=\"5\">5</option>\r\n								<option value=\"6\">6</option>\r\n								<option value=\"7\">7</option>\r\n								<option value=\"8\">8</option>\r\n								<option value=\"9\">9</option>\r\n								<option value=\"10\">10</option>\r\n								<option value=\"11\">11</option>\r\n								<option value=\"12\">12</option>\r\n								<option value=\"13\">13</option>\r\n								<option value=\"14\">14</option>\r\n								<option value=\"15\">15</option>\r\n								<option value=\"16\">16</option>\r\n								<option value=\"17\">17</option>\r\n								<option value=\"18\">18</option>\r\n								<option value=\"19\">19</option>\r\n								<option value=\"20\">20</option>\r\n								<option value=\"21\">21</option>\r\n								<option value=\"22\">22</option>\r\n								<option value=\"23\">23</option>\r\n								<option value=\"24\">24</option>\r\n								<option value=\"25\">25</option>\r\n								<option value=\"26\">26</option>\r\n								<option value=\"27\">27</option>\r\n								<option value=\"28\">28</option>\r\n								<option value=\"29\">29</option>\r\n								<option value=\"30\">30</option>\r\n								\r\n							</select>\r\n						weeks\r\n						</div>\r\n					</div>\r\n					\r\n				</div>\r\n				<div id=\"Daily_table\" class=\"div-table\">\r\n					<div class=\"div-table-row\">\r\n						<div class=\"div-table-cell right\"><label for=\"repeat_options_daily\">Repeat every</label></div>\r\n						<div class=\"div-table-cell\">\r\n							<select id=\"repeat_options_daily\" name=\"repeat_options_daily\">\r\n								<option value=\"1\">1</option>\r\n								<option value=\"2\">2</option>\r\n								<option value=\"3\">3</option>\r\n								<option value=\"4\">4</option>\r\n								<option value=\"5\">5</option>\r\n								<option value=\"6\">6</option>\r\n								<option value=\"7\">7</option>\r\n								<option value=\"8\">8</option>\r\n								<option value=\"9\">9</option>\r\n								<option value=\"10\">10</option>\r\n								<option value=\"11\">11</option>\r\n								<option value=\"12\">12</option>\r\n								<option value=\"13\">13</option>\r\n								<option value=\"14\">14</option>\r\n								<option value=\"15\">15</option>\r\n								<option value=\"16\">16</option>\r\n								<option value=\"17\">17</option>\r\n								<option value=\"18\">18</option>\r\n								<option value=\"19\">19</option>\r\n								<option value=\"20\">20</option>\r\n								<option value=\"21\">21</option>\r\n								<option value=\"22\">22</option>\r\n								<option value=\"23\">23</option>\r\n								<option value=\"24\">24</option>\r\n								<option value=\"25\">25</option>\r\n								<option value=\"26\">26</option>\r\n								<option value=\"27\">27</option>\r\n								<option value=\"28\">28</option>\r\n								<option value=\"29\">29</option>\r\n								<option value=\"30\">30</option>\r\n								\r\n							</select>\r\n						days</div>\r\n					</div>\r\n				</div>\r\n				<div id=\"Monthly_table\" class=\"div-table\">\r\n					<div class=\"div-table-row\">\r\n						<div class=\"div-table-cell right\"><label for=\"repeat_options_monthly\">Repeat every</label></div>\r\n						<div class=\"div-table-cell\">\r\n							<select id=\"repeat_options_monthly\" name=\"repeat_options_monthly\">\r\n								<option value=\"1\">1</option>\r\n								<option value=\"2\">2</option>\r\n								<option value=\"3\">3</option>\r\n								<option value=\"4\">4</option>\r\n								<option value=\"5\">5</option>\r\n								<option value=\"6\">6</option>\r\n								<option value=\"7\">7</option>\r\n								<option value=\"8\">8</option>\r\n								<option value=\"9\">9</option>\r\n								<option value=\"10\">10</option>\r\n								<option value=\"11\">11</option>\r\n								<option value=\"12\">12</option>\r\n								<option value=\"13\">13</option>\r\n								<option value=\"14\">14</option>\r\n								<option value=\"15\">15</option>\r\n								<option value=\"16\">16</option>\r\n								<option value=\"17\">17</option>\r\n								<option value=\"18\">18</option>\r\n								<option value=\"19\">19</option>\r\n								<option value=\"20\">20</option>\r\n								<option value=\"21\">21</option>\r\n								<option value=\"22\">22</option>\r\n								<option value=\"23\">23</option>\r\n								<option value=\"24\">24</option>\r\n								<option value=\"25\">25</option>\r\n								<option value=\"26\">26</option>\r\n								<option value=\"27\">27</option>\r\n								<option value=\"28\">28</option>\r\n								<option value=\"29\">29</option>\r\n								<option value=\"30\">30</option>\r\n								\r\n							</select>\r\n						months</div>\r\n					</div>\r\n				</div>\r\n				<div id=\"date_table\" class=\"div-table\">\r\n					<div class=\"div-table-row\">\r\n						<div class=\"div-table-cell right\"><label for=\"starts_on\">Starts on</label></div>\r\n						<div class=\"div-table-cell\"><input type=\"text\" id=\"starts_on\" value=\"\" /></div>\r\n					</div>\r\n					<div class=\"div-table-row\">\r\n						<div class=\"div-table-cell right\"><label>Ends</label></div>\r\n						<div class=\"div-table-cell\">\r\n							<div class=\"div-table\" id=\"ends_table\">\r\n								<div class=\"div-table-row\">\r\n									<div class=\"div-table-cell\">\r\n										<input type=\"radio\" id=\"ends_on_never\" name=\"ends_on\" value=\"never\" checked=\"checked\">\r\n										<label for=\"ends_on_never\">Never</label>\r\n									</div>\r\n								</div>\r\n								<div class=\"div-table-row\" id=\"occ\">\r\n									<div class=\"div-table-cell\">\r\n										<input type=\"radio\" id=\"ends_on_after\" name=\"ends_on\" value=\"after\">\r\n										<label for=\"ends_on_after\">After <input type=\"text\" id=\"occurances\"/> occurrences</label>\r\n									</div>\r\n								</div>\r\n								<div class=\"div-table-row\">\r\n									<div class=\"div-table-cell\">\r\n										<input type=\"radio\" name=\"ends_on\" id=\"ends_on_on\" value=\"on\">\r\n										<label for=\"ends_on_on\">On <input type=\"text\" id=\"ends_on_date\" value=\"\" /></label>\r\n									</div>\r\n								</div>\r\n							</div>\r\n							\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			\r\n		</div>	\r\n</div>	";
  },"useData":true});
templates['new_task_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div id=\"inventory_task_header\" class=\"panel-heading\">"
    + escapeExpression(((helper = (helper = helpers.heading || (depth0 != null ? depth0.heading : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"heading","hash":{},"data":data}) : helper)))
    + "</div>\r\n<div id=\"filter_box\" class=\"panel-body\">\r\n  <div id=\"toc_warehouse\"></div>\r\n  <div id=\"toc_title\"></div>\r\n  \r\n  <div class=\"clear\"></div>\r\n  <div id=\"error_block\"></div>\r\n  <div id=\"all_container\">\r\n    <div id=\"all_container2\">\r\n      <div id=\"toc_area\"></div>\r\n      <div id=\"toc_selected_area\"></div>\r\n      <div id=\"users_body\"></div>\r\n      <div id=\"deadline_body\"></div>\r\n      <div id=\"create_task_body\"></div>\r\n      <div class=\"clear\"></div>\r\n    </div>\r\n  </div>\r\n</div>";
},"useData":true});
templates['storage_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<label for=\"storage_list_box\">Select Warehouse</label>\r\n<input id='storage_list_box' type='text' class='form-control' />\r\n<input id=\"selected_storage_id\" type=\"hidden\" value=\"0\"/>\r\n";
  },"useData":true});
templates['task'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			<div class=\"loc\">\r\n				<input type=\"checkbox\" name=\"location\" id=\"location_"
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\"/><label for=\"location_"
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "</label>\r\n			</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"task_box_unselected task_box\" id=\"task_box_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\" >\r\n\r\n	<div class=\"task_header\" id=\"header_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.name : stack1), depth0))
    + "</div>\r\n	<div class=\"select_all\">\r\n		<input type=\"checkbox\" name=\"select_all_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\" id=\"area_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "_select_all\"/><label for=\"area_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "_select_all\">Whole Area</label>\r\n		</div>\r\n	<div class=\"task_body\">\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.locations : stack1), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</div>\r\n	\r\n</div>\r\n	";
},"useData":true});
templates['title_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<label for=\"title_list_box\">Select Title</label>\r\n<input id='title_list_box' type='text' class='form-control' />\r\n<input id=\"selected_title_id\" type=\"hidden\" value=\"0\"/>\r\n";
  },"useData":true});
templates['user_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"panel panel-default\">\r\n		<div class=\"panel-heading header\">\r\n			<a class=\"previous\" id=\"back_area_selection\">Area Selection</a>\r\n			<div class=\"disabled\" id=\"next_date_selection_div\"><a class=\"next\" id=\"next_date_selection\">Task Schedule</a></div>\r\n			<div class=\"clear\"></div>\r\n		</div>\r\n		\r\n		<div class=\"body panel-body\">	\r\n			<div class=\"group_box_div\">\r\n				<div class=\"employee_box group_box\">\r\n					<div class=\"employee_name\" id=\"0\">Anyone in the Group</div>\r\n				</div>\r\n				<div class=\"clear\"></div>\r\n			</div>\r\n			<div class=\"employee_box_div\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.users : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\r\n			<div class=\"clear\"></div>\r\n		</div>\r\n		\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "				<div class=\"employee_box\">\r\n					<div class=\"employee_name\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ADID : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.EMPLOYE_NAME : depth0), depth0))
    + "</div>\r\n				</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
})();