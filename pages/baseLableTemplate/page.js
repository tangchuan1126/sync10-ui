"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "./model/labelModel",
  "./view/labelView", 
  "./view/tab2View",
  "jqueryui/tabs",
  "jqueryui/dialog"
  
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, LabelModel, LabelView, Tab2View) {
  $(function() { 
    $("#tabs").tabs();
    var setActiveTab = function(i) {
      $("#tabs").tabs("option", "active", i);
    };
    setActiveTab(0);

    var tab2View = new Tab2View();
    tab2View.render();

    var lableView = new LabelView();
    lableView.render("", "");


    //打印控件 页面影响
    var $fixed = $('#main').next();
    if($fixed && $fixed.children().length){
      $('body').css('margin-top', '80px');
    }
  });
}); //page_init