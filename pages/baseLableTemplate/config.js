(function(){
    var configObj = {
       	lableCollection: {
            url:"/Sync10/action/administrator/lableTemplate/lableTemplate.action"
        },
        lableModel:{
            url:"/Sync10/action/administrator/lableTemplate/lableTemplate.action"
        },
        containerCollection: {
            url:"/Sync10/action/administrator/lableTemplate/lableTemplate.action"
        },
        containerModel:{
            url:"/Sync10/action/administrator/lableTemplate/lableTemplate.action"
        }
    };
    if (typeof define === 'function' && define.amd) {
        define(configObj);
    }
    else {
        //传统模式，非AMD标准
        this.page_config = configObj;
    }
}).call(this);