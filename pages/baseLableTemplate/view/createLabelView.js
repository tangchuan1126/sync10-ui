"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/labelModel",
  "assembleUtils",
  "showMessage",
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, LabelModel,assembleUtils) {
  // CreateLableView 拖动元素时所需要的变量
    var inteval = null;
    var element = null;
    var height = 243;
    var width = 115;
    var posX, posY;
    //标签类型
    var paperType =[{type:"60mmX30mm",width:"230",height:"105"},
                    {type:"80mmX40mm",width:"305",height:"145"},
                    {type:"100mmX50mm",width:"380",height:"185"},
                    {type:"102mmX152mm",width:"385",height:"555"}]
    var initialLabelModel = new LabelModel.LabelModel();

   
    var templateName = $("#input_lableName").val();
    var templateType = $("#input_templateType").val();//获取查询条件
	//新建模板视图
    var CreateLableView = Backbone.View.extend({
      template: templates.editLable,
      el: "#editLable",
      parentView: null,     
      model:new LabelModel.LabelModel(),
      initialize: function () {
        document.onkeydown = this.keyMove;
      }, 
      render: function(model,typekey, collection) {
    	this.collection = collection;
        if(typeof(model) != 'undefined' && model != ""){
          this.model = model;          
        }else{
          this.model = initialLabelModel;
        }      
        this.$el.html(this.template({
          model: this.model.toJSON(),
          typekey:typekey,
          paperType:paperType
        }));
        height = parseInt(this.$el.find("#edit").css("height"));
        width = parseInt(this.$el.find("#edit").css("width"));
        // console.log(this.model.get("lablecontent"));
      },
      events: {
        "click #addLine": "addLine",
        "click #addVertical": "addVertical",
        "click .common-element-button": "addCommonElement",
//        "click #addElName": "addElName",
//        "click #addElText": "addElText",
        "mousedown #yuangong": "yuangong",
        "mousedown #line": "move",
        "mousedown #verline": "move",
        "mousedown .elementName": "move",
        
        "mouseover #line": "changeMouse",
        "mouseover #verline": "changeMouse",
        
        "mouseout #line": "mouseoutColor",
        "mouseout #verline": "mouseoutColor",

        "mouseup #edit": "onmouseup",
        "mouseup #lableContent": "onmouseup",

        "mousedown #turnLeft": "turnLeft",
        "mouseup #turnLeft": "turnMouseUp",
        "mouseout #turnLeft": "turnMouseUp",

        "mousedown #turnRight": "turnRight",
        "mouseup #turnRight": "turnMouseUp",
        "mouseout #turnRight": "turnMouseUp",

        "mousedown #turnTop": "turnTop",
        "mouseup #turnTop": "turnMouseUp",
        "mouseout #turnTop": "turnMouseUp",

        "mousedown #turnBottom": "turnBottom",
        "mouseup #turnBottom": "turnMouseUp",
        "mouseout #turnBottom": "turnMouseUp",

        "click #turn": "turn",
        "click #remove": "removeElement",

        "click #addCommon": "addCommon",
        "click #addBarCode": "addBarCode",
        "click #goBack": "goBack",

        "change #lableType": "setLableType",
        
        "change #lableWidth": "changeWidth",
        "change #lableHeight": "changeHeight",
        "change #elWidth": "changeElWidth",
        "change #elHeight": "changeElHeight",
        "change #elFont": "changeElFont",
        "change #elAlign": "changeElAlign",
        "click #preview": "previewPrinter",
        "click #create": "saveLabel",
        "change #lableName": "changeName"
      },
      changeName:function(evt){
    	  var v = this;
    	  var flag = true;
    	  var lableName = v.$el.find("#lableName").val();
    	  var prevLableName = v.$el.find("#prevLableName").val();
          if(lableName!=prevLableName){
        	  var lablemodel = v.collection.findWhere({'lable_name':lableName});
        	  if(lablemodel){
        		  showMessage("Lable Name【"+lableName+"】 already exists","alert");
        		  v.$el.find("#lableName").focus();
        		  flag = false;//若lable name已存在，不保存
        	  }
          }
          return flag;
      },
      setLableType:function(evt){
    	var width = this.$el.find("#lableType").find("option:selected").data("width");
    	var height = this.$el.find("#lableType").find("option:selected").data("height");
    	this.$el.find("#lableWidth").val(width);
        this.$el.find("#lableHeight").val(height);
        this.changeWidth();
        this.changeHeight();
      },
      saveLabel: function(){
    	  var _self = this;
          $(element).css({"border-color": "#000","color": "#000","border": "0px",});//将选中元素颜色设置为黑色
         
          var flag = _self.changeName();//name 唯一验证
          if(flag){
        	//保存数据
              var lableName = this.$el.find("#lableName").val();
              var lableType = this.$el.find("#lableType").val();
              var lableWidth = this.$el.find("#lableWidth").val();
              var lableHeight = this.$el.find("#lableHeight").val();
              var printName = this.$el.find("#printName").val();
              var lableContent = this.$el.find("#lableContent").html().trim();
              var type = this.$el.find("#type").val()*1;
              var lableTemplateType = 1
              var v = this;
              this.model.save({
                lable_name: lableName,
                lable_type: lableType,
                lable_height: lableHeight,
                lable_width: lableWidth,
                print_name: printName,
                lable_content: lableContent,
                type:type,
                lable_template_type:lableTemplateType
              }, {
                success: function(m) {
            	 showMessage("Success","succeed");
                 
                 setTimeout(function(){
                	 v.parentView.render(templateName, templateType);
                    var d = $("#editLable").data('dialog');
                    if(d){
                      d.close();
                    }
                 },1500);
                }
              });  
              initialLabelModel = new LabelModel.LabelModel();
          }
      },
      addBarCode:function(evt){
        var btn = $(evt.target);
        var commonType= btn.data('val');
        $(this.el).find("#commonType").val(commonType);
        $(this.el).find(".tag_font").text("BarCode Tag");
        $(this.el).find("#common").css('display','');
        $(this.el).find("#custom").css('display','none');
        $(this.el).find("#addBarCode").css('display','none');//按钮显示
        $(this.el).find("#addCommon").css('display','');
        $(this.el).find("#goBack").css('display','');
      },
      addCommon:function(evt){
        var btn = $(evt.target);
        var commonType= btn.data('val');
        $(this.el).find("#commonType").val(commonType);
        $(this.el).find(".tag_font").text("Common Tag");
        $(this.el).find("#common").css('display','');//内容显示
        $(this.el).find("#custom").css('display','none');
        $(this.el).find("#addCommon").css('display','none');//按钮显示
        $(this.el).find("#addBarCode").css('display','');
        $(this.el).find("#goBack").css('display','');
      },
      goBack:function(){
    	$(this.el).find("#commonType").val("");
        $(this.el).find("#common").css('display','none');
        $(this.el).find("#custom").css('display','');
        $(this.el).find("#goBack").css('display','none');//按钮显示
        $(this.el).find("#addCommon").css('display','');
        $(this.el).find("#addBarCode").css('display','');
      },
      /*********************** add elements ***********************/
      addLine: function(evt) {
        $(this.el).find("#edit").append(" <hr id='line' size='2px'style='background:#000000; paddind:0px;margin-left:1px;margin-top:5px;position:absolute;height:2px;width:" + (width - 2) + "px'/>");
      },
      addVertical: function(evt) {
//        $(this.el).find("#edit").append(" <hr id='verline' size='2px'style='background:#000000;paddind:0px;margin-left:5px;margin-top:1px;position:absolute;width:2px;height:98%'/>");
        $(this.el).find("#edit").append(" <div id='verline' style='background:#000000;paddind:0px;margin-left:5px;margin-top:1px;position:absolute;width:2px;height:" + (height - 2) + "px'></div>");
      },
      addElName: function(evt) {
        var elementName = $(this.el).find("#elementName").val().toUpperCase();
        if(elementName!=""){//word-wrap: break-word; word-break: normal;
          $(this.el).find("#edit").append("<div class='elementName' style='font-family:Arial;position:absolute;margin-top:10px;margin-left:10px;width:120px;font-size:12px;text-align:left;'>{"+elementName+"}</div>");
          $(this.el).find("#elementName").val('');
        }else{
//          alert("Please enter the name, and then pressed the ADD button.");
          showMessage("Please enter the name, and then pressed the ADD button.","alert");
          $(this.el).find("#elementName").focus();
        }
      },
      addElText: function(evt) {
          var elementText = $(this.el).find("#elementText").val();
          if(elementText!=""){//word-wrap: break-word; word-break: normal;
            $(this.el).find("#edit").append("<div class='elementName' style='font-family:Arial;position:absolute;margin-top:10px;margin-left:10px;width:120px;font-size:12px;text-align:left;'>"+elementText+"</div>");
            $(this.el).find("#elementText").val('');
          }else{
//            alert("Please enter the [Element Text], and then pressed the ADD button.");
            showMessage("Please enter the [Element Text], and then pressed the ADD button.","alert");
            $(this.el).find("#elementText").focus();
          }
      },
      addCommonElement: function(evt) {
        var btn = $(evt.target);
        var commonElement = btn.data("val");
        var commonType = $(this.el).find("#commonType").val();
        if(commonType=="1"){
          $(this.el).find("#edit").append("<div class='elementName' style='font-family:Arial;position:absolute;margin-top:10px;margin-left:10px;width:120px;font-size:12px;text-align:left;'>{"+commonElement+"}</div>");
        }else if(commonType=="2"){
          $(this.el).find("#edit").append("<img class='elementName' draggable='false' style='position:absolute;paddind:0px;margin-top:30px;margin-left:20px;display:block;' src='/barbecue/barcode?data=$"+commonElement+"$&width=1&type=code39&height=35'/>");
        }
      },
      //add elements END
	  /*********************** mouse operation ***********************/
      changeMouse: function(evt) {
          //点击是添加样式和值
  	        $(element).css({
  	          "border-color": "#000",
  	          "border": "0px",
  	          "color": "#000"
  	        });
  	        
  	        $(evt.target).css({
  	          "border-color": "#3ccccc",
  	          "border": "1px solid",
  	          "color": "#3ccccc"
  	        });
      },
      mouseoutColor:function(evt){
    	  $(element).css({
  	          "border-color": "#000",
  	          "border": "0px",
  	          "color": "#000"
  	        });
    	  $(evt.target).css({
    		  "border-color": "#000",
  	          "border": "0px",
  	          "color": "#000"
  	        });
      },
      move: function(evt) {
        //点击是添加样式和值
    	evt.target.style.cursor = 'grabbing';
    	$('#edit').css('cursor', 'grabbing');
    	
        $(element).css({
          "border-color": "#000",
          "border": "0px",
          "color": "#000"
        });
        
        $(evt.target).css({
          "border-color": "#3ccccc",
          "border": "1px solid",
          "color": "#3ccccc"
        });
        
        element = evt.target;
        
        //barCoed不能修改元素宽度
        var elTagName = $(element).get(0).tagName;
        if(elTagName=='IMG'){
        	$("#elWidth").attr("disabled",true);
        }else{
        	$("#elWidth").attr("disabled",false);
        }
        
        var presentWidth = parseInt($(evt.target).css("width"));
        var presentHeight = parseInt($(evt.target).css("height"));
        var presentFont = parseInt($(evt.target).css("font-size"));
        var presentAlign = $(evt.target).css("text-align").trim();
        // if ($(evt.target).attr("id") == "verline") {
        //   presentWidth = parseInt($(evt.target).attr("size"));
        // };
        // if ($(evt.target).attr("id") == "line") {
        //   presentHeight = parseInt($(evt.target).attr("size"));
        // };
        
        $(this.el).find("#elWidth").val(presentWidth);
        $(this.el).find("#elHeight").val(presentHeight);
        $(this.el).find("#elFont").val(presentFont);
        $(this.el).find("#elAlign").val(presentAlign);

        //mousemove start
        posX = evt.pageX - parseInt($(evt.target).css("margin-left"));
        posY = evt.pageY - parseInt($(evt.target).css("margin-top"));
        this.$el.find("#edit").bind("mousemove", this.mousemove);
        //mousemove  end
        
        evt.preventDefault();
        return false;
      },
      onmouseup: function(evt) {
        evt.target.style.cursor = 'grab';
	    $('#edit').css('cursor', 'grab');
        $(this.el).find("#edit").unbind("mousemove");
      },
      mousemove: function(evt) {
    	evt.target.style.cursor = 'grabbing';
      	$('#edit').css('cursor', 'grabbing');
        // console.log(element);
        var moveX = evt.pageX - posX;

        var _width = parseInt($(element).css("width"));
        var _height = parseInt($(element).css("height"));
        
        if (moveX >= 0 & moveX <= (width - _width)) {
          //  console.log("x---"+moveX+"------------"+(width-_width));

          $(element).css("margin-left", moveX + "px");
        }
        var moveY = evt.pageY - posY;
        
        if (moveY >= 0 & moveY <= (height - _height)) {
          //  console.log("y---"+moveY);
          $(element).css("margin-top", moveY + "px");
        }
      },
      turnMouseUp: function() {
        clearInterval(inteval);
      },
      turn: function(evt) {
        var el = evt.target;
        $(el);
      },
      //移除元素
      removeElement: function() {
	     if (element != null) {
	        $(this.el).find("#elWidth").val("");
	        $(this.el).find("#elHeight").val("");
	        $(this.el).find("#elFont").val("");
	        $(element).remove();
	        element = null;
	     }
	  },
	  /************** According to the arrow button to move **************/
      turnLeft: function(evt) {
    	changeElementColor(element);//将被选中的元素设置为蓝色边框显示
        var sped = $("#sped").val() * 1;
        if (evt.button == 0) {
          inteval = setInterval(function() {
            if (element != null) {
              var left = parseInt($(element).css("margin-left"));
              left -= sped;
              if (left > 0) {
                $(element).css("margin-left", left + "px");
              }
            }
          }, 40);
        }
      },
      turnRight: function(evt) {
    	  changeElementColor(element);//将被选中的元素设置为蓝色边框显示
        var sped = $("#sped").val() * 1;
        if (evt.button == 0) {
          inteval = setInterval(function() {
            if (element != null) {
              var left = parseInt($(element).css("margin-left"));
              left += sped;
              var _width = parseInt($(element).css("width"));
              if (left < width - _width) {
                $(element).css("margin-left", left + "px");
              }
            }
          }, 40);
        }
      },
      turnTop: function(evt) {
    	  changeElementColor(element);//将被选中的元素设置为蓝色边框显示
        var sped = $("#sped").val() * 1;
        if (evt.button == 0) {
          inteval = setInterval(function() {
            if (element != null) {
              var top = parseInt($(element).css("margin-top"));
              top -= sped;
              if (top > 0) {
                $(element).css("margin-top", top + "px");
              }
            }
          }, 40);
        }
      },
      turnBottom: function(evt) {
    	  changeElementColor(element);//将被选中的元素设置为蓝色边框显示
        var sped = $("#sped").val() * 1;
        if (evt.button == 0) {
          inteval = setInterval(function() {
            if (element != null) {
              var top = parseInt($(element).css("margin-top"));
              top += sped;
              var _height = parseInt($(element).css("height"));
              if (top < height - _height) {
                $(element).css("margin-top", top + "px");
              }
            }
          }, 40);
        }
      },
      //end the arrow button to move
      /******** change attribute for the selected element **********/
      changeWidth: function(evt) {
        var lableWidth = this.$el.find("#lableWidth").val();
        if (lableWidth == "") {
        	
//        	alert("Please enter the Lable Width");
        	showMessage("Enter the Lable Width","alert");
            this.$el.find("#lableWidth").focus();
        } else {
          this.$el.find("#edit").css("width", lableWidth);
          width = lableWidth;
        }
      },
      changeHeight: function(evt) {
        var lableHeight = this.$el.find("#lableHeight").val();
        if (lableHeight == "") {
//        	 alert("Please enter the Lable Height");
        	showMessage("Enter the Lable Height","alert");
             this.$el.find("#lableHeight").focus();
        } else {
          this.$el.find("#edit").css("height", lableHeight);
          height = lableHeight;

        }
      },
      changeElWidth: function(evt) {
        var elWidth = this.$el.find("#elWidth").val();
        if (elWidth == "") {
//        	alert("Please enter the width");
        	showMessage("Enter the width","alert");
            this.$el.find("#elWidth").focus();
        }else {
        //   if ($(element).attr("id") == "verline") {
        //     $(element).attr("size", elWidth);
        //   } else {

            $(element).css("width", elWidth + "px");
          // }
        }

      },
      changeElHeight: function(evt) {
    	var elTagName = $(element).get(0).tagName;
    	  
        var elHeight = this.$el.find("#elHeight").val();
        if (elHeight == "") {
//        	alert("Please enter the height");
        	showMessage("Enter the height","alert");
            this.$el.find("#elHeight").focus();
        }else {
        //   if ($(element).attr("id") == "line") {
        //     $(element).attr("size", elHeight + "px");
        //   } else {

//            $(element).css("height", elHeight + "px");
          // }
            
            if(elTagName=='IMG'){
            	var imgSrc = $(element).attr("src");
            	var indexOfHeight = imgSrc.indexOf("height");
            	var newSrc=imgSrc.substring(0,indexOfHeight)+"height="+elHeight;
//            	console.log(imgSrc+"\n"+newSrc);
            	$(element).attr("src",newSrc)
            }else{
            	$(element).css("height", elHeight + "px");
            }
        }
      },
      changeElFont: function(evt) {
        var elFont = this.$el.find("#elFont").val();
        
        if (elFont == "") {
//        	alert("Please enter the font-size");
        	showMessage("Enter the font-size","alert");
            this.$el.find("#elFont").focus();
        } else {
          $(element).css("font-size", elFont + "px");
        }
      },
      changeElAlign:function(evt){
        var elAlign = this.$el.find("#elAlign").val();
        $(element).css("text-align", elAlign);
      },
      // end
      //the keyboard arrows to move
      keyMove:function(event){
    	changeElementColor(element);//将被选中的元素设置为蓝色边框显示
        var sped = $("#sped").val() * 1;
        if (sped != 0 && element != null) {   
          switch ((event || window.event).keyCode) 
          { 
            case 37:
                var left = parseInt($(element).css("margin-left"));
                left -= sped;
                if (left > 0) {
                  $(element).css("margin-left", left + "px");
                }
            break; 
            case 38:
                var top = parseInt($(element).css("margin-top"));
                top -= sped;
                if (top > 0) {
                  $(element).css("margin-top", top + "px");
                }
            break; 
            case 39: 
              var left = parseInt($(element).css("margin-left"));
                left += sped;
                var _width = parseInt($(element).css("width"));
                if (left < width - _width) {
                  $(element).css("margin-left", left + "px");
                } 
            break; 
            case 40: 
                var top = parseInt($(element).css("margin-top"));
                top += sped;
                var _height = parseInt($(element).css("height"));
                if (top < height - _height) {
                  $(element).css("margin-top", top + "px");
                }
            break; 
          }
        }
      },
      previewPrinter: function(){
    	 $(element).css({"border-color": "#000","color": "#000","border": "0px",});//将选中元素颜色设置为黑色
        //获取打印机名字列表
        var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
         //判断是否有该名字的打印机
        var printName = this.$el.find("#printName").val();
        var printer = "LabelPrinter";
        if(printName&&printName!=""){
        	printer = printName;
        }
        var printerExist = "false";
        var containPrinter = printer;
        var lableType = this.$el.find("#lableType").val().split("X");
        var size=[];
        if(lableType&&lableType!=''){
            size=this.$el.find("#lableType").val().split("X");
        }
//        var lableWidth = size[0]+"mm";
//        var lableHeight = size[1]+"mm";
        var lableWidth = size[0];
        var lableHeight = size[1];
//         alert("lableWidth="+lableWidth+"  lableHeight="+lableHeight);
        for(var i = 0;i<printer_count;i++){
          if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
            printerExist = "true";
            containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
            break;
          }
        }
	
        visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
        visionariPrinter.SET_PRINT_PAGESIZE(1,lableWidth,lableHeight,"");
//        console.log($('#lableContent').html());
        visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#lableContent').html());
        visionariPrinter.SET_PRINT_COPIES(1);
        visionariPrinter.PREVIEW();
	//visionariPrinter.PRINT();
      },
      yuangong:function(){
	var items=[
		   {"zhiwei":"Print","xingming":"周 卫 斌","pinyin":"Zhou Wei Bin"},
		   {"zhiwei":"Print","xingming":"葛 庆 玲","pinyin":"Ge Qing Ling"},
		   {"zhiwei":"Print","xingming":"郑 子 奇","pinyin":"Zheng Zi Qi"}
		   ];
	
	var html='<div id="edit" style="background: none repeat scroll 0% 0% rgb(255, 255, 255); padding: 0px; margin-right: auto; margin-left: auto; width: 230px; height: 110px; font-size: 12px; cursor: grab;">'+
	    '<hr id="line" size="2px" style="background: none repeat scroll 0% 0% rgb(0, 0, 0); margin-left: 0px; margin-top: 40px; position: absolute; height: 2px; width: 230px; cursor: grab; border: 0px none; color: rgb(0, 0, 0);">'+
	    '<div class="elementName" style="font-family: 黑体; position: absolute; margin-top: 7px; margin-left: 12px; width: 200px; font-size: 22px; text-align: center; cursor: grab; border: 0px none; color: rgb(0, 0, 0);">{ZHIWEI}</div>'+
	    '<div class="elementName" style="font-family: 黑体; position: absolute; margin-top: 53px; margin-left: 50px; width: 120px; font-size: 22px; text-align: center; cursor: grab; border: 0px none; color: rgb(0, 0, 0);">{XINGMING}</div>'+
	    '<div class="elementName" style="font-family: 黑体; position: absolute; margin-top: 83px; margin-left: 2px; width: 220px; font-size: 22px; text-align: center; cursor: grab; border: 0px none; color: rgb(0, 0, 0);">{PINYIN}</div>'+
	    '</div>';
	for(var i=0;i<items.length;i++){
	     var datas = { 
		    ZHIWEI:items[i].zhiwei,
		    XINGMING:items[i].xingming,
		    PINYIN:items[i].pinyin,	
		    };
	    var printhtml=assembleTemplate(datas,html);
	    visionariPrinter.SET_PRINTER_INDEXA ("tsc");//指定打印机打印  
	    visionariPrinter.SET_PRINT_PAGESIZE(1,"6cm","3cm","60X30");
	    visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",printhtml);
	    visionariPrinter.SET_PRINT_COPIES(1);
	    visionariPrinter.PREVIEW();
	    //visionariPrinter.PRINT();
		    
	}
      },
    });
    
    var changeElementColor = function changeElementColor(obj){
    	$(obj).css({
  		  "border-color": "#3ccccc",
            "border": "1px solid",
            "color": "#3ccccc"
          });
    }
    
    

    return CreateLableView;

}); //page_init