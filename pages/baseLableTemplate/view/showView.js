"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates) {

   //预览视图
    var ShowView = Backbone.View.extend({
      el: "#showView",
      template: templates.showView,
      render: function(lableContent, width, height) {       
        this.$el.html(this.template({
          lableContent: lableContent,
          width: width,
          height: height
        }));
      },
    });
  

	return ShowView;

}); //page_init