"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/labelModel",
  "./showView",
  "./createLabelView",
  "./createContainerView",
  "oso.lib/art/plugins/newArtDialog",
  "jqueryui/dialog",
  "showMessage"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, LabelModel, ShowView, CreateLabelView, CreateContainerView, artDialog) {


  var updateLable = new CreateLabelView();
  updateLable.render(new LabelModel.LabelModel());

  var updateContainer = new CreateContainerView();
  updateContainer.render(new LabelModel.LabelModel({
    lable_height: "426",
    lable_width: "363",
    lable_template_type: "2"
  }));

  var labelCollection = new LabelModel.LabelCollection();

  var updateDialog = function(elementId, title, containerWidth) {
    var width = '780';
    if(typeof(containerWidth) != 'undefined' && containerWidth > 0){
      width = containerWidth;
    }
    // $("#" + elementId).dialog({
    //   title: title,
    //   width: width,
    //   height: '580',
    //   draggable: true,
    //   modal: true,
    // });
    // modify by huhy
    var d = new artDialog({
        title: title,
        lock: true,
        draggable: true,
        width: width,
        opacity:0.3,
        content: $("#" + elementId).get(0),
    });
    $("#" + elementId).data('dialog', d);
    //end
  };  
 
  var LableView = Backbone.View.extend({
    el: "#main",
    template: templates.lableList,
    collection: labelCollection,
    typekey: "",
    containerTypeKey: "",
    render: function(lableName, templateType) {
      var v = this;
      this.collection.fetch({
        data: {
          pageNo: labelCollection.pageCtrl.pageNo,
          pageSize: labelCollection.pageCtrl.pageSize,
          lablename: lableName,
          templateType: templateType
        },
        success: function(collection) {
          v.typekey = LabelModel.LabelCollection.typeKey;
          v.containerTypeKey = LabelModel.LabelCollection.containerTypeKey;
          // console.log(collection.toJSON());
          v.$el.find("#lableList").html(v.template({
            lables: collection.toJSON(),
            pageCtrl: LabelModel.LabelCollection.pageCtrl
          }));
          
          
          onLoadInitZebraTable();
        }
      });
    },
    events: {
      "click .gop": "changePage",
      "click #addLable": "addLable",
      "click #addContainer": "addContainer",
      "click button[name='Submit1'][templateTypeValue='1']": "editLable",
      "click button[name='Submit1'][templateTypeValue='2']": "editContainer",
      "click button[name='Submit2']": "delLable",
      "click button[name='showTemp']": "showLable",
      "click #search": "searchCollection",
      "keydown #input_lableName":"keydownSearch"
    },   
    keydownSearch: function(evt) {
    	var v = this;
    	if(evt.keyCode==13){
    		var lableName = $("#input_lableName").val();
    	    var templateType = $("#input_templateType").val();
    	    if(lableName&&lableName!=""){
    	    	v.render(lableName, templateType);
    	    }else{
    	    	showMessage("Enter the [Template Name]","alert");
    	    }
    	}
    },
    searchCollection: function(evt) {
      var lableName = $("#input_lableName").val();
      var templateType = $("#input_templateType").val();
      this.render(lableName, templateType);

    },
    changePage: function(evt) {

      var btn = $(evt.target);
      if (btn.data("pageno")) {
        labelCollection.pageCtrl.pageNo = parseInt(btn.data("pageno"));
      } else if (btn.data("pageplus")) {
        labelCollection.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
      } else if (btn.data("pageto")) {
        var pageto = $("#main").find("#jump_p2").val();
        labelCollection.pageCtrl.pageNo = parseInt(pageto);
      } else return;
      var lableName = $("#input_lableName").val();
      var lableType = $("#input_lableType").val();
      this.render(lableName, lableType);
    },
    addLable: function(evt) {
      var _self = this;
      var typekey = this.typekey;
      updateLable.parentView = this;
      updateLable.render("", typekey, _self.collection);
      updateDialog("editLable", "Create label template", 780);
    },
    addContainer: function(evt) {
      var _self = this;
      var containerTypeKey = this.containerTypeKey;
      updateContainer.parentView = this;
      updateContainer.render("", containerTypeKey, _self.collection);
      updateDialog("editContainer", "Create container template", 1000);
    },
    editLable: function(evt) {
      var _self = this;
      var evt = evt.target;
      var lableid = $(evt).attr("id") * 1;
      // alert(lableid);
      if (!lableid) return;
      var collection = this.collection;
      var model = collection.findWhere({
        lable_id: lableid
      });
      // console.log(model);
      updateLable.parentView = this;
      var typekey = this.typekey;
      updateLable.render(model, typekey, _self.collection);
//      console.log( _self.collection);
      updateDialog("editLable", "Update label template", 780);
    },
    editContainer: function(evt) {
      var _self = this;
      var evt = evt.target;
      var lableid = $(evt).attr("id") * 1;
      if (!lableid) return;
      var collection = this.collection;
      var model = collection.findWhere({
        lable_id: lableid
      });
      updateContainer.parentView = this;
      var containerTypeKey = this.containerTypeKey;
      updateContainer.render(model, containerTypeKey, _self.collection);
      updateDialog("editContainer", "Update container templates", 1000);
    },
    delLable: function(evt) {
      var lableName = $("#input_lableName").val();
      var templateType = $("#input_templateType").val();//查询条件
      var evt = evt.target;
      var lableid = $(evt).attr("id") * 1;
      if (!lableid) return;
      var collection = this.collection;
      var model = collection.findWhere({
        lable_id: lableid
      });
      var name = model.get("lable_name");
      var v = this;
      //modify by huhy 改confirm提示为dialog提示
      var dialog = new artDialog({
            icon: 'question',
            title: 'Delete['+name+']',
            lock: true,
            width: 250,
            height: 100,
            opacity:0.3,
            content: '<p>Delete 【'+ name + '】 label?</p>',
            ok: function () {
                collection.remove(model);
                model.url = model.url + "?id=" + model.get("lable_id");
                model.destroy({
                  success: function(model, response) {
                    showMessage("Success","succeed");
                    v.render(lableName, templateType);
                  },
                  error: function() {
                   showMessage("Error","error");
                  }
                });
            },
            okVal:'Sure',
            cancel: function () {
                dialog.close();
            },
            cancelVal:'Cancel'
      });
      //end
    },
    showLable: function(evt) {
      var evt = evt.target;
      var lableContent = $(evt).attr("lableContent");
      var lableName = $(evt).attr("lablename");
      var evt_width = $(evt).data("width") + 110;
      var evt_height = $(evt).data("height") + 110;
      
      // console.log("evt_width="+evt_width+"  evt_height="+evt_height);
      // var array=[
      //   {key:'logo',val:'VVME'},
      //   {key:'pcId',val:'1000034'},
      //   {key:'billNo',val:'E8654512'},
      //   {key:'supplier',val:'S8546222'},
      //   {key:'pcName',val:'BULB/H1/25K'},
      //   {key:'origin',val:'made in chian'},
      //   {key:'barCode',val:"<img src='/barbecue/barcode?data=NA&width=1&height=35&type=code39'/>"}];
      // for (var i = 0; i<array.length; i++) {
      //     var key = '{'+array[i].key+'}';
      //     var val = array[i].val;
      //     lableContent = lableContent.replace(key,val);
      // };
     
      var showView = new ShowView();
      showView.render(lableContent, $(evt).data("width") + 5, $(evt).data("height"));
      // $("#showView").dialog({
      //   title: "show Lable " + lableName,
      //   draggable: true,
      //   width: evt_width,
      //   height: evt_height,
      //   modal: true,
      // });
      new artDialog({
            title: "Show label",
            lock: true,
            draggable: true,
            width: evt_width,
            height: evt_height,
            opacity:0.3,
            content: showView.el,
      });
    }
  });

  return LableView;

}); //page_init