"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/labelModel",
  "showMessage"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, LabelModel) {   
    var height = 510;
    var width = 390;
    var animateHeight = 0;
    var initialContainerModel = new LabelModel.LabelModel({
      lable_height: "510", 
      lable_width: "390", 
      lable_template_type: "2", 
      print_name: "LabelPrinter", 
      lable_type: "102mmX152mm"
    });   

    function insertCursor(val) {
      var textObj = document.getElementById('containerText');
      var maxLength = $('#containerText').val().length;
      if (textObj.setSelectionRange) {
          var rangeStart = textObj.selectionStart;
          var rangeEnd = textObj.selectionEnd;
          if(rangeStart >= maxLength || rangeStart == 0){
        	  showMessage("Please stay cursor in text area!","alert");
            return;
          } 
          var tempStr1 = textObj.value.substring(0, rangeStart);
          var tempStr2 = textObj.value.substring(rangeEnd);
          textObj.value = tempStr1 + val + tempStr2;
          textObj.selectionStart = rangeStart + val.length;
//          console.log(textObj.selectionStart);
          textObj.setSelectionRange(textObj.selectionStart, textObj.selectionStart);
      } else {
    	  showMessage("This version of Mozilla based browser does not support setSelectionRange!","alert");
//        alert("This version of Mozilla based browser does not support setSelectionRange");
      }
    } 
  
    var templateName = $("#input_lableName").val();
    var templateType = $("#input_templateType").val();//获取查询条件

    var CreateContainerView = Backbone.View.extend({
      template: templates.editContainer,
      el: "#editContainer",
      parentView: null,     
      model: initialContainerModel, 
      render: function(model, containerTypeKey, collection) {  
    	this.collection = collection;
        if(typeof(model) != 'undefined' && model != ""){
          this.model = model;          
        }else{
          this.model = initialContainerModel;
        }  
        this.$el.html(this.template({
          model: this.model.toJSON(),
          containerTypeKey:containerTypeKey
        }));            
      },
      events: {
        "click #create": "saveContainer",
        "click #generate": "generateContainer", 
        "click #preview": "previewPrinter",
        "click input[name='msgInsert']": "insertMsg",
        "click input[name='barcodeInsert']": "insertBarcode",
        "click #downArrow": "animateDown",
        "click #upArrow": "animateUp",
        "change #type": "changeType",
        "change #lableName": "changeName"
      },
      changeName:function(evt){
    	  var v = this;
    	  var flag = true;
    	  var lableName = v.$el.find("#lableName").val();
    	  var prevLableName = v.$el.find("#prevLableName").val();
          if(lableName!=prevLableName){
        	  var lablemodel = v.collection.findWhere({'lable_name':lableName});
        	  if(lablemodel){
        		  showMessage("Lable Name【"+lableName+"】 already exists","alert");
        		  v.$el.find("#lableName").focus();
        		  flag = false;
        	  }
          }
          return flag;
      },
      insertMsg: function(evt){
        var value = $(evt.target).data("value");
        insertCursor('{' + value + '}');
      },
      insertBarcode: function(evt){
        var value = $(evt.target).data("value");
        insertCursor('<img src="/barbecue/barcode?data=$' + value + '$&width=1&height=50&type=code39"/>');
      },
      animateUp: function(evt){
          animateHeight += 339;
          if(animateHeight > 0){
          animateHeight = -339;
        }
        if(!$(".inner").is(":animated")){          
          $(".inner").css("position","relative").animate({'top': animateHeight+'px'});
          $('#upArrow').attr("disabled", true);
          $('#downArrow').attr("disabled", false);
        } 
      },
      animateDown: function(evt){
        animateHeight -= 339;
        if(animateHeight < -339){
          animateHeight = 0;
        }
       
        if(!$(".inner").is(":animated")){         
          $(".inner").css("position","relative").animate({'top': animateHeight+'px'});
          $('#upArrow').attr("disabled", false);
          $('#downArrow').attr("disabled", true);
        }   
      },
      changeType: function(evt){        
    	  
    	  //4、11修改为可编辑的
    	  var content = $("#clpText").html();
    	  if($(evt.target).val() == 1){  //clp
    		  $("#containerText").val(content);
  	      }else if($(evt.target).val() == 3){//tlp
  	    	content = $("#tlpText").html();
  	    	 $("#containerText").val(content);
  	      }
  	      
    	  
    	  var containerText = $("#containerText").val(); 
    	  
    	  
    	  
    	  
		 /*
		  * 自动替换4、11号之前
		  var typeValue = "C";        
	      if($(evt.target).val() == 1){
	        typeValue = "C";
	      }else if($(evt.target).val() == 3){
	        typeValue = "T";
	      }
	      
        //获取textarea内容        
        var content = $("#containerText").val(); 
        //正则获取container type        
        var reBed = /<center><b>\s*[CT]\s*<\/b><\/center>/;
        var target = content.match(reBed);     
        content = content.replace(target, "<center><b>"+typeValue+"</b></center>");
        
        var brBed = /<center>\s*[CT]LP\{\w*\}\s*<\/center>/;
        target = content.match(brBed);
        content = content.replace(target, "<center>"+typeValue+"LP{CONID}</center>");
        $("#containerText").val(content);*/
        
      //获取预览内容,存在预览的内容，修改相应类型
        var containerPreview= this.$el.find("#containerPreview").html().trim();
        if(containerPreview&&containerPreview!=""){
        	this.$el.find("#containerPreview").html(containerText);
        }
      },
      previewPrinter: function(){
        //获取打印机名字列表
        var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
         //判断是否有该名字的打印机
        var printer = "LabelPrinter";
        var printerExist = "false";
        var containPrinter = printer;
        for(var i = 0;i<printer_count;i++){
          if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
            printerExist = "true";
            containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
            break;
          }
        }
        visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
        visionariPrinter.SET_PRINT_PAGESIZE(1,"10.2cm","15.2cm","102X152");
        visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#containerPreview').html());
        visionariPrinter.SET_PRINT_COPIES(1);
        visionariPrinter.PREVIEW();
      },
      saveContainer: function(){           
    	  var v = this;
    	  var flag = v.changeName();//唯一性验证
    	  //保存数据
    	  if(flag){
        	  var lableName = this.$el.find("#lableName").val();
              var lableType = this.$el.find("#lableType").val();
              var lableWidth = this.$el.find("#lableWidth").val();
              var lableHeight = this.$el.find("#lableHeight").val();
              var printName = this.$el.find("#printName").val();
              var lableContent = this.$el.find("#containerText").val();
              var type = this.$el.find("#type").val(); 
              this.model.save({
                lable_name: lableName,
                lable_type: lableType,
                lable_height: lableHeight,
                lable_width: lableWidth,
                print_name: printName,
                lable_content: lableContent,
                type:type,
                lable_template_type:2
              }, {
                success: function(m) {
                  showMessage("Success","succeed");
                  
                  setTimeout(function(){
                	  v.parentView.render(templateName,templateType);
                    var d = $("#editContainer").data('dialog');
                    if(d){
                      d.close();
                    }
                  },1500);
                }
              });
              initialContainerModel = new LabelModel.LabelModel({
              lable_height: "510", 
              lable_width: "390", 
              lable_template_type: "2", 
              print_name: "LabelPrinter", 
              lable_type: "102X152"
            });
          }
    	         
      },
      generateContainer: function(){
        var content = $("#containerText").val();         
        this.$el.find("#containerPreview").html(content);
      }
    });
    return CreateContainerView;

}); //page_init




