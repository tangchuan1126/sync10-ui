"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "select2",
  ""
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates) {

	//    tab2视图
    var Tab2View = Backbone.View.extend({
      el: "#tab2",
      template: templates.tab2,
      render: function(st_date, en_date) {
        this.$el.html(this.template());
        this.$el.find('#input_templateType').select2({
          placeholder: "Template Type...",
          allowClear: true,
        });
      },
    });

    return Tab2View;

}); //page_init