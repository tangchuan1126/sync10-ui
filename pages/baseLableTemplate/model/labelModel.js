"use strict";
define([
  "config",
  "jquery",
  "backbone"
], function(page_config, $, Backbone) {


//模板数据模型
    var LableModel = Backbone.Model.extend({
      url: page_config.lableModel.url,
      idAttribute: "lable_id",
      initialize:function(){
          this.on("invalid",function(model, error) {
              console.log(error);
              alert(error);
//               $('<div></div>').appendTo("body").html(error.toString())
//              .dialog({
//                  title:'数据校验错误',
//                  modal:true,
//                  dialogClass: "alert",
//                  buttons: [ { text: "OK", click: function() { $(this).dialog("close").remove(); } } ]
//              });
          });
      },
      defaults: {
        lable_name: "",
        lable_type: "60mmX30mm",
        lable_height: "105",
        lable_width: "230",
        print_name: "LabelPrinter",
        lable_content: "",
        type:"1",
        lable_template_type:"1"
      },
      validate:function(attrs) {
          if(attrs.lable_name=="") return "Please enter the [Lable Name]";
      }
    });

    //模板数据集合
    var LableCollection = Backbone.Collection.extend({
      model: LableModel,
      url: page_config.lableCollection.url,
      parse: function(response) {
        LableCollection.pageCtrl = response.pageCtrl;
        LableCollection.typeKey =  response.typeKey;
        LableCollection.containerTypeKey =  response.containerTypeKey;
        return response.lables;
      }, 
      pageCtrl: {
        pageNo: 1,
        pageSize: 20
      }, 
      typeKey: {
        key: "",
        value: ""
      },
      containerTypeKey: {
        key: "",
        value: ""
      }
    });

    return {
      LabelModel:LableModel,
      LabelCollection:LableCollection
    };

  }); //page_init