define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['canvassion'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=helpers.helperMissing, alias2=this.escapeExpression, alias3=this.lambda;

  return "<div class=\"tag-container\">\n    <h2>Track No <span style=\"position:absolute;top:2px;left:420px\">"
    + alias2((helpers.addOne || (depth0 && depth0.addOne) || alias1).call(depth0,(data && data.index),{"name":"addOne","hash":{},"data":data}))
    + "/"
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].details : depths[1])) != null ? stack1.ITEMS : stack1)) != null ? stack1.length : stack1), depth0))
    + "</span></h2>\n    <div class=\"tag-module main-module\" style=\"border:none;border-top:1px solid #333\">\n        <div class=\"module-left\">\n            <dl>\n              <dd style=\"font-size: 20px;\">TO: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_WHS_NAME : stack1), depth0))
    + "</dd>\n              <dd>FROM: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_WHS_NAME : stack1), depth0))
    + "</dd>\n              <dd>"
    + ((stack1 = (helpers.formatDate || (depth0 && depth0.formatDate) || alias1).call(depth0,((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CREATE_TIME : stack1),{"name":"formatDate","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</dd>\n            </dl>\n        </div>      \n        <div class=\"module-right\">\n            <dl>\n              <dd>TN: "
    + alias2(alias3((depth0 != null ? depth0.WAYBILL_ITEM_ID : depth0), depth0))
    + "</dd>\n              <dd><img src=\"/barbecue/barcode?data="
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.WAYBILL : stack1)) != null ? stack1.waybill_id : stack1), depth0))
    + "&width=1&height=50&type=code39\"/></dd>\n            </dl>\n        </div>\n\n    </div>\n    <div class=\"tag-module\" style=\"position:relative;\">\n        <dl>\n          <dt>Shipper</dt>\n          <dd class=\"module-name\">Contanct: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_NAME : stack1), depth0))
    + "</dd>\n          <dd class=\"module-tel\">Tel: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_TEL : stack1), depth0))
    + "</dd>\n          <dd class=\"module-address\">Address: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_ADDRESS : stack1), depth0))
    + "</dd>\n        </dl>\n    </div>\n    <div class=\"tag-module\">\n        <dl>\n          <dt>Consignee</dt>\n          <dd class=\"module-name\">Contanct: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_NAME : stack1), depth0))
    + "</dd>\n          <dd class=\"module-tel\">Tel: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_TEL : stack1), depth0))
    + "</dd>\n          <dd class=\"module-address\">Address: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_ADDRESS : stack1), depth0))
    + "</dd>\n        </dl>\n    </div>\n    <div class=\"tag-module info-module\">\n        <div>\n            <dl>\n              <dt>货物信息</dt>\n              <dd>货物内容: "
    + alias2(alias3((depth0 != null ? depth0.CONTENT : depth0), depth0))
    + "</dd>\n              <dd>数量: "
    + alias2(alias3(((stack1 = (depths[1] != null ? depths[1].details : depths[1])) != null ? stack1.QUANTITY : stack1), depth0))
    + "</dd>\n              <dd>体积: "
    + ((stack1 = (helpers.formatVolume || (depth0 && depth0.formatVolume) || alias1).call(depth0,(depth0 != null ? depth0.LENGTH : depth0),(depth0 != null ? depth0.WIDTH : depth0),(depth0 != null ? depth0.HEIGHT : depth0),(depth0 != null ? depth0.VOLUME_UNIT_NAME : depth0),{"name":"formatVolume","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</dd>\n            </dl>\n        </div>\n        <div>\n            <dl>\n              <dt>费用信息</dt>\n              <dd>运费: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_WAYBILL : stack1), depth0))
    + "</dd>\n              <dd>声明价值: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.DECLARED_VALUE : stack1), depth0))
    + "</dd>\n              <dd>保价费用: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_INSURED : stack1), depth0))
    + "</dd>\n              <dd>费用合计: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_TOTAL : stack1), depth0))
    + "</dd>\n              <dd>付款方式: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_PAYMENT_NAME : stack1), depth0))
    + "</dd>\n              <dd>月结账号: "
    + alias2(alias3(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.MONTHLY_PAYMENT : stack1), depth0))
    + "</dd>\n            </dl>\n        </div>\n    </div>\n    <div class=\"tag-module\">\n        <dl>\n          <dt>不可多层码放</dt>\n        </dl>\n    </div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    return "";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "﻿<style>\n*{box-sizing: border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;}\nbody{font-size: 12px;font-family: Arial;color: #333333;}\n.tag-container{ height:575px;padding:0px;text-align: left;font-weight: bold;word-break: break-word;margin: 0px;position: relative;}\n.tag-container h2{text-align: center;font-size: 24px;}\n.tag-container .tag-module{line-height: 25px;padding: 0px;border-top: 1px solid #333;position: relative;}\n.tag-module dt{font-size: 18px;}\n.tag-module .module-name{width: 205px;margin-left:0px;}\n.tag-module .module-address{margin-left:0px;}\n.tag-module .module-tel{width: 134px; margin-left:0px}\n.tag-module.info-module:before, .tag-module.main-module:before, .tag-module.info-module:after, .tag-module.main-module:after{ content: \" \"; display: table;}\n.tag-module.info-module:after, .tag-module.main-module:after{clear: both;}\n.tag-module.main-module{border-top-color: #fff;font-size: 14px;}\n.tag-module.main-module div.module-left{float: left;height: 100px;width:180px;}\n.tag-module.main-module div.module-right{float: left;height: 100px;}\n.tag-module.main-module div dd{display: block;width: auto;}\n.tag-module dl{\n  margin-left: 20px;\n}\n\n.tag-module dl dd{\n  margin-left: 0px;\n}\n.main-module .module-right{width:200px;}\n/*.main-module .module-left{width: 134px;}*/\n.main-module .module-left dd:nth-child(1){font-size: 20px;}\n.tag-module.info-module div{width: 49%; float: left;}\n.tag-module.info-module div dd{font-weight: normal;display: block;width: auto;line-height: 18px;}\n</style>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.details : depth0)) != null ? stack1.ITEMS : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "";
},"useData":true});
return templates;
});