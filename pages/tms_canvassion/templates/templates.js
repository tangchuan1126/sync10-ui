(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['canvassion'] = template({"1":function(depth0,helpers,partials,data,depths) {
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "<div class=\"tag-container\">\r\n    <h2>Track No <span style=\"position:absolute;top:2px;left:420px\">"
    + escapeExpression(((helpers.addOne || (depth0 && depth0.addOne) || helperMissing).call(depth0, (data && data.index), {"name":"addOne","hash":{},"data":data})))
    + "/"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].details : depths[1])) != null ? stack1.ITEMS : stack1)) != null ? stack1.length : stack1), depth0))
    + "</span></h2>\r\n    <div class=\"tag-module main-module\" style=\"border:none;border-top:1px solid #333\">\r\n        <div class=\"module-left\">\r\n            <dl>\r\n              <dd style=\"font-size: 20px;\">TO: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_WHS_NAME : stack1), depth0))
    + "</dd>\r\n              <dd>FROM: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_WHS_NAME : stack1), depth0))
    + "</dd>\r\n              <dd>";
  stack1 = ((helpers.formatDate || (depth0 && depth0.formatDate) || helperMissing).call(depth0, ((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CREATE_TIME : stack1), {"name":"formatDate","hash":{},"fn":this.program(2, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</dd>\r\n            </dl>\r\n        </div>      \r\n        <div class=\"module-right\">\r\n            <dl>\r\n              <dd>TN: "
    + escapeExpression(lambda((depth0 != null ? depth0.WAYBILL_ITEM_ID : depth0), depth0))
    + "</dd>\r\n              <dd><img src=\"/barbecue/barcode?data="
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.WAYBILL : stack1)) != null ? stack1.waybill_id : stack1), depth0))
    + "&width=1&height=50&type=code39\"/></dd>\r\n            </dl>\r\n        </div>\r\n\r\n    </div>\r\n    <div class=\"tag-module\" style=\"position:relative;\">\r\n        <dl>\r\n          <dt>Shipper</dt>\r\n          <dd class=\"module-name\">Contanct: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_NAME : stack1), depth0))
    + "</dd>\r\n          <dd class=\"module-tel\">Tel: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_TEL : stack1), depth0))
    + "</dd>\r\n          <dd class=\"module-address\">Address: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.SHIPPER : stack1)) != null ? stack1.SHIPPER_ADDRESS : stack1), depth0))
    + "</dd>\r\n        </dl>\r\n    </div>\r\n    <div class=\"tag-module\">\r\n        <dl>\r\n          <dt>Consignee</dt>\r\n          <dd class=\"module-name\">Contanct: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_NAME : stack1), depth0))
    + "</dd>\r\n          <dd class=\"module-tel\">Tel: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_TEL : stack1), depth0))
    + "</dd>\r\n          <dd class=\"module-address\">Address: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.CONSIGNEE : stack1)) != null ? stack1.CONSIGNEE_ADDRESS : stack1), depth0))
    + "</dd>\r\n        </dl>\r\n    </div>\r\n    <div class=\"tag-module info-module\">\r\n        <div>\r\n            <dl>\r\n              <dt>货物信息</dt>\r\n              <dd>货物内容: "
    + escapeExpression(lambda((depth0 != null ? depth0.CONTENT : depth0), depth0))
    + "</dd>\r\n              <dd>数量: "
    + escapeExpression(lambda(((stack1 = (depths[1] != null ? depths[1].details : depths[1])) != null ? stack1.QUANTITY : stack1), depth0))
    + "</dd>\r\n              <dd>体积: ";
  stack1 = ((helpers.formatVolume || (depth0 && depth0.formatVolume) || helperMissing).call(depth0, (depth0 != null ? depth0.LENGTH : depth0), (depth0 != null ? depth0.WIDTH : depth0), (depth0 != null ? depth0.HEIGHT : depth0), (depth0 != null ? depth0.VOLUME_UNIT_NAME : depth0), {"name":"formatVolume","hash":{},"fn":this.program(2, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</dd>\r\n            </dl>\r\n        </div>\r\n        <div>\r\n            <dl>\r\n              <dt>费用信息</dt>\r\n              <dd>运费: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_WAYBILL : stack1), depth0))
    + "</dd>\r\n              <dd>声明价值: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.DECLARED_VALUE : stack1), depth0))
    + "</dd>\r\n              <dd>保价费用: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_INSURED : stack1), depth0))
    + "</dd>\r\n              <dd>费用合计: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_TOTAL : stack1), depth0))
    + "</dd>\r\n              <dd>付款方式: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.FEE_PAYMENT_NAME : stack1), depth0))
    + "</dd>\r\n              <dd>月结账号: "
    + escapeExpression(lambda(((stack1 = ((stack1 = (depths[1] != null ? depths[1].waybillInfo : depths[1])) != null ? stack1.FEE : stack1)) != null ? stack1.MONTHLY_PAYMENT : stack1), depth0))
    + "</dd>\r\n            </dl>\r\n        </div>\r\n    </div>\r\n    <div class=\"tag-module\">\r\n        <dl>\r\n          <dt>不可多层码放</dt>\r\n        </dl>\r\n    </div>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,depths) {
  var stack1, buffer = "﻿<style>\r\n*{box-sizing: border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;}\r\nbody{font-size: 12px;font-family: Arial;color: #333333;}\r\n.tag-container{ height:575px;padding:0px;text-align: left;font-weight: bold;word-break: break-word;margin: 0px;position: relative;}\r\n.tag-container h2{text-align: center;font-size: 24px;}\r\n.tag-container .tag-module{line-height: 25px;padding: 0px;border-top: 1px solid #333;position: relative;}\r\n.tag-module dt{font-size: 18px;}\r\n.tag-module .module-name{width: 205px;margin-left:0px;}\r\n.tag-module .module-address{margin-left:0px;}\r\n.tag-module .module-tel{width: 134px; margin-left:0px}\r\n.tag-module.info-module:before, .tag-module.main-module:before, .tag-module.info-module:after, .tag-module.main-module:after{ content: \" \"; display: table;}\r\n.tag-module.info-module:after, .tag-module.main-module:after{clear: both;}\r\n.tag-module.main-module{border-top-color: #fff;font-size: 14px;}\r\n.tag-module.main-module div.module-left{float: left;height: 100px;width:180px;}\r\n.tag-module.main-module div.module-right{float: left;height: 100px;}\r\n.tag-module.main-module div dd{display: block;width: auto;}\r\n.tag-module dl{\r\n  margin-left: 20px;\r\n}\r\n\r\n.tag-module dl dd{\r\n  margin-left: 0px;\r\n}\r\n.main-module .module-right{width:200px;}\r\n/*.main-module .module-left{width: 134px;}*/\r\n.main-module .module-left dd:nth-child(1){font-size: 20px;}\r\n.tag-module.info-module div{width: 49%; float: left;}\r\n.tag-module.info-module div dd{font-weight: normal;display: block;width: auto;line-height: 18px;}\r\n</style>\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.details : depth0)) != null ? stack1.ITEMS : stack1), {"name":"each","hash":{},"fn":this.program(1, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true,"useDepths":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
})();