/**
 * Created by huhy on 2015-3-23 10:48:13
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "handlebars_ext"
    //"js/models/.."
   // "require_css!../../css/canvassion_tag.css"
],function($,Backbone,Handlebars,templates,Handlebars_ext){
    //格式化时间
    Handlebars_ext.registerHelper("formatDate",function(oldTime){
        console.log(oldTime)
        if(oldTime!=undefined&&oldTime!=""){
            return oldTime.substring(0,16);
        } else{
            return "";
        }
        
    })

    // 格式化体积
    Handlebars_ext.registerHelper("formatVolume",function(length,width,height,volumeUnitName){
        var result="";
        if(length!=undefined&&width!=undefined&&height!=undefined
            &&length!=""&&width!=""&&height!=""){
            result  =length+" x " + width+" x "+height+" " +volumeUnitName;
        }

        return result;
        
    })    

    // debug
    Handlebars_ext.registerHelper("debug",function(obj){
        console.log(obj);

        // return Handlebars_ext.SafeString(result);
        
    }) 

      //注册索引+1的helper
       Handlebars_ext.registerHelper("addOne",function(index){
         //返回+1之后的结果
         return index+1;
       });    
    
    return Backbone.View.extend({
        template: templates.canvassion,
        initialize: function( option ) {
            this.setElement( option.el );
        },
        render:function( waybill_id ) {
            var that = this;
            console.log(waybill_id);
            $.ajax({
                    url: '/Sync10/_tms/tmsWaybill/getTmsWaybillById',
                    data: 'waybillId=' + waybill_id,
                    dataType: 'json',
                    type: 'get',
                    success: function( data ){
                        console.dir(data);
                        if (data.DATA!=undefined) {
                            var waybillInfo = data.DATA[0] 
                            that.$el.html( that.template({
                                    waybillInfo:waybillInfo,
                                    details:waybillInfo.WAYBILL.DETAILS
                                }

                            ) );
                        }

                    },
                    error:function(){
                        
                    }
            });             
            
        }       
    });
});