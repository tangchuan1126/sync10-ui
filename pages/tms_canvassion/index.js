"use strict";
require([
     "../../requirejs_config", 
], function(){
	require( ["jquery", "./js/views/canvassionView"], function( $, View ) {
    	$( function(){
            var request = {
                QueryString: function (val) {
                    var uri = window.location.search;
                    var re = new RegExp("" + val + "\=([^\&\?]*)", "ig");
                    console.log(uri.match(re));
                    return ((uri.match(re)) ? (uri.match(re)[0].substr(val.length + 1)) : null);
                }
            }             
    		// 如果当前页面有请求参数
            var waybill_id=request.QueryString('waybillId');
            if (waybill_id!=undefined&&waybill_id!="") {
                new View( { el: 'body' } ).render( waybill_id );
            }



    		
    	} )
    	
    } );

});
