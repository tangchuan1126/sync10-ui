/**
 * Created by subin on 2014.9.12.
 */
"use strict";
require([
    "jquery",
    "backbone",
    "handlebars",
    "./view/productTitleView",
    "./view/productTitleResultView",
    "blockui",
    "jqueryui/tabs"
],function($ ,Backbone,Handlebars,ProductTitleView,ProductTitleResultView){
	var url = location.href;
	var productId = 0;
	if(url.indexOf("&") > 0){
		productId = url.substring(url.indexOf("id=") + 3,url.indexOf("&"));
	}else{
		productId = url.substring(url.indexOf("id=") + 3);
	}
	var titleId = 0;
	//正在查看的Title ID
	var view_title_id = 0;
	if(url.indexOf("title_id") > 0){
		view_title_id = parseInt($.trim(url.substring(url.indexOf("title_id=") + 9)));
	}
	var customerId = 0;
	
	var role = "Admin";
	if(url.indexOf("titleId") > 0){
		role = "Title";
		titleId = url.substring(url.indexOf("titleId=") + 8);
	}else if(url.indexOf("customerId") > 0){
		role = "Customer";
		customerId = url.substring(url.indexOf("customerId=") + 11);
	}
	
	var view_result = new ProductTitleResultView({"el":"#customer_title_result","productId":productId,"role":role,"customerId":customerId,"titleId":titleId});
	var view_form = new ProductTitleView({"el":"#customer_title_form","role":role,"customerId":customerId,"titleId":titleId});
	view_result.setRole(role);
	view_result.setTitleId(titleId);
	view_result.setCustomerId(customerId);
	view_result.setFormView(view_form);
	
	view_form.setRole(role);
	view_form.setTitleId(titleId);
	view_form.setCustomerId(customerId);	
	view_form.setProductId(productId);
	
	view_form.setResultView(view_result);
	
	view_form.render();
	view_result.render();
	if(view_title_id > 0){
		$("a.edit[data-titleId='" + view_title_id + "']").click();
		$("a.edit[data-titleId='" + view_title_id + "']").parent().parent().addClass("over")
	}
	
	$(window.parent.document).find("div.blockUI").remove();
	
	$("div.aui_content2").hide();
});