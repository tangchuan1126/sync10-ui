define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['product_title'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value=\""
    + alias3(((helper = (helper = helpers.title_id || (depth0 != null ? depth0.title_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "	<form id=\"customerTitleForm\" method=\"post\">\n		<table width=\"100%\" style=\"margin:5px 5px 5px 5px;\" align=\"center\">\n			<tr>\n				<td align=\"left\" width=\"24%\">\n					<select id=\"title\" name=\"title\" style=\"width:100%;\" data-placeholder=\"Title...\"> \n						<option value=\"\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titleList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n				</td>\n				<td width=\"10%\" align=\"right\">Customer:</td>\n				<td  align=\"left\" width=\"48%\">\n					<select id=\"customer\" name=\"customer\" style=\"width:350px;\" multiple=\"multiple\"> \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customerList : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>			\n				</td>				\n				<td width=\"6.4%\" align=\"right\" valign=\"center\">\n					<div style=\"position:relative;left:2px;padding-bottom:3px;\"> \n						<a id=\"addButton\" href=\"javascript:void(0)\" style=\"margin-top:6px;\"  class=\"buttons icon add\" style=\"background-image:none;\"></a> \n					</div>\n				</td>\n				\n				<td width=\"7%\" align=\"right\" valign=\"center\">\n                    <a href=\"javascript:void(0)\" class=\"buttons cancel\" style=\"display:none;height:25px;width:36px;\" title=\"Cancel\"><i class=\"fa fa-minus-circle\" style=\"font-size:12px;\"></i></a>\n				</td>\n				<td width=\"18\"></td>\n			</tr>\n		</table>\n	</form>\n	";
},"useData":true});
templates['product_title_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "				<tr height=\"40px\"  class=\"record\">\n					<td align=\"left\" width=\"25.4%\" style=\"padding-left:5px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.name : stack1), depth0))
    + "</td>\n					<td  align=\"left\" class=\"customer\" width=\"59%\" style=\"padding-left:5px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</td>\n					<td align=\"center\" valign=\"center\"> \n						\n	                    <a title=\"Edit\"  class=\"buttons icon edit\" name=\"mod\" href=\"javascript:void(0)\"  data-titleId=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"></a>\n	                    <a title=\"Delete\"  class=\"buttons icon remove\" name=\"del\" href=\"javascript:void(0)\"   data-titleId=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.title : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"></a>					\n					</td>\n				</tr>				\n";
},"2":function(depth0,helpers,partials,data) {
    var helper;

  return "							"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + ",\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "		<table width=\"100%\" class=\"zebraTable\"  cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</table>";
},"useData":true});
return templates;
});