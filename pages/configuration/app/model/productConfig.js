define(['backbone','../config'], function (Backbone,page_config) {

    //生产商
    var Title = Backbone.Model.extend({
    	default : {
			id : 0,
			name : ""
    	}
    });  
    
    //品牌商
    var Customer = Backbone.Model.extend({
    	default : {
			id : 0,
			name : ""
    	}
    });        

    //生产商集合
    var CustomerCollection = Backbone.Collection.extend({model:Customer});       
    
    //品牌商生产商关系模型
    var CustomerTitle = Backbone.Model.extend({
    	url:page_config.productCustomer,
    	default : {
    		productId : 0,
    		title : {
    			"id" : 0,
    			"name" : ""
    		},
    		customers : []
    	}
    });


    
    //品牌商集合
    var TitleCollection = Backbone.Collection.extend(
    	{
	        url: page_config.productCustomer,
	        model: CustomerTitle
    	}
    );


    return {
    	Customer:Customer,
    	Title:Title,
    	TitleCollection:TitleCollection,   	
    	CustomerTitle:CustomerTitle,
    	CustomerCollection:CustomerCollection
    };    
});