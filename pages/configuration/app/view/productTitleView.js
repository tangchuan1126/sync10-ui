/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/productConfig",
    "../config",
    "../role",
    "jqueryui/tooltip",
    "artDialog",
    "showMessage",
    "select2"
],function($,Backbone,Handlebars,templates,models,UrlConfig,Role){

	
    var customerList;
    var titleList;
    var temp ;
    var mainModel = models;

	
    return Backbone.View.extend({
    	template : templates.product_title,
        initialize:function(options){
            this.setElement(options.el);
            this.collection = new mainModel.CustomerCollection;
        	$.ajax({
	       		 type : "get",
	       		 url  : "/Sync10/basicdata/customer/all",
	       		 async : false,
	       		 success : function(data){
	       			 customerList = data;
//	       			 for(var i = 0; i < data.length; i++){
//	       				 $("#customer").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
//	       			 }
	       		 }
       	});

	       	$.ajax({
	       		 type : "get",
	       		 url  : "/Sync10/basicdata/title/getAll",
	       		 async : false,
	       		 success : function(data){
	       			 titleList = data;
//	       			 for(var i = 0; i < data.length; i++){
//	       				 $("#title").append("<option value='" + data[i].title_id + "'>" + data[i].title_name + "</option>");
//	       			 }	       			 
	       		 }
	       	});	   	            
            temp = this;
        },
        setResultView : function(view){
        	this.resultView = view;
        },
        setRole : function(role){
        	this.role = role;
        },
        setProductId : function(id){
        	this.productId = id;
        },
        setTitleId : function(id){
        	this.titleId = id;
        },
        setCustomerId : function(id){
        	this.customerId = id;
        },        
        events:{
        	"click #addButton":"add",
        	"click a.cancel":"cancel",
        	"change #title" : "change"
        }, 
        validate : function(isUpdate){
        	var customerId = $("#customer").val();
        	var titleId = $("#title").val();
        	var count = 0;
        	if($.trim(titleId).length == 0){
        		$("#title").next().find("ul").addClass("error");
        		showMessage("Please select title","alert");
        		count++;
        	}else{
        		if(!isUpdate){
        			var url = UrlConfig.productCustomer + "/" + temp.productId + "/" + titleId + "/exist";
        			if(temp.role == Role.Customer){
        				url = UrlConfig.productCustomer + "/" + temp.productId + "/" + titleId + "/" + customerId + "/exist";
        			}
        			
                   	$.ajax({
	   	   	       		 type : "get",
	   	   	       		 url  : url,
	   	   	       		 async : false,
	   	   	       		 success : function(result){
	   	   	       			 if(!result.success){
	   	   	       				$("#title").addClass("error");
	   	   	       				showMessage("Title exists","alert");
	   	   	       				count++;
	   	   	       			 }else{
	   	   	       				 $("#title").removeClass("error");
	   	   	       			 }
	   	   	       		 }
                   	});	 
        		}
        	}
        	if(count == 0 && $.trim(customerId).length == 0){
        		$("#customer").next().find("ul").addClass("error");
        		showMessage("Please select customer","alert");
        		count++;
        	}else{
        		$("#customer").next().find("ul").removeClass("error");
        	} 
        	return (count == 0);
        },
        render : function(){
        	this.$el.html(this.template({customerList:customerList,titleList:titleList}));
        	if(temp.role == Role.Customer){
        		$("#customer").removeAttr("multiple");
        		$("#customer").val(temp.customerId);
        		$("#customer").attr("disabled","true");
        	}
    		$("#customer").select2();
			$("#title").select2({
				placeholder: "Select...",
				allowClear: true
			});
        	$("#customer_title_result,#customer_title_head,#main").show();
        	$("#addButton").attr("title","Add");
//        	$("div").show();  
//        	$("div.select2-drop").hide();
        },
        "add":function(){

        	if(temp.validate(false)){
            	var customerId = $("#customer").val() + "";
            	var titleId = $("#title").val() + "";
            	var customer_array = customerId.split(",");
            	var customerCollection = new mainModel.CustomerCollection;
            	if(customer_array.length > 0){
            		for(var i=0;i < customer_array.length;i++){
            			var customer_model = new mainModel.Customer;
            			customer_model.set("id",parseInt($.trim(customer_array[i])));
            			customer_model.set("name", $("#customer option[value='" + $.trim(customer_array[i]) + "']").text() );
            			customerCollection.add(customer_model);
            		}
            	}        		
        		var model = new mainModel.CustomerTitle;
                model.save({
                    "title":{"id":titleId},
                    "productId": temp.productId,
                    "customers" : customerCollection.toJSON()
                },{
                    success: function (data) {
                        if(data.get("success")){
                        	$.ajax({
                    			 type : "get",
                    			 url  : UrlConfig.updateIndex + temp.productId,
                    			 success : function(data){
                    			 }  
                        	});
                        	
//                        	var titleName = $("#title option:selected").text();
//                        	var titleModel = new mainModel.Title({id:titleId,name:titleName});
//                        
//                        	var customerTitle  = new mainModel.CustomerTitle;
//                        	customerTitle.set("productId",temp.productId);
//                        	customerTitle.set("title",titleModel.toJSON());
//                        	customerTitle.set("customers",customerCollection.toJSON());
//                        	
//                        	temp.resultView.collection.add(customerTitle);
//                        	temp.resultView.render();
                        	
                            showMessage("Success","succeed");
                                                       
                            temp.render();  
                        	temp.resultView.initData();
                        	temp.resultView.render();
                        }else{
                        	showMessage("Failure","error");
                        }
                    },
                    error: function(model, response, options){
                    	showMessage("Failure","error");
                    }
                });
        		
        	}
        	return false;
        },
        "update":function(){
        	var customerId = $("#customer").val() + "";
        	var titleId = $("#title").val() + "";
        	var customer_array = customerId.split(",");
        	var customerCollection = new mainModel.CustomerCollection;
        	if(customer_array.length > 0){
        		for(var i=0;i < customer_array.length;i++){
        			var customer_model = new mainModel.Customer;
        			customer_model.set("id",parseInt($.trim(customer_array[i])));
        			customer_model.set("name", $("#customer option[value='" + $.trim(customer_array[i]) + "']").text() );
        			customerCollection.add(customer_model);
        		}
        	}
        	if(temp.validate(true)){
    			var data = {
                    "title":{"id":titleId},
                    "productId": temp.productId,
                    "customers" : customerCollection.toJSON()
                };
    		
	       		 $.ajax({
	      		   type: "PUT",
	      		   url: UrlConfig.productCustomer + "/" + temp.productId + "/" + titleId,
	      		   data:JSON.stringify(data),
	      		   contentType:"application/json",
                   success: function (data) {
                       if(data.success){
	                       	$.ajax({
	                   			 type : "get",
	                   			 url  : UrlConfig.updateIndex + temp.productId,
	                   			 success : function(data){
	                   			 }                        			
	                       	});  	   
//	                       	var titleName = $("#title option:selected").text();
//	                       	var titleModel = new mainModel.Title({id:titleId,name:titleName});
//	                       	var customerTitle  = new mainModel.CustomerTitle;
//	                       	customerTitle.set("productId",temp.productId);
//	                       	customerTitle.set("title",titleModel.toJSON());
//	                       	customerTitle.set("customers",customerCollection.toJSON());
//	                       	
//	                    	var model_array = temp.resultView.collection.models;
//	                    	for(var i=0;i<model_array.length; i++){
//	                    		if(model_array[i].toJSON().title.id == titleId){
//	                    			//修改原有记录
//	                    			model_array[i].set("customers",customerTitle.get("customers"));	                    			
//	                    			break;
//	                    		}
//	                    	}  
	                   		
	               			
	                    	temp.render(); 
    	            		temp.resultView.initData();
    	            		temp.resultView.render();
    	            		/*
	    	            	if($("tr.record").size() == 1){
	    	            		temp.resultView.initData();
	    	            		temp.resultView.render();
	    	            	}else{
	    	            		$("a.edit,a.remove").removeAttr("edit");
		    	       			//$("a.edit,a.remove").unbind();
		    	       			$("a.edit").bind("click",temp.resultView.edit);
		    	       			$("a.remove").bind("click",temp.resultView.remove);	    	            		
	    	            	}
	    	            	*/
    	            		$("tr.over").removeClass("over");
    	                	$("tr.record").hover(
    	                			function(){
    	                				$(this).addClass("over");
    	        	        		},function(){
    	        	        			$(this).removeClass("over");
    	        	        		}
    	                	);	                       	
	                        showMessage("Success","succeed");
	                        
	                        
                       }else{
                    	   if(data.errors.data_error){
                    		   if(data.errors.data_error == "NOT_ALLOWED"){
                    			   var invalid_customers = data.errors.data;
                    			   var temp_str = "";
                    			   for(var i = 0; i < invalid_customers.length; i++){
                    				   if(i > 0){
                    					   temp_str += " ";
                    				   }
                    				   temp_str += invalid_customers[i].name;
                    			   }
                    			    var content = "<div style='width:300px;font-size:14px;'>Can't update,please delete SOP and CLP Type Configuration with this Title and Customer<br>[" + temp_str + "] first.</div>";
                    	            art.dialog({
                    	                title:''
                    	                ,lock: true
                    	                ,opacity:0.3
                    	                ,init:function(){
                    	                    this.content(content);
                    	                }
                    	                ,icon:'warning'
                    	                ,okVal:'Sure'
                    	            });                    		   
                    		   
                    		   }else{
                    			   showMessage(data.errors.data_error,"alert"); 
                    		   }
                    		   
                    		   
                    	   }else{
                    		   showMessage("Failure","error");
                    	   }
                    	   
                       }
                   },
                   error: function(model, response, options){
	                   	showMessage("Failure","error");
                   }			        		   
		      	 });            		
        	}
        	return false;
        },
        "cancel" :function(event){
        	$("a.cancel").hide();
        	temp.render(); 
        	if($("tr.record").size() == 1){
        		temp.resultView.initData();
        		temp.resultView.render();
        	}else{
       			$("a.edit,a.remove").removeAttr("edit");
       			$("a.edit").bind("click",temp.resultView.edit);
       			$("a.remove").bind("click",temp.resultView.remove);           		
        	}

   			$("#addButton").attr("title","Add").removeClass("approve").addClass("add");
   			$("tr.over").removeClass("over");
        	$("tr.record").hover(
        			function(){
        				$(this).addClass("over");
	        		},function(){
	        			$(this).removeClass("over");
	        		}
        	);
        },
        "change" : function(event){
        	var titleId = $("#title").val();
        	var url = "";
        	if(temp.role == Role.Customer){
            	if($.trim(titleId).length == 0){
            		url = UrlConfig.productCustomer + "/" + this.productId + "/customer/" + this.customerId;
            	}else{
            		url = UrlConfig.productCustomer + "/" + this.productId + "/title/" + titleId + "/customer/" + this.customerId; 
            	}        		
        	}else{
            	if($.trim(titleId).length == 0){
            		url = UrlConfig.productCustomer + "/" + this.productId;
            	}else{
            		url = UrlConfig.productCustomer + "/" + this.productId + "/title/" + titleId; 
            	}        		
        	}

        	$.ajax({
	       		 type : "get",
	       		 url  : url ,
	       		 async : false,
	       		 success : function(data){
	       			 temp.resultView.collection.reset();
	       			 if(data.length > 0){
		       			 for(var i=0;i<data.length;i++  ){
		       				 var customerTitle = new models.CustomerTitle({title:data[i].title,customers:data[i].customers,productId:temp.productId});
		       				 temp.resultView.collection.add(customerTitle);
		       			 }	       				 
	       			 }
	       			temp.resultView.render();
	       		 }
        	});            	
        	
        }
    });
});
