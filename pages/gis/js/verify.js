 function postVerify(data, url, obj, conblck) {
   var v_flag;
   $.ajax({
     url: url,
     data: data,
     dataType: 'json',
     type: 'post',
     //async: false,
     beforeSend: function(request) {},
     success: function(data) {

       //if(conblck && data.flag == "true"){
       conblck(data.flag, obj);
       //}

       // if (data.flag == "true") {
       //   verifyMgs['verifyName'] = 1;
       // } else {
       //  verifyMgs['verifyName'] = 0;
       //  showErrorVerifyMsg(obj, 'Name can not repeat!');
       // }
     },
     error: function() {

       verifyMgs['verifyName'] = 2;
       showErrorVerifyMsg(obj, 'Name can not repeat!');
     }
   });
   //debug
   /* console.log("ajax.return");
   return v_flag;*/
 }
 /**
  *psId,type,oldName_为空时只验证name不为空
  */
 var verifyMgs = {};

 function serverval(reval, _OBJ) {

   if (reval == "true") {

     //服务端通过
     verifyMgs['verifyName'] = 1;


   } else {

     //服务端存在不通过
     verifyMgs['verifyName'] = 0;
     showErrorVerifyMsg(_OBJ, 'Name can not repeat!');

   }


 }

 function verifyName(obj1, psId, type, oldName_, op_flag) {

   //客服端判断
   var obj = $(obj1);
   var name;
   if (obj.length)
     name = $.trim(obj.val());

   if (name != "" && typeof name != "undefined" && !/#/g.test(name)) {

     showRightVerifyMsg(obj);
     verifyMgs['verifyName'] = 1;

   } else {

     showErrorVerifyMsg(obj, 'Name is empty!');
     verifyMgs['verifyName'] = 0;
     return;

   }

   
   //服务端判断
   if (oldName_ || op_flag) {
     if (oldName_ != name) {
       var data = {
         "ps_id": psId,
         "name": name,
         "type": type
       };

       var url = systenFolder + 'action/administrator/gis/verifyName.action';
       postVerify(data, url, obj, serverval);
     } else {
       showRightVerifyMsg(obj);
       verifyMgs['verifyName'] = 1;
     }

   } else {
     verifyMgs['verifyName'] = 1;
   }




 }

 function verifyNumber(obj, data_flag) {
   var x_val;
   if (obj.length) {
     x_val = obj[0].value;
   } else {
     x_val = obj.value;
   }
   var reg = /^-?\d+(\.\d+)?$/;
   if (x_val) {
     if (reg.test(x_val)) {
       showRightVerifyMsg(obj);
       verifyMgs[data_flag] = 1;
     } else {
       showErrorVerifyMsg(obj, 'Is not num!');
       verifyMgs[data_flag] = 0;
     }
   } else {
     showErrorVerifyMsg(obj, 'Is empty!');
     verifyMgs[data_flag] = 0;
   }

 }

 function verifyIP(obj) {
   var regIp = new RegExp("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
   var ip;
   if (obj.length) {
     ip = obj[0].value;
   } else {
     ip = obj.value;
   }
   if (regIp.test(ip)) {
     showRightVerifyMsg(obj);
     verifyMgs['verifyIP'] = 1;
   } else {
     showErrorVerifyMsg(obj, 'Invalid format!');
     verifyMgs['verifyIP'] = 0;
   }
 }

 function verifyLatLng(obj, data_flag) {
   var val_;
   if (obj.length) {
     val_ = obj[0].value;
   } else {
     val_ = obj.value;
   }
   if (val_) {
     var reg = /^-?\d+(\.\d+(,-?\d+(\.\d+)))?$/;
     if (reg.test(val_)) {
       showRightVerifyMsg(obj);
       verifyMgs[data_flag] = 1;
     } else {
       showErrorVerifyMsg(obj, 'Invalid format!');
       verifyMgs[data_flag] = 0;
     }
   } else {
     showErrorVerifyMsg(obj, 'Position is empty!');
     verifyMgs[data_flag] = 0;
   }

 }