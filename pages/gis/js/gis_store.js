
//3D类
var _Store = null;

//3D对象实例
var store = null;

requirejs.config({
    paths: {
        "_3d":"./js/3d_store",
        "css":"/Sync10-ui/lib/threejs/css/object3d"
    },
    shim: {
    	"_3d" : {
        	deps:['require_css!css']
        }
    }
});

define(['_3d','three'/*,'css3drenderer'*/], function(_3d){
	_Store = _3d;
});

function test_3d(){
	resize3dStore();
	$("#store_3d").empty();
	if(store){
		store = null;
		$("#store_3d").hide();
	}else{
		$("#store_3d").show();
		creatStoreInfo(test_data);
		store = new _Store({
			el_id : "store_3d",
			store_info : test_data
		});
	}
}

//3d库存div大小,设置为与map完全重合
function resize3dStore(){
	var map=$("#jsmap");
	var _3d=$("#store_3d");
	if(_3d.css("display") != "none"){
		_3d.offset(map.offset());
		_3d.width(map.width());
		_3d.height(map.height());
	}
}


//包装container数据
function creatStoreInfo(location_tree){
	var cons = [];
	var cs = location_tree.containers;
	if(cs){
		for(var i=0; i<cs.length; i++){
			var html = creatContainerHtml(cs[i]);
			cs[i].html = html;
		}
	}
}
//拼container
function creatContainerHtml(con){
	var pro = con.products[0];
	var html = "<div id='con_"+con.con_id+"' class='element'>" +
				"	<div class='top_title'>"+
				"		<div class='con_type'>"+ con.container_type +"</div>"+
				"		<div class='title'>"+con.title_id+"</div>"+
				"	</div>"+
				"	<h1>"+con.container+"</h1>" +
				"	<div class='uls'>"+
				"		<ul>" +
				"		<li><span>PDT : "+pro.p_name+"</span><p>"+(pro.quantity - pro.locked_quantity)+"/"+pro.quantity+"</p></li>" +
				"		<li><span>PDT Line : "+pro.product_line+"</span></li>" +
				"		<li><span>Lot : "+con.lot_number+"</span></li>" +
				"</div>";
	return html;
}

var test_data = {
	"containers" : [ {
		"con_id" : 2001006,
		"container" : 2001006,
		"container_type" : 1,
		"is_full" : 3,
		"is_has_sn" : 1,
		"lot_number" : "lot1004",
		"products" : [ {
			"catalogs" : [ 354, 392 ],
			"locked_quantity" : 0,
			"p_name" : "SE26HQ04",
			"pc_id" : 1003006,
			"product_line" : 1000017,
			"quantity" : 100,
			"sn" : [],
			"title_id" : 16,
			"total_locked_quantity" : 0,
			"total_quantity" : 100,
			"union_flag" : 0
		} ],
		"title_id" : 16,
		"type_id" : 71
	}, {
		"con_id" : 2001007,
		"container" : 2001007,
		"container_type" : 1,
		"is_full" : 3,
		"is_has_sn" : 1,
		"lot_number" : "lot1004",
		"products" : [ {
			"catalogs" : [ 354, 392 ],
			"locked_quantity" : 0,
			"p_name" : "SE26HQ04",
			"pc_id" : 1003006,
			"product_line" : 1000017,
			"quantity" : 100,
			"sn" : [],
			"title_id" : 16,
			"total_locked_quantity" : 0,
			"total_quantity" : 100,
			"union_flag" : 0
		} ],
		"title_id" : 16,
		"type_id" : 71
	}, {
		"children" : [ {
			"con_id" : 2001104,
			"container" : 2001104,
			"container_type" : 1,
			"is_full" : 3,
			"is_has_sn" : 1,
			"lot_number" : "lot007",
			"products" : [ {
				"catalogs" : [ 354, 356 ],
				"locked_quantity" : 0,
				"p_name" : "WD32HB1120",
				"pc_id" : 1003039,
				"product_line" : 1000017,
				"quantity" : 100,
				"sn" : [],
				"title_id" : 16,
				"total_locked_quantity" : 0,
				"total_quantity" : 100,
				"union_flag" : 0
			} ],
			"title_id" : 16,
			"type_id" : 78
		} ],
		"con_id" : 2002502,
		"container" : 2002502,
		"container_type" : 3,
		"is_full" : 3,
		"is_has_sn" : 1,
		"lot_number" : "lot007",
		"products" : [ {
			"catalogs" : [ 354, 356 ],
			"locked_quantity" : 0,
			"p_name" : "WD32HB1120",
			"pc_id" : 1003039,
			"product_line" : 1000017,
			"quantity" : 100,
			"sn" : [],
			"title_id" : 16,
			"total_locked_quantity" : 0,
			"total_quantity" : 200,
			"union_flag" : 0
		} ],
		"title_id" : 16,
		"type_id" : 8
	} ],
	"is_three_dimensional" : 0,
	"ps_id" : 1000005,
	"slc_area" : 1000225,
	"slc_id" : 1005116,
	"slc_position" : "L003",
	"slc_type" : 1
};