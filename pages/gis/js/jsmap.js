//-----------------
//
var MARKER_ANIMATION = 0; // 默认marker动画
var DEFAULT_CENTER = { lat:39.896252, lon:-102.099611 };  //默认地图中心
var DEFAULT_ZOOM = 4;  //默认缩放等级
//var COLOR_LEVEL = { from:0xFFFFAA, to:0xFF6633, stroke:"#FFFFFF" };   //默认颜色渐变(16进制整数)   浅黄-->橘红
//var COLOR_LEVEL = { from:0x66ffff, to:0x3333ff, stroke:"#FFFFFF" };   //默认颜色渐变(16进制整数)  浅绿-->深蓝
//var COLOR_LEVEL = { from:0xffcccc, to:0xcc3333, stroke:"#FFFFFF" };   //默认颜色渐变(16进制整数)  浅绿-->深蓝
var COLOR_LEVEL = { from:0xcccccc, to:0x444444, stroke:"#FFFFFF" };   //默认颜色渐变(16进制整数)  浅绿-->深蓝
var HIGH_LIGHT_COLOR = "#880000";  //高亮颜色
var BOUNDARY_TYPE = {"COUNTRY":1,"PROVINCE":2};    //行政边界类型
var systenFolder ="/Sync10/";
//仓库停车位、门占用状态颜色

/** obj_status
 * 数据库定义：
 *        parking     dock
 *  空闲		1			3
 *  保留		-			1
 *  占用		2			2
 * 	
 */
/**rel_type 定义:
 *          parking     docks
 * delivery  1            1
 * pick up   2            2
 */
/**child_status 定义:
 *          parking     docks
 * open      0            0
 * close     1            1
 */
//仓库停车位、门占用状态颜色
var PACKING_OCCU_COLOR = {
		/*
		"-1": "#cee6d8", //浅灰绿
		"0" : "#93FBBF", //绿色 
		"1" : "#91dafe", //浅蓝
		"2" : "#3399ff", //  ↓
		"3" : "#1b4bfd", //  ↓
		"4" : "#02208f", //深蓝
		"5" : "#b3d465", //草绿
		"12": "#b3d465", //草绿
		"6" : "#BB0000", //深红
		"7" : "#FF0000", //  ↓
		"8" : "#FF7777", //  ↓
		"9" : "#FFDDDD", //浅红
		"10": "#fff799", //淡黄
		"13": "#fff799", //淡黄
		"11": "#888888"	 //灰色
			*/
		//           门或者停车位占用状态                 D/P : 1/2      open/close : 0/1
		//colorKey =obj.obj_status+ rel_type+  child_status;//
		    "-1": "#cee6d8", //浅灰绿
		   // "000": "#888888",	 //灰色
		    "100" : "#FF3300", //红色
		    "200" : "#0033FF",//蓝色
		    "400" : "#666699",//紫色
			"500" : "#93FBBF", //绿色 
			"600" : "#FF99FF" //绿色 
			/*"200" : "#1b4bfd", // /浅蓝 pick up   close ↓ 
			"220" : "#7EDFF7", // /浅蓝 pick up   open ↓ 
			"211" : "#BB0000", //  ↓delivery open 
			"210" : "#FF7777", //深红 delivery close
			"121" : "#1b4bfd", // /浅蓝 pick up   open ↓ 
			"120" : "#7EDFF7", //浅蓝 pick up   close
			"111" : "#BB0000", //  ↓delivery open 备用
			"110" : "#FF7777" //深红 delivery close */	
}
//仓库停车位、门占用状态名称
var PACKING_OCCU_STATUS = {
		/*
		"-1": "Not Available",
		"0" : "Free",
		"1" : "Pick up:Open",
		"2" : "Pick up:Processing",
		"3" : "Pick up:Closed InYard",
		"4" : "Pick up:Closed Leaving",
		"12" : "Pick up:Trailer InYard",
		"6" : "Delivery:Open",
		"7" : "Delivery:Processing",
		"8" : "Delivery:Closed InYard",
		"9" : "Delivery:Closed Leaving",
		"13" : "Delivery:Trailer InYard",
		"11": "None"
			*/
		"-1": "Broken",
		"100" : "InBound",//hong色
		"200" : "OutBound",//蓝色
		"400" : "Others",//紫色
		"500":"Available",
		"600":"Reserve",
				//"220" : "Pick&nbsp&nbspup:Open",
				//"221" : "Pick&nbsp&nbspup:Close",
				//"22" : "Pick up:Processing",
				//"24" : "Pick up:Entry Closed Leaving",
				//"12" : "Delivery:Processing",
				//"210" : "Delivery:Open",
				//"211" : "Delivery:Close",
				//"14" : "Delivery:Entry Closed Leaving",
				//"000": "None",
}
//仓库位置类型
var STORAGE_POSITION_TYPE ={
		"LOCATION" : 1,
		"STAGING" : 2,
		"DOCKS" : 3,
		"PARKING" : 4,
		"AREA" : 5,
		"WEBCAM" : 6,
		"PRINTER" : 7,
		"ROAD" : 8,
		"LIGHT" : 9,
		"ROAD_POINT" : 10,
}
//仓库位置颜色
var STORAGE_POSITION_COLOR = {
		"BASE" : {"strokeColor" : "#AAAAAA", "strokeWeight" : 3, "strokeOpacity" : 1, "fillColor" : "#AAAAAA", "fillOpacity" : 1},
		"LOCATION" : {"strokeColor" : "#795046", "strokeWeight" : 2, "strokeOpacity" : 1, "fillColor" : "#B8FA7D", "fillOpacity" : 1},
		"LOCATION3D" : {"strokeColor" : "#795046", "strokeWeight" : 2, "strokeOpacity" : 1, "fillColor" : "#87CEEB", "fillOpacity" : 1},
		"LOCATION2D" : {"strokeColor" : "#795046", "strokeWeight" : 2, "strokeOpacity" : 1, "fillColor" : "#EEDC82", "fillOpacity" : 1},
		"STAGING" : {"strokeColor" : "#966E0F", "strokeWeight" : 1, "strokeOpacity" : 1, "fillColor" : "#FFFFB4", "fillOpacity" : 1},
		"DOCKS" : {"strokeColor" : "#69D46E", "strokeWeight" : 2, "strokeOpacity" : 1, "fillColor" : "#93FBBF", "fillOpacity" : 1},
		"PARKING" : {"strokeColor" : "#69D46E", "strokeWeight" : 2, "strokeOpacity" : 1, "fillColor" : "#93FBBF", "fillOpacity" : 1},
		"AREA" : {"strokeColor" : "#AAAAAA", "strokeWeight" : 2, "strokeOpacity" : 1, "fillColor" : "#AAAAAA", "fillOpacity" : 1},
		"WEBCAM" : {"strokeColor" : "#F88F87", "strokeWeight" : 1, "strokeOpacity" : 0.2, "fillColor" : "#F88F87", "fillOpacity" : 0.2}
}

var GRADS_COLOR = ["#fdfc58","#e3cc07","#fdcb00","#e67c00","#ff7e7e","#e60000","#a80505","#9000ff","#560495","#010897"];  //需求分布等颜色梯度
var ICON_DIR = 'imgs/' ;   //ICON图片目录

//-------------------
var MAP_ID = "jsmap";  //map div id
var MAP_ZOOM = "map_zoom"; //地图层级div
var MOUSE_POSITION = "mouse_position";  //鼠标位置div
var jsmap = null;
var jsmapElem = null; 
var pps = [];
var replaySpeed = 5;   //历史记录回放速度，默认5points/s
var replayPause = false;  //暂停回放
/**
 * initialization map
 */
function jsMapInit(simpleMap){  //simpleMap 无任何组件的地图，默认为false
    if (jsmap == null) {
        jsmapElem = document.getElementById(MAP_ID);
        if (jsmapElem != null) {
            try {
                jsmap = new JSMap(jsmapElem,simpleMap);
                if (jsmap) {
                } else {
                    alert("[jsMapInit]: " + MAP_PROVIDER_NAME + "\n" + 
                        "Error occured while creating JSMap" +
                        "(map provider service may be temporarily unavailable)");
                }
            } catch (e) {
                alert(
                    "[jsMapInit]: " + MAP_PROVIDER_NAME + "\n" + 
                    "Error initializing map\n" +
                    "(map provider service may be temporarily unavailable)\n" +
                    e
                    );
            }
        } else {
            alert(
                "[jsMapInit]: " + MAP_PROVIDER_NAME + "\n" + 
                "Div '" + MAP_ID + "' not found"
                );
        }
    }
}
/*
 * //初始化MapTool
function initMapTool(){
	if($("#"+MAP_ZOOM)){
		$("#"+MAP_ZOOM).html("地图层级:" +jsmap.zoom);
	}
	if($("#"+MOUSE_POSITION)){
		$("#"+MOUSE_POSITION).html("经纬度:"+DEFAULT_CENTER.lon+","+DEFAULT_CENTER.lat);
	}
}
*/
/**
 * JSMapPushpin
 */
function JSMapPushpin(lat, lon, data, icon){

    this.lat         = lat;
    this.lon         = lon;
    this.position    = jsNewLatLng(lat,lon);
    

    this.data        = data;
    this.html        = null;    // create from data

    this.icon		 = icon;  //string|MarkerImage
    this.marker      = null;  //for InfoWindow,will be set later if need show a InfoWindow
    this.hoverPopup  = false;
    this.popup       = null;
    this.popupShown  = false;

}

JSMapPushpin.prototype.getHtml = function(){
	if(this.html == null){
		var htmlTemp = "<div>infoWindow<br/>" +
				"lat:"+this.lat+"<br/>"+
				"lng:"+this.lon+"<br/>"+
				"...</div>";
		//待补充
		
		
		this.html = htmlTemp;
	}
	return this.html;
};

function getHeadingMarkerIconURL(color,frPosition,toPosition){
	var index = null;
	var url = "";
	if(frPosition && toPosition){
		var heading = computeHeading(frPosition,toPosition);
		heading = heading<0 ? heading+360 : heading;
		index = parseInt((heading + 22.5)/45);
		index = (index != 8 ? index:0);
		url = ICON_DIR+"car_"+color+"_"+index+".png";
	}else{
		url = ICON_DIR+"car_"+color+"_0.png";
	}
	return url;
}
function getHeadingMarkerIconURL2(color,rad){
	var url = "";
	var index = parseInt((rad + 22.5)/45);
	index = (index != 8 ? index:0);
	url = ICON_DIR+"car_"+color+"_"+index+".png";
	return url;
}
//历史轨迹播放    all: true从头开始,false继续播放
function historyPlay(all){ 
	var flag = 0;
	if(all){
		if(pps.length>0){
			rePlay(all);
			flag=1;
		}
	}else{
		rePlay(all);
	}
	pps = [];
	return flag;
}
//停止播放历史轨迹
function stopReplay(){
	jsmap.replayIndex = 0;
}
//清除播放数据
function clearHistory(){
	jsmap.clearHistory();
}
var replayCount = 0; //总记录数
function rePlay(all){
	if(all){
		initFloatWindow($("#replayCtrl").html());
		replayCount = jsmap.drawPushpins(pps,false);
		jsmap.replayIndex = 0;
	}
	showCurrentMarker();
}
function showCurrentMarker(){
	var time = parseInt(1000/replaySpeed);
	setTimeout(function(){
		if(!replayPause){
			var hasNext = jsmap.showCurrentMarker();  //this function will return false when all markers was shown
			jsmap.drawPolyline(jsmap.latLngList.slice(0,jsmap.replayIndex+1));
			if(setReplayProcess){
				setReplayProcess(replayCount,jsmap.replayIndex);
			}
			if(hasNext){
				jsmap.replayIndex++;
				showCurrentMarker();
			}
		}
	},time);
}
//调整历史轨迹视口
function fitHistoryBounds(force){
	jsmap.fitHistoryBounds(force);
}
function showOrHideHeatmap(){
	if(jsmap.showOrHideHeatmap()){
		jsmap.hideAllMarker();
		jsmap.hidePolyline();
		//jsmap.backToOriginalView();
	}else{
		jsmap.showAllMarker();
		jsmap.showPolyline();
	}
}

//浮动窗
var floatWindow = false;
var initEventX = false;
var initEventY = false;
var floatWinX = false;
var floarWinY = false;
var floatWinW = false;
var floatWinH = false;
var mapWinX = false;
var mapWinY = false;
var mapWinW = false;
var mapWinH = false;
function initFloatWindowMove(e){
	initEventX = e.clientX;
	initEventY = e.clientY;
	
	var map = document.getElementById(MAP_ID);
	mapWinX = map.offsetLeft;
	mapWinY = map.offsetTop;
	mapWinW = map.offsetWidth;
	mapWinH = map.offsetHeight;
	
	//floatWindow = document.getElementById("floatWindow");
	floatWinW = floatWindow.offsetWidth;
	floatWinH = floatWindow.offsetHeight;
	floatWinX = floatWindow.offsetLeft;
	floatWinY = floatWindow.offsetTop;

	document.onmousemove = floatWindowMove;
}
function floatWindowMove(e){
	var newWinX = floatWinX + e.clientX - initEventX;
	var newWinY = floatWinY + e.clientY - initEventY;
	if(mapWinX < newWinX && (mapWinX + mapWinW) >(newWinX + floatWinW)){ //禁止浮动框移出地图
		floatWindow.style.left = newWinX+"px";
	}
	if(mapWinY < newWinY && (mapWinY + mapWinH) >(newWinY + floatWinH)){//禁止浮动框移出地图
		floatWindow.style.top = newWinY+"px";
	}
	document.onmouseup = floatWindowStop;
}
function floatWindowStop(){
	document.onmousemove = null;
	document.onmousedown = null;
}
//初始化浮动窗
function initFloatWindow(infoHtml){
	if(floatWindow){
		$("#floatWindowInfo").html(infoHtml);
		$("#floatWindow").show(); 
		jsmap.hideMapControl("floatWindow");
		//浮动窗位置
		floatWindow.style.top = mapWinY + 90 + 'px'; //距地图上边距90px
		floatWindow.style.left = mapWinX + mapWinW - floatWindow.offsetWidth - 30 +'px';  //距地图右边距20px
		return;
	}
	//map位置及尺寸
	var map = document.getElementById(MAP_ID);
	mapWinX = map.offsetLeft;
	mapWinY = map.offsetTop;
	mapWinW = map.offsetWidth;
	mapWinH = map.offsetHeight;
	//主窗体
	var floatWinDiv = document.createElement('div');
	floatWinDiv.setAttribute("id", "floatWindow");
	floatWinDiv.style.backgroundColor = '#DD0000';
	floatWinDiv.style.position = 'absolute';
	//floatWinDiv.style.width = floatWinW+'px';
	floatWinDiv.style.borderWidth = '1px';
	floatWinDiv.style.borderColor = 'red';
	floatWinDiv.style.padding = '1px';
	//floatWinDiv.style.top = mapWinY + 90 + 'px'; //距地图上边距60px
	//floatWinDiv.style.left = mapWinX + mapWinW - floatWinW - 20 +'px';  //距地图右边距10px
	floatWindow = floatWinDiv;
	//移动条
	var moveBar = document.createElement("div");
	moveBar.setAttribute("id", "floatWindowMoveBar");
	moveBar.setAttribute("align", "right");
	moveBar.setAttribute("onmousedown", "initFloatWindowMove(event)");
	moveBar.setAttribute("onmouseup", "floatWindowStop()");
	moveBar.style.cursor = 'move';
	//最小化按钮
	var minButton = document.createElement("div");
	minButton.style.cursor = 'default';
	minButton.style.width = '13px';
	minButton.style.height = '11px';
	minButton.style.backgroundColor = '#FFFFFF';
	minButton.style.borderColor = '#DDDDDD';
	minButton.style.borderWidth = '1px';
	minButton.style.margin = '1px 1px 2px 1px';
	minButton.setAttribute("onmouseover", "this.style.backgroundColor = '#DDDDDD'");
	minButton.setAttribute("onmouseout", "this.style.backgroundColor = '#FFFFFF'");
	minButton.setAttribute("onclick", "miniFloatWindow(this.parentNode.parentNode)");
	minButton.innerHTML = '<img alt="-" src="'+ICON_DIR+'min_black_small.png"  style="vertical-align: top;">';
	//数据显示区
	var info = document.createElement("div");
	info.setAttribute("id","floatWindowInfo");
	info.style.backgroundColor = '#FFFFFF';
	//info.style.height = '150px';
	info.innerHTML = infoHtml;
	//加载窗体
	moveBar.appendChild(minButton);
	floatWinDiv.appendChild(moveBar);
	floatWinDiv.appendChild(info);
	document.body.appendChild(floatWinDiv);
	//浮动窗位置
	floatWinDiv.style.top = mapWinY + 90 + 'px'; //距地图上边距90px
	floatWinDiv.style.left = mapWinX + mapWinW - floatWindow.offsetWidth - 30 +'px';  //距地图右边距20px
}
//地图大小改变时，重新设置浮动窗位置
function setFloatWindowPosition(){
	if(floatWindow){
		var map = document.getElementById(MAP_ID);
		mapWinX = map.offsetLeft;
		mapWinY = map.offsetTop;
		mapWinW = map.offsetWidth;
		mapWinH = map.offsetHeight;
		var resetX = floatWindow.offsetLeft + floatWindow.offsetWidth + 20 > mapWinX + mapWinW;
		var resetY = floatWindow.offsetTop + floatWindow.offsetHeight + 20 > mapWinY + mapWinH;
		if(resetX){
			var left = mapWinX + mapWinW - floatWindow.offsetWidth - 30;
			if(left < mapWinX){
				left = mapWinX;
			}
			floatWindow.style.left = left +'px';
		}
		if(resetY){
			var top = mapWinY + mapWinH - floatWindow.offsetHeight - 30;
			if(top < mapWinY){
				top = mapWinY;
			}
			floatWindow.style.top = top +'px';
		}
	}
}
//最小化浮动窗
function miniFloatWindow(ele){
	//创建控件
	jsmap.addMapControl("right_top","click",function(){
			ele.style.display = "inline";  
			jsmap.hideMapControl("floatWindow");
		},
		"floatWindow",
		ICON_DIR+"full_screen.png"
	);
	//显示控件
	jsmap.showMapControl("floatWindow");
	//隐藏浮动窗
	ele.style.display = "none";
}
//设置浮动窗内容
function setFloatWindowContent(obj){
	$("#floatWindowInfo").html(obj);
}
//移除浮动窗
function removeFloatWindow(){
	if(floatWindow){
		floatWindow.style.display = "none";
	}
	jsmap.hideMapControl("floatWindow");
}
//添加地图控件
function addMapControl(position,eventType,func,name,img){
	var imgUrl = null;
	if(img){
		imgUrl = ICON_DIR+img;
	}
	jsmap.addMapControl(position,eventType,func,name,imgUrl);
}
//隐藏地图控件
function hideMapControl(name){
	jsmap.hideMapControl(name);
}
//地图工具
function setMapTool(tool){
	jsmap.setMapTool(tool)
}
//绘制围栏等
function drawOnMap(tool,fun){
	jsmap.setMapTool(tool);
	jsmap.mapToolBackFun = fun;
}
//清除地图
function clearMap(){
	jsmap.clearLayers();
}
//实时显示车辆位置
function showCarsInMap(data){
	var markers = jsmap.markerCurrent;
	var labels = jsmap.labelCurrent;
	var infoWindows = jsmap.infoWindowCurrent;
	for(var i=0; i<data.length; i++){
		var aid = data[i].aid;
		var latlng = jsNewLatLng(data[i].lat,data[i].lon);
		var icon = getHeadingMarkerIconURL2("green",data[i].rad);
		var markerImage = jsNewMarkerImege(icon, null, null, jsNewPoint(16, 16), null);
		if(markers["asset_"+aid]){
			setMarkerPosition(markers["asset_"+aid],latlng);
		}else{
			markers["asset_"+aid] = jsNewMarker(latlng,markerImage);
		}
		markers["asset_"+aid].putData(data[i]);
		if(labels["asset_"+aid]){
			labels["asset_"+aid].setPosition(latlng);
		}else{
			labels["asset_"+aid] = new LabelOverlay(latlng,data[i].nam,"bottom",18);
		}
		var asset = $(".truck_third input[name='asset_"+aid+"']");
		var html =  "<div style='width: 200px;color: black;'>"+
					"	<table style='width: 100%'>"+
					"		<tr>"+
					"			<td style='width: 40%' align='right'>Name:</td>"+
					"			<td style='width: 60%'>"+data[i].nam+"</td>"+
					"		</tr>";
		if(asset.data('asset_def') && asset.data('asset_def') != ""){
			html += "		<tr>"+
					"			<td align='right'>Licence plate:</td>"+
					"			<td>"+asset.data('asset_def')+"</td>"+
					"		</tr>";
		}
		html +=		"		<tr>"+
					"			<td align='right'>IMEI:</td>"+
					"			<td>"+asset.data('asset_imei')+"</td>"+
					"		</tr>"+
					"		<tr>"+
					"			<td align='right'>Phone:</td>"+
					"			<td>"+asset.data('asset_callnum')+"</td>"+
					"		</tr>"+
					"		<tr>"+
					"			<td align='right'>Group:</td>"+
					"			<td>"+asset.data('group_name')+"</td>"+
					"		</tr>"+
					"		<tr>"+
					"			<td align='right'>Last time:</td>"+
					"			<td>"+getLocalTimeFormat(data[i].gt)+"</td>"+
					"		</tr>"+
					"	</table>"+
					"</div>";
		if(infoWindows["asset_"+aid]){
			infoWindows["asset_"+aid].setContent(html);
		}else{
			infoWindows["asset_"+aid] = addInfoWindowToAttach(markers["asset_"+aid],html);
		}
		 MapsEvent.addListener(markers["asset_"+aid],"rightclick",function(e){
			var position = jsmap.overlay.getProjection().fromLatLngToContainerPixel(e.latLng);
			var offset = $("#jsmap").offset();
			var posx =parseInt(position.x+offset.left);
			var posy =parseInt(position.y+offset.top);
			var menu=document.getElementById("truckMarkerMenu");
			$("#truckMarkerMenu").data('data',this.data);
			menu.style.display="block";//设置菜单可见   
			menu.style.top=posy+"px";//设置菜单位置为鼠标右击的位置   
			menu.style.left=posx+"px";
		});
	}
}
//获取当前时区的格式化时间
function getLocalTimeFormat(millisecond){
	 millisecond = parseInt(millisecond);
	 var d = new Date(millisecond);
	 var year = d.getFullYear();
	 var month = d.getMonth()+1;
	 var day = d.getDate();
	 var hours = d.getHours();
	 var min = d.getMinutes();
	 var sec = d.getSeconds();
	 month = month > 9 ? month : "0"+month;
	 day = day > 9 ? day : "0"+day;
	 hours = hours > 9 ? hours : "0"+hours;
	 min = min > 9 ? min : "0"+min;
	 sec = sec > 9 ? sec : "0"+sec;
	 var timeStr = year+"-"+month+"-"+day+" "+hours+":"+min+":"+sec;
	 return timeStr;
}
//车辆定位
function getCarMapLocation(t){
	var marker = jsmap.markerCurrent[t.id];
	if(marker){
		var LatLng = marker.position;
	    jsmap.showMarker(marker);
		jsmap.fitBounds(jsNewLatLngBounds().extend(LatLng));
	}
}
/*//加载kml
function loadKml(url,fitBounds,fun){
	jsmap.loadKml(url,fun,null,fitBounds);
}
//移除kml
function unLoadKml(url){
	jsmap.unLoadKml(url);
}
//调整kml视口
function fitKmlBounds(kml){
	if(jsmap.storageObjsBounds[kml]){
		jsmap.fitBounds(jsmap.storageObjsBounds[kml]);
	}
}
*/
//获取kml连接
function getKmlUrl(kml){
	var filename=kml.split(".")[0];
	var href = window.location.href.split("/");
	var url_=document.domain;
	var hostIp=href[2];
	if(url_=='192.168.88.15'){
		hostIp="124.205.21.209";
	}
	var url = href[0]+"//"+hostIp+"/Sync10/_gis/public/getKmlUrl/"+filename;
	return url;
};

var parkingDocksLegend = false;
function showOccupiedLegend(psId,data){
	if(parkingDocksLegend){
		return;
	}
	var html = "<table style='font-family: Arail'>";
	for(var key in PACKING_OCCU_STATUS){
		html += 	"	<tr>";
		html += 	"		<td><div style='width: 45px; height: 10px;margin:0 10px; background-color: "+PACKING_OCCU_COLOR[key]+"'></div></td>";
		html += 	"		<td>"+PACKING_OCCU_STATUS[key]+"</td>";
		html += 	"	</tr>";
	}
	html += 	"</table>";
	initFloatWindow(html);
	parkingDocksLegend = true;
}

//显示库存位置，catalogData：库存信息
function showStorageCatalogLocation(psId, catalogData){
	jsmap.showStorageCatalogLocation(psId, catalogData);
}
//清除门和停车位
function clearStorageObj(layer){
	jsmap.clearStorageObj(layer);
}
//清除围栏、线路、关键点
function clearGeoObj(){
	jsmap.clearGeofencing();
}
//显示围栏和线路、关键点
function displayGeoFencing(obj,id){
	var id = id;
	var show =obj.checked;
	var type = $(obj.parentElement).data("typeen");
	var fens = jsmap.geoFencing;
	var labels = jsmap.geoFencingLabel;
	if(show){//显示
		if(fens[id]){
			fens[id].setMap(jsmap.getMap());
			if(type == "point"){
				jsmap.contains(fens[id].getPosition(),"position") ? null : jsmap.setCenter(fens[id].getPosition());
				if(labels[id+"_label"]){
					labels[id+"_label"].show();
				}else{
					var p = $(obj.parentElement).data("points").split(",");
					var latlng = jsNewLatLng(p[1],p[0]);
					labels[id+"_label"] = new LabelOverlay(latlng,$(obj.parentElement).data("name"),"right");
				}
			}else{
				jsmap.fitBounds(fens[id].getBounds());
			}
		}else{
			var points = $(obj.parentElement).data("points");
			var mapObj = null;
			var label = null;
			if(type == "point"){
				var p = points.split(",");
				var latlng = jsNewLatLng(p[1],p[0]);
				var icon = null;  //ICON_DIR + "";
				mapObj = jsNewMarker(latlng, icon);
				label = new LabelOverlay(latlng,$(obj.parentElement).data("name"),"right");
			}else if(type == "polyline"){
				var ps = points.split(";");
				var path = [];
				for(var i=0; i<ps.length; i++){
					var p = ps[i].split(",");
					var latlng = jsNewLatLng(p[1],p[0]);
					path.push(latlng);
				}
				mapObj = jsNewPolyline(path, "#0000DD", 2, 1);
			}else if(type == "polygon"){
				var ps = points.split(";");
				var path = [];
				for(var i=0; i<ps.length; i++){
					var p = ps[i].split(",");
					var latlng = jsNewLatLng(p[1],p[0]);
					path.push(latlng);
				}
				mapObj = jsNewPolygon(path, "#0000DD", 1, 1, "#0000DD", 0.2);
			}else if(type == "circle"){
				var p = points.split(",");
				var center = jsNewLatLng(p[1],p[0]);
				mapObj = jsNewCircle(center, parseFloat(p[2]), "#0000DD", 1, 1, "#0000DD", 0.2);
			}else if(type == "rect"){
				var p = points.split(",");
				var sw = jsNewLatLng(p[1],p[0]);
				var ne = jsNewLatLng(p[3],p[2]);
				var bounds = jsNewLatLngBounds().extend(sw).extend(ne);
				mapObj = jsNewRectangle(bounds, "#0000DD", 1, 1, "#0000DD", 0.2);
			}
			fens[id] = mapObj;
			if(label != null){
				labels[id+"_label"] = label;
			}
			if(type == "point"){
				jsmap.contains(mapObj.getPosition(),"position") ? null : jsmap.setCenter(mapObj.getPosition());
			}else{
				jsmap.fitBounds(mapObj.getBounds());
			}
		}
	}else{//隐藏
		if(fens[id]){
			fens[id].setMap(null);
			if(type == "point"){
				if(labels[id+"_label"]){
					labels[id+"_label"].hide();
				}
			}
		}
	}
}
//显示分布热图
function showBoundaryInMap(data,fun){
	var maxCount = 0;
	for(var i=0; i<data.length; i++){
		var c = parseInt(data[i].count);
		if(c>maxCount){
			maxCount = c;
		}
	}
	var last = 0;
	var grads = GRADS_COLOR.length;
	var html = "<table>";   //数量梯度图例
	for(var i=0; i<grads; i++){
		var curr = parseInt(maxCount/grads*(i+1));
		html += "<tr>";
		html += "	<td><div style='width: 10px; height: 10px; background-color: "+GRADS_COLOR[i]+"'></div></td>";
		html += "	<td>"+last+"-"+curr+"</td>";
		html += "</tr>";
		last = curr;
	}
	html += "</table>";
	initFloatWindow(html);
	for(var i=0; i<data.length; i++){
		var c = parseInt(data[i].count);
		var index = parseInt(c/maxCount*grads);
		if(index == GRADS_COLOR.length){
			index--;
		}
		data[i].color = GRADS_COLOR[index];
	}
	jsmap.showRegionBoundary(data,fun);
}
//显示road图层
function loadRoadLayer(psId,layer){ //layer：{main：boolean,entery：boolean} 标记分别加载主路和辅路
	jsmap.loadRoadLayer(psId,layer);
}
//清除road图层
function clearRoadLayer(){
	jsmap.clearRoad();
	//jsmap.clearRoutePath();
}
//显示线路规划
function showRoutePath(paths){
	jsmap.drawRoutePath(paths);
}
//清除线路规划
function clearRoutePath(){
	jsmap.clearRoutePath();
}
//添加线路规划标记点
function addRouteMarker(latlng, type){
	jsmap.addRouteMarker(latlng, type);
}
//选择仓库图层时清除地图遮挡
function onSelectLayer(){
	jsmap.clearRegion();	//需求分布
}
//显示坐标系
function showCoordinateSystem(psId,data){
	jsmap.showStorageCoordinateSys(psId,data);
}
//定位仓库时提前加载大数据
function initStorageData(psId){
	//location
	if(jsmap.storageBounds["loc_"+psId]){
		getStorageBoundsAjax(psId,"kml_storage","loc");
	}
	//storage road
	if(!jsmap.storageBounds["road_"+psId]){
		getStorageRoadAjax(psId,{"main":true,"entery":true,"point":true})
	}
}

