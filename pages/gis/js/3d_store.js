(function(root, factory) {
	if (typeof define === 'function' && define.amd) {

		// AMD. Register 
		define(['jquery', 'three' /*, 'css3drenderer', 'stats'*/ ], factory);
	} else if (typeof module !== 'undefined' && module.exports) {
		// CommonJS
		module.exports = factory(require('jquery'), require('stats'), require('three')(root));

	} else {
		// Browser globals
		//root===Window
		root.cret_shelwes = factory(root.$, root.Stats, root.THREE);
	}
}(this, function($) {
	'use strict';

	//货架
	var Shelf = function(option) {

		this.object; //货架threejs对象

		this.thickness = 5; //隔板厚度
		this.base_height = 15; //架子脚高度
		this.height = 150; //层高度	y方向
		this.width = 130; //宽度	x方向
		this.length = 1000; //长度	z方向
		this.floors = 3; //层数

		this.ctn_top = 0; //container堆叠的高度

		this.creatShelf(option);
	}
	Shelf.prototype = {
		creatShelf: function(option) {
			if (!this.validation(option)) {
				return;
			}
			var _this = this;
			this.object = new THREE.Group();


			if (this.floors == 0) { //2D位置
				var plane = creatPlane(this.width, this.length, 1);
				this.object.add(plane);
			} else { //3D位置
				for (var i = 0; i <= this.floors; i++) {
					var plane = creatPlane(this.width, this.length, this.thickness);
					var y = this.base_height + i * this.height;
					plane.position.set(0, y, 0);
					this.object.add(plane);
				}
				this.object.add(creatBrace());
			}
			//创建隔板

			function creatPlane(width, length, thickness) {
				var geometry = new THREE.BoxGeometry(width, thickness, length);
				var material = null;
				if (_this.floors) {
					material = new THREE.MeshLambertMaterial({
						color: 0xffffff,
						map: THREE.ImageUtils.loadTexture("3d_model/texture/pic2.jpg")
					});
				} else {
					//2D用蓝色
					material = new THREE.MeshLambertMaterial({
						color: 0x70B8FF
					});
				}
				var cube = new THREE.Mesh(geometry, material);

				//产生影子
				cube.castShadow = true;
				//接收影子
				cube.receiveShadow = true;
				return cube;
			}
			//创建支架

			function creatBrace() {
				var group = new THREE.Group();
				var height = _this.base_height + _this.floors * _this.height + _this.thickness / 2;
				//主支架
				var pos = [{x: _this.width / 2,y: height / 2,z: _this.length / 2,h: height, w: 5, l: 5}, 
				           {x: -_this.width / 2,y: height / 2,z: _this.length / 2,h: height,w: 5,l: 5}, 
				           {x: _this.width / 2,	y: height / 2,z: -_this.length / 2,h: height,w: 5,l: 5}, 
				           {x: -_this.width / 2,y: height / 2,z: -_this.length / 2,h: height,w: 5,l: 5}];
				var lookat = [null, null, null, null];
				//支架
				var len = Math.sqrt(_this.height * _this.height + _this.width * _this.width);
				var look_height = _this.base_height + len * len / _this.height / 2;

				for (var i = 0; i <= _this.floors; i++) {
					var y_pos = _this.base_height + _this.height / 2 + _this.height * i;
					var y_pos2 = _this.base_height + _this.height * i;

					var y_look = look_height + _this.height * i;
					var x_look = i % 2 == 1 ? _this.width / 2 : -_this.width / 2;
					//斜
					if (i < _this.floors) {
						pos.push({x: 0,	y: y_pos,z: _this.length / 2,h: len,w: 4,l: 4});
						pos.push({x: 0,y: y_pos,z: -_this.length / 2,h: len,w: 4,l: 4});
						lookat.push({x: x_look,y: y_look,z: _this.length / 2});
						lookat.push({x: x_look,y: y_look,z: -_this.length / 2});
					}
					//侧
					pos.push({x: 0,y: y_pos2,z: _this.length / 2,h: 5,w: _this.width,l: 5});
					pos.push({x: 0,y: y_pos2,z: -_this.length / 2,h: 5,w: _this.width,l: 5});
					//正
					pos.push({x: _this.width / 2,y: y_pos2,z: 0,h: 5,w: 5,l: _this.length});
					pos.push({x: -_this.width / 2,y: y_pos2,z: 0,h: 5,w: 5,l: _this.length});

					lookat.push(null, null, null, null);
				}

				for (var i = 0; i < pos.length; i++) {
					var repeat = Math.max(parseInt(height / 100 + 0.5), 1);
					var geometry = new THREE.BoxGeometry(pos[i].w, pos[i].h, pos[i].l);
					var texture = THREE.ImageUtils.loadTexture("3d_model/texture/shelf_brace.jpg");

					//texture.wrapS = texture.wrapT = THREE.RepeatWreapping;
					texture.repeat.set(1, repeat);

					var material = new THREE.MeshLambertMaterial({
						color: 0xffffff,
						map: texture
					});
					var brace = new THREE.Mesh(geometry, material);
					brace.position.set(pos[i].x, pos[i].y, pos[i].z);
					if (lookat[i]) {
						brace.lookAt(lookat[i]);
					}
					//产生影子
					brace.castShadow = true;
					//接收影子
					brace.receiveShadow = true;
					group.add(brace);
				}
				return group;
			}
		},
		//参数验证
		validation: function(option) {
			//必选
			var required = ["floors"];
			//可选
			var optional = ["width", "height", "length", "base_height", "thickness"];

			if (!option) {
				return required.length == 0;
			}

			for (var i = 0; i < required.length; i++) {
				var key = required[i];
				if (option[key] != null && option[key] != undefined) {
					this[key] = option[key];
				} else {
					console.error("Creat Shelf need [" + key + "] parameter");
					return false;
				}
			}
			for (var i = 0; i < optional.length; i++) {
				var key = optional[i];
				if (option[key] != null && option[key] != undefined) {
					this[key] = option[key];
				}
			}
			return true;
		},
		//货架中心坐标
		getCenter: function() {
			var y = this.base_height + this.floors * this.height + this.thickness / 2;
			y = Math.max(y, this.ctn_top);
			return {
				x: 0,
				y: y / 2,
				z: 0
			};
		},
		//获取货架尺寸
		getSize: function() {
			var y = this.base_height + this.floors * this.height + this.thickness / 2;
			y = Math.max(y, this.ctn_top);
			return {
				x: this.width,
				y: y,
				z: this.length
			};
		},
		//计算货架隔板每层高度
		getFloorPosition: function() {
			var data = {};
			data.width = this.width;
			data.length = this.length;
			data.height = [];

			if (this.floors == 0) {
				data.height.push(1);
			} else {
				for (var i = 0; i < this.floors; i++) {
					var h = this.base_height + this.floors * this.height + this.thickness / 2;
					data.height.push(h);
				}
			}
		}
	};

	//容器
	var Container = function(option) {

		this.object; //容器threejs对象

		this.children; //子容器

		this.length = 100; //长度	z方向
		this.width = 120; //宽度	x方向
		this.height = 120; //层高度	y方向

		this.position = {x: 0,y: 0,z: 0}; //位置

		this.fillColor = 0xffffff;	//默认填充色

		this.con_info = {}; //container信息

		this.init(option);
	}
	Container.prototype = {
		init: function(option) {
			if (!this.validation(option)) {
				return;
			}
			var _this = this;
			this.object = creatContainer(this.width, this.length, this.height);

			function creatContainer(width, length, height) {
				var geometry = new THREE.BoxGeometry(width, height, length);
				var damage = option.con_info.damage;
				if (damage && damage != 1) {
					_this.fillColor = 0xf2dede;
				}
				
				var cube = new THREE.Mesh(geometry, getMaterial());
				var pos = _this.position;
				cube.position.set(pos.x, pos.y, pos.z);

				if (_this.con_info) {
					cube.name = _this.con_info.container + '';
					cube.userData = _this.con_info;
				}
				
				cube.userData.fillColor = _this.fillColor;

				//产生影子
				cube.castShadow = true;
				//接收影子
				cube.receiveShadow = true;

				return cube;
			}
			//贴图
			function getMaterial(){
				var img;
				if(_this.con_info.level==0){	//暂时用这个区分接地容器
					img = {
							top : "3d_model/texture/default_top.png",
							left : "3d_model/texture/default_left.png",
							front : "3d_model/texture/default_front.png"
					};
				}else{
					img = {
							top : "3d_model/texture/default_top2.png",
							left : "3d_model/texture/default_left2.png",
							front : "3d_model/texture/default_front2.png"
					};
				}
				
				
				function _m(img_src){
					return new THREE.MeshLambertMaterial({
						color: _this.fillColor,
						transparent:true,
						map: THREE.ImageUtils.loadTexture(img_src)
					});
				}
				
				return new THREE.MeshFaceMaterial([_m(img.front), _m(img.front), _m(img.top), _m(img.top), _m(img.left), _m(img.left)]);
			}
		},
		
		//设置position
		setPosition: function(position) {
			this.position = position;
			this.object.position.set(position.x, position.y, position.z);;
		},
		//参数验证
		validation: function(option) {
			//必选
			var required = [];
			//可选
			var optional = ["width", "height", "length", "position", "con_info"];

			if (!option) {
				return required.length == 0;
			}

			for (var i = 0; i < required.length; i++) {
				var key = required[i];
				if (option[key] != null && option[key] != undefined) {
					this[key] = option[key];
				} else {
					console.error("Creat Container need [" + key + "] parameter");
					return false;
				}
			}
			for (var i = 0; i < optional.length; i++) {
				var key = optional[i];
				if (option[key] != null && option[key] != undefined) {
					this[key] = option[key];
				}
			}
			return true;
		}
	};

	//3d库存
	var _3d = function(option) {

		this.dom_el; //3D库存容器DOM标签

		this.renderer;
		this.camera;
		this.scene;
		this.light;

		this.css3drenderer;



		this.clock; // = new THREE.Clock();
		this.stats;

		this.lookat = {x: 0,y: 0,z: 0}; //视野中心坐标
		this.mouse = {}; //鼠标	x,y

		this.raycaster = new THREE.Raycaster(); //用于鼠标选中事件
		this.mouse_v2 = new THREE.Vector2(); //记录鼠标Vector2位置


		this.shelf = null; //货架
		this.containers = []; //container(货物)

		this.selected_container;

		this.store_info; //库存数据
		this.childrenCon = []; // 小盒子
		this.toPosition={};//container移动的目标位置



		if (option) {
			this.init(option);
		}
	}
	_3d.prototype = {
		//初始化
		init: function(option) {
			if (!this.validation(option)) {
				return;
			}
			var _this = this;

			var el = $(this.dom_el);
			var width = el.width();
			var height = el.height();

			//初始化渲染器
			if (this.renderer == null) {
				this.renderer = new THREE.WebGLRenderer({
					antialias: true,
					shadowMapEnabled: true
					//domElement : el[0]
				});
				this.renderer.setSize(width, height);
				this.renderer.setClearColor(0xFFFFFF, 1.0);

				el.append(this.renderer.domElement);

				/*
				this.stats = new Stats();
		        this.stats.domElement.style.position = 'absolute';
		        this.stats.domElement.style.top = '0px';
		        el.append(this.stats.domElement);
		        */

				/*
				this.css3drenderer = new THREE.CSS3DRenderer();
				this.css3drenderer.setSize(width, height);
				this.css3drenderer.domElement.style.position = 'absolute';
				el.append(this.css3drenderer.domElement);
				*/
			}

			//初始化相机
			if (this.camera == null) {
				this.camera = new THREE.PerspectiveCamera(45, width / height, 1, 10000);
				//此处为设置透视投影的相机，默认情况下，相机的上方向为Y轴，右方向为X轴，沿着Z轴垂直朝里（视野角：fov； 纵横比：aspect； 相机离视最近的距离：near； 相机离视体积最远距离：far）
				this.camera.position.set(500, 0, 0); //设置相机的位置坐标
				this.camera.up.set(0, 1, 0); //设置相机的上
				this.camera.lookAt(this.lookat); //设置视野的中心坐标
			}

			//初始化场景
			if (this.scene == null) {
				//初始化光源
				var light = new THREE.DirectionalLight(0xFFFFFF, 0.7); //设置平行光DirectionalLight
				light.position.set(1, 1, -1); //光源向量，即光源的位置
				light.castShadow = true; //该光源能产生影子
				light.shadowCameraVisible = true; //产生阴影的相机可见，调试用
				var light2 = new THREE.AmbientLight(0x808080); //环境光

				this.scene = new THREE.Scene();

				this.scene.add(light); //追加光源到场景
				this.scene.add(light2);
				//this.scene.fog=new THREE.Fog(0xffffff, 2, 10000);
			}

			this.addGrid();

			//this.loadModel();


			var floors = this.store_info.is_three_dimensional ? 3 : 0;
			this.shelf = new Shelf({
				"floors": floors
			});
			this.scene.add(this.shelf.object);

			this.addContainer(this.store_info.containers);

			

			this.initView(this.shelf.getCenter());

			//this.containers.push(new Container);

			//渲染
			//this.render();


			this.addMouseWheelEvent();
			this.addMouseEvent();



			animate();
			console.log("rander OK!");


			function animate() {
				_this.render();
				requestAnimationFrame(animate);
			}
			/*
			//自动刷新5S,临时解决初次渲染图片未加载的问题
			setTimeout(autoRender, 200);
			var time = 0;
			var _this = this
			function autoRender(){
				time++;
				_this.render();
				if(time<25){
					setTimeout(autoRender, 200);
				}
			}*/
		},
		//参数验证
		validation: function(option) {
			if (!option) {
				return false;
			}
			var el = $("#" + option.el_id);
			if (el.length > 0) {
				this.dom_el = el[0];
				//信息栏
				el.append("<div id='info1'></div><div id='info'></div>");
			} else {
				alert("Not found element '" + option.el_id + "' for 3D store");
				return;
			}

			//必选
			var required = ["store_info"];
			//可选
			var optional = [];

			for (var i = 0; i < required.length; i++) {
				var key = required[i];
				if (option[key] != null && option[key] != undefined) {
					this[key] = option[key];
				} else {
					console.error("Creat 3D store need [" + key + "] parameter");
					return false;
				}
			}
			for (var i = 0; i < optional.length; i++) {
				var key = optional[i];
				if (option[key] != null && option[key] != undefined) {
					this[key] = option[key];
				}
			}
			return true;
		},
		//初始化视野,将货架调整到视野中心
		initView: function() {
			if (!this.shelf) {
				return;
			}
			var lookat = this.shelf.getCenter();
			var cam_pos = this.camera.position;

			var shelf_size = this.shelf.getSize();

			var aspect = this.camera.aspect; //相机纵横比

			var obj_height = shelf_size.y / 2;
			if (shelf_size.z / shelf_size.y > aspect) {
				obj_height = shelf_size.z / aspect;
			}
			var distance = obj_height * 1.5 / Math.tan(Math.PI * 22.5 / 180);


			var x = distance + lookat.x;
			var y = Math.max(lookat.y, distance * Math.tan(Math.PI / 12)); //保证相机到货架地面俯角最小为15°
			var z = lookat.z;

			this.lookat = lookat;
			this.camera.position.set(x, y, z);
			this.camera.lookAt(this.lookat);
			//this.render();
		},
		//重置画面大小
		resize: function() {
			var el = $(this.dom_el);
			var width = el.width();
			var height = el.height();

			this.camera.aspect = width / height;
			this.camera.updateProjectionMatrix();

			this.renderer.setSize(width, height);

			//this.render();
		},
		//渲染
		render: function() {
			TWEEN.update();
			this.renderer.render(this.scene, this.camera);
			//this.css3drenderer.render(this.scene,this.camera);
		},
		//加载模型
		loadModel: function() {
			var _this = this;
			var loader = new THREE.ColladaLoader();
			var url = "./3d_model/avatar/avatar.dae";

			function animate() {
				requestAnimationFrame(animate, _this.renderer.domElement);
				THREE.AnimationHandler.update(_this.clock.getDelta());
				_this.render();
				//_this.stats.update();
			}

			//url = "./3d_model/shelf/shelf.dae";
			loader.load(url, function(collada) {

				/*collada.scene.traverse( function ( child ) {
					if ( child instanceof THREE.SkinnedMesh) {
						var animation = new THREE.Animation( child, child.geometry.animation );
						animation.play();
					}
				});*/
				_this.scene.add(collada.scene);
				//_this.render();
				//animate();
			});
		},
		//添加网格
		addGrid: function() {
			var size = 2500;
			//网格的线--效果
			var material = new THREE.LineBasicMaterial({
				color: 0x000000,
				opacity: 0.2,
				transparent: true
			});

			var geometry = new THREE.Geometry();
			for (var i = -size; i <= size; i += 50) {
				geometry.vertices.push(new THREE.Vector3(-size, 0, i));
				geometry.vertices.push(new THREE.Vector3(size, 0, i));

				geometry.vertices.push(new THREE.Vector3(i, 0, -size));
				geometry.vertices.push(new THREE.Vector3(i, 0, size));
			}
			//画出线
			var line = new THREE.Line(geometry, material, THREE.LinePieces);
			line.receiveShadow = true;
			//写入场景
			this.scene.add(line);
		},
		//添加container
		addContainer: function(containers) {

			var shelf = this.shelf;
			//var pos = this.shelf.getFloorPosition();
			//var ys = pos.height;
			var len = shelf.length;
			var height = shelf.height;
			var floor = 0;
			var base_y = shelf.base_height + shelf.thickness / 2;
			var z = len / 2; //初始位置在最左边

			for (var i = 0; i < containers.length; i++) {
				var con = new Container({
					con_info: containers[i]
				});
				//超出货架往上加一层
				if (z - con.length < -len / 2) {
					floor++;
					z = len / 2;
				}
				z -= con.length / 2 + 5;
				var y = base_y + floor * height + con.height / 2;
				//2D货架叠加放置	暂时考虑所有container尺寸一样
				if (shelf.floors == 0) {
					y = con.height * (floor + 1 / 2);
					shelf.ctn_top = con.height * (floor + 1);
				}
				var position = {x: 0,y: y,z: z};
				con.setPosition(position);

				z -= con.length / 2; //z挪到container右侧
				if (con.object) {
					this.scene.add(con.object);
					this.containers.push(con);
				}
			}
			//先计算好所有position
			function calcPosition(){
				var shelf = this.shelf;
				//var pos = this.shelf.getFloorPosition();
				//var ys = pos.height;
				var len = shelf.length;
				var height = shelf.height;
				var floor = 0;
				var base_y = shelf.base_height + shelf.thickness / 2;
				var z = len / 2; //初始位置在最左边
			}
		},
		//获取所有container THREE.JS对象
		getContainers: function() {
			var cons = this.containers;
			var objs = [];
			if (cons && cons.length > 0) {
				for (var i = 0; i < cons.length; i++) {
					if (cons[i].object) {
						objs.push(cons[i].object);
					}
				}
			}
			cons = this.obj;
			if (cons && cons.length > 0) {
				for (var i = 0; i < cons.length; i++) {
					if (cons[i].object) {
						objs.push(cons[i].object);
					}
				}
			}
			return objs;
		},
		removeContainers:function(containersObj){
			var cons=this.containers;
			//var array=new Array();
			if(containersObj && containersObj.length!=0){
				for(var i=0;i<containersObj.length;i++){
					cons.pop();
				}
			}
		},
		/**
		 * 动画效果
		 * obj : 加载动画的对象，可以是THREE对象或Container、Shelf
		 * to : 目的位置，为null时沿原路返回。可以是THREE.Vector3对象或json对象
		 * time ： 动画时长
		 * cameraFollow ： 相机是否跟随移动
		 * lookatFollow ： 相机lookat是否跟随移动
		 * onCompleteFunc ： 动画完成后调用function
		 * onUpdateFunc ：动画过程中function
		 */
		animation : function(obj, to, time, cameraFollow, lookatFollow, onCompleteFunc, onUpdateFunc){
			var _this = this;
			var level;
			if(obj.userData)
				level=obj.userData.level;
			else
				level=obj.con_info.level;
			if(level!=0){
				level=level-1;
			}
			var jsonObject=_this.childrenCon[level];
			var position;
				
			if(jsonObject)
				position=jsonObject.group.position;
			else
				position=obj.position;
			//obj类型转换
			if(obj instanceof Container || obj instanceof Shelf){
				obj = obj.object;
			}
			if(!obj || !obj instanceof THREE.Object3D){
				return;
			}
			//to为空时返回
			var path = obj.userData.move_path;
			if(to == null　|| to == undefined){
				if(!path || path.length==0){
					return;
				}
				to = path.pop();
			}else{
				//to类型转换
				if(to instanceof THREE.Vector3){
					to = {x : to.x, y : to.y, z : to.z};
				}
				//保存轨迹
				var org = {x:position.x, y:position.y, z:position.z}
				if(path){
					path.push($.extend({}, org));
				}else{
					obj.userData.move_path = [$.extend({}, org)];
				}
			}
			
			var obj_tw = new TWEEN.Tween(obj.position).to(to, time).easing(TWEEN.Easing.Exponential.Out).start();
			
			var l_org = $.extend({}, this.lookat);
			var obj_org = obj.position.clone();
			obj_tw.onUpdate(function(){
				if(lookatFollow){
					var lx = l_org.x + (to.x - l_org.x)*(this.x - obj_org.x)/(to.x - obj_org.x);
					var ly = l_org.y + (to.y - l_org.y)*(this.y - obj_org.y)/(to.y - obj_org.y);
					var lz = l_org.z + (to.z - l_org.z)*(this.z - obj_org.z)/(to.z - obj_org.z);
					
					_this.lookat = {x : lx, y : ly, z : lz};
				}
				_this.camera.lookAt(_this.lookat);
				
				if(onUpdateFunc){
					onUpdateFunc();
				}
			});
			if(onCompleteFunc){
				obj_tw.onComplete(function(){
					if(onCompleteFunc){
						onCompleteFunc();
					}
				});
			}
			
			
			if(cameraFollow){
				
				
				
				var cam_p = this.camera.position;
				var cam_x = cam_p.x + to.x - position.x;
				var cam_y = cam_p.y + to.y - position.y;
				var cam_z = cam_p.z + to.z - position.z;
				var cam_to = {x : cam_x, y : cam_y, z : cam_z};
				var cam_tw = new TWEEN.Tween(cam_p).to(cam_to, time).easing(TWEEN.Easing.Exponential.Out).start();
			}
		},
		/*tweenAnimation:function(position,toPosition,speed){
		 	return new TWEEN.Tween(position).to(toPosition, speed)
			.easing(TWEEN.Easing.Exponential.Out).start();
		},*/
		cloneContainer:function(containerObj){
			var obj_clone = containerObj.clone();
			obj_clone.name = 'clone';
			this.scene.remove(this.scene.getObjectByName("clone"));
			this.scene.add(obj_clone);
			return obj_clone;
		},
		/*隐藏clone container，展开子container；缩回子container，显示clone container
		*@param Opacity  开始透明度，格式为json {x:1}
		*@param targetOpacity 最终透明度
		*@param onCompleteFunc 动作完成后的回调函数
		*/
		conHideOrShowAnimation:function(Opacity,targetOpacity,targetObj,onCompleteFunc){
			var materials= targetObj.material.materials;
			var material0=materials[0];
			var material1=materials[1];
			var material2=materials[2];
			var material3=materials[3];
			var material4=materials[4];
			var material5=materials[5];
			new TWEEN.Tween(Opacity).to(targetOpacity,200).easing( TWEEN.Easing.Exponential.InOut )
				.start().onUpdate(function(){
					//console.log(this);
					material0.opacity=this.x;
					material1.opacity=this.x;
					material2.opacity=this.x;
					material3.opacity=this.x;
					material4.opacity=this.x;
					material5.opacity=this.x;
					//_this.render();
				}).onComplete(onCompleteFunc);
		},
		/*创建子container
		*@param align 排列方式{l:1,w:1,h:1} l表示长，w表示宽，h表示高
		*@param min 子container 数组
		*@param position 子container的坐标
		*/
		createChildrenCons:function(align,min,position){
			//创建小container
			var vector3_={};
			var consArray=new Array();
			var children_index=0;
			var planeH=100*align.l+70,planeW=100*align.w+70;
			var group=new THREE.Group();
			group.name="level"+min[0].level;
			group.position.set(position.x,position.y,position.z);
			this.scene.add(group);
			for(var h_=0;h_<align.h;h_++){
				for(var j=0;j<align.w;j++){//排数	
					for (var i = 0; i < align.l; i++) {
						if(children_index==min.length){
							var planeGeometry = new THREE.PlaneGeometry(planeW, planeH);
					        var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff});
					        var plane = new THREE.Mesh(planeGeometry, planeMaterial);
					       	plane
					       	plane.userData={level:min[0].level};
					        plane.rotation.x = -0.5 * Math.PI;
					        
					        plane.position.set(position.x,0,position.z);
					        group.position.set(position.x,50,position.z);
					        /*plane.position.x = position.x+align.w*20;
				        	plane.position.y = 0;
				        	plane.position.z = position.z+20*align.l;*/
				        	this.scene.add(plane);
				        	
							return {'cons':consArray,'group':group,"plane":plane};
						}
						vector3_={
								x:75*j-40,
								y:85*h_,
								z:75*i-20*(i+1)
								/*x:position.x+75*j,
								y:position.y+85*h_,
								z:position.z+75*i*/
							};
						
						
						var con = new Container({
							con_info: min[children_index],
							width: 60,
							height: 60,
							length: 50,
							position: {
								x:50*j-40,
								y:60*h_,
								z:50*i-20*(i+1)
								/*x:position.x+50*j,
								y:position.y+60*h_,
								z:position.z+50*i
								x: 1000+50*j,
								y: 25+60*h_,
								z: 475+50*i*/
							}

						});
						con.toPosition=vector3_;
						if (con.object) {
							consArray.push(con);
							this.containers.push(con);
							group.add(con.object);
							//this.scene.add(con.object);
						}
						children_index++;
					}
				}
			}
			// create the ground plane
	        var planeGeometry = new THREE.PlaneGeometry(planeW, planeH, 1, 1);
	        var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff});
	        var plane = new THREE.Mesh(planeGeometry, planeMaterial);
	        plane.receiveShadow = true;
	        plane.position.x = position.x;
        	plane.position.y = 0;
        	plane.position.z = position.z;
        	this.scene.add(plane);
			return {'cons':consArray,'group':group};
		},

		/*
		*将展开的子container缩回隐藏，显示父级container，并将父级container移回原处
		*container 层级
		*/
		backParentContainPosition:function(level,fn){
			var _this=this;
			var cons=_this.childrenCon[_this.childrenCon.length-1].cons;
			var isLastIndex=(_this.childrenCon.length-1)>level?false:true;//判断level是否是childrencon这中的最后一个
		
			if (cons.length != 0) {
				//等待子container收回成功好后执行后续方法
				var onComplete=function(){
					//复制container
					var cloneContainer=_this.cloneContainer(_this.childrenCon[_this.childrenCon.length-1]['cloneContainer']);
					var onCompleteFn=function(){
						//移除子container的group,plane
							
							_this.scene.remove(_this.childrenCon[_this.childrenCon.length-1].plane);
							_this.scene.remove(_this.childrenCon[_this.childrenCon.length-1].group);
							_this.removeContainers(cons);
							
							if(_this.childrenCon.length==1){//如果childreCon的长度为1时，货物回到货架，将container添加到场景中
								_this.scene.add(_this.childrenCon[level].parentCon);
							}/*else{//否则将parent container加入到上一级的group中
								_this.childrenCon[level-1].group.add(_this.childrenCon[level].parentCon);
							}*/
							_this.childrenCon.pop();
							if(_this.childrenCon.length==0){
								var onCompleteFn_;
								if(fn){
									onCompleteFn_=function(){
										fn();
									}
								}else{
									onCompleteFn_=function(){
										$("#info1").html('');
										_this.scene.remove(_this.scene.getObjectByName('clone'));
									}
								}
								//移动复制的container，camera ，lookat
								_this.animation(_this.scene.getObjectByName("clone"),null,1000,true,true,onCompleteFn_);
								
							}else{
								//var containers_=cons;
								var containers_=_this.childrenCon[_this.childrenCon.length-1].cons;
								var parentGroup=_this.childrenCon[_this.childrenCon.length-1].group;
								var index=0;
								for(var i=0;i<containers_.length;i++){
									var obj=containers_[i].object;
									index++;
									//var _onCompleteFn=function(){
										//_this.scene.add(containers_[index].object);
										if(cloneContainer.userData.container==obj.userData.container){
											var object_=obj;
											var isLastIndex_=isLastIndex;
											var cloneContainer_=cloneContainer;
											var onCompleteFn_=function(){
												parentGroup.add(object_);
												
												if(/*index_==containers_.length && */!isLastIndex_){
													_this.backParentContainPosition(level,fn);
												}else if(/*index_==containers_.length && */isLastIndex_){
													if(fn){
														fn();
													}
													
												}
												if(_this.scene.getObjectByName('clone')){
													//_this.scene.remove(_this.scene.getObjectByName('clone'));
													_this.scene.remove(cloneContainer_);
												}
											}
											_this.animation(_this.scene.getObjectByName("clone"),null,1000,true,true,onCompleteFn_);
											
										}
										
									//}
									//_this.conHideOrShowAnimation({x:1},{x:0},obj,_onCompleteFn);
								}
							}
							
					}
					_this.conHideOrShowAnimation({x:0},{x:1},cloneContainer,onCompleteFn);	
				};
				//将container收回
				for(var i=0;i<cons.length;i++){
					if(i!=cons.length-1){
						_this.animation(cons[i],cons[i].position,Math.random() * 500 + 500);
					}else{
						_this.animation(cons[i],cons[i].position,Math.random() * 500 + 500,false,false,onComplete);
					}
				}						
			};
		},
		/*
		*将父container移到指定位置，并且展开子container
		*@param cloneContainer clone 的container
		*@param intersects 通过计算出来的container
		*@param level container层数
		*@param childrenCons 选择的container中的子container
		*@param select 选择的container
		*/
		spreadsChildrenCons:function(cloneContainer,intersects,level,childrenCons,select){
			var _this=this;
			var min=childrenCons;
			//动画执行完后执行事件
			var onComplete=function(){
				//创建子container
				var childrenConArray= _this.createChildrenCons({l:2,w:2,h:2},min,childrenPosition);
				childrenConArray['parentCon']=select;
				childrenConArray['cloneContainer']=cloneContainer;
				if(_this.childrenCon[level])
					//_this.childrenCon[level]={'parentCon':select,'cons':childrenConArray,'cloneContainer':cloneContainer};
					_this.childrenCon[level]=childrenConArray;
				else
					_this.childrenCon.push(childrenConArray);
					//_this.childrenCon.push({'parentCon':select,'cons':childrenConArray,'cloneContainer':cloneContainer});
				
				var consArray=_this.childrenCon[_this.childrenCon.length-1].cons;
				var onCompleteFn=function(){
					_this.scene.remove(_this.scene.getObjectByName("clone"));
					for(var i=0;i<consArray.length;i++){
						_this.animation(consArray[i],consArray[i].toPosition,Math.random() * 1000 + 1000);
					}
				}
				//慢慢隐藏clone container,并展开子container
				_this.conHideOrShowAnimation({x:1},{x:0},cloneContainer,onCompleteFn);
			}
			//复制选择的container并动画移动到指定的位置
			var parameters = intersects[0].object.geometry.parameters;
			var cloneConPosX=1030;
			var childrenConPosX=1000;
			if(_this.camera.position.x<0){
				cloneConPosX=-1030;
				childrenConPosX=-1000;
			}
			var cloneObjToPosition={
					x: cloneConPosX,
					y: intersects[0].object.geometry.parameters.height/2,
					z: 800-level*500
				};;
			var childrenPosition={
					x: childrenConPosX,
					y: 25,
					z: 775-level*500
				};
			_this.animation(_this.scene.getObjectByName('clone'),cloneObjToPosition,500,true,true,onComplete);	
		},
		/*
		*显示container信息
		*@param  select选择的container
		*@param  intersects通过鼠标跟camera计算出来的container
		*@param  info1展示信息div
		*/
		showContainerInfo:function(select,intersects,info1){
			var _this=this;
			//var clickColor = 0xff6666;
			if (intersects.length > 0) {
				if (select && select.uuid != intersects[0].object.uuid) {
					return;
				}
				_this.selected_container = intersects[0].object;
			} else {
				_this.selected_container = null;
			}
			if (select) {
				if (!select.userData.trigger) {
					info1.css({
						display: 'block'
					}).append(select.userData.html);
					var html = $('#con_' + select.userData.con_id);
					html.on("click", function() {
						var _this = $(this);
						if (_this.data("pack_up")) {
							_this.find('h1').siblings().show();
							_this.data("pack_up", false);
						} else {
							_this.find('h1').siblings().hide();
							_this.data("pack_up", true);
						}
					});
					var triggers = select.userData.trigger = false;
				} else {
					var triggers = select.userData.trigger = false;
					$('#con_' + select.userData.con_id).remove();
					info1.css({
						display: 'block'
					});
					//info.css({left:0,display:'block'});
					for (var i = 0; i < mat.materials.length; i++) {
						mat.materials[i].color.set(selectColor);
					}
				}
			};
			//var info1_h = info1.height();
			var info1NumNode=info1.find('.element');
			//var body_h = $(_this.dom_el).height();
			if (info1.find('.uls').css('display') == 'none') {
				var info1_show = info1.find('h1').siblings().show();
				var info1_show_h = info1.height();
				info1.find('h1').siblings().hide();
				$(info1.find('h1')[info1.find('h1').length-1]).siblings().show();
				if (info1NumNode.length<=1) {
					info1.find('h1').siblings().show();
					info1.children().data("pack_up", false);
				}
			} else if (info1NumNode.length>1) {
				info1.find('h1').siblings().hide();
				$(info1.find('h1')[info1.find('h1').length-1]).siblings().show();
				info1.children().data("pack_up", true);
			}
		},
		//取消选中container
		unselect: function() {
			var cons = this.getContainers();

			var select = this.selected_container;
			for (var i = 0; i < cons.length; i++) {
				var con = cons[i];
				if (con.userData.trigger) {
					var color = 0xffffff;
					if (select && select.uuid == con.uuid) {
						color = 0xffaaaa;
					}
					var mat = con.material;
					if (mat instanceof THREE.MeshFaceMaterial) {
						for (var j = 0; j < mat.materials.length; j++) {
							mat.materials[j].color.set(color);
						}
					} else {
						mat.color.set(color);
					}
					con.userData.trigger = false;
				}
			}
			//this.render();
			$(this.dom_el).find("#info1").children().remove();
		},
		//相机远离		speed:移动速度，相对于camera_position到lookAt_position距离的百分比; >0远离，<0靠近。
		cameraFarAway: function(speed) {
			var far = this.camera.far; //最远距离
			var near = this.camera.near; //最近距离

			var p = this.camera.position;
			var l = this.lookat;

			var x = p.x + (p.x - l.x) * speed;
			var y = p.y + (p.y - l.y) * speed;
			var z = p.z + (p.z - l.z) * speed;

			var len = Math.sqrt((x-l.x) * (x-l.x) + (y-l.y) * (y-l.y) + (z-l.z) * (z-l.z));
			if (len < near || len > far) {
				return;
			}

			p.set(x, y, z);
			//this.render();
		},
		//相机绕视野中心转动		horizontal:true 绕Y轴转动（水平转动），false 绕Z轴转动（上下转动）；	 clockwise:是否逆时针方向旋转（沿指定轴负方向观察）
		cameraRound: function(horizontal, clockwise) {
			var p = this.camera.position;
			var l = this.lookat;

			var len; //相机到视野中心距离
			var m, n; //平面坐标系mn,以视野中心为原点
			if (horizontal) { //水平转动，操作xz平面
				m = p.x - l.x;
				n = p.z - l.z;
			} else { //上下转动，操作xy平面
				m = p.x - l.x;
				n = p.y - l.y;
			}
			len = Math.sqrt(m * m + n * n);

			var rad = Math.atan(n / m);
			var rad_flag = m > 0 ? 1 : -1;

			var add_flag = clockwise ? 1 : -1;
			//上下移动时如果摄像机在x轴正方向，要反向转动
			if (!horizontal && m > 0) {
				add_flag = -add_flag;
			}
			rad += add_flag * Math.PI / 100;

			//上下视角保持在0°-60°
			if (!horizontal && !((m > 0 && rad < Math.PI / 3 && rad > 0) || (m < 0 && rad > -Math.PI / 3 && rad < 0))) {
				return;
			}

			m = rad_flag * len * Math.cos(rad);
			n = rad_flag * len * Math.sin(rad);

			if (horizontal) {
				p.x = l.x + m;
				p.z = l.z + n;
			} else {
				p.x = l.x + m;
				p.y = l.y + n;
			}

			this.camera.lookAt(l);
			//this.render();
		},
		//相机平移		horizontal:是否水平移动; positive：是否正方向（向右或向上为正方向）。距离默认为货架长或高的百分比，禁止将物体移动到视野外
		cameraMove: function(horizontal, positive) {
			var p = this.camera.position;
			var l = this.lookat;

			var len = horizontal ? this.shelf.length : this.shelf.getSize().y;
			var step = len * 0.02; //移动距离
			var dir = horizontal ? "z" : "y";

			//y轴正方向修正
			if (!horizontal) {
				positive = !positive;
			}
			//z轴正方向修正
			if (horizontal && p.x < 0) {
				positive = !positive;
			}
			var add_flag = positive ? 1 : -1;

			//限制移动范围，保证货架在视野内
			var range = horizontal ? [-len / 2, len / 2] : [0, len];
			var rl = l[dir] + step * add_flag;
			if (rl < range[0] || rl > range[1]) {
				return;
			}

			l[dir] = rl;
			p[dir] += step * add_flag;

			this.camera.lookAt(l);
			//this.render();
		},
		//添加鼠标滚轮事件
		addMouseWheelEvent: function() {
			var _this = this;

			_this.dom_el.oncontextmenu = function() {
				return false;
			}

			function onMouseWheel(e) {
				var far = 1; //相机远离物体，缩小
				e = e || window.event;
				if (e.wheelDelta) { //IE/Opera/Chrome
					if (e.wheelDelta > 0) { //前滚,放大
						far = -1;
					}
				} else if (e.detail) { //Firefox
					if (e.detail < 0) { //前滚,放大
						far = -1;
					}
				}
				_this.cameraFarAway(0.1 * far);
			}
			/*注册事件*/
			if (document.addEventListener) {
				_this.dom_el.addEventListener('DOMMouseScroll', onMouseWheel, false); //Firefox
			}
			_this.dom_el.onmousewheel = onMouseWheel; //IE/Opera/Chrome

		},
		//鼠标事件
		addMouseEvent: function() {
			var _this = this;
			var selectColor = 0xffaaaa;
			var clickColor = 0xff6666;
			//鼠标移动事件（移动到container上）
			this.dom_el.addEventListener('mousemove', mouseOverContainer, false);
			//鼠标放到container上
			function mouseOverContainer(event) {
				var x = event.layerX;
				var y = event.layerY;

				_this.mouse_v2.x = (event.layerX / _this.dom_el.clientWidth) * 2 - 1;
				_this.mouse_v2.y = -(event.layerY / _this.dom_el.clientHeight) * 2 + 1;

				renderSelectedContainer();
			}
			//container选中事件,渲染选中效果
			function renderSelectedContainer(event) {
				var cons = _this.getContainers();
				if (cons.length == 0) {
					return;
				}
				_this.raycaster.setFromCamera(_this.mouse_v2, _this.camera);

				var render = false; //标记是否需要渲染，去掉鼠标移动过程中不必要的渲染
				var intersects = _this.raycaster.intersectObjects(cons);
				var triggers = null;
				var sceneIntersects=_this.raycaster.intersectObjects(_this.scene.children);//选择场景中的object
				if(sceneIntersects.length>0){
					for(var i=0;i<sceneIntersects.length;i++){
						var sceneObj=sceneIntersects[i].object;
						if(sceneObj.geometry instanceof THREE.PlaneGeometry){
							_this.dom_el.style.cursor="pointer";
							break;
							//$(_this.dom_el).css({'cursor':'-webkit-grab','cursor':'-moz-grab','cursor':'grab'});
						}else{
							_this.dom_el.style.cursor='default';
						}
					}
				}
				//恢复上次选中的container
				var select = _this.selected_container;
				if (select && (intersects.length==0 || select.uuid != intersects[0].object.uuid)) {
					var color = select.userData.trigger ? clickColor : select.userData.fillColor;
					setContaierColor(select, color);
					render = true;
				}
				//高亮选中
				if (intersects.length > 0) {

					if (select && select.uuid == intersects[0].object.uuid) {
						return;
					}
					_this.selected_container = intersects[0].object;

					setContaierColor(intersects[0].object, selectColor);
					render = true;
				} else {
					_this.selected_container = null;
				}

				if (render) {
					var info = $(_this.dom_el).find("#info");
					if (intersects.length > 0) {
						$("#store_3d #info1").hide();
						showContainerInfo(_this.selected_container.userData.html);
					} else {
						$("#store_3d #info1").show();
						showContainerInfo(null);
					}
				}
			}
			//改变ctn颜色
			function setContaierColor(obj, color){
				var mat = obj.material;
				//6面不同
				if (mat instanceof THREE.MeshFaceMaterial) {
					for (var i = 0; i < mat.materials.length; i++) {
						mat.materials[i].color.set(color);
					}
				} else {//6面相同
					mat.color.set(color);
				}
			}
			//显示ctn信息
			function showContainerInfo(html) {
				var info = $(_this.dom_el).find("#info");
				info.empty();
				if (html) {
					//var html = "<div class='element'>"+html+"<div>";
					info.css({
						display: 'block'
					}).append(html);
				}
			}
			//======================================================================
			//点击container事件
			this.dom_el.addEventListener('mousedown', clickContainer, false);

			function clickContainer(event) {
				var cons = _this.getContainers();
				_this.raycaster.setFromCamera(_this.mouse_v2, _this.camera);

				var render = false; //标记是否需要渲染，去掉鼠标移动过程中不必要的渲染
				var intersects = _this.raycaster.intersectObjects(cons);
				var select = _this.selected_container;
				var info1 = $(_this.dom_el).find("#info1");
				//var info = $(_this.dom_el).find("#info");
				//  点击添加true  如果有true 改为false
				if (cons.length == 0) {
					return;
				}
				if (select == null || intersects.length == 0) {
					return false;
				};
				//仅左击有效
				if (event.buttons == 1) {
					
				}else if(event.button ==2){
					//console.log('右击事件');
					var level=select.userData.level;
					if(level==0){
						if(select.name!=_this.childrenCon[0].parentCon.name)
							return;
					}else{
						level=level-1;
					}
					_this.backParentContainPosition(level);
				}else{
					return;
				}
			}
			// ============================================================
			// 双击事件
			this.dom_el.addEventListener('dblclick', dblclickContainer, false);
			function dblclickContainer(event) {
				if (event.buttons != 0) {
					return;
				}
				var cons = _this.getContainers();
				var render = false; //标记是否需要渲染，去掉鼠标移动过程中不必要的渲染
				var intersects = _this.raycaster.intersectObjects(cons);
				var select = _this.selected_container;
				if (cons.length == 0) {
					return;
				}
				if (select == null || intersects.length == 0) {
					return false;
				};
				if (select) {
					var level=select.userData.level;//container里的层级
					var info1 = $(_this.dom_el).find("#info1");
					if(level==0){
						info1.html('');
					}
					_this.showContainerInfo(select,intersects,info1);
					var min = select.userData.children; // 小盒子的个数
					/*var min=[
                    {
                        "con_id": 2001106,
                        "container": 2001106,
                        "container_type": 1,
                        "damage": 1,
                        "is_full": 3,
                        "is_has_sn": 1,
                        "level":1,
                        "lot_number": "lot008",
                        "products": [
                            {
                                "catalogs": [
                                    354,
                                    392
                                ],
                                "locked_quantity": 0,
                                "model_number": "SE39FH03",
                                "p_name": "SE39FH03",
                                "pc_id": 1003009,
                                "product_line": 1000017,
                                "quantity": 100,
                                "sn": [],
                                "title_id": 166,
                                "total_locked_quantity": 0,
                                "total_quantity": 100,
                                "union_flag": 0
                            }
                        ],
                        "title_id": 166,
                        "type_id": 76
                    },
                    {
                        "con_id": 2001105,
                        "container": 2001105,
                        "container_type": 1,
                        "damage": 1,
                        "is_full": 3,
                        "level":1,
                        "is_has_sn": 1,
                        "lot_number": "lot008",
                        "products": [
                            {
                                "catalogs": [
                                    354,
                                    392
                                ],
                                "locked_quantity": 0,
                                "model_number": "SE39FH03",
                                "p_name": "SE39FH03",
                                "pc_id": 1003009,
                                "product_line": 1000017,
                                "quantity": 100,
                                "sn": [],
                                "title_id": 166,
                                "total_locked_quantity": 0,
                                "total_quantity": 100,
                                "union_flag": 0
                            }
                        ],
                        "title_id": 166,
                        "type_id": 76
                    },
                    {
                        "con_id": 2001108,
                        "container": 2001106,
                        "container_type": 1,
                        "damage": 1,
                        "is_full": 3,
                        "is_has_sn": 1,
                        "level":1,
                        "lot_number": "lot008",
                        "products": [
                            {
                                "catalogs": [
                                    354,
                                    392
                                ],
                                "locked_quantity": 0,
                                "model_number": "SE39FH03",
                                "p_name": "SE39FH03",
                                "pc_id": 1003009,
                                "product_line": 1000017,
                                "quantity": 100,
                                "sn": [],
                                "title_id": 166,
                                "total_locked_quantity": 0,
                                "total_quantity": 100,
                                "union_flag": 0
                            }
                        ],
                        "title_id": 166,
                        "type_id": 76
                    },
                    {
                        "con_id": 2001107,
                        "container": 2001105,
                        "container_type": 1,
                        "damage": 1,
                        "is_full": 3,
                        "level":1,
                        "is_has_sn": 1,
                        "lot_number": "lot008",
                        "products": [
                            {
                                "catalogs": [
                                    354,
                                    392
                                ],
                                "locked_quantity": 0,
                                "model_number": "SE39FH03",
                                "p_name": "SE39FH03",
                                "pc_id": 1003009,
                                "product_line": 1000017,
                                "quantity": 100,
                                "sn": [],
                                "title_id": 166,
                                "total_locked_quantity": 0,
                                "total_quantity": 100,
                                "union_flag": 0
                            }
                        ],
                        "title_id": 166,
                        "type_id": 76
                    }
                ];*/
                
					if (min != undefined) {
						/*min[0].level=level+1;
						min[0].container=20011+''+level+''+1;
						min[1].level=level+1;
						min[1].container=20011+''+level+''+2;
						min[2].level=level+1;
						min[2].container=20011+''+level+''+3;
						min[3].level=level+1;
						min[3].container=20011+''+level+''+4;*/
						var fn=function(){
							//复制container
							var cloneContainer=_this.cloneContainer(intersects[0].object);
							_this.scene.remove(intersects[0].object);
							if(_this.childrenCon[level-1]){
								_this.childrenCon[level-1].group.remove(intersects[0].object);
								var groupPosition=_this.childrenCon[level-1].group.position;
								cloneContainer.position.x=cloneContainer.position.x+groupPosition.x;
								cloneContainer.position.y=cloneContainer.position.y+groupPosition.y;
								cloneContainer.position.z=cloneContainer.position.z+groupPosition.z;
							}
							_this.spreadsChildrenCons(cloneContainer,intersects,level,min,select);
						}
						if(_this.childrenCon[level]){
							if(_this.childrenCon[level].parentCon.name==intersects[0].object.name){
								return;
							}else{
								_this.backParentContainPosition(level,fn);
							}
						}else{
							fn();
						}
						
					}else{
						if(_this.childrenCon[level]){
							_this.backParentContainPosition(level,fn);
						}
							
					}
				};
			}
			//=======================================================================
			//鼠标拖动事件
			this.dom_el.addEventListener('mousedown', onDocumentMouseDown, false);
			function onDocumentMouseMove(event) {
				if(_this.childrenCon.length>0){
					_this.raycaster.setFromCamera(_this.mouse_v2, _this.camera);
					var sceneIntersects=_this.raycaster.intersectObjects(_this.scene.children);//选择场景中的object
					for(var i=0;i<sceneIntersects.length;i++){
						var obj=sceneIntersects[i].object;
						if(obj.geometry instanceof THREE.PlaneGeometry){
							var rotation_z=obj.rotation.z;
							if(event.layerX>_this.mouse.x){
								rotation_z+=Math.PI/120;
							}else{
								rotation_z-=Math.PI/120;
							}
							obj.rotation.z=rotation_z;
							//_this.scene.getObjectByName('level'+obj.userData.level).rotation.y=rotation_z;
							_this.childrenCon[obj.userData.level-1].group.rotation.y=rotation_z;
							
							_this.mouse.x=event.layerX;
							return;
						}
					}
				}
					var x = event.layerX;
					var y = event.layerY;

					var move_x = x - _this.mouse.x;
					var move_y = y - _this.mouse.y;
					if (move_x == 0 && move_y == 0) {
						return;
					}

					var abs = Math.abs(move_x / move_y);

					var move_on_x = abs > 1; //水平移动
					var move_right = move_x > 0; //鼠标右移
					var move_up = move_y < 0; //鼠标上移

					var clockwise = move_on_x ? move_right : move_up; //是否顺时针

					if (event.ctrlKey || event.button == 2) {
						//ctrl或鼠标右键时平移相机
						_this.cameraMove(move_on_x, clockwise);
					} else if (event.button == 0) {
						//转动相机
							//move_on_x=false;
						_this.cameraRound(move_on_x, clockwise);
					}

					_this.mouse.x = x;
					_this.mouse.y = y;
				
				
			}

			function onDocumentMouseDown(event) {
				event.preventDefault();

				_this.dom_el.addEventListener('mousemove', onDocumentMouseMove, false);
				_this.dom_el.addEventListener('mouseup', onDocumentMouseUp, false);
				_this.dom_el.addEventListener('mouseout', onDocumentMouseOut, false);

				_this.mouse.x = event.layerX;
				_this.mouse.y = event.layerY;
			}

			function onDocumentMouseUp(event) {
				//鼠标移入卸载
				_this.dom_el.removeEventListener('mousemove', onDocumentMouseMove, false);
				_this.dom_el.removeEventListener('mouseup', onDocumentMouseUp, false);
				_this.dom_el.removeEventListener('mouseout', onDocumentMouseOut, false);
			}

			function onDocumentMouseOut(event) {
				//鼠标移出时卸载
				_this.dom_el.removeEventListener('mousemove', onDocumentMouseMove, false);
				_this.dom_el.removeEventListener('mouseup', onDocumentMouseUp, false);
				_this.dom_el.removeEventListener('mouseout', onDocumentMouseOut, false);
			}
		}
	};
	return _3d;
}));