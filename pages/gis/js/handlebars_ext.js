define(['handlebars'],function(Handlebars){
	Handlebars = Handlebars.default;
	Handlebars.registerHelper('iftrue', function(conditional, options) {
		  if(conditional=="true") {
		    return options.fn(this);
		  } else if(conditional=="false") {
		    return options.inverse(this);
		  }
	});
	
  	return Handlebars;  
});