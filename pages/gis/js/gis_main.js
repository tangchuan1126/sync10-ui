var menus = []; //左侧菜单
var layer_;
var assetIds = ""; //用于实时位置
//定时任务的刷新时间
var time = {
	"truck": 120000,
	"dock": 10000
};
var assetNotDisplay = {}; //不需要实时显示的车辆,true不显示,false显示
//定位仓库
var storageObjInterval = null;
var storageLostFocus = false;
//查询围栏 路线  关键点
var pageRaedy = true; //页码初始化完成，解决初始化页码和初始化页面数据异步执行导致的页码错误
var initPage = false;
var initGeoFen = false;
var initGeoLine = false;
var initGeoPoint = false;

var geoPageSize = 10;
var linePageNo = 1;
var linePageCount = 0;
var lineDataCount = 0;

var fenPageNo = 1;
var fenPageCount = 0;
var fenDataCount = 0;

var pointPageNo = 1;
var pointPageCount = 0;
var pointDataCount = 0;

var currentGeoType = null;
var imgHeight = "";
var imgWidth = "";
var imgEventX = "";
var imgEventY = "";
var imgDiv = "";
var imgDivWinW = "";
var imgDivWinH = "";
var imgDivWinX = "";
var imgDivWinY = "";
var systenFolder = "/Sync10/";
var menusSelectedFlag = {}; //存放选中的菜单
var focusNum = 0; //input获取焦点次数

var kmalName;

var createLocInventoryList = "";
/*$(function () {
	//初始化菜单权限
	initmenuAuth();
	//初始化左侧菜单div高度
	initMenuSize();
	//设置map div高度
	$("#jsmap").height($("#gis_main").height() - $("#map_tool").height());
	//初始化时间控件
	initTimeField();
	//初始化资产信息树
	initAssetTree();
	//初始化map
	jsMapInit();
	//初始化车辆定位
	getLastPosition();
	//初始化map tool
	initMapTool();
	//初始化图层div大小
	initStorageKmlLayerSie();
	//屏蔽右侧菜单事件
	$(document).find("#jsmap").bind("contextmenu",function(e){
		return false;
	});
	//屏蔽左侧菜单事件
	$(document).find("#menu").bind("contextmenu",function(e){
		return false;
	});
});
$(window).resize(function() {
	initMenuSize();
	resizeMap();
	setFloatWindowPosition();
	initStorageKmlLayerSie();
});
 */

var __playcom = "";
require(["oso.lib/videolanrtsp/rtspPlay"], function(rtspPlay) {

	__playcom = new rtspPlay({
		width: "640px",
		height: "485px",
		zindex: "900"
	});
	//console.dir(__playcom);
});



function initMenuSize() {
	var menuHeight = 0;
	for (var i = 0; i < menus.length; i++) {
		menuHeight += $("#" + menus[i] + "_top").height();
	}
	//$("#menu").height($("body").height());
	//设置左侧菜单div高度
	for (var i = 0; i < menus.length; i++) {
		$("#" + menus[i]).height($("#gis_left").height() - menuHeight);
	}
}

function resizeMap() {
	var leftWidth = 0;
	leftWidth += $("#gis_left")[0].style.display != "none" ? $("#gis_left").width() : 0;
	//leftWidth += $("#left_show_img")[0].style.display!="none" ? $("#left_show_img").width() : 0;
	$("#jsmap").height($("body").height());
	$("#jsmap").width($("body").width() - leftWidth-1);

	//更改库存大小
	if(resizeInventoryListWin){
		resizeInventoryListWin();
	}
}

function initMapTool() {
	//清除图层控件
	addMapControl("right_bottom", "click", clearMap, "clearMap", "clear1.png");
	//map tool控件
	addMapControl("top_right", "mouseover", displayMapTool, "tool");
}
/*function initTimeField(){
	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth()+1;
	var day = d.getDate();
	month = month > 9 ? month : "0"+month;
	day = day > 9 ? day : "0"+day;
	var date = year+"-"+month+"-"+day;
	$("#start_time").val(date);
	$("#end_time").val(date);
	$("#start_time").datetimepicker({
		dateFormat: "yy-mm-dd",
		timeFormat: "",
		showTime: false,
		showSecond: false,
		showMinute: false,
		showHour: false
	});
 	$("#end_time").datetimepicker({
 		dateFormat: "yy-mm-dd",
		timeFormat: "",
		showTime: false,
		showSecond: false,
		showMinute: false,
		showHour: false
	});
 	$("#ui-datepicker-div").css("display","none");
}
*/
/*function initAssetTree(){
	$.ajax({
		url:systenFolder+'action/administrator/gis/GetAllAssetAction.action',
		data:'',
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.length>0){
				assetIds = "";
				var html = "<ul>\n"
				var lastGroup = -1;
				for(var i=0; i<data.length; i++){
					var groupId = data[i].group_id;
					var assetId = data[i].asset_id;
					var assetName = data[i].asset_name;
					var assetImei = data[i].asset_imei;
					var icon = data[i].icon_url;
					if(groupId != lastGroup){
						var groupName = data[i].group_name;
						if(lastGroup != -1){
							html += "</li>\n"
						}
						html += "<li id='group_"+groupId+"' rel='group'><a href='#'><ins>&nbsp;</ins>"+groupName+"</a>\n";
					}
					html += "<ul id='child_ul'>\n"+
							"<li id='asset_"+assetId+"' rel='asset' imei='"+assetImei+"'><a href='#'><ins>&nbsp;</ins> <img alt='pic' src='<%=systenFolder%>"+icon+"' >&nbsp;"+assetName+"</a></li>\n"+
							"</ul>\n";
					lastGroup = groupId;
					assetIds += assetId+",";
					$("#asset").data("asset_"+assetId,data[i]);
				}
				html += "</li>\n</ul>";
				assetIds = assetIds.substr(0,assetIds.length-1);
				$("#asset").html(html);
			}
			creatAssetTree();
		},
		error:function(){
		}
	});
}*/
/*function creatAssetTree(){
	$("#asset").tree({
		ui : {
			theme_name : "checkbox"
		},
		rules : {
			// only nodes of type root can be top level nodes
			valid_children : [ "group" ],
			multiple:true,	//支持多选
			drag_copy:false	//禁止拷贝
		},
		types : {
			// all node types inherit the "default" node type
			"default" : {
				deletable : false,
				renameable : false,
				draggable : false
			},
			"group" : {
				valid_children : [ "asset" ]
			}, 
			"asset" : {
				valid_children : "none",
				max_depth :0
			}
		},
		"callback" : {
			onselect : function(node,tree_obj){ //选择
				treeNodeSelect(node);
				startCarIsChecked();
            },
            onrgtclk : function(NODE,TREE_OBJ,EV){	//右键
				
            }
        },
		plugins : {
			checkbox : {}
		}
	});
	//assetTree节点默认全选
	$("[rel='group']").each(function(){jQuery.tree.plugins.checkbox.uncheck(this);});
	$("[rel='asset']").each(function(){assetNotDisplay[this.id]=true;});
	contextMenu();
}*/
//树节点点击  

function treeNodeSelect(node) {
	var ids = "";
	for (var i = 0; i < node.length; i++) {
		var id = $(node[i]).data('id').trim();
		var assetId = $(node[i]).data('assetid');

		if (node[i].checked) {
			if (ids) {
				ids += "," + assetId;
			} else {
				ids = assetId;
			}
			if (jsmap.markerCurrent[id]) {
				jsmap.showMarker(jsmap.markerCurrent[id]);
				jsmap.labelCurrent[id].setMap(jsmap.getMap());
			}
			assetNotDisplay[id] = false;
		} else {
			if (jsmap.markerCurrent[id]) {
				jsmap.hideMarker(jsmap.markerCurrent[id]);
				jsmap.labelCurrent[id].setMap(null);
			}
			assetNotDisplay[id] = true;
		}
	}
	return ids;
	/*var nodeType = $(node).attr("rel");
	var itemId = $(node).attr("itemId");
	var t = $.tree.reference(node);
	var checked = t.get_node(node).children("a").hasClass("checked");  //点击之前的状态
	if(nodeType == "group"){
		$(node).find("[rel='asset']").each(function(){
			treeNodeSelect(this);
		});
	}
	if(nodeType == "asset"){
		if(checked==true){  //取消勾选
			if(jsmap.markerCurrent[node.id]){
				jsmap.hideMarker(jsmap.markerCurrent[node.id]);
				jsmap.labelCurrent[node.id].hide();
			}
			assetNotDisplay[node.id] = true;
		}else{  //勾选
			if(jsmap.markerCurrent[node.id]){
				jsmap.showMarker(jsmap.markerCurrent[node.id]);
			    jsmap.labelCurrent[node.id].show();
			}
			assetNotDisplay[node.id] = false;
		}
	}*/
}
//工具选择

function mapToolClick(el) {
	var radioButton = ["move_map", "measure_distance", "measure_area"];
	var id = el.id;
	//设置工具
	setMapTool(id);
	if (id == 'coordinate_system') {
		coordinateSystem();
	} else if (id == 'display_legend') {

	} else if (id == 'draw_road') {

	} else {
		//设置工具
		setMapTool(id);
		//修改map_tool选中状态
		for (var i = 0; i < radioButton.length; i++) {
			if (radioButton[i] != id && $("radioButton[i]")) {
				$("#" + radioButton[i]).attr("flag", "0");
				$("#" + radioButton[i]).attr("class", "map_tool");
			}
		}
	}

}
//隐藏/显示左侧菜单

function showOrHideAssetTree(flat) {
	if (flat == 1) { //hide
		//$("#left_show_img").show();
		$("#gis_left").hide();
		//菜单显示控件
		addMapControl("top_left", "click", showOrHideAssetTree, "showAssetTree", "hide_light_right.png");
	} else { //show
		//$("#left_show_img").hide();
		$("#gis_left").show();
		hideMapControl("showAssetTree");
	}
	initMenuSize();
	resizeMap();
}

function displayLegend() {
	showParkingDocksLegend();
}
//菜单选择

function selectMenu(menu) {
	var dis = document.getElementById(menu).style.display;
	var isOpen = false;
	var index = 0;
	if (dis != "none") {
		isOpen = true;
	}
	for (var i = 0; i < menus.length; i++) {
		$("#" + menus[i]).hide();
		$("#" + menus[i] + "_img").attr({
			src: "../imgs/maps/Closed.png"
		});
		if (menus[i] == menu) {
			index = i;
		}
	}
	if (isOpen && menus.length > 1) {
		index < menus.length - 1 ? index++ : index--;
		//未点击过的围栏需要刷新
		var geoAlarm = "geoFencing,geoLine,geoPoint";
		if (geoAlarm.indexOf(menus[index]) > -1) {
			initGeoAlarmLabel(menus[index]);
		}
	}
	$("#" + menus[index]).show();
	$("#" + menus[index] + "_img").attr({
		src: "../imgs/maps/Opened.png"
	});
	//关闭绘图
	cancelGeo();
}
//仓库列表样式

function storageKmlMouseout() {
	var selected = $("#storageKml").data("selected");
	$("#storageKml div[id!=" + selected + "][kmlname]").css("backgroundColor", "#EEEEEE");
}

function initStorageKmlLayerSie() {
	var layer = $("#storageKml div[eltype='layer']");
	layer.width($("#storageKml").width() - 10);
}
//显示图层选项

function showStorageKmlLayer(el) {
	//initStorageKmlLayerSie();
	$("#storageKml div[eltype='layer']").hide();
	var layer = $(el).find("div").last();
	//layer.width($("#storageKml").width()-10);
	layer.show();
}

function showWebcamLayer() {
	var webcamCheck = $("#kml_" + psId + "_layer input[id='webcam']").attr("checked");
}


function selectKml(el, psId, kmlName) {
	// var menuCheck=new Array();
	if (jsmap.isFitBounds && jsmap.isFitBounds["ps_id"] && jsmap.isFitBounds["ps_id"] != psId) {
		//切换仓库
		if ($.isEmptyObject(jsmap.storageBasePolyline[psId])) {
			getStorageBaseDataAjax(psId, false); //获取地图base数组 并且有数据时绘制base
		} else {
			var bounds = jsmap.storageBasePolyline[psId].data.bounds;
			jsmap.googleMap.fitBounds(bounds);
		}
	} else {
		//未切换仓库
		if ($.isEmptyObject(jsmap.storageBasePolyline[psId])) {
			getStorageBaseDataAjax(psId, false); //第一次进来时执行
		}
	}

	var areaCheck = $("#area").prop("checked");
	//if(el=="area"){
	if (areaCheck) {
		//menuCheck.push(el);
		drawAreaByPsId(psId, false);
	} else {
		jsmap.deleteAllArea(psId);
		$("#title").attr("checked", false);
		/*var index =$.inArray(el,menuCheck);
			if(index!=-1){
				menuCheck.splice(index,1);
			}*/

	}
	//}
	var titleCheck = $("#title").prop("checked");
	//	if(el=="title"){
	if (titleCheck) {
		if (!areaCheck) {
			$("#area").prop("checked", true);
			drawAreaByPsId(psId, false);
		}
		//menuCheck.push(el);
		loadZoneTitle(psId, false);
	} else {
		/*var index =$.inArray(el,menuCheck);
			if(index!=-1){
				menuCheck.splice(index,1);
			}*/
		jsmap.clearAllTitle(psId);
	}
	//	}
	var zoneDockCheck = $("#zonedock").prop("checked");
	//if(el=="zonedock"){
	if (zoneDockCheck) {
		//menuCheck.push("zonedock");
		loadZoneResource(psId, false);
	} else {
		/*var index =$.inArray("zonedock",menuCheck);
			if(index!=-1){
				menuCheck.splice(index,1);
			}*/
		jsmap.clearZoneDocks(psId);
	}
	//}

	var zonePersonCheck = $("#zonePerson").prop("checked");
	//if(el=="zonePerson"){
	if (zonePersonCheck) {
		//menuCheck.push("zonePerson");
		loadZonePerson(psId, false);
	} else {
		/*var index =$.inArray("zonePerson",menuCheck);
			if(index!=-1){
				menuCheck.splice(index,1);
			}*/
		jsmap.clearZonePerson(psId);
	}
	//	}
	
	var locCheck_2d=$("#location_2d").prop("checked");
	var kml_2d=$("#location_2d").data("kml");
	if(locCheck_2d){
		var url=getKmlUrl("2d_locs_" + kml_2d);
		jsmap.flag["drawLocation"] = true;
		jsmap.flag["draw2DLocation"] = true;
		jsmap.loadKml(psId+"_2d_locs", url);
	}else{
		jsmap.flag["draw2DLocation"] = false;
		jsmap.clearAllLocation(psId);
		jsmap.unLoadKml(psId+"_2d_locs");
	}
	var locCheck_3d=$("#location_3d").prop("checked");
	var kml_3d=$("#location_3d").data("kml");
	if(locCheck_3d){
		var url=getKmlUrl("3d_locs_" + kml_3d);
		jsmap.flag["drawLocation"] = true;
		jsmap.flag["draw3DLocation"] = true;
		jsmap.loadKml(psId+"_3d_locs", url);
	}else{
		jsmap.flag["draw3DLocation"] = false;
		jsmap.clearAllLocation(psId);
		jsmap.unLoadKml(psId+"_3d_locs");
	}
	/*if(!locCheck_3d && !locCheck_2d){
		jsmap.flag["drawLocation"] = false;
	}*/
	var stagingCheck = $("#staging").prop("checked");
	//if(el=="staging"){
	if (stagingCheck) {
		//menuCheck.push("staging");
		drawStagingByPsId(psId, false);
	} else {
		/*var index =$.inArray("staging",menuCheck);
			if(index!=-1){
				menuCheck.splice(index,1);
			}*/
		jsmap.clearAllStaging(psId);

	}
	//}


	var dockCheck = $("#docks").prop("checked");
	if (dockCheck) {
		jsmap.flag["drawDocks"] = true;
		drawDockYardByPsId(psId, false);
		startDocksParkingRefresh();
	} else {
		jsmap.flag["drawDocks"] = false;
		jsmap.clearAllDocks(psId);
	}
	var parkingCheck = $("#parking").prop("checked");
	if (parkingCheck) {
		jsmap.flag["drawParking"] = true;
		drawDockYardByPsId(psId, false);
		startDocksParkingRefresh();
	} else {
		jsmap.flag["drawParking"] = false;
		jsmap.clearAllParking(psId);
	}

	//webcam
	var webcamCheck = $("#webcam").prop("checked");
	//if(el=="webcam"){
	if (webcamCheck) {
		//menuCheck.push("webcam");
		drawWebcamByPsid(psId, false);
		if(jsmap.zoom<=13){

			//drawWebcam(jsmap.storageWebcam[psId]);
			var camMarker=jsmap.storageWebcamMarker;
			for(var key in camMarker){
				if(key.indexOf(psId)>-1){
					camMarker[key].setMap(null);
				}
			}
			if(jsmap.typicalSelectStorageCam[psId]){
				jsmap.typicalSelectStorageCam[psId].data.label.setMap(jsmap.getMap());
				jsmap.typicalSelectStorageCam[psId].setMap(jsmap.getMap());
			}else{
				hideSelectStorageCam();
			}
		}/*else{
			
		}*/
		
	} else {
			if(jsmap.typicalSelectStorageCam[psId]){
				jsmap.typicalSelectStorageCam[psId].data.label.setMap(null);
				jsmap.typicalSelectStorageCam[psId].setMap(null);
			}
			jsmap.deleteAllWebcam(psId);
	}
	//}
	//printer
	var printerCheck = $("#printer").prop("checked");
	//if(el=="printer"){
	if (printerCheck) {
		//menuCheck.push("printer");
		drawPrinterByPsId(psId, false);
	} else {
		/*var index =$.inArray("printer",menuCheck);
			if(index!=-1){
				menuCheck.splice(index,1);
			}*/
		jsmap.clearAllPrinter(psId);
	}
	//}

	//road
	var roadCheck = $("#road").prop("checked");
	//if(el=="road"){
	if (roadCheck) {
		jsmap.loadRoadLayer(psId, {
			"main": true,
			"entery": true
		});
	} else {
		jsmap.clearRoadLayer(psId);
	}
	var inventoryCheck=$("#inventory").prop("checked");
	if(inventoryCheck){
		var dt={'ps_id':psId};
		queryInventoryFlagInMap(psId,dt);
	}else{
		jsmap.clearStorageCatalog();
	}
	/*
	//light
	var lightCheck = $("#light").prop("checked");
	//if(el=="light"){
		if(lightCheck){
			drawLightByPsId(psId);
		}else{
			jsmap.clearLight('all');
		}
		
	//}
	
	jsmap.menuCheck[psId]=menuCheck;
 */
	jsmap.isFitBounds["ps_id"] = psId;
}
//仓库位置点击事件

function storageKmlClick(data, e) {
	var type = data.state;
	if (type == 1) {
		var event = null;
		for (var v in e) {
			if (e[v] && e[v].type == "click") {
				event = e[v];
			}
		}
		if (event.ctrlKey) {
			//prescribedRoute_(this,e);
		} else {
			storageMenuClick(data);
		}

	} else if (type == 6) {
		showWebcam(data.dbdata);
	} else if (type == 3 || type == 4) {
		showOccupiedDetail(data.id, type, data.name, data.psId);
	}
}

//仓库位置右击事件

function storageKmlRightClick(data, position, latlng) {
	var type = data.state;
	if (!type) {
		return false;
	}
	//执行方法之前先隐藏右键菜单
	if ($("#zonedock").prop('checked')) {
		$('#addDock').show();
		$('#addDock').data('area_id', data.area_id);
		$('#addDock').data('ps_id', data.psId);
		$('#addDock').data('name', data.name);
		$("#addDock").data('position', data.position);
	} else {
		$('#addDock').hide();
	}
	if ($("#zonePerson").prop('checked')) {
		$("#addPerson").show();
		$("#addPerson").data('ps_id', data.psId);
		$("#addPerson").data('area_id', data.area_id);
		$("#addPerson").data('name', data.name);
		$("#addPerson").data('position', data.position);

	} else {
		$("#addPerson").hide();
	}
	if ($("#title").prop('checked') && type == 5) {
		$('#modifyTitle').data('key', data.key);
		$("#modifyTitle").show();
		$('#modifyPosition').hide();
	} else {
		$("#modifyTitle").hide();
		if (type == 1 || type == 2 || type == 3 || type == 4 || type == 5) {
			$('#modifyPosition').data('data-key', data.key);
			$('#modifyPosition').data('data-state', type);
			$('#modifyPosition').data('data-name', data.name);
			/*$('#addMultiLayer').data('data-key',data.key);
				$('#addMultiLayer').data('data-state',type);
				$('#addMultiLayer').data('data-name',data.name);*/
			$("#modifyPosition").show();
		}
		if (type == 1) {
			var locCheck_2d=$("#location_2d").prop("checked");
			var locCheck_3d=$("#location_3d").prop("checked");
			var is_three_dimensional=data['is_three_dimensional'];
			if(locCheck_2d &&  is_three_dimensional==0){
				$('#modifyPosition').children()[0].text = 'Modify Location';
			}else if(locCheck_3d &&  is_three_dimensional==1){
				$('#modifyPosition').children()[0].text = 'Modify Location';
			}else{
				return true;
			}
			
		} else if (type == 2) {
			$('#modifyPosition').children()[0].text = 'Modify Staging';
		} else if (type == 3) {
			$('#modifyPosition').children()[0].text = 'Modify Docks';
		} else if (type == 4) {
			$('#modifyPosition').children()[0].text = 'Modify Parking';
		} else if (type == 5) {
			$('#modifyPosition').children()[0].text = 'Modify Zone';
			//$("#addMultiLayer").show();
		}
	}



	if (type == 6) {
		openWebcamWind(data.dbdata);
		var _type = data.state;
		var psId = data.psId;
		var id = data.dbdata.id;
		dragStorageLayerObject(_type, psId, id, "webCamInfo");
		return;
	}
	var position_name = (type == 1 ? data.name_full : data.name);
	if (type == 3) {
		if (data.availableStatus == "0") {
			$('#stopUse').data('data-id', data.id);
			$('#stopUse').data('data-ps_id', data.psId);
			$("#stopUse").show();
			$("#startUse").hide();
		} else {
			$("#stopUse").hide();
			$('#startUse').data('data-id', data.id);
			$('#startUse').data('data-ps_id', data.psId);
			$("#startUse").show();
		}
	} else {
		$("#stopUse").hide();
		$("#startUse").hide();
	}
	$("#jsmap").data("latlng", latlng);
	showRightMenu(position);
	/*var offset = $("#jsmap").offset();
	var posx = parseInt(position.x + offset.left);
	var posy = parseInt(position.y + offset.top);
	var menu = document.getElementById("menu");
	menu.style.display = "block"; //设置菜单可见   
	menu.style.top = posy + "px"; //设置菜单位置为鼠标右击的位置   
	menu.style.left = posx + "px";*/
}

document.onclick = function() { //左击清除弹出菜单   
	var menu = document.getElementById("menu");
	menu.style.display = "none";
}

function openPinterWindow(data) {
	data.pageType = 1;
	require(['views/modify_printerView', 'templates'], function(modify_printerView, templates) {
		modify_printerView.render(data, 'printerInfo');
	})
}

function showOccupiedDetail(id, type, name, psId) {
	var data = {
		"id": id,
		"type": type,
		"ps_id": psId
	};
	require(['views/occupied_detailView'],
		function(occupiedDetailView) {
			occupiedDetailView.render(data, name);
			//tablecloth.start();
		});
}
//修改Light

function modifyLight(data) {
	require(['views/modify_lightView', 'templates'],
		function(modify_lightView, templates) {
			modify_light.render(data, templates.modify_light);
		});
}

function openWebcamWind(data) {
	data['pageType'] = '1';
	require(['views/webcamView', 'templates'],
		function(webcamView, templates) {
			webcamView.render(data, templates.webcam, 'webCamInfo');
		})
}
// 在菜单处回调查看商品信息

function storageMenuClick(data) {

	var psId = data.psId;
	var position_all = data.name_full;
	var state = data.state;
	if (state == 1) { //点击的是location
		var title = $("#select_stock").data("pro_title") == -1 ? null : $("#select_stock").data("pro_title");
		var lot_num = $("#select_stock").data("pro_lot_num") == -1 ? null : $("#select_stock").data("pro_lot_num");
		var line = $("#select_stock").data("pro_line") == -1 ? 0 : $("#select_stock").data("pro_line");
		var pro_category_1 = $("#select_stock").data("pro_category_1");
		var pro_category_2 = $("#select_stock").data("pro_category_2");
		var pro_category_3 = $("#select_stock").data("pro_category_3");
		var catalog_id = null;
		if (!"-1" == pro_category_1) {
			catalog_id = pro_category_1;
		}
		if (!"-1" == pro_category_2) {
			catalog_id = pro_category_2;
		}
		if (!"-1" == pro_category_3) {
			catalog_id = pro_category_3;
		}
		if (!catalog_id) {
			catalog_id = "";
		}
		if (!title) {
			title = "";

		}
		if (!lot_num) {
			lot_num = "";
		}
		if (!line) {
			line = "";
		}
		require(["views/locInventoryListView"], function(locInventoryListView) {
			var dt = {
				'ps_id': parseInt(data.psId),
				'slc_id': parseInt(data.key.split('_')[1]),
				'slc_type': 1,
				'name':data.name
			};
			var dt_ = {};
			if (!$.isEmptyObject($('#productStorageFrom'))) {
				dt_ = $('#productStorageFrom #queryDt').data('queryDt');
				$.extend(dt, dt_);
			}
			locInventoryListView.render(dt);
		})
		/* var data={ps_id:psId,position_all:position_all,title:title,lot_num:lot_num,line:line,catalog_id:catalog_id};
	 require(['views/dialog_product_infoView'],function(dialog_product_infoView){
	 	dialog_product_infoView.render(data);
	 })*/
	}
	$("#menu").data("data", "");
}

//生成摄像头rtsp地址

function getWebcamRtsp(webCam) {
	var rtsp = "rtsp://";
	if (webCam.user && webCam.user != "" && webCam.password && webCam.password != "") {
		rtsp += webCam.user + ":" + webCam.password + "@";
	}
	rtsp += webCam.ip + ":" + webCam.port + "/h264/ch1/main/av_stream";
	return rtsp;
}
/**
 * 返回点击图层时 图层的信息
 * @returns
 */

function getClickLayerinfo() {
	var data = $("#jsmap").data("curr_poly_data");
	return data;
}

//停用门

function stopUse() {
	var ps_id = $("#stopUse").data("data-ps_id");
	var sd_id = $("#stopUse").data("data-id");
	setStorageDoorStatus(ps_id, sd_id, 1);

}
//启用门

function startUse() {
	var ps_id = $("#startUse").data("data-ps_id");
	var sd_id = $("#startUse").data("data-id");
	setStorageDoorStatus(ps_id, sd_id, 0);
}
/**
 *parking dock占用情况
 */

function parkingDocksOccupancy() {
	if (jsmap.flag["drawDocks"] || jsmap.flag["drawParking"]) {
		getParkingDocksOccupancyAjax(currentPsId);
	}
}
/**
 * 异步加载docks and parking数据并且绘制图层
 * @param psId
 * 由于定时任务 所以在当出现system error的时候停止该方法体的继续运行
 */

function getParkingDocksOccupancyAjax(psId) {
	if (loading["docks_parking_" + psId]) {
		return;
	}
	$.ajax({
		url: '/Sync10/_gis/dockInfoCotroller/getDocksParkingOccupancy',
		data: {
			"ps_id": psId
		},
		dataType: 'json',
		type: 'get',
		timeout: 5000,
		beforeSend: function(request) {
			loading["docks_parking_" + psId] = true;
		},
		success: function(data) {
			if (data && data.flag == 'true') {
				renderParkingDocksOccupancy(psId, data.objs);
			}
			loading["docks_parking_" + psId] = false;
		},
		error: function() {
			loading["docks_parking_" + psId] = false;
		}
	});
}

function initGeoAlarmLabel(type) {
	if (type) {
		currentGeoType = type;
	} else {
		type = currentGeoType;
	}
	if (type == "geoFencing") {
		var data = {
			type: 'geoFencing'
		};
		loadGeoAlarmTableView(data, 'geoFencing');
	}
	if (type == "geoLine") {
		var data = {
			type: 'geoLine'
		};
		loadGeoAlarmTableView(data, 'geoLine');
		/*getGeoAlarmLabel("geoLine",linePageNo,geoPageSize);
		initGeoLine = true;*/
	}
	if (type == "geoPoint") {
		var data = {
			type: 'geoPoint'
		};
		loadGeoAlarmTableView(data, 'geoPoint');
	}
}

/*function showGeoAlarmLabel(type,pageTo){
	if(type=="geoFencing"){
		if(pageTo=="next"){
			fenPageNo<fenPageCount ? fenPageNo++ : "" ;
		}else if(pageTo=="prev"){
			fenPageNo>1 ? fenPageNo-- : "" ;
		}else if(pageTo=="first"){
			fenPageNo=1;
		}else if(pageTo=="last"){
			fenPageNo=fenPageCount;
		}else{
			fenPageNo=1;
		}
		var data={type:'geoFencing',pageNo:fenPageNo,pageSize:geoPageSize};
		loadGeoAlarmTableView(data,'geoFencing');
		//getGeoAlarmLabel("geoFencing",fenPageNo,geoPageSize);
	}
	if(type=="geoLine"){
		if(pageTo=="next"){
			linePageNo<linePageCount ? linePageNo++ : "";
		}else if(pageTo=="prev"){
			linePageNo>1 ? linePageNo-- : "";
		}else if(pageTo=="first"){
			linePageNo=1;
		}else if(pageTo=="last"){
			linePageNo=linePageCount;
		}else{
			linePageNo=1;
		}
		var data={type:'geoLine',pageNo:linePageNo,pageSize:geoPageSize};
		loadGeoAlarmTableView(data,'geoLine');
	}
	if(type=="geoPoint"){
		if(pageTo=="next"){
			pointPageNo<pointPageCount ? pointPageNo++ : "";
		}else if(pageTo=="prev"){
			pointPageNo>1 ? pointPageNo-- : "";
		}else if(pageTo=="first"){
			pointPageNo=1;
		}else if(pageTo=="last"){
			pointPageNo=pointPageCount;
		}else{
			pointPageNo=1;
		}
		var data={type:'geoPoint',pageNo:pointPageNo,pageSize:geoPageSize};
		loadGeoAlarmTableView(data,'geoPoint');
	}
}*/

function loadGeoAlarmTableView(data, handlebars_str) {
	require(['views/geoAlarmTableView', 'templates'], function(geoAlarmTableView, templates) {
		var template = templates[handlebars_str];
		geoAlarmTableView.render(data, template);
	});
}
/*function getGeoAlarmLabel(type,pageNo,pageSize,key){
	var pageData;
	$.ajax({
		url:systenFolder+'action/administrator/gis/GetGeoAlarmLabelAction.action',
		data:'type='+type+"&pageNo="+pageNo+"&pageSize="+pageSize+(key ? "&key="+key : ""),
		dataType:'json',
		type:'post',
		async:false,
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				 pageData =data;
				//creatGeoAlarmTable(data);
			}
		},
		error:function(){
		}
	});
	return pageData;
}*/
/*function creatGeoAlarmTable(data){
	var type = data.type;
	var geo = data.geo;
	if(type=="count"){
		pointDataCount = 0;
		lineDataCount = 0;
		fenDataCount = 0;
		for(var i=0; i<geo.length; i++){
			var geotype = geo[i].geotype;
			if(geo[i].geotype == 1){
				pointDataCount += parseInt(geo[i].num);
			}else if(geo[i].geotype == 2){
				lineDataCount += parseInt(geo[i].num);
			}else if(geotype==3 || geotype==4 || geotype==5){
				fenDataCount += parseInt(geo[i].num);
			}
		}
		fenPageCount = Math.ceil(fenDataCount/geoPageSize);
		linePageCount = Math.ceil(lineDataCount/geoPageSize);
		pointPageCount = Math.ceil(lineDataCount/geoPageSize);
		if(fenPageNo > fenPageCount){
			fenPageNo = fenPageCount;
		}
		if(linePageNo > linePageCount){
			linePageNo = linePageCount;
		}
		if(pointPageNo > pointPageCount){
			pointPageNo = pointPageCount;
		}
		pageRaedy = true;
	}
	refreshPageCtrl();
}*/
/*function refreshPageCtrl(){
	if(fenPageNo == fenPageCount){
		$("#geoFencing_next").attr("src","imgs/page_next_dis.gif");
		$("#geoFencing_last").attr("src","imgs/page_last_dis.gif");
	}else{
		$("#geoFencing_next").attr("src","imgs/page_next.gif");
		$("#geoFencing_last").attr("src","imgs/page_last.gif");
	}
	if(fenPageNo == 1){
		$("#geoFencing_first").attr("src","imgs/page_first_dis.gif");
		$("#geoFencing_prev").attr("src","imgs/page_prev_dis.gif");
	}else{
		$("#geoFencing_first").attr("src","imgs/page_first.gif");
		$("#geoFencing_prev").attr("src","imgs/page_prev.gif");
	}
	
	if(linePageNo == linePageCount){
		$("#geoLine_next").attr("src","imgs/page_next_dis.gif");
		$("#geoLine_last").attr("src","imgs/page_last_dis.gif");
	}else{
		$("#geoLine_next").attr("src","imgs/page_next.gif");
		$("#geoLine_last").attr("src","imgs/page_last.gif");
	}
	if(linePageNo == 1){
		$("#geoLine_first").attr("src","imgs/page_first_dis.gif");
		$("#geoLine_prev").attr("src","imgs/page_prev_dis.gif");
	}else{
		$("#geoLine_first").attr("src","imgs/page_first.gif");
		$("#geoLine_prev").attr("src","imgs/page_prev.gif");
	}
	
	if(linePageNo == linePageCount){
		$("#geoPoint_next").attr("src","imgs/page_next_dis.gif");
		$("#geoPoint_last").attr("src","imgs/page_last_dis.gif");
	}else{
		$("#geoPoint_next").attr("src","imgs/page_next.gif");
		$("#geoPoint_last").attr("src","imgs/page_last.gif");
	}
	if(linePageNo == 1){
		$("#geoPoint_first").attr("src","imgs/page_first_dis.gif");
		$("#geoPoint_prev").attr("src","imgs/page_prev_dis.gif");
	}else{
		$("#geoPoint_first").attr("src","imgs/page_first.gif");
		$("#geoPoint_prev").attr("src","imgs/page_prev.gif");
	}
	$("#geoFencing_page").html(fenPageNo+"/"+fenPageCount);
	$("#geoFencing_page").prev().hide();
	$("#geoLine_page").html(linePageNo+"/"+linePageCount);
	$("#geoLine_page").prev().hide();
	$("#geoPoint_page").html(linePageNo+"/"+linePageCount);
	$("#geoPoint_page").prev().hide();
}*/
/*function selectPage(type,source,event){
	var span = $("#"+type+"_page");
	var pageNo = (type=="geoLine"?linePageNo:(type=="geoPoint"?pointPageNo:fenPageNo));
	var pageCount = (type=="geoLine"?linePageCount:(type=="geoPoint"?pointPageCount:fenPageCount));
	if(source=="click"){
		span.html("/"+pageCount);
		span.prev().show();
		span.prev().focus();
	}else{
		if(source=="keyup" && event.keyCode!=13){
			return;
		}
		var val = parseInt(span.prev().val());
		if(isNaN(val) || val<1 || val>pageCount || val==pageNo){
			span.prev().hide();
			span.html(pageNo+"/"+pageCount);
			return;
		}
		type=="geoLine" ? (linePageNo=val) : (type=="geoPoint" ? (pointPageNo=val) : (fenPageNo=val));
		var data={type:type,pageNo:val,pageSize:geoPageSize};
		var template_str="";
		if(type=='geoFencing'){
			template_str='geoFencing'
		}else if(type=='geoLine'){
			template_str='geoLine';
		}else if(type=='geoPoint'){
			template_str='geoPoint';
		}
		loadGeoAlarmTableView(data,template_str);
	}
	
}*/
//显示/隐藏围栏、线路、关键点

function selectGeoAlarmlabel(obj, id) {

	displayGeoFencing(obj, id);
}
//搜索围栏、线路、关键点

function searchAlarmLabel(type) {

}

function addGeofencing(type) {
	var mapType = type;
	var pointEl = "";
	$("#fencing_points").val("");
	$("#line_points").val("");
	$("#point_points").val("");
	if (type == "geoFencing" || type == "polygon" || type == "circle" || type == "rect") {
		/*$("#fencing_name").val("");
		$("#fencing_def").val("");*/
		//$("#fencing_type").val("");
		mapType = $("#fencing_type").val();
		$("#addGeoFencing").show(500);
		pointEl = "#fencing_points";
	} else if (type == "geoLine") {
		/*$("#line_name").val("");
		$("#line_def").val("");*/
		mapType = $("#line_type").val();
		$("#addGeoLine").show(500);
		pointEl = "#line_points";
	} else if (type == "geoPoint") {
		/*	$("#point_name").val("");
		$("#point_def").val("");*/
		mapType = $("#point_type").val();
		$("#addGeoPoint").show(500);
		pointEl = "#point_points";
	}
	//绘制并回填结果
	drawOnMap(mapType, function(points) {
		$(pointEl).val(points);
	});
}
//删除围栏

function removeGeoFencing(type) {
	var divs = $("#" + type + "_list div");
	if (divs && divs.length > 0) {
		var ids = "";
		for (var i = 0; i < divs.length; i++) {
			var checked = $(divs[i]).find("input").prop("checked");
			if (checked) {

				ids += divs[i].id.split('_')[1] + ",";
			}
		}
		if (ids == "") {
			alert("Please choose item first!");
			return;
		}
		ids = ids.substr(0, ids.length - 1);
		$.ajax({
			url: systenFolder + 'action/administrator/gis/RemoveAlarmLabelAction.action',
			data: "ids=" + ids + "&type=" + type,
			dataType: 'json',
			type: 'post',
			beforeSend: function(request) {

			},
			success: function(data) {
				if (data && data.flag == "true") {
					clearGeoObj(); //清除图层

					initPage = false;
					initGeoFen = false;
					initGeoLine = false;
					initGeoPoint = false;
					initGeoAlarmLabel(data.geotype);
				} else {
					showMessage("Delete failed", "alert");
				}
			},
			error: function() {
				showMessage("System error", "error");
			}
		});
	}
}
//取消画图

function cancelGeo() {
	setMapTool("move_map");
	$("#addGeoFencing").hide(500);
	$("#addGeoLine").hide(500);
	$("#addGeoPoint").hide(500);
}
//保存围栏

function saveAlarmLabel(type) {
	var form = (type == "line" ? $("#addGeoLineForm") : (type == "point" ? $("#addGeoPointForm") : $("#addGeoFencingForm")));
	var name = form.find("[name='name']").val();
	var points = form.find("[name='points']").val();
	if (!name) {
		alert("Name can't be empty...");
		return;
	}
	if (!points) {
		alert("Please create Map first...");
		return;
	}
	$.ajax({
		url: systenFolder + 'action/administrator/gis/SaveGeoAlarmLabelAction.action',
		data: form.serialize() + "&points=" + points,
		dataType: 'json',
		type: 'post',
		beforeSend: function(request) {

		},
		success: function(data) {
			if (data && data.flag == "true") {
				cancelGeo();
				initPage = false;
				initGeoFen = false;
				initGeoLine = false;
				initGeoPoint = false;
				initGeoAlarmLabel(data.geotype);
				showMessage("Save successfully", "succeed");
			} else {
				showMessage("Save failed", "alert");
			}
		},
		error: function() {
			showMessage("System error", "error");
		}
	});
}
//刷新围栏 

function refreshGeoFencing(type) {
	linePageNo = 1;
	fenPageNo = 1;
	pointPageNo = 1;
	initPage = false;
	initGeoFen = false;
	initGeoLine = false;
	initGeoPoint = false;
	initGeoAlarmLabel(type);
}

//库存查询
var pro_url = {
	//产品线
	url: systenFolder + 'action/administrator/product/AjaxLoadProductLineAction.action',
	//一级分类
	url1: systenFolder + 'action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
	//二级分类
	url2: systenFolder + 'action/administrator/product/AjaxLoadSecondProductCatalogAction.action',
	//三级分类
	url3: systenFolder + 'action/administrator/product/AjaxLoadThirdProductCatalogAction.action'
}
//选择title

	function selectTitle(titleId, parent) {
		if (titleId == "-1") {
			$("#" + parent + " #pro_lot_num_tr").hide();
			$("#" + parent + " #pro_lot_num").html("");
			$("#" + parent + " #pro_line_tr").hide();
			$("#" + parent + " #pro_line").html("");
			selectCategory(1, "-1", parent);
		} else {
			ajaxLotNumber(titleId);
			ajaxProductData(titleId, 0, "#pro_line", parent, pro_url.url);
		}
	}
	//加载批次

	function ajaxLotNumber(titleId) {
		$.ajax({
			url: systenFolder + 'action/administrator/product/AjaxLotNumberByTitleAction.action',
			type: 'post',
			dataType: 'json',
			data: "titleId=" + titleId,
			success: function(data) {
				if (data) {
					$("#pro_lot_num_tr").show();
					var html = "<option value='-1'>ALL</option>\n";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							html += "<option value=" + data[i].id + ">" + data[i].name + "</option>\n";
						}
					}
					$("#pro_lot_num").html(html);
				}
			}
		});
	}
	//加载产品线或分类     id:TITLE或产品线或上级分类id   titleId：TITLE ID   eleId:HTML标签id

	function ajaxProductData(id, titleId, eleId, parent, url) {
		var _parent = parent;
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: 'id=' + id + '&atomicBomb=0&title_id=' + titleId,
			success: function(data) {
				if (data) {
					$(eleId + "_tr").show();
					var html = "<option value='-1' selected='selected'>ALL</option>\n";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							html += "<option value=" + data[i].id + ">" + data[i].name + "</option>\n";
						}
					} else {
						selectCategory(1, "-1", _parent);
					}
					$(eleId).html(html);
				}
			}
		});
	}
	//选择分类

	function selectCategory(level, id, parent) {
		if (id == "-1") {
			for (var i = level; i <= 3; i++) {
				$("#" + parent + " #pro_category_" + i + "_tr").hide();
				$("#" + parent + " #pro_category_" + i).html("");
			}
		} else {
			var url = pro_url["url" + level];
			var titleId = $("#pro_title").val();
			var eleId = "#" + parent + " #pro_category_" + level;
			ajaxProductData(id, titleId, eleId, parent, url);
		}
	}
	//库存位置查询

	/*function searchStorageCatalog() {
		
	}*/
	//查询需求分布，国家

	function searchProductDemandByCountry() {
		var condition = {
			title_id: $("#productDemand #pro_title").val(),
			product_line: $("#productDemand #pro_line").val(),
			pc_name: $("#productDemand #pro_name").val(),
			start_time: $("#productDemand #start_time").val().trim(),
			end_time: $("#productDemand #end_time").val().trim(),
			product_catalog: ""
		}
		if (condition.title_id == "-1") {
			condition.title_id = "";
		}
		for (var i = 1; i <= 3; i++) {
			var category = $("#productDemand #pro_category_" + i).val();
			if (category && category != "-1") {
				condition.product_catalog = category;
			}
		}
		$("#productDemand").data("condition", condition);
		$.ajax({
			url: systenFolder + 'action/administrator/b2b_order/GetB2BOrderOrProductCountGroupCountryAction.action',
			type: 'post',
			dataType: 'json',
			data: condition,
			success: function(data) {
				if (data && data.length > 0) {
					var data_temp = [];
					for (var i = 0; i < data.length; i++) {
						var d = data[i];
						data_temp.push({
							regionId: d.deliver_ccid,
							count: d.product_count
						});
					}
					showBoundaryInMap(data_temp, searchProductDemandByProvince);
					storageLostFocus = true;
				} else {
					showMessage("No Data...", "alert");
				}
			},
			error: function() {
				showMessage("System error...", "error");
			}
		});
	}
	//查询需求分布，省份

	function searchProductDemandByProvince(country_id) {
		var condition = $("#productDemand").data("condition");
		condition.ccid = country_id;
		$.ajax({
			url: systenFolder + 'action/administrator/b2b_order/GetB2BOrderOrProductCountGroupProvinceAction.action',
			type: 'post',
			dataType: 'json',
			data: condition,
			success: function(data) {
				if (data && data.length > 0) {
					var data_temp = [];
					for (var i = 0; i < data.length; i++) {
						var d = data[i];
						data_temp.push({
							regionId: d.deliver_pro_id,
							count: d.product_count
						});
					}
					showBoundaryInMap(data_temp);
				} else {
					showMessage("No Data...", "alert");
				}
			},
			error: function() {
				showMessage("System error...", "error");
			}
		});
	}
var nav = {};
//线路规划

function routePlan(type) {
	var poly = $("#jsmap").data("route");
	var latlng = $("#jsmap").data("latlng");
	addRouteMarker(latlng, type);
	nav[type] = {
		"type": poly.state,
		"name": poly.name,
		"latlng": latlng,
		"psId": poly.psId
	};
	if (nav.from && nav.to) {
		getRoutePathAjax(nav);
	}
	jsmap.routeMarkerFlag[type] = true;
}
//获取导航线路

function getRoutePathAjax(data) {
	if (data.from.psId != data.to.psId) {
		return;
	}
	var _data = {
		'ps_id': data.from.psId
	};
	var fromLatlng = data.from.latlng.lng() + " " + data.from.latlng.lat();
	var toLatlng = data.to.latlng.lng() + " " + data.to.latlng.lat();
	_data['from_type'] = data.from.type;
	_data['from_position'] = data.from.name;
	_data['from_latlng'] = fromLatlng;
	_data['to_type'] = data.to.type;
	_data['to_position'] = data.to.name;
	_data['to_latlng'] = toLatlng;
	var dt = JSON.stringify(_data);
	$.ajax({
		url: '/Sync10/_gis/roadInfoCotroller/queryRoutePath',
		type: 'post',
		dataType: 'json',
		data: dt,
		contentType: "application/json; charset=UTF-8",
		success: function(data) {
			if (data && data.length > 0) {
				showRoutePath(data);
			} else {
				showMessage("Roadless...", "alert");
			}
		},
		error: function() {
			nav = {};
			clearRoutePath();
			showMessage("Routing fail.", "error");
		}
	});
}


function coordinateSystem() {
	var ps_id = viewData["storageThirdlyMenusTabsView"].ps_id;
	if (ps_id) {
		$.ajax({
			url: systenFolder + 'action/administrator/maps/GetStorageCoordinateSysAction.action',
			type: 'post',
			dataType: 'json',
			data: {
				"ps_id": currentPsId
			},
			success: function(data) {
				if (data && data.length > 0) {
					showCoordinateSystem(currentPsId, data);
				} else {
					showMessage("No coordinate system data.", "alert");
				}
			},
			error: function() {
				showMessage("Get coordinate system fail.", "error");
			}
		});
	}
}

/**
 * 改变图层勾选状态
 * layers: 图层id数组
 * select: 勾选(true)或取消勾选(false)
 * triger: 触发更改(true)或仅更改状态(false)
 * invertOther: 反向处理其它图层(true)或仅处理layers指定的图层(false)
 */

function layerSelectStatus(layers, select, triger, invertOther) {
	var ls = $("#kml_" + currentPsId + "_layer input[type='checkbox']");
	var radio = ["area", "title", "zonedock", "zonePerson"]; //单选处理
	var radio_id = null;
	for (var i = 0; i < ls.length; i++) {
		var l = $(ls[i]);
		var id = l.attr("id");
		if ($.inArray(id, radio) > -1) {
			if ($.inArray(id, layers) > -1) {
				//l.attr("checked",select);
				radio_id = id;
			}
		} else {
			if ($.inArray(id, layers) > -1) {
				l.attr("checked", select);
			} else {
				if (invertOther) {
					l.attr("checked", !select);
				}
			}
		}
	}
	//单选处理
	if ((select && invertOther) || radio_id) {
		for (var i = 0; i < radio.length; i++) {
			$("#kml_" + currentPsId + "_layer input[type='checkbox'][id='" + radio[i] + "']").attr("checked", false);
		}
		if (radio_id) {
			$("#kml_" + currentPsId + "_layer input[type='checkbox'][id='" + radio_id + "']").attr("checked", select);
		}
	}
	if (triger) {
		var kml = $("#kml_" + currentPsId).attr("kmlname");
		selectKml(kml, currentPsId, "select_status");
	}
}

function setStorageDoorStatus(ps_id, sd_id, status) {
	$.ajax({
		url: '/Sync10/_gis/dockInfoCotroller/setStorageDoorStatus',
		data: {
			'sd_id': sd_id,
			'status': status
		},
		dataType: 'json',
		type: 'POST',
		timeout: 5000,
		beforeSend: function(request) {

		},
		success: function(data) {
			if (data && data.flag == "true") {
				jsmap.clearSingleDocks(ps_id, sd_id);
				drawDockYardByPsId(ps_id, true);
			}
		},
		error: function() {}
	});
}
//显示地图工具

function displayMapTool(el) {
	if (jsmap.mapControls["tool"]) {
		var el = $(jsmap.mapControls["tool"].div);
		var top = (el.offset().top + el.height() + 2).toFixed(0);
		var left = el.offset().left.toFixed(0);
		var tool = $("#map_tool_control");
		tool.offset({
			top: top,
			left: left
		});
		tool.show();
		//tool.attr("flag","1");
		//el.attr("onmouseout","mapToolCtrlMouseout()");

		el.bind("mouseover", function() {
			$("#map_tool_control").val('1');
		});
		el.bind("mouseout", mapToolCtrlMouseout);
	}
}

function mapToolCtrlMouseout() {
	setTimeout(function() {
		var t = $("#map_tool_control");
		if (t && t.val() != "1") {
			t.hide();
		}
	}, 1500);

}
//设置回放速度

function setReplaySpeed(e, flag) {
	var maxValue = 50;
	var ele = $("#floatWindowInfo #replay_speed");
	var len = e.clientX - ele.offset().left;
	var per = len / ele.parent().width();
	var value = parseInt((maxValue - 1) * per + 1); //范围1-50

	var valShow = $("#floatWindowInfo #replay_speed_value_div");
	valShow.html(value);
	var top = ele.offset().top - valShow.height() - 3;
	var left = e.clientX - valShow.width() / 2;
	valShow.show();
	valShow.offset({
		top: top,
		left: left
	});
	if (flag) { //鼠标点击时设置
		replaySpeed = value;
		$("#floatWindowInfo #replay_speed_value").html(value + "p/s");
		ele.css("width", parseInt(per * 100) + "%");
	}
}

function unSetReplaySpeed() {
	$("#floatWindowInfo #replay_speed_value_div").hide();
}

function setReplayProcess(count, num) {
	var per = num / count;
	var value = parseInt(per * 100) + "%";
	$("#floatWindowInfo #replay_process").css("width", value);
	$("#floatWindowInfo #replay_process_value").html(value);
	if (per == 1) {
		replayCtrl("stop");
	}
}

function replayCtrl(ctrl) {
	if (ctrl == "play") {
		replayPause = false;
		historyPlay(false);
		$("#floatWindowInfo #play").hide();
		$("#floatWindowInfo #pause").show();
	}
	if (ctrl == "pause") {
		replayPause = true;
		$("#floatWindowInfo #pause").hide();
		$("#floatWindowInfo #play").show();
	}
	if (ctrl == "stop") {
		replayPause = true;
		$("#floatWindowInfo #pause").hide();
		$("#floatWindowInfo #play").show();
		stopReplay();
	}
}

function historyPlayAfterFitMapBounds() {
	replayPause = false;
	var force = false;
	var currentPsId = $("#select_all_truck").data('groupid');
	if (currentPsId == null) {
		force = true;
	}
	//先清除正在播放的轨迹
	clearHistory();
	var flag = historyPlay(true);
	fitHistoryBounds(force);
	return flag;
}

//gps实时位置刷新时间
var currentPositionInterval = null;
//setInterval(setRefreshTime, time.truck);
//解析历史轨迹

function parseHistory(data) {
	pps = [];
	var list = [];
	for (var i = 0; i < data.length; i++) {
		var p = new JSMapPushpin(data[i].lat, data[i].lon, {}, null);
		var icon = getHeadingMarkerIconURL2("green", data[i].rad);
		var markerImage = jsNewMarkerImege(icon, null, null, jsNewPoint(18, 18), null);
		p.icon = markerImage;
		pps.push(p);
		//new LabelOverlay(list[i],i)
	}
}

function getLastPosition(ids) {
	var ids = ids;
	if (!ids) {
		return;
	}
	$.ajax({
		url: '/Sync10/_gis/carInfoCotroller/queryLasthis',
		type: 'get',
		dataType: 'json',
		cache: false,
		data: {
			"aids": ids
		},
		beforeSend: function(request) {

		},
		error: function(e) {
			//alert("定位失败,请稍后重新尝试");
		},
		success: function(data) {
			var dt = new Array();
			if (data) {
				//移除不需要显示的数据
				for (var i = 0; i < data.length; i++) {
					dt.push(data[i][0]);
					var id = "asset_" + data[i][0].aid;
					if (assetNotDisplay[id]) {
						data.splice(i--, 1);
					}
				}
				showCarsInMap(dt);
				if (data.length == 1) {
					var latlng = jsNewLatLng(data[0][0].lat, data[0][0].lon);
					jsmap.getMap().setCenter(latlng);
					jsmap.getMap().setZoom(17);
				}
			}
		}
	});
}

function addAsset() {
	require(['views/add_assetView'], function(add_assetView) {
		add_assetView.render();
	});
}
//保持添加到GPS

function saveAddAsset(dialog) {
	if ($('#add_asset #assetName').val().trim() == "") {
		verifyMgs['verifyName'] = 0;
		showErrorVerifyMsg('#add_asset #assetName', 'Name is empty!');
	}
	if ($('#add_asset #gps_tracker').val().trim() == "") {
		verifyMgs['gps_tracker'] = 0;
		showErrorVerifyMsg('#add_asset #gps_tracker', 'GPS num is empty!');
	}
	if ($('#add_asset #callnum').val().trim() == "") {
		verifyMgs['callnum'] = 0;
		showErrorVerifyMsg('#add_asset #callnum', 'Phone num is empty!');
	}
	if ($("#add_asset  #flag").val() != "true") {
		var group = "";
		if ($('#add_asset  #group').val() == "0") {
			group = $('#add_asset #newGroup').val().trim();
			if (group == "" || group == "*Group name") {
				verifyMgs['newGroup'] = 0;
				showErrorVerifyMsg('#add_asset #newGroup', 'GroupName is empty!');
			}
		} else {
			group = $('#group option:selected').text().trim();
		}
		$("#groupName").val(group);
		for (var key in verifyMgs) {
			if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
				return;
			}
		}
		$.ajax({
			url: systenFolder + 'action/administrator/gis/AddAssetAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 6000,
			cache: false,
			data: $("#add_asset").serialize(),
			beforeSend: function(request) {

			},
			error: function(e) {

			},
			success: function(data) {
				//alert("添加成功");
				//$.artDialog.opener.initAssetTree();
				var selectedNode;
				if (menusSelectedFlag['seconeMenu']) {
					selectedNode = menusSelectedFlag['seconeMenu'].nodeClass;
				}
				if (selectedNode == group) {
					require(['views/truck_treeView'], function(truck_treeView) {
						truck_treeView.render(menusSelectedFlag['seconeMenu'].data);
					});
				} else {
					require(['views/truckView'], function(truckView) {
						truckView.render();
					});
				}
				dialog.close();
			}
		});
	} else {
		//$("#errInfo").html("GPS num already exists");
	}
}

function inputIn() {
	$("#newGroup").val("").css("color", "black");
}

function outInput() {
	var st = $("#newGroup").val();
	if ($.trim(st).length < 1) {
		$("#newGroup").val("*Group name").css("color", "silver");
	}
}

function refreshTruck() {
	require(['views/truckView'], function(truckView) {
		menusSelectedFlag['seconeMenu'] = undefined;
		$("#bottom_menus").html('');
		truckView.render();
	});
}

function queryHistory() {
	var startTime = $("#startTime").val();
	var endTime = $("#endTime").val();
	var asset_id = Number($("#truck_aid").val());
	var timeZone = $("#timeZone").val();
	$.ajax({
		url: '/Sync10/_gis/carInfoCotroller/queryHis',
		type: 'get',
		dataType: 'json',
		timeout: 30000,
		cache: false,
		data: {
			aid: asset_id,
			stl: startTime,
			etl: endTime,
			option: 4,
			timeZone: timeZone
		},
		beforeSend: function(request) {},
		error: function(e) {
			$("#queryTip").html("System error.");

		},
		success: function(data) {
			if (data.length == 0) {
				$("#queryTip").html("No records in this time.");
			} else {
				$("#queryTip").html("Found " + data.length + " records.");
				parseHistory(data);
			}
		}
	});
}

function historyPlay_(dialog) {
	var flag = historyPlayAfterFitMapBounds();
	if (flag == 1) {
		dialog.close().remove();
	} else {
		$("#queryTip").html("Please query record first");
	}
}
/**
 * 缓慢下滑一个div
 */

function showLayerDome(id) {
	$("#" + id).slideToggle("slow");
}

/**
 * 图层创建时，拖拽时间按下事件
 * @param type
 * @param el
 * @param event
 */

function LayerMousedown(type, el, event) {
	var psId = $("#maintainLayer #ps_id").val();

	var IsComplet=lastAddLayerIsCom[type].call();
	if(!IsComplet){
		return;
	}
	var e = event || window.event;
	e.preventDefault();
	e.stopPropagation();
	imgDiv = $(el).clone();
	imgDiv.css("z-index", 100);
	imgDiv.removeAttr("onmousedown");
	imgDiv.attr({
		layertype: type
	});
	imgDiv.id = "drag_img";
	imgHeight = $(el).height();
	imgWidth = $(el).width();
	imgDiv.css({
		position: 'absolute'
	});
	imgEventX = e.clientX - imgWidth / 2;
	imgEventY = e.clientY - imgHeight;
	imgDiv.offset({
		left: imgEventX,
		top: imgEventY
	});
	$("body").append(imgDiv);
	document.onmousemove = LayerMousemove;
	document.onmouseup = LayerMouseStop;
}
/**
 * 图层创建时，拖拽时间move事件
 * @param e
 */

function LayerMousemove(e) {
	imgEventX = e.clientX - imgWidth / 2;
	imgEventY = e.clientY - imgHeight / 2;
	imgDiv.offset({
		left: imgEventX,
		top: imgEventY
	});
}
/**
 * 图层创建时，拖拽时间Stop事件
 * @param e
 */

function LayerMouseStop(e,type_) {
	$.blockUI({
		message: '<h3><img src="imgs/loading.gif" />Loading...</h3>'
	});
	var currentPsId = $("#maintainLayer #ps_id").val();
	if (!currentPsId && $.isEmptyObject(currentPsId)) {
		alert("Please choose storage first!");
		document.onmouseup = null;
		imgDiv.remove();
		return;
	}
	var type;
	if(type_){
		type=type_;
	}else{
		type= imgDiv.attr("layertype");
	}
	 
	document.onmousemove = null;
	document.onmouseup = null;
	imgEventX = e.clientX;
	imgEventY = e.clientY;
	var map = document.getElementById(MAP_ID);
	var mapWinX = map.offsetLeft;
	var mapWinY = map.offsetTop;
	var mapWinW = map.offsetWidth;
	var mapWinH = map.offsetHeight;
	//imgDivWinW = imgDiv.width();
	//imgDivWinH = imgDiv.height();
	var x = imgEventX - mapWinX;
	var y = imgEventY - mapWinY;
	if(type_){
		x=x-(imgEventX-$("#menu")[0].offsetLeft);
		y=y-(imgEventY-$("#menu")[0].offsetTop);
	}
	//imgDivWinX = imgDiv.position().left;
	//imgDivWinY = imgDiv.position().top;
	/*	var newWinX = imgDivWinX + e.clientX - imgEventX;
	var newWinY = imgDivWinY + e.clientY - imgEventY;*/
	var newWinX = e.clientX;
	var newWinY = e.clientY;
	if ((mapWinX < newWinX && (mapWinX + mapWinW) > (newWinX)) && (mapWinY < newWinY && (mapWinY + mapWinH) > (newWinY))) { //禁止浮动框移出地图
		if (type == 6) {
			y += imgHeight / 2;
			var latlng = jsmap.fromContainerPixelToLatLng(x, y);
			var lat = latlng.lat();
			var lng = latlng.lng();
			createLayerDemo(type, currentPsId, lat, lng, "addWebCam");
			var point = convertLatlngToCoordinateAjax(currentPsId, lat, lng);
			var data = {
				"ps_id": currentPsId,
				"ip": "",
				"port": "",
				"user": "",
				"password": "",
				"x": point.x,
				"y": point.y,
				"inner_radius": 0,
				"outer_radius": 100,
				"s_degree": 0,
				"e_degree": 150
			}
			require(['views/webcamDemoView'],
				function(webcamDemoView) {
					webcamDemoView.render(data, "addWebCam");
				});
		}
		if (type == 7) {
			x -= parseInt(imgWidth / 2);
			y += parseInt(imgHeight / 2);
			var latlng = jsmap.fromContainerPixelToLatLng(x, y);
			var lat = latlng.lat();
			var lng = latlng.lng();
			createLayerDemo(type, currentPsId, lat, lng, "addprinter");
			var printerMarkerDemoDt = jsmap.storagePrinterMarker[currentPsId + "_demo"];
			require(['views/modify_printerDemoView'],
				function(modify_printerDemoView) {
					modify_printerDemoView.render(printerMarkerDemoDt.data.dbdata, 'addprinter');
				});

		}
		if (type == 9) {
			y += imgHeight / 2;
			var latlng = jsmap.fromContainerPixelToLatLng(x, y);
			var lat = latlng.lat();
			var lng = latlng.lng();
			createLayerDemo(type, currentPsId, lat, lng);
			var url = systenFolder + 'administrator/gis/modify_light.html?ps_id=' + currentPsId + '&id=0&pageType=0&lat=' + lat + '&lng=' + lng;
			$.artDialog.open(url, {
				title: "Set Light [dome]",
				width: '320px',
				lock: false,
				opacity: 0.3,
				fixed: true,
				id: "light",
				cancel: false
			});
			$.unblockUI();
		}
		if (type == "x") {
			var storageName = $("#maintainLayer #storageName").val();
			var latlng = jsmap.fromContainerPixelToLatLng(x, y);
			var lat = latlng.lat();
			var lng = latlng.lng();
			createLayerDemo(type, currentPsId, lat, lng, "modify_storage_layer");
			var data = {
				ps_id: currentPsId,
				lat: lat,
				lng: lng,
				storageName: storageName
			};
			require(['views/modify_storage_layerDemoView'], function(modify_storage_layerDemoView) {
				modify_storage_layerDemoView.render(data);
			})
		}
	} else {
		$.unblockUI();
	}
	if(imgDiv){
		imgDiv.remove();
	}
	
}

function showZonedocks(psId, area_id, area_name) {
	var data = {
		ps_id: psId,
		area_id: area_id
	};
	require(["views/zoneResourceView", "config"],
		function(zoneResourceView, config) {
			zoneResourceView.render(area_name, data, config.queryAreaDoor.url);
		});
}

function showZonePerson(psId, area_id, area_name) {

	var data = {
		ps_id: psId,
		area_id: area_id
	};
	require(['views/zonePersonView', 'config'], function(zonePersonView, config) {
		zonePersonView.render(area_name, data, config.queryAreaPerson.url);
	})
}

/**
 * 在area内添加人员
 */

function addPerson() {
	var data = {
		ps_id: $('#addPerson').data('ps_id'),
		area_id: $('#addPerson').data('area_id')
	};
	var area_name = $('#addPerson').data('name');
	require(['views/zonePersonView', 'config'], function(zonePersonView, config) {
		zonePersonView.render(area_name, data, config.queryAreaPerson.url);
	})
}

/**
 * 在area中添加门
 */

function addDock() {
	var data = {
		ps_id: $('#addDock').data('ps_id'),
		area_id: $('#addDock').data('area_id')
	};
	var area_name = $('#addDock').data('name');
	require(["views/zoneResourceView", "config"],
		function(zoneResourceView, config) {
			zoneResourceView.render(area_name, data, config.queryAreaDoor.url);
		});

}
//添加ZoneTitle

function addZoneTitle() {
	var key = $("#modifyTitle").data("key");
	var psId = key.split("_")[0];
	var area_id = key.split("_")[1];
	var titles = jsmap.storageTitle[psId][area_id];
	openStorageTitleSelectDialog(psId, area_id, titles);
}
//打开storage title选择dialog

function openStorageTitleSelectDialog(psId, area_id, titles) {
	require(["views/modifyTitleView", "config"],
		function(modifyTitleView, config) {
			modifyTitleView.render(psId, area_id, titles);
		});
}
/**
 * 刷新窗口
 */

function refreshWindow() {
	window.location.reload();
}


/**
 * ctrl+单击鼠标指定路线
 */

function prescribedRoute_(point, e, area) {
	var from = jsmap.routeMarkerFlag["from"];
	var to = jsmap.routeMarkerFlag["to"];
	var latLng = jsmap.fromContainerPixelToLatLng(e.clientX - point.offsetLeft, e.clientY - point.offsetTop);
	var position = jsmap.overlay.getProjection().fromLatLngToContainerPixel(latLng);
	if (e.ctrlKey) {
		$("#jsmap").data("latlng", latLng);
		$("#jsmap").data("route", area.data);
		if (!from && !to) {
			routePlan('from');
		} else if (from && !to) {
			routePlan('to');
		}
	}
}
/**
 * 打开设置刷新时间的窗口
 */

function openRefreshTimeWin() {
	require(['views/set_refresh_time'],
		function(set_refresh_time) {
			set_refresh_time.render(time);
		})
}
/* 
  * 设置 docks staging car 定时自动刷新时间
  * @param parm
  
 function  setRefreshTime(parm){
	 time.truck=parm.truck;
	 time.dock=parm.dock;
	 if(storageObjInterval!=null){
		 clearInterval(storageObjInterval);//清除定时任务
		 parkingDocksOccupancy();
		 storageObjInterval= setInterval(parkingDocksOccupancy,time.dock);
	 }
	 startCarIsChecked();
 }*/
/**
 * 定时刷新dock和parkin的状态
 */

function startDocksParkingRefresh() {
	if (storageObjInterval != null) {
		clearInterval(storageObjInterval); //清除定时任务
	}
	showOccupiedLegend();
	parkingDocksOccupancy();
	storageObjInterval = setInterval(parkingDocksOccupancy, time.dock);
}
/**
 * docks staging car 定时自动刷新
 */

function startCarIsChecked(ids) {
	var readGps = false;
	for (var key in assetNotDisplay) {
		if (!assetNotDisplay[key]) {
			readGps = true;
			break;
		}
	}
	if (currentPositionInterval != null) {
		clearInterval(currentPositionInterval);
	}
	if (readGps) {
		getLastPosition(ids);
		currentPositionInterval = setInterval(getLastPosition, time.truck);
	}
}
/**
 * 查询staging上的plate信息
 */

function queryPlate() {

	if (!currentPsId) {
		showMessage("Choose storage please!", "alert");
		return;
	}
	initStagingState(currentPsId);
	layerSelectStatus(['staging'], true, true, false);
	var filter_id = $("#ic_id").val();
	$.ajax({
		url: '/Sync10/_gis/stagingInfoCotroller/queryContainerCounts',
		type: 'get',
		dataType: 'json',
		timeout: 6000,
		cache: false,
		data: {
			"ic_id": filter_id,
			"ps_id": currentPsId
		},
		beforeSend: function(request) {

		},
		success: function(data) {
			if (data && data.flag == "true") {
				showStagingPlate(data.rows, filter_id);
			} else if (data && data.flag == "nodata") {
				showMessage("no data", "alert");
			}
		},
		error: function(e) {
			showMessage("System error", "error");
		}
	});


}
/**
 * 打开container artDialog窗口
 * @param data
 */

function openContainerWin(data) {
	var staging_id = data.staging_id;
	var ic_id = data.filter_id;
	var url = systenFolder + 'administrator/gis/showStagingPlate.html?staging_id=' + staging_id + '&ic_id=' + ic_id;
	$.artDialog.open(url, {
		title: "[" + data.name + "]",
		width: '360px',
		lock: false,
		opacity: 0.3,
		fixed: true,
		id: "stagingPlate"
	})
}
/**
 * button 样式修改
 * @param flag_el
 */

function onmouseoverButton(flag_el) {
	flag_el.style.background = '#E6E6FA';
	flag_el.style.border = 'solid 1px #C5C1AA';
}
/**
 * button 样式修改
 * @param flag_el
 */

function onmouseoutButton(flag_el) {
	flag_el.style.background = '#2288cc';
	flag_el.style.border = 'solid 1px #1c6a9e';
}



/**
 *修改modify area保存功能
 */

function modify_area_save(old_data, artDialog) {
	var isChanged = ($("#modify_area #x").val() != old_data.x ||
		$("#modify_area #y").val() != old_data.y ||
		$("#modify_area #height").val() != old_data.height ||
		$("#modify_area #width").val() != old_data.width ||
		$("#modify_area #angle").val() != old_data.angle) ? 'true' : 'false';

	var area_id = $("#modify_area input:hidden#zone_id").val();
	var flag = area_id ? "update" : "add";
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');
			return;
		}
	}
	var subtype = false;
	if (old_data.area_subtype && $("#modify_area #area_subtype").val() != old_data.area_subtype) {
		subtype = true;
	}
	if (!old_data.area_subtype && $("#modify_area #area_subtype").val() != -1) {
		subtype = true;
	}
	var formJson = $("#modify_area").serializeObject();
	formJson['isChanged'] = isChanged;
	formJson['location_type'] = old_data.obj_type;
	var formStr = JSON.stringify(formJson);
	if ($("#modify_area #zone_name").val() != old_data.obj_name || isChanged == "true" || subtype) {
		$.ajax({
			url: '/Sync10/_gis/storageCotroller/updateModifyLayer',
			data: formStr,
			dataType: 'json',
			type: 'post',
			contentType: "application/json; charset=UTF-8",
			beforeSend: function(request) {
				$.blockUI({
					message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
				});
			},
			success: function(data) {
				$.unblockUI();
				if (data && data.flag == 'true') {
					showMessage("Modify succeed", "succeed");
					artDialog.close(flag);
					return;
				} else if (data && data.flag == 'authError') {

					showMessage("Auth error!", "error");
				} else if (data && data.flag == 'false') {

					showMessage("System Error!", "error");
				} else if (data && data.flag == "neo4jError") {
					showMessage("Neo4j Error!", "error");

				}
				alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');

			},
			error: function() {
				$.unblockUI();
				alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');
				$("#errInfo").html("System Error!");
			}
		});
	} else {
		if (!isChanged) {
			artDialog.close();
			return;
		}
		alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');
	}

}

/**
 *回填值
 */

function fillXAndY(x, y, formId) {
	$("#" + formId + " #width").val(x);
	$("#" + formId + " #height").val(y);
}

function fillAngle(angle, formId) {
	$("#" + formId + " #angle").val(angle);
}

function convertCoordinateAjax(psId, lat, lng, formId) {
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/convertLatlngToCoordinate',
		data: {
			"ps_id": psId,
			"lat": lat,
			"lng": lng
		},
		dataType: 'json',
		type: 'get',
		beforeSend: function(request) {},
		success: function(data) {
			if (data && data.flag == "true") {
				var x = data.x;
				var y = data.y;
				if(formId){
					$("#" + formId + " #x").val(x);
					$("#" + formId + " #y").val(y);
				}else{
					$("#menu").find("#xy").html("XY:("+x+","+y+")");
					/*$("#menu").find("#y").html("Y:"+y);*/
				}
			}
		},
		error: function() {}
	});
}

//移除node对象

function removeTitle(ele, obj_id) {
	var areaName = $("#areaName").val();
	var info = 'Is it really will be removed ' + ele.parentNode.title + ' from ' + areaName;
	maskLayer(ele, info, obj_id);
}
/**
 * 遮罩层
 */

function maskLayer(el, info, obj_id) {
	var dock_names
	require(['jquery', 'art_Dialog/dialog-plus'],
		function($, dialog) {
			var d = dialog({
				title: 'Notify',
				lock: true,
				backdropOpacity: 0.3,
				icon: 'question',
				content: "<span style='font-weight:bold;'>" + info + "?</span>",
				button: [{
					value: 'YES',
					callback: function() {
						if (obj_id) {
							if (obj_id) {
								dock_names = $("#" + obj_id).val().split(',');
								$("#" + obj_id).val(dock_names);
								var dt = el.parentNode.title;
								var obj_id_id;
								if (obj_id == 'dock_names') {
									obj_id_id = 'olddock_Ids';

								} else if (obj_id == 'employe_names') {
									obj_id_id = 'adids';

								}
								var index = $.inArray(dt, dock_names);
								if (index >= 0) {
									dock_names.splice(index, 1);
								}
								$("#" + obj_id).val(dock_names.toString());
							}
							$(el.parentNode).remove();
						} else {
							reloadZonePerson_ele(el);
						}

					},
					autofocus: true
				}, {
					value: 'NO'
				}]
			});
			d.showModal();
		});
}
/**
 *提交信息遮罩提示信息
 */
/*function submit*/
/**
 * 刷新person
 */

function reloadZonePerson_ele(el) {
	var adid = $('#assignArea').data('adid');
	/*var oldAdids=$("#adids").val();
	 if(oldAdids.indexOf(adid)==0){
		 oldAdids=oldAdids.substring(adid.length+1,oldAdids.length);
	 }else{
		 oldAdids=oldAdids.substring(0,oldAdids.indexOf(adid)-1)+oldAdids.substring((oldAdids.indexOf(adid)+adid.length),oldAdids.length);
	 }*/
	var ps_id = $("#ps_id").val();
	/*if(oldAdids==''){
 		var data_array=[{area_id:$("#area_id").val()}];
 		reloadZonePerson(ps_id,'delete',data_array);
 	}*/
	//$("#adids").val(oldAdids);
	var area_id = $(el).find("#area_id").val();
	$.ajax({
		url: '/Sync10/_gis/areaInfoCotroller/updatePersonAreaByAdid',
		data: {
			'area_id': area_id,
			'adid': adid,
			'ps_id': ps_id
		},
		dataType: 'json',
		type: 'post',
		success: function(data) {
			if (data && data.flag == "true") {
				showMessage("Successful operation", "succeed");
				//var eleNode=$("#assignArea").data('ele');
				//$(eleNode.parentNode).remove();
				var obj = jsmap.storagePerson[ps_id]; //得到所有storage数据
				//var startIndex=JSON.stringify(obj).indexOf('{"area_id":"'+area_id+'"');//查找当前storage的起始位置，JSON.stringify()---将方法json转为string
				//var endIndex=JSON.stringify(obj).substring(startIndex).indexOf('}')+1;//查找当前storage的终止位置
				//var data=JSON.parse(JSON.stringify(obj).substr(startIndex,endIndex));//截取当前storage的数据，并将截取出来的数据转为json数据
				/*var datas=new Array();
     			 	for(var i=0;i<obj.length;i++){
     			 		var dt=obj[i];
     			 		if(JSON.stringify(dt).indexOf('area_id')>-1){
     			 			
     			 		}else{
     			 			datas.push(dt);
     			 		}
     			 	}
     			 	data.counts=Number(data.counts)+1;
     			 	var data_=[data];*/
				reloadZonePerson(ps_id);
			}
		},
		error: function() {
			showMessage("System error", "error");
		}
	});

}

/**
 * 提交修改
 */

function showZoneDocks_commitData(olddock_Ids, ps_id, area_id) {
	var sdidNodes = document.getElementsByName("sdid");
	var sdids = "";
	var contrastFlag = false;
	if (sdidNodes.length > 0) {
		for (var i = 0; i < sdidNodes.length; i++) {
			if (olddock_Ids) {
				if ($.inArray(sdidNodes[i].value, olddock_Ids.toString().split(',')) < 0) {
					contrastFlag = true;
				} else if (olddock_Ids.toString().split(',').length > sdidNodes.length) {
					contrastFlag = true;
				}
			} else {
				contrastFlag = true;
			}
			if (sdids != "") {
				sdids += "," + sdidNodes[i].value;
			} else {
				sdids += sdidNodes[i].value;
			}
		}
	} else {
		contrastFlag = true;
	}

	if (contrastFlag) {
		$.ajax({
			data: {
				'area_id': area_id,
				'sdIds': sdids
			},
			url: '/Sync10/_gis/areaInfoCotroller/updateZoneResource',
			dataType: 'json',
			type: 'post',
			beforeSend: function(request) {},
			success: function(data) {
				if (data && data.flag == "true") {
					showMessage("Save succeed", "succeed");
					reloadZoneDocks(ps_id);
				} else {
					alterDialogDisabled("div[aria-labelledby='title:showZoneDock'] button", false, 'Submit');
				}
			},
			error: function() {
				alterDialogDisabled("div[aria-labelledby='title:showZoneDock'] button", false, 'Submit');
				showMessage("System error", "error");
			}
		});
	} else {
		alterDialogDisabled("div[aria-labelledby='title:showZoneDock'] button", false, 'Submit');
		showMessage("didn't any change", "error");
	}


}

//在area上添加门

function selectDock() {
	var psId = $('#ps_id').val();
	openDocksDialog(psId);

}
//打开dock select dialog

function openDocksDialog(psId) {
	require(['views/selectDoorView', 'templates', 'config'],
		function(selectDoorView, templates, config) {
			var ulr = config.queryDoorByPsId.url;
			var data = {
				ps_id: psId
			};
			selectDoorView.render(data, ulr, 'selectDock', templates.selectDoor);
		});
}
//添加door ,person,addPrinterServer中的zone

function addTitle(ele, obj_id) {
	if ($("#exitTitle").find("div").length == 0) {
		$("#exitTitle").html("");
	}
	var je = $(ele);
	var id = je[0].childNodes[1].childNodes[1].value;
	var name = je.attr("title");
	var node = creatTitleNode(name, id, false, obj_id);
	$("#exitTitle").append(node);
	je.remove();
}
//创建标签

function creatTitleNode(title, id, isFree, obj_id) {
	var node = null;
	if (isFree) {
		node = $("#title_free_model").clone();
	} else {
		node = $("#title_model").clone();
	}
	node.removeAttr("id");
	node.attr("title", title);
	node[0].childNodes[1].childNodes[1].value = id;
	node.find("div").first().append(title);
	node.css("display", "");
	return node;
}
//删除door,person,addPrinterServer中的zone标签

function removeTitle_(ele, obj_id) {
	if ($("#notExitTitle").find("div").length == 0) {
		$("#notExitTitle").html("");
	}
	var je = $(ele);
	var id = je[0].childNodes[1].childNodes[1].value;
	var title = je.attr("title");
	var node = creatTitleNode(title, id, true);
	$("#notExitTitle").append(node);
	je.remove();
}
//提交新增的门

function confirm_door(dialogObject) {
	var ids = "";
	var names = "";
	var ts = $("#exitTitle #sd_id");
	for (var i = 0; i < ts.length; i++) {
		ids += $(ts[i]).val() + ",";
		names += $(ts[i].parentNode.parentNode).attr("title") + ",";
	}
	if (ids != "") {
		ids = ids.substr(0, ids.length - 1);
		names = names.substr(0, names.length - 1);
	}
	var dock_names = $("#dock_names");
	if (dock_names.length > 0) {
		backFillDoorNode(names, ids);
	} else {
		backFillDoors(names, ids);
	}
	dialogObject.close().remove();
}


/**
 * 回填
 */

function backFillDoorNode(names, ids) {
	var dName=names;
	$("#dock_names").val(dName);
	var names_ = names.split(',');
	var ids_ = ids.split(',');
	var node="";
	var add_dockNode=$("#notExitDoor #add_dock").clone();
	if(!$.isEmptyObject(names)){
		for (var i = 0; i < names_.length; i++) {
			node =node+ "<div title='" + names_[i] + "' class='zone_dock_div_2' >" +
			"<div class='zone_dock_div_3' style='width: 100%;height:100%; float: left; text-align: center;background-color: #CCFFFF;position: relative;' onmouseover=\"this.style.backgroundColor='#2DF7AB'\" onmouseout=\"this.style.backgroundColor='#CCFFFF'\" >" +
			names_[i] + "<input type='hidden' name='sdid' class='adid' value='" + ids_[i] + "'>" +
			"<div class='zone_dock_div_img' onclick=\"removeTitle(this.parentNode,'dock_names')\"></div>" +
			"</div> </div>";
		}
	}
	$("#showZoneDocks #notExitDoor").html(node);
	add_dockNode.appendTo("#showZoneDocks #notExitDoor");
	$("#showZoneDocks").focus();
}

//显示人员右键菜单

function showPersonMenu(ele, event) {
	var eleNode = ele;
	var employe_name = ele.childNodes[0].data.trim();
	var adid = ele.childNodes[1].value;
	$("#assignArea").data('employe_name', employe_name);
	$("#assignArea").data('ele', ele);
	$("#assignArea").data('adid', adid);
	//var adid=ele.childNodes[1].value;


	var evt = event || window.event;
	var container = document.getElementById('zonePersons');
	var dialog_div = container.offsetParent.offsetParent.offsetParent;
	var container_parent = container.offsetParent;
	var menu = document.getElementById('menu_');
	var display = $('#menu_').css('display');
	if (display = 'inherit') {
		$('#menu_').css('display', 'none');
	}
	display = $('#menu_').css('display');

	if (display == 'none') {
		$('#menu_').css('display', 'inherit');
		menu.style.left = (evt.clientX - parseInt(dialog_div.style.left) - container_parent.offsetLeft) + "px";
		menu.style.top = (evt.clientY - parseInt(dialog_div.style.top) - container_parent.offsetTop) + "px";
	}
}
/*隐藏人员菜单*/

function hidePersonMenu() {
	$('#menu_').css('display', 'none');
}
/**
 *修改area上的人后提交
 */

function zonePersonCommitData() {
	var adidNodes = document.getElementsByName("adid");
	var adids = "";
	var oldAdids = $('#adids').val();
	var area_id = $('#area_id').val();
	var ps_id = $('#ps_id').val();
	var flag = "";
	var contrastFlag = false;
	if (adidNodes.length > 0) {
		for (var i = 0; i < adidNodes.length; i++) {
			if ($.inArray(adidNodes[i].value, oldAdids.split(',')) < 0) {
				contrastFlag = true;
			} else if (oldAdids.split(',').length > adidNodes.length) {
				contrastFlag = true;
			}
			if (adids != "") {
				adids += "," + adidNodes[i].value;
			} else {
				adids += adidNodes[i].value;
			}
		}
	} else {
		contrastFlag = true;
	}
	if (contrastFlag) {
		$.ajax({
			data: {
				'area_id': area_id,
				'adids': adids,
				'oldAdids': oldAdids,
				'ps_id': ps_id
			},
			url: '/Sync10/_gis/areaInfoCotroller/updateZonePerson',
			dataType: 'json',
			type: 'post',
			beforeSend: function(request) {},
			success: function(data) {
				if (data && data.flag == "true") {
					reloadZonePerson(ps_id);
				} else {
					alterDialogDisabled("div[aria-labelledby='title:showZonePerson'] button", false, 'Submit');
				}
			},
			error: function() {
				alterDialogDisabled("div[aria-labelledby='title:showZonePerson'] button", false, 'Submit');
			}
		});
	} else {}


}
/**
 *area中增加person
 */

function selectPerson() {
	var adidNodes = document.getElementsByName("adid");
	var adids = "";
	for (var i = 0; i < adidNodes.length; i++) {
		if (adids != "") {
			adids += "," + adidNodes[i].value;
		} else {
			adids += adidNodes[i].value;
		}
	}
	var data = {
		psId: $('#ps_id').val(),
		adids: adids
	}
	require(['views/selectPersonView', 'templates'],
		function(selectPersonView, templates) {
			selectPersonView.render(data, templates.selectPerson);
		})
}

function modifyTitleAjax(psId, area_id, dialogObj) {
	var area_id = area_id;
	var psId = psId;
	/*var ids = "";
	var ts = $("#exitTitle #title_id");
	var names = "";
	var datas=new Array();
	for(var i=0; i<ts.length; i++){
		ids += $(ts[i]).val()+",";
		names +=  $(ts[i].parentNode.parentNode).attr("title")+",";
		var title_data={"OBJ_ID":$(ts[i]).val(),"OBJ_NAME": $(ts[i].parentNode.parentNode).attr("title")};
		datas.push(title_data);
	}
	ids = ids.substr(0,ids.length-1);*/
	var data_ = selectedTitleData();
	var ids = data_.ids;
	var names = data_.names;
	var datas = data_.datas;
	if (ids != "") {
		$.ajax({
			data: {
				"area_id": area_id,
				"ids": ids
			},
			url: '/Sync10/_gis/addTitleCotroller/addTitle',
			dataType: 'json',
			type: 'post',
			beforeSend: function(request) {},
			success: function(data) {
				if (data) {
					showMessage("Modify  succeed!", "succeed");
					jsmap.storageTitle[psId][area_id] = datas;
					loadZoneTitle(psId, false);
				}
			},
			error: function() {}

		});
	} else {
		$.ajax({
			data: {
				"area_id": area_id
			},
			url: '/Sync10/_gis/titleInfoCotroller/deleteTitle',
			dataType: 'json',
			type: 'post',
			beforeSend: function(request) {},
			success: function(data) {
				if (data) {
					showMessage("Modify  succeed!", "succeed");
					delete(jsmap.storageTitle[psId])[area_id];
					loadZoneTitle(psId, true);
				}
			},
			error: function() {}
		});
	}

	dialogObj.close();
}
//获取select title dialog中选择的title

function selectedTitleData() {
	var ids = "";
	var ts = $("#exitTitle #title_id");
	var names = "";
	var datas = new Array();
	for (var i = 0; i < ts.length; i++) {
		ids += $(ts[i]).val() + ",";
		names += $(ts[i].parentNode.parentNode).attr("title") + ",";
		var title_data = {
			"obj_id": $(ts[i]).val(),
			"obj_name": $(ts[i].parentNode.parentNode).attr("title")
		};
		datas.push(title_data);
	}
	ids = ids.substr(0, ids.length - 1);
	names = names.substr(0, names.length - 1);
	return {
		ids: ids,
		names: names,
		datas: datas
	};

}

/**
 *选择添加的人员
 */

function confirm_person(dialogObj) {
	var ids = "";
	var names = "";
	var ts = $("#exitTitle #title_id");
	for (var i = 0; i < ts.length; i++) {
		ids += $(ts[i]).val() + ",";
		names += $(ts[i].parentNode.parentNode).attr("title") + ",";


	}
	if (ids != "") {
		ids = ids.substr(0, ids.length - 1);
		names = names.substr(0, names.length - 1);
	}
	dialogObj.close();
	backFillPerson(names, ids);
}
//回填persons

function backFillPerson(names, ids) {
	var names_ = names.split(',');
	var ids_ = ids.split(',');
	/*if ($("#employe_names").val()) {
		$("#employe_names").val($("#employe_names").val() + ',' + names_);
	} else {*/
		$("#employe_names").val(names_);
	/*}*/
	var node="";
	var add_personNode=$("#notExitDoor #add_person").clone();
	for (var i = 0; i < names_.length; i++) {
		node =node+ "<div title='" + names_[i] + "' class='zone_person_div_2' >" +
			"<div class='zone_dock_div_3'>" +
			names_[i] + "<input type='hidden' name='adid' class='adid' value='" + ids_[i] + "'>" +
			"<div class='zone_dock_div_img' onclick=\"removeTitle(this.parentNode,'employe_names')\"></div>" +
			"</div> </div>";
		//$("#add_person").before(node);
	}
	$("#zonePersons").find("#notExitDoor").html(node);
	add_personNode.appendTo("#zonePersons #notExitDoor");
	$("#zonePersons").focus();
}
//page分页

function pageCtrl(response, pagesize) {
	var pagesize = parseInt(pagesize);
	var data = response.toJSON();
	var page_num = 0;
	if (data.length % pagesize == 0) {
		page_num = data.length / pagesize;
	} else {
		page_num = parseInt(data.length / pagesize) + 1;
	}
	var dt_ = new Array();
	var page_data = new Array();
	for (var i = 0; i < page_num; i++) {
		page_data = [];
		for (var j = i * pagesize; j < data.length; j++) {
			if (j < (i + 1) * pagesize) {
				page_data.push(data[j]);
			} else break;
		}
		dt_.push(page_data);
	}
	return dt_;
}

/*指派工作区域*/

function assignArea(obj) {
	var employe_name = $("#assignArea").data('employe_name');
	//var area_name=$("#areaName").val();
	var psId = $("#ps_id").val();
	var type = "-1";
	var data = {
		ps_id: psId,
		area_type: type,
		employe_name: employe_name,
		title: 'Reassign Area[' + employe_name + ']'
	};
	openSingleSelectAreaDialog(data);
	hidePersonMenu();
}
//选择door

function selectDoor() {
	if (focusNum == 0) {
		focusNum++;
		var psId = $("#ps_id").val();
		openSingleDoorDialog(psId);
	}
}
//打开单个选择的door dialog

function openSingleDoorDialog(psId) {
	require(['views/selectDoorView', 'templates', 'config'],
		function(selectDoorView, templates, config) {
			var data = {
				ps_id: psId
			};
			var ulr = config.queryDoorByPsId.url;
			selectDoorView.render(data, ulr, 'selectDoor', templates.select_door_single);
		});
}
//选择单个area(已用)

function selectAreaSingle(from_id) {
	if (focusNum == 0) {
		focusNum++;
		var type;
		if (from_id) {
			type = $("#" + from_id + " #type").val();
		} else {
			type = $("#type").val();
		}
		var _type = "";
		if (type == 1) {
			_type = 1;
		} else if (type == 4) {
			_type = 3;
		} else if (type == 3) {
			_type = 2;
		} else if (!type) {
			_type = -1;
		}
		var psId = $("#ps_id").val();
		var name = $("#name").val() ? $("#name").val() : "";
		var data = {
			ps_id: psId,
			area_type: _type
		};
		openSingleSelectAreaDialog(data, from_id);
	}


}
//打开单个选择 area dialog

function openSingleSelectAreaDialog(data, from_id) {
	require(['views/selectAreaView', 'templates', 'config'],
		function(selectAreaView, templates, config) {
			var url = config.queryAreaByPsId.url;
			selectAreaView.render(data, url, templates.select_area, from_id);

		})
}
/**
 *choice door
 */

function updata_door_confirm(el) {
	var name = $(el).attr("title");
	var id = $(el).find("#sd_id").val();
	$('.ui-dialog-autofocus').focus();
	backFillDoor_(name, id);
	//$(".ui-popup")[1].remove();
	//$.artDialog.opener.backFillDoor(name,id,'<%=objId %>');
	//$.artDialog.close();
}

function backFillDoor_(names, ids) {
	$("#dock").val(names);
	$("#sd_id").val(ids);
	verifyMgs['v_dock'] = 1;
	showRightVerifyMsg("#modify_storage_layer #dock");

}
//保存location，staging,docks,parking的修改

function modify_location_save(old_data, dialog) {



	var isChanged = ($("#x").val() != old_data.x ||
		$("#modify_data #y").val() != old_data.y ||
		$("#modify_data #height").val() != old_data.height ||
		$("#modify_data #width").val() != old_data.width ||
		$("#modify_data #angle").val() != old_data.angle
	) ? 'true' : 'false';
	if (old_data.type == 1 && $("#modify_data #_dimensional .gis_main_select").val() != old_data.is_three_dimensional) {
		isChanged = 'true';
	}
	var zoneVal = $("#modify_data #zone").val();
	var dockVal = $("#modify_data #dock").val();
	var changed = 'false';
	if ((zoneVal && zoneVal != old_data.area_name) || (dockVal && dockVal != old_data.doorid)) {
		changed = 'true';
	}

	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');
			return;
		}
	}
	var formJson = $("#modify_data").serializeObject();
	formJson['isChanged'] = isChanged;
	formJson['location_type'] = old_data.obj_type;
	var formStr = JSON.stringify(formJson);
	if ($("#modify_data #positionName").val() != old_data.obj_name || $("select[name=is_three_dimensional]").val() != old_data.is_three_dimensional || isChanged == "true" || changed == "true") {
		$.ajax({
			url: '/Sync10/_gis/storageCotroller/updateModifyLayer',
			data: formStr,
			dataType: 'json',
			type: 'post',
			contentType: "application/json; charset=UTF-8",
			beforeSend: function(request) {
				$.blockUI({
					message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
				});
			},
			success: function(data) {
				$.unblockUI();
				if (data && data.flag == 'true') {
					showMessage("Modify succeed ", "succeed");
					dialog.close("update");
					return;
				} else if (data && data.flag == 'authError') {
					showMessage("Auth error!", "error");

				} else if (data && data.flag == 'false') {
					showMessage("System error!", "error");
				} else if (data && data.flag == 'Neo4j Error') {
					showMessage("Modify succeed ", "error");

				}
				alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');

			},
			error: function() {
				$.unblockUI();
				alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');
			}
		});
	} else {
		dialog.close();
	}
}

function dataValid() {
	if ($("#positionName").val().trim() == "") {
		$("#errInfo").html("Name cannot be empty.");
		return false;
	}
	var reg = /^-?\d+(\.\d+)?$/;
	if (!reg.test($("#x").val())) {
		$("#errInfo").html("X position must be number.");
		return false;
	}
	if (!reg.test($("#y").val())) {
		$("#errInfo").html("Y position must be number.");
		return false;
	}
	if (!reg.test($("#height").val())) {
		$("#errInfo").html("X length must be number.");
		return false;
	}
	if (!reg.test($("#width").val())) {
		$("#errInfo").html("Y length must be number.");
		return false;
	}
	if ($("#angle").val() != "") {
		if (!reg.test($("#angle").val())) {
			$("#errInfo").html("Offset angle must be number.");
			return false;
		}
	}
	$("#errInfo").html(" ");
	return true;
}

function webcam_CommitData(data, dialog, formId) {
	if ($("#ip").val() != data.ip || $("#port").val() != data.port || $("#username").val() != data.username || $("#password").val() != data.password || $("#x").val() != data.x || $("#y").val() != data.y || $("#inner_radius").val() != data.inner_radius || $("#outer_radius").val() != data.outer_radius || $("#s_degree").val() != data.s_degree || $("#e_degree").val() != data.e_degree || $("#storage").val() != data.ps_id) {
		var ischanged = 1;
		webcam_updateData(ischanged, data, dialog, formId);
	}

}

function webcam_updateData(ischanged, data, dialog, formId) {
	/*var objIp = document.getElementById("ip");
 var objPort = document.getElementById("port");*/
	if (!$("#" + formId + " #ip").val()) {
		verifyIP($("#" + formId + " #ip"));
	}
	if (!$("#" + formId + " #port").val()) {
		verifyNumber($("#" + formId + " #port"), 'verifyPort');
	}
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			alterDialogDisabled("div[aria-labelledby='title:Webcam_id'] button", false, 'Submit');

			return;
		}
	}
	var dt = $("#" + formId).serializeObject();
	dt['ischanged'] = ischanged;
	dt['ps_id'] = data.ps_id;
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/saveWebcam',
		type: 'post',
		dataType: 'json',
		data: JSON.stringify(dt),
		contentType: "application/json; charset=UTF-8",
		async: false,
		beforeSend: function(request) {
			$.blockUI({
				message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
			});
		},
		success: function(data) {
			$.unblockUI();
			if (data && data.flag == "true") {
				showMessage("Save succeed", "succeed");
				dialog.close("update");
			} else if (data && data.flag == "authError") {
				alterDialogDisabled("div[aria-labelledby='title:Webcam_id'] button", false, 'Submit');
				showMessage("Auth error!", "error");
			}

		},
		error: function() {
			$.unblockUI();
			showMessage("System error", "error");
			alterDialogDisabled("div[aria-labelledby='title:Webcam_id'] button", false, 'Submit');
		}
	});
}


function webcamDemoSubmit(psId, dialog, formId) {
	if (!$("#" + formId + " #ip").val().trim()) {
		verifyIP($("#" + formId + " #ip"));
	}
	if (!$("#" + formId + " #port").val().trim()) {
		verifyNumber($("#" + formId + " #port"), 'verifyPort');
	}
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			alterDialogDisabled("div[aria-labelledby='title:webcam_deme'] button", false, 'Submit');

			return;
		}
	}
	var dt = $("#" + formId).serializeObject();
	dt['pageType'] = 0;
	dt['id'] = 0;
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/saveWebcam',
		type: 'post',
		dataType: 'json',
		contentType: "application/json; charset=UTF-8",
		data: JSON.stringify(dt),
		async: false,
		beforeSend: function(request) {
			$.blockUI({
				message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
			});
		},
		success: function(data) {
			$.unblockUI();
			if (data.flag == "true") {
				dialog.close("add");
			} else {
				alterDialogDisabled("div[aria-labelledby='title:webcam_deme'] button", false, 'Submit');
			}
		},
		error: function() {
			$.unblockUI();
			showMessage("System error", "error");
			alterDialogDisabled("div[aria-labelledby='title:webcam_deme'] button", false, 'Submit');
		}
	});


}

function camp_fillAngle(a, b, formId) {
	$("#" + formId + " #s_degree").val(a);
	$("#" + formId + " #e_degree").val(b);
}

function fillRadius(a, b, formId) {
	$("#" + formId + " #inner_radius").val(a);
	$("#" + formId + " #outer_radius").val(b);
}

function onSelect(eleNode) {
	var p_type = $("#" + eleNode + " input[name='radioType']:checked").val();
	if (p_type == "0") {
		$("#" + eleNode + " #_servers").hide();
		$("#" + eleNode + " #_ip").show();
		$("#" + eleNode + " #_port").show();
	} else if (p_type == "1") {
		$("#" + eleNode + " #_servers").show();
		$("#" + eleNode + " #_ip").hide();
		$("#" + eleNode + " #_port").hide();
	}

}

function setOption() {
	var val = $("select[name='type']").val();
	var lable = "<option value='0'>60*30</option>" +
		"<option value='1'>80*35</option>" +
		"<option value='2'>80*40</option>" +
		"<option value='3'>100*50</option>" +
		"<option value='4'>120*152</option>";
	var letter = "<option value='5'>216*275</option>" +
		"<option value='6'>210*297</option>";
	if (val == 0) {
		$("select[name='size']").html(lable);
	} else if (val == 1) {
		$("select[name='size']").html(letter);
	}
}

function printer_commitData(data, printer_dialog) {
	if ($("#printerInfo_printer_dialog #servers_id").val() != data.servers || $("#printerInfo_printer_dialog #name").val() != data.name || $("#printerInfo_printer_dialog #size").val() != data.size || $("#printerInfo_printer_dialog #x").val() != data.x || $("#printerInfo_printer_dialog #y").val() != data.y || $("#printerInfo_printer_dialog select[name =\"type\"]").val() != data.type || $("#printerInfo_printer_dialog #zone").val() != data.area_name ||
		$("#printerInfo_printer_dialog #storage").val() != data.psId) {
		printer_updateData(data, printer_dialog, "printerInfo");
	}

}

function printer_updateData(data, dialog, formId) {
	var ischanged = 0;
	if ($("#" + formId + " #x").val() != data.x || $("#" + formId + " #y").val() != data.y) {
		ischanged = 1;
	}
	var service = $("#" + formId + " #servers_id").val();
	var p_type = $("#" + formId + " input:radio[name='radioType']:checked").val();
	var servers_name = $("#" + formId + " #servers_name").val();
	var flag1 = false,
		flag2 = false;
	var type = $("#" + formId + " select[name='type']").val();
	var ip = $("#" + formId + " #ip").val();
	var port = $("#" + formId + " #port").val();
	var x = $("#" + formId + " #x").val();
	var y = $("#" + formId + " #y").val();
	var size = $("#" + formId + " select[name='size']").val();
	var area_id = $("#area_id").val();
	var oldservers = data.ervers;
	var oldip = data.ip;
	var oldport = data.port;
	var oldName = data.name;
	var name = $("#" + formId + " #name").val();
	if (name == '') {
		verifyName($("#" + formId + " #name"));
	}

	if (p_type == 0) {
		if (ip == '') {
			verifyIP($('#' + formId + ' #ip'));
		}
		if (port == '') {
			verifyNumber($('#' + formId + ' #port'), 'verifyPort');

		}
		if (data.p_type != p_type || data.area_id != area_id ||
			data.size != size || data.y != y || data.x != x ||
			data.type != type || oldip != ip || oldport != port) {
			flag1 = true;
		}
	} else {
		var obj_parent = $("#" + formId + " #servers_name").parent();
		var obj_next = $("#" + formId + " #servers_name").next();
		if (service == "") {
			obj_parent.removeClass('has-success');
			obj_next.removeClass('glyphicon-ok');
			obj_parent.addClass('has-error');
			obj_next.addClass('glyphicon-remove');
			verifyMgs['VServersName'] = 0;
		} else {
			obj_parent.removeClass('has-error');
			obj_next.removeClass('glyphicon-remove');
			obj_parent.addClass('has-success');
			obj_next.addClass('glyphicon-ok');
			verifyMgs['VServersName'] = 1;
		}

		if (data.p_type != p_type || data.area_id != area_id ||
			data.size != size || data.y != y || data.x != x ||
			data.type != type || oldservers != service) {
			flag1 = true;
		}
	}
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			if ($("#" + formId + " input:hidden[name='p_id']").val()) {
				alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Submit');
			} else {
				alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button", false, 'Submit');
			}

			return;
		}
	}
	if (oldName != $("#" + formId + " #name").val()) {
		flag2 = true;
	}


	if (!(flag1 || flag2)) {
		if ($("#" + formId + " input:hidden[name='p_id']").val()) {
			alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Submit');
		} else {
			alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button", false, 'Submit');
		}
		showMessage(" Didn't any change propertys", "error");

	} else {

		var pageType = data.pageType;
		var dt = $("#" + formId).serializeObject()
		dt['ps_id'] = data.ps_id;
		dt['ischanged'] = ischanged;
		dt['pageType'] = data.pageType;
		dt['p_type'] = p_type;
		dt['servers_name'] = servers_name;
		$.ajax({
			url: '/Sync10/_gis/printerInfoCotroller/savePrinter',
			type: 'post',
			dataType: 'json',
			data: JSON.stringify(dt),
			contentType: "application/json; charset=UTF-8",
			async: false,
			beforeSend: function(request) {
				$.blockUI({
					message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
				});
			},
			success: function(data) {
				$.unblockUI();
				if (data && data.flag == "true") {
					showMessage("Save succeed", "succeed");
					var flag = "";
					var ps_id = $("#" + formId + " input:hidden[name='ps_id']").val();
					if ($("#" + formId + " input:hidden[name='p_id']").val()) {
						flag = "update"
					} else {
						flag = "add";
						reSetObjectDraggable(7, ps_id, data.id);
					}
					dialog.close(flag);

				} else if (data && data.flag == "exist") {
					if ($("#" + formId + " input:hidden[name='p_id']").val()) {
						alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Submit');
					} else {
						alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button", false, 'Submit');
					}
					showMessage("printerName is exist", "error");
				} else if (data && data.flag == "authError") {
					if ($("#" + formId + " input:hidden[name='p_id']").val()) {
						alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Submit');
					} else {
						alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button", false, 'Submit');
					}
					showMessage("Auth error!", "error");
				} else {
					if ($("#" + formId + " input:hidden[name='p_id']").val()) {
						alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Submit');
					} else {
						alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button", false, 'Submit');
					}
					showMessage("Save failed!", "error");
				}
			},
			error: function() {
				$.unblockUI();
				showMessage("System error", "error");
				if ($("#" + formId + " input:hidden[name='p_id']").val()) {
					alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Submit');
				} else {
					alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button", false, 'Submit');
				}

			}
		});
	}

}

function printer_closeWindow(printer_data, flag) {
	var pageType = printer_data.pageType;
	if (pageType == "0") {
		clearSinglePrinter(printer_data.ps_id, printer_data.p_id);
	}
	var data = new Array();
	data.push(printer_data);
	reLoadPrinter(printer_data.ps_id, flag, data);
	reSetObjectDraggable(7, printer_data.ps_id, printer_data.p_id);
}

function printer_doDelete(printer_data, printer_dialog) {
	require(['art_Dialog/dialog-plus'], function(artDialog) {
		var d = artDialog({
			title: 'Notify',
			lock: true,
			backdropOpacity: 0.3,
			icon: 'question',
			content: "<span style='font-weight:bold;'>Delete [" + printer_data.name + "] ?</span>",
			button: [{
				value: 'YES',
				callback: function() {

					deletePrinter(printer_data, printer_dialog);
				},
				autofocus: true
			}, {
				value: 'NO',
				callback: function() {

					alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Delete');
				}
			}]
		}).showModal();
	})
}

function deletePrinter(printer_data, printer_dialog) {
	$.ajax({
		url: '/Sync10/_gis/printerInfoCotroller/deletePrint',
		type: 'get',
		dataType: 'json',
		data: {
			"ps_id": printer_data.ps_id,
			"p_id": printer_data.p_id
		},
		async: true,
		beforeSend: function(request) {
			$.blockUI({
				message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
			});
		},
		success: function(data) {
			$.unblockUI();
			if (data && data.flag == "true") {
				showMessage("Delete succeed ", "succeed");
				printer_dialog.close('delete');
			} else {
				alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Delete');
				showMessage("Delete fail ", "error");
			}


		},
		error: function() {
			$.unblockUI();
			showMessage("System error", "error");
			alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button", false, 'Delete');
		}
	});
}

/**
 * 修改printer server (已用)
 */

function add_printerServers(ps_id, formId) {

	if (ps_id) {
		require(['views/printerServersView', 'templates', 'config'], function(printerServersView, templates, config) {
			var url = config.qureyprintServers.url;
			var data;
			currentPsId = ps_id;
			printerServersView.render({
				el: '#tab4'
			});


		});
	} else {
		if (focusNum == 0) {
			focusNum++;
			require(['views/selectPrinterServiersView'], function(selectPrinterServiersView) {
				currentPsId = $("#ps_id").val();
				selectPrinterServiersView.render(formId);
			});

		}
	}

	/* var url='<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/printers_list.html?ps_id=<%=psId%>';
     $.artDialog.open(url, {title: "Choose Printer Servers ",width:'50%',height:'60%', lock: false,opacity: 0.3,fixed: true,id:"create_servers"})*/
}
/**
 * 回填printer server （已用）
 */

function backFillPrinterServer(s_id, s_name, formId) {
	$("#" + formId + " #servers_name").val(s_name);
	$("#" + formId + " #servers_id").val(s_id);
}
/**
 *选中printer server
 */

function choosePrinterServer(s_id, s_name, formId) {
	backFillPrinterServer(s_id, s_name, formId);


}
/**
 *增添pinter server
 */

function addPrinterServer(ps_id) {
	focusNum = 1;
	require(['views/addPrinterServerView', 'templates', 'config'], function(addPrinterServerView, templates, config) {
		var url = config.storageModel.url;
		addPrinterServerView.render(url, templates.add_printer_server, ps_id);
	});
}
/**
 *Docks,Parking,选择zone时对area的选中操作(已用)
 */

function selectArea_confirm(el, from_id) {
	var name = $(el).attr("title");
	var id = $(el).find("#area_id").val();
	backFillArea(name, id, from_id);
}
/**
 *回填aread，到Docks,Parking中的zone（已用）
 */

function backFillArea(names, ids, from_id) {
	$('#' + from_id + " #zone").val(names);
	$('#' + from_id + " #area_id").val(ids);
	showRightVerifyMsg('#' + from_id + " #zone");
	//
}

//显示监控画面（已用）

function showWebcam(data) {
	var webCam = data;
	var port = data.port;
	/*if(webCam){
		var rtsp = getWebcamRtsp(webCam);
		require(['views/showWebcamView','templates'],
			function(showWebcam,templates){
				var data={rtsp:rtsp,port:port};
				
				//showWebcam.render(data,templates.showWebcam);
		})
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/web_cam.html?rtsp='+rtsp;
		$.artDialog.open(url, {title: "Webcam Screen["+port+"]",width:'860px',height:'660px', lock: false,opacity: 0.3,fixed: true});
	}*/
	var rtsp = getWebcamRtsp(webCam);

	//var urls="rtsp://admin:12345@174.127.17.156:9606/h264/ch1/main/av_stream";
	__playcom.add({
		url: rtsp,
		title: port
	});

	/*//require(["oso.lib/videolanrtsp/rtspPlay",'templates'],
		function(rtspPlay,templates){
			var data={rtsp:rtsp,port:port};
			
			//showWebcam.render(data,templates.showWebcam);
	})
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/web_cam.html?rtsp='+rtsp;
	$.artDialog.open(url, {title: "Webcam Screen["+port+"]",width:'860px',height:'660px', lock: false,opacity: 0.3,fixed: true});
	*/
}
//生成摄像头rtsp地址（已用）

function getWebcamRtsp(webCam) {
	var rtsp = "rtsp://";
	if (webCam.user && webCam.user != "" && webCam.password && webCam.password != "") {
		rtsp += webCam.user + ":" + webCam.password + "@";
	}
	rtsp += webCam.ip + ":" + webCam.port + "/h264/ch1/main/av_stream";
	return rtsp;
}
//--------------------------------------webcam显示影像方法-------------------------------------------------------------
/**
 *webcam显示影像方法
 */

function isInsalledFFVLC() {
	var numPlugins = navigator.plugins.length;
	var plu = "";
	for (i = 0; i < numPlugins; i++) {
		plugin = navigator.plugins[i];
		plu += plugin.name + "\n";
		if (plugin.name.indexOf("VideoLAN") > -1 || plugin.name.indexOf("VLC") > -1) {
			return true;
		}
	}
	// alert(plu);
	return false;
}

function alertInfo(data) {
	var flag = isInsalledFFVLC();
	//alert(flag);
	var vlc = document.getElementById("vlc");
	if (flag) {
		//var id=vlc.playlist.add(data.rtsp);
		//vlc.playlist.playItem(id);
	} else {
		$("#vlc").remove();
		$("#playerInfo").show();
	}
}

function getVlcPlayer() {
	var url = "http://www.videolan.org/vlc";
	window.open(url);
}

function addPrinter_server_initPs(ps_id_) {
	var ps_id = ps_id_;
	var varPsId = ps_id * 1;
	if (varPsId > 0) {
		$("#addForm #storage").val(ps_id_);
		$("#addForm #storage").attr("disabled", "true");
	}
}

function addPrinter_server_selectZone() {
	if (focusNum == 1) {
		focusNum++;
		var storage = $("#addForm #storage").val();
		require(['views/selectZoneView', 'templates', 'config'], function(selectZoneView, templates, config) {
			var data = {
				ps_id: storage
			};
			var url = config.queryAreaByPsId.url;
			selectZoneView.render(data, url, templates.select_zone);
		});
	}


}

function select_zone_confirm() {
	var ids = "";
	var names = "";
	var ts = $("#exitTitle #area_id");
	for (var i = 0; i < ts.length; i++) {
		ids += $(ts[i]).val() + ",";
		names += $(ts[i].parentNode.parentNode).attr("title") + ",";
	}
	if (ids != "") {
		ids = ids.substr(0, ids.length - 1);
		names = names.substr(0, names.length - 1);
	}
	backFillZone(names, ids);

}

function backFillZone(names, ids) {
	$("#add_printer_server_zone").val(names);
	$("#add_printer_server_area_id").val(ids);
}

function addServer(ps_id, dialog) {
	if (validate()) {
		ajaxAddServer(ps_id, dialog);
	} else {
		alterDialogDisabled("div[aria-labelledby='title:addPrinterServer'] button", false, 'Submit');
	}
}
//添加打印机服务

function ajaxAddServer(ps_id, dialog) {
	var title = $("#addForm #storage").find("option:selected").text();
	var psId = $("#addForm #storage").val();
	$.ajax({
		url: systenFolder + 'action/checkin/PrintServerSetOperAction.action',
		data: $("#addForm").serialize() + "&title=" + title + "&ps_id=" + psId,
		dataType: 'json',
		beforeSend: function(request) {
			//$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success: function(data) {
			//$.unblockUI();
			if (data.ret * 1 == 1) {
				dialog.close().remove();
				focusNum = 0;
				add_printerServers(ps_id, 'addprinter');
				//windowClose();
			} else {
				//if(data.err * 1 == '<%= BCSKey.AndroidPrintServerNameExitsException%>'*1){
				showMessage("Server Name is Exit.", "alert");
				alterDialogDisabled("div[aria-labelledby='title:addPrinterServer'] button", false, 'Submit');
				$("#printer_server_name").val("").foucs();
				//}
			}
		},
		error: function() {
			//$.unblockUI();
			alterDialogDisabled("div[aria-labelledby='title:addPrinterServer'] button", false, 'Submit');
			showMessage("System Eror,Try Later.", "error")
		}
	})

}
//是否删除打印机提示信息

function deletePServer(id, serverName) {
	require(['jquery', 'art_Dialog/dialog-plus'],
		function($, dialog) {
			var ps_id = $("#ps_id_pServer").val();
			var d = dialog({
				title: 'Notify',
				lock: true,
				backdropOpacity: 0.3,
				icon: 'question',
				content: "<span style='font-weight:bold;'>Delete [" + serverName + "] ?</span>",
				button: [{
					value: 'YES',
					callback: function() {
						ajaxDeletePServer(id, ps_id);
					},
					focus: true
				}, {
					value: 'NO'
				}]
			}).showModal();
		});

}
//是否删除打印机提示信息

function dialogDeletePServer(id, serverName) {
	require(['jquery', 'backbone', 'templates', 'art_Dialog/dialog-plus', 'views/printerServersView', 'config', ],
		function($, backbone, templates, dialog, printerServersView, config) {
			var ps_id = currentPsId;
			var d = dialog({
				title: 'Notify',
				lock: true,
				backdropOpacity: 0.3,
				icon: 'question',
				content: "<span style='font-weight:bold;'>Delete [" + serverName + "] ?</span>",
				button: [{
					value: 'YES',
					callback: function() {
						deletePServerAjax(id, ps_id, this);
						return false;
					},
					focus: true
				}, {
					value: 'NO'
				}]
			}).showModal();
			d.addEventListener('close', function() {
				printerServersView.render({
					el: "#tab4"
				});
			});
		});

}
//删除打印机服务

function ajaxDeletePServer(id, ps_id) {
	$.ajax({
		url: systenFolder + 'action/checkin/PrintServerSetOperAction.action',
		dataType: 'json',
		data: 'Method=Delete&printer_server_id=' + id,
		beforeSend: function(request) {},
		success: function(data) {
			if (data != null) {
				add_printerServers(ps_id);
			} else {
				showMessage("System Error", "error");
			}
		},
		error: function() {
			showMessage("System Error,Try Later.", "error");
		}
	})
}
//删除打印机服务

function deletePServerAjax(id, ps_id, dialog) {
	$.ajax({
		url: systenFolder + 'action/checkin/PrintServerSetOperAction.action',
		dataType: 'json',
		data: 'Method=Delete&printer_server_id=' + id,
		beforeSend: function(request) {},
		success: function(data) {
			if (data.ret == 1) {
				showMessage("Delete succeed", "succeed");
				dialog.close();
			}
		},
		error: function() {
			showMessage("System Error,Try Later.", "error");
		}
	})
}
//根据选择类型在地图中添加storage轮廓

function inMapsOptionStorage(nodeObj) {
	var value_ = nodeObj.value;
	var ps_id = $("#maintain_storage_add_form select[name='ps_id']").val();

	if (ps_id == 0) {
		$("#maintain_storage_add_form select[name='ps_id']").parent().addClass('has-error');
		$("#maintain_storage_add_form select[name='ps_id']").next().addClass('glyphicon-remove');
		$(nodeObj).removeAttr('checked');
		return;
	} else {
		$("#maintain_storage_add_form select[name='ps_id']").parent().removeClass('has-error');
		$("#maintain_storage_add_form select[name='ps_id']").next().removeClass('glyphicon-remove');
		$("#maintain_storage_add_form .maintain_add_storage_latLngs").html('');
		if (value_ == 1) {
			jsmap.setMapTool('move_map');
			inputPointNum();
			//$("#hand_fill_point").show();
			jsmap.clreaPolygon();
			jsmap.clreaLabelMarker();
		} else {
			jsmap.clreaPolygon();
			jsmap.clreaLabelMarker();
			$("#hand_fill_point").hide();
			jsmap.setMapTool('measure_area');
		}
	}

}
//弹出storage 点数输入框

function inputPointNum() {
	require(['jquery', 'art_Dialog/dialog-plus'], function($, dialog) {
		var d = dialog({
			title: '请输入点个数',
			backdropOpacity: 0.2,
			content: '<input type="text" id="inputPointNum">',
			button: [{
				value: 'Confirm',
				callback: function() {
					var pointNum = $("#inputPointNum").val();
					if (pointNum) {
						addMaintainStorageLatLng(pointNum);
					}
				},
				autofocus: true
			}, {
				value: 'Cancel'
			}]

		}).showModal();
	})
}
//添加maintain storage

function addMaintainStorageLatLng(storageLatLngDt) {
	var latLngCode = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
		'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
	];
	//var htmlStr=$(".maintain_add_storage_latLngs").html('');
	var addHtml = '';
	var before_latLngStr = "";
	var latLngStr = "";
	if (!isNaN(storageLatLngDt)) {
		before_latLngStr = 'num';
	}
	var length = isNaN(storageLatLngDt) ? storageLatLngDt.length : storageLatLngDt;
	var lastPositionCode = "",
		secondPositionCode = "",
		firstPosition, secondPosition, lastPosition, max_X, max_Y;
	for (var i = 0; i < length; i++) {
		if (isNaN(storageLatLngDt)) {
			if (i == 0) {
				firstPosition = storageLatLngDt[i];
				latLngStr = storageLatLngDt[i].lat() + ',' + storageLatLngDt[i].lng();
			} else {
				if (i == 1) {
					secondPosition = storageLatLngDt[i];
				}
				lastPosition = storageLatLngDt[i];
				before_latLngStr = storageLatLngDt[i - 1].lat() + ',' + storageLatLngDt[i - 1].lng();
				latLngStr = storageLatLngDt[i].lat() + ',' + storageLatLngDt[i].lng();
			}
			if (before_latLngStr != latLngStr) {
				jsmap.drawLabelMarker(storageLatLngDt[i], latLngCode[i], jsmap.getMap());
			}
		}
		if (i == 1) {
			secondPositionCode = latLngCode[i];
		}
		if (before_latLngStr != latLngStr) {
			lastPositionCode = latLngCode[i];
			addHtml += '<tr style="height:40px;">' +
			/*'<td align="right" valign="middle" class="STYLE1 STYLE2" >Position_'+latLngCode[i]+'</td>'+*/
			'<td align="left" valign="middle" ><div class="input-group form-group has-feedback"><span class="input-group-addon">' + latLngCode[i] +
				'</span><input name="latLng[' + i + ']"+ type="text" class="form-control input-mStorage" id="title_' + latLngCode[i] + '" value="' + latLngStr + '" onblur="verifyLatLng_(this)">' + '<span class="glyphicon form-control-feedback"  style="top:0;"></span></div></td>' +
				'</tr>';
		}
	}
	if (isNaN(storageLatLngDt)) {
		max_X = computeDistanceBetween_(firstPosition, secondPosition);
		max_Y = computeDistanceBetween_(firstPosition, lastPosition);
	}

	addHtml += '<tr style="height:40px;">' +
	/*'<td align="right" valign="middle" class="STYLE1 STYLE2" >Max_x</td>'+*/
	'<td align="left" valign="middle" class="mNotify"><div class="input-group form-group has-feedback" ><span class="input-group-addon">A' + secondPositionCode + '</span>' +
		'<input name="maxX" type="text" class="form-control input-mStorage" id="title" value="' + max_X + '" onblur="verifyNumber(this,"\maxX"\)">' +
		'<span class="glyphicon form-control-feedback"  style="top:0;"></span>' +
		'<span class="input-group-addon">ft</span></div></td></tr>' +
		'<tr>' +
	/*'<td align="right" valign="middle" class="STYLE1 STYLE2" >Max_y</td>'+*/
	'<td align="left" valign="middle" class="mNotify"><div class="input-group" ><span class="input-group-addon">A' + lastPositionCode + '</span>' +
		'<input name="maxY" type="text" class="form-control input-mStorage" id="title" value="' + max_Y + '" onblur="verifyNumber(this,"\maxY"\)">' +
		'<span class="glyphicon form-control-feedback"  style="top:0;"></span>' +
		'<span class="input-group-addon">ft</span></div></td></tr>';


	$(".maintain_add_storage_latLngs").html(addHtml);
	$("#maintain_addStorage_btn").removeClass('disabled');
	$("#maintain_addStorage_cancel").removeClass('disabled');
	/*	$("#maintain_addStorage_btn").css('display','initial');
	$("#maintain_addStorage_cancel").css('display','initial');*/
}



//定位Maintain storage

function locationStorage_() {
	var latLngStr = $("#locationStorage").val();
	if (latLngStr) {
		jsmap.seachAddress(latLngStr);
		/*var latLng=jsNewLatLng(latLngStr.split(',')[0],latLngStr.split(",")[1]);
		jsmap.getMap().setCenter(latLng);
		jsmap.getMap().setZoom(17);*/
	}

}

// 保存新添加的storage

function addMaintainStorage() {
	var storage_dt = {};
	var latLngs = new Array();
	$.each($('#maintain_storage_add_form').serializeArray(), function(index, field) {
		var latLng = {};
		if (field.name.indexOf('latLng') > -1) {
			//latLngs.push(field.value);
			if (field.value) {
				latLng["latlng"] = field.value;
				latLngs.push(latLng);
			} else {
				verifyLatLng_($("input[name='" + field.name + "']"));
			}

		} else if (field.name == 'parentid') {
			storage_dt[field.name] = 0;
		} else if (field.name == 'ps_id') {
			storage_dt[field.name] = parseFloat(field.value);
		} else if (field.name == 'option') {

		} else {
			if (field.value) {
				storage_dt[field.name] = field.value;
			} else {
				verifyNumber($("input[name='" + field.name + "']"), field.name);
			}
		}
	})
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			return;
		}
	}
	storage_dt['latlngs'] = latLngs;
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/saveStorage',
		dataType: 'json',
		data: JSON.stringify(storage_dt),
		type: 'POST',
		async: false,
		contentType: "application/json; charset=UTF-8",
		beforeSend: function(request) {},
		success: function(data) {
			showMessage("Save success", "alert");
			var latlngs_ = "";
			for (var i = 0; i < latLngs.length; i++) {
				if (latlngs_) {
					latlngs_ += " " + latLngs[i].latlng.split(',')[1] + "," + latLngs[i].latlng.split(',')[0];
				} else {
					latlngs_ = latLngs[i].latlng.split(',')[1] + "," + latLngs[i].latlng.split(',')[0];
				}
			}
			latlngs_ += " " + latLngs[0].latlng.split(',')[1] + "," + latLngs[0].latlng.split(',')[0];
			drawStorageBase(data.ps_id, {
				latlng: latlngs_
			});
			jsmap.isFitBounds["ps_id"] = data.ps_id;
			jsmap.clreaPolygon();
			jsmap.clreaLabelMarker();
		}
	});
}
//取消新增storage

function addStorageCancel() {
	$(".maintain_add_storage_latLngs").html('');
	$("#maintain_addStorage_btn").addClass("disabled");
	$("#maintain_addStorage_cancel").addClass("disabled");
	jsmap.clreaPolygon();
	jsmap.clreaLabelMarker();
	jsmap.setMapTool('measure_area');
}
//验证storage顶点坐标格式

function verifyLatLng_(obj) {
	verifyLatLng(obj, obj.name);
}
//选择画storage轮廓时，在地图上的操作

function storageInMapOPtion(tool) {
	if ($(".maintain_add_storage_info").length > 0 && tool == "measure_area") { //添加仓库
		if ($('input:radio[name="op"]:checked').val() == 2) {
			addMaintainStorageLatLng(jsmap.mapToolPositionList);
			jsmap.drawPolygon(jsmap.mapToolPositionList);
		}

		setMapTool('move_map');
	}
}
//计算两经纬度之间的长度（单位为fx）

function computeDistanceBetween_(latlng1, latlng2) {
	return (computeDistanceBetween(latlng1, latlng2) * 3.28083).toFixed(2);
}
//拖拽storage 第一个顶点，第二个顶点及最后一个顶点时给Max_X,Max_Y中的其一或者两者都重新赋值

function backMaxVal(obj, markers) {
	var title = obj.title;
	var maxX, maxY;
	if (obj.title == 'A') {
		maxX = computeDistanceBetween_(obj.position, markers[1].maker_.position);
		maxY = computeDistanceBetween_(obj.position, markers[markers.length - 1].maker_.position);
	} else if (title == markers[1].maker_.title) {
		maxX = computeDistanceBetween_(markers[0].maker_.position, obj.position);
	} else if(title==markers[markers.length-1].maker_.title){
		maxY = computeDistanceBetween_(markers[0].maker_.position, obj.position);
	}
	if (maxX) {
		if ($("#maintain_storage_add_form input[name='maxX']").length == 0) {
			$("#alterStorageForm input[name='maxX']").val(maxX);
		} else {
			$("#maintain_storage_add_form input[name='maxX']").val(maxX);
		}

	}
	if (maxY) {
		if ($("#maintain_storage_add_form input[name='maxY']").length == 0) {
			$("#alterStorageForm input[name='maxY']").val(maxY);
		} else {
			$("#maintain_storage_add_form input[name='maxY']").val(maxY);
		}

	}
}
//-------------------------------------------------------------

function pageCtrlforJson(data, pagesize) {
	var page_num = 0;
	if (data.length % pagesize == 0) {
		page_num = data.length / pagesize;
	} else {
		page_num = parseInt(data.length / pagesize) + 1;
	}
	var dt_ = new Array();
	var page_data = new Array();
	for (var i = 0; i < page_num; i++) {
		page_data = [];
		for (var j = i * pagesize; j < data.length; j++) {
			if (j < (i + 1) * pagesize) {
				page_data.push(data[j]);
			} else break;
		}
		dt_.push(page_data);
	}
	return dt_;
}

function arrayPush(Array, element) {
	var key = Array.indexOf(element);
	if (key == -1) {
		Array.push(element);
	} else {
		delete Array[key];
		Array.push(element);
	}
}
// -------------------------添加layer------------------

function typeChange(type) {
	var dock_obj_parent = $("#modify_storage_layer  #dock").parent();
	var dock_obj_next = $("#modify_storage_layer  #dock").next();
	var title_obj_parent = $("#modify_storage_layer  #title").parent();
	var title_obj_next = $("#modify_storage_layer  #title").next();
	var zone_obj_parent = $("#modify_storage_layer  #zone").parent();
	var zone_obj_next = $("#modify_storage_layer  #zone").next();
	removeErrorVerifyMsg(dock_obj_parent, dock_obj_next);
	removeRightVerifyMsg(dock_obj_parent, dock_obj_next);
	removeErrorVerifyMsg(title_obj_parent, title_obj_next);
	removeRightVerifyMsg(title_obj_parent, title_obj_next);
	removeErrorVerifyMsg(zone_obj_parent, zone_obj_next);
	removeRightVerifyMsg(zone_obj_parent, zone_obj_next);
	if (type == "1" || type == "3" || type == "4") {

		$("#modify_storage_layer #_zone").show();
		$("#modify_storage_layer #_title").hide();
		$("#modify_storage_layer #_dock").hide();
		$("#modify_storage_layer #set_l").hide();
		$("#modify_storage_layer #set_location").hide();
		$("#_dimensional").hide();
		//clearAutoLayer();
	}
	if (type == "2") {
		$("#modify_storage_layer #_zone").hide();
		$("#modify_storage_layer #_title").hide();
		$("#modify_storage_layer #_dock").show();
		$("#modify_storage_layer #_dock>input").val('')
		$("#modify_storage_layer #set_l").hide();
		$("#modify_storage_layer #set_location").hide();
		$("#_dimensional").hide();
		//clearAutoLayer();
	}
	if (type == "5") {
		$("#modify_storage_layer #dock_con").hide();
		$("#modify_storage_layer #dock_del").hide();
		$("#modify_storage_layer #_zone").hide();
		$("#modify_storage_layer #_title").show();
		$("#modify_storage_layer #_dock").show();
		$("#modify_storage_layer #set_location").hide();
		$("#modify_storage_layer #set_l").show();
		$("#modify_storage_layer input[name='set_local']").attr("checked", false);

		$("#_dimensional").hide();


	}
	if (type == "1") {
		$("#_dimensional").show();
	}
	if (type == "-1") {
		$("#_zone").hide();
		$("#_title").hide();
		$("#_dock").hide();
		$("#set_l").hide();
		$("#_dimensional").hide();
	}
}
//
//选择title

function addLayerSelectTitle() {
	if (focusNum == 0) {
		focusNum++;
		var psId = $("#modify_storage_layer #storage_id").val();
		var titles=$("#modify_storage_layer #title").val();
		openStorageTitleSelectDialog(psId,'',titles);
	}
}
//
//回填title

function backFillTitle(dialog) {
	var datas = selectedTitleData();
	$("#modify_storage_layer #title").val(datas.names);
	$("#modify_storage_layer #title_id").val(datas.ids);
}
//选择door

function addLayerSelectDoor() {
	if (focusNum == 0) {
		focusNum++;
		var type = $("#modify_storage_layer input[name='type']:checked").val();
		var psId = $("#modify_storage_layer #storage_id").val();
		if (type == 5) {
			openDocksDialog(psId);
		}
		if (type == 2) {
			openSingleDoorDialog(psId);
		}
	}
}
//回填door

function backFillDoors(names, ids) {
	$('#modify_storage_layer #dock').val(names);
	$('#modify_storage_layer #sd_id').val(ids);
}

//勾选 Draw Child Layer 显示子操作输入框

function setLocation() {
	$("#modify_storage_layer #set_location").slideToggle("slow");
}
//创建child Layer

function createLocation() {
	var x = $("#modify_storage_layer #x").val();
	var y = $("#modify_storage_layer #y").val();
	var x_length = $("#modify_storage_layer #height").val();
	var y_length = $("#modify_storage_layer #width").val();
	var angle = $("#modify_storage_layer #angle").val();
	var h = $("#modify_storage_layer #horizontally").val();
	var z = $("#modify_storage_layer #vertically").val();
	var psId = $("#modify_storage_layer #storage_id").val();
	var loc_name = $("#modify_storage_layer #loc_name").val();
	var v_interval = $("#modify_storage_layer #v_interval").val();
	var h_interval = $("#modify_storage_layer #h_interval").val();
	var verify = /^(-?\d+)(\.\d+)?$/;
	if (verifyMgs['verifyX'] == 0 || verifyMgs['verifyX'] == 2 || verifyMgs['verifyY'] == 0 || verifyMgs['verifyY'] == 2 || verifyMgs['verifyWidth'] == 0 || verifyMgs['verifyWidth'] == 2 || verifyMgs['verifyHeight'] == 0 || verifyMgs['verifyHeight'] == 2 || verifyMgs['verifyAngle'] == 0 || verifyMgs['verifyAngle'] == 2 || verifyMgs['verifyVer'] == 0 || verifyMgs['verifyVer'] == 2 || verifyMgs['verifyHor'] == 0 || verifyMgs['verifyHor'] == 2) {
		return;
	}
	autoCreatRectangles(psId, x, y, x_length, y_length, angle, h, z, loc_name, h_interval, v_interval);
	$("#reverseName_").show();
}
//验证name

function verifyName_(obj, psId, type1, oldName, op_flag) {
	var type = type1;
	if (!type1) {
		type = $("#modify_storage_layer input[name='type']:checked").val();
	}
	if (type) {
		verifyName(obj, psId, type, oldName, op_flag);
	}

}
//提交新添加的layer

function commitAddLayer(dialog) {
	var dialog = dialog;
	var type = $("#modify_storage_layer input[name='type']:checked").val();
	var layers = "area";
	var aotulocations = [];
	var child_type = ""
	//var checkbox =$("#modify_storage_layer input[name='set_local']").prop("checked");
	/*if(type==5 &&checkbox){
		aotulocations=getAotuLocations();
	    child_type=$("#modify_storage_layer input[name='child_type']:checked").val();
		var layer="";
		if(child_type==1){
			layer=",location";
		}
		if(child_type==2){
			layer=",staging";
		}
		if(child_type==3){
			layer=",docks";
		}
		if(child_type==4){
			layer=",parking";
		}
		layers+=layer;
	    
	}*/
	if (type) {
		if (type == 1 || type == 3 | type == 4) {
			if (!$("#modify_storage_layer #zone").val()) {
				showErrorVerifyMsg("#modify_storage_layer #zone", 'Zone is empty!');
				verifyMgs['v_zone'] = 0;

			} else {
				verifyMgs['v_zone'] = 1;
				showRightVerifyMsg("#modify_storage_layer #zone");
			}
		} else if (type == 2) {
			if (!$("#modify_storage_layer #dock").val()) {
				showErrorVerifyMsg("#modify_storage_layer #dock", 'Dock id empty!');
				verifyMgs['v_dock'] = 0;

			} else {
				verifyMgs['v_dock'] = 1;
				showRightVerifyMsg("#modify_storage_layer #dock");
			}
		} else if (type == 5) {
			verifyMgs['v_dock'] = 1;
		}
	} else {
		showMessage("Please select layer type!", "error");
		alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
		return;
	}

	var name = $("#modify_storage_layer #name").val();
	if ($.isEmptyObject(name)) {
		verifyName($("#modify_storage_layer #name"));
	}
	if (name.indexOf("_") > -1) {
		showErrorVerifyMsg("#modify_storage_layer #name", 'Name cannot contain "_"');
		verifyMgs['verifyName'] = 0;
	}
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
			return;
		}
	}
	if (type == 1 || type == 3 | type == 4) {
		if (!$("#modify_storage_layer #zone").val()) {
			if (verifyMgs['v_zone'] == 0) {
				alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
				return;
			}
		}
	} else if (type == 2) {
		if (verifyMgs['v_dock'] == 0) {
			alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
			return;
		}
	}
	var polygon = jsmap.storageDemoLayer["drag"];
	var paths = polygon.getPath();
	var latlngStr = pathTolatlngStr(paths);
	var formJson = $("#modify_storage_layer").serializeObject();
	formJson['location_type'] = type;
	formJson['aotulocations'] = aotulocations;
	var is_three_dimensional=formJson['is_three_dimensional'];
	delete formJson['child_type'];
	//formJson['child_type']=child_type;
	formJson['latlng'] = latlngStr;
	var formStr = JSON.stringify(formJson);
	//var data = $("#modify_storage_layer").serialize()+"&location_type="+type+"&aotulocations="+aotulocations+"&child_type="+child_type+"&latlng="+latlngStr;
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/saveModifyLayer',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data: formStr,
		cache: false,
		contentType: "application/json; charset=UTF-8",
		async: false,
		beforeSend: function(request) {
			$.blockUI({
				message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
			});
		},
		success: function(data) {
			$.unblockUI();
			if (data && data.flag == "true") {
				showMessage("Save succeed", "succeed");
				var data_ = {
					type: type,
					position_id: data.returnvalue
					,is_three_dimensional:is_three_dimensional
				};
				dialog.close(data_);
				return;
			} else if (data && data.flag == "false") {
				if (data.returnvalue == 0) {
					showErrorVerifyMsg($("#modify_storage_layer #name"), 'Name can not repeat!');
				} else if (data.returnvalue == "-1") {
					clearAutoLayer();
					showErrorVerifyMsg($("#modify_storage_layer #loc_name"), 'AutolocationName can not repeat!');
				}

			} else if (data && data.flag == "authError") {
				showMessage("Auth error!", "error");

			} else if (data && data.flag == "neo4jError") {
				showMessage("Neo4j Error!", "error");

			}

			alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$.unblockUI();
			var reqJson = JSON.parse(XMLHttpRequest.responseText);
			if (reqJson.message == "java.lang.Exception: -1") {
				showErrorVerifyMsg($("#modify_storage_layer #loc_name"), 'Duplicate name!')
			} else if (reqJson.message == "java.lang.Exception: Add neo4j nodes error") {
				showMessage("Neo4j Error!", "error");
			}else if(reqJson.message == "java.lang.Exception: 0"){
				showMessage("Name is repeat!","error");
			}
			alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
		}
	});
}
/**
 *提交批量添加layer
 */

function addMutliLayer(dialog, dialogId) {
	var type = $("#modify_storage_layer input[name='child_type']:checked").val();
	var aotulocations = getAotuLocations();
	if (type == 1 || type == 3 || type == 4) {
		if (!$("#modify_storage_layer #zone").val()) {
			showErrorVerifyMsg("#modify_storage_layer #zone", 'Zone is empty!');
			verifyMgs['v_zone'] = 0;

		} else {
			showRightVerifyMsg("#modify_storage_layer #zone");
		}
	} else if (!type) {
		showMessage("Please select layer type!", "error");
		alterDialogDisabled("div[aria-labelledby='title:" + dialogId + "'] button", false, 'Submit');
		return;
	}
	if ($.isEmptyObject(aotulocations)) {
		showMessage("Do not create layer!", "error");
		alterDialogDisabled("div[aria-labelledby='title:" + dialogId + "'] button", false, 'Submit');
		return;
	}
	for (var key in verifyMgs) {
		if (verifyMgs[key] == 0 || verifyMgs[key] == 2) {
			alterDialogDisabled("div[aria-labelledby='title:" + dialogId + "'] button", false, 'Submit');
			return;
		}
	}
	var formJson = $("#modify_storage_layer").serializeObject();
	formJson['aotulocations'] = aotulocations;
	formJson['type'] = 5;
	var is_three_dimensional=formJson['is_three_dimensional'];
	var formStr = JSON.stringify(formJson);

	$.ajax({
		url: '/Sync10/_gis/storageCotroller/saveModifyLayer',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data: formStr,
		cache: false,
		contentType: "application/json; charset=UTF-8",
		async: false,
		beforeSend: function(request) {
			$.blockUI({
				message: '<h3><img src="imgs/loading.gif" />Submit...</h3>'
			});
		},
		success: function(data) {
			$.unblockUI();
			if (data && data.flag == "true") {
				showMessage("Save succeed", "succeed");
				var data_ = {
					type: type,
					position_id: data.returnvalue
					,is_three_dimensional:is_three_dimensional
				};
				dialog.close(data_);
				return;
			} else if (data && data.flag == "false") {
				if (data.returnvalue == 0) {
					showErrorVerifyMsg($("#modify_storage_layer #name"), 'Name can not repeat!');
				} else if (data.returnvalue == "-1") {
					clearAutoLayer();
					showErrorVerifyMsg($("#modify_storage_layer #loc_name"), 'AutolocationName can not repeat!');
				}

			} else if (data && data.flag == "authError") {
				showMessage("Auth error!", "error");

			} else if (data && data.flag == "neo4jError") {
				showMessage("Neo4j Error!", "error");

			}

			alterDialogDisabled("div[aria-labelledby='title:" + dialogId + "'] button", false, 'Submit');
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$.unblockUI();
			var reqJson = JSON.parse(XMLHttpRequest.responseText);
			if (reqJson.message == "java.lang.Exception: -1") {
				clearAutoLayer('clearAutoLayer');
				showMessage("Duplicate name!", "error");
				showErrorVerifyMsg($("#modify_storage_layer #loc_name"), 'Duplicate name!')
			} else if (reqJson.message == "java.lang.Exception: Add neo4j nodes error") {
				showMessage("Neo4j Error!", "error");
			}
			//showErrorVerifyMsg($("#modify_storage_layer #loc_name"),'AutolocationName can not repeat!');
			alterDialogDisabled("div[aria-labelledby='title:" + dialogId + "'] button", false, 'Submit');
		}
	});

}
/**
 *处理批量生产图层后数据方便传到后台处理
 * @returns {String}
 */

function getAotuLocations() {
	var obj = jsmap.storageDemoLayer;
	var datastr = "";
	for (var key in obj) {
		if (key.indexOf("autoDraw") > -1) {
			if (!$.isEmptyObject(obj[key].data)) {
				var poly = obj[key];
				var paths = poly.getPath();
				var name = obj[key].data.name;
				var psId = obj[key].data.psId;
				var height = obj[key].data.height;
				var width = obj[key].data.width;
				var latlng = obj[key].data.point;
				var angle = obj[key].data.angle;
				var latlngStr = pathTolatlngStr(paths);
				var point = convertLatlngToCoordinateAjax(psId, latlng.lat(), latlng.lng());
				var x = point.x;
				var y = point.y;
				var local = psId + "#" + name + "#" + height + "#" + width + "#" + x + "#" + y + "#" + angle + "#" + latlngStr + ";";
				datastr += local;
			}
		}
	}
	return datastr;
}
/**
 *@author tw
 *变更批量创建的layer的名称的顺序
 */

function reverseName() {
	var obj = jsmap.storageDemoLayer;
	var layerNameArr = new Array();
	//从layer中获取名字并把名字放置数组中
	for (var key in obj) {
		if (key.indexOf('autoDraw') > -1) {
			layerNameArr.push(obj[key]['data']['name']);
		}
	}
	//将layer名字的顺序颠倒过来
	var i = 0;
	var nameArrReverse = layerNameArr.reverse();
	for (var key in obj) {
		if (key.indexOf('autoDraw') > -1) {
			obj[key]['data']['name'] = nameArrReverse[i];
			obj[key]['data']['labelContent'] = "<span style='color: #A129E6'><p align='center'>" + nameArrReverse[i].toUpperCase() + "</p></span>";
			var label = obj[key]['data']['label'];
			label.setHtml("<span style='color: #A129E6'><p align='center'>" + nameArrReverse[i].toUpperCase() + "</p></span>");
			i++;
		}

	}
}
//添加location staging ，docks ，parking，printer,webcam拖拽添加layer时，上次这些操作是否完成

function stopNextActionDialog() {
	require(['art_Dialog/dialog-plus'],
		function(dialog) {
			dialog({
				title: 'Notify',
				content: 'The last action not complete',
				cancel: false,
				backdropOpacity: 0.2,
				ok: function() {}
			}).showModal();
		});
}


function pathTolatlngStr(paths) {
	var latlng = "";
	if (!$.isEmptyObject(paths)) {
		for (var i = 0; i < paths.length - 1; i++) {
			latlng += paths.getArray()[i].lng() + "," + paths.getArray()[i].lat() + ",0.0 ";
		}
		latlng += latlng.split(" ")[0];
	}
	return latlng;

}

//提交重新设置刷新任务时间

function commitRefreshTime() {
	var tt = verifyTt();
	var dt = verifyDt();
	if (tt == '1' && dt == '1') {
		var truck_time = $("#truck_time").val();
		var dock_time = $("#dock_time").val();
		var time = {
			"truck": truck_time,
			"dock": dock_time
		};
		setRefreshTime(time);
	}
}
//ajax 异步加载title

function ajaxPostProductTitle() {
	var titleData;
	$.ajax({
		url: '/Sync10/_gis/titleInfoCotroller/queryTitle',
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(data) {
			titleData = data;
		}
	});
	return titleData;
}
//-------------------显示验证信息--------------

function showErrorVerifyMsg(obj, errorMsg) {
	$(obj).attr('title', errorMsg);
	var obj_parent = $(obj).parent();
	var obj_next = $(obj).next();
	removeErrorVerifyMsg(obj_parent, obj_next)
	obj_parent.addClass('has-error');
	obj_next.addClass('glyphicon-remove');
}

function showRightVerifyMsg(obj) {
	$(obj).attr('title', '');
	var obj_parent = $(obj).parent();
	var obj_next = $(obj).next();
	removeRightVerifyMsg(obj_parent, obj_next);
	obj_parent.addClass('has-success');
	obj_next.addClass('glyphicon-ok');
}
//----------------移除验证信息---------------------------

function removeErrorVerifyMsg(obj_parent, obj_next) {
	obj_parent.removeClass('has-success');
	obj_next.removeClass('glyphicon-ok');
}

function removeRightVerifyMsg(obj_parent, obj_next) {
	obj_parent.removeClass('has-error');
	obj_next.removeClass('glyphicon-remove');
}
//---------------------全选truck---------------------------

function selectAllTruck(obj) {
	var nodes
	if ($(obj).prop('checked')) {
		nodes = $(obj).parent().nextAll().find('input').prop('checked', 'checked');
	} else {
		nodes = $(obj).parent().nextAll().find('input').prop('checked', false);
	}

	var ids = treeNodeSelect(nodes);
	startCarIsChecked(ids);
}
//----------------------选择truck--------------------------

function selectTruck(obj) {
	var parentNode = $("#select_all_truck")
	if (parentNode.prop('checked')) {
		parentNode.prop('checked', false);
		//parentNode.parent().nextAll().find('input').prop('checked',false);
	}
	var allTruckNodes = parentNode.parent().nextAll().find('input');
	var ids = treeNodeSelect(allTruckNodes);
	if ((ids + '').indexOf(',') > -1 && allTruckNodes.length == ids.split(',').length) {
		parentNode.prop('checked', true);
	}
	if ($(obj).prop('checked')) {
		var id = $(obj).data("assetid");
		startCarIsChecked(id);
	}

}

function dataValid_() {
	var interval = $("#interval").val();
	var batch = $("#batch").val();
	var err = null;
	$("#errInfo").html("");
	if (batch == "") {
		err = "Batch cannot be empty,valid range [1-16]";
		return err;
	} else {
		var reg = batch.match(/\D+/);
		if (reg != null) {
			err = "Valid range of batch is [1-16]";
			return err;
		}
		var i = parseInt(batch);
		if (i < 1 || i > 65535) {
			err = "Valid range of batch is [1-16]";
			return err;
		}
	}
	if (interval == "") {
		err = "Interval cannot be empty,valid range [1-65535]";
		return err;
	} else {
		var reg = interval.match(/\D+/);
		if (reg != null) {
			err = "Valid range of interval is [1-65535]";
			return err;
		}
		var i = parseInt(interval);
		if (i < 1 || i > 65535) {
			err = "Valid range of interval is [1-65535]";
			return err;
		}
	}
	return err;
}

function saveCmd(dialog) {
	var err = dataValid_();
	if (err != null) {
		$("#errInfo").html(err);
		return;
	}
	var asset = $("#truck_data").data('dt');
	$.ajax({
		url: systenFolder + 'action/administrator/gis/AddCmdAction.action',
		data: $("#loc_frequency").serialize() + "&aid=" + asset.aid + "&call=" + asset.cal + "&name=" + asset.nam,
		dataType: 'json',
		type: 'post',
		beforeSend: function(request) {

		},
		success: function(data) {
			dialog.close();
		},
		error: function() {
			$("#errInfo").html("System error...");
		}
	});
}
//--------------------设置artDialog中按钮的disabled属性--------------------------------

function alterDialogDisabled(eleNode, disabledVal, btnText) {
	var btns = $(eleNode).prop('disabled', disabledVal);
	btns[0].disabled = false;
	if (btns.length == 3) {
		if (btns[1].disabled) {
			btns[1].innerHTML = btnText + "……";
		} else {
			btns[1].innerHTML = btnText;
		}
	} else if (btns.length == 4) {
		if (btnText == 'Submit') {
			if (btns[2].disabled) {
				btns[2].innerHTML = btnText + "……";
			} else {
				btns[2].innerHTML = btnText;
			}
		} else if (btnText == 'Delete') {
			if (btns[1].disabled) {
				btns[1].innerHTML = btnText + "……";
			} else {
				btns[1].innerHTML = btnText;
			}
		}
	}

}
//添加printer_server时的验证

function validate() {
	var printer_server_name = $("#printer_server_name").val();
	var employe_name = $("#employe_name").val();
	if (printer_server_name.length < 1) {
		showMessage("Input Server Name . eg (WareHouse)", "alert");
		$("#printer_server_name").focus();
		return false;
	}
	return true;
}
//将选择的菜单着色

function changeSelectMenuBgCss(btnobj, parentClass) {
	var p_buttonbox = btnobj.parents(".category_container").eq(0);
	p_buttonbox.css("background", "#F2F2F2");
	return p_buttonbox;

}
//保存修改的storage轮廓

function saveMaintainStorage() {
	var storage_dt = {};
	var latLngs = new Array();
	$.each($('#alterStorageForm').serializeArray(), function(index, field) {
		var latLng = {};
		if (field.name.indexOf('latLng') > -1) {
			//latLngs.push(field.value);
			latLng["latlng"] = field.value;
			latLngs.push(latLng);
		} else if (field.name == 'parentid') {
			storage_dt[field.name] = 0;
		} else if (field.name == 'ps_id') {
			storage_dt[field.name] = parseFloat(field.value);
		} else if (field.name == 'option') {

		} else {
			storage_dt[field.name] = field.value;
		}
	})
	storage_dt['latlngs'] = latLngs;
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/updateStorageBase',
		dataType: 'json',
		data: JSON.stringify(storage_dt),
		type: 'POST',
		async: false,
		contentType: "application/json; charset=UTF-8",
		beforeSend: function(request) {},
		success: function(data) {
			if (data.flag) {
				showMessage("Save success", "alert");
				var latlngs_ = "";
				jsmap.mapToolPositionList = [];
				for (var i = 0; i < latLngs.length; i++) {
					if (latlngs_) {
						latlngs_ += " " + latLngs[i].latlng.split(',')[1] + "," + latLngs[i].latlng.split(',')[0];
					} else {
						latlngs_ = latLngs[i].latlng.split(',')[1] + "," + latLngs[i].latlng.split(',')[0];
					}
					var latLng = jsNewLatLng(latLngs[i].latlng.split(',')[0], latLngs[i].latlng.split(',')[1]);
					jsmap.mapToolPositionList.push(latLng);
				}
				latlngs_ += " " + latLngs[0].latlng.split(',')[1] + "," + latLngs[0].latlng.split(',')[0];
				delete jsmap.storageBasePolyline[storage_dt['ps_id']];
				drawStorageBase(data.ps_id, {
					latlng: latlngs_
				});
				jsmap.clreaPolygon();
				jsmap.clreaLabelMarker();
				//$("#storageAlter_btn_cancel").addClass('disabled');
				$("#storageAlter_btn_cancel").hide();
				$("#storageAlter_btn_save").hide();
				$("#storageAlter_btn_edit").show();
				$("#storageAlter_btn_delete").show();
			} else {
				showMessage("Save fail", "alert");
			}

		}
	});
}
//重新设置库存列表大小

function resizeInventoryListWin() {
	var gis_mainObj = $("#jsmap");
	var height = gis_mainObj.height();
	var width = gis_mainObj.width();
	//设置列表搞定跟宽带
	var inentoryObj = $('#locInventoryEle');
	if (inentoryObj.length!=0) {
		inentoryObj.height(height).width(width);
	}
	//设置3d，list切换按钮左边距离
	var inventory_model=$("#inventory_model");
	if(inventory_model.length!=0){
		inventory_model.css({'left':(width/2-45)+"px"});
	}
	//设置显示列表和3d库存的容器高度
	var height_=height-25-$("#main_inventory #inventoryQueryForm").height();
    var inventory_cont=$("#inventory_cont");
    if(inventory_cont.length!=0){
    	inventory_cont.css('height',height_);
    }
    //设置list列表中的内容的滚动条
  	var height1=height_ - $("#thead1").height()-$("#thead2").height();
    var inventory_content=$("#inventory_content");
    if(inventory_content.length!=0){
    	inventory_content.height(height1-40).mCustomScrollbar();
    }
    //设置联想输入框宽度
    var queryFormObj=$("#inventoryQueryForm");
    var titleScrollbarArraObj=queryFormObj.find(".titleScrollbar");
    for(var i=0;i<titleScrollbarArraObj.length;i++){
    	var titleScrollbarObj=$(titleScrollbarArraObj[i]);
    	var parentObj=titleScrollbarObj.parent();
    	var parentWidth=parentObj.width();
    	var labelObj=parentObj.find('.input-label');
    	var labelWidth=labelObj.width();
    	titleScrollbarObj.width(parentWidth-labelWidth-25);
    }
    if(resize3dStore){
		resize3dStore();
	}
    
}
//更改3d库存的大小
function resize3dStore(){
	var inv = $('#locInventoryEle');
	var close = $('#closeInventoryWin');
	var form = $('#inventoryQueryForm');
	var div = $('#store_3d');
	if (!$.isEmptyObject(div)) {
		var gis_mainObj = $("#jsmap");
		var height = inv.height() - close.height() - form.height();
		var width = inv.width();
		div.height(height).width(width);
		
		var _3d = div.data("_3d");
		if(_3d && _3d.resize){
			_3d.resize();
		}
	}
}
//联想输入

function associatingInputing(evt, titleDt, singleVal) {

	var _this = this;
	//var jsonDt={};
	var keyCode = evt.keyCode;
	var targetObj = $(evt.target);
	var parentObj = targetObj.parent().parent();
	var titlelistObj = parentObj.find('.titleList_').eq(0);
	/*var*/ //_this.titleIds = "";
	/*var*/
	_this.titles = "";
	var titleVal = targetObj.val();
	//var titleIdsObj = targetObj.next();
	var old_titles = targetObj.data('oldTitles');
	//var old_titleIds=targetObj.data('oldTitleIds');
	//var titleIdsVal = '';
	var title_array = titlelistObj.children(); //获得联想输入时关联出来的结果集
	var titlesIndex = targetObj.data('titlesIndex'); //获得键盘上下键时在结果集中的起始位置
	/*if (titleIdsObj.length > 0) {
		titleIdsVal = titleIdsObj.val();
	}*/
	var titleArray = titleVal.split(',');
	//var titleIdsValArray = titleIdsVal.split(',');
	var unbindFn = function() {
		targetObj.unbind('blur');
		titlelistObj.unbind();
		parentObj.find('.associating_title').unbind();
		$("#jsmap").unbind();
		$("#store_3d").unbind();
	}
	unbindFn();
	var keyboardUpDownEvt = function() { //键盘上下键，选择联想出来的结果集
		titlelistObj.children().css('background', '');
		$(titlelistObj.children()[titlesIndex]).css('background', 'wheat');
		var title_val = $(titlelistObj.children()[titlesIndex]).data('title');
		//var titleId_val = $(titlelistObj.children()[titlesIndex]).data('titleid');
		var offsetTop_ = $(titlelistObj.children()[titlesIndex])[0].offsetTop;
		var scrollHeight = parentObj.find('.titleScrollbar').height();

		parentObj.find('.titleScrollbar').mCustomScrollbar("scrollTo", offsetTop_ + "px");
		var old_title = "";
		//var old_titleId = "";
		if (!singleVal) {
			old_title = targetObj.data('oldTitles') == "" ? "" : targetObj.data('oldTitles').replace(/,$/g, "") + ",";
			//old_titleId = targetObj.data('oldTitleIds') == "" ? "" : targetObj.data('oldTitleIds') + ",";
		}
		var old_title_arr = old_title.split(',');
		for (var i = 0; i < old_title_arr.length; i++) {
			if (old_title_arr[i] == title_val) {
				targetObj.val((old_title).replace(/,$/g, ""));
				//titleIdsObj.val(old_titleId.replace(/,$/g,""));
				return;
			}
		}
		//_this.titleIds=old_titleId.replace(/,$/g,"");
		_this.titles = old_title.replace(/,$/g, "");

		targetObj.val((old_title + title_val).replace(/,$/g, ""));
		//titleIdsObj.val(old_titleId + titleId_val);
	}
	if (keyCode == 40) {
		if (title_array.length > 0) {
			if ((title_array.length - 1) > titlesIndex) {
				titlesIndex = titlesIndex + 1;
				targetObj.data('titlesIndex', titlesIndex);
				keyboardUpDownEvt();

			}
		}

	} else if (keyCode == 38) {
		if (title_array.length > 0) {
			if (titlesIndex > 0) {
				titlesIndex = titlesIndex - 1;
				targetObj.data('titlesIndex', titlesIndex);
				keyboardUpDownEvt();

			}
		}
	}else if(keyCode==13){
		if(titlesIndex>=0){
			//var title_val = $(titlelistObj.children()[titlesIndex]).data('title');
			parentObj.find('.titleScrollbar').eq(0).hide();
			return;
		}
	} else {
		if (keyCode == 8) {
			var old_titles_arr = $.isEmptyObject(old_titles)==true?('').split(','):old_titles.split(',');
			var titleVal_arr = titleVal.split(',');
			var titles_ = "";
			for (var i = 0; i < titleVal_arr.length; i++) {
				for (var j = 0; j < old_titles_arr.length; j++) {
					if ((titleVal_arr[i] + '').toUpperCase() == (old_titles_arr[j] + '').toUpperCase()) {
						titles_ += titles_ == "" ? titleVal_arr[i] : ',' + titleVal_arr[i];
					}
				}
			}
			//新添加
			targetObj.data('oldTitles', titles_);

			/*if (titleArray.length < titleIdsValArray.length || titleVal == '') {
				var idArray = titleIdsValArray.slice(0, titleIdsValArray.length - 1);
				//targetObj.next().val(idArray.toString());
			}else if(titleArray.length == titleIdsValArray.length) {
				if(titleArray[titleArray.length-1]==""){
					var idArray = titleIdsValArray.slice(0, titleIdsValArray.length - 1);
					targetObj.next().val(idArray.toString());
				}
			}*/
		}
		targetObj.data('titlesIndex', -1);
		//targetObj.data('oldTitles',targetObj.val());
		//targetObj.data('oldTitleIds',titleIdsObj.val());
	}
	if (keyCode != 38 && keyCode != 40) {
		titlelistObj.html('');
		if (titleArray.length > 0) {
			for (var j = 0; j < titleArray.length; j++) {
				for (var i = 0; i < titleDt.length; i++) {
					if (typeof(titleDt[i]) == 'object') {
						if (titleDt[i].obj_name == titleArray[j].toUpperCase()) {
							//_this.titleIds == "" ? _this.titleIds = titleDt[i].obj_id + '' : _this.titleIds += ',' + titleDt[i].obj_id;
							_this.titles == "" ? _this.titles = titleDt[i].obj_name : _this.titles += ',' + titleDt[i].obj_name;
							//targetObj.next().val(_this.titleIds);
							//新添加
							targetObj.data('oldTitles', _this.titles);
							//targetObj.data('oldTitleIds',_this.titleIds);
							//jsonDt[titleDt[i].obj_name]=titleDt[i].obj_id;
							//targetObj.data('jsonDt',jsonDt);
							//---end--
							if (j == titleArray.length - 1) {
								titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i].obj_name + "' data-titleId='" + titleDt[i].obj_id + "'>" + titleDt[i].obj_name + "</div>");
							}
						} else if (j == titleArray.length - 1 && titleDt[i].obj_name.indexOf(titleArray[j].toUpperCase()) > -1) {
							titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i].obj_name + "' data-titleId='" + titleDt[i].obj_id + "'>" + titleDt[i].obj_name + "</div>");
						}
					} else {
						if ((titleDt[i] + '').toUpperCase() == titleArray[j].toUpperCase()) {
							_this.titles == "" ? _this.titles = titleDt[i] : _this.titles += ',' + _this.titles;
							//新添加
							targetObj.data('oldTitles', _this.titles);
							if (j == titleArray.length - 1)
								titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i] + "' >" + titleDt[i] + "</div>");
						} else if (j == titleArray.length - 1 && (titleDt[i] + '').toUpperCase().indexOf(titleArray[j].toUpperCase()) > -1) {
							titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i] + "' >" + titleDt[i] + "</div>");
						}
					}


				}
			}
		} else {
			for (var i = 0; i < titleDt.length; i++) {
				if (typeof(titleDt[i]) == 'object') {
					if (titleDt[i].obj_name.indexOf(titleVal.toUpperCase()) > -1) {
						titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i].obj_name + "' data-titleId='" + titleDt[i].obj_id + "'>" + titleDt[i].obj_name + "</div>");
					}
					/*else if (keyCode == 8) {
						//_this.titleIds = targetObj.next().val();
						_this.titles = titleVal;
						titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i].obj_name + "' data-titleId='" + titleDt[i].obj_id + "'>" + titleDt[i].obj_name + "</div>");
					}*/
				} else {
					if ((titleDt[i] + '').toUpperCase().indexOf(titleVal.toUpperCase()) > -1) {
						titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i] + "' >" + titleDt[i] + "</div>");
					}
					/*else if (keyCode == 8) {
						_this.titles = titleVal;
						titlelistObj.append("<div class='associating_title' data-title='" + titleDt[i] + "' >" + titleDt[i] + "</div>");
					}*/
				}

			}
		}
		parentObj.find('.titleScrollbar').eq(0).show();
	}


	parentObj.find('.associating_title').click(function(evt_) {
		var title_ = $(evt_.target).data('title');
		var inputVal = targetObj.val();
		var titlesArray = (inputVal + '').split(',');

		var titlesArrayLast = titlesArray[titlesArray.length - 1] + '';
		if (titlesArrayLast.toUpperCase() != title_) {
			titlesArray = titlesArray.slice(0, titlesArray.length - 1);
		}
		for (var i = 0; i < titlesArray.length; i++) {
			if (titlesArray[i] == title_) {
				targetObj.val(titlesArray.toString().replace(/,$/g, ""));
				parentObj.find('.titleScrollbar').eq(0).hide();
				return;
			}
		}
		_this.titles = titlesArray.toString().replace(/,$/g, "");
		if ($.isEmptyObject(singleVal))
			_this.titles == '' ? _this.titles = title_ : _this.titles += ',' + title_;
		else
			_this.titles = title_;
		targetObj.val((_this.titles + '').replace(/,$/g, ""));
		parentObj.find('.titleScrollbar').eq(0).hide();
	});
	$("#store_3d").click(function(){
		parentObj.find('.titleScrollbar').eq(0).hide();
		$("#jsmap").unbind();
		$("#store_3d").unbind();
	});
	$("#jsmap").click(function(){
		parentObj.find('.titleScrollbar').eq(0).hide();
		$("#jsmap").unbind();
		$("#store_3d").unbind();
	});
	targetObj.blur(function() {
		parentObj.find('.titleScrollbar').eq(0).hide();
	});
	titlelistObj.mouseover(function(evt_) {
		targetObj.unbind('blur');
	});
	titlelistObj.mouseout(function(evt_) {
		targetObj.bind('blur', function() {
			parentObj.find('.titleScrollbar').eq(0).hide();
		});
	});


}
//menu中Inventory的Inquiry条件数据

function InquiryDt(titleDt) {
	var dt = {};
	$.each($('#productStorageFrom').serializeArray(), function(index, field) {
		if (!$.isEmptyObject(field.value)) {
			if (field.name == 'titles') {
				var titles = field.value;
				var title_ids=byCNTTitleNameSwitchCNTTitleId(titleDt,titles);
				if(!$.isEmptyObject(title_ids+'')){
					dt['title_ids'] = title_ids;
				}
			}
			dt[field.name] = field.value;
		}
	});
	return dt;
}
//拿到浏览器语言格式

function browserLanguage() {
	var language;
	if (navigator.language) //对于mozilla, Firefor
		language = navigator.language;
	else if (navigator.browserLanguage) //对于IE
		language = navigator.browserLanguage;
	return language.toLowerCase();
}
//返回处理过containerType的查询条件

function inventoryQueryDt(containerTypeKey, dt) {
	var data = dt;
	var language = browserLanguage();
	var type=dt['container_type'];
	if (!$.isEmptyObject(dt['container_type'])) {
		for (var key in containerTypeKey) {
			if (containerTypeKey[key][language] == (dt['container_type']).toUpperCase()) {
				type = parseInt(key);
				break;
			} else {
				type = '';
			}
		}
	}
	if(!$.isEmptyObject((type+''))){
		data['container_type'] = type;
	}
	return data;
}
//根据containerTitle Name，将其转换成titleId

function byCNTTitleNameSwitchCNTTitleId(titleDt,titles) {
	//
	var titles_array = titles.split(',');
	var title_ids = "";
	for (var j = 0; j < titles_array.length; j++) {
		for (var i = 0; i < titleDt.length; i++) {
			var dt_ = titleDt[i];

			if (dt_['obj_name'] == titles_array[j]) {
				title_ids += title_ids == '' ? dt_['obj_id'] : ',' + dt_['obj_id'];
			}

		}
	}
	return title_ids;
}
//返回处理过containerName，显示container list时，containerType的查询条件上显示name

function inventoryQueryValue(containerTypeKey, dt) {
	var data = dt;
	var typeName = '';
	var language = browserLanguage(); //获取浏览器语言
	if (!$.isEmptyObject(dt['container_type'] + '')) {
		for (var key in containerTypeKey) { //遍历containerType,将遍历containerType的name设置到dt中
			if (key == dt.container_type) {
				typeName = containerTypeKey[key][language];
				break;
			} else {
				typeName = '';
			}
		}
	}
	data['container_type'] = typeName;
	return data;
}
//返回containerType key跟name数组
function returnContainerArray(containerTypeKey){
	var cntDt=new Array();
    for (var key in containerTypeKey) {
      var language = browserLanguage().toLowerCase();
      if (key!=0) {
        var cntName=containerTypeKey[key][language];
        cntDt.push({'key':key,'name':cntName});
      }else{
        cntDt.push({'key':0,'name':'All'});
      }
    }
    return cntDt;
}

//关闭库存列表窗口
function closeInventoryWind(){
	var main_inventoryEle=$("#main_inventory");
	if (main_inventoryEle.length!=0) {
          main_inventoryEle.html('');
          main_inventoryEle.hide();
          main_inventoryEle.css('height','0px');
        }
}
//查询已有库存的location或者storage，并在地图上标红
function queryInventoryFlagInMap(psId,data) {
		var dt=data;
		$.ajax({
			url: systenFolder + '_gis/BaseController/queryHasInventLocationss',
			type: 'post',
			dataType: 'json',
			data: JSON.stringify(dt),
			contentType: "application/json; charset=UTF-8",
			success: function(data) {
				if (data) {
					waitLoadStorageDt(psId, data);
					//jsmap.showStorageCatalogLocation(psId, data);
				} else {
					jsmap.clearStorageCatalog();
				}
			},
			error: function() {
				$.unblockUI();
				jsmap.clearStorageCatalog();
			}
		});

}
//等待数据加载完成后再执行后续操作
function waitLoadStorageDt(psId, data) {
	var v_ = this;
	var ps_id = psId;
	var dt = data;
	setTimeout(function() {
		if ($.isEmptyObject(jsmap.locationBounds[ps_id])) {
			v_.waitLoadStorageDt(ps_id, dt);
		} else {
			$.unblockUI();
			jsmap.showStorageCatalogLocation(ps_id, dt);//渲染有库存的location跟storage
		}
	}, 500);
}
//绑定菜单上的定位storage功能
function bindMenuLocImg(ps_id){
	$(".location_img").css('cursor', 'pointer');
	$(".location_img").bind('click', function() {
		if ($.isEmptyObject(jsmap.storageBasePolyline[ps_id])) {
			getStorageBaseDataAjax(ps_id, false); //获取地图base数组 并且有数据时绘制base
		} else {
			var bounds = jsmap.storageBasePolyline[ps_id].data.bounds;
			jsmap.googleMap.fitBounds(bounds);
		}
	});
}

/*当zoom小于等于13时，隐藏当前选中时storage中的camera marker，
*显示一个marker代表当前storage中隐藏的camera marker
*/
function hideSelectStorageCam(){
	var camMarker=jsmap.storageWebcamMarker;
	var ps_id=jsmap.ps_id;
	var storageCamLastPosition={};
	var storageCamNum={};
	var numCam=0;
	var latlng;
	for(var key in camMarker){
		var ps_id=key.split("_")[0];
		if(ps_id in storageCamLastPosition){
			storageCamNum[ps_id]=storageCamNum[ps_id]+1;
			storageCamLastPosition[ps_id]=camMarker[key].position;
		}else{
			storageCamNum[ps_id]=1;
			storageCamLastPosition[ps_id]=camMarker[key].position;
		}
		camMarker[key].setMap(null);
		
	}
	for(var key in storageCamLastPosition){
		if(!jsmap.typicalSelectStorageCam[key]){
			var marker = jsNewMarker(storageCamLastPosition[key], "./imgs/webcam.png");
			var labelContent = "<span style='color: #A129E6'><p align='center'>" + "X"+storageCamNum[key]+ "</p></span>";
			var label = new LabelOverlay(storageCamLastPosition[key], "X"+storageCamNum[key], "bottom");
			label.zoomRange = [0, 13];
			marker.putData({
				labelContent: labelContent,
				psId: key,
				name:"X"+storageCamNum[key],
				label: label,
				state: STORAGE_POSITION_TYPE.WEBCAM
			});
			jsmap.typicalSelectStorageCam[key]=marker;
			MapsEvent.addListener(marker, "mouseover", function(e) {
				//显示边框、名称
				if (this.data.label) {
					this.data.label.setHtml(this.data.labelContent);
					this.data.label.show();
				}
			});
			MapsEvent.addListener(marker, "mouseout", function(e) {
			//隐藏边框、名称
				if (this.data.label) {
					this.data.label.setHtml(this.data.name.toUpperCase());
					this.data.label.autoDisplay();
				}
				
			});
		}else{
			jsmap.typicalSelectStorageCam[key].data.label.setMap(jsmap.getMap());
			jsmap.typicalSelectStorageCam[key].setMap(jsmap.getMap());
		}
		
	}
}
/**
*当zoom大于13时显示当前选中时storage中的camera marker，
*并隐藏代表的cam marker
*/
function showSelectStorageCam(){
	var camMarker=jsmap.storageWebcamMarker;
	var ps_id=jsmap.ps_id;
	for(var key in camMarker){
		if(key.indexOf(ps_id)==-1){
			camMarker[key].setMap(jsmap.getMap());
		}else{
			if($("#webcam").prop("checked")==true){
				//camMarker[key].data.label.setMap(jsmap.getMap());
				camMarker[key].setMap(jsmap.getMap());
			}else if($("#webcam").prop("checked")==undefined){
				camMarker[key].setMap(jsmap.getMap());
			}
		}
	}
	for(var key in jsmap.typicalSelectStorageCam){
		jsmap.typicalSelectStorageCam[key].setMap(null);
	}
	/*if(jsmap.typicalSelectStorageCam[ps_id]){
		jsmap.typicalSelectStorageCam.setMap(null);
	}*/
}
/*
*显示或隐藏右键菜单中的添加layer
*/ 
function showOrHideAddLayer(flag){
	if(flag){
		$("#menu").show();
		$("#addWebcam").show();
		$("#addPrinter").show();
		$("#addRectangle").show();
		$("#menu").find("#xy").show();
		//$("#menu").find("#y").show();
	}else{
		$("#menu").hide();
		$("#addWebcam").hide();
		$("#addPrinter").hide();
		$("#addRectangle").hide();
		$("#menu").find("#xy").hide();
		//$("#menu").find("#y").hide();
	}
}
/*
*显示右键菜单
*/
function showRightMenu(position){
	var offset = $("#jsmap").offset();
	var posx = parseInt(position.x + offset.left);
	var posy = parseInt(position.y + offset.top);
	var menu = document.getElementById("menu");
	menu.style.display = "block"; //设置菜单可见   
	menu.style.top = posy + "px"; //设置菜单位置为鼠标右击的位置   
	menu.style.left = posx + "px";
}
//清除所有图层
function clearAllLayer(){
	var psId=jsmap.ps_id;
	if(psId){
		jsmap.deleteAllArea(psId);
		jsmap.clearAllTitle(psId);
		jsmap.clearZoneDocks(psId);
		jsmap.clearZonePerson(psId);
		jsmap.clearAllLocation(psId);
		jsmap.flag["draw2DLocation"] = false;
		jsmap.unLoadKml(psId+"_2d_locs");
		jsmap.flag["draw3DLocation"] = false;
		jsmap.clearAllLocation(psId);
		jsmap.unLoadKml(psId+"_3d_locs");
		jsmap.flag["drawLocation"] = false;
		jsmap.clearAllStaging(psId);
		jsmap.flag["drawDocks"] = false;
		jsmap.clearAllDocks(psId);
		jsmap.flag["drawParking"] = false;
		jsmap.clearAllParking(psId);
		jsmap.deleteAllWebcam(psId);
		jsmap.clearAllPrinter(psId);
		jsmap.clearRoadLayer(psId);
		jsmap.clearStorageCatalog();
		if(jsmap.typicalSelectStorageCam[psId]){
			jsmap.typicalSelectStorageCam[psId].data.label.setMap(null);
			jsmap.typicalSelectStorageCam[psId].setMap(null);
		}
		if(jsmap.storageBasePolyline[psId]){
			jsmap.storageBasePolyline[psId].setMap(null);
		}
		jsmap.ps_id="";
	}
   	
	
}
//storage 三级菜单layer复选框钩选操作
var storageLayerOp={
	baseLayer:function(){
		var psId=jsmap.ps_id;
		if (jsmap.isFitBounds && jsmap.isFitBounds["ps_id"] && jsmap.isFitBounds["ps_id"] != psId) {
		//切换仓库
			if ($.isEmptyObject(jsmap.storageBasePolyline[psId])) {
				getStorageBaseDataAjax(psId, false); //获取地图base数组 并且有数据时绘制base
			} else {
				jsmap.storageBasePolyline[psId].setMap(jsmap.getMap());
				var bounds = jsmap.storageBasePolyline[psId].data.bounds;
				jsmap.googleMap.fitBounds(bounds);
			}
		} else {
			//未切换仓库
			if ($.isEmptyObject(jsmap.storageBasePolyline[psId])) {
				getStorageBaseDataAjax(psId, false); //第一次进来时执行
			}
		}
	},
	area:function(){
		var psId=jsmap.ps_id;
		var areaCheck = $("#area").prop("checked");
		if (areaCheck) {
			drawAreaByPsId(psId, false);
		} else {
			jsmap.deleteAllArea(psId);
			$("#title").attr("checked", false);
		}
	},
	title:function(){
		var psId=jsmap.ps_id;
		var titleCheck = $("#title").prop("checked");
		var areaCheck = $("#area").prop("checked");
		if (titleCheck) {
			if (!areaCheck) {
				$("#area").prop("checked", true);
				drawAreaByPsId(psId, false);
			}
			loadZoneTitle(psId, false);
		} else {

			jsmap.clearAllTitle(psId);
		}
	},
	zonedock:function(){
		var psId=jsmap.ps_id;
		var zoneDockCheck = $("#zonedock").prop("checked");

		if (zoneDockCheck) {
			loadZoneResource(psId, false);
		} else {
			jsmap.clearZoneDocks(psId);
		}
	},
	zonePerson:function(){
		var psId=jsmap.ps_id;
		var zonePersonCheck = $("#zonePerson").prop("checked");
		if (zonePersonCheck) {
			loadZonePerson(psId, false);
		} else {
			jsmap.clearZonePerson(psId);
		}
	},
	location_2d:function(){
		var psId=jsmap.ps_id;
		var locCheck_2d=$("#location_2d").prop("checked");
		var kml_2d=$("#location_2d").data("kml");
		if(locCheck_2d){
			var url=getKmlUrl("2d_locs_" + kml_2d);
			jsmap.flag["drawLocation"] = true;
			jsmap.flag["draw2DLocation"] = true;
			jsmap.loadKml(psId+"_2d_locs", url);
		}else{
			jsmap.flag["draw2DLocation"] = false;
			jsmap.clearAllLocation(psId);
			jsmap.unLoadKml(psId+"_2d_locs");
			
		}
	},
	location_3d:function(){
		var psId=jsmap.ps_id;
		var locCheck_3d=$("#location_3d").prop("checked");
		var kml_3d=$("#location_3d").data("kml");
		if(locCheck_3d){
			var url=getKmlUrl("3d_locs_" + kml_3d);
			jsmap.flag["drawLocation"] = true;
			jsmap.flag["draw3DLocation"] = true;
			jsmap.loadKml(psId+"_3d_locs", url);
		}else{
			jsmap.flag["draw3DLocation"] = false;
			jsmap.clearAllLocation(psId);
			jsmap.unLoadKml(psId+"_3d_locs");
		}
	},
	staging:function(){
		var psId=jsmap.ps_id;
		var stagingCheck = $("#staging").prop("checked");
		if (stagingCheck) {
			drawStagingByPsId(psId, false);
		} else {
			jsmap.clearAllStaging(psId);
		}
	},
	docks:function(){
		var psId=jsmap.ps_id;
		var dockCheck = $("#docks").prop("checked");
		if (dockCheck) {
			jsmap.flag["drawDocks"] = true;
			drawDockYardByPsId(psId, false);
			startDocksParkingRefresh();
		} else {
			jsmap.flag["drawDocks"] = false;
			jsmap.clearAllDocks(psId);
		}
	},
	parking:function(){
		var psId=jsmap.ps_id;
		var parkingCheck = $("#parking").prop("checked");
		if (parkingCheck) {
			jsmap.flag["drawParking"] = true;
			drawDockYardByPsId(psId, false);
			startDocksParkingRefresh();
		} else {
			jsmap.flag["drawParking"] = false;
			jsmap.clearAllParking(psId);
		}
	},
	webcam:function(){
		var psId=jsmap.ps_id;
		var webcamCheck = $("#webcam").prop("checked");
		if (webcamCheck) {
			drawWebcamByPsid(psId, false);
			if(jsmap.zoom<=13){
				var camMarker=jsmap.storageWebcamMarker;
				for(var key in camMarker){
					if(key.indexOf(psId)>-1){
						camMarker[key].setMap(null);
					}
				}
				if(jsmap.typicalSelectStorageCam[psId]){
					jsmap.typicalSelectStorageCam[psId].data.label.setMap(jsmap.getMap());
					jsmap.typicalSelectStorageCam[psId].setMap(jsmap.getMap());
				}else{
					hideSelectStorageCam();
				}
			}
		} else {
				if(jsmap.typicalSelectStorageCam[psId]){
					jsmap.typicalSelectStorageCam[psId].data.label.setMap(null);
					jsmap.typicalSelectStorageCam[psId].setMap(null);
				}
				jsmap.deleteAllWebcam(psId);
		}
	},
	printer:function(){
		var psId=jsmap.ps_id;
		var printerCheck = $("#printer").prop("checked");
		if (printerCheck) {
			drawPrinterByPsId(psId, false);
		} else {
			jsmap.clearAllPrinter(psId);
		}
	},
	inventory:function(){
		var psId=jsmap.ps_id;
		var inventoryCheck=$("#inventory").prop("checked");
		if(inventoryCheck){
			var dt={'ps_id':psId};
			queryInventoryFlagInMap(psId,dt);
		}else{
			jsmap.clearStorageCatalog();
		}
	},
	road:function(){
		var psId=jsmap.ps_id;
		var roadCheck = $("#road").prop("checked");
		if (roadCheck) {
			jsmap.loadRoadLayer(psId, {
				"main": true,
				"entery": true
			});
		} else {
			jsmap.clearRoadLayer(psId);
		}
	}
}

/*添加layer时判断当前添加类型的layer是否还有一个没有完成,
*如果这一类型没有完成的话，不让添加。如果已完成，则根据storage
*三级菜单中的复选框的选中情况判断是否需要在地图上显示添加的这种类
*型的layer
*/
var lastAddLayerIsCom={
	6:function(){
		var psId=jsmap.ps_id;
		var storageWebcamMarker_ = jsmap.storageWebcamMarker[psId + "_demo"];
		if (storageWebcamMarker_) {
			stopNextActionDialog();
			return false;
		}
		if (!$("#webcam").prop("checked")) {
			$("#webcam").prop("checked", true);
			drawWebcamByPsid(psId, false);
		}
		return true;
	},
	7:function(){
		var psId=jsmap.ps_id;
		var storagePrinterMarker_ = jsmap.storagePrinterMarker[psId + "_demo"];
		if (storagePrinterMarker_) {
			stopNextActionDialog();
			return false;
		}
		if (!$("#printer").prop("checked")) {
			$("#printer").prop("checked", true);
			drawPrinterByPsId(psId, false);
		}
		return true;
	},
	9:function(){
		var psId=jsmap.ps_id;
	},
	x:function(){
		var polygon = jsmap.storageDemoLayer["drag"];
		if (polygon) {
			stopNextActionDialog();
			return false;
		}
		return true;
	}
}
/*
*在创建layer时通过键盘上下左右键微调layer
*/
function keyboardMoverLayer(type,evt,formId){
	if(type=="DemoLayer" &&(evt.keyCode==38||evt.keyCode==40||evt.keyCode==37||evt.keyCode==39)){//创建zone，staging，dock等layer
		var demoLayer=jsmap.storageDemoLayer;
		if(jsmap.getMap().keyboardShortcuts){
			jsmap.getMap().keyboardShortcuts=false;
		}
		if(demoLayer){
			if(demoLayer.drag){
				var paths=demoLayer.drag.getPath();
				var paths_=new Array();
				var circles=jsmap.storageDemoLayer.drag.data.circles;
				for(var i=0;i<paths.length;i++){
					var px=jsmap.fromLatLngToContainerPixel(paths.getAt(i).lat(),paths.getAt(i).lng());
					var latlng;
					if(evt.keyCode==38){//上
						latlng=jsmap.fromContainerPixelToLatLng(px.x,px.y-1);
					}else if(evt.keyCode==40){//下
						latlng=jsmap.fromContainerPixelToLatLng(px.x,px.y+1);
					}else if(evt.keyCode==37){//左
						latlng=jsmap.fromContainerPixelToLatLng(px.x-1,px.y);
					}else if(evt.keyCode==39){//右
						latlng=jsmap.fromContainerPixelToLatLng(px.x+1,px.y);
					}
					if(i==0){
						convertCoordinateAjax(jsmap.ps_id, latlng.lat(), latlng.lng(), formId)
					}
					if(i<circles.length){
						circles[i].setPosition(latlng);
					}
					paths_.push(latlng);
				}
				demoLayer.drag.setPaths(paths_);
			}
		}
		return;
	}
}
