define(
	["jquery","artDialog"]
	,function($) {

/*
  opens artDialog:
  title: title of the widget
  content: html to show on the widget
  width: width of widget
  save callback: method to be invoked on OK button
  saveCallbackParams: array of params to be used in saveCallback function
  cancelCallback: method to be invoked on Cancel button

  cancelCallback params can be added if required
*/

	return  function(title,content,width,saveCallback,saveCallbackParams,cancelCallback){
      art.dialog({
          title:title
          ,width:width
          ,lock: true
          ,content:$(content).html()
          ,ok:function(){
            return saveCallback(saveCallbackParams);
          }
        ,cancel:true
          });
    };   

});