/**
 * editRoad {} 所有编辑数据全部保存在此json
 * edit_status 编辑状态，0 不变，1 新增，2 修改，3 删除
 * 
 * 
 * 
 * 
 * 
 * 
 */

var editRoad = {};
//编辑路径初始化
function initEditRoad(psId){
	if(!psId){
		alert("Please choose storage First!");
		return;
	}
	//只显示area图层
	layerSelectStatus(["area"],true,true,true);
	//显示控制窗口
	//initControlWindow();
	//加载主路\
	$("#edit_road.btn.btn-info").hide();
	$("#reset_edit.btn.btn-primary").show();
	$("#save.btn.btn-success").removeAttr("disabled");
	loadRoadLayer(psId,{"main":true});
	if($.isEmptyObject(jsmap.storageRoad)){
		getRoadDataAjax(psId,{"main":true});
	}
	//复制路线数据
	editRoad["road"] = $.extend(true,{},jsmap.storageRoad[psId]["main"]);
	editRoad["point"] = $.extend(true,{},jsmap.storageRoad[psId]["point"]);
	//初始化路径
	initRoadPath();
	//启动画图模式
	drawOnMap("road",onCreateRoad);
	//初始化新数据id
	editRoad["index_point"] = 0;
	editRoad["index_road"] = 0;
	editRoad["polyline"] = jsmap.storageRoadPolyline;
}
/*//控制窗口
function initControlWindow(){
var html = "<table>" +
				"	<tr>" +
				"		<td>" +
				"           <input type='button' value='Del All' class='btn btn-warning' style='height:30px; width:60px;font-size:9px;' onclick='' /> " +          
				"		<td>" +
				"		<td>" +
				"			<input type='button' class='btn btn-primary' style='height:30px; width:60px;font-size:9px' value='Save' onclick='saveToDB('/>" +
				"		<td>" +
				"	</tr>" +
				"</table>";
	initFloatWindow(html);
}*/
//初始化线路latlng路径
function initRoadPath(){
	var road = editRoad.road;
	for(var key in road){
		if(road[key] && road[key].geom){
			var path = wktToPath(road[key].geom);//将数据库里面的blob数据转换为字符串
			road[key].path = path[0];
			road[key].edit_status = 0;
		}
	}
	var point = editRoad.point;
	for(var key in point){
		if(point[key] && point[key].geom){
			var path = wktToPath(point[key].geom);//将数据库里面的blob数据转换为字符串
			point[key].path = path;
			point[key].edit_status = 0;
		}
	}
}
//画图
function onCreateRoad(path){
	var road = editRoad.road;
	var point = editRoad.point;
	
	//起始点在路口处理
	var pointKey = nearToExistsRoadPoint(path);
	//线路相交处理
	crossExistRoad(path, pointKey);
	
	//var r = jsNewPolyline(path, "#91dafe",3,1);
	jsmap.mapToolObject.setPath(path);
}
//可忽略的长度
function getIgnoreLength(pix_num){
	var _pix = pix_num;
	if(pix_num==null && pix_num==undefined){
		_pix = 10;//默认10像素
	}
	return computeDistanceBetweenByPixel(jsNewPoint(1, 1),jsNewPoint(1, _pix+1));
}
//计算起始点是否在已有point附近，并保存或修改相应传入路径path起始点，返回point id
function nearToExistsRoadPoint(path,pix){
	var point_id = [];
	var l = getIgnoreLength();//可忽略长度
	var newpath = [];
	var point = editRoad.point;
	for(var i=0; i<path.length; i++){
		var isNew = true;
		for(var key in point){
			if(point[key] && point[key].edit_status!=3 && point[key].path){
				var len = computeLength([path[i],point[key].path]);
				//pix像素以内算作同一point
				if(len < l){
					path[i] = point[key].path;
					point_id.push(point[key].rp_id);
					isNew = false;
					break;
				}
			}
		}
		if(isNew){
			//新添加点   key以n开头表示new
			var key = "n_"+editRoad.index_point;
			point[key] = {
					"path" : path[i],
					"edit_status" : 1,
					"rp_id" : key};	
			editRoad.index_point += 1;
			point_id.push(key);
		}
	}
	return point_id;
}
//线路相交处理
function crossExistRoad(path, pointId){
	/*var roadPointList = [path[0], path[1]];	//新路径点
	var roadPointKey = [pointKey[0], pointKey[1]];*/
	var roadPointList = [];	//新路径点
	var roadPointId = [];
	
	var oldRoad = [];	//已有的路径，与新路径有交点，需要处理
	var road = editRoad.road;
	var point = editRoad.point;
	
	var ignoreLen = getIgnoreLength();//可忽略长度
	for(var key in road){
		var r = road[key];
		if(r.edit_status == 3){	//跳过删除的线路
			continue;
		}
		//var p = computeIntersectionOfPath(path,r.path);	//交点,线上
		var p = null;	//交点
		//补足新路径长度，与已有路径连接
		//if(p == null){
			var _p = computeIntersectionOfLine(path, r.path);	//[latlng, type]计算两个path的交点，返回交点经纬度坐标和交点的状态交点状态标志，0 不在任意线段上， 1 在path1上， 2 在path2上， 3 在path1和path2上
			
			if(_p[1] == 2){ //交点在r上
				var near = -1;
				var l1 = computeLength([path[0],_p[0]]);
				var l2 = computeLength([path[1],_p[0]]);
				if(l1<=ignoreLen){
					near = 0;
				}
				if(l2<=ignoreLen){
					near = 1;
				}
				if(near>-1){
					p = _p[0];
					path[near] = p;
					getRoadObjByid(pointId[near], 1).path = p;
				}
			}else if(_p[1] == 3){
				p = _p[0];
			}
		//}
		if(p != null){
			var tempPath = [p];
			var p_id = nearToExistsRoadPoint(tempPath);
			//去掉新路径多余长度,将新路径端点改为与已有路径交点
			if(tempPath[0] == path[0]){
				getRoadObjByid(pointId[0], 1).path = p;
				path[0] = p;
				tempPath[0] = p;
				//roadPointList[0] = p;
			}else if(tempPath[0] == path[1]){
				getRoadObjByid(pointId[1], 1).path = p;
				
				path[1] = p;
				tempPath[0] = p;
				//roadPointList[1] = p;
			}else{
				roadPointList.push(tempPath[0]);
				roadPointId.push(p_id[0]);
			}
			//去掉已有路径多余长度，将端点改为与新路径交点
			
			//补足已有路径长度，与新路径连接
			if(tempPath[0] == p){	//交点不是已有路径终点时，要处理旧路
				r.new_point = p_id[0];
				oldRoad.push(r);
			}
			//jsNewMarker(tempPath[0], null);
			
		}
	}
	roadPointList.unshift(path[0]);
	roadPointId.unshift(pointId[0]);
	roadPointList.push(path[1]);
	roadPointId.push(pointId[1]);
	
	//处理新路径
	dealNewRoad(roadPointList,roadPointId);
	//处理旧的路径
	dealOldRoad(oldRoad);
}
//处理新添加的路   将各路口排序后生成新的路径
function dealNewRoad(path, pointId){
	var road = editRoad.road;
	var sortIndex = [0];
	var len = [0];
	for(var i=1; i<path.length; i++){
		var l = computeDistanceBetween(path[0],path[i]);
		var isLong = true;  //最长标记   排序位于最后
		for(var j=0; j<len.length; j++){
			if(l<len[j]){
				len.splice(j,0,l);
				sortIndex.splice(j,0,i);
				isLong = false;
				break;
			}
		}
		if(isLong){
			sortIndex.push(len.length);
			len.push(l);
		}
	}
	for(var i=0; i<sortIndex.length-1; i++){
		var p1 = path[sortIndex[i]];
		var p2 = path[sortIndex[i+1]];
		if(p1 == p2){
			continue;
		}
		var newPath = [p1, p2];
		var source = pointId[sortIndex[i]];
		var target = pointId[sortIndex[i+1]];
		var key = "n_"+editRoad.index_road;
		road[key] = {
				"r_id" : key,
				"path" : newPath,
				"source" : source,
				"target" : target,
				"edit_status" : 1
				};
		editRoad.index_road += 1;
		var r = jsNewPolyline(newPath, "#91dafe",3,1);
		r.putData({
			id : key, 
			psId : currentPsId,
			state : STORAGE_POSITION_TYPE.ROAD_POINT
		});
		editRoad.polyline["main_"+key] = r;
	}
}
//处理与新线路产生交点的已有线路
function dealOldRoad(roads){
	var road = editRoad.road;
	var point = editRoad.point;
	for(var i=0; i<roads.length; i++){
		var r = roads[i];
		var source = r.source;
		var target = r.target;
		var newPoint = r.new_point;
		if(source==newPoint || target==newPoint){
			continue;
		}
		//添加新路径
		var ps = getRoadObjByid(source,1);
		var pt = getRoadObjByid(target,1);
		var pn = getRoadObjByid(newPoint,1);
		
		var newPath = [ps.path, pn.path];
		var key = "n_"+editRoad.index_road;
		road[key] = {
				"path" : newPath,
				"source" : source,
				"target" : newPoint,
				"edit_status" : 1
		};
		editRoad.index_road += 1;
		
		var rr = jsNewPolyline(newPath, "#00FF00",2,1);	//91dafe
		rr.putData({
			id : key, 
			psId : currentPsId,
			state : STORAGE_POSITION_TYPE.ROAD
		});
		editRoad.polyline["main_"+key] = rr;
		
		
		//jsNewPolyline(newPath, "#00FF00",5,1);
		
		newPath = [pn.path, pt.path];
		key = "n_"+editRoad.index_road;
		road[key] = {
				"path" : newPath,
				"source" : newPoint,
				"target" : target,
				"edit_status" : 1
			};
		editRoad.index_road += 1;
		
		//jsNewPolyline(newPath, "#00FF00",5,1);
		
		rr = jsNewPolyline(newPath, "#00FF00",2,1);
		rr.putData({
			id : key, 
			psId : currentPsId,
			state : STORAGE_POSITION_TYPE.ROAD
		});
		editRoad.polyline["main_"+key] = rr;
		
		//删除旧路径
		if(r.edit_status == 1){
			delete roads[i];	//新加的直接删
		}else{
			r.edit_status = 3;	//已有的标记   需要操作数据库
		}
		
		var oldPolyline = editRoad.polyline["main_"+r.r_id];
		if(oldPolyline!=null && oldPolyline!=undefined){
			oldPolyline.setMap(null);
		}
	}
}
//查找指定的road或point
function getRoadObjByid(id, type){ //type: 0 road, 1 point
	var val = null;
	var road = editRoad.road;
	var point = editRoad.point;
	if(type == 0){
		for(var key in road){
			if(road[key].r_id == id){
				val = road[key];
				break;
			}
		}
	}else if(type == 1){
		for(var key in point){
			if(point[key].rp_id == id){
				val = point[key];
				break;
			}
		}
	}
	return val;
}
//通过起始点判断是否已存在路径
function existRoad(){
	var val = false;
	var road = editRoad.road;
	for(var k in road){
		var r = road[k];
		if(k.edit_status!=3 && (k.source==p.rp_id || k.target==r.rp_id)){
			count++;
		}
	}
}

//计算point占用数量
function countPointShare(){
	var road = editRoad.road;
	var point = editRoad.point;
	for(var key in point){
		var p = point[key];
		if(p.edit_status == 3){	//跳过删除点
			continue;
		}
		var count = 0;
		for(var k in road){
			var r = road[k];
			if(r.edit_status!=3 && (r.source==p.rp_id || r.target==p.rp_id)){
				count++;
			}
		}
		p.count = count;
		if(count == 0){
			if(p.edit_status == 1){ //新加的point被弃用后直接删除，不需要操作数据库
				delete point[key];
			}else if(p.edit_status == 0 || p.edit_status == 2){
				p.edit_status = 3;	//数据库原有的point打上删除标记，需要操作数据库
			}
		}
	}
}
//获取最终结果，用于保存数据
function getFinalData(){
	countPointShare();
	var data = {"point" : [], "road" : []};
	var point = editRoad.point;
	var road = editRoad.road;
	for(var key in point){
		var p = point[key];
		if(p){
			var status = p.edit_status;
			//增加 修改
			if(status == 1 || status == 2){
				var geomStr = latlngToString(p.path," ",7);
				data.point.push({
					"id" : p.rp_id,
					"geom_str" : geomStr,
					"status" : status
				});
			}
			//删除，不添加坐标参数，减少数据传输量
			if(status == 3){
				jsNewMarker(p.path, null);
				data.point.push({
					"id" : p.rp_id,
					"status" : status
				});
			}
		}
	}
	for(var key in road){
		var r = road[key];
		if(r){
			var status = r.edit_status;
			//增加 修改
			if(status == 1 || status == 2){
				var geomStr = latlngToString(r.path[0]," ",7);
				geomStr += ","+latlngToString(r.path[r.path.length-1]," ",7);
				data.road.push({
					"id" : r.r_id,
					"geom_str" : geomStr,
					"source" : r.source,
					"target" : r.target,
					"status" : status
				});
			}
			//删除
			if(status == 3){
				jsNewPolyline(r.path, "#FF0000",5,1);
				data.road.push({
					"id" : r.r_id,
					"status" : status
				});
			}
		}
	}
	
	
	return data;
}
//保存到数据库
function saveToDB(){
	var data = getFinalData();
	saveRoadAjax(data);
}

function saveRoadAjax(roadData){
	var data={"psid":currentPsId,"data":roadData};
	$.ajax({
		url:'/Sync10/_gis/roadInfoCotroller/savaRoadAndPoint',
		data: JSON.stringify(data),
		dataType:'json',
		type:'post',
		contentType: "application/json; charset=UTF-8",
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data){
	    		showMessage("Save road succeed!","succeed");
	    		reSetButton();
	    		clearEditRoadData();
	    		reloadRoadLayer(psId);
	    	}else{
	    		showMessage("Save road failed!","error");
	    	}
		},
		error:function(data){
			showMessage("System error","error");
		}
	});
}

//清除画图的数据，结束绘制
function clearEditRoadData(){
	
}
function reSetEdit(){
	delete jsmap.storageRoad[currentPsId];//删除数据
	jsmap.clearRoad();
	reSetButton();
}
//回执delete和save按钮为不可用
function reSetButton(){
	jsmap.setMapTool('move_map');
	$("#edit_road.btn.btn-info").show();
	$("#reset_edit.btn.btn-primary").hide();
	$("#save.btn.btn-success").attr("disabled","true");
}
function deleteRoad(poly){
		if($("#reset_edit.btn").css("display")=="none"){
			  return ;
		}
		require(['art_Dialog/dialog-plus'],function(artDialog){
			var d=artDialog({
			 	title:'Delete',
	  			lock:true,
	  			backdropOpacity: 0.3,
	 		 	icon:'question',
			    content: "<span style='font-weight:bold;'>Delete this road ?</span>",
			    button: [
			        {
			            value: 'YES',
			            callback: function () {
			            	deleteRoadCallback(poly);
			            },
			          
			        },
			        {
			            value: 'NO',
			            callback:function(){
			            },
			        	autofocus: true
			        }
			    ]
			}).showModal();
		})
}

function deleteRoadCallback(poly){
	var psId=poly.data.psId;
	var data ={"ps_id":psId,"r_id":poly.data.id};
	$.ajax({
		url:'/Sync10/_gis/roadInfoCotroller/deleteRoad',
		data:data ,
		dataType:'json',
		type:'get',
		contentType: "application/json; charset=UTF-8",
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data){
	    		poly.setMap(null);
	    		showMessage("Delete road succeed!","succeed");
	    		//reloadRoadLayer(psId);
	    	}else{
	    		showMessage("Delete road failed!","error");
	    	}
		},
		error:function(data){
			showMessage("System error!","error");
		}
	});
	
}


function reloadRoadLayer(psId){
	jsmap.clearRoad();
	delete jsmap.storageRoad[psId];//删除road数据
	jsmap.loadRoadLayer(psId,{"main":true});
}