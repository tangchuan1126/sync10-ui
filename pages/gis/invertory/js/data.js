object = [{
	containers : [{
			"con_id": 2001003,
			"container": 2001003,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [
					
				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			},{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [
					
				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 69
		}, {
			"con_id": 2001005,
			"container": 2001005,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 70
		}, {
			"con_id": 2001008,
			"container": 2001008,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 72
		}, {
			"con_id": 2001009,
			"container": 2001009,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 72
		}, {
			"con_id": 2001014,
			"container": 2001014,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 16,
			"type_id": 75
		}, {
			"con_id": 2001015,
			"container": 2001015,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 16,
			"type_id": 75
		}, {
			"con_id": 2001018,
			"container": 2001018,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 69
		}, {
			"con_id": 2001019,
			"container": 2001019,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 69
		}, {
			"con_id": 2001022,
			"container": 2001022,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE39HE02",
				"pc_id": 1003010,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 77
		}, {
			"con_id": 3012981,
			"container": "3013513",
			"container_type": 3,
			"is_full": 2,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 1,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 1
			}],
			"type_id": 0
		}, {
			"con_id": 3012981,
			"container": "3013513",
			"container_type": 3,
			"is_full": 2,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 1,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 1
			}],
			"type_id": 0
		}, {
			"con_id": 3012981,
			"container": "3013513",
			"container_type": 3,
			"is_full": 2,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 1,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 1
			}],
			"type_id": 0
		}, {
			"con_id": 3012980,
			"container": "3013512",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012980,
			"container": "3013512",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012979,
			"container": "3013511",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012979,
			"container": "3013511",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012978,
			"container": "3013510",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012978,
			"container": "3013510",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012978,
			"container": "3013510",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012977,
			"container": "3013509",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012977,
			"container": "3013509",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012976,
			"container": "3013508",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012976,
			"container": "3013508",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012975,
			"container": "3013507",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012975,
			"container": "3013507",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012974,
			"container": "3013506",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3012974,
			"container": "3013506",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 60,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 60
			}],
			"type_id": 0
		}, {
			"con_id": 3013137,
			"container": "3013665",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 2,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 2
			}],
			"type_id": 0
		}, {
			"con_id": 3013137,
			"container": "3013665",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 2,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 2
			}],
			"type_id": 0
		}, {
			"con_id": 3013137,
			"container": "3013665",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 2,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 2
			}],
			"type_id": 0
		}, {
			"con_id": 3013181,
			"container": "3013724",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013180,
			"container": "3013723",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013179,
			"container": "3013722",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013178,
			"container": "3013721",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013177,
			"container": "3013720",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013176,
			"container": "3013719",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013175,
			"container": "3013718",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013174,
			"container": "3013717",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013173,
			"container": "3013716",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013172,
			"container": "3013715",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013171,
			"container": "3013714",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013170,
			"container": "3013713",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013169,
			"container": "3013712",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013168,
			"container": "3013711",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013167,
			"container": "3013710",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013166,
			"container": "3013709",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013165,
			"container": "3013708",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}, {
			"con_id": 3013138,
			"container": "3013666",
			"container_type": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236010221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 4,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 4
			}],
			"type_id": 5
		}],
		slc_position : 'ghost',
		slc_id : 1,
		slc_type : 1
}, {
	containers : [{
			"con_id": 3013119,
			"container": "3013651",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10223540221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "10223540221/E32-C1",
				"pc_id": 1002050,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 0
		}],
		slc_area : 1000230,
		slc_position : 'I078',
		slc_id : 1005093,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 2021012,
			"container": 2021012,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 74
		}, {
			"con_id": 2021013,
			"container": 2021013,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 74
		}, {
			"con_id": 2021014,
			"container": 2021014,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 75
		}, {
			"con_id": 2021015,
			"container": 2021015,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 75
		}, {
			"con_id": 2021016,
			"container": 2021016,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"con_id": 2021017,
			"container": 2021017,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"con_id": 2021018,
			"container": 2021018,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2021019,
			"container": 2021019,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2021020,
			"container": 2021020,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 76
		}, {
			"con_id": 2021021,
			"container": 2021021,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 76
		}, {
			"con_id": 2021022,
			"container": 2021022,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE39HE02",
				"pc_id": 1003010,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 77
		}, {
			"con_id": 2021023,
			"container": 2021023,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE39HE02",
				"pc_id": 1003010,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 77
		}, {
			"children": [{
				"con_id": 2021108,
				"container": 2021108,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot005",
				"products": [{
					"catalogs": [
						354,
						397
					],
					"locked_quantity": 0,
					"p_name": "SE29HY34",
					"pc_id": 1003007,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 72
			}, {
				"con_id": 2021107,
				"container": 2021107,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot005",
				"products": [{
					"catalogs": [
						354,
						397
					],
					"locked_quantity": 0,
					"p_name": "SE29HY34",
					"pc_id": 1003007,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 72
			}],
			"con_id": 2022504,
			"container": 2022504,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 0,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 200,
				"union_flag": 0
			}],
			"type_id": 2
		}, {
			"children": [{
				"con_id": 2021110,
				"container": 2021110,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot0011",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "DWM48F1Y1",
					"pc_id": 1003037,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 74
			}, {
				"con_id": 2021109,
				"container": 2021109,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot0011",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "DWM48F1Y1",
					"pc_id": 1003037,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 74
			}],
			"con_id": 2022505,
			"container": 2022505,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot0011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 3
		}, {
			"con_id": 2022506,
			"container": 2022506,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot0012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 8
		}],
		slc_area : 1000230,
		slc_position : 'I082',
		slc_id : 1005097,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 2021000,
			"container": 2021000,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 68
		}, {
			"con_id": 2021001,
			"container": 2021001,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 68
		}, {
			"con_id": 2021002,
			"container": 2021002,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2021003,
			"container": 2021003,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2021004,
			"container": 2021004,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 70
		}, {
			"con_id": 2021005,
			"container": 2021005,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 70
		}, {
			"con_id": 2021006,
			"container": 2021006,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1004",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ04",
				"pc_id": 1003006,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 71
		}, {
			"con_id": 2021007,
			"container": 2021007,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1004",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ04",
				"pc_id": 1003006,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 71
		}, {
			"con_id": 2021008,
			"container": 2021008,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 72
		}, {
			"con_id": 2021009,
			"container": 2021009,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 72
		}, {
			"con_id": 2021010,
			"container": 2021010,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"con_id": 2021011,
			"container": 2021011,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"children": [{
				"con_id": 2021103,
				"container": 2021103,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot003",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE26HQ02",
					"pc_id": 1003005,
					"product_line": 1000018,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 70
			}, {
				"con_id": 2021102,
				"container": 2021102,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot003",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE26HQ02",
					"pc_id": 1003005,
					"product_line": 1000018,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 70
			}],
			"con_id": 2022501,
			"container": 2022501,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 3
		}, {
			"children": [{
				"con_id": 2021104,
				"container": 2021104,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot007",
				"products": [{
					"catalogs": [
						354,
						356
					],
					"locked_quantity": 0,
					"p_name": "WD32HB1120",
					"pc_id": 1003039,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 78
			}],
			"con_id": 2022502,
			"container": 2022502,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot007",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "WD32HB1120",
				"pc_id": 1003039,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 200,
				"union_flag": 0
			}],
			"type_id": 8
		}, {
			"children": [{
				"con_id": 2021106,
				"container": 2021106,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot008",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE39FH03",
					"pc_id": 1003009,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 76
			}, {
				"con_id": 2021105,
				"container": 2021105,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot008",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE39FH03",
					"pc_id": 1003009,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 76
			}],
			"con_id": 2022503,
			"container": 2022503,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 9
		}],
		slc_area : 1000230,
		slc_position : 'I083',
		slc_id : 1005098,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 2011000,
			"container": 2011000,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 68
		}, {
			"con_id": 2011001,
			"container": 2011001,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 68
		}, {
			"con_id": 2011002,
			"container": 2011002,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2011003,
			"container": 2011003,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2011004,
			"container": 2011004,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 70
		}, {
			"con_id": 2011005,
			"container": 2011005,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 70
		}, {
			"con_id": 2011006,
			"container": 2011006,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1004",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ04",
				"pc_id": 1003006,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 71
		}, {
			"con_id": 2011007,
			"container": 2011007,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1004",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ04",
				"pc_id": 1003006,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 71
		}, {
			"con_id": 2011008,
			"container": 2011008,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 72
		}, {
			"con_id": 2011009,
			"container": 2011009,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 72
		}, {
			"con_id": 2011010,
			"container": 2011010,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"con_id": 2011011,
			"container": 2011011,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"children": [{
				"con_id": 2011101,
				"container": 2011101,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot001",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "SE24FY10",
					"pc_id": 1003003,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 68
			}, {
				"con_id": 2011100,
				"container": 2011100,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot001",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "SE24FY10",
					"pc_id": 1003003,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 68
			}],
			"con_id": 2012500,
			"container": 2012500,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 2
		}, {
			"children": [{
				"con_id": 2011103,
				"container": 2011103,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot003",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE26HQ02",
					"pc_id": 1003005,
					"product_line": 1000018,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 70
			}, {
				"con_id": 2011102,
				"container": 2011102,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot003",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE26HQ02",
					"pc_id": 1003005,
					"product_line": 1000018,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 70
			}],
			"con_id": 2012501,
			"container": 2012501,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 3
		}, {
			"children": [{
				"con_id": 2011104,
				"container": 2011104,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot007",
				"products": [{
					"catalogs": [
						354,
						356
					],
					"locked_quantity": 0,
					"p_name": "WD32HB1120",
					"pc_id": 1003039,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 78
			}],
			"con_id": 2012502,
			"container": 2012502,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot007",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "WD32HB1120",
				"pc_id": 1003039,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 200,
				"union_flag": 0
			}],
			"type_id": 8
		}, {
			"children": [{
				"con_id": 2011106,
				"container": 2011106,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot008",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE39FH03",
					"pc_id": 1003009,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 76
			}, {
				"con_id": 2011105,
				"container": 2011105,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot008",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE39FH03",
					"pc_id": 1003009,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 76
			}],
			"con_id": 2012503,
			"container": 2012503,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 9
		}],
		slc_area : 1000230,
		slc_position : 'I084',
		slc_id : 1005099,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 3912166,
			"container": "3012062",
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 2,
			"lot_number": "120384",
			"products": [{
				"catalogs": [
					0
				],
				"locked_quantity": 0,
				"p_name": "10223530021/D320-B1",
				"pc_id": 1001748,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 0
		}, {
			"con_id": 3012788,
			"container": "3013320",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10248010245",
			"products": [{
				"catalogs": [
					0
				],
				"locked_quantity": 0,
				"p_name": "10248010245/M55-C2",
				"pc_id": 1002091,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 0
		}, {
			"con_id": 3013190,
			"container": "3013733",
			"container_type": 1,
			"is_full": 1,
			"is_has_sn": 1,
			"lot_number": "10223540221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 5
		}, {
			"con_id": 3013190,
			"container": "3013733",
			"container_type": 1,
			"is_full": 1,
			"is_has_sn": 1,
			"lot_number": "10223540221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 5
		}, {
			"con_id": 3013190,
			"container": "3013733",
			"container_type": 1,
			"is_full": 1,
			"is_has_sn": 1,
			"lot_number": "10223540221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 5
		}, {
			"con_id": 3013190,
			"container": "3013733",
			"container_type": 1,
			"is_full": 1,
			"is_has_sn": 1,
			"lot_number": "10223540221",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "S5451W-C2/10615050045",
				"pc_id": 1002044,
				"product_line": 0,
				"quantity": 3,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 3
			}],
			"type_id": 5
		}],
		slc_area : 1000230,
		slc_position : 'I088',
		slc_id : 1005103,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 2011012,
			"container": 2011012,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 74
		}, {
			"con_id": 2011013,
			"container": 2011013,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 74
		}, {
			"con_id": 2011014,
			"container": 2011014,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 75
		}, {
			"con_id": 2011015,
			"container": 2011015,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 75
		}, {
			"con_id": 2011016,
			"container": 2011016,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"con_id": 2011017,
			"container": 2011017,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 73
		}, {
			"con_id": 2011018,
			"container": 2011018,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2011019,
			"container": 2011019,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 69
		}, {
			"con_id": 2011020,
			"container": 2011020,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 76
		}, {
			"con_id": 2011021,
			"container": 2011021,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 76
		}, {
			"con_id": 2011022,
			"container": 2011022,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE39HE02",
				"pc_id": 1003010,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 77
		}, {
			"con_id": 2011023,
			"container": 2011023,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE39HE02",
				"pc_id": 1003010,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 77
		}, {
			"children": [{
				"con_id": 2011108,
				"container": 2011108,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot005",
				"products": [{
					"catalogs": [
						354,
						397
					],
					"locked_quantity": 0,
					"p_name": "SE29HY34",
					"pc_id": 1003007,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 72
			}, {
				"con_id": 2011107,
				"container": 2011107,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot005",
				"products": [{
					"catalogs": [
						354,
						397
					],
					"locked_quantity": 0,
					"p_name": "SE29HY34",
					"pc_id": 1003007,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 72
			}],
			"con_id": 2012504,
			"container": 2012504,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 0,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 200,
				"union_flag": 0
			}],
			"type_id": 2
		}, {
			"children": [{
				"con_id": 2011110,
				"container": 2011110,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot0011",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "DWM48F1Y1",
					"pc_id": 1003037,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 74
			}, {
				"con_id": 2011109,
				"container": 2011109,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot0011",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "DWM48F1Y1",
					"pc_id": 1003037,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 74
			}],
			"con_id": 2012505,
			"container": 2012505,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot0011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 3
		}, {
			"con_id": 2012506,
			"container": 2012506,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot0012",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "DWM55F2Y1",
				"pc_id": 1003038,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"type_id": 8
		}],
		slc_area : 1000230,
		slc_position : 'I094',
		slc_id : 1005109,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 2001013,
			"container": 2001013,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10011",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "DWM48F1Y1",
				"pc_id": 1003037,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 16,
			"type_id": 74
		}, {
			"con_id": 2001017,
			"container": 2001017,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 73
		}, {
			"con_id": 2001020,
			"container": 2001020,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 76
		}, {
			"con_id": 2001021,
			"container": 2001021,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 76
		}, {
			"children": [{
				"con_id": 2021101,
				"container": 2021101,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot10021",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "SE24FY10",
					"pc_id": 1003003,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 68
			}, {
				"con_id": 2021100,
				"container": 2021100,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot10021",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "SE24FY10",
					"pc_id": 1003003,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"type_id": 68
			}],
			"con_id": 2022500,
			"container": 2022500,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot10021",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"type_id": 2
		}, {
			"children": [{
				"con_id": 2001108,
				"container": 2001108,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot005",
				"products": [{
					"catalogs": [
						354,
						397
					],
					"locked_quantity": 0,
					"p_name": "SE29HY34",
					"pc_id": 1003007,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 72
			}, {
				"con_id": 2001107,
				"container": 2001107,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot005",
				"products": [{
					"catalogs": [
						354,
						397
					],
					"locked_quantity": 0,
					"p_name": "SE29HY34",
					"pc_id": 1003007,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 72
			}],
			"con_id": 2002504,
			"container": 2002504,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot005",
			"products": [{
				"catalogs": [
					354,
					397
				],
				"locked_quantity": 0,
				"p_name": "SE29HY34",
				"pc_id": 1003007,
				"product_line": 1000017,
				"quantity": 0,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 200,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 2
		}],
		slc_area : 1000230,
		slc_position : 'I098',
		slc_id : 1005113,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 2001000,
			"container": 2001000,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 68
		}, {
			"con_id": 2001001,
			"container": 2001001,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 68
		}, {
			"con_id": 2001002,
			"container": 2001002,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1002",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE24HE03",
				"pc_id": 1003004,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 69
		}, {
			"con_id": 2001004,
			"container": 2001004,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 70
		}, {
			"con_id": 2001006,
			"container": 2001006,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1004",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ04",
				"pc_id": 1003006,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 16,
			"type_id": 71
		}, {
			"con_id": 2001007,
			"container": 2001007,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1004",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ04",
				"pc_id": 1003006,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 16,
			"type_id": 71
		}, {
			"con_id": 2001010,
			"container": 2001010,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 73
		}, {
			"con_id": 2001011,
			"container": 2001011,
			"container_type": 1,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot1001",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "SE32HY10",
				"pc_id": 1003008,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 100,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 73
		}, {
			"children": [{
				"con_id": 2001101,
				"container": 2001101,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot001",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "SE24FY10",
					"pc_id": 1003003,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 68
			}, {
				"con_id": 2001100,
				"container": 2001100,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot001",
				"products": [{
					"catalogs": [
						354
					],
					"locked_quantity": 0,
					"p_name": "SE24FY10",
					"pc_id": 1003003,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 68
			}],
			"con_id": 2002500,
			"container": 2002500,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot001",
			"products": [{
				"catalogs": [
					354
				],
				"locked_quantity": 0,
				"p_name": "SE24FY10",
				"pc_id": 1003003,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 2
		}, {
			"children": [{
				"con_id": 2001103,
				"container": 2001103,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot003",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE26HQ02",
					"pc_id": 1003005,
					"product_line": 1000018,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 70
			}, {
				"con_id": 2001102,
				"container": 2001102,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot003",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE26HQ02",
					"pc_id": 1003005,
					"product_line": 1000018,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 70
			}],
			"con_id": 2002501,
			"container": 2002501,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot003",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE26HQ02",
				"pc_id": 1003005,
				"product_line": 1000018,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 3
		}, {
			"children": [{
				"con_id": 2001104,
				"container": 2001104,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot007",
				"products": [{
					"catalogs": [
						354,
						356
					],
					"locked_quantity": 0,
					"p_name": "WD32HB1120",
					"pc_id": 1003039,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 16,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 16,
				"type_id": 78
			}],
			"con_id": 2002502,
			"container": 2002502,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot007",
			"products": [{
				"catalogs": [
					354,
					356
				],
				"locked_quantity": 0,
				"p_name": "WD32HB1120",
				"pc_id": 1003039,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 16,
				"total_locked_quantity": 0,
				"total_quantity": 200,
				"union_flag": 0
			}],
			"title_id": 16,
			"type_id": 8
		}, {
			"children": [{
				"con_id": 2001106,
				"container": 2001106,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot008",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE39FH03",
					"pc_id": 1003009,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 76
			}, {
				"con_id": 2001105,
				"container": 2001105,
				"container_type": 1,
				"damage": 1,
				"is_full": 3,
				"is_has_sn": 1,
				"lot_number": "lot008",
				"products": [{
					"catalogs": [
						354,
						392
					],
					"locked_quantity": 0,
					"p_name": "SE39FH03",
					"pc_id": 1003009,
					"product_line": 1000017,
					"quantity": 100,
					"sn": [

					],
					"title_id": 166,
					"total_locked_quantity": 0,
					"total_quantity": 100,
					"union_flag": 0
				}],
				"title_id": 166,
				"type_id": 76
			}],
			"con_id": 2002503,
			"container": 2002503,
			"container_type": 3,
			"damage": 1,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "lot008",
			"products": [{
				"catalogs": [
					354,
					392
				],
				"locked_quantity": 0,
				"p_name": "SE39FH03",
				"pc_id": 1003009,
				"product_line": 1000017,
				"quantity": 100,
				"sn": [

				],
				"title_id": 166,
				"total_locked_quantity": 0,
				"total_quantity": 300,
				"union_flag": 0
			}],
			"title_id": 166,
			"type_id": 9
		}],
		slc_area : 1000225,
		slc_position : 'L003',
		slc_id : 1005116,
		ps_id : 1000005,
		is_three_dimensional : 1,
		slc_type : 1
}, {
	containers : [{
			"con_id": 3015561,
			"contain_type": 3,
			"container": "3032721",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10231090245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E40-C2/10231090245",
				"pc_id": 1002053,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015562,
			"contain_type": 3,
			"container": "3032722",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10231090245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E40-C2/10231090245",
				"pc_id": 1002053,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015563,
			"contain_type": 3,
			"container": "3032723",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10231090245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E40-C2/10231090245",
				"pc_id": 1002053,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015564,
			"contain_type": 3,
			"container": "3032724",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10231090245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E40-C2/10231090245",
				"pc_id": 1002053,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015565,
			"contain_type": 3,
			"container": "3032725",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10231090245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E40-C2/10231090245",
				"pc_id": 1002053,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}],
		slc_area : 1000245,
		slc_position : 'F022',
		slc_id : 1005796,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 3015566,
			"contain_type": 3,
			"container": "3032808",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015567,
			"contain_type": 3,
			"container": "3032809",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015568,
			"contain_type": 3,
			"container": "3032810",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015569,
			"contain_type": 3,
			"container": "3032811",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015570,
			"contain_type": 3,
			"container": "3032812",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}],
		slc_area : 1000246,
		slc_position : 'F038',
		slc_id : 1005812,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 3015647,
			"contain_type": 3,
			"container": "3032939",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015648,
			"contain_type": 3,
			"container": "3032940",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015649,
			"contain_type": 3,
			"container": "3032941",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015650,
			"contain_type": 3,
			"container": "3032942",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015651,
			"contain_type": 3,
			"container": "3032943",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10236000245",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E43-C2/10236000245",
				"pc_id": 1002055,
				"product_line": 1000012,
				"quantity": 27,
				"sn": [

				],
				"title_id": 6,
				"total_locked_quantity": 0,
				"total_quantity": 27,
				"union_flag": 0
			}],
			"type_id": 0
		}],
		slc_area : 1000246,
		slc_position : 'F040',
		slc_id : 1005814,
		ps_id : 1000005,
		is_three_dimensional : 0,
		slc_type : 1
}, {
	containers : [{
			"con_id": 3015652,
			"contain_type": 3,
			"container": "3033039",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10228060021",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E390I-B1E/10228060021",
				"pc_id": 1002052,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015653,
			"contain_type": 3,
			"container": "3033040",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10228060021",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E390I-B1E/10228060021",
				"pc_id": 1002052,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015654,
			"contain_type": 3,
			"container": "3033041",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10228060021",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E390I-B1E/10228060021",
				"pc_id": 1002052,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015655,
			"contain_type": 3,
			"container": "3033042",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10228060021",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E390I-B1E/10228060021",
				"pc_id": 1002052,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}, {
			"con_id": 3015656,
			"contain_type": 3,
			"container": "3033043",
			"container_type": 3,
			"is_full": 3,
			"is_has_sn": 1,
			"lot_number": "10228060021",
			"products": [{
				"catalogs": [
					355
				],
				"locked_quantity": 0,
				"p_name": "E390I-B1E/10228060021",
				"pc_id": 1002052,
				"product_line": 1000012,
				"quantity": 36,
				"sn": [

				],
				"title_id": 4,
				"total_locked_quantity": 0,
				"total_quantity": 36,
				"union_flag": 0
			}],
			"type_id": 0
		}],
		slc_area : 1000249,
		slc_position : 'E041',
		slc_id : 1005931,
		ps_id : 1000005,
		is_three_dimensional : 1,
		slc_type : 1
}]