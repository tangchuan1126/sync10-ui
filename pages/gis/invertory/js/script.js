;$(function() {
	var obj = [];
//		$.ajax({
//			url: '/Sync10/_gis/BaseController/qureyInvenByslc',
//			type: 'POST',
//			dataType: 'JSON',
//			contentType: "application/json; charset=UTF-8",
//			data: JSON.stringify({
//				'ps_id': 1000005
//			}),
//			error: function() {
//				console.log('错误')
//			},
//			success: function(json) {
//				loadContent(json);
//			}
//		});


	loadContent(object);
	function loadContent(json) {
		var $table = $('<table width="100%" class="table table-striped"></table>');
		$table.append('<tr id="thead1">' +
			'<td class="container thead theadLeft" rowspan="2" width="200px">Container</td>' +
			'<td class="container_type thead" rowspan="2" width="200px">LP Type</td>' +
			'<td class="product" colspan="8" width="280px">Product</td>' +
			'</tr>' +
			'<tr id="thead2"> ' +
			'<td class="p_name thead">PDT Name</td>' +
			'<td class="product_line thead">PDT Line</td>' +
			'<td class="title thead">Title</td>' +
			'<td class="quantity thead">Quantity</td>' +
			'<td class="total_quantity thead">Total Quantity</td>' +
			'<td class="locked_quantity thead">Locked Quantity</td>' +
			'<td class="locked_total_quantity thead">Total Locked Quantity</td>' +
			'<td class="sn thead">SN</td>' +
			'</tr>'
		);
		for (var i = 0; i < json.length; i++) {
			var dt = json[i];
			for (var j = 0; j < dt.containers.length; j++) {
				var map = new Map();
				for (var m = 0; m < dt.containers[j].products.length; m++) {
					var con_id = dt.containers[j].con_id;
					var container = dt.containers[j].container;
					var clp = dt.containers[j].container_type == 1 ? 'CLP' : 'TLP';
					var container_type = dt.containers[j].container_type;
					var p_name = dt.containers[j].products[m].p_name;
					var product_line = dt.containers[j].products[m].product_line;
					var title_id = dt.containers[j].products[m].title_id;
					var quantity = dt.containers[j].products[m].quantity;
					var total_quantity = dt.containers[j].products[m].total_quantity;
					var locked_quantity = dt.containers[j].products[m].locked_quantity;
					var total_locked_quantity = dt.containers[j].products[m].total_locked_quantity;
					var sn = dt.containers[j].products[m].sn;
					var key = p_name + '#' + product_line + '#' + title_id;
					var d = map.get(key);
					if (d) {
						d.quantity = d.quantity + quantity;
						d.total_quantity = d.total_quantity + total_quantity;
						d.locked_quantity = d.locked_quantity + locked_quantity;
						d.total_locked_quantity = d.total_locked_quantity + total_locked_quantity;
						map.put(key, d);
					} else {
						map.put(key, dt.containers[j].products[m]);
					};
				};
				dt.containers[j].products = map.values();
			};
		};
		map.each(function(key, value, index) {
			// 打印 合并的东西
//			console.log(key + "==" + value.quantity);
		});
		function Map() {
			this.keys = new Array();
			this.data = new Object();
			this.put = function(key, value) {
				if (this.data[key] == null) {
					this.keys.push(key);
				}
				this.data[key] = value;
			};
			this.get = function(key) {
				return this.data[key];
			};
			this.remove = function(key) {
				this.keys.remove(key);
				this.data[key] = null;
			};
			this.each = function(fn) {
				if (typeof fn != 'function') {
					return;
				}
				var len = this.keys.length;
				for (var i = 0; i < len; i++) {
					var k = this.keys[i];
					fn(k, this.data[k], i);
				}
			};
			this.entrys = function() {
				var len = this.keys.length;
				var entrys = new Array(len);
				for (var i = 0; i < len; i++) {
					entrys[i] = {
						key: this.keys[i],
						value: this.data[i]
					};
				}
				return entrys;
			};
			this.isEmpty = function() {
				return this.keys.length == 0;
			};
			this.size = function() {
				return this.keys.length;
			};
			this.toString = function() {
				var s = "{";
				for (var i = 0; i < this.keys.length; i++, s += ',') {
					var k = this.keys[i];
					s += k + "=" + this.data[k];
				}
				s += "}";
				return s;
			};
			this.values = function() {
				var len = this.keys.length;
				var entrys = new Array(len);
				for (var i = 0; i < len; i++) {
					entrys[i] = this.data[this.keys[i]];
				}
				return entrys;
			};
		}
		
		

		for (var i = 0; i < json.length; i++) {
			var dd = json[i];
			for (var j = 0; j < json[i].containers.length; j++) {
				for (var m = 0; m < json[i].containers[j].products.length; m++) {
					var con_id = dd.containers[j].con_id;
					var container = dd.containers[j].container;
					var clp = dd.containers[j].container_type == 1 ? 'CLP' : 'TLP';
					var container_type = dd.containers[j].container_type;
					var p_name = dd.containers[j].products[m].p_name;
					var product_line = dd.containers[j].products[m].product_line;
					var quantity = dd.containers[j].products[m].quantity;
					var total_quantity = dd.containers[j].products[m].total_quantity;
					var locked_quantity = dd.containers[j].products[m].locked_quantity;
					var total_locked_quantity = dd.containers[j].products[m].total_locked_quantity;
					var sn = dd.containers[j].products[m].sn;
					$table.append('<tr class="tr_' + con_id + '">' +
						'<td class="container">' + container + '</td>' +
						'<td class="container_type">' + clp + '</td>' +
						'<td class="p_name">' + p_name + '</td>' +
						'<td class="product_line">' + product_line + '</td>' +
						'<td class="title">' + 'title' + '</td>' +
						'<td class="quantity">' + quantity + '</td>' +
						'<td class="total_quantity">' + total_quantity + '</td>' +
						'<td class="locked_quantity">' + locked_quantity + '</td>' +
						'<td class="locked_total_quantity">' + total_locked_quantity + '</td>' +
						'<td class="sn">' + sn + '</td>' +
						'</tr>');
				};
			};
		};
		$('body').append( $table );
	};
});