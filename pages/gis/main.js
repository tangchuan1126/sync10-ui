define([
    './config',
    'jquery',
    'backbone',
    './views/classifyView',
    './views/mapToolsView',
    'oso.lib/videolanrtsp/rtspPlay',
    /*'./js/gis_store',*/
    './js/gis_main',
    'jqueryui/tabs',
    "require_css!bootstrap-css/bootstrap.min",
    './js/GoogleMapsV3',
    './js/jsmap',
    './js/storageRoad'

],
function(config,$,Backbone,ClassifyView,mapToolsView,rtspPlay){
	window.topViewArray=new Array();//定义全局数组，存放正在使用的 top view对象
	window.middleViewArray=new Array();//定义全局数组，存放正在使用的middle view对象
	window.lowerViewArray=new Array();//定义全局数组，存放正在使用的lower view对象
	window.viewData={};//定义全局数组，存放正在使用的lower view对象
	
	//gis_store里面_store变量赋值
	
    $(function(){   	
    	
    	
    	//设置map的宽度
    	$("#jsmap").width($("#gis").width() - $("#gis_left").width()-1);
    	//初始化map
    	jsMapInit();
    	initMapTool();
    	$(document).find("#jsmap").bind("contextmenu",function(e){
    		return false;
    	});
    	//屏蔽左侧菜单事件
    	$(document).find("#menu").bind("contextmenu",function(e){
    		return false;
    	});
    	$("#left_show_img").bind("click",function(){
    		$("#left_show_img").hide();
    		$("#gis_left").hide();
    		$("#right_show_img").show();
    		initMenuSize();
    		resizeMap();
    	});
    	$("#right_show_img").bind("click",function(){
    		$("#left_show_img").show();
    		$("#gis_left").show();
    		$("#right_show_img").hide();
    		initMenuSize();
	        resizeMap();
    	});
    }); 
    $(window).resize(function() {
    	initMenuSize();
    	resizeMap();
	    //gis左侧导航菜单滚动条设置
	    var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
    	$("#gis_left_auto_overflow").height(height).mCustomScrollbar();
    });
   ClassifyView.render();
   mapToolsView.render();
   arrayPush(topViewArray,ClassifyView);

   (function($){ 
   	$.fn.serializeObject=function(){ 
   	           var inputs=$(this).find("input,textarea,select"); 
   	           var o = {}; 
   	           $.each(inputs,function(i,n){ 
   	               switch(n.nodeName.toUpperCase()){ 
   	                   case "INPUT": 
   	                       if($(n).is(":checkbox")){ 
   	                           if($(n).is(":checked")){ 
   	                               o[n.name]=true; 
   	                           }else{ 
   	                               o[n.name]=false; 
   	                           } 
   	                       }else if($(n).is(":radio")){ 
   	                           if($(n).is(":checked")){ 
   	                               o[n.name]=n.value; 
   	                           } 
   	                       }else{ 
   	                           o[n.name]=n.value;  
   	                       } 

   	                       break; 
   	                   case "TEXTAREA": 
   	                       o[n.name]=$(n).text(); 
   	                       break; 
   	                   case "SELECT": 
   	                       o[n.name]=n.value; 
   	                       break; 
   	               } 
   	           }); 
   	           return o; 
   	       } 
   	})(jQuery);
});
