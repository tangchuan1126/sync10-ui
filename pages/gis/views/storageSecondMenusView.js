define(['jquery',
        'backbone',
        'handlebars',
        'templates',
        '../models/gis_collection',
        './storageThirdlyMenusTabsView',
        'art_Dialog/dialog-plus',
        '../js/gis_main',
        '../config',
        '../js/GoogleMapsV3']
	,function($,backbone,
			handlebars,
			templates,
			gis_collection,
			storageThirdlyMenusTabsView,
			dialog,
			gis_main,
			config,
			GoogleMapsV3){
		var storageSecond= backbone.View.extend({
			el:".all_storage",
			template:templates.storageSecondMenus,
			collection:new gis_collection(),
			initialize:function(){
        	},
			render:function(num){
				var v_=this;
				
				v_.setElement("#all_storage");
				v_.$el.html('');
				v_.collection.fetch({
					url:config.storageModel.url,
					success:function(response){
					   var parentwidth= $(".gis_left").width();
					   var n =parseInt(parentwidth*0.96/85);//每一个图标所占宽度为85px,内侧div占整个div宽度的96%
					   if(!response){
					   		v_.$el.html(v_.template({pageplus_L:false,pageplus_R:false,num:0}));
					   		return;
					   }
					   var dt=pageCtrl(response,n*2);
						 v_.collection.reset(dt);
						 if(menusSelectedFlag['seconeMenu']){
						 	var num_=menusSelectedFlag['seconeMenu'].num;
						 	if(dt.length<num_+1){
						 		if(dt.length==1){
									v_.$el.html(v_.template({menus_dt:dt[0],pageplus_L:false,pageplus_R:false,num:0}));
								}else{
									v_.$el.html(v_.template({menus_dt:dt[num_-1],pageplus_L:true,pageplus_R:false,num:num_-1}));
								}
						 	}else if(dt.length>num_+1){
						 		if(dt.length==1){
									v_.$el.html(v_.template({menus_dt:dt[0],pageplus_L:false,pageplus_R:false,num:0}));
								}else if(num_==0){
									v_.$el.html(v_.template({menus_dt:dt[0],pageplus_L:false,pageplus_R:true,num:0}));
								}else{
									v_.$el.html(v_.template({menus_dt:dt[num_],pageplus_L:true,pageplus_R:true,num:num_}));
								}
						 	}else{
						 		if(dt.length==1){
									v_.$el.html(v_.template({menus_dt:dt[0],pageplus_L:false,pageplus_R:false,num:0}));
								}else{
									v_.$el.html(v_.template({menus_dt:dt[num_],pageplus_L:true,pageplus_R:false,num:num_}));
								}
						 	}
						 	changeSelectMenuBgCss($('.'+menusSelectedFlag['seconeMenu'].nodeClass));
						 }else{
						 	if(dt.length==1){
								v_.$el.html(v_.template({menus_dt:dt[0],pageplus_L:false,pageplus_R:false,num:0}));
							}else{
								v_.$el.html(v_.template({menus_dt:dt[0],pageplus_L:false,pageplus_R:true,num:0}));
							}
						 }
						
						
					}
				});
			},
			 events:{  
					   "click #second_boxConR_storage":"changePage", 
					   "click #second_boxConL_stroage":"changePage",
					   "click div.category_container_Storage .storageSecondMenusBtn":"showStorageAndLoadThirdlyMenus"
				   },
				   
		    changePage:function(evt){
			    this.$el.html('');
			    var num=$(evt.target).data("num");
			    var pageplus=$(evt.target).data("pageplus");
			    var storageSecondMenus_dt_=this.collection.toJSON();
				    if (pageplus==1) {
				    	if(storageSecondMenus_dt_.length==(num+2)){
				    		this.$el.html(this.template({menus_dt:storageSecondMenus_dt_[num+1],pageplus_L:true,pageplus_R:false,num:num+1}));
				    	}else{
				    		this.$el.html(this.template({menus_dt:storageSecondMenus_dt_[num+1],pageplus_L:true,pageplus_R:true,num:num+1}));
				    	}
				    }else{
				    	if(num-1==0){
				    		this.$el.html(this.template({menus_dt:storageSecondMenus_dt_[num-1],pageplus_L:false,pageplus_R:true,num:num-1}));
				    	}else{
				    		this.$el.html(this.template({menus_dt:storageSecondMenus_dt_[num-1],pageplus_L:true,pageplus_R:true,num:num-1}));
				    	}
				    }
				    if(menusSelectedFlag['seconeMenu']){
				    	changeSelectMenuBgCss($('.'+menusSelectedFlag['seconeMenu'].nodeClass));
				    }
			   
		   		},
				   showStorageAndLoadThirdlyMenus:function(evt){
				   		$(".category_container_Storage").css("background","initial");//将所有class='category_container_Storage'背景颜色设置为无色
				   		var btnObj=$(evt.target);
				   		var p_buttonbox=changeSelectMenuBgCss(btnObj);
				   		var nodeClassArray=p_buttonbox.find('.category_icon_seconde').attr('class').split(' ');
				   		var num=$(".second_boxCon_storage").data('num');
				   		menusSelectedFlag['seconeMenu']={'nodeClass':nodeClassArray[nodeClassArray.length-1],'num':num};
				   		viewData["storageThirdlyMenusTabsView"]={"title":btnObj.data("name"),"kml":btnObj.data("kml"),"ps_id":btnObj.data("ps_id")};
				   		$(".location_img").unbind('click');
				   		$(".location_img").css('cursor','not-allowed');
				   		jsmap.isFitBounds["ps_id"]="a";
				   		var selectKmlOpFlag=false;
				   		if(btnObj.data("ps_id")!=jsmap.ps_id){
				   			selectKmlOpFlag=true;
				   			clearAllLayer();
				   			jsmap.ps_id=btnObj.data("ps_id");
				   		}
				   		
				   		storageThirdlyMenusTabsView.render(selectKmlOpFlag);
				   		arrayPush(lowerViewArray,storageThirdlyMenusTabsView);
				   		showOrHideAddLayer(true);//显示右键添加layer菜单
				   		preloadDataAjax(btnObj.data("ps_id"));//预加载storage图层所用的数据
				   }
		});
		
		return new storageSecond();
});