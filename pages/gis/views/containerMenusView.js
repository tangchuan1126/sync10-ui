define(["jquery","backbone","handlebars","templates",
	"../models/gis_collection","../config","../js/GoogleMapsV3",
    "../js/jsmap","../js/gis_main"]
	,function($,backbone,handlebars,templates,gis_collection,config){
		var containerMunusView=backbone.View.extend({
			template:templates.containerTemplate,
			collection:new gis_collection(),
			initialize:function(options){
			},
			render:function(options){
				var v_ =this;
				var ps_id =options.ps_id;
                var storageName=options.name;
				v_.setElement("#tab3");
				v_.collection.fetch({
					url:config.qureyContainer.url,
					success:function(){
						var data =v_.collection.toJSON();
						v_.$el.html(v_.template({container_dt:data,storageName:storageName}));
					}
					
				});
				
			}
		});
		return new containerMunusView();
})