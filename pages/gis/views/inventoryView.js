define(["jquery",
    "backbone",
    "handlebars",
    "templates",
    "../config",
    "../models/gis_collection",
    "../../../../Sync10/appkey/ContainerTypeKey",
    '../js/gis_main',
    '../js/GoogleMapsV3',
    '../js/jsmap', "../js/gis_main", "jquery.browser"
  ],
  function($,
    Backbone,
    Handlebars,
    templates,
    config,
    gis_Collection,
    containerTypeKey
  ) {
    var inventory = Backbone.View.extend({
      collection: new gis_Collection,
      template: templates.inventory,
      el: '.childNode',
      fitBoundsStorage:null,
      render: function() {
        var v_ = this;
        v_.$el.html('');
        var storageData, titleData;
        v_.collection.fetch({
          url: config.storageModel.url,
          success: function() {
            storageData = v_.collection.toJSON();
            var cntDt = returnContainerArray(containerTypeKey);
            v_.$el.html(v_.template({
              'storageData': storageData,
              'cntDt': cntDt
            }));
            var ps_id = $("#productStorageFrom #ps_id").val();
            if (ps_id) {
              v_.loadStorageDt(ps_id);
            }


            $("#productStorageFrom .titleScrollbar").height(150).mCustomScrollbar();
          }
        });

      },
      events: {
        'keyup #productStorageFrom #title': 'associatingInputingTitle',
        'click #productStorageFrom #inquiry': 'searchStorageCatalog',
        'click #productStorageFrom #title': 'showTitle',
        'click #productStorageFrom #p_line': 'showPLine',
        'keyup #productStorageFrom #p_line': 'associatingInputingPLine',
        'click #productStorageFrom #lot_numbers': 'showLotNum',
        'keyup #productStorageFrom #lot_numbers': 'associatingInputingLotNum',
        'change #productStorageFrom #ps_id': 'selectStroage'
      },
      associatingInputingTitle: function(evt) {
        var titleDt = this.getCollectionArr('titleDt');
        if (!$.isEmptyObject(titleDt)) {
          associatingInputing(evt, titleDt);
        }

      },
      searchStorageCatalog: function(evt) {
        var v_ = this;
        $.blockUI({
          message: '<h3><img src="imgs/loading.gif" />Loading...</h3>'
        });
        closeInventoryWind();

        var psId = $("#productStorageFrom #ps_id").val();
       
        var kmlname = $('#productStorageFrom #ps_id option:selected').data('kml');
        var polyline = jsmap.storageBasePolyline[psId];
        if(psId!=jsmap.ps_id){
           jsmap.ps_id=psId;
           if (!$.isEmptyObject(polyline)){
            polyline.setMap(jsmap.getMap());
            if(v_.fitBoundsStorage!=psId){
              jsmap.googleMap.fitBounds(polyline.data.bounds);
              v_.fitBoundsStorage=psId;
            }
          }else{
            getStorageBaseDataAjax(psId, true);
            v_.fitBoundsStorage=psId;
          }
          drawAreaByPsId(psId, false);
        }
        var arrayDt = this.collection.toJSON();
        var titleDt;
        for (var i = 0; i < arrayDt.length; i++) {
          var dt = arrayDt[i];
          for (var key in dt) {
            if (key == 'titleDt') {
              titleDt = dt['titleDt'];
            }
          }
        }

        var dt = InquiryDt(titleDt);
        //dt = inventoryQueryDt(containerTypeKey, dt);
        $(evt.target).next().data('queryDt', dt);
        queryInventoryFlagInMap(psId,dt);
      },
      showTitle: function(evt) {
        titleData = ajaxPostProductTitle();
        this.collection.add({
          'titleDt': titleData
        });
        var targetObj = $(evt.target);
        targetObj.data('titlesIndex', -1);
        targetObj.data('oldTitles', targetObj.val());
        if (!$.isEmptyObject(titleData)) {
          associatingInputing(evt, titleData);
        }

      },
      associatingInputingPLine: function(evt) {
        var pLineDt = this.getCollectionArr('pLineDt');
        if (!$.isEmptyObject(pLineDt)) {
          associatingInputing(evt, pLineDt, 'singleVal');
        }
      },
      showPLine: function(evt) {
        var v_ = this;
        var plineDt = {};
        var ps_id = $('#productStorageFrom #ps_id').val();
        $.ajax({
          url: config.queryProductLine.url,
          data: JSON.stringify({
            'ps_id': ps_id
          }),
          type: 'post',
          dataType: 'json',
          async: false,
          contentType: 'application/json; charset=UTF-8',
          success: function(data) {
            if (!$.isEmptyObject(data)) {
              v_.collection.add({
                'pLineDt': data
              });
              var targetObj = $(evt.target);
              targetObj.data('titlesIndex', -1);
              targetObj.data('oldTitles', targetObj.val());
              associatingInputing(evt, data, 'singleVal');
            }
          }
        })
      },

      showLotNum: function(evt) {
        var v_ = this;
        var plineDt = {};
        var ps_id = $('#productStorageFrom #ps_id').val();
        $.ajax({
          url: config.querylotNumbers.url,
          data: JSON.stringify({
            'ps_id': ps_id
          }),
          type: 'post',
          dataType: 'json',
          async: false,
          contentType: 'application/json; charset=UTF-8',
          success: function(data) {
            if (!$.isEmptyObject(data)) {
              v_.collection.add({
                'lotNumbersDt': data
              });
              var targetObj = $(evt.target);
              targetObj.data('titlesIndex', -1);
              targetObj.data('oldTitles', targetObj.val());
              associatingInputing(evt, data);
            }
          }
        })
      },
      associatingInputingLotNum: function(evt) {
        //var arrayDt = this.collection.toJSON();
        var lotNumbersDt = this.getCollectionArr('lotNumbersDt');
        /*for(var i=0;i<arrayDt.length;i++ ){
          var dt=arrayDt[i];
          for(var key in dt){
            if(key=='lotNumbersDt'){
              lotNumbersDt=dt['lotNumbersDt'];
            }
          }
        }*/
        if (!$.isEmptyObject(lotNumbersDt)) {
          associatingInputing(evt, lotNumbersDt);
        }
      },
      getCollectionArr: function(key) {
        var arrayDt = this.collection.toJSON();
        var arrDt = [];
        for (var i = 0; i < arrayDt.length; i++) {
          var dt = arrayDt[i];
          for (var key_ in dt) {
            if (key_ == key) {
              arrDt = dt[key];
            }
          }
        }
        return arrDt;
      },
      selectStroage: function(evt) {
        var ps_id = $(evt.target).val();
        this.loadStorageDt(ps_id);
      },
      loadStorageDt: function(ps_id) {
        if ($.isEmptyObject(jsmap.locationBounds[ps_id])) {
          getLocationBoundsAjax(ps_id, true);
        }
      }/*,
      waitLoadStorageDt: function(psId, data) {
        var v_ = this;
        var ps_id = psId;
        var dt = data;
        setTimeout(function() {
          if ($.isEmptyObject(jsmap.locationBounds[ps_id])) {
            v_.waitLoadStorageDt(ps_id, dt);
          } else {
            $.unblockUI();
            jsmap.showStorageCatalogLocation(ps_id, dt);
          }
        }, 500);
      }*/
    });
    return new inventory;
  })