define(["jquery",
  "backbone",
  "handlebars",
  "templates",
  "../config",
  "../models/gis_collection",
  "art_Dialog/dialog-plus",
  '../js/gis_main',
  '../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
    function($,backbone,handlebars,templates,config,gis_collection,dialog){
    	var dialog_product_info=backbone.View.extend({
    		template:templates.dialog_product_info,
    		collection:new gis_collection,
    		render:function(data){
    			var v_=this;
    			/*v_.collection.fetch({
    				data:'',
    				url:'',
    				success:function(){

    				}
    			});*/
    			var d=dialog({
    				title:'Product Info',
    				content:v_.template(),
    				backdropOpacity:0.2
    			}).showModal();
    		}
    	});
    	return new dialog_product_info;
})