define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",'../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
function($,backbone,handlebars,templates,gis_collection,dialog){
	var selectAreaView= backbone.View.extend({
		collection:new gis_collection,
		render:function(data,url,template,from_id){
			this.template=template;
			var v_=this;
			v_.collection.fetch({
				url:url,
				data:data,
				success:function(){
					var area_data=v_.collection.toJSON();
					var name;
					if(data.area_type==-1){
						name=$('#name').val();
					}else{
						name=$('#positionName').val();
					}
					
					var dataIsNull;
					if(area_data){
						dataIsNull=false;
					}else{
						dataIsNull=true;
					}
					var title;
					if(data.title){
						title=data.title;
					}else{
						if(data.area_type==-1){
							title="Select Area ";
						}else{
							title="Select Area ["+name+"]";
						}
						
					}
					var d=dialog({
						title:title,
						backdropOpacity:0.2,
						content:v_.template({area_data:area_data})
					}).showModal();
					d.addEventListener('close',function(){
						focusNum=0;

					})
					setTimeout(v_.loadingCickEvt(d,data,from_id),500);

				}
			});
			
		},
		loadingCickEvt:function(d,data,from_id){
			$(".location_updata_area").bind('click',function(){
				var title=d._.title;
				if(data.title){
					var el=this.parentNode;
					var info='Are you sure to employee '+$("#assignArea").data('employe_name')+' assigned to '+$(el).attr("title");
					maskLayer(el,info);
					
				}else{
					var from_id_="";
					if(data.area_type==-1){
						if(from_id){
							from_id_=from_id;
						}else{
							from_id_='modify_storage_layer';
						}
						
					}else{
						from_id_='modify_data';
					}
					selectArea_confirm(this.parentNode,from_id_);
				}
               d.close().remove();
            });
              
		}
		
	});
	return new selectAreaView;
})