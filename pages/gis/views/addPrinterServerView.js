define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",'../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
    function($,backbone,handlebars,templates,gis_collection,dialog){
		var addPrinterServerView= backbone.View.extend({
			collection:new gis_collection,
			render:function(url,template,ps_id){
				this.template=template;
				var v_=this;
				focusNum=1;
				v_.collection.fetch({
					url:url,
					success:function(){
						var d=dialog({
							id:'addPrinterServer',
							title:'Add Print Server',
							width:'350px',
							content:v_.template({allStorageKml:v_.collection.toJSON()}),
							backdropOpacity:0.2,
							button:[
								{
									value:'Submit',
									callback:function(){
										//var dialog_= dialog.get('printerServer');
										/*if(!ps_id){
											ps_id=currentPsId;
										}*/
										alterDialogDisabled("div[aria-labelledby='title:addPrinterServer'] button",true,'Submit');
										addServer(ps_id,this);
										return false;
									},
									autofocus: true
								},
								{
									value:'Cancel'
								}
							]
						});
						var ps_id_;
						if(ps_id){
							ps_id_=ps_id;
						}else{
							ps_id_=$('#ps_id').val();
						}
						addPrinter_server_initPs(ps_id_);
						d.showModal();
						
					}
				})
			}
		});
		
		return  new addPrinterServerView();
})
