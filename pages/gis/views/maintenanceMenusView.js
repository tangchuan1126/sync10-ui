define(["jquery","backbone","handlebars","templates",
	"../models/gis_collection","../js/GoogleMapsV3",
    "../js/jsmap","../js/gis_main","blockui"],
    function($,backbone,handlebars,templates,gis_collection){
	var maintenanceMenusView=  backbone.View.extend({
    		template:templates.maintenanceTemplate,
    		collection:new gis_collection(),
    		initialize:function(){
    		},
    		render:function(options){
    			var v_=this;
    			var ps_id =options.ps_id;
                var storageName=options.name;
    			v_.setElement("#tab2");
    			v_.$el.html(v_.template({ps_id:ps_id,storageName:storageName}));
    		    v_.loadBlockUICss();
            },
           loadBlockUICss:function(){
                $.blockUI.defaults = {
                    css: { 
                        padding:        '8px',
                        margin:         0,
                        width:          '230px', 
                        top:            '45%', 
                        left:           '40%', 
                        textAlign:      'center', 
                        color:          '#000', 
                        border:         '3px solid #999999',
                        backgroundColor:'#eeeeee',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius':    '10px',
                        '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                        '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
                    },
                    //设置遮罩层的样式
                    overlayCSS:  { 
                        backgroundColor:'#000', 
                        opacity:        '0.6' 
                    },
                    baseZ: 99999, 
                    centerX: true,
                    centerY: true, 
                    fadeOut:  1000,
                    showOverlay: true
                };
           }
    	});
	return new maintenanceMenusView();
})