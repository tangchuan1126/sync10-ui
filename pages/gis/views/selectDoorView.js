define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",
    '../config','../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
    function($,backbone,handlebars,templates,gis_collection,dialog,config){
    	var selectDoorView= backbone.View.extend({
    		collection:new gis_collection,
    		render:function(data,url,flag,template){
                this.template=template;
    			//var psId=$('#ps_id').val();
    			var v_=this;
    			this.collection.fetch({
    				url:url,
    				data:data,
    				success:function(){
                        var dt_=v_.collection.toJSON();
                        //if(dt_.length>0){
                            var title='';
                            var door_data_;
                            var dialogContent;
                            if(flag=='selectDock'){
                                var selectDocks=new Array();
                                door_data_=new Array();;
                                var dock_names;
                                if($("#areaName").val()){//area上选添加门
                                    title=$("#areaName").val()+' Info';
                                    dock_names=$("#dock_names").val();
                                   
                                    
                                }else{//添加area时选择门
                                    title='Select Docks';
                                    dock_names=$("#modify_storage_layer #dock").val();

                                }
                                for(var i=0;i<dt_.length;i++){
                                    if(dock_names && $.inArray(dt_[i].obj_name,dock_names.split(','))>=0){
                                        //continue;
                                        selectDocks.push(dt_[i]);
                                    }else{
                                        door_data_.push(dt_[i]);
                                    }
                                }
                                dialogContent=v_.template({door_data:door_data_,exit_dock_data:selectDocks});
                            }else if(flag=='selectDoor'){
                               title='Select Dock ['+$("#positionName").val()+']';
                               door_data_=dt_;
                               dialogContent=v_.template({door_data:door_data_});
                            }
                            var d=dialog({
                                    title:title,
                                    esc: true,
                                    id:'door_id_',
                                    backdropOpacity:0.2,
                                    content: dialogContent
                                    /*,
                                    onclose: function () {
                                        this.onblur();
                                       //document.getElementById('dock').blur();
                                    }*/
                                });
                            if(flag=='selectDock'){
                                d.button([
                                            {
                                                value:'Confirm',
                                                callback:function(){
                                                    confirm_door(this);
                                                },
                                                autofocus: true
                                            },
                                            {
                                                value:'Cancel'
                                            }
                                        ]  )
                            
                            }
                            d.showModal();
                            setTimeout(v_.loadingCickEvt(d),500);
                            d.addEventListener('close',function(){
                                focusNum=0;
                            })
                        //}
                        
    				}
    			})
    		},
            loadingCickEvt:function(d){
               $(".location_updata_doors").bind('click',function(){
                   updata_door_confirm(this.parentNode);
                   d.close().remove();
                });
               $(".location_updata_door_add_img").bind('click',function(){
                   updata_door_confirm(this.parentNode);
                   d.close().remove();
                });
            }
    	});
    return new selectDoorView;
})