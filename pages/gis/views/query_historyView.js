define(['jquery',"backbone","handlebars",
    "templates",'art_Dialog/dialog-plus',
    "bootstrap.datetimepicker",
    "require_css!bootstrap.datetimepicker-css",
    "../js/gis_main"]
    ,function($,backbone,handlebars,templates,dialog,datetimepicker){
        var query_historyView=backbone.View.extend({
            template:templates.query_history,
            render:function(aid,name){
                var v_=this;
                var dt=new Array();
                    for(var i=-12; i<=12; i++){
                        var s = "";
                        if(i>=0) s = "+0"+i;
                        if(i>9) s = "+"+i;
                        if(i<0) s = "-0"+(-i);
                        if(i<-9) s = "-"+(-i);
                        s = "GMT"+s+"00";
                        dt.push({'val':i,'text':s});
                    }
                var d=dialog({
                    title:"Historical track ["+name+"]",
                    content: v_.template({dt:dt,aid:aid}),
                    backdropOpacity: 0.2, // 透明度
                    button: [{
                        value: 'Query',
                        callback: function() {
                            queryHistory();
                            return false;
                        },
                        autofocus: true
                    }, {
                        value: 'Playback',
                        callback:function(){
                            historyPlay_(this);
                            return false;
                        }
                    }]
                }).showModal();
                //初始化时区
			 var d = new Date();
			 var timeZone = -d.getTimezoneOffset()/60;
			 $("#timeZone").val(timeZone);
			 var year = d.getFullYear();
			 var month = d.getMonth()+1;
			 var day = d.getDate();
			 var hours = d.getHours();
			 var min = d.getMinutes();
			 var sec = d.getSeconds();
			 month = month > 9 ? month : "0"+month;
			 day = day > 9 ? day : "0"+day;
			 hours = hours > 9 ? hours : "0"+hours;
			 min = min > 9 ? min : "0"+min;
			 sec = sec > 9 ? sec : "0"+sec;
			 var startTime = year+"-"+month+"-"+day+" 00:00:00";
			 var endTime = year+"-"+month+"-"+day+" "+hours+":"+min+":00";
			 $("#startTime").val(startTime);
			 $("#endTime").val(endTime); 
		    var dssr=$(".form-datatime").datetimepicker({format:'yyyy-mm-dd hh:ii:ss',language:'en', startView:2,autoclose: true,
            forceParse: 2}).on('changeDate',function(ev){
                if(ev.target.id=='startTime'){
                    var date=Date.parse(ev.date)-8*3600000;
                    $("#endTime").datetimepicker('setStartDate', new Date(date));
                }
            });
			}
        });
        return new query_historyView;
})
