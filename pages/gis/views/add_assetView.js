define(['jquery',
        "backbone",
        "handlebars",
        'templates',
        "../models/gis_collection",
        "../config",
        "art_Dialog/dialog-plus",
        "../js/gis_main",
        '../js/GoogleMapsV3',
        '../js/jsmap'],
        function($,backbone,handlebars,templates,gis_collection,page_config,dialog){
        	var add_asset=backbone.View.extend({
        		collection:new gis_collection,
        		template:templates.add_asset,
        		render:function(){
        			var v_=this;
        			v_.collection.fetch({
        				url:page_config.qureyAllGroup.url,
        				success:function(){
        					var groupData=v_.collection.toJSON();
        					var d=dialog({
		        				title:'Add GPS',
		        				backdropOpacity:0.2,
		        				content:v_.template({groupData:groupData}),
		        				button:[
		        					{
	        						value:'Submit',
	        						callback:function(){
                                            saveAddAsset(this);
                                            return false;
	        						},
	        						autofocus:true
		        					},
		        					{
	        						value:'Cancel'
		        					}
		        				]
		        			}).showModal();
                            setTimeout(v_.loadEvent(),500);
                            d.addEventListener('close', function () {
                                verifyMgs={};
                                this.remove();
                            });
        				}
        			});
        			
        		},
                        loadEvent:function(){
                               $("#group").change(function(){  
                                        if($("#group").val()=="0"){
                                                //$("#group").attr("style","display:none");
                                                $("#newGroup_td").html('<span class="input-group-addon" style="width:52%;">GroupName:</span><input id="newGroup" class="form-control" type="text"  name="newGroup" style="width:100%;line-height:20px;color:silver;" value="*Group name" onfocus="inputIn()" onblur="outInput()" /><span class="glyphicon form-control-feedback"  style="top:0;"></span>');
                                        }else{
                                                $("#newGroup_td").html("&nbsp;");
                                        }
                                });
                               $("#gps_tracker").blur(function(){
                                        var imei = $("#gps_tracker").val().trim();
                                        if(imei == ""){
                                                return;
                                         }
                                         $.ajax({
                                                url:systenFolder+'action/administrator/checkin/AjaxFindAssetByGPSNumberAction.action',
                                                data:'gps_tracker='+$("#gps_tracker").val().trim(),
                                                dataType:'json',
                                                type:'post',
                                                beforeSend:function(request){
                                            
                                                },
                                                success:function(data){
                                                        if(!$.isEmptyObject(data)){
                                                                verifyMgs['gps_tracker']=0;
                                                                showErrorVerifyMsg('#add_asset #gps_tracker');
                                                                $("#flag").val("true");
                                                        }else{
                                                              showRightVerifyMsg('#add_asset #gps_tracker');
                                                                $("#flag").val("");
                                                        }
                                                        
                                                },
                                                error:function(){
                                                        showMessage("System error","error"); 
                                                }
                                         });
                                 });
                        }
        	});
        	return new add_asset;
})