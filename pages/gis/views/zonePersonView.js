define(['jquery',"backbone","handlebars","templates",
    	"../models/gis_collection","art_Dialog/dialog-plus",'../js/GoogleMapsV3',
        '../js/jsmap',"../js/gis_main"],
        function ($,
        		backbone,
        		handlebars,
        		templates,
        		gis_collection,
        		dialog){
		var zonePersonView= backbone.View.extend({
			
			template:templates.show_zone_person,
			
			collection :new gis_collection(),
			
			render:function(title,data,url){
				var v_=this;
				v_.collection.fetch({
				url:url,
				data:data,
				success:function(datas){
					var array_data=v_.collection.toJSON();
					var adids,employe_names;
					for(var i=0;i<array_data.length;i++){
						if(adids){
							adids+=","+array_data[i].adid;
						}else{
							adids=array_data[i].adid;
						}
						if(employe_names){
							employe_names+=","+array_data[i].employe_name;
						}else{
							employe_names=array_data[i].employe_name;
						}
					}
					var d ;
					if(dialog.get('showZonePerson')){
						d=dialog.get('showZonePerson').title(title)
						.content(v_.template({zonePerson_data:array_data,name:title,adids:adids,ps_id:data.ps_id,area_id:data.area_id,employe_names:employe_names}));
					}else{
					 d=dialog({
					    title: title,
					    id:"showZonePerson",
					    esc: true,
					    backdropOpacity:0.2,
					    content: v_.template({zonePerson_data:array_data,name:title,adids:adids,ps_id:data.ps_id,area_id:data.area_id,employe_names:employe_names}),
					    button:[
								{
						            value: 'Submit',
						            callback: function () {
						            	alterDialogDisabled("div[aria-labelledby='title:showZonePerson'] button",true,'Submit');
						            	zonePersonCommitData();
						            },
						            autofocus: true
						        },
						        {
						            value: 'Cancel'
						        }
						       
					    ],
					    onclose: function () {
					    }
					 		});
		    			}
					d.showModal();
					//屏蔽鼠标右键菜单
					$(document).find("#zonePersons").bind("contextmenu",function(e){
						return false;
					});
					}
				})
			}
		
		});
		
		return new zonePersonView;
	
})