define(['jquery',"backbone","handlebars","templates","./query_historyView","./loc_frequencyView"],
    function($,backbone,handlebars,templates,query_historyView,loc_frequencyView){
        var truckMarkerMenuView=backbone.View.extend({
            template:templates.truckMarkerRightMenu,
            el:'#truckMarkerMenu',
            render:function(){
                this.$el.html('');
                this.$el.html(this.template());
            },
            events:{
                'click .truckMarkerMenu_li':'truckMarkerMenuEvent'
            },
            truckMarkerMenuEvent:function(evt){
                var menuNodeId=$(evt.target).attr('id');
                var data=$(evt.target).parents().find('#truckMarkerMenu').data('data');
                var aid=data.aid;
                var name=data.nam;
                var call=data.cal;
                var p='1,1';
                if(menuNodeId=='History'){
                    query_historyView.render(aid,name);
                }else if(menuNodeId=='LocFrequency'){
                	loc_frequencyView.render(data);
                }else if(menuNodeId=='KeyPoint'){
                	
                }else if(menuNodeId=='Road'){

                }else{
                    
                }
                
            }

        });
        return new truckMarkerMenuView();

})
