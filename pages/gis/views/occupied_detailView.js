define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",
    '../config','js/tablecloth','../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main",'../js/handlebars_ext'],
    function($,backbone,handlebars,templates,gis_collection,dialog,config,tablecloth){
    	var occupiedDetail= backbone.View.extend({
    		collection:new gis_collection,
    		template:templates.docks_parking_ocupied,
    		render:function(data,name){
    			var v_=this;
    		    v_.collection.fetch({
    		    	url:config.occupiedDetail.url,
    		    	data:data,
    		    	success:function(){
    		    		var datas=v_.collection.toJSON();
    		    		if($.isEmptyObject(datas)){
    		    			return ;
    		    		}
    		    		var _title="";
    		    		if(data.type==3){
    		    			_title="Dock[ "+name+" ]";
    		    		}else{
    		    			_title="Parking[ "+name+" ]";
    		    		}
    		    		var d=dialog({
    						title:_title,
    						width:880,
    						content:v_.template({datas:datas}),
    						id:'occupied',
    						button:[
    							{
    								value:'Cancel'
    							}
    						]
    					});	
    		    		
    					d.addEventListener('close', function () {
    						this.remove(); 
    					});
    					d.show();
    				}
    		    });
              
    	    }
    	
    	});
    return new occupiedDetail;
})