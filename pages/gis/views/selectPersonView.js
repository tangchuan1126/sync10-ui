define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"
	],
	function($, backbone, handlebars, templates, gis_collection, dialog,config) {
		var selectPersonView= backbone.View.extend({
			collection: new gis_collection,
			render: function(data,template) {
				this.template = template;
				var v_ = this;
				v_.collection.fetch({
					url: config.queryPersonByPsId.url,
					data: {
						ps_id: data.psId
					},
					success: function() {
						var p_data = v_.collection.toJSON();
						var p_dt = new Array();
						var nodes=$("#notExitDoor").children();
						var employe_names = new Array();
						for(var i=0;i<nodes.length;i++){
							employe_names.push(nodes[i].title);
						}
						var exitPersonDt=new Array();
						for (var i = 0; i < p_data.length; i++) {
							if ($.inArray(p_data[i].employe_name, employe_names) >= 0) {
								exitPersonDt.push(p_data[i]);
							} else {
								p_dt.push(p_data[i]);
							}
						}
						var exit_data=false;
						if(exitPersonDt.length>0){
							exit_data=true;
						}
						var d = dialog({
							title: 'Select person[' + $("#areaName").val() + ']',
							content: v_.template({
								exit_title_data:exitPersonDt,person_data: p_dt
							}),
							backdropOpacity: 0.2, // 透明度
							button: [{
								value: 'Confirm',
								callback: function() {
									confirm_person(this);
								},
								autofocus: true
							}, {
								value: 'Cancel'
							}]
						}).showModal();
					}
				})
			}
		});
	return new selectPersonView;
	})