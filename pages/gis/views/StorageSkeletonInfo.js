define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",
    '../config','../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"]
	,function($,backbone,handlebars,templates,
		gis_collection,dialog,config){
	var addProductStorgeCatalog=backbone.View.extend({
		collection:new gis_collection,
		render:function(el){
			var v_=this;
			verifyMgs={};
			v_.template=templates.StorageSkeletonInfo;
			v_.setElement(el);
			v_.$el.html('');
			v_.collection.fetch({
				url:config.qureyStorageWithoutOnMap.url,
				success:function(){
					var req_Dt=v_.collection.toJSON()[0];
					if(req_Dt['flag']==true){
						var storageData=req_Dt.data;
						v_.$el.html(v_.template({storageData:storageData}));
						setTimeout(function(){
							$("#locationStorage").bind('keypress',function(evt){
								if(evt.keyCode ==13){
									locationStorage_();
								}
							})
						},500);
					}else if(req_Dt['flag']=='authError'){
						showMessage("Auth error!", "error");
					}
					
				}
			});
			
		}
		
	})
	return new addProductStorgeCatalog;
})