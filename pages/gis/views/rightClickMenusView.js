define(['jquery', "backbone", "handlebars", "templates", "art_Dialog/dialog-plus",
		"../models/gis_collection", "../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main", "../js/verify", "bootstrap"
	],
	function($, backbone, handlebars, templates, dialog, gis_collection, config) {
		var rightClickMenus = backbone.View.extend({
			collection: new gis_collection,
			render: function(options) {
				verifyMgs={};
				focusNum = 0;
				verifyMgs={};
				this.setElement(options.el);
				this.template = options.template;
				this.$el.html('');
				//渲染右键菜单
				var right_dt = [
				{
					'id': 'modifyPosition',
					'name': 'Modify Layer'
				}, {
					'id': 'addMultiLayer',
					'name': 'Add Multi Layer'
				}, {
					'id': 'modifyTitle',
					'name': 'Modify Title'
				}, {
					'id': 'stopUse',
					'name': 'Stop Use'
				}, {
					'id': 'startUse',
					'name': 'Reuse'
				}, {
					'id': 'addPerson',
					'name': 'Add Person'
				}, {
					'id': 'addDock',
					'name': 'Add Dock'
				},{
					'id':'addWebcam',
					'name':'Add Webcam'
				},{
					'id':'addPrinter',
					'name':'Add Printer'
				},{
					'id':'addRectangle',
					'name':'Add Rectangle'
				}];
				this.$el.html(this.template({
					data: right_dt
				}));
			},
			events: {
				'click li.map_rightClick': 'right_click_option'
			},
			right_click_option: function(evt) {
				var id = $(evt.target).data('id');
				var polygon = jsmap.storageDemoLayer["drag"];
				if (id == "modifyPosition") {
					if (polygon) {
						stopNextActionDialog();
						return;
					}
					this.modify_position_click(evt);
				} else if (id == "stopUse") {
					stopUse();
				} else if (id == "startUse") {
					startUse();
				} else if (id == "addPerson") {
					addPerson();
				} else if (id == "addDock") {
					addDock();
				} else if (id == "modifyTitle") {
					addZoneTitle();
				} else if (id == "addMultiLayer") {
					this.addMultiLayer(evt);
				}else if (id == "addWebcam") {
					var IsComplet=lastAddLayerIsCom[6].call();
					if(!IsComplet){
						return;
					}
					LayerMouseStop(evt,6)
				}else if (id == "addPrinter") {
					var IsComplet=lastAddLayerIsCom[7].call();
					if(!IsComplet){
						return;
					}
					LayerMouseStop(evt,7)
				}else if (id == "addRectangle") {
					var IsComplet=lastAddLayerIsCom['x'].call();
					if(!IsComplet){
						return;
					}
					LayerMouseStop(evt,'x')
				}

			},
			modify_position_click: function(evt) {
				var type = $('#modifyPosition').data('data-state');
				var key = $('#modifyPosition').data('data-key');
				var name = $('#modifyPosition').data('data-name');
				var psId = key.split('_')[0];
				var id = "";
				var title = null;
				var layer;
				var area_id = "";
				if (type == 1) {
					title = "Modify Location [";
					id = key.split('_')[1];
					layer = jsmap.storageLocationPolygon[key];
					area_id = layer.data.area_id;
				} else if (type == 2) {
					title = "Modify Staging [";
					id = key.split('_')[1];
					layer = jsmap.storageStagingPolygon[key];
				} else if (type == 3) {
					title = "Modify Docks [";
					id = key.split('_')[2];
					layer = jsmap.storageObjPolygon[key];
				} else if (type == 4) {
					title = "Modify Parking [";
					id = key.split('_')[2];
					layer = jsmap.storageObjPolygon[key];
				} else if (type == 5) {
					title = "Modify Zone[";
					id = key.split('_')[1];
					layer = jsmap.storageAreaPolyline[key];
				}

				var path = layer.getPath();
				jsmap.clearSingleLayer(key, type); //清除图层delete 数组元素
				title += name.toUpperCase() + "]";
				//重新查询条件
				var data = {
					ps_id: psId,
					name: name,
					type: type
				}
				var template = null;
				if (type != 5) {
					template = templates.modify_location;
				} else {
					template = templates.modify_area;
				}
				var url = config.querySingleLayerInfo.url;
				var v_ = this;
				v_.collection.fetch({

					url: url,
					data: data,
					success: function() {
						var datas = v_.collection.toJSON();
						var dbrow = datas[0];
						if (dbrow.angle == undefined) {
							dbrow['angle'] = 0;
						}


						var type = data.type ? data.type : "";
						var d = dialog({
							title: title,
							width: 350,
							esc: true,
							id: "modify",
							content: template({
								data: dbrow,
								type: type
							})
						});
						if (type && type != 5) {
							if (type == 1) {
								jsmap.flag["drawLocation"] = false;
							}
							drawDraggableLayer(path, psId, dbrow.angle, "modify_data");
							d.button([{
									value: 'Submit',
									callback: function() {
										
										alterDialogDisabled("div[aria-labelledby='title:modify'] button", true, 'Submit');
										var this_ = this;
										var obj = $("#modify_data #positionName");
										v_.ajaxVerifyNameAndSubmit(this_, dbrow,type,psId,obj,modify_location_save);
										//localtion staging  docks parking 图层
										return false;
									},
									autofocus: true
								}, {
									value: 'Cancel',
									callback: function() {
										this.close();
									}
								}

							]);
							if (type == 1) {
								$("#modify_data #_dimensional").show();
								$("#modify_data #_dimensional .gis_main_select").val(dbrow.is_three_dimensional);
							}
						} else {
							drawDraggableLayer(path, psId, dbrow.angle, "modify_area");
							d.button([{
									value: 'Submit',
									callback: function(evt) {
										alterDialogDisabled("div[aria-labelledby='title:modify'] button", true, 'Submit');
										var this_ = this;
										var obj = $("#modify_area #zone_name");
										v_.ajaxVerifyNameAndSubmit(this_, dbrow,type,psId,obj,modify_area_save);
										//modify_area_save(dbrow, this);
										return false;
									},
									autofocus: true
								}, {
									value: 'Cancel',
									callback: function() {
										this.close();
									}
								}

							]);
							if (dbrow.type == 3) {
								$("#area_subtype").show();
								if (dbrow.area_subtype) {
									$("#area_subtype option[value=" + dbrow.area_subtype + "]").attr('selected', 'selected');
								}
							}
						}
						d.addEventListener('close', function() {
							jsmap.getMap().keyboardShortcuts=true;//运行map上使用快捷键
							clearstorageDemoLayer();
							var flag = "cancel";
							if (this.returnValue) {
								flag = this.returnValue;
							}
							this.remove();
							if (type == "1") {
								reLoadLocation(psId, flag, area_id, id);
								jsmap.flag["drawLocation"] = true;
							} else if (type == "2") {
								reLoadStaging(psId, flag, id);
							} else if (type == "3") {
								reLoadDocks(psId, flag, id);
							} else if (type == "4") {
								reLoadParking(psId, flag, id);
							} else if (type == "5") {
								reLoadArea(psId, flag, id);
							}
							this.remove();
							verifyMgs = {};
						});
						if (type == "1" || type == "3" || type == "4") {
							$("#_zone").show();
							$("#_dock").hide();
						}
						if (type == "2") {
							$("#_zone").hide();
							$("#_dock").show();
						}
						d.show();
					}
				})
			},
			ajaxVerifyNameAndSubmit: function(_this,dbrow,type,psId,obj,submitFn) {
				
				var name;
				if (obj.length)
					name = $.trim(obj.val());
				var data = {
					"ps_id": psId,
					"name": name,
					"type": type
				};

				var url = systenFolder + 'action/administrator/gis/verifyName.action';
				if (dbrow.obj_name != name) {
					postVerify(data, url, obj, function(reval, OBJ) {

						if (reval == "true") {

							submitFn(dbrow, _this);

						} else {
							alterDialogDisabled("div[aria-labelledby='title:modify'] button", false, 'Submit');
							verifyMgs['verifyName'] = 0;
							showErrorVerifyMsg(OBJ, 'Name can not repeat!');
							return false;
						}

					});
				} else {
					verifyMgs['verifyName'] = 1;
					submitFn(dbrow, _this);
				}
			}
			/*,
			addMultiLayer:function(evt){
				var v_=this;
				var key=$(evt.target).parent().data("data-key");
				var type=$(evt.target).parent().data("data-state");
				var name=$(evt.target).parent().data("data-name");
				var psId=key.split('_')[0];
				var dt={
					ps_id:psId,
					name:name,
					type:type
				}
				var id=key.split('_')[1];
				var layer=jsmap.storageAreaPolyline[key];
				var path=layer.getPath();
				jsmap.clearSingleLayer(key,type);//清除图层delete 数组元素
				v_.collection.fetch({
					url:config.querySingleLayerInfo.url,
					data:dt,
					success:function(){
						var datas=v_.collection.toJSON();
						var	dbrow=datas[0];
						if(dbrow.angle==undefined){
							dbrow['angle']=0;
						}
						var type=dt.type?dt.type:"";
						var d=dialog({
							    title: "Add Multi Layer",
							    width:350,
							    esc: true,
							    id:"addMultiLayer",
							    content: templates.addMultiLayer({ps_id:dt.ps_id,storageName:dt.name,isExistForm:false,dbrow:dbrow}),
							    button:[
							    	 {
			                            value:'Save',
			                            callback:function(){
			                                alterDialogDisabled("div[aria-labelledby='title:addMultiLayer'] button",true,'Save');
			                               var layerTabVal=$(".modify_storage_layer_div input[name='options']:checked").val();
		                                    addMutliLayer(this,"addMultiLayer"); 
			                                return false;
			                            },
			                            autofocus: true
			                        },
			                        {
			                            value:'Cancel'
			                        }
							    ]
							}).show();
						drawDraggableLayer(path,psId,dbrow.angle,"modify_storage_layer");
						v_.createScroll();
						 d.addEventListener('close',function(){
						 	var flag="cancel";
						 	clearstorageDemoLayer();
						 	
						 	clearAutoLayer();
						 	if(this.returnValue){
						 		flag='add';
	                        	var  child_type=$("#modify_storage_layer input[name='child_type']:checked").val();
	                        	if(child_type==1){
	                        		delete	jsmap.storageLocation[psId];
	                        	}
	                        	if(child_type==2){
	                        		delete jsmap.storageStaging[psId];	
	                        	}	
	                            delete jsmap.storageTitle[psId];
	                            delete jsmap.storageResource[psId]
						 	}
                    		reLoadArea(psId,flag,id);
                    		this.remove();
						 });
					}
				})
			},
           createScroll:function(){
            $(".modify_storage_layer_div").mCustomScrollbar();

           }*/

		});
		return new rightClickMenus;
	})
