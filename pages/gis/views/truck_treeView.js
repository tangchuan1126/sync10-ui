define(['jquery',"backbone","handlebars","templates","../models/gis_collection",'../js/gis_main',
	'../views/storageThirdlyMenusTabsView',"art_Dialog/dialog-plus"]
	,function($,backbone,handlebars,templates,gis_collection,gis_main,storageThirdlyMenus,dialog){
		var truck_treeView= backbone.View.extend({
			template:templates.truck_tree,
			collection:new gis_collection(),
			initialize:function(){
        	},
			render:function(data){
				this.setElement('#bottom_menus');
				var group_id=data.group_id;
				var title=data.group_name;
				var v_= this;
				
				v_.collection.fetch({
					url:'/Sync10/_gis/truckInfoCotroller/qureyAssetByGroup',
					data:{groupId:group_id},
					success:function(response){
					var dt_=response.toJSON();
					var groupId;
					if(dt_.length>0){
						groupId=dt_[0].group_id;
					}
			        /*v_.collection.reset(dt_);
			        var truckThirdMene_dt=	v_.collection.toJSON();*/
			        v_.$el.html('');
			        v_.$el.html(v_.template({menus_dt:dt_,title:title,groupId:groupId}));
			        /*if(truckThirdMene_dt.length==1){
			        	v_.$el.html(v_.template({menus_dt:truckThirdMene_dt[0],pageplus_L:false,pageplus_R:false,num:0,title:title}));
			        }else{
			        	v_.$el.html(v_.template({menus_dt:truckThirdMene_dt[0],pageplus_L:false,pageplus_R:true,num:0,title:title}));
			        }*/
					}
				});
			}
		});
	return new truck_treeView;
});