define(
		[  "jquery",
		   "backbone",
		   "handlebars",
		   "templates"]
		, function($,
				Backbone,
				Handlebars,
		        templates){
			
			var mapToolsView= Backbone.View.extend({
				   el:"#map_tool_control",
				   template:templates.mapTool,
				   render:function (){
					   var data={"tools":[{"id":"measure_area","img":"./imgs/biaochi.png","name":"Area"},
					                      {"id":"measure_distance","img":"./imgs/biaochi.png","name":"Distance"},
					                      {"id":"move_map","img":"./imgs/pan1.png","name":"Move Map"},
					                      {"id":"display_legend","img":"./imgs/setting.png","name":"Set Refresh Time"},
					                      {"id":"coordinate_system","img":"./imgs/coordinate_sys.png","name":"Coordinate System"}
					                      ]};
					   this.$el.html(this.template({tools:data.tools}));
				   },
			});
			return new mapToolsView();
		})