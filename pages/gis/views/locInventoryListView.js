requirejs.config({
    paths: {
        "_3d":"./js/3d_store",
        "css":"/Sync10-ui/pages/gis/css/object3d"
    },
    shim: {
    	"_3d" : {
        	deps:['require_css!css']
        }
    }
});

define(["jquery",
    "backbone",
    "handlebars",
    "templates",
    "art_Dialog/dialog-plus",
    "../models/gis_collection",
    "../config",
    "../../../../Sync10/appkey/ContainerTypeKey",
    '_3d',
    'three',
    'tween',
    "mCustomScrollbar",
    "jquery.browser"
  ],
  function($, backbone, handlebars, templates, dialog, gis_collection, config, containerTypeKey, _3d) {
    var locInventory = backbone.View.extend({
      template: templates.locInventoryList,
      el: '#main_inventory',
      collection: new gis_collection(),
      _3d_store:null,
      titleDt : null,
      render: function(data) {
        var v_ = this;
        $.blockUI({
          message: '<h3><img src="imgs/loading.gif" />Loading...</h3>'
        });
        var dt = data;
        v_.collection.fetch({
          url: config.qureyInvenByslc.url,
          data: JSON.stringify(dt),
          type: 'post',
          contentType: "application/json; charset=UTF-8",
          success: function(response) {
            var collection = v_.collection;
            var datas = collection.toJSON()[0];
            var cntDt = returnContainerArray(containerTypeKey);
            v_.collection.add({
              'titleDt': v_.titleDt
            });
            if (!$.isEmptyObject(datas)) {
              dt['name'] = datas['slc_position'];
              v_.$el.html(v_.template({
                'queryDt': dt,
                'cntDt': cntDt
              }));
              $("#inventoryQueryForm .titleScrollbar").height(150).mCustomScrollbar();
              $("#locInventoryEle").height(v_.windowofset().height).width(v_.windowofset().width);
              var data = datas['containers'];
              if (!$.isEmptyObject(data)) {

                if ($("#main_inventory").is(':hidden')) {
                  var height = v_.windowofset().height;
                  var width = v_.windowofset().width;
                  $("#main_inventory").show();
                  $("#main_inventory").animate({
                    height: height + "px"
                  }, 300);
                }
                var cntVal = !$.isEmptyObject(dt['container_type']) ? dt['container_type'] : '0';

                $("#inventoryQueryForm #container_type").val(cntVal);
                v_.containerTypeKeyToLowerCase();
                var h = v_.windowofset().height;
                var width=v_.windowofset().width;
                $("#inventory_model").css({'left':(width/2-45)+"px"});
                var height=h-25-$("#main_inventory #inventoryQueryForm").height();
                $("#inventory_cont").css('height',height);
                $("#inventory_tab").hide();
                v_._3d_store = v_.build_3d(datas);
               var html = v_.loadContent(data, v_.titleDt);
                $("#locInventoryEle #inventory_content").html("<table width='100%' class='table table-striped'>" + html + "</table>");                
                $("#inventory_model #inventory3D").css({'background-color': 'rgba(9, 139, 124, 0.8)'});
              }
              setTimeout(function() {
                $(".tree_op").click(function(evt) {
                  v_.showOrHideChlidrenEle(evt);
                });
              }, 500);              
               v_.createScroll('inventory_content');
            } else {
              var inventory_contentEle = $("#locInventoryEle #inventory_content")
              if (inventory_contentEle.length != 0) {
                var cntVal = !$.isEmptyObject(dt['container_type']) ? dt['container_type'] : '0';
                $("#inventoryQueryForm #container_type").val(cntVal);
                inventory_contentEle.html('').html("<table width='100%' class='table table-striped'><tr><td style='text-align: center;'>No data...</td></tr></table>");
              }
              showMessage("No inventory!", "error");
            }
            
            $.unblockUI();
            $("tr").find("[style='']").addClass('last_tr');
          },
          error: function() {
            $.unblockUI();
          }
        });
        this.titleDt = ajaxPostProductTitle();
      },
      events: {
        'click #closeWin': 'closeInventoryList',
        'click #inventoryQueryForm #queryInventory': 'queryInventory',
        'keyup #inventoryQueryForm #title': 'associatingInputingTitle_',
        'click #inventoryQueryForm #title': 'showTitle_',
        'keyup #inventoryQueryForm #p_line': 'associatingInputingPLine_',
        'click #inventoryQueryForm #p_line': 'showPLine_',
        'keyup #inventoryQueryForm #lot_number': 'associatingInputingLotNum_',
        'click #inventoryQueryForm #lot_number': 'showLotNum_',
        'click #inventory_model #inventory3D':'show3DInventory',
        'click #inventory_model #inventoryList':'showInventoryList',
        'click #inventory_model #clearSelect3DObj':'clearSelect3DObj'
      },
      createScroll: function(eleId) {
        var h = this.windowofset().height;
        var height=h-25-$("#main_inventory #inventoryQueryForm").height();
        var height1=height - $("#thead1").height()-$("#thead2").height();
        $("#" + eleId).height(height1-40).mCustomScrollbar();

      },
      windowofset: function() {
        var h = $(".gis_main").height();
        var width_ = $(".gis_main").width();
        return {
          height: h,
          width: width_
        }
      },
      loadContent: function(data, titleDt, children_con_class) {
        var html = ""

        for (var i = 0; i < data.length; i++) {
          var dt = data[i];
          var con_id=dt['con_id'];
          if (con_id) {
            var container_img = "close_img";
            var collapsed_img = "";
            var childrenDt = dt['children'];
            var tr_class = dt['con_id'];
            var tree_class = "tree-collapsed";
            var tr_isShow = "";
            var spaceSpan = "";
            var indent_width = 0;
            var container_total;
            if (!$.isEmptyObject(children_con_class)) {
              tr_class = children_con_class + " " + dt['con_id'];
              var arr_class = children_con_class.split(' ');
              for (var j = 0; j < arr_class.length; j++) {
                indent_width += 16;
                spaceSpan += "<span class='tree-indent'></span>";
              }
              spaceSpan += "<span class='tree-indent'></span>";
              tr_isShow = "display:none;";
             
              
            } else if ($.isEmptyObject(children_con_class) && $.isEmptyObject(childrenDt)) {
              spaceSpan += "<span class='tree-indent'></span>";
              indent_width += 16;
            }
            if ($.isEmptyObject(childrenDt)) {
              container_img = "not_children";
              indent_width += 16;
              tree_class = "";

            } else if (!$.isEmptyObject(children_con_class) && !$.isEmptyObject(childrenDt)) {
              container_img = "open_img";
              tree_class = "tree-expanded";
              indent_width += 32;
            }
            var container_td_width = $('.container').width();
            if (indent_width == 0) {
              indent_width = 32;
            }
            var language = browserLanguage();
            var damage=false;
            if(dt['damage']!=undefined){
             damage=dt['damage']==1?false:true;
            }
            //var damage = $.isEmptyObject(dt['damage'] + '') == true ? false : (dt['damage'] + '') == '1' ? false : true;
            var damageClass = "";

            if (damage)
              damageClass = "danger";
            var containerType_ = $.isEmptyObject(containerTypeKey[dt['container_type']][language]) == true ? '--' : containerTypeKey[dt['container_type']][language];
            var container=dt['container'];
            var title_width = container_td_width - indent_width - 51;
            html += "<tr class='" + damageClass + " " + tr_class + "' style='" + tr_isShow + /*";"+background+*/ "'>" +
              "<td class='container'><div>" + spaceSpan + "<span class='tree_op " + tree_class + "'>" 
              + "</span><span class='" + container_img + "'></span>" 
              + "<span class='container_title' title='" + container + "' style='width:" + title_width + "px;'>" + container + "</span>" +
              "</div></td><td class='container_type'>" + containerType_+ "</td>"
            var products = dt['products'];
            if (!$.isEmptyObject(products)) {
              var title = "NA";
              for (var j = 0; j < titleDt.length; j++) {
                if (titleDt[j].obj_id == products[0].title_id) {
                  title = titleDt[j].obj_name;
                  break;
                }
              }
              var quantity=products[0].quantity;
              var total_quantity=products[0].total_quantity;
              var locked_quantity=products[0].locked_quantity;
              var total_locked_quantity= products[0].total_locked_quantity;
  
              
              var sn=products[0].sn;
              if(sn){
                sn=sn.toString();
              }
              html += "<td class='p_name' title='" + products[0].p_name + "'>" + products[0].p_name + "</td><td class='product_line' title='" + products[0].product_line + "'>" + products[0].product_line +
                "</td><td class='title' title='" + title + "'>" +
                title + "</td><td class='quantity' title='" + quantity + "'>" + quantity + "</td><td class='total_quantity' title='" + total_quantity + "'>" + total_quantity +
                "</td><td class='locked_quantity' title='" + products[0].locked_quantity + "'>" +
                locked_quantity + "</td><td class='locked_total_quantity' title='" + total_locked_quantity + "'>" + total_locked_quantity +
                "</td><td class='sn' title='" + sn + "'>" + sn + "</td>"

            }
            html += "</tr>"

            if (!$.isEmptyObject(childrenDt)) {
              html += this.loadContent(childrenDt, this.titleDt, tr_class + "");
            }
          }
        }
        return html;
      },
      showOrHideChlidrenEle: function(evt) {
        var obj = $(evt.target);
        var class_ = obj.attr('class');
        var parent_tr_obj = obj.parents().closest('tr').eq(1);
        var parent_tr_class_arr = parent_tr_obj.attr('class').split(' ');
        var parent_tr_class = parent_tr_class_arr[parent_tr_class_arr.length - 1];
        $(evt.target).toggleClass('tree-collapsed').toggleClass('tree-expanded');
        obj.next().toggleClass('close_img').toggleClass('open_img');
        if (class_.indexOf('tree-collapsed') > 0) {

          var objs = parent_tr_obj.nextAll();
          for (var i = 0; i < objs.length; i++) {
            var obj_class = $(objs[i]).attr('class');
            if (obj_class.indexOf(parent_tr_class) > -1) {
              var tree_opEle = $(objs[i]).children().eq(0).find('.tree_op');
              if (tree_opEle.attr('class').trim().split(' ').length > 1) {
                tree_opEle.removeClass('tree-collapsed').removeClass('tree-expanded');
                tree_opEle.addClass('tree-expanded');
              }
              $(objs[i]).show();
            } else {
              return;
            }
          }
        } else if (class_.indexOf('tree-expanded') > 0) {
          var objs = parent_tr_obj.nextAll();
          for (var i = 0; i < objs.length; i++) {
            var obj_class = $(objs[i]).attr('class');
            if (obj_class.indexOf(parent_tr_class) > -1) {
              $(objs[i]).hide();
            } else {
              return;
            }
          }

        }

      },
      closeInventoryList: function() {
        closeInventoryWind();
      },
      queryInventory: function(evt) {
        var models = this.collection.toJSON();
        var targetParentObj = $(evt.target).parent().parent();
        if (!$.isEmptyObject(models)) {
          var data = {
            'ps_id': parseInt(targetParentObj.find('#ps_id').eq(0).val()),
            'slc_id': parseInt(targetParentObj.find('#slc_id').eq(0).val()),
            'slc_type': parseInt(targetParentObj.find('#slc_type').eq(0).val())
          };

          var container = targetParentObj.find("#container").val();
          var titles = targetParentObj.find("#title").val();
          titles == "" ? "" : data['titles'] = titles;
          var arrayDt = this.collection.toJSON();
          var titleDt = [];
          for (var i = 0; i < arrayDt.length; i++) {
            var dt = arrayDt[i];
            for (var key in dt) {
              if (key == 'titleDt') {
                titleDt = dt['titleDt'];
              }
            }
          }
          var title_ids = byCNTTitleNameSwitchCNTTitleId(titleDt, titles);
          title_ids == "" ? '' : data['title_ids'] = title_ids;
          var p_line = targetParentObj.find("#p_line").val();
          p_line == '' ? '' : data['product_line'] = p_line;
          var p_name = targetParentObj.find("#p_name").val();
          p_name == '' ? '' : data['p_names'] = p_name;
          var lot_number = targetParentObj.find("#lot_number").val();
          lot_number == '' ? '' : data['lot_numbers'] = lot_number;
          var container_type = targetParentObj.find("#container_type").val();
          if (!$.isEmptyObject(container_type + '')) {
            data['container_type'] = container_type;
          }
          this.render(data);
        }
      },
      associatingInputingTitle_: function(evt) {
        var arrayDt = this.collection.toJSON();
        var titleDt = [];
        for (var i = 0; i < arrayDt.length; i++) {
          var dt = arrayDt[i];
          for (var key in dt) {
            if (key == 'titleDt') {
              titleDt = dt['titleDt'];
            }
          }
        }
        if (!$.isEmptyObject(titleDt)) {
          associatingInputing(evt, titleDt);
        }
      },
      showTitle_: function(evt) {
        var titleDt = this.getCollectionArr('titleDt');
        var targetObj = $(evt.target);
        targetObj.data('titlesIndex', -1);
        targetObj.data('oldTitles', targetObj.val());
        if (!$.isEmptyObject(titleDt)) {
          associatingInputing(evt, titleDt);
        }

      },
      associatingInputingPLine_: function(evt) {
        var pLineDt = this.getCollectionArr('pLineDt');
        if (!$.isEmptyObject(pLineDt)) {
          associatingInputing(evt, pLineDt, 'singleVal');
        }
      },
      showPLine_: function(evt) {
        var v_ = this;
        var plineDt = {};
        var ps_id = $('#inventoryQueryForm #ps_id').val();
        $.ajax({
          url: config.queryProductLine.url,
          data: JSON.stringify({
            'ps_id': ps_id
          }),
          type: 'post',
          dataType: 'json',
          async: false,
          contentType: 'application/json; charset=UTF-8',
          success: function(data) {
            if (!$.isEmptyObject(data)) {
              v_.collection.add({
                'pLineDt': data
              });
              var targetObj = $(evt.target);
              targetObj.data('titlesIndex', -1);
              targetObj.data('oldTitles', targetObj.val());
              associatingInputing(evt, data, 'singleVal');
            }
          }
        })
      },
      associatingInputingLotNum_: function(evt) {
        var lotNumbersDt = this.getCollectionArr('lotNumbersDt');
        if (!$.isEmptyObject(lotNumbersDt)) {
          associatingInputing(evt, lotNumbersDt);
        }
      },
      showLotNum_: function(evt) {
        var v_ = this;
        var plineDt = {};
        var ps_id = $('#inventoryQueryForm #ps_id').val();
        $.ajax({
          url: config.querylotNumbers.url,
          data: JSON.stringify({
            'ps_id': ps_id
          }),
          type: 'post',
          dataType: 'json',
          async: false,
          contentType: 'application/json; charset=UTF-8',
          success: function(data) {
            if (!$.isEmptyObject(data)) {
              v_.collection.add({
                'lotNumbersDt': data
              });
              var targetObj = $(evt.target);
              targetObj.data('titlesIndex', -1);
              targetObj.data('oldTitles', targetObj.val());
              associatingInputing(evt, data);
            }
          }
        })
      },
      getCollectionArr: function(key) {
        var arrayDt = this.collection.toJSON();
        var arrDt = [];
        for (var i = 0; i < arrayDt.length; i++) {
          var dt = arrayDt[i];
          for (var key_ in dt) {
            if (key_ == key) {
              arrDt = dt[key];
            }
          }
        }
        return arrDt;
      },
      containerTypeKeyToLowerCase: function() {
        var containerTypeKey_ = {};
        for (var key in containerTypeKey) {
          var dt = containerTypeKey[key];
          var dt_ = {};
          for (var key_ in dt) {
            dt_[key_.toLowerCase()] = dt[key_];
          }
          containerTypeKey_[key] = dt_;
        }
        containerTypeKey = containerTypeKey_;
      },
      getTitle : function(title_id){
    	  var title = "NA";
          for (var j = 0; j < this.titleDt.length; j++) {
            if (this.titleDt[j].obj_id == title_id) {
              title = this.titleDt[j].obj_name;
              break;
            }
          }
          return title;
      },
      getCtnType : function(ctn_type_id){
    	  var language = browserLanguage();
          var ctn_type = $.isEmptyObject(containerTypeKey[ctn_type_id][language]) ? 'NA' : containerTypeKey[ctn_type_id][language];
          return ctn_type;
      },
      build_3d : function(store_data){
    	  	resize3dStore();
    		$("#store_3d").empty();
    		$("#store_3d").show();
    		this.dealStoreData(store_data);
    		var store = new _3d({
    			el_id : "store_3d",
    			store_info : store_data
    		});
    		$("#store_3d").data("_3d", store);
    		return store;
    	},
    	//包装container数据
    	dealStoreData : function(location_tree){
    		var _this=this;
    		var cons = location_tree.containers;
    		var level = 0;
	        if(cons){
	        	deal(cons);
	        }
	        function deal(cs){
	            for(var i=0; i<cs.length; i++){
	            	if(cs[i].children){
	            		level++;
	            		deal(cs[i].children);
	            	}
		        	cs[i].ctn_type = _this.getCtnType(cs[i].container_type);
		            cs[i].title = _this.getTitle(cs[i].products[0].title_id);
		            cs[i].html = _this.creatContainerHtml(cs[i]);
		            cs[i].level = cs[i].children ? --level : level;
	            }
	         }
    	},
    	//拼container
    	creatContainerHtml : function(con){
    		var pro = con.products[0];
    		var html = "<div id='con_"+con.con_id+"' class='element'>" +
    					"	<div class='top_title'>"+
    					"		<div class='con_type'>"+ con.ctn_type +"</div>"+
    					"		<div class='_title'>"+con.title+"</div>"+
    					"	</div>"+
    					"	<h1>"+con.container+"</h1>" +
    					"	<div class='uls'>"+
    					"		<ul>" +
    					"			<li><span>PDT: "+pro.p_name+"</span><p>"+(pro.quantity - pro.locked_quantity)+"/"+pro.quantity+"</p></li>" +
    					"			<li><span>PDT Line: "+pro.product_line+"</span></li>" +
    					"			<li><span>Lot: "+con.lot_number+"</span></li>" +
    					"		</ul>" +
    					"	</div>" +
    					"</div>";
    		return html;
    	},
      show3DInventory:function(evt){
        $("#store_3d").show();
        $("#inventory_tab").hide();
        $("#clearSelect3DObj").show();
        $("#inventory_model_parent").unbind('mouseover mouseout');
        $("#inventory_model #inventory3D").css({'background': 'rgba(9, 139, 124, 0.8)'});
        $("#inventory_model #inventoryList").css({'background': '#C1CDC1'});
      },
      showInventoryList:function(evt){
        $("#inventory_tab").show();
        $("#store_3d").hide();
        $("#clearSelect3DObj").hide();
        $("#inventory_model #inventoryList").css({'background': 'rgba(9, 139, 124, 0.8)'});
        $("#inventory_model #inventory3D").css({'background': '#C1CDC1'});
        $("#inventory_model_parent").mouseover(function(){
          $("#inventory_model").show();
        });
        $("#inventory_model_parent").mouseout(function(){
          $("#inventory_model").hide();
        });
      },
      clearSelect3DObj:function(evt){
        this._3d_store.unselect();
      }
    });
    return new locInventory();

  });
