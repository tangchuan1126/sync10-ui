define([
	"jquery",
	"backbone",
	"handlebars",
	"templates",
	"../models/gis_collection",
	"./rightClickMenusView",
	"./truckView",
	"./storageSecondTabsView",
	"./truckMarkerRightMenuView",
	"../js/gis_main", "mCustomScrollbar"
], function($,
	Backbone,
	Handlebars,
	templates, gis_Collection, rightClickMenusView, truckView,
	storageSecondTabsView, truckMarkerRightMenuView) {
	var firstMenuView = Backbone.View.extend({
		el: "#top_menus",
		template: templates.classify,
		collection: new gis_Collection(),
		initialize: function() {

		},
		render: function() {
			var v_ = this;
			//gis左侧导航菜单滚动条设置
			var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
			$("#gis_left_auto_overflow").height(height).mCustomScrollbar();
			var parentwidth = $(".gis_left").width();
			var n = parseInt(parentwidth * 0.96 / 85);
			var dt = [{
				"name": "Storage",
				"id": "storage"
			}, {
				"name": "Truck",
				"id": "truck"
			}, {
				"name": "Inventory",
				"id": "inventory"
			}, {
				"name": "Demand",
				"id": "demand"
			}, {
				"name": "Add",
				"id": "add storage"
			}, {
				"name": "GeoFence",
				"id": "geofence"
			}, {
				"name": "Route",
				"id": "route"
			}, {
				"name": "Point",
				"id": "point"
			}, ];
			var data = pageCtrlforJson(dt, n * 2);
			v_.collection.reset(data);
			v_.$el.html(v_.template({
				classify: data[0],
				R_pageplus: true,
				L_pageplus: false,
				num: 0
			}));
			rightClickMenusView.render({
				el: '#menu',
				template: templates.rightclick_menus
			});

		},

		events: {
			"click #boxConR": "changePage",
			"click #boxConL": "changePage",
			"click div.category_container .buttons": "loadSecondMenus"
		},
		changePage: function(evt) {
			this.$el.html('');
			var pageplus_;
			var data = this.collection.toJSON();
			var num = $(evt.target).data("num");
			$(".location_img").unbind('click');
			$(".location_img").css('cursor', 'not-allowed');
			if ($(evt.target).data('pageplus') == 1) {
				if (num + 2 == data.length) {
					this.$el.html(this.template({
						classify: data[num + 1],
						R_pageplus: false,
						L_pageplus: true,
						num: num + 1
					}));
				} else {
					this.$el.html(this.template({
						classify: data[num + 1],
						R_pageplus: true,
						L_pageplus: true,
						num: num + 1
					}));
				};
			} else {
				if (num - 1 == 0) {
					this.$el.html(this.template({
						classify: data[num - 1],
						R_pageplus: true,
						L_pageplus: false,
						num: num - 1
					}));
				} else {
					this.$el.html(this.template({
						classify: data[num - 1],
						R_pageplus: true,
						L_pageplus: true,
						num: num - 1
					}));
				};
			}
			if (menusSelectedFlag['firstMenu']) {
				changeSelectMenuBgCss($('.' + menusSelectedFlag['firstMenu']));
			}
		},
		loadSecondMenus: function(evt) {
			showOrHideAddLayer(false);//隐藏右键添加layer菜单
			var btnobj = $(evt.target);
			var title = btnobj.data('name');
			$(".childNode").html('');
			var botton_all = $(".category_container");
			botton_all.css("background", "initial");
			
			setMapTool('move_map');
			//jsmap.clearStorageCatalog();//隐藏已有库存的storage，location
			jsmap.clreaPolygon();
			jsmap.clreaLabelMarker();
			closeInventoryWind();

			var p_buttonbox = changeSelectMenuBgCss(btnobj);

			var nodeClassArray = p_buttonbox.find('.category_icon_base').attr('class').split(' ');
			menusSelectedFlag['firstMenu'] = nodeClassArray[nodeClassArray.length - 1];
			/*$("#middle_menus").html('');
				   	$("#bottom_menus").html('');*/
			menusSelectedFlag['seconeMenu'] = undefined;
			viewData["storageThirdlyMenusTabsView"] = undefined;
			if (title == "Storage") {
				$(".childNode").html('<div id="storage_second"></div>');
				storageSecondTabsView.render();
				//storageSecondMenusView.render(title);
			} else if (title == "Truck") {
				clearAllLayer();
				$(".childNode").html('<div id="assetTree"></div><div id="bottom_menus" style="width: 100%;background-color: white;max-height:none;"></div>');
				truckView.render();
				//loading truckMarkerMenu
				truckMarkerRightMenuView.render();
			} else if (title == 'Demand') {
				clearAllLayer();
				require(['views/demandMenusView'], function(demandMenusView) {
					demandMenusView.render('.childNode');
				})
			} else if (title == 'Add') {
				clearAllLayer();
				require(['views/StorageSkeletonInfo'],
					function(StorageSkeletonInfo) {
						StorageSkeletonInfo.render('.childNode');
					})
			} else if (title == 'Inventory') {
				require(['views/inventoryView'], function(inventoryView) {
					inventoryView.render();
				})
			} else if (title == 'GeoFence') {
				clearAllLayer();
				var data = {
					type: 'geoFencing'
				};
				loadGeoAlarmTableView(data, 'geoFencing');
			} else if (title == 'Route') {
				var data = {
					type: 'geoLine'
				};
				loadGeoAlarmTableView(data, 'geoLine');
			} else if (title == 'Point') {
				clearAllLayer();
				var data = {
					type: 'geoPoint'
				};
				loadGeoAlarmTableView(data, 'geoPoint');
			}


		}
	});

	return new firstMenuView();
});