define(["jquery","backbone","handlebars","templates",
	"../models/gis_collection","../js/GoogleMapsV3",
    "../js/jsmap","../js/gis_main","jqueryui/datepicker",
    "require_css!../css/datepicker"]
	,function($,backbone,handlebar,templates,gis_collection){
	var demandMenusView=backbone.View.extend({
			//el:"#tab2",
			collection:new gis_collection,
			template:templates.demand,
			render:function(el){
				this.setElement(el);
			//	console.log(this.template());
				var demand_dt=[
					{title_id:23,title_name:'ANTINC'},
					{title_id:11,title_name:'ANTINC0001'},
					{title_id:8,title_name:'EBAY'},
					{title_id:24,title_name:'ELUINT'},
					{title_id:14,title_name:'ELUINT001'},
					{title_id:12,title_name:'EPI'},
					{title_id:2,title_name:'HONHAI'},
					{title_id:159,title_name:'LENOVO'},
					{title_id:13,title_name:'LTL OUTBOUND'},
					{title_id:22,title_name:'LUCKY SEVEN'},
					{title_id:3,title_name:'ONKYO'},
					{title_id:9,title_name:'QUANTA'},
					{title_id:18,title_name:'SCT'},
					{title_id:25,title_name:'SEIDIG'},
					{title_id:20,title_name:'SEIDIG0005'},
					{title_id:17,title_name:'SEIDIG0006'},
					{title_id:21,title_name:'SEIDIG0007'},
					{title_id:10,title_name:'TCL'},
					{title_id:26,title_name:'TINLLC'},
					{title_id:27,title_name:'TINLLC0001'},
					{title_id:4,title_name:'TPV'},
					{title_id:5,title_name:'VIZIO'},
					{title_id:19,title_name:'VVME'},
					{title_id:15,title_name:'VZB'},
					{title_id:16,title_name:'WESTINGHOUSE'},
					{title_id:6,title_name:'WISTRON'},
					{title_id:7,title_name:'ZYLUX'}
				]
				this.collection=demand_dt;
				this.$el.html(this.template());
				$("#start_time").datetimepicker({
					format: "mm/dd/yyyy",
			        autoclose:1,
			        minView: 2,
			        forceParse: 0
				}).on('changeDate',function(ev){
					$("#end_time").val();
					$('#end_time').datetimepicker('setStartDate', ev.date);
				});
				$("#end_time").datetimepicker({
					format: 'mm/dd/yyyy',
					autoclose: 1,
		            minView: 2,
		            forceParse: 0
				});
				setTimeout(this.titleClickEvent(),500);
				$(".demand_title").hide();
				this.loadDemandTitle(demand_dt);
				
			},
			events:{
				'keyup #pro_title':'seachDemandTitle'
			},
			seachDemandTitle:function(evt){
				var title_v=$(evt.target).val().toUpperCase();
				var data=this.collection;
				var re = new RegExp(title_v,"i");
				var dt=new Array();
				for(var i=0;i<data.length;i++){
					if(data[i].title_name.match(re)){
						dt.push(data[i]);
					}
				}
				this.loadDemandTitle(dt);
			},
			loadDemandTitle:function(demand_dt){
				require(['views/demandTitleView','templates'],function(demandTitleView,templates){
						demandTitleView.render(demand_dt,templates.demandTitlePanle,'.demand_title');
					})
				
				
			},
			titleClickEvent:function(){
				$("#productDemandFrom #pro_title").bind('click',function(){
					$(".demand_title").show();
				})
			}
			
		});
	return new demandMenusView();
		
})