define(["jquery", "backbone", "handlebars", "templates",
        "art_Dialog/dialog-plus", "../models/gis_collection", '../js/GoogleMapsV3',
        '../js/jsmap', "../js/gis_main", "mCustomScrollbar", "blockui"
    ],
    function($, backbone, handlebars, templates, dialog, gis_collection) {
        var modify_storage_layer = backbone.View.extend({
            template: templates.modify_storage_layer,
            render: function(data) {
                var v_ = this;
                verifyMgs={};
                focusNum = 0;
                var d = dialog({
                    id: "modify_storage_layer",
                    title: 'Add New Layer',
                    content: v_.template({
                        ps_id: data.ps_id,
                        storageName: data.storageName
                    }),
                    button: [{
                        value: 'Save',
                        callback: function() {
                            var this_ = this;

                            var ps_id = data.ps_id;
                            alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", true, 'Save');
                            var layerTabVal = $(".modify_storage_layer_div input[name='options']:checked").val();
                            if (layerTabVal == 0) {
                                var type = $("#modify_storage_layer input[name='type']:checked").val();
                                v_.ajaxVerifyNameAndSubmit(this_, type, ps_id, commitAddLayer);
                                //commitAddLayer(this);
                            } else {
                                verifyMgs['verifyName']=1;
                                addMutliLayer(this, "modify_storage_layer");
                            }

                            return false;
                        },
                        autofocus: true
                    }, {
                        value: 'Cancel'
                    }]
                }).show();
                convertCoordinateAjax(data.ps_id, data.lat, data.lng, 'modify_storage_layer');
                var dt = v_.belongArea(data.lat, data.lng);
                $.extend(data, dt);
                $("#modify_storage_layer #zone").val(data.area_name);
                $("#modify_storage_layer #area_id").val(data.area_id);
                var addSingleLayeFormHtml = $("#modify_storage_layer").html();
                $(".modify_storage_layer_div .btn-group .btn").bind('click', function(evt) {
                    clearAutoLayer();
                    v_.getValue(data);
                    var childrenEle = $(evt.target).children().eq(0);
                    $(".modify_storage_layer_div input[name='options']").attr("checked", false);
                    childrenEle.attr("checked", true);
                    if (childrenEle.val() == 0) {
                        $("#modify_storage_layer").html(addSingleLayeFormHtml);
                        v_.fillValue(data);
                    } else {
                        var isExistArea = false;
                        if (!$.isEmptyObject(dt)) {
                            isExistArea = true;
                        }
                        $("#modify_storage_layer").html(templates.addMultiLayer({
                            ps_id: data.ps_id,
                            storageName: data.storageName,
                            isExistArea: isExistArea
                        }));
                        v_.fillValue(data);


                    }

                });
                d.addEventListener('close', function() {
                    clearstorageDemoLayer();
                    clearAutoLayer();
                    jsmap.getMap().keyboardShortcuts=true;//运行map上使用快捷键
                    var addLayerTab = $(".modify_storage_layer_div input[name='options']:checked").val();
                    if (addLayerTab == 1) {
                        clearAutoLayer();
                    }
                    if (jsmap.storageDemoLayer["drag_line"]) {
                        jsmap.storageDemoLayer["drag_line"].setMap(null);
                        delete jsmap.storageDemoLayer["drag_line"];
                    }

                    if (this.returnValue) {
                        var type = this.returnValue.type;
                        var id = this.returnValue.position_id;
                        var is_three_dimensional=this.returnValue['is_three_dimensional'];
                        this.remove();
                        var ps_id = data.ps_id;
                        var flag = 'add';
                        if (type == 1) {
                            delete jsmap.storageLocation[ps_id];
                            //var locationCheck = $("#location").prop("checked");
                            var locCheck_2d=$("#location_2d").prop("checked");
                            var kmlName=$("#location_2d").data("kml");
                            var locCheck_3d=$("#location_3d").prop("checked");
                            if (locCheck_2d || locCheck_3d) {
                       
                                if(is_three_dimensional==0){//2d
                                    var url=getKmlUrl("2d_locs_" + kmlName);
                                    jsmap.flag["drawLocation"] = true;
                                    jsmap.flag["draw2DLocation"] = true;
                                    jsmap.loadKml(ps_id+"_2d_locs", url);
                                }else if(is_three_dimensional==1){//3d
                                    var url=getKmlUrl("3d_locs_" + kmlName);
                                    jsmap.flag["drawLocation"] = true;
                                    jsmap.flag["draw3DLocation"] = true;
                                    jsmap.loadKml(ps_id+"_3d_locs", url);
                                }
                                 
                            }
                        } else if (type == 2) {
                            reLoadStaging(ps_id, flag, id);
                            var stagingCheck = $("#staging").prop("checked");
                            if (stagingCheck) {
                                drawStagingByPsId(ps_id, false);
                            }
                        } else if (type == 3) {
                            reLoadDocks(ps_id, flag, id);
                            var dockCheck = $("#docks").prop("checked");
                            if (dockCheck) {
                                jsmap.flag["drawDocks"] = true;
                                drawDockYardByPsId(ps_id, false);
                                startDocksParkingRefresh();
                            }
                        } else if (type == 4) {
                            delete jsmap.storageDocksParking[ps_id];
                            reLoadParking(ps_id, flag, id);
                            var parkingCheck = $("#parking").prop("checked");
                            if (parkingCheck) {
                                jsmap.flag["drawParking"] = true;
                                drawDockYardByPsId(ps_id, false);
                                startDocksParkingRefresh();
                            }
                        } else {
                            reLoadArea(ps_id, flag, id);
                            var areaCheck = $("#area").prop("checked");
                            if (areaCheck) {
                                drawAreaByPsId(ps_id, false);
                            }
                            delete jsmap.storageTitle[ps_id];
                            delete jsmap.storageResource[ps_id];
                        }

                    }
                    
                    //this.remove();
                    verifyMgs = {};
                });
                $.unblockUI();
                v_.createScroll();
            },
            fillValue: function(data) {
                $("#modify_storage_layer #x").val(data.x);
                $("#modify_storage_layer #y").val(data.y);
                $("#modify_storage_layer #height").val(data.height);
                $("#modify_storage_layer #width").val(data.width);
                $("#modify_storage_layer #angle").val(data.angle);
                $("#modify_storage_layer #zone").val(data.area_name);
                $("#modify_storage_layer #area_id").val(data.area_id);
            },
            belongArea: function(lat, Lng) {
                var latLng = jsNewLatLng(lat, Lng);
                var data = {};
                for (var key in jsmap.storageAreaPolygon) {
                    var area = jsmap.storageAreaPolygon[key];
                    if (area && containsLocation(latLng, area)) {
                        data['area_id'] = area.data.area_id;
                        data['area_name'] = area.data.name;
                        break;
                    }
                }
                return data;
            },
            getValue: function(data) {
                if (!$.isEmptyObject($("#modify_storage_layer #x").val())) {
                    data["x"] = $("#modify_storage_layer #x").val();
                }
                if (!$.isEmptyObject($("#modify_storage_layer #y").val())) {
                    data["y"] = $("#modify_storage_layer #y").val();
                }
                if (!$.isEmptyObject($("#modify_storage_layer #height").val())) {
                    data["height"] = $("#modify_storage_layer #height").val();
                }
                if (!$.isEmptyObject($("#modify_storage_layer #width").val())) {
                    data["width"] = $("#modify_storage_layer #width").val();
                }
                if (!$.isEmptyObject($("#modify_storage_layer #angle").val())) {
                    data["angle"] = $("#modify_storage_layer #angle").val();
                }
                if (!$.isEmptyObject($("#modify_storage_layer #zone").val())) {
                    data["area_name"] = $("#modify_storage_layer #zone").val();
                }
                if (!$.isEmptyObject($("#modify_storage_layer #area_id").val())) {
                    data["area_id"] = $("#modify_storage_layer #area_id").val();
                }
                return data;
            },
            createScroll: function() {
                $(".modify_storage_layer_div").mCustomScrollbar();

            },
            ajaxVerifyNameAndSubmit: function(_this, type, psId, submitFn) {
                var obj = $("#modify_storage_layer #name");
                var name;
                if (obj.length)
                    name = $.trim(obj.val());
                var data = {
                    "ps_id": psId,
                    "name": name,
                    "type": type
                };

                var url = systenFolder + 'action/administrator/gis/verifyName.action';

                postVerify(data, url, obj, function(reval, OBJ) {

                    if (reval == "true") {

                        submitFn(_this);

                    } else {
                        alterDialogDisabled("div[aria-labelledby='title:modify_storage_layer'] button", false, 'Submit');
                        verifyMgs['verifyName'] = 0;
                        showErrorVerifyMsg(OBJ, 'Name can not repeat!');
                        return false;
                    }

                });

            }
        });
        return new modify_storage_layer;
    })