define(['jquery',
        "backbone",
        "handlebars",
        "templates",
        "../models/gis_collection",
        "../js/gis_main",
        '../js/GoogleMapsV3',
        '../js/jsmap'],
    function($,backbone,
    		handlebars,
    		templates,
    		gis_collection,
    		gis_main){
    	var storageThirdlyMenus = backbone.View.extend({
    		el:"#tab1",
    		template:templates.storageThirdlyMenus,
    		collection:new gis_collection,
    		initialize:function(options){
        	},
    		render:function(selectKmlOpFlag){
    			this.setElement("#tab1");
    			this.$el.html('');
    			var dt=     [{"name":"Area","layer":"area"},
    						 {"name":"Title","layer":"title"},
    						 {"name":"Zone-Resource","layer":"zonedock"},
    						 {"name":"Zone-Person","layer":"zonePerson"},
    						 //{"name":"Location","layer":"location"},
                             {"name":"2D-LOCS","layer":"location_2d"},
                             {"name":"3D-LOCS","layer":"location_3d"},
    						 {"name":"Staging","layer":"staging"},
    						 {"name":"Docks","layer":"docks"},
    						 {"name":"Parking","layer":"parking"},
    						 {"name":"Webcam","layer":"webcam"},
    						 {"name":"Printer","layer":"printer"},
                             {"name":"Inventory","layer":"inventory"},
    						 {"name":"Road","layer":"road"},
    						 {"name":"Light","layer":"light"}
    						];
    			var kml =viewData["storageThirdlyMenusTabsView"].kml;
    			var ps_id=viewData["storageThirdlyMenusTabsView"].ps_id;
				for(var key in dt){
					var dt_ =dt[key];
					dt[key]["kml"]=kml;
					dt[key]["ps_id"]=ps_id;
				}
				this.$el.html(this.template({menus_dt:dt,pageplus_L:false,pageplus_R:false,num:0}));
			
				$("#area").attr("checked",true);
                if(selectKmlOpFlag){
                    storageLayerOp['baseLayer'].call();
                    storageLayerOp['area'].call();
                    //selectKml('area',ps_id,"");
                }
                bindMenuLocImg(ps_id);
    		},
    		events:{
					   "click input.storage_layer_checkbox":"showLayer"
				   },
		
			showLayer:function(evt){
				var elId =$(evt.target).attr("id");
                storageLayerOp[elId].call();
				/*if($(evt.target).attr("checked")){
					$(evt.target).attr("checked",false);
				}else{
					$(evt.target).attr("checked",true);
				}
				selectKml(elId ,$(evt.target).data('ps_id'),$(evt.target).data('kml'));*/
			}
    	});
    	return new  storageThirdlyMenus();
})