define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"
	],
	function($, backbone, handlebars, templates, gis_collection, dialog,config){
			var modifyPrinterView= backbone.View.extend({
				
			template:templates.modify_printer,
			initialize:function(options){
			},
			render:function(data,formId){
				var v_=this;
				verifyMgs={};
				focusNum=0;
				verifyMgs={};
				var d=dialog.get('modify_printer');
				if(d){
					var key =data.ps_id+"_"+$('#printerInfo input:hidden[name="p_id"]').val();
					var before_modify_printer_data=jsmap.storagePrinterMarker[key];
					var dbdata=before_modify_printer_data.data.dbdata;
					reLoadPrinter(dbdata.ps_id,'cancel',dbdata.p_id);
					d.content(v_.template({data:data,formId:formId}));
				}else{
					d=dialog({
						title:"Set Printer ["+data.name+"]",
						content:v_.template({data:data,formId:formId}),
						id:'modify_printer',
						button:[
							{
								value:'Delete',
								callback:function(){
									alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button",true,'Delete');
									printer_doDelete(data,this);
									return false;
								}
							},
							{
								value:'Submit',
								callback:function(){
									
									alterDialogDisabled("div[aria-labelledby='title:modify_printer'] button",true,'Submit');
									printer_commitData(data,this);
									return false;
								},
								autofocus: true
							},
							{
								value:'Cancel'
							}
						]
					}).show();
				}
				
				this.initPorperty_dt(data);
				d.addEventListener('close', function () {
					var flag='';
				    if(!this.returnValue){
				    	flag='cancel';
				    }else{
				    	flag=this.returnValue;
				    }
				    reLoadPrinter(data.ps_id,flag,data.p_id);
				    focusNum=0;
				    this.remove();
				});
				
			},
			initPorperty_dt:function(data){
				$("#printerInfo select[name='type']").val(data.type);
				setOption();
				$("#printerInfo select[name='size']").val(data.size);
				if(data.servers){
					$("#printerInfo #servers_id").val(data.servers);
					$("#printerInfo #servers_name").val(data.servers_name);
				}
				if(data.p_type=="0"){
					$("#printerInfo input:radio[value='0']").attr('checked','true');
					$("#printerInfo #_servers").hide();
					$("#printerInfo #_ip").show();
					$("#printerInfo #_port").show();
				}else{
					$("#printerInfo input:radio[value='1']").attr('checked','true');
					$("#printerInfo #_servers").show();
					$("#printerInfo #_ip").hide();
					$("#printerInfo #_port").hide();
				}
			}
		});
	return new modifyPrinterView();
})