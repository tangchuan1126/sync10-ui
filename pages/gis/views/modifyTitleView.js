define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"
	],
	function($, backbone, handlebars, templates, gis_collection, dialog,config) {
		var modifyTitle= backbone.View.extend({
			collection: new gis_collection,
			  template:templates.select_title,
			render: function(psId,area_id,titles) {
				var v_ = this;
				v_.collection.fetch({
					url: config.queryTitle.url,
					success: function() {
						var title_data = v_.collection.toJSON();
						if(titles){
							if(typeof titles!=='string'){
								var title_char=JSON.stringify(title_data);
								for(var key in titles){
									var startIndex=title_char.indexOf('{"obj_name":"'+titles[key].obj_name+'"');
									if(startIndex>-1){
										var char1=title_char.substring(0,startIndex);
										var lenght =title_char.substring(startIndex).indexOf('}')+2;
										var endIndex=parseInt(startIndex)+parseInt(lenght);
										var char2=title_char.substring(endIndex);
										title_char=char1+char2;
									}
									
								}
								title_data=JSON.parse(title_char);
								if(JSON.stringify(titles).indexOf('obj_name')==-1){
									titles=[];
								}
							}else{
								var exitDt=new Array();
								var titles_=titles.split(',');
								var titlesDt=new Array();
								for(var i=0;i<title_data.length;i++){
									if($.inArray(title_data[i].obj_name,titles_)>-1){
										exitDt.push(title_data[i]);
										
									}else{
										titlesDt.push(title_data[i]);
									}
								}
								titles=exitDt;
								title_data=titlesDt;
							}
							
						}
						
						var title='';
						if(jsmap.storageTitle[psId] && jsmap.storageTitle[psId][area_id]){
							title="Select title["+jsmap.storageTitle[psId][area_id][0].area_name+"]";
						}else{
							title="Select title";
						}
						var d = dialog({
							title: title,
							content: v_.template({
								exit_title_data: titles,
								notExit_title:title_data
							}),
							backdropOpacity: 0.2 // 透明度
							 
						}).showModal();
						if(jsmap.storageTitle[psId] && jsmap.storageTitle[psId][area_id]){
							d.button([{
								value: 'Confirm',
								callback: function() {
									modifyTitleAjax(psId,area_id,this);
								},
								autofocus: true
							}, {
								value: 'Cancel'
							}]);
						}else{//添加layer时，选择title
							d.button([{
								value: 'Confirm',
								callback: function() {
									backFillTitle(this);
								},
								autofocus: true
							}, {
								value: 'Cancel'
							}]);
						}
						d.addEventListener('close',function(){
							focusNum=0;
						})
						
					}
				})
			}
		});
	return new modifyTitle;
	})