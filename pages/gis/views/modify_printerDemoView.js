define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"],
		function($, backbone, handlebars, templates, gis_collection, dialog,config){
			var modify_printerDemo=backbone.View.extend({
				template:templates.modify_printer,
				render:function(data,formId){
					var v_=this;
					verifyMgs={};
					focusNum=0;
					var d=dialog({
						title:'Add New Printer',
						content:v_.template({data:data,formId:formId}),
						id:'printerDemo',
						button:[
							{
								value:'Submit',
								callback:function(){
									
									alterDialogDisabled("div[aria-labelledby='title:printerDemo'] button",true,'Submit');
									printer_updateData(data,this,"addprinter");
									return false;
								},
								autofocus: true
							},
							{
								value:'Cancel'
							}
						]
					}).show();
					$.unblockUI();
					this.initProperty_dt(data,formId);
					d.addEventListener('close', function () {
					    if(this.returnValue=='add'){
					    	reLoadPrinter(data.ps_id,this.returnValue);
					    }
				    	this.remove();//
				    	verifyMgs={};
				    	jsmap.clearSinglePrinter(data.ps_id,"demo");
				    	jsmap.setOptions({draggable:true,scrollwheel:true});//地图的draggable 和scrollwheel 状态还原
					});
				},
				initProperty_dt:function(data,formId){
					$("#"+formId+" #x").attr("readonly","readonly");
					$("#"+formId+" #y").attr("readonly","readonly");
					$("#addprinter select[name='type']").val(data.type);
					setOption();
					$("#addprinter select[name='size']").val(data.size);
					if(data.servers){
						$("#addprinter #servers_id").val(data.servers);
						$("#addprinter #servers_name").val(data.servers_name);
					}
					if(data.p_type=="0"){
						$("#addprinter  input:radio[value='0']").attr('checked','true');
						$("#addprinter #_servers").hide();
						$("#addprinter #_ip").show();
						$("#addprinter #_port").show();
					}else{
						$("#addprinter  input:radio[value='1']").attr('checked','true');
						$("#addprinter #_servers").show();
						$("#addprinter #_ip").hide();
						$("#addprinter #_port").hide();
					}
				}

			})
			return new modify_printerDemo;

})