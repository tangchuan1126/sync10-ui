define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",'../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
    function($,backbone,handlebars,templates,gis_collection,dialog){
    	var zoneResourceView= backbone.View.extend({
    		collection:new gis_collection,
    		template:templates.showZoneDocks,
    		render:function(title,data,url){
    			var v_=this;
    			v_.collection.fetch({
    				url:url,
    				data:data,
    				success:function(){
    					var array_data=v_.collection.toJSON();
			    			var olddock_Ids,dock_names;
			    			for(var i=0;i<array_data.length;i++){
			    				if(olddock_Ids){
			    					olddock_Ids+=","+array_data[i].sd_id;
			    				}else{
			    					olddock_Ids=array_data[i].sd_id;
			    				}
			    				if(dock_names){
			    					dock_names+=","+array_data[i].doorid;
			    				}else{
									dock_names=array_data[i].doorid;
			    				}
			    			}
			    			var d  ;
			    			
			    			if(dialog.get('showZoneDock')){
			    				d=dialog.get('showZoneDock').title(title)
			    				.content(v_.template({docks_dt:array_data,name:title,olddock_Ids:olddock_Ids,dock_names:dock_names,ps_id:data.ps_id,area_id:data.area_id}));
			    			}else{
			    			d = dialog({
								    title: title,
								    esc: true,
								    backdropOpacity:0.2,
								    id:"showZoneDock",
								    content: v_.template({docks_dt:array_data,name:title,olddock_Ids:olddock_Ids,dock_names:dock_names,ps_id:data.ps_id,area_id:data.area_id}),
								    button:[
											{
									            value: 'Submit',
									            callback: function () {
									            	alterDialogDisabled("div[aria-labelledby='title:showZoneDock'] button",true,'Submit');
									            	showZoneDocks_commitData(olddock_Ids,data.ps_id,data.area_id);
									            },
									            autofocus: true
									        },
									        {
									            value: 'Cancel'
									        }
									       
								    ],
								    onclose: function () {
								    
								    }
								});
			    			}
							d.showModal();
    				}
    			})
    		}
    	});
		return new zoneResourceView;
})