define('jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"
	],
	function($,backbone, handlebars, templates, gis_collection, dialog,config){
		var modify_lightView= backbone.View.extend({
			sender:function(data,template){
				this.template=template;
				verifyMgs={};
				var v_=this;
				var d=dialog({
					title:'',
					content:v_.template({data:data}),
					button:[
						{
							value:'Submit',
							callback:function(){

							},
							autofocus: true
						},
						{
							value:'Cancel'
						}
					]
				}).show();
			}
		});
		return new modify_lightView;
	}
)