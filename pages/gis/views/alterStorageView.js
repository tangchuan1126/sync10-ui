define(['jquery',
        'backbone',
        'templates',
        '../models/gis_collection',
        'config',
        "art_Dialog/dialog-plus"],
        function($,backbone,templates,gis_collection,config,dialog){
        	var alterStorage=backbone.View.extend({
        		collection:new gis_collection,
        		template:templates.alterStorage,
        		render:function(){
        			var v_=this;
        			v_.$el.html('');
        			v_.setElement('#alterStorage');

        			v_.collection.fetch({
        				url:config.queryOwnStorage.url,
        				success:function(){
        					var req_dt=v_.collection.toJSON()[0];
        					if(req_dt.flag=='true'){
        						var data=req_dt.datas;
	        					v_.$el.html(v_.template({storageData:data}));
	        					$("#storageAlter_btn").hide();
        					}else if('authError'==req_dt.flag){
        						 showMessage("Auth error!", "error");
        					}
        					
        				}
        			});

        		},
        		events:{
					  'change #select_storage':'selectStorage',
					  'click #storageAlter_btn_edit':'edit',
					  'click #storageAlter_btn_save':'save',
					  'click #storageAlter_btn_cancel':'cancel',
					  'click #storageAlter_btn_delete':'delete'
					   
			   },
			   edit:function(evt){
			   		$(evt.target).hide();
			   		$("#storageAlter_btn_save").show();
			   		$("#storageAlter_btn_delete").hide();
			   		$("#storageAlter_btn_cancel").show();
			   		var positions=jsmap.mapToolPositionList;
			   		var latLngCode=['A','B','C','D','E','F','G','H','I','J','K',
		'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
			   		for(var i=0;i<positions.length;i++){
			   			jsmap.drawLabelMarker(positions[i],latLngCode[i],jsmap.getMap());
			   		}
			   		jsmap.drawPolygon(positions);
			   		var ps_id=$("#select_storage").val();
			   		var polyline=jsmap.storageBasePolyline[ps_id];
			   		polyline.setMap(null);


			   },
			   save:function(){
			   		saveMaintainStorage();
			   	    
			   },
			   cancel:function(){
			   	    $("#storageAlter_btn_cancel").hide();
			   	    $("#storageAlter_btn_save").hide();
			   	    $("#storageAlter_btn_edit").show();
			   	    $("#storageAlter_btn_delete").show();
			   	    jsmap.clreaPolygon();
					jsmap.clreaLabelMarker();
					var ps_id=$("#select_storage").val();
					var polyline=jsmap.storageBasePolyline[ps_id];
			   		polyline.setMap(jsmap.getMap());

			   },
			   delete:function(){
			   	var v_=this;
			   	dialog({
					title:'Notify',
					content:'Delete '+$("#select_storage").find("option:selected").text(),
					cancel: false,
					backdropOpacity:0.2,
		   			button: [
				        {
				            value: 'YES',
				            callback: function () {
				            	var ps_id=$("#select_storage").val();
						   	    $.ajax({
						   	    	url:config.deleteStorageBase.url,
				    				data:{ps_id:parseFloat(ps_id)},
				    				type:'post',
				    				cache:false,
									//contentType: "application/json; charset=UTF-8",
				    				async:false,
				    				dataType:'json',
				    				success:function(data){
				    					if(data==true){
					    					var polyline=jsmap.storageBasePolyline[ps_id];
								   			polyline.setMap(null);
								   			delete jsmap.storageBasePolyline[ps_id];
								   			if(ps_id==viewData["storageThirdlyMenusTabsView"].ps_id){
								   				delete viewData["storageThirdlyMenusTabsView"];
								   			}
					    					v_.render();
				    					}
				    					
				    				}
						   	    })
				            }
				        },
				        {
				            value: 'NO',
				            autofocus: true
				        }
				    ]
				}).showModal();
			   	
			   },
        		selectStorage:function(evt){
        			$("#storageAlter_btn").hide();
        			var ps_id=$(evt.target).val();
        			jsmap.mapToolPositionList=[];
					$("#storageOutline").html('');
        			if(ps_id!=0){
						var dt=this.postStorageInfo(ps_id);
						var index_=0;
						var pointStr="",maxStr='';
						var firstPosition='',allPosistionStr='';
						for(var i=0;i<dt.length;i++){
							var dt_=dt[i];
							for(var key in dt_){
								if(key.indexOf('max')==-1 ){
									pointStr+='<div class="input-group" style="margin-bottom: 10px;"><span class="input-group-addon">Position_'+key+
		 										'</span><input name="latLng['+index_+']"+ type="text" class="form-control input-mStorage" id="title_'+key+'" value='+dt_[key]+'></div>';
		 										
		 							var latLng=jsNewLatLng(dt_[key].split(',')[0],dt_[key].split(',')[1]);
		 							jsmap.mapToolPositionList.push(latLng);
		 							if(index_==0){
		 								firstPosition=dt_[key].split(',')[1]+','+dt_[key].split(',')[0];
		 							}
		 							if(allPosistionStr){
		 								allPosistionStr+=' '+dt_[key].split(',')[1]+','+dt_[key].split(',')[0];
		 							}else{
		 								allPosistionStr=dt_[key].split(',')[1]+','+dt_[key].split(',')[0];
		 							}
		 							index_++;
								}else{
									maxStr+= '<div class="input-group" style="margin-bottom: 10px;"><span class="input-group-addon">'+key+'</span><input name="'+key+'" type="text" class="form-control input-mStorage" id="title" value="'+dt_[key]+'">'+
												 '<span class="input-group-addon">fx</span></div>'
								}
							}
						}
						allPosistionStr+=" "+firstPosition;
						if(!$.isEmptyObject(jsmap.storageBasePolyline[ps_id])){
							jsmap.storageBasePolyline[ps_id].setMap(null);
							delete jsmap.storageBasePolyline[ps_id];
						}
						drawStorageBase(ps_id,{latlng:allPosistionStr});
						this.hideChildrenLayer(ps_id);
						$("#storageOutline").html(pointStr+maxStr);
						$("#storageAlter_btn").show();
        			}
        		},
        		postStorageInfo:function(ps_id){
        			var dt;
        			$.ajax({
        				url:config.queryStorageBase.url,
        				data:{ps_id:ps_id},
        				type:'get',
        				cache:false,
						contentType: "application/json; charset=UTF-8",
        				async:false,
        				dataType:'json',
        				success:function(data){
        					dt=data
        				}
        			});
        			return dt;
        		},
        		hideChildrenLayer:function(psId,data){
        			jsmap.deleteAllArea(psId);
					jsmap.clearAllTitle(psId);
					jsmap.clearZoneDocks(psId);
					jsmap.clearZonePerson(psId);
					jsmap.unLoadKml(psId+"_2d_locs");
					jsmap.unLoadKml(psId+"_3d_locs");
					jsmap.clearAllLocation(psId);
					jsmap.clearAllStaging(psId);
					jsmap.clearAllDocks(psId);
					jsmap.clearAllParking(psId);
					jsmap.deleteAllWebcam(psId);
					jsmap.clearAllPrinter(psId);
					jsmap.clearRoadLayer(psId);
        		}
        	});
        	return new alterStorage();
})