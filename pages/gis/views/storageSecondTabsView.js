define(['jquery',
        'backbone',
        'templates',
        '../models/gis_collection',
        './storageSecondMenusView',
        'config',
        './alterStorageView',
		'jqueryui/tabs'],
	function($,backbone,
		templates,
		gis_collection,	
		storageSecondMenusView,
		config,
		alterStorageView
		){
		var storageSecondTabsView=backbone.View.extend({
			template:templates.storageSencondTabs,
			el:"#storage_second",
			render:function(){
				this.setElement("#storage_second");
				this.$el.html('');
				this.$el.html(this.template({}));
				$("#storage_tabs").tabs({active:0});
				storageSecondMenusView.render();
				$("#storage_tabs").tabs({
					activate: function( event, ui ) {
					  	if(ui.newPanel.selector=='#storage'){
					  		jsmap.clreaPolygon();
							jsmap.clreaLabelMarker();
							var dt= viewData["storageThirdlyMenusTabsView"];
							if(!$.isEmptyObject(dt)){
								var objEle= $(".category_icon_collection_third").children().eq(0);
								var ps_id=objEle.data('ps_id');
								var kmlName=objEle.data('kml');
								//selectKml('',ps_id,kmlName);
								bindMenuLocImg(ps_id)
							}else{
								$("#bottom_menus").html("");
							}
					  		storageSecondMenusView.render();
					  	}else if(ui.newPanel.selector=='#alterStorage'){
					  		alterStorageView.render();
					  		$("#bottom_menus").html();
					  		$(".location_img").unbind('click');
					  		showOrHideAddLayer(false);//隐藏右键添加layer菜单
					  		$(".location_img").css('cursor','not-allowed');
					  	}
					}
				});
				
			}
		});
		return new storageSecondTabsView();
	})