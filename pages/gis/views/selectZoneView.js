define(['jquery',"backbone","handlebars","templates",
	"../models/gis_collection","art_Dialog/dialog-plus",'../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
    function($,backbone,handlebars,templates,gis_collection,dialog){
		var selectZoneView= backbone.View.extend({
			collection:new gis_collection,
			render:function(data,url,template){
				this.template=template;
				var v_=this;
				v_.collection.fetch({
					data:data,
					url:url,
					success:function(){
						var area_name = $("#add_printer_server_zone").val();
						var area_id = $("#add_printer_server_area_id").val();
						var zone_data_=v_.collection.toJSON();
						var zoneDataIsNotExit;
						if(zone_data_.length>0){
							zoneDataIsNotExit=false;
						}else{
							zoneDataIsNotExit=true;
						}
						var exitZone=false;
						var exitZone_data=new Array();
						if(area_name){
							if(area_name.split(',').length>0){
								exitZone=true;
							}else{
								exitZone=false;
							}

							/*for(var i=0;i<area_name.split(',').length;i++){
								var e_zone_data={};
								e_zone_data['obj_name']=area_name.split(',')[i];
								e_zone_data['obj_id']=area_id.split(',')[i];
								exitZone_data.push(e_zone_data);
							}*/
						}
						
						
						var zone_data=new Array();
						for(var i=0;i<zone_data_.length;i++){
							if(area_name){
								if($.inArray(zone_data_[i].obj_name,area_name.split(','))>-1){
									exitZone_data.push(zone_data_[i]); ;
								}else{
									zone_data.push(zone_data_[i]);
								}
							}else{
								zone_data.push(zone_data_[i]);
							}
							
						}
						var d=dialog({
							title:'Select Zone',
							backdropOpacity:0.2,
							content:v_.template({exitZone:exitZone,exitZone_data:exitZone_data,zoneDataIsNotExit:zoneDataIsNotExit,zone_data:zone_data}),
							button:[
								{
									value:'Confirm',
									callback:function(){
										select_zone_confirm();
									},
									autofocus: true
								},
								{
									value:'Cancel'
								}
							]
						});
						d.showModal();
						d.addEventListener('close',function(){
							focusNum=1;
						})
					}
				});
			}
		});
	return new selectZoneView;
})