define(["jquery",
        "backbone",
        "handlebars",
        "templates",
	    "art_Dialog/dialog-plus",
	    "../models/gis_collection",
	    'config',
	    '../js/GoogleMapsV3',
        '../js/jsmap',
        "../js/gis_main"]
    ,function($,
    		backbone,
    		handlebars,
    		templates,
    		dialog,
    		gis_collection,
    		config){
		var selectPrinterServiersView= backbone.View.extend({
			template:templates.printer_server_list,
			collection:new gis_collection,
			pageCtrl:{ pageNo:1, pageSize: 3},
			render:function(formId){
				var v_=this;
				var formId_;
				if(formId){
					v_.pageCtrl.pageNo=1;
					window.printerForm=formId;
					formId_=formId	
				}else{
					formId_=window.printerForm;
				}

				focusNum=1;
				v_.template=templates.printerServers;
				var d;
				v_.collection.fetch({
					url:config.qureyprintServers.url,
					data: { pageNo: v_.pageCtrl.pageNo, pageSize: v_.pageCtrl.pageSize,ps_id:currentPsId },
					success:function(){
						var p_serviers_data=v_.collection.toJSON()[0].data;
						var pageCtrl=v_.collection.pageCtrl;
						var isExistData;
						if(p_serviers_data && p_serviers_data.length>0){
							isExistData=true;
						}else{
							isExistData=false;
						}
						var ps_id=viewData["storageThirdlyMenusTabsView"].ps_id;
						
						if(dialog.get('printerServer')){
							d=dialog.get('printerServer').content(v_.template({p_serviers_data:p_serviers_data,isExistData:isExistData,pageCtrl:pageCtrl})).showModal();
						}else{
							d=dialog({
								title:'Choose Printer Servers',
								width:600,
								height:230,
								cid:'aa',
								content:v_.template({p_serviers_data:p_serviers_data,isExistData:isExistData,pageCtrl:pageCtrl}),
								backdropOpacity:0.2,
								id:'printerServer',
							}).showModal();
						}
						setTimeout(v_.loadingCickEvt(d,formId_),500);
						d.addEventListener('close',function(){
							focusNum=0;
						})
					},
					error:function(collection,resp){
						
					}
				})
			},
			loadingCickEvt:function(d,formId){
				var v_=this;
				$('.choicePrinterServer').bind('click',function(){
					var s_id=$(this).data('serverid');
					var s_name=$(this).data('severname');
					choosePrinterServer(s_id,s_name,formId);
					d.close().remove();
				});
				$('button.selectPServers').bind('click',function(evt){
					v_.changePage_(evt);
				})
				
			},
			events: {
				"click button.selectPServers":"changePage_"
			},
			changePage_:function(evt){
	            var btn = $(evt.currentTarget);
	            if(btn.data("pageno")){
	                this.pageCtrl.pageNo = parseInt(btn.data("pageno"));
	            }
	            else if(btn.data("pageplus")){
	                this.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
	            }
	            else return;
	            this.render();
	        }
		});
	return new selectPrinterServiersView;
})