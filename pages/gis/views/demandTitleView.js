define(["jquery","backbone","handlebars","templates",
	"../models/gis_collection","../js/GoogleMapsV3",
    "../js/jsmap","../js/gis_main"],
    function($,backbone,handlebars,templates){
		var demandTitle=backbone.View.extend({
			render:function(data,template,el){
				this.setElement(el);
				this.template=template;
				this.$el.html('');
				this.$el.html(this.template({demand_dt:data}));
			},
			events:{
				'click .demand_title_bnt':'select_demand_title',
				'click .demand_all_title_div input[type="radio"]':'select_all_title'
			},
			select_demand_title:function(evt){
				var tile_div=$(evt.target).parents('.demand_title_div').eq(0);
				$(".demand_title_div").css('background','#F4F4F4');
				tile_div.css('background','#CDCDC1');
				var titleName=tile_div.data('name');
				var titleId=tile_div.data('id');
				$("#pro_title").val(titleName);
				$("#pro_title").data('id',titleId);
				$('.demand_all_title_div input[type="radio"]').removeAttr("checked");
				ajaxLotNumber(titleId);
				ajaxProductData(titleId,0,"#productDemand #pro_line","productDemand",pro_url.url);

			},
			select_all_title:function(evt){
				
				var titleName=$(evt.target).attr('title');
				var titleId=$(evt.target).val();
				$("#pro_title").val(titleName);
				$("#pro_title").data('id',titleId);
				$(".demand_title_div").css('background','#CDCDC1');
				var parent="productDemand";
				$("#"+parent+" #pro_lot_num_tr").hide();
				$("#"+parent+" #pro_lot_num").html("");
				$("#"+parent+" #pro_line_tr").hide();
				$("#"+parent+" #pro_line").html("");
				selectCategory(1,"-1",parent);
			}
		});
		return new demandTitle;
})