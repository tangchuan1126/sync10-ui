define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"
	],
	function($, backbone, handlebars, templates, gis_collection, dialog,config){
		var webcamView= backbone.View.extend({
			render:function(data,template,formId){
				this.template=template;
				var v_=this;
				verifyMgs={};
				var d=dialog.get("Webcam_id");
				if(d){
					var id=$('#'+formId+' input:hidden[name="id"]').val();
					//将前一个webcam设置为不可以拖动的
					reSetObjectDraggable(6,data.ps_id,id);
					var key=data.ps_id+"_"+id;
					reLoadWebcam(data.ps_id,'cancel',id);
					d.content(v_.template({data:data,formId:formId}));
				}else{
					d=dialog({
						title:"Set Webcam ["+data.port+"]",
						content:v_.template({data:data,formId:formId}),
						id:'Webcam_id',
						button:[
							{
								value:'Submit',
								callback:function(){
									alterDialogDisabled("div[aria-labelledby='title:Webcam_id'] button",true,'Submit');
									webcam_CommitData(data,this,formId);
									return false;
								},
								autofocus: true
							},
							{
								value:'Cancel'
							}
						]
					}).show();;
				}
				d.addEventListener('close', function () {
					var flag="cancel";
				    if(this.returnValue){
				    	flag=this.returnValue
				    }
				    this.remove();
				    verifyMgs={};
			    	reLoadWebcam(data.ps_id,flag,data.id);
			    	reSetObjectDraggable(6,data.ps_id,data.id);
				});
				
			}
		});
		return new webcamView;
})