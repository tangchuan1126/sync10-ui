define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus","../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"],
		function($,backbone,handlebars,templates,gis_collection,dialog,config){
		var setRefreshTime=backbone.View.extend({
			template:templates.set_refresh_time,
			render:function(data){
				var v_=this;
				backdropOpacity
				var d=dialog({
					title:'Set Refresh Time ',
					content:v_template({timeData:data}),
					backdropOpacity:0.2,
					button:[
						{
							value:'Submit',
							callback:function(){
								commitRefreshTime();
							},
							autofocus:true
						},
						{
							value:'Cancel'
						}
					]

				})
			}
		})
		return new setRefreshTime;
})