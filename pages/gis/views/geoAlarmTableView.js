define(["jquery",
  "backbone",
  "handlebars",
  "templates",
  "../config",
  "../models/gis_collection",
  '../js/gis_main',
  '../js/GoogleMapsV3',
    '../js/jsmap',"../js/gis_main"],
    function($,backbone,handlebars,templates,config,gis_collection){
    	var geoFencing=backbone.View.extend({
    		el:'.childNode',
    		collection:new gis_collection,
    		pageCtrl:{ pageNo:1, pageSize: 10},
    		render:function(data,template){
    			this.template=template;
    			var v_=this;
    			v_.dt=$.extend({},data,v_.pageCtrl);
    			v_.$el.html("");
    			v_.collection.fetch({
    				url:'/Sync10/_gis/geoRoutePointCotroller/getGeoAlarmLabel',
    				data:v_.dt,
    				success:function(){
	    				var gaoData=v_.collection.toJSON()[0].datas;
	    				var pageCtrl=v_.collection.pageCtrl;
	    				v_.$el.html(v_.template({gaoData:gaoData,pageCtrl:pageCtrl}));
    				}
    			});
    		},
    	events: {
			"click button.page-ctrl-geo":"changePage"
		},
		changePage:function(evt){
            var btn = $(evt.currentTarget);
            if(btn.data("pageno")){
                this.pageCtrl.pageNo = parseInt(btn.data("pageno"));
            }
            else if(btn.data("pageplus")){
                this.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
            }
            else return;
            this.render(this.dt,this.template);
        }
    	});
    	return new geoFencing;
})