define(['jquery',
        'backbone',
        'templates',
        '../models/gis_collection',
        './storageThirdlyMenusView',
        './maintenanceMenusView',
        './containerMenusView',
        './printerServersView',
        'config',
		'jqueryui/tabs'],
	function($,backbone,
		templates,
		gis_collection,	
		storageThirdlyMenusView,
		maintenanceMenusView,
		containerMenusView,
		printerServersView,
		config
		){
	var storageTabsView= backbone.View.extend({
		//el:"#bottom_menus",
		collection:new gis_collection,
		template:templates.three_level_tabs,
		initialize:function(){
		},
		render:function(selectKmlOpFlag){
			this.setElement('#bottom_menus');
			$("#bottom_menus").html("");
			var data=[  {"title":"Layer","id":"tab1"},
						{"title":"Maintain","id":"tab2"},
						{"title":"Container","id":"tab3"},
						{"title":"PrintServer","id":"tab4"}
							];
			var ps_id=viewData["storageThirdlyMenusTabsView"].ps_id;
			var storageName=viewData["storageThirdlyMenusTabsView"].title;
			window.currentPsId=ps_id;//定义一个window的全局的ps_id变量;
			this.$el.html(this.template({data:data}));
			$("#tabs").tabs({active:0});
			storageThirdlyMenusView.render(selectKmlOpFlag);
			maintenanceMenusView.render({"ps_id":ps_id,name:storageName});
			//containerMenusView.render({"ps_id":ps_id,name:storageName});
			//printerServersView.render({el:"#tab4"});
			$( "#tabs" ).tabs({
			  activate: function( event, ui ) {
			  	if(ui.newPanel.selector=='#tab1'){
			  		focusNum=0;
			  		//selectKml('',ps_id,"");
			  	}else if(ui.newPanel.selector=='#tab4'){
			  		focusNum=1;
			  		printerServersView.render({el:"#tab4"});
			  	}else if(ui.newPanel.selector=='#tab3'){
			  		containerMenusView.render({"ps_id":ps_id,name:storageName});
			  	}
			  }
			});
		}
	});
	return new storageTabsView();
})