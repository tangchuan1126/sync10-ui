define(['jquery',
        "backbone",
        "handlebars",
        'templates',
        "../models/gis_collection",
        "../js/gis_main",
        "../config",
        './truck_treeView',
        '../js/GoogleMapsV3',
        '../js/jsmap'
        ],
  function($,backbone,handlebars,templates,gis_collection,gis_main,page_config,truck_treeView){
  	var truckView= backbone.View.extend({
  		template:templates.truckTemplate,
  		collection:new gis_collection(),
      events:{
        "click #truck_second_boxConR":"changePage", 
        "click #truck_second_boxConR":"changePage",
        "click div.truck_btn":"loadThirdTruck"
      },
  		render:function(){
  			var v_=this;
        v_.setElement('#assetTree');
  			v_.$el.html('');
  			v_.collection.fetch({
  				url:page_config.truckByGroubModel.url,
  				success:function(response){
  					var dt_=pageCtrl(response,8);
			        v_.collection.reset(dt_);//j
			        if(dt_.length==1){
			        	v_.$el.html(v_.template({menus_dt:dt_[0],pageplus_L:false,pageplus_R:false,num:0}));
			        }else{
			        	v_.$el.html(v_.template({menus_dt:dt_[0],pageplus_L:false,pageplus_R:true,num:0}));
			        }
              if(menusSelectedFlag['seconeMenu']){
                changeSelectMenuBgCss($('.'+menusSelectedFlag['seconeMenu'].nodeClass));
              }
				}
  			})
  		},
      loadThirdTruck:function(evt){
      $(".category_container_truck").css("background","initial");
      var btnObj=$(evt.target);
      var p_buttonbox=changeSelectMenuBgCss(btnObj);
      var nodeClassArray=p_buttonbox.find('.truck_second_menu').attr('class').split(' ');
    	var group_id =$(evt.target).data("group_id");
    	var group_name =$(evt.target).data("name");
      menusSelectedFlag['seconeMenu']={'nodeClass':nodeClassArray[nodeClassArray.length-1],data:{"group_id":group_id,"group_name":group_name}};
			truck_treeView.render({"group_id":group_id,"group_name":group_name});
      	},
      changePage:function(evt){
        this.$el.html('');
        var num=$(evt.target).data("num");
        var pageplus=$(evt.target).data("pageplus");
        var truckSecondMenus_dt_=this.collection.toJSON();
        if (pageplus==1) {
          if(truckSecondMenus_dt_.length==(num+2)){
            this.$el.html(this.template({menus_dt:truckSecondMenus_dt_[num+1],pageplus_L:true,pageplus_R:false,num:num+1,title:flag}));
          }else{
            this.$el.html(this.template({menus_dt:truckSecondMenus_dt_[num+1],pageplus_L:true,pageplus_R:true,num:num+1,title:flag}));
          }
        }else{
          if(num-1==0){
            this.$el.html(this.template({menus_dt:truckSecondMenus_dt_[num-1],pageplus_L:false,pageplus_R:true,num:num-1,title:flag}));
          }else{
            this.$el.html(this.template({menus_dt:truckSecondMenus_dt_[num-1],pageplus_L:true,pageplus_R:true,num:num-1,title:flag}));
          }
        }
        if(menusSelectedFlag['seconeMenu']){
              changeSelectMenuBgCss($('.'+menusSelectedFlag['seconeMenu'].nodeClass));
            }
      },
      
  	});
  	return new truckView();
})