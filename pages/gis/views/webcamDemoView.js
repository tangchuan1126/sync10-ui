define(['jquery', 'backbone', 'handlebars', "templates",
		"../models/gis_collection", "art_Dialog/dialog-plus", "../config",
		'../js/GoogleMapsV3', '../js/jsmap', "../js/gis_main"
	],
	function($, backbone, handlebars, templates, gis_collection, dialog, config) {
		var webcamView = backbone.View.extend({
			template: templates.webcamDemo,
			collection: new gis_collection,
			render: function(data, formId) {
				//verifyMgs={};
				var v_ = this;

				v_.collection.fetch({
					url: config.queryCam.url,
					data: {
						'ps_id': data.ps_id
					},
					success: function() {
						var dt_ = v_.collection.toJSON();
						$.extend(data,dt_[0]);
						var d = dialog({
							title: "Add New Webcam",
							content: v_.template({
								data: data,
								formId: formId
							}),
							id: 'webcam_deme',
							button: [{
								value: 'Submit',
								callback: function() {

									alterDialogDisabled("div[aria-labelledby='title:webcam_deme'] button", true, 'Submit');
									webcamDemoSubmit(data.ps_id, this, formId);
									return false;
								},
								autofocus: true
							}, {
								value: 'Cancel'
							}]
						});
						$.unblockUI();
						d.addEventListener('close', function() {
							var flag = "";
							if (this.returnValue) {
								flag = this.returnValue;
							} else {
								flag = 'cancel';
							}
							jsmap.clearSingleWebcam(data.ps_id, 'demo');
							reLoadWebcam(data.ps_id, flag, 'demo');
							jsmap.setOptions({
								draggable: true,
								scrollwheel: true
							}); //地图的draggable 和scrollwheel 状态还原
							this.remove();
							verifyMgs = {};
						});
						d.show();
					}
				});

			}
		});
		return new webcamView;
	})