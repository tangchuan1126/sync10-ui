define(['jquery',"backbone","handlebars",
    "templates",'art_Dialog/dialog-plus'],
    function($,backbone,handlebars,templates,dialog){
    	var loc_frequencyView=backbone.View.extend({
    		template:templates.loc_frequency,
    		render:function(data){
    			var dt=data;
    			var v_=this;
    			var d=dialog({
    				title:'Set GPS Frequency['+data.nam+']',
    				content:v_.template({aid:dt.aid}),
    				backdropOpacity: 0.2, // 透明度
                    button: [{
                        value: 'Confirm',
                        callback: function() {
                            saveCmd(this);
                            return false;
                        },
                        autofocus: true
                    }, {
                        value: 'Cancel'
                    }]
                }).showModal();
                $("#truck_data").data('dt',dt);
    		}
    	});
    	return new loc_frequencyView;
})	