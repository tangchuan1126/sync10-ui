define(["jquery",
        "backbone",
        "handlebars",
        "templates",
	    "art_Dialog/dialog-plus",
	    "../models/gis_collection",
	    'config',
	    '../js/GoogleMapsV3',
        '../js/jsmap',
        "../js/gis_main"]
    ,function($,
    		backbone,
    		handlebars,
    		templates,
    		dialog,
    		gis_collection,
    		config){
		var printerServersView= backbone.View.extend({
			template:templates.printer_server_list,
			collection:new gis_collection,
			pageCtrl:{ pageNo:1, pageSize: 5},
			render:function(options){
				var v_=this;
				v_.setElement(options.el);
				v_.template=templates.printer_server_list;
				v_.collection.fetch({
					url:config.qureyprintServers.url,
					data: { pageNo: v_.pageCtrl.pageNo, pageSize: v_.pageCtrl.pageSize,ps_id:currentPsId },
					success:function(){
						var p_serviers_data=v_.collection.toJSON()[0].data;
						var pageCtrl=v_.collection.pageCtrl;
						var isExistData;
						if(p_serviers_data && p_serviers_data.length>0){
							isExistData=true;
						}else {
							//删除printerServer时进行判断
							if(v_.pageCtrl.pageNo==1){
								isExistData=false;
							}else{
								v_.pageCtrl.pageNo=v_.pageCtrl.pageNo-1;
								v_.render({el:"#tab4"});
							}
						}
						var ps_id=viewData["storageThirdlyMenusTabsView"].ps_id;
						v_.$el.html('');
						v_.$el.html(v_.template({p_serviers_data:p_serviers_data,isExistData:isExistData,ps_id:ps_id,pageCtrl:pageCtrl}));
					},
					error:function(collection,resp){
						
					}
				})
			},
			events: {
				"click button.page-ctrl":"changePage"
			},
			changePage:function(evt){
	            var btn = $(evt.currentTarget);
	            if(btn.data("pageno")){
	                this.pageCtrl.pageNo = parseInt(btn.data("pageno"));
	            }
	            else if(btn.data("pageplus")){
	                this.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
	            }
	            else return;
	            this.render({el:"#tab4"});
	        }
		});
	return new printerServersView;
})