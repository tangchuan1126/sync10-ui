define(['backbone','../config','./gis_models'], function(Backbone,page_config,AdminModel){
    return Backbone.Collection.extend({
        model:AdminModel,
        url:null,
        pageCtrl:{
            pageNo:1,
            pageSize:8
        },
        flag:{
            name:''
        },
        parse:function(response){
            this.pageCtrl = response.pageCtrl;
        	return response;
        }
    });
});