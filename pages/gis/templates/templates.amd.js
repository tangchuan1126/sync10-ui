define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['StorageSkeletonInfo'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		    			<option value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"raised\">\n	<div class=\"boxcontent\">\n		Add Storage\n	</div> \n</div>\n<div class=\"maintain_storage\">\n<br/>\n<fieldset class=\"maintain_storage_fieldset\" style=\"border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;margin-bottom: 20px;\">\n	<legend style=\"font-size:15px;font-weight:normal;color:#999999;\">\n		Add New Storage\n	</legend>	 		\n	<form  id=\"maintain_storage_add_form\" method=\"post\" action=\"\" >\n		<table width=\"98%\" border=\"0\" cellspacing=\"5\" cellpadding=\"2\" class=\"maintain_add_storage_info\">\n		  <tr >\n		   \n		    <td align=\"left\" valign=\"middle\" >\n		    	<div class=\"input-group form-group has-feedback\">\n		    		<span class=\"input-group-addon\">Name</span>\n		    		<select name=\"ps_id\" class=\"gis_main_select\" style=\"height:34px;width: 100%;\">\n		    		<option value=\"0\">choose...</option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.storageData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			    	</select> \n			    	<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\n		    	</div>\n		    	\n		    </td>\n		  </tr>\n		  <tr >\n			<td>\n				<br>\n				<div style=\"position: relative;\" class=\"input-group\">\n					<span class=\"input-group-addon\">Locate</span>\n					<input type=\"text\" style=\"width:170px;\" class=\"form-control\" id=\"locationStorage\">\n					<img src=\"imgs/orientation_img.png\" onclick=\"locationStorage_()\" style=\"position: absolute;cursor: pointer;\" alt=\"Position the warehouse\"/>\n				<div>\n			</td>\n		  </tr>\n		  <tr>\n		  \n			\n			<td colspan=\"2\">\n				<div style=\"position: relative;\">\n					<input name=\"op\" value=\"1\" type=\"radio\" onclick=\"inMapsOptionStorage(this)\"/>Existing Points\n					<input name=\"op\" value=\"2\" type=\"radio\" onclick=\"inMapsOptionStorage(this)\"/>Draw Border\n\n\n				<div>\n			</td>\n		  </tr>\n		  <tr id=\"hand_fill_point\" style=\"display:none;\">\n			<td align=\"right\" valign=\"middle\" class=\"STYLE3\" >Points</td>\n			<td>\n				<div style=\"position: relative;\">\n					<input id=\"point_num\" value=\"\" type=\"text\" style=\"width:100px;\"/>\n					<input type=\"button\" id=\"maintain_drawStorage_next\" name=\"Submit2\" style=\"background: url(imgs/normal_green.gif); \" value=\"Next\" class=\"normal-green\" >\n				<div>\n			</td>\n		  </tr>\n		</table>\n		<table width=\"98%\" border=\"0\" cellspacing=\"5\" cellpadding=\"2\" class=\"maintain_add_storage_latLngs\" style=\"padding-right: 35px;\">\n			\n		</table>\n	</form>	\n</fieldset>\n\n<table style=\"width: 100%;\">\n  <tr style=\"width: 100%;\" align=\"right\">\n    <td style=\"width: 100%\">\n	  <input type=\"button\" id=\"maintain_addStorage_btn\" name=\"Submit2\"  value=\"Save\" class=\"btn btn-success disabled\" onClick=\"addMaintainStorage();\">\n	  <input type=\"button\" id=\"maintain_addStorage_cancel\" name=\"Submit3\" value=\"Canle\" class=\"btn btn-info disabled\" onClick=\"addStorageCancel();\">\n	</td>\n  </tr>\n</table> \n\n</div>";
},"useData":true});
templates['addMultiLayer'] = template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "\n			<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Area name：</span>\n				<input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" value=\"\" onchange=\"this.value=this.value.trim();verifyName_(this,'"
    + this.escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "','5','','add')\" />\n				<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\n			</div>\n		";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n			<div id=\"_zone\" class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Zone：</span>\n				<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('modify_storage_layer')\" onfocus=\"selectAreaSingle('modify_storage_layer')\"\n				/>\n				<span class=\"glyphicon form-control-feedback\"></span>\n				<input type=\"hidden\" style=\"width: 80%;\" name=\"area_id\" id=\"area_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\"/>\n			</div>\n		";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression, alias4=this.lambda;

  return "\n		<input type=\"hidden\" id =\"storage_id\" name=\"ps_id\" value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" name=\"storage_name\" id =\"storage_name\" value=\""
    + alias3(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"storageName","hash":{},"data":data}) : helper)))
    + "\"> \n		<div class=\"layerTypeRadio\" style=\"width: 98%;height: 33px;border: 1px solid #ccc;border-radius: 5px;margin-bottom: 5px;\">\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"1\" onclick=\"typeChange(1)\" >Location</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"2\" onclick=\"typeChange(2)\">Staging</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"3\" onclick=\"typeChange(3)\">Dock</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"4\" onclick=\"typeChange(4)\" >Parking</div>\n		</div>\n		<!--"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.isExistArea : depth0),{"name":"unless","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "-->\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X Position：</span>\n			<input type=\"text\" class=\"form-control\"  name=\"x\" id=\"x\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.x : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y Position：</span>\n			<input type=\"text\" class=\"form-control\" name=\"y\" id=\"y\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.y : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X length：</span>\n			<input type=\"text\" class=\"form-control\" name=\"height\" id=\"height\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.height : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y length：</span>\n			<input type=\"text\" class=\"form-control\" name=\"width\" id=\"width\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.width : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Offset angle：</span>\n			<input type=\"text\" class=\"form-control\" name=\"angle\" id=\"angle\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.angle : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div id=\"_zone\" class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Zone：</span>\n				<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('modify_storage_layer')\" onfocus=\"selectAreaSingle('modify_storage_layer')\"\n				 readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n				<span class=\"glyphicon form-control-feedback\"></span>\n				<input type=\"hidden\" style=\"width: 80%;\" name=\"area_id\" id=\"area_id\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\"/>\n			</div>\n		<!--"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isExistArea : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "-->\n		<div id=\"_dimensional\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Dimensional：</span>\n			<select class=\"gis_main_select\" name=\"is_three_dimensional\" style=\"width: 200px;\">\n				<option value=\"0\">2D</option>\n				<option value=\"1\">3D</option>\n			</select>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">vertically:</span>\n			<input type=\"text\" class=\"form-control\" value=\"\" id=\"vertically\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVer')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">horizontally:</span>\n			<input type=\"text\" class=\"form-control\" value=\"\" id=\"horizontally\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHor')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Name Prefix:</span>\n			<input type=\"text\" class=\"form-control\" value=\"\" id=\"loc_name\"   onchange=\"this.value=this.value.trim()\" >\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">V_Interval:</span>\n			<input type=\"text\" class=\"form-control\" value=\"0\" id=\"v_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVi')\" >\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\"> \n			<span class=\"input-group-addon\" style=\"width: 38%;\">H_Interval:</span>\n			<input type=\"text\" class=\"form-control\" value=\"0\" id=\"h_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHi')\"  >\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div style=\"margin-bottom: 5px;float: right;\">\n			<input id=\"reverseName_\" style=\"display:none;\"  type=\"button\"  class=\"btn btn-info\" value=\"ReverseName\" onclick=\"reverseName()\">\n			<input type=\"button\" class=\"btn btn-success btn-sm\" value=\"Create\"   onclick=\"createLocation()\" >\n			<input type=\"button\" class=\"btn btn-info btn-sm\" value=\"Clear\" onclick=\"clearAutoLayer('clearAutoLayer')\">\n		</div>\n		\n\n\n		\n		";
},"useData":true});
templates['add_asset'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "											<option value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:300px;\">\n	<form id=\"add_asset\">\n	  <table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n		  <tr> \n		    <td colspan=\"2\" align=\"center\" valign=\"top\">\n				  <fieldset style=\"border:2px #cccccc solid;padding:15px;margin:0px 10px 0px 10px;width:90%;\">\n							<div class=\"input-group\" style=\"margin-bottom: 10px;\">\n										<span class=\"input-group-addon\" style=\"width:52%;\">Name：</span>\n										<input type=\"text\" class=\"form-control\" name=\"assetName\" id=\"assetName\" onblur=\"verifyName(this);\"/>\n										<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n									</div>\n									\n					   	    \n									<div class=\"input-group\" style=\"margin-bottom: 10px;\">\n										<span class=\"input-group-addon\" style=\"width:52%;\">License plate：</span>\n										<input type=\"text\" class=\"form-control\" name=\"gate_liscense_plate\" />\n										<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \n									</div>\n								\n						   	    	<div class=\"input-group\" style=\"margin-bottom: 10px;\">\n						   	    		<span class=\"input-group-addon\" style=\"width:52%;\">GPS num：</span>\n						   	    		<input type=\"text\" class=\"form-control\" name=\"gps_tracker\" id=\"gps_tracker\" />\n						   	    		<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n						   	    	</div>\n					   	    	\n							  		<div class=\"input-group\" style=\"margin-bottom: 10px;\">\n							  			<span class=\"input-group-addon\" style=\"width:52%;\">Phone num：</span>\n							  			 <input type=\"text\" class=\"form-control\"  name=\"callnum\" id=\"callnum\" />\n							  			 <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n							  		</div>\n						  		\n						    	<div class=\"input-group\" style=\"margin-bottom: 10px;\">\n						    		<span class=\"input-group-addon\">Group： </span>\n						    		<select id=\"group\" name=\"groupId\" style=\"width:100%;font-size:12px;height:34px;\">\n										<option value=\"-1\"></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.groupData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "											<option value=\"0\" style=\"color: silver\">New group</option>\n									</select>\n									<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n						    	</div>\n						    	<div id=\"newGroup_td\" class=\"input-group\" style=\"margin-bottom: 10px;\">\n\n						    	</div>\n						   		<input type=\"hidden\" name=\"groupName\" id=\"groupName\"/>\n						   		<input type=\"hidden\" name=\"flag\" id=\"flag\"/>\n					  </fieldset>\n	           </td>\n	        </tr>\n	   </table>\n	</form>\n</div>";
},"useData":true});
templates['add_printer_server'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<option value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" >"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<form id=\"addForm\">\n   <input type=\"hidden\" name=\"Method\" value=\"Add\"/>\n   		<div class=\"input-group\" style=\"margin-bottom: 10px;width: 300px;\">\n   			<span class=\"input-group-addon\">Server&nbsp;Name:</span>\n   			<input class=\"form-control\" name=\"printer_server_name\" id=\"printer_server_name\" type=\"text\">\n   		</div>\n   		<div class=\"input-group\" style=\"margin-bottom: 10px;width: 300px;\">\n   			<span class=\"input-group-addon\" style=\"width:38%;\">Storage:</span>\n   			<select id=\"storage\" style=\"height:34px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.allStorageKml : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            	 \n			</select>\n   		</div>\n   		<div class=\"input-group\" style=\"width: 300px;\">\n   			<span class=\"input-group-addon\" style=\"width:38%;\">Zone：</span>\n   			<input type=\"text\"\n				class=\"form-control\" name=\"zone\" id=\"add_printer_server_zone\"\n				value=\"\" onclick=\"addPrinter_server_selectZone()\" onfocus=\"addPrinter_server_selectZone()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n			<input type=\"hidden\"\n				style=\"width: 100%;\" name=\"area_id\" id=\"add_printer_server_area_id\"\n				value=\"\"/>\n   		</div>\n  	 	 \n</form>";
},"useData":true});
templates['alterStorage'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		    			<option value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"background: white;\">\n	<fieldset>\n		<form id=\"alterStorageForm\" method=\"post\"> \n			<div class=\"input-group form-group has-feedback\" style=\"margin-top:5px;\">\n				<span class=\"input-group-addon\">Storage Name</span>\n				<select name=\"ps_id\" class=\"gis_main_select\" id=\"select_storage\"  style=\"height:34px;width: 100%;\">\n		    		<option value=\"0\">choose...</option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.storageData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		    	</select> \n			</div>\n			<div id=\"storageOutline\">\n\n			</div>\n			<div class=\"input-group \" id=\"storageAlter_btn\"  style=\"position: relative;width: 100%;height:40px;\">\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_edit\" style=\"position: absolute;right:75px;width:64px;\" value=\"Edit\" class=\"btn btn-primary\">\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_cancel\" style=\"position: absolute;right:75px;width:64px;display:none;\" value=\"Cancel\" class=\"btn btn-default\">\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_save\" style=\"position: absolute;right:0px;width:64px;display:none;\" value=\"Save\" class=\"btn btn-success \">\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_delete\" style=\"position: absolute;right:0px;width:64px;\" value=\"Delete\" class=\"btn btn-warning\">\n			</div>\n		</form>\n	</fieldset>\n</div>";
},"useData":true});
templates['classify'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        <div class=\"category_container\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n        <div class=\"buttons\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n		<div class=\" category_icon_base "
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"></div>\n		<a class=\" toolBtn-cont\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"></a>\n        </div>\n		</div>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "		   <div id=\"boxConR\" data-pageplus=\"1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base boxConR\">\n		   </div> \n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "		   <div id=\"boxConL\" data-pageplus=\"-1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base boxConL\">\n		   </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"category_icon_collection\">\n    <div style=\"margin-left: 8px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.classify : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.R_pageplus : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.L_pageplus : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n\n";
},"useData":true});
templates['containerTemplate'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "										<option value=\""
    + alias3(((helper = (helper = helpers.ic_id || (depth0 != null ? depth0.ic_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ic_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.container_no || (depth0 != null ? depth0.container_no : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"container_no","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div id=\"stagingPlate\" style=\"width:100%; overflow: auto;background-color: #FFFFFF;\">\n		<table class=\"table table-hover\">\n					<tboby>\n						<tr >\n							<td style=\"text-align: center;\">Storage:</td>\n							<td style=\"text-align: center;\">\n								<span>"
    + this.escapeExpression(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"storageName","hash":{},"data":data}) : helper)))
    + "</span>\n							</td>\n						</tr>\n						<tr > \n							<td style=\"text-align: center;\">CTNR:</td>\n							<td style=\"text-align: center;\">\n								<select id=\"ic_id\" class=\"gis_main_select\" style=\"width:100px;\">\n									<option value=\"-1\">ALL</option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.container_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "								</select>\n							</td>\n						</tr>\n						<tr>\n							<td colspan=\"2\" align=\"right\">\n								<input type=\"button\" value=\"Inquiry\" class=\"btn btn-success\" onclick=\"queryPlate()\" />\n							</td>\n						</tr>\n					</tbody>\n		</table>\n</div>\n";
},"useData":true});
templates['demand'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"raised\">\n	<div class=\"boxcontent\">\n		Demand\n	</div>\n</div>\n\n<div id=\"productDemand\" \n	style=\" width:100%; overflow: auto;background: white;\n	border-bottom: 1px solid #ccc;\">\n	<div style=\"background-color: #FFF;\">\n		<form id=\"productDemandFrom\" class=\"form-horizontal\" role=\"form\">\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">Title:</span>\n				<input style=\"width: 95%;\" type=\"text\" class=\"form-control\" placeholder=\" Title\" name=\"pro_title\" id=\"pro_title\" />\n			</div>\n			<div class=\"input-group\" id=\"pro_line_tr\"  style=\"display: none;margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">PDT Line:</span>\n				<select style=\"width: 95%;\" name=\"pro_line\" class=\"gis_main_select\" id=\"pro_line\" onchange=\"selectCategory(1,this.value,'productDemand')\">\n					<option value='-1' selected='selected'>ALL</option>\n				</select>\n			</div>\n			<div class=\"input-group\" id=\"pro_category_1_tr\"  style=\"display: none;margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">1st Level:</span>\n				<select style=\"width: 95%;\" name=\"pro_line\" class=\"gis_main_select\" id=\"pro_line\" onchange=\"selectCategory(1,this.value,'productDemand')\">\n					<option value='-1' selected='selected'>ALL</option>\n				</select>\n			</div>\n			<div class=\"input-group\" id=\"pro_category_2_tr\"  style=\"display: none;margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">2nd Level:</span>\n				<select style=\"width: 95%;\" name=\"pro_category_2\" class=\"gis_main_select\" id=\"pro_category_2\"  onchange=\"selectCategory(3,this.value,'productDemand')\">\n					<option value='-1' selected='selected'>ALL</option>\n				</select>\n			</div>\n			<div class=\"input-group\" id=\"pro_category_3_tr\"  style=\"display: none;margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">3rd Level:</span>\n				<select style=\"width: 95%;\" name=\"pro_category_3\" class=\"gis_main_select\" id=\"pro_category_3\">\n					<option value='-1' selected='selected'>ALL</option>\n				</select>\n			</div>\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">PDT Name:</span>\n				<input style=\"width: 95%;\" type=\"text\" id=\"pro_name\" class=\"form-control\" placeholder=\" PDT Name\"/>\n			</div>\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">Begin:</span>\n				<input style=\"width: 95%;\" type=\"text\" class=\"form-control\"placeholder=\"Begin time\" id=\"start_time\" />\n			</div>\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\n				<span class=\"input-group-addon\" style=\"width: 40%;\">End:</span>\n				<input style=\"width: 95%;\" type=\"text\" class=\"form-control\" placeholder=\"End time\" id=\"end_time\" />\n			</div>\n			<div class=\"input-group\" style=\"margin: 4px 0;float:right;margin-right:10px;\">\n			<input type=\"button\" value=\"Inquiry\" style='height:30px; width:60px;font-size:9px' class=\"btn btn-primary\" onclick=\"searchProductDemandByCountry()\" />\n	</div>\n</div>\n<div class=\"demand_title\" style=\"width:100%;\">\n</div>";
},"useData":true});
templates['demandTitlePanle'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<div class=\"demand_title_div\" title=\""
    + alias3(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_name","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_name","hash":{},"data":data}) : helper)))
    + "\" data-id=\""
    + alias3(((helper = (helper = helpers.title_id || (depth0 != null ? depth0.title_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_id","hash":{},"data":data}) : helper)))
    + "\">\n				<div class=\"demand_title_bnt\">\n					<div class=\"text-overflow\" style=\"width: 85%; float: left; text-align: center;\" >\n						"
    + alias3(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title_name","hash":{},"data":data}) : helper)))
    + "\n					</div>\n				</div>\n			</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"raised\">\n		<div class=\"boxcontent\">\n			Demand Title\n		</div>\n	</div>\n	<div style=\" width:100%; overflow: auto;background: white;border-bottom: 1px solid #ccc;border-top: 2px solid #ccc;min-height:50px;max-height:320px;font-size: 9px\">\n		<div class=\"demand_all_title_div\" style=\"margin-left:15px\" titile=\"All\" data-name=\"All\" data-id=\"-1\" \n		  style=\"background: #EEEEEE;\">\n			<input type=\"radio\" value=\"-1\" title=\"All\">All\n		</div>\n		<div class=\"demand_title_parent\" style=\"margin-left:5px;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.demand_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n	</div>";
},"useData":true});
templates['dialog_product_info'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "			 <tr > \n			     \n				  <td height=\"60\" valign=\"middle\"   style='font-size:14px;' >\n			       <fieldset style=\"border:1px blue solid;padding:7px;width:240px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;\">\n				      <legend style=\"font-size:15px;font-weight:bold;color:black;\"> \n							 "
    + this.escapeExpression(((helper = (helper = helpers.p_name || (depth0 != null ? depth0.p_name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"p_name","hash":{},"data":data}) : helper)))
    + "\n					  </legend>	\n						    <p style=\"font-size: 12px\">商品分类:\n						    </p>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.pro_title_rows : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "								  								\n				   </fieldset>	\n		         </td>\n			     <td align=\"center\" valign=\"middle\"   >\n					  <fieldset style=\"border:1px green solid;padding:7px;width:80%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;\">\n							<legend style=\"font-size:15px;font-weight:bold;color:green;\">\n								\n							   <span style=\"font-size:13px;color:#999999;font-weight:normal\"><%=positionName %></span>\n							</legend>  \n								<p align=\"left\"><a href=\"javascript:void(0)\" onclick=\"openlot()\" style=\"text-decoration:none\">按批次查看库存</a></p>\n								<p align=\"left\"><a href=\"javascript:void(0)\" onclick=\"openlp()\" style=\"text-decoration:none\">按配置查看</a></p>\n								<p align=\"left\"><a href=\"javascript:void(0)\" onclick=\"opentitle()\" style=\"text-decoration:none\">按title查看</a></p>\n									<div>可用数量：</div>\n								 	<div>物理数量：</div>\n					  </fieldset>\n				  &nbsp;\n			  </td>\n			    <td align=\"center\" valign=\"middle\"   style='word-break:break-all;' nowrap=\"nowrap\"> \n				<input name=\"\" type=\"hidden\" value=\"\"  >\n				\n				<span style=\"display: block;overflow: inherit\"><font color=\"blue\">有效库存:</font></span>\n				<span style=\"display: block;overflow: inherit\"><font color=\"red\">盘点库存:</font></span>\n				<span style=\"display: block;overflow: inherit\">出库中:</span>\n			  </td>\n			   <td align=\"center\" valign=\"middle\"   style='word-break:break-all;font-weight:bold'>\n			  	功能残损:<br/>\n			  	外观残损:\n			  </td>\n		      <td align=\"center\" valign=\"middle\"   style='word-break:break-all;font-weight:bold'>\n			  	<div style=\"width:80%\" align=\"left\">\n			  		采样周期:<br/>\n			  		<font color=\"green\">需求数量:</font><br/>\n				  	<font color=\"blue\">计划备货:</font><br/>\n				  	<font color=\"\">发货数量:</font><br/> \n				  	销售系数:<br/>\n				  	备货天数:\n			  	</div>	  	\n			  </td>\n		 </tr>\n";
},"2":function(depth0,helpers,partials,data) {
    var helper;

  return "						    <p style=\"padding-left: 70px;font-size: 12px\">\n						         <span>\n						         <a href=\"javascript:void(0)\" onclick=\"\" style=\"text-decoration:none\">"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n								 </span>\n							</p>\n							\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<fieldset style=\"border:1px #C00 solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;\">\n		 <legend style=\"font-size:15px;font-weight:bold;\"> \n			"
    + alias3(((helper = (helper = helpers.positionName || (depth0 != null ? depth0.positionName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"positionName","hash":{},"data":data}) : helper)))
    + "  \n		 </legend>\n		 <div align=\"left\" style=\"border:2px #dddddd solid;background:#E8E8E8;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;\">\n      \n		 	<span style=\"font-size:12px; font-weight: bold; color:black\">当前位置:</span>\n            <span style=\"font-size:12px; font-weight: bold; color:#00F\">"
    + alias3(((helper = (helper = helpers.positionName || (depth0 != null ? depth0.positionName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"positionName","hash":{},"data":data}) : helper)))
    + "</span><br/>\n            <hr size=\"1px;\" color=\"#dddddd\">\n     	 </div>\n     	  	 <table width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"zebraTable\">\n	     	   <tr> \n		      <th width=\"20%\" align=\"left\" class=\"right-title\"  style=\"vertical-align: center;text-align: cenleftter;\">商品信息</th>\n		      <th width=\"20%\" class=\"right-title\"  style=\"vertical-align: center;text-align: center;\">库存信息</th>\n		      <!--<th width=\"17%\" class=\"right-title\"  style=\"vertical-align: center;text-align: center;\">残损</th>-->\n		      <th width=\"18%\" class=\"right-title\"  style=\"vertical-align: center;text-align: center;\">存取货时间</th>\n		 	 </tr>\n		 	 \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.pro_rows : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		 </table>\n	<table width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"3\" cellspacing=\"0\">\n	 <form name=\"dataForm\">\n      <input type=\"hidden\" name=\"p\" >\n	  <input type=\"hidden\" name=\"title_id\" id=\"title_id\" value=\""
    + alias3(((helper = (helper = helpers.titleId || (depth0 != null ? depth0.titleId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"titleId","hash":{},"data":data}) : helper)))
    + "\"/>\n	  <input type=\"hidden\" name=\"lot_number_id\" id=\"lot_number_id\" value=\""
    + alias3(((helper = (helper = helpers.lot_number_id || (depth0 != null ? depth0.lot_number_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"lot_number_id","hash":{},"data":data}) : helper)))
    + "\">	  \n	  <input type=\"hidden\" name=\"positionName\" id=\"positionName\" value=\""
    + alias3(((helper = (helper = helpers.positionName || (depth0 != null ? depth0.positionName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"positionName","hash":{},"data":data}) : helper)))
    + "\">	  \n	 </form>\n		       <!-- <tr> \n          \n			    <td height=\"28\" align=\"right\" valign=\"middle\"> \n			      <%\n			int pre = pc.getPageNo() - 1;\n			int next = pc.getPageNo() + 1;\n			out.println(\"页数：\" + pc.getPageNo() + \"/\" + pc.getPageCount() + \" &nbsp;&nbsp;总数：\" + pc.getAllCount() + \" &nbsp;&nbsp;\");\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"首页\",\"javascript:go(1)\",null,pc.isFirst()));\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"上一页\",\"javascript:go(\" + pre + \")\",null,pc.isFornt()));\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"下一页\",\"javascript:go(\" + next + \")\",null,pc.isNext()));\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"末页\",\"javascript:go(\" + pc.getPageCount() + \")\",null,pc.isLast()));\n			%>\n			      跳转到 \n			      <input name=\"jump_p2\" type=\"text\" id=\"jump_p2\" style=\"width:28px;\" value=\"<%=StringUtil.getInt(request,\"p\")%>\"> \n			      <input name=\"Submit22\" type=\"button\" class=\"page-go\" style=\"width:28px;padding-top:0px;\" onClick=\"javascript:go(document.getElementById('jump_p2').value)\" value=\"GO\"> \n			    </td>\n			        </tr>-->\n		 </table>\n		 \n      </fieldset>";
},"useData":true});
templates['docks_parking_ocupied'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "				<tbody> \n					<tr >\n						<td style=\"vertical-align: middle;\">\n							<div style=\"float: left;margin-left: 50px;\">\n								<div><img src=\"./imgs/"
    + alias3(((helper = (helper = helpers.equipment_type || (depth0 != null ? depth0.equipment_type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"equipment_type","hash":{},"data":data}) : helper)))
    + ".png\"></div>\n								<h7 style=\"text-align: left;margin-left: 10px; font-style: italic;\" class=\"text-muted\">\n								"
    + alias3(((helper = (helper = helpers.check_in_time || (depth0 != null ? depth0.check_in_time : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"check_in_time","hash":{},"data":data}) : helper)))
    + "\n								</h7>\n							</div>\n							<div style=\"float:left;width: 50%;\">\n								<h4 style=\"margin-left: 10px;\">\n								"
    + alias3(((helper = (helper = helpers.equipment_number || (depth0 != null ? depth0.equipment_number : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"equipment_number","hash":{},"data":data}) : helper)))
    + "\n								</h4>\n							</div>\n							<div style=\"float:left;width: 50%;\">\n								<h7 style=\"text-align: left;margin-left: 10px; font-style: italic;\" class=\"text-muted\">\n								"
    + alias3(((helper = (helper = helpers.occupy_status || (depth0 != null ? depth0.occupy_status : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"occupy_status","hash":{},"data":data}) : helper)))
    + "\n								</h7>\n							</div>\n							<div style=\"float:left;width: 50%;\">\n								<h7 style=\"text-align: left;margin-left: 10px; font-style: italic;\" class=\"text-muted\" >\n								"
    + alias3(((helper = (helper = helpers.rel_type || (depth0 != null ? depth0.rel_type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"rel_type","hash":{},"data":data}) : helper)))
    + "\n								</h7>\n							</div>\n						</td>\n						<td style=\"vertical-align: middle;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.tasks : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</td>\n					</tr>\n				</tbody>			\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<div style=\"width: 100%;line-height: 20px;float:left;margin: auto;\">\n								    <div style=\"width: 60%;float: left;\">\n						               <h5 style=\"text-align: left;font-style: italic;\">\n						             "
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.number_type : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " "
    + alias3(((helper = (helper = helpers.number || (depth0 != null ? depth0.number : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"number","hash":{},"data":data}) : helper)))
    + "\n						               </h5>\n						            </div>  \n						            <div style=\"width: 40%;float: left;\">\n						               <h6 style=\"text-align: center;font-style: italic;\"class=\"text-muted\">\n						               "
    + alias3(((helper = (helper = helpers.number_status || (depth0 != null ? depth0.number_status : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"number_status","hash":{},"data":data}) : helper)))
    + "\n						               </h6>\n						            </div>    \n								</div>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return " ["
    + this.escapeExpression(((helper = (helper = helpers.number_type || (depth0 != null ? depth0.number_type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"number_type","hash":{},"data":data}) : helper)))
    + "]:";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return " <div id=\"occupy_detail\">\n		<table id =\"occupy_detail\" class=\"table table-hover occupy_detail\" cellspacing=\"0\" cellpadding=\"0\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.datas : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</table>\n</div>	\n ";
},"useData":true});
templates['geoAlarmTable_list'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	<div class=\"text-overflow\" title=\"[ "
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + alias3(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + alias3(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\n	data-points=\""
    + alias3(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\n						"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + " \n	</div> \n	<hr style=\"size: 1px; width: 100%; color: #FFFFFF\" >\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.gaoFencingData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['geoFencing'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<div class=\"text-overflow\" title=\"[ "
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + alias3(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + alias3(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\n								data-points=\""
    + alias3(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n									<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\n													"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + " \n								</div>\n								<hr style=\"size: 1px; width: 100%; color: #FFFFFF;margin-top: 1px;margin-bottom: 1px;\" >\n";
},"3":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"raised\">\n	<div class=\"boxcontent\">\n		GeoFence\n	</div> \n</div>\n<div id=\"geoFencing\" style=\" width:98%; overflow-y: auto;margin:0 1%; background: #FFF;border-top: 2px solid #888;max-height: 450px;float:left;\">\n	<table style=\"width:100%;margin: auto;position:relative;\" class=\"table\">\n					<tr>\n						<td>\n							<div style=\"float: left; width: 100%;\">\n							    <input type=\"button\" value=\"Add\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-success\" onclick=\"addGeofencing('geoFencing')\" />\n							    <input type=\"button\" value=\"Ref\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" onclick=\"refreshGeoFencing('geoFencing')\" />\n							    <input type=\"button\" value=\"Del\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-danger\" onclick=\"removeGeoFencing('geoFencing')\" />\n							</div>\n							<div id=\"addGeoFencing\" style=\"display: none; float: left; width: 100%;background-color: #FFFFFF;margin-left:2%;\">\n								<form id=\"addGeoFencingForm\" class=\"form-horizontal\" role=\"form\" style=\"width:100%;\" >\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;margin-left:2%;\">Name:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"fencing_name\" name=\"name\" placeholder=\"Input Name\"   onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;margin-left:2%;\">Explain:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"fencing_def\" name=\"def\" placeholder=\"Input Explain\" onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 37%;margin-left:2%;\">Type:</span>\n	  									<select id=\"fencing_type\" name=\"type\" class=\"gis_main_select\" onchange=\"addGeofencing(this.value)\">\n											<option value=\"polygon\" selected=\"selected\">Polygon</option>\n											<option value=\"circle\">Circle</option>\n											<option value=\"rect\">Rectangle</option>\n										</select>\n	  								</div>\n	  								<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;margin-left:2%;\">Geometry:</span>\n	  									<input class=\"form-control\" type=\"text\" disabled=\"disabled\" id=\"fencing_points\" name=\"points\" onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<input type=\"button\" class=\"btn btn-primary\" style=\"margin:0 35px\" value=\"Cancel\" onclick=\"cancelGeo()\"/>\n									<input type=\"button\" class=\"btn btn-success\" value=\"Save\" onclick=\"saveAlarmLabel('fencing')\" />\n								</form>\n							</div>\n						</td>\n					</tr>\n					<tr>\n						<td>\n							<div id=\"geoFencing_list\" style=\" width:100%;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.gaoData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n							</div>\n						</td>\n					</tr>\n </table>\n <table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;width:288px;\">\n  	<thead>\n  			<tr>\n	    		<td >\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\n	                	First\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\n	                	Last\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\n	                	Next\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\n	                	End\n	                </button>\n	            </td>\n	    	</tr>\n	</thead>\n  </table>  \n</div>";
},"useData":true});
templates['geoLine'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "									<div class=\"text-overflow\" title=\"[ "
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + alias3(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + alias3(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\n									data-points=\""
    + alias3(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n										<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\n														"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + " \n									</div>\n									<hr style=\"size: 1px; width: 100%; color: #FFFFFF;margin-top: 1px;margin-bottom: 1px;\" >\n";
},"3":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"raised\">\n	<div class=\"boxcontent\">\n		Route\n	</div>\n</div>  \n<div id=\"geoLine\" style=\" width:98%; overflow-y: auto;margin:0 1%; background: #FFF;border-top: 2px solid #888;max-height: 450px;\">\n		<table style=\"width:100%;margin: auto;\" class=\"table\">\n					<tr> \n						<td>\n							<div style=\"float: left; width: 100%;\">\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-success\" onclick=\"addGeofencing('geoLine')\" value=\"Add\" / >\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" onclick=\"refreshGeoFencing('geoLine')\" value=\"Ref\" />\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-danger\" onclick=\"removeGeoFencing('geoLine')\" value=\"Del\" >\n							</div>\n							<div id=\"addGeoLine\" style=\"display: none; float: left; width: 100%; background: #FFFFFF;margin-left:2%;\">\n								<form id=\"addGeoLineForm\" class=\"form-horizontal\" role=\"form\">\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Name:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"line_name\" name=\"name\" placeholder=\"Input Name\"   onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Explain:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"line_def\" name=\"def\" placeholder=\"Input Explain\" onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Route:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"line_points\" name=\"points\" disabled=\"disabled\" onchange=\"this.value=this.value.trim()\"/>\n										<input type=\"hidden\" id=\"line_type\" name=\"type\" value=\"line\" />\n	  								</div>\n									<input type=\"button\" class=\"btn btn-primary\" style=\"margin:0 35px\" value=\"Cancel\" onclick=\"cancelGeo()\"/>\n									<input type=\"button\" class=\"btn btn-success\" value=\"Save\" onclick=\"saveAlarmLabel('line')\" />\n								</form>\n							</div>\n						</td>\n					</tr>\n					<tr>\n						<td>\n							<div id=\"geoLine_list\" style=\" width:100%;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.gaoData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							</div>\n						</td>\n					</tr>\n    </table>\n	<table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;width:288px;\">\n  	<thead>\n  			<tr>\n	    		<td >\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\n	                	First\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\n	                	Last\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\n	                	Next\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\n	                	End\n	                </button>\n	            </td>\n	    	</tr>\n	</thead>\n  </table>  \n				\n</div>";
},"useData":true});
templates['geoPoint'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "									<div class=\"text-overflow\" title=\"[ "
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + alias3(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + alias3(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\n									data-points=\""
    + alias3(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n										<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\n														"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + " \n									</div>\n									<hr style=\"size: 1px; width: 100%; color: #FFFFFF;margin-top: 1px;margin-bottom: 1px;\" >\n";
},"3":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"raised\">\n	<div class=\"boxcontent\">\n		GeoPoint\n	</div>\n</div> \n<div id=\"geoPoint\" style=\" width:98%; overflow-y: auto;margin:0 1%; background: #FFF;border-top: 2px solid #888;max-height: 450px;\">\n		<table style=\"width:100%;margin: auto;\" class=\"table\">\n					<tr> \n						<td>\n							<div style=\"float: left; width: 100%;\">\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-success\" onclick=\"addGeofencing('geoPoint')\" value=\"Add\" / >\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" onclick=\"refreshGeoFencing('geoPoint')\" value=\"Ref\" />\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-danger\" onclick=\"removeGeoFencing('geoPoint')\" value=\"Del\" >\n							</div>\n							<div id=\"addGeoPoint\" style=\"display: none; float: left; width: 100%; background: #FFFFFF;margin-left:2%;\">\n								<form id=\"addGeoPointForm\" class=\"form-horizontal\" role=\"form\">\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Name:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"point_name\" name=\"name\" placeholder=\"Input Name\"   onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Explain:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"point_def\" name=\"def\" placeholder=\"Input Explain\" onchange=\"this.value=this.value.trim()\"/>\n	  								</div>\n									<div class=\"input-group form-group has-feedback\">\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Point:</span>\n	  									<input class=\"form-control\" type=\"text\"  id=\"point_points\" name=\"points\" disabled=\"disabled\" onchange=\"this.value=this.value.trim()\"/>\n										<input type=\"hidden\" id=\"point_type\" name=\"type\" value=\"point\" />\n	  								</div>\n									<input type=\"button\" class=\"btn btn-primary\" style=\"margin:0 35px\" value=\"Cancel\" onclick=\"cancelGeo()\"/>\n									<input type=\"button\" class=\"btn btn-success\" value=\"Save\" onclick=\"saveAlarmLabel('point')\" />\n								</form>\n							</div>\n						</td>\n					</tr>\n					<tr>\n						<td>\n							<div id=\"geoPoint_list\" style=\" width:100%;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.gaoData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							</div>\n						</td>\n					</tr>\n		\n	</table>\n	 <table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;width:288px;\">\n  		<thead>\n  			<tr>\n	    		<td >\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\n	                	First\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\n	                	Last\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\n	                	Next\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\n	                	End\n	                </button>\n	            </td>\n	    	</tr>\n		</thead>\n  </table>  \n</div>";
},"useData":true});
templates['gis_storage'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<form id=\"keywordTxtForm\">\n<input id=\"keywordTxt\" class=\"txt300 floatl\" value=\"\" data-city=\"\" city-search=\"\">\n<input class=\"magnifier_button floatl\" type=\"submit\" value=\"\">\n<div class=\"clear\"></div>\n</form> ";
},"useData":true});
templates['inventory'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<option value='"
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "' data-kml='"
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "'>"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			      		<option value='"
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "'>"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"raised\">\n	<div class=\"boxcontent\">\n		Inventory\n	</div>\n</div> \n<div id=\"productStorage\" style=\"width:100%; overflow: auto;height:auto;background: #FFFFFF;\">\n			<form id=\"productStorageFrom\" class=\"form-horizontal\" role=\"form\">\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">			    \n		    	<div class=\"input-group\" style=\"width:100%;\">\n		      		<div class=\"input-group-addon\" style=\"width:100px;\">Storage:</div>\n			      	<select name=\"ps_id\" id=\"ps_id\"  class=\"gis_main_select\" style=\"width: 100%;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.storageData : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				   </select>\n			    </div>\n			</div>\n			 <div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;position:relative;\">\n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon\" style=\"width:34%;\">Title</div>\n			      <input type=\"text\" name=\"titles\" class=\"form-control\" id=\"title\" placeholder=\"Title\" >\n			      <!--<input type=\"hidden\" name=\"title_ids\" id=\"title_ids\" >-->\n			    </div>\n			    <div class=\"titleScrollbar\" style=\"width:66%;right: 0px;\">\n			    	<div class=\"titleList_\"></div>\n			    </div>\n			 </div>\n			 <div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\n				<div class=\"input-group\" >\n			      	<div class=\"input-group-addon\" style=\"width:34%;\">Lot Number</div>\n				    <input type=\"text\" name=\"lot_numbers\" class=\"form-control\" id=\"lot_numbers\" value=\"\" placeholder=\"Lot Number\">\n				 </div>\n				 <div class=\"titleScrollbar\" style=\"width:66%;right: 0px;\">\n			    	<div class=\"titleList_\"></div>\n			    </div>\n			</div>\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">			    \n		    	<div class=\"input-group\" style=\"width:100%;\">\n		      		<div class=\"input-group-addon\" style=\"width:34%;\">PDT Line</div>\n			      	<input type=\"text\" name=\"product_line\" class=\"form-control\" id=\"p_line\" value=\"\" placeholder=\"PDT Line\">\n			    </div>\n			    <div class=\"titleScrollbar\" style=\"width:66%;right: 0px;\">\n			    	<div class=\"titleList_\"></div>\n			    </div>\n			</div>\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\n				<div class=\"input-group\" style=\"width:100%;\">\n			      	<div class=\"input-group-addon\" style=\"width:100px;\">Catalog</div>\n				    <input type=\"text\" name=\"catalog\" class=\"form-control\" id=\"catalog\" value=\"\" placeholder=\"Catalog\">\n				</div>\n			</div>\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\n				<div class=\"input-group\" style=\"width:100%;\">\n			      	<div class=\"input-group-addon\" style=\"width:100px;\">PDT Name</div>\n				    <input type=\"text\" name=\"p_names\" class=\"form-control\" id=\"p_names\" value=\"\" placeholder=\"PDT Name\">\n				</div>\n			</div>\n\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\n		      	<div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon\" style=\"width:100px;\">LP Type</div>\n			      <select type=\"text\" name=\"container_type\" class=\"gis_main_select\" id=\"container_type\" placeholder=\"Item/Set\" style=\"width: 100%;\" value=\"0\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.cntDt : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			      </select>\n			      <!--<input type=\"text\" name=\"container_type\" class=\"form-control\" id=\"container_type\" placeholder=\"Container Type\" value=\"\">-->\n			    </div>\n			</div>\n			<!--<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\n		      	<div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon\" style=\"width:100px;\">Type Id</div>\n			      <input type=\"text\" name=\"type_id\" class=\"form-control\" id=\"type_id\" placeholder=\"Type Id\" value=\"\">\n			    </div>\n			</div>-->\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\n		      	<div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon\" style=\"width:100px;\">Item/Set</div>\n			      <select type=\"text\" name=\"union_flag\" class=\"gis_main_select\" id=\"union_flag\" placeholder=\"Item/Set\" style=\"width: 100%;\" value=\"0\">\n			      	<option value='-1'>All</option>\n			      	<option value='0'>Loose</option>\n			      	<option value='1'>Set</option>\n			      </select>\n			    </div>\n			</div>\n			 \n			 <div class=\"input-group\" style=\"float: right;margin-right:10px;\" >\n\n				<input type=\"button\" id=\"inquiry\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" value=\"Inquiry\"  />\n				<input type=\"hidden\"  id=\"queryDt\" >\n			 </div>\n			</form>\n</div>";
},"useData":true});
templates['locInventoryList'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			      		<option value='"
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "'>"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.dt : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"4":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<tr class=\"locInventory\"  data-index=\""
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">\n						<td  valign=\"middle\" style=\"text-align: center;\">\n							"
    + alias3(((helper = (helper = helpers.container || (depth0 != null ? depth0.container : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"container","hash":{},"data":data}) : helper)))
    + "\n						</td>\n						<td valign=\"middle\" >\n							<fieldset class=\"products_fieldset\">\n								<legend class=\"products_legend\">"
    + alias3(((helper = (helper = helpers.container || (depth0 != null ? depth0.container : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"container","hash":{},"data":data}) : helper)))
    + "</legend>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.products : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "								\n							</fieldset>\n						</td>\n					</tr>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "									<fieldset class=\"product_fieldset\" >\n										<div style=\"margin-top: 5px;width: 50px;float: left;margin-left: 5px;\">Name:</div>\n										<div style=\"margin-top:5px;\">"
    + alias3(((helper = (helper = helpers.p_name || (depth0 != null ? depth0.p_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"p_name","hash":{},"data":data}) : helper)))
    + "</div>\n										<div style=\"width: 140px;margin-left: 5px;margin-top: 5px;float: left;\">Total_locked_quantity:</div>\n										<div style=\"margin-top:5px;\">"
    + alias3(((helper = (helper = helpers.total_locked_quantity || (depth0 != null ? depth0.total_locked_quantity : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"total_locked_quantity","hash":{},"data":data}) : helper)))
    + "</div>\n										<div style=\"width: 100px;margin-left: 5px;margin-top: 5px;float: left;\">Total_quantity:</div>\n										<div style=\"margin-top:5px;\">"
    + alias3(((helper = (helper = helpers.total_quantity || (depth0 != null ? depth0.total_quantity : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"total_quantity","hash":{},"data":data}) : helper)))
    + "</div>\n										<div style=\"margin-left: 5px;margin-top: 5px;width: 90px;float: left;\">Product_line:</div>\n										<div style=\"margin-top:5px;\">"
    + alias3(((helper = (helper = helpers.product_line || (depth0 != null ? depth0.product_line : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"product_line","hash":{},"data":data}) : helper)))
    + "</div>\n\n									</fieldset>	\n";
},"7":function(depth0,helpers,partials,data) {
    return "				<tr>\n					<td colspan=\"2\">No data...</td>\n				</tr>\n			";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div id=\"locInventoryEle\" style=\"margin-left: 0px;margin-right: 0px;\">\n\n	<div id=\"closeInventoryWin\" >\n		<div class=\"windowTitle\" style=\"margin-left: 10px;padding-top: 2px;\">Inventory["
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.name : stack1), depth0))
    + "]</div>\n		<div id=\"closeWin\" >X</div>\n	</div>\n	<div id=\"inventoryQueryForm\" style=\"position: relative; margin-left:-1.02%;\">\n		<form class=\"form-inline\" >\n			<input type=\"hidden\" id=\"ps_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\"/>\n			<input type=\"hidden\" id=\"slc_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.slc_id : stack1), depth0))
    + "\"/>\n			<input type=\"hidden\" id=\"slc_type\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.slc_type : stack1), depth0))
    + "\"/>\n			<!--<div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;\">\n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon input-label\" style=\"width:100px;\">Container</div>\n			      <input type=\"text\" name=\"container\" class=\"form-control\"  id=\"container\" placeholder=\"Container\">\n			    </div>\n			 </div>-->\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;min-width:150px;\">\n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon  input-label\" style=\"width:100px;height: 34px;\">LP Type</div>\n			      <select type=\"text\" name=\"container_type\" class=\"gis_main_select\" id=\"container_type\" placeholder=\"Container Type\" style=\"width: 100%;  height: 34px;\" value=\"0\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.cntDt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			      </select>\n			      <!--<input type=\"text\" class=\"form-control\"  id=\"container_type\" placeholder=\"Container Type\" value="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.container_type : stack1), depth0))
    + ">-->\n			    </div>\n			 </div>\n			<div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;position:relative;min-width:150px;\">\n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon  input-label\" style=\"width:32%;\">Title</div>\n			      <input type=\"text\" name=\"title\" class=\"form-control\" id=\"title\" placeholder=\"Title\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.titles : stack1), depth0))
    + "\">\n			      <!--<input type=\"hidden\" id=\"title_ids\" value="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.title_ids : stack1), depth0))
    + ">-->\n			    </div>\n			    <div class=\"titleScrollbar\" style=\"width: 68%;right:0px;\">\n			    	<div class=\"titleList_\"></div>\n			    </div>\n			    \n			 </div>\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;position: relative;min-width:150px;\">\n			    \n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon  input-label\" style=\"width:32%;\">PDT Line</div>\n			      <input type=\"text\" name=\"p_line\" class=\"form-control\" id=\"p_line\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.product_line : stack1), depth0))
    + "\" placeholder=\"PDT Line\">\n			    </div>\n			    <div class=\"titleScrollbar\" style=\"width: 68%;right:0px;\">\n			    	<div class=\"titleList_\"></div>\n			    </div>\n			 </div>\n			 <div class=\"form-group\" style=\"width: 30%;margin: -5px 1.5% 5px 1.5%;min-width:150px;\">\n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon  input-label\" style=\"width:100px;\">PDT Name</div>\n			      <input type=\"text\" name=\"p_name\" class=\"form-control\" id=\"p_name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.p_names : stack1), depth0))
    + "\" placeholder=\"PDT Name\">\n			    </div>\n			 </div>\n			 <div class=\"form-group\" style=\"width: 30%;margin: -5px 1.5% 5px 1.5%;position: relative;min-width:150px;\">\n			    <div class=\"input-group\" style=\"width:100%;\">\n			      <div class=\"input-group-addon  input-label\" style=\"width:32%;\">Lot Number</div>\n			      <input type=\"text\" name=\"lot_number\" class=\"form-control\" id=\"lot_number\" placeholder=\"Lot Number\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.lot_numbers : stack1), depth0))
    + "\">\n			    </div>\n			    <div class=\"titleScrollbar\" style=\"width: 68%;right:0px;\">\n			    	<div class=\"titleList_\"></div>\n			    </div>\n			 </div>\n			 <div class=\"form-group\" style=\"width: 30%;margin: 4px 1.5% 5px 1.5%;position: relative;height: 35px;min-width:150px;\">\n			    <div class=\"input-group\" style=\"  width:110px;float: right;margin-right: 90px;margin-top: 8px;\">\n			      <div class=\"input-group-addon\" style=\"width: 40px;margin-bottom: 20px;background-color: #f2dede;border-radius: 0px;height: 15px;\"></div>\n			      <div style=\"margin-left: 10px;font-size: 12px;\">Damage</div>\n			    </div>\n			   <input type=\"button\" style=\"position: absolute;right: 1.8%;width:80px;  background: #5bc0de;border-color: #46b8da;\" id=\"queryInventory\" class=\"btn btn-info\" value=\"Query\">\n			 </div>\n		</form>\n	</div>\n	<div id=\"inventory_cont\">\n		<div id=\"inventory_model_parent\">\n			<div id=\"inventory_model\">\n				<div id=\"inventory3D\">3D</div>\n				<div id=\"inventoryList\">List</div>\n				<!--<div id=\"clearSelect3DObj\">Unselect</div>-->\n			</div>\n			\n		</div>\n		<div id=\"store_3d\" class=\"store_3d\" width=\"100%\" height=\"100%\"></div>\n		<table id=\"inventory_tab\" width=\"100%\" style=\"border-collapse: separate;width: 99%;margin-left: 0.5%;margin-top:-4px;\">\n			<tr id=\"thead1\">\n\n				<td class=\"container thead theadLeft\" rowspan=\"2\" width=\"200px\" >Container</td>\n				<td class=\"container_type thead\" rowspan=\"2\" width=\"200px\">LP</br>Type</td>\n\n\n				<td class=\"product\" colspan=\"8\" width=\"280px\" >Product</td>\n			</tr> \n			<tr id=\"thead2\"> \n				<td class=\"thead\" style=\"width:5%;\">PDT</br>Name</td>\n				<td class=\"thead\" style=\"width:5%;\">PDT</br>Line</td>\n				<td class=\"thead\" style=\"width:8%;\">Title</td>\n				<td class=\"thead\" style=\"width:5%;\">Quantity</td>\n				<td class=\"thead\" style=\"width:5%;\">Total</br>Quantity</td>\n				<td class=\"thead\" style=\"width:5%;\">Locked</br>Quantity</td>\n				<td class=\"thead\" style=\"width:5%;\">Total</br>Locked</br>Quantity</td>\n				<td class=\"sn thead\" style=\"width:5%;/*box-shadow: 1px 2px 5px #888888;*/\">SN</td>\n			</tr>\n			<tr>\n				<td colspan=\"10\" style=\"text-align: center;\">\n					<div id=\"inventory_content\">\n\n					</div>\n				</td>\n			</tr>\n			<!--"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isExistDt : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.isExistDt : depth0),{"name":"unless","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "-->\n		</table>\n	</div>\n</div>\n<style type=\"text/css\">\n #locInventoryEle{height:400px;}\n #locInventoryEle .mCSB_inside .mCSB_container{margin-right:0px;}\n #locInventoryEle .mCSB_scrollTools{right:-6px}\n.input-label{\n	/*background: #4eabb9;\n  	color: white;*/\n  	border-color: rgb(41,179,255);\n}\n</style>";
},"useData":true});
templates['loc_frequency'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n  <tr>\n    <td align=\"center\" valign=\"top\">\n		  <fieldset style=\"border:2px #cccccc solid;padding:15px;margin:10px;width:80%;\">\n		  	<form id=\"loc_frequency\">\n				<table > \n					<tr>\n						<td align=\"right\" valign=\"middle\">Interval(S)：</td>\n					    <td align=\"left\" valign=\"middle\">\n					    	<input type=\"text\" style=\"width: 100%; background: \" id=\"interval\" name=\"interval\" value=\"120\">\n			            </td>\n			   	    </tr>\n					<tr>\n						<td align=\"right\" valign=\"middle\">Batch：</td>\n					    <td align=\"left\" valign=\"middle\">\n			              <input type=\"text\" style=\"width: 100%;\" id=\"batch\" name=\"batch\" value=\"1\"/> \n			              <input type=\"hidden\"  id=\"truck_data\" />  \n			            </td>\n			   	    </tr>\n				 </table>\n		  	</form>\n		 </fieldset>\n       </td>\n    </tr>\n    <tr>\n   		<td style=\"text-align: center\"><span style=\"color: red;\" id=\"errInfo\">&nbsp;</span></td>\n	   </tr>\n   \n</table>";
},"useData":true});
templates['maintenanceTemplate'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div style=\"width:100%;margin-top: 10px;background: #F2F3A3;line-height:30px;font-size:15px;text-align:center;font-family: Georgia;\">Add    New   Layer</div>\n<div id=\"maintainLayer\"	style=\" width:100%;overflow: auto;background: white; height:100px;\"> \n	<div class=\"maintenance_div\">\n	    <div class=\"maintenance_img\">\n			<img id=\"webcam_img\" style=\"margin: 30%;\"   src=\"./imgs/webcam.png\" onmousedown=\"LayerMousedown(6,this,event)\">\n		</div>			\n		<div class=\"maintenance_b_div\">Webcam</div>\n	</div>	\n	<div class=\"maintenance_div\">\n	    <div class=\"maintenance_img\">\n			<img id=\"printer_img\" style=\"margin: 35% 10%;\"  src=\"./imgs/printer_layer.png\" onmousedown=\"LayerMousedown(7,this,event)\">\n		</div>\n		<div class=\"maintenance_b_div\">Printer</div>\n	</div>\n	<div class=\"maintenance_div\">\n	    <div class=\"maintenance_img\">\n			<img id=\"rectangle_img\" style=\"margin: 30% 0;\"  src=\"./imgs/rectangle_img.png\" onmousedown=\"LayerMousedown('x',this,event)\">\n		</div>\n		<div class=\"maintenance_b_div\">Rectangle</div>\n	</div>\n	<input type=\"hidden\" id =\"ps_id\" value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\n	<input type=\"hidden\" id =\"storageName\" value=\""
    + alias3(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"storageName","hash":{},"data":data}) : helper)))
    + "\">\n</div>\n<div style=\"width:100%;background:#F2F3A3;line-height:30px;font-size:15px;text-align:center;font-family: Georgia;\">Edit    Road</div>\n<div style=\"height:110px;width:100%;background:#fff;\">\n       <img style=\"margin: 5px 0 0 10px;\"  src=\"./imgs/draw_road.png\" onclick=\"initEditRoad("
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + ")\"/>\n       <input type='button' id=\"edit_road\" value='Edit' class='btn btn-info'style=\"margin: 5px 0 0 30px;height:30px; width:60px;font-size:9px;\" onclick=\"initEditRoad("
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + ")\"/> \n       <input type='button' id=\"reset_edit\" value='Cancel' class='btn btn-primary'style=\"margin: 5px 0 0 30px;height:30px; width:60px;font-size:9px;display:none;\" onclick=\"reSetEdit()\"/> \n       <input type='button' id=\"save\" value='Save' class='btn btn-success' style='margin: 5px 0 0 10px;height:30px; width:60px;font-size:9px' onclick='saveToDB()' disabled=\"disabled\"/>\n</div>";
},"useData":true});
templates['mapTool'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	        <div id=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"map_tool\" onclick=\"mapToolClick(this)\"\n				onmouseover=\"this.className='map_tool_selected'\"\n				onmouseout=\"if(this.getAttribute('flag')!='1'){this.className='map_tool'}\"\n				onmousedown=\"this.className='map_tool_mousedown';\"\n				onmouseup=\"this.className='map_tool_selected'\">\n				<img alt=\"clear\" src=\""
    + alias3(((helper = (helper = helpers.img || (depth0 != null ? depth0.img : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"img","hash":{},"data":data}) : helper)))
    + "\" align=\"top\">\n				<span style=\"cursor: default;font-size: 9px\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n			</div> \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.tools : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['modify_area'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<form id=\"modify_area\">\n<div class=\"area_div\"  >\n	  <div class=\"input-group form-group has-feedback area_div1\"> \n	 	<span class=\"input-group-addon\" style=\"width:44%;\">Name</span>\n	  	<input type=\"text\" class=\"form-control area_input\" id=\"zone_name\" name=\"zone_name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onblur=\"this.value=this.value.trim()\" onchange=\"verifyName(this,'"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "','5','"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "')\"> \n	  	<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n    	<input type=\"hidden\"  id=\"zone_name_old\" name=\"zone_name_old\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\">\n    	<input type=\"hidden\"  id=\"zone_id\" name=\"zone_id\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\">\n    	<input type=\"hidden\"  id=\"ps_id\" name=\"ps_id\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\">\n	  </div>\n	  <div class=\"input-group form-group has-feedback area_div1\"> \n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">X position</span>\n	  	 <input type=\"text\" class=\"form-control area_input\" id=\"x\" name=\"x\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX')\"/>\n	  	 <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \n	  </div>\n	  <div class=\"input-group form-group has-feedback area_div1\"> \n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">Y position</span>\n	  	 <input type=\"text\" class=\"form-control area_input\" id=\"y\" name=\"y\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY')\"/> \n	  	 <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\n	  </div>\n	  <div class=\"input-group form-group has-feedback area_div1\"> \n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">X length</span>\n	     <input type=\"text\" class=\"form-control area_input\" id=\"height\" name=\"height\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.height : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight')\"/>\n	     <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \n	  </div>\n	  <div class=\"input-group form-group has-feedback area_div1\"> \n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">Y length</span>\n		<input type=\"text\" class=\"form-control area_input\" id=\"width\" name=\"width\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.width : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth')\"/>\n		<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \n	  </div>\n	  <div class=\"input-group form-group has-feedback area_div1\"> \n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">Offset angle</span>\n		  <input type=\"text\" class=\"form-control area_input\" id=\"angle\" name=\"angle\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.angle : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle')\"/>\n		  <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \n	  </div>\n	  <div class=\"input-group form-group has-feedback area_div1\" style=\"display:none;\" id=\"area_subtype\"> \n	 	  <span class=\"input-group-addon\" style=\"width:44%;\">Yard type</span>\n		 	 <select id=\"area_subtype\" class=\"gis_main_select\" name=\"area_subtype\">\n		  		<option value=\"-1\">Choose...</option>\n		  		<option value=\"1\">Parking</option>\n		  		<option value=\"2\">Waiting</option>\n		  		<option value=\"3\">Empty CTN</option>\n		  		<option value=\"4\">Full CTN</option>\n		 	 </select>\n		  <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \n	  </div>\n	  <div style=\"display:none;\"> \n	  	  <span style=\"color: red;\" id=\"errInfo\">&nbsp;</span>\n	  </div>\n</div\n</form>\n";
},"useData":true});
templates['modify_light'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<form id=\"light\">\n		<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\"\n			cellspacing=\"0\">\n			<tr>\n				<td align=\"center\" valign=\"top\" colspan=\"2\">\n						<table> \n							<tr>\n								<td align=\"right\" >Name：</td>\n								<td align=\"left\" style=\"position:relative;\"><input type=\"text\"\n									style=\"width: 85%;\" name=\"name\" id=\"name\"\n									value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" onblur=\"verifyName(this)\" /><span style=\"height: 16px;width: 16px;position: absolute;top: 5px;\" id=\"vNameMsg\">\n									<img  title=\"Name is exist\" src=\"imgs/delete_red.png\" id=\"VNameError\" style=\"display: none;\">\n									<img  src=\"imgs/confirm.png\" id=\"VNameRight\" style=\"display: none;\">\n									</span>\n								</td>\n								\n							</tr>\n							<tr>\n								<td align=\"right\" >X Position：</td>\n								<td align=\"left\" style=\"position: relative;\"><input type=\"text\"\n									style=\"width: 85%;\" name=\"x\" id=\"x\"\n									value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"verifyX()\"/>\n									<span style=\"position: absolute;top: 5px;height: 16px;width:16px;\">\n										<img id=\"x_con\" src=\"imgs/confirm.png\" style=\"display: none\">\n					             		<img id=\"x_del\" title=\"Data Error!\" src=\"imgs/delete_red.png\" style=\"display: none\">\n									</span>\n								</td>\n								\n							</tr>\n							<tr>\n								<td align=\"right\" >Y Position：</td>\n								<td align=\"left\" style=\"position: relative;\"><input type=\"text\"\n									style=\"width: 85%;\" name=\"y\" id=\"y\"\n									value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"verifyY()\" />\n									<span style=\"position: absolute;top: 5px;height: 16px;width:16px;\">\n										<img id=\"y_con\" src=\"imgs/confirm.png\" style=\"display: none\">\n					             		<img id=\"y_del\" title=\"Data Error!\" src=\"imgs/delete_red.png\" style=\"display: none\">\n									</span>\n								</td>\n								\n							</tr>\n							<tr>\n								<td align=\"right\" >Status：</td>\n								<td align=\"left\" >\n								<select name=\"type\" style=\"width: 85%\">\n								<option value=\"0\">OFF </option>\n								<option value=\"1\">ON</option>\n								</select>\n								</td>\n							</tr>\n				\n						</table>\n				</td>\n			</tr>\n			<input id=\"id\" name=\"id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\n			<input id =\"ps_id\" name=\"ps_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.psId : stack1), depth0))
    + "\" />\n		</table>\n	</form>";
},"useData":true});
templates['modify_location'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div style=\"width:100%;height:auto;border-radius: 5px;background-color: #eeeeee;\">\n  	<form id=\"modify_data\">\n  		<div style=\"margin-left: 35px;padding-top: 20px;padding-bottom: 10px;width:80%;\">\n  			<div class=\"input-group form-group has-feedback\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Name：</span>\n  				<input type=\"text\" class=\"form-control\"  id=\"positionName\" name=\"positionName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onblur=\"this.value=this.value.trim();verifyName(this,'"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "','"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_type : stack1), depth0))
    + "','"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "')\" >\n          <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n  				<input type=\"hidden\"  id=\"positionName_old\" name=\"positionName_old\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\">\n			    	<input type=\"hidden\"  id=\"position_id\" name=\"position_id\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\">\n			    	<input type=\"hidden\"  id=\"ps_id\" name=\"ps_id\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\"> \n  			</div>\n  			<div class=\"input-group form-group has-feedback\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">X position：</span>\n  				<input class=\"form-control\" type=\"text\"  id=\"x\" name=\"x\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX');\"/>\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \n  			</div>\n  			<div class=\"input-group form-group has-feedback\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Y position：</span>\n  				<input class=\"form-control\" type=\"text\"  id=\"y\" name=\"y\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY');\"/> \n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \n  			</div>\n  			<div class=\"input-group form-group has-feedback\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">X length：</span>\n  				<input type=\"text\" class=\"form-control\"  id=\"height\" name=\"height\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.height : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight');\"/>\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \n  			</div>\n  			<div class=\"input-group form-group has-feedback\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Y length：</span>\n  				<input type=\"text\" class=\"form-control\"  id=\"width\" name=\"width\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.width : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth');\"/>\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>  \n  			</div>\n  			<div class=\"input-group form-group has-feedback\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Offset angle：</span>\n  				<input type=\"text\" class=\"form-control\"  id=\"angle\" name=\"angle\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.angle : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle');\"/> \n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n  			</div>\n        <div id=\"_dimensional\" class=\"input-group form-group has-feedback\" style=\"display:none;width: 98%;\">\n        <span class=\"input-group-addon\" style=\"width: 38%;\">Dimensional：</span>\n          <select class=\"gis_main_select\" name=\"is_three_dimensional\" style=\"width: 163px;\">\n            <option value=\"0\">2D</option>\n            <option value=\"1\">3D</option>\n          </select>\n          <span class=\"glyphicon form-control-feedback\"></span>\n        </div>\n  			<div class=\"input-group form-group has-feedback\" id=\"_zone\" style=\"display: none\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Zone：</span>\n  				<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('modify_data')\" onfocus=\"selectAreaSingle('modify_data')\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n				<input type=\"hidden\" name=\"area_id\" id=\"area_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\"/>\n  			</div>\n  			<div class=\"input-group form-group has-feedback\" id=\"_dock\" style=\"display: none\">\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Dock：</span>\n  				<input type=\"text\" class=\"form-control\"  name=\"dock\" id=\"dock\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.doorid : stack1), depth0))
    + "\" onfocus=\"selectDoor()\" onclick=\"selectDoor()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n				<input type=\"hidden\" name=\"sd_id\" id=\"sd_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.sd_id : stack1), depth0))
    + "\"  />\n  			</div>\n  		</div>\n		 <input type=\"hidden\" name=\"location_type\" id=\"type\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_type : stack1), depth0))
    + "\"/>\n     \n  	</form>\n				\n</div>";
},"useData":true});
templates['modify_printer'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression, alias4=this.lambda;

  return "<form id=\""
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "\"  class=\"form-horizontal\" role=\"form\">\n		<div class=\"input-group form-group has-feedback\" style=\"margin: 4px 1px;border: 1px solid #ccc;width: 100%;background: #eee;border-radius: 5px;\">\n			 <input type=\"radio\" style=\"margin:10px;\" name=\"radioType\" value=\"0\"  onclick=\"onSelect('"
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"/>IP-Printer	\n			 <input type=\"radio\" style=\"margin:10px;\" name=\"radioType\" value=\"1\" onclick=\"onSelect('"
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"/>Servers-Printer\n		  </div> \n		  <div class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\n		  	<span class=\"input-group-addon\" style=\"width:40%;\">Name:</span>\n		    <input type=\"text\" class=\"form-control area_input\" name=\"name\" id=\"name\" onchange=\"verifyName(this);\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" />\n		    <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n		  </div>\n		  \n		  <div id=\"_servers\"  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;display: none;\">\n		  	<span class=\"input-group-addon\" style=\"width:40%;\">Servers:</span>\n	  		<input type=\"text\" class=\"form-control area_input\" name=\"serversName\" id=\"servers_name\" value=\"Choose Servers!\" onClick=\"add_printerServers('','"
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"  onfocus=\"add_printerServers('','"
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "')\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\">\n	  		<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n			<input type=\"hidden\" name=\"servers\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.ps_id : depth0)) != null ? stack1.servers : stack1), depth0))
    + "\" id=\"servers_id\">\n		  </div>\n		  <div id=\"_ip\"  class=\"input-group form-group has-feedback \" style=\"margin: 4px 0;width:310px;display: none;\">\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> IP:</span>\n	  	    <input type=\"text\" class=\"form-control area_input\" name=\"ip\" id=\"ip\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ip : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyIP(this)\"/>\n	  	    <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n		  </div>\n		  <div id=\"_port\"  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;display: none;\">\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> Port:</span>\n		  	<input type=\"text\" class=\"form-control area_input\" name=\"port\" id=\"port\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.port : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyPort')\"/>\n		  	<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n		  </div>\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> X Position:</span>\n		  	<input type=\"text\" class=\"form-control area_input\" name=\"x\" id=\"x\"value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verifyX')\"/>\n		  	<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n		  </div>\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> Y Position:</span>\n	  	    <input type=\"text\" class=\"form-control area_input\" name=\"y\" id=\"y\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verifyY')\" />\n	  	    <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n		  </div>\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\n		  	<span class=\"input-group-addon\" style=\"width: 125px;\" >  Type:</span>\n  	     		<select name=\"type\"  class=\"gis_main_select\" onchange=\"setOption()\">\n					<option class=\"select-option\" value=\"0\">Label </option>\n					<option class=\"select-option\" value=\"1\">Letter/A4</option>\n				</select>\n		  </div> \n		  \n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\n		  	<span class=\"input-group-addon\" style=\"width: 125px;\"> Size:</span>\n	  	    <select name=\"size\"   class=\"gis_main_select\" >\n					<option class=\"select-option\" value='0'>60*30</option>\n					<option class=\"select-option\" value='1'>80*35</option>\n					<option class=\"select-option\" value='2'>80*40</option>\n					<option class=\"select-option\" value='3'>100*50</option>\n					<option class=\"select-option\" value='4'>120*152</option>\n			</select>\n		  </div>\n	  	  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\n	  		  <span class=\"input-group-addon\" style=\"width:40%;\"> Physical Area:</span>\n		  	  <input type=\"text\" class=\"form-control area_input\" name=\"zone\" id=\"zone\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('"
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "')\" onfocus=\"selectAreaSingle('"
    + alias3(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"  readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n		  </div>\n	    <input name=\"area_id\" id=\"area_id\" type=\"hidden\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\" />\n		<input id=\"p_id\" name=\"p_id\" type=\"hidden\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.p_id : stack1), depth0))
    + "\" />\n		<input id =\"ps_id\" name=\"ps_id\" type=\"hidden\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\" />\n	</div>\n</form>";
},"useData":true});
templates['modify_storage_layer'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"modify_storage_layer_div\">\n	<div class=\"btn-group\" data-toggle=\"buttons\" style=\"margin-bottom: 5px;border-bottom: 1px solid #ddd;width: 100%;\">\n	  <label class=\"btn active\">\n	    <input type=\"radio\" name=\"options\" id=\"singleLayer\" value=\"0\" autocomplete=\"off\" checked=\"checked\"> Single layer\n	  </label>\n	  <label class=\"btn\">\n	    <input type=\"radio\" name=\"options\" id=\"multiLayer\" value=\"1\" autocomplete=\"off\"> Multi layer\n	  </label>\n	</div>\n	<form id=\"modify_storage_layer\">\n		<input type=\"hidden\" id =\"storage_id\" name=\"ps_id\" value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" name=\"storage_name\" id =\"storage_name\" value=\""
    + alias3(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"storageName","hash":{},"data":data}) : helper)))
    + "\">\n		<div class=\"layerTypeRadio\" style=\"width: 98%;height: 34px;margin-bottom: 5px;border-radius: 5px;border: 1px solid #ccc;\">\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"1\" onclick=\"typeChange(1)\">Location</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"2\" onclick=\"typeChange(2)\">Staging</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"3\" onclick=\"typeChange(3)\">Dock</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"4\" onclick=\"typeChange(4)\">Parking</div>\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"5\" onclick=\"typeChange(5)\" >Zone</div>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Name：</span>\n			<input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" value=\"\" onchange=\"this.value=this.value.trim();verifyName_(this,'"
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "','','','add')\" />\n			<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\n		</div> \n		\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X Position：</span>\n			<input type=\"text\" class=\"form-control\"  name=\"x\" id=\"x\" value=\"\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y Position：</span>\n			<input type=\"text\" class=\"form-control\" name=\"y\" id=\"y\" value=\"\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X length：</span>\n			<input type=\"text\" class=\"form-control\" name=\"height\" id=\"height\" value=\"50\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y length：</span>\n			<input type=\"text\" class=\"form-control\" name=\"width\" id=\"width\" value=\"50\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Offset angle：</span>\n			<input type=\"text\" class=\"form-control\" name=\"angle\" id=\"angle\" value=\"0\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle')\" readonly=\"readonly\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<div id=\"_title\" class=\"input-group form-group has-feedback\" style=\"display: none;margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Title：</span>\n			<input type=\"text\" class=\"form-control\"  name=\"title\" id=\"title\" value=\"\" onclick=\"addLayerSelectTitle()\" onfocus=\"addLayerSelectTitle()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n			<input type=\"hidden\" style=\"width: 80%;\" name=\"title_id\" id=\"title_id\" value=\"\" />\n		</div>\n		<div id=\"_zone\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Zone：</span>\n			<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\"\" onclick=\"selectAreaSingle('modify_storage_layer')\" onfocus=\"selectAreaSingle('modify_storage_layer')\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n			<span class=\"glyphicon form-control-feedback\" ></span>\n			<input type=\"hidden\" style=\"width: 80%;\" name=\"area_id\" id=\"area_id\" value=\"\"/>\n		</div>\n		<div id=\"_dock\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Dock：</span>\n			<input type=\"text\" class=\"form-control\" name=\"dock\" id=\"dock\" value=\"\" onclick=\"addLayerSelectDoor()\" onfocus=\"addLayerSelectDoor()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\n			<span class=\"glyphicon form-control-feedback\"></span>\n			<input type=\"hidden\" style=\"width: 80%;\" name=\"sd_id\" id=\"sd_id\" value=\"\"  />\n		</div>\n		<div id=\"_dimensional\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Dimensional：</span>\n			<select class=\"gis_main_select\" name=\"is_three_dimensional\" style=\"width: 210px;\">\n				<option value=\"0\">2D</option>\n				<option value=\"1\">3D</option>\n			</select>\n			<span class=\"glyphicon form-control-feedback\"></span>\n		</div>\n		<!--<div id=\"set_l\">\n			<input type=\"checkbox\" style=\"margin-left: 33%;\" name=\"set_local\" value=\"setLocation\" onclick=\"setLocation()\"><span class=\"input-group-add\">Draw Child Layer</span>\n		</div>-->\n		<div id=\"set_location\" style=\"display:none;\">\n			<div class=\"childrenLayerType\" style=\"width:100%;height:25px;\">\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"1\" checked=\"checked\">Location</div>\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"2\">Staging</div>\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"3\">Dock</div>\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"4\">Parking</div>\n			</div>\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">vertically:</span>\n				<input type=\"text\" class=\"form-control\" value=\"\" id=\"vertically\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVer')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\n				<span class=\"glyphicon form-control-feedback\"></span>\n			</div>\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">horizontally:</span>\n				<input type=\"text\" class=\"form-control\" value=\"\" id=\"horizontally\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHor')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\n				<span class=\"glyphicon form-control-feedback\"></span>\n			</div>\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Loc Name:</span>\n				<input type=\"text\" class=\"form-control\" value=\"\" id=\"loc_name\"   onchange=\"this.value=this.value.trim()\" >\n				<span class=\"glyphicon form-control-feedback\"></span>\n			</div>\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\n				<span class=\"input-group-addon\" style=\"width: 38%;\">V_Interval:</span>\n				<input type=\"text\" class=\"form-control\" value=\"0\" id=\"v_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVi')\" >\n				<span class=\"glyphicon form-control-feedback\"></span>\n			</div>\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\"> \n				<span class=\"input-group-addon\" style=\"width: 38%;\">H_Interval:</span>\n				<input type=\"text\" class=\"form-control\" value=\"0\" id=\"h_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHi')\"  >\n				<span class=\"glyphicon form-control-feedback\"></span>\n			</div>\n			<div style=\"margin-bottom: 5px;float: right;\">\n				<input type=\"button\" class=\"btn btn-success btn-sm\" value=\"Create\"   onclick=\"createLocation()\" >\n				<input type=\"button\" class=\"btn btn-info btn-sm\" value=\"Clear\" onclick=\"clearAutoLayer()\"> \n			</div>\n		</div>\n	</form>\n</div>\n<style type=\"text/css\">\n .modify_storage_layer_div .mCSB_inside .mCSB_container{margin-right:0px;}\n .modify_storage_layer_div .mCSB_scrollTools{right:-6px}\n\n</style>";
},"useData":true});
templates['printerServers'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.p_serviers_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    	 	 					<tr class=\"choicePrinterServer\" data-serverId=\""
    + alias3(((helper = (helper = helpers.printer_server_id || (depth0 != null ? depth0.printer_server_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_id","hash":{},"data":data}) : helper)))
    + "\" data-severName=\""
    + alias3(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "\">\n    	 	 						<td title=\""
    + alias3(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "</td>\n    	 	 						<td title=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\n    	 	 						<td title=\""
    + alias3(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_name","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_name","hash":{},"data":data}) : helper)))
    + "</td>\n     	 	 						<td style=\"text-align: center;\">\n     	 	 							<input type=\"checkbox\" >\n    	 	 							\n    	 	 							&nbsp;\n    	 	 						</td>\n    	 	 					</tr> \n";
},"4":function(depth0,helpers,partials,data) {
    return "    			<tr>\n              <td colspan=\"4\" style=\"text-align:center;line-height:155px;height:155px;background:#e8edff;border:1px solid silver;\">No Records</td>      \n          </tr>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "     <table class=\"table table-condensed no-margin bg-info\" style=\"position: absolute;top:197px;\">\n      <thead>\n          <tr>\n            <td>\n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n                      <span class=\"glyphicon glyphicon-step-backward\"></span>\n                      首页\n                    </button>&nbsp;&nbsp; \n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n                      <span class=\"glyphicon glyphicon-chevron-left\"></span>\n                      上一页\n                    </button>&nbsp;&nbsp; \n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n                      <span class=\"glyphicon glyphicon-chevron-right\"></span>\n                      下一页\n                    </button>&nbsp;&nbsp; \n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageno=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n                      <span class=\"glyphicon glyphicon-step-forward\"></span>\n                      末页\n                    </button>\n                </td>\n                <td width=\"200px\">\n                    <span class=\"pull-right small\">\n                      当前："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\n                      页数："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\n                      总数："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;  \n                    </span>\n            </td>\n          </tr>\n    </thead>\n  </table>  \n";
},"7":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return " 	<div style=\"position: relative;\">  \n    <span class=\"btn btn-success fileinput-button\" onclick=\"addPrinterServer();\">\n  	 	 	<i class=\"icon-plus icon-white\"></i>\n			AddPrintServer\n		</span>\n    <div style=\"height:152px;background-color: #e8edff;\">\n    <table  class=\"table table-hover printer_servers\" style=\"margin-bottom: 0px;margin-left: 0;\">\n	    <thead> \n	        <th scope=\"col\">Server Name</th>\n  	    	<th scope=\"col\" style=\"width:20%;\">Storage</th> \n  	    	<th scope=\"col\">Area Name</th> \n  	     	<th scope=\"col\" style=\"width:15%;\">Opera</th> \n	    </thead> \n	   <tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isExistData : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.isExistData : depth0),{"name":"unless","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	   </tbody>\n  	 </table>\n     </div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isExistData : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n";
},"useData":true});
templates['printer_server_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.p_serviers_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "      <tr style=\"border-bottom: 1px solid #ddd;\">\n         <td style=\"padding: 2px;text-align: center;\" title="
    + alias3(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + ">"
    + alias3(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "</td>\n         <td style=\"padding: 2px;text-align: center;\">"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\n         <td style=\"padding: 2px;text-align: center;\" title="
    + alias3(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_name","hash":{},"data":data}) : helper)))
    + ">"
    + alias3(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_name","hash":{},"data":data}) : helper)))
    + "</td>\n         <td style=\"padding: 2px;text-align: center;\"><img style=\"width:40px;\" src=\"./imgs/print_delete.png\" onclick=\"dialogDeletePServer("
    + alias3(((helper = (helper = helpers.printer_server_id || (depth0 != null ? depth0.printer_server_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_id","hash":{},"data":data}) : helper)))
    + ",'"
    + alias3(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "')\"/></td>\n      </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "		<tr>\n		  <td style=\"padding: 2px;text-align: center;\" colspan=\"4\">\n			No Records		\n		  </td>	\n		</tr>\n";
},"6":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression;

  return "<div style=\"background: white;height: 280px;\">\n<div style=\"width:100%;height:35px; float:left;\">\n	<input style=\"margin-left:1%;\" type=\"button\" class='btn btn-success' value=\"Add\" onclick=\"addPrinterServer("
    + alias1(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + ")\" />\n</div>\n<div style=\"width:100%;overflow-y:auto;float:left;position: relative;height:200px;\">\n<table class=\"table table-hover\" style=\"margin-bottom: 0px;position:relative;\">\n  <thead>\n     <tr>\n         <th style=\"width:28%;text-align: center;padding: 2px;font-size:12px;\">Name</th>\n         <th style=\"width:26%;text-align: center;padding: 2px;font-size:12px;\">Storage</th>\n         <th style=\"width:26%;text-align: center;padding: 2px;font-size:12px;\">Zone</th>\n         <th style=\"width:20%;text-align: center;padding: 2px;font-size:12px;\">Opera</th>\n     </tr>\n  </thead>\n <tbody> \n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isExistData : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.isExistData : depth0),{"name":"unless","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "   </tbody>\n</table>\n <table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;top: 155px;\">\n  	<thead>\n  			<tr>\n	    		<td width=\"100%\">\n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\n	                	First\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\n	                	Last\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\n	                	Next\n	                </button>&nbsp;&nbsp; \n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\""
    + alias1(this.lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\n	                	End\n	                </button>\n	            </td>\n	    	</tr>\n	</thead>\n  </table>  \n</div>\n</div>";
},"useData":true});
templates['query_history'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							    		<option value='"
    + alias3(((helper = (helper = helpers.val || (depth0 != null ? depth0.val : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"val","hash":{},"data":data}) : helper)))
    + "'>"
    + alias3(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"text","hash":{},"data":data}) : helper)))
    + "</option> \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n		  <tr> \n		    <td colspan=\"2\" align=\"center\" valign=\"top\">\n				  <fieldset style=\"border:2px #cccccc solid;padding:15px;margin:10px;width:90%;\">\n						<table >   \n							<tr style=\"display: none;\">\n								<td align=\"right\" valign=\"middle\">Time zone：</td>\n							    <td align=\"left\" valign=\"middle\">\n							    	<select class=\"txt\" style=\"width: 100%;\" id=\"timeZone\" name=\"timeZone\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							    	</select>\n					            </td>\n					   	    </tr>\n							<tr>\n								<td align=\"right\" valign=\"middle\" width=\"60px;\">Begin：</td>\n							    <td align=\"left\" valign=\"middle\">\n					              <input type=\"text\" readonly class=\"form-datatime form-control\" id=\"startTime\" name=\"startTime\" style=\"cursor: pointer;\n\" /> \n					            </td>\n					   	    </tr>\n					   	    <tr>\n					   	    	<td align=\"right\" valign=\"middle\">End： </td>\n					                  		  \n					            <td align=\"left\" valign=\"middle\">     		  \n					              <input type=\"text\" readonly class=\"form-datatime form-control\" id=\"endTime\" name=\"endTime\"\" style=\"cursor: pointer;\n\"/>\n					              <input type=\"hidden\" name=\"aid\" id=\"truck_aid\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.aid || (depth0 != null ? depth0.aid : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"aid","hash":{},"data":data}) : helper)))
    + "\"/>\n					            </td>\n						   </tr>\n						 </table>\n					  </fieldset>\n	           </td>\n	        </tr>\n	        <tr>\n		   	      <td colspan=\"2\" style=\"vertical-align: middle;\">\n		   	   		<span id=\"queryTip\" style=\"color:red;\"></span>\n		   	   	  </td>\n	   	   </tr>\n		   \n	   </table>";
},"useData":true});
templates['rightclick_menus'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		<li id=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"map_rightClick\"  style=\"display: none;\" data-id=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n		<a href=\"#\" data-id=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a></li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<ul>\n	<li id=\"xy\" style=\"display: none;\"></li>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</ul>\n\n			";
},"useData":true});
templates['search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<input id=\"keywordTxt\" class=\"txt300 floatl\" value=\"\">\n<input class=\"magnifier_button floatl\" type=\"submit\" value=\"\">\n<a  id =\"return\" style=\"color: rgb(103, 103, 103); text-align: center;\" >返&nbsp;&nbsp;回</a> \n";
},"useData":true});
templates['selectDoor'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.exit_dock_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<div style=\"width: 110px; height: 15px; border: 1px solid rgb(204, 204, 204); margin: 5px; float: left; cursor: default;\" title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\">\n								<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'sd_id')\">\n									<input type=\"hidden\" id=\"sd_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n								"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "</div>\n								<div style=\"width: 12px; height: 12px; float: right; margin: 1px; cursor: pointer; background-image: url(http://localhost/Sync10-ui/pages/gis/imgs/delete_grey.png);\" onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\" onclick=\"removeTitle_(this.parentNode,'sd_id')\">\n								</div>\n							</div>\n\n								<!--<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,"
    + alias3(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adid","hash":{},"data":data}) : helper)))
    + ")\">\n										"
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\n										<input type=\"hidden\" id=\"title_id\" value=\""
    + alias3(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\n									</div>\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n											onclick=\"removeTitle_(this.parentNode,"
    + alias3(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adid","hash":{},"data":data}) : helper)))
    + ")\">\n									</div>\n								</div>-->\n";
},"4":function(depth0,helpers,partials,data) {
    return "							<center>\n								no data\n							</center>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 105px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n						<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode,'sd_id')\">\n							"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n							<input type=\"hidden\" id=\"sd_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n						</div>\n						<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n									onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n									onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n									onclick=\"addTitle(this.parentNode,'sd_id')\">\n						</div>\n					</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"about_the_layout_div\">\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Docks</div> \n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Docks</div>\n	  <div class=\"about_the_layout_div_centext margin_right\">\n			<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.exit_dock_data : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.exit_dock_data : depth0),{"name":"unless","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n		</div>	\n	    <div  class=\"about_the_layout_div_centext margin_left\">\n			<div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.door_data : depth0),{"name":"each","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n	 </div>\n</div>\n	<!-- model -->\n	<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n		<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'sd_id')\">\n			<input type=\"hidden\" id=\"sd_id\">\n		</div>\n		<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n				onclick=\"removeTitle_(this.parentNode,'sd_id')\">\n		</div>\n	</div>\n	<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n		<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\n			<input type=\"hidden\" id=\"sd_id\">\n		</div>\n		<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n					onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n					onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n					onclick=\"addTitle(this.parentNode)\">\n		</div>\n	</div>\n	<!-- model end-->\n	";
},"useData":true});
templates['selectPerson'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<div title=\""
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'title_id')\">\n										"
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\n										<input type=\"hidden\" id=\"title_id\" value=\""
    + alias3(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\n									</div>\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n											onclick=\"removeTitle_(this.parentNode,'title_id')\">\n									</div>\n								</div>\n";
},"3":function(depth0,helpers,partials,data) {
    return "							<center>\n								no data\n							</center>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<div title=\""
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n						<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode,'title_id')\">\n							"
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\n							<input type=\"hidden\" id=\"title_id\" value=\""
    + alias3(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\n						</div>\n						<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n									onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n									onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n									onclick=\"addTitle(this.parentNode,'title_id')\">\n						</div>\n					</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"about_the_layout_div\">\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Person</div> \n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Person</div>\n	  <div class=\"about_the_layout_div_centext margin_right\">\n	  	  		<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n					\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.exit_title_data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.exit_title_data : depth0),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			 </div>\n	  </div>\n      <div  class=\"about_the_layout_div_centext margin_left\">\n		<div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.person_data : depth0),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n	 </div>\n</div>\n<!-- model -->\n<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n	<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'title_id')\">\n		<input type=\"hidden\" id=\"title_id\">\n	</div>\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n			onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n			onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n			onclick=\"removeTitle_(this.parentNode,'title_id')\">\n	</div>\n</div>\n<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n	<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\n		<input type=\"hidden\" id=\"title_id\">\n	</div>\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n				onclick=\"addTitle(this.parentNode)\">\n	</div>\n</div>\n<!-- model end-->";
},"useData":true});
templates['select_area'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.area_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "							<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n								<div class=\"text-overflow location_updata_area\" style=\"width: 85%; float: left; \" >\n									"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n									<input type=\"hidden\" id=\"area_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n								</div>\n								<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" class=\"location_updata_area\"\n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n											>\n								</div>\n							</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "						<center>\n							no data\n						</center>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px; width:650px;height:420px;\">\n	    <table style=\"width: 98%;margin:0 1%;height:auto;\">\n	    	<tr> \n	    		<td>\n					<div id=\"notExitDoor\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:400px; overflow:auto\" >\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.dataIsNull : depth0),{"name":"unless","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.dataIsNull : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n				</td>\n	    	</tr>\n	    </table>\n	</div>";
},"useData":true});
templates['select_area_single'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n\n						<div title=\""
    + alias3(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n							<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"confirm1(this.parentNode)\">\n								"
    + alias3(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_name","hash":{},"data":data}) : helper)))
    + "\n								<input type=\"hidden\" id=\"area_id\" value=\""
    + alias3(((helper = (helper = helpers.area_id || (depth0 != null ? depth0.area_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_id","hash":{},"data":data}) : helper)))
    + "\">\n							</div>\n							<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n										onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n										onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n										onclick=\"confirm1(this.parentNode)\">\n							</div>\n						</div>\n										\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:450px;height:400px;\">\n	    <table style=\"width: 100%;\">\n	    	<tr> \n	    		<td>\n					<div id=\"notExitDoor\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.area_data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n					</div>\n				</td>\n	    	</tr>\n	    </table>\n	</div>";
},"useData":true});
templates['select_door_single'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.door_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n									<div class=\"text-overflow location_updata_doors\" style=\"width: 85%; float: left; \" >\n										"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n										<input type=\"hidden\" id=\"sd_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n									</div>\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n												onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n												onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n												class=\"location_updata_door_add_img\"\n												>\n									</div>\n								</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "							<center>\n								no data\n							</center>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:760px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;\">\n	    <table style=\"width: 100%;\">\n	    	<tr> \n	    		<td>\n					<div id=\"notExitDoor\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.doorDataIsNull : depth0),{"name":"unless","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.doorDataIsNull : depth0),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n				</td>\n	    	</tr>\n	    </table>\n	</div>";
},"useData":true});
templates['select_title'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.exit_title_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,"
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\n										"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n										<input type=\"hidden\" id=\"title_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n									</div>\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n											onclick=\"removeTitle_(this.parentNode,"
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\n									</div>\n								</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "							<center>\n								no data\n							</center>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.notExit_title : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"7":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n									<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n										<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\n											"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n											<input type=\"hidden\" id=\"title_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n										</div>\n										<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n													onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n													onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n													onclick=\"addTitle(this.parentNode)\">\n										</div>\n									</div>\n";
},"9":function(depth0,helpers,partials,data) {
    return "						<center>\n							no data\n						</center>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"about_the_layout_div\">\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Title</div> \n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Title</div>\n	  <div class=\"about_the_layout_div_centext margin_right\">\n	  		<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.exit_title_data : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.exit_title_data : depth0),{"name":"unless","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			 </div>\n	  </div>		 \n	  <div  class=\"about_the_layout_div_centext margin_left\">\n			 <div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.notExit_title : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.notExit_title : depth0),{"name":"unless","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n	 </div>\n</div>\n<!-- model -->\n<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n	<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'title_id')\">\n		<input type=\"hidden\" id=\"title_id\">\n	</div>\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n			onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n			onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n			onclick=\"removeTitle_(this.parentNode,'title_id')\">\n	</div>\n</div>\n<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n	<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\n		<input type=\"hidden\" id=\"title_id\">\n	</div>\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n				onclick=\"addTitle(this.parentNode)\">\n	</div>\n</div>\n<!-- model end-->\n";
},"useData":true});
templates['select_zone'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.exitZone_data : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "								<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,"
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\n										"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n										<input type=\"hidden\" id=\"area_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n									</div>\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n											onclick=\"removeTitle_(this.parentNode,"
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\n									</div>\n								</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "							<center>\n								no data\n							</center>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.zone_data : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"7":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n								<div title=\""
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n									<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\n										"
    + alias3(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\n										<input type=\"hidden\" id=\"area_id\" value=\""
    + alias3(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\n									</div>\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n												onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n												onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n												onclick=\"addTitle(this.parentNode)\">\n									</div>\n								</div>\n";
},"9":function(depth0,helpers,partials,data) {
    return "					<center>\n						no data\n					</center>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"about_the_layout_div\">\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Zone</div> \n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Zone</div>\n	  <div class=\"about_the_layout_div_centext margin_right\">\n					<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.exitZone : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.exitZone : depth0),{"name":"unless","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</div>\n		  </div>	\n		  <div  class=\"about_the_layout_div_centext margin_left\">\n				<div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.zoneDataIsNotExit : depth0),{"name":"unless","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.zoneDataIsNotExit : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			  </div>\n		</div>\n</div>\n<!-- model -->\n<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n	<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode)\">\n		<input type=\"hidden\" id=\"area_id\">\n	</div>\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \n			onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \n			onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\n			onclick=\"removeTitle_(this.parentNode)\">\n	</div>\n</div>\n<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\n	<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\n		<input type=\"hidden\" id=\"area_id\">\n	</div>\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\n				onclick=\"addTitle(this.parentNode)\">\n	</div>\n</div>\n<!-- model end-->";
},"useData":true});
templates['set_refresh_time'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<form id=\"setTime\">\n		<table border=\"0\" style=\"width: 100%;height: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n		<tr> \n		<td align=\"right\" width=\"40%\">\n		Truck Refresh Time:\n		</td>\n		<td align=\"left\" width=\"60%\">\n		 <input type=\"text\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.timeData : depth0)) != null ? stack1.truck : stack1), depth0))
    + "\" id=\"truck_time\" style=\"width: 60%\" onchange=\"this.value=this.value.trim();verifyTt()\"/>\n		 <span style=\"color: red;\">(ms)</span>\n		 <img id=\"truck_time_con\" src=\"imgs/confirm.jpg\" style=\"display: none\">\n         <img id=\"truck_time_del\" title=\"Data Error!\" src=\"imgs/delete_red.jpg\" style=\"display: none\">\n		</td>\n		</tr>\n		<tr>\n		<td align=\"right\" width=\"40%\">\n		Dock Refresh Time:\n		</td>\n		<td align=\"left\" width=\"60%\">\n		<input type=\"text\" id=\"dock_time\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.timeData : depth0)) != null ? stack1.dock : stack1), depth0))
    + "\" style=\" width: 60%\" onchange=\"this.value=this.value.trim();verifyDt()\" />\n		 <span style=\"color: red;\">(ms)</span> \n		 <img id=\"dock_time_con\" src=\"imgs/confirm.jpg\" style=\"display: none\">\n         <img id=\"dock_time_del\" title=\"Data Error!\" src=\"imgs/delete_red.jpg\" style=\"display: none\">\n		</td>\n		</tr>\n		</table>\n	</form>";
},"useData":true});
templates['showWebcam'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<embed type=\"application/x-vlc-plugin\" id=\"vlc\"\n			target=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.rtsp : stack1), depth0))
    + "\"\n			 height=\"100%\" width=\"100%\"\n			 autoplay=\"yes\" loop=\"no\"\n			 version=\"VideoLAN.VLCPlugin.2\"/>\n	  <div id=\"playerInfo\" style=\"display: none;\" align=\"center\" >\n	  	<span>Not install VLC player plugin</span>\n	  	</br>\n	  	<a href=\"javascript:\" onclick=\"getVlcPlayer()\">Get VLC media player</a>\n	  </div> ";
},"useData":true});
templates['showZoneDocks'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					<div title=\""
    + alias3(((helper = (helper = helpers.doorid || (depth0 != null ? depth0.doorid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"doorid","hash":{},"data":data}) : helper)))
    + "\" class=\"zone_dock_div_2\">\n						<div class=\"zone_dock_div_3\" oncontextmenu=\"showMenu(this,event)\">\n							"
    + alias3(((helper = (helper = helpers.doorid || (depth0 != null ? depth0.doorid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"doorid","hash":{},"data":data}) : helper)))
    + "\n							<input type=\"hidden\" id=\"sd_id\" name=\"sdid\" value=\""
    + alias3(((helper = (helper = helpers.sd_id || (depth0 != null ? depth0.sd_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"sd_id","hash":{},"data":data}) : helper)))
    + "\">\n							<div class=\"zone_dock_div_img\"\n							onclick=\"removeTitle(this.parentNode,'dock_names')\">\n							</div>\n						</div> \n					</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"showZoneDocks\" class=\"zone_dock_div\" tabindex='0'>\n		<input type=\"hidden\" id=\"areaName\" value=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"olddock_Ids\" value=\""
    + alias3(((helper = (helper = helpers.olddock_Ids || (depth0 != null ? depth0.olddock_Ids : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"olddock_Ids","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"dock_names\" value=\""
    + alias3(((helper = (helper = helpers.dock_names || (depth0 != null ? depth0.dock_names : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"dock_names","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"ps_id\" value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"area_id\" value=\""
    + alias3(((helper = (helper = helpers.area_id || (depth0 != null ? depth0.area_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_id","hash":{},"data":data}) : helper)))
    + "\">\n		<div id=\"notExitDoor\" class=\"zone_dock_div_1\" >\n				\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.docks_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				<div title=\"add dock\" id=\"add_dock\" style=\"width: 100px; height: 25px; border: solid; border-color: #eeeeee; border-width: 1px; margin: 5px; float: left; cursor: default;\" >\n						<div class=\"text-overflow\" style=\"width: 100%;height:100%; float: left; text-align: center;background-color: #eeeeee;cursor: pointer;\" onclick=\"selectDock()\">\n							<div id=\"add_bg_img\" style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_blue.png'); margin: 1px; cursor: pointer;margin-top: 8px;margin-right: 50px;\" \n									onmouseover=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\" \n									onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\"\n									\">\n							</div>\n						</div>\n				</div>\n		</div>\n</div>";
},"useData":true});
templates['show_zone_person'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "				<div title=\""
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\" class=\"zone_person_div_2\" >\n					<div class=\"zone_dock_div_3\" oncontextmenu=\"showPersonMenu(this,event)\">\n						"
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\n						<input type=\"hidden\" class=\"adid\" name=\"adid\" value=\""
    + alias3(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\n						<input type=\"hidden\" class=\"ad_name\" name=\"ad_name\" value=\""
    + alias3(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\">\n						<div class=\"zone_dock_div_img\" onclick=\"removeTitle(this.parentNode,'employe_names')\">\n						</div>\n					</div>\n				</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"zonePersons\" onclick=\"hidePersonMenu()\" class=\"zone_person_div\" >\n		<input type=\"hidden\" id=\"areaName\" value=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"adids\" value=\""
    + alias3(((helper = (helper = helpers.adids || (depth0 != null ? depth0.adids : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"adids","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"ps_id\" value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"area_id\" value=\""
    + alias3(((helper = (helper = helpers.area_id || (depth0 != null ? depth0.area_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"area_id","hash":{},"data":data}) : helper)))
    + "\">\n		<input type=\"hidden\" id=\"employe_names\" value=\""
    + alias3(((helper = (helper = helpers.employe_names || (depth0 != null ? depth0.employe_names : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"employe_names","hash":{},"data":data}) : helper)))
    + "\">\n		<div id=\"notExitDoor\" class=\"zone_person_div_1\" >\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.zonePerson_data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			<div title=\"add person\" id=\"add_person\" style=\"width: 130px; height: 25px; border: solid; border-color: #eeeeee; border-width: 1px; margin: 5px 14px; float: left; cursor: default;\" >\n				<div class=\"text-overflow\" style=\"width: 100%;height:100%; float: left; text-align: center;background-color: #eeeeee;cursor: pointer;\" onclick=\"selectPerson()\">\n					<div id=\"add_bg_img\" style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_blue.png'); margin: 1px; cursor: pointer;margin: 6px 55px 0px 0px;\" \n							onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\" \n							onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\"\n							\">\n					</div>\n				</div>\n			</div>\n		</div>\n</div>\n		<!-- 右键菜单样式 -->\n		<div id=\"menu_\" style=\"width:auto;\">\n			<ul>\n				<li id=\"assignArea\" onclick=\"assignArea(this)\" >\n				<a href=\"#\">Reassign </a></li>\n			</ul>\n		</div>";
},"useData":true});
templates['storageSecondMenus'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        <div class=\"category_container category_container_Storage\" data-ps_id=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n        <div class=\"storageSecondMenusBtn\" data-ps_id=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n			<div class=\"category_icon_seconde "
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\" data-ps_id=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n			</div>\n			"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n		</div> \n		</div>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "		   <div id=\"second_boxConR_storage\" data-pageplus=\"1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConR second_boxCon_storage\">\n		   </div>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "		   <div id=\"second_boxConL_stroage\" data-pageplus=\"-1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConL second_boxCon_storage\">\n		   </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "     <div class=\"category_icon_collection\">\n     <div id=\"storageSecondMenuCon\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.menus_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		<div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pageplus_R : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pageplus_L : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n	 </div>\n                       \n\n\n";
},"useData":true});
templates['storageSencondTabs'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id=\"storage_tabs\">\n	<div style=\"height: 20px;width: 98%;background: #aaf;margin: 0 1%;\">\n		<ul>\n			<li style=\"width: 50%;\">\n				<a href=\"#storage\">Storage</a>\n			</li>\n			<li style=\"width: 50%;\">\n				<a href=\"#alterStorage\">Edit Storage</a>\n			</li>\n		</ul> \n	</div>\n	<div id=\"storage\" style=\"width:100%\">\n		<div id=\"all_storage\"></div>\n		<div id=\"bottom_menus\"></div>\n	</div>\n	<div id=\"alterStorage\" style=\"width:100%;\"></div>\n</div>";
},"useData":true});
templates['storageThirdlyMenus'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "      	<div class=\"storage_third_menus\"  data-ps_id=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-layer=\""
    + alias3(((helper = (helper = helpers.layer || (depth0 != null ? depth0.layer : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"layer","hash":{},"data":data}) : helper)))
    + "\">\n      		<input type=\"checkbox\" id=\""
    + alias3(((helper = (helper = helpers.layer || (depth0 != null ? depth0.layer : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"layer","hash":{},"data":data}) : helper)))
    + "\" class=\"storage_layer_checkbox\" data-ps_id=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-layer=\""
    + alias3(((helper = (helper = helpers.layer || (depth0 != null ? depth0.layer : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"layer","hash":{},"data":data}) : helper)))
    + "\">&nbsp;&nbsp;\n			<span style='vertical-align : middle; color: #333333' data-ps_id=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + alias3(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span> \n      	</div> \n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "		   <div id=\"thirdly_boxConR\" data-pageplus=\"1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base thirdly_boxConR\">\n		   </div>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "		   <div id=\"thirdly_boxConL\" data-pageplus=\"-1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base thirdly_boxConL\">\n		   </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n     <div class=\"category_icon_collection_third\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.menus_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pageplus_R : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pageplus_L : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n	 </div>\n\n";
},"useData":true});
templates['three_level_tabs'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<li>\n				<a href=\"#"
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" >\n				"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n				</a>\n			</li>	\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "		    <div id=\""
    + this.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"width:98%;margin: 0 1%;\">\n		    </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"tabs\">\n	<div style=\"height:20px;width:98%;background:#aaf;margin:0 1%\">\n	<ul>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</ul> \n	</div>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['truckMarkerRightMenu'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<ul class=\"truckMarkerMenu_ul\" >\n    <li class=\"truckMarkerMenu_li\" id=\"History\" >Tracking History\n    </li>\n    <li class=\"truckMarkerMenu_li\" id=\"LocFrequency\" >Positioning Frequency Set Up\n    </li>\n    <li class=\"truckMarkerMenu_li\" id=\"KeyPoint\" >Key Point Set Up\n    </li>\n    <li class=\"truckMarkerMenu_li\" id=\"Road\" >Route Set Up\n    </li>\n    \n    <li class=\"truckMarkerMenu_li\" id=\"GeoFence\" >Geofencing Set Up\n    </li> \n</ul>\n";
},"useData":true});
templates['truckTemplate'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "	        <div class=\"category_container category_container_truck\" data-name=\""
    + alias3(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" data-group_id=\""
    + alias3(((helper = (helper = helpers.group_id || (depth0 != null ? depth0.group_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_id","hash":{},"data":data}) : helper)))
    + "\" >\n	        	<div class=\"truck_btn\">\n					<div class=\"category_icon_seconde truck_second_menu "
    + alias3(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" data-group_id=\""
    + alias3(((helper = (helper = helpers.group_id || (depth0 != null ? depth0.group_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_id","hash":{},"data":data}) : helper)))
    + "\" ></div>\n					"
    + alias3(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_name","hash":{},"data":data}) : helper)))
    + "\n				</div> \n			</div>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "			   <div id=\"truck_second_boxConR\" data-pageplus=\"1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConR\">\n			   </div>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "			   <div id=\"truck_second_boxConL\" data-pageplus=\"-1\" data-num=\""
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConL\">\n			   </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n	    <div style=\"width:98%;margin: 0 1%;height:20px;background: #ccc;\">\n		  <div class=\"boxcontent raised floatl\">Truck </div>\n		  </div>\n	     <div class=\"category_icon_collection_truck_second\" style=\"position: relative;\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.menus_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pageplus_R : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pageplus_L : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			<div style=\"float: left; width: 96%;bottom: 5px;position: absolute;\">\n				<input type=\"button\" style='margin: 5px 0 0 45%;height:30px; width:60px;font-size:9px' class=\"btn btn-success\" onclick=\"addAsset()\" value=\"Add\"/>\n				<input type=\"button\" style='margin: 5px 0 0 10px;height:30px; width:60px;font-size:9px' class=\"btn btn-primary\" onclick=\"refreshTruck()\" value=\"Refresh\"/>\n			</div>\n	   </div>\n\n ";
},"useData":true});
templates['truck_tree'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "		<div class=\"truck_third\">\n		   <div class=\"truck_img\" >\n			   <img src=\"./imgs/truck.png\" style=\"align:center;\" alt=\""
    + alias3(((helper = (helper = helpers.asset_name || (depth0 != null ? depth0.asset_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_name","hash":{},"data":data}) : helper)))
    + "\" />\n			   <span class=\"truck_tree_title\" title=\""
    + alias3(((helper = (helper = helpers.asset_name || (depth0 != null ? depth0.asset_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_name","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.asset_name || (depth0 != null ? depth0.asset_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_name","hash":{},"data":data}) : helper)))
    + "</span>\n		   </div>\n		    <div class=\"checkbox_truck\" >	\n				<input type=\"checkbox\" data-asset_def=\""
    + alias3(((helper = (helper = helpers.asset_def || (depth0 != null ? depth0.asset_def : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_def","hash":{},"data":data}) : helper)))
    + "\" data-asset_callnum=\""
    + alias3(((helper = (helper = helpers.asset_callnum || (depth0 != null ? depth0.asset_callnum : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_callnum","hash":{},"data":data}) : helper)))
    + "\" data-group_name=\""
    + alias3(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" name=\"asset_"
    + alias3(((helper = (helper = helpers.asset_id || (depth0 != null ? depth0.asset_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_id","hash":{},"data":data}) : helper)))
    + "\" data-id=\"asset_"
    + alias3(((helper = (helper = helpers.asset_id || (depth0 != null ? depth0.asset_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_id","hash":{},"data":data}) : helper)))
    + " \" data-asset_imei=\""
    + alias3(((helper = (helper = helpers.asset_imei || (depth0 != null ? depth0.asset_imei : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_imei","hash":{},"data":data}) : helper)))
    + "\" data-assetid='"
    + alias3(((helper = (helper = helpers.asset_id || (depth0 != null ? depth0.asset_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"asset_id","hash":{},"data":data}) : helper)))
    + "'  class=\"select_truck\" onclick=\"selectTruck(this)\">&nbsp;&nbsp;\n			</div>\n		</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div style=\"width: 98%;margin: 0 1%;height: 20px;background: #ccc;\" >\n	<div class=\"boxcontent raised\" data-title=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n		"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n	</div>\n</div>\n<div class=\"category_icon_collection_truck\">\n	<div style=\"background: #FFF;\">\n		<input id=\"select_all_truck\" type=\"checkbox\" style=\"margin-left:24px;\" onclick=\"selectAllTruck(this)\" data-groupId=\""
    + alias3(((helper = (helper = helpers.groupId || (depth0 != null ? depth0.groupId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"groupId","hash":{},"data":data}) : helper)))
    + "\"/>ALL\n	</div> \n	<div style=\"background:white;overflow: auto;\"> \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.menus_dt : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n<div>";
},"useData":true});
templates['webcam'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=this.lambda;

  return "<form id=\""
    + alias1(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "\">\n <div class=\"cam_div\"> \n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">IP:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"ip\" id=\"ip\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ip : stack1), depth0))
    + "\" onblur=\"verifyIP(this)\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div> \n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Port:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"port\" id=\"port\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.port : stack1), depth0))
    + "\" onblur=\"verifyNumber(this,'verifyPort')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\">\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Username:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"username\" id=\"username\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.user : stack1), depth0))
    + "\" />\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Password:</span>\n					<input type=\"text\" class=\"form-control cam_input\"  name=\"password\" id=\"password\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.password : stack1), depth0))
    + "\" />\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">X:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"x\" id=\"x\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyX');\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Y:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"y\" id=\"y\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyY')\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Inner_radius:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"inner_radius\" id=\"inner_radius\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.inner_radius : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyInner_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n					\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Outer_radius:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"outer_radius\" id=\"outer_radius\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.outer_radius : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyOuter_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n					\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">S_degree:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"s_degree\" id=\"s_degree\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.s_degree : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyS_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n					\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">E_degree:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"e_degree\" id=\"e_degree\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.e_degree : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyE_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div>\n		</div>\n		<div> \n			<input name=\"id\" type=\"hidden\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\n			<input name=\"ps_id\" type=\"hidden\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\" />\n		</div>\n	</div>\n</form>";
},"useData":true});
templates['webcamDemo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=this.lambda;

  return "<form id=\""
    + alias1(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"formId","hash":{},"data":data}) : helper)))
    + "\">\n <div class=\"cam_div\"> \n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">IP:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"ip\" id=\"ip\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ip : stack1), depth0))
    + "\" onblur=\"verifyIP(this)\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div> \n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Port:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"port\" id=\"port\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.port : stack1), depth0))
    + "\" onblur=\"verifyNumber(this,'verifyPort')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\">\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Username:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"username\" id=\"username\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.user : stack1), depth0))
    + "\" />\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Password:</span>\n					<input type=\"text\" class=\"form-control cam_input\"  name=\"password\" id=\"password\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.password : stack1), depth0))
    + "\" />\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">X:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"x\" id=\"x\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyX');\" readonly=\"readonly\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Y:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"y\" id=\"y\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyY')\" readonly=\"readonly\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Inner_radius:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"inner_radius\" id=\"inner_radius\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.inner_radius : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyInner_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n					\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Outer_radius:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"outer_radius\" id=\"outer_radius\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.outer_radius : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyOuter_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n					\n				</div>\n		</div>\n		 <div class=\"cam_div1\">\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">S_degree:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"s_degree\" id=\"s_degree\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.s_degree : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyS_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n					\n				</div>\n				<div class=\"input-group form-group has-feedback cam_div2\">\n					<span class=\"input-group-addon\" style=\"width: 42%;\">E_degree:</span>\n					<input type=\"text\" class=\"form-control cam_input\" name=\"e_degree\" id=\"e_degree\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.e_degree : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyE_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\n				</div>\n		</div>\n		<div> \n			<input name=\"id\" type=\"hidden\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\n			<input name=\"ps_id\" type=\"hidden\" value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\" />\n		</div>\n	</div>\n</form>";
},"useData":true});
return templates;
});