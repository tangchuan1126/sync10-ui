(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addMultiLayer'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n			<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Area name：</span>\r\n				<input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" value=\"\" onchange=\"this.value=this.value.trim();verifyName_(this,'"
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "','5','','add')\" />\r\n				<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\r\n			</div>\r\n		";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "\r\n			<div id=\"_zone\" class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Zone：</span>\r\n				<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('modify_storage_layer')\" onfocus=\"selectAreaSingle('modify_storage_layer')\"\r\n				/>\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n				<input type=\"hidden\" style=\"width: 80%;\" name=\"area_id\" id=\"area_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\"/>\r\n			</div>\r\n		";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "\r\n		<input type=\"hidden\" id =\"storage_id\" name=\"ps_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" name=\"storage_name\" id =\"storage_name\" value=\""
    + escapeExpression(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"storageName","hash":{},"data":data}) : helper)))
    + "\"> \r\n		<div class=\"layerTypeRadio\" style=\"width: 98%;height: 33px;border: 1px solid #ccc;border-radius: 5px;margin-bottom: 5px;\">\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"1\" onclick=\"typeChange(1)\" >Location</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"2\" onclick=\"typeChange(2)\">Staging</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"3\" onclick=\"typeChange(3)\">Dock</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"child_type\" value=\"4\" onclick=\"typeChange(4)\" >Parking</div>\r\n		</div>\r\n		<!--";
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.isExistArea : depth0), {"name":"unless","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "-->\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X Position：</span>\r\n			<input type=\"text\" class=\"form-control\"  name=\"x\" id=\"x\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.x : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y Position：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"y\" id=\"y\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.y : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X length：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"height\" id=\"height\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.height : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y length：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"width\" id=\"width\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.width : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Offset angle：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"angle\" id=\"angle\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.angle : stack1), depth0))
    + "\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div id=\"_zone\" class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Zone：</span>\r\n				<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('modify_storage_layer')\" onfocus=\"selectAreaSingle('modify_storage_layer')\"\r\n				 readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n				<input type=\"hidden\" style=\"width: 80%;\" name=\"area_id\" id=\"area_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.dbrow : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\"/>\r\n			</div>\r\n		<!--";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isExistArea : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "-->\r\n		<div id=\"_dimensional\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Dimensional：</span>\r\n			<select class=\"gis_main_select\" name=\"is_three_dimensional\" style=\"width: 200px;\">\r\n				<option value=\"0\">2D</option>\r\n				<option value=\"1\">3D</option>\r\n			</select>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">vertically:</span>\r\n			<input type=\"text\" class=\"form-control\" value=\"\" id=\"vertically\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVer')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">horizontally:</span>\r\n			<input type=\"text\" class=\"form-control\" value=\"\" id=\"horizontally\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHor')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Name Prefix:</span>\r\n			<input type=\"text\" class=\"form-control\" value=\"\" id=\"loc_name\"   onchange=\"this.value=this.value.trim()\" >\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">V_Interval:</span>\r\n			<input type=\"text\" class=\"form-control\" value=\"0\" id=\"v_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVi')\" >\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\"> \r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">H_Interval:</span>\r\n			<input type=\"text\" class=\"form-control\" value=\"0\" id=\"h_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHi')\"  >\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div style=\"margin-bottom: 5px;float: right;\">\r\n			<input id=\"reverseName_\" style=\"display:none;\"  type=\"button\"  class=\"btn btn-info\" value=\"ReverseName\" onclick=\"reverseName()\">\r\n			<input type=\"button\" class=\"btn btn-success btn-sm\" value=\"Create\"   onclick=\"createLocation()\" >\r\n			<input type=\"button\" class=\"btn btn-info btn-sm\" value=\"Clear\" onclick=\"clearAutoLayer('clearAutoLayer')\">\r\n		</div>\r\n		\r\n\r\n\r\n		\r\n		";
},"useData":true});
templates['add_asset'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "											<option value=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"width:300px;\">\r\n	<form id=\"add_asset\">\r\n	  <table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n		  <tr> \r\n		    <td colspan=\"2\" align=\"center\" valign=\"top\">\r\n				  <fieldset style=\"border:2px #cccccc solid;padding:15px;margin:0px 10px 0px 10px;width:90%;\">\r\n							<div class=\"input-group\" style=\"margin-bottom: 10px;\">\r\n										<span class=\"input-group-addon\" style=\"width:52%;\">Name：</span>\r\n										<input type=\"text\" class=\"form-control\" name=\"assetName\" id=\"assetName\" onblur=\"verifyName(this);\"/>\r\n										<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n									</div>\r\n									\r\n					   	    \r\n									<div class=\"input-group\" style=\"margin-bottom: 10px;\">\r\n										<span class=\"input-group-addon\" style=\"width:52%;\">License plate：</span>\r\n										<input type=\"text\" class=\"form-control\" name=\"gate_liscense_plate\" />\r\n										<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \r\n									</div>\r\n								\r\n						   	    	<div class=\"input-group\" style=\"margin-bottom: 10px;\">\r\n						   	    		<span class=\"input-group-addon\" style=\"width:52%;\">GPS num：</span>\r\n						   	    		<input type=\"text\" class=\"form-control\" name=\"gps_tracker\" id=\"gps_tracker\" />\r\n						   	    		<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n						   	    	</div>\r\n					   	    	\r\n							  		<div class=\"input-group\" style=\"margin-bottom: 10px;\">\r\n							  			<span class=\"input-group-addon\" style=\"width:52%;\">Phone num：</span>\r\n							  			 <input type=\"text\" class=\"form-control\"  name=\"callnum\" id=\"callnum\" />\r\n							  			 <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n							  		</div>\r\n						  		\r\n						    	<div class=\"input-group\" style=\"margin-bottom: 10px;\">\r\n						    		<span class=\"input-group-addon\">Group： </span>\r\n						    		<select id=\"group\" name=\"groupId\" style=\"width:100%;font-size:12px;height:34px;\">\r\n										<option value=\"-1\"></option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.groupData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "											<option value=\"0\" style=\"color: silver\">New group</option>\r\n									</select>\r\n									<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n						    	</div>\r\n						    	<div id=\"newGroup_td\" class=\"input-group\" style=\"margin-bottom: 10px;\">\r\n\r\n						    	</div>\r\n						   		<input type=\"hidden\" name=\"groupName\" id=\"groupName\"/>\r\n						   		<input type=\"hidden\" name=\"flag\" id=\"flag\"/>\r\n					  </fieldset>\r\n	           </td>\r\n	        </tr>\r\n	   </table>\r\n	</form>\r\n</div>";
},"useData":true});
templates['add_printer_server'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<option value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" >"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<form id=\"addForm\">\r\n   <input type=\"hidden\" name=\"Method\" value=\"Add\"/>\r\n   		<div class=\"input-group\" style=\"margin-bottom: 10px;width: 300px;\">\r\n   			<span class=\"input-group-addon\">Server&nbsp;Name:</span>\r\n   			<input class=\"form-control\" name=\"printer_server_name\" id=\"printer_server_name\" type=\"text\">\r\n   		</div>\r\n   		<div class=\"input-group\" style=\"margin-bottom: 10px;width: 300px;\">\r\n   			<span class=\"input-group-addon\" style=\"width:38%;\">Storage:</span>\r\n   			<select id=\"storage\" style=\"height:34px;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.allStorageKml : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            	 \r\n			</select>\r\n   		</div>\r\n   		<div class=\"input-group\" style=\"width: 300px;\">\r\n   			<span class=\"input-group-addon\" style=\"width:38%;\">Zone：</span>\r\n   			<input type=\"text\"\r\n				class=\"form-control\" name=\"zone\" id=\"add_printer_server_zone\"\r\n				value=\"\" onclick=\"addPrinter_server_selectZone()\" onfocus=\"addPrinter_server_selectZone()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n			<input type=\"hidden\"\r\n				style=\"width: 100%;\" name=\"area_id\" id=\"add_printer_server_area_id\"\r\n				value=\"\"/>\r\n   		</div>\r\n  	 	 \r\n</form>";
},"useData":true});
templates['alterStorage'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		    			<option value=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"background: white;\">\r\n	<fieldset>\r\n		<form id=\"alterStorageForm\" method=\"post\"> \r\n			<div class=\"input-group form-group has-feedback\" style=\"margin-top:5px;\">\r\n				<span class=\"input-group-addon\">Storage Name</span>\r\n				<select name=\"ps_id\" class=\"gis_main_select\" id=\"select_storage\"  style=\"height:34px;width: 100%;\">\r\n		    		<option value=\"0\">choose...</option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.storageData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		    	</select> \r\n			</div>\r\n			<div id=\"storageOutline\">\r\n\r\n			</div>\r\n			<div class=\"input-group \" id=\"storageAlter_btn\"  style=\"position: relative;width: 100%;height:40px;\">\r\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_edit\" style=\"position: absolute;right:75px;width:64px;\" value=\"Edit\" class=\"btn btn-primary\">\r\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_cancel\" style=\"position: absolute;right:75px;width:64px;display:none;\" value=\"Cancel\" class=\"btn btn-default\">\r\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_save\" style=\"position: absolute;right:0px;width:64px;display:none;\" value=\"Save\" class=\"btn btn-success \">\r\n			<input type=\"button\" name=\"Submit2\" id=\"storageAlter_btn_delete\" style=\"position: absolute;right:0px;width:64px;\" value=\"Delete\" class=\"btn btn-warning\">\r\n			</div>\r\n		</form>\r\n	</fieldset>\r\n</div>";
},"useData":true});
templates['classify'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "        <div class=\"category_container\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n        <div class=\"buttons\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n		<div class=\" category_icon_base "
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n		<a class=\" toolBtn-cont\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\"></a>\r\n        </div>\r\n		</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		   <div id=\"boxConR\" data-pageplus=\"1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base boxConR\">\r\n		   </div> \r\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		   <div id=\"boxConL\" data-pageplus=\"-1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base boxConL\">\r\n		   </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"category_icon_collection\">\r\n    <div style=\"margin-left: 8px;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.classify : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	</div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.R_pageplus : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.L_pageplus : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\r\n\r\n";
},"useData":true});
templates['containerTemplate'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "										<option value=\""
    + escapeExpression(((helper = (helper = helpers.ic_id || (depth0 != null ? depth0.ic_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ic_id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.container_no || (depth0 != null ? depth0.container_no : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"container_no","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div id=\"stagingPlate\" style=\"width:100%; overflow: auto;background-color: #FFFFFF;\">\r\n		<table class=\"table table-hover\">\r\n					<tboby>\r\n						<tr >\r\n							<td style=\"text-align: center;\">Storage:</td>\r\n							<td style=\"text-align: center;\">\r\n								<span>"
    + escapeExpression(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"storageName","hash":{},"data":data}) : helper)))
    + "</span>\r\n							</td>\r\n						</tr>\r\n						<tr > \r\n							<td style=\"text-align: center;\">CTNR:</td>\r\n							<td style=\"text-align: center;\">\r\n								<select id=\"ic_id\" class=\"gis_main_select\" style=\"width:100px;\">\r\n									<option value=\"-1\">ALL</option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.container_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "								</select>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td colspan=\"2\" align=\"right\">\r\n								<input type=\"button\" value=\"Inquiry\" class=\"btn btn-success\" onclick=\"queryPlate()\" />\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n		</table>\r\n</div>\r\n";
},"useData":true});
templates['demand'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"raised\">\r\n	<div class=\"boxcontent\">\r\n		Demand\r\n	</div>\r\n</div>\r\n\r\n<div id=\"productDemand\" \r\n	style=\" width:100%; overflow: auto;background: white;\r\n	border-bottom: 1px solid #ccc;\">\r\n	<div style=\"background-color: #FFF;\">\r\n		<form id=\"productDemandFrom\" class=\"form-horizontal\" role=\"form\">\r\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">Title:</span>\r\n				<input style=\"width: 95%;\" type=\"text\" class=\"form-control\" placeholder=\" Title\" name=\"pro_title\" id=\"pro_title\" />\r\n			</div>\r\n			<div class=\"input-group\" id=\"pro_line_tr\"  style=\"display: none;margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">PDT Line:</span>\r\n				<select style=\"width: 95%;\" name=\"pro_line\" class=\"gis_main_select\" id=\"pro_line\" onchange=\"selectCategory(1,this.value,'productDemand')\">\r\n					<option value='-1' selected='selected'>ALL</option>\r\n				</select>\r\n			</div>\r\n			<div class=\"input-group\" id=\"pro_category_1_tr\"  style=\"display: none;margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">1st Level:</span>\r\n				<select style=\"width: 95%;\" name=\"pro_line\" class=\"gis_main_select\" id=\"pro_line\" onchange=\"selectCategory(1,this.value,'productDemand')\">\r\n					<option value='-1' selected='selected'>ALL</option>\r\n				</select>\r\n			</div>\r\n			<div class=\"input-group\" id=\"pro_category_2_tr\"  style=\"display: none;margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">2nd Level:</span>\r\n				<select style=\"width: 95%;\" name=\"pro_category_2\" class=\"gis_main_select\" id=\"pro_category_2\"  onchange=\"selectCategory(3,this.value,'productDemand')\">\r\n					<option value='-1' selected='selected'>ALL</option>\r\n				</select>\r\n			</div>\r\n			<div class=\"input-group\" id=\"pro_category_3_tr\"  style=\"display: none;margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">3rd Level:</span>\r\n				<select style=\"width: 95%;\" name=\"pro_category_3\" class=\"gis_main_select\" id=\"pro_category_3\">\r\n					<option value='-1' selected='selected'>ALL</option>\r\n				</select>\r\n			</div>\r\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">PDT Name:</span>\r\n				<input style=\"width: 95%;\" type=\"text\" id=\"pro_name\" class=\"form-control\" placeholder=\" PDT Name\"/>\r\n			</div>\r\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">Begin:</span>\r\n				<input style=\"width: 95%;\" type=\"text\" class=\"form-control\"placeholder=\"Begin time\" id=\"start_time\" />\r\n			</div>\r\n			<div class=\"input-group\" style=\"margin: 4px 0;\">\r\n				<span class=\"input-group-addon\" style=\"width: 40%;\">End:</span>\r\n				<input style=\"width: 95%;\" type=\"text\" class=\"form-control\" placeholder=\"End time\" id=\"end_time\" />\r\n			</div>\r\n			<div class=\"input-group\" style=\"margin: 4px 0;float:right;margin-right:10px;\">\r\n			<input type=\"button\" value=\"Inquiry\" style='height:30px; width:60px;font-size:9px' class=\"btn btn-primary\" onclick=\"searchProductDemandByCountry()\" />\r\n	</div>\r\n</div>\r\n<div class=\"demand_title\" style=\"width:100%;\">\r\n</div>";
  },"useData":true});
templates['demandTitlePanle'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<div class=\"demand_title_div\" title=\""
    + escapeExpression(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title_name","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title_name","hash":{},"data":data}) : helper)))
    + "\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.title_id || (depth0 != null ? depth0.title_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title_id","hash":{},"data":data}) : helper)))
    + "\">\r\n				<div class=\"demand_title_bnt\">\r\n					<div class=\"text-overflow\" style=\"width: 85%; float: left; text-align: center;\" >\r\n						"
    + escapeExpression(((helper = (helper = helpers.title_name || (depth0 != null ? depth0.title_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title_name","hash":{},"data":data}) : helper)))
    + "\r\n					</div>\r\n				</div>\r\n			</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"raised\">\r\n		<div class=\"boxcontent\">\r\n			Demand Title\r\n		</div>\r\n	</div>\r\n	<div style=\" width:100%; overflow: auto;background: white;border-bottom: 1px solid #ccc;border-top: 2px solid #ccc;min-height:50px;max-height:320px;font-size: 9px\">\r\n		<div class=\"demand_all_title_div\" style=\"margin-left:15px\" titile=\"All\" data-name=\"All\" data-id=\"-1\" \r\n		  style=\"background: #EEEEEE;\">\r\n			<input type=\"radio\" value=\"-1\" title=\"All\">All\r\n		</div>\r\n		<div class=\"demand_title_parent\" style=\"margin-left:5px;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.demand_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\r\n	</div>";
},"useData":true});
templates['dialog_product_info'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "			 <tr > \r\n			     \r\n				  <td height=\"60\" valign=\"middle\"   style='font-size:14px;' >\r\n			       <fieldset style=\"border:1px blue solid;padding:7px;width:240px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;\">\r\n				      <legend style=\"font-size:15px;font-weight:bold;color:black;\"> \r\n							 "
    + escapeExpression(((helper = (helper = helpers.p_name || (depth0 != null ? depth0.p_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"p_name","hash":{},"data":data}) : helper)))
    + "\r\n					  </legend>	\r\n						    <p style=\"font-size: 12px\">商品分类:\r\n						    </p>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.pro_title_rows : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "								  								\r\n				   </fieldset>	\r\n		         </td>\r\n			     <td align=\"center\" valign=\"middle\"   >\r\n					  <fieldset style=\"border:1px green solid;padding:7px;width:80%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;\">\r\n							<legend style=\"font-size:15px;font-weight:bold;color:green;\">\r\n								\r\n							   <span style=\"font-size:13px;color:#999999;font-weight:normal\"><%=positionName %></span>\r\n							</legend>  \r\n								<p align=\"left\"><a href=\"javascript:void(0)\" onclick=\"openlot()\" style=\"text-decoration:none\">按批次查看库存</a></p>\r\n								<p align=\"left\"><a href=\"javascript:void(0)\" onclick=\"openlp()\" style=\"text-decoration:none\">按配置查看</a></p>\r\n								<p align=\"left\"><a href=\"javascript:void(0)\" onclick=\"opentitle()\" style=\"text-decoration:none\">按title查看</a></p>\r\n									<div>可用数量：</div>\r\n								 	<div>物理数量：</div>\r\n					  </fieldset>\r\n				  &nbsp;\r\n			  </td>\r\n			    <td align=\"center\" valign=\"middle\"   style='word-break:break-all;' nowrap=\"nowrap\"> \r\n				<input name=\"\" type=\"hidden\" value=\"\"  >\r\n				\r\n				<span style=\"display: block;overflow: inherit\"><font color=\"blue\">有效库存:</font></span>\r\n				<span style=\"display: block;overflow: inherit\"><font color=\"red\">盘点库存:</font></span>\r\n				<span style=\"display: block;overflow: inherit\">出库中:</span>\r\n			  </td>\r\n			   <td align=\"center\" valign=\"middle\"   style='word-break:break-all;font-weight:bold'>\r\n			  	功能残损:<br/>\r\n			  	外观残损:\r\n			  </td>\r\n		      <td align=\"center\" valign=\"middle\"   style='word-break:break-all;font-weight:bold'>\r\n			  	<div style=\"width:80%\" align=\"left\">\r\n			  		采样周期:<br/>\r\n			  		<font color=\"green\">需求数量:</font><br/>\r\n				  	<font color=\"blue\">计划备货:</font><br/>\r\n				  	<font color=\"\">发货数量:</font><br/> \r\n				  	销售系数:<br/>\r\n				  	备货天数:\r\n			  	</div>	  	\r\n			  </td>\r\n		 </tr>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "						    <p style=\"padding-left: 70px;font-size: 12px\">\r\n						         <span>\r\n						         <a href=\"javascript:void(0)\" onclick=\"\" style=\"text-decoration:none\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\r\n								 </span>\r\n							</p>\r\n							\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<fieldset style=\"border:1px #C00 solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;\">\r\n		 <legend style=\"font-size:15px;font-weight:bold;\"> \r\n			"
    + escapeExpression(((helper = (helper = helpers.positionName || (depth0 != null ? depth0.positionName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"positionName","hash":{},"data":data}) : helper)))
    + "  \r\n		 </legend>\r\n		 <div align=\"left\" style=\"border:2px #dddddd solid;background:#E8E8E8;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;\">\r\n      \r\n		 	<span style=\"font-size:12px; font-weight: bold; color:black\">当前位置:</span>\r\n            <span style=\"font-size:12px; font-weight: bold; color:#00F\">"
    + escapeExpression(((helper = (helper = helpers.positionName || (depth0 != null ? depth0.positionName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"positionName","hash":{},"data":data}) : helper)))
    + "</span><br/>\r\n            <hr size=\"1px;\" color=\"#dddddd\">\r\n     	 </div>\r\n     	  	 <table width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"zebraTable\">\r\n	     	   <tr> \r\n		      <th width=\"20%\" align=\"left\" class=\"right-title\"  style=\"vertical-align: center;text-align: cenleftter;\">商品信息</th>\r\n		      <th width=\"20%\" class=\"right-title\"  style=\"vertical-align: center;text-align: center;\">库存信息</th>\r\n		      <!--<th width=\"17%\" class=\"right-title\"  style=\"vertical-align: center;text-align: center;\">残损</th>-->\r\n		      <th width=\"18%\" class=\"right-title\"  style=\"vertical-align: center;text-align: center;\">存取货时间</th>\r\n		 	 </tr>\r\n		 	 \r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.pro_rows : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		 </table>\r\n	<table width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"3\" cellspacing=\"0\">\r\n	 <form name=\"dataForm\">\r\n      <input type=\"hidden\" name=\"p\" >\r\n	  <input type=\"hidden\" name=\"title_id\" id=\"title_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.titleId || (depth0 != null ? depth0.titleId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"titleId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n	  <input type=\"hidden\" name=\"lot_number_id\" id=\"lot_number_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.lot_number_id || (depth0 != null ? depth0.lot_number_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lot_number_id","hash":{},"data":data}) : helper)))
    + "\">	  \r\n	  <input type=\"hidden\" name=\"positionName\" id=\"positionName\" value=\""
    + escapeExpression(((helper = (helper = helpers.positionName || (depth0 != null ? depth0.positionName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"positionName","hash":{},"data":data}) : helper)))
    + "\">	  \r\n	 </form>\r\n		       <!-- <tr> \r\n          \r\n			    <td height=\"28\" align=\"right\" valign=\"middle\"> \r\n			      <%\r\n			int pre = pc.getPageNo() - 1;\r\n			int next = pc.getPageNo() + 1;\r\n			out.println(\"页数：\" + pc.getPageNo() + \"/\" + pc.getPageCount() + \" &nbsp;&nbsp;总数：\" + pc.getAllCount() + \" &nbsp;&nbsp;\");\r\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"首页\",\"javascript:go(1)\",null,pc.isFirst()));\r\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"上一页\",\"javascript:go(\" + pre + \")\",null,pc.isFornt()));\r\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"下一页\",\"javascript:go(\" + next + \")\",null,pc.isNext()));\r\n			out.println(HtmlUtil.aStyleLink(\"gop\",\"末页\",\"javascript:go(\" + pc.getPageCount() + \")\",null,pc.isLast()));\r\n			%>\r\n			      跳转到 \r\n			      <input name=\"jump_p2\" type=\"text\" id=\"jump_p2\" style=\"width:28px;\" value=\"<%=StringUtil.getInt(request,\"p\")%>\"> \r\n			      <input name=\"Submit22\" type=\"button\" class=\"page-go\" style=\"width:28px;padding-top:0px;\" onClick=\"javascript:go(document.getElementById('jump_p2').value)\" value=\"GO\"> \r\n			    </td>\r\n			        </tr>-->\r\n		 </table>\r\n		 \r\n      </fieldset>";
},"useData":true});
templates['docks_parking_ocupied'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "				<tbody> \r\n					<tr >\r\n						<td style=\"vertical-align: middle;\">\r\n							<div style=\"float: left;margin-left: 50px;\">\r\n								<div><img src=\"./imgs/"
    + escapeExpression(((helper = (helper = helpers.equipment_type || (depth0 != null ? depth0.equipment_type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"equipment_type","hash":{},"data":data}) : helper)))
    + ".png\"></div>\r\n								<h7 style=\"text-align: left;margin-left: 10px; font-style: italic;\" class=\"text-muted\">\r\n								"
    + escapeExpression(((helper = (helper = helpers.check_in_time || (depth0 != null ? depth0.check_in_time : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"check_in_time","hash":{},"data":data}) : helper)))
    + "\r\n								</h7>\r\n							</div>\r\n							<div style=\"float:left;width: 50%;\">\r\n								<h4 style=\"margin-left: 10px;\">\r\n								"
    + escapeExpression(((helper = (helper = helpers.equipment_number || (depth0 != null ? depth0.equipment_number : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"equipment_number","hash":{},"data":data}) : helper)))
    + "\r\n								</h4>\r\n							</div>\r\n							<div style=\"float:left;width: 50%;\">\r\n								<h7 style=\"text-align: left;margin-left: 10px; font-style: italic;\" class=\"text-muted\">\r\n								"
    + escapeExpression(((helper = (helper = helpers.occupy_status || (depth0 != null ? depth0.occupy_status : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"occupy_status","hash":{},"data":data}) : helper)))
    + "\r\n								</h7>\r\n							</div>\r\n							<div style=\"float:left;width: 50%;\">\r\n								<h7 style=\"text-align: left;margin-left: 10px; font-style: italic;\" class=\"text-muted\" >\r\n								"
    + escapeExpression(((helper = (helper = helpers.rel_type || (depth0 != null ? depth0.rel_type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"rel_type","hash":{},"data":data}) : helper)))
    + "\r\n								</h7>\r\n							</div>\r\n						</td>\r\n						<td style=\"vertical-align: middle;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.tasks : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "						</td>\r\n					</tr>\r\n				</tbody>			\r\n";
},"2":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "								<div style=\"width: 100%;line-height: 20px;float:left;margin: auto;\">\r\n								    <div style=\"width: 60%;float: left;\">\r\n						               <h5 style=\"text-align: left;font-style: italic;\">\r\n						             ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.number_type : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " "
    + escapeExpression(((helper = (helper = helpers.number || (depth0 != null ? depth0.number : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"number","hash":{},"data":data}) : helper)))
    + "\r\n						               </h5>\r\n						            </div>  \r\n						            <div style=\"width: 40%;float: left;\">\r\n						               <h6 style=\"text-align: center;font-style: italic;\"class=\"text-muted\">\r\n						               "
    + escapeExpression(((helper = (helper = helpers.number_status || (depth0 != null ? depth0.number_status : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"number_status","hash":{},"data":data}) : helper)))
    + "\r\n						               </h6>\r\n						            </div>    \r\n								</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return " ["
    + escapeExpression(((helper = (helper = helpers.number_type || (depth0 != null ? depth0.number_type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"number_type","hash":{},"data":data}) : helper)))
    + "]:";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <div id=\"occupy_detail\">\r\n		<table id =\"occupy_detail\" class=\"table table-hover occupy_detail\" cellspacing=\"0\" cellpadding=\"0\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.datas : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</table>\r\n</div>	\r\n ";
},"useData":true});
templates['geoAlarmTable_list'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "	<div class=\"text-overflow\" title=\"[ "
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + escapeExpression(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + escapeExpression(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\r\n	data-points=\""
    + escapeExpression(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\r\n						"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + " \r\n	</div> \r\n	<hr style=\"size: 1px; width: 100%; color: #FFFFFF\" >\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.gaoFencingData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['geoFencing'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "								<div class=\"text-overflow\" title=\"[ "
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + escapeExpression(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + escapeExpression(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\r\n								data-points=\""
    + escapeExpression(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n									<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\r\n													"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + " \r\n								</div>\r\n								<hr style=\"size: 1px; width: 100%; color: #FFFFFF;margin-top: 1px;margin-bottom: 1px;\" >\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"raised\">\r\n	<div class=\"boxcontent\">\r\n		GeoFence\r\n	</div> \r\n</div>\r\n<div id=\"geoFencing\" style=\" width:98%; overflow-y: auto;margin:0 1%; background: #FFF;border-top: 2px solid #888;max-height: 450px;float:left;\">\r\n	<table style=\"width:100%;margin: auto;position:relative;\" class=\"table\">\r\n					<tr>\r\n						<td>\r\n							<div style=\"float: left; width: 100%;\">\r\n							    <input type=\"button\" value=\"Add\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-success\" onclick=\"addGeofencing('geoFencing')\" />\r\n							    <input type=\"button\" value=\"Ref\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" onclick=\"refreshGeoFencing('geoFencing')\" />\r\n							    <input type=\"button\" value=\"Del\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-danger\" onclick=\"removeGeoFencing('geoFencing')\" />\r\n							</div>\r\n							<div id=\"addGeoFencing\" style=\"display: none; float: left; width: 100%;background-color: #FFFFFF;margin-left:2%;\">\r\n								<form id=\"addGeoFencingForm\" class=\"form-horizontal\" role=\"form\" style=\"width:100%;\" >\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;margin-left:2%;\">Name:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"fencing_name\" name=\"name\" placeholder=\"Input Name\"   onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;margin-left:2%;\">Explain:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"fencing_def\" name=\"def\" placeholder=\"Input Explain\" onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 37%;margin-left:2%;\">Type:</span>\r\n	  									<select id=\"fencing_type\" name=\"type\" class=\"gis_main_select\" onchange=\"addGeofencing(this.value)\">\r\n											<option value=\"polygon\" selected=\"selected\">Polygon</option>\r\n											<option value=\"circle\">Circle</option>\r\n											<option value=\"rect\">Rectangle</option>\r\n										</select>\r\n	  								</div>\r\n	  								<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;margin-left:2%;\">Geometry:</span>\r\n	  									<input class=\"form-control\" type=\"text\" disabled=\"disabled\" id=\"fencing_points\" name=\"points\" onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<input type=\"button\" class=\"btn btn-primary\" style=\"margin:0 35px\" value=\"Cancel\" onclick=\"cancelGeo()\"/>\r\n									<input type=\"button\" class=\"btn btn-success\" value=\"Save\" onclick=\"saveAlarmLabel('fencing')\" />\r\n								</form>\r\n							</div>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td>\r\n							<div id=\"geoFencing_list\" style=\" width:100%;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.gaoData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n							</div>\r\n						</td>\r\n					</tr>\r\n </table>\r\n <table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;width:288px;\">\r\n  	<thead>\r\n  			<tr>\r\n	    		<td >\r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\r\n	                	First\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n	                	Last\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n	                	Next\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\r\n	                	End\r\n	                </button>\r\n	            </td>\r\n	    	</tr>\r\n	</thead>\r\n  </table>  \r\n</div>";
},"useData":true});
templates['geoLine'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "									<div class=\"text-overflow\" title=\"[ "
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + escapeExpression(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + escapeExpression(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\r\n									data-points=\""
    + escapeExpression(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n										<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\r\n														"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + " \r\n									</div>\r\n									<hr style=\"size: 1px; width: 100%; color: #FFFFFF;margin-top: 1px;margin-bottom: 1px;\" >\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"raised\">\r\n	<div class=\"boxcontent\">\r\n		Route\r\n	</div>\r\n</div>  \r\n<div id=\"geoLine\" style=\" width:98%; overflow-y: auto;margin:0 1%; background: #FFF;border-top: 2px solid #888;max-height: 450px;\">\r\n		<table style=\"width:100%;margin: auto;\" class=\"table\">\r\n					<tr> \r\n						<td>\r\n							<div style=\"float: left; width: 100%;\">\r\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-success\" onclick=\"addGeofencing('geoLine')\" value=\"Add\" / >\r\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" onclick=\"refreshGeoFencing('geoLine')\" value=\"Ref\" />\r\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-danger\" onclick=\"removeGeoFencing('geoLine')\" value=\"Del\" >\r\n							</div>\r\n							<div id=\"addGeoLine\" style=\"display: none; float: left; width: 100%; background: #FFFFFF;margin-left:2%;\">\r\n								<form id=\"addGeoLineForm\" class=\"form-horizontal\" role=\"form\">\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Name:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"line_name\" name=\"name\" placeholder=\"Input Name\"   onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Explain:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"line_def\" name=\"def\" placeholder=\"Input Explain\" onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Route:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"line_points\" name=\"points\" disabled=\"disabled\" onchange=\"this.value=this.value.trim()\"/>\r\n										<input type=\"hidden\" id=\"line_type\" name=\"type\" value=\"line\" />\r\n	  								</div>\r\n									<input type=\"button\" class=\"btn btn-primary\" style=\"margin:0 35px\" value=\"Cancel\" onclick=\"cancelGeo()\"/>\r\n									<input type=\"button\" class=\"btn btn-success\" value=\"Save\" onclick=\"saveAlarmLabel('line')\" />\r\n								</form>\r\n							</div>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td>\r\n							<div id=\"geoLine_list\" style=\" width:100%;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.gaoData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "							</div>\r\n						</td>\r\n					</tr>\r\n    </table>\r\n	<table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;width:288px;\">\r\n  	<thead>\r\n  			<tr>\r\n	    		<td >\r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\r\n	                	First\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n	                	Last\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n	                	Next\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\r\n	                	End\r\n	                </button>\r\n	            </td>\r\n	    	</tr>\r\n	</thead>\r\n  </table>  \r\n				\r\n</div>";
},"useData":true});
templates['geoPoint'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "									<div class=\"text-overflow\" title=\"[ "
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + " +] "
    + escapeExpression(((helper = (helper = helpers.def || (depth0 != null ? depth0.def : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"def","hash":{},"data":data}) : helper)))
    + " \" id=\"geo_"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer;\" data-typeen=\""
    + escapeExpression(((helper = (helper = helpers.typeen || (depth0 != null ? depth0.typeen : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"typeen","hash":{},"data":data}) : helper)))
    + "\"\r\n									data-points=\""
    + escapeExpression(((helper = (helper = helpers.points || (depth0 != null ? depth0.points : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"points","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n										<input type=\"checkbox\" onclick=\"selectGeoAlarmlabel(this,'"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "')\" style=\"vertical-align : middle\">&nbsp;\r\n														"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + " \r\n									</div>\r\n									<hr style=\"size: 1px; width: 100%; color: #FFFFFF;margin-top: 1px;margin-bottom: 1px;\" >\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"raised\">\r\n	<div class=\"boxcontent\">\r\n		GeoPoint\r\n	</div>\r\n</div> \r\n<div id=\"geoPoint\" style=\" width:98%; overflow-y: auto;margin:0 1%; background: #FFF;border-top: 2px solid #888;max-height: 450px;\">\r\n		<table style=\"width:100%;margin: auto;\" class=\"table\">\r\n					<tr> \r\n						<td>\r\n							<div style=\"float: left; width: 100%;\">\r\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-success\" onclick=\"addGeofencing('geoPoint')\" value=\"Add\" / >\r\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" onclick=\"refreshGeoFencing('geoPoint')\" value=\"Ref\" />\r\n								<input type=\"button\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-danger\" onclick=\"removeGeoFencing('geoPoint')\" value=\"Del\" >\r\n							</div>\r\n							<div id=\"addGeoPoint\" style=\"display: none; float: left; width: 100%; background: #FFFFFF;margin-left:2%;\">\r\n								<form id=\"addGeoPointForm\" class=\"form-horizontal\" role=\"form\">\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Name:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"point_name\" name=\"name\" placeholder=\"Input Name\"   onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Explain:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"point_def\" name=\"def\" placeholder=\"Input Explain\" onchange=\"this.value=this.value.trim()\"/>\r\n	  								</div>\r\n									<div class=\"input-group form-group has-feedback\">\r\n	  									<span class=\"input-group-addon\" style=\"width: 35%;\">Point:</span>\r\n	  									<input class=\"form-control\" type=\"text\"  id=\"point_points\" name=\"points\" disabled=\"disabled\" onchange=\"this.value=this.value.trim()\"/>\r\n										<input type=\"hidden\" id=\"point_type\" name=\"type\" value=\"point\" />\r\n	  								</div>\r\n									<input type=\"button\" class=\"btn btn-primary\" style=\"margin:0 35px\" value=\"Cancel\" onclick=\"cancelGeo()\"/>\r\n									<input type=\"button\" class=\"btn btn-success\" value=\"Save\" onclick=\"saveAlarmLabel('point')\" />\r\n								</form>\r\n							</div>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td>\r\n							<div id=\"geoPoint_list\" style=\" width:100%;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.gaoData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "							</div>\r\n						</td>\r\n					</tr>\r\n		\r\n	</table>\r\n	 <table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;width:288px;\">\r\n  		<thead>\r\n  			<tr>\r\n	    		<td >\r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\r\n	                	First\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n	                	Last\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n	                	Next\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl-geo btn btn-default btn-xs\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\r\n	                	End\r\n	                </button>\r\n	            </td>\r\n	    	</tr>\r\n		</thead>\r\n  </table>  \r\n</div>";
},"useData":true});
templates['gis_storage'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<form id=\"keywordTxtForm\">\r\n<input id=\"keywordTxt\" class=\"txt300 floatl\" value=\"\" data-city=\"\" city-search=\"\">\r\n<input class=\"magnifier_button floatl\" type=\"submit\" value=\"\">\r\n<div class=\"clear\"></div>\r\n</form> ";
  },"useData":true});
templates['inventory'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "							<option value='"
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "' data-kml='"
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "'>"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			      		<option value='"
    + escapeExpression(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"key","hash":{},"data":data}) : helper)))
    + "'>"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"raised\">\r\n	<div class=\"boxcontent\">\r\n		Inventory\r\n	</div>\r\n</div> \r\n<div id=\"productStorage\" style=\"width:100%; overflow: auto;height:auto;background: #FFFFFF;\">\r\n			<form id=\"productStorageFrom\" class=\"form-horizontal\" role=\"form\">\r\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">			    \r\n		    	<div class=\"input-group\" style=\"width:100%;\">\r\n		      		<div class=\"input-group-addon\" style=\"width:100px;\">Storage:</div>\r\n			      	<select name=\"ps_id\" id=\"ps_id\"  class=\"gis_main_select\" style=\"width: 100%;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.storageData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "				   </select>\r\n			    </div>\r\n			</div>\r\n			 <div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;position:relative;\">\r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon\" style=\"width:34%;\">Title</div>\r\n			      <input type=\"text\" name=\"titles\" class=\"form-control\" id=\"title\" placeholder=\"Title\" >\r\n			      <!--<input type=\"hidden\" name=\"title_ids\" id=\"title_ids\" >-->\r\n			    </div>\r\n			    <div class=\"titleScrollbar\" style=\"width:66%;right: 0px;\">\r\n			    	<div class=\"titleList_\"></div>\r\n			    </div>\r\n			 </div>\r\n			 <div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\r\n				<div class=\"input-group\" >\r\n			      	<div class=\"input-group-addon\" style=\"width:34%;\">Lot Number</div>\r\n				    <input type=\"text\" name=\"lot_numbers\" class=\"form-control\" id=\"lot_numbers\" value=\"\" placeholder=\"Lot Number\">\r\n				 </div>\r\n				 <div class=\"titleScrollbar\" style=\"width:66%;right: 0px;\">\r\n			    	<div class=\"titleList_\"></div>\r\n			    </div>\r\n			</div>\r\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">			    \r\n		    	<div class=\"input-group\" style=\"width:100%;\">\r\n		      		<div class=\"input-group-addon\" style=\"width:34%;\">PDT Line</div>\r\n			      	<input type=\"text\" name=\"product_line\" class=\"form-control\" id=\"p_line\" value=\"\" placeholder=\"PDT Line\">\r\n			    </div>\r\n			    <div class=\"titleScrollbar\" style=\"width:66%;right: 0px;\">\r\n			    	<div class=\"titleList_\"></div>\r\n			    </div>\r\n			</div>\r\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\r\n				<div class=\"input-group\" style=\"width:100%;\">\r\n			      	<div class=\"input-group-addon\" style=\"width:100px;\">Catalog</div>\r\n				    <input type=\"text\" name=\"catalog\" class=\"form-control\" id=\"catalog\" value=\"\" placeholder=\"Catalog\">\r\n				</div>\r\n			</div>\r\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\r\n				<div class=\"input-group\" style=\"width:100%;\">\r\n			      	<div class=\"input-group-addon\" style=\"width:100px;\">PDT Name</div>\r\n				    <input type=\"text\" name=\"p_names\" class=\"form-control\" id=\"p_names\" value=\"\" placeholder=\"PDT Name\">\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\r\n		      	<div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon\" style=\"width:100px;\">LP Type</div>\r\n			      <select type=\"text\" name=\"container_type\" class=\"gis_main_select\" id=\"container_type\" placeholder=\"Item/Set\" style=\"width: 100%;\" value=\"0\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.cntDt : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			      </select>\r\n			      <!--<input type=\"text\" name=\"container_type\" class=\"form-control\" id=\"container_type\" placeholder=\"Container Type\" value=\"\">-->\r\n			    </div>\r\n			</div>\r\n			<!--<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\r\n		      	<div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon\" style=\"width:100px;\">Type Id</div>\r\n			      <input type=\"text\" name=\"type_id\" class=\"form-control\" id=\"type_id\" placeholder=\"Type Id\" value=\"\">\r\n			    </div>\r\n			</div>-->\r\n			<div class=\"form-group\" style=\"margin: 5px 2px 5px 1px;\">\r\n		      	<div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon\" style=\"width:100px;\">Item/Set</div>\r\n			      <select type=\"text\" name=\"union_flag\" class=\"gis_main_select\" id=\"union_flag\" placeholder=\"Item/Set\" style=\"width: 100%;\" value=\"0\">\r\n			      	<option value='-1'>All</option>\r\n			      	<option value='0'>Loose</option>\r\n			      	<option value='1'>Set</option>\r\n			      </select>\r\n			    </div>\r\n			</div>\r\n			 \r\n			 <div class=\"input-group\" style=\"float: right;margin-right:10px;\" >\r\n\r\n				<input type=\"button\" id=\"inquiry\" style=\"margin:5px 0 0 5px;height:30px; width:60px;font-size:9px\" class=\"btn btn-primary\" value=\"Inquiry\"  />\r\n				<input type=\"hidden\"  id=\"queryDt\" >\r\n			 </div>\r\n			</form>\r\n</div>";
},"useData":true});
templates['locInventoryList'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			      		<option value='"
    + escapeExpression(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"key","hash":{},"data":data}) : helper)))
    + "'>"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.dt : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"4":function(depth0,helpers,partials,data) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = "					<tr class=\"locInventory\"  data-index=\""
    + escapeExpression(lambda((data && data.index), depth0))
    + "\">\r\n						<td  valign=\"middle\" style=\"text-align: center;\">\r\n							"
    + escapeExpression(((helper = (helper = helpers.container || (depth0 != null ? depth0.container : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"container","hash":{},"data":data}) : helper)))
    + "\r\n						</td>\r\n						<td valign=\"middle\" >\r\n							<fieldset class=\"products_fieldset\">\r\n								<legend class=\"products_legend\">"
    + escapeExpression(((helper = (helper = helpers.container || (depth0 != null ? depth0.container : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"container","hash":{},"data":data}) : helper)))
    + "</legend>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.products : depth0), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "								\r\n							</fieldset>\r\n						</td>\r\n					</tr>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "									<fieldset class=\"product_fieldset\" >\r\n										<div style=\"margin-top: 5px;width: 50px;float: left;margin-left: 5px;\">Name:</div>\r\n										<div style=\"margin-top:5px;\">"
    + escapeExpression(((helper = (helper = helpers.p_name || (depth0 != null ? depth0.p_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"p_name","hash":{},"data":data}) : helper)))
    + "</div>\r\n										<div style=\"width: 140px;margin-left: 5px;margin-top: 5px;float: left;\">Total_locked_quantity:</div>\r\n										<div style=\"margin-top:5px;\">"
    + escapeExpression(((helper = (helper = helpers.total_locked_quantity || (depth0 != null ? depth0.total_locked_quantity : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"total_locked_quantity","hash":{},"data":data}) : helper)))
    + "</div>\r\n										<div style=\"width: 100px;margin-left: 5px;margin-top: 5px;float: left;\">Total_quantity:</div>\r\n										<div style=\"margin-top:5px;\">"
    + escapeExpression(((helper = (helper = helpers.total_quantity || (depth0 != null ? depth0.total_quantity : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"total_quantity","hash":{},"data":data}) : helper)))
    + "</div>\r\n										<div style=\"margin-left: 5px;margin-top: 5px;width: 90px;float: left;\">Product_line:</div>\r\n										<div style=\"margin-top:5px;\">"
    + escapeExpression(((helper = (helper = helpers.product_line || (depth0 != null ? depth0.product_line : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"product_line","hash":{},"data":data}) : helper)))
    + "</div>\r\n\r\n									</fieldset>	\r\n";
},"7":function(depth0,helpers,partials,data) {
  return "				<tr>\r\n					<td colspan=\"2\">No data...</td>\r\n				</tr>\r\n			";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div id=\"locInventoryEle\" style=\"margin-left: 0px;margin-right: 0px;\">\r\n\r\n	<div id=\"closeInventoryWin\" >\r\n		<div class=\"windowTitle\" style=\"margin-left: 10px;padding-top: 2px;\">Inventory["
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.name : stack1), depth0))
    + "]</div>\r\n		<div id=\"closeWin\" >X</div>\r\n	</div>\r\n	<div id=\"inventoryQueryForm\" style=\"position: relative;\">\r\n		<form class=\"form-inline\" >\r\n			<input type=\"hidden\" id=\"ps_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\"/>\r\n			<input type=\"hidden\" id=\"slc_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.slc_id : stack1), depth0))
    + "\"/>\r\n			<input type=\"hidden\" id=\"slc_type\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.slc_type : stack1), depth0))
    + "\"/>\r\n			<!--<div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;\">\r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon input-label\" style=\"width:100px;\">Container</div>\r\n			      <input type=\"text\" name=\"container\" class=\"form-control\"  id=\"container\" placeholder=\"Container\">\r\n			    </div>\r\n			 </div>-->\r\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;min-width:150px;\">\r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon  input-label\" style=\"width:100px;height: 34px;\">LP Type</div>\r\n			      <select type=\"text\" name=\"container_type\" class=\"gis_main_select\" id=\"container_type\" placeholder=\"Container Type\" style=\"width: 100%;  height: 34px;\" value=\"0\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.cntDt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "			      </select>\r\n			      <!--<input type=\"text\" class=\"form-control\"  id=\"container_type\" placeholder=\"Container Type\" value="
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.container_type : stack1), depth0))
    + ">-->\r\n			    </div>\r\n			 </div>\r\n			<div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;position:relative;min-width:150px;\">\r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon  input-label\" style=\"width:32%;\">Title</div>\r\n			      <input type=\"text\" name=\"title\" class=\"form-control\" id=\"title\" placeholder=\"Title\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.titles : stack1), depth0))
    + "\">\r\n			      <!--<input type=\"hidden\" id=\"title_ids\" value="
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.title_ids : stack1), depth0))
    + ">-->\r\n			    </div>\r\n			    <div class=\"titleScrollbar\" style=\"width: 68%;right:0px;\">\r\n			    	<div class=\"titleList_\"></div>\r\n			    </div>\r\n			    \r\n			 </div>\r\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;position: relative;min-width:150px;\">\r\n			    \r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon  input-label\" style=\"width:32%;\">PDT Line</div>\r\n			      <input type=\"text\" name=\"p_line\" class=\"form-control\" id=\"p_line\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.product_line : stack1), depth0))
    + "\" placeholder=\"PDT Line\">\r\n			    </div>\r\n			    <div class=\"titleScrollbar\" style=\"width: 68%;right:0px;\">\r\n			    	<div class=\"titleList_\"></div>\r\n			    </div>\r\n			 </div>\r\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;min-width:150px;\">\r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon  input-label\" style=\"width:100px;\">PDT Name</div>\r\n			      <input type=\"text\" name=\"p_name\" class=\"form-control\" id=\"p_name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.p_names : stack1), depth0))
    + "\" placeholder=\"PDT Name\">\r\n			    </div>\r\n			 </div>\r\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;position: relative;min-width:150px;\">\r\n			    <div class=\"input-group\" style=\"width:100%;\">\r\n			      <div class=\"input-group-addon  input-label\" style=\"width:32%;\">Lot Number</div>\r\n			      <input type=\"text\" name=\"lot_number\" class=\"form-control\" id=\"lot_number\" placeholder=\"Lot Number\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.queryDt : depth0)) != null ? stack1.lot_numbers : stack1), depth0))
    + "\">\r\n			    </div>\r\n			    <div class=\"titleScrollbar\" style=\"width: 68%;right:0px;\">\r\n			    	<div class=\"titleList_\"></div>\r\n			    </div>\r\n			 </div>\r\n			 <div class=\"form-group\" style=\"width: 30%;margin: 5px 1.5% 5px 1.5%;position: relative;height: 35px;min-width:150px;\">\r\n			    <div class=\"input-group\" style=\"  width:110px;float: right;margin-right: 90px;margin-top: 8px;\">\r\n			      <div class=\"input-group-addon  input-label\" style=\"width: 40px;margin-bottom: 20px;background-color: #f2dede;border-radius: 0px;height: 15px;\"></div>\r\n			      <div style=\"margin-left: 10px;font-size: 12px;font-weight: 800;\">Damage</div>\r\n			    </div>\r\n			   <input type=\"button\" style=\"position: absolute;right: 1.8%;width:80px;  background: #5bc0de;border-color: #46b8da;\" id=\"queryInventory\" class=\"btn btn-info\" value=\"Query\">\r\n			 </div>\r\n		</form>\r\n	</div>\r\n	<div id=\"inventory_cont\">\r\n		<div id=\"inventory_model_parent\">\r\n			<div id=\"inventory_model\">\r\n				<div id=\"inventory3D\">3D</div>\r\n				<div id=\"inventoryList\">List</div>\r\n				<!--<div id=\"clearSelect3DObj\">Unselect</div>-->\r\n			</div>\r\n			\r\n		</div>\r\n		<div id=\"store_3d\" class=\"store_3d\" width=\"100%\" height=\"100%\"></div>\r\n		<table id=\"inventory_tab\" width=\"100%\" style=\"border-collapse: separate;width: 99%;margin-left: 0.5%;\">\r\n			<tr id=\"thead1\">\r\n				<td class=\"container thead theadLeft\" rowspan=\"2\" width=\"200px\" >Container</td>\r\n				<td class=\"container_type thead\" rowspan=\"2\" width=\"200px\">LP</br>Type</td>\r\n\r\n				<td class=\"product\" colspan=\"8\" width=\"280px\" >Product</td>\r\n			</tr> \r\n			<tr id=\"thead2\"> \r\n				<td class=\"p_name thead\">PDT Name</td>\r\n				<td class=\"product_line thead\">PDT</br>Line</td>\r\n				<td class=\"title thead\">Title</td>\r\n				<td class=\"quantity thead\" >Quantity</td>\r\n				<td class=\"total_quantity thead\">Total</br>Quantity</td>\r\n				<td class=\"locked_quantity thead\">Locked</br>Quantity</td>\r\n				<td class=\"locked_total_quantity thead\">Total</br>Locked</br>Quantity</td>\r\n				<td class=\"sn thead\" style=\"/*box-shadow: 1px 2px 5px #888888;*/\">SN</td>\r\n			</tr>\r\n			<tr>\r\n				<td colspan=\"10\" style=\"text-align: center;\">\r\n					<div id=\"inventory_content\">\r\n\r\n					</div>\r\n				</td>\r\n			</tr>\r\n			<!--";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isExistDt : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.isExistDt : depth0), {"name":"unless","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "-->\r\n		</table>\r\n	</div>\r\n</div>\r\n<style type=\"text/css\">\r\n #locInventoryEle{height:400px;}\r\n #locInventoryEle .mCSB_inside .mCSB_container{margin-right:0px;}\r\n #locInventoryEle .mCSB_scrollTools{right:-6px}\r\n.input-label{\r\n	background: #4eabb9;\r\n  	color: white;\r\n}\r\n</style>";
},"useData":true});
templates['loc_frequency'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n  <tr>\r\n    <td align=\"center\" valign=\"top\">\r\n		  <fieldset style=\"border:2px #cccccc solid;padding:15px;margin:10px;width:80%;\">\r\n		  	<form id=\"loc_frequency\">\r\n				<table > \r\n					<tr>\r\n						<td align=\"right\" valign=\"middle\">Interval(S)：</td>\r\n					    <td align=\"left\" valign=\"middle\">\r\n					    	<input type=\"text\" style=\"width: 100%; background: \" id=\"interval\" name=\"interval\" value=\"120\">\r\n			            </td>\r\n			   	    </tr>\r\n					<tr>\r\n						<td align=\"right\" valign=\"middle\">Batch：</td>\r\n					    <td align=\"left\" valign=\"middle\">\r\n			              <input type=\"text\" style=\"width: 100%;\" id=\"batch\" name=\"batch\" value=\"1\"/> \r\n			              <input type=\"hidden\"  id=\"truck_data\" />  \r\n			            </td>\r\n			   	    </tr>\r\n				 </table>\r\n		  	</form>\r\n		 </fieldset>\r\n       </td>\r\n    </tr>\r\n    <tr>\r\n   		<td style=\"text-align: center\"><span style=\"color: red;\" id=\"errInfo\">&nbsp;</span></td>\r\n	   </tr>\r\n   \r\n</table>";
  },"useData":true});
templates['maintenanceTemplate'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div style=\"width:100%;margin-top: 10px;background: #F2F3A3;line-height:30px;font-size:15px;text-align:center;font-family: Georgia;\">Add    New   Layer</div>\r\n<div id=\"maintainLayer\"	style=\" width:100%;overflow: auto;background: white; height:100px;\"> \r\n	<div class=\"maintenance_div\">\r\n	    <div class=\"maintenance_img\">\r\n			<img id=\"webcam_img\" style=\"margin: 30%;\"   src=\"./imgs/webcam.png\" onmousedown=\"LayerMousedown(6,this,event)\">\r\n		</div>			\r\n		<div class=\"maintenance_b_div\">Webcam</div>\r\n	</div>	\r\n	<div class=\"maintenance_div\">\r\n	    <div class=\"maintenance_img\">\r\n			<img id=\"printer_img\" style=\"margin: 35% 10%;\"  src=\"./imgs/printer_layer.png\" onmousedown=\"LayerMousedown(7,this,event)\">\r\n		</div>\r\n		<div class=\"maintenance_b_div\">Printer</div>\r\n	</div>\r\n	<div class=\"maintenance_div\">\r\n	    <div class=\"maintenance_img\">\r\n			<img id=\"rectangle_img\" style=\"margin: 30% 0;\"  src=\"./imgs/rectangle_img.png\" onmousedown=\"LayerMousedown('x',this,event)\">\r\n		</div>\r\n		<div class=\"maintenance_b_div\">Rectangle</div>\r\n	</div>\r\n	<input type=\"hidden\" id =\"ps_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\r\n	<input type=\"hidden\" id =\"storageName\" value=\""
    + escapeExpression(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"storageName","hash":{},"data":data}) : helper)))
    + "\">\r\n</div>\r\n<div style=\"width:100%;background:#F2F3A3;line-height:30px;font-size:15px;text-align:center;font-family: Georgia;\">Edit    Road</div>\r\n<div style=\"height:110px;width:100%;background:#fff;\">\r\n       <img style=\"margin: 5px 0 0 10px;\"  src=\"./imgs/draw_road.png\" onclick=\"initEditRoad("
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + ")\"/>\r\n       <input type='button' id=\"edit_road\" value='Edit' class='btn btn-info'style=\"margin: 5px 0 0 30px;height:30px; width:60px;font-size:9px;\" onclick=\"initEditRoad("
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + ")\"/> \r\n       <input type='button' id=\"reset_edit\" value='Cancel' class='btn btn-primary'style=\"margin: 5px 0 0 30px;height:30px; width:60px;font-size:9px;display:none;\" onclick=\"reSetEdit()\"/> \r\n       <input type='button' id=\"save\" value='Save' class='btn btn-success' style='margin: 5px 0 0 10px;height:30px; width:60px;font-size:9px' onclick='saveToDB()' disabled=\"disabled\"/>\r\n</div>";
},"useData":true});
templates['mapTool'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "	        <div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"map_tool\" onclick=\"mapToolClick(this)\"\r\n				onmouseover=\"this.className='map_tool_selected'\"\r\n				onmouseout=\"if(this.getAttribute('flag')!='1'){this.className='map_tool'}\"\r\n				onmousedown=\"this.className='map_tool_mousedown';\"\r\n				onmouseup=\"this.className='map_tool_selected'\">\r\n				<img alt=\"clear\" src=\""
    + escapeExpression(((helper = (helper = helpers.img || (depth0 != null ? depth0.img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"img","hash":{},"data":data}) : helper)))
    + "\" align=\"top\">\r\n				<span style=\"cursor: default;font-size: 9px\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\r\n			</div> \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.tools : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['modify_area'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<form id=\"modify_area\">\r\n<div class=\"area_div\"  >\r\n	  <div class=\"input-group form-group has-feedback area_div1\"> \r\n	 	<span class=\"input-group-addon\" style=\"width:44%;\">Name</span>\r\n	  	<input type=\"text\" class=\"form-control area_input\" id=\"zone_name\" name=\"zone_name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onblur=\"this.value=this.value.trim()\" onchange=\"verifyName(this,'"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "','5','"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "')\"> \r\n	  	<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n    	<input type=\"hidden\"  id=\"zone_name_old\" name=\"zone_name_old\"  value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\">\r\n    	<input type=\"hidden\"  id=\"zone_id\" name=\"zone_id\"  value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\">\r\n    	<input type=\"hidden\"  id=\"ps_id\" name=\"ps_id\"  value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\">\r\n	  </div>\r\n	  <div class=\"input-group form-group has-feedback area_div1\"> \r\n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">X position</span>\r\n	  	 <input type=\"text\" class=\"form-control area_input\" id=\"x\" name=\"x\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX')\"/>\r\n	  	 <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \r\n	  </div>\r\n	  <div class=\"input-group form-group has-feedback area_div1\"> \r\n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">Y position</span>\r\n	  	 <input type=\"text\" class=\"form-control area_input\" id=\"y\" name=\"y\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY')\"/> \r\n	  	 <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\r\n	  </div>\r\n	  <div class=\"input-group form-group has-feedback area_div1\"> \r\n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">X length</span>\r\n	     <input type=\"text\" class=\"form-control area_input\" id=\"height\" name=\"height\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.height : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight')\"/>\r\n	     <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \r\n	  </div>\r\n	  <div class=\"input-group form-group has-feedback area_div1\"> \r\n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">Y length</span>\r\n		<input type=\"text\" class=\"form-control area_input\" id=\"width\" name=\"width\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.width : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth')\"/>\r\n		<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \r\n	  </div>\r\n	  <div class=\"input-group form-group has-feedback area_div1\"> \r\n	 	 <span class=\"input-group-addon\" style=\"width:44%;\">Offset angle</span>\r\n		  <input type=\"text\" class=\"form-control area_input\" id=\"angle\" name=\"angle\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.angle : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle')\"/>\r\n		  <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \r\n	  </div>\r\n	  <div class=\"input-group form-group has-feedback area_div1\" style=\"display:none;\" id=\"area_subtype\"> \r\n	 	  <span class=\"input-group-addon\" style=\"width:44%;\">Yard type</span>\r\n		 	 <select id=\"area_subtype\" class=\"gis_main_select\" name=\"area_subtype\">\r\n		  		<option value=\"-1\">Choose...</option>\r\n		  		<option value=\"1\">Parking</option>\r\n		  		<option value=\"2\">Waiting</option>\r\n		  		<option value=\"3\">Empty CTN</option>\r\n		  		<option value=\"4\">Full CTN</option>\r\n		 	 </select>\r\n		  <span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span> \r\n	  </div>\r\n	  <div style=\"display:none;\"> \r\n	  	  <span style=\"color: red;\" id=\"errInfo\">&nbsp;</span>\r\n	  </div>\r\n</div\r\n</form>\r\n";
},"useData":true});
templates['modify_light'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<form id=\"light\">\r\n		<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\"\r\n			cellspacing=\"0\">\r\n			<tr>\r\n				<td align=\"center\" valign=\"top\" colspan=\"2\">\r\n						<table> \r\n							<tr>\r\n								<td align=\"right\" >Name：</td>\r\n								<td align=\"left\" style=\"position:relative;\"><input type=\"text\"\r\n									style=\"width: 85%;\" name=\"name\" id=\"name\"\r\n									value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" onblur=\"verifyName(this)\" /><span style=\"height: 16px;width: 16px;position: absolute;top: 5px;\" id=\"vNameMsg\">\r\n									<img  title=\"Name is exist\" src=\"imgs/delete_red.png\" id=\"VNameError\" style=\"display: none;\">\r\n									<img  src=\"imgs/confirm.png\" id=\"VNameRight\" style=\"display: none;\">\r\n									</span>\r\n								</td>\r\n								\r\n							</tr>\r\n							<tr>\r\n								<td align=\"right\" >X Position：</td>\r\n								<td align=\"left\" style=\"position: relative;\"><input type=\"text\"\r\n									style=\"width: 85%;\" name=\"x\" id=\"x\"\r\n									value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"verifyX()\"/>\r\n									<span style=\"position: absolute;top: 5px;height: 16px;width:16px;\">\r\n										<img id=\"x_con\" src=\"imgs/confirm.png\" style=\"display: none\">\r\n					             		<img id=\"x_del\" title=\"Data Error!\" src=\"imgs/delete_red.png\" style=\"display: none\">\r\n									</span>\r\n								</td>\r\n								\r\n							</tr>\r\n							<tr>\r\n								<td align=\"right\" >Y Position：</td>\r\n								<td align=\"left\" style=\"position: relative;\"><input type=\"text\"\r\n									style=\"width: 85%;\" name=\"y\" id=\"y\"\r\n									value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"verifyY()\" />\r\n									<span style=\"position: absolute;top: 5px;height: 16px;width:16px;\">\r\n										<img id=\"y_con\" src=\"imgs/confirm.png\" style=\"display: none\">\r\n					             		<img id=\"y_del\" title=\"Data Error!\" src=\"imgs/delete_red.png\" style=\"display: none\">\r\n									</span>\r\n								</td>\r\n								\r\n							</tr>\r\n							<tr>\r\n								<td align=\"right\" >Status：</td>\r\n								<td align=\"left\" >\r\n								<select name=\"type\" style=\"width: 85%\">\r\n								<option value=\"0\">OFF </option>\r\n								<option value=\"1\">ON</option>\r\n								</select>\r\n								</td>\r\n							</tr>\r\n				\r\n						</table>\r\n				</td>\r\n			</tr>\r\n			<input id=\"id\" name=\"id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\r\n			<input id =\"ps_id\" name=\"ps_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.psId : stack1), depth0))
    + "\" />\r\n		</table>\r\n	</form>";
},"useData":true});
templates['modify_location'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div style=\"width:100%;height:auto;border-radius: 5px;background-color: #eeeeee;\">\r\n  	<form id=\"modify_data\">\r\n  		<div style=\"margin-left: 35px;padding-top: 20px;padding-bottom: 10px;width:80%;\">\r\n  			<div class=\"input-group form-group has-feedback\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Name：</span>\r\n  				<input type=\"text\" class=\"form-control\"  id=\"positionName\" name=\"positionName\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\" onblur=\"this.value=this.value.trim();verifyName(this,'"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "','"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_type : stack1), depth0))
    + "','"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "')\" >\r\n          <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n  				<input type=\"hidden\"  id=\"positionName_old\" name=\"positionName_old\"  value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_name : stack1), depth0))
    + "\">\r\n			    	<input type=\"hidden\"  id=\"position_id\" name=\"position_id\"  value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_id : stack1), depth0))
    + "\">\r\n			    	<input type=\"hidden\"  id=\"ps_id\" name=\"ps_id\"  value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\"> \r\n  			</div>\r\n  			<div class=\"input-group form-group has-feedback\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">X position：</span>\r\n  				<input class=\"form-control\" type=\"text\"  id=\"x\" name=\"x\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX');\"/>\r\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \r\n  			</div>\r\n  			<div class=\"input-group form-group has-feedback\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Y position：</span>\r\n  				<input class=\"form-control\" type=\"text\"  id=\"y\" name=\"y\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY');\"/> \r\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \r\n  			</div>\r\n  			<div class=\"input-group form-group has-feedback\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">X length：</span>\r\n  				<input type=\"text\" class=\"form-control\"  id=\"height\" name=\"height\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.height : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight');\"/>\r\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span> \r\n  			</div>\r\n  			<div class=\"input-group form-group has-feedback\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Y length：</span>\r\n  				<input type=\"text\" class=\"form-control\"  id=\"width\" name=\"width\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.width : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth');\"/>\r\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>  \r\n  			</div>\r\n  			<div class=\"input-group form-group has-feedback\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Offset angle：</span>\r\n  				<input type=\"text\" class=\"form-control\"  id=\"angle\" name=\"angle\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.angle : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle');\"/> \r\n  				<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n  			</div>\r\n        <div id=\"_dimensional\" class=\"input-group form-group has-feedback\" style=\"display:none;width: 98%;\">\r\n        <span class=\"input-group-addon\" style=\"width: 38%;\">Dimensional：</span>\r\n          <select class=\"gis_main_select\" name=\"is_three_dimensional\" style=\"width: 163px;\">\r\n            <option value=\"0\">2D</option>\r\n            <option value=\"1\">3D</option>\r\n          </select>\r\n          <span class=\"glyphicon form-control-feedback\"></span>\r\n        </div>\r\n  			<div class=\"input-group form-group has-feedback\" id=\"_zone\" style=\"display: none\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Zone：</span>\r\n  				<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('modify_data')\" onfocus=\"selectAreaSingle('modify_data')\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n				<input type=\"hidden\" name=\"area_id\" id=\"area_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\"/>\r\n  			</div>\r\n  			<div class=\"input-group form-group has-feedback\" id=\"_dock\" style=\"display: none\">\r\n  				<span class=\"input-group-addon\" style=\"width: 35%;\">Dock：</span>\r\n  				<input type=\"text\" class=\"form-control\"  name=\"dock\" id=\"dock\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.doorid : stack1), depth0))
    + "\" onfocus=\"selectDoor()\" onclick=\"selectDoor()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n				<input type=\"hidden\" name=\"sd_id\" id=\"sd_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.sd_id : stack1), depth0))
    + "\"  />\r\n  			</div>\r\n  		</div>\r\n		 <input type=\"hidden\" name=\"location_type\" id=\"type\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.obj_type : stack1), depth0))
    + "\"/>\r\n     \r\n  	</form>\r\n				\r\n</div>";
},"useData":true});
templates['modify_printer'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "<form id=\""
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "\"  class=\"form-horizontal\" role=\"form\">\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin: 4px 1px;border: 1px solid #ccc;width: 100%;background: #eee;border-radius: 5px;\">\r\n			 <input type=\"radio\" style=\"margin:10px;\" name=\"radioType\" value=\"0\"  onclick=\"onSelect('"
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"/>IP-Printer	\r\n			 <input type=\"radio\" style=\"margin:10px;\" name=\"radioType\" value=\"1\" onclick=\"onSelect('"
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"/>Servers-Printer\r\n		  </div> \r\n		  <div class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\r\n		  	<span class=\"input-group-addon\" style=\"width:40%;\">Name:</span>\r\n		    <input type=\"text\" class=\"form-control area_input\" name=\"name\" id=\"name\" onchange=\"verifyName(this);\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" />\r\n		    <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n		  </div>\r\n		  \r\n		  <div id=\"_servers\"  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;display: none;\">\r\n		  	<span class=\"input-group-addon\" style=\"width:40%;\">Servers:</span>\r\n	  		<input type=\"text\" class=\"form-control area_input\" name=\"serversName\" id=\"servers_name\" value=\"Choose Servers!\" onClick=\"add_printerServers('','"
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"  onfocus=\"add_printerServers('','"
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "')\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\">\r\n	  		<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n			<input type=\"hidden\" name=\"servers\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.ps_id : depth0)) != null ? stack1.servers : stack1), depth0))
    + "\" id=\"servers_id\">\r\n		  </div>\r\n		  <div id=\"_ip\"  class=\"input-group form-group has-feedback \" style=\"margin: 4px 0;width:310px;display: none;\">\r\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> IP:</span>\r\n	  	    <input type=\"text\" class=\"form-control area_input\" name=\"ip\" id=\"ip\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ip : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyIP(this)\"/>\r\n	  	    <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n		  </div>\r\n		  <div id=\"_port\"  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;display: none;\">\r\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> Port:</span>\r\n		  	<input type=\"text\" class=\"form-control area_input\" name=\"port\" id=\"port\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.port : stack1), depth0))
    + "\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyPort')\"/>\r\n		  	<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n		  </div>\r\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\r\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> X Position:</span>\r\n		  	<input type=\"text\" class=\"form-control area_input\" name=\"x\" id=\"x\"value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verifyX')\"/>\r\n		  	<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n		  </div>\r\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\r\n		  	<span class=\"input-group-addon\" style=\"width:40%;\"> Y Position:</span>\r\n	  	    <input type=\"text\" class=\"form-control area_input\" name=\"y\" id=\"y\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verifyY')\" />\r\n	  	    <span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n		  </div>\r\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\r\n		  	<span class=\"input-group-addon\" style=\"width: 125px;\" >  Type:</span>\r\n  	     		<select name=\"type\"  class=\"gis_main_select\" onchange=\"setOption()\">\r\n					<option class=\"select-option\" value=\"0\">Label </option>\r\n					<option class=\"select-option\" value=\"1\">Letter/A4</option>\r\n				</select>\r\n		  </div> \r\n		  \r\n		  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\r\n		  	<span class=\"input-group-addon\" style=\"width: 125px;\"> Size:</span>\r\n	  	    <select name=\"size\"   class=\"gis_main_select\" >\r\n					<option class=\"select-option\" value='0'>60*30</option>\r\n					<option class=\"select-option\" value='1'>80*35</option>\r\n					<option class=\"select-option\" value='2'>80*40</option>\r\n					<option class=\"select-option\" value='3'>100*50</option>\r\n					<option class=\"select-option\" value='4'>120*152</option>\r\n			</select>\r\n		  </div>\r\n	  	  <div  class=\"input-group form-group has-feedback\" style=\"margin: 4px 0;width:310px;\">\r\n	  		  <span class=\"input-group-addon\" style=\"width:40%;\"> Physical Area:</span>\r\n		  	  <input type=\"text\" class=\"form-control area_input\" name=\"zone\" id=\"zone\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_name : stack1), depth0))
    + "\" onclick=\"selectAreaSingle('"
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "')\" onfocus=\"selectAreaSingle('"
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "')\"  readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n		  </div>\r\n	    <input name=\"area_id\" id=\"area_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\" />\r\n		<input id=\"p_id\" name=\"p_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.p_id : stack1), depth0))
    + "\" />\r\n		<input id =\"ps_id\" name=\"ps_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\" />\r\n	</div>\r\n</form>";
},"useData":true});
templates['modify_storage_layer'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"modify_storage_layer_div\">\r\n	<div class=\"btn-group\" data-toggle=\"buttons\" style=\"margin-bottom: 5px;border-bottom: 1px solid #ddd;width: 100%;\">\r\n	  <label class=\"btn active\">\r\n	    <input type=\"radio\" name=\"options\" id=\"singleLayer\" value=\"0\" autocomplete=\"off\" checked=\"checked\"> Single layer\r\n	  </label>\r\n	  <label class=\"btn\">\r\n	    <input type=\"radio\" name=\"options\" id=\"multiLayer\" value=\"1\" autocomplete=\"off\"> Multi layer\r\n	  </label>\r\n	</div>\r\n	<form id=\"modify_storage_layer\">\r\n		<input type=\"hidden\" id =\"storage_id\" name=\"ps_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" name=\"storage_name\" id =\"storage_name\" value=\""
    + escapeExpression(((helper = (helper = helpers.storageName || (depth0 != null ? depth0.storageName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"storageName","hash":{},"data":data}) : helper)))
    + "\">\r\n		<div class=\"layerTypeRadio\" style=\"width: 98%;height: 34px;margin-bottom: 5px;border-radius: 5px;border: 1px solid #ccc;\">\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"1\" onclick=\"typeChange(1)\">Location</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"2\" onclick=\"typeChange(2)\">Staging</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"3\" onclick=\"typeChange(3)\">Dock</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"4\" onclick=\"typeChange(4)\">Parking</div>\r\n			 <div style=\"width:20%;float:left;margin-top:4px;\"><input type=\"radio\"  name=\"type\" value=\"5\" onclick=\"typeChange(5)\" >Zone</div>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Name：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\" value=\"\" onchange=\"this.value=this.value.trim();verifyName_(this,'"
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "','','','add')\" />\r\n			<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\r\n		</div> \r\n		\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X Position：</span>\r\n			<input type=\"text\" class=\"form-control\"  name=\"x\" id=\"x\" value=\"\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyX')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y Position：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"y\" id=\"y\" value=\"\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyY')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">X length：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"height\" id=\"height\" value=\"50\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHeight')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Y length：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"width\" id=\"width\" value=\"50\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyWidth')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div class=\"input-group form-group has-feedback\" style=\"margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Offset angle：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"angle\" id=\"angle\" value=\"0\"  onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyAngle')\" readonly=\"readonly\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<div id=\"_title\" class=\"input-group form-group has-feedback\" style=\"display: none;margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Title：</span>\r\n			<input type=\"text\" class=\"form-control\"  name=\"title\" id=\"title\" value=\"\" onclick=\"addLayerSelectTitle()\" onfocus=\"addLayerSelectTitle()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n			<input type=\"hidden\" style=\"width: 80%;\" name=\"title_id\" id=\"title_id\" value=\"\" />\r\n		</div>\r\n		<div id=\"_zone\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Zone：</span>\r\n			<input type=\"text\" class=\"form-control\"  name=\"zone\" id=\"zone\" value=\"\" onclick=\"selectAreaSingle('modify_storage_layer')\" onfocus=\"selectAreaSingle('modify_storage_layer')\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n			<span class=\"glyphicon form-control-feedback\" ></span>\r\n			<input type=\"hidden\" style=\"width: 80%;\" name=\"area_id\" id=\"area_id\" value=\"\"/>\r\n		</div>\r\n		<div id=\"_dock\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Dock：</span>\r\n			<input type=\"text\" class=\"form-control\" name=\"dock\" id=\"dock\" value=\"\" onclick=\"addLayerSelectDoor()\" onfocus=\"addLayerSelectDoor()\" readonly=\"readonly\" style=\"background-color: white;cursor: pointer;\"/>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n			<input type=\"hidden\" style=\"width: 80%;\" name=\"sd_id\" id=\"sd_id\" value=\"\"  />\r\n		</div>\r\n		<div id=\"_dimensional\" class=\"input-group form-group has-feedback\" style=\"display:none;margin-bottom: 5px;width: 98%;\">\r\n			<span class=\"input-group-addon\" style=\"width: 38%;\">Dimensional：</span>\r\n			<select class=\"gis_main_select\" name=\"is_three_dimensional\" style=\"width: 210px;\">\r\n				<option value=\"0\">2D</option>\r\n				<option value=\"1\">3D</option>\r\n			</select>\r\n			<span class=\"glyphicon form-control-feedback\"></span>\r\n		</div>\r\n		<!--<div id=\"set_l\">\r\n			<input type=\"checkbox\" style=\"margin-left: 33%;\" name=\"set_local\" value=\"setLocation\" onclick=\"setLocation()\"><span class=\"input-group-add\">Draw Child Layer</span>\r\n		</div>-->\r\n		<div id=\"set_location\" style=\"display:none;\">\r\n			<div class=\"childrenLayerType\" style=\"width:100%;height:25px;\">\r\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"1\" checked=\"checked\">Location</div>\r\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"2\">Staging</div>\r\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"3\">Dock</div>\r\n				<div style=\"width:25%;float:left;\"><input type=\"radio\" name=\"child_type\" value=\"4\">Parking</div>\r\n			</div>\r\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">vertically:</span>\r\n				<input type=\"text\" class=\"form-control\" value=\"\" id=\"vertically\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVer')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n			</div>\r\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">horizontally:</span>\r\n				<input type=\"text\" class=\"form-control\" value=\"\" id=\"horizontally\"   onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHor')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\">\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n			</div>\r\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">Loc Name:</span>\r\n				<input type=\"text\" class=\"form-control\" value=\"\" id=\"loc_name\"   onchange=\"this.value=this.value.trim()\" >\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n			</div>\r\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\">\r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">V_Interval:</span>\r\n				<input type=\"text\" class=\"form-control\" value=\"0\" id=\"v_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyVi')\" >\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n			</div>\r\n			<div class=\"input-group form-group has-feedback\" style=\"width: 98%;\"> \r\n				<span class=\"input-group-addon\" style=\"width: 38%;\">H_Interval:</span>\r\n				<input type=\"text\" class=\"form-control\" value=\"0\" id=\"h_interval\" onchange=\"this.value=this.value.trim();verifyNumber(this,'verifyHi')\"  >\r\n				<span class=\"glyphicon form-control-feedback\"></span>\r\n			</div>\r\n			<div style=\"margin-bottom: 5px;float: right;\">\r\n				<input type=\"button\" class=\"btn btn-success btn-sm\" value=\"Create\"   onclick=\"createLocation()\" >\r\n				<input type=\"button\" class=\"btn btn-info btn-sm\" value=\"Clear\" onclick=\"clearAutoLayer()\"> \r\n			</div>\r\n		</div>\r\n	</form>\r\n</div>\r\n<style type=\"text/css\">\r\n .modify_storage_layer_div .mCSB_inside .mCSB_container{margin-right:0px;}\r\n .modify_storage_layer_div .mCSB_scrollTools{right:-6px}\r\n\r\n</style>";
},"useData":true});
templates['printerServers'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.p_serviers_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    	 	 					<tr class=\"choicePrinterServer\" data-serverId=\""
    + escapeExpression(((helper = (helper = helpers.printer_server_id || (depth0 != null ? depth0.printer_server_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_id","hash":{},"data":data}) : helper)))
    + "\" data-severName=\""
    + escapeExpression(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "\">\r\n    	 	 						<td title=\""
    + escapeExpression(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "</td>\r\n    	 	 						<td title=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\r\n    	 	 						<td title=\""
    + escapeExpression(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_name","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_name","hash":{},"data":data}) : helper)))
    + "</td>\r\n     	 	 						<td style=\"text-align: center;\">\r\n     	 	 							<input type=\"checkbox\" >\r\n    	 	 							\r\n    	 	 							&nbsp;\r\n    	 	 						</td>\r\n    	 	 					</tr> \r\n";
},"4":function(depth0,helpers,partials,data) {
  return "    			<tr>\r\n              <td colspan=\"4\" style=\"text-align:center;line-height:155px;height:155px;background:#e8edff;border:1px solid silver;\">No Records</td>      \r\n          </tr>\r\n";
  },"6":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "     <table class=\"table table-condensed no-margin bg-info\" style=\"position: absolute;top:197px;\">\r\n      <thead>\r\n          <tr>\r\n            <td>\r\n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n                      <span class=\"glyphicon glyphicon-step-backward\"></span>\r\n                      首页\r\n                    </button>&nbsp;&nbsp; \r\n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n                      <span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n                      上一页\r\n                    </button>&nbsp;&nbsp; \r\n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n                      <span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n                      下一页\r\n                    </button>&nbsp;&nbsp; \r\n                    <button class=\"btn btn-default btn-xs selectPServers\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n                      <span class=\"glyphicon glyphicon-step-forward\"></span>\r\n                      末页\r\n                    </button>\r\n                </td>\r\n                <td width=\"200px\">\r\n                    <span class=\"pull-right small\">\r\n                      当前："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\r\n                      页数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\r\n                      总数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;  \r\n                    </span>\r\n            </td>\r\n          </tr>\r\n    </thead>\r\n  </table>  \r\n";
},"7":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = " 	<div style=\"position: relative;\">  \r\n    <span class=\"btn btn-success fileinput-button\" onclick=\"addPrinterServer();\">\r\n  	 	 	<i class=\"icon-plus icon-white\"></i>\r\n			AddPrintServer\r\n		</span>\r\n    <div style=\"height:152px;background-color: #e8edff;\">\r\n    <table  class=\"table table-hover printer_servers\" style=\"margin-bottom: 0px;margin-left: 0;\">\r\n	    <thead> \r\n	        <th scope=\"col\">Server Name</th>\r\n  	    	<th scope=\"col\" style=\"width:20%;\">Storage</th> \r\n  	    	<th scope=\"col\">Area Name</th> \r\n  	     	<th scope=\"col\" style=\"width:15%;\">Opera</th> \r\n	    </thead> \r\n	   <tbody>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isExistData : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.isExistData : depth0), {"name":"unless","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	   </tbody>\r\n  	 </table>\r\n     </div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isExistData : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "  </div>\r\n";
},"useData":true});
templates['printer_server_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.p_serviers_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "      <tr style=\"border-bottom: 1px solid #ddd;\">\r\n         <td style=\"padding: 2px;text-align: center;\" title="
    + escapeExpression(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + ">"
    + escapeExpression(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "</td>\r\n         <td style=\"padding: 2px;text-align: center;\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\r\n         <td style=\"padding: 2px;text-align: center;\" title="
    + escapeExpression(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_name","hash":{},"data":data}) : helper)))
    + ">"
    + escapeExpression(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_name","hash":{},"data":data}) : helper)))
    + "</td>\r\n         <td style=\"padding: 2px;text-align: center;\"><img style=\"width:40px;\" src=\"./imgs/print_delete.png\" onclick=\"dialogDeletePServer("
    + escapeExpression(((helper = (helper = helpers.printer_server_id || (depth0 != null ? depth0.printer_server_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_id","hash":{},"data":data}) : helper)))
    + ",'"
    + escapeExpression(((helper = (helper = helpers.printer_server_name || (depth0 != null ? depth0.printer_server_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer_server_name","hash":{},"data":data}) : helper)))
    + "')\"/></td>\r\n      </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "		<tr>\r\n		  <td style=\"padding: 2px;text-align: center;\" colspan=\"4\">\r\n			No Records		\r\n		  </td>	\r\n		</tr>\r\n";
  },"6":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "<div style=\"background: white;height: 280px;\">\r\n<div style=\"width:100%;height:35px; float:left;\">\r\n	<input style=\"margin-left:1%;\" type=\"button\" class='btn btn-success' value=\"Add\" onclick=\"addPrinterServer("
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + ")\" />\r\n</div>\r\n<div style=\"width:100%;overflow-y:auto;float:left;position: relative;height:200px;\">\r\n<table class=\"table table-hover\" style=\"margin-bottom: 0px;position:relative;\">\r\n  <thead>\r\n     <tr>\r\n         <th style=\"width:28%;text-align: center;padding: 2px;font-size:12px;\">Name</th>\r\n         <th style=\"width:26%;text-align: center;padding: 2px;font-size:12px;\">Storage</th>\r\n         <th style=\"width:26%;text-align: center;padding: 2px;font-size:12px;\">Zone</th>\r\n         <th style=\"width:20%;text-align: center;padding: 2px;font-size:12px;\">Opera</th>\r\n     </tr>\r\n  </thead>\r\n <tbody> \r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isExistData : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.isExistData : depth0), {"name":"unless","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "   </tbody>\r\n</table>\r\n <table class=\"table table-condensed no-margin bg-info\" style=\"margin-bottom:0px;position: absolute;top: 155px;\">\r\n  	<thead>\r\n  			<tr>\r\n	    		<td width=\"100%\">\r\n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\r\n	                	First\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n	                	Last\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n	                	Next\r\n	                </button>&nbsp;&nbsp; \r\n	                <button style=\"width:22%;\" class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\r\n	                	End\r\n	                </button>\r\n	            </td>\r\n	    	</tr>\r\n	</thead>\r\n  </table>  \r\n</div>\r\n</div>";
},"useData":true});
templates['query_history'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "							    		<option value='"
    + escapeExpression(((helper = (helper = helpers.val || (depth0 != null ? depth0.val : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"val","hash":{},"data":data}) : helper)))
    + "'>"
    + escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"text","hash":{},"data":data}) : helper)))
    + "</option> \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n		  <tr> \r\n		    <td colspan=\"2\" align=\"center\" valign=\"top\">\r\n				  <fieldset style=\"border:2px #cccccc solid;padding:15px;margin:10px;width:90%;\">\r\n						<table >   \r\n							<tr style=\"display: none;\">\r\n								<td align=\"right\" valign=\"middle\">Time zone：</td>\r\n							    <td align=\"left\" valign=\"middle\">\r\n							    	<select class=\"txt\" style=\"width: 100%;\" id=\"timeZone\" name=\"timeZone\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "							    	</select>\r\n					            </td>\r\n					   	    </tr>\r\n							<tr>\r\n								<td align=\"right\" valign=\"middle\" width=\"60px;\">Begin：</td>\r\n							    <td align=\"left\" valign=\"middle\">\r\n					              <input type=\"text\" readonly class=\"form-datatime form-control\" id=\"startTime\" name=\"startTime\" style=\"cursor: pointer;\r\n\" /> \r\n					            </td>\r\n					   	    </tr>\r\n					   	    <tr>\r\n					   	    	<td align=\"right\" valign=\"middle\">End： </td>\r\n					                  		  \r\n					            <td align=\"left\" valign=\"middle\">     		  \r\n					              <input type=\"text\" readonly class=\"form-datatime form-control\" id=\"endTime\" name=\"endTime\"\" style=\"cursor: pointer;\r\n\"/>\r\n					              <input type=\"hidden\" name=\"aid\" id=\"truck_aid\" value=\""
    + escapeExpression(((helper = (helper = helpers.aid || (depth0 != null ? depth0.aid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"aid","hash":{},"data":data}) : helper)))
    + "\"/>\r\n					            </td>\r\n						   </tr>\r\n						 </table>\r\n					  </fieldset>\r\n	           </td>\r\n	        </tr>\r\n	        <tr>\r\n		   	      <td colspan=\"2\" style=\"vertical-align: middle;\">\r\n		   	   		<span id=\"queryTip\" style=\"color:red;\"></span>\r\n		   	   	  </td>\r\n	   	   </tr>\r\n		   \r\n	   </table>";
},"useData":true});
templates['rightclick_menus'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		<li id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"map_rightClick\"  style=\"display: none;\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<a href=\"#\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</a></li>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n<ul>\r\n	<li id=\"xy\" style=\"display: none;\"></li>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</ul>\r\n\r\n			";
},"useData":true});
templates['search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<input id=\"keywordTxt\" class=\"txt300 floatl\" value=\"\">\r\n<input class=\"magnifier_button floatl\" type=\"submit\" value=\"\">\r\n<a  id =\"return\" style=\"color: rgb(103, 103, 103); text-align: center;\" >返&nbsp;&nbsp;回</a> \r\n";
  },"useData":true});
templates['selectDoor'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.exit_dock_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "							<div style=\"width: 110px; height: 15px; border: 1px solid rgb(204, 204, 204); margin: 5px; float: left; cursor: default;\" title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\">\r\n								<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'sd_id')\">\r\n									<input type=\"hidden\" id=\"sd_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n								"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "</div>\r\n								<div style=\"width: 12px; height: 12px; float: right; margin: 1px; cursor: pointer; background-image: url(http://localhost/Sync10-ui/pages/gis/imgs/delete_grey.png);\" onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\" onclick=\"removeTitle_(this.parentNode,'sd_id')\">\r\n								</div>\r\n							</div>\r\n\r\n								<!--<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,"
    + escapeExpression(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adid","hash":{},"data":data}) : helper)))
    + ")\">\r\n										"
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\r\n										<input type=\"hidden\" id=\"title_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\r\n									</div>\r\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n											onclick=\"removeTitle_(this.parentNode,"
    + escapeExpression(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adid","hash":{},"data":data}) : helper)))
    + ")\">\r\n									</div>\r\n								</div>-->\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "							<center>\r\n								no data\r\n							</center>\r\n";
  },"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 105px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n						<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode,'sd_id')\">\r\n							"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n							<input type=\"hidden\" id=\"sd_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n						</div>\r\n						<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n									onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n									onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n									onclick=\"addTitle(this.parentNode,'sd_id')\">\r\n						</div>\r\n					</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"about_the_layout_div\">\r\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Docks</div> \r\n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Docks</div>\r\n	  <div class=\"about_the_layout_div_centext margin_right\">\r\n			<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.exit_dock_data : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.exit_dock_data : depth0), {"name":"unless","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</div>\r\n		</div>	\r\n	    <div  class=\"about_the_layout_div_centext margin_left\">\r\n			<div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.door_data : depth0), {"name":"each","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\r\n	 </div>\r\n</div>\r\n	<!-- model -->\r\n	<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n		<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'sd_id')\">\r\n			<input type=\"hidden\" id=\"sd_id\">\r\n		</div>\r\n		<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n				onclick=\"removeTitle_(this.parentNode,'sd_id')\">\r\n		</div>\r\n	</div>\r\n	<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n		<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\r\n			<input type=\"hidden\" id=\"sd_id\">\r\n		</div>\r\n		<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n					onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n					onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n					onclick=\"addTitle(this.parentNode)\">\r\n		</div>\r\n	</div>\r\n	<!-- model end-->\r\n	";
},"useData":true});
templates['selectPerson'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "								<div title=\""
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'title_id')\">\r\n										"
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\r\n										<input type=\"hidden\" id=\"title_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\r\n									</div>\r\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n											onclick=\"removeTitle_(this.parentNode,'title_id')\">\r\n									</div>\r\n								</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "							<center>\r\n								no data\r\n							</center>\r\n";
  },"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<div title=\""
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n						<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode,'title_id')\">\r\n							"
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\r\n							<input type=\"hidden\" id=\"title_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\r\n						</div>\r\n						<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n									onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n									onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n									onclick=\"addTitle(this.parentNode,'title_id')\">\r\n						</div>\r\n					</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"about_the_layout_div\">\r\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Person</div> \r\n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Person</div>\r\n	  <div class=\"about_the_layout_div_centext margin_right\">\r\n	  	  		<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n					\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.exit_title_data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "							\r\n";
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.exit_title_data : depth0), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "			 </div>\r\n	  </div>\r\n      <div  class=\"about_the_layout_div_centext margin_left\">\r\n		<div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.person_data : depth0), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\r\n	 </div>\r\n</div>\r\n<!-- model -->\r\n<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n	<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'title_id')\">\r\n		<input type=\"hidden\" id=\"title_id\">\r\n	</div>\r\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n			onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n			onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n			onclick=\"removeTitle_(this.parentNode,'title_id')\">\r\n	</div>\r\n</div>\r\n<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n	<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\r\n		<input type=\"hidden\" id=\"title_id\">\r\n	</div>\r\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n				onclick=\"addTitle(this.parentNode)\">\r\n	</div>\r\n</div>\r\n<!-- model end-->";
},"useData":true});
templates['select_area'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.area_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "							<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n								<div class=\"text-overflow location_updata_area\" style=\"width: 85%; float: left; \" >\r\n									"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n									<input type=\"hidden\" id=\"area_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n								</div>\r\n								<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" class=\"location_updata_area\"\r\n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n											>\r\n								</div>\r\n							</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "						<center>\r\n							no data\r\n						</center>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px; width:650px;height:420px;\">\r\n	    <table style=\"width: 98%;margin:0 1%;height:auto;\">\r\n	    	<tr> \r\n	    		<td>\r\n					<div id=\"notExitDoor\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:400px; overflow:auto\" >\r\n";
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.dataIsNull : depth0), {"name":"unless","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.dataIsNull : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "					</div>\r\n				</td>\r\n	    	</tr>\r\n	    </table>\r\n	</div>";
},"useData":true});
templates['select_area_single'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n\r\n						<div title=\""
    + escapeExpression(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n							<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"confirm1(this.parentNode)\">\r\n								"
    + escapeExpression(((helper = (helper = helpers.area_name || (depth0 != null ? depth0.area_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_name","hash":{},"data":data}) : helper)))
    + "\r\n								<input type=\"hidden\" id=\"area_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.area_id || (depth0 != null ? depth0.area_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_id","hash":{},"data":data}) : helper)))
    + "\">\r\n							</div>\r\n							<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n										onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n										onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n										onclick=\"confirm1(this.parentNode)\">\r\n							</div>\r\n						</div>\r\n										\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:450px;height:400px;\">\r\n	    <table style=\"width: 100%;\">\r\n	    	<tr> \r\n	    		<td>\r\n					<div id=\"notExitDoor\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.area_data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n					</div>\r\n				</td>\r\n	    	</tr>\r\n	    </table>\r\n	</div>";
},"useData":true});
templates['select_door_single'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.door_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "								<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n									<div class=\"text-overflow location_updata_doors\" style=\"width: 85%; float: left; \" >\r\n										"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n										<input type=\"hidden\" id=\"sd_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n									</div>\r\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n												onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n												onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n												class=\"location_updata_door_add_img\"\r\n												>\r\n									</div>\r\n								</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "							<center>\r\n								no data\r\n							</center>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div style=\"width:760px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;\">\r\n	    <table style=\"width: 100%;\">\r\n	    	<tr> \r\n	    		<td>\r\n					<div id=\"notExitDoor\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.doorDataIsNull : depth0), {"name":"unless","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.doorDataIsNull : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "					</div>\r\n				</td>\r\n	    	</tr>\r\n	    </table>\r\n	</div>";
},"useData":true});
templates['select_title'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.exit_title_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "								<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,"
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\r\n										"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n										<input type=\"hidden\" id=\"title_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n									</div>\r\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n											onclick=\"removeTitle_(this.parentNode,"
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\r\n									</div>\r\n								</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "							<center>\r\n								no data\r\n							</center>\r\n";
  },"6":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.notExit_title : depth0), {"name":"each","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n									<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n										<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\r\n											"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n											<input type=\"hidden\" id=\"title_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n										</div>\r\n										<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n													onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n													onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n													onclick=\"addTitle(this.parentNode)\">\r\n										</div>\r\n									</div>\r\n";
},"9":function(depth0,helpers,partials,data) {
  return "						<center>\r\n							no data\r\n						</center>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"about_the_layout_div\">\r\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Title</div> \r\n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Title</div>\r\n	  <div class=\"about_the_layout_div_centext margin_right\">\r\n	  		<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.exit_title_data : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.exit_title_data : depth0), {"name":"unless","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "			 </div>\r\n	  </div>		 \r\n	  <div  class=\"about_the_layout_div_centext margin_left\">\r\n			 <div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.notExit_title : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.notExit_title : depth0), {"name":"unless","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\r\n	 </div>\r\n</div>\r\n<!-- model -->\r\n<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n	<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,'title_id')\">\r\n		<input type=\"hidden\" id=\"title_id\">\r\n	</div>\r\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n			onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n			onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n			onclick=\"removeTitle_(this.parentNode,'title_id')\">\r\n	</div>\r\n</div>\r\n<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n	<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\r\n		<input type=\"hidden\" id=\"title_id\">\r\n	</div>\r\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n				onclick=\"addTitle(this.parentNode)\">\r\n	</div>\r\n</div>\r\n<!-- model end-->\r\n";
},"useData":true});
templates['select_zone'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.exitZone_data : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "								<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n									<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode,"
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\r\n										"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n										<input type=\"hidden\" id=\"area_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n									</div>\r\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n											onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n											onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n											onclick=\"removeTitle_(this.parentNode,"
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + ")\">\r\n									</div>\r\n								</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "							<center>\r\n								no data\r\n							</center>\r\n";
  },"6":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.zone_data : depth0), {"name":"each","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n								<div title=\""
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\" style=\"width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n									<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\r\n										"
    + escapeExpression(((helper = (helper = helpers.obj_name || (depth0 != null ? depth0.obj_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_name","hash":{},"data":data}) : helper)))
    + "\r\n										<input type=\"hidden\" id=\"area_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.obj_id || (depth0 != null ? depth0.obj_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"obj_id","hash":{},"data":data}) : helper)))
    + "\">\r\n									</div>\r\n									<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n												onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n												onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n												onclick=\"addTitle(this.parentNode)\">\r\n									</div>\r\n								</div>\r\n";
},"9":function(depth0,helpers,partials,data) {
  return "					<center>\r\n						no data\r\n					</center>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"about_the_layout_div\">\r\n	  <div class=\"about_the_layout_label margin_right\" >&nbsp;Existence Zone</div> \r\n	  <div class=\"about_the_layout_label margin_left\" >&nbsp;Free Zone</div>\r\n	  <div class=\"about_the_layout_div_centext margin_right\">\r\n					<div id=\"exitTitle\" style=\"background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.exitZone : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.exitZone : depth0), {"name":"unless","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "					</div>\r\n		  </div>	\r\n		  <div  class=\"about_the_layout_div_centext margin_left\">\r\n				<div id=\"notExitTitle\" style=\"background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto\" >\r\n";
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.zoneDataIsNotExit : depth0), {"name":"unless","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.zoneDataIsNotExit : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			  </div>\r\n		</div>\r\n</div>\r\n<!-- model -->\r\n<div id=\"title_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n	<div class=\"text-overflow\"  style=\"width: 85%; float: left; \" ondblclick=\"removeTitle_(this.parentNode)\">\r\n		<input type=\"hidden\" id=\"area_id\">\r\n	</div>\r\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/delete_grey.png'); margin: 1px; cursor: pointer;\" \r\n			onmousemove=\"this.style.backgroundImage='url(&quot;imgs/delete_blue.png&quot;)'\" \r\n			onmouseout=\"this.style.backgroundImage='url(&quot;imgs/delete_grey.png&quot;)'\"\r\n			onclick=\"removeTitle_(this.parentNode)\">\r\n	</div>\r\n</div>\r\n<div id=\"title_free_model\" style=\"display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;\">\r\n	<div class=\"text-overflow\" style=\"width: 85%; float: left; \" ondblclick=\"addTitle(this.parentNode)\">\r\n		<input type=\"hidden\" id=\"area_id\">\r\n	</div>\r\n	<div style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_grey.png'); margin: 1px; cursor: pointer;\" \r\n				onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\" \r\n				onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\"\r\n				onclick=\"addTitle(this.parentNode)\">\r\n	</div>\r\n</div>\r\n<!-- model end-->";
},"useData":true});
templates['set_refresh_time'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<form id=\"setTime\">\r\n		<table border=\"0\" style=\"width: 100%;height: 100%\" cellpadding=\"0\" cellspacing=\"0\">\r\n		<tr> \r\n		<td align=\"right\" width=\"40%\">\r\n		Truck Refresh Time:\r\n		</td>\r\n		<td align=\"left\" width=\"60%\">\r\n		 <input type=\"text\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.timeData : depth0)) != null ? stack1.truck : stack1), depth0))
    + "\" id=\"truck_time\" style=\"width: 60%\" onchange=\"this.value=this.value.trim();verifyTt()\"/>\r\n		 <span style=\"color: red;\">(ms)</span>\r\n		 <img id=\"truck_time_con\" src=\"imgs/confirm.jpg\" style=\"display: none\">\r\n         <img id=\"truck_time_del\" title=\"Data Error!\" src=\"imgs/delete_red.jpg\" style=\"display: none\">\r\n		</td>\r\n		</tr>\r\n		<tr>\r\n		<td align=\"right\" width=\"40%\">\r\n		Dock Refresh Time:\r\n		</td>\r\n		<td align=\"left\" width=\"60%\">\r\n		<input type=\"text\" id=\"dock_time\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.timeData : depth0)) != null ? stack1.dock : stack1), depth0))
    + "\" style=\" width: 60%\" onchange=\"this.value=this.value.trim();verifyDt()\" />\r\n		 <span style=\"color: red;\">(ms)</span> \r\n		 <img id=\"dock_time_con\" src=\"imgs/confirm.jpg\" style=\"display: none\">\r\n         <img id=\"dock_time_del\" title=\"Data Error!\" src=\"imgs/delete_red.jpg\" style=\"display: none\">\r\n		</td>\r\n		</tr>\r\n		</table>\r\n	</form>";
},"useData":true});
templates['showWebcam'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<embed type=\"application/x-vlc-plugin\" id=\"vlc\"\r\n			target=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.rtsp : stack1), depth0))
    + "\"\r\n			 height=\"100%\" width=\"100%\"\r\n			 autoplay=\"yes\" loop=\"no\"\r\n			 version=\"VideoLAN.VLCPlugin.2\"/>\r\n	  <div id=\"playerInfo\" style=\"display: none;\" align=\"center\" >\r\n	  	<span>Not install VLC player plugin</span>\r\n	  	</br>\r\n	  	<a href=\"javascript:\" onclick=\"getVlcPlayer()\">Get VLC media player</a>\r\n	  </div> ";
},"useData":true});
templates['showZoneDocks'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					<div title=\""
    + escapeExpression(((helper = (helper = helpers.doorid || (depth0 != null ? depth0.doorid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"doorid","hash":{},"data":data}) : helper)))
    + "\" class=\"zone_dock_div_2\">\r\n						<div class=\"zone_dock_div_3\" oncontextmenu=\"showMenu(this,event)\">\r\n							"
    + escapeExpression(((helper = (helper = helpers.doorid || (depth0 != null ? depth0.doorid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"doorid","hash":{},"data":data}) : helper)))
    + "\r\n							<input type=\"hidden\" id=\"sd_id\" name=\"sdid\" value=\""
    + escapeExpression(((helper = (helper = helpers.sd_id || (depth0 != null ? depth0.sd_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sd_id","hash":{},"data":data}) : helper)))
    + "\">\r\n							<div class=\"zone_dock_div_img\"\r\n							onclick=\"removeTitle(this.parentNode,'dock_names')\">\r\n							</div>\r\n						</div> \r\n					</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div id=\"showZoneDocks\" class=\"zone_dock_div\" tabindex='0'>\r\n		<input type=\"hidden\" id=\"areaName\" value=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"olddock_Ids\" value=\""
    + escapeExpression(((helper = (helper = helpers.olddock_Ids || (depth0 != null ? depth0.olddock_Ids : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"olddock_Ids","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"dock_names\" value=\""
    + escapeExpression(((helper = (helper = helpers.dock_names || (depth0 != null ? depth0.dock_names : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"dock_names","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"ps_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"area_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.area_id || (depth0 != null ? depth0.area_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<div id=\"notExitDoor\" class=\"zone_dock_div_1\" >\r\n				\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.docks_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "				<div title=\"add dock\" id=\"add_dock\" style=\"width: 100px; height: 25px; border: solid; border-color: #eeeeee; border-width: 1px; margin: 5px; float: left; cursor: default;\" >\r\n						<div class=\"text-overflow\" style=\"width: 100%;height:100%; float: left; text-align: center;background-color: #eeeeee;cursor: pointer;\" onclick=\"selectDock()\">\r\n							<div id=\"add_bg_img\" style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_blue.png'); margin: 1px; cursor: pointer;margin-top: 8px;margin-right: 50px;\" \r\n									onmouseover=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\" \r\n									onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\"\r\n									\">\r\n							</div>\r\n						</div>\r\n				</div>\r\n		</div>\r\n</div>";
},"useData":true});
templates['show_zone_person'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "				<div title=\""
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\" class=\"zone_person_div_2\" >\r\n					<div class=\"zone_dock_div_3\" oncontextmenu=\"showPersonMenu(this,event)\">\r\n						"
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\r\n						<input type=\"hidden\" class=\"adid\" name=\"adid\" value=\""
    + escapeExpression(((helper = (helper = helpers.adid || (depth0 != null ? depth0.adid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adid","hash":{},"data":data}) : helper)))
    + "\">\r\n						<input type=\"hidden\" class=\"ad_name\" name=\"ad_name\" value=\""
    + escapeExpression(((helper = (helper = helpers.employe_name || (depth0 != null ? depth0.employe_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_name","hash":{},"data":data}) : helper)))
    + "\">\r\n						<div class=\"zone_dock_div_img\" onclick=\"removeTitle(this.parentNode,'employe_names')\">\r\n						</div>\r\n					</div>\r\n				</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div id=\"zonePersons\" onclick=\"hidePersonMenu()\" class=\"zone_person_div\" >\r\n		<input type=\"hidden\" id=\"areaName\" value=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"adids\" value=\""
    + escapeExpression(((helper = (helper = helpers.adids || (depth0 != null ? depth0.adids : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"adids","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"ps_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"area_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.area_id || (depth0 != null ? depth0.area_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"area_id","hash":{},"data":data}) : helper)))
    + "\">\r\n		<input type=\"hidden\" id=\"employe_names\" value=\""
    + escapeExpression(((helper = (helper = helpers.employe_names || (depth0 != null ? depth0.employe_names : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"employe_names","hash":{},"data":data}) : helper)))
    + "\">\r\n		<div id=\"notExitDoor\" class=\"zone_person_div_1\" >\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.zonePerson_data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			<div title=\"add person\" id=\"add_person\" style=\"width: 130px; height: 25px; border: solid; border-color: #eeeeee; border-width: 1px; margin: 5px 14px; float: left; cursor: default;\" >\r\n				<div class=\"text-overflow\" style=\"width: 100%;height:100%; float: left; text-align: center;background-color: #eeeeee;cursor: pointer;\" onclick=\"selectPerson()\">\r\n					<div id=\"add_bg_img\" style=\"width: 12px; height: 12px; float: right; background-image: url('imgs/add_blue.png'); margin: 1px; cursor: pointer;margin: 6px 55px 0px 0px;\" \r\n							onmousemove=\"this.style.backgroundImage='url(&quot;imgs/add_grey.png&quot;)'\" \r\n							onmouseout=\"this.style.backgroundImage='url(&quot;imgs/add_blue.png&quot;)'\"\r\n							\">\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n</div>\r\n		<!-- 右键菜单样式 -->\r\n		<div id=\"menu_\" style=\"width:auto;\">\r\n			<ul>\r\n				<li id=\"assignArea\" onclick=\"assignArea(this)\" >\r\n				<a href=\"#\">Reassign </a></li>\r\n			</ul>\r\n		</div>";
},"useData":true});
templates['storageSecondMenus'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "        <div class=\"category_container category_container_Storage\" data-ps_id=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\r\n        <div class=\"storageSecondMenusBtn\" data-ps_id=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\r\n			<div class=\"category_icon_seconde "
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\" data-ps_id=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\r\n			</div>\r\n			"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\r\n		</div> \r\n		</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		   <div id=\"second_boxConR_storage\" data-pageplus=\"1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConR second_boxCon_storage\">\r\n		   </div>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		   <div id=\"second_boxConL_stroage\" data-pageplus=\"-1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConL second_boxCon_storage\">\r\n		   </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "     <div class=\"category_icon_collection\">\r\n     <div id=\"storageSecondMenuCon\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.menus_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pageplus_R : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pageplus_L : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n	 </div>\r\n                       \r\n\r\n\r\n";
},"useData":true});
templates['storageSencondTabs'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"storage_tabs\">\r\n	<div style=\"height: 20px;width: 98%;background: #aaf;margin: 0 1%;\">\r\n		<ul>\r\n			<li style=\"width: 50%;\">\r\n				<a href=\"#storage\">Storage</a>\r\n			</li>\r\n			<li style=\"width: 50%;\">\r\n				<a href=\"#alterStorage\">Edit Storage</a>\r\n			</li>\r\n		</ul> \r\n	</div>\r\n	<div id=\"storage\" style=\"width:100%\">\r\n		<div id=\"all_storage\"></div>\r\n		<div id=\"bottom_menus\"></div>\r\n	</div>\r\n	<div id=\"alterStorage\" style=\"width:100%;\"></div>\r\n</div>";
  },"useData":true});
templates['StorageSkeletonInfo'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		    			<option value=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"raised\">\r\n	<div class=\"boxcontent\">\r\n		Add Storage\r\n	</div> \r\n</div>\r\n<div class=\"maintain_storage\">\r\n<br/>\r\n<fieldset class=\"maintain_storage_fieldset\" style=\"border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;margin-bottom: 20px;\">\r\n	<legend style=\"font-size:15px;font-weight:normal;color:#999999;\">\r\n		Add New Storage\r\n	</legend>	 		\r\n	<form  id=\"maintain_storage_add_form\" method=\"post\" action=\"\" >\r\n		<table width=\"98%\" border=\"0\" cellspacing=\"5\" cellpadding=\"2\" class=\"maintain_add_storage_info\">\r\n		  <tr >\r\n		   \r\n		    <td align=\"left\" valign=\"middle\" >\r\n		    	<div class=\"input-group form-group has-feedback\">\r\n		    		<span class=\"input-group-addon\">Name</span>\r\n		    		<select name=\"ps_id\" class=\"gis_main_select\" style=\"height:34px;width: 100%;\">\r\n		    		<option value=\"0\">choose...</option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.storageData : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			    	</select> \r\n			    	<span class=\"glyphicon form-control-feedback\" style=\"top:0;\"></span>\r\n		    	</div>\r\n		    	\r\n		    </td>\r\n		  </tr>\r\n		  <tr >\r\n			<td>\r\n				<br>\r\n				<div style=\"position: relative;\" class=\"input-group\">\r\n					<span class=\"input-group-addon\">Locate</span>\r\n					<input type=\"text\" style=\"width:170px;\" class=\"form-control\" id=\"locationStorage\">\r\n					<img src=\"imgs/orientation_img.png\" onclick=\"locationStorage_()\" style=\"position: absolute;cursor: pointer;\" alt=\"Position the warehouse\"/>\r\n				<div>\r\n			</td>\r\n		  </tr>\r\n		  <tr>\r\n		  \r\n			\r\n			<td colspan=\"2\">\r\n				<div style=\"position: relative;\">\r\n					<input name=\"op\" value=\"1\" type=\"radio\" onclick=\"inMapsOptionStorage(this)\"/>Existing Points\r\n					<input name=\"op\" value=\"2\" type=\"radio\" onclick=\"inMapsOptionStorage(this)\"/>Draw Border\r\n\r\n\r\n				<div>\r\n			</td>\r\n		  </tr>\r\n		  <tr id=\"hand_fill_point\" style=\"display:none;\">\r\n			<td align=\"right\" valign=\"middle\" class=\"STYLE3\" >Points</td>\r\n			<td>\r\n				<div style=\"position: relative;\">\r\n					<input id=\"point_num\" value=\"\" type=\"text\" style=\"width:100px;\"/>\r\n					<input type=\"button\" id=\"maintain_drawStorage_next\" name=\"Submit2\" style=\"background: url(imgs/normal_green.gif); \" value=\"Next\" class=\"normal-green\" >\r\n				<div>\r\n			</td>\r\n		  </tr>\r\n		</table>\r\n		<table width=\"98%\" border=\"0\" cellspacing=\"5\" cellpadding=\"2\" class=\"maintain_add_storage_latLngs\" style=\"padding-right: 35px;\">\r\n			\r\n		</table>\r\n	</form>	\r\n</fieldset>\r\n\r\n<table style=\"width: 100%;\">\r\n  <tr style=\"width: 100%;\" align=\"right\">\r\n    <td style=\"width: 100%\">\r\n	  <input type=\"button\" id=\"maintain_addStorage_btn\" name=\"Submit2\"  value=\"Save\" class=\"btn btn-success disabled\" onClick=\"addMaintainStorage();\">\r\n	  <input type=\"button\" id=\"maintain_addStorage_cancel\" name=\"Submit3\" value=\"Canle\" class=\"btn btn-info disabled\" onClick=\"addStorageCancel();\">\r\n	</td>\r\n  </tr>\r\n</table> \r\n\r\n</div>";
},"useData":true});
templates['storageThirdlyMenus'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "      	<div class=\"storage_third_menus\"  data-ps_id=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-layer=\""
    + escapeExpression(((helper = (helper = helpers.layer || (depth0 != null ? depth0.layer : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"layer","hash":{},"data":data}) : helper)))
    + "\">\r\n      		<input type=\"checkbox\" id=\""
    + escapeExpression(((helper = (helper = helpers.layer || (depth0 != null ? depth0.layer : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"layer","hash":{},"data":data}) : helper)))
    + "\" class=\"storage_layer_checkbox\" data-ps_id=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-layer=\""
    + escapeExpression(((helper = (helper = helpers.layer || (depth0 != null ? depth0.layer : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"layer","hash":{},"data":data}) : helper)))
    + "\">&nbsp;&nbsp;\r\n			<span style='vertical-align : middle; color: #333333' data-ps_id=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\" data-kml=\""
    + escapeExpression(((helper = (helper = helpers.kml || (depth0 != null ? depth0.kml : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"kml","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</span> \r\n      	</div> \r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		   <div id=\"thirdly_boxConR\" data-pageplus=\"1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base thirdly_boxConR\">\r\n		   </div>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		   <div id=\"thirdly_boxConL\" data-pageplus=\"-1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base thirdly_boxConL\">\r\n		   </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n     <div class=\"category_icon_collection_third\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.menus_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pageplus_R : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pageplus_L : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n	 </div>\r\n\r\n";
},"useData":true});
templates['three_level_tabs'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<li>\r\n				<a href=\"#"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" >\r\n				"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\r\n				</a>\r\n			</li>	\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		    <div id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"width:98%;margin: 0 1%;\">\r\n		    </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"tabs\">\r\n	<div style=\"height:20px;width:98%;background:#aaf;margin:0 1%\">\r\n	<ul>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	</ul> \r\n	</div>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});
templates['truckMarkerRightMenu'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<ul class=\"truckMarkerMenu_ul\" >\r\n    <li class=\"truckMarkerMenu_li\" id=\"History\" >Tracking History\r\n    </li>\r\n    <li class=\"truckMarkerMenu_li\" id=\"LocFrequency\" >Positioning Frequency Set Up\r\n    </li>\r\n    <li class=\"truckMarkerMenu_li\" id=\"KeyPoint\" >Key Point Set Up\r\n    </li>\r\n    <li class=\"truckMarkerMenu_li\" id=\"Road\" >Route Set Up\r\n    </li>\r\n    \r\n    <li class=\"truckMarkerMenu_li\" id=\"GeoFence\" >Geofencing Set Up\r\n    </li> \r\n</ul>\r\n";
  },"useData":true});
templates['truckTemplate'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "	        <div class=\"category_container category_container_truck\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" data-group_id=\""
    + escapeExpression(((helper = (helper = helpers.group_id || (depth0 != null ? depth0.group_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_id","hash":{},"data":data}) : helper)))
    + "\" >\r\n	        	<div class=\"truck_btn\">\r\n					<div class=\"category_icon_seconde truck_second_menu "
    + escapeExpression(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" data-group_id=\""
    + escapeExpression(((helper = (helper = helpers.group_id || (depth0 != null ? depth0.group_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_id","hash":{},"data":data}) : helper)))
    + "\" ></div>\r\n					"
    + escapeExpression(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_name","hash":{},"data":data}) : helper)))
    + "\r\n				</div> \r\n			</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			   <div id=\"truck_second_boxConR\" data-pageplus=\"1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConR\">\r\n			   </div>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			   <div id=\"truck_second_boxConL\" data-pageplus=\"-1\" data-num=\""
    + escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"num","hash":{},"data":data}) : helper)))
    + "\" class=\"boxCon_base second_boxConL\">\r\n			   </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n	    <div style=\"width:98%;margin: 0 1%;height:20px;background: #ccc;\">\r\n		  <div class=\"boxcontent raised floatl\">Truck </div>\r\n		  </div>\r\n	     <div class=\"category_icon_collection_truck_second\" style=\"position: relative;\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.menus_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pageplus_R : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pageplus_L : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			<div style=\"float: left; width: 96%;bottom: 5px;position: absolute;\">\r\n				<input type=\"button\" style='margin: 5px 0 0 45%;height:30px; width:60px;font-size:9px' class=\"btn btn-success\" onclick=\"addAsset()\" value=\"Add\"/>\r\n				<input type=\"button\" style='margin: 5px 0 0 10px;height:30px; width:60px;font-size:9px' class=\"btn btn-primary\" onclick=\"refreshTruck()\" value=\"Refresh\"/>\r\n			</div>\r\n	   </div>\r\n\r\n ";
},"useData":true});
templates['truck_tree'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "		<div class=\"truck_third\">\r\n		   <div class=\"truck_img\" >\r\n			   <img src=\"./imgs/truck.png\" style=\"align:center;\" alt=\""
    + escapeExpression(((helper = (helper = helpers.asset_name || (depth0 != null ? depth0.asset_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_name","hash":{},"data":data}) : helper)))
    + "\" />\r\n			   <span class=\"truck_tree_title\" title=\""
    + escapeExpression(((helper = (helper = helpers.asset_name || (depth0 != null ? depth0.asset_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_name","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.asset_name || (depth0 != null ? depth0.asset_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_name","hash":{},"data":data}) : helper)))
    + "</span>\r\n		   </div>\r\n		    <div class=\"checkbox_truck\" >	\r\n				<input type=\"checkbox\" data-asset_def=\""
    + escapeExpression(((helper = (helper = helpers.asset_def || (depth0 != null ? depth0.asset_def : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_def","hash":{},"data":data}) : helper)))
    + "\" data-asset_callnum=\""
    + escapeExpression(((helper = (helper = helpers.asset_callnum || (depth0 != null ? depth0.asset_callnum : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_callnum","hash":{},"data":data}) : helper)))
    + "\" data-group_name=\""
    + escapeExpression(((helper = (helper = helpers.group_name || (depth0 != null ? depth0.group_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"group_name","hash":{},"data":data}) : helper)))
    + "\" name=\"asset_"
    + escapeExpression(((helper = (helper = helpers.asset_id || (depth0 != null ? depth0.asset_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_id","hash":{},"data":data}) : helper)))
    + "\" data-id=\"asset_"
    + escapeExpression(((helper = (helper = helpers.asset_id || (depth0 != null ? depth0.asset_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_id","hash":{},"data":data}) : helper)))
    + " \" data-asset_imei=\""
    + escapeExpression(((helper = (helper = helpers.asset_imei || (depth0 != null ? depth0.asset_imei : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_imei","hash":{},"data":data}) : helper)))
    + "\" data-assetid='"
    + escapeExpression(((helper = (helper = helpers.asset_id || (depth0 != null ? depth0.asset_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"asset_id","hash":{},"data":data}) : helper)))
    + "'  class=\"select_truck\" onclick=\"selectTruck(this)\">&nbsp;&nbsp;\r\n			</div>\r\n		</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div style=\"width: 98%;margin: 0 1%;height: 20px;background: #ccc;\" >\r\n	<div class=\"boxcontent raised\" data-title=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\r\n		"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\r\n	</div>\r\n</div>\r\n<div class=\"category_icon_collection_truck\">\r\n	<div style=\"background: #FFF;\">\r\n		<input id=\"select_all_truck\" type=\"checkbox\" style=\"margin-left:24px;\" onclick=\"selectAllTruck(this)\" data-groupId=\""
    + escapeExpression(((helper = (helper = helpers.groupId || (depth0 != null ? depth0.groupId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"groupId","hash":{},"data":data}) : helper)))
    + "\"/>ALL\r\n	</div> \r\n	<div style=\"background:white;overflow: auto;\"> \r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.menus_dt : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</div>\r\n<div>";
},"useData":true});
templates['webcam'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "<form id=\""
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "\">\r\n <div class=\"cam_div\"> \r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">IP:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"ip\" id=\"ip\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ip : stack1), depth0))
    + "\" onblur=\"verifyIP(this)\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div> \r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Port:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"port\" id=\"port\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.port : stack1), depth0))
    + "\" onblur=\"verifyNumber(this,'verifyPort')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\">\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Username:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"username\" id=\"username\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.user : stack1), depth0))
    + "\" />\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Password:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\"  name=\"password\" id=\"password\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.password : stack1), depth0))
    + "\" />\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">X:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"x\" id=\"x\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyX');\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Y:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"y\" id=\"y\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyY')\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Inner_radius:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"inner_radius\" id=\"inner_radius\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.inner_radius : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyInner_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n					\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Outer_radius:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"outer_radius\" id=\"outer_radius\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.outer_radius : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyOuter_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n					\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">S_degree:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"s_degree\" id=\"s_degree\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.s_degree : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyS_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n					\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">E_degree:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"e_degree\" id=\"e_degree\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.e_degree : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyE_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div>\r\n		</div>\r\n		<div> \r\n			<input name=\"id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\r\n			<input name=\"ps_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\" />\r\n		</div>\r\n	</div>\r\n</form>";
},"useData":true});
templates['webcamDemo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "<form id=\""
    + escapeExpression(((helper = (helper = helpers.formId || (depth0 != null ? depth0.formId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formId","hash":{},"data":data}) : helper)))
    + "\">\r\n <div class=\"cam_div\"> \r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">IP:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"ip\" id=\"ip\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ip : stack1), depth0))
    + "\" onblur=\"verifyIP(this)\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div> \r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Port:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"port\" id=\"port\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.port : stack1), depth0))
    + "\" onblur=\"verifyNumber(this,'verifyPort')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" />\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\">\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Username:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"username\" id=\"username\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.user : stack1), depth0))
    + "\" />\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Password:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\"  name=\"password\" id=\"password\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.password : stack1), depth0))
    + "\" />\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">X:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"x\" id=\"x\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.x : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyX');\" readonly=\"readonly\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Y:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"y\" id=\"y\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.y : stack1), depth0))
    + "\"  onchange=\"verifyNumber(this,'verifyY')\" readonly=\"readonly\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Inner_radius:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"inner_radius\" id=\"inner_radius\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.inner_radius : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyInner_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n					\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">Outer_radius:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"outer_radius\" id=\"outer_radius\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.outer_radius : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyOuter_radius')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n					\r\n				</div>\r\n		</div>\r\n		 <div class=\"cam_div1\">\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">S_degree:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"s_degree\" id=\"s_degree\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.s_degree : stack1), depth0))
    + "\"   onchange=\"verifyNumber(this,'verfyS_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n					\r\n				</div>\r\n				<div class=\"input-group form-group has-feedback cam_div2\">\r\n					<span class=\"input-group-addon\" style=\"width: 42%;\">E_degree:</span>\r\n					<input type=\"text\" class=\"form-control cam_input\" name=\"e_degree\" id=\"e_degree\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.e_degree : stack1), depth0))
    + "\" onchange=\"verifyNumber(this,'verfyE_degree')\" onkeyup=\"this.value=this.value.replace(/\\D/g,'')\"  onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" readonly=\"readonly\"/>\r\n					<span class=\"glyphicon form-control-feedback\"  style=\"top:0;\"></span>\r\n				</div>\r\n		</div>\r\n		<div> \r\n			<input name=\"id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\r\n			<input name=\"ps_id\" type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.ps_id : stack1), depth0))
    + "\" />\r\n		</div>\r\n	</div>\r\n</form>";
},"useData":true});
})();