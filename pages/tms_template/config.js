(function(){
    var configObj = {
		printCollection: {
            url:"/Sync10/_tms/print/getPrintBolOrderData"
        }
    };
    if (typeof define === 'function' && define.amd) {
        define(configObj);
    }
    else {
        //传统模式，非AMD标准
        this.page_config = configObj;
    }
}).call(this);
