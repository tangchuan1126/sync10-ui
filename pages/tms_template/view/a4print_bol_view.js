"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "../templates",
  "../model/print_model",
  "artDialog",
  "../i18n/bolTemplate-en-US",
  "domready"
], function(page_config, $, Backbone, Handlebars, handlebars_ext, templates,Models, artDialog) {
    //var canvassion_language =  '../i18n/bolTemplate-' + window.navigator.languages[0];
    var palletI18n = window.regional['defaultLanguage'];
    console.log(palletI18n);
    var Music = Backbone.Model.extend({  
        defaults: {  
            name: palletI18n,  
            artist: "Not specified"  
        },  
        initialize: function(){  
            console.log("Welcome to the music world ");    }  
    }); 

    var A4PrintBolView = Backbone.View.extend({
    	  initialize:function(options){
    	  	console.log(options.el);
            this.setElement(options.el);
          },
          collection:new Models.PrintCollection(),
	      template:templates.a4print_bol,
	      render: function(param) {
	    	var v = this;
	    	var json = $.parseJSON(param);
	    	console.log(json);
	    	v.$el.html(v.template({
	    		i18n:palletI18n,
            	datas: json.datas
            }));
            this.print();


	    	  // console.log(v);
        //       v.collection.url = page_config.printCollection.url+param;
        //       v.collection.fetch({
        //         success:function(collection){
        //         	console.log(collection.toJSON());
        //         	v.$el.html(v.template({
        //         		datas: collection.toJSON()[0]
        //         	}));
        //         }
        //       });
	      },
	      events:{
	    	  "click #printInfo": "print",
	    	  "click #shipperSign":"shipperSign",
	    	  "click #carrierSign":"carrierSign"
	    	  //"click #androidGetImage":"androidGetImage"
	      },
	      shipperSign:function(){
	      	var entryId = $("#entryId").val();
	      	var door_id = $("#door_id").val();
	      	var bol_id = $("#bol_id").val();
	      	var signType = $("#signType").val();
	      	var tempFile = entryId+"_"+door_id+"_"+bol_id;
	      	Android.androidSign(tempFile,signType,signType);
	      },
	      carrierSign:function(){
	      	var entryId = $("#entryId").val();
	      	var door_id = $("#door_id").val();
	      	var bol_id = $("#bol_id").val();
	      	var carriersignType = $("#carriersignType").val();
	      	var tempFile = entryId+"_"+door_id+"_"+bol_id;
	      	Android.androidSign(tempFile,carriersignType,carriersignType);
	      },
	      i18n:function(){
	      	console.log($("font").length);
	    	$("font").each(function(i,n){
	    		for(var j in palletI18n) {
	    			if($(n).attr("id")==j){
	    				$(n).append(palletI18n[j]);
	    			}
	    		}
	    	});
	    },
	   //    androidGetImage:function(relativeFile , tempShipperKey , fileName){
			 // var src = 'http://192.168.1.11/Sync10/upload/signature/'+fileName;
			 // $(".shipper_image").attr("src", src);
	   //    },
	      print:function(){
	    	  var printHtml=$('div[name="printHtml"]');
	    		for(var i=0;i<printHtml.length;i++) {
	    			var a1=$('#a1',printHtml[i]);
	    		 	var a2=$('#a2',printHtml[i]);
	    		 	var a3=$('#a3',printHtml[i]);
	    		 	var a4=$('#a4',printHtml[i]);
	    		 	var a5=$('#a5',printHtml[i]);
	    		 	
	    		 	visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");			// 设置页码字体
	    		 	visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"A4");
	    		 	visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());
	    		 	visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);
	    		
	    		 	visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());
	    		 	visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
	    		 
	    		 	visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());
	    		 	visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1); 

	    		 	visionariPrinter.NewPageA();
	    			visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",a4.html());	
	    			visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);

	    			var total =$('#total',printHtml[i]).val()*1; 
	    		 	if(total>6){
	    				visionariPrinter.ADD_PRINT_TABLE("0",0,"100%","100%",a5.html());	
	    				visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1); 
	    			}
	    				
	    		 	
	    		 	visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top').html());
	    			visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
	    			visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
	    			 
	    			visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
	    			visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
	    			
	    		 	visionariPrinter.SET_PRINT_COPIES(1);
	    	        var flag = visionariPrinter.print();
	    	        //var flag = visionariPrinter.PREVIEW();
					if(flag){ 	
						window.parent.printTask.updatePrintTaskState(1);
					}else{	
						window.parent.printTask.updatePrintTaskState(0);
					}
	    	        $.artDialog.close();
	    		}			 
	      }
	      
	      
	      
	    });
    
    return A4PrintBolView;

});