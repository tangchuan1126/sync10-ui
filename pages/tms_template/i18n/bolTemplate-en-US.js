;( function( w ) {
	w.regional = {};
	w.regional['en_US'] = {	
		date:"Date",
		title:"NON NEGOTIABLE BILL OF LADING",
		
		shipFrom:"SHIP FROM",
		shipFromName:"Name",
		shipFromAddress:"Address",
		shipFromCityStateZip:"City/State/Zip",
		shipFromCID:"SID#",
		
		shipTo:"SHIP TO",
		shipToLocation:"Location ",
		shipToCID:"CID#",
		
		/*THIRD PARTY FREIGHT CHARGES BILL TO*/
		billTo:"THIRD PARTY FREIGHT CHARGES BILL TO:",

		/*SPECIAL INSTRUCTIONS*/
		specialInstructions:"SPECIAL INSTRUCTIONS",

		billOfLoadingNumber:"Bill of Lading Number",
		loadNo:"Load No",
		appointmentDate:"Appointment Date",
		carrierName:"CARRIER NAME",
		trailerNumber:"Trailer Number",
		tractorNumber:"Tractor Number",
		driverLicenseNo:"Driver License No",
		sealNo:"Seal No.",
		scac:"SCAC",
		proNumber:"Pro number",
		barCodeSpace:"BAR CODE SPACE",

		freightChargeTerms:"Freight Charge Terms",
		freightDescripation:"(freight charges are prepaid unless marked otherwise)",
		prepaid:"Prepaid",
		collect:"Collect",
		rdParty:"3rd Party",
		checkBox:"check box",
		masterBillof:"Master Bill of Lading:width attached",
		billofs:"underlying  Bills of lading",

		/*CUSTOMER ORDER INFORMATION*/
		customerOrderInformation:"CUSTOMER ORDER INFORMATION",
		customerOrderNumber:"CUSTOMER ORDER NUMBER",
		pkgs:"#PKGS",
		weight:"WEIGHT",
		palletSlip:"PALLET/SLIP",
		circleOne:"CIRCLE ONE",
		additionalShipperInfo:"ADDITIONAL SHIPPER INFO",
		seeAttachedSupplementPage:"SEE ATTACHED SUPPLEMEANT PAGE",
		grandTotal:"GRAND TOTAL",

		/*CARRIER INFORMATION*/
		carrierInformation:"CARRIER INFORMATION",
		handling:"HANDLING ",
		util:"UNIT",
		packages:"PACKAGE",
		weight:"WEIGHT",
		qty:"QTY",
		type:"TYPE",
		commodityDescripation:"COMMODITY DESCRIPTION",
		commDescripation:"Commodities requiring special or additional care or attention in handling or stowing must be so marked and packaged as to ensure safe transportation with ordinary care.See Section 2(e) of NMFC Item 360",
		ltlOnly:"LTL ONLY",
		nmfc:"NMFC#",
		clasz:"CLASS",


		consigneeSignatureDate:"CONSIGNEE SIGNATURE/DATE",
		propertyDescribed:"Property described above is received in good order,except as noted.",
		consigneeSignaturePrintName:"Consignee Signature/Print Name",
		codAmount:"COD Amount",
		freeTerms:"Fee Terms",
		collect:"Collect",
		prepaid:"Prepaid",
		customerCheckAcceptable:"Customer check acceptable",
		noteLiability:"NOTE Liability Limitation for loss or damage in datas shipment may be applicable. See 49 U.S.C. §14706(c)(1)(A)and (B).",

		shipperSignatureDate:"SHIPPER SIGNATURE/DATE",
		shipperDescripation:"datas is to certify that the above named materials are properly classified,packaged,marked and labeled, and are in proper condition for transportation according to the applicable regulations of the DOT.",
		signaturePrintName:"Signature/ Print Name",
		trailerLoader:"Trailer Loader",
		byShipper:"By Shipper",
		byDriver:"By Driver",
		freightCounted:"Freight Counted",
		byDriverPieces:"By Driver/Pieces",
		noShipperLoaderCount:"NO SHIPPER LOADER COUNT",

		carrierSignaturePickUpDate:"CARRIER SIGNATURE/PICKUP DATE",
		carrierDescripation:"Carrier acknowledges receipt of packages and required placards.Carrier certifies emergency response information was made available and/or carrier has the DOT emergency response guidebook or equivalent documentation in the vehicle.",
		gateIn:"Gate In",
		windowIn:"Window In",
		dockIn:"Dock In",
		dockClose:"Dock Close"
		
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'en_US' ];

}( window ) )