"use strict";
define([
         "config", 
         "jquery",
         "backbone",
         "handlebars",
         "handlebars_ext",
         "templates",
         "./view/a4print_bol_view",
         "domready"
    ],
 function(page_config,$,Backbone,Handlebars,handlebars_ext,templates,V1){
    var request = {
        QueryString: function (val) {
            var uri = decodeURI(window.location.search);
            var re = new RegExp("" + val + "\=([^\&\?]*)", "ig");
            return ((uri.match(re)) ? (uri.match(re)[0].substr(val.length + 1)) : null);
        }
    }             
 //            // 如果当前页面有请求参数
 //    var entry_id=request.QueryString('entry_id');
 //    var door_id=request.QueryString('door_id');
 //    var bol_id=request.QueryString('bol_id');
	// var parsm="?entry_id="+entry_id+"&bol_id="+bol_id+"&door_id="+door_id;
    var parsm = request.QueryString('json');
    var a4PrintBolView = new V1({el:"#datas"});
    a4PrintBolView.render(parsm);
    
});
