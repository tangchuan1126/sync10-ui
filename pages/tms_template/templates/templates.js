(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['a4print_bol'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "	<div style=\"width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;\">\r\n		<input class=\"short-short-button-print\" type=\"button\" value=\"Print\" id=\"printInfo\">\r\n	</div>\r\n	   	<input type=\"hidden\" id=\"entryId\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.entryId : stack1), depth0))
    + "\"/>\r\n		<input type=\"hidden\" id=\"door_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.door_id : stack1), depth0))
    + "\"/>\r\n		<input type=\"hidden\" id=\"bol_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.bol_id : stack1), depth0))
    + "\"/>\r\n		<input type=\"hidden\" id=\"signType\" value=\"1\"/>\r\n		<input type=\"hidden\" id=\"carriersignType\" value=\"2\"/>\r\n    <div style=\"border:1px red solid; width:210mm; margin:0 auto; margin-top:3px;\" name=\"printHtml\">\r\n    	<input type=\"hidden\" id=\"total\" value=\""
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1), depth0))
    + "\"/>\r\n	<div id=\"a1\">\r\n		<!-- ͷ -->\r\n		<div style=\"border:1px red solid; width:210mm;  margin:0 auto; margin-top:3px;display:none;\" id=\"top\">\r\n			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n				<tbody>\r\n				<tr>\r\n		   		 <td colspan=\"6\" style=\"border-top: 1px solid black; border-left: 1px solid black;border-right: 1px solid black;border-bottom:1px solid black;\" align=\"center\">\r\n		   		 	 <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n					  <tbody><tr>\r\n					    <td style=\"font-size:12px; font-family:Arial;\" align=\"center\" height=\"39\" width=\"17%\"><b>Date:</b><span style=\"font-style: italic;\">04/22/15 18:30</span></td>\r\n					    <td align=\"center\" width=\"72%\">\r\n					    	<div style=\"font-size:18px; font-family:Arial; font-weight: bold;\" align=\"center\">SUPPLEMENT TO THE BILL OF LADING</div>\r\n					        <div style=\"font-size:14px; font-family:Arial;\" align=\"\">&nbsp;&nbsp;&nbsp;&nbsp;<b>BILL OF Lading Number:</b><span style=\"font-style: italic;\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.billOfLoadingNo : stack1), depth0))
    + "</span></div>\r\n					    </td>\r\n					    <td align=\"center\" width=\"11%\">&nbsp;</td>\r\n					  </tr>\r\n					</tbody></table>\r\n		   		 </td>\r\n			   	</tr>\r\n			   </tbody>\r\n			</table>\r\n		</div>\r\n		\r\n		\r\n	  <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n		<tbody>\r\n	       <tr>\r\n	         <td style=\"border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black; font-size: 11px; font-family: Arial;\" align=\"center\" height=\"36\" width=\"20%\"><b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.date : stack1), depth0))
    + ":"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.date : stack1), depth0))
    + "</span></td>\r\n	         <td style=\"border-bottom: 1px solid black; border-top: 1px solid black; font-size: 18px; font-weight: bold; font-family: Arial;\" align=\"center\" width=\"67%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.title : stack1), depth0))
    + "</td>\r\n	         <td style=\"border-bottom: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;\" align=\"center\" width=\"13%\">&nbsp;</td>\r\n	       </tr>\r\n		</tbody>\r\n	</table>\r\n	<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n		<tbody>\r\n		      <tr>\r\n		        <td style=\"border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; font-size: 12px; font-family: Arial; \" align=\"left\" height=\"48\" width=\"55%\">\r\n		          <div style=\"background-color:#000; color: #FFF; font-weight:bold;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFrom : stack1), depth0))
    + "</div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromName : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_title : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromAddress : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_address : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCityStateZip : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_city_name : stack1), depth0))
    + "/\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_state_name : stack1), depth0))
    + "/\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_zipcode : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>Phone:</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_linkman_phone : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div>\r\n		            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n					  <tbody><tr>\r\n					    <td style=\"font-size: 12px; font-family: Arial;padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCID : stack1), depth0))
    + ":</b>\r\n					    	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_psid : stack1), depth0))
    + "\r\n					    </td>\r\n					    <td style=\"font-size: 12px; font-family: Arial;width:80px;margin-right:10px;\"><b>FOB: </b><input style=\"text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;\" type=\"text\"></td>\r\n					  </tr>\r\n					</tbody></table>\r\n		          </div>\r\n		          <div style=\"background-color:#000; color: #FFF;font-weight:bold; clear: left\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipTo : stack1), depth0))
    + "</div>\r\n		        </td>\r\n		        <td style=\" border-bottom: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;\" align=\"left\" valign=\"top\" width=\"45%\">\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">Bill of Lading Number:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.billOfLoadingNo : stack1), depth0))
    + "</span></div>\r\n		          <div>&nbsp;</div>\r\n				  <div>&nbsp;</div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">Load No:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.loadNo : stack1), depth0))
    + "</span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">Appointment Date:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.appointmentDate : stack1), depth0))
    + "</span></div>\r\n		        </td>\r\n		      </tr>\r\n		      <tr>\r\n		        <td style=\"border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:14px;font-family:Arial;\" align=\"left\">\r\n		           <div style=\"padding-left: 2px; width:425px; border:red solid 0px;white-space:nowrap;overflow:hidden;\">\r\n		          \r\n		          &nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromName : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_title : stack1), depth0))
    + "\r\n		          </span>\r\n		          &nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipToLocation : stack1), depth0))
    + "&nbsp;#:</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\r\n		          \r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromAddress : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_address : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCityStateZip : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_city_name : stack1), depth0))
    + "/\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_state_name : stack1), depth0))
    + "/\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_zipcode : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div>\r\n		            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n					  <tbody><tr>\r\n					    <td style=\"font-size: 12px;font-family: Arial;padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipToCID : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n					    		"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_psid : stack1), depth0))
    + "\r\n					    </span></td>\r\n					    <td style=\"font-size: 12px;font-family: Arial; width: 80px; margin-right: 10px;\"><b> FOB: </b>\r\n					      <input style=\"text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;\" type=\"text\"></td>\r\n					  </tr>\r\n					</tbody></table>\r\n		          </div>\r\n		          <div style=\"background-color:#000; color:#FFF;font-family:Arial; clear: left\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.billTo : stack1), depth0))
    + "</div>\r\n		        </td>\r\n		        <td style=\"border-bottom:1px solid black;font-size:13px;font-family:Arial;border-right:1px solid black;\" align=\"left\" valign=\"top\">\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierName : stack1), depth0))
    + ": </span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.carrierName : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.trailerNumber : stack1), depth0))
    + "Trailer Number:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.trailerNo : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.tractorNumber : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.tractorNo : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.driverLicenseNo : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		          	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.driverLicense : stack1), depth0))
    + "\r\n		          </span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.sealNo : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\r\n		        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.sealNo : stack1), depth0))
    + "\r\n		          </span></div>\r\n		        </td>\r\n		      </tr>\r\n		      <tr>\r\n		        <td style=\"border-bottom:1px solid black;border-left:1px solid black; border-right:1px solid black;font-size:12px;font-family:Arial;\" align=\"left\" valign=\"top\">\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromName : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromAddress : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCityStateZip : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\r\n		          <div style=\"border-bottom:1px solid black;\"></div>\r\n		          <div style=\"font-weight: bold;padding-left: 2px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.specialInstructions : stack1), depth0))
    + ":</div>\r\n		          <div style=\"font-style:italic;\">\r\n		          </div>\r\n		        </td>\r\n		        <td style=\"border-bottom:1px solid black;border-right:1px solid black; font-size:12px;font-family:Arial;\" align=\"left\" valign=\"top\">\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.scac : stack1), depth0))
    + ":</span>&nbsp;&nbsp;\r\n					<span style=\"font-style: italic;\">\r\n						"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.carrier_id : stack1), depth0))
    + "\r\n				    </span>\r\n				  </div>\r\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.proNumber : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\r\n		          <div style=\"border-bottom: 1px solid black\" align=\"center\">  \r\n		          	<font style=\"FONT-SIZE: 18px; font-family: time Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman'; WIDTH: 100%; COLOR: #B4B4B4; LINE-HEIGHT: 150%;\">\r\n		          		"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.barCodeSpace : stack1), depth0))
    + "\r\n		          	</font>\r\n		          	<div style=\"padding-left: 2px;\">&nbsp;</div>\r\n		          	<div style=\"padding-left: 2px;\">&nbsp;</div>\r\n		          </div>\r\n		          <div style=\"font-size: 11px;font-weight: bold;margin-left:5px;text-align:left;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freightChargeTerms : stack1), depth0))
    + ":<span style=\"font-style: italic;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freightDescripation : stack1), depth0))
    + "</span></div>\r\n		          <div style=\"padding-left:2px; border-bottom:1px solid black;font-weight:bold;padding-bottom:5px; \">\r\n			        "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.prepaid : stack1), depth0))
    + "&nbsp;____\r\n			        "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.collect : stack1), depth0))
    + "&nbsp;____\r\n			        "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.rdParty : stack1), depth0))
    + "&nbsp;____	     \r\n		          </div>\r\n		          <div style=\"clear:both\">\r\n		          	<div style=\"float:left;margin-left:5px;\">\r\n		          		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n		          		<input style=\"text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000; margin-top:1px;\" type=\"text\">\r\n		            	<br><span style=\"font-size:12px;\">("
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.checkBox : stack1), depth0))
    + ")</span>\r\n		            </div>\r\n		            <div style=\"float:right;margin-bottom:5px; margin-right:30px;\">\r\n		            	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.masterBillof : stack1), depth0))
    + "<br>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.billofs : stack1), depth0))
    + "\r\n		            </div>\r\n		          </div>\r\n		        </td>\r\n		      </tr>\r\n		</tbody>\r\n	</table>\r\n\r\n	  <!-- CUSTOMER ORDER INFORMATION -->\r\n	  <table cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#000000\" border=\"0\" width=\"100%\">\r\n	      <tbody><tr>\r\n	        <td colspan=\"6\" align=\"left\" valign=\"top\">\r\n	          <div style=\"background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;\" align=\"center\">\r\n	          "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderInformation : stack1), depth0))
    + "</div>\r\n	        </td>\r\n	      </tr>\r\n	      <tr>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-weight:bold;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"24%\">\r\n	        "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderNumber : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-weight:bold;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.pkgs : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-weight:bold;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.weight : stack1), depth0))
    + "</td>\r\n	        <td colspan=\"2\" style=\"font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\"><span style=\"font-weight:bold;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.palletSlip : stack1), depth0))
    + "</span><br><span style=\"font-size: 9px;\">("
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.circleOne : stack1), depth0))
    + ")</span></td>\r\n	        <td colspan=\"3\" style=\"font-size:12px;font-weight:bold;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.additionalShipperInfo : stack1), depth0))
    + "</td>\r\n	      </tr>\r\n	  		<tr>\r\n		        <td style=\"font-size:12px;font-family:Arial;font-style: italic;white-space:nowrap\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"24%\" colspan=8>\r\n		        	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.seeAttachedSupplementPage : stack1), depth0))
    + "\r\n		        </td>\r\n	      	</tr> \r\n	      	<tr>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;white-space:nowrap\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"24%\">\r\n	        </td>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">\r\n	        </td>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">\r\n	        </td>\r\n	        <td style=\"font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"6%\">Y</td>\r\n	        <td style=\"font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"6%\">N</td>\r\n	        <td colspan=\"3\" style=\"font-size:12px;font-weight:bold;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">	\r\n	        </td>\r\n	      </tr>   	      \r\n	      <tr>\r\n	        <td style=\"font-weight:bold;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" height=\"15\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderCount : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\">\r\n				"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderWeight : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td colspan=\"5\" style=\"font-size:12px;font-family:Arial;\" bgcolor=\"#999999\">&nbsp;</td>\r\n	      </tr>\r\n	    </tbody></table>\r\n    </div>\r\n    \r\n    <div id=\"a2\">\r\n      <!-- CARRIER INFORMATION -->\r\n      \r\n       <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n	     <thead>\r\n	      <tr>\r\n	        <td colspan=\"9\" style=\"color:#FFF;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#000000\" height=\"19\" valign=\"top\">\r\n	            "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierInformation : stack1), depth0))
    + "\r\n	        </td>\r\n	      </tr>     \r\n	      <tr>\r\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.handling : stack1), depth0))
    + " <br>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.util : stack1), depth0))
    + "</td>\r\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.packages : stack1), depth0))
    + "</td>\r\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\"><span style=\"font-weight:bold\"><span style=\"font-weight:bold\">WEIGH</span>T</span></td>\r\n	        <td rowspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">H.M.<br>(X)</td>\r\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial; \" align=\"center\" width=\"38%\"><span style=\"font-weight:bold\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.commodityDescripation : stack1), depth0))
    + "</span><br>\r\n	        <span style=\"font-family:Arial;font-size:7px; padding-left:5px;padding-right:5px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.commDescripation : stack1), depth0))
    + "\r\n</span></td>\r\n	        <td colspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.ltlOnly : stack1), depth0))
    + "</td>\r\n	      </tr>\r\n	      <tr>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.nmfc : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.clasz : stack1), depth0))
    + "</td>\r\n	      </tr>\r\n	     </thead>  \r\n	      <tbody>\r\n\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1), "6", "<=", {"name":"xifCond","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1), "6", ">", {"name":"xifCond","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.times || (depth0 && depth0.times) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.blankCount : stack1), {"name":"times","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.palletCount : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalPackageCount : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalWeight : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\"></td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-weight: bold;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	      </tr>\r\n	    </tbody>\r\n	 </table>\r\n	</div>\r\n    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->\r\n    <div id=\"a3\">\r\n	 <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n	    <tbody>\r\n			<!-- ################################################ -->\r\n			      <tr>\r\n	        <td style=\"border-bottom:1px solid black;border-top:3px solid black; border-right:1px solid black;border-left:1px solid black; font-size:10px;font-family:Arial;\" align=\"left\" height=\"20\" valign=\"top\" width=\"57%\">     \r\n              <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->\r\n              	  <table>\r\n                	<tr>\r\n                		<td valign=\"top\" width=\"200px;\">\r\n                			<span style=\"font-size:10px;font-family:Verdana;font-weight:bold;\"><b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.consigneeSignatureDate : stack1), depth0))
    + "</b></span>\r\n                		</td>\r\n                		<td>\r\n                			<div style=\"font-size: 7px; font-family:Arial;font-style: italic;\">\r\n                				"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.propertyDescribed : stack1), depth0))
    + "\r\n                			</div>\r\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\r\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\r\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\r\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\r\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\r\n		                	<div style=\"font-size: 5px; font-family:Arial;\">X_________________________________________________________________________</div>\r\n		                	<div style=\"font-size: 10px; font-family:Arial;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.consigneeSignaturePrintName : stack1), depth0))
    + "</div>\r\n                		</td>\r\n                	</tr>\r\n                </table>\r\n              <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->\r\n            </td>\r\n	        <td style=\"border-bottom:3px solid black;border-right:3px solid black;border-left:3px solid black; border-top:3px solid black;font-family:Arial; font-size:15px;\" align=\"left\" valign=\"top\" width=\"45%\">\r\n	         <div><span style=\"font-weight:bold; padding-left:2px; margin-bottom:10px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.codAmount : stack1), depth0))
    + ":</span> $___________________________</div>\r\n	          <div style=\"font-weight:bold;text-align:center;margin-top:2px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freeTerms : stack1), depth0))
    + ":&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.collect : stack1), depth0))
    + ":\r\n	          \r\n	            <input style=\"text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;\" type=\"text\">\r\n				&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.Prepaid : stack1), depth0))
    + ":\r\n	          \r\n	            <input style=\"text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;\" type=\"text\">\r\n	          </div>\r\n	          <div style=\"font-weight:bold;text-align:center;margin-top:2px;\"><label for=\"customer\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerCheckAcceptable : stack1), depth0))
    + ":</label>	     \r\n	            <img alt=\"\" src=\"/Sync10/administrator/imgs/print/cha.jpg\">\r\n	          </div>\r\n	        </td>\r\n	      </tr>\r\n	      <tr>\r\n	        <td colspan=\"2\" style=\"font-size:12px;font-family:Arial;border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;font-weight:bold;\" align=\"left\" height=\"20\" valign=\"top\">\r\n	        	<div style=\"margin-left:5px;margin-right:5px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.noteLiability : stack1), depth0))
    + "</div></td>\r\n	      </tr>        \r\n	      <tr>\r\n	        <td colspan=\"2\">\r\n	        		<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n				      <tbody><tr>\r\n				        <td id=\"shipperSign\" style=\"border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:12px;font-family:Arial;\" height=\"180\" valign=\"top\" width=\"34%\">\r\n				          <div style=\"font-weight:bold; padding-left: 5px;margin-top:5px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipperSignatureDate : stack1), depth0))
    + "</div>\r\n				          <div style=\"font-size: 9px; padding-left: 5px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipperDescripation : stack1), depth0))
    + "</div>\r\n				          <br>\r\n				           <div id='"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.entryId : stack1), depth0))
    + "_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.door_id : stack1), depth0))
    + "_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.bol_id : stack1), depth0))
    + "_1' style=\"padding-left: 5px; border-bottom:1px solid black;font-family:Arial;font-size:12px; height:70px;\">\r\n				              		  <div style=\"float:left;\">\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "				              		  </div>\r\n				          		 	<div style=\"float:right;padding-top:50px;font-size:10px;\" >&nbsp;&nbsp; \r\n				          			\r\n				          			&nbsp;&nbsp;\r\n				          		</div>\r\n\r\n				              \r\n				          </div>\r\n				          \r\n 				          <div style=\"padding-left: 5px; font-family:Arial;font-size:12px;\">\r\n				          	<div style=\"float:left;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.signaturePrintName : stack1), depth0))
    + "</div> \r\n				          	<div style=\"float:right;margin-right: 50px;\" >Date</div>\r\n				          </div>\r\n				        </td>\r\n				        <td style=\"border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;\" valign=\"top\" width=\"14%\">\r\n				          <div style=\"text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;\">\r\n				          "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.trailerLoader : stack1), depth0))
    + ":</div>\r\n				          <div style=\"margin-top:5px;margin-left:5px;text-align:left;font-size:12px;\">\r\n				          	<img alt=\"\" src=\"/Sync10/administrator/imgs/print/cha.jpg\">\r\n				          	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.byShipper : stack1), depth0))
    + "\r\n				          </div>\r\n				          <div style=\"margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;\">\r\n				            <p><img alt=\"\" src=\"/Sync10/administrator/imgs/print/kongcha.jpg\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.byDriver : stack1), depth0))
    + "</p>\r\n				          </div>\r\n				        </td>\r\n				        <td style=\"border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;\" valign=\"top\" width=\"16%\">\r\n				          <div style=\"text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;\">\r\n				          "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freightCounted : stack1), depth0))
    + ":</div>\r\n				          <div style=\"margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;\">\r\n				          <img alt=\"\" src=\"/Sync10/administrator/imgs/print/cha.jpg\">\r\n				          "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.byDriverPieces : stack1), depth0))
    + "\r\n				          </div>\r\n				          <div style=\"margin-left:5px;margin-top:12px;font-family:Arial;font-size:12px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.noShipperLoaderCount : stack1), depth0))
    + "</div>\r\n				        </td>\r\n				        <td id=\"carrierSign\" style=\"border-bottom:1px solid black;border-right:1px solid black;\" valign=\"top\" width=\"36%\">\r\n				          <div style=\"font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;margin-top:5px;\">\r\n				          	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierSignaturePickUpDate : stack1), depth0))
    + "\r\n				          </div>\r\n				          <div style=\"font-size:7px;font-family:Arial;margin-left:5px;margin-right:5px;\">\r\n				          	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierDescripation : stack1), depth0))
    + "\r\n						  </div>\r\n				          <div style=\"font-style: italic;font-size:9px;font-weight: bold;margin:5px 5px 0px 5px;font-family:Arial;\">\r\n				          	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.propertyDescribed : stack1), depth0))
    + "\r\n				          </div>\r\n				        <div id='"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.entryId : stack1), depth0))
    + "_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.door_id : stack1), depth0))
    + "_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.bol_id : stack1), depth0))
    + "_2' style=\"border-bottom:1px solid black;font-family:Arial;font-size:12px; height:70px;  \">\r\n				              		  <div style=\"float:left; margin-left:5px;\">\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1), {"name":"if","hash":{},"fn":this.program(15, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "				              		  </div>\r\n\r\n				          		 <div style=\"float:right;padding-top:50px;font-size:10px;\" >&nbsp;&nbsp;\r\n				          			&nbsp;&nbsp; \r\n				          		</div>\r\n				          </div>\r\n				          <div style=\"font-size:12px;font-family:Arial;margin-left:5px;\">\r\n				          	<div style=\"float:left; margin-right:50px;\">Signature/ Print Name</div> \r\n				          	<div style=\"float:right; margin-right:60px;\" >Date</div>			      \r\n				          </div>\r\n				          <div>&nbsp;</div>\r\n				          <div style=\"font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;\"> \r\n				          	  <div style=\"float:left; width:100%\">\r\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.gateIn : stack1), depth0))
    + ":&nbsp;\r\n									  	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.gateInDate : stack1), depth0))
    + "\r\n								</span>&nbsp;&nbsp;&nbsp;&nbsp;\r\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.windowIn : stack1), depth0))
    + ":&nbsp;\r\n										"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.windowInDate : stack1), depth0))
    + "\r\n								</span>\r\n				             </div>\r\n				             <div style=\"float:left; width:100%\">\r\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.dockIn : stack1), depth0))
    + ":&nbsp;\r\n									  	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.dockInDate : stack1), depth0))
    + "\r\n								</span>&nbsp;&nbsp;&nbsp;&nbsp;\r\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.dockClose : stack1), depth0))
    + ":&nbsp;\r\n									  	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.dockOutDate : stack1), depth0))
    + "\r\n								</span>\r\n				             </div>				        	\r\n				          </div>\r\n				        </td>\r\n				      </tr>\r\n				    </tbody></table>\r\n	        </td>\r\n	      </tr>\r\n			<!-- ################################################ -->\r\n	    </tbody>\r\n	</table>\r\n    </div>\r\n    \r\n<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ --> \r\n	<div id=\"a4\">\r\n		<table  cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n	      <tbody><tr>\r\n	        <td colspan=\"8\" align=\"left\" valign=\"top\" bgcolor=\"#000000\"  height=\"19\">\r\n	          <div style=\"background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;\" align=\"center\">\r\n	          	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderInformation : stack1), depth0))
    + "\r\n	          </div>\r\n	        </td>\r\n	      </tr>\r\n	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\"  width=\"24%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderNumber : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.pkgs : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.weight : stack1), depth0))
    + "</td>\r\n	        <td colspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\"><span style=\"font-weight:bold;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.palletSlip : stack1), depth0))
    + "</span><br><span style=\"font-size: 9px;\">("
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.circleOne : stack1), depth0))
    + ")</span></td>\r\n	        <td colspan=\"3\" style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.additionalShipperInfo : stack1), depth0))
    + "</td>\r\n	      </tr>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.order : stack1), {"name":"if","hash":{},"fn":this.program(17, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" height=\"15\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderCount : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">\r\n				"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderWeight : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td colspan=\"5\" style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" bgcolor=\"#999999\">&nbsp;</td>\r\n	      </tr>\r\n	    </tbody></table>\r\n	</div>\r\n\r\n";
  stack1 = ((helpers.xifCond || (depth0 && depth0.xifCond) || helperMissing).call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1), "6", ">", {"name":"xifCond","hash":{},"fn":this.program(20, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n	    \r\n \r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"3":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\r\n	        	1\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">Plts</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	        	"
    + escapeExpression(lambda((depth0 != null ? depth0.total_package : depth0), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">CTNS</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + escapeExpression(lambda((depth0 != null ? depth0.weight : depth0), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n	       	<td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	       	</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	        &nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n	      </tr>\r\n";
},"6":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1), {"name":"each","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"7":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	     	<tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\r\n	        	0\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">Plts</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	        	1\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">CTNS</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1)) != null ? stack1.weight : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n	       	<td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	       	\r\n	       	</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n	      </tr>\r\n";
},"9":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	       <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" colspan=7>\r\n	        	"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.seeAttachedSupplementPage : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n";
},"11":function(depth0,helpers,partials,data) {
  return "		      <tr>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n		      </tr>\r\n";
  },"13":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "										<img class=\"shipper_image\" src=\"http://192.168.1.11/Sync10/"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1)) != null ? stack1.shipper_relative_file_path : stack1), depth0))
    + "\" width=\"150px\" height=\"70px\" id=\"androidGetImage\"/>\r\n";
},"15":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "										<img class=\"carrier_image\" src=\"http://192.168.1.11/Sync10/"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1)) != null ? stack1.carrier_relative_file_path : stack1), depth0))
    + "\" width=\"150px\" height=\"70px\" />\r\n";
},"17":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.order : stack1), {"name":"each","hash":{},"fn":this.program(18, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"18":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\"  width=\"24%\">\r\n	        	"
    + escapeExpression(lambda((depth0 != null ? depth0.waybill_id : depth0), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">\r\n	        	"
    + escapeExpression(lambda((depth0 != null ? depth0.totalPackage : depth0), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">\r\n	        	"
    + escapeExpression(lambda((depth0 != null ? depth0.weightPackage : depth0), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"6%\">Y</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"6%\">N</td>\r\n	        <td colspan=\"3\" style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">	\r\n	        </td>\r\n	      </tr>\r\n";
},"20":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "	 <div id=\"a5\">\r\n      <!-- CARRIER INFORMATION -->\r\n      \r\n       <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n	     <thead>\r\n	      <tr>\r\n	        <td colspan=\"9\" style=\"color:#FFF;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#000000\" height=\"19\" valign=\"top\">\r\n	           "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierInformation : stack1), depth0))
    + "\r\n	        </td>\r\n	      </tr>     \r\n	      <tr>\r\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.handling : stack1), depth0))
    + " <br>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.util : stack1), depth0))
    + "</td>\r\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.packages : stack1), depth0))
    + "</td>\r\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\"><span style=\"font-weight:bold\"><span style=\"font-weight:bold\">WEIGH</span>T</span></td>\r\n	        <td rowspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">H.M.<br>(X)</td>\r\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial; \" align=\"center\" width=\"38%\"><span style=\"font-weight:bold\">COMMODITY DESCRIPTION</span><br>\r\n	        <span style=\"font-family:Arial;font-size:7px; padding-left:5px;padding-right:5px;\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.commDescripation : stack1), depth0))
    + "</span>\r\n			</td>\r\n	        <td colspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.ltlOnly : stack1), depth0))
    + "</td>\r\n	      </tr>\r\n	      <tr>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.type : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.type : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.nmfc : stack1), depth0))
    + "</td>\r\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.clasz : stack1), depth0))
    + "</td>\r\n	      </tr>\r\n	     </thead>  \r\n	      <tbody>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1), {"name":"if","hash":{},"fn":this.program(21, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.palletCount : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalPackageCount : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalWeight : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\"></td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-weight: bold;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\r\n	      </tr>\r\n	    </tbody>\r\n	 </table>\r\n	</div>\r\n";
},"21":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1), {"name":"each","hash":{},"fn":this.program(22, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"22":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	      <tr>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\r\n	        	1\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">Plts</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	        	"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1)) != null ? stack1.total_package : stack1), depth0))
    + "\r\n	        </td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">CTNS</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + escapeExpression(lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1)) != null ? stack1.weight : stack1), depth0))
    + "</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\r\n	       	<td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n	       	\r\n	       	</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\r\n\r\n	        &nbsp;</td>\r\n	      </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.datas : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
})();