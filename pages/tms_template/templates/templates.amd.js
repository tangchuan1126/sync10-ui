define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['a4print_bol'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "	<div style=\"width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;\">\n		<input class=\"short-short-button-print\" type=\"button\" value=\"Print\" id=\"printInfo\">\n	</div>\n	   	<input type=\"hidden\" id=\"entryId\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.entryId : stack1), depth0))
    + "\"/>\n		<input type=\"hidden\" id=\"door_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.door_id : stack1), depth0))
    + "\"/>\n		<input type=\"hidden\" id=\"bol_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.bol_id : stack1), depth0))
    + "\"/>\n		<input type=\"hidden\" id=\"signType\" value=\"1\"/>\n		<input type=\"hidden\" id=\"carriersignType\" value=\"2\"/>\n    <div style=\"border:1px red solid; width:210mm; margin:0 auto; margin-top:3px;\" name=\"printHtml\">\n    	<input type=\"hidden\" id=\"total\" value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1), depth0))
    + "\"/>\n	<div id=\"a1\">\n		<!-- ͷ -->\n		<div style=\"border:1px red solid; width:210mm;  margin:0 auto; margin-top:3px;display:none;\" id=\"top\">\n			<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n				<tbody>\n				<tr>\n		   		 <td colspan=\"6\" style=\"border-top: 1px solid black; border-left: 1px solid black;border-right: 1px solid black;border-bottom:1px solid black;\" align=\"center\">\n		   		 	 <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n					  <tbody><tr>\n					    <td style=\"font-size:12px; font-family:Arial;\" align=\"center\" height=\"39\" width=\"17%\"><b>Date:</b><span style=\"font-style: italic;\">04/22/15 18:30</span></td>\n					    <td align=\"center\" width=\"72%\">\n					    	<div style=\"font-size:18px; font-family:Arial; font-weight: bold;\" align=\"center\">SUPPLEMENT TO THE BILL OF LADING</div>\n					        <div style=\"font-size:14px; font-family:Arial;\" align=\"\">&nbsp;&nbsp;&nbsp;&nbsp;<b>BILL OF Lading Number:</b><span style=\"font-style: italic;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.billOfLoadingNo : stack1), depth0))
    + "</span></div>\n					    </td>\n					    <td align=\"center\" width=\"11%\">&nbsp;</td>\n					  </tr>\n					</tbody></table>\n		   		 </td>\n			   	</tr>\n			   </tbody>\n			</table>\n		</div>\n		\n		\n	  <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n		<tbody>\n	       <tr>\n	         <td style=\"border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black; font-size: 11px; font-family: Arial;\" align=\"center\" height=\"36\" width=\"20%\"><b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.date : stack1), depth0))
    + ":"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.date : stack1), depth0))
    + "</span></td>\n	         <td style=\"border-bottom: 1px solid black; border-top: 1px solid black; font-size: 18px; font-weight: bold; font-family: Arial;\" align=\"center\" width=\"67%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.title : stack1), depth0))
    + "</td>\n	         <td style=\"border-bottom: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;\" align=\"center\" width=\"13%\">&nbsp;</td>\n	       </tr>\n		</tbody>\n	</table>\n	<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n		<tbody>\n		      <tr>\n		        <td style=\"border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; font-size: 12px; font-family: Arial; \" align=\"left\" height=\"48\" width=\"55%\">\n		          <div style=\"background-color:#000; color: #FFF; font-weight:bold;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFrom : stack1), depth0))
    + "</div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromName : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_title : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromAddress : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_address : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCityStateZip : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_city_name : stack1), depth0))
    + "/\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_state_name : stack1), depth0))
    + "/\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_zipcode : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>Phone:</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_linkman_phone : stack1), depth0))
    + "\n		          </span></div>\n		          <div>\n		            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n					  <tbody><tr>\n					    <td style=\"font-size: 12px; font-family: Arial;padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCID : stack1), depth0))
    + ":</b>\n					    	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipFrom : stack1)) != null ? stack1.send_psid : stack1), depth0))
    + "\n					    </td>\n					    <td style=\"font-size: 12px; font-family: Arial;width:80px;margin-right:10px;\"><b>FOB: </b><input style=\"text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;\" type=\"text\"></td>\n					  </tr>\n					</tbody></table>\n		          </div>\n		          <div style=\"background-color:#000; color: #FFF;font-weight:bold; clear: left\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipTo : stack1), depth0))
    + "</div>\n		        </td>\n		        <td style=\" border-bottom: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;\" align=\"left\" valign=\"top\" width=\"45%\">\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">Bill of Lading Number:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.billOfLoadingNo : stack1), depth0))
    + "</span></div>\n		          <div>&nbsp;</div>\n				  <div>&nbsp;</div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">Load No:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.loadNo : stack1), depth0))
    + "</span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">Appointment Date:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.appointmentDate : stack1), depth0))
    + "</span></div>\n		        </td>\n		      </tr>\n		      <tr>\n		        <td style=\"border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:14px;font-family:Arial;\" align=\"left\">\n		           <div style=\"padding-left: 2px; width:425px; border:red solid 0px;white-space:nowrap;overflow:hidden;\">\n		          \n		          &nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromName : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_title : stack1), depth0))
    + "\n		          </span>\n		          &nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipToLocation : stack1), depth0))
    + "&nbsp;#:</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\n		          \n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromAddress : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_address : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCityStateZip : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_city_name : stack1), depth0))
    + "/\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_state_name : stack1), depth0))
    + "/\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_zipcode : stack1), depth0))
    + "\n		          </span></div>\n		          <div>\n		            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n					  <tbody><tr>\n					    <td style=\"font-size: 12px;font-family: Arial;padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipToCID : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n					    		"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.strogeMap : stack1)) != null ? stack1.shipTo : stack1)) != null ? stack1.receive_psid : stack1), depth0))
    + "\n					    </span></td>\n					    <td style=\"font-size: 12px;font-family: Arial; width: 80px; margin-right: 10px;\"><b> FOB: </b>\n					      <input style=\"text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;\" type=\"text\"></td>\n					  </tr>\n					</tbody></table>\n		          </div>\n		          <div style=\"background-color:#000; color:#FFF;font-family:Arial; clear: left\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.billTo : stack1), depth0))
    + "</div>\n		        </td>\n		        <td style=\"border-bottom:1px solid black;font-size:13px;font-family:Arial;border-right:1px solid black;\" align=\"left\" valign=\"top\">\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierName : stack1), depth0))
    + ": </span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.carrierName : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.trailerNumber : stack1), depth0))
    + "Trailer Number:</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.trailerNo : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.tractorNumber : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.tractorNo : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.driverLicenseNo : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		          	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.driverLicense : stack1), depth0))
    + "\n		          </span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.sealNo : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\">\n		        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentMap : stack1)) != null ? stack1.sealNo : stack1), depth0))
    + "\n		          </span></div>\n		        </td>\n		      </tr>\n		      <tr>\n		        <td style=\"border-bottom:1px solid black;border-left:1px solid black; border-right:1px solid black;font-size:12px;font-family:Arial;\" align=\"left\" valign=\"top\">\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromName : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromAddress : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipFromCityStateZip : stack1), depth0))
    + ":</b>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\n		          <div style=\"border-bottom:1px solid black;\"></div>\n		          <div style=\"font-weight: bold;padding-left: 2px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.specialInstructions : stack1), depth0))
    + ":</div>\n		          <div style=\"font-style:italic;\">\n		          </div>\n		        </td>\n		        <td style=\"border-bottom:1px solid black;border-right:1px solid black; font-size:12px;font-family:Arial;\" align=\"left\" valign=\"top\">\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.scac : stack1), depth0))
    + ":</span>&nbsp;&nbsp;\n					<span style=\"font-style: italic;\">\n						"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.carrier_id : stack1), depth0))
    + "\n				    </span>\n				  </div>\n		          <div style=\"padding-left: 2px;\">&nbsp;<span style=\"font-weight: bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.proNumber : stack1), depth0))
    + ":</span>&nbsp;&nbsp;<span style=\"font-style: italic;\"></span></div>\n		          <div style=\"border-bottom: 1px solid black\" align=\"center\">  \n		          	<font style=\"FONT-SIZE: 18px; font-family: time Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman'; WIDTH: 100%; COLOR: #B4B4B4; LINE-HEIGHT: 150%;\">\n		          		"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.barCodeSpace : stack1), depth0))
    + "\n		          	</font>\n		          	<div style=\"padding-left: 2px;\">&nbsp;</div>\n		          	<div style=\"padding-left: 2px;\">&nbsp;</div>\n		          </div>\n		          <div style=\"font-size: 11px;font-weight: bold;margin-left:5px;text-align:left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freightChargeTerms : stack1), depth0))
    + ":<span style=\"font-style: italic;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freightDescripation : stack1), depth0))
    + "</span></div>\n		          <div style=\"padding-left:2px; border-bottom:1px solid black;font-weight:bold;padding-bottom:5px; \">\n			        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.prepaid : stack1), depth0))
    + "&nbsp;____\n			        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.collect : stack1), depth0))
    + "&nbsp;____\n			        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.rdParty : stack1), depth0))
    + "&nbsp;____	     \n		          </div>\n		          <div style=\"clear:both\">\n		          	<div style=\"float:left;margin-left:5px;\">\n		          		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n		          		<input style=\"text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000; margin-top:1px;\" type=\"text\">\n		            	<br><span style=\"font-size:12px;\">("
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.checkBox : stack1), depth0))
    + ")</span>\n		            </div>\n		            <div style=\"float:right;margin-bottom:5px; margin-right:30px;\">\n		            	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.masterBillof : stack1), depth0))
    + "<br>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.billofs : stack1), depth0))
    + "\n		            </div>\n		          </div>\n		        </td>\n		      </tr>\n		</tbody>\n	</table>\n\n	  <!-- CUSTOMER ORDER INFORMATION -->\n	  <table cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#000000\" border=\"0\" width=\"100%\">\n	      <tbody><tr>\n	        <td colspan=\"6\" align=\"left\" valign=\"top\">\n	          <div style=\"background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;\" align=\"center\">\n	          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderInformation : stack1), depth0))
    + "</div>\n	        </td>\n	      </tr>\n	      <tr>\n	        <td style=\"font-size:12px;font-family:Arial;font-weight:bold;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"24%\">\n	        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderNumber : stack1), depth0))
    + "</td>\n	        <td style=\"font-size:12px;font-family:Arial;font-weight:bold;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.pkgs : stack1), depth0))
    + "</td>\n	        <td style=\"font-size:12px;font-family:Arial;font-weight:bold;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.weight : stack1), depth0))
    + "</td>\n	        <td colspan=\"2\" style=\"font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\"><span style=\"font-weight:bold;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.palletSlip : stack1), depth0))
    + "</span><br><span style=\"font-size: 9px;\">("
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.circleOne : stack1), depth0))
    + ")</span></td>\n	        <td colspan=\"3\" style=\"font-size:12px;font-weight:bold;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.additionalShipperInfo : stack1), depth0))
    + "</td>\n	      </tr>\n	  		<tr>\n		        <td style=\"font-size:12px;font-family:Arial;font-style: italic;white-space:nowrap\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"24%\" colspan=8>\n		        	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.seeAttachedSupplementPage : stack1), depth0))
    + "\n		        </td>\n	      	</tr> \n	      	<tr>\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;white-space:nowrap\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"24%\">\n	        </td>\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">\n	        </td>\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"12%\">\n	        </td>\n	        <td style=\"font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"6%\">Y</td>\n	        <td style=\"font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" width=\"6%\">N</td>\n	        <td colspan=\"3\" style=\"font-size:12px;font-weight:bold;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">	\n	        </td>\n	      </tr>   	      \n	      <tr>\n	        <td style=\"font-weight:bold;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" height=\"15\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderCount : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" bgcolor=\"#FFFFFF\">\n				"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderWeight : stack1), depth0))
    + "\n	        </td>\n	        <td colspan=\"5\" style=\"font-size:12px;font-family:Arial;\" bgcolor=\"#999999\">&nbsp;</td>\n	      </tr>\n	    </tbody></table>\n    </div>\n    \n    <div id=\"a2\">\n      <!-- CARRIER INFORMATION -->\n      \n       <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n	     <thead>\n	      <tr>\n	        <td colspan=\"9\" style=\"color:#FFF;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#000000\" height=\"19\" valign=\"top\">\n	            "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierInformation : stack1), depth0))
    + "\n	        </td>\n	      </tr>     \n	      <tr>\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.handling : stack1), depth0))
    + " <br>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.util : stack1), depth0))
    + "</td>\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.packages : stack1), depth0))
    + "</td>\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\"><span style=\"font-weight:bold\"><span style=\"font-weight:bold\">WEIGH</span>T</span></td>\n	        <td rowspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">H.M.<br>(X)</td>\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial; \" align=\"center\" width=\"38%\"><span style=\"font-weight:bold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.commodityDescripation : stack1), depth0))
    + "</span><br>\n	        <span style=\"font-family:Arial;font-size:7px; padding-left:5px;padding-right:5px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.commDescripation : stack1), depth0))
    + "\n</span></td>\n	        <td colspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.ltlOnly : stack1), depth0))
    + "</td>\n	      </tr>\n	      <tr>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.nmfc : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.clasz : stack1), depth0))
    + "</td>\n	      </tr>\n	     </thead>  \n	      <tbody>\n\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1),"6","<=",{"name":"xifCond","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1),"6",">",{"name":"xifCond","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.times || (depth0 && depth0.times) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.blankCount : stack1),{"name":"times","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.palletCount : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalPackageCount : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalWeight : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\"></td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-weight: bold;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	      </tr>\n	    </tbody>\n	 </table>\n	</div>\n    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->\n    <div id=\"a3\">\n	 <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n	    <tbody>\n			<!-- ################################################ -->\n			      <tr>\n	        <td style=\"border-bottom:1px solid black;border-top:3px solid black; border-right:1px solid black;border-left:1px solid black; font-size:10px;font-family:Arial;\" align=\"left\" height=\"20\" valign=\"top\" width=\"57%\">     \n              <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->\n              	  <table>\n                	<tr>\n                		<td valign=\"top\" width=\"200px;\">\n                			<span style=\"font-size:10px;font-family:Verdana;font-weight:bold;\"><b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.consigneeSignatureDate : stack1), depth0))
    + "</b></span>\n                		</td>\n                		<td>\n                			<div style=\"font-size: 7px; font-family:Arial;font-style: italic;\">\n                				"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.propertyDescribed : stack1), depth0))
    + "\n                			</div>\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\n		                	<div style=\"font-size: 5px; font-family:Arial;\">&nbsp;</div>\n		                	<div style=\"font-size: 5px; font-family:Arial;\">X_________________________________________________________________________</div>\n		                	<div style=\"font-size: 10px; font-family:Arial;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.consigneeSignaturePrintName : stack1), depth0))
    + "</div>\n                		</td>\n                	</tr>\n                </table>\n              <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->\n            </td>\n	        <td style=\"border-bottom:3px solid black;border-right:3px solid black;border-left:3px solid black; border-top:3px solid black;font-family:Arial; font-size:15px;\" align=\"left\" valign=\"top\" width=\"45%\">\n	         <div><span style=\"font-weight:bold; padding-left:2px; margin-bottom:10px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.codAmount : stack1), depth0))
    + ":</span> $___________________________</div>\n	          <div style=\"font-weight:bold;text-align:center;margin-top:2px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freeTerms : stack1), depth0))
    + ":&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.collect : stack1), depth0))
    + ":\n	          \n	            <input style=\"text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;\" type=\"text\">\n				&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.Prepaid : stack1), depth0))
    + ":\n	          \n	            <input style=\"text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;\" type=\"text\">\n	          </div>\n	          <div style=\"font-weight:bold;text-align:center;margin-top:2px;\"><label for=\"customer\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerCheckAcceptable : stack1), depth0))
    + ":</label>	     \n	            <img alt=\"\" src=\"/Sync10/administrator/imgs/print/cha.jpg\">\n	          </div>\n	        </td>\n	      </tr>\n	      <tr>\n	        <td colspan=\"2\" style=\"font-size:12px;font-family:Arial;border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;font-weight:bold;\" align=\"left\" height=\"20\" valign=\"top\">\n	        	<div style=\"margin-left:5px;margin-right:5px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.noteLiability : stack1), depth0))
    + "</div></td>\n	      </tr>        \n	      <tr>\n	        <td colspan=\"2\">\n	        		<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n				      <tbody><tr>\n				        <td id=\"shipperSign\" style=\"border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:12px;font-family:Arial;\" height=\"180\" valign=\"top\" width=\"34%\">\n				          <div style=\"font-weight:bold; padding-left: 5px;margin-top:5px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipperSignatureDate : stack1), depth0))
    + "</div>\n				          <div style=\"font-size: 9px; padding-left: 5px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.shipperDescripation : stack1), depth0))
    + "</div>\n				          <br>\n				           <div id='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.entryId : stack1), depth0))
    + "_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.door_id : stack1), depth0))
    + "_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.bol_id : stack1), depth0))
    + "_1' style=\"padding-left: 5px; border-bottom:1px solid black;font-family:Arial;font-size:12px; height:70px;\">\n				              		  <div style=\"float:left;\">\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1),{"name":"if","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				              		  </div>\n				          		 	<div style=\"float:right;padding-top:50px;font-size:10px;\" >&nbsp;&nbsp; \n				          			\n				          			&nbsp;&nbsp;\n				          		</div>\n\n				              \n				          </div>\n				          \n 				          <div style=\"padding-left: 5px; font-family:Arial;font-size:12px;\">\n				          	<div style=\"float:left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.signaturePrintName : stack1), depth0))
    + "</div> \n				          	<div style=\"float:right;margin-right: 50px;\" >Date</div>\n				          </div>\n				        </td>\n				        <td style=\"border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;\" valign=\"top\" width=\"14%\">\n				          <div style=\"text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;\">\n				          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.trailerLoader : stack1), depth0))
    + ":</div>\n				          <div style=\"margin-top:5px;margin-left:5px;text-align:left;font-size:12px;\">\n				          	<img alt=\"\" src=\"/Sync10/administrator/imgs/print/cha.jpg\">\n				          	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.byShipper : stack1), depth0))
    + "\n				          </div>\n				          <div style=\"margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;\">\n				            <p><img alt=\"\" src=\"/Sync10/administrator/imgs/print/kongcha.jpg\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.byDriver : stack1), depth0))
    + "</p>\n				          </div>\n				        </td>\n				        <td style=\"border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;\" valign=\"top\" width=\"16%\">\n				          <div style=\"text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;\">\n				          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.freightCounted : stack1), depth0))
    + ":</div>\n				          <div style=\"margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;\">\n				          <img alt=\"\" src=\"/Sync10/administrator/imgs/print/cha.jpg\">\n				          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.byDriverPieces : stack1), depth0))
    + "\n				          </div>\n				          <div style=\"margin-left:5px;margin-top:12px;font-family:Arial;font-size:12px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.noShipperLoaderCount : stack1), depth0))
    + "</div>\n				        </td>\n				        <td id=\"carrierSign\" style=\"border-bottom:1px solid black;border-right:1px solid black;\" valign=\"top\" width=\"36%\">\n				          <div style=\"font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;margin-top:5px;\">\n				          	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierSignaturePickUpDate : stack1), depth0))
    + "\n				          </div>\n				          <div style=\"font-size:7px;font-family:Arial;margin-left:5px;margin-right:5px;\">\n				          	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierDescripation : stack1), depth0))
    + "\n						  </div>\n				          <div style=\"font-style: italic;font-size:9px;font-weight: bold;margin:5px 5px 0px 5px;font-family:Arial;\">\n				          	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.propertyDescribed : stack1), depth0))
    + "\n				          </div>\n				        <div id='"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.entryId : stack1), depth0))
    + "_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.door_id : stack1), depth0))
    + "_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.bol_id : stack1), depth0))
    + "_2' style=\"border-bottom:1px solid black;font-family:Arial;font-size:12px; height:70px;  \">\n				              		  <div style=\"float:left; margin-left:5px;\">\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1),{"name":"if","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				              		  </div>\n\n				          		 <div style=\"float:right;padding-top:50px;font-size:10px;\" >&nbsp;&nbsp;\n				          			&nbsp;&nbsp; \n				          		</div>\n				          </div>\n				          <div style=\"font-size:12px;font-family:Arial;margin-left:5px;\">\n				          	<div style=\"float:left; margin-right:50px;\">Signature/ Print Name</div> \n				          	<div style=\"float:right; margin-right:60px;\" >Date</div>			      \n				          </div>\n				          <div>&nbsp;</div>\n				          <div style=\"font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;\"> \n				          	  <div style=\"float:left; width:100%\">\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.gateIn : stack1), depth0))
    + ":&nbsp;\n									  	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.gateInDate : stack1), depth0))
    + "\n								</span>&nbsp;&nbsp;&nbsp;&nbsp;\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.windowIn : stack1), depth0))
    + ":&nbsp;\n										"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.windowInDate : stack1), depth0))
    + "\n								</span>\n				             </div>\n				             <div style=\"float:left; width:100%\">\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.dockIn : stack1), depth0))
    + ":&nbsp;\n									  	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.dockInDate : stack1), depth0))
    + "\n								</span>&nbsp;&nbsp;&nbsp;&nbsp;\n								<span style=\"font-style: italic;font-size:10px;font-weight: normal;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.dockClose : stack1), depth0))
    + ":&nbsp;\n									  	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.equipmentCheckDateMap : stack1)) != null ? stack1.dockOutDate : stack1), depth0))
    + "\n								</span>\n				             </div>				        	\n				          </div>\n				        </td>\n				      </tr>\n				    </tbody></table>\n	        </td>\n	      </tr>\n			<!-- ################################################ -->\n	    </tbody>\n	</table>\n    </div>\n    \n<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ --> \n	<div id=\"a4\">\n		<table  cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n	      <tbody><tr>\n	        <td colspan=\"8\" align=\"left\" valign=\"top\" bgcolor=\"#000000\"  height=\"19\">\n	          <div style=\"background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;\" align=\"center\">\n	          	"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderInformation : stack1), depth0))
    + "\n	          </div>\n	        </td>\n	      </tr>\n	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\"  width=\"24%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.customerOrderNumber : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.pkgs : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.weight : stack1), depth0))
    + "</td>\n	        <td colspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\"><span style=\"font-weight:bold;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.palletSlip : stack1), depth0))
    + "</span><br><span style=\"font-size: 9px;\">("
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.circleOne : stack1), depth0))
    + ")</span></td>\n	        <td colspan=\"3\" style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.additionalShipperInfo : stack1), depth0))
    + "</td>\n	      </tr>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.order : stack1),{"name":"if","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\" height=\"15\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderCount : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#FFFFFF\">\n				"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.orderWeight : stack1), depth0))
    + "\n	        </td>\n	        <td colspan=\"5\" style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" bgcolor=\"#999999\">&nbsp;</td>\n	      </tr>\n	    </tbody></table>\n	</div>\n\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.total : stack1),"6",">",{"name":"xifCond","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n	    \n \n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"4":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\n	        	1\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">Plts</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	        	"
    + alias2(alias1((depth0 != null ? depth0.total_package : depth0), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">CTNS</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + alias2(alias1((depth0 != null ? depth0.weight : depth0), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n	       	<td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	       	</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	        &nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n	      </tr>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"7":function(depth0,helpers,partials,data) {
    var stack1;

  return "	     	<tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\n	        	0\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">Plts</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	        	1\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">CTNS</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1)) != null ? stack1.weight : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n	       	<td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	       	\n	       	</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n	      </tr>\n";
},"9":function(depth0,helpers,partials,data) {
    var stack1;

  return "	       <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" colspan=7>\n	        	"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.seeAttachedSupplementPage : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n";
},"11":function(depth0,helpers,partials,data) {
    return "		      <tr>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n		      </tr>\n";
},"13":function(depth0,helpers,partials,data) {
    var stack1;

  return "										<img class=\"shipper_image\" src=\"http://192.168.1.11/Sync10/"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1)) != null ? stack1.shipper_relative_file_path : stack1), depth0))
    + "\" width=\"150px\" height=\"70px\" id=\"androidGetImage\"/>\n";
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return "										<img class=\"carrier_image\" src=\"http://192.168.1.11/Sync10/"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.relative : stack1)) != null ? stack1.carrier_relative_file_path : stack1), depth0))
    + "\" width=\"150px\" height=\"70px\" />\n";
},"17":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.orderList : stack1)) != null ? stack1.order : stack1),{"name":"each","hash":{},"fn":this.program(18, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"18":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\"  width=\"24%\">\n	        	"
    + alias2(alias1((depth0 != null ? depth0.waybill_id : depth0), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">\n	        	"
    + alias2(alias1((depth0 != null ? depth0.totalPackage : depth0), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"12%\">\n	        	"
    + alias2(alias1((depth0 != null ? depth0.weightPackage : depth0), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"6%\">Y</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\" width=\"6%\">N</td>\n	        <td colspan=\"3\" style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">	\n	        </td>\n	      </tr>\n";
},"20":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "	 <div id=\"a5\">\n      <!-- CARRIER INFORMATION -->\n      \n       <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n	     <thead>\n	      <tr>\n	        <td colspan=\"9\" style=\"color:#FFF;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#000000\" height=\"19\" valign=\"top\">\n	           "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.carrierInformation : stack1), depth0))
    + "\n	        </td>\n	      </tr>     \n	      <tr>\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.handling : stack1), depth0))
    + " <br>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.util : stack1), depth0))
    + "</td>\n	        <td colspan=\"2\" style=\"font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.packages : stack1), depth0))
    + "</td>\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\"><span style=\"font-weight:bold\"><span style=\"font-weight:bold\">WEIGH</span>T</span></td>\n	        <td rowspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">H.M.<br>(X)</td>\n	        <td rowspan=\"2\" style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial; \" align=\"center\" width=\"38%\"><span style=\"font-weight:bold\">COMMODITY DESCRIPTION</span><br>\n	        <span style=\"font-family:Arial;font-size:7px; padding-left:5px;padding-right:5px;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.commDescripation : stack1), depth0))
    + "</span>\n			</td>\n	        <td colspan=\"2\" style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.ltlOnly : stack1), depth0))
    + "</td>\n	      </tr>\n	      <tr>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.type : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"7%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.qty : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"8%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.type : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.nmfc : stack1), depth0))
    + "</td>\n	        <td style=\"font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" width=\"9%\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.clasz : stack1), depth0))
    + "</td>\n	      </tr>\n	     </thead>  \n	      <tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1),{"name":"if","hash":{},"fn":this.program(21, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1['package'] : stack1),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.palletCount : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalPackageCount : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.totalWeight : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\"></td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-weight: bold;\" align=\"center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.i18n : depth0)) != null ? stack1.grandTotal : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\" bgcolor=\"#999999\">&nbsp;</td>\n	      </tr>\n	    </tbody>\n	 </table>\n	</div>\n";
},"21":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1),{"name":"each","hash":{},"fn":this.program(22, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"22":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "	      <tr>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">\n	        	1\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">Plts</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	        	"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1)) != null ? stack1.total_package : stack1), depth0))
    + "\n	        </td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">CTNS</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.datas : depth0)) != null ? stack1.pltOrpckMap : stack1)) != null ? stack1.pallet : stack1)) != null ? stack1.weight : stack1), depth0))
    + "</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;\" align=\"center\">&nbsp;</td>\n	       	<td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n	       	\n	       	</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n	        <td style=\"border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;\" align=\"center\">\n\n	        &nbsp;</td>\n	      </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.datas : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "";
},"useData":true});
return templates;
});