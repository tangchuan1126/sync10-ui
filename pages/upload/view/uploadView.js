"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "jquery.ui.widget",
  "jQueryFileUpload"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates,widget,upload) {
  
 
  var LableView = Backbone.View.extend({
      el:"#container_upload",
      template:templates.upload,
      render:function(){
        // console.log($('#fileupload'));
      },
      events:{
        "click #button_upload":"down"
      },
      down:function(evt){
        this.$el.html(this.template());
        $('#fileupload').fileupload({
            formAcceptCharset:'utf-8',
            acceptFileTypes:/(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize:1 * 1000000,
            maxNumberOfFiles:1 
        });	//初始化
        $("#file_up_type").html("(gif,jpg,jpeg,png)");
        this.$el.dialog({
            title: "Upload Photo",
            draggable: true,
            width: 700,
            height: 500,
            modal: true,
        });
      }
    });
  return LableView;
}); //page_init