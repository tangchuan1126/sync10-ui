"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "./view/uploadView", 
  "jqueryui/tabs",
  "jqueryui/dialog",
  "jquery.ui.widget",
  "jQueryFileUpload"
  
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, uploadView,widget,upload) {
 
    var uploadView = new uploadView();

    uploadView.render();
    


}); //page_init