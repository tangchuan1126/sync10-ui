define(["jquery"],function($){
	return {
		showPictrueOnline: function (fileWithType,fileWithId ,currentName,productFileType,uploadPath) {
			var obj = {
				file_with_type : fileWithType,
				file_with_id : fileWithId,
				current_name : currentName ,
				product_file_type : productFileType,
				cmd : "multiFile",
				table : 'file',
				base_path : "/Sync10/upload/"+uploadPath
			}
			return obj;
		}
	}
});