
requirejs.config({
    paths: {
        "oso.bower": "/Sync10-ui/bower_components",
        "oso.lib": "/Sync10-ui/lib",
        "require_css": "/Sync10-ui/lib/require-css/css.min",
        "jquery": "/Sync10-ui/lib/appointment_autolayout/js/jquery/1.8.3/jquery.min",
        "fullPage": "/Sync10-ui/pages/appointment_autolayout/js/fullpage/fullPage",
        "select2": "/Sync10-ui/lib/appointment_autolayout/js/select2.full",
        "moment": "/Sync10-ui/bower_components/moment/min/moment.min",
        "iscroll": "/Sync10-ui/lib/appointment_autolayout/js/iscroll",
        "smalot-bootstrap-datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "datetimepicker-plus": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "Font-Awesome": "/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min",
        "bootstrap-css": "/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min",
        "select2-css": "/Sync10-ui/lib/appointment_autolayout/css/select2.min",
        "datetimepicker-css": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "smalot-datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "datetimepicker-plus-css": "/Sync10-ui/lib/appointment_autolayout/css/bootstrap-datetimepicker-plus",
        "default-css": "/Sync10-ui/pages/appointment_autolayout/css/default1",

        "backbone":"/Sync10-ui/bower_components/backbone/backbone.min",
        "templates":"templates/templates.amd",
        "handlebars.runtime":"/Sync10-ui/bower_components/handlebars/handlebars.runtime.amd.min",
        "handlebars":"/Sync10-ui/bower_components/handlebars/handlebars.amd.min",
        "underscore":"/Sync10-ui/bower_components/underscore/underscore.min",
        "artDialog":"/Sync10-ui/lib/artDialog/src",
        "immybox":"/Sync10-ui/lib/immybox-master/jquery.immybox"
    },
    shim: {        
        // "abc2": {
        //     deps: ["require_css!abc2_css"]
        // }
    }
});


require(["jquery","fullPage","iscroll", "../js/view/loginView"],
function(jq_,_fullPage,_iscroll, LoginView)
{
    var runPage = new FullPage({
      id: 'pageContain',
      slideTime: 800,
      effect: {
        transform: {
          translate: 'X'
        },
        opacity: [0, 1]
      },
      mode: 'wheel, touch',
      easing: 'ease',
      callback: function(index, thisPage){
       // console.log(index+1);
        //console.log(thisPage);
      }
    });
        
    var cssall = [
            "require_css!Font-Awesome",
            "require_css!bootstrap-css",
            "require_css!default-css",
            "require_css!select2-css",
            "require_css!datetimepicker-css",
            "require_css!smalot-datetimepicker-css",
            "require_css!datetimepicker-plus-css"
        ];
    var browserv=navigator.userAgent;
    if(/MSIE/.test(browserv)) cssall=[];  
    require(cssall, function(){
           //进入login
          var data = {admin:null},
          cssCore = function(testCss) {
              switch (true) {
                case testCss.webkitTransition === '':
                return 'webkit'; break;
                case testCss.MozTransition === '':
                return 'Moz'; break;
                case testCss.msTransition === '':
                return 'ms'; break;
                case testCss.OTransition === '':
                return 'O'; break;
                default:
                return '';
              }
          }(document.createElement('liuyi').style),
          //var eventAcrossView =  _.extend(data, Backbone.Events);
          eventAcrossView =  $.extend(true, {"cssCore":cssCore},data, Backbone.Events);
          loginView = new LoginView({"eventAcrossView":eventAcrossView}).render();

         runPage.next();
        require(["select2",
          "moment",
          "datetimepicker-plus",
          "smalot-bootstrap-datetimepicker",
          "../js/view/inOutView",
          "../js/view/loadView",
          "../js/view/calendarView",
          "../js/view/carrierView",
          "../js/view/previewView"
        ],function(select2,
          moment,
          Acars_datetimepicker,
          smalot_datetimepicker,
          InOutView,
          LoadView,
          CalendarView,
          CarrierView,
          PreviewView)
        {
            //登陆验证
            //返回登陆页面或者关闭页面退出登陆
            //换账号登陆，需要销毁
          
            var inOutView = null;
            var loadView = null;
            var calendarView = null;
            var carrierView = null;
            var previewView = null;
            eventAcrossView.on('showInOut',  function(data) {
                if(inOutView == null)
                {
                     inOutView = new InOutView({"eventAcrossView":eventAcrossView}).render();
                      runPage.next();
                } 
                else
                {    
                      //通过缓存的页面来展示页面
                     if(data && data.display=="show")
                     {
                        inOutView.show();
                        runPage.prev();
                     }
                     else
                     {
                         //通过对象重新渲染
                         inOutView.render(); 
                        runPage.next();
                     }  
                }

            });

            eventAcrossView.on('showLoad',  function(data) {
                var inOutData = inOutView.getData();
                if(loadView == null)
                {
                     loadView = new LoadView({"eventAcrossView":eventAcrossView}).render(inOutData);
                      runPage.next();
                } 
                else
                {    
                      //通过缓存的页面来渲染页面
                     if(data && data.display=="show")
                     {
                        loadView.show();
                         runPage.prev();
                     }
                     else
                     {
                         //通过对象重新渲染
                         loadView.render(inOutData); 
                         runPage.next();
                     }  
                }
            });

            eventAcrossView.on('showCalendar',  function(data) {
                var inOutData  = inOutView.getData();
                if(calendarView == null)
                {
                     calendarView = new CalendarView({"eventAcrossView":eventAcrossView}).render(inOutData);
                      runPage.next();
                }
                else
                {
                    if(data && data.display=="show")
                    {
                        calendarView.show();
                         runPage.prev();
                    }   
                    else
                    {
                        calendarView.render(inOutData);
                         runPage.next();
                    } 
                }
            });

            eventAcrossView.on('showCarrier',  function(data) {
                var calendarData = calendarView.getData();
                if(carrierView == null)
                {
                    carrierView = new CarrierView({"eventAcrossView":eventAcrossView}).render(calendarData);
                     runPage.next();
                }
                else
                {
                     if(data && data.display=="show")
                     {
                         carrierView.show();
                          runPage.prev();
                     }
                     else
                     {
                         carrierView.render(calendarData);
                          runPage.next();
                     } 
                }
            });

            eventAcrossView.on('showPreviewView',function(){
                var inOutData = inOutView.getData();
                var loadData = loadView.getData();
                var calendarData = calendarView.getData();
                var carrierData = carrierView.getData();

                var previewData =   {
                      "inOutData":inOutData,
                      "loadData":loadData,
                      "calendarData":calendarData,
                      "carrierData":carrierData
                    }
                if(previewView == null)
                    previewView = new PreviewView({"eventAcrossView":eventAcrossView}).render(previewData);
                else
                    previewView.render(previewData);
                    runPage.next();
            });

            eventAcrossView.on('distroy',function(){
              $("inout").html('');
              $("load").html('');
              $("carrier").html('');
              $("calendar").html('');
              inOutView= null;
              loadView = null;
              calendarView = null;
              carrierView = null;
            });
          
            window.onunload=function(){
              loginView.loginout();
            }
        });
       

     });
});
