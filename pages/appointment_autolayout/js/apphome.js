(function(global, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], function($) {
            return factory($, global, global.document, global.Math);
        });
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'), global, global.document, global.Math);
    } else {
        factory(jQuery, global, global.document, global.Math);
    }
})(typeof window !== 'undefined' ? window : this, function($, window, document, Math, undefined) {
    'use strict';

    

    function winsize(){

     return  {
        win_h: $(window).height(),
        doc_h: $(document).height(),
        win_w: $(window).width(),
        doc_w: $(document).width()
    }

    }

    var apphonme = function(options) {

        this.opt = {};
        this._doc = "";
        this.wind_sizetime="";
        if (options) {
        
        this.opt = $.extend(this.opt, options);

            if (this.opt.appbody && typeof this.opt.appbody != "undefined") {
                this._doc = $(this.opt.appbody);
            }

        }

        this._int();

    }

    apphonme.prototype = {
        win_size:null,
        _int: function() {            
            var _this = this;
            _this.win_size = winsize();

            var this_opt = _this.opt;

        
            _this._re_w_h();

            _this._event();
        },
        _event: function() {
            var _this = this;
            var this_opt = _this.opt;

        $(window).resize(function() {

        if(_this.wind_sizetime!="" && typeof _this.wind_sizetime!="undefined"){
            clearTimeout(_this.wind_sizetime);
        }

       _this.wind_sizetime=setTimeout(function(){ 

                 _this.win_size = winsize();
                _this._re_w_h();
                //���ڴ�С����
                if (this_opt.resizeCallback && this_opt.resizeCallback!=null) {
                    this_opt.resizeCallback(_this._doc);
                }        


            },60);

          });

        },
        _re_w_h: function() {


            var _this = this;
            var this_opt = _this.opt;
            var ScrSize = _this.win_size;
            //�������߿�
            var autow_h={
                width: ScrSize.win_w + "px",
                height: ScrSize.win_h + "px"
            }         
            
          
           _this._doc.css(autow_h); 
              //ÿ���߿�
            var li_slide=_this._doc.find(".slide");
            li_slide.css({
                width: ScrSize.win_w + "px",
                height: ScrSize.win_h + "px"
            });

            var Center_Container=_this._doc.find(".Center-Container");
            Center_Container.css({
                width: ScrSize.win_w + "px",
                height: (ScrSize.win_h - 1) + "px"
            });


            var fixed_Container_top=_this._doc.find(".fixed_Container_top");
            var _topfixed=44;
            fixed_Container_top.css({
                "width": ScrSize.win_w + "px",
                "height": (ScrSize.win_h - _topfixed) + "px",
                "margin-top":_topfixed+"px"
            });


        var fixed_Container_top_foo=_this._doc.find(".fixed_Container_top_foo");
          var _foofixed=31;          
          fixed_Container_top_foo.css({
                "width": ScrSize.win_w + "px",
                "height": ScrSize.win_h - (_topfixed + _foofixed) + "px",
                "margin-top":_topfixed+"px"
            });
          
  
       var fixed_Container_top_foo1=_this._doc.find(".fixed_Container_top_foo1");
          var _foofixed1=44;          
          fixed_Container_top_foo1.css({
                "width": ScrSize.win_w + "px",
                "height": ScrSize.win_h - (_topfixed + _foofixed1) + "px",
                "margin-top":_foofixed1+"px"
            });

        setTimeout(function(){ 
          _this._doc.removeClass('hide');
         },10);


        }

    };


    return apphonme;

});
