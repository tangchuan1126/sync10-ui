define(["jquery","backbone","../../config","../../templates/templates.amd","../../MsgHelper","immybox"],
function($,Backbone,config,templates,MsgHelper,immybox) {
	return Backbone.View.extend({
		el:"#inout",
		template: templates.inOut,
		//model:new (Backbone.Model.extend({url:""}))(),//
		hubs:false,
		hubInfo:null,
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		render: function() {
			var that = this;
			this.$el.html(this.template());
			this.initHub();
			return this;
		},
		events:{
			"click #inbound": "selectInOut",
			"click #outbound": "selectInOut",
			"click #inOutContainer .nextStep": "nextStep"
		},
		initHub:function()
		{
			var that = this;
			if(that.hubs)
			{
			   var rls = [];
                for (var i = 0; i < that.hubs.length; i++) {
                    rls.push({
                        "VALUE": that.hubs[i].data,
                        "TEXT": that.hubs[i].name
                    });
                }
				/**$("#hub").immybox({
			 		 choices: rls,
			 		 Pleaseselect:"Clear Select",
			 		 change:function(e,d){
			 		 	var hub = d.value;
			 		 	that.getHubInfo();
			 		 }, 
				});**/
				var obj = $("#hub")[0];
				var options = obj.options;
				options.length = 0;
				options.add(new Option('',''));
				for(var i =0,len = that.hubs.length;i<len ; i++)
				{
					options.add(new Option(that.hubs[i].name,that.hubs[i].data));
				}
			}
			else
			{
                $.getJSON(config.getHubs.url + "?type=1&lv=0&rootType=Ship+From&Reqnumr=" +new Date().getTime(), function(json) {
                    that.hubs = json;
					that.initHub();
				});
			}
		},
		getHubInfo:function(hub_id){
			var that = this;
			var selecedtHub = {};
			if(that.hubs)
			{
				$.each(that.hubs, function(index, hub) {
					 if(hub.data == hub_id)
					 {
					 	selecedtHub = hub;
					 }
				});
			}
			return selecedtHub;
		},
		nextStep:function(e)
		{	
			var that = this;
			var inOutData = this.getData();
			console.log(inOutData);
			if(inOutData.hub == undefined || inOutData.hub=="")
			{
				MsgHelper.showMessage('Please select Hub!','error');
				return false;
			}
			if(!inOutData.type)
			{
				MsgHelper.showMessage('Please select Inbound or Outbound!','error');
				return false;
			}
			that.eventAcrossView.trigger('showLoad',{"display":"render"});
		},
		selectInOut:function(e)
		{
			$("#inOutBtn .btn").removeClass('btn-primary');
			$(e.target).addClass('btn-primary');
			this.nextStep();
		},
		getData:function()
		{
			/**  使用imbox控件 以下面方式获取值
			return {
			 "hub":$("#hub").attr('data-value'),
			 "hub_name":$("#hub").val(),
			 "type":$("#inOutBtn .btn-primary").attr("data-value")
			}; **/
			return {
			 "hub":$("#hub").val(),
			 "hub_name" :$("#hub option:selected").text(),
			 "type":$("#inOutBtn .btn-primary").attr("data-value")
			}
		},
		getAllData:function()
		{
			var data = this.getData();
			var selecedtHub = this.getHubInfo(data.hub);
			return $.extend(true, data, selecedtHub);
		},
	});
});