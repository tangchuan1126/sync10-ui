define(["jquery","backbone","../../config","../../templates/templates.amd","../../MsgHelper","iscroll","artDialog/dialog"],
function($,Backbone,config,templates,MsgHelper,iscroll,Dialog) {
	return Backbone.View.extend({
		el:"#load",
		template: templates.load,
		orderTemplate:templates.orderTemplate,
		panelWidth:250,
		//model:new (Backbone.Model.extend({url:""}))(),
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		events:{
			"click #loadContainer .nextStep": "nextStep",
			"click #loadContainer .prevStep": "prevStep",
			"click #searchOrder": "searchOrder",
			"click #loadContainer .orderPanel .fa-remove":"deleteOrder",
			"keyup #refNo":"quickSearchOrder",
			"click .trunkbox input":"selectTrunkModel",
			"click .Slidcontainerpano .text-left":"transferRight",
			"click .Slidcontainerpano .text-right":"transferLeft"
		},
		render: function(inOutData) {
			var that = this;
			this.inOutData = inOutData;
			this.$el.html(this.template(this.initRenderData(inOutData)));
			this.initScroll();
			this.panelWidth = $(".orderPanel:first-child").width()+15;
			return this;
		},
		selectTrunkModel:function(e){
			//为了支持IE，采用JS驱动，放弃CSS驱动
			$(".trunkbox").removeClass('ckbox-success');
			$(e.target).parent().addClass('ckbox-success');
			//$(e.target).attr({checked: true});
			$("#refNo").attr({disabled: false});
			if(e.target.id=='oneLoad')
			{
				var data = this.getData();
				if(data.loads.length > 0 || data.orders.length > 0){
					var that = this;
					//如果已经选择了 mutiload 当改为oneload时，提示是否清单据
					var d= Dialog({
						content:'<i class="fa fa-exclamation-circle"></i><span class="waring-content">This operation will clear all the selected invoices!<span>',
						title:'Waring',
						skin:'waring',
						button:[
							{
								value:'Confirm',
								callback:function(){
									$("#loadSilder .orderPanel:not(.hide)").remove();
								},
								autofocus:false
							},
							{
								value:'Cancel',
								callback:function(){
									$(".trunkbox").removeClass('ckbox-success');
									$("#multiLoad").attr('checked',true).parent().addClass('ckbox-success');
									$("#oneLoad").attr('checked',false);
								},
								autofocus:true
							},
						],
						close:function(){
							d.remove();
						}
					}).showModal();
				}
			}
		},
		getLoadModel:function(){
			var model = "";
			var selected = $(".trunkbox input[type='radio']:checked");
			if(selected)
			{
				model = selected.attr('id');
			}
			return model;
		},
		initScroll:function(){
	       	 if (!this.eventAcrossView.browser.iebrowsev){
		       	this.scroll =  new IScroll('.Slidcontainer', {
		                scrollX: true,
		                scrollY: false,
		                hScrollbar: true,
		                mouseWheel: true,
		                click:this.eventAcrossView.browser.isMobile
		            });  
	       	 }else
	       	 {
	       	 	 $(".trunkbox").removeClass('ckbox');
	       	 }
		},
		initScrollWidth:function(panelCount,isGoHead){
            var sub_panel = $("#loadContainer .orderPanel");
            var panel_w = this.panelWidth;

            if (sub_panel.eq(0).width() > 0) 
            {
                panel_w = sub_panel.eq(0).width()+14;
            }
            var scrollw = (panel_w * panelCount) + 20;
             $("#loadContainer .Slidecard_box").width(scrollw); 
			if(this.scroll)
			{
				var that = this;
				if(isGoHead)
				{
					this.scroll._translate(0,0);
				}
				setTimeout(function(){
					that.scroll.refresh();
				},500);
				
			}else
			{
				$(".Slidecard_box").css('left', "0px");
			}
		},
		transferLeft:function(x){
			if(this.scroll)
			{
				//手机谷歌火狐等采用CSS3
				$(".Slidecard_box").css({
					"transition-duration": '800ms'
				});
				var parent = $("#loadSilder").width();
				//移动到最左边，不允许再左移
				var isTransfer = $("#loadSilder .Slidecard_box").width() + this.scroll.x - parent > 0;
				if(isTransfer)
				{
					var moveLen =  $("#loadSilder .Slidecard_box").width() + this.scroll.x -this.panelWidth - parent;
					if(moveLen <= 0)
					{
						//如果移动后超出右侧
						this.scroll._translate(this.scroll.x- this.panelWidth-moveLen,0);
					}
					else
					{
						this.scroll._translate(this.scroll.x- this.panelWidth,0);
					}
				}
			}else
			{	//IE采用animate
				var left = $(".Slidecard_box").position().left-this.panelWidth;
				var silderWidth = $("#loadContainer .Slidecard_box").width();
				var parentWidth = $("#loadSilder").width();
				if(silderWidth + left <= parentWidth)
				{
					left = parentWidth - silderWidth ;
				}
				if(true)
				{
					$(".Slidecard_box").animate({'left': left+'px'},'slow',function() {});
				}
			}
		},
		transferRight:function(){
			if(this.scroll)
			{
				$(".Slidecard_box").css({
					"transition-duration": '800ms'
				});
				//移动到最右边，不允许再左移
				var isTransfer = this.scroll.x < 0;
				if(isTransfer)
				{
					var moveLen = this.scroll.x+this.panelWidth >=0 ?0:this.scroll.x+this.panelWidth;
					this.scroll._translate(moveLen,0);
				}
			}
			else
			{

				var right = $(".Slidecard_box").position().left+this.panelWidth;
				if(right >= 0)
				{
					right = 0;
				}
				$(".Slidecard_box").animate({'left': right+'px'},'slow',function() {});
			}
		},
		initRenderData:function(inOutData)
		{
			var rendData = {"placeHolder":"Load/Order/PO"};
			if(inOutData.type=="inbound")
			{
				rendData.placeHolder = "Load/Order";
			}
			return rendData;
		},
		quickSearchOrder:function(e){
			if(e.keyCode==13)
			{
				this.searchOrder();
			}
		},
		searchOrder:function()
		{
			var that = this;
			var loadFlag = this.getLoadModel =="oneLoad"?0:1;
			var otype = this.inOutData.type == "inbound" ?"in":"out";
			var searchNo = $("#refNo").val();
			if($.trim(searchNo)=="")return;
			param = {
				"checkLIds": this.getSelectedLoad(),
				"checkOIds":this.getSelectedOrder(),
                "otype":otype,
                "rfno":searchNo,
                "loadFlag":loadFlag
			};

			/**
			var rows =[{"order_no":"001","pallets":"70","weights":"100","city":"Valley","state":"CA","type":"order"},
			{"load_no":"L00005","pallets":"70","weights":"1256","city":"BJ","state":"BJ","type":"load"}];
			that.addOrder(rows); **/
			d = Dialog({ id:'1'}).showModal();
			$.ajax({
				url: config.LRList.url,
				type: 'POST',
				dataType: 'json',
				data: {"0":param,"rows":20,"page":1},
			})
			.done(function(data) {
				var orders = data.rows;
				if(orders.length > 0 )
				{
					that.addOrder(orders);
				}
				else
				{
					MsgHelper.showMessage('No invoices found ！','alert');
				}
			})
			.fail(function(e) {
				MsgHelper.showMessage('Find invoices failed ！','error');
			}).always(function(){
				d.close().remove();
			});  
		},
		getSelectedLoad:function()
		{
			var loads = this.getData().loads;
			var ids = new Array();
			$.each(loads, function(index, load) 
			{
				ids.push(load.id);
				//if(index == 0)ids = load.id;elseids = "," +ids;
			});
			return ids;
		},
		getSelectedOrder:function()
		{
			var orders = this.getData().orders;
			var ids = "";
			$.each(orders, function(index, load) 
			{
				ids.push(load.id);
				//if(index == 0)ids = load.id;else ids = "," +ids;
			});
			return ids;
		},
		addOrder:function(orders)
		{
			var that = this;
			var data = this.getData();
			var ignoreCount = 0;
			var addedCount = 0;
			var notMacthCount = 0;
			$("#loadSilder .orderPanel").removeClass('new');
			$.each(orders, function(index, orderInfo) {
				if(orderInfo.scac != that.eventAcrossView.admin.SCAC)
				{
					//不属于登陆的carrier，过滤掉
					notMacthCount ++;
					return true;
				}
				orderInfo.isNew = true;
				if(orderInfo.type=='order')
				{
					orderInfo.orderType ="Order";
					if(that.findOrder('id',orderInfo.id,data.orders))
					{
						ignoreCount++;
						return true;
					}
				}
				else
				{
					orderInfo.orderType ="Load";
					if(that.findOrder('id',orderInfo.id,data.loads))
					{
						ignoreCount++;
						return true;
					}
				}
				$("#loadSilder .orderPanel:first-child").before($(that.orderTemplate(orderInfo)).data('value',orderInfo));
				addedCount ++;
			});
		
			if(addedCount > 0)
			{
				MsgHelper.showMessage(addedCount+' invoices added !','succeed');
			}
			if(ignoreCount > 0)
			{
				MsgHelper.showMessage(ignoreCount+' invoices ingored because of repeat !','alert');
			}
			if(notMacthCount > 0)
			{
				var scac = that.eventAcrossView.admin.SCAC;
				MsgHelper.showMessage('Invoices that  not belong to '+ scac +' have been passed!','alert');
			}
			
			var panelCount = this.getCount();
			this.initScrollWidth(panelCount,true);
			// 单load 直接下一步
			//this.valiNext();
		},
		findOrder:function(idAttribute,id,collection){
			var result = false;
			$.each(collection, function(index, obj) {
				if(obj[idAttribute]==id){
				 	result = true;
				 	return false;
				}
			});
			return result;
		},
		deleteOrder:function(e)
		{
			var that = this;
			if(!this.eventAcrossView.browser.iebrowsev)
			{
				$(e.target).parent().parent().parent().addClass('shrink');
				setTimeout(function(){
					$(e.target).parent().parent().parent().remove();
					var panelCount = that.getCount();
					that.initScrollWidth(panelCount);
				},800);
			}
			else
			{
				// for IE
				$(e.target).parent().parent().parent().animate({
					width: '0px',
					height:'0px'},
					'slow', function() {
						$(e.target).parent().parent().parent().remove();
						var panelCount = that.getCount();
						that.initScrollWidth(panelCount);
				});
			}
		},
		valiNext:function()
		{
			if(this.getLoadModel() == 'oneLoad')
			{
				//手机谷歌浏览器通过程序执行下一步无法触发 动画效果
				//$("#loadContainer .nextStep")[0].click();
				this.nextStep();
			}
		},
		nextStep:function()
		{
			if(this.validate())
			{
				var that = this;
				that.eventAcrossView.trigger('showCalendar',{display:"render"});
			}
		},
		prevStep:function(){
			var that = this;
			//上一步，销毁本页面,但是缓存对象
			that.eventAcrossView.trigger('showInOut',{display:"show",callback:function(){
				//$(that.el).html('');
			}});
		},
		validate:function(){
			var data = this.getData();
			if(data.loads.length == 0 &&  data.orders.length ==0)
			{
				MsgHelper.showMessage("Please add Load/Order first !","error");
				return false;
			}
			else
			{
				return true;
			}
		},
		getData:function()
		{
			var loads = [],orders =[];
			$.each($("#loadContainer .orderPanel"), function(index, obj) {
				var data = $(obj).data('value');
				if(data){
					if(data.type=='load')
					{
						loads.push(data);
					}else if(data.type=='order')
					{
						orders.push(data);
					}
				}
			});
			return {"loads":loads,"orders":orders};
		},
		getCount:function(){
			var data  = this.getData();
			$("#loadCount").html(data.loads.length);
			$("#orderCount").html(data.orders.length);
			return data.loads.length+data.orders.length;
		},
	});
});