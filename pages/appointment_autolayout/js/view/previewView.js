define(["jquery","backbone","../../config","../../templates/templates.amd","iscroll","artDialog/dialog","../../MsgHelper"],
function($,Backbone,config,templates,iscroll,Dialog,MsgHelper) {
	return Backbone.View.extend({
		el:"#preview",
		template: templates.preview,
		//model:new (Backbone.Model.extend({url:""}))(),
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		events:{
			"click #previewContainer .prevStep": "prevStep",
			"click #submitBtn .btn":"saveData"
		},
		render: function(data) {
			var that = this;
			this.data = data;
		
			if(this.eventAcrossView.browser.isMobile || this.eventAcrossView.browser.iebrowsev)
			{
				data.isMobile = true;
			}
			this.$el.html(this.template(data));
			this.ininScroll();
			return this;
		},
		ininScroll:function()
		{	
			if (!this.eventAcrossView.browser.iebrowsev && this.eventAcrossView.browser.isMobile){
				 new IScroll('#previewContainer .scrollWrap');
				 $("#previewContainer .scrollWrap").css('overflow', 'hidden');
			}
			else
			{
				 $("#previewContainer .scrollWrap").mCustomScrollbar();
			}
		},
		touchEvent:function()
		{
			var that = this;
			var touch = ('ontouchstart' in window) ||  window.DocumentTouch && document instanceof DocumentTouch;
			var pageContain =$(".scrollWrap")[0];
			touchEvent = {
				start : function(e) {
					var touches = e.touches[0];
					start = {
						x : touches.pageX,
						y : touches.pageY,
						time : +new Date
					}

					pageContain.addEventListener('touchmove', touchEvent.move, false);
					pageContain.addEventListener('touchend', touchEvent.end, false);
				},
				move : function(e) {

					var touches = e.touches[0];

					e.preventDefault();
					// ensure swiping with one touch and not pinching
						if ( event.touches.length > 1 || event.scale && event.scale !== 1) return

					delta = {
						x : touches.pageX - start.x,
						y : touches.pageY - start.y
					}
					
				},
				end : function(e) {

					var touches = e.changedTouches[0],
						duration = +new Date - start.time,
						abs = {},
						nextDiff = 0,
						isValidSlide = false;

					delta = {
						x : touches.pageX - start.x,
						y : touches.pageY - start.y
					}
					abs = {
						x : Math.abs(delta.x),
						y : Math.abs(delta.y)
					}
					console.log(abs);
					if(abs.y > abs.x && delta.y >0){
						//下滑
						pageContain.scrollTop -= delta.y+80;
					}else if(abs.y > abs.x && delta.y <0)
					{	//上滑
						pageContain.scrollTop -= delta.y-80;
					}
					
					pageContain.removeEventListener('touchmove', touchEvent.move, false);
					pageContain.removeEventListener('touchend', touchEvent.end, false);
				}
			}
			pageContain.addEventListener('touchstart', touchEvent.start, false);
		},
		transfer:function(obj,y){
			$(obj).css({
				"transform": 'translateY('+y+'px)',
				"-ms-transform": 'translateY('+y+'px)',
				"-moz-transform": 'translateY('+y+'px)',
				"-webkit-transform": 'translateY('+y+'px)',
				"-o-transform": 'translateY('+y+'px)',
				"transition-duration": "500ms",
                "-moz-transition-duration":  "500ms", /* Firefox 4 */
 				"-webkit-transition-duration":" 500ms", /* Safari 和 Chrome */
 				"-o-transition-duration":  "500ms" /* Opera */
			});
		},
		saveData:function(){
			var that = this;
			var data  = that.data;
			console.log("previewData",data);
			var param ={
					key:data.calendarData.key,
				 	storage_id:  data.inOutData.hub,
                    storage_name: encodeURI(data.inOutData.hub_name),
                    storage_linkman: encodeURI(data.inOutData.send_contact),
                    storage_linkman_tel: data.inOutData.send_phone,
                    carrier_id : "AAFA",//data.carrierData.carrier_id,
                    carrier_name: encodeURI(data.carrierData.carrier),
                    appointment_time: data.calendarData.displayTime,
                    appointment_type: data.inOutData.type,
                    carrier_linkman: encodeURI(data.carrierData.carrier_linkman),
                    carrier_linkman_tel: data.carrierData.carrier_linkman_tel,
                    //etd: data.carrierData.etd,
                    //eta: data.carrierData.eta,
                    licenseplate: encodeURI(data.carrierData.licenseplate),
                    driver_license: encodeURI(data.carrierData.driver_license),
                    driver_name: encodeURI(data.carrierData.driver_name),
                    invoices: data.loadData.loads.concat(data.loadData.orders),
                    status: 'Open'
			};
			console.log(param);
			
			var d = Dialog({ id:'1'}).showModal();
			$.ajax({
				url: config.addAppointment.url,
				type: 'POST',
				dataType: 'json',
				contentType:"application/json; charset=UTF-8",
				data: JSON.stringify(param)
			})
			.done(function(saveResult) {
				d.close().remove();
				if(saveResult.result=="0")
				{
				
					that.eventAcrossView.trigger('showResultView',{appointment_time:data.calendarData.displayTime});
				}
				else
				{
					MsgHelper.showMessage('Save appointment error !','error');
				}
			})
			.fail(function() {
				d.close().remove();
				console.log("error");
			});
		},
		prevStep:function(){
			var that = this;
			this.eventAcrossView.trigger('showCarrier',{display:"show",callback:function(){
				//$(that.el).html('');
			}});
		},
	
	});
});