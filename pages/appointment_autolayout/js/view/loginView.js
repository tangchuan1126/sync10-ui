define(["jquery","backbone","../../config","../../templates/templates.amd","artDialog/dialog","../../MsgHelper"],
function($,Backbone,config,templates,Dialog,MsgHelper) {
	return Backbone.View.extend({
		el:"#login",
		template: templates.login,
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		events:{
			"click #loginBtn": "login"
		},
		render: function(licence_url) {
			var that = this;
			this.$el.html(this.template({licence:licence_url}));
			return this;
		},
		login:function(e)
    {
        	var that = this;
        	//that.nextStep();
       
           var userName = $("#userName").val();
            var password = $("#password").val();
            var licence = $("#licence").val(); 
          	
            if($.trim(userName)==""){
            	this.alertMessage($("#userName")[0],'Please input account !');
            	return;
            }
            if($.trim(password)==""){
            	this.alertMessage($("#password")[0],'Please input password !');
            	return;
            }
            if($.trim(licence)==""){
            	this.alertMessage($("#licence")[0],'Please input verification code !');
            	return;
            }
           
            var d = Dialog({id:'1'}).showModal();
            $.ajax({
              url: config.login.url,
              type: 'POST',
              dataType: 'JSON',
              data: {account: userName,pwd:password,licence:1,loginType:1},
            })
            .done(function(data) {
                d.close().remove();
                if(data && data.success==1)
                {
                   that.getAdmin(function(){
                       that.nextStep();
                   });
                }
                else
                {
                  	 that.alertMessage($("#userName")[0],'Account or password error !');
                } 
            }).fail(function() {
              d.close().remove();
              MsgHelper.showMessage('System error !','error');
              console.log("login error");
            });
            that.events.admin = {"name":"test"}; 
    },
    alertMessage:function(obj,msg)
    {
            obj.focus();
            var d = Dialog({ content: '<i class="valiIcon fa fa-info-circle"></i>'+msg,align: 'top',id:'1',skin:'validate'}).show(obj);
			         setTimeout(function () {
	   		         d.close().remove();
			         }, 1500);
    },
    loginout:function()
    {
    },
    getAdmin:function(success)
		{
			var that = this;
			$.ajax({url: config.session.url,type: 'GET',dataType: 'json',}).done(function(data)
			{
				//存储admin信息
				that.eventAcrossView.admin = data;
        //需要保证此处 SCAC 存在
        that.eventAcrossView.admin.SCAC="AACR";
				console.log("admin",that.eventAcrossView.admin);
				success();
			}).fail(function() {
				console.log("get admin error");
			});
		},
		distroy:function(callback)
		{
				$(this.el).html('');
				if(callback)callback();
		},
		nextStep:function()
		{
			var that = this;
			that.eventAcrossView.trigger('showInOut',{display:"render"});
		}
	});
});