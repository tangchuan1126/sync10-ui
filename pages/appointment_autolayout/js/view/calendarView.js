define(["jquery","backbone","../../config","../../templates/templates.amd","artDialog/dialog","../../MsgHelper","moment","datetimepicker-plus",],
function($,Backbone,config,templates,Dialog,MsgHelper) {
	return Backbone.View.extend({
		el:"#calendar",
		template: templates.calendar,
		inOutData:null,
		date:"",
		time:"",
		key:"",
		//model:new (Backbone.Model.extend({url:""}))(),
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		render: function(inOutData) {
			this.inOutData = inOutData;
			this.$el.html(this.template());
			this.initCalendar();
			this.initScroll();
			return this;
		},
		events:{
			"click #time .timeEnable":"timeEvent",
			"click #calendarContainer .return":"showDate",
			"click #calendarContainer .nextStep":"nextStep",
			"click #calendarContainer .prevStep":"prevStep"
			
		},
		initCalendar:function()
		{
			var that = this;
		    var calendar =  $('#datetimepicker12').Acars_datetimepicker({
		           inline: true,
		           icons:{
		           previous: 'fa fa-chevron-circle-left',
		           next: 'fa fa-chevron-circle-right'
		           },
		           toolbar:false,
		           format: 'YYYY-MM-DD',
		           tdcellclick:function(dates){
		           	that.date = dates;
		           //	$("#selectedDate").html(moment(dates).format("MM/DD/YYYY"));
		            that.initTime(dates);
		           },
		           Initialization:function(setbuttondom){
		          
		            setbuttondom.show();           
		            setbuttondom.click(function(){
		              console.log(2233444);
		            });

		           }
		    });
		   // that.initTimeEvent();
		   if(this.eventAcrossView.browser.iebrowsev)
		   {
		   		//IE下 如果宽度不够 增加滚动条
		   	 $("#datetimeContainer").css('overflow-x','auto');
		   }
		},
		showTime:function(){
			var dateWidth = $("#datetimeContainer .date").width(),
			timeWidth = $("#datetimeContainer .time").width(),
			allWidth = $("#datetimeContainer").width(),
			padding = 20,
			hidenWidth =  allWidth - (dateWidth + timeWidth + padding);
			if(this.scroll)
			{
			 	$("#datetimeContainer .OnedayTimebox").css({
					"transition-duration": '1000ms'
				});
				this.scroll._translate(hidenWidth,0);
			}
		},
		initTime:function(dates)
		{
			var that = this;
			$("#calendarContainer .time").show();

			//-----获取可预约的小时，并且初始化时间table
			var inOutData  = this.inOutData;
			$("#time td").removeClass('timeEnable').removeClass('timeBorder');
			var param = {"storage_id":inOutData.hub , "date":dates};
			var d = Dialog({ id:'1'}).showModal();
			$.ajax({
				url: config.appiontmentCalendarTimeUrl.url+"?time="+ new Date().getTime(),
				type: 'GET',
				dataType: 'JSON',
				data: param,
			})
			.done(function(result) {
				var flag = false;
				$.each(result.DATA, function(index, obj) {
					 if(inOutData.type=="inbound")
					 {
					 	 if(obj.CNTIN == 1){
					 	 	$("#time"+obj.ID).addClass('timeEnable');
					 	 	flag = true;
					 	 }
					 }
					 else
					 {
					 	if(obj.CNTOUT == 1){
					 	 	$("#time"+obj.ID).addClass('timeEnable');
					 	 	flag = true;
					 	}
					 }
				});
				if(flag)
				{
					that.showTime();
				}else
				{
					MsgHelper.showMessage('No time available!','alert');
				}
				
			}).fail(function(e) {
				console.log(e);
				MsgHelper.showMessage('Request error','error');
			}).always(function(){
				d.close().remove();
			});
		},
		initScroll:function()
		{	
			 if (!this.eventAcrossView.browser.iebrowsev){
			 		this.scroll = new IScroll('#datetimeContainer', {
	                scrollX: true,
	                scrollY: false,
	                hScrollbar: true,
	                mouseWheel: true,
	                click:this.eventAcrossView.browser.isMobile
	            });
			 }
			  
		},
		refresh:function()
		{
			if(this.scroll){
				this.scroll.refresh();
			}
		},
		timeEvent:function(e){
			$("#time td").removeClass('timeBorder');
			$(e.target).addClass('timeBorder');
			var that = this,
				time = $(e.target).text(),
				hour  = $(e.target).attr("data-id");
			that.time = time;
			console.log(moment(this.date).format("MM/DD/YYYY")+" "+ this.time);
			console.log("datetime",that.date+" "+ time);
			
			if(that.key!="")
			{
				//先取消之前的key，再取得新的key
				this.cancelKey({
					"storageId": that.inOutData.hub,
					"date": that.date,
					"hour": hour,
					"type": that.inOutData.type,
					"key": that.key
				},function(){
					that.getKey({
						"storageId": that.inOutData.hub,
						"date": that.date,
						"hour": hour,
						"type": that.inOutData.type
					});
				});
			}
			else
			{
				that.getKey({
					"storageId": that.inOutData.hub,
					"date": that.date,
					"hour": hour,
					"type": that.inOutData.type
				});
			}

		},
		getKey:function(param){
			var that = this;
			var d = Dialog({ id:'1'}).showModal();
			$.ajax({url: config.makeAppointment.url,type: 'POST',dataType: 'json',data: param}).done(function(data){
				if(data.status=="200")
				{
					that.key = data.key;
					that.nextStep();
				}
				else
				{
					MsgHelper.showMessage("Get data failed ,please try again later",'alert');
				}
			}).fail(function(error) {
				MsgHelper.showMessage('All the appointment have been taken !','alert');
				that.initTime(that.date);
			}).always(function(){
				d.close().remove();
			});  
		},
		cancelKey:function(param,callback){
			var that = this;
			that.key="";
			var d = Dialog({ id:'1'}).showModal();
			$.ajax({url: config.cancelKey.url,type: 'POST',dataType: 'json',data: param}).done(function(data){
				if(callback)
				{
					callback();
				}
			}).fail(function(error) {
				MsgHelper.showMessage(error.responseText,'alert');
			}).always(function(){
				d.close().remove();
			});  
		},
		nextStep:function()
		{
			var that = this;
			that.eventAcrossView.trigger('showCarrier','loadno...');
		},
		prevStep:function(){
			var that = this;
			//上一步，销毁本页面,并缓存对象
			that.eventAcrossView.trigger('showLoad',{display:"show",callback:function(){
				//$(that.el).html('');
			}});
			
		},
		getData:function()
		{
			return{
				"datetime":this.date+" "+ this.time,
				"displayTime":moment(this.date).format("MM/DD/YYYY")+" "+ this.time,
				"key":this.key
			} 
		},
		distroyData:function()
		{
			this.data="";
			this.time="";
			this.key="";
		}		
	});
});