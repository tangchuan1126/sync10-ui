define(["jquery","backbone","../../config","../../templates/templates.amd","../../MsgHelper","immybox","immybox"],
function($,Backbone,config,templates,MsgHelper) {
	return Backbone.View.extend({
		el:"#carrier",
		template: templates.carrier,
		//model:new (Backbone.Model.extend({url:""}))(),
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		events:{
			//"click #loadContainer .nextStep": "nextStep"
		},
		render: function(calendarData) {
			//var that = this;
			this.$el.html(this.template());
         	this.initEvents();
         	this.initApptime(calendarData);
         	this.initCarrier();
			return this;
		},
		initApptime:function(calendarData)
		{
			$("#appointmentTime").val(calendarData.displayTime);
			
		},
		initCarrier:function()
		{
			$("#appt_carrier").val(this.eventAcrossView.admin.SCAC);
			/**
			var that = this;
			if(that.carriers)
			{
				$("#appt_carrier").immybox({
			 		 choices: that.carriers
				});
			}
			else
			{
                $.getJSON(config.carrier.url, function(json) {
                    var carriers = [];
                    carriers.push({
                            "VALUE": "-1",
                            "TEXT": "Select SCAC"
                        });
                    for (var i = 0; i < json.length; i++) {
                        carriers.push({
                            "VALUE": json[i].value,
                            "TEXT": json[i].text
                        });
                    }
                    that.carriers = carriers;
					that.initCarrier();
				});
			}  **/
		},
		initEvents:function()
		{
			var that = this;
			$("#carrierContainer .nextStep").click(function(event) {
         		that.nextStep();
         	}); 
         	$("#carrierContainer .prevStep").click(function(event) {
         		that.prevStep();
         	}); 
		},
		valiCarrier:function()
		{
			var data = this.getData();
			$(this.el).find('.has-error').removeClass('has-error');
			if(!data.carrier || data.carrier=="")
			{
				$("#appt_carrier").parent().addClass('has-error');
				return;
			}
		},
		valiResult:function(){
			var result  = $(this.el).find('.has-error').length < 1;
			if(!result){
				 MsgHelper.showMessage('Please modify the errors','error');
			}
			return result;
		},
		nextStep:function()
		{
			var that = this;
			this.valiCarrier();
			if(this.valiResult())
			{
				that.eventAcrossView.trigger('showPreviewView',{display:"render"});
			}
		},
		prevStep:function(){
			var that = this;
			//上一步，销毁本页面,并缓存对象
			that.eventAcrossView.trigger('showCalendar',{display:"show",callback:function(){
				//$(that.el).html('');
			}});
		},
		getData:function()
		{
			return {
				"carrier":$("#appt_carrier").val(),
				"carrier_linkman":$("#contact").val(),
				"carrier_linkman_tel":$("#phone").val(),
				"licenseplate":$("#licensePlate").val(),
				"driver_license":$("#driverLicense").val(),
				"driver_name":$("#driverName").val()
			};
		}
	});
});