define(["jquery","backbone","../../config","../../templates/templates.amd","iscroll"],
function($,Backbone,config,templates,iscroll) {
	return Backbone.View.extend({
		el:"#result",
		template: templates.result,
		initialize:function(options) {
			this.eventAcrossView = options.eventAcrossView;
		},
		events:{
			"click #continueAppointment": "continueAppointment",
			"click #logout": "logout",
		},
		render: function(data) {
			var that = this;
			var renderDate = this.getDelWindowTime(data.appointment_time);
			console.log(renderDate);
			this.$el.html(this.template(renderDate));
			return this;
		},
		getDelWindowTime:function(appointment_time){
			var m = moment(appointment_time,"MM/DD/YYYY HH:mm");
			var start = m.add(-15,'minutes').format('MM/DD/YYYY HH:mm');
			var end = m.add(30,'minutes').format('MM/DD/YYYY HH:mm');	
			return {
				startTime : start,
				endTime:end
			}
		},
		continueAppointment:function(){
			this.eventAcrossView.trigger('distroy');
			this.eventAcrossView.trigger('showInOut',{display:'render'});
		},
		logout:function()
		{
			this.eventAcrossView.trigger('logout');
		}
	
	});
});