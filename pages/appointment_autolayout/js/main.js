
requirejs.config({
    paths: {
        "require_css": "/Sync10-ui/lib/require-css/css.min",
        "jquery": "../lib/jquery/1.8.3/jquery.min",
        "fullPage": "../lib/fullpage/fullPage.min",
        "moment": "/Sync10-ui/bower_components/moment/min/moment.min",
        "iscroll": "../lib/iscroll/iscroll",
        "smalot-bootstrap-datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "datetimepicker-plus": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "Font-Awesome": "/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min",
        "bootstrap-css": "/Sync10-ui/bower_components/Bootstrap/dist/css/bootstrap.min",
        "datetimepicker-css": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "smalot-datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "datetimepicker-plus-css": "/Sync10-ui/pages/appointment_autolayout/css/bootstrap-datetimepicker-plus",
        "default-css": "/Sync10-ui/pages/appointment_autolayout/css/default",
        "mCustomScrollbar": "/Sync10-ui/lib/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min",
        "mCustomScrollbar-css":"/Sync10-ui/lib/mCustomScrollbar/css/jquery.mCustomScrollbar",

        "backbone":"/Sync10-ui/bower_components/backbone/backbone.min",
        "templates":"templates/templates.amd",
        "handlebars.runtime":"/Sync10-ui/bower_components/handlebars/handlebars.runtime.amd.min",
        "handlebars":"/Sync10-ui/bower_components/handlebars/handlebars.amd.min",
        "underscore":"/Sync10-ui/bower_components/underscore/underscore.min",
        "artDialog":"/Sync10-ui/lib/artDialog/src",
        "immybox":"/Sync10-ui/lib/immybox-master/jquery.immybox"
    },
    shim: {        
        // "abc2": {
        //     deps: ["require_css!abc2_css"]
        // }
    }
});


require(["jquery","fullPage", "../js/view/loginView", "../config","require_css!bootstrap-css"],
function(jq_,_fullPage, LoginView,config)
{
    var options = {
      id: 'pageContain',
      slideTime: 800,
      effect: {
        transform: {
          translate: 'X'
        },
        opacity: [1, 1]
      },
      //mode: 'wheel, touch',
      easing: 'ease',
      //callback: function(index, thisPage){}
    };
    var runPage = new FullPage(options);
        
    var cssall = [
            "require_css!Font-Awesome",
            "require_css!default-css",
            "require_css!datetimepicker-css",
            "require_css!smalot-datetimepicker-css",
            "require_css!datetimepicker-plus-css",
            "require_css!mCustomScrollbar-css",
            "require_css!/Sync10-ui/lib/immybox-master/immybox.css"
        ];
    var browserv=navigator.userAgent;
    var iebrowsev =/MSIE/.test(browserv);
    if(iebrowsev) cssall=[];  
    require(cssall, function(){
         
          var data = {admin:null};
          var ua = window.navigator.userAgent;
          var browser = {
               cssCore : function(testCss) {
                switch (true) {
                  case testCss.webkitTransition === '':
                  return 'webkit'; break;
                  case testCss.MozTransition === '':
                  return 'Moz'; break;
                  case testCss.msTransition === '':
                  return 'ms'; break;
                  case testCss.OTransition === '':
                  return 'O'; break;
                  default:
                  return '';
                }
              }(document.createElement('hehemeiyong').style),
              isMobile : /Android/i.test(ua) || /Mobile/i.test(ua) || /X11;/i.test(ua),
              iebrowsev:iebrowsev
          },
          eventAcrossView =  $.extend(true, {"browser":browser},data, Backbone.Events),
          loginView = new LoginView({"eventAcrossView":eventAcrossView});
        require(["moment",
          "datetimepicker-plus",
          "smalot-bootstrap-datetimepicker",
          "artDialog/dialog",
          "../js/view/inOutView",
          "../js/view/loadView",
          "../js/view/calendarView",
          "../js/view/carrierView",
          "../js/view/previewView",
          "../js/view/resultView",
          "mCustomScrollbar"
        ],function(moment,
          Acars_datetimepicker,
          smalot_datetimepicker,
          Dialog,
          InOutView,
          LoadView,
          CalendarView,
          CarrierView,
          PreviewView,
          ResultView)
        {
           var d = null;
           $(document).ajaxStart(function(){
              //d = Dialog({ id:'1'}).showModal();
           });
           $(document).ajaxStop(function(){
              //d.close().remove();
           });
            $(document).ajaxError(function(e,xhr){
                if(xhr.readyState == 0)
                {
                  alert('Please make sure that your device has to connect to the Internet!');
                }
                else if(xhr.readyState  < 4){
                  alert('Your network is not force!');
                }
            });
            var inOutView = null;
            var loadView = null;
            var calendarView = null;
            var carrierView = null;
            var previewView = null;
            var resultView  = null;
            eventAcrossView.on('showInOut',  function(data) {
                if(inOutView == null)
                {
                     inOutView = new InOutView({"eventAcrossView":eventAcrossView}).render();
                } 
                else
                {    
                    if(data.display=='show')
                    {

                    }
                    else
                    {
                       //通过对象重新渲染
                       inOutView.render(); 
                    }
                }
                runPage.go(2,data.callback);
            });

            eventAcrossView.on('showLoad',  function(data) {
                var inOutData = inOutView.getData();
                if(loadView == null)
                {
                     loadView = new LoadView({"eventAcrossView":eventAcrossView}).render(inOutData);
                } 
                else
                {    
                    if(data.display=='show')
                    {
                      
                    }
                    else
                    {
                       //通过对象重新渲染
                      loadView.render(inOutData); 
                    }
                  
                }
                runPage.go(3,data.callback);
            });

            eventAcrossView.on('showCalendar',  function(data) {
                var inOutData  = inOutView.getData();
                if(calendarView == null)
                {
                     calendarView = new CalendarView({"eventAcrossView":eventAcrossView}).render(inOutData);
                }
                else
                {
                   if(data.display=='show')
                    {
                      
                    }else
                    {
                       calendarView.render(inOutData);
                    }
                }
                if(data.callback)
                {
                  //上一步
                   runPage.go(4,data.callback);
                }
                else
                {
                  //下一步，滚动后刷新表头
                  runPage.go(4,function(){
                    calendarView.refresh();
                  });
                }
            });

            eventAcrossView.on('showCarrier',  function(data) {
                var calendarData = calendarView.getData();
                if(carrierView == null)
                {
                    carrierView = new CarrierView({"eventAcrossView":eventAcrossView}).render(calendarData);
                }
                else
                {
                    if(data.display=='show')
                    {
                      
                    }else{
                      carrierView.render(calendarData);
                    }
                }
                 runPage.go(5,data.callback);
            });

            eventAcrossView.on('showPreviewView',function(){
                var inOutData = inOutView.getAllData();
                var loadData = loadView.getData();
                var calendarData = calendarView.getData();
                var carrierData = carrierView.getData();

                var previewData =   {
                      "inOutData":inOutData,
                      "loadData":loadData,
                      "calendarData":calendarData,
                      "carrierData":carrierData
                    }
                if(previewView == null)
                    previewView = new PreviewView({"eventAcrossView":eventAcrossView}).render(previewData);
                else
                    previewView.render(previewData);
                    runPage.go(6,function(){
                        if(browser.isMobile)
                        { //移动端，加触屏效果
                          previewView.ininScroll();
                        }
                    });
            });

            eventAcrossView.on('showResultView',function(data){
                if(resultView == null)
                    resultView = new ResultView({"eventAcrossView":eventAcrossView}).render(data);
                else
                    resultView.render(data);
                    runPage.go(7);
            });

            eventAcrossView.on('distroy',function(){
              $("inout").html('');
              $("load").html('');
              $("carrier").html('');
              $("calendar").html('');
              calendarView.distroyData();
            });

            eventAcrossView.on('appointmentSuccess',function(){
               calendarView.distroyData();
            });
          
            eventAcrossView.on('logout',function(){
                  $.ajax({
                  url: config.logout.url,
                  type: 'POST',
                  dataType: 'html',
                  data: {backurl: config.appointment.url,type:'admin'},
                }).always(function(){
                    window.location.reload();
                });
            });
            
            $.ajax({
              url: config.session.url,
              type: 'GET',
              dataType: 'json',
            })
            .done(function(admin) 
            {
                // has session ，escape login tab
                eventAcrossView.admin = admin;
                eventAcrossView.admin.SCAC="AACR";
                eventAcrossView.trigger('showInOut',{});
            }).fail(function(e) {
                //需要登陆
                loginView.render(config.licence.url);
                runPage.next();
            });

            window.onbeforeunload=function()
            {
               console.log(1222);
            }
             
        });
       

     });
});
