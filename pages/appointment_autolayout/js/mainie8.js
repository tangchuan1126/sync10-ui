requirejs.config({
    paths: {
        "oso.bower": "/Sync10-ui/bower_components",
        "oso.lib": "/Sync10-ui/lib",
        "require_css": "/Sync10-ui/lib/require-css/css.min",
        "jquery1.9": "/Sync10-ui/lib/appointment_autolayout/js/jquery/1.8.3/jquery.min",
        "jquery": "/Sync10-ui/lib/appointment_autolayout/js/jquery/1.8.3/jquery.min",
        "jqueryui1.9.1": "/Sync10-ui/lib/appointment_autolayout/js/jqueryui/1.9.1/jquery-ui.min",
        "fullPage": "/Sync10-ui/lib/appointment_autolayout/js/jquery.fullPage",
        "select2": "/Sync10-ui/lib/appointment_autolayout/js/select2.full",
        "moment": "/Sync10-ui/bower_components/moment/min/moment.min",
        "iscroll": "/Sync10-ui/lib/appointment_autolayout/js/iscroll",
        "smalot-bootstrap-datetimepicker": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker",
        "datetimepicker-plus": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "Font-Awesome": "/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min",
        "bootstrap-css": "/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min",
        "select2-css": "/Sync10-ui/lib/appointment_autolayout/css/select2.min",
        "datetimepicker-css": "/Sync10-ui/lib/bootstrap-datetimepicker-plus/bootstrap-datetimepicker",
        "smalot-datetimepicker-css": "/Sync10-ui/lib/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min",
        "datetimepicker-plus-css": "/Sync10-ui/lib/appointment_autolayout/css/bootstrap-datetimepicker-plus",
        "fullPage-css": "/Sync10-ui/lib/appointment_autolayout/css/jquery.fullPage",
        "default-css": "/Sync10-ui/lib/appointment_autolayout/css/defaultie8",
        "respondjs":"/Sync10-ui/lib/appointment_autolayout/js/respond.min"
    },
    shim: {
        // "abc2": {
        //     deps: ["require_css!abc2_css"]
        // }
    }
});




require([
    "jquery",
    "fullPage",
    "select2",
    "moment",
    "datetimepicker-plus",
    "smalot-bootstrap-datetimepicker",
    "iscroll",
    "require_css!fullPage-css"
], function(jq_, _fullPage, select2, moment, Acars_datetimepicker, smalot_datetimepicker, iscroll) {

    $(function() {

        var apphome = $('#fullpage');
        //apphome.find('.section').removeClass('hide');
        // apphome.fullpage({
        //     loopHorizontal: false,
        //     controlArrows: false,
        //     //sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
        //     anchors:['Loading', 'Log in', '3rdPage', '4thpage', 'lastPage']
        // });


        var cssall = [
            "require_css!Font-Awesome",
            "require_css!bootstrap-css",
            "require_css!default-css",
            "require_css!select2-css",
            "require_css!datetimepicker-css",
            "require_css!smalot-datetimepicker-css",
            "require_css!datetimepicker-plus-css",
            "respondjs"
        ]

        require(cssall, function() {

            //进入login
            // $.fn.fullpage.moveSectionDown();

            // var header=$("#header");
            // //登录
            // $("button:submit").click(function(e) {
            //     e.preventDefault();                             
            //     header.removeClass('hide');
            //     $("#Step1").find("div").eq(0).removeClass('fp-tableCell');
            //     $.fn.fullpage.moveSlideRight();

            // });


            // //左右导航按钮
            // var Step5scrll = true,
            //     Step3scrll = true,
            //     Step2scrll = true;
            // $(".fa-chevron-right,.fa-chevron-left").click(function(e) {
            //     var this_ = $(this);
            //     var is_right = this_.hasClass('fa-chevron-right');
            //     var _page = this_.parents(".slide").eq(0);
            //     var _pageNo = _page.attr("id");

            //    //右边
            //     if (is_right) {

            //       var next_slide =_page.next(".slide").eq(0);  
            //       if(next_slide.length > 0){
            //        $.fn.fullpage.moveSlideRight();                        
            //       }


            //     } else {
            //   //左边

            //    var prev_slide =_page.prev(".slide").eq(0);  
            //       if(prev_slide.length > 0){
            //        $.fn.fullpage.moveSlideLeft();                        
            //       }



            //     }

            // });







        });




    });



});
