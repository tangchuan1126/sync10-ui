define({
	login:{
		url:'/appt/public/login'
	},
	logout:{
		url:"/appt/logout"
	},
	licence:{
		url:"/appt/public/loginLicence"
	},
	cancelKey:{
		url: "/appt/cancelAppointment"
	},
	appointment:{
		 url: "/Sync10-ui/pages/appointment_autolayout"
	},
	addAppointment:{
		 url: "/appt/saveAppointment"
	},
	LRList:{
		//url:"/Sync10/_load/getAppLRList"
		url:"/appt/getAppLRList"
	},
	makeAppointment:{
		//url: "/Sync10/_load/makeAppointment"
		url: "/appt/makeAppointment"
	},
	getHubs: {   //仓库地址json
        //url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
        url:"/appt/getStorage"
    },
	appiontmentCalendarTimeUrl: {    
        //url: "/Sync10/_load/sumWorkEnable"
        url: "/appt/sumWorkByDate"
    },
	session: {
		url: "/appt/session"
	}
});