(function(){
    var configObj = {
    	//产品code
        productCodeUrl:{
            addUrl:"/Sync10/basicdata/productCodes/insertProductCode",
            modUrl:"/Sync10/basicdata/productCodes/updateProductCode",
            delUrl:"/Sync10/basicdata/productCodes/deleteProductCode",
            checkUrl:"/Sync10/basicdata/productCodes/checkProductCode",
            getAllUrl:"/Sync10/basicdata/productCodes/productCodeList",
            getAllRetailer:"/Sync10/basicdata/productCodes/getRetailerList"
        }
        
    };
	define(configObj);
	
}).call(this);
