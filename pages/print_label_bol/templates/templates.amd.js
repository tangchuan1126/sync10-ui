define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['bolList'] = template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<tr>\n    <td>"
    + alias3(((helper = (helper = helpers.template_name || (depth0 != null ? depth0.template_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"template_name","hash":{},"data":data}) : helper)))
    + "</td>\n    <td>"
    + alias3(((helper = (helper = helpers.printer || (depth0 != null ? depth0.printer : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"printer","hash":{},"data":data}) : helper)))
    + "</td>\n    <td>"
    + alias3(((helper = (helper = helpers.paper || (depth0 != null ? depth0.paper : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"paper","hash":{},"data":data}) : helper)))
    + "</td>\n    <td>"
    + alias3(((helper = (helper = helpers.print_range_width || (depth0 != null ? depth0.print_range_width : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"print_range_width","hash":{},"data":data}) : helper)))
    + "</td>\n    <td>"
    + alias3(((helper = (helper = helpers.print_range_height || (depth0 != null ? depth0.print_range_height : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"print_range_height","hash":{},"data":data}) : helper)))
    + "</td>\n    <td>"
    + alias3(((helper = (helper = helpers.template_path || (depth0 != null ? depth0.template_path : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"template_path","hash":{},"data":data}) : helper)))
    + "</td>\n    <td>\n    	<button type=\"button\" class=\"btn btn-default J-btn-edit\" data-id=\""
    + alias3(((helper = (helper = helpers.lable_template_id || (depth0 != null ? depth0.lable_template_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"lable_template_id","hash":{},"data":data}) : helper)))
    + "\">\n        	<i class=\"glyphicon glyphicon-pencil\"> Edit</i>\n        </button>\n        <button type=\"button\" data-id=\""
    + alias3(((helper = (helper = helpers.lable_template_id || (depth0 != null ? depth0.lable_template_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"lable_template_id","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.template_name || (depth0 != null ? depth0.template_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"template_name","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-default J-btn-del\">\n        	<i class=\"glyphicon glyphicon-trash\">  Del</i>\n        </button>\n        <button type=\"button\" data-src=\""
    + alias3(((helper = (helper = helpers.img_path || (depth0 != null ? depth0.img_path : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"img_path","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-default J-btn-preview\">\n        	<i class=\"glyphicon glyphicon-eye-open\">  Preview</i>\n       </button>\n    </td>\n</tr> \n";
},"3":function(depth0,helpers,partials,data) {
    return "<tr class=\"no-data\" style=\"height: 70px;\">\n	<td class=\"no-data\" colspan=\"7\">NO Data</td>\n</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['bolTop'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "";
},"useData":true});
templates['main'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"bol\">\n    <div role=\"tabpanel\" class=\"panel panel-default\">\n        <div class=\"panel-heading\"><span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\">&nbsp;BOL Maintain</span></div>\n        <div class=\"panel-body\">\n            <!-- Tab panes -->\n            <div class=\"tab-content\">\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"common\">\n                    <div >\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_task\">Add Template</button>\n                    </div>\n                </div>\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\n                    <!-- todo -->\n                </div>\n            </div>\n        </div>\n        <div class=\"modal-container\">\n            <table class=\"table table-hover table-condensed\" id=\"bol_table\">\n                <thead>\n                <tr>\n                    <th>Name</th>\n                    <th>Printer</th>\n                    <th>Paper</th>\n                    <th>Paper Width</th>\n                    <th>Paper Height</th>\n                    <th>File</th>\n                    <th>&nbsp;</th>\n                </tr>\n                </thead>\n                <tbody>\n                </tbody>\n            </table>\n            <div class=\"modal\"><div class=\"loading\"></div></div>\n        </div>\n        <!-- <ul id=\"pagebox_task\" class='clearfix pagebox mb20 mt20'></ul> -->\n    </div>\n</div>";
},"useData":true});
return templates;
});