(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['bolList'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<tr>\r\n    <td>"
    + escapeExpression(((helper = (helper = helpers.template_name || (depth0 != null ? depth0.template_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"template_name","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td>"
    + escapeExpression(((helper = (helper = helpers.printer || (depth0 != null ? depth0.printer : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"printer","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td>"
    + escapeExpression(((helper = (helper = helpers.paper || (depth0 != null ? depth0.paper : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"paper","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td>"
    + escapeExpression(((helper = (helper = helpers.print_range_width || (depth0 != null ? depth0.print_range_width : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"print_range_width","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td>"
    + escapeExpression(((helper = (helper = helpers.print_range_height || (depth0 != null ? depth0.print_range_height : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"print_range_height","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td>"
    + escapeExpression(((helper = (helper = helpers.template_path || (depth0 != null ? depth0.template_path : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"template_path","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td>\r\n    	<button type=\"button\" class=\"btn btn-default J-btn-edit\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.lable_template_id || (depth0 != null ? depth0.lable_template_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lable_template_id","hash":{},"data":data}) : helper)))
    + "\">\r\n        	<i class=\"glyphicon glyphicon-pencil\"> Edit</i>\r\n        </button>\r\n        <button type=\"button\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.lable_template_id || (depth0 != null ? depth0.lable_template_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lable_template_id","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + escapeExpression(((helper = (helper = helpers.template_name || (depth0 != null ? depth0.template_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"template_name","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-default J-btn-del\">\r\n        	<i class=\"glyphicon glyphicon-trash\">  Del</i>\r\n        </button>\r\n        <button type=\"button\" data-src=\""
    + escapeExpression(((helper = (helper = helpers.img_path || (depth0 != null ? depth0.img_path : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"img_path","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-default J-btn-preview\">\r\n        	<i class=\"glyphicon glyphicon-eye-open\">  Preview</i>\r\n       </button>\r\n    </td>\r\n</tr> \r\n";
},"3":function(depth0,helpers,partials,data) {
  return "<tr class=\"no-data\" style=\"height: 70px;\">\r\n	<td class=\"no-data\" colspan=\"7\">NO Data</td>\r\n</tr>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['bolTop'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
templates['main'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"bol\">\r\n    <div role=\"tabpanel\" class=\"panel panel-default\">\r\n        <div class=\"panel-heading\"><span class=\"glyphicon glyphicon-tasks\" aria-hidden=\"true\">&nbsp;BOL Maintain</span></div>\r\n        <div class=\"panel-body\">\r\n            <!-- Tab panes -->\r\n            <div class=\"tab-content\">\r\n                <div role=\"tabpanel\" class=\"tab-pane active panel-body\" id=\"common\">\r\n                    <div >\r\n                        <button class=\"btn btn-default\" type=\"button\" id=\"add_task\">Add Template</button>\r\n                    </div>\r\n                </div>\r\n                <div role=\"tabpanel\" class=\"tab-pane panel-body\" id=\"advanced\">\r\n                    <!-- todo -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-container\">\r\n            <table class=\"table table-hover table-condensed\" id=\"bol_table\">\r\n                <thead>\r\n                <tr>\r\n                    <th>Name</th>\r\n                    <th>Printer</th>\r\n                    <th>Paper</th>\r\n                    <th>Paper Width</th>\r\n                    <th>Paper Height</th>\r\n                    <th>File</th>\r\n                    <th>&nbsp;</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                </tbody>\r\n            </table>\r\n            <div class=\"modal\"><div class=\"loading\"></div></div>\r\n        </div>\r\n        <!-- <ul id=\"pagebox_task\" class='clearfix pagebox mb20 mt20'></ul> -->\r\n    </div>\r\n</div>";
  },"useData":true});
})();