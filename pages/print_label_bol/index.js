"use strict";
require([
     "../../requirejs_config",
], function(){
    require(["jquery","./js/views/mainView", "blockui"], function($, View){
    	$(function(){
    		$.blockUI.defaults = {
    	            css: {
    	                padding:        '8px',
    	                margin:         0,
    	                width:          '170px',
    	                top:            '45%',
    	                left:           '40%',
    	                textAlign:      'center',
    	                color:          '#000',
    	                border:         '3px solid #999999',
    	                backgroundColor:'#ffffff',
    	                '-webkit-border-radius': '10px',
    	                '-moz-border-radius':    '10px',
    	                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
    	                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
    	            },
    	            //设置遮罩层的样式
    	            overlayCSS:  {
    	                backgroundColor:'#000',
    	                opacity:        '0.3'
    	            },
    	            baseZ: 99999,
    	            centerX: true,
    	            centerY: true,
    	            fadeOut:  1000,
    	            showOverlay: true
    	        };

            new View({el:'body'}).render();
    		
    	})
    	
    });

});
