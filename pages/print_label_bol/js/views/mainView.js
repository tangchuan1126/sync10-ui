/**
 * Created by huhy on 2015-3-30
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "./../config",
    "./../../templates/templates.amd.js",
    "./bolListView.js",
    "bootstrap_tab",
    "require_css!bootstrap-css/bootstrap.min.css",
    "require_css!/Sync10-ui/pages/print_label_bol/css/main.css",
    "domready!"
],function($, Backbone, Handlebars, config, templates
		, BolListView
		){

    return Backbone.View.extend({
        template: templates.main,
        initialize: function(option){
            this.setElement(option.el);
        },
        listView:"",
        render:function(){
        	var _self = this;
        	_self.$el.html(_self.template());
        	_self.listView = new BolListView({el: "#bol_table tbody"});
        	
        	_self.listView.render();
            
        } ,
        events: {
        	"click #add_bol" : "addBOL"
        },
        addBOL:function(e){
        	
        }
    });
});