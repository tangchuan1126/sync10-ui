/**
 * Created by huhy on 2015-3-30 10:27:16
 */
 "use strict";
 define([
    "jquery",
    "backbone",
    'handlebars.runtime',
    "handlebars_ext",
    "Paging",
    "./../../templates/templates.amd.js",
    "./../models/collections.js",
//    "./addInvoiceView.js",
    "./../config.js",
//    "../../../artDialog.js",
    "showMessage"
    ],function($, Backbone, Handlebars, Handlebars_ext, Paging, templates, Collection, config){

        return Backbone.View.extend({
            template: templates.bolList,
            collection: Collection,
            initialize: function(option){
                this.loadingModal = option.loadingModal;
                this.setElement(option.el);
            },
            render:function(){

//                this.loadingModal.show();

            this.buildList();       //查询数据，构造列表
            
        },
        buildList: function(){

            var _self = this;

//            _self.collection.reset();
//
//            _self.collection.fetch({
//                success: function(collection){
//                    var data_arr = collection.toJSON();
//                    _self.$el.html(_self.template({'list' : data_arr}));
//
////                    _self.loadingModal.hide();
//
//                },
//                error: function(){
//                	 _self.$el.html(_self.template({'list' : []}));
//                	 showMessage("Error","error");
////                    _self.loadingModal.hide();
//                },
//                cache: false
//            });
            
            _self.$el.html(_self.template({'list' : _self.collection}));
	},
	events: {
	    'click .J-btn-edit' : 'editBol',
	    'click .J-btn-del' : 'delBol',
	    'click .J-btn-preview' : 'previewImg',
	},
	editBol: function(e){
		
	},
	delBol: function(e){
		
	},
	previewImg: function(e){
		  
	},

    });
});