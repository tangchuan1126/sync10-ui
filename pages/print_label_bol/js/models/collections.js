"use strict";
define([
  "./../config.js",
  "./models.js",
  "jquery",
  "backbone"
], function(config, Models) {
	
	/*var BolCollection = Backbone.Collection.extend({
		model:Models,
		url: config.selectUrl + '?key=getAllUserInformation',
        parse: function(response) {
        	console.log(response);
        	return response;
        },
    });*/
	var BolCollection = [
	                     {"template_name":"产品60X30",
	                    	 "printer":"60X30",
	                    	 "paper":"60X30",
	                    	 "print_range_width":"60.0",
	                    	 "print_range_height":"30.0",
	                    	 "template_path":"lable_template/productbarcodeSmall.html",
	                    	 "lable_template_id":"101011",
	                    	 "img_path":""
	                    },
	                    {"template_name":"产品60X30无名称",
	                    	"printer":"60X30",
	                    	"paper":"60X30",
	                    	"print_range_width":"60.0",
	                    	"print_range_height":"30.0",
	                    	"template_path":"lable_template/productbarcodeSmallNoPname.html",
	                    	"lable_template_id":"101012",
	                    	"img_path":""
	                    },
	                    {"template_name":"60X30(韩老师专用)",
	                    	"printer":"60X30",
	                    	"paper":"60X30",
	                    	"print_range_width":"60.0",
	                    	"print_range_height":"30.0",
	                    	"template_path":"lable_template/productbarcodeSmall.html",
	                    	"lable_template_id":"101013",
	                    	"img_path":""
	                    },
	                    {"template_name":"产品80X35",
	                    	"printer":"80X35",
	                    	"paper":"80X35",
	                    	"print_range_width":"79.0",
	                    	"print_range_height":"34.0",
	                    	"template_path":"lable_template/productbarcode.html",
	                    	"lable_template_id":"101014",
	                    	"img_path":""
	                    },
	                    {"template_name":"产品80X35无名称",
	                    	"printer":"80X35",
	                    	"paper":"80X35",
	                    	"print_range_width":"80.0",
	                    	"print_range_height":"35.0",
	                    	"template_path":"lable_template/productbarcodeNoPname.html",
	                    	"lable_template_id":"101016",
	                    	"img_path":""
	                    }
	                     
	                     ];
	
	 return BolCollection;
}); 