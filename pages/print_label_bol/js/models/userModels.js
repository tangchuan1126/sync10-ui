"use strict";
define([
  "../../../config.js",
  "jquery",
  "backbone"
], function(config) {
	
	var UserCollection = Backbone.Collection.extend({
		url: config.selectUrl + '?key=getAllUserInformation',
        parse: function(response) {
//        	console.log(response);
        	return response;
        },
        initialize: function(){
//        	console.log("initialize DeptCollection!!!");
        	this.reset();
        	this.fetch({
                success: function(collection){
//                	var data = collection.toJSON();
                }
            });
        }
    });
	
	 return UserCollection;
}); 