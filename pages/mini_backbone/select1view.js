define(["jquery","backbone","handlebars","select.amd","art_Dialog/dialog-plus"],function($,Backbone,handlebars,tlp,dialog){

 	var selectModel = Backbone.Model.extend();

    var selectCollection =Backbone.Collection.extend();

      var View = Backbone.View.extend({
       el: null,
       selecttemplate: tlp.select1,       
       collection: new selectCollection(),
       initialize: function(_default) {
           if(_default){
           this.$el = this.el = $(_default.renderTo);
           this.collection.url=_default.dataUrl;
           }

       },
       events:{
        "change #selectbox1 select":"selectitmeclick"
       },
       render: function() {

     var _this=this;

      this.collection.fetch({
        data: {id:"1"},
        success: function(jsondata) {

           var _data=jsondata.toJSON();

           var setedata={"selctitme":_data};

        	_this.$el.html(_this.selecttemplate(setedata));
             
       }

     });

  },
  selectitmeclick:function(e){

    var selectobj=$(this.$el).find('select');
    var _txt=selectobj.text();
  	var _val = selectobj.val();
    var evntsname=selectobj.data("select1");

    console.log(evntsname);

    this.trigger(evntsname,_val);
     

  }

});

return View;

});