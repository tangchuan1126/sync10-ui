define(["jquery","backbone","handlebars","select.amd","art_Dialog/dialog-plus","./select1view"],function($,Backbone,handlebars,tlp,dialog,select1view){

 	var selectModel = Backbone.Model.extend();

    var selectCollection =Backbone.Collection.extend();

      var View = Backbone.View.extend({
       el: null,
       selecttemplate: tlp.select,       
       collection: new selectCollection(),
       initialize: function(_default) {
           if(_default){
           this.$el = this.el = $(_default.renderTo);
           this.collection.url=_default.dataUrl;
           }

       },
       events:{
        "change select":"selectitmeclick"
       },
       render: function() {

     var _this=this;

      this.collection.fetch({
        data: {id:"1"},
        success: function(jsondata) {

           var _data=jsondata.toJSON();

           var setedata={"selctitme":_data};

        	_this.$el.html(_this.selecttemplate(setedata));
             
       }

     });

  },
  selectitmeclick:function(e){

    var selectobj=$(this.$el).find('select');
    var _txt=selectobj.text();
  	var _val = selectobj.val();

    var evntsname=selectobj.data("select");

    this.trigger(evntsname,_val);
     this.opendialog(_val);

  },opendialog:function(_val){
     
         var d = new dialog({
          id:"seelctdialogbox",
          content: "<div id='selectbox1'></div>",
            icon: 'question',
            lock: true,
            width: 200,
            height: 70,
            title: '你选中了：'+_val
          });
          d.showModal();
         // var dialogobj=d.get("seelctdialogbox");

      var Viewdialog = new select1view({dataUrl:"./dialog.json",renderTo:"#selectbox1"});  
         Viewdialog.render();
        Viewdialog.on("select1",function(e){
          console.log(e);
        });


  }



});

return View;

});