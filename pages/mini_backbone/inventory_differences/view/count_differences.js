"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/count_difference_model.js",
  "../model/approve_request_model.js",
  "art_Dialog/dialog",
  "bootstrap_tab",

], function( config, $, Backbone, Handlebars, templates, HandleBarsExt, difference_models, approve_model, dialog) {

    var CountDifferencesView =  Backbone.View.extend({
      template: templates.count_differences,
      saa_id:"",
      
      initialize:function(parentView){
          var dis = this;
          dis.parentView = parentView;
      },
      
      render:function(container_id,sac_id,con_id,ps_id,slc_id,pending,saa_id){

        var dis = this;
        dis.collection= new difference_models.DifferenceCollection(sac_id);
        dis.saa_id = saa_id;
        dis.collection.fetch({dataType: "json",async: false});
        var html =dis.template({countDifferences: dis.collection.countDifferenceCollection.toJSON(),
                  systemContainerTree: dis.collection.systemContainerTreeCollection.toJSON(),
                  physicalContainerTree: dis.collection.physicalContainerTreeCollection.toJSON(),
                   ps_id: ps_id, con_id:con_id,slc_id:slc_id,sac_id:sac_id,pending:pending});
        $("#"+container_id).html("");
        $("#"+container_id).html(html);
        
        if($("button[name='approve_btn']")){
          
          $("button[name='approve_btn']").click(function(evt){dis.approve_difference(evt);});
        }
        
     },
     approve_difference:function(evt){
        var dis = this;
        
        var parent = $(evt.target).parent();
        var con_id = parent.find($('input[name="CON_ID"]')).val();
        var ps_id = parent.find($('input[name="PS_ID"]')).val();
        var sac_id = parent.find($('input[name="SAC_ID"]')).val();
        var slc_id = parent.find($('input[name="SLC_ID"]')).val();

        var rows = $("#count_diff_table_body_"+sac_id+" textarea[name='comments']");
        var valid = true;
        var invalidList = [];

        var col = new approve_model.ChildrenCollection();
        $.each(rows,function(i,item){
         
            if($.trim(item.value)==""){
              valid=false;
              invalidList.push(item)
            }else{
              dis.mark_valid($(item));
              var note = $.trim(item.value);
              var sld_id = parseInt($(item).parent().parent().attr('id'));
              var child = new approve_model.ChildrenModel({reason:note,sld_id:sld_id});
              col.add(child);
              
            }
          
        });

        if(!valid){
          $.each(invalidList, function(i,item){
              dis.mark_invalid($(item));
          });
        }
        else{
          var model;
          
          if(col.length > 0){
            model = new approve_model.DifferenceModel({ps_id:parseInt(ps_id),con_id:parseInt(con_id),slc_id:parseInt(slc_id),sac_id:parseInt(sac_id),container_detail_differents:col.toJSON()});
          }else{
            model = new approve_model.DifferenceModel({ps_id:parseInt(ps_id),con_id:parseInt(con_id),slc_id:parseInt(slc_id),sac_id:parseInt(sac_id)});
          }
          
          var loading = new dialog();
          loading.showModal();
          //dis.refresh_list();
          $.ajax({
            type: "PUT",
            url: config.saveApproveDifference,
            data: JSON.stringify(model),
            contentType:"application/json; charset=utf-8",
           // dataType: 'json',
            success: function(){dis.refresh_list(dis.parentView);},
            error:function(xhr, ajaxOptions, thrownError){
                console.log("error",xhr.responseText);
            },
            async:false
            
          });
         loading.close().remove();
        }
        
     },

    refresh_list:function(parent){
      var dis = this;
      parent.parentView.render(dis.saa_id);
    },

    mark_valid:function(item){
      var error_box = item.parent().find($("span[name='validation_msg']"))[0];
      $(error_box).text("");
      $(error_box).removeClass("visible").addClass("hidden");
            
     },
     mark_invalid:function(item){
      var error_box = item.parent().find($("span[name='validation_msg']"))[0];
      $(error_box).text("please enter comments");
      $(error_box).removeClass("hidden").addClass("visible");
      item.focus();
     },

  });
return CountDifferencesView;
}); 

