"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/area_model.js",
  "immybox",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models) {
    
    return Backbone.View.extend({
      el:"#area_list_box_container",
      template:templates.area_list_box,
      
      render:function(){

        var dis = this;
        
          dis.collection.fetch({dataType: "json",async: false});
          
          dis.$el.html(dis.template());
          var place_holder = new area_models.AreaImmyModel();
          place_holder.text = "Any";
          place_holder.value = "0";
          dis.collection.add(place_holder,{at:0})
          
          var ib = $('#area_list_box').immybox({
            choices: dis.collection,
            maxResults: 10000,
          });
          ib[0].selectChoiceByValue("0");

          $("#area_list_box").focus({box:ib},function(evt){
            var box = evt.data.box;

            if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_area_id").val()=="0"){

             box[0].selectChoiceByValue(null);
             
            }
            if(box[0].selectedChoice==null && $("#selected_area_id").val()!="0"){
              $("#selected_area_id").val("0");
              box[0].selectChoiceByValue(null);
             
            }
            else if(box[0].selectedChoice && $("#selected_area_id").val()!=box[0].selectedChoice.value){

              $("#selected_area_id").val(box[0].selectedChoice.value);
              
            }

          });

          $("#area_list_box").blur({box:ib},function(evt){
            var box = evt.data.box;
            if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_area_id").val()=="0"){

             box[0].selectChoiceByValue("0");
             
            }
            if((box[0].selectedChoice==null && $("#selected_area_id").val()!="0")
              || ( $("#selected_area_id").val()!="0" && $("#area_list_box").val()=="")
              ){

                $("#selected_area_id").val("0");
              box[0].selectChoiceByValue("0");
               
            }
            else if(box[0].selectedChoice && $("#selected_area_id").val()!=box[0].selectedChoice.value){

              $("#selected_area_id").val(box[0].selectedChoice.value);
              
            }

          });
        
        
        
        
      }
      
    });

}); 