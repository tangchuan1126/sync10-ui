"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "./differences_list.js",
  "art_Dialog/dialog",
  "bootstrap",
    "immybox",

  
], function( config, $, Backbone, Handlebars, templates, differencesView , dialog) {
    
    var ButtonView =  Backbone.View.extend({
      template: templates.filter_button,
      el:"#filter_button_container",
     

      render:function(){
        var dis = this;
        var html =dis.template();
        dis.$el.html("");
        dis.$el.html(html);
        
        var ib = $('#status_select').immybox({
          choices: [
              {text: 'Any', value: '0'},
              {text: 'Pending', value: '1'},
              {text: 'Approved', value: '2'}
            ]
       
        });
        ib[0].selectChoiceByValue("0");
        $("#status_select").focus({box:ib},function(evt){
            var box = evt.data.box;

            if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_status").val()=="0"){
              box[0].selectChoiceByValue(null);
            }
            
            if(box[0].selectedChoice==null && $("#selected_status").val()!="0"){
              $("#selected_status").val("0");
              box[0].selectChoiceByValue(null);
            }
            
            else if(box[0].selectedChoice && $("#selected_status").val()!=box[0].selectedChoice.value){
              $("#selected_status").val(box[0].selectedChoice.value);
            }
            
          });

          $("#status_select").blur({box:ib},function(evt){
            var box = evt.data.box;
            if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_status").val()=="0"){
              box[0].selectChoiceByValue("0");
            }
            
            if((box[0].selectedChoice==null && $("#selected_status").val()!="0")
              || ( $("#selected_status").val()!="0" && $("#status_select").val()=="")
              ){
              $("#selected_status").val("0");
              box[0].selectChoiceByValue("0");
            }
            else if(box[0].selectedChoice && $("#selected_status").val()!=box[0].selectedChoice.value){
              $("#selected_status").val(box[0].selectedChoice.value);
            }
            
          });

          $("#filter_btn").click(function(evt){dis.filter_btn_click(evt)});
          $("#filter_btn_bk").click(function(evt){dis.filter(evt)});
     },
     events:{
     
     },
     filter_btn_click:function(evt){
        var dis = this;
        
        var selected_storage_id = $("#selected_storage_id").val();
        var selected_area_id = $("#selected_area_id").val();
        var status_select = $("#selected_status").val();
        
          $("#filtered_storage_id").val($("#selected_storage_id").val());
          $("#filtered_area_id").val($("#selected_area_id").val());
          $("#filtered_status").val($("#selected_status").val());
        dis.filter();


     },
     filter:function(evt){
        
        var filtered_storage_id = $("#filtered_storage_id").val();
        var filtered_area_id = $("#filtered_area_id").val();
        var filtered_status = $("#filtered_status").val();
        
        var loading = dialog();
        loading.showModal();
        new differencesView().render({ps_id:filtered_storage_id,area_id:filtered_area_id,
          approve_status:filtered_status,sortby:"",sort:"",pageNo:1})
        loading.close().remove();
        
     }
      
  });
return ButtonView;
}); 

