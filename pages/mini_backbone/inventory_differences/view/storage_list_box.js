"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/storage_model.js",
  "../model/area_model.js",
  "./area_list_box.js",
  "immybox",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,storage_models,area_models,areaListBoxView ) {
    
    return Backbone.View.extend({
      el:"#storage_list_box_container",
      template:templates.storage_list_box,
      collection: new storage_models.StorageImmyCollection(),
      
      
      
      render:function(){

        var dis = this;
        dis.$el.html(dis.template());
        var place_holder = new storage_models.StorageImmyModel();
        place_holder.text = "Any";
        place_holder.value = "0";
        dis.collection.add(place_holder,{at:0})
        var ib = $('#storage_list_box').immybox({
          choices: dis.collection,
          maxResults: 10000,
        });
        ib[0].selectChoiceByValue("0");
        
        dis.pull_areas();
        

        $("#storage_list_box").focus({box:ib},function(evt){
          var box = evt.data.box;
          
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_storage_id").val()=="0"){

           box[0].selectChoiceByValue(null);
           
          }
          if(box[0].selectedChoice==null && $("#selected_storage_id").val()!="0"){
            
            $("#selected_storage_id").val("0");
            box[0].selectChoiceByValue(null);
            dis.pull_areas();
          }
          else if(box[0].selectedChoice && $("#selected_storage_id").val()!=box[0].selectedChoice.value){

            $("#selected_storage_id").val(box[0].selectedChoice.value);
            dis.pull_areas();
          }
          
          
        });

        $("#storage_list_box").blur({box:ib},function(evt){
          var box = evt.data.box;
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_storage_id").val()=="0"){

           box[0].selectChoiceByValue("0");
           
          }
          if((box[0].selectedChoice==null && $("#selected_storage_id").val()!="0")
            || ( $("#selected_storage_id").val()!="0" && $("#storage_list_box").val()=="")
            ){
            
            $("#selected_storage_id").val("0");
            box[0].selectChoiceByValue("0");
            dis.pull_areas();
          
          }
          else if(box[0].selectedChoice && $("#selected_storage_id").val()!=box[0].selectedChoice.value){

            $("#selected_storage_id").val(box[0].selectedChoice.value);
            dis.pull_areas();
          }
          
          
        });
      },

      pull_areas:function(){
        
        var val = $("#selected_storage_id").val();
        var col = new area_models.AreaImmyCollection(val);
        var areas = new areaListBoxView({collection:col});
        areas.render();
        
        
      }
      
    });

}); 