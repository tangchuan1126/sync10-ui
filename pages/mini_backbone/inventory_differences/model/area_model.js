"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var AreaImmyModel = Backbone.Model.extend({
          idAttribute: "area_id",

          parse: function(data){
            
            this.text = /*data.title+*/data.area_name ;
            this.value = data.area_id;
            
          }
        
       });
       
      var AreaImmyCollection =  Backbone.Collection.extend({
           model: AreaImmyModel,
           url: config.getAreaForStorageJSON,

           initialize: function(ps_id){
              this.url = this.url+ps_id;
          }

        
       });

      return {
	      AreaImmyModel:AreaImmyModel,
	      AreaImmyCollection:AreaImmyCollection,

	    };




}); //page_init