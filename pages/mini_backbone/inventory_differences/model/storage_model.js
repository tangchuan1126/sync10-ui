"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var StorageImmyModel = Backbone.Model.extend({
          idAttribute: "id",

          parse: function(data){
            this.text = data.title;
            this.value = data.id;
          }
        
       });
       
      var StorageImmyCollection =  Backbone.Collection.extend({
           model: StorageImmyModel,
           url: config.getAllStorageJSON,

           initialize: function(){
            
              this.fetch({dataType: "json",async: false});
               
            }
        /*comparator: function(item) {
            return item.get('name');
        }*/
       });

      return {
	      StorageImmyModel:StorageImmyModel,
	      StorageImmyCollection:StorageImmyCollection,

	    };




}); //page_init