"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "oso.lib/DateUtil"
  
], function(config, $, Backbone, Handlebars,date) {

		  
      
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "SAA_ID",
          percentage_completed: 0,

          parse:function(data){
            var dateString = data.POST_DATE;
            var date = dateString.replace(/-/g, "/").split(".")[0];
            data["POST_DATE"] = new Date(date).format("yyyy-MM-dd hh:mm");
            if(data.APPROVE_DATE){
              dateString = data.APPROVE_DATE;
              date = dateString.replace(/-/g, "/").split(".")[0];
              data["APPROVE_DATE"] = new Date(date).format("yyyy-MM-dd hh:mm");
            }
            
            this.percentage_completed = (data.DIFFERENCE_APPROVE_LOCATION_COUNT / data.DIFFERENCE_LOCATION_COUNT)*100;
            data["percentage_completed"] = this.percentage_completed*2;
           
            return data;
          }
       });
       
      var DifferenceCollection =  Backbone.Collection.extend({
         model: DifferenceModel,
         url: config.getAreaDifferencesJSON,
         ps_id:0,
          area_id:0,
          approve_status:0,
          sortby:"post_date",
          sort:"true",
          pageNo:1,


           initialize: function(params){
            

            if(params.ps_id){this.ps_id=params.ps_id}
            if(params.area_id){this.area_id=params.area_id}
            if(params.approve_status){this.approve_status=params.approve_status}
            if(params.sortby){this.sortby=params.sortby}
            if(params.sort){this.sort=params.sort}
            if(params.pageNo){this.pageNo=params.pageNo}
            

            var ps_id=this.ps_id;
            var area_id= this.area_id;
            var approve_status = this.approve_status;
            var sortby= this.sortby;
            var sort= this.sort;
            var pageNo= this.pageNo;
            

            this.url = this.url+"?ps_id="+ps_id+"&area_id="+area_id+"&status="+
            approve_status+"&sortby="+sortby+"&sort="+sort+"&pageNo="+pageNo;
              
          },
          parse:function(response){
              
              if(response.pageCtrl){this.pageCtrl = response.pageCtrl;}
              return response.items;
              //return response;
            }
          },
            {
              pageCtrl:{
                pageNo:1,
                pageSize:10
          },
    
       });

      return {
	      DifferenceModel:DifferenceModel,
	      DifferenceCollection:DifferenceCollection,

	    };




}); //page_init