(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['admins'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <tr data-adid=\""
    + escapeExpression(lambda((depth0 != null ? depth0.adid : depth0), depth0))
    + "\">\n            <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.account : depth0), depth0))
    + "</td>\n            <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.adid : depth0), depth0))
    + "</td>\n            <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "</td>\n            <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.email : depth0), depth0))
    + "</td>\n        </tr>\n";
},"3":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<table class=\"zebraTable\">\n    <thead>\n        <tr>\n            <th>ACCOUNT</th>\n            <th>ADID</th>\n            <th>EMPLOYE_NAME</th>\n            <th>EMAIL</th>\n        </tr>\n    </thead>\n    <tbody>\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.admins : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "        <tr>\n            <th colspan=\"4\">\n                当前："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\n                页数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\n                总数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;    \n                <button data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >首页</button>&nbsp;&nbsp; \n                <button data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >上一页</button>&nbsp;&nbsp; \n                <button data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >下一页</button>&nbsp;&nbsp; \n                <button data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >末页</button>\n            </th>\n        </tr>\n    </tbody>\n</table>\n";
},"useData":true});
templates['create_admin'] = template({"1":function(depth0,helpers,partials,data) {
  return "        <tr>\n            <td colspan=\"2\">\n                <button class=\"normal-green\">提交</button>\n            </td>\n        </tr>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<table>\n<tbody>\n    <tr>\n        <th>ADID</th>\n        <td>\n            <input type=\"number\" name=\"adid\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.adid : stack1), depth0))
    + "\" disabled=\"disabled\" />\n        </td>\n    </tr>\n    <tr>\n        <th>ACCOUNT</th>\n        <td>\n            <input type=\"text\" name=\"account\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.account : stack1), depth0))
    + "\"/>\n        </td>\n    </tr>\n    <tr>\n        <th>EMPLOYE_NAME</th>\n        <td>\n            <input type=\"text\" name=\"employe_name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.employe_name : stack1), depth0))
    + "\"/>\n        </td>\n    </tr>\n    <tr>\n        <th>EMAIL</th>\n        <td>\n            <input type=\"email\" name=\"email\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.email : stack1), depth0))
    + "\"/>\n        </td>\n    </tr>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.submitBtn : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</tbody>\n</table>\n";
},"useData":true});
})();