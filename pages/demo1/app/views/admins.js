define(['jquery',
        'backbone',
        'handlebars',
        '../../templates/templates.amd',
        '../models/admin_collection',
        '../views/create_admin',
        './utils'], 
function($,
         Backbone,
         Handlebars,
         templates,
         AdminCollection,
         CreateAdminView,
         view_utils){    
    var AdminsView = Backbone.View.extend({
           el:"#admins",
           template:templates.admins,
           collection:new AdminCollection(),
           render:function(){
            
                var v = this;
               // v.collection.selectdata=[{"val":"1000004","name":"0000"},{"val":"1000007","name":"1111"},{"val":"1000005","name":"2222"}];

                $.get("/test_ps_id/",function(itmedata){
                    var selectd=[];
                    for(var keys=0;keys < 10;keys++){
                      selectd.push(itmedata[keys]);
                    }
                   v.collection.selectdata=selectd;
                }).error(function(e){
                    alert(e);
                });
                
                this.collection.fetch({
                    data:{
                        pageNo:this.collection.pageCtrl.pageNo,
                        pageSize:this.collection.pageCtrl.pageSize
                    },
                    success:function(collection){
                       v.$el.html(v.template({
                           admins:collection.toJSON(),
                           pageCtrl:v.collection.pageCtrl
                       })); 
                       onLoadInitZebraTable();
                    }
                }) ;         
            },
            events:{
                "click button":"changePage",
                "click td":"adminClick"
            },
            changePage:function(evt){
                var btn = $(evt.target);
                if(btn.data("pageno")){
                    this.collection.pageCtrl.pageNo = parseInt(btn.data("pageno"));
                }
                else if(btn.data("pageplus")){
                    this.collection.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
                }
                else return;
                this.render();
            },
            adminClick:function(evt){
              var v = this;
                var adid = $(evt.target).parent().data("adid");
                if(!adid) return;
                var collection = this.collection;
                var model = this.collection.findWhere({adid:adid});
                model=model.set({selectdata:v.collection.selectdata});                
                
                //从列表中打开一个dailog
                var updateAdmin = new CreateAdminView(model,{el:"#update_admin"});
                updateAdmin.render(false);
                
                //写入dailog里更新和删除按钮
                view_utils.updateDialog(function(){
                  model.unset("selectdata");  
                    //更新数据
                   model.save(
                       {
                            account: updateAdmin.$el.find("input[name='account']").val(),
                            employe_name:updateAdmin.$el.find("input[name='employe_name']").val(),
                            email:updateAdmin.$el.find("input[name='email']").val(),
                            ps_id:updateAdmin.$el.find("select[name='ps_id']").val()
                        },
                        {
                            success:function(){
                                v.render();
                            }
                        }
                   ); 
                },
                function(){
                    //删除数据
                    collection.remove(model);
                    model.url = model.url + "?id="+model.get("adid");
                    model.destroy({
                        success: function(model, response) {
                                v.render();
                        }
                    });
                });
           }
    });
    return new AdminsView();
});