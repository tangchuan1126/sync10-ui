define(['jquery',
        'backbone',
        'handlebars',
        '../../templates/templates.amd',        
        '../models/admin','handlebars_ext'], 
function($,
         Backbone,
         Handlebars,
         templates,
         AdminModel,handlebars_ext){  

    return  Backbone.View.extend({
        template:templates.create_admin,
        initialize:function(model,options){
            this.setElement(options.el);
            if(model){
                this.model = model;
                console.log(this.model);
            }else{
             this.model = new AdminModel();
             console.log(this.model);
            }
        },
        render:function(submitBtn){
         var v=this;
                $.get("./app/data.json",function(itmedata){
                    var selectd=[];
                    for(var keys=0;keys < 10;keys++){
                      selectd.push(itmedata[keys]);
                    }
                    v.model.set({selectdata:selectd});
               v.$el.html(v.template({model:v.model.toJSON(),submitBtn:submitBtn}));

                }).error(function(e){
                    alert(e);
                });
           
            
        },
        events:{
            "click button":"createAdmin"
        },
        createAdmin:function(evt){
            var el = this.$el;
            var v = this;
            this.model.unset("selectdata"); 
            this.model.save({
                "account": el.find("input[name='account']").val(),
                "employe_name":el.find("input[name='employe_name']").val(),
                "email":el.find("input[name='email']").val(),
                "ps_id":el.find("select[name='ps_id']").val()
            },
            { 
                success:function(m){
                    v.model = new AdminModel();
                    v.render(true);
                    $("#tabs").tabs("option","active",0);
                }
            });
        }
    });
    
});