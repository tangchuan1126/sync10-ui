define(['underscore'], function(underscore) {
	Date.prototype.format = function (format){
		var o = {
			"M+": this.getMonth() + 1,
			"d+": this.getDate(),
			"h+": this.getHours(),
			"m+": this.getMinutes(),
			"s+": this.getSeconds(),
			"q+": Math.floor((this.getMonth() + 3) / 3),
			"S": this.getMilliseconds()
		}
		if (/(y+)/.test(format)) {
			format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		}
		for (var k in o)
		{
			if (new RegExp("(" + k + ")").test(format)) {
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
			}
		}
		return format;
	}
	String.prototype.startWith = function(str){var t=this;return t.indexOf(str) == 0;};
	String.prototype.endWith = function(str){var t = this;return t.substring(t.length-str.length,t.length) == str;};
	var locallanguage = ((""+window.navigator.language).startWith("en")?"en":"zh");
	var rt = {
		/**
		获取语言环境(zh,en)
		**/
		getLanguage: function() {
			return locallanguage;
		},
		/**
		标准时间格式,适用适应语言
		**/
		formatDate: function (date) {
			if(typeof date !="undefined" && date !="") {
				var rtdate = "";
				if ("en"== locallanguage) {
					//dd/MM/yyyy HH:mm
					rtdate = new Date(date).format('dd/MM/yyyy');
				} else {
					rtdate = new Date(date).format('yyyy-MM-dd');
				}
				if (rtdate.endWith(" 00:00")) {
					rtdate = rtdate.substr(0,10);
				}
				return rtdate;
			} else {
				return "";
			}
		},
		/**
		短时间
		**/
		shortFormatDate: function (date) {
			if(typeof date !="undefined" && date !="") {
				var rtdate = "";
				if ("en"== locallanguage) {
					rtdate = new Date(date).format('dd/MM HH:mm');
				} else {
					rtdate = new Date(date).format('MM-dd hh:mm');
				}
				return rtdate;
			} else {
				return "";
			}
		},
		/**
		
		**/
		getDate2LongTime: function(date) {
			return Date.parse(date)
		},
		Now: function() {
			return new Date();
		},
		changeOneDecimal: function(floatvar) {
			var f_x = parseFloat(floatvar);
			if (isNaN(f_x)) return "";
			var f_x = Math.round(floatvar*10)/10;
			return f_x;
		},
		getDiffDate: function(endDate,rtnType) {
			var startT = rt.getDate2LongTime(new Date());
			var ss=(startT-endDate)/(1000); //共计秒数
			var mm = ss/60;   //共计分钟数
			var hh= ss/3600;  //共计小时数
			var dd= hh/24;   //共计天数
			if(rtnType=="ss") {
				return rt.changeOneDecimal(ss);
			} else if(rtnType=="hh") {
				return rt.changeOneDecimal(hh);
			} else if(rtnType=="mm") {
				return rt.changeOneDecimal(mm);
			} else if(rtnType=="dd") {
				return rt.changeOneDecimal(dd);
			} else {
				return rt.changeOneDecimal(dd);
			}
		},
		
		getEndsOn:function(obj){
			
			var res = "";
			switch (obj.IS_REPEAT){
				case true:
					if(obj.ENDS_TYPE){
						switch (obj.ENDS_TYPE){
							case 1:
								res = "Never"
								break;
							case 2:
								res = "After "+obj.ENDS_OCCURRENCE+" occurrences"
								break;
							case 3:
								res = rt.formatDate(obj.ENDS_ON);	
								break;
						}
					}
					break;
				case false:
					res = rt.formatDate(obj.ENDS_ON);
					break;
			}
			
			return res;
		
			
		},
		formatTitle:function(title){
			if(title==null){return "--";}
			else{return title;}
		},
		getClass:function(value){
			if(value==null || value == false){return "hidden";}
			return "";
		},
		getNegateClass:function(value){
			if(value==true){return "hidden";}
			return "";
		},
		formatAssignedUser:function(value){
			if(value==null){return "Anyone in the group";}
			return value;
		},
		formatTaskType:function(value){
			var val = value;
			switch (value){
				case 2:
					val= "Verify";
					break;
				case 1:
					val = "Blind";
					break;

			}
			return val;
		},
		formatStatus:function(value){
			var val = value;
			switch (value){
				case 1:
					val= "New";
					break;
				case 2:
					val = "In-progress";
					break;
				case 3:
					val = "Done";
					break;
				case 4:
					val = "Pending Approval";
					break;
				case 5:
					val = "Reviewed";
					break;

			}
			return val;
		},
		formatRepeatBy:function(value){
			var val = value;
			switch (value){
				case 1:
					val= "Daily";
					break;
				case 2:
					val = "Weekly";
					break;
				case 3:
					val = "Monthly";
					break;

			}
			return val;
		},
		formatRepeatEvery:function(obj){
			var repeatBy = obj.REPEAT_BY;
			var val="";
			switch (repeatBy){
				case 1:
					val= "Day(s)";
					break;
				case 2:
					val = "Week(s)";
					break;
				case 3:
					val = "Month(s)";
					break;

			}
			return obj.REPEAT_EVERY + " "+ val;
		},
		formatDuration:function(value){
			
			return value+" Day(s)";
		},
		formatCycle:function(value){
			var val = value;
			switch (value){
				case true:
					val= "Repeat Task";
					break;
				case false:
					val = "Non-Repeat Task";
					break;
				
			}
			return val;
		},
		formatState:function(value){
			var val = value;
			switch (value){
				case 1:
					val= "Active";
					break;
				case 2:
					val = "Inactive";
					break;
				case 3:
					val = "Completed";
					break;

			}
			return val;
		},
	}
	return rt;
});