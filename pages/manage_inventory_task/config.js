define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getUsersOfWarehouseJSON: "/Sync10/_inventoryControl/search/usersOfWarehouse?ps_id=",
    getAllTitlesJSON: "/Sync10/_inventoryControl/search/titles",
    searchTasks: "/Sync10/_inventoryControl/search/taskList",
   	stopTaskURL: "/Sync10/_inventoryControl/update/updateTaskState?task_id=",
   	resumeTaskURL: "/Sync10/_inventoryControl/update/updateTaskState?task_id=",
   	deleteTaskURL: "/Sync10/_inventoryControl/update/updateTaskState?task_id=",
   	reAssignTaskURL : "/Sync10/_inventoryControl/update/reassignTask?task_id=",
   	getSingleTaskDetail: "/Sync10/_inventoryControl/search/taskDetail?task_id=",
    editCopyTaskDetail: "/Sync10/_inventoryControl/search/taskDefinition?task_id=",
    getTaskProgress: "/Sync10/_inventoryControl/search/taskProgress?task_id=",

    submitScan:"/Sync10/_inventoryControl/execute/submitLocationScan",
   	
   	deleteTaskString: "Are you sure you want to delete this task?",
});
