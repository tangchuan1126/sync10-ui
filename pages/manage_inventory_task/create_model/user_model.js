"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var UserModel = Backbone.Model.extend({
          idAttribute: "ADID",
        
       });
       
      var UserCollection =  Backbone.Collection.extend({
           model: UserModel,
           url: config.getUsersOfWarehouseJSON,

           initialize:function(ps_id){
            this.url = this.url+ps_id;
            
           },
           comparator: function(item) {
                return item.get('EMPLOYE_NAME');
            },
       });

      return {
	      UserModel:UserModel,
	      UserCollection:UserCollection,

	    };




}); //page_init