"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "./create_task_view.js",
  "jqueryui/datepicker",
  "../create_js/common",
  "bootstrap",
  "iToggle"


], function( $, Backbone, Handlebars, templates, taskView, datepicker,common) {
    
    return Backbone.View.extend({
      el:"#deadline_body",
      template:templates.deadline,
      task:null,
      
      initialize:function(data){
          var dis = this;
          dis.task = data.task;
      },
      setDefaultValue:function(){
        var dis = this;
        if(dis.task){
          $("#created_by").val(dis.task.CREATED_BY);
          if(dis.task.TYPE==1){$("#is_blind").click();}
          if(dis.task.IS_REPEAT){
            $("#is_repeating").click();
            $("#task_duration option[value='"+dis.task.DURATION+"']").prop("selected",true);
            var repeat_by,repeat_options;
            switch(dis.task.REPEAT_BY){
              case 1:
                repeat_by="Daily";
                repeat_options = "repeat_options_daily";
                break;
              case 2:
                repeat_by="Weekly";
                repeat_options = "repeat_options_weekly";
                break;
              case 3:
                repeat_by="Monthly";
                repeat_options = "repeat_options_monthly";
                break;
            }

            $("#repeat_options option[value='"+repeat_by+"']").prop("selected",true);
            $("#repeat_options").change();
            $("#"+repeat_options+" option[value='"+dis.task.REPEAT_EVERY+"']").prop("selected",true);
            
            $("#starts_on").val(dis.task.STARTS_ON);
            var dt2 = $('#ends_on_date');
            dt2.datepicker('option', 'minDate', dis.task.STARTS_ON);

            switch(dis.task.ENDS_TYPE){
              case 1:
                $("#ends_on_never").prop("checked",true);
                break;
              case 2:
                $("#ends_on_after").prop("checked",true);
                break;
              case 3:
                $("#ends_on_on").prop("checked",true);
                break;
            }
            
            if(dis.task.ENDS_ON){
              $('#ends_on_date').val(dis.task.ENDS_ON);
            }
            if(dis.task.ENDS_OCCURRENCE){
              $('#occurances').val(dis.task.ENDS_OCCURRENCE);
            }

          }else{
            $("#start_date").val(dis.task.STARTS_ON);
            $("#end_date").val(dis.task.ENDS_ON);
          }
          dis.check_preview();
        }
      },
      render:function(){

        var dis = this;
        var html=dis.template();
        dis.$el.html(html);

        $("#start_date").datepicker({ dateFormat: "yy-mm-dd",minDate:0,onSelect: function (date) {
            var dt2 = $('#end_date');
            var minDate = $(this).datepicker('getDate');
            dt2.val("");
            dt2.datepicker('option', 'minDate', minDate);
            dis.check_preview();
        }});
        $("#end_date").datepicker({ dateFormat: "yy-mm-dd",minDate:1});
        $("#back_user_selection").click(function(){common.show_users("right");});
        $("#starts_on").datepicker({ dateFormat: "yy-mm-dd",minDate:0,onSelect: function (date) {
            var dt2 = $('#ends_on_date');
            var minDate = $(this).datepicker('getDate');
            dt2.val("");
            dt2.datepicker('option', 'minDate', minDate);
            dis.check_preview();
            
        }});
        $("#ends_on_date").datepicker({ dateFormat: "yy-mm-dd",minDate:1});
        $("#create_task_link").click(function(){dis.create_task();});
        $("#is_repeating").change(function(){dis.repeating_clicked();});
        $("#repeat_options").change(function(){dis.repeat_options_changed()})
        $("#ends_on_date").focus(function(){$("#ends_on_on").prop('checked',true);dis.check_preview();});
        $("#occurances").focus(function(){$("#ends_on_after").prop('checked',true);dis.check_preview();})
        $("#is_repeating").iToggle();
        $("#is_blind").iToggle();
        $("#deadline_body input").change(function(evt){dis.check_preview(evt);})
        
        $("#deadline_body select").change(function(){dis.check_preview();})
        dis.setDefaultValue();
      },
      callCommon:function(){
         common.checkEnableCreateButton();
      },
      check_preview:function(evt){
        var dis=this;
        var valid = true;
        
        if($("#is_repeating").prop('checked')){
          if($("#repeat_options").val()=="Daily"){
            valid = dis.validate_daily();
          }else if($("#repeat_options").val()=="Weekly"){
            valid = dis.validate_weekly();
          }
          else if($("#repeat_options").val()=="Monthly"){
            valid = dis.validate_monthly();
          }
        }else{
          if($("#start_date").val()==""){valid = false;}
          if($("#end_date").val()==""){valid = false;}
        }

        if(valid){
          $("#create_task_link").parent().removeClass("disabled");
        }else{
          $("#create_task_link").parent().addClass("disabled");
        }
      },
      
      validate_monthly:function(){
       var dis=this;
       return dis.validate_schedule_dates();
      },
      validate_weekly:function(){
       var dis=this;
       return dis.validate_schedule_dates();
      },
      validate_daily:function(){
       var dis=this;
       return dis.validate_schedule_dates();
      },
      validate_schedule_dates:function(){
        var v = true;
        if($("#starts_on").val()==""){v=false;}
        
        if($('#ends_table input:radio[name="ends_on"]:checked').val()=="never"){

        }else if($('#ends_table input:radio[name="ends_on"]:checked').val()=="after"){
            if($("#occurances").val()==""){v=false;}
        }else if($('#ends_table input:radio[name="ends_on"]:checked').val()=="on"){
            if($("#ends_on_date").val()==""){v=false;}
        } 
        
        return v;
      },
      repeat_options_changed:function(){
          
          var selected = $("#repeat_options").val();
          $("#Daily_table").hide();
          $("#Weekly_table").hide();
          $("#Monthly_table").hide();
          $("#"+selected+"_table").show();

      },
      repeating_clicked:function(){
        if($("#is_repeating_checkbox_div :checked").length>0){
          $("#repeating_div").show();
          $("#non-repeating-div").hide();
        }else{
          $("#repeating_div").hide();
          $("#non-repeating-div").show();
        }
      },

      create_task:function(){
        common.show_date("left");
        new taskView().render();
      }

      
    });

}); 