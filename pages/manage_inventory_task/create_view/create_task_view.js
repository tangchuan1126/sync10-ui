"use strict";
define([
  "jquery",
  '../create_config/config',
  "backbone",
  "handlebars",
  "templates",
  "../create_model/task_request_model.js",
  "./selected_areas_view.js",
  "../create_js/common",
  "immybox",
  "bootstrap",
  "showMessage",

], function( $,config, Backbone, Handlebars, templates, task_model, selectedAreas , common) {
    
    return Backbone.View.extend({
      el:"#create_task_body",
      template:templates.create_task,
      collection:null,
      
      render:function(){
        var dis = this;
        
        var warehouse_id = $("#selected_storage_id").val();
        var warehouse_name = $("#storage_list_box").val();
        var title = $("#selected_title_id").val() == "0" ? null:$("#selected_title_id").val()  ;
        var user_id = 0;
        var user_name = "";
        $.each($("#users_body .selected_employee .employee_name"), function(i,item){
          user_id = $(item).attr("id");
          user_name = $(item).html();
        });
        var isRepeat = $("#is_repeating").prop('checked')? "Yes" : "No";
        var scheduled = $("#is_repeating").prop('checked');
        var type = $("#is_blind").prop('checked')? "Blind" : "Verify";
        var typeVal = $("#is_blind").prop('checked')? 1 : 2;
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var duration = $("#task_duration").val() + " Day(s)";
        var repeatOptions = $("#repeat_options").val();
        var repeatEvery;
        var endsOn;
        var occurance;
        var endsType;
        var endsOnDate, repeatBy;
        if(repeatOptions == "Daily"){
          repeatBy = 1;
          repeatEvery = $("#repeat_options_daily").val() + " Day(s)";
        }
        if(repeatOptions == "Weekly"){
          repeatBy = 2;
          repeatEvery = $("#repeat_options_weekly").val()+ " Week(s)";
          
        }
        if(repeatOptions == "Monthly"){
          repeatBy = 3;
          repeatEvery = $("#repeat_options_monthly").val() + " Month(s)";
        }
        var startsOn = $("#starts_on").val();
        var endsOnOption = $("input[type='radio'][name='ends_on']:checked").val();
        if(endsOnOption=="never"){
            endsType = 1;
            endsOn = "Never";
        }else if(endsOnOption=="after"){
            endsType = 2;
            occurance = parseInt($("#occurances").val());
            endsOn = "After "+$("#occurances").val()+" occurrences"
        }else{
          endsType = 3;
            endsOn = $("#ends_on_date").val();
            endsOnDate = $("#ends_on_date").val();
        }
        var areas = new task_model.AreaListCollection();
        $.each($("#toc_area .focusedInput"), function(i,item){
          var area_id = $(item).parent().attr("id").split("_")[1];
          var area_name = $(item).find(".panel_heading_title").html();
          var locations = new task_model.ListCollection();
          
              $.each($(item).find(".itmeInventory_box :checked"),function(a,loc){
                var location_id = $(loc).attr("data-checkboxid");
                var location_name = $(loc).parent().find("span").html();
                var location = new task_model.ListModel({locationName:location_name,locationId:parseInt(location_id)});
                locations.add(location);
              });
          var area = new task_model.AreaListModel({areaName:area_name,areaId:parseInt(area_id),locations:locations.toJSON()});    
          areas.add(area);
        });

        var buttonTitle = "Create Task";
        if($("#create_task_type").val()=="edit"){
          buttonTitle = "Update Task";
        }

        var html=dis.template({buttonTitle:buttonTitle,startsOn:startsOn,endsOn:endsOn,repeatEvery:repeatEvery,repeatOptions:repeatOptions,duration:duration,scheduled:scheduled,type:type,isRepeat:isRepeat,user:user_name,title:title,warehouse:warehouse_name,start_date:start_date,end_date:end_date});
        dis.$el.html(html);
        if(isRepeat=="No"){
          endsOnDate = end_date;
          startsOn = start_date;
        }
        var task = new task_model.TaskModel({areas:areas,isRepeat:scheduled,endsType:endsType,startsOn:startsOn,endsOn:endsOnDate,occurrence:occurance,repeatEvery:repeatEvery.split(" ")[0],repeatBy:repeatBy,duration:duration.split(" ")[0],type:typeVal,user:parseInt(user_id),title:title==null?0:parseInt(title),warehouse:parseInt(warehouse_id)});
        new selectedAreas({collection:areas}).render();
        
        $("#back_date_selection").click(function(){common.show_date("right");});
        $("#create_task_btn").click(function(){dis.create_task(task);});
        
      },
      create_task:function(task){
        var dis = this;

        task.duration = task.duration.split(" ")[0];
        task.repeatEvery = task.repeatEvery.split(" ")[0];
        var taskType = $("#create_task_type").val();
        
        if(taskType=="edit"){
          var taskId = $("#task_id").val();
          var created_by = $("#created_by").val();
          task.set({id:taskId,createdBy:created_by});
          
          $.ajax({
              url:  config.updateTaskDetail,
              type: 'put',
              data: JSON.stringify(task),
              contentType: 'application/json; charset=UTF-8',
              timeout: 60000,
              cache:false,
              dataType: 'json',
              async:true,
              
              error: function(jqXHR, textStatus, errorThrown) {
                showMessage("error while updating task.","error");
              },
              success: function(data){
                showMessage("Task has been updated successfully.","succeed");
                $("#back").click();
              }
            });
        }else{
          task.save(null,{success:function(){
              showMessage("Task has been created successfully.","succeed");
              $("#back").click();
          },
         error:function(){
            showMessage("error while creating task.","error");
          }});
        }
        
        
      }
      
    });

}); 