"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/title_model.js",
  "../create_model/area_model.js",
    "../create_model/user_model.js",
  "./area_list_box.js",
    "oso.lib/InventoryControls/Inventory",
    "../create_js/common",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,title_models,area_models,userModel,areaListBoxView, InventoryControl, common,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#toc_title",
      template:templates.title_list_box,
      collection: new title_models.TitleImmyCollection(),
      task:null,
      
      selectDefaultValue:function(immyBoxIns){
        var dis= this;
        if(dis.task.TITLE_ID){
          immyBoxIns.selectChoiceByValue(dis.task.TITLE_ID);
          $("#selected_title_id").val(dis.task.TITLE_ID);
        }else{
          immyBoxIns.selectChoiceByValue("0");
          $("#selected_title_id").val("0");
        }
        
        dis.pull_areas();
      },
      render:function(data){
        
        var dis = this;
        dis.$el.empty();
        dis.collection.url = config.getAllTitlesJSON+ "?ps_id="+data.warehouse_id;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");
        dis.$el.html(dis.template());
        
        var selectedValue = "0";
        if(dis.task.TITLE_ID){
          
          selectedValue = dis.task.TITLE_ID;
        }
        var titleImmydata = {
          renderTo: "#create_title_div",
          dataUrl: dis.collection,
          placeHolder:"All", 
          inputId: "#title_list_box",
          selectedInputId: "#selected_title_id",
          defaultValue: selectedValue
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        dis.pull_areas();
      },

      pull_areas:function(){
        var dis = this;
        var title = $("#selected_title_id").val();
        var warehouse = $("#selected_storage_id").val();

          common.hideError();
        
          $("#toc_title").show();
          $("#toc_area").empty();
          
          var col = new area_models.AreaImmyCollection(warehouse,title);
          var areas = new areaListBoxView({collection:col,task:dis.task});
          if(areas.collection.length > 0){
                common.show_other_fields();
                areas.render();

            }else{
              common.hide_other_fields();
              common.showError("Selected Title has no areas");
              $("#toc_title").show();
            }

      }
      
    });

}); 