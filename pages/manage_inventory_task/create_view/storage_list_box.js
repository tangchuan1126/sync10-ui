"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/storage_model.js",
  "../create_model/area_model.js",
  "../create_model/user_model.js",
  "../create_model/task_request_model",
  "../create_model/title_model",
  "./area_list_box.js",
  "./title_list_box.js",
  "./users_view.js",
  "./deadline_view.js",
  "oso.lib/InventoryControls/Inventory",
    "../create_js/common",
  "immybox",
  "bootstrap",


], function( $, Backbone, Handlebars, templates,storage_models,area_models, userModel,task_model,title_models, areaListBoxView , titleListBoxView, userView,deadlineView, InventoryControl, common) {
    
    return Backbone.View.extend({
      el:"#toc_warehouse",
      template:templates.storage_list_box,
      collection: new storage_models.StorageImmyCollection(),
      task:null,

      initialize:function(data){
        var dis = this;
        dis.task = data.task;
       
      },
      
      selectDefaultValue:function(immyBoxIns){
        var dis= this;
        if(dis.task.WAREHOUSE_ID){
          immyBoxIns.selectChoiceByValue(dis.task.WAREHOUSE_ID);
          $("#selected_storage_id").val(dis.task.WAREHOUSE_ID);
        }else{
          immyBoxIns.selectChoiceByValue("0");
          $("#selected_storage_id").val("0");
        }
        
        dis.pull_areas();
      },

      render:function(){

        var dis = this;
        
        dis.$el.html(dis.template());
        if(dis.collection.at(0).value!=0){
          var place_holder = new storage_models.StorageImmyModel();
          place_holder.text = "Please Select";
          place_holder.value = "0";
          dis.collection.add(place_holder,{at:0})
        }
       
        
        var ib = $('#storage_list_box').immybox({
          choices: dis.collection,
          maxResults: 10000,
        });
        //ib[0].selectChoiceByValue("0");
        
        $("#toc_title").hide();
        $("#storage_list_box").focus({box:ib},function(evt){
          var box = evt.data.box;
          
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_storage_id").val()=="0"){

           box[0].selectChoiceByValue(null);
           
          }
          if(box[0].selectedChoice==null && $("#selected_storage_id").val()!="0"){
            
            $("#selected_storage_id").val("0");
            box[0].selectChoiceByValue(null);
            dis.pull_areas();
          }
          else if(box[0].selectedChoice && $("#selected_storage_id").val()!=box[0].selectedChoice.value){

            $("#selected_storage_id").val(box[0].selectedChoice.value);
            dis.pull_areas();
          }
          
          
        });

        $("#storage_list_box").blur({box:ib},function(evt){
          var box = evt.data.box;
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $("#selected_storage_id").val()=="0"){

           box[0].selectChoiceByValue("0");
           
          }
          if((box[0].selectedChoice==null && $("#selected_storage_id").val()!="0")
            || ( $("#selected_storage_id").val()!="0" && $("#storage_list_box").val()=="")
            ){
            
            $("#selected_storage_id").val("0");
            box[0].selectChoiceByValue("0");
            dis.pull_areas();
          
          }
          else if(box[0].selectedChoice && $("#selected_storage_id").val()!=box[0].selectedChoice.value){

            $("#selected_storage_id").val(box[0].selectedChoice.value);
            dis.pull_areas();
          }
   
        });

        dis.selectDefaultValue(ib[0]);
      },

      pull_areas:function(){
        var dis=this;
        var val = $("#selected_storage_id").val();

        common.hideError();
        if(val!="0"){
          
          var titleListBox = new titleListBoxView();
          titleListBox.render({warehouse_id:val,task:dis.task});
          
          $("#toc_title").show();
          $("#toc_area").empty();

          var userCollection = new userModel.UserCollection(val);
          var userViewObj = new userView({collection:userCollection, userId:dis.task.USER_ID});
          
          if(userCollection.length ==0){
            common.hide_other_fields();
            common.showError("Selected warehouse has no users assigned");
          }else{
            userViewObj.render();
            
            new deadlineView({task:dis.task}).render();
            
          }
        }
      },

      
      create_task:function(){
        var dis = this;
        console.log("creating task");
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var warehouse = $("#selected_storage_id").val();
        var title = $("#selected_title_id").val();
        var user = 0;
        $.each($("#users_body .selected_employee .employee_name"), function(i,item){
          user = $(item).attr("id");
        });
        var areas = new task_model.AreaListCollection();
        $.each($("#toc_area .focusedInput"), function(i,item){
          var area_id = $(item).parent().attr("id").split("_")[1];
          
          var locations = new task_model.ListCollection();
          
              $.each($(item).find(".itmeInventory_box :checked"),function(a,loc){
                var location_id = $(loc).attr("data-checkboxid");
                var location = new task_model.ListModel({location_id:parseInt(location_id)});
                locations.add(location);
              });
          var area = new task_model.AreaListModel({area_id:parseInt(area_id),locations:locations});    
          areas.add(area);
        });
        var taskModel = new task_model.TaskModel({user:parseInt(user),warehouse:parseInt(warehouse),areas:areas,title:parseInt(title),start_date:start_date, end_date:end_date});
        
        console.log("task",JSON.stringify(taskModel));

      }

        


        
      
      
    });

}); 