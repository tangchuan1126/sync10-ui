"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar) {
    
    return Backbone.View.extend({
      el:"#selected_areas_create",
      template:templates.area_list_box,
      collection:null,
      
      render:function(){
        var dis = this;
        var html=dis.template({list: dis.collection.toJSON()});
        dis.$el.html(html);

        var list_unstyled = $("#areas_in_selection .locations_block");
          // _ztreebox.height(_def.scrollH);
          list_unstyled.mCustomScrollbar({
            axis: "y",
            //scrollInertia: scrollH,
            scrollbarPosition: "outside"
          });
          list_unstyled.find(".mCSB_scrollTools").css("right", "-22px");
      },
      

      
    });

}); 