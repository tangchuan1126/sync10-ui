define(["./jsontohtml_templates"], function(tmp) {

	return {
		"View_control": {
			"head": [{
				"title": "Task Info",
				"field": "basicInfo"
			}, {
				"title": "Schedule Info",
				"field": "ScheduleInfo"
			}
			],
			
			"idAttribute":"ID",
			"authIdAttribute":"ID",
			"footer": [
				
				[
					"events.stop", "Stop"
				],
				[
					"events.resume", "Resume"
				],
				[
					"events.edit", "Edit"
				],
				[
					"events.reassign", "Reassign"
				],
				[
					"events.copy", "Copy"
				],
				[
					"events.delete", "Delete"
				],
				
				
			],
			"templates": tmp
		}
	}

});