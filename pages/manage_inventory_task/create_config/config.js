define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getAreaForStorageJSON: "/Sync10/action/administrator/storage_location/GetLocationAreasJSON.action?ps_id=",
    getAreaLocationUserJSON: "/Sync10/_inventoryControl/search/detailedAreaLocations?ps_id=",
    getUsersOfWarehouseJSON: "/Sync10/_inventoryControl/search/usersOfWarehouse?ps_id=",
    getAreaDifferencesJSON: "/Sync10/_inventoryControl/search/filterStorageApproveAreas",
    getAllTitlesJSON: "/Sync10/_inventoryControl/search/titles",
    getLocationDifferencesJSON: "/Sync10/_inventoryControl/search/getStorageApproveLocations?saa_id=",
    //getContainerDifferencesJSON: "/Sync10/action/administrator/location/GetContainerDifferencesJSONAction.action",
    getContainerDifferencesJSON: "/Sync10/_inventoryControl/search/getStorageApproveContainers",
    getCountDifferenceOfContainerJSON: "/Sync10/_inventoryControl/search/getStorageApproveDifferents?sac_id=",
    saveApproveDifference: "/Sync10/_inventoryControl/update/approveDifferents",
    createTaskDifference: "/Sync10/_inventoryControl/add/taskDefinition",
    updateTaskDetail: "/Sync10/_inventoryControl/update/taskDefinition",
    editCopyTaskDetail: "/Sync10/_inventoryControl/search/taskDefinition?task_id=",
   
});
