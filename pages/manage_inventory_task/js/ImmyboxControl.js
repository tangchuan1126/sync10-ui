/*
 * Inventory.js
 * Copyright 2014, Connor
 * http://127.0.0.1/Sync10-ui/ilb/InventoryControls/
 */

;
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {

    // AMD. Register as an anonymous module.
    define(['jquery', 'underscore', 'immybox'], factory);

  } else if (typeof exports === 'object') {

    // Node. Does not work with strict CommonJS

    module.exports = factory(require('jquery'), require('underscore'), require('immybox'));

  } else {
    // Browser globals (root is window)
    root.Inventory = factory(root.jquery, root.underscore, root.immybox);
  }
}(this, function($, underscore, immybox) {
  'use strict';

  function InventoryPlugin(opts) {

    if (!(this instanceof InventoryPlugin))
      return new InventoryPlugin(opts);

    if (typeof opts === 'string')
      opts = {
        renderTo: opts
      };

    if (!opts.renderTo)
      return this.error('A renderTo needs to be specified');

    this.opts = opts;
    this._Dom = null;
    this._Int(this.opts);
    this.id = false;
    
  }


  InventoryPlugin.prototype._Int = function(opts) {

    var _this = this;   
    var _opts = _this.opts;
    var dom = $(_opts.renderTo);
    _this.opts._Dom = dom;
    _this.opts = _.extend(_this.opts, opts);
    dom.addClass('hidden');   

  }


  InventoryPlugin.prototype.on = function(events, callback) {

    Componbox._events(events, callback, this);

  };

  InventoryPlugin.prototype.render = function() {

    var _this = this;

    var _opts = _this.opts;

    if (_opts._Dom.length > 0) {

      //_this.opts._Dom = dom;
      Componbox.createcss(_opts);
      Componbox.createboxhtml(_opts);
      $(_opts.inputId).blur();
    } else {

      var errorobj = {
        txt: "没有容器",
        error: ""
      };
      Componbox.error(_opts, errorobj);

    }



  }


  var Componbox = {
    ConfingDATA: {},
    JSONDATA: {},
    //提供的事件
    _events: function(functionname, callback, _this) {

      switch (functionname) {

        case "events.Error":
          _this.opts.error = callback;
          break;
        case "events.change":
          _this.opts.change = callback;
          break;
        case "Initialize":
          _this.opts.Initialize = callback;
          break;

      }

    },
    error: function(opts, errorobj) {

      if (opts.error) {
        opts.error(errorobj);
      } else {

       if(window.console)
        window.console.log(errorobj.txt);
        //alert(errorobj.txt);
      }

      return false;

    },
    
       
    createcss: function(opts) {
     

      var cssfileall = [
        "require_css!bootstrap-css/bootstrap.min",
        "require_css!Font-Awesome",
        "require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css",
        "require_css!oso.lib/InventoryControls/css/style.css"
      ];

      var _dom=opts._Dom;

      require(cssfileall, function(){

       setTimeout(function(){

        _dom.removeClass('hidden');

        },10);

      });
      

    },
    createboxhtml: function(_opts) {
        if(_opts.placeHolder){
          var place_holder = {
          text :_opts.placeHolder,
          value : "0"};
          if(_opts.dataUrl.at(0) && _opts.dataUrl.at(0).value!="0"){
             _opts.dataUrl.add(place_holder,{at:0});
             
          }
         
        }
        var choice = _opts.dataUrl;
        if(typeof _opts.dataUrl.models == 'object'){
          choice = _opts.dataUrl.toJSON();
        }
        
        var ib = $(_opts.inputId).immybox({
          choices: choice,
          maxResults: 10000,
        });
        if(_opts.defaultValue){
          ib[0].selectChoiceByValue(_opts.defaultValue);
        }else{
          ib[0].selectChoiceByValue("0");
        }
        

        $(_opts.inputId).focus({box:ib},function(evt){
          var box = evt.data.box;
        
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $(_opts.selectedInputId).val()=="0"){

           box[0].selectChoiceByValue(null);
           
          }
          if(box[0].selectedChoice==null && $(_opts.selectedInputId).val()!="0"){
            
            $(_opts.selectedInputId).val("0");
            box[0].selectChoiceByValue(null);
            _opts.change();
          }
          else if(box[0].selectedChoice && $(_opts.selectedInputId).val()!=box[0].selectedChoice.value){

            $(_opts.selectedInputId).val(box[0].selectedChoice.value);
            _opts.change();
          }
          
          
        });

        $(_opts.inputId).blur({box:ib},function(evt){
          var box = evt.data.box;
          if((box[0].selectedChoice==null || box[0].selectedChoice.value==0)  && $(_opts.selectedInputId).val()=="0"){

           box[0].selectChoiceByValue("0");
           
          }
          if((box[0].selectedChoice==null && $(_opts.selectedInputId).val()!="0")
            || ( $(_opts.selectedInputId).val()!="0" && $(_opts.inputId).val()=="")
            ){
            
            $(_opts.selectedInputId).val("0");
            box[0].selectChoiceByValue("0");
             _opts.change();
          
          }
          else if(box[0].selectedChoice && $(_opts.selectedInputId).val()!=box[0].selectedChoice.value){

            $(_opts.selectedInputId).val(box[0].selectedChoice.value);
             _opts.change();
          }
          
          
        });

    },
    


  };



  return InventoryPlugin;

}));