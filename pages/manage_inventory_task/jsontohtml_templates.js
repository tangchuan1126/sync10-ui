define(["JSONTOHTML",
	"DateUtil",
	"./Map"
	], function(JSONTOHTML,
		DateUtil,
		Map
		) {

	//第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index)
					{
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}"
		}

	};
	
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		int: function(jsons,View_control) 
		{
			try
			{
				var start = new Date().getTime();
				//初始化全局配置
				_tbody.idAttribute = View_control.idAttribute;
				_tbody.authIdAttribute = View_control.authIdAttribute;
				_tbody.headallobj = View_control.head;	
				_tbody.footer = View_control.footer;
				
				var data = new Map(_tbody.idAttribute,jsons.DATA);
				var pageSize=jsons.PAGECTRL.pageSize;

				//按表头转换成行和列
				var __tr=[];	
				for (var tri = 0; tri < jsons.DATA.length; tri++) 
				{
					var rowData = jsons.DATA[tri];
	                var __td = [];
					for (var headkey in _tbody.headallobj) 
					{
						var column = _tbody.headallobj[headkey];
						var columnFiled = column.field;
						var columnData = eval("({" + columnFiled + ":rowData})");
						__td.push(columnData);
					}

					__tr.push({"rowId":rowData[_tbody.idAttribute],"children": __td});

					if(tri >= pageSize)
					{
						break;
					}
				}

				//拼装tr,trfooter，
				var __rowsHtml = "";
				for (var trkey in __tr) 
				{
					var _row = __tr[trkey];
					var  rowId = _row.rowId;
					__rowsHtml += (JSONTOHTML.transform(_row, _tbody.tr));		
					var trfooterdata = {
						"colspan": _tbody.headallobj.length,
						"children": _tbody.footer,
						"rowId":rowId,
						"rowData":data.get(rowId),
						
						};
					//将行值传给按钮行，用于判断按钮是否需要显示
					__rowsHtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
				}
				data = null;
				
				var end = new Date().getTime();

				return __rowsHtml;
			}catch(e)
			{
				alert("数据解析异常，请联系管理员");
			}
		},
		keyname: function(obj) 
		{
			//列对象，只有一个key,那就是列名对应的字段
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id":function(obj)
			{
				return obj.rowId;
			},
			//行对象ID
			"data-itmeid":function(obj){
				return _tbody.idAttribute+"|"+obj.rowId;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {				
				//chuldren :列数组[]
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			"id":function(obj)
			{
				return "foot_"+obj.rowData[_tbody.idAttribute];
			},
			class: "TFOOTbutton",
			html: function(obj, index) {
				var rowData = obj.rowData,
					auth = obj.auth,
					tfootData = _tbody.idAttribute+"|"+obj.rowId,
					trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tfootData+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) 
				{
					var itmea = obj.children[akey];
					if(_utils.isHasButton(itmea[0],rowData))
					{
						var name = itmea[1];
						
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + name+ '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
	
		td: {
			tag: "td",
			class: function(obj, index) {

				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				if("basicInfo" == keynames)
				{
					classname = "td_" + "TRAILER";
				}
				else
				{
					classname = "td_" + keynames;
				}
				
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "inherit";
				}
				return valignsing;
			},
			html: function(obj, index) {

				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);

				if (keynames != "" && typeof keynames != "undefined")
				{
					if(typeof keynames == "string")
					{
						if (typeof cellTemplate[keynames] != "undefined" && cellTemplate[keynames] != "") 
						{
							retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
						}
					}
					else if(typeof keynames == "object")
					{
						retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
					}

				}
				return retuhtml;
			}
		},
		
		updateOneRow:function(parentId,rowData)
		{
			var __td = [];
			for (var headkey in this.headallobj) 
			{
				var column = this.headallobj[headkey];
				var columnFiled = column.field;
				var columnData = eval("({" + columnFiled + ":rowData})");
				__td.push(columnData);
			}
			var trfooterdata = {
					"colspan": this.headallobj.length,
					"children": this.footer,
					"rowId":parentId,
					"rowData":rowData,
					
					};
			$("#foot_"+parentId).remove();
			$("#"+parentId).empty().html(JSONTOHTML.transform(__td,this.td)).after(JSONTOHTML.transform(trfooterdata,this.trfooter));
		}
	}

	var cellTemplate =
	{
		basicInfo:
		{
			tag: "fieldset",
			class: "inventory_legend",
			children: [
			
			{
				tag: 'legend',
				class: "td_legend",
				children:
				[
					{"tag":"span","class":"lightlink buttonallevnts","data-eventname":"taskDetails","html":"Task:${ID}"},
					
				]
			}, 
			{
				 tag: "div",
				 class: "Tractor_ulitme",
				 children: 
				 [
					{tag: "div",
					class: "div-table",
					children:
					[
						{tag: "div",
						class: "div-table-row",
						children:
						[
							{tag: "div",
							class: "div-table-cell left",
							children:
								[
									{"tag":"span","class":"namestyle","html":"Warehouse:"},
									{"tag":"span","class":"valstyle","html":"${WAREHOUSE_NAME}"}
								]
							},
							{tag: "div",
							class: function(obj,index){return DateUtil.getClass(obj.TITLE_NAME) + " div-table-cell";},	
							children:
								[
									{"tag":"span","class":"namestyle","html":"Title:"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatTitle(obj.TITLE_NAME);}}
								]
							},
						]},
						
						{tag: "div",
						class: "div-table-row",
						children:
						[
							{tag: "div",
							class: "div-table-cell left",
							children:
								[
									{"tag":"span","class":"namestyle","html":"Assigned User:"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatAssignedUser(obj.USER_NAME);}}
								]
							},
							
							{tag: "div",
							class: function(obj,index){return DateUtil.getClass(obj.STATUS) + " div-table-cell";},	
							children:
								[
									{"tag":"span","class":"namestyle","html":"Status:"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatStatus(obj.STATUS);}}
								]
							},
						]},

						{tag: "div",
						class: "div-table-row",
						children:
						[

							{tag: "div",
							class: "div-table-cell left",
							children:
								[
									{"tag":"span","class":"namestyle","html":"Created by:"},
									{"tag":"span","class":"valstyle","html":"${CREATED_BY_NAME}"}
								]
							},

							{tag: "div",
							class: "div-table-cell",
							children:
								[
									{"tag":"span","class":"namestyle","html":"Type:"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatTaskType(obj.TYPE);}}
									
								]
							},
						]},
						
					]}	
				 ]
			}
			]
		},
		ScheduleInfo: 
		{
				 tag: "fieldset",
				class: "inventory_legend",
				children: [
				
				{
					tag: 'legend',
					class: "td_legend",
					children:
					[
						{"tag":"span","class":"lightlink nolink","html":function(obj,index){return DateUtil.formatCycle(obj.IS_REPEAT);}}
						
					]
				}, 
				{
				 
				 tag: "div",
				 class: "Tractor_ulitme",
				 children:
				 [
				 	{tag: "div",
						class: "div-table",
						children:
						[
						 	{tag: "div",
							class: "div-table-row",
							children:
							[
						 		{tag: "div",
									class: "div-table-cell left",
									children:
									[
										{"tag":"span","class":"namestyle","html":"Start Date:"},
										{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.STARTS_ON);}}
									]
								},
								{tag:"div",
						 		class: function(obj,index){return DateUtil.getClass(obj.REPEAT_BY) + " div-table-cell";},	
						 		children:
						 		[
						 			{"tag":"span","class":"namestyle","html":"Repeats:"},
		 							{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatRepeatBy(obj.REPEAT_BY);}}
						 		]},
								
						 	]},
						 	{tag: "div",
							class: "div-table-row",
							children:
							[
						 		{tag:"div",
								class: function(obj,index){return DateUtil.getNegateClass(obj.IS_REPEAT) + " div-table-cell left";},	
								children:
						 		[
						 			{"tag":"span","class":"namestyle","html":"End Date:"},
		 							{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.getEndsOn(obj);}}
						 		]},
						 		{tag: "div",
						 		class: function(obj,index){return DateUtil.getClass(obj.IS_REPEAT) +" div-table-cell left";},	
						 		children:
						 		[
						 			{"tag":"span","class":"namestyle","html":"Ends:"},
		 							{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.getEndsOn(obj);}}
						 		]},
						 		{tag:"div",
						 		class: function(obj,index){return DateUtil.getClass(obj.REPEAT_EVERY) + " div-table-cell";},	
						 		children:
						 		[
						 			{"tag":"span","class":"namestyle","html":"Repeats every:"},
		 							{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatRepeatEvery(obj);}}
						 		]},
						 		
						 	]},
						 	{tag: "div",
							class: "div-table-row",
							children:
							[
						 		
						 		{tag:"div",
						 		class: function(obj,index){return DateUtil.getClass(obj.DURATION)+ " div-table-cell left";},	
						 		children:
						 		[
						 			{"tag":"span","class":"namestyle","html":"Duration:"},
		 							{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDuration(obj.DURATION);}}
						 		]},

						 		{tag:"div",
						 		class: function(obj,index){return DateUtil.getClass(obj.IS_REPEAT) +" div-table-cell";},	
						 		children:
						 		[
						 			{"tag":"span","class":"namestyle","html":"Cycle State:"},
		 							{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatState(obj.STATE);}}
						 		]},
						 	]}	
						 		
						]}
				 ]
			}
			]

		},
		
	}

	var _utils = 
	{
		handleEmpty:function(str,defaultValue)
		{
			if(str == "" || str == undefined || str.length == 0 || str == "null" || str == null)
			{
				return defaultValue;
			}
			else
			{
				return str;
			}
		},
		isHasButton:function(itmea,obj)
		{
			var res = true;
			if(itmea =="events.stop"){
				if(obj.STATE!=1){res = false;}
				if(obj.IS_REPEAT==false){res = false;}
			}
			else if(itmea =="events.edit"){res=obj.IS_NEW==1;}
			else if(itmea =="events.resume"){
				if(obj.STATE!=2){res = false;}
				if(obj.IS_REPEAT==false){res = false;}
			}
			else if(itmea =="events.reassign"){
				if(!obj.IS_REASSIGNABLE){res = false;}
			}
			else if(itmea =="events.delete"){
				res=obj.IS_NEW==1;
			}
			return res;
		},
		
		
		

	}
	
	return {
		tbody: _tbody,
		table: _table
	}

});