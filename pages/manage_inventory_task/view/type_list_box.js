"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, ImmyBox ) {
    
    return Backbone.View.extend({
      
     
      render:function(data){
        var typeImmydata = {
          renderTo: "#type_div",
          dataUrl: [{text:"All",value:"0"},{text:"Verify",value:"2"},{text:"Blind",value:"1"}], 
          inputId: "#search_type",
          selectedInputId: "#search_selected_type"
        };
        var TypeImmy = new ImmyBox(typeImmydata);
        TypeImmy.on("events.change",function(){});
        TypeImmy.render();
        
      },

    
      
    });

}); 