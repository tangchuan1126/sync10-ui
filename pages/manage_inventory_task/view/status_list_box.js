"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, ImmyBox ) {
    
    return Backbone.View.extend({
      
     
      render:function(data){
        var typeImmydata = {
          renderTo: "#status_div",
          dataUrl: [{text:"All",value:"0"},{text:"New",value:"1"},{text:"In-Progress",value:"2"},
          {text:"Done",value:"3"},{text:"Pending Approval",value:"4"},{text:"Reviewed",value:"5"}], 
          inputId: "#search_status",
          selectedInputId: "#search_selected_status"
        };
        var TypeImmy = new ImmyBox(typeImmydata);
        TypeImmy.on("events.change",function(){});
        TypeImmy.render();
        
      },

    
      
    });

}); 