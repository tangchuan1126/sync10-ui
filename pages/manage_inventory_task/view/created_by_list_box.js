"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/creator_model",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, cModels, ImmyBox ) {
    
    return Backbone.View.extend({

      collection: new cModels.UserCollection(),
      
      render:function(data){
        var dis = this;
       
        dis.collection.fetch({dataType: "json",async: false});
        
        var userImmydata = {
          renderTo: "#created_by_div",
          dataUrl: dis.collection.sort(),
          placeHolder:"Any", 
          inputId: "#search_created_by",
          selectedInputId: "#search_selected_created_by"
        };

        var userImmy = new ImmyBox(userImmydata);
        userImmy.on("events.change",function(){});
        userImmy.render();
        
        
        
      },

    
      
    });

}); 