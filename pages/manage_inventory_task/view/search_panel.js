"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/storage_model",
  "./title_list_box",
  "./user_list_box",
  "./type_list_box",
  "./repeat_list_box",
  "./created_by_list_box",
  "./status_list_box",
  "../js/ImmyboxControl",
  "jqueryui/datepicker",
  "../componentListMain",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, storageModels, titleView, userView, typeView,repeatView,createdByView,statusView, ImmyBox , datepicker, component) {
    
    return Backbone.View.extend({
      el:"#search_box",
      template:templates.search_panel,
      resultView:null,

      initialize:function(data){
        var dis = this;
        dis.resultView = data.resView;
      },
      render:function(){
        var dis = this;
        var html=dis.template;
        dis.$el.html(html);
        
        var storageCollection= new storageModels.StorageImmyCollection();
        
        var storageImmydata = {
          renderTo: "#warehouse_div",
          dataUrl: storageCollection,
          placeHolder:"Any", 
          inputId: "#search_warehouse",
          selectedInputId: "#search_selected_warehouse"
        };

        var StorageImmy = new ImmyBox(storageImmydata);
        StorageImmy.on("events.change",function(){dis.pull_titles();});
        StorageImmy.render();

        var _titleView = new titleView();
        _titleView.render({warehouse_id: 0});
        
        var _userView = new userView();
        _userView.render({warehouse_id: 0});
        
        var _createdByView = new createdByView();
        _createdByView.render();

        var _typeView = new typeView();
        _typeView.render();

        var _repeatView = new repeatView();
        _repeatView.render();

        var _statusView = new statusView();
        _statusView.render();
        

        $("#search_start_date").datepicker({ dateFormat: "yy-mm-dd",onSelect: function (date) {
            var dt2 = $('#search_end_date');
            var minDate = $(this).datepicker('getDate');
            dt2.val("");
            dt2.datepicker('option', 'minDate', minDate);
            
        }});
       
         $("#search_end_date").datepicker({ dateFormat: "yy-mm-dd"});
         $("#search_task_btn").click(function(){dis.search();})
      },
      search:function(){
        var dis = this;
        var start_date = $("#search_start_date").val();
        var end_date = $("#search_end_date").val();
        var warehouse = $("#search_selected_warehouse").val();
        var title = $("#search_selected_title").val();
        var user = $("#search_selected_user").val();
        var created_by = $("#search_selected_created_by").val();
        var type = $("#search_selected_type").val();
        var repeat=$("#search_selected_repeat").val();
        var status = $("#search_selected_status").val();
        var queryCondition = {"warehouse_id":warehouse,"title_id":title, "created_by":created_by, "user_id":user,
           "type":type, "repeating":repeat, "status":status, "start_date":start_date, "end_date":end_date}
        
        //commenting this code to use the same compoundList with different options
        /*dis.resultView.undelegateEvents();
        dis.resultView.$el.html("");
        var queryOptions = {el: "#tasks_list", url:config.searchTasks,queryCondition:queryCondition};
         var rView  = new component(queryOptions);
         rView.render();
         dis.resultView = rView;*/
        dis.resultView.setQuery(queryCondition);
         
      },
      
      pull_titles:function(){
        
        var selected_warehouse = $("#search_selected_warehouse").val();
        
        var _titleView = new titleView();
        _titleView.render({warehouse_id: selected_warehouse});
        
        var _userView = new userView();
        _userView.render({warehouse_id: selected_warehouse});
        
      }
      
    });

}); 