"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, ImmyBox ) {
    
    return Backbone.View.extend({
      
     
      render:function(data){
        var typeImmydata = {
          renderTo: "#repeat_div",
          dataUrl: [{text:"All",value:"0"},{text:"Repeating",value:"1"},{text:"Non-Repeating",value:"2"}], 
          inputId: "#search_repeat",
          selectedInputId: "#search_selected_repeat"
        };
        var TypeImmy = new ImmyBox(typeImmydata);
        TypeImmy.on("events.change",function(){});
        TypeImmy.render();
        
      },

    
      
    });

}); 