"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/user_model",
  "art_Dialog/dialog-plus",
  "../jsontohtml_templates",
  "bootstrap",
  "handlebars_ext"

], function( $,config, Backbone, Handlebars, templates, userModels, Dialog, Template ) {
    
    return Backbone.View.extend({
      el:"#reassign_div",
      template:templates.reassign_view,
      collection: new userModels.UserCollection(),
      
      render:function(data){
        var dis = this;
        var assignedUser = data.assignedUser;
        dis.collection.url = config.getUsersOfWarehouseJSON+data.warehouseId;
        dis.collection.fetch({dataType: "json",async: false});
        var html=dis.template({users:dis.collection.sort().toJSON(), assignedUser:assignedUser});
        //dis.$el.html(html);
        var dialog = new Dialog({
              content: html,
              lock:true,
              width: 750,
              height: 300,
              title:"Reassign Task",
              ok: function (event) {
                dis.reassign_user(data.taskId,dialog,data.list);
                
            },
              cancelVal: 'No',
              cancel: function(){
                dialog.close();
                
              }
          }).showModal();
        $(".employee_box #"+assignedUser).parent().addClass("selected_employee");
        $(".employee_box").click(function(evt){
           $(".employee_box").each(function(i,item){
                $(item).removeClass("selected_employee");
           });
           $(this).addClass("selected_employee");
        });        
        
      },
      reassign_user:function(taskId,dialog,list) {
        var dis = this;
        var user_id = 0;
        $.each($(".selected_employee .employee_name"), function(i,item){
          user_id = $(item).attr("id");
          
        });
         $.ajax({
                  url:  config.reAssignTaskURL+taskId+"&user_id="+user_id,
                  type: 'put',
                  timeout: 60000,
                  cache:false,
                  dataType: 'json',
                  async:true,
                  
                  error: function(jqXHR, textStatus, errorThrown) {
                    showMessage("Error while reassigning task.","error");
                    dialog.close();
                  },
                  success: function(data){
                    showMessage("Task has been reassigned.","succeed");
                    dis.renderOneRow(list,taskId);
                    dialog.close();
                  }
                });
      },

      renderOneRow:function(olist,taskId)
      {
        $.getJSON(config.getSingleTaskDetail+taskId, {},
          function(json, textStatus) 
          {
            if(textStatus == "success")
            {
              olist.upSubitmeData({
              upitme:[{Root:"",itmeid:taskId,itmekey:"ID"}],
                DATA:json
                });
            Template.tbody.updateOneRow(taskId,json);
            }
        });
        
      }

    
      
    });

}); 