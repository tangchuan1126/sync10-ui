"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/title_model",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, titleModels, ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#title_list_view",
      template:templates.title_list_view,
      collection: new titleModels.TitleImmyCollection(),
      
      render:function(data){
        var dis = this;
        var html=dis.template;
        dis.$el.html(html);
        
        dis.collection.url = config.getAllTitlesJSON+ "?ps_id="+data.warehouse_id;
        dis.collection.fetch({dataType: "json",async: false});
        
        var titleImmydata = {
          renderTo: "#title_div",
          dataUrl: dis.collection,
          placeHolder:"All", 
          inputId: "#search_title",
          selectedInputId: "#search_selected_title"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){});
        titleImmy.render();
        
        
        
      },

    
      
    });

}); 