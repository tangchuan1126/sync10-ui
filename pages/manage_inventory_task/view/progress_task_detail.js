"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/progress_task_detail_model",
  "../Differences/view/location_differences_view",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, taskModels, differencesView ) {
    
    return Backbone.View.extend({
      el:"#progress_task",
      template:templates.progress_task,
      collection: null,
      taskId:0,
      
      render:function(data){
        var dis = this;
        dis.$el.html("");       
        var task = new taskModels.TaskModel();
        if(dis.taskId==0){
          dis.taskId = data.taskId;
        }
        
        task.url = config.getTaskProgress+dis.taskId;
        task.fetch({dataType: "json",async: false});
        task.formatFields();
        var repeating = task.get("IS_REPEAT")=="Yes"? true : null;
        var html=dis.template({task:task.toJSON(), repeating:repeating});
        
        dis.$el.html(html);   
        if(repeating){
          if(task.get("INSTANCES") && task.get("INSTANCES").length > 0){
            var ht = templates.instances_progress_view({instances:task.get("INSTANCES")});
            $("#instances_progress").append(ht);
            $.each(task.get("INSTANCES"), function(index,item){
              console.log(item.ID)
              var h = templates.area_progress_view({repeating:repeating,instanceId:item.ID,areas:item.AREAS});
              $("#instances_area_progress").append(h);
            });
          }else{
            // no instances to show
          }    

          $(".instance_link").click(function(e){dis.instanceClicked(e);});
          $(".instances_areas_table_back").click(function(e){dis.instancesAreaTableBack(e);});
          
        }else{
          var areas;
          var insId;
          if(task.get("INSTANCES") && task.get("INSTANCES").length > 0){
            areas = task.get("INSTANCES")[0].AREAS;
            insId = task.get("INSTANCES")[0].ID;
          }else{
            areas = task.get("AREAS");
            insId = "0";
          }
          
              var h = templates.area_progress_view({repeating:repeating,instanceId:insId,areas:areas});
              $("#instances_area_progress").append(h);
              $(".instance_area_table").show();
              $("#instances_area_progress").show();
          
        }
        $.each($(".instance_area_table"), function(index,item){
          var insId = item.dataset.instanceid;
          $.each($(item).find(".area_view_detail"), function(ind,data){
              var areaId= data.dataset.areaid;
              $(data).click(function(e){dis.view_detail(insId,areaId)});
          });
        });
        //console.log("link",$(".area_view_detail"))
        $("#task_progress_back").click(function(e){dis.backToManage(e);});
        $("#progress_task").show();
        $("#task_order_content").hide();
      },

      view_detail:function (insId, areaId) {
        var dis = this;
        var diffView = new differencesView().render({parent:dis,instanceId:insId,areaId:areaId});
        
      },

      instanceClicked:function (e) {
        var instanceId = e.currentTarget.dataset.value;
        $("#instances_progress").hide();
        $("#instances_area_progress").show();
        $("#"+instanceId+"_area_table").show();
      },

      instancesAreaTableBack:function(e){
        
        $(".instance_area_table").hide();
        $("#instances_progress").show();
        
      },
      backToManage:function(e){
        
        $("#progress_task").hide();
        $("#task_order_content").show();
        
      }
      
    });

}); 