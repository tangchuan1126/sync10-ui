(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"instance_area_table\" id=\""
    + escapeExpression(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"instanceId","hash":{},"data":data}) : helper)))
    + "_area_table\" data-instanceId=\""
    + escapeExpression(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"instanceId","hash":{},"data":data}) : helper)))
    + "\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.repeating : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	<table  class=\"table table-striped\">\r\n		<thead>\r\n			<th>Area Name</th>\r\n			<th>Status</th>\r\n			<th>Progress</th>\r\n			<th></th>\r\n		</thead>\r\n		<tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.areas : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</tbody>\r\n	</table>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "		<div class=\"instances_areas_table_back_div\">\r\n			<a href=\"#\" class=\"instances_areas_table_back btn btn-default btn-sm\">Back to Instances</a>\r\n		</div>\r\n";
  },"4":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "		\r\n			<tr>\r\n				<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\r\n				<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\r\n				\r\n					<td>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.SHOW_PROGRESS : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "					</td>\r\n				\r\n				<td>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.SHOW_DETAIL : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "				</td>\r\n			</tr>\r\n		\r\n";
},"5":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "						<div class=\"custom_progress_box\">\r\n			             <div class=\"custom_progress\" style=\"background-position:"
    + escapeExpression(lambda((depth0 != null ? depth0.PERCENT_DONE : depth0), depth0))
    + "px 0px;\">\r\n			                "
    + escapeExpression(lambda((depth0 != null ? depth0.DONECOUNT : depth0), depth0))
    + " / "
    + escapeExpression(lambda((depth0 != null ? depth0.TOTALCOUNT : depth0), depth0))
    + "\r\n			             </div>\r\n			           </div>  \r\n";
},"7":function(depth0,helpers,partials,data) {
  return "			        --\r\n";
  },"9":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "					<a href=\"#\" class=\"area_view_detail btn btn-default btn-sm\" data-areaId=\""
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_ID : depth0), depth0))
    + "\" >View Detail</a>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.areas : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['create_task_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div  id=\"buttons-div\">\r\n	<a href=\"#\" id=\"create\" class=\"btn btn-warning\">\r\n	<span class=\"glyphicon glyphicon-plus\"></span> Create New Task</a>\r\n</div>";
  },"useData":true});
templates['instances_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n<table id=\"instances_table\" class=\"table table-striped\">\r\n	<thead>\r\n		<th>Instance</th>\r\n		<th>Start Date</th>\r\n		<th>End Date</th>\r\n		<th>Status</th>\r\n		<th></th>\r\n	</thead>\r\n	<tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.instances : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</tbody>\r\n</table>\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	\r\n		<tr>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "</td>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.START_DATE : depth0), depth0))
    + "</td>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "</td>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\r\n			<td><a href=\"#\" class=\"instance_link btn btn-default btn-sm\" data-value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">View Detail</a></td>\r\n		</tr>\r\n	\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.instances : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['progress_task'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  						<td class=\"labele\">Title</td>\r\n  						<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</td>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  						<td class=\"labele\">End Date</td>\r\n  						<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1), depth0))
    + "</td>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  						<td class=\"labele\">End After</td>\r\n  						<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_OCCURRENCE : stack1), depth0))
    + " Occurrences</td>\r\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  				<tr>\r\n  					<td class=\"labele\">Repeat By</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_BY : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Repeat Every</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_EVERY : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Duration</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.DURATION : stack1), depth0))
    + "</td>\r\n  					\r\n  				</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"panel panel-default\">\r\n  	<div id=\"progress_task_header\" class=\"panel-heading\">\r\n      <div class=\"left\">Task Progress</div>\r\n      <div id=\"progress-back-buttons-div\"><a href=\"#\" id=\"task_progress_back\" class=\"btn btn-warning\">Back</a></div>\r\n      <div class=\"clear\"></div>\r\n  	</div>\r\n  	<div class=\"panel-body\">\r\n  			<table class=\"info_table\">\r\n  				<tr>\r\n  					<td class=\"labele\">Warehouse</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.WAREHOUSE_NAME : stack1), depth0))
    + "</td>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLE_NAME : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "  					<td class=\"labele\">Assignee</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.USER_NAME : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Created By</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.CREATED_BY_NAME : stack1), depth0))
    + "</td>\r\n  				</tr>\r\n  				<tr>\r\n  					<td class=\"labele\">Repeating</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.IS_REPEAT : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Type</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Start Date</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STARTS_ON : stack1), depth0))
    + "</td>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.program(5, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "  				</tr>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.repeating : depth0), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "  			</table>	\r\n			\r\n			<div id=\"second_level_progress\">\r\n				<div id=\"instances_progress\"></div>\r\n				<div id=\"instances_area_progress\"></div>\r\n				<div id=\"location_differences_container\"></div>\r\n			</div>\r\n\r\n	</div>\r\n</div>";
},"useData":true});
templates['reassign_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div >\r\n	<div class=\"group_box_div\">\r\n		<div class=\"employee_box group_box \" >\r\n			<div class=\"employee_name\" id=\"0\">Anyone in the Group</div>\r\n		</div>\r\n		<div class=\"clear\"></div>\r\n	</div>\r\n	<div class=\"employee_box_div\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.users : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</div>\r\n	<div class=\"clear\"></div>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "		<div class=\"employee_box\" >\r\n			<div class=\"employee_name\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</div>\r\n		</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['search_panel'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"div-table\">\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"warehouse_div\">\r\n				<div class=\"input-group-addon\">Warehouse</div><input type=\"hidden\" id=\"search_selected_warehouse\"/>\r\n				<input type=\"text\" id=\"search_warehouse\" class=\"form-control min-200\"/>\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"title_list_view\"></div>\r\n		</div>\r\n		\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"user_list_view\"></div>\r\n		</div>\r\n		\r\n	</div>\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"created_by_div\">\r\n				<div class=\"input-group-addon\">Created By</div><input type=\"hidden\" id=\"search_selected_created_by\"/>\r\n				<input type=\"text\" id=\"search_created_by\" class=\"form-control\"/>\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"type_div\">\r\n				<div class=\"input-group-addon\">Type</div><input type=\"hidden\" id=\"search_selected_type\"/>\r\n				<input type=\"text\" id=\"search_type\" class=\"form-control\"/>\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"repeat_div\">\r\n				<div class=\"input-group-addon\">Repeating</div><input type=\"hidden\" id=\"search_selected_repeat\"/>\r\n				<input type=\"text\" id=\"search_repeat\" class=\"form-control\"/>\r\n			</div>\r\n		</div>\r\n		\r\n	</div>\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"status_div\">\r\n				<div class=\"input-group-addon\">Status</div><input type=\"hidden\" id=\"search_selected_status\"/>\r\n				<input type=\"text\" id=\"search_status\" class=\"form-control\"/>\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" >\r\n				<div class=\"input-group-addon\">Start Date</div>\r\n				<input type=\"text\" id=\"search_start_date\" />\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" >\r\n				<div class=\"input-group-addon\">End Date</div>\r\n				<input type=\"text\" id=\"search_end_date\" />\r\n			</div>\r\n		</div>\r\n\r\n		<div class=\"div-table-cell\" id=\"search_btn_div\">\r\n			<a href=\"#\" id=\"search_task_btn\" class=\"btn btn-info\"><span class=\"glyphicon glyphicon-search\"></span> Search</a>\r\n		</div>\r\n	</div>	\r\n	\r\n</div>";
  },"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "test";
  },"useData":true});
templates['title_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "\r\n<div class=\"input-group-addon\" id=\"title_div\">Title</div><input type=\"hidden\" id=\"search_selected_title\"/>\r\n<input type=\"text\" id=\"search_title\" class=\"form-control\"/>\r\n\r\n	";
  },"useData":true});
templates['user_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"input-group-addon\" id=\"user_div\">Assignee</div><input type=\"hidden\" id=\"search_selected_user\"/>\r\n<input type=\"text\" id=\"search_user\" class=\"form-control\"/>\r\n";
  },"useData":true});
})();