define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"instance_area_table\" id=\""
    + alias3(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"instanceId","hash":{},"data":data}) : helper)))
    + "_area_table\" data-instanceId=\""
    + alias3(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"instanceId","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.repeating : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	<table  class=\"table table-striped\">\n		<thead>\n			<th>Area Name</th>\n			<th>Status</th>\n			<th>Progress</th>\n			<th></th>\n		</thead>\n		<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.areas : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</tbody>\n	</table>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    return "		<div class=\"instances_areas_table_back_div\">\n			<a href=\"#\" class=\"instances_areas_table_back btn btn-default btn-sm\">Back to Instances</a>\n		</div>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "		\n			<tr>\n				<td>"
    + alias2(alias1((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\n				<td>"
    + alias2(alias1((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\n				\n					<td>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SHOW_PROGRESS : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "					</td>\n				\n				<td>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SHOW_DETAIL : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</td>\n			</tr>\n		\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<div class=\"custom_progress_box\">\n			             <div class=\"custom_progress\" style=\"background-position:"
    + alias2(alias1((depth0 != null ? depth0.PERCENT_DONE : depth0), depth0))
    + "px 0px;\">\n			                "
    + alias2(alias1((depth0 != null ? depth0.DONECOUNT : depth0), depth0))
    + " / "
    + alias2(alias1((depth0 != null ? depth0.TOTALCOUNT : depth0), depth0))
    + "\n			             </div>\n			           </div>  \n";
},"7":function(depth0,helpers,partials,data) {
    return "			        --\n";
},"9":function(depth0,helpers,partials,data) {
    return "					<a href=\"#\" class=\"area_view_detail btn btn-default btn-sm\" data-areaId=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.AREA_ID : depth0), depth0))
    + "\" >View Detail</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.areas : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['create_task_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  id=\"buttons-div\">\n	<a href=\"#\" id=\"create\" class=\"btn btn-warning\">\n	<span class=\"glyphicon glyphicon-plus\"></span> Create New Task</a>\n</div>";
},"useData":true});
templates['instances_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<table id=\"instances_table\" class=\"table table-striped\">\n	<thead>\n		<th>Instance</th>\n		<th>Start Date</th>\n		<th>End Date</th>\n		<th>Status</th>\n		<th></th>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.instances : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\n</table>\n\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	\n		<tr>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "</td>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.START_DATE : depth0), depth0))
    + "</td>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "</td>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\n			<td><a href=\"#\" class=\"instance_link btn btn-default btn-sm\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">View Detail</a></td>\n		</tr>\n	\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.instances : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['progress_task'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  						<td class=\"labele\">Title</td>\n  						<td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</td>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return "  						<td class=\"labele\">End Date</td>\n  						<td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1), depth0))
    + "</td>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "  						<td class=\"labele\">End After</td>\n  						<td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_OCCURRENCE : stack1), depth0))
    + " Occurrences</td>\n";
},"7":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "  				<tr>\n  					<td class=\"labele\">Repeat By</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_BY : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Repeat Every</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_EVERY : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Duration</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.DURATION : stack1), depth0))
    + "</td>\n  					\n  				</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel panel-default\">\n  	<div id=\"progress_task_header\" class=\"panel-heading\">\n      <div class=\"left\">Task Progress</div>\n      <div id=\"progress-back-buttons-div\"><a href=\"#\" id=\"task_progress_back\" class=\"btn btn-warning\">Back</a></div>\n      <div class=\"clear\"></div>\n  	</div>\n  	<div class=\"panel-body\">\n  			<table class=\"info_table\">\n  				<tr>\n  					<td class=\"labele\">Warehouse</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.WAREHOUSE_NAME : stack1), depth0))
    + "</td>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLE_NAME : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  					<td class=\"labele\">Assignee</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.USER_NAME : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Created By</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.CREATED_BY_NAME : stack1), depth0))
    + "</td>\n  				</tr>\n  				<tr>\n  					<td class=\"labele\">Repeating</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.IS_REPEAT : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Type</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Start Date</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STARTS_ON : stack1), depth0))
    + "</td>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "  				</tr>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.repeating : depth0),{"name":"if","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  			</table>	\n			\n			<div id=\"second_level_progress\">\n				<div id=\"instances_progress\"></div>\n				<div id=\"instances_area_progress\"></div>\n				<div id=\"location_differences_container\"></div>\n			</div>\n\n	</div>\n</div>";
},"useData":true});
templates['reassign_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div >\n	<div class=\"group_box_div\">\n		<div class=\"employee_box group_box \" >\n			<div class=\"employee_name\" id=\"0\">Anyone in the Group</div>\n		</div>\n		<div class=\"clear\"></div>\n	</div>\n	<div class=\"employee_box_div\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n	<div class=\"clear\"></div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "		<div class=\"employee_box\" >\n			<div class=\"employee_name\" id=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</div>\n		</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['search_panel'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"div-table\">\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"warehouse_div\">\n				<div class=\"input-group-addon\">Warehouse</div><input type=\"hidden\" id=\"search_selected_warehouse\"/>\n				<input type=\"text\" id=\"search_warehouse\" class=\"form-control min-200\"/>\n			</div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"title_list_view\"></div>\n		</div>\n		\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"user_list_view\"></div>\n		</div>\n		\n	</div>\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"created_by_div\">\n				<div class=\"input-group-addon\">Created By</div><input type=\"hidden\" id=\"search_selected_created_by\"/>\n				<input type=\"text\" id=\"search_created_by\" class=\"form-control\"/>\n			</div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"type_div\">\n				<div class=\"input-group-addon\">Type</div><input type=\"hidden\" id=\"search_selected_type\"/>\n				<input type=\"text\" id=\"search_type\" class=\"form-control\"/>\n			</div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"repeat_div\">\n				<div class=\"input-group-addon\">Repeating</div><input type=\"hidden\" id=\"search_selected_repeat\"/>\n				<input type=\"text\" id=\"search_repeat\" class=\"form-control\"/>\n			</div>\n		</div>\n		\n	</div>\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"status_div\">\n				<div class=\"input-group-addon\">Status</div><input type=\"hidden\" id=\"search_selected_status\"/>\n				<input type=\"text\" id=\"search_status\" class=\"form-control\"/>\n			</div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" >\n				<div class=\"input-group-addon\">Start Date</div>\n				<input type=\"text\" id=\"search_start_date\" />\n			</div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" >\n				<div class=\"input-group-addon\">End Date</div>\n				<input type=\"text\" id=\"search_end_date\" />\n			</div>\n		</div>\n\n		<div class=\"div-table-cell\" id=\"search_btn_div\">\n			<a href=\"#\" id=\"search_task_btn\" class=\"btn btn-info\"><span class=\"glyphicon glyphicon-search\"></span> Search</a>\n		</div>\n	</div>	\n	\n</div>";
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "test";
},"useData":true});
templates['title_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "\n<div class=\"input-group-addon\" id=\"title_div\">Title</div><input type=\"hidden\" id=\"search_selected_title\"/>\n<input type=\"text\" id=\"search_title\" class=\"form-control\"/>\n\n	";
},"useData":true});
templates['user_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group-addon\" id=\"user_div\">Assignee</div><input type=\"hidden\" id=\"search_selected_user\"/>\n<input type=\"text\" id=\"search_user\" class=\"form-control\"/>\n";
},"useData":true});
return templates;
});