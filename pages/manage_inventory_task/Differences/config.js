define({
    
    locationDifferencesForArea: "/Sync10/_inventoryControl/search/scannedAreaLocations",
    containerDifferencesForLocation:"/Sync10/_inventoryControl/cycleCount/search/locationDifferences",
    processDifference:"/Sync10/_inventoryControl/update/difference"
});
