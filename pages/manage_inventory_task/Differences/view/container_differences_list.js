"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/container_difference_model.js",
  "./count_differences.js",
  "art_Dialog/dialog",
  "bootstrap_tab"
], function( config, $, Backbone, Handlebars, templates , HandleBarsExt, difference_models, countDifferencesView, dialog) {

    return  Backbone.View.extend({
      template: templates.container_differences_list,
      el:"#container_differences",
      saa_id:"",


      initialize:function(parentView){
          var dis = this;

          dis.parentView = parentView;

      },
      render:function(data){
        var dis = this;
        var loading = dialog();
        loading.showModal();
        dis.collection= new difference_models.DifferenceCollection(data.locationId);
        dis.saa_id = data.areaId;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html =dis.template({differences: dis.collection.toJSON()});
        $("#"+data.container_id).html("");
        $("#"+data.container_id).html(html);

        $("#containerTabs a").click(function (evt) {
          evt.preventDefault();
          var sac_id, container,li;
          if($(evt.target).attr('href')){
            sac_id = $(evt.target).attr("name");
            li = $(evt.target).parent();
            container = $(evt.target).find($("span")).attr("name");
          }else{
            sac_id = $(evt.target).parent().attr("name");
            container = $(evt.target).attr("name");
            li = $(evt.target).parent().parent();
          }
          
          var con_id = $(li).find($('input[name="con_id"]')).val();
          var ps_id = $(li).find($('input[name="ps_id"]')).val();
          var slc_id = $(li).find($('input[name="slc_id"]')).val();
          var pending = $(li).find($('input[name="pending"]')).val();

          if(pending=="true"){
            pending = true;
          }else{
            pending = false;
          }
          var loading = dialog();
          loading.showModal();
         
          new countDifferencesView(dis).render(container,sac_id,con_id,ps_id,slc_id,pending,dis.saa_id);
          loading.close().remove();
        });



        var selectedTab = -1;
        var selectedContent = -1;
        var pending = false;
        $.each(dis.collection.toJSON(), function(i, item) {
         
          if(item.APPROVE_STATUS==2){
            $("#"+item.SAC_ID+" > a > span").addClass("approved");
            $("#"+item.SAC_ID+" > a").addClass("approved_container");
            
          }else{
            pending = true;
          }

          if(selectedTab==-1 && item.APPROVE_STATUS==1){
            selectedTab = item.SAC_ID;
            selectedContent = item.CONTAINER;
          }
        });
        
        if(selectedTab==-1  &&  dis.collection.length>0){ // incase all are approved, select first
          var selected = dis.collection.models[0];
          selectedTab = selected.attributes["SAC_ID"];
          selectedContent = selected.attributes["CONTAINER"];
         

        }
          
          $("#"+selectedTab).addClass("active");
          $("#"+selectedContent).addClass("active");
          var con_id = $("#"+selectedTab).find($('input[name="con_id"]')).val();
          var ps_id = $("#"+selectedTab).find($('input[name="ps_id"]')).val();
          var slc_id = $("#"+selectedTab).find($('input[name="slc_id"]')).val();

          
          new countDifferencesView(dis).render(selectedContent,selectedTab,con_id,ps_id,slc_id,pending,dis.saa_id);
        
        loading.close().remove();
     },
      
  });

}); 

