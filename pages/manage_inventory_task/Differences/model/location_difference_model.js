"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var LocationModel = Backbone.Model.extend({
          idAttribute: "LOCATION_ID",
          parse:function(data){
            this.set("LOCATION_ID",data.LOCATION_ID);
            this.set("LOCATION_NAME",data.LOCATION_NAME);
            this.set("STATUS",data.STATUS)
            switch(data.STATUS){
              case 1:
                  this.set("STATUS_STRING","new");
                  break;
              case 2:
                  this.set("STATUS_STRING","in-progress");
                  break;
              case 3:
                  this.set("STATUS_STRING","done");
                  break;
              case 4:
                  this.set("STATUS_STRING","pending review");
                  break;
              case 5:
                  this.set("STATUS_STRING","reviewed");
                  break;
            }
          }
       });
       var LocationCollection =  Backbone.Collection.extend({
           model: LocationModel,
           selected:"",
           parse:function(data){
            return data;
           }
       
       });
       
      var DifferenceModel =  Backbone.Model.extend({
           url: config.locationDifferencesForArea,
           idAttribute: "AREA_ID",

           initialize: function(data){
              this.url = this.url+"?task_instance_id="+data.instanceId+"&area_id="+data.areaId;
              
          },
          parse:function(data){
            this.set("AREA_ID",data.AREA_ID);
            this.set("INSTANCE_ID",data.TASK_INSTANCE_ID);
            this.set("locations", new LocationCollection(data.LOCATIONS,{parse:true}));

          }

       });

      return {
	      DifferenceModel:DifferenceModel,
	    };




}); //page_init