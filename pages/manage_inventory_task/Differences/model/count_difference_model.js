"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      
      var SystemContainerTree = Backbone.Model.extend({
          idAttribute: "CONTAINER_ID",
          
          
       });
      var PhysicalContainerTree = Backbone.Model.extend({
          idAttribute: "CONTAINER_ID",
          
       });
      var ProductModel = Backbone.Model.extend({
          idAttribute: "PC_ID",
          
       });
      var ProductCollection =  Backbone.Collection.extend({
           model: ProductModel,

      });
      var ContainerDifferenceModel = Backbone.Model.extend({
          idAttribute: "ID",
          parse:function(data){
              if(data.SYSTEM_TREE){
                this.set("systemContainerTree",new SystemContainerTree(data.SYSTEM_TREE,{parse:true}));
                var col = new ProductCollection();
                col = this.countProducts(data.SYSTEM_TREE,col);
                this.set("systemProducts",col);
              }
              if(data.PHYSICAL_TREE){
                 this.set("physicalContainerTree" , new PhysicalContainerTree(data.PHYSICAL_TREE,{parse:true})); 
                 var col = new ProductCollection();
                  col = this.countProducts(data.PHYSICAL_TREE,col);   
                  this.set("physicalProducts",col) 
              }
              this.set("STATUS",data.STATUS);
              this.set("USERNAME",data.USER_NAME);
              this.set("ID",data.ID);
              this.set("COMMENTS",data.COMMENTS);
              this.set("PROCESSED_BY_NAME",data.PROCESSED_BY_NAME);
              if(data.STATUS == 3){
                this.set("PROCESSED_TITLE","Rejected By:");
              }
              if(data.STATUS == 2){
                this.set("PROCESSED_TITLE","Approved By:");
              }
          },

          countProducts:function(tree,col){
            var dis = this;
            if(tree.PC_ID){
              if(col.get(tree.PC_ID)){
                var prod = col.remove(tree.PC_ID);
                prod.set("QUANTITY",prod.get("QUANTITY")+tree.QUANTITY);
                col.add(prod);
              }else{
                var prod = new ProductModel();
                prod.set("PC_ID",tree.PC_ID);
                prod.set("P_NAME",tree.P_NAME);
                prod.set("QUANTITY",tree.QUANTITY);
                col.add(prod);
              }
              
            }else{
              if(tree.CHILDREN){
                $.each(tree.CHILDREN, function(index, item){
                  dis.countProducts(item, col);
                });
              }
            }
            return col;
          },

          createDifference:function(){
            var col = new ProductCollection();
            var sysProds = this.get("systemProducts");
            var phyProds = this.get("physicalProducts");
            if(sysProds && phyProds){
              $.each(sysProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("P_NAME",item.get("P_NAME"));
                p.set("systemCount",item.get("QUANTITY"));
                if(phyProds.get(item.get("PC_ID"))){
                  if(item.get("QUANTITY") == phyProds.get(item.get("PC_ID")).get("QUANTITY")){

                  }else{
                      p.set("physicalCount",phyProds.get(item.get("PC_ID")).get("QUANTITY"));
                      col.add(p);
                  }
                  phyProds.remove(item.get("PC_ID"))
                  
                }else{
                  p.set("physicalCount",0);
                  col.add(p);
                }
                
              });
              $.each(phyProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("P_NAME",item.get("P_NAME"));
                p.set("systemCount",0);
                p.set("physicalCount",item.get("QUANTITY"));
                col.add(p);
              });
            }else if(sysProds){
              $.each(sysProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("P_NAME",item.get("P_NAME"));
                p.set("physicalCount",0);
                p.set("systemCount",item.get("QUANTITY"));
                col.add(p);
              });
            }else if(phyProds){
              $.each(phyProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("P_NAME",item.get("P_NAME"));
                p.set("systemCount",0);
                p.set("physicalCount",item.get("QUANTITY"));
                col.add(p);
              });
            }else{
              return null;
            }
            
            return col;
          }
       });
      var ProductDifferenceModel = Backbone.Model.extend({
          idAttribute: "ID",

       });
      var ProductDifferenceCollection =  Backbone.Collection.extend({
           model: ProductDifferenceModel,
            
      });
       
       var ChildrenModel = Backbone.Model.extend({
          idAttribute: "ID",

       });
      var ChildrenCollection =  Backbone.Collection.extend({
           model: ChildrenModel,
      });
      var ContainerModel = Backbone.Model.extend({
          idAttribute: "ID",
          parse:function(data){
            
              this.set("containerDifference",new ContainerDifferenceModel(data.CONTAINER_DIFFERENCE,{parse:true}));
              this.set("productDifferences",new ProductDifferenceCollection(data.PRODUCT_DIFFERENCES,{parse:true}));
              this.set("CONTAINER_ID",data.CONTAINER_ID);
              this.set("CONTAINER",data.CONTAINER);
              
              var status =-1;
              if(data.CONTAINER_DIFFERENCE){
                status =data.CONTAINER_DIFFERENCE.STATUS;
              }
              if(status == -1 || data.PRODUCT_DIFFERENCES ){
                $.each(data.PRODUCT_DIFFERENCES,function(index,item){
                  
                  status = item.STATUS;
                });
              }
              this.set("STATUS",status);
          }
       });
      
      var DifferenceCollection =  Backbone.Collection.extend({
           model: ContainerModel,
           url: config.containerDifferencesForLocation,
           
           initialize: function(data){
              this.url = this.url+"?task_instance_id="+data.instanceId+"&location_id="+data.locationId;
          },
          
       });

      return {
       
        DifferenceCollection:DifferenceCollection,

      };




}); //page_init