(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['container_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "	<ul class=\"nav nav-tabs\" id=\"containerTabs\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " 	</ul>\r\n 	<div class=\"tab-content\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <li id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + "\">\r\n            <a href=\"#"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\" data-toggle=\"tab\" >\r\n                "
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\r\n                <span class=\"\" >&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n            </a>\r\n        </li>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "		<div class=\"tab-pane\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">\r\n           \r\n		</div>\r\n";
},"6":function(depth0,helpers,partials,data) {
  return "<div class=\"alert alert-info\" role=\"alert\">\r\n  \r\n  No Difference found.\r\n</div>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(6, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['container_tree_differences'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "               <span class=\"parent\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                    <span class=\"child product\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CHILDREN : stack1), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                        <span class=\"child\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.program(8, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"6":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                            <span class=\"child1 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"8":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"9":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                <span class=\"child1\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(10, data),"inverse":this.program(12, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"10":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                    <span class=\"child2 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"12":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"13":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                        <span class=\"child3\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(14, data),"inverse":this.program(16, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"14":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                            <span class=\"child3 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"16":function(depth0,helpers,partials,data) {
  return "                                            \r\n";
  },"18":function(depth0,helpers,partials,data) {
  return "               <span class=\"new\">New to system</span>\r\n";
  },"20":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "               <span class=\"parent\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1), {"name":"if","hash":{},"fn":this.program(21, data),"inverse":this.program(23, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"21":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                  <span class=\"child product\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\r\n";
},"23":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CHILDREN : stack1), {"name":"each","hash":{},"fn":this.program(24, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"24":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                        <span class=\"child\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(25, data),"inverse":this.program(27, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"25":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                          <span class=\"child1 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"27":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(28, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"28":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                              <span class=\"child1\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(29, data),"inverse":this.program(31, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"29":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <span class=\"child2 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"31":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(32, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"32":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                  <span class=\"child2\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(33, data),"inverse":this.program(35, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"33":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                      <span class=\"child3 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"35":function(depth0,helpers,partials,data) {
  return "                                      \r\n";
  },"37":function(depth0,helpers,partials,data) {
  return "              <span class=\"missing\">Missing now</span>\r\n";
  },"39":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing, buffer = "  <table class=\"table table-striped\" class=\"count_diff_table\" >\r\n        <thead>\r\n            <th>Product Name</th>\r\n            <th>System Count</th>\r\n            <th>Physical Count</th>\r\n            \r\n        </thead>\r\n        <tbody >\r\n";
  stack1 = ((helper = (helper = helpers.calculatedDiffs || (depth0 != null ? depth0.calculatedDiffs : depth0)) != null ? helper : helperMissing),(options={"name":"calculatedDiffs","hash":{},"fn":this.program(40, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.calculatedDiffs) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </tbody>\r\n  </table>      \r\n";
},"40":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    \r\n          <tr >\r\n            <td>"
    + escapeExpression(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td>"
    + escapeExpression(((helper = (helper = helpers.systemCount || (depth0 != null ? depth0.systemCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"systemCount","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td class=\"difference\">"
    + escapeExpression(((helper = (helper = helpers.physicalCount || (depth0 != null ? depth0.physicalCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"physicalCount","hash":{},"data":data}) : helper)))
    + "</td>\r\n                  \r\n          </tr>\r\n";
},"42":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div class=\"approval_div\">\r\n          <div class=\"comments_div\">\r\n          <input type=\"hidden\" name=\"diff_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.conDiffId || (depth0 != null ? depth0.conDiffId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"conDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n              <label>Comments</label>\r\n              <textarea  name=\"comments\"></textarea>\r\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n          </div>\r\n          <div class=\"approve_btn_div\">\r\n          <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\r\n            <span class=\"glyphicon glyphicon-ok\">\r\n            Approve\r\n          </a>\r\n          <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\r\n            <span class=\"glyphicon glyphicon-remove\">\r\n            Reject\r\n          </a> \r\n          </div>\r\n    </div>\r\n";
},"44":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  <div class=\"processed_div\">\r\n\r\n    <div class=\"username\">\r\n       <b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_TITLE : stack1), depth0))
    + "</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_BY_NAME : stack1), depth0))
    + "\r\n    </div>\r\n    <div class=\"username\">\r\n       <b>Comments:</b> "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.COMMENTS : stack1), depth0))
    + "\r\n    </div>\r\n  </div>  \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "\r\n<div class=\"count_container\">\r\n    <div id=\"container_count_dif_div\">\r\n        <div class=\"username\">\r\n           <b>Scanned By:</b> "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.USERNAME : stack1), depth0))
    + "\r\n        </div>\r\n        <div id=\"system_physical_div\">\r\n          <div class=\"system_div\">\r\n            <h5>System Containers</h5>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.systemTree : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(18, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </div>   \r\n          <div class=\"physical_div\">\r\n            <h5>Physical Containers</h5>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.physicalTree : depth0), {"name":"if","hash":{},"fn":this.program(20, data),"inverse":this.program(37, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </div>   \r\n          <div class=\"clear\"></div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n\r\n\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.calculatedDiffs : depth0), {"name":"if","hash":{},"fn":this.program(39, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(42, data),"inverse":this.program(44, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});
templates['count_differences'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, blockHelperMissing=helpers.blockHelperMissing, buffer = "<div class=\"count_container\">\r\n  <div class=\"username\">\r\n     <b>Scanned By:</b> "
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "\r\n  </div>\r\n  <table class=\"table table-striped\" class=\"count_diff_table\" >\r\n      <thead>\r\n          <th>Product Name</th>\r\n          <th>System Count</th>\r\n          <th>Physical Count</th>\r\n          \r\n      </thead>\r\n      <tbody >\r\n";
  stack1 = ((helper = (helper = helpers.productDifferences || (depth0 != null ? depth0.productDifferences : depth0)) != null ? helper : helperMissing),(options={"name":"productDifferences","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.productDifferences) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "      </tbody>\r\n  </table>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(4, data),"inverse":this.program(6, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "  \r\n        <tr >\r\n          <td>"
    + escapeExpression(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\r\n          <td>"
    + escapeExpression(((helper = (helper = helpers.SYSTEM_COUNT || (depth0 != null ? depth0.SYSTEM_COUNT : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"SYSTEM_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\r\n          <td class=\"difference\">"
    + escapeExpression(((helper = (helper = helpers.PHYSICAL_COUNT || (depth0 != null ? depth0.PHYSICAL_COUNT : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"PHYSICAL_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\r\n        </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div class=\"approval_div\">\r\n          <div class=\"comments_div\">\r\n              <input type=\"hidden\" name=\"diff_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.productDiffId || (depth0 != null ? depth0.productDiffId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"productDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n              <label>Comments</label>\r\n              <textarea  name=\"comments\"></textarea>\r\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n          </div>\r\n          <div class=\"approve_btn_div\">\r\n            <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\r\n              <span class=\"glyphicon glyphicon-ok\">"
    + escapeExpression(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ID","hash":{},"data":data}) : helper)))
    + "\r\n              Approve\r\n            </a>\r\n            <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\r\n              <span class=\"glyphicon glyphicon-remove\">\r\n              Reject\r\n            </a> \r\n          </div>\r\n    </div>\r\n";
},"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div class=\"processed_div\">\r\n\r\n      <div class=\"username\">\r\n         <b>"
    + escapeExpression(((helper = (helper = helpers.processed_by_title || (depth0 != null ? depth0.processed_by_title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"processed_by_title","hash":{},"data":data}) : helper)))
    + "</b>&nbsp;"
    + escapeExpression(((helper = (helper = helpers.processed_by || (depth0 != null ? depth0.processed_by : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"processed_by","hash":{},"data":data}) : helper)))
    + "\r\n      </div>\r\n      <div class=\"username\">\r\n         <b>Comments:</b> "
    + escapeExpression(((helper = (helper = helpers.comments || (depth0 != null ? depth0.comments : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"comments","hash":{},"data":data}) : helper)))
    + "\r\n      </div>\r\n    </div>  \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.productDifferences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            \r\n\r\n";
},"useData":true});
templates['location_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "  <div id=\"location_differences_scroll_container\">\r\n    <div class=\"panner scroll_up\" data-scroll-modifier='-1'>&nbsp;</div>\r\n    <div id=\"location_differences_scroll\">\r\n      \r\n      <div id=\"location_differences\">\r\n        <div class=\"tabbable tabs-left\">\r\n          <ul class=\"nav nav-tabs\" id=\"locationTabs\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"panner scroll_down\" data-scroll-modifier='1'>&nbsp;</div>\r\n  </div>\r\n  <div id=\"container_differences\">  \r\n    <div class=\"tab-content\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n  </div>\r\n  <div class=\"clear\"></div>\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "              <li id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_ID : depth0), depth0))
    + "\">\r\n                <a href=\"#"
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_ID : depth0), depth0))
    + "\">\r\n                  "
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\r\n                  <span class=\"\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n                </a>\r\n              </li>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        \r\n        <div class=\"tab-pane\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\" >\r\n            "
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\r\n        </div>\r\n        \r\n";
},"6":function(depth0,helpers,partials,data) {
  return "\r\n    <div class=\"no_data\">\r\n       No Data\r\n    </div>\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"back_btn_view_detail_div\">\r\n  <a href=\"#\" id=\"back_btn_view_detail\" class=\"btn btn-default btn-sm\">Back to Areas</a>\r\n</div>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(6, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
})();