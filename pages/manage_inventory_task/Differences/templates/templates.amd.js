define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['container_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "	<ul class=\"nav nav-tabs\" id=\"containerTabs\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " 	</ul>\n 	<div class=\"tab-content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        <li id=\""
    + alias2(alias1((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + "\">\n            <a href=\"#"
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\" data-toggle=\"tab\" >\n                "
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\n                <span class=\"\" >&nbsp;&nbsp;&nbsp;&nbsp;</span>\n            </a>\n        </li>\n";
},"4":function(depth0,helpers,partials,data) {
    return "		<div class=\"tab-pane\" id=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">\n           \n		</div>\n";
},"6":function(depth0,helpers,partials,data) {
    return "<div class=\"alert alert-info\" role=\"alert\">\n  \n  No Difference found.\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['container_tree_differences'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "               <span class=\"parent\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <span class=\"child product\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CHILDREN : stack1),{"name":"each","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "                        <span class=\"child\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"6":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <span class=\"child1 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"8":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"9":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                <span class=\"child1\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(10, data, 0),"inverse":this.program(12, data, 0),"data":data})) != null ? stack1 : "");
},"10":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                    <span class=\"child2 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"12":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"13":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                        <span class=\"child3\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(14, data, 0),"inverse":this.program(16, data, 0),"data":data})) != null ? stack1 : "");
},"14":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                            <span class=\"child3 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"16":function(depth0,helpers,partials,data) {
    return "                                            \n";
},"18":function(depth0,helpers,partials,data) {
    return "               <span class=\"new\">New to system</span>\n";
},"20":function(depth0,helpers,partials,data) {
    var stack1;

  return "               <span class=\"parent\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1),{"name":"if","hash":{},"fn":this.program(21, data, 0),"inverse":this.program(23, data, 0),"data":data})) != null ? stack1 : "");
},"21":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                  <span class=\"child product\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\n";
},"23":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CHILDREN : stack1),{"name":"each","hash":{},"fn":this.program(24, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"24":function(depth0,helpers,partials,data) {
    var stack1;

  return "                        <span class=\"child\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(25, data, 0),"inverse":this.program(27, data, 0),"data":data})) != null ? stack1 : "");
},"25":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                          <span class=\"child1 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"27":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(28, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"28":function(depth0,helpers,partials,data) {
    var stack1;

  return "                              <span class=\"child1\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(29, data, 0),"inverse":this.program(31, data, 0),"data":data})) != null ? stack1 : "");
},"29":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <span class=\"child2 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"31":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(32, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"32":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                  <span class=\"child2\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(33, data, 0),"inverse":this.program(35, data, 0),"data":data})) != null ? stack1 : "");
},"33":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                      <span class=\"child3 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"35":function(depth0,helpers,partials,data) {
    return "                                      \n";
},"37":function(depth0,helpers,partials,data) {
    return "              <span class=\"missing\">Missing now</span>\n";
},"39":function(depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "  <table class=\"table table-striped\" class=\"count_diff_table\" >\n        <thead>\n            <th>Product Name</th>\n            <th>System Count</th>\n            <th>Physical Count</th>\n            \n        </thead>\n        <tbody >\n";
  stack1 = ((helper = (helper = helpers.calculatedDiffs || (depth0 != null ? depth0.calculatedDiffs : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"calculatedDiffs","hash":{},"fn":this.program(40, data, 0),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers.calculatedDiffs) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </tbody>\n  </table>      \n";
},"40":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    \n          <tr >\n            <td>"
    + alias3(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\n            <td>"
    + alias3(((helper = (helper = helpers.systemCount || (depth0 != null ? depth0.systemCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"systemCount","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"difference\">"
    + alias3(((helper = (helper = helpers.physicalCount || (depth0 != null ? depth0.physicalCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"physicalCount","hash":{},"data":data}) : helper)))
    + "</td>\n                  \n          </tr>\n";
},"42":function(depth0,helpers,partials,data) {
    var helper;

  return "    <div class=\"approval_div\">\n          <div class=\"comments_div\">\n          <input type=\"hidden\" name=\"diff_id\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.conDiffId || (depth0 != null ? depth0.conDiffId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"conDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\n              <label>Comments</label>\n              <textarea  name=\"comments\"></textarea>\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n          </div>\n          <div class=\"approve_btn_div\">\n          <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\n            <span class=\"glyphicon glyphicon-ok\">\n            Approve\n          </a>\n          <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\n            <span class=\"glyphicon glyphicon-remove\">\n            Reject\n          </a> \n          </div>\n    </div>\n";
},"44":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "  <div class=\"processed_div\">\n\n    <div class=\"username\">\n       <b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_TITLE : stack1), depth0))
    + "</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_BY_NAME : stack1), depth0))
    + "\n    </div>\n    <div class=\"username\">\n       <b>Comments:</b> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.COMMENTS : stack1), depth0))
    + "\n    </div>\n  </div>  \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<div class=\"count_container\">\n    <div id=\"container_count_dif_div\">\n        <div class=\"username\">\n           <b>Scanned By:</b> "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.USERNAME : stack1), depth0))
    + "\n        </div>\n        <div id=\"system_physical_div\">\n          <div class=\"system_div\">\n            <h5>System Containers</h5>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.systemTree : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(18, data, 0),"data":data})) != null ? stack1 : "")
    + "          </div>   \n          <div class=\"physical_div\">\n            <h5>Physical Containers</h5>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.physicalTree : depth0),{"name":"if","hash":{},"fn":this.program(20, data, 0),"inverse":this.program(37, data, 0),"data":data})) != null ? stack1 : "")
    + "          </div>   \n          <div class=\"clear\"></div>\n        </div>\n\n    </div>\n\n\n\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.calculatedDiffs : depth0),{"name":"if","hash":{},"fn":this.program(39, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(42, data, 0),"inverse":this.program(44, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['count_differences'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=helpers.helperMissing, alias2="function", buffer = 
  "<div class=\"count_container\">\n  <div class=\"username\">\n     <b>Scanned By:</b> "
    + this.escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"username","hash":{},"data":data}) : helper)))
    + "\n  </div>\n  <table class=\"table table-striped\" class=\"count_diff_table\" >\n      <thead>\n          <th>Product Name</th>\n          <th>System Count</th>\n          <th>Physical Count</th>\n          \n      </thead>\n      <tbody >\n";
  stack1 = ((helper = (helper = helpers.productDifferences || (depth0 != null ? depth0.productDifferences : depth0)) != null ? helper : alias1),(options={"name":"productDifferences","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.productDifferences) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "      </tbody>\n  </table>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias1).call(depth0,(depth0 != null ? depth0.status : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(4, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "  \n        <tr >\n          <td>"
    + alias3(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\n          <td>"
    + alias3(((helper = (helper = helpers.SYSTEM_COUNT || (depth0 != null ? depth0.SYSTEM_COUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SYSTEM_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\n          <td class=\"difference\">"
    + alias3(((helper = (helper = helpers.PHYSICAL_COUNT || (depth0 != null ? depth0.PHYSICAL_COUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PHYSICAL_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\n        </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <div class=\"approval_div\">\n          <div class=\"comments_div\">\n              <input type=\"hidden\" name=\"diff_id\" value=\""
    + alias3(((helper = (helper = helpers.productDiffId || (depth0 != null ? depth0.productDiffId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\n              <label>Comments</label>\n              <textarea  name=\"comments\"></textarea>\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n          </div>\n          <div class=\"approve_btn_div\">\n            <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\n              <span class=\"glyphicon glyphicon-ok\">"
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\n              Approve\n            </a>\n            <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\n              <span class=\"glyphicon glyphicon-remove\">\n              Reject\n            </a> \n          </div>\n    </div>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <div class=\"processed_div\">\n\n      <div class=\"username\">\n         <b>"
    + alias3(((helper = (helper = helpers.processed_by_title || (depth0 != null ? depth0.processed_by_title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"processed_by_title","hash":{},"data":data}) : helper)))
    + "</b>&nbsp;"
    + alias3(((helper = (helper = helpers.processed_by || (depth0 != null ? depth0.processed_by : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"processed_by","hash":{},"data":data}) : helper)))
    + "\n      </div>\n      <div class=\"username\">\n         <b>Comments:</b> "
    + alias3(((helper = (helper = helpers.comments || (depth0 != null ? depth0.comments : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"comments","hash":{},"data":data}) : helper)))
    + "\n      </div>\n    </div>  \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.productDifferences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            \n\n";
},"useData":true});
templates['location_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  <div id=\"location_differences_scroll_container\">\n    <div class=\"panner scroll_up\" data-scroll-modifier='-1'>&nbsp;</div>\n    <div id=\"location_differences_scroll\">\n      \n      <div id=\"location_differences\">\n        <div class=\"tabbable tabs-left\">\n          <ul class=\"nav nav-tabs\" id=\"locationTabs\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "          </ul>\n        </div>\n      </div>\n    </div>\n    <div class=\"panner scroll_down\" data-scroll-modifier='1'>&nbsp;</div>\n  </div>\n  <div id=\"container_differences\">  \n    <div class=\"tab-content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n  </div>\n  <div class=\"clear\"></div>\n\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "              <li id=\""
    + alias2(alias1((depth0 != null ? depth0.LOCATION_ID : depth0), depth0))
    + "\">\n                <a href=\"#"
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + alias2(alias1((depth0 != null ? depth0.LOCATION_ID : depth0), depth0))
    + "\">\n                  "
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\n                  <span class=\"\" name=\""
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\n                </a>\n              </li>\n";
},"4":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        \n        <div class=\"tab-pane\" id=\""
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\" >\n            "
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\n        </div>\n        \n";
},"6":function(depth0,helpers,partials,data) {
    return "\n    <div class=\"no_data\">\n       No Data\n    </div>\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"back_btn_view_detail_div\">\n  <a href=\"#\" id=\"back_btn_view_detail\" class=\"btn btn-default btn-sm\">Back to Areas</a>\n</div>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
return templates;
});