"use strict";
define([
         "config", 
         "jquery",
         "blockui",
        
    ], 
 function(page_config,$,blockui){
  (function(){
            $.blockUI.defaults = {
                 css: { 
                  padding:        '8px',
                  margin:         0,
                  width:          '170px', 
                  top:            '45%', 
                  left:           '40%', 
                  textAlign:      'center', 
                  color:          '#000', 
                  border:         '3px solid #999999',
                  backgroundColor:'#ffffff',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius':    '10px',
                  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
                 },
                 //设置遮罩层的样式
                 overlayCSS:  { 
                  backgroundColor:'#000', 
                  opacity:        '0.6' 
                 },
                 
                 baseZ: 99999, 
                 centerX: true,
                 centerY: true, 
                 fadeOut:  1000,
                 showOverlay: true,
            };
        })();
      });