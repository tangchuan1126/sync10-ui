define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_list_box'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n		<div class=\"panel panel-default\">\n			<div class=\"panel-heading header\">\n				<div>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.areaName : depth0), depth0))
    + "</div>\n			</div>\n			<div class=\"body panel-body\">\n				<div class=\"locations_block\">	\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.locations : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\n\n			</div>\n		</div>\n";
},"2":function(depth0,helpers,partials,data) {
    return "					\n						<div>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.locationName : depth0), depth0))
    + "</div>\n					\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"areas_in_selection\">\n\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['create_task'] = template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Title:</label></div>\n							<div class=\"div-table-cell\">"
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>User:</label></div>\n							<div class=\"div-table-cell\">"
    + this.escapeExpression(((helper = (helper = helpers.user || (depth0 != null ? depth0.user : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"user","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Duration:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.duration || (depth0 != null ? depth0.duration : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"duration","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Repeats:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.repeatOptions || (depth0 != null ? depth0.repeatOptions : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"repeatOptions","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Repeat every:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.repeatEvery || (depth0 != null ? depth0.repeatEvery : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"repeatEvery","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.dayOfWeek : depth0),{"name":"if","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Starts on:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.startsOn || (depth0 != null ? depth0.startsOn : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"startsOn","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Ends:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.endsOn || (depth0 != null ? depth0.endsOn : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"endsOn","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper;

  return "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Repeat on:</label></div>\n							<div class=\"div-table-cell\">"
    + this.escapeExpression(((helper = (helper = helpers.dayOfWeek || (depth0 != null ? depth0.dayOfWeek : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"dayOfWeek","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n";
},"8":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Start Date:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.start_date || (depth0 != null ? depth0.start_date : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"start_date","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>End Date:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.end_date || (depth0 != null ? depth0.end_date : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"end_date","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<div class=\"panel panel-default\">\n	<div class=\"panel-heading header\">\n		<a class=\"previous\" id=\"back_date_selection\">Back</a>\n		<div class=\"clear\"></div>\n	</div>\n\n	<div class=\"body panel-body\">\n		<div class=\"div-table\" >\n			<div class=\"div-table-row\">\n				<div class=\"div-table-cell\">\n					<div class=\"div-table\">\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Warehouse:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.warehouse || (depth0 != null ? depth0.warehouse : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"warehouse","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.user : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>is Repeating:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.isRepeat || (depth0 != null ? depth0.isRepeat : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"isRepeat","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell right\"><label>Type:</label></div>\n							<div class=\"div-table-cell\">"
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + "</div>\n						</div>\n					</div>\n				</div>\n				<div class=\"div-table-cell\">\n					<div class=\"div-table\">\n						\n						\n					</div>\n				</div>	\n\n				<div class=\"div-table-cell\">\n					<div class=\"div-table\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.scheduled : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + "					</div>\n				</div>\n				<div class=\"div-table-cell\" >\n					&nbsp;\n				</div>\n				<div class=\"div-table-cell\" >\n					<div class=\"div-table\" >\n						<div class=\"div-table-row\">\n							<div class=\"div-table-cell\" >&nbsp;</div>\n							<div class=\"div-table-cell\" id=\"create_btn_div\">\n								<a href=\"#\" id=\"create_task_btn\" class=\"btn btn-primary\">"
    + alias3(((helper = (helper = helpers.buttonTitle || (depth0 != null ? depth0.buttonTitle : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"buttonTitle","hash":{},"data":data}) : helper)))
    + "</a>\n							</div>\n						</div>\n					</div>			\n				</div>\n			</div>\n		</div>\n		\n		<div id=\"selected_areas_create\"></div>\n		\n	</div>\n</div>\n";
},"useData":true});
templates['deadline'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<input type=\"hidden\" id=\"created_by\" />\n<div class=\"panel panel-default\">\n		<div class=\"panel-heading header\">\n			<a class=\"previous\" id=\"back_user_selection\">User Selection </a>\n			\n			<div class=\"disabled\" id=\"next_date_selection_div\"><a class=\"next\" id=\"create_task_link\">Task Preview</a></div>\n			<div class=\"clear\"></div>\n		</div>\n		\n		<div class=\"body panel-body\" >\n			<div class=\"div-table\">\n				<div class=\"div-table-row\" id=\"is_repeating_checkbox_div\">\n					<div class=\"div-table-cell right\"><label for=\"is_repeating\">Is Repeating?</label></div>\n					<div class=\"div-table-cell\">\n						<input type=\"checkbox\" name=\"is_repeating\" id=\"is_repeating\" style=\"width:100px\" data-label-on=\"   Yes   \" data-label-off=\"   No   \" class=\"iToggle\"/>\n					</div>\n				</div>\n				<div class=\"div-table-row\" >\n					<div class=\"div-table-cell right\"><label >Task Type</label></div>\n					<div class=\"div-table-cell\" id=\"is_blind_checkbox_div\">\n					<input type=\"checkbox\" name=\"is_blind\" id=\"is_blind\" data-label-on=\"Blind\" data-label-off=\"Verify\" class=\"iToggle\"/>\n						\n					</div>\n				</div>\n				\n			</div>\n			<div class=\"div-table\" id=\"non-repeating-div\">\n				<div class=\"div-table-row\" id=\"start_date_box\">\n					<div class=\"div-table-cell right\"><label for=\"start_date\">Start Date</label></div>\n					<div class=\"div-table-cell\"><input type=\"text\" id=\"start_date\" value=\"\" readonly=\"readonly\"/></div>\n				</div>\n				<div class=\"div-table-row\" id=\"end_date_box\">\n					<div class=\"div-table-cell right\"><label for=\"end_date\">End Date</label></div>\n					<div class=\"div-table-cell\"><input type=\"text\" id=\"end_date\" value=\"\" readonly=\"readonly\"/></div>\n				</div>\n			</div>\n			<div id=\"repeating_div\">\n				<div class=\"div-table\">\n					<div class=\"div-table-row\" id=\"task_duration_div\">\n						<div class=\"div-table-cell right\"><label for=\"task_duration\">Duration</label></div>\n						<div class=\"div-table-cell\">\n							<select id=\"task_duration\" name=\"task_duration\">\n								<option value=\"1\">1</option>\n								<option value=\"2\">2</option>\n								<option value=\"3\">3</option>\n								<option value=\"4\">4</option>\n								<option value=\"5\">5</option>\n								<option value=\"6\">6</option>\n								<option value=\"7\">7</option>\n								\n							</select>\n							Days\n						</div>\n					</div>\n					<div class=\"div-table-row\">\n						<div class=\"div-table-cell right\"><label for=\"repeat_options\">Repeats</label></div>\n						<div class=\"div-table-cell\">\n							<select id=\"repeat_options\" name=\"repeat_options\">\n								<option value=\"Daily\">Daily</option>\n								<option value=\"Weekly\">Weekly</option>\n								<option value=\"Monthly\">Monthly</option>\n							</select>\n						</div>\n					</div>\n				</div>\n				<div id=\"Weekly_table\" class=\"div-table\">\n					<div class=\"div-table-row\">\n						<div class=\"div-table-cell right\"><label for=\"repeat_options_weekly\">Repeat every</label></div>\n						<div class=\"div-table-cell\">\n							<select id=\"repeat_options_weekly\" name=\"repeat_options_weekly\">\n								<option value=\"1\">1</option>\n								<option value=\"2\">2</option>\n								<option value=\"3\">3</option>\n								<option value=\"4\">4</option>\n								<option value=\"5\">5</option>\n								<option value=\"6\">6</option>\n								<option value=\"7\">7</option>\n								<option value=\"8\">8</option>\n								<option value=\"9\">9</option>\n								<option value=\"10\">10</option>\n								<option value=\"11\">11</option>\n								<option value=\"12\">12</option>\n								<option value=\"13\">13</option>\n								<option value=\"14\">14</option>\n								<option value=\"15\">15</option>\n								<option value=\"16\">16</option>\n								<option value=\"17\">17</option>\n								<option value=\"18\">18</option>\n								<option value=\"19\">19</option>\n								<option value=\"20\">20</option>\n								<option value=\"21\">21</option>\n								<option value=\"22\">22</option>\n								<option value=\"23\">23</option>\n								<option value=\"24\">24</option>\n								<option value=\"25\">25</option>\n								<option value=\"26\">26</option>\n								<option value=\"27\">27</option>\n								<option value=\"28\">28</option>\n								<option value=\"29\">29</option>\n								<option value=\"30\">30</option>\n								\n							</select>\n						weeks\n						</div>\n					</div>\n					\n				</div>\n				<div id=\"Daily_table\" class=\"div-table\">\n					<div class=\"div-table-row\">\n						<div class=\"div-table-cell right\"><label for=\"repeat_options_daily\">Repeat every</label></div>\n						<div class=\"div-table-cell\">\n							<select id=\"repeat_options_daily\" name=\"repeat_options_daily\">\n								<option value=\"1\">1</option>\n								<option value=\"2\">2</option>\n								<option value=\"3\">3</option>\n								<option value=\"4\">4</option>\n								<option value=\"5\">5</option>\n								<option value=\"6\">6</option>\n								<option value=\"7\">7</option>\n								<option value=\"8\">8</option>\n								<option value=\"9\">9</option>\n								<option value=\"10\">10</option>\n								<option value=\"11\">11</option>\n								<option value=\"12\">12</option>\n								<option value=\"13\">13</option>\n								<option value=\"14\">14</option>\n								<option value=\"15\">15</option>\n								<option value=\"16\">16</option>\n								<option value=\"17\">17</option>\n								<option value=\"18\">18</option>\n								<option value=\"19\">19</option>\n								<option value=\"20\">20</option>\n								<option value=\"21\">21</option>\n								<option value=\"22\">22</option>\n								<option value=\"23\">23</option>\n								<option value=\"24\">24</option>\n								<option value=\"25\">25</option>\n								<option value=\"26\">26</option>\n								<option value=\"27\">27</option>\n								<option value=\"28\">28</option>\n								<option value=\"29\">29</option>\n								<option value=\"30\">30</option>\n								\n							</select>\n						days</div>\n					</div>\n				</div>\n				<div id=\"Monthly_table\" class=\"div-table\">\n					<div class=\"div-table-row\">\n						<div class=\"div-table-cell right\"><label for=\"repeat_options_monthly\">Repeat every</label></div>\n						<div class=\"div-table-cell\">\n							<select id=\"repeat_options_monthly\" name=\"repeat_options_monthly\">\n								<option value=\"1\">1</option>\n								<option value=\"2\">2</option>\n								<option value=\"3\">3</option>\n								<option value=\"4\">4</option>\n								<option value=\"5\">5</option>\n								<option value=\"6\">6</option>\n								<option value=\"7\">7</option>\n								<option value=\"8\">8</option>\n								<option value=\"9\">9</option>\n								<option value=\"10\">10</option>\n								<option value=\"11\">11</option>\n								<option value=\"12\">12</option>\n								<option value=\"13\">13</option>\n								<option value=\"14\">14</option>\n								<option value=\"15\">15</option>\n								<option value=\"16\">16</option>\n								<option value=\"17\">17</option>\n								<option value=\"18\">18</option>\n								<option value=\"19\">19</option>\n								<option value=\"20\">20</option>\n								<option value=\"21\">21</option>\n								<option value=\"22\">22</option>\n								<option value=\"23\">23</option>\n								<option value=\"24\">24</option>\n								<option value=\"25\">25</option>\n								<option value=\"26\">26</option>\n								<option value=\"27\">27</option>\n								<option value=\"28\">28</option>\n								<option value=\"29\">29</option>\n								<option value=\"30\">30</option>\n								\n							</select>\n						months</div>\n					</div>\n				</div>\n				<div id=\"date_table\" class=\"div-table\">\n					<div class=\"div-table-row\">\n						<div class=\"div-table-cell right\"><label for=\"starts_on\">Starts on</label></div>\n						<div class=\"div-table-cell\"><input type=\"text\" id=\"starts_on\" value=\"\" /></div>\n					</div>\n					<div class=\"div-table-row\">\n						<div class=\"div-table-cell right\"><label>Ends</label></div>\n						<div class=\"div-table-cell\">\n							<div class=\"div-table\" id=\"ends_table\">\n								<div class=\"div-table-row\">\n									<div class=\"div-table-cell\">\n										<input type=\"radio\" id=\"ends_on_never\" name=\"ends_on\" value=\"never\" checked=\"checked\">\n										<label for=\"ends_on_never\">Never</label>\n									</div>\n								</div>\n								<div class=\"div-table-row\" id=\"occ\">\n									<div class=\"div-table-cell\">\n										<input type=\"radio\" id=\"ends_on_after\" name=\"ends_on\" value=\"after\">\n										<label for=\"ends_on_after\">After <input type=\"text\" id=\"occurances\"/> occurrences</label>\n									</div>\n								</div>\n								<div class=\"div-table-row\">\n									<div class=\"div-table-cell\">\n										<input type=\"radio\" name=\"ends_on\" id=\"ends_on_on\" value=\"on\">\n										<label for=\"ends_on_on\">On <input type=\"text\" id=\"ends_on_date\" value=\"\" /></label>\n									</div>\n								</div>\n							</div>\n							\n						</div>\n					</div>\n				</div>\n			</div>\n			\n		</div>	\n</div>	";
},"useData":true});
templates['new_task_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<div class=\"panel panel-default\">\n  <div id=\"inventory_task_header\" class=\"panel-heading\">\n      <div class=\"left\">"
    + alias3(((helper = (helper = helpers.heading || (depth0 != null ? depth0.heading : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"heading","hash":{},"data":data}) : helper)))
    + "</div>\n      <div id=\"back-buttons-div\"><a href=\"#\" id=\"back\" class=\"btn btn-warning\">Back</a></div>\n      <div class=\"clear\"></div>\n    </div>\n  <div id=\"filter_box\" class=\"panel-body\">\n    <input type=\"hidden\" name=\"create_task_type\" id=\"create_task_type\" value=\""
    + alias3(((helper = (helper = helpers.taskType || (depth0 != null ? depth0.taskType : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"taskType","hash":{},"data":data}) : helper)))
    + "\"/>\n    <input type=\"hidden\" name=\"task_id\" id=\"task_id\" value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"/>\n    <div id=\"toc_warehouse\"></div>\n    <div id=\"toc_title\"></div>\n    \n    <div class=\"clear\"></div>\n    <div id=\"error_block\"></div>\n    <div id=\"all_container\">\n      <div id=\"all_container2\">\n        <div id=\"toc_area\"></div>\n        <div id=\"toc_selected_area\"></div>\n        <div id=\"users_body\"></div>\n        <div id=\"deadline_body\"></div>\n        <div id=\"create_task_body\"></div>\n        <div class=\"clear\"></div>\n      </div>\n    </div>\n  </div>\n</div>  ";
},"useData":true});
templates['storage_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\">\n<div class=\"input-group-addon\" >Warehouse</div><input id=\"selected_storage_id\" type=\"hidden\" value=\"0\"/>\n<input id='storage_list_box' type='text' class='form-control' />\n</div>\n\n";
},"useData":true});
templates['task'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "			<div class=\"loc\">\n				<input type=\"checkbox\" name=\"location\" id=\"location_"
    + alias2(alias1((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\"/><label for=\"location_"
    + alias2(alias1((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "</label>\n			</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"task_box_unselected task_box\" id=\"task_box_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\" >\n\n	<div class=\"task_header\" id=\"header_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.name : stack1), depth0))
    + "</div>\n	<div class=\"select_all\">\n		<input type=\"checkbox\" name=\"select_all_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "\" id=\"area_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "_select_all\"/><label for=\"area_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.area_id : stack1), depth0))
    + "_select_all\">Whole Area</label>\n		</div>\n	<div class=\"task_body\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.area : depth0)) != null ? stack1.locations : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n	\n</div>\n	";
},"useData":true});
templates['title_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\" id=\"create_title_div\">\n<div class=\"input-group-addon\" >Title</div><input id=\"selected_title_id\" type=\"hidden\" value=\"0\"/>\n<input id='title_list_box' type='text' class='form-control' />\n</div>\n\n";
},"useData":true});
templates['user_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\">\n		<div class=\"panel-heading header\">\n			<a class=\"previous\" id=\"back_area_selection\">Area Selection</a>\n			<div class=\"disabled\" id=\"next_date_selection_div\"><a class=\"next\" id=\"next_date_selection\">Task Schedule</a></div>\n			<div class=\"clear\"></div>\n		</div>\n		\n		<div class=\"body panel-body\">	\n			<div class=\"group_box_div\">\n				<div class=\"employee_box group_box\">\n					<div class=\"employee_name\" id=\"0\">Anyone in the Group</div>\n				</div>\n				<div class=\"clear\"></div>\n			</div>\n			<div class=\"employee_box_div\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n			<div class=\"clear\"></div>\n		</div>\n		\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "				<div class=\"employee_box\">\n					<div class=\"employee_name\" id=\""
    + alias2(alias1((depth0 != null ? depth0.ADID : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.EMPLOYE_NAME : depth0), depth0))
    + "</div>\n				</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
return templates;
});