"use strict";
define(["jquery","bootstrap","metisMenu","CompondList/View","art_Dialog/dialog-plus","./view_config","./outbound","./followup"],
	function($,bootstrap,metisMenu,CompondList,Dialog,view_config,Outbound,Followup) {
		return function(options) {
			var ListView = {
				
				initialize: function(options) {

					var list = new CompondList(view_config, {
						renderTo: options.el,
						dataUrl: options.url,
						PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
						//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
					});
					//绑定事件
					list.render();

					list.on("events.OUTBOUND",function(e) {
						var obj = Outbound.showPictrueOnline('40',e.data.linedata.TRANSPORT_ID,"",0,"transport");
						if(!obj) {return;}
						if(window.top && window.top.openPictureOnlineShow){
							window.top.openPictureOnlineShow(obj);
						} else {
							var param = jQuery.param(obj);
							var uri = "/Sync10/administrator/file/picture_online_show.html?"+param;
							var d = new Dialog({
								url: uri, title: 'photo', width: 1100, height: 600, lock: true, esc: true
							});
							d.showModal();
						}
					});
					//跟进备货
					list.on("events.FOLLOWUP",function(e) {
						//console.log(new Followup({}).html());
						var html = $("#followup").html(new Followup({}).html()).html();
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var d = new Dialog({
							url: "/Sync10/administrator/transport/transport_follow_up.html?transport_id="+transport_id,
							//content:html,
							title: "转运单跟进["+transport_id+"]",width:'600px',height:'300px', lock: true, esc: true
						});
						d.showModal();
						//alert(e.data.linedata.TRANSPORT_ID);
					});
					list.on("events.BOOKDOORORLOCATION",function(e) {
						console.log(e);
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var d = new Dialog({
							url: "/Sync10/administrator/transport/book_door_or_location.html?transport_id="+transport_id+"&rel_occupancy_use=1",
							//content:html,
							title: "转运单["+transport_id+"]司机签到",width:'800',height:'600', lock: true, esc: true
						});
						d.showModal();
					});
					list.on("events.moreLogs",function(e) {
						//自定义事件没有linedata
						console.log(e);
						alert(e.data.TRANSPORT_ID);
					});
					list.on("transportDetail",function(e) {
						console.log(e);
						alert(e.data.TRANSPORT_ID);
						window.open("detail/index_transportdetail.html");
					});
					//构造一个视图
					this.view = list;
				},
				render:function(options) {
					//先清空之前的数据
					this.view.collection.remove();
					if(typeof options =="object") {
						//清空原来的页面
						$(options.el).empty();
						this.view.collection.el = options.el;
						this.view.collection.url = options.url;
						this.view.collection.PostData = (typeof options.queryCondition =="object" )?options.queryCondition:{};
					}
					this.view.render();
				},
			};
			ListView.initialize(options);

			return ListView.view;
		};
});
