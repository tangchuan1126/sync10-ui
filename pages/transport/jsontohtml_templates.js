define(["JSONTOHTML","./key/TransportOrderKey","./key/TransportQualityInspectionKey"], function(JSONTOHTML,TransportOrderKey,TransportQualityInspectionKey) {

	//第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index)
					{
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}"
		}
	};

	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		//初始化，将json数据分类：分为对应的行和列
		AUTHS:null,
		int: function(jsons,View_control) 
		{
			//数据转换成表格形式
			//["tr":"index","children":[{},{}]]
			_tbody.AUTHS=jsons.AUTHS;
			//_.extend({a:"112"},{b:"222"});
			var headallobj =View_control.head;
			var footer = View_control.footer;
			var trfooterdata = {
				"colspan": headallobj.length,
				"children": footer,
				"trid":""
		};
			//tr表脚
			//var trfooter = (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
			var pageSize=jsons.PAGECTRL.pageSize;
			//按表头转换成行和列
			var __tr=[];
			for (var tri = 0; tri < jsons.DATA.length; tri++) 
			{
				var tritme = jsons.DATA[tri];
					_.map(_tbody.AUTHS,function(obj,key){
				//	console.log(obj);
				});
				var __td = [];
				for (var headkey in headallobj) 
				{
					var tritmes = headallobj[headkey];
					var Rowname = tritmes.field;
					//config--head。field名值
					var Rowobj = {};
					if (typeof Rowname == "object") 
					{
						//没有对应的列名，把整行存进去
						 __td.push(eval("({" + Rowname.name + ":tritme})"));
					}
					else
					{
						//有列名，也把整行存入，方便取整行数据
						Rowobj = tritme;
						//Rowobj = eval("tritme." + Rowname);
						var setobjs = eval("({" + Rowname + ":Rowobj})");
						__td.push(setobjs);
					}
				}
				__tr.push({"trid":tritme.TRANSPORT_ID,"children": __td});
				if(tri >= pageSize)
				{
					break;
				}
			}
			//拼装tr,trfooter，
			var __trhtml = "";
			for (var trkey in __tr) 
			{
				var _tritmes = __tr[trkey];
				__trhtml += (JSONTOHTML.transform(_tritmes, _tbody.tr));
				trfooterdata["trid"]=_tritmes.trid;
				//将行值传个按钮行，用于判断按钮是否需要显示
				trfooterdata["linedata"]=tritme;
				__trhtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
			}

			return __trhtml;

		},
		keyname: function(obj) 
		{
			//列对象，只有一个key,那就是列名对应的字段
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"data-itmeid":function(obj){
				return "TRANSPORT_ID|"+obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				//自由列数console
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				//console.log(obj);
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+"TRANSPORT_ID|"+obj.trid+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) 
				{
					var itmea = obj.children[akey];
					if(_tbody.isHasButton(itmea[0],obj.trid,_tbody.AUTHS))
					{
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		isHasButton:function(itmea,trid,AUTHS)
		{
			var res = false;
			_.map(AUTHS,function(obj,key)
			{
				
				if(""+obj.ID==trid+"")
				{
					
					if(itmea =="events.OUTBOUND")
					{
						res = obj["BUTTON_OUTBOUND"];
					}
					else if(itmea =="events.FOLLOWUP")
					{
						res = obj["BUTTON_FOLLOWUP"];
					}
					else if(itmea =="events.BOOKDOORORLOCATION")
					{
						res = obj["BUTTON_BOOKDOORORLOCATION"];
					}
					else if(itmea =="events.BOOKDOORORLOCATIONUPDATEORVIEW")
					{
						res = obj["BUTTON_BOOKDOORORLOCATIONUPDATEORVIEW"];
					}
					else
					{
						res = false;
					}
				}
			});
			return res;
		},
		td: {
			tag: "td",
			class: function(obj, index) {
				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				if("basicInfo" == keynames)
				{
					classname = "td_" + "TRAILER";
				}
				else
				{
					classname = "td_" + keynames;
				}
				
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "top";
				}
				return valignsing;
			},
			html: function(obj, index) {
				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);

				if (keynames != "" && typeof keynames != "undefined")
				{
					if(typeof keynames == "string")
					{
						if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") 
						{
							retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
						}
					}
					else if(typeof keynames == "object")
					{
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames.name)));
					}
				}
				return retuhtml;
			}
		},
		basicInfo:
		{
			"tag": "fieldset", 
			"class": "TRAILER", 
			"children": [
				{
					"tag": "span", 
					"class": "fieldset_tfoot", 
					"children": [
						{
							"tag": "em", 
							"html": "总容器：${basicInfo.CONTAINER_COUNT}"
						}
					]
				}, 
				{
					"tag": "legend", 
					"class": "td_legend", 
					"children": [
						{
							"tag": "span", "data-eventname": "transportDetail", "class": "valuestyle buttonallevnts", 
							"html": "${basicInfo.TRANSPORT_ID}"
						}, 
						{
							"tag": "span", 
							"class": function(obj,index) {
								if (obj.basicInfo.TRANSPORT_STATUS=="5") {
									return "status color-green";
								} else if (obj.basicInfo.TRANSPORT_STATUS=="2") {
									return "status color-blue";
								} else if (obj.basicInfo.TRANSPORT_STATUS=="3") {
									return "status color-red";
								} else if (obj.basicInfo.TRANSPORT_STATUS=="12") {
									return "status color-5B4B00";
								} else {
									return "status";
								}
							}, 
							"html": function (obj,index){ return TransportOrderKey[obj.basicInfo.TRANSPORT_STATUS][window.navigator.language];}
						}
					],
					
				}, 
				{
					"tag": "div", 
					"class": "Tractor_ulitme", 
					"children": [
						{
							"tag": "ul", 
							"children": [
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "创建人:"}, 
										{"tag": "span", "class": "valstyle", "html": "${basicInfo.CREATE_ACCOUNT}"}
									]
								}, 
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "TITLE:"}, 
										{"tag": "span", "class": "valstyle", "html": "${basicInfo.TITLE_NAME}"}
									]
								}, 
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "允许装箱:"}, 
										{"tag": "span", "class": "valstyle", "html": "${basicInfo.PACKING_ACCOUNT_NAME}"}
									]
								}, 
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "创建时间:"}, 
										{"tag": "span", "class": "valstyle", "html": "${basicInfo.TRANSPORT_DATE}"}
									]
								}, 
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "更新时间:"}, 
										{"tag": "span", "class": "valstyle", "html": "${basicInfo.UPDATEDATE}"}
									]
								}, 
								{
									"tag": "li", 
									"children": [
										{"tag": "span", "class": "namestyle", "html": "ETA:"},
										{"tag": "span", "class": "valstyle", "html": "${basicInfo.TRANSPORT_RECEIVE_DATE}"}
									]
								}
							]
						}
					]
				}
			]
		},
		transportInfo: {
				 tag: "div",
				 //class: "Tractor_ulitme",
				 class: "Log_container",
				 html: function(obj, index)
				 {
					return _tbody.orderedULLayout(
						[
						{"lable":"收货仓库","valueKey":"RECEIVE_PSNAME"},
						{"lable":"提货仓库","valueKey":"SEND_PSNAME"}
						
						],
						obj.transportInfo,
						_tbody._layLout)
					     +"<div class='box1px'></div>"
					     +
					   _tbody.orderedULLayout(
						[
						
						{"lable":"运单号","valueKey":"DELIVER_ZIP_CODE"},
						{"lable":"货运公司","valueKey":"TRANSPORT_WAYBILL_NAME"},
						{"lable":"承运公司","valueKey":"TRANSPORT_WAYBILL_NAME"},
						{"lable":"始发国/始发港","valueKey":"TRANSPORT_SEND_PLACE"},
						{"lable":"目的国/目的港","valueKey":"TRANSPORT_RECEIVE_PLACE"},
						{"lable":"总体积","valueKey":"TRANSPORT_VOLUME"},
						],
						obj.transportInfo,
						_tbody._layLout);
				}
		},
		FLOWS: 
		{
			tag: "div",
			class: "Log_container",
			html: function (obj, index) {

				var flowsliste=obj.FLOWS.FLOWS;	

		  return JSONTOHTML.transform({p_obj:obj,children:flowsliste}, _tbody._ulList);

			}
		},
		_ulList:{
			tag: "ul",
			html:function(obj){

          //  console.log(obj);
            
            var subitme=obj.children;

            var lihtml="";

            //每个变了输出 2个 li
            for(var likey in subitme){
                
                 var itme_li=subitme[likey];
                 if(lihtml!="" && typeof lihtml!="undefined" && subitme.length > 1){
                 	lihtml+="<li class='box1px'></li>" 
                 }


                 lihtml+="<li>";

                 //第一个span--左边
                 if(itme_li.TYPE==4){
                 	lihtml+="<span class='namestyle'>货物状态:</span>";
                 }else if(itme_li.TYPE==11){
                 	lihtml+="<span class='namestyle'>质检流程:</span>";
                 }else{
                 	lihtml+="<span class='namestyle'>"+itme_li.TYPE+":</span>";
                 }

                 //第二个span --右边
                 var STATUStoint=window.parseInt(itme_li.STATUS);
               
               
                 var languagesrin=window.navigator.language.split("-")[0];

                 if(itme_li.TYPE==4){
                 	lihtml+="<span class='valstyle'>"+TransportOrderKey[STATUStoint][languagesrin]+"</span>";
                 }else if(itme_li.TYPE==11){
                 	lihtml+="<span class='valstyle'>"+TransportQualityInspectionKey[STATUStoint][languagesrin]+"</span>";
                 }else{
                 	lihtml+="<span class='valstyle'>"+TransportOrderKey[STATUStoint][languagesrin]+"</span>";
                 }

                 lihtml+="</li>";

                 if(itme_li.EMPLOYE_NAMES){
                 lihtml+="<li><span class='namestyle'>"+itme_li.EMPLOYE_NAMES+":</span>";
                 lihtml+="<span class='valstyle'>xxx</span></li>";
                }

              

            }// end for

            return lihtml;
            

			}
		},
		LOGS: {
			tag: "div",
			class: "Log_container",
			html: function(obj, index) 
			{

				var ulsring = "";
				var arryjson = obj.LOGS.LOGS;
				var box1px = "<div class='box1px'></div>";
				for (var _key = 0; _key < arryjson.length; _key++)
				{

					var arryitmes = arryjson[_key];
					var ul = '<ul><li><span class="logvaluestyle">' + arryitmes.FOLLOWUPTYPE + ':&nbsp;&nbsp;</span><span >' + arryitmes.ADMINNAME + '</span>&nbsp;&nbsp;<span>' + arryitmes.TRANSPORT_DATE + '</span></li><li><span>' + arryitmes.TRANSPORT_CONTENT + '</span></li>';

					ul += '</ul>';

					if (ulsring != "" && typeof ulsring != "undefined") 
					{
						ulsring = ulsring + box1px + ul;
					}
					 else 
					{
						
						ulsring = ul;
					}

					if (_key == 2) 
					{
						ulsring = ulsring +'<span class="valuestyle buttonallevnts" data-eventname="events.moreLogs"><a class="more">更多</a></span>';
						break;
					}

				}

				// ulsring;
				//if (obj.CHECKINLOG.LOGS_ID) 
				//{

				//	ulsring += '<label>';
					//ulsring += '<span class="Morestyle" id="more_' + obj.CHECKINLOG.MORE.PARA + '">' + obj.CHECKINLOG.MORE.VALUE + '</span>';
				//	ulsring += '</label>';

			//	}

				return ulsring;
			}
		},

		//====================================工具方法===================================//
		//处理空字符串
		emptyHandle:function(str,defaultValue)
		{
			if(typeof str !="undefined" && str !="")
			{
					return str;
			}
			else
			{
				return defaultValue;
			}
		},
		_stateClass:function(state)
		{
			var tempclass="Sub_trailer";
			if(state == "0")
			{
				tempclass="Sub_trailer";
			}
			else if(state =="1")
			{
				tempclass="Sub_trailer";
			}
			else
			{
				tempclass="Sub_trailer";
			}
			return tempclass;
		},
		//====================================工具方法===================================//

		//=====================================布局工具==================================//
		//产生一个 ul 
		orderedULLayout:function(orderArr,dataObj,template)
		{
			for(var len=orderArr.length,i=0;i<len;i++)
			{
				//获取lable
				var lable = orderArr[i].lable;
				var valueKey = orderArr[i].valueKey;
				var value ="";
				if(typeof dataObj == "object")
				{
					value = dataObj[valueKey];
				}
				else if(typeof dataObj == "array")
				{
					for(var length=dataObj.length,j=0;j<length;j++)
					{
						if(dataObj[j].hasOwnProperty(valueKey))
						{
							//... 暂不支持数组排序
						}
					}
				}
				orderArr[i].value=value;
			}
			return JSONTOHTML.transform(orderArr, template);
		},
		
		_layLout:
		{
			tag: "ul",
			html: function(obj, index) 
			{
				return (JSONTOHTML.transform(obj, _tbody._layoutLi));
			}
		},
		_layoutLi:
		{
			tag: "li",
			html: function(obj, index) 
			{
				return '<span class="namestyle">'+obj.lable+':&nbsp;&nbsp;</span><span class="valstyle">' + _tbody.emptyHandle(obj.value,"") + '</span>';
			}
		},

		//====================================布局工具=========================================//
	}

	return {
		tbody: _tbody,
		table: _table
	}

});