"use strict";
require(["../../requirejs_config"] ,function($) {
	require(["jquery", 'backbone','handlebars',"jqueryui/tabs",
		"./js/view/searchView","./js/view/filterView","./transportListMain"],
		function($,Backbone,Handlebars,ui,SearchView,FilterView,TransportList) {

		$("#tabs").tabs();

		var queryOptions = {el: "#data", url: "/Sync10-ui/pages/transport/temp/sendData.json",queryCondition:{send_psid:100000}};
		var resultView = new TransportList(queryOptions);
  


		 var t = new SearchView({el:"#transport_search",resultView:resultView});
		 t.render();

		//var temp = new FilterView({resultView:resultView});
	   // temp.render();


	});
});