define(["jquery","handlebars","JSONTOHTML","../plugs/Layout"], 
function($,Handlebars,JSONTOHTML,Layout)
{
	return function(data)
	{

		var basicInfoTable = function(data)
		{
			var tdName = ["cols1","cols2","cols3"];
			var tdConfig =
			{
				cols1:
				{
					tag:"div",
					html:function(obj,index)
					{
						return Layout.ul_label_value(
									[
										{"label":"运单号","valueKey":"TRANSPORT_ID"},
									],
									obj.DATA);
					}
				},
				cols2:
				{
					tag:"div",
					html:function(obj,index)
					{
						return Layout.ul_label_value(
									[
										{"label":"TITLE","valueKey":"TITLE_NAME"},
									],
									obj.DATA);
					}
				},
				cols3:
				{
					tag:"div",
					html:function(obj,index)
					{
						return Layout.ul_label_value(
									[
										{"label":"货物状态","valueKey":"TRANSPORT_STATUS"},
									],
									obj.DATA);
					}
				},
			};
			return  Layout.oneRow_table(data,{tdName:tdName,tdConfig:tdConfig,tableId:"basicInfoTable"});
		};

		var detailInfoTable = function(data)
		{
			var tdName = ["cols1","cols2","cols3"];
			var tdConfig =
			{	
				cols1:
				{
					tag:"fieldset",
					children:
					[
						{
							tag:"legend",
							html:"运单基础信息",
						},
						{
							tag:"div",
							class:"ulDiv",
							html:function(obj,index)
							{
								// valueKey  要是obj的子节点
								return Layout.ul_label_value(
									[
										{"label":"运单号","valueKey":"TRANSPORT_ID"},
										{"label":"TITLE","valueKey":"TITLE_NAME"},
										{"label":"创建人","valueKey":"CREATE_ACCOUNT"},
										{"label":"创建时间","valueKey":"TRANSPORT_DATE"},
										{"label":"ETD","valueKey":"TRANSPORT_RECEIVE_DATE"},
										{"label":"允许装箱","valueKey":""}
									],
									obj.DATA);
							}
						}
					]
				},
				cols2:
				{
					tag:"fieldset",
					children:
					[
						{
							tag:"legend",
							html:"运输基础信息"
						},
						{
							tag:"div",
							class:"ulDiv",
							html:function(obj,index)
							{

								return Layout.ul_label_value(
									[
										{"label":"货物状态","valueKey":"TRANSPORT_STATUS"},
										{"label":"出口报关","valueKey":"DECLARATION"},
										{"label":"进口清关","valueKey":"CLEARANCE"},
										{"label":"运费状态","valueKey":"STOCK_IN_SET"},
									],
									obj.DATA);
							}
						}
					]
				},
				cols3:
				{
					tag:"fieldset",
					children:
					[
						{
							tag:"legend",
							html:"流程基础信息"
						},
						{
							tag:"div",
							class:"ulDiv",
							html:function(obj,index)
							{
								return Layout.ul_label_value(
									[
										{"label":"制签流程","valueKey":"TAG"},
										{"label":"第三方标签","valueKey":"TAG_THIRD"},
										{"label":"实物图片","valueKey":"PRODUCT_FILE"},
										{"label":"质检流程","valueKey":"QUALITY_INSPECTION"},
										{"label":"单证流程","valueKey":"CERTIFICATE"},
									],
									obj.DATA);
							}
						}
					]
				},
			};

			return Layout.oneRow_table(data,{tdName:tdName,tdConfig:tdConfig,tableId:"detailInfoTable"});
		};
		return basicInfoTable(data)+detailInfoTable(data);
	};

});






