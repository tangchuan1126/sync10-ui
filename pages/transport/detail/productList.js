define(["jquery","JSONTOHTML","../plugs/Layout"], 
function($,JSONTOHTML,Layout)
{
	return function(data)
	{
		var caption =function(dataObj)
		{
			return "总体积："+dataObj.volume+" cm³，总重量："+dataObj.weight+" Kg";
		};
		var thead= function(dataObj)
		{
			var html = "<tr>";
			for(var i=0,len=theadName.length;i<len;i++)
			{
				html +="<td>";
				html += theadName[i].label;
				html +="</td>";
			}
			html+="</tr>";
			return html;
		};

		var tbody = function(dataObj)
		{
			return _trHtml(dataObj.DATA.DETAILS);
		};

		var _trHtml = function(dataArr)
		{
			var html="";
			for(var i=0;i<dataArr.length;i++)
			{
				 html+="<tr>";
				for(var j=0;j<theadName.length;j++)
				{
					html+=_tdHtml(dataArr[i],theadName[j].filed);
				}
				 html+="</tr>";
			}
			return html;
		};
		var _tdHtml=function(data,attr)
		{
			return JSONTOHTML.transform(data,eval("tdConfig."+attr));
		};

		var theadName = [
			{label:"商品名(商品条码)",filed:"P_NAME"},
			{label:"货物总数",filed:"TRANSPORT_NORMAL_COUNT"},
			{label:"V(cm³),W(Kg),估算运费",filed:"VW"},
			{label:"商品序列号",filed:"ser"},
			{label:"容器数",filed:"cont"},
			{label:"批次",filed:"acc"}
		];
		var tdConfig = 
		{
			productName:
			{
				tag:"td",
				html:function(obj,index)
				{
					return "obj.productName";
				}
			},
			all:
			{
				tag:"td",
				html:function(obj,index)
				{
					return "obj.all";
				}
			},
			Vom:
			{
				tag:"td",
				html:function(obj,index)
				{
					return "obj.Vom";
				}
			},
			ser:
			{
				tag:"td",
				html:function(obj,index)
				{
					return "obj.ser";
				}
			},
			cont:
			{
				tag:"td",
				html:function(obj,index)
				{
					return "obj.cont";
				}
			},
			acc:
			{
				tag:"td",
				html:function(obj,index)
				{
					return "obj.acc";
				}
			}

		};

		var tableConfig = {caption:caption,thead:thead,tbody:tbody,tableId:"productListTable"};

		return Layout.mutiRow_caption_table(data,tableConfig);
	}

});