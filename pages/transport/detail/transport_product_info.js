define(["jquery","handlebars","JSONTOHTML","../plugs/Layout"], 
function($,Handlebars,JSONTOHTML,Layout)
{
	return function(data)
	{
		var tempData = data;
		//处理加载时候的逻辑这里写函数
		var getSrc = function()
			{
				return "../imgs/air_type.png";
			};

		var productBasicInfo  =function(data)
		{
			var tdName = ["cols1","cols2","cols3"];
			var tdConfig =
			{
				cols1:
				{
					tag:"div",
					html:function(obj,index)
					{

						return Layout.ul_label_value(
									[
										{"label":"发货仓库","valueKey":"SEND_PSID"},
									],
									obj.DATA);
					}
				},
				cols2:
				{
					tag:"div",
					html:function(obj,index)
					{
						return Layout.ul_label_value(
									[
										{"label":"运输公司","valueKey":"TRANSPORT_WAYBILL_NAME"},
									],
									obj.DATA);
					}
				},
				cols3:
				{
					tag:"div",
					html:function(obj,index)
					{
						return Layout.ul_label_value(
									[
										{"label":"收货仓库","valueKey":"RECEIVE_PSNAME"},
									],
									obj.DATA);
					}
				},
			};
			return  Layout.oneRow_table(data,{tdName:tdName,tdConfig:tdConfig});
		};

		var productDetailInfo  =function(data)
		{
			var tdName = ["cols1","cols2","cols3"];
			var tdConfig =
			{	
				cols1:
				{
					tag:"fieldset",
					children:
					[
						{
							tag:"legend",
							html:function(obj,index)
							{
								return obj.DATA.SEND_PSNAME;
							},
						},
						{
							tag:"div",
							class:"ulDiv",
							html:function(obj,index)
							{

								return Layout.ul_label_value(
									[
										{"label":"门牌号","valueKey":"SEND_HOUSE_NUMBER"},
										{"label":"街道","valueKey":"SEND_STREET"},
										{"label":"城市","valueKey":"SEND_CITY"},
										{"label":"邮编","valueKey":"SEND_ZIP_CODE"},
										{"label":"联系人","valueKey":"SEND_NAME"},
										{"label":"联系电话","valueKey":"SEND_LINKMAN_PHONE"},
										{"label":"所属省份","valueKey":"ADDRESS_STATE_SEND"},
										{"label":"所属国家","valueKey":"SEND_CCID"}
									],
									obj.DATA);
							}
						}
					]
				},

				cols2:
				{
						tag:"div",
						children:[
						{
								tag:"img",
								src: getSrc(),
						},
						{
							tag:"fieldset",
							children:
							[
								
								{
									tag:"legend",
									html:"运输基础信息"
								},
								{
									tag:"div",
									class:"ulDiv",
									html:function(obj,index)
									{

										return Layout.ul_label_value(
											[
												{"label":"运输公司","valueKey":"TRANSPORT_WAYBILL_NAME"},
												{"label":"运输方式","valueKey":"TRANSPORTBY"},
												{"label":"发货港","valueKey":"TRANSPORT_SEND_PLACE"},
												{"label":"运单号","valueKey":"TRANSPORT_WAYBILL_NUMBER"},
												{"label":"承运公司","valueKey":"CARRIERS"},
												{"label":"目的港","valueKey":"TRANSPORT_RECEIVE_PLACE"},

											],
											obj.DATA);
									}
								}
							]
						},]
				},
				cols3:
				{
					tag:"fieldset",
					children:
					[
						{
							tag:"legend",
							html:"流程基础信息"
						},
						{
							tag:"div",
							class:"ulDiv",
							html:function(obj,index)
							{

								return Layout.ul_label_value(
									[
										{"label":"门牌号","valueKey":"DELIVER_HOUSE_NUMBER"},
										{"label":"街道","valueKey":"DELIVER_STREET"},
										{"label":"城市","valueKey":"DELIVER_CITY"},
										{"label":"邮编","valueKey":"DELIVER_ZIP_CODE"},
										{"label":"联系人","valueKey":"TRANSPORT_LINKMAN"},
										{"label":"联系电话","valueKey":"TRANSPORT_LINKMAN_PHONE"},
										{"label":"所属省份","valueKey":"ADDRESS_STATE_DELIVER"},
										{"label":"所属国家","valueKey":"DELIVER_CCID"}
									],
									obj.DATA);
							}
						}
					]
				},
			};
			return Layout.oneRow_table(data,{tdName:tdName,tdConfig:tdConfig});
		};

		return productBasicInfo(tempData)+productDetailInfo(data);
		
	}

});






