define(
["jquery","backbone","../../templates","../model/transportModel",
"../../transport_basic_info","../../transport_product_info","../../productList"],
function($,Backbone,templates,TransportModel,BasicInfo,ProductInfo,ProductList)
{
	return Backbone.View.extend(
	{
			ela:"#transport_basic_info",
			elb:"#transport_info",
			elc:"#productList",
			model:new TransportModel(),
			initialize:function(options)
			{
				//this.resultView = options.resultView;
			},
			
			render:function(data)
			{
				var temp = this;
				this.model.fetch(
				{
					success:function(response)
					{
						var data = response.toJSON();
						//基础信息页面
						$(temp.ela).html(BasicInfo(data));
						//货单信息页面
						$(temp.elb).html(ProductInfo(data));
						//收缩条
						//this.$elc.html();
						//货物信息
						$(temp.elc).html(ProductList(data));
						
					}
				});
				
			},
		
	});

});