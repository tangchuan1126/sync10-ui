define(
["jquery","backbone","../../templates","../model/transportModel","handlebars"],
function($,Backbone,templates,TransportModel)
{
	return Backbone.View.extend(
	{
			el:"#transport_info",
			template:templates.productInfo,
			initialize:function(options)
			{
				//this.resultView = options.resultView;
			},
			
			render:function(data)
			{
				this.$el.html(this.template({
					transportDetail:data,
				}));
			},
		
	});

});