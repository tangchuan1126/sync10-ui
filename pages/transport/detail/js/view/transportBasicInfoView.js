define(
["jquery","backbone","../../templates","../model/transportModel","./productInfoView","handlebars"],
function($,Backbone,templates,TransportModel,ProductInfoView)
{
	return Backbone.View.extend(
	{
			el:"#transport_basic_info",
			template:templates.transportBasicInfo,
			model:new TransportModel(),
			initialize:function(options)
			{
				//this.resultView = options.resultView;
			},
			events:
			{
				"click #filter" : "filter"
			},
			render:function()
			{
				var v = this;
				this.model.fetch(
				{
					success:function(response)
					{
						v.$el.html(v.template(
						{
							transportDetail:response.toJSON(),
						}));
						new ProductInfoView().render(response.toJSON());
					}
				});

			},

			filter:function()
			{
				this.resultView.render(this.queryOptions);
				//new TransportList(queryOptionsa);
			}
	});

});