define
(
["jquery","backbone","../../urlConfig"],
function($,Backbone,config)
{
	return Backbone.Model.extend(
	{
		url:config.transportDetail.url,
		initialize:function()
		{

		}
	});
}
);