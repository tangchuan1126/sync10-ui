(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['productInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<table class=\"basicTable\">\r\n	<tr>\r\n		<td>\r\n			<span class=\"nameClass\">运单号:</span>\r\n			<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n		</td>\r\n		<td>\r\n			<span class=\"nameClass\">TITLE:</span>\r\n			<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n		</td>\r\n		<td>\r\n			<span class=\"nameClass\">货物状态:</span>\r\n			<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n		</td>\r\n	</tr>\r\n</table>\r\n<table class=\"basicInfoTable\" id=\"basicInfoTable\">\r\n	<tr>\r\n		<td >\r\n			<fieldset>\r\n				<legend>运单基础信息</legend>\r\n				<ul>\r\n					<li>\r\n						<span class=\"nameClass\">运单号:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">TITLE:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">创建人:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">创建时间:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">ETD:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">允许装箱:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n				</ul>\r\n			</fieldset>\r\n		</td>\r\n		<td >\r\n			<fieldset>\r\n				<legend>运输基础信息</legend>\r\n				<ul>\r\n					<li>\r\n						<span class=\"nameClass\">货物状态:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">出口报关:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">进口清关:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">运费状态:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n				</ul>\r\n			</fieldset>\r\n		</td>\r\n		<td >\r\n			<fieldset>\r\n				<legend>流程基础信息</legend>\r\n				<ul>\r\n					<li>\r\n						<span class=\"nameClass\">制签流程:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">第三方标签:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">实物图片:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">质检流程:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">单证流程:</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n				</ul>\r\n			</fieldset>\r\n		</td>\r\n	</tr>\r\n</table>";
},"useData":true});
templates['transportBasicInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<table class=\"basicTable\">\r\n	<tr>\r\n		<td>\r\n			<span class=\"nameClass\">运单号</span>\r\n			<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n		</td>\r\n		<td>\r\n			<span class=\"nameClass\">TITLE</span>\r\n			<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n		</td>\r\n		<td>\r\n			<span class=\"nameClass\">货物状态</span>\r\n			<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n		</td>\r\n	</tr>\r\n</table>\r\n<table class=\"basicInfoTable\">\r\n	<tr>\r\n		<td >\r\n			<fieldset>\r\n				<legend>运单基础信息</legend>\r\n				<ul>\r\n					<li>\r\n						<span class=\"nameClass\">运单号</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">TITLE</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">创建人</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">创建时间</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">ETD</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">允许装箱</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n				</ul>\r\n			</fieldset>\r\n		</td>\r\n		<td >\r\n			<fieldset>\r\n				<legend>运输基础信息</legend>\r\n				<ul>\r\n					<li>\r\n						<span class=\"nameClass\">货物状态</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">出口报关</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">进口清关</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">运费状态</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n				</ul>\r\n			</fieldset>\r\n		</td>\r\n		<td >\r\n			<fieldset>\r\n				<legend>流程基础信息</legend>\r\n				<ul>\r\n					<li>\r\n						<span class=\"nameClass\">制签流程</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">第三方标签</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">实物图片</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">质检流程</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n					<li>\r\n						<span class=\"nameClass\">单证流程</span>\r\n						<span class=\"valueClass\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\r\n					</li>\r\n				</ul>\r\n			</fieldset>\r\n		</td>\r\n	</tr>\r\n</table>";
},"useData":true});
})();