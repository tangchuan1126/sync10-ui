define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['productInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"basicTable\">\n	<tr>\n		<td>\n			<span class=\"nameClass\">运单号:</span>\n			<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n		</td>\n		<td>\n			<span class=\"nameClass\">TITLE:</span>\n			<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n		</td>\n		<td>\n			<span class=\"nameClass\">货物状态:</span>\n			<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n		</td>\n	</tr>\n</table>\n<table class=\"basicInfoTable\" id=\"basicInfoTable\">\n	<tr>\n		<td >\n			<fieldset>\n				<legend>运单基础信息</legend>\n				<ul>\n					<li>\n						<span class=\"nameClass\">运单号:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">TITLE:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">创建人:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">创建时间:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">ETD:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">允许装箱:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n				</ul>\n			</fieldset>\n		</td>\n		<td >\n			<fieldset>\n				<legend>运输基础信息</legend>\n				<ul>\n					<li>\n						<span class=\"nameClass\">货物状态:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">出口报关:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">进口清关:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">运费状态:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n				</ul>\n			</fieldset>\n		</td>\n		<td >\n			<fieldset>\n				<legend>流程基础信息</legend>\n				<ul>\n					<li>\n						<span class=\"nameClass\">制签流程:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">第三方标签:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">实物图片:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">质检流程:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">单证流程:</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n				</ul>\n			</fieldset>\n		</td>\n	</tr>\n</table>";
},"useData":true});
templates['transportBasicInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"basicTable\">\n	<tr>\n		<td>\n			<span class=\"nameClass\">运单号</span>\n			<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n		</td>\n		<td>\n			<span class=\"nameClass\">TITLE</span>\n			<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n		</td>\n		<td>\n			<span class=\"nameClass\">货物状态</span>\n			<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n		</td>\n	</tr>\n</table>\n<table class=\"basicInfoTable\">\n	<tr>\n		<td >\n			<fieldset>\n				<legend>运单基础信息</legend>\n				<ul>\n					<li>\n						<span class=\"nameClass\">运单号</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">TITLE</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">创建人</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">创建时间</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">ETD</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">允许装箱</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n				</ul>\n			</fieldset>\n		</td>\n		<td >\n			<fieldset>\n				<legend>运输基础信息</legend>\n				<ul>\n					<li>\n						<span class=\"nameClass\">货物状态</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">出口报关</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">进口清关</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">运费状态</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n				</ul>\n			</fieldset>\n		</td>\n		<td >\n			<fieldset>\n				<legend>流程基础信息</legend>\n				<ul>\n					<li>\n						<span class=\"nameClass\">制签流程</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TRANSPORT_ID : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">第三方标签</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">实物图片</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.CREATE_ACCOUNT : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">质检流程</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n					<li>\n						<span class=\"nameClass\">单证流程</span>\n						<span class=\"valueClass\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.transportDetail : depth0)) != null ? stack1.TITLE_NAME : stack1), depth0))
    + "</span>\n					</li>\n				</ul>\n			</fieldset>\n		</td>\n	</tr>\n</table>";
},"useData":true});
return templates;
});