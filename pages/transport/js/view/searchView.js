"use strict";
define(["jquery","backbone",'handlebars',"../../templates","../../config", "auto","../../transportListMain"],
	function($,Backbone,HandleBars,templates,config,auto,TransportList)
	{
		return Backbone.View.extend(
		{
			template:templates.search_transport,
			queryOptions:
			{
				el:"#data",
				url:config.transportModelCollection.url,
				queryCondition:{}
			},
			initialize:function(opts)
			{
				   this.setElement(opts.el);
				   this.searchUrl = opts.searchUrl;
				   this.resultView = opts.resultView
			},
			render:function()
			{
				var temp = this;
				
				//console.log(temp.template({}));
				
				temp.$el.html(temp.template({}));

				 //注册键盘事件事件
				 $("#search_key").bind("keydown",function(evt)
				 {
               		 if(evt.keyCode==13)
               		 {
                   		temp.search();
              		 }
              	 });
              	 
				 return this;
			},

			events:
			{
				"click #eso_search":"search"
			},

			search:function(evt)
			{
				
				var val = $("#search_key").val();
				if (val=="")
				{
					alert("你好像忘记填写关键词了？");
				}
				else
				{
					val = val.replace(/\'/g,'');
					$("#search_key").val(val);
					this.queryOptions.queryCondition={"key":val};

					//var queryOptions = {el: "#data", url: "/Sync10-ui/pages/temp/sendPS.json",queryCondition:{}};
					this.resultView.setQuery({"key":val});
					//this.resultView.render(this.queryOptions);
					//new TransportList(this.queryOptions);
				}
			}
		});
	}
);