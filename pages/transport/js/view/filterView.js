define(
["jquery","backbone","../../config","../../templates","../model/filterModel","../../transportListMain","handlebars"],
function($,Backbone,config,templates,FilterModel,TransportList)
{
	return Backbone.View.extend(
	{
			el:"#transport_filter",
			template:templates.filter_transport,
			model:new FilterModel(),
			initialize:function(options)
			{
				this.resultView = options.resultView;
			},
			events:
			{
				"click #filter" : "filter"
			},
			queryOptions:
			{
				el: "#data", 
				url: "/Sync10-ui/pages/temp/sendPS.json",
				queryCondition:{}
			},
			render:function()
			{
				var v = this;

				this.model.fetch(
				{
					data:{},
					success:function(response)
					{
						v.$el.html(v.template(
						{
							send_ps:response.toJSON().send_ps,
							recive_ps:response.toJSON().recive_ps
						}));
					}
				});

			},

			filter:function()
			{
				this.resultView.render(this.queryOptions);
				//new TransportList(queryOptionsa);
			}
	});

}
);