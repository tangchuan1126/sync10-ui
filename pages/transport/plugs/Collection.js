define(
["jquery","backbone","./Model"],
function($,Backbone,Model)
{
		return Backbone.Collection.extend(
		{
			Model:Model,
			initialize:function(url)
			{
				this.url = url;
			} ,
			parse:function(response)
			{
           		 return response;
       		}

		}
		);
});