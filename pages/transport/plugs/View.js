define(
["jquery","backbone","./Model"],
function($,Backbone,Model)
{
	return Backbone.View.extend(
	{
			//页面渲染数据源，支持从外部传入数据，用于多个view使用同一个数据源，不用每个都去请求服务器
			//url:"/Sync10-ui/pages/temp/transportDetail.json",
			initialize:function()
			{
				var viewConfig = arguments[0];
				var renderOptions =  arguments[1];
				if(typeof renderOptions  == "object")
				{
					this.el = viewConfig.el;
				};

				if(typeof renderOptions  == "object")
				{
					this.viewConfig = viewConfig;
					this.adapter = viewConfig.adapter;
					this.dataSource = renderOptions.dataSource;
					this.renderType="dataSource";
				}
				
			},

			render:function()
			{
				//直接渲染模式
				if(this.renderType=="dataSource")
				{
					this.$el.html(this.adapter(dataSource));
				}
			},
	});

});