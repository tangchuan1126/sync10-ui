define(["jquery","JSONTOHTML","./Util"], 
function($,JSONTOHTML,Util)
{
	return function()
	{
		var Layout = 
		{
			//orderArr 格式[{"label":"运单号","valueKey":"RECEIVE_PSNAME"}],
			// 布局
			ul_label_value:function(orderArr,dataObj)
			{
				return this.f_ul_layout(orderArr,dataObj,templates.t_ul_lable_value);
			},

			ul_value_title:function(orderArr,dataObj)
			{
				return this.f_ul_layout(orderArr,dataObj,templates.t_ul_value_title);
			},
			//产生一个 ul 
			//orderArr ：lable --value 数组
			//dataObj ：数据源
			//f_ 一般函数
			f_ul_layout:function(orderArr,dataObj,template)
			{
				for(var len=orderArr.length,i=0;i<len;i++)
				{
					//获取lable
					//var label = orderArr[i].label;
					var valueKey = orderArr[i].valueKey;
					var value ="";
					if(typeof dataObj == "object")
					{
						value = dataObj[valueKey];
					}
					else if(typeof dataObj == "array")
					{
						for(var length=dataObj.length,j=0;j<length;j++)
						{
							if(dataObj[j].hasOwnProperty(valueKey))
							{
								//... 暂不支持数组排序
							}
						}
					}
					orderArr[i].value=value;
				}

				return JSONTOHTML.transform(orderArr, template);
			},
			//单行table布局
			oneRow_table:function(data,tableConfig)
			{
				//将配置传入data中
				data.tdName= tableConfig.tdName;
				data.tdConfig = tableConfig.tdConfig;
				data.tableId = tableConfig.tableId;
				return JSONTOHTML.transform(data, templates.t_oneRow_table);
			},
			mutiRow_caption_table:function(data,tableConfig)
			{
				var tempData = data;
				tempData.tableConfig = tableConfig;
				return JSONTOHTML.transform(tempData, templates.t_mutiRow_caption_table);
			},

		};
		
		//============================================template==========================//
		var templates ={	
			//t_  template
			t_ul_lable_value:
			{
				tag: "ul",
				html: function(obj, index) 
				{
					return (JSONTOHTML.transform(obj, templates.t_li_lable_value));
				}
			},
			t_li_lable_value:
			{
				tag: "li",
				html: function(obj, index) 
				{
					return '<span class="namestyle">'+obj.label+':&nbsp;&nbsp;</span><span class="valstyle">' + Util.emptyHandle(obj.value,"") + '</span>';
				}
			},

			t_ul_value_title:
			{
				tag: "ul",
				html: function(obj, index) 
				{
					return (JSONTOHTML.transform(obj, templates.t_li_value_title));
				}
			},
			t_li_value_title:
			{
				tag: "li",
				html: function(obj, index) 
				{
					return '<span  titile="'+obj.title+'">' + Util.emptyHandle(obj.value,"") + '</span>';
				}
			},

			//***************单行table模板*******************//
			//单行table布局
			t_oneRow_table:
			{
				tag:"table",
				class:"dataTable",
				id:function(obj,index)
				{
					return obj.tableId;
				},
				html:function(obj,index)
				{
					return JSONTOHTML.transform(obj,templates._tr);
				}
			},
			//行布局 ,需要全局参数列名数组，和列布局
			_tr:
			{
				tag:"tr",
				html:function(obj,index)
				{
					var tdHtml  = "";
					for(var i = 0 ;i<obj.tdName.length;i++)
					{
						var colsName = obj.tdName[i];
						var template = eval("obj.tdConfig."+colsName);
						tdHtml+="<td class='tdclass'>"
						tdHtml+= JSONTOHTML.transform(obj,template);
						tdHtml+="</td>"
					}
					return tdHtml;
				}
			},
			t_mutiRow_caption_table:
			{
				tag:"table",
				id:function(obj,index)
				{
					return obj.tableConfig.tableId;
				},
				children:
				[
					{
						tag:"caption",
						class:"tcaption",
						html:function(obj,index)
						{
							return obj.tableConfig.caption(obj);
						}
					},
					{
						tag:"thead",
						class:"thead",
						html:function(obj,index)
						{
							return obj.tableConfig.thead(obj);
						}
					},
					{
						tag:"tbody",
						html:function(obj,index)
						{
							return obj.tableConfig.tbody(obj);
						}
					}
				]
			}
			//**************************************//
		}	
		return Layout;
	}();
});