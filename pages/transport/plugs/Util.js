define(["jquery"], function($)
{
	return {
		//处理空字符串
		emptyHandle:function(str,defaultValue)
		{
			if(typeof str !="undefined" && str !="")
			{
					return str;
			}
			else
			{
				return defaultValue;
			}
		},
	};
});	
	