define
(
["jquery","backbone"],
function($,Backbone)
{
	return Backbone.Model.extend(
	{
		//url:config.filterModel.url,
		initialize:function(options)
		{
			this.url = options.url;
		}
	});
}
);