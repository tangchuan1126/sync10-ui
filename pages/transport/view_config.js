define(["./jsontohtml_templates"], function(tmp) {
	return {
		"View_control": {
			"head": [{
				"title": "转运单基本信息",
				"field": {"name":"basicInfo"}
			}, {
				"title": "库房及运输信息",
				"field": {"name":"transportInfo"}
			}, {
				"title": "流程信息",
				"field": "FLOWS"
			}, {
				"title": "跟进",
				"field": "LOGS"
			}
			],
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"footer": [
				[
					"events.OUTBOUND", "装货图片"
				],
				[
					"events.FOLLOWUP", "跟进备货中"
				],
				[
					"events.BOOKDOORORLOCATION", "装货司机签到"
				]
			],
			"templates": tmp
		}
	}
});