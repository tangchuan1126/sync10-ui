(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['filter_transport'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "    				<option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing, buffer = "<table class=\"filter\" >\r\n	<tr>\r\n		<td>\r\n			<select >\r\n				<option>转运仓库</option>\r\n";
  stack1 = ((helper = (helper = helpers.send_ps || (depth0 != null ? depth0.send_ps : depth0)) != null ? helper : helperMissing),(options={"name":"send_ps","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.send_ps) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>目的仓库</option>\r\n";
  stack1 = ((helper = (helper = helpers.recive_ps || (depth0 != null ? depth0.recive_ps : depth0)) != null ? helper : helperMissing),(options={"name":"recive_ps","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.recive_ps) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>货物状态</option>\r\n";
  stack1 = ((helper = (helper = helpers.product_state || (depth0 != null ? depth0.product_state : depth0)) != null ? helper : helperMissing),(options={"name":"product_state","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.product_state) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>运费流程</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>选择部门</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>选择职员</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td>\r\n			<select >\r\n				<option>出口报关</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>进口清关</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>发票流程</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\r\n		</td>\r\n		<td>\r\n			<select >\r\n				<option>退税流程</option>\r\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : helperMissing),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.stock_in_set) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</select>\r\n		</td>\r\n		<td>\r\n			<button id=\"filter\">filter</button>\r\n		</td>\r\n	</tr>\r\n</table>";
},"useData":true});
templates['followup'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<table>\r\n	<tr>\r\n		<td>\r\n			当前状态：\r\n		</td>\r\n		<td>\r\n			运输中\r\n		</td>\r\n		<td colspan=\"4\"></td>\r\n	</tr>\r\n	<tr>\r\n		<td>\r\n			跟进流程：\r\n		</td>\r\n		<td>\r\n			<select>\r\n				<option>货物状态</option>\r\n			</select>\r\n		</td>\r\n		\r\n		<td>\r\n			预计本阶段完成时间：\r\n		</td>\r\n		<td>\r\n			<input/>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td >\r\n			备注\r\n		</td>\r\n		<td colspan=\"5\">\r\n			<textarea id=\"testID\" rows=\"5\" cols=\"60\" ></textarea>\r\n		</td>\r\n	</tr>\r\n</table>";
  },"useData":true});
templates['search_transport'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<table width=\"100%\" height=\"61\" border=\"0\" cellpadding=\"0\"cellspacing=\"0\">\r\n	<tr>\r\n		<td width=\"30%\" style=\"padding-top: 3px;\">\r\n			<div id=\"easy_search_father\"> \r\n				<div id=\"easy_search\">\r\n					<img id=\"eso_search\" src=\"./imgs/easy_search.gif\" width=\"70\" height=\"29\" border=\"0\" />  \r\n				</div>\r\n			</div>\r\n				<table width=\"485\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n					<tr>\r\n						<td width=\"418\">\r\n							<div class=\"search_shadow_bg\">\r\n								<input name=\"search_key\" type=\"text\" class=\"search_input\" style=\"font-size: 17px; font-family: Arial; color: #333333\" id=\"search_key\" />\r\n							</div>\r\n						</td>\r\n						<td width=\"67\"></td>\r\n					</tr>\r\n				</table> \r\n		</td>\r\n		<td width=\"33%\"></td>\r\n		<td width=\"24%\"></td>\r\n	</tr>\r\n</table>";
  },"useData":true});
templates['transport_list'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			<tr height=\"30px\" data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.transport_id : depth0), depth0))
    + "\">\r\n				<td>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.transport_id : depth0), depth0))
    + "\r\n				</td>\r\n				<td>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.transport_name : depth0), depth0))
    + "\r\n				</td>\r\n				<td>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.transport_name : depth0), depth0))
    + "\r\n				</td>\r\n				<td>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.transport_name : depth0), depth0))
    + "\r\n				</td>\r\n			</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing, buffer = "<table id=\"dataTable\" width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"zebraTable\" isNeed=\"true\" isBottom=\"true\">\r\n	<thead id=\"tableHead\" width=\"100%\">\r\n		<tr>\r\n			<th width=\"25%\" nowrap=\"nowrap\" class=\"right-title\"\r\n						style=\"vertical-align: center; text-align: center;\">转运单基本信息</th>\r\n			<th width=\"27%\" nowrap=\"nowrap\" class=\"right-title\"\r\n						style=\"vertical-align: center; text-align: center;\">库房及运输信息</th>\r\n			<th width=\"18%\" nowrap=\"nowrap\" class=\"right-title\"\r\n						style=\"vertical-align: center; text-align: center;\">流程信息</th>\r\n			<th width=\"20%\" nowrap=\"nowrap\" class=\"right-title\"\r\n						style=\"vertical-align: center; text-align: center;\">跟进</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n";
  stack1 = ((helper = (helper = helpers.transport || (depth0 != null ? depth0.transport : depth0)) != null ? helper : helperMissing),(options={"name":"transport","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.transport) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</tbody>\r\n</table>			\r\n";
},"useData":true});
})();