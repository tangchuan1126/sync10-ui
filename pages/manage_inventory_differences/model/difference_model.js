"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "/Sync10-ui/lib/DateUtil.js"
  
], function(config, $, Backbone, Handlebars,date) {

		  
      
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "SAA_ID",
          percentage_completed: 0,

          parse:function(data){

            var dateString = data.CREATED_AT;
            var date = dateString.replace(/-/g, "/").split(".")[0];
            data["CREATED_AT"] = new Date(date).format("yyyy-MM-dd");
            if(data.PROCESSED_AT){
              dateString = data.PROCESSED_AT;
              date = dateString.replace(/-/g, "/").split(".")[0];
              data["PROCESSED_AT"] = new Date(date).format("yyyy-MM-dd");
            }
            if(!data["TITLE_NAME"]){data["TITLE_NAME"]="--";}
            switch(data.STATUS){
              case 1:
                data["STATUS_STRING"]="Pending";
                data["PROCESSED_AT"] = "--";
                data["PROCESSED_BY_NAME"] = "--"
                break;
              case 2:
                data["STATUS_STRING"] = "Approved";
                break;
              case 3:
                data["STATUS_STRING"] = "Rejected";
                break;

            }
            return data;
          }
       });
       
      var DifferenceCollection =  Backbone.Collection.extend({
         model: DifferenceModel,
         url: config.getAllDifferences,
          ps_id:0,
          area_id:0,
          location_id:0,
          approve_status:0,
          sortby:"id",
          sort:"desc",
          pageNo:1,
          pageSize:10,
          title_id:0,

           initialize: function(params){
            
            
            if(params.ps_id){this.ps_id=parseInt(params.ps_id);}
            if(params.area_id){this.area_id=parseInt(params.area_id)}
            if(params.location_id){this.location_id=parseInt(params.location_id)}
            if(params.approve_status){this.approve_status=parseInt(params.approve_status)}
            if(params.sortby){this.sortby=params.sortby}
            if(params.sort){this.sort=params.sort}
            if(params.pageNo){this.pageNo=parseInt(params.pageNo)}
            if(params.pageSize){this.pageSize=parseInt(params.pageSize)}
            if(params.title_id){this.title_id=parseInt(params.title_id)}

            this.url = this.url+"?warehouse_id="+this.ps_id+"&area_id="+this.area_id+"&location_id="+this.location_id+
            "&status="+this.approve_status+"&sort_by="+this.sortby+"&title_id="+this.title_id+
            "&sort_direction="+this.sort+"&pageNo="+this.pageNo+"&pageSize="+this.pageSize;
              
          },
          parse:function(response){
              
              if(response.PAGECTRL){this.pageCtrl = response.PAGECTRL;}
              return response.DATA;
              //return response;
            }
          },
            {
              pageCtrl:{
                pageNo:1,
                pageSize:10
          },
    
       });

      return {
	      DifferenceModel:DifferenceModel,
	      DifferenceCollection:DifferenceCollection,

	    };




}); //page_init