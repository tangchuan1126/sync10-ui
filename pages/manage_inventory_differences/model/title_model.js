"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var TitleImmyModel = Backbone.Model.extend({
          idAttribute: "TITLE_ID",

          parse: function(data){
            
            this.text = data.TITLE_NAME ;
            this.value = data.TITLE_ID;
            return this;
          }
        
       });
       
      var TitleImmyCollection =  Backbone.Collection.extend({
           model: TitleImmyModel,
           url: config.getAllTitlesJSON,
           initialize:function(ps_id){
              this.url= this.url+"?ps_id="+ps_id;
           }
       });

      return {
	      TitleImmyModel:TitleImmyModel,
	      TitleImmyCollection:TitleImmyCollection,

	    };




}); //page_init