"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "SAL_ID",

       });
       
      var DifferenceCollection =  Backbone.Collection.extend({
           model: DifferenceModel,
           url: config.getLocationDifferencesJSON,
           selected:"",
           
           initialize: function(saa_id){
              this.url = this.url+saa_id;
              
          }

       });

      return {
	      DifferenceModel:DifferenceModel,
	      DifferenceCollection:DifferenceCollection,

	    };




}); //page_init