define([
    './config',
    'jquery',
    'backbone',
    'handlebars',
    'templates',
    "./view/storage_list_box",
    "./view/differences_list",
    "./view/filter_button",
     "art_Dialog/dialog",
    "bootstrap",

],
function(config,$,Backbone,Handlebars,templates, storageListBoxView,differenceListView, buttonView, dialog){
    
    $(function(){
        var loading = dialog();
        loading.showModal();
    	var storageListBox = new storageListBoxView();
    	storageListBox.render();
        new buttonView().render();
        new differenceListView().render({});

        loading.close().remove();
    }); 
    
});
