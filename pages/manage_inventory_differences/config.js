define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getAreaForStorageJSON: "/Sync10/action/administrator/storage_location/GetLocationAreasJSON.action?ps_id=",
    getAllDifferences: "/Sync10/_inventoryControl/search/differenceList",
    getDifferenceDetail: "/Sync10/_inventoryControl/search/differenceDetail",
    processDifference:"/Sync10/_inventoryControl/update/difference",
    getLocationsOfArea: "/Sync10/_inventoryControl/search/getLocationsOfArea?area_id=",
    getAllTitlesJSON: "/Sync10/_inventoryControl/search/titles",
   
});
