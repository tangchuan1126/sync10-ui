"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/area_model.js",
  "../model/location_model.js",
  "./location_list_box.js",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models,location_models,locationView, ImmyBox) {
    
    return Backbone.View.extend({
      el:"#area_list_box_container",
      template:templates.area_list_box,
      
      render:function(){

        var dis = this;
        
          dis.collection.fetch({dataType: "json",async: false});
          
          dis.$el.html(dis.template());

          var areaImmydata = {
            renderTo: "#area_list_box_container",
            dataUrl: dis.collection,
            placeHolder:"Any", 
            inputId: "#area_list_box",
            selectedInputId: "#selected_area_id"
          };

          var areaImmy = new ImmyBox(areaImmydata);
          areaImmy.on("events.change",function(){dis.pull_loc();});
          areaImmy.render();
          dis.pull_loc();
        
        
      },
      pull_loc:function(){
        
        var val = $("#selected_area_id").val();
        var col = new location_models.LocationImmyCollection(val);
        var areas = new locationView({collection:col});
        areas.render();
        
      }
      
    });

}); 