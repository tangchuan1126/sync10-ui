"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "./differences_list.js",
      "../js/ImmyboxControl",
  "art_Dialog/dialog",

  "bootstrap",
  
], function( config, $, Backbone, Handlebars, templates, differencesView ,ImmyBox,dialog) {
    
    var ButtonView =  Backbone.View.extend({
      template: templates.filter_button,
      el:"#filter_button_container",
     

      render:function(){
        var dis = this;
        var html =dis.template();
        dis.$el.html("");
        dis.$el.html(html);
        
       
        

        $("#filter_btn").click(function(evt){dis.filter_btn_click(evt)});
        $("#filter_btn_bk").click(function(evt){dis.filter(evt)});
     },
     events:{
     
     },
     filter_btn_click:function(evt){
        var dis = this;

          $("#filtered_storage_id").val($("#selected_storage_id").val());
          $("#filtered_area_id").val($("#selected_area_id").val());
          $("#filtered_location_id").val($("#selected_location_id").val());
          $("#filtered_status").val($("#selected_status").val());
          $("#filtered_title_id").val($("#selected_title_id").val());
        dis.filter();


     },
     filter:function(evt){
        
        var filtered_storage_id = $("#filtered_storage_id").val();
        var filtered_area_id = $("#filtered_area_id").val();
        var filtered_location_id = $("#filtered_location_id").val();
        var filtered_title_id = $("#filtered_title_id").val();
        var filtered_status = $("#filtered_status").val();
        console.log("title"+filtered_title_id)
        var loading = dialog();
        loading.showModal();
        new differencesView().render({ps_id:filtered_storage_id,area_id:filtered_area_id,location_id:filtered_location_id,
          approve_status:filtered_status,sortby:"",sort:"",pageNo:1,title_id:filtered_title_id})
        loading.close().remove();
        
     }
      
  });
return ButtonView;
}); 

