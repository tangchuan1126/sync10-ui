"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/area_model.js",
  "../model/location_model.js",
  "./location_list_box.js",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models,location_models,locationView, ImmyBox) {
    
    return Backbone.View.extend({
      el:"#title_list_box_container",
      template:templates.title_list_box,
      collection:null,

      initialize:function(data){
        var dis = this;
        dis.collection = data.collection;
      },
      render:function(){

        var dis = this;
        
        dis.collection.fetch({dataType: "json",async: false});
          
          dis.$el.html(dis.template());

          var titleImmydata = {
            renderTo: "#title_list_box_container",
            dataUrl: dis.collection,
            placeHolder:"Any", 
            inputId: "#title_list_box",
            selectedInputId: "#selected_title_id"
          };

          var titleImmy = new ImmyBox(titleImmydata);
          titleImmy.on("events.change",function(){});
          titleImmy.render();

        
        
      },
      
      
    });

}); 