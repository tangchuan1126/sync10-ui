"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/location_model.js",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models, ImmyBox) {
    
    return Backbone.View.extend({
      el:"#location_list_box_container",
      template:templates.location_list_box,
      
      render:function(){

        var dis = this;
        
          dis.collection.fetch({dataType: "json",async: false});
          
          dis.$el.html(dis.template());
          
          var locImmydata = {
            renderTo: "#location_list_box_container",
            dataUrl: dis.collection,
            placeHolder:"Any", 
            inputId: "#location_list_box",
            selectedInputId: "#selected_location_id"
          };

          var locationImmy = new ImmyBox(locImmydata);
          locationImmy.on("events.change",function(){});
          locationImmy.render();
          
      }
      
    });

}); 