"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/count_difference_model.js",
  /*"../model/approve_request_model.js",*/
  "art_Dialog/dialog",
  "bootstrap_tab",

], function( config, $, Backbone, Handlebars, templates, HandleBarsExt, difference_models/*, approve_model*/, dialog) {

    var CountDifferencesView =  Backbone.View.extend({
      template: templates.count_differences,
      el:"#location_differences_container",
      
      initialize:function(data){
          var dis = this;
          dis.differenceId = data.differenceId;
          dis.parentView = data.parentView;
      },
      
      render:function(){

        var dis = this;
        dis.collection= new difference_models.DifferenceCollection({differenceId:dis.differenceId});
        dis.collection.fetch({dataType: "json",async: false});
        
        //add containers tabs
        var Containershtml =templates.container_differences_list({differences: dis.collection.toJSON()});
        dis.$el.html("");
        dis.$el.show();
        dis.$el.html(Containershtml);

        // add containers and products differences
        $.each(dis.collection.toJSON(),function(index,item){

          var any=false;var systemContainerTree,physicalContainerTree;
          if(item.containerDifference.toJSON().systemContainerTree){
            var systemContainerTree = item.containerDifference.toJSON().systemContainerTree.toJSON();
            any = true;
          }
          if(item.containerDifference.toJSON().physicalContainerTree){
            var physicalContainerTree = item.containerDifference.toJSON().physicalContainerTree.toJSON();
            any = true;
          }
          var level=0;

          var c = item.containerDifference.createDifference();
          console.log("conDiffId",item.containerDifference.toJSON().ID)
          var conDiffId = item.containerDifference.toJSON().ID;
          if(item.containerDifference.toJSON().systemContainerTree || item.containerDifference.toJSON().physicalContainerTree){
            var containersTreeshtml = templates.container_tree_differences({title:item.TITLE_NAME,conDiffId:conDiffId,status:item.containerDifference.toJSON().STATUS,containerDifference:item.containerDifference.toJSON(), systemTree: systemContainerTree, physicalTree:physicalContainerTree, calculatedDiffs:c.toJSON()});
            $("#"+item.CONTAINER).append(containersTreeshtml);
          }
          
          var pstatus = 2;var username="";var prodDiffId,processed_by,processed_by_title,comments;
          if(item.productDifferences){
            
              $.each(item.productDifferences.toJSON(),function(index,item){
                username = item.USER_NAME;
                if(item.STATUS==1){
                  pstatus=1;
                  
                }else{
                  processed_by = item.PROCESSED_BY_NAME;
                  comments = item.COMMENTS;
                  if(item.STATUS ==2){
                    
                      processed_by_title = "Approved By: ";
                  }else{
                      
                      processed_by_title = "Rejected By: ";
                  }
                }
                
                
                prodDiffId = item.DIFFERENCE_ID;
              });
            
          }

          var productsTreehtml = templates.count_differences({title:item.TITLE_NAME,comments:comments,processed_by_title:processed_by_title,processed_by:processed_by,productDiffId:prodDiffId,username:username,status:pstatus,productDifferences:item.productDifferences.toJSON()});
          $("#"+item.CONTAINER).append(productsTreehtml);
        });
        var selectedTab = -1;
        var selectedContent = -1;
        $.each(dis.collection.toJSON(),function(index,item){
          
          if(selectedTab==-1 && item.STATUS==1){
            selectedTab = item.CONTAINER_ID;
            selectedContent = item.CONTAINER;
          }
          var className = ""
          switch(item.STATUS){
            case 1:
              className = "";
              break;
            case 2:
              className = "approved_container";
              break;
            case 3:
              className = "rejected_container";
              break;
            

          }
          
          
          $("#"+item.CONTAINER_ID).addClass(className);
        });
        if(selectedTab==-1 && dis.collection.length >0){
          var item = dis.collection.models[0].toJSON();
          selectedTab = item.CONTAINER_ID;
          selectedContent = item.CONTAINER;
        }
        $("#"+selectedTab).addClass("active");
        $("#"+selectedContent).addClass("active");

        if($("a[name='approve_btn']")){
          
          $("a[name='approve_btn']").click(function(evt){dis.approve_difference(evt);});
        }
        if($("a[name='reject_btn']")){
          
          $("a[name='reject_btn']").click(function(evt){dis.reject_difference(evt);});
        }
        $("#back_btn_view_detail").click(function(evt){dis.back(evt)});
        
     },
     back:function(evt){
        var dis = this;
        $("#location_differences_container").hide();
        $("#area_differences").show();
        
     },
     process:function(diff_ids,status,comments){
      var dis = this;
      var data = {"difference_id":parseInt(diff_ids), "status":status, "comments":comments}
        $.ajax({
            type: "put",
            url: config.processDifference+"?difference_id="+parseInt(diff_ids)+"&status="+status+"&comments="+comments,
            contentType: 'application/json; charset=UTF-8',
            timeout: 60000,
            cache:false,
            dataType: 'json',
            async:true,
            success: function(){dis.back();dis.refresh_list();},
            error:function(xhr, ajaxOptions, thrownError){
                console.log("error",xhr.responseText);
            },
           
            
          });
     },
     reject_difference:function(evt){
        var dis = this;
        evt.preventDefault();
        var parent = $(evt.target).parent().parent().parent();
        var diff_id = dis.differenceId;
        console.log(parent);
        var comments =$(parent).find(".comments_div textarea");
        var commentsVal = $(comments).val();

        if(commentsVal==null || commentsVal==""){
          dis.mark_invalid($(parent).find("span[name='validation_msg']")[0],comments);
        } else{
          dis.mark_valid($(parent).find("span[name='validation_msg']")[0],comments);
          dis.process(diff_id,3,commentsVal);
        }
         
     },
     approve_difference:function(evt){
        var dis = this;
        evt.preventDefault();
        var parent = $(evt.target).parent().parent().parent();
        var diff_id = dis.differenceId;
        console.log(parent);
        var comments =$(parent).find(".comments_div textarea");
        var commentsVal = $(comments).val();

        if(commentsVal==null || commentsVal==""){
          dis.mark_invalid($(parent).find("span[name='validation_msg']")[0],comments);
        } else{
          dis.mark_valid($(parent).find("span[name='validation_msg']")[0],comments);
          dis.process(diff_id,3,commentsVal);
        }
         
     },

    refresh_list:function(){
      var dis = this;
      dis.parentView.render();
      //this.render();
    },

    mark_valid:function(error_box,comments){
      $(error_box).text("");
      $(error_box).removeClass("visible").addClass("hidden");
            
     },
     mark_invalid:function(error_box,comments){
      $(error_box).text("please enter comments");
      $(error_box).removeClass("hidden").addClass("visible");
      $(comments).focus();
     },

  });
return CountDifferencesView;
}); 

