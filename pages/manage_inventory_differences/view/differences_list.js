"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/difference_model.js",
  "./count_differences.js",
  "Paging",
  "art_Dialog/dialog",


], function( config, $, Backbone, Handlebars, templates , HandleBarsExt, difference_models, countDifferencesView,Paging,dialog) {

    return Backbone.View.extend({
      template: templates.difference_list,
      el:"#differences_list_container",
      
      initialize:function(data){
        var dis = this;
        //dis.collection= new difference_models.DifferenceCollection(data);
      },
      render:function(data){

        var dis = this;
        var loading = dialog();
        loading.showModal();
        if(data){
          dis.collection= new difference_models.DifferenceCollection(data);
        }
        
        dis.collection.fetch({dataType: "json",async: false});
        
        var post_date_css = "";
        var approve_date_css = "";
        if(dis.collection.sortby=="created_at"){
          if(dis.collection.sort=="asc"){
            post_date_css = "up";
          }else{
            post_date_css = "down"
          }
        }else if(dis.collection.sortby=="processed_at"){
          if(dis.collection.sort=="asc"){
            approve_date_css = "up";
          }else{
            approve_date_css = "down"
          }
        }
        var html =dis.template({differences: dis.collection.toJSON(), approve_date_css:approve_date_css, post_date_css:post_date_css});
        dis.$el.html("");
        dis.$el.html(html);

        
        Paging.update_page(dis.collection.pageCtrl.pageCount,dis.collection.pageCtrl.pageNo,"pagination",function(pageNo){
          console.log("page"+pageNo)
          dis.render({title_id:dis.collection.title_id,pageNo:pageNo,area_id:dis.collection.area_id,ps_id:dis.collection.ps_id,location_id:dis.collection.location_id,approve_status:dis.collection.approve_status,
            sortby:dis.collection.sortby,sort:dis.collection.sort});
        });

        $("a[name='created_at']").click(function(evt){dis.sort_list(evt);});
        $("a[name='processed_at']").click(function(evt){dis.sort_list(evt);});
        $(".review_btn_difference").click(function(evt){dis.detail_review(evt);});
        loading.close().remove();
        
     },
     events:{

     },

     sort_list:function(evt){
      var dis = this;
      var sortby = $(evt.target).attr("name");
      var loading = dialog();
      loading.showModal();
      dis.collection.sortby = sortby;
      if(dis.collection.sort == "asc"){
        
        dis.collection.sort = "desc";
      
      }else{
        
        dis.collection.sort = "asc";

      }
      
      dis.render({title_id:dis.collection.title_id,pageNo:1,area_id:dis.collection.area_id,ps_id:dis.collection.ps_id,approve_status:dis.collection.approve_status,location_id:dis.collection.location_id,
            sortby:dis.collection.sortby,sort:dis.collection.sort});
      loading.close().remove();
    },

    detail_review:function(evt){
        var dis = this;
        $("#area_differences").hide();

        var loading = dialog();
        loading.showModal();

        var dif_id = $(evt.target).attr("name");
        new countDifferencesView({differenceId:dif_id,parentView:dis}).render();
        loading.close().remove();
    },

    
      
  });
}); 

