"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/storage_model.js",
  "../model/area_model.js",
  "./area_list_box.js",
  "../js/ImmyboxControl",
  "../model/title_model.js",
  "./title_list_box.js",
   "./status_list_box.js",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,storage_models,area_models,areaListBoxView, ImmyBox, title_models, titleListBoxView, statusListBoxView ) {
    
    return Backbone.View.extend({
      el:"#storage_list_box_container",
      template:templates.storage_list_box,
      collection: new storage_models.StorageImmyCollection(),
      
      
      
      render:function(){

        var dis = this;
        dis.$el.html(dis.template());
        
        var storageImmydata = {
          renderTo: "#storage_list_box_container",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#storage_list_box",
          selectedInputId: "#selected_storage_id"
        };

        var StorageImmy = new ImmyBox(storageImmydata);
        StorageImmy.on("events.change",function(){dis.pull_areas();});
        StorageImmy.render();
        dis.pull_areas();
        
      },

      pull_areas:function(){
        
        var val = $("#selected_storage_id").val();
        var titleCol = new title_models.TitleImmyCollection(val);
        var titles = new titleListBoxView({collection:titleCol});
        titles.render();
        var col = new area_models.AreaImmyCollection(val);
        var areas = new areaListBoxView({collection:col});
        areas.render();

        new statusListBoxView().render();
        
        
      }
      
    });

}); 