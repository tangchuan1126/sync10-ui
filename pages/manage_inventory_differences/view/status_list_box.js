"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",
  
], function( config, $, Backbone, Handlebars, templates ,ImmyBox) {
    
    var ButtonView =  Backbone.View.extend({
      el:"#status_list_box_container",
      template:templates.status_list_box,
      
     

      render:function(){
        var dis = this;
        var html =dis.template();
        dis.$el.html("");
        dis.$el.html(html);
        
        var statusImmydata = {
            renderTo: "#status_list_box_container",
            dataUrl: [{text: 'Any', value: '0'},{text: 'Pending', value: '1'},
              {text: 'Approved', value: '2'},{text: 'Rejected', value: '3'}],
            inputId: "#status_select",
            selectedInputId: "#selected_status"
          };

          var statusImmy = new ImmyBox(statusImmydata);
          statusImmy.on("events.change",function(){});
          statusImmy.render();
        

         
     },
     
      
  });
return ButtonView;
}); 

