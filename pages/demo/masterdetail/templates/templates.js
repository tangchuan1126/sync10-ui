(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['detail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<table class=\"table table-condensed table-bordered table-hover no-margin\">\r\n	<tbody>\r\n		<tr><th class=\"bg-info text-right col-sm-2\">姓名</th><td class=\"col-sm-10\">"
    + escapeExpression(lambda((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "</td></tr>\r\n    	<tr><th class=\"bg-info text-right\">编号</th><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.adid : depth0), depth0))
    + "</td></tr>\r\n		<tr><th class=\"bg-info text-right\">组号</th><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.adgid : depth0), depth0))
    + "</td></tr>\r\n		<tr><th class=\"bg-info text-right\">仓库号</th><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.ps_id : depth0), depth0))
    + "</td></tr>\r\n		<tr><th class=\"bg-info text-right\">邮箱</th><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.email : depth0), depth0))
    + "</td></tr>\r\n		<tr><th class=\"bg-info text-right\">最近登录时间</th><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "</td></tr>\r\n	</tbody>\r\n</table>";
},"useData":true});
templates['master'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "    		<tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.adid : depth0), depth0))
    + "\" class=\"show-detail\">\r\n    			<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "</td>\r\n    			<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.account : depth0), depth0))
    + "</td>\r\n    			<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "</td>\r\n    		</tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"table-responsive\">\r\n  <table class=\"table table-condensed table-bordered table-hover no-margin\">\r\n    <thead>\r\n      <tr class=\"bg-info\">\r\n      	<th>姓名</th>\r\n      	<th>账号</th>\r\n      	<th>最近登录时间</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.items : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "    </tbody>\r\n  </table>\r\n  <table class=\"table table-condensed no-margin bg-info\">\r\n  	<thead>\r\n  			<tr>\r\n	    		<th>\r\n	    			\r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\r\n	                	首页\r\n	                </button>&nbsp;&nbsp; \r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n	                	上一页\r\n	                </button>&nbsp;&nbsp; \r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n	                	下一页\r\n	                </button>&nbsp;&nbsp; \r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\r\n	                	末页\r\n	                </button>\r\n	            </th>\r\n	            <th>\r\n	                <span class=\"pull-right small\">\r\n	    			当前："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\r\n	                页数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\r\n	                总数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;  \r\n	                </span>\r\n	    		</th>\r\n	    	</tr>\r\n	</thead>\r\n  </table>  \r\n</div>";
},"useData":true});
templates['master2'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	<h3 data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.adid : depth0), depth0))
    + "\" class=\"show-detail\">\r\n		"
    + escapeExpression(lambda((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "("
    + escapeExpression(lambda((depth0 != null ? depth0.account : depth0), depth0))
    + ")\r\n	</h3>\r\n	<div class=\"detail-view\">\r\n	</div>\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "disabled";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"accordion-list\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.items : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "</div>\r\n<table class=\"table table-condensed no-margin bg-info\">\r\n  	<thead>\r\n  			<tr>\r\n	    		<th>\r\n	    			\r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\r\n	                	首页\r\n	                </button>&nbsp;&nbsp; \r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"-1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n	                	上一页\r\n	                </button>&nbsp;&nbsp; \r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"1\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " >\r\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n	                	下一页\r\n	                </button>&nbsp;&nbsp; \r\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" ";
  stack1 = helpers.unless.call(depth0, ((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1), {"name":"unless","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " >\r\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\r\n	                	末页\r\n	                </button>\r\n	            </th>\r\n	            <th>\r\n	                <span class=\"pull-right small\">\r\n	    			当前："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\r\n	                页数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\r\n	                总数："
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;  \r\n	                </span>\r\n	    		</th>\r\n	    	</tr>\r\n	</thead>\r\n</table>";
},"useData":true});
})();