define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['detail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"table table-condensed table-bordered table-hover no-margin\">\n	<tbody>\n		<tr><th class=\"bg-info text-right col-sm-5\">姓名</th><td class=\"col-sm-7\">"
    + alias2(alias1((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "</td></tr>\n    	<tr><th class=\"bg-info text-right\">编号</th><td>"
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "</td></tr>\n		<tr><th class=\"bg-info text-right\">组号</th><td>"
    + alias2(alias1((depth0 != null ? depth0.adgid : depth0), depth0))
    + "</td></tr>\n		<tr><th class=\"bg-info text-right\">仓库号</th><td>"
    + alias2(alias1((depth0 != null ? depth0.ps_id : depth0), depth0))
    + "</td></tr>\n		<tr><th class=\"bg-info text-right\">邮箱</th><td>"
    + alias2(alias1((depth0 != null ? depth0.email : depth0), depth0))
    + "</td></tr>\n		<tr><th class=\"bg-info text-right\">最近登录时间</th><td>"
    + alias2(alias1((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "</td></tr>\n	</tbody>\n</table>";
},"useData":true});
templates['master'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "    		<tr data-id=\""
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "\" class=\"show-detail\">\n    			<td>"
    + alias2(alias1((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "</td>\n    			<td>"
    + alias2(alias1((depth0 != null ? depth0.account : depth0), depth0))
    + "</td>\n    			<td>"
    + alias2(alias1((depth0 != null ? depth0.last_login_date : depth0), depth0))
    + "</td>\n    		</tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"table-responsive\">\n  <table class=\"table table-condensed table-bordered table-hover no-margin\">\n    <thead>\n      <tr class=\"bg-info\">\n      	<th>姓名</th>\n      	<th>账号</th>\n      	<th>最近登录时间</th>\n      </tr>\n    </thead>\n    <tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </tbody>\n  </table>\n  <table class=\"table table-condensed no-margin bg-info\">\n  	<thead>\n  			<tr>\n	    		<th>\n	    			\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\n	                	首页\n	                </button>&nbsp;&nbsp; \n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\n	                	上一页\n	                </button>&nbsp;&nbsp; \n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\n	                	下一页\n	                </button>&nbsp;&nbsp; \n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\n	                	末页\n	                </button>\n	            </th>\n	            <th>\n	                <span class=\"pull-right small\">\n	    			当前："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\n	                页数："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\n	                总数："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;  \n	                </span>\n	    		</th>\n	    	</tr>\n	</thead>\n  </table>  \n</div>";
},"useData":true});
templates['master2'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	<h3 data-id=\""
    + alias2(alias1((depth0 != null ? depth0.adid : depth0), depth0))
    + "\" class=\"show-detail\">\n		"
    + alias2(alias1((depth0 != null ? depth0.employe_name : depth0), depth0))
    + "("
    + alias2(alias1((depth0 != null ? depth0.account : depth0), depth0))
    + ")\n	</h3>\n	<div class=\"detail-view\">\n	</div>\n";
},"3":function(depth0,helpers,partials,data) {
    return "disabled";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"accordion-list\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n<table class=\"table table-condensed no-margin bg-info\">\n  	<thead>\n  			<tr>\n	    		<th>\n	    			\n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-backward\"></span>\n	                	首页\n	                </button>&nbsp;&nbsp; \n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"-1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-left\"></span>\n	                	上一页\n	                </button>&nbsp;&nbsp; \n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageplus=\"1\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-chevron-right\"></span>\n	                	下一页\n	                </button>&nbsp;&nbsp; \n	                <button class=\"page-ctrl btn btn-default btn-xs\" data-pageno=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\" "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " >\n	                	<span class=\"glyphicon glyphicon-step-forward\"></span>\n	                	末页\n	                </button>\n	            </th>\n	            <th>\n	                <span class=\"pull-right small\">\n	    			当前："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "&nbsp;&nbsp;\n	                页数："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\n	                总数："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp;  \n	                </span>\n	    		</th>\n	    	</tr>\n	</thead>\n</table>";
},"useData":true});
return templates;
});