require([
     "../../../requirejs_config", 
], function(){
	require(["jquery",
			 "oso.demo.masterdetail",
			 "templates",
			 "jqueryui/tabs",
			 "jqueryui/accordion",
			 "jqueryui/dialog",
			 "domready"],
	function($, MasterDetail, templates){
		$("#tabs").tabs();
		

		var example1 = new MasterDetail({
			el:"#master-view",
			masterUrl: "/Sync10/_demo/admins",
			detailUrl: "/Sync10/_demo/admin",
			idAttribute: "adid",
			detailEl:"#detail-view",
			isDetailViewShared: true,
			template: templates.master,
			detailTemplate: templates.detail,
			itemsKey:"items",
			pageCtrlKey:"pageCtrl",
			pageSize:10
		});
		example1.render();

		var example2 = new MasterDetail({
			el:"#master-view-2",
			masterUrl: "/Sync10/_demo/admins",
			detailUrl: "/Sync10/_demo/admin",
			idAttribute: "adid",
			detailEl:".detail-view",
			isDetailViewShared: false,
			template: templates.master2,
			detailTemplate: templates.detail,
			itemsKey:"items",
			pageCtrlKey:"pageCtrl",
			pageSize:10
		});
		
		example2.on("events.renderMaster",function(){
			var target = $("#master-view-2 .accordion-list");
			if(target.hasClass("ui-accordion")){
				target.accordion("refresh");
			}
			else {
				target.accordion({
					active:false,
					collapsible:true,
					animate:false
				});
			}
		});
		example2.render();

		var example3 = new MasterDetail({
			el:"#master-view-3",
			masterUrl: "/Sync10/_demo/admins",
			detailUrl: "/Sync10/_demo/admin",
			idAttribute: "adid",
			detailEl:"#detail-view-dialog",
			isDetailViewShared: true,
			template: templates.master,
			detailTemplate: templates.detail,
			itemsKey:"items",
			pageCtrlKey:"pageCtrl",
			pageSize:10
		});
		example3.on("events.renderDetail",function(item){
			$("#detail-view-dialog").dialog({ 
				title:item.employe_name + "(" + item.account + ")" + " 详情",
				modal:true,
				buttons: [ 
					{ 
						text: "确定", 
						click: function() { 
							$(this).dialog("close"); 
						} 
					} 
				] 
			});

		});
		example3.render();

		//组件继承演示，通过继承，添加新行为或覆盖其原有的部分行为，来为组件增加新功能来满足需求
		var MasterDetailExt = MasterDetail.extend({
			//覆盖组件的构造函数，添加一个内部数据，用来记录打开的tab
			constructor: function(options) {
				MasterDetail.apply( this, arguments );
				this.detailEl = $(options.detailEl);
				this.detailEl.tabs();
				var v = this;
				this.detailEl.delegate(".ui-icon-close","click",function(evt){
					var href = $(evt.currentTarget).siblings("a").attr("href");
					var m = href.match(/^#detail-view-(.+)$/);
					if(!m) return;
					v.removeDetailTab(m[1]);
				});
			},
			//覆盖组件的findDetailEl方法，在下部Detail视图区创建一个新tab（或找到已存在的匹配ID的tab）
			//使得，主列表点击记录时，记录详情从服务端获取后，渲染到Detail视图区指定的tab中
			findDetailEl:function(fromEl){
				var modelId = $(fromEl).data("id");
				var tabNo = this.getDetailTab(modelId);
				if(tabNo >= 0){
					//这条记录已打开，将其置为活跃的Tab，返回其DOM元素
					this.detailEl.tabs("option","active", tabNo);
					return $("#detail-view-"+modelId).get();
				}
				else {
					//这条记录之前未曾打开，需添加tab
					return this.addDetailTab(modelId).get();
				}
			},
			getDetailTab: function(modelId){
				var tabNo = -1;
				this.detailEl.children("div").each(function(i){
					if($(this).attr("id") == "detail-view-"+modelId) tabNo = i;
				});
				return tabNo;
			},
			addDetailTab: function(modelId){
				$('<li><a href="#detail-view-'+modelId+'">'+
					modelId+'</a><span class="ui-icon ui-icon-close"></span></li>')
				.appendTo(this.detailEl.find('ul'));

				var tab = $('<div id="detail-view-'+modelId+'"></div>');
				tab.appendTo(this.detailEl);

				this.detailEl.tabs("refresh");
				this.detailEl.tabs("option","active", -1); //选中最后一个tab为活跃
				return tab;
			},
			removeDetailTab: function(modelId){
				this.detailEl.find('li:has(a[href="#detail-view-'+modelId+'"])').remove();
				this.detailEl.find('div#detail-view-'+modelId).remove();
				this.detailEl.tabs("refresh");
			}
		});
		var example4 = new MasterDetailExt({
			el:"#master-view-4 .master-view",
			masterUrl: "/Sync10/_demo/admins",
			detailUrl: "/Sync10/_demo/admin",
			idAttribute: "adid",
			detailEl:"#master-view-4 .detail-view",
			isDetailViewShared: false, //此开关置为false，以为在渲染Detail时调用findDetailEl方法
			template: templates.master,
			detailTemplate: templates.detail,
			itemsKey:"items",
			pageCtrlKey:"pageCtrl",
			pageSize:10
		});
		example4.render();
	});
});