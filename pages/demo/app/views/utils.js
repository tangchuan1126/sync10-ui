define([
    "jquery",
    "jqueryui/dialog"
],
function($){
    return {
      updateDialog:function(saveCallback,destroyCallback){
        $("#update_admin").dialog({
            draggable:true,
            modal: true,
            buttons: [
                {
                    text: "删除",
                    icons:{ primary:"ui-icon-trash" },
                    click: function() {
                        destroyCallback();
                        $(this).dialog("close");
                    }
                },
                {
                    text: "取消",
                    icons:{ primary:"ui-icon-cancel" },
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    text: "更新",
                    icons:{ primary:"ui-icon-disk" },
                    click: function() {
                        saveCallback();
                        $(this).dialog("close");
                    }
                }
            ]
            });
        }
    };
});