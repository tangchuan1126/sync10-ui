define(['jquery',
        'backbone',
        'handlebars',
        'templates',
        '../models/admin'], 
function($,
         Backbone,
         Handlebars,
         templates,
         AdminModel){   

    return  Backbone.View.extend({
        template:templates.create_admin,
        initialize:function(model,options){
            this.setElement(options.el);
            if(model) this.model = model;
            else this.model = new AdminModel();
        },
        render:function(submitBtn){
            console.log(this);
            this.$el.html(this.template({model:this.model.toJSON(),submitBtn:submitBtn}));
        },
        events:{
            "click button":"createAdmin"
        },
        createAdmin:function(evt){
            var el = this.$el;
            var v = this;

            this.model.save({
                "account": el.find("input[name='account']").val(),
                "employe_name":el.find("input[name='employe_name']").val(),
                "email":el.find("input[name='email']").val()
            },
            { 
                success:function(m){
                    v.model = new AdminModel();
                    v.render(true);
                    $("#tabs").tabs("option","active",0);
                }
            });
        }
    });
    
});