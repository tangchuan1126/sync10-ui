define(['jquery',
        'backbone',
        'handlebars',
        'templates',
        '../models/admin_collection',
        '../views/create_admin',
        './utils'], 
function($,
         Backbone,
         Handlebars,
         templates,
         AdminCollection,
         CreateAdminView,
         view_utils){    
    var AdminsView = Backbone.View.extend({
           el:"#admins",
           template:templates.admins,
           collection:new AdminCollection(),
           render:function(){
                var v = this;
                
                this.collection.fetch({
                    data:{
                        pageNo:this.collection.pageCtrl.pageNo,
                        pageSize:this.collection.pageCtrl.pageSize
                    },
                    success:function(collection){
                       v.$el.html(v.template({
                           admins:collection.toJSON(),
                           pageCtrl:v.collection.pageCtrl
                       })); 
                       onLoadInitZebraTable();
                    }
                }) ;         
            },
            events:{
                "click button":"changePage",
                "click td":"adminClick"
            },
            changePage:function(evt){
                var btn = $(evt.target);
                if(btn.data("pageno")){
                    this.collection.pageCtrl.pageNo = parseInt(btn.data("pageno"));
                }
                else if(btn.data("pageplus")){
                    this.collection.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
                }
                else return;
                this.render();
            },
            adminClick:function(evt){
                var adid = $(evt.target).parent().data("adid");
                if(!adid) return;
                var collection = this.collection;
                var model = this.collection.findWhere({adid:adid});
                var v = this;
                
                var updateAdmin = new CreateAdminView(model,{el:"#update_admin"});
                updateAdmin.render(false);
                
                view_utils.updateDialog(function(){
                   model.save(
                       {
                            account: updateAdmin.$el.find("input[name='account']").val(),
                            employe_name:updateAdmin.$el.find("input[name='employe_name']").val(),
                            email:updateAdmin.$el.find("input[name='email']").val()
                        },
                        {
                            success:function(){
                                v.render();
                            }
                        }
                   ); 
                },
                function(){
                    collection.remove(model);
                    model.url = model.url + "?id="+model.get("adid");
                    model.destroy({
                        success: function(model, response) {
                                v.render();
                        }
                    });
                });
           }
    });
    return new AdminsView();
});