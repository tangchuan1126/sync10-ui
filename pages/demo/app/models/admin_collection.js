define(['backbone','../../config','./admin'], function(Backbone,page_config,AdminModel){
    return Backbone.Collection.extend({
        model:AdminModel,
        url:page_config.adminCollection.url,
        pageCtrl:{
            pageNo:1,
            pageSize:20
        },
        parse:function(response){
            this.pageCtrl = response.pageCtrl;
            return response.items;
        }
    
    });
});