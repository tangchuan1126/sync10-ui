define(['jquery',
        'backbone',
        '../../config',
        'jqueryui/dialog'], 
    function($,
             Backbone,
             page_config){
    return Backbone.Model.extend({
        url:page_config.adminModel.url,
        idAttribute:"adid",
        initialize:function(){
            this.on("invalid",function(model, error) {
                console.log(error);
                 $('<div></div>').appendTo("body").html(error.toString())
                .dialog({
                    title:'数据校验错误',
                    modal:true,
                    dialogClass: "alert",
                    buttons: [ { text: "OK", click: function() { $(this).dialog("close").remove(); } } ]
                });
            });
        },
        defaults:{
            account:"newadmin",
            employe_name:"新管理员",
            email:""
        },
        validate:function(attrs) {
            if(! (attrs.account && attrs.account.match(/^[a-zA-Z0-9_]+$/))) return "account字段必须是由字母、数字、下划线组成";
        }
    });
});