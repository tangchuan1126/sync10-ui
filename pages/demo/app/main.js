"use strict";

require([
     "../../../requirejs_config", 
], function(){
    
    require(["jquery",
             "models/admin",
    		 "views/admins",
    		 "views/create_admin",
     		 "jqueryui/tabs",
             "domready"],
     function($,
              AdminModel,
     		  adminsView,
     		  CreateAdminView){

     	$("#tabs").tabs({
            active:0,
            beforeActivate: function( event, ui ) {
                console.log(ui);
                adminsView.render();
            }
        });
        adminsView.render();

        (new CreateAdminView(new AdminModel,{el:"#create_admin"})).render(true);

        
     });
});