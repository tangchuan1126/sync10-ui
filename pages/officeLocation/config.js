(function(){
    var configObj = {
            officeTree:{
                url:"/Sync10/action/administrator/officeTree.action"
            },
            officeCollection: {
                url:"/Sync10/action/administrator/officeTree.action"
            }
    };
    if (typeof define === 'function' && define.amd) {
        define(configObj);
    }
    else {
        //传统模式，非AMD标准
        this.page_config = configObj;
    }
}).call(this);
