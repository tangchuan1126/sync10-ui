define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['create_office_location'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table>\n<tbody>\n   \n	    \n    <tr>\n        <th>office_name</th>\n        <td>\n            <input type=\"text\" name=\"office_name\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" />\n        </td>\n            <input type=\"text\" name=\"id\" style=\"display:none\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\n            <input type=\"text\" name=\"parentid\" 	style=\"display:none\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pid : stack1), depth0))
    + "\" />\n       \n</tbody>\n</table>";
},"useData":true});
templates['officeLocation'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<table class=\"zebraTable\">\n    <tbody>\n     	<div class=\"content_wrap\" id=\"officeTree\">\n			<div class=\"zTreeDemoBackground left\">\n				<ul id=\"treeDemo\" class=\"ztree\"></ul>\n			</div>	\n		</div>\n       \n    	<div id=\"rMenu\"> \n			<ul>\n				<li id=\"m_add\">���ӽڵ�</li>\n				<li id=\"m_update\">�޸Ľڵ�</li>\n				<li id=\"m_del\">ɾ���ڵ�</li>\n			</ul>\n		</div>\n    </tbody>\n</table>\n";
},"useData":true});
templates['query'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "         <tr data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\n         <td id=\"id1\">\n		         	"
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "</br>\n		            "
    + alias2(alias1((depth0 != null ? depth0.office_name : depth0), depth0))
    + "</br>\n		            "
    + alias2(alias1((depth0 != null ? depth0.house_number : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.street : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.city : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.zip_code : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.provinces : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.nation : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.contact : depth0), depth0))
    + "</br>\n					"
    + alias2(alias1((depth0 != null ? depth0.phone : depth0), depth0))
    + "</br>\n			</td>	\n			 \n        </tr>\n        <tr>\n        <td data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\n         <button id=\"button1\">编辑</button>\n        </td>\n        </tr>\n        \n       \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return " <table class=\"zebraTable\">\n    <tbody>\n  \n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.Address : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        \n    </tbody>\n   \n</table>\n		";
},"useData":true});
templates['update_address'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <option value=\""
    + alias2(alias1((depth0 != null ? depth0.adgid : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table>\n<tbody>\n\n	    \n    <tr>\n        <th>house_number</th>\n        <td>\n            <input type=\"text\" name=\"house_number\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.house_number : stack1), depth0))
    + "\" />\n        </td>\n    </tr> \n    <tr>\n        <th>street</th>\n        <td>\n            <input type=\"text\" name=\"street\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.street : stack1), depth0))
    + "\" />\n        </td>\n    </tr>\n    <tr>\n        <th>city</th>\n        <td>\n            <input type=\"text\" name=\"city\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\" />\n        </td>\n    </tr>   \n    \n      <tr>\n        <th>zip_code</th>\n        <td>\n            <input type=\"text\" name=\"zip_code\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.zip_code : stack1), depth0))
    + "\" />\n        </td>\n    </tr>\n    \n      <tr>\n        <th>provinces</th>\n        <td>\n            <input type=\"text\" name=\"provinces\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.provinces : stack1), depth0))
    + "\" />\n        </td>\n    </tr>\n    \n      <tr>\n        <th>nation</th>\n        <td>\n            <input type=\"text\" name=\"nation\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.nation : stack1), depth0))
    + "\" />\n            <div>\n                        <select id=\"selectDeptment\" style=\"width:200px;\">\n                            <option value=\"\">��ѡ�񡣡�</option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.deptments : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </select>\n           </div>\n        </td>\n    </tr>\n    \n     <tr>\n        <th>contact</th>\n        <td>\n            <input type=\"text\" name=\"contact\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\" />\n        </td>\n    </tr>\n    \n     <tr>\n        <th>phone</th>\n        <td>\n            <input type=\"text\" name=\"phone\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\" />\n        </td>\n    </tr>\n\n       \n</tbody>\n</table>";
},"useData":true});
return templates;
});