(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['create_office_location'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<table>\r\n<tbody>\r\n   \r\n	    \r\n    <tr>\r\n        <th>office_name</th>\r\n        <td>\r\n            <input type=\"text\" name=\"office_name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" />\r\n        </td>\r\n            <input type=\"text\" name=\"id\" style=\"display:none\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" />\r\n            <input type=\"text\" name=\"parentid\" 	style=\"display:none\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pid : stack1), depth0))
    + "\" />\r\n       \r\n</tbody>\r\n</table>";
},"useData":true});
templates['officeLocation'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<table class=\"zebraTable\">\r\n    <tbody>\r\n     	<div class=\"content_wrap\" id=\"officeTree\">\r\n			<div class=\"zTreeDemoBackground left\">\r\n				<ul id=\"treeDemo\" class=\"ztree\"></ul>\r\n			</div>	\r\n		</div>\r\n       \r\n    	<div id=\"rMenu\"> \r\n			<ul>\r\n				<li id=\"m_add\">���ӽڵ�</li>\r\n				<li id=\"m_update\">�޸Ľڵ�</li>\r\n				<li id=\"m_del\">ɾ���ڵ�</li>\r\n			</ul>\r\n		</div>\r\n    </tbody>\r\n</table>\r\n";
  },"useData":true});
templates['query'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "         <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n         <td id=\"id1\">\r\n		         	"
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "</br>\r\n		            "
    + escapeExpression(lambda((depth0 != null ? depth0.office_name : depth0), depth0))
    + "</br>\r\n		            "
    + escapeExpression(lambda((depth0 != null ? depth0.house_number : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.street : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.city : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.zip_code : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.provinces : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.nation : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.contact : depth0), depth0))
    + "</br>\r\n					"
    + escapeExpression(lambda((depth0 != null ? depth0.phone : depth0), depth0))
    + "</br>\r\n			</td>	\r\n			 \r\n        </tr>\r\n        <tr>\r\n        <td data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n         <button id=\"button1\">编辑</button>\r\n        </td>\r\n        </tr>\r\n        \r\n       \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <table class=\"zebraTable\">\r\n    <tbody>\r\n  \r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.Address : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        \r\n    </tbody>\r\n   \r\n</table>\r\n		";
},"useData":true});
templates['update_address'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.adgid : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<table>\r\n<tbody>\r\n\r\n	    \r\n    <tr>\r\n        <th>house_number</th>\r\n        <td>\r\n            <input type=\"text\" name=\"house_number\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.house_number : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr> \r\n    <tr>\r\n        <th>street</th>\r\n        <td>\r\n            <input type=\"text\" name=\"street\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.street : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <th>city</th>\r\n        <td>\r\n            <input type=\"text\" name=\"city\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr>   \r\n    \r\n      <tr>\r\n        <th>zip_code</th>\r\n        <td>\r\n            <input type=\"text\" name=\"zip_code\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.zip_code : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr>\r\n    \r\n      <tr>\r\n        <th>provinces</th>\r\n        <td>\r\n            <input type=\"text\" name=\"provinces\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.provinces : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr>\r\n    \r\n      <tr>\r\n        <th>nation</th>\r\n        <td>\r\n            <input type=\"text\" name=\"nation\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.nation : stack1), depth0))
    + "\" />\r\n            <div>\r\n                        <select id=\"selectDeptment\" style=\"width:200px;\">\r\n                            <option value=\"\">��ѡ�񡣡�</option>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.deptments : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        </select>\r\n           </div>\r\n        </td>\r\n    </tr>\r\n    \r\n     <tr>\r\n        <th>contact</th>\r\n        <td>\r\n            <input type=\"text\" name=\"contact\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr>\r\n    \r\n     <tr>\r\n        <th>phone</th>\r\n        <td>\r\n            <input type=\"text\" name=\"phone\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\" />\r\n        </td>\r\n    </tr>\r\n\r\n       \r\n</tbody>\r\n</table>";
},"useData":true});
})();