"use strict";
(function(){

var page_init = function(page_config,$,Backbone,Handlebars,templates){
	
  $(function(){

    $("#tabs").tabs();
    var setActiveTab = function(i){ 
        $("#tabs").tabs("option","active",i);
    };
    setActiveTab(0);
    var updateDialog = function(saveCallback,destroyCallback){
        $("#update_admin").dialog({
        	width:330,
        	draggable:true,
            modal: true,
            buttons: [
                {
                    text: "删除",
                    icons:{ primary:"ui-icon-trash" },
                    click: function() {
                        destroyCallback();
                        $(this).dialog("close");
                    }
                },
                {
                    text: "取消",
                    icons:{ primary:"ui-icon-cancel" },
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    text: "更新",
                    icons:{ primary:"ui-icon-disk" },
                    click: function() {
                        saveCallback();
                        $(this).dialog("close");
                    }
                }
            ]
        });
   
    };
    
    var AdminModel = Backbone.Model.extend({
        url:page_config.adminModel.url,
        idAttribute:"adid",
        defaults:{
            account:"newadmin",
            employe_name:"新管理员",
            email:"",
            skype:"",
            msn:"",
            proqq:"",
            entrytime:""
        }
    });

    var AdminCollection = Backbone.Collection.extend({
        model:AdminModel,
        url:page_config.adminCollection.url,
        parse:function(response){
            AdminCollection.pageCtrl = response.pageCtrl;
            return response.items;
        }
    },{
        pageCtrl:{
            pageNo:1,
            pageSize:20
        },
    });
    
    var AdminsView = Backbone.View.extend({
           el:"#admins",
           template:templates.admins,
           collection:new AdminCollection(),
           render:function(){
                var v = this;
                this.collection.fetch({
                    data:{
                        pageNo:AdminCollection.pageCtrl.pageNo,
                        pageSize:AdminCollection.pageCtrl.pageSize
                    },
                    success:function(collection){
                       v.$el.html(v.template({
                           admins:collection.toJSON(),
                           pageCtrl:AdminCollection.pageCtrl
                       })); 
                       onLoadInitZebraTable();
                    }
                }) ;         
            },
            events:{
                "click button":"changePage",
                "click td":"adminClick"
            },
            changePage:function(evt){
                var btn = $(evt.target);
                if(btn.data("pageno")){
                    AdminCollection.pageCtrl.pageNo = parseInt(btn.data("pageno"));
                }
                else if(btn.data("pageplus")){
                    AdminCollection.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
                }
                else return;
                adminsView.render();
            },
            adminClick:function(evt){
                var adid = $(evt.target).parent().data("adid");
                if(!adid) return;
                var collection = this.collection;
                var model = this.collection.findWhere({adid:adid});
                
                var updateAdmin = new CreateAdminView(model,{el:"#update_admin"});
                updateAdmin.render(false);
                updateDialog(function(){
                   model.save(
                       {
                            account: updateAdmin.$el.find("input[name='account']").val(),
                            employe_name:updateAdmin.$el.find("input[name='employe_name']").val(),
                            email:updateAdmin.$el.find("input[name='email']").val(),
                            skype:updateAdmin.$el.find("input[name='skype']").val(),
                            msn:updateAdmin.$el.find("input[name='msn']").val(),
                            proqq:updateAdmin.$el.find("input[name='proqq']").val(),
                            entrytime:updateAdmin.$el.find("input[name='entrytime']").val()

                        },
                        {
                            success:function(){
                                adminsView.render();
                            }
                        }
                   ); 
                },
                function(){
                    collection.remove(model);
                    model.url = model.url + "?id="+model.get("adid");
                    model.destroy({
                        success: function(model, response) {
                                adminsView.render();
                        }
                    });
                });
           }
    });
    var adminsView = new AdminsView();

    var CreateAdminView = Backbone.View.extend({
        template:templates.create_admin,
        initialize:function(model,options){
            this.setElement(options.el);
            this.model = model;
        },
        render:function(submitBtn){
            this.$el.html(this.template({model:this.model.toJSON(),submitBtn:submitBtn}));
        },
        events:{
            "click button":"createAdmin"
        },
        createAdmin:function(evt){
            var el = this.$el;
            this.model.save({
                "account": el.find("input[name='account']").val(),
                "employe_name":el.find("input[name='employe_name']").val(),
                "email":el.find("input[name='email']").val(),
                "skype":el.find("input[name='skype']").val(),
                "msn":el.find("input[name='msn']").val(),
                "proqq":el.find("input[name='proqq']").val(),
                "entrytime":el.find("input[name='entrytime']").val()

            },
            { 
                success:function(m){
                    createAdminView.model = new AdminModel();
                    createAdminView.render(true);
                    adminsView.render();
                    setActiveTab(0);
                }
            });
        }
    });
    var createAdminView = new CreateAdminView(new AdminModel(),{el:"#create_admin"});

    createAdminView.render(true);
    adminsView.render();
  });

}; //page_init

if (typeof define === 'function' && define.amd) {
    define([
         "config", 
         "jquery",
         "backbone",
         "handlebars",
         "templates",
         "jqueryui/tabs",
         "jqueryui/dialog"
    ],page_init);
}
else {
    page_init(page_config,$,Backbone,Handlebars,Handlebars.templates);
}


}).call(this);
