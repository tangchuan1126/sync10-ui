define(['backbone','./basemodel'], function (Backbone,Base) {
	var Address = Backbone.Model.extend({
			defaults:{
				address:"",
				zipcode:""
			}
	});
	var Addresses = Backbone.Collection.extend({
		model:Address
	});
	var Person = Base.extend({
		defaults:{
			name:"",
			addresses:[]
		}
	})
    return {
    	Person:Person,
    	Address:Address,
    	Addresses:Addresses,
    	PersonCollection:Backbone.Collection.extend({model:Person})
   	};
});