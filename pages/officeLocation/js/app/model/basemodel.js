define(['backbone'],
function(Backbone){
    var modelChange = function(model){
        model.off("change");
        model.set("timestamp",new Date());
        model.on("change",modelChange);
    };
    return Backbone.Model.extend({
        initialize:function(){
            this.set("timestamp",new Date());
            this.on("change",modelChange);
        }
    });
});
