define(['jquery','backbone','handlebars','../model/m1','jqueryui/button','jqueryui/accordion'], 
function ($,Backbone,Handlebars,MyModel) {
    Handlebars = Handlebars.default;

    Handlebars.registerHelper('formatDate', function(d) {
        return new Handlebars.SafeString(d.toISOString());
    });

    return Backbone.View.extend({
        el:"#view-panel",
        template:Handlebars.compile($("#view-template").html()),
        events:{
            "click button.remove-model":"removeModel",
            "change input":"addModel"
        },
        addModel:function(){
            this.collection.add(new MyModel({name:this.$el.find("input").val()}));
            console.log(this.collection.toJSON());
            this.render();
        },
        removeModel:function(evt){
            var m = this.collection.where({ name: $(evt.target).data("name") });
            this.collection.remove(m);
            this.render();
        },
        render:function(){
            var t = this.$el.find("#accordion");
            t.html(
                this.template({models:this.collection.toJSON()})
            );
            if(t.hasClass("ui-accordion"))
                t.accordion("refresh");
            else
                t.accordion();
        }
    });
});
