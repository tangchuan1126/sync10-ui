define(['jquery','backbone','handlebars','../model/m3','jqueryui/button','jqueryui/accordion'], 
function ($,Backbone,Handlebars,models) {

    return Backbone.View.extend({
        el:"#view-panel",
        template:Handlebars.default.compile($("#view-template").html()),
        events:{
            "click button.remove-model":"removeModel",
            "change input#add-model":"addModel",
            "click button.add-address":"addAddress",
            "click button.remove-address":"removeAddress"
        },
        addModel:function(){
            this.collection.add(new models.Person({name:this.$el.find("input#add-model").val()}));
            this.render();
        },
        removeModel:function(evt){
            var m = this.collection.findWhere({ name: $(evt.target).data("name") });
            console.log(m);
            this.collection.remove(m);
            this.render();
        },
        addAddress:function(evt){
            var m = this.collection.findWhere({ name: $(evt.target).data("name") });
            var addresses = new models.Addresses(m.get("addresses"));
            var address = $(evt.target).parent().find("input.address").val();
            var zipcode = $(evt.target).parent().find("input.zipcode").val();
            console.log(addresses);
            addresses.add(new models.Address(
                {
                    address:address,
                    zipcode:zipcode               
                }
            ));
            m.set("addresses",addresses.toJSON());
            this.render();
        },
        removeAddress:function(evt){
            var m = this.collection.findWhere({ name: $(evt.target).data("name") });
            var addresses = new models.Addresses(m.get("addresses"));
            var addrModel = addresses.findWhere({address:$(evt.target).data("address")});
            addresses.remove(addrModel);
            m.set("addresses",addresses.toJSON());
            this.render();
        },
        render:function(){
            var t = this.$el.find("#accordion");
            t.html(
                this.template({models:this.collection.toJSON()})
            );
            if(t.hasClass("ui-accordion")){
                var activeIdx =  t.accordion("option", "active");
                t.accordion("refresh");
                t.accordion("option","active",activeIdx);
            }
            else {
                t.accordion();
            }

        }
    });
});
