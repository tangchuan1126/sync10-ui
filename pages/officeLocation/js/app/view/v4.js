define(
		[ 'jquery', 'backbone', 'handlebars', '../model/m4', '../../../config',
				'templates', 'jqueryui/dialog', 'core', 'excheck', 'exedit' ],
		function($, Backbone, Handlebars, TreeModel, page_config, templates,
				dialog) {

			var createDialog = function(saveCallback) {
				$("#create").dialog({
					draggable : true,
					modal : true,
					buttons : [ {
						text : "提交",
						icons : {
							primary : "ui-icon-disk"
						},
						click : function() {
							saveCallback();
							$(this).dialog("close");
						}
					}, {
						text : "取消",
						icons : {
							primary : "ui-icon-cancel"
						},
						click : function() {
							$(this).dialog("close");
						}
					},

					]
				});
			};
			var updateDialog = function(saveCallback) {
				$("#create").dialog({
					draggable : true,
					modal : true,
					buttons : [ {
						text : "提交",
						icons : {
							primary : "ui-icon-disk"
						},
						click : function() {
							saveCallback();
							$(this).dialog("close");
						}
					}, {
						text : "取消",
						icons : {
							primary : "ui-icon-cancel"
						},
						click : function() {
							$(this).dialog("close");
						}
					},

					]
				});
			};

			var TreeCollection = Backbone.Collection.extend({
				model : TreeModel,
				url : page_config.officeCollection.url,
				parse : function(response) {
					return response.children;
				}
			});

			var TreeModel = Backbone.Model.extend({
				url : page_config.officeTree.url,
				idAttribute : "idr",
				defaults : {
					id : "",
					office_name : "",
					parentid : ""
				},
			});
			var date = new TreeModel();
			date.fetch({
				success : function(model, text) {
					// console.log(text.define);
				},
				error : function() {
					alert("报错！");
				}
			});

			var TreeView = Backbone.View
					.extend({
						el : "#officeTree",
						template : templates.officeLocation,
						collection : new TreeCollection(),

						events : {
						// 'click #m_add':'add',
						// 'click #m_del':'del'
						},
						render : function() {
							var v = this;
							var setting = {
								view : {
									dblClickExpand : false
								},
								check : {
									enable : false
								},
								callback : {
									onRightClick : OnRightClick

								},

							};

							function OnRightClick(event, treeId, treeNode) {

								var m_add = rMenu.find("#m_add");

								var m_del = rMenu.find("#m_del");

								var m_update = rMenu.find("#m_update");

								m_add.click(function() {
									v.opendialog(treeNode);
								});

								m_del.click(function() {
									v.destroyCallback(treeNode);
								});

								m_update.click(function() {
									v.updateCallback(treeNode);
								});

								//var addCount = 1;
								if (!treeNode
										&& event.target.tagName.toLowerCase() != "button"
										&& $(event.target).parents("a").length == 0) {
									zTree.cancelSelectedNode();
									$("#rMenu ul").show();
									$("#m_update").hide();
									$("#m_del").hide();
									rMenu.css({
										"top" : event.clientY + "px",
										"left" : event.clientX + "px",
										"visibility" : "visible"
									});
									$("body")
											.bind("mousedown", onBodyMouseDown);
								} else if (treeNode && !treeNode.noR) {
									zTree.selectNode(treeNode);
									$("#rMenu ul").show();
									$("#m_del").show();
									$("#m_update").show();
									rMenu.css({
										"top" : event.clientY + "px",
										"left" : event.clientX + "px",
										"visibility" : "visible"
									});
									$("body")
											.bind("mousedown", onBodyMouseDown);
								}
								;

								// console.log(treeNode);
							}

							function hideRMenu() {
								if (rMenu)
									rMenu.css({
										"visibility" : "hidden"
									});
								$("body").unbind("mousedown", onBodyMouseDown);
							}
							;
							function onBodyMouseDown(event) {
								if (!(event.target.id == "rMenu" || $(
										event.target).parents("#rMenu").length > 0)) {
									rMenu.css({
										"visibility" : "hidden"
									});
								}
							}
							;

							this.collection.fetch({
								success : function(collection) {
							//			console.log(collection.toJSON());
									v.$el.html(v.template({
										zNodes : collection.toJSON()
									}));
									$.fn.zTree.init($("#treeDemo"), setting,
											collection.toJSON());
									zTree = $.fn.zTree.getZTreeObj("treeDemo");
									rMenu = $("#rMenu");
								}

							})

						},
						opendialog : function(jsondata) {
							var createTree = new CreateTreeView();
							var model = new TreeModel();
							new CreateTreeView().render(true);
							var uidialog = $(".ui-dialog");
							var parentid = $("#create input[name='parentid']");
							if (jsondata == null) {
								var treeId = 0;
							} else {
								var treeId = jsondata.id;
							}
							if (uidialog.length < 1) {
								$("#create").dialog({
									open : function() {
										$(".ui-dialog-titlebar-close").hide();
										parentid.val(treeId);
									}
								});

							} else {
								parentid.val(treeId);
								uidialog.show();
							}
							;
							createDialog(function() {
								model.save({
									id : createTree.$el
											.find("input[name='id']").val(),
									office_name : createTree.$el.find(
											"input[name='office_name']").val(),
									parentid : createTree.$el.find(
											"input[name='parentid']").val()
								}, {
									success : function() {
										treeView.render();
									}
								});
							});
						},
						destroyCallback : function(jsondata) {
							var id = jsondata.id;
							var model = new TreeModel({
								idr : id
							});
							var collection = this.collection;
							model.url = model.url + "?id=" + id;

							model.destroy({
								success : function(model, response) {
									treeView.render();
								}
							});

						},
						updateCallback : function(jsondata) {
							var id = jsondata.id;
							if (!id)return;
							var collection = this.collection;
							var model = new TreeModel();
					
							var url = model.url;
							model = this.collection.findWhere({id : id});
							
							model.url = url;
							console.log(model.url);
							var updateTree = new UpdateTreeView(model, {el : "#create"});
							updateTree.render();
								$("#create").dialog({
									open : function() {
										$(".ui-dialog-titlebar-close").hide();
									}
								});

							
							

							updateDialog(function() {

								model.save({
									"id" : updateTree.$el.find(
											"input[name='id']").val(),
									"office_name" : updateTree.$el.find(
											"input[name='office_name']").val(),
									"parentid" : updateTree.$el.find(
											"input[name='parentid']").val()
								}, {
									success : function() {
										treeView.render();
									}
								});
							});

						}

					});

			new TreeView().render();

			var CreateTreeView = Backbone.View.extend({
				el : "#create",
				template : templates.create_office_location,
				model : new TreeModel(),
				render : function() {
					this.$el.html(this.template({
						model : this.model.toJSON()
					}));
				}
			});

			var UpdateTreeView = Backbone.View.extend({
				el : "#create",
				template : templates.create_office_location,
				initialize : function(model) {
					this.model = model;
				},
				render : function(submitBtn) {
					this.$el.html(this.template({
						model : this.model.toJSON()
					}));
				}

			});
			
			

			var treeView = new TreeView();

		});