define(['jquery','backbone','./handlebars_ext','jqueryui/button','jqueryui/dialog'], 
function ($,Backbone,Handlebars) {
    return Backbone.View.extend({
        el:'#view-panel',
        template:Handlebars.compile($("#view-template").html()),
        events:{
            "click  button#show-model":"showModel",
            "click  button#update-model":"updateModel"
        },
        showModel:function(){
            $('<div></div>').appendTo("body").html(
                this.template(this.model.toJSON())    
            )
            .dialog({
                title:'查看模型',
                buttons: [
                    {
                      text: "OK",
                      click: function() {
                        $(this).dialog("close");
                        $(this).remove();
                      }
                    }
              ]
            });
        },
        updateModel:function(){
            this.model.set("name",this.$el.find('input[name="name"]').val());
            this.model.set("gender",parseInt(this.$el.find('input[name=gender]:checked').val()));
        },
        render:function(){
            this.$el.find('button').button({
                icons: { primary: "ui-icon-newwin" }
            });
        }
    });
});
