define(['jquery','./model/m3','./view/v3'],
function ($,models,View) {
    var view = new View({ collection:new models.PersonCollection() });
    var m = new models.Person({name:"某人"});
    var addresses = new models.Addresses();
    addresses.add(new models.Address({address:'北京市朝阳区媒体村',zipcode:"100015"}));
    m.set("addresses",addresses.toJSON());
    view.collection.add(m);
    $(function () {
        view.render();
    });
});
