define(['jquery','./model/m2','./view/v2','./model/m1'],
function ($,Collection,View,MyModel) {
    var view = new View({ collection:new Collection() });
    view.collection.add(new MyModel({name:"初始内容"}));
    $(function () {
        view.render();
    });
});
