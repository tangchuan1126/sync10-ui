requirejs.config({
    paths: {
        jquery: '../../../bower_components/jquery/dist/jquery.min',
        jqueryui:'../../../bower_components/jqueryui/jqueryui',
        underscore:'../../../bower_components/underscore/underscore',
        backbone:'../../../bower_components/backbone/backbone',
        handlebars:'../../../bower_components/handlebars/handlebars.amd.min',
        templates:'../templates/templates.amd',
        core: '../../../bower_components/ztree/js/jquery.ztree.core-3.5.min',
        excheck: '../../../bower_components/ztree/js/jquery.ztree.excheck-3.5.min',
        exedit: '../../../bower_components/ztree/js/jquery.ztree.exedit-3.5.min'
    }
});

