"use strict";

(function(){

    //创建样式
  var childstyle = {
    global: {
      path: "/Sync10-ui/bower_components"
    },
    _int: function() {
        this.jstreestyle();
        this.mainstyle();
    },
    jstreestyle : function(){
       var jstree = document.getElementById("jstreestyle");
       if(jstree == null){

        var container = document.getElementsByTagName("head")[0];
        var jstreelink = document.createElement("link");
        jstreelink.id = "jstreestyle";
        jstreelink.rel = "stylesheet";
        jstreelink.type = "text/css";
        jstreelink.media = "screen";
        jstreelink.href = this.global.path + "/jstree/dist/themes/default/style.css";
        container.appendChild(jstreelink);

       }
    },
    mainstyle : function(){
       var mainstyle = document.getElementById("mainstyle");
       if(mainstyle == null){

        var container = document.getElementsByTagName("head")[0];
        var mainlink = document.createElement("link");
        mainlink.id = "jstreestyle";
        mainlink.rel = "stylesheet";
        mainlink.type = "text/css";
        mainlink.media = "screen";
        mainlink.href = "css/style.css";
        container.appendChild(mainlink);

       }
    }

  }

var page_init = function(page_config,$,Backbone,Handlebars,templates){

  $(function(){

    childstyle._int();

    var TreeModel = Backbone.Model.extend({
        idAttribute:"id",
        url :page_config.treeModel.url,
        defaults : {
            id : "",
            office_name : "",
            parentid : ""
        }
    });

    var TreeCollection = Backbone.Collection.extend({
        model:TreeModel,
        url:page_config.treeCollection.url,
        parse:function(response){
            return response.children;
        }
    });

    var AddNodeView = Backbone.View.extend({
        template : templates.addnode,
        initialize : function(clickFun){
            this.$el = $(this.template());
            this.$el.appendTo('body');
            this.clickFun = clickFun;
            this.$el.hover(function(){

            },function(){
                $(this).hide();
            });
        },
        render : function(page){
            this.$el.css({'left' : page.x, 'top' : page.y}).show();
        },
        events : {
            "click li" : "addNode"
        },
        addNode : function(){
            this.clickFun({pid : 0}, "新增节点");
        }
    });

    var TreeView = Backbone.View.extend({
        el:'#tree',
        template:templates.tree,
        collection:new TreeCollection(),
        initialize:function(){  
            var v = this;  
            v.$el.html(v.template({title : "产品线"}));
            v.$tree = v.$el.find(".tree_template");       
            this.$dialog = $("<div style='display:none;'></div>");
            this.addNodeView = new AddNodeView(function(){
                v.$dialog.html(templates.dialog());
                v.openDialog.apply(v, arguments);
            });
        },
        render : function(){
            var v = this;

            v.collection.fetch({
                success : function(collection){

                    v.$tree.jstree({
                        'core' : {
                            'data' : v.formatJSON(collection.toJSON()),
                            "check_callback" : true,
                            "themes" : { "stripes" : true }
                        },
                        "plugins" : [ "contextmenu"],
                        "contextmenu": {  
                            items:{
                                "create": {  
                                    "label": "增加节点", 
                                    //"icon" : "",
                                    "action": function (obj) { 

                                        v.$dialog.html(templates.dialog());

                                        v.openDialog({pid : obj.reference.attr('id')}, "新增节点");

                                    }  
                                }, 
                                "update": {  
                                    "label": "修改节点",  
                                    "action": function (obj) { 

                                        var model = {
                                            id : obj.reference.attr('id'),
                                            pid : obj.reference.attr('pid'),
                                            name : obj.reference.attr('name')
                                        };

                                        v.$dialog.html(templates.dialog({model : model}));

                                        v.openDialog(model, "修改节点");
                                    }  
                                }, 
                                "delete": {  
                                    "label": "删除节点",  
                                    "action": function (obj) { 

                                        if(v.$tree.jstree().is_parent({ id : obj.reference.attr('id')})){

                                            alert("此节点下面还有子节点, 请先删除子节点!");
                                            return;
                                        }

                                        var model = {
                                            id : obj.reference.attr('id'),
                                            pid : obj.reference.attr('pid'),
                                            name : obj.reference.attr('name')
                                        };

                                        v.$dialog.html(templates.dialog({model : model, isDel : true}));

                                        v.openDialog(new TreeModel(model), "删除节点", true);
                                    }  
                                },
                            }
                        } 
                    }).bind('click.jstree', function(event) {               
                        var eventNodeName = event.target.nodeName;               
                        if (eventNodeName == 'INS') {                   
                            return;               
                        } else if (eventNodeName == 'A') {                   
                            var $subject = $(event.target).parent();                   
                            
                            //选择的id值
                            alert($(event.target).parents('li').attr('id'));                   
                                          
                        }           
                    }).bind('contextmenu.jstree', function(event){
                        var eventNodeName = event.target.nodeName;
                        if(eventNodeName == "A" || eventNodeName == "I"){
                            return;
                        }
                        
                        v.addNodeView.render({x : event.pageX + 'px', y : event.pageY + 'px'});

                        return false;

                    }).bind('open_node.jstree', function(e, tree){
                        v.$tree.find('a#' + tree.node.id + ' .jstree-themeicon').css('background-image', 'url("img/folderopen.gif")');
                    }).bind('close_node.jstree', function(e, tree){
                        if(tree.node.children.length){
                            v.$tree.find('a#' + tree.node.id + ' .jstree-themeicon').css('background-image', 'url("img/folder.gif")');
                        }
                    });
                }
            });
            
        },
        openDialog : function(model, title, isDel){

            var v = this;

            v.$dialog.dialog({
                draggable:true,
                modal: true,
                title: title,
                buttons: [
                    {
                        text: "取消",
                        icons:{ primary:"ui-icon-cancel" },
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "确定",
                        icons:{ primary:"ui-icon-disk" },
                        click: function() {
                            if(isDel){
                                v.delCallback(model);
                            }else{
                                v.saveCallback(model);
                            }
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        },
        saveCallback : function(model){
            var v = this;
            var id = model.id;
            new TreeModel().save(
                { 
                    id : model.id,  
                    parentid : model.pid,
                    office_name : v.$dialog.find("input[name='name']").val()
                },
                {
                    success:function(model){
                        var json = model.toJSON();
                        var node = {
                                id : json.id,
                                parent : json.parentid === 0 ? '#' : json.parentid,
                                text : json.office_name,
                                icon : json.parentid !== 0 ? 'img/catalogOpen.gif' : 'img/page.gif',
                                a_attr : {
                                    id : json.id,
                                    pid : json.parentid,
                                    name : json.office_name
                                }
                           };

                        if(!id){

                            v.$tree.jstree().create_node({id : json.parentid === 0 ? '#' : json.parentid}, node, "last");

                            if(json.parentid === 0) {
                                return;
                            }

                            if(v.$tree.jstree().is_closed({id : json.parentid})){
                                v.$tree.jstree().open_node({id:json.parentid});
                            }else{
                                v.$tree.find('a#' + json.parentid + ' .jstree-themeicon').css('background-image', 'url("img/folderopen.gif")');
                            }

                        }else{

                            v.$tree.jstree().rename_node({id : json.id}, json.office_name);
                            v.$tree.find('a#' + json.id).attr('name', json.office_name);

                        }
                        
                    }
                }
            ); 
        },
        delCallback:function(model){
            var v = this;
            model.url = model.url + "?id="+model.get("id");
            model.destroy({
                success: function(model, response) {

                    var parent = v.$tree.jstree().get_parent({id : model.id});

                    v.$tree.jstree().delete_node({
                        id : model.id
                    });

                    if(!v.$tree.jstree().is_parent({id:parent})){
                        v.$tree.find('a#' + parent + ' .jstree-themeicon').css('background-image', 'url("img/page.gif")');
                        v.$tree.jstree().close_node({id:parent});
                    }else{
                         v.$tree.find('a#' + parent + ' .jstree-themeicon').css('background-image', 'url("img/folderopen.gif")');
                    }

                }
            });
        },
        formatJSON : function(json, arr){
            var arr = arr || [],
                len,
                i;

            for(i = 0, len = json.length; i < len; i ++){

                var node = json[i];

                arr.push({
                    id : node.id,
                    parent : node.pid === 0 ? '#' : node.pid,
                    text : node.name,
                    icon : node.children && node.children.length ? 'img/folder.gif' : (node.pid !== 0 ? 'img/catalogOpen.gif' : 'img/page.gif'),
                    state : {
                        opened : node.open === "true" ? true : false
                    },
                    a_attr : {
                        id : node.id,
                        pid : node.pid,
                        name : node.name
                    }
                });

                if(node.children && node.children.length){

                    this.formatJSON(node.children, arr);
                }
            }

            return arr;
        }
    });
    
    
    new TreeView().render();

  });
}; //page_init

if (typeof define === 'function' && define.amd) {
    define([
         "tree_config", 
         "jquery",
         "backbone",
         "handlebars",
         "templates",
         "jqueryui/dialog",
         "jstree"
    ],page_init);
}
else {
    page_init(page_config,$,Backbone,Handlebars,Handlebars.templates);
}


}).call(this);


 