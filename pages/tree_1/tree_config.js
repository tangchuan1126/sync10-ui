(function(){
    var configObj = {
            treeCollection: {
                url:"/Sync10/action/administrator/officeTree.action"
            },
            treeModel:{
                url:"/Sync10/action/administrator/officeTree.action"
            }
    };
    if (typeof define === 'function' && define.amd) {
        define(configObj);
    }
    else {
        //传统模式，非AMD标准
        this.page_config = configObj;
    }
}).call(this);
