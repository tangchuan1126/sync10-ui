define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addnode'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<ul class=\"vakata-context\">\n	<li class=\"\">\n		<a rel=\"0\" href=\"#\"><i></i><span class=\"vakata-contextmenu-sep\">&nbsp;</span>增加节点</a>\n	</li>\n</ul>";
},"useData":true});
templates['dialog'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "<label>"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "</label>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return "<input name=\"name\" value=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\"/>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isDel : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "<input name=\"pid\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pid : stack1), depth0))
    + "\" type=\"hidden\"/>\n<input name=\"id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" type=\"hidden\"/>";
},"useData":true});
templates['tree'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div>\n	<div class=\"tree-base\">\n		<i class=\"tree-base\"></i>\n		"
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n	</div>\n	<div class=\"tree_template\"></div>\n</div>";
},"useData":true});
return templates;
});