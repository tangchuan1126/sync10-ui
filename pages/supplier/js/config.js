﻿(function(){
    var configObj = {

        //商品列表
        supplierUrl:{
            url:"/Sync10/basicdata/supplier"
        },
        //商品基本信息
        supplierInfoUrl:{
            url:"/Sync10/basicdata/supplierInfo"
        },
        fileUrl:{
            url:"/Sync10/basicdata/supplier/file"
        },
        supplierContextUrl:{
            url:"/Sync10/basicdata/supplier/context/web"
        }
    };
    
	define(configObj);
	
}).call(this);
