﻿"use strict";
define([
    "js/view/supplier-table",
    "js/view/supplier-search",
    "js/view/supplier-picture",
    "js/view/supplier-update",
    "js/view/supplier-info"
],function(
    SupplierTable,
    SupplierSearch,
    SupplierPicture,
    SupplierUpdate,
    SupplierInfo
){
    return {
        get:function(option){
            if(!this[option.name]){
                this.create(option);
            }else{
                this.update(option);
            }
            return this[option.name];
        },
        create:function(option){
            switch(option.name){

                case "SupplierTable": this[option.name] = new SupplierTable({el:option.el, view:this});break;
                case "SupplierSearch": this[option.name] = new SupplierSearch({el:option.el, view:this});break;
                case "SupplierPicture": this[option.name] = new SupplierPicture({el:option.el, view:this});break;
                case "SupplierUpdate": this[option.name] = new SupplierUpdate({el:option.el, view:this});break;
                case "SupplierInfo": this[option.name] = new SupplierInfo({el:option.el, view:this});break;
            }
        },
        update:function(option){
            this[option.name].setElement(option.el);
        }
    }
});