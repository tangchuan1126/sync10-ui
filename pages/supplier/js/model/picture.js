"use strict";
define([
    "../config",
    "jquery",
    "backbone"
],function(page_config,$,Backbone){
    
    var FileModel = Backbone.Model.extend({
        url: page_config.fileUrl.url,
        idAttribute: "file_id",
        defaults: {
            file_with_id: 0 ,
            file_with_type: 0 ,
            file_with_class: 0 ,
            expire_in_secs: 0
        }
    });

    var FileColl = Backbone.Collection.extend({
        url: page_config.fileUrl.url,
        parse: function (response) {
            
            return response.file;
        }
    });

    return {
        FileModel:FileModel
        ,FileColl:FileColl
    };
});