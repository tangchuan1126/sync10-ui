define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['rest_req_params'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<table>\n<tbody>\n    <tr>\n        <th>请求URI</th>\n        <td colspan=\"3\">\n            <input type=\"text\" id=\"params-uri\" class=\"uri-input\"/>         \n        </td>\n    </tr>\n    <tr>\n        <th>请求METHOD</th>\n        <td colspan=\"3\">\n            <select id=\"params-method\">\n                 <option value=\"GET\" selected>GET</option> \n                 <option value=\"POST\" >POST</option>\n                 <option value=\"PUT\">PUT</option>\n                 <option value=\"DELETE\">DELETE</option>\n            </select>\n        </td>\n    </tr>\n    <tr>\n        <th>请求BODY<br/>(JSON编辑器)</th>\n        <td>\n            <div id=\"params-body\" style=\"width: 550px; height: 300px;\"></div>\n        </td>\n        <th>\n            <button id=\"clear-req\">清除</button><br/>\n        </th>\n        <th>\n            <button id=\"submit-req\">提交</button>\n        </th>\n    </tr>\n</tbody>\n</table>\n";
},"useData":true});
templates['rest_res'] = template({"1":function(depth0,helpers,partials,data) {
    return "            <span class=\"error-message\">错误信息</span>\n";
},"3":function(depth0,helpers,partials,data) {
    return "             <span class=\"response-message\">响应内容</span>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "        <pre><code>"
    + this.escapeExpression(((helper = (helper = helpers.error || (depth0 != null ? depth0.error : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"error","hash":{},"data":data}) : helper)))
    + "</code></pre>\n";
},"7":function(depth0,helpers,partials,data) {
    var helper;

  return "        <pre><code class=\"json\">"
    + this.escapeExpression(((helper = (helper = helpers.response || (depth0 != null ? depth0.response : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"response","hash":{},"data":data}) : helper)))
    + "</code></pre>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return " <fieldset>\n     <legend>\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.error && this.errorThrown",{"name":"xif","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "    </legend>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.error : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "</fieldset>\n";
},"useData":true});
return templates;
});