(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['rest_req_params'] = template({"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  return "<table>\r\n<tbody>\r\n    <tr>\r\n        <th>请求URI</th>\r\n        <td colspan=\"3\">\r\n            <input type=\"text\" id=\"params-uri\" class=\"uri-input\"/>         \r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <th>请求METHOD</th>\r\n        <td colspan=\"3\">\r\n            <select id=\"params-method\">\r\n                 <option value=\"GET\" selected>GET</option> \r\n                 <option value=\"POST\" >POST</option>\r\n                 <option value=\"PUT\">PUT</option>\r\n                 <option value=\"DELETE\">DELETE</option>\r\n            </select>\r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <th>请求BODY<br/>(JSON编辑器)</th>\r\n        <td>\r\n            <div id=\"params-body\" style=\"width: 550px; height: 300px;\"></div>\r\n        </td>\r\n        <th>\r\n            <button id=\"clear-req\">清除</button><br/>\r\n        </th>\r\n        <th>\r\n            <button id=\"submit-req\">提交</button>\r\n        </th>\r\n    </tr>\r\n</tbody>\r\n</table>\r\n";
  },"useData":true});
templates['rest_res'] = template({"1":function(depth0,helpers,partials,data) {
  return "\r\n            <span class=\"error-message\">错误信息</span>\r\n         ";
  },"3":function(depth0,helpers,partials,data) {
  return "\r\n             <span class=\"response-message\">响应内容</span>\r\n         ";
  },"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", escapeExpression=this.escapeExpression;
  return "\r\n        <pre><code>"
    + escapeExpression(((helper = helpers.error || (depth0 && depth0.error)),(typeof helper === functionType ? helper.call(depth0, {"name":"error","hash":{},"data":data}) : helper)))
    + "</code></pre>\r\n    ";
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", escapeExpression=this.escapeExpression;
  return "\r\n        <pre><code class=\"json\">"
    + escapeExpression(((helper = helpers.response || (depth0 && depth0.response)),(typeof helper === functionType ? helper.call(depth0, {"name":"response","hash":{},"data":data}) : helper)))
    + "</code></pre>\r\n    ";
},"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, buffer = " <fieldset>\r\n     <legend>\r\n         ";
  stack1 = (helper = helpers.xif || (depth0 && depth0.xif) || helperMissing,helper.call(depth0, "this.error && this.errorThrown", {"name":"xif","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data}));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n    </legend>\r\n    ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.error), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\r\n</fieldset>\r\n";
},"useData":true});
})();