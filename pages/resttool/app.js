define([
    './config',
    'jquery',
    'backbone',
    'handlebars',
    'handlebars_ext',
    'templates',
    'jsoneditor',
    'highlightjs',
    'jqueryui/tabs',
    'jqueryui/button',
    'jqueryui/autocomplete'
],
function(config,$,Backbone,Handlebars,HandlebarsExt,templates,JSONEditor,hljs){
    $(function(){
       var RestServiceUri = Backbone.Model.extend({
           defaults:{ uri:"" }
       });
       var RestServiceUris = Backbone.Collection.extend({
           model:RestServiceUri,
           url:config.restServiceUris,
           findByUriPattern: function(uriPattern) {
             var models =  this.filter(function(m) {
                return m.get("uri").match(new RegExp(uriPattern,"i"));
              });
             return new RestServiceUris(models);
           },
           toSuggests:function(){
              return this.toJSON().map(function(m){ return { label:m.uri, value:m.uri }; } );
           }
       });
 
       $("#tabs").tabs(); 
       var ParamsView = Backbone.View.extend({
            el:"#rest_req_params",
            template:templates.rest_req_params,
            collection: new RestServiceUris(),
            events:{
                "click button#submit-req":"sendReq",
                "click button#clear-req":"clearReq",
                "change select#params-method":"changeMethod"
            },
            sendReq:function(){
                var settings = {
                    dataType:"json"
                };
                settings.url = this.$el.find("#params-uri").val();
                if(!settings.url || settings.url.length==0) return;
                settings.type = this.$el.find("#params-method").val();
                if(settings.type != "GET" && settings.type != "DELETE"){
                    //has body
                    settings.contentType = "application/json; charset=UTF-8";
                    settings.data = JSON.stringify(this.jsonEditor.get());
                }
                var v = this;
                settings.success=function(data,textStatus,jqXHR){
                        $("#rest_res").html(
                            templates.rest_res({response:JSON.stringify(data,null,4)})
                        );
                        $('pre code').each(function(i, e) {hljs.highlightBlock(e)});
                        $("#tabs").tabs("option","active",1);
                        if(! v.collection.findWhere({ uri: settings.url })){
                            v.collection.unshift(new RestServiceUri({ uri:settings.url }));
                        }
                    };
                settings.error=function( jqXHR, textStatus, errorThrown ){
                        console.log("status="+textStatus);
                        console.log("error="+errorThrown);
                        $("#rest_res").html(
                            templates.rest_res({error:jqXHR.responseText, errorThrown:errorThrown})
                        );
                        $('pre code').each(function(i, e) {hljs.highlightBlock(e)});
                        $("#tabs").tabs("option","active",1);
                    };
                //send the request
                $.ajax(settings);
            },
            clearReq:function(){
                this.jsonEditor.set({});
            },
            changeMethod:function(){
                if($("#params-method").val().match(/GET|DELETE/)){
                    $("#params-body").hide();
                    $("#clear-req").hide();
                }
                else {
                    $("#params-body").show();
                    $("#clear-req").show();
                }
            },
            render:function(){
                this.$el.html(this.template({}));
                this.$el.find("button#submit-req").button({ icons: { primary: "ui-icon-play" } });
                this.$el.find("button#clear-req").button({ icons: { primary: "ui-icon-arrowrefresh-1-e" } });
                this.jsonEditor = new JSONEditor($("#params-body").get(0));
                this.changeMethod();
                var v = this;
                this.collection.fetch({
                    success:function(collection){
                        $("#params-uri").autocomplete({
                            source: function(req,respCallback){
                                var matched = collection.findByUriPattern(req.term);
                                respCallback(matched.toSuggests());
                            }
                        });
                    }
                });

            }
       });
       new ParamsView().render();
    });    
});
