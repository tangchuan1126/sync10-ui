﻿/**
    *create by cuixiang 20141202
*/
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "bootstrap",
    "nprogress",
    "immybox",

    "js/view/advancedSearch",
    "js/view/luceneSearch",
    "js/view/appiontmentList",
    "js/view/addAppiontment",

    "blockui",
    "jqueryui/tabs"
], function ($, Backbone, handlebars, bootstrap, NProgress, immybox, advancedSearch, luceneSearch, appiontmentList, addAppiontment) {
        NProgress.start();
        //Tab切换.
        $('#listTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#listTab a:last-child').tab('show');

        //创建视图.
        var advancedSearchView = new advancedSearch();
        advancedSearchView.render();

        var luceneSearchView = new luceneSearch();
        luceneSearchView.render();

        var appiontmentListView = new appiontmentList();
        appiontmentListView.render();

        var addAppiontmentView = new addAppiontment();
        addAppiontmentView.render();

        //var fromTree = new AsynLoadQueryTree({
        //    renderTo: "#searchFrom",
        //    selectid: { value: v.shipFrom },
        //    PostData: { rootType: "Ship From" },
        //    dataUrl: "./json/shipTo.json",
        //    scrollH: 400,
        //    multiselect: false,
        //    Async: true,
        //    placeholder: "Ship From"
        //});
        //fromTree.render();


        $.getJSON("json/carrier.json", function (result) {
            $('#inputQuickCarrier').immybox({
                choices: result
            });
        });

        $('.quick-create').on('click', function (e) {
            e.stopPropagation();
            return false;
        });

        NProgress.done();
    }
);