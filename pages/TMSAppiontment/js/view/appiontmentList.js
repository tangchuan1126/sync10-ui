﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "metisMenu", "CompondList/View", "../config/appiontmentConfig", "../config/jsontohtml_templates"],
    function ($, Backbone, handlebars, templates, metisMenu,CompondList,list_config,listTemplates) {
    return Backbone.View.extend({
        render: function () {
            var list = new CompondList(list_config, {
                renderTo: "#appiontmentList",
                dataUrl: "../WMSAppiontment/json/appiontmentList.json",
                PostData: { "A": "11", "b": "22" }
            });
            list.render();
        }
    });
});