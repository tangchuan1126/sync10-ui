﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "createBoxPanel"], function ($, Backbone, handlebars, templates, CompondBox, createBoxPanel) {
    return Backbone.View.extend({
        el: "#initCarrier",
        template: templates.addCarrier,
        render: function () {
            var v = this;

            var temp = new CompondBox({
                content: v.template().trim(),
                title: "预约仓位",
                container: v.el,
                isExpand: true
            });


            $("#btnAddCarrier").on("click", function () { v.addCarrier(); });

        },
        addCarrier: function () {
            var v = this;
            var createPanelView = new createBoxPanel({
                content: v.template().trim(),
                width: '',
                container: v.el,
                close: function () { return v.delBol(); }
            });
        }
    });
});