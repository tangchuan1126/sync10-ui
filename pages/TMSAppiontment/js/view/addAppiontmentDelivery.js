﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox"], function ($, Backbone, handlebars, templates, CompondBox) {
    return Backbone.View.extend({
        el: "#DeliveryAddress",
        template: templates.addDelivery,
        render: function () {
            var v = this;
            var temp = new CompondBox({
                content: v.template().trim(),
                title: "发货地址",
                container: v.el,
                isExpand: true
            });
        }
    });
});