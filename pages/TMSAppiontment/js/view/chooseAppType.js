﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "createBoxPanel", "art_Dialog/dialog-plus", 
    "js/view/addAppiontmentDelivery",
    "js/view/addAppiontmentReceiving",
    "js/view/addAppiontmentBol",
    "js/view/initCarrier", ], function ($, Backbone, handlebars, templates, CompondBox, createBoxPanel, Dialog,
        AddAppiontmentDelivery,
        AddAppiontmentReceiving,
        AddAppiontmentBol,
        InitCarrier) {
    return Backbone.View.extend({
        el: "#chooseType",
        template: templates.ChoseAppiontmentType,
        render: function () {
            var v = this;
            console.log(v.el);
            $(v.template().trim()).appendTo(v.el).find("#appType1tab").on("click", function () { v.clearChooseType();v.appType1tabFun(); }).end()
                .find("#appType2tab").on("click", function () { v.clearChooseType(); v.appType2tabFun(); }).end()
                .find("#appType3tab").on("click", function () { v.clearChooseType(); v.appType3tabFun(); }).end()
                .find("#appType4tab").on("click", function () { v.clearChooseType(); v.appType4tabFun(); }).end()
                .find("#appType5tab").on("click", function () { v.clearChooseType(); v.appType5tabFun(); }).end();
        },
        clearChooseType: function () {
            $("#chooseType").html("");
        },
        appType1tabFun: function () {
            $(templates.app1Tab().trim()).appendTo("#addAppionmentBox");

            var addAppiontmentDeliveryView = new AddAppiontmentDelivery();
            addAppiontmentDeliveryView.render();

            var addAppiontmentReceivingView = new AddAppiontmentReceiving();
            addAppiontmentReceivingView.render();

            var initCarrierView = new InitCarrier();
            initCarrierView.render();

            var addAppiontmentBolView = new AddAppiontmentBol();
            addAppiontmentBolView.render();

            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('#myTab a:first').tab('show');
        },
        appType2tabFun: function () {
            $(templates.app1Tab().trim()).appendTo("#addAppionmentBox");

            var addAppiontmentDeliveryView = new AddAppiontmentDelivery();
            addAppiontmentDeliveryView.render();

            var addAppiontmentReceivingView = new AddAppiontmentReceiving();
            addAppiontmentReceivingView.render();

            var initCarrierView = new InitCarrier();
            initCarrierView.render();

            var addAppiontmentBolView = new AddAppiontmentBol();
            addAppiontmentBolView.render();

            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('#myTab a:first').tab('show');
        },
        appType3tabFun: function () {
            $(templates.app1Tab().trim()).appendTo("#addAppionmentBox");

            var addAppiontmentDeliveryView = new AddAppiontmentDelivery();
            addAppiontmentDeliveryView.render();

            var addAppiontmentReceivingView = new AddAppiontmentReceiving();
            addAppiontmentReceivingView.render();

            var initCarrierView = new InitCarrier();
            initCarrierView.render();

            var addAppiontmentBolView = new AddAppiontmentBol();
            addAppiontmentBolView.render();

            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('#myTab a:first').tab('show');
        },
        appType4tabFun: function () {
            $(templates.app1Tab().trim()).appendTo("#addAppionmentBox");

            var addAppiontmentDeliveryView = new AddAppiontmentDelivery();
            addAppiontmentDeliveryView.render();

            var addAppiontmentReceivingView = new AddAppiontmentReceiving();
            addAppiontmentReceivingView.render();

            var initCarrierView = new InitCarrier();
            initCarrierView.render();

            var addAppiontmentBolView = new AddAppiontmentBol();
            addAppiontmentBolView.render();

            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('#myTab a:first').tab('show');
        },
        appType5tabFun: function () {
            $(templates.app1Tab().trim()).appendTo("#addAppionmentBox");

            var addAppiontmentDeliveryView = new AddAppiontmentDelivery();
            addAppiontmentDeliveryView.render();

            var addAppiontmentReceivingView = new AddAppiontmentReceiving();
            addAppiontmentReceivingView.render();

            var initCarrierView = new InitCarrier();
            initCarrierView.render();

            var addAppiontmentBolView = new AddAppiontmentBol();
            addAppiontmentBolView.render();

            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('#myTab a:first').tab('show');
        }
    });
});