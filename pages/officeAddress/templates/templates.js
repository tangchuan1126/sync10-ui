(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addnode'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<ul class=\"vakata-context\">\r\n	<li class=\"\">\r\n		<a rel=\"0\" href=\"#\"><i class=\"fa fa-plus\"></i><span class=\"vakata-contextmenu-sep\">&nbsp;</span>ADD</a>\r\n	</li>\r\n</ul>";
  },"useData":true});
templates['dialog'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<label>delete \""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" and all of the following office address？</label>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<input name=\"name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\"/>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isDel : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "<input name=\"pid\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pid : stack1), depth0))
    + "\" type=\"hidden\"/>\r\n<input name=\"id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" type=\"hidden\"/>";
},"useData":true});
templates['query'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <tr>\r\n                          \r\n            <th width=\"100%\" align=\"center\" class=\"left-title\" style=\"vertical-align: center;text-align: center;\" colspan=\"3\">"
    + escapeExpression(lambda((depth0 != null ? depth0.office_name : depth0), depth0))
    + "</th>\r\n        \r\n        </tr>\r\n        \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n        	<td align=\"right\" ><font size=\"3\"><strong>Address1:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.house_number : depth0), depth0))
    + " </td>\r\n        </tr>\r\n        \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n        	<td align=\"right\" ><font size=\"3\"><strong>Address2:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.street : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n        \r\n         <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n       	 	<td align=\"right\"><font size=\"3\"><strong>City:</font></td>\r\n       		<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.city : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n         \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n       		<td align=\"right\"><font size=\"3\"><strong>Zip Code:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.zip_code : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n        \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n       		<td align=\"right\"><font size=\"3\"><strong>Province:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.provinces_name : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n        \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n       		<td align=\"right\"><font size=\"3\"><strong>Nation:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.nation_name : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n        \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n       		<td align=\"right\"><font size=\"3\"><strong>Contact:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.contact : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n        \r\n        <tr data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n       		<td align=\"right\"><font size=\"3\"><strong>Phone:</font></td>\r\n        	<td><strong>"
    + escapeExpression(lambda((depth0 != null ? depth0.phone : depth0), depth0))
    + " </strong></td>\r\n        </tr>\r\n   \r\n        <tr>\r\n		<td> </td>\r\n		<td align=\"center\"> <span data-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\"> <button  id=\"button1\">Update</button></span></td>\r\n        </tr>\r\n        \r\n        \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = " <table class=\"zebraTable\">\r\n    <tbody>\r\n\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.Address : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        \r\n    </tbody>\r\n   \r\n</table>\r\n		";
},"useData":true});
templates['selectNation'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.cid : depth0), depth0))
    + "\" ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.selected : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">"
    + escapeExpression(lambda((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "selected";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n     			      <select id=\"selectNation\" name=\"nation\" style=\"width:206px;\">\r\n                            \r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.Nation : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        </select>\r\n\r\n";
},"useData":true});
templates['selectProvinces'] = template({"1":function(depth0,helpers,partials,data) {
  return "                            \r\n";
  },"3":function(depth0,helpers,partials,data) {
  return "                            <option value=\"0\">input</option>\r\n";
  },"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.pro_id : depth0), depth0))
    + "\"  ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.selected : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">"
    + escapeExpression(lambda((depth0 != null ? depth0.pro_name : depth0), depth0))
    + "</option>\r\n";
},"6":function(depth0,helpers,partials,data) {
  return "selected";
  },"8":function(depth0,helpers,partials,data) {
  var stack1, buffer = "                            <option value=\"0\" ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isInput : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">input</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n                        <select id=\"selectProvinces\" name=\"provinces\" style=\"width:150px;\" >\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.Province : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.Province : depth0), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.Province : depth0), {"name":"if","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        </select> \r\n\r\n                        ";
},"useData":true});
templates['tree'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div>\r\n	<div class=\"tree-base\">\r\n		<i class=\"tree-base\"></i>\r\n		<a href=\"javascript:;\" class=\"main-node\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</a>\r\n	</div>\r\n	<div class=\"tree_template\"></div>\r\n</div>\r\n\r\n";
},"useData":true});
templates['update_address'] = template({"1":function(depth0,helpers,partials,data) {
  return "                           \r\n";
  },"3":function(depth0,helpers,partials,data) {
  return "                            <option value=\"0\">input</option>\r\n";
  },"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.pro_id : depth0), depth0))
    + "\"  ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.selected : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + ">"
    + escapeExpression(lambda((depth0 != null ? depth0.pro_name : depth0), depth0))
    + "</option>\r\n";
},"6":function(depth0,helpers,partials,data) {
  return "selected";
  },"8":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.cid : depth0), depth0))
    + "\" >"
    + escapeExpression(lambda((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<table>\r\n<tbody>\r\n\r\n	    \r\n    <tr height=\"35\">\r\n        <th  align=\"right\">Address1 : </th>\r\n        <td>\r\n            <input type=\"text\" name=\"house_number\" style=\"width:450px;\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.house_number : stack1), depth0))
    + "\" />\r\n        </td>\r\n    \r\n    </tr> \r\n    <tr height=\"35\">\r\n        <th  align=\"right\">Address2 : </th>\r\n        <td>\r\n            <input type=\"text\" name=\"street\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.street : stack1), depth0))
    + "\" style=\"width:450px;;\" />\r\n        </td>\r\n    </tr>\r\n    <tr height=\"35\">\r\n        <th  align=\"right\">City : </th>\r\n        <td>\r\n            <input type=\"text\" name=\"city\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\" style=\"width:450px;\"/>\r\n        </td>\r\n    </tr>   \r\n    \r\n      <tr height=\"35\">\r\n        <th  align=\"right\">Zip Code : </th>\r\n        <td>\r\n            <input type=\"text\" name=\"zip_code\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.zip_code : stack1), depth0))
    + "\" style=\"width:245px;\" />\r\n        </td>\r\n    </tr>\r\n     \r\n      <tr height=\"35\">\r\n        <th  align=\"right\"><b class=\"require_property\">*</b>Province :</th>\r\n        <td>\r\n               \r\n                        <select id=\"selectProvinces\" name=\"provinces\" style=\"width:250px;\" >\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.Province : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.Province : depth0), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "                	    </select>\r\n						<input type=\"text\" name=\"provinces_name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.provinces_name : stack1), depth0))
    + "\"  style=\"display : none; width:198px; height:22px;\"/>\r\n        </td>\r\n    </tr>\r\n    \r\n      <tr height=\"35\">\r\n        <th align=\"right\" ><b class=\"require_property\">*</b>Nation :</th>\r\n        <td>\r\n           \r\n            <div>	\r\n                        <select id=\"selectNation\" name=\"nation\" style=\"width:250px;\">\r\n                         \r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.Nation : depth0), {"name":"each","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        </select>\r\n                        <input type=\"text\" name=\"nation_name\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.nation_name : stack1), depth0))
    + "\"  style=\"display:none\"  />\r\n                         \r\n            </div>\r\n        </td>\r\n    </tr>\r\n     <tr height=\"35\">\r\n        <th align=\"right\">Contact :</th>\r\n        <td>\r\n            <input type=\"text\" name=\"contact\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\" style=\"width:245px;\"/>\r\n        </td>\r\n    </tr>\r\n    	\r\n     <tr height=\"35\">\r\n        <th align=\"right\">Phone :</th>\r\n        <td>\r\n            <input type=\"text\" name=\"phone\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\" style=\"width:245px;\"/>\r\n        </td>\r\n    </tr>\r\n       \r\n</tbody>\r\n</table>";
},"useData":true});
})();