"use strict";

(function() {

	//	遮罩

	// 创建样式
	var childstyle = {
		global: {
			path: "/Sync10-ui/bower_components"
		},
		_int: function() {
			this.jstreestyle();
			this.mainstyle();
		},
		jstreestyle: function() {
			var jstree = document.getElementById("jstreestyle");
			if (jstree == null) {

				var container = document.getElementsByTagName("head")[0];
				var jstreelink = document.createElement("link");
				jstreelink.id = "jstreestyle";
				jstreelink.rel = "stylesheet";
				jstreelink.type = "text/css";
				jstreelink.media = "screen";
				jstreelink.href = this.global.path + "/jstree/dist/themes/default/style.css";
				container.appendChild(jstreelink);

			}
		},
		mainstyle: function() {
			var mainstyle = document.getElementById("mainstyle");
			if (mainstyle == null) {

				var container = document.getElementsByTagName("head")[0];
				var mainlink = document.createElement("link");
				mainlink.id = "jstreestyle";
				mainlink.rel = "stylesheet";
				mainlink.type = "text/css";
				mainlink.media = "screen";
				mainlink.href = "css/style.css";
				container.appendChild(mainlink);

			}
		}

	}

	var page_init = function(page_config, $, Backbone, Handlebars, templates) {


		$(function() {
			$.blockUI.defaults = {
				css: {
					padding: '8px',
					margin: 0,
					width: '170px',
					top: '45%',
					left: '40%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #999999',
					backgroundColor: '#ffffff',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
					'-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
				},
				overlayCSS: {
					backgroundColor: '#000',
					opacity: '0.3'
				},
				baseZ: 99999,
				centerX: true,
				centerY: true,
				fadeOut: 1000,
				showOverlay: true
			};

			var addressDialog = function(office_name, saveCallback) {
				$.unblockUI();
				//	console.log(office_name);
				//alert(saveCallback);
				$("#address").dialog({

					draggable: true,
					modal: true,
					title: office_name,
					width: 650,
					close:function(){

						postnobt=true;

					},
					opened: function() {

						//$(".ui-dialog-titlebar-close").hide();
					},

					buttons: [{
							text: "submit",
							icons: {
								primary: "ui-icon-disk"
							},
							click: function() {
								saveCallback();
								postnobt=true;
								//	$(this).dialog("close");
							}
						}, {
							text: "cancel",
							icons: {
								primary: "ui-icon-cancel"
							},
							click: function() {
								$(this).dialog("close");
								postnobt=true;
							}
						},

					]
				});
			};
			childstyle._int();

			var TreeModel = Backbone.Model.extend({
				idAttribute: "id",
				url: page_config.treeModel.url,
				defaults: {
					id: "",
					office_name: "",
					parentid: ""
				}
			});

			var TreeCollection = Backbone.Collection.extend({
				model: TreeModel,
				url: page_config.treeCollection.url,
				parse: function(response) {
					return response.children;
				}
			});

			var AddNodeView = Backbone.View.extend({
				template: templates.addnode,
				initialize: function(clickFun) {
					this.$el = $(this.template());
					this.$el.appendTo('body');
					//this.$el.appendTo('div.tree-base');

					this.clickFun = clickFun;
					this.$el.hover(function() {

					}, function() {
						$(this).hide();
					});
				},
				render: function(page) {
					this.$el.css({
						'left': page.x,
						'top': page.y

					}).show();
				},
				events: {
					"click li": "addNode"
				},
				addNode: function() {
					$.blockUI({
						message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/>'
					});
					this.clickFun({
						pid: 0
					}, "ADD", $.unblockUI());
				}

			});

			var TreeView = Backbone.View
				.extend({
					el: '#tree',
					template: templates.tree,
					collection: new TreeCollection(),
					initialize: function() {
						var v = this;
						v.$el.html(v.template({
							title: ""
						}));
						v.$tree = v.$el.find(".tree_template");
						this.$dialog = $("<div style='display:none;'></div>");
						this.addNodeView = new AddNodeView(function() {
							v.$dialog.html(templates.dialog());
							v.openDialog.apply(v, arguments);
						});
					},
					events: {
						"contextmenu div.tree-base": "addMainNode"

					},
					addMainNode: function(event) {
						this.addNodeView.render({
							x: event.pageX,
							y: event.pageY
						});


						return false;
					},
					render: function() {

						 $.blockUI({message:'<img src="img/loadingAnimation6.gif" align="absmiddle"/>'});
						// $.blockUI({
						// 	message: 'message:'<img src="img/loadingAnimation6.gif" align="absmiddle"/>
						// });
						var v = this;

						v.collection.fetch({
							success: function(collection) {
								$.unblockUI();
								v.$tree.jstree({
									'core': {
										'data': v.formatJSON(collection.toJSON()),
										"check_callback": true,
										"multiple": false,

										"themes": {
											"stripes": true
										}
									},
									"plugins": ["contextmenu", "unique"],
									"contextmenu": {
										items: {
											"create": {
												"label": "ADD",
												"icon": "fa fa-plus",
												modal: true,
												"action": function(obj) {
													$.blockUI({
														message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/>'
													});
													v.$dialog.html(templates.dialog());
													var title = obj.reference.text();
													v.openDialog({
															pid: obj.reference.attr('id')
														},
														"ADD>>" + title);
													$.unblockUI();

												}
											},
											"update": {
												"label": "EDIT",
												"icon": "fa fa-pencil-square-o",
												"action": function(obj) {
													$.blockUI({
														message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/>'
													});
													var model = {
														id: obj.reference.attr('id'),
														pid: obj.reference.attr('pid'),
														name: obj.reference.text()
													};

													v.$dialog.html(templates.dialog({
														model: model
													}));
													var title = obj.reference.text();
													// console.log(obj);
													// console.log(title);
													v.openDialog(model, "MOD >>" + title, $.unblockUI());
												}
											},
											"delete": {
												"label": "DELETE",
												"icon": "fa fa-times",
												"action": function(obj) {
													$.blockUI({
														message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/>'
													});

													var model = {
														id: obj.reference.attr('id'),
														pid: obj.reference.attr('pid'),
														name: obj.reference.text()
													};

													v.$dialog.html(templates.dialog({
														model: model,
														isDel: true
													}));
													var title = obj.reference.text();

													v.delDialog(new TreeModel(model), "DELETE >>" + title, true, $.unblockUI());
												}
											},
										}
									}
								})
									.bind(
										'click.jstree',
										function(event) {

											postnobt=true;
											var eventNodeName = event.target.nodeName;
											if (eventNodeName == 'INS') {
												return;
											} else if (eventNodeName == 'A') {
												var $subject = $(event.target).parent();

												// 选择的id值
												var id = $(event.target).parents('li').attr('id');
												new AddressView().render(id);

												// alert(id);

											}
										})
									.bind('contextmenu.jstree', function(event) {

										return false;

									})
									.bind('open_node.jstree', function(e, tree) {
										//														
									})
									.bind(
										'close_node.jstree',
										function(e, tree) {
											if (tree.node.children.length) {
												// v.$tree
												// 		.find(
												// 				'a#'
												// 						+ tree.node.id
												// 						+ ' .jstree-themeicon')
												// 		.css(
												// 				'background-image',
												//						'url("img/folder.gif")');
											}
										});
							}
						});

					},
					openDialog: function(model, title, isDel) {
						$.unblockUI();
						var v = this;

						v.$dialog.dialog({
							draggable: true,
							modal: true,
							title: title,
							buttons: [{
								text: "submit",
								icons: {
									primary: "ui-icon-disk"
								},
								id: 'submit_dialog',
								click: function() {
									if (isDel) {
										v.delCallback(model);
										$(this).dialog("close");
									} else {
										v.saveCallback(model);
									}
									// $(this).dialog("close");
								}
							}, {
								text: "cancel",
								icons: {
									primary: "ui-icon-cancel"
								},
								click: function() {
									$(this).dialog("close");
								}
							}]
						});

						//keypress
						v.$dialog.on('keydown', function(event) {
							if (event.which == 13) {
								$('#submit_dialog').trigger('click');
							}
						});

					},
					delDialog: function(model, title, isDel) {
						$.unblockUI();
						var v = this;

						v.$dialog.dialog({
							draggable: true,
							modal: true,
							title: title,
							buttons: [{
								text: "Suer",
								icons: {
									primary: "ui-icon-disk"
								},
								id: 'submit_dialog',
								click: function() {
									if (isDel) {
										v.delCallback(model);
										$(this).dialog("close");
									} else {
										v.saveCallback(model);
									}
									// $(this).dialog("close");
								}
							}, {
								text: "cancel",
								icons: {
									primary: "ui-icon-cancel"
								},
								click: function() {
									$(this).dialog("close");
								}
							}]
						});

						//keypress
						// v.$dialog.on('keydown', function(event) {
						// 	if (event.which == 13) {
						// 		$('#submit_dialog').trigger('click');
						// 	}
						// });

					},

					saveCallback: function(model) {

						var v = this;
						var id = model.id;
						new TreeModel()
							.save({
								id: model.id,
								parentid: model.pid,
								office_name: v.$dialog.find("input[name='name']").val().replace(/(^\s*)|(\s*$)/g, "")
							}, {
								success: function(model) {
									var json = model.toJSON();
									var office_name = json.office_name;
									var node = {
										id: json.id,
										parent: json.parentid === 0 ? '#' : json.parentid,
										text: json.office_name,
										a_attr: {
											id: json.id,
											pid: json.parentid,
											name: json.office_name
										}
									};
									if (office_name == "") {
										showMessage("Enter the full name.", "alert");
										return false
									};

									if (!id) {


										//	console.log("add:"+model.get("success"));
										if (model.get("success")) {

											v.$tree.jstree().create_node({
												id: json.parentid === 0 ? '#' : json.parentid
											}, node, "last");


											if (json.parentid === 0) {
												v.$tree.jstree().deselect_node(v.$tree.jstree().get_selected());
												$('#' + json.id + '.jstree-anchor').trigger('click')

											} else {
												if (v.$tree.jstree().is_closed({
													id: json.parentid
												})) {
													v.$tree.jstree().open_node({
														id: json.parentid
													});
												}
												v.$tree.jstree().deselect_node(v.$tree.jstree().get_selected());
												$('#' + json.id + '.jstree-anchor').trigger('click')

											}

											v.$dialog.dialog("close");
										} else {

											showMessage("Repeat.", "alert");

										}
									} else {
										//	console.log("update:" + model.get("success"));
										if (model.get("success")) {

											v.$tree.jstree().rename_node({
												id: json.id
											}, json.office_name);

											v.$tree.find('a#' + json.id).attr('name', json.office_name);

											if (json.parentid === 0) {
												v.$tree.jstree().deselect_node(v.$tree.jstree().get_selected());
												$('#' + json.id + '.jstree-anchor').trigger('click')

											} else {
												if (v.$tree.jstree().is_closed({
													id: json.parentid
												})) {
													v.$tree.jstree().open_node({
														id: json.parentid
													});
												}
												v.$tree.jstree().deselect_node(v.$tree.jstree().get_selected());
												$('#' + json.id + '.jstree-anchor').trigger('click')

											}
											v.$dialog.dialog("close");


										} else {
											showMessage("Repeat.", "alert");
										}


									}
								}
							});
					},
					delCallback: function(model) {
						//		console.log(model);
						var v = this;
						model.url = model.url + "?id=" + model.get("id");
						model.destroy({
							success: function(model, response) {

								var parent = v.$tree.jstree().get_parent({
									id: model.id
								});


								if (!response.success) {
									showMessage("Cann't delete, Users with this address.", "alert");
								} else {
									v.$tree.jstree().delete_node({
										id: model.id
									});
									$('#query').empty();
									//parent.location.reload();
								}


							}
						});
					},
					formatJSON: function(json, arr) {
						var arr = arr || [],
							len, i;

						for (i = 0, len = json.length; i < len; i++) {

							var node = json[i];

							arr
								.push({
									id: node.id,
									parent: node.pid === 0 ? '#' : node.pid,
									text: node.name,
									//											icon : node.children
									//													&& node.children.length ? 'img/folder.gif'
									//													: (node.pid !== 0 ? 'img/catalogOpen.gif'
									//															: 'img/page.gif'),
									state: {
										opened: node.open === "true" ? true : false
									},
									a_attr: {
										id: node.id,
										pid: node.pid,
										name: node.name
									}
								});

							if (node.children && node.children.length) {

								this.formatJSON(node.children, arr);
							}
						}

						return arr;
					}
				});

			new TreeView().render();

			//
			var AddressCollection = Backbone.Collection.extend({
				url: page_config.officeDetailedAddress.url,
				parse: function(response) {
					return response.items;
				}
			});

			// 办公地址model

			var AddressModel = Backbone.Model.extend({
				url: page_config.officeDetailedAddress.url,
				defaults: {
					id: "",
					office_name: "",
					parentid: "",
					sort: "",
					house_number: "",
					street: "",
					city: "",
					zip_code: "",
					provinces_name: "",
					provinces: "",
					nation_name: "",
					nation: "",
					contact: "",
					phone: ""

				},
			});
			// 修改办公地址view
            var postnobt=true;
			var AddressView = Backbone.View.extend({
				el: "#query",
				template: templates.query,
				collection: new AddressCollection(),

				events: {
					"click #button1": "addressClick"
				},
				render: function(node) {
					$.blockUI({
						message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/>'
					});
					var v = this;

					v.collection.url = page_config.officeDetailedAddress.url + "?id=" + node;

					// this.nation.url= page_config.selectNation.url;
					// console.log(this.nation.url);

					this.collection.fetch({

						success: function(collection) {
							//console.log(collection.toJSON());

							v.$el.html(v.template({
								Address: collection.toJSON(),

							}));
							$.unblockUI();
						}

					});

				},
				addressClick: function(evt) {
                    
                if(postnobt){

                	postnobt=false;
					var v = this;
					var id = $(evt.target).parent().data("id");

					if (!id)
						return;

					var collection = this.collection;

					var model = new AddressModel();

					var url = model.url;

					var model = this.collection.findWhere({
						id: id
					});

					model.url = url;

					var office_name = model.toJSON().office_name;

					var updateAddress = new UpdateAddressView(model, {
						el: "#address"
					});

					updateAddress.render();
					new NationView().render(model.toJSON().nation);
					new ProvinceView().render({
						cid: model.toJSON().nation
					}, model.toJSON().provinces);
					addressDialog(office_name, function() {

						var provinces_name = updateAddress.$el.find("input[name='provinces_name']").val();
						var provinces = updateAddress.$el.find("#selectProvinces").val();
						var nation = updateAddress.$el.find("input[name='nation_name']").val();
						var nationid = updateAddress.$el.find("#selectNation").val();
						//console.log(provinces);
						if (provinces_name == '' && provinces == '0') {
							showMessage("Enter the full Province.", "alert");
							return false
						};
						if (nation == '') {
							showMessage("Enter the full nation.", "alert");
							return false
						};


						model.save({
							house_number: updateAddress.$el.find(
								"input[name='house_number']").val().replace(/(^\s*)|(\s*$)/g, ""),
							street: updateAddress.$el.find(
								"input[name='street']").val().replace(/(^\s*)|(\s*$)/g, ""),
							city: updateAddress.$el.find("input[name='city']")
								.val().replace(/(^\s*)|(\s*$)/g, ""),
							zip_code: updateAddress.$el.find(
								"input[name='zip_code']").val().replace(/(^\s*)|(\s*$)/g, ""),
							provinces_name: updateAddress.$el.find(
								"input[name='provinces_name']").val().replace(/(^\s*)|(\s*$)/g, ""),
							provinces: updateAddress.$el.find("#selectProvinces").val().replace(/(^\s*)|(\s*$)/g, ""),
							nation_name: updateAddress.$el.find("input[name='nation_name']").val().replace(/(^\s*)|(\s*$)/g, ""),
							nation: updateAddress.$el.find("#selectNation").val().replace(/(^\s*)|(\s*$)/g, ""),
							contact: updateAddress.$el.find(
								"input[name='contact']").val().replace(/(^\s*)|(\s*$)/g, ""),
							phone: updateAddress.$el.find(
								"input[name='phone']").val().replace(/(^\s*)|(\s*$)/g, "")
						}, {
							success: function() {
								//parent.location.reload();

								//dialog.dialog( "close" );
								$("#address").dialog("close");
								new AddressView().render(id);



							}
						});
					})

                   }
				}

			});

			var UpdateAddressView = Backbone.View.extend({
				el: "#address",
				template: templates.update_address,
				initialize: function(model) {
					this.model = model;

				},
				render: function(submitBtn) {
					$.blockUI({message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/>'});
					this.$el.html(this.template({
						model: this.model.toJSON()
					}));

				},


			});

			//国家下拉
			var NationCollection = Backbone.Collection.extend({
				url: page_config.selectNation.url,
				parse: function(response) {
					return response.items;
				}
			});

			//省下拉
			var ProvinceCollection = Backbone.Collection.extend({
				url: page_config.selectProvince.url,
				parse: function(response) {
					return response.items;
				}
			});

			var NationView = Backbone.View.extend({
				el: "#selectNation",
				template: templates.selectNation,
				collection: new NationCollection(),
				events: {
					// "click #button1":"adminClick"
				},
				render: function(cid) {

					var v = this;

					v.provinceView = new ProvinceView;

					this.collection.fetch({

						success: function(collection) {
							var nid = $("#selectNation").val();
							var nation = collection.toJSON();


							for (var i = 0, len = nation.length; i < len; i++) {
								if (cid == nation[i].cid) {
									nation[i].selected = true;
									//console.log("1"+nation[i].c_country);
								//	console.log(nation.length);
									var nation_name1  =	$("input[name='nation_name']").val();
									var nation_name2 = nation[i].c_country ;
									//console.log("2"+	nation_name1);
									if (nation_name1==nation_name2) {
									//	console.log("aa");
										$("input[name='nation_name']").val(nation[i].c_country);

									};

								
								}
							}
						//	console.log("1"+nation_name1);
						//	console.log("2"+nation_name2);
							if (i == nation.length&&nation_name1==undefined) {
							
								$("input[name='nation_name']").val(nation[0].c_country);
							};


							v.$el.html(v.template({

								Nation: nation
							}));


							$("#selectNation").select2().on("select2-selecting", function(e) {

								$("input[name='nation_name']").val(e.choice.text);

								//DOTO在这里bind点击联动。。。。
								//console.log ("selecting val="+ e.val +" choice="+ JSON.stringify(e.choice));
								v.provinceView.render({
									cid: e.val
								}); //传递查询数据的参数

								var provinces_name = $("input[name='provinces_name']");

								provinces_name.val("");

							});

						}

					});

				},

			});



			//省下拉
			var ProvinceView = Backbone.View.extend({
				el: "#selectProvinces",
				template: templates.selectProvinces,
				collection: new ProvinceCollection(),
				events: {
					// "click #button1":"adminClick"
				},
				render: function(data, provincesId) {

					var v = this;

					this.collection.fetch({


						success: function(collection) {


							var provinces = collection.toJSON();
							var isInput = false;
							var provinces_name = $("input[name='provinces_name']").val();
						
							if (provinces.length > 0 &&provincesId!==0) {
						
								$("input[name='provinces_name']").val(provinces[0].pro_name);
							};
							if (provincesId == "0") {
								isInput = true;
							} else {
								for (var i = 0, len = provinces.length; i < len; i++) {
									if (provincesId == provinces[i].pro_id) {
										provinces[i].selected = true;
									}
								}

							}
							v.$el.html(v.template({
								Province: provinces,
								isInput: isInput
							}));

							//判断回调数据
							if (provinces && provinces.length) { //返回有数据
								$('#selectProvinces').select2().on("select2-selecting", function(e) {});

								$('#selectProvinces').select2().on("select2-selecting", function(e) {

									if (e.val == 0) {
										$(this).next().show().val(provinces_name);
									
										 var provinces_name = $("input[name='provinces_name']");

										 provinces_name.val("");
									} else {
										$(this).next().hide();

										$("input[name='provinces_name']").val(e.choice.text);



									}

									//console.log(JSON.stringify(e.choice));
								}).next().hide();

								//初始化状态
								if (isInput) {
									//console.log($('#selectProvinces').next());
									$('#selectProvinces').next().show();
								}

							} else { //返回没有数据

								$('#selectProvinces').select2().next().show();
							}


						},

						data: data

					});

				},

			});

		});
	}; // page_init

	if (typeof define === 'function' && define.amd) {
		define(["tree_config", "jquery", "backbone", "handlebars", "templates", "jqueryui/dialog", "jstree", "select2", "require_css!Font-Awesome", "artDialog", "blockui"], page_init);
	} else {
		page_init(page_config, $, Backbone, Handlebars, Handlebars.templates);
	}



}).call(this);