﻿/**
    *create by cuixiang 20141202
*/
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "bootstrap",
    "nprogress",

    "js/view/v3AddRelWarehouse",
    "js/view/v3AddRelOrder",
    "js/view/v3AddCNTR"
    

], function ($, Backbone, handlebars, bootstrap, NProgress, v3AddRelWarehouse, v3AddRelOrder, v3AddCNTR) {
    NProgress.start();

    var v3AddRelWarehouseView = new v3AddRelWarehouse();
    v3AddRelWarehouseView.render();

    var v3AddRelOrderView = new v3AddRelOrder();
    v3AddRelOrderView.render();

    var v3AddCNTRView = new v3AddCNTR();
    v3AddCNTRView.render();

    NProgress.done();
}
);