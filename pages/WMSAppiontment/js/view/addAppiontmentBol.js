﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "createBoxPanel", "art_Dialog/dialog-plus"], function ($, Backbone, handlebars, templates, CompondBox, createBoxPanel, Dialog) {
    return Backbone.View.extend({
        el: "#addBol",
        template: templates.addBol,
        render: function () {
            var v = this;
            var temp = new CompondBox({
                content: v.template().trim(),
                title: "BOL",
                container: v.el,
                isExpand: true
            });


            $("#btnAddBol").on("click", function () { v.addBol(); });


           

        },
        addBol: function () {
            var v = this;

            var d = Dialog({
                title: '关联',
                width: 700,
                height: 400,
                lock: true,
                opacity: 0.6,
                content: '请选择order po load',
                ok: function () {
                    var that = this;
                    

                    
                    //d.close();

                    var createPanelView = new createBoxPanel({
                        content: templates.singleBoxBol(),
                        width: '',
                        container: '#boxBol',
                        close: function () { return v.delBol(); }
                    });

                    d.close();

                    return false;
                },
                cancel: function () {
                    d.close();
                    return false;
                }
            });
            d.showModal();


            
        },
        delBol: function () {
            if (confirm("确定删除关联BOL吗?")) {
                return true;
            }
            return false;
        }
        
    });
});