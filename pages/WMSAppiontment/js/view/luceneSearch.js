﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates"], function ($, Backbone, handlebars, templates) {
    return Backbone.View.extend({
        el: "#searchAppiontmentTab",
        template: templates.luceneSearch,
        render: function () {
            var v = this;
            v.$el.html(v.template().trim());
        }
    });
});