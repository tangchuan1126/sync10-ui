﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "createBoxPanel", "art_Dialog/dialog-plus", "oso.lib/table/js/table"], function ($, Backbone, handlebars, templates, CompondBox, createBoxPanel, Dialog,Table) {
    return Backbone.View.extend({
        el: "#containerAddRelOder",
        template: templates.v3AddRelOrder,
        render: function () {
            var v = this;
            var quickSearch = "关联单据" + templates.v3QuickSearchOrder().trim();

            var temp = new CompondBox({
                content: v.template().trim(),
                title: quickSearch,
                container: v.el,
                isExpand: true
            });

            v.init();
            $(".quick-search").on("click", function (e) { e.stopPropagation(); return false; })
            $("#btnAddOrder").on("click", function () { v.addOrder(); });
        },
        init: function () {
            var v = this;

            var orderid = v.GetRequest("orderid");
            var ordertype = v.GetRequest("ordertype");
            if (orderid && ordertype) {
                var createPanelView = new createBoxPanel({
                    content: templates.singleBoxBol(),
                    width: '',
                    container: '#ContainerAddBol',
                    close: function () { return v.delOrder(); }
                });
            }

        },
        addOrder: function () {
            var v = this;
            var h = $(templates.v3SelectOrder().trim());

            var d = Dialog({
                title: "关联单据",
                width: 700,
                height: 400,
                lock: true,
                opacity: 0.6,
                content: h,
                cancelValue: '关闭',
                cancel: function () {
                }
            });
            d.reset();
            d.showModal();

            $('#tabsSelectOrder a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('#tabsSelectOrder a:first').tab('show');

            v.loadList();
        },
        delOrder: function () {
            if (confirm("确定删除关联BOL吗?")) {
                return true;
            }
            return false;
        },
        loadList: function () {
            var v = this;
            $("#tabSelectLoadList").append(templates.v3SelectOrderToolbars().trim());


            var tableSelectLoad = new Table({
                container: '#tabSelectLoadList',
                url: '../WMSAppiontment/json/a.json',
                rownumbers: true,
                pagination: false,
                checkbox:true,
                columns: [
                    { field: 'code', title: 'Code', width: 100 },
                    {
                        field: 'first', title: 'First Name', width: 100, formatter: function (data, row) {
                            return '<a href="#">' + data + '</a>';
                        }
                    },
                    { field: 'last', title: 'Last Name', align: 'center', width: 100 },
                    { field: 'username', title: 'Username', width: 100 }
                ],
                toolbar: [
                    {
                        text: '关联',
                        iconCls: 'add',
                        handler: function () {

                            var createPanelView = new createBoxPanel({
                                content: templates.singleBoxBol(),
                                width: '',
                                container: '#ContainerAddBol',
                                close: function () { return v.delOrder(); }
                            });

                            //tableSelectLoad.addLocalData({
                            //    "code": "New001",
                            //    "first": "Otto",
                            //    "last": "Mark",
                            //    "username": "@mdo"
                            //}, function (index, row) {
                            //    tableSelectLoad.reloadCurrent(true);
                            //});
                        }
                    }
                ]
            });

            tableSelectLoad.on("events.clickRow", function (index, row) {
                console.log(index); console.log(row);
            });
        },
        GetRequest: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = window.location.search.substr(1).match(reg);  //匹配目标参数
            if (r != null) return unescape(r[2]);
            return null; //返回参数值
        }
    });
});