﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "immybox", "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"], function ($, Backbone, handlebars, templates, immybox, AsynLoadQueryTree) {
    return Backbone.View.extend({
        el: "#searchAdvancedTab",
        template: templates.advancedSearch,
        render: function(){
            var v = this;
            v.$el.html(v.template().trim());

            var fromTree = new AsynLoadQueryTree({
                renderTo: "#searchFrom",
                selectid: { value: v.shipFrom },
                PostData: { rootType: "Ship From" },
                dataUrl: "./json/shipTo.json",
                scrollH: 400,
                multiselect: false,
                Async: true,
                placeholder: "Ship From"
            });
            fromTree.render();


            $.getJSON("json/carrier.json", function (result) {
                $('#inputCarrier').immybox({
                    choices: result
                });
            });

            $.getJSON("json/orderType.json", function (result) {
                $('#selectOrderType').immybox({
                    choices: result
                });
            });
        }
    });
});