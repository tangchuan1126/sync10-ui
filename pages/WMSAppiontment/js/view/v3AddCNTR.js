﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox", "immybox", "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"
 ], function ($, Backbone, handlebars, templates, CompondBox, immybox, AsynLoadQueryTree) {
    return Backbone.View.extend({
        el: "#containerAddCar",
        template: templates.v3AddCNTR,
        render: function () {
            var v = this;
            var temp = new CompondBox({
                content: v.template().trim(),
                title: "预约车辆",
                container: v.el,
                isExpand: true
            });



            //$.getJSON("json/carrier.json", function (result) {
            //    $('#inputAddCarrier').immybox({
            //        choices: result
            //    });
            //});
            v.init();

        },
        init: function () {
            var fromTree = new AsynLoadQueryTree({
                renderTo: "#inputAddCarrier",
                dataUrl: "./json/carrier.json",
                scrollH: 400,//多高后出现滚动条
                Async: false,//非异步获取树节点，（一次加载完成）
                multiselect: false,//多选
                PostData: { "a": "1", "b": "2" },//异步时请求每个节点时往服务端传值,
                placeholder: "请输入预约仓库",//默认input框值
                Parentclick: true//开启父节点可以选中
            });

            fromTree.render();
            console.log("render");

            fromTree.on("Initialize", function (treeId, treeNode, treeNodeAll) {
                console.log(treeNodeAll);
                $("#inputAddCarrierLinkman").val(treeNodeAll.linkman);
                $("#inputAddCarrierTel").val(treeNodeAll.tel);
            });

            fromTree.on("events.Itemclick", function (treeId, treeNode, treeNodeAll) {
                console.log(treeNodeAll);
                $("#inputAddCarrierLinkman").val(treeNodeAll.linkman);
                $("#inputAddCarrierTel").val(treeNodeAll.tel);                
            });
        }
    });
});