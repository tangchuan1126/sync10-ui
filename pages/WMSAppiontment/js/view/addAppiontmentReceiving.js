﻿"use strict";
define(["jquery", "backbone", "handlebars", "templates", "compondBox"], function ($, Backbone, handlebars, templates, CompondBox) {
    return Backbone.View.extend({
        el: "#ReceivingAddress",
        template: templates.addReceiving,
        render: function () {
            var v = this;
            var temp = new CompondBox({
                content: v.template().trim(),
                title: "收货地址",
                container: v.el,
                isExpand: true
            });
        }
    });
});