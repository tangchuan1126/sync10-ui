﻿define(["../config/jsontohtml_templates"], function (tmp) {

    return {
        "View_control": {
            "head": [{
                "title": "Appiontment",
                "field": "Appiontment"
            }, {
                "title": "Truck",
                "field": "Truck"
            }, {
                "title": "Invoice",
                "field": "Invoice"
            }, {
                "title": "Log",
                "field": "Log"
            }],
            "footer": [
				[
					"events.UpdateAppiontmentDate", "取消约车"
				], [
					"events.Edit", "Edit"
				]
            ],
            "templates": tmp
        }
    }

});