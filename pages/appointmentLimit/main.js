"use strict";
require(["../../requirejs_config","./config","./addIndex"] ,function(con,config) {
	require(["jquery","./js/view/appointmentDefaultLimit","bootstrap"],
		function($,AppointmentDefaultLimit) {
			function getQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				var r = window.location.search.substr(1).match(reg);
				if (r != null) return unescape(r[2]); return null;
			}
			var pid = getQueryString("storage_id");
			var dates = getQueryString("alltddata");
			//var appointmentLimit = new AppointmentLimit({defaultStore:pid,WeekDate:dates});
			new AppointmentDefaultLimit({defaultStore:pid,WeekDate:dates}).render();
			//new DayLimit({defaultStore:pid,WeekDate:dates}).render();
		});
});