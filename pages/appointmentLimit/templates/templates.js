(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['appointmentLimit'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "				<th data-date=\""
    + escapeExpression(lambda(depth0, depth0))
    + "\" class=\"appointmentLimitTime\" align=\"center\">\r\n<!-- 		   			<span class=\"appointmentLimitLabel \"><b>date</b></span> -->\r\n					<span><b>"
    + escapeExpression(lambda(depth0, depth0))
    + "</b></span>\r\n	  			</th>\r\n";
},"3":function(depth0,helpers,partials,data,depths) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "		  <tr id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.hour : depth0), depth0))
    + "\" class=\"appointmentLimitTr\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.value : depth0), {"name":"each","hash":{},"fn":this.program(4, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		  </tr>\r\n";
},"4":function(depth0,helpers,partials,data,depths) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depths[1] != null ? depths[1].hour : depths[1]), "sum", {"name":"sif","hash":{},"fn":this.program(5, data, depths),"inverse":this.program(7, data, depths),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers.unless.call(depth0, (data && data.index), {"name":"unless","hash":{},"fn":this.program(9, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depths[1] != null ? depths[1].hour : depths[1]), "date", {"name":"sif","hash":{},"fn":this.program(11, data, depths),"inverse":this.program(13, data, depths),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		  			</td>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			 			<th data-date=\""
    + escapeExpression(lambda((depth0 != null ? depth0.work_date : depth0), depth0))
    + "\" class=\"appointmentLimitTd\" align=\"center\">\r\n";
},"7":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "						<td data-date=\""
    + escapeExpression(lambda((depth0 != null ? depth0.work_date : depth0), depth0))
    + "\" class=\"appointmentLimitTd\" align=\"center\">\r\n";
},"9":function(depth0,helpers,partials,data,depths) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			   		<span class=\"appointmentLimitLabel \"><b>"
    + escapeExpression(lambda((depths[2] != null ? depths[2].hour : depths[2]), depth0))
    + "</b></span>\r\n";
},"11":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "						<span class=\" \"><b>"
    + escapeExpression(lambda((depth0 != null ? depth0.work_date : depth0), depth0))
    + "</b></span>\r\n";
},"13":function(depth0,helpers,partials,data,depths) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<input type=\"text\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.index : depth0), depth0))
    + escapeExpression(lambda((depths[2] != null ? depths[2].hour : depths[2]), depth0))
    + "sumin\" data-sumtype=\"sumin\" index=\""
    + escapeExpression(lambda((depth0 != null ? depth0.index : depth0), depth0))
    + "\" data-hour=\""
    + escapeExpression(lambda((depths[2] != null ? depths[2].hour : depths[2]), depth0))
    + "\" data-date=\""
    + escapeExpression(lambda((depth0 != null ? depth0.work_date : depth0), depth0))
    + "\" class=\"appointmentLimitInput inBound\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.sumin : depth0), depth0))
    + "\" />\r\n<input type=\"text\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.index : depth0), depth0))
    + escapeExpression(lambda((depths[2] != null ? depths[2].hour : depths[2]), depth0))
    + "sumout\" data-sumtype=\"sumout\" index=\""
    + escapeExpression(lambda((depth0 != null ? depth0.index : depth0), depth0))
    + "\" data-hour=\""
    + escapeExpression(lambda((depths[2] != null ? depths[2].hour : depths[2]), depth0))
    + "\" data-date=\""
    + escapeExpression(lambda((depth0 != null ? depth0.work_date : depth0), depth0))
    + "\" class=\"appointmentLimitInput outBound\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.sumout : depth0), depth0))
    + "\" />\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,depths) {
  var stack1, buffer = "<div class=\"panel-body appointmentLimit\">\r\n	<table id=\"appointmentLimitTable\" class=\"appointmentLimitTable\"> \r\n		<tr id=\"date\" class=\"appointmentLimitTr even\">\r\n\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.dateArr : depth0), {"name":"each","hash":{},"fn":this.program(1, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "		</tr>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.data : depth0), {"name":"each","hash":{},"fn":this.program(3, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n\r\n\r\n	</table>\r\n</div>    \r\n<div class=\"footer\">\r\n    <div class=\"opbar\">\r\n        <button type=\"button\" id=\"saveLimit\" class=\"btn btn-info pull-right\">Save</button>\r\n    </div>\r\n<div>";
},"useData":true,"useDepths":true});
templates['default'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
templates['defaultLimit'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"panel panel-default\">\r\n	<div class=\"panel-heading\">\r\n		<div class=\"row\">\r\n			<div class=\"form-group col-sm-4\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\">Type</div>\r\n					<input id=\"limitType\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select Type\">\r\n				</div>\r\n			</div>\r\n			<div class=\"form-group col-sm-4\" style=\"display:none\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\">AppointmentType</div>\r\n					<input id=\"boundType\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select Type\" data-value=\"Inbound\">\r\n				</div>\r\n			</div>\r\n			<div class=\"form-group col-sm-4\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\">HUB</div>\r\n					<input id=\"company\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select HUB\">\r\n				</div>\r\n			</div>\r\n			<div class=\"form-group col-sm-4\" style=\"display:none\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\">Start Date</div>\r\n					<input id=\"startdate\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select Date\">\r\n				</div>\r\n			</div>\r\n			<div class=\"form-group col-sm-4\" style=\"display:none\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\">End Date</div>\r\n					<input id=\"enddate\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select Date\">\r\n				</div>\r\n			</div>\r\n			<div class=\"form-group col-sm-4\" style=\"display:none\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\">Date</div>\r\n					<input id=\"byday\" type=\"text\" class=\"form-control min-200 immybox immybox_witharrow\" placeholder=\"Select Date\">\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\"panel-body\">\r\n		<div class=\"row\">\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<div class=\"input-group-addon\"><button id=\"setAll\" class=\"\">Set ALL </button></div>\r\n					<input class=\"form-control\" min=\"0\" type=\"number\" id=\"allvalue\"/>\r\n				</div>\r\n			</div>\r\n			<div class=\"btn-group col-sm-4\">\r\n				<button type=\"button\" id=\"selectALL\" class=\"btn btn-default\">Select ALL</button>\r\n				<button type=\"button\" id=\"selectInvert\" class=\"btn btn-default\">Select Invert</button>\r\n			</div>\r\n		<!-- 	<div class=\"form-group col-sm-2\">\r\n				<div class=\"input-group\">\r\n					<label for=\"selectInvert\" class=\"input-group-addon\"> Select Invert  </label>\r\n					<div class=\"form-control\"><input id=\"selectInvert\"  type=\"checkbox\" checked=\"checked\" /></div>\r\n				</div>\r\n			</div> -->\r\n		</div>\r\n		<div class=\"row\">\r\n\r\n		\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 00:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"0\" name=\"0\" id=\"time_0\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_0 || (depth0 != null ? depth0.time_0 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_0","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 01:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"1\" name=\"1\" id=\"time_1\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_1 || (depth0 != null ? depth0.time_1 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_1","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 02:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"2\" name=\"2\" id=\"time_2\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_2 || (depth0 != null ? depth0.time_2 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_2","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 03:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"3\" name=\"3\" id=\"time_3\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_3 || (depth0 != null ? depth0.time_3 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_3","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 04:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"4\" name=\"4\" id=\"time_4\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_4 || (depth0 != null ? depth0.time_4 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_4","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 05:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"5\" name=\"5\" id=\"time_5\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_5 || (depth0 != null ? depth0.time_5 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_5","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 06:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"6\" name=\"6\" id=\"time_6\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_6 || (depth0 != null ? depth0.time_6 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_6","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 07:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"7\" name=\"7\" id=\"time_7\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_7 || (depth0 != null ? depth0.time_7 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_7","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 08:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"8\" name=\"8\" id=\"time_8\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_8 || (depth0 != null ? depth0.time_8 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_8","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 09:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"9\" name=\"9\" id=\"time_9\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_9 || (depth0 != null ? depth0.time_9 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_9","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 10:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"10\" name=\"10\" id=\"time_10\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_10 || (depth0 != null ? depth0.time_10 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_10","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 11:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"11\" name=\"11\" id=\"time_11\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_11 || (depth0 != null ? depth0.time_11 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_11","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 12:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"12\" name=\"12\" id=\"time_12\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_12 || (depth0 != null ? depth0.time_12 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_12","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 13:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"13\" name=\"13\" id=\"time_13\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_13 || (depth0 != null ? depth0.time_13 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_13","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 14:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"14\" name=\"14\" id=\"time_14\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_14 || (depth0 != null ? depth0.time_14 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_14","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 15:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"15\" name=\"15\" id=\"time_15\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_15 || (depth0 != null ? depth0.time_15 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_15","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 16:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"16\" name=\"16\" id=\"time_16\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_16 || (depth0 != null ? depth0.time_16 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_16","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 17:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"17\" name=\"17\" id=\"time_17\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_17 || (depth0 != null ? depth0.time_17 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_17","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 18:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"18\" name=\"18\" id=\"time_18\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_18 || (depth0 != null ? depth0.time_18 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_18","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 19:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"19\" name=\"19\" id=\"time_19\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_19 || (depth0 != null ? depth0.time_19 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_19","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 20:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"20\" name=\"20\" id=\"time_20\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_20 || (depth0 != null ? depth0.time_20 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_20","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 21:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"21\" name=\"21\" id=\"time_21\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_21 || (depth0 != null ? depth0.time_21 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_21","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 22:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"22\" name=\"22\" id=\"time_22\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_22 || (depth0 != null ? depth0.time_22 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_22","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"form-group col-sm-3\">\r\n				<div class=\"input-group\">\r\n					<label class=\"input-group-addon\"><input class=\"setvalue\"  type=\"checkbox\" checked=\"checked\" /> 23:00</label>\r\n					<input class=\"form-control min-50 value\" type=\"number\" data-hour=\"23\" name=\"23\" id=\"time_23\" value=\""
    + escapeExpression(((helper = (helper = helpers.time_23 || (depth0 != null ? depth0.time_23 : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"time_23","hash":{},"data":data}) : helper)))
    + "\" />\r\n				</div>\r\n			</div>\r\n\r\n\r\n\r\n\r\n\r\n		</div>\r\n	</div>\r\n	<div class=\"panel-footer\" style=\"height:60px\"><button type=\"button\" id=\"saveDefaultLimit\" class=\"btn btn-info pull-right\">Save</button></div>\r\n</div>";
},"useData":true});
})();