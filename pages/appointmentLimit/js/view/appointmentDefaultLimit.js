"use strict";
define([
"jquery","backbone","../../config","../../templates",
"handlebars_ext","nprogress","art_Dialog/dialog-plus","immybox",
"bootstrap.datetimepicker",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"require_css!oso.lib/immybox-master/immybox.css",
"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,templates,Handlebars,NProgress,Dialog,immybox,datetimepicker,AsynLoadQueryTree) {
	return Backbone.View.extend({
		el:"#appointmentLimitDefaultForm",
		template: templates.defaultLimit,
		defaultStore : "1000005",
		defaultBound : "Inbound",
		defaultLimitType : "default",
		initialize:function(options) {
			var v = this;
			v.defaultStore = options.defaultStore || v.defaultStore;
		},
		events: {
			"click #saveDefaultLimit" : "saveDefaultLimit" ,
			"click #setAll" : "setAll" ,
			"click #selectInvert" : "selectInvert" ,
			"click #selectALL" : "selectALL" 
		},
		render: function(){
			var v = this;
			
			v.getData({storage_id:v.defaultStore , bound_type:v.defaultBound });

			$.getJSON(config.limitType.json, function (json) {
			//	console.log(json);
				$('#limitType').immybox({
				//	Pleaseselect:'Clean Select',
					Defaultselect:"default",
					choices: json,
					change : function (e,d){

						v.defaultLimitType = d.value;

						var start_end_date = $("#startdate,#enddate").parent().parent();
						var byday = $("#byday").parent().parent();
						if(d.value=='other'){
							start_end_date.show();
						//	enddate.show();
							byday.hide();
						}else if(d.value=='byday'){
							start_end_date.hide();
							//enddate.hide();
							byday.show();
						}else if(d.value=='default'){
							v.render();
							start_end_date.hide();
							byday.hide();
						}
					}
				});
				
			});
			$.getJSON(config.boundType.json, function (json) {
			//	console.log(json);
				$('#boundType').immybox({
				//	Pleaseselect:'Clean Select',
					Defaultselect:v.defaultBound,
					choices: json,
					change : function (e,d){
						
						/*
							bound改变时,也改变默认的defaultBound
						*/
						v.defaultBound = d.value;
						

						var param = {storage_id: v.defaultStore , bound_type: v.defaultBound };
					//	console.log(param);
						v.render();

					}

				});
			});
			/*
			var company = new AsynLoadQueryTree({
				renderTo: "#company",
				PostData: {rootType:"Ship From"},
				dataUrl: config.shipFrom.url,
				scrollH: 400,
				multiselect: false,
				selectid:{value : v.defaultStore},	//默认选中的
				Async: true,
			//	Pleaseselect: "Clean Select",
				placeholder:"company"
			});
			company.on("Pleaseselect",function(jqinput){
				jqinput.val("");
			});
			company.on("events.Itemclick",function(treeId,treeNode){                   
			  //    console.log(treeNode);
			     
			     // *
			     	store,也改变默认的defaultStore
			     * //
			      v.defaultStore =  treeNode.data;

			      v.render();
			});
			company.render();
			*/
			$.getJSON(config.shipFrom.url+"&type=1&lv=0&rootType=Hub&Reqnumr="+Date.parse(new Date()), function (json) {
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].data,"text":json[i].name});
				}
				if (v.defaultStore) {
					$('#company').immybox({
						Pleaseselect:'Clean Select',
						choices: rls,
						Defaultselect: ""+v.defaultStore,
						change:function(e,d) {
							v.defaultStore = d.value;
							//console.log(d.value);
							//v.defaultStore =  treeNode.data;
							v.render();
						}
					});
				} else {
					$('#company').immybox({
						Pleaseselect:'Clean Select',
						choices: rls,
						change:function(e,d) {
							v.defaultStore = d.value;
							//console.log(d.value);
							//v.defaultStore =  treeNode.data;
							v.render();
						}
					});
				}
			});


			var datatimeOptions = {
				format:'mm/dd/yyyy',
				weekStart: 1,
	            todayBtn: 1,
	            autoclose: 1,
	            todayHighlight: 1,
	            startView: 2,
	            minView: 2,
	            forceParse: 0
	        }
			$("#startdate").datetimepicker(datatimeOptions);
			$("#enddate").datetimepicker(datatimeOptions);
    		$("#byday").datetimepicker(datatimeOptions);


		},
		getData: function(param) {
			var v = this;

			$.ajax({
			    type: 'get',
			    // url: "/Sync10/_load/setDefaultValue",
				url: config.getDefaultValue.url,
				data: param, dataType: 'json', async:false,
				success: function (data) {
					var data = data.DATA;
					var tempdata = {};
					$.each(data,function(i,e){
						eval("tempdata.time_"+e.ID +"="+ e.VALUE);
					});
				//	console.log(tempdata);
					v.$el.html(v.template(tempdata));
			    },
			    error: function (data) {
			    	v.$el.html(v.template({}));
			    }
			});

				
		},

		saveDefaultLimit:function( evt )
		{
			var v = this;


				var daaaaa = [];
				v.$el.find(":input.value").each(function(i,e){
					var hour = $(this).data("hour");
					var value = $(this).val();

					if(value != ""){
						var obj = {
							value: value ? value : 0,	hour : hour
						}
						daaaaa.push(obj);
					}
					
					
				});

				var defaultValueJsonString = JSON.stringify(daaaaa);
			//	console.log(defaultValueJsonString);

				var limitType = v.$el.find("#limitType").attr("data-value");
			//	console.log(limitType);

				var boundType = v.$el.find("#boundType").attr("data-value");
			//	console.log(boundType);

				var storage_id = v.$el.find("#company").attr("data-value");
			//	console.log(storage_id);

				var startdate = v.$el.find("#startdate").val();
			//	console.log(startdate);

				var enddate = v.$el.find("#enddate").val();
			//	console.log(enddate);

				var byday = v.$el.find("#byday").val();
			//	console.log(byday);


			if( limitType == 'default' )
			{
				v.setDefaultValue({data : defaultValueJsonString , bound_type: boundType , storage_id : storage_id || v.defaultStore});
			}

			else if( limitType == 'other' )
			{

				if( !v.vaildOther() ) return;

				var param = {
								data : defaultValueJsonString , 
								bound_type: boundType , 
								storage_id : storage_id || v.defaultStore ,
								start: startdate,
								end: enddate
							}
				v.setDefaultValueByDate(param);
			}

			else if(limitType == 'byday')
			{

				if( !v.vaildbyday() ) return;

				var param = {
								data : defaultValueJsonString , 
								bound_type: boundType , 
								storage_id : storage_id || v.defaultStore ,
								start: byday,
								end: byday
							}
				v.setDefaultValueByDate(param);
			}
		},
		/*
			保存默认值
		*/
		setDefaultValue:function( param )
		{
			var v = this;

			$.ajax({
			    type: 'post',
			    // url: "/Sync10/_load/setDefaultValue",
				url: config.setDefaultValue.url,
				data: param ,
				dataType: 'json',
				async:false,
				success: function (data) {
					console.log(data);
			    },
			    error: function (data) {
			    	console.log(data);
			    }
			});

		},
		setDefaultValueByDate:function( param )
		{
			var v = this;

			$.ajax({
				type: 'post',
			    // url: "/Sync10/_load/setDefaultValue",
				url: config.setDefaultValueByDate.url,
				data: param ,
				dataType: 'json',
				async:false,
				success: function (data) {
					console.log(data);
			    },
			    error: function (data) {
			    	console.log(data);
			    }
			});
		},
		/*
			统一设置全部吞吐量
		*/
		setAll:function(evt)
		{
			var allvalue =  $("#allvalue").val();
			$("input:checkbox[class='setvalue']:checked").parent().next("input").val(allvalue);
		},
		/*
			全选/全取消
		*/
		selectALL:function(evt)
		{
			$("input:checkbox[class='setvalue']").prop("checked","true");
		},
		/*
			反选
		*/
		selectInvert:function(evt)
		{
			$("input:checkbox[class='setvalue']").each(function () {
				$(this).prop("checked", !this.checked);
			})
		},
		vaildOther : function()
		{
			var v = this;

			var f = true;

			var $startdate = v.$el.find("#startdate");
			var startdate = $startdate.val();
			if( startdate ){
				$startdate.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end();
			}else{
				 if (!$startdate.parent().hasClass("has-feedback")) {
			        $startdate.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
			    }
			    f = false;
			}


			var $enddate = v.$el.find("#enddate");
			var enddate = $enddate.val();
			if( enddate ){
				$enddate.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
			}else{
				if (!$enddate.parent().hasClass("has-feedback")) {
				    $enddate.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
				}
				f = false;
			}
			return f;

		},
		vaildbyday : function()
		{
			var v = this;

			var $byday = v.$el.find("#byday");
			var byday = $byday.val();
			if( byday ){
				$byday.parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end();
				return true;
			}else{
				 if (!$byday.parent().hasClass("has-feedback")) {
			        $byday.parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
			    }
			    return false;
			}
		}
	});
});