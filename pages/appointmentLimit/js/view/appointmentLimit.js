define(["jquery","backbone","../../config","../../templates", "handlebars_ext","nprogress","art_Dialog/dialog-plus"],
function($,Backbone,config,templates,Handlebars,NProgress,Dialog) {
	return Backbone.View.extend(
	{
			el:"#appointmentLimitForm",
			template:templates.appointmentLimit,
			defaultStore : "1000005" ,
			
			initialize:function(options)
			{
				var v = this;
				v.defaultStore = options.defaultStore;
				v.WeekDate = (""+options.WeekDate).split(",");
				v.render();
			},
			events:
			{
				"click #saveLimit" : "saveLimit",
			},
			render:function(){
				var v = this;
				v.getBaseWorkByWeek(v.WeekDate);
			},
			getBaseWorkByWeek:function()
			{
				var v = this;

				//点击日历控件后,会给一周的日期
				var dateArr = v.WeekDate;
				
				$.ajax({
				    type: 'get',
				    // url: "/Sync10/_load/getBaseWorkByWeek",
				    url: config.getBaseWorkByWeekUrl.url,
				    data: {start : dateArr[0] , end : dateArr[dateArr.length-1] , storage_id : v.defaultStore},
				    dataType: 'json',
				    async:false,
				    success: function (_data) {
						//基础数据,作用:为模板提供数据模型
		    			var baseData = [{hour:"0",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"1",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"2",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"3",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"4",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"5",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"6",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"7",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"8",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"9",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"10",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"11",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"12",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"13",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"14",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"15",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"16",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"17",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"18",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"19",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"20",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"21",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"22",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" },]	}, {hour:"23",	value:[{ work_date:dateArr[0] , index:"A" , sumin:"0" , sumout:"0" },{ work_date:dateArr[1] , index:"B" , sumin:"0" , sumout:"0" },{ work_date:dateArr[2] , index:"C" , sumin:"0" , sumout:"0" },{ work_date:dateArr[3] , index:"D" , sumin:"0" , sumout:"0" },{ work_date:dateArr[4] , index:"E" , sumin:"0" , sumout:"0" },{ work_date:dateArr[5] , index:"F" , sumin:"0" , sumout:"0" },{ work_date:dateArr[6] , index:"G" , sumin:"0" , sumout:"0" } ]	}, ];

		    			var data = _data.DATA;
		    			//console.log(data);
		    			//将台后数据 覆盖到 baseData上.
					    for(var i=0 ;i<24 ;i++){
					    	var tempDataArr = [];
					    	for(var inx = 0;inx<data.length;inx++){
					    		var sdata = data[inx];
					    		if(sdata.hour == i){
					    			var subD = baseData[i];
					    			var subDValue = subD.value;
					    			for(var subin =0;subin < subDValue.length ; subin ++){
					    				var val = subDValue[subin];
										if(val.work_date === sdata.work_date){
											val.sumin = sdata.sumin;
											val.sumout = sdata.sumout;
											break;
										}
					    			}
					    		}
					    	}
					    }

						v.$el.html(v.template( {data:baseData, dateArr:dateArr} ));
						v.$el.find("tr:even").addClass("even");
				    },
				    error: function (data) {
				    	console.log(data);
				    }
				});
			},
			saveLimit:function(evt)
			{
				NProgress.start();
				var v = this;
				
				var daaaaa = [];
				//v.$el.find(":input[id!=saveLimit]").each(function(i,e){
				$("#appointmentLimitForm").find(":input[id!=saveLimit]").each(function(i,e){
					var value = $(this).val();
			 		var hour = $(this).data("hour");
			 		var date = $(this).data("date");
			 		var sumtype = $(this).data("sumtype");
			 		var obj = {
			 			value: value ? value : 0,	hour : hour,	date : date,	sumtype : sumtype
			 		}
			 		daaaaa.push(obj);
				});
				//提交数据
				var jsonString = JSON.stringify(daaaaa)
				
				$.ajax({
				    type: 'post',
				    // url: "/Sync10/_load/setBaseWork",
				    url: config.setBaseWorkUrl.url,
				    data: {data : jsonString , storage_id : v.defaultStore},
				    dataType: 'json',
				    async:false,
				    success: function (data) {
						/*
						var d = new Dialog({
							content: "submit is ok, it will close!",
							icon: 'ok',
							lock: true,
							width: 200,
							height: 40,
							title: 'alert',
							cancelVal: 'close',
							cancel: function () {
								
							}
						}).show();
						*/
						//
						window.parent.postMessage("", '*');
						//console.log(data);
				    },
				    error: function (data) {
				    	console.log(data);
				    }
				});
				NProgress.done();
			}
	});

}
);