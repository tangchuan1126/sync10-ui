define(["jquery",
	"backbone",
	"../../config",
	"../../templates",
	"handlebars_ext","nprogress",
	"oso.lib/fullcalendar/fullcalendar","art_Dialog/dialog-plus"],
function($,Backbone,config,templates,Handlebars,NProgress,fullCalendar,Dialog) {
	return Backbone.View.extend({
		el:"#appointmentLimitForm",
		defaultStore : "1000005" ,
		initialize:function(options){
			var v = this;
			v.defaultStore = options.defaultStore;
			v.WeekDate = (""+options.WeekDate).split(",");
			v.render();
		},
		events:{
			"click #saveLimit" : "saveLimit",
		},
		getBaseWorkByWeek: function(param) {
			var v = this;
			var cssfileall = [ 
				"require_css!Font-Awesome",
				"require_css!oso.lib/fullcalendar/fullcalendar",
				"require_css!oso.lib/fullcalendar/style.calendar"
			];
			requirejs(cssfileall, function(fullCalendar) {
				$(function() {
					var dateArr = v.WeekDate;
					//url: config.getBaseWorkByWeekUrl.url,
				    //data: {start : dateArr[0] , end : dateArr[dateArr.length-1] , storage_id : v.defaultStore},
					var brotab = $("#condition");
					//brotab.show();
					$('#weekLimitForm').fullCalendar({
						header: {
							left: 'prev,next today',
							center: '',
							right: ''
						},
						defaultDate: v.getCurrentDate(),
						editable: true,
						eventLimit: true,
						inboundshow:true,
						outboundshow:false,
						showTdDetailed:config.getBaseWorkByWeekUrl.url,
						TdDetailedval:function(){
							return {storage_id:v.defaultStore};
						},
						ringhtconfigclick:function() {
							
						},
						tdcellclick : function(todate){},
						events: {
							url: config.calendarSumAppointmentUrl.url,
							type: 'GET',
							data: {
							  storage_id : v.defaultStore 
							},
							error: function(e) {
							   console.log(e);
							}
						}
					});
					if (brotab.length > 0) {
						brotab.find("div[role='tabpanel']").addClass('tab-pane');
					}
				});
			});
		},
		render:function(){
			var v = this;
			v.getBaseWorkByWeek(v.WeekDate);
		},
		saveLimit:function(evt)
		{
			NProgress.start();
			
			NProgress.done();
		},
		getCurrentDate:function (){
			var myDate = new Date();
			//myDate.getYear();        //获取当前年份(2位)
			var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
			var month = myDate.getMonth();       //获取当前月份(0-11,0代表1月)
			var date = myDate.getDate();        //获取当前日(1-31)
			var hours =myDate.getHours();       //获取当前小时数(0-23)
			var minutes = myDate.getMinutes();     //获取当前分钟数(0-59)
			var seconds = myDate.getSeconds();     //获取当前秒数(0-59)

			month++;
			if(month<=9)month="0"+month;
			if(date<=9)date="0"+date;
			return year + "-" +month+ "-" +date;// +" "+hours+ ":" +minutes+ ":" +seconds
		}
	});
});