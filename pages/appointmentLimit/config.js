define(
{
    setBaseWorkUrl : {
        url: "/Sync10/_load/setBaseWork"
    },
    getBaseWorkByWeekUrl : {
        url: "/Sync10/_load/getBaseWorkByWeek"
    },
    limitType : {
     //   url: "/Sync10/_load/setBaseWork",
        json: "/Sync10-ui/pages/appointmentLimit/json/limitType.json"
    },
    boundType : {
     //   url: "/Sync10/_load/setBaseWork",
        json: "/Sync10-ui/pages/appointmentLimit/json/boundType.json"
    },
    shipFrom:
    {
        url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
    },
    getDefaultValue:
    {
    	url: "/Sync10/_load/getDefaultValue"
    },
    setDefaultValue:
    {
    	url: "/Sync10/_load/setDefaultValue"
    },
    setDefaultValueByDate:
    {
    	url: "/Sync10/_load/setDefaultValueByDate"
    },
	calendarSumAppointmentUrl:{
	   url: "/Sync10/_load/sumWorkByStartEnd"
	}
});