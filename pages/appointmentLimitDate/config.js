define(
{
    limitType : {
     //   url: "/Sync10/_load/setBaseWork",
        json: "/Sync10-ui/pages/appointmentLimitDate/json/limitType.json"
    },
    boundType : {
     //   url: "/Sync10/_load/setBaseWork",
        json: "/Sync10-ui/pages/appointmentLimitDate/json/boundType.json"
    },
    shipFrom:
    {
        url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
    },
    defaultValue:
    {
    	url: "/Sync10/_load/getDefaultValue"
    },
});