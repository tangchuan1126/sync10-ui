define(["jquery","backbone","../../config","../../templates", "immybox", "bootstrap.datetimepicker",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree","handlebars_ext",
	"require_css!oso.lib/immybox-master/immybox.css",
	"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,templates,immybox,datetimepicker,AsynLoadQueryTree,Handlebars) {
	return Backbone.View.extend(
	{
			el:"#appointmentLimitDateForm",
		//	template:templates.appointmentLimit,
			defaultStore :  "1000005" ,
			
			initialize:function(options)
			{
				var v = this;
				v.defaultStore = options.defaultStore || v.defaultStore;
				v.WeekDate = (""+options.WeekDate).split(",");
				v.render();
			},
			events:
			{
				"click #saveLimit" : "saveLimit",
				"click #setAll" : "setAll"
			},
			render:function(){
				var v = this;
				$.getJSON(config.limitType.json, function (json) {
				//	console.log(json);
					$('#limitType').immybox({
					//	Pleaseselect:'Clean Select',
						Defaultselect:"default",
						choices: json,
						change : function (e,d){
						//	alert("dddd");
						//	console.log(e);
						//	console.log(d);
							if(d.value=='other'){
								$(".otherShowHide").show();
								$(".bydayShowHide").hide();
							}else if(d.value=='byday'){
								$(".bydayShowHide").show();
								$(".otherShowHide").hide();
							}else{
								$(".bydayShowHide").hide();
								$(".otherShowHide").hide();
							}
						}
					});
					
				});
				$.getJSON(config.boundType.json, function (json) {
				//	console.log(json);
					$('#boundType').immybox({
					//	Pleaseselect:'Clean Select',
						Defaultselect:"Inbound",
						choices: json

					});
				});

				$("#startdate").datetimepicker({
					format:'yyyy-mm-dd',
					weekStart: 1,
		            todayBtn: 1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0
		        });

				$("#enddate").datetimepicker({
					format:'yyyy-mm-dd',
					weekStart: 1,
		            todayBtn: 1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0
		        });
        		$("#byday").datetimepicker({
        			format:'yyyy-mm-dd',
        			weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0
                });

				var company = new AsynLoadQueryTree({
					renderTo: "#company",
					//selectid: {value: v.shipFrom},
					PostData: {rootType:"Ship From"},
					dataUrl: config.shipFrom.url,
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder:"company"
				});
				company.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				company.render();

				//到默认值并设置
				v.getDefaultValue({ storage_id:v.defaultStore , bound_type:"inbound" });

			},

			//到默认值并设置
			getDefaultValue:function(param)
			{

				$.getJSON(config.defaultValue.url, param, function (json) {
					console.log(json);
					var data = json.DATA;
					$.each(data,function(i,e){
						var _id = e.ID;
						$("#time_" + _id).val(e.VALUE);
					});
				});
			},
			saveLimit:function(evt)
			{
			//	alert("saveLimit");
				var v = this;
				
				var daaaaa = [];
				v.$el.find(":input[id!=saveLimit]").each(function(i,e){
					var value = $(this).val();
			 		var hour = $(this).data("hour");
			 		var date = $(this).data("date");
			 		var sumtype = $(this).data("sumtype");

			 		var obj = {
			 			value: value ? value : 0,	hour : hour,	date : date,	sumtype : sumtype
			 		}
			 		daaaaa.push(obj);
				});
				//提交数据
				var jsonString = JSON.stringify(daaaaa)
			 

			//	console.log(jsonString);
				return;
				$.ajax({
				    type: 'post',
				    // url: "/Sync10/_load/setBaseWork",
				    url: config.setBaseWorkUrl.url,
				    data: {data : jsonString , storage_id : v.defaultStore},
				    dataType: 'json',
				    async:false,
				    success: function (data) {
				       console.log(data);
				    },
				    error: function (data) {
				    	console.log(data);
				    }
				});

			},
			setAll:function(evt)
			{
				var allvalue =  $("#allvalue").val();
				$(".sum").val(allvalue);
			}
	});

}
);