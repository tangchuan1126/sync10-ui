"use strict";
require(["../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery",
			"./js/view/appointmentLimitDate",
			"bootstrap"],
			function($,appointmentLimitDate)
			{
				function getQueryString(name)
				{
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var pid = getQueryString("storage_id");
				var dates = getQueryString("alltddata");
				//console.log(pid);
				//console.log(dates);
			    var appointmentLimitDate = new appointmentLimitDate({defaultStore:pid,WeekDate:dates});
				
			});
	});