(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_brand'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\r\n   <form id=\"addBrandForm\" name=\"addBrandForm\">\r\n        <table width=\"580px\" >\r\n            <tr>\r\n                <td style=\"width:125px;\">\r\n                    <label for=\"b_name\">Brand Name</label>\r\n                </td>\r\n                <td style=\"width:255px;\">    \r\n                    <input name='b_name' id='b_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\" style=\"width:200px;\">*</td>\r\n            </tr>\r\n\r\n            <tr>\r\n                <td>\r\n                    <label for=\"b_number\">Brand Number</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='b_number' id='b_number' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            \r\n        </table>\r\n    </form>\r\n</div>\r\n\r\n";
},"useData":true});
templates['add_customer'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div>\r\n   <form id=\"addCustomerForm\" name=\"addCustomerForm\">\r\n        <table width=\"660px\" >\r\n            <tr>\r\n                <td>\r\n                    <label for=\"customer_id\">CustomerID</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='customer_id' id='customer_id' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n\r\n            <tr>\r\n                <td>\r\n                    <label for=\"customer_name\">Customer Name</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='customer_name' id='customer_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n\r\n\r\n            <tr>\r\n                <td>\r\n                    <label for=\"warehouse_name\">Address Name</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"house_address\">Address1</label>\r\n                </td>\r\n                <td>\r\n                    <input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"street_address\">Address2</label>\r\n                </td>\r\n                <td>\r\n                    <input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"city\">City</label>\r\n                </td>\r\n                <td>\r\n                    <input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"zipcode\">Zip Code</label>\r\n                </td>\r\n                <td>\r\n                    <input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr id=\"tr_state_dd\">\r\n                <td>\r\n                    <label for=\"state_dd\">State</label>\r\n                </td>\r\n                <td>\r\n                    <select id=\"state_dd\" name=\"state_dd\" style=\"width:300px;\">\r\n                        <option value=\"\" ></option>\r\n                    </select>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr  id=\"tr_state\">\r\n                <td>\r\n                    <label for=\"state\"></label>\r\n                </td>\r\n                <td>\r\n                    <input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"false\"/>\r\n                    <input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\"></td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"country\">Country</label>\r\n                </td>\r\n                <td>\r\n                    <select id=\"country\" name=\"country\" style=\"width:300px;\">\r\n                        <option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\r\n                    \r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            \r\n            <tr>\r\n                <td>\r\n                    <label for=\"contact\">Contact</label>\r\n                </td>\r\n                <td>\r\n                    <input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">Phone</label>\r\n                </td>\r\n                <td>\r\n                    <input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">E-Mail</label>\r\n                </td>\r\n                <td>\r\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">Fax</label>\r\n                </td>\r\n                <td>\r\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"payment_term\">Payment Term</label>\r\n                </td>\r\n                <td>\r\n                    <select id=\"payment_term\" name=\"payment_term\" style=\"width:300px;\">\r\n                        <option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.payments : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\r\n                    \r\n                </td>\r\n                <td  class=\"status validator_style\"></td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>\r\n\r\n";
},"useData":true});
templates['advanced_search_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style>\r\n#ad_tr_state{display:none}\r\n#advanced_search_tab_content table input,select{margin-bottom:0px;}\r\n</style>\r\n\r\n<div id=\"advanced_search_tab_content\">\r\n\r\n	<table>\r\n		<tr>\r\n			<td>CustomerID:</br><input id=\"advanced_search_customer_id\" value=\"\" type=\"text\" class=\"input_text_password_select\" style=\"width:200px;\"/></td>\r\n			<td>Customer Name:</br><input id=\"advanced_search_customer_name\" value=\"\" type=\"text\" class=\"input_text_password_select\" style=\"width:200px;\"/></td>\r\n			<td>Country:</br>\r\n				<select id=\"country_list\" name=\"country_list\" style=\"width:186px;\">\r\n					<option value=\"\" ></option>\r\n					\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</select>\r\n			</td>\r\n			<td>State:</br>\r\n				<div id=\"ad_tr_state_dd\">\r\n					<select id=\"ad_state_dd\" name=\"ad_state_dd\" style=\"width:186px;\">\r\n						<option value=\"\"></option>\r\n					</select>\r\n				</div>\r\n			</td>\r\n			<td>\r\n				</br>\r\n				<div id=\"ad_tr_state\">	\r\n						<input name=\"ad_is_text_state\" id=\"ad_is_text_state\" type=\"hidden\" value=\"false\"/>\r\n						<input name='ad_state' id='ad_state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" style=\"width:180px;\" value=\"\"/>\r\n				</div>\r\n			</td>\r\n			<td>Is Active:</br>\r\n				<select id=\"active\" name=\"active\" style=\"width:135px;\">\r\n					<option value=\"\"></option>\r\n					<option value=\"-1\" >All</option>\r\n					<option value=\"1\" selected>Active</option>\r\n					<option value=\"0\" >Inactive</option>\r\n				</select>\r\n			</td>\r\n			<td><br/>\r\n				 <a id=\"advanced_search_btn\" name=\"advanced_search_btn\" href=\"javascript:void(0)\" class=\"buttons\"><i class=\"fa fa-search\"></i>   Search</a>\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</div>";
},"useData":true});
templates['brand_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.brands : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "                <tr data-brandid=\""
    + alias2(alias1((depth0 != null ? depth0.cb_id : depth0), depth0))
    + "\">\r\n\r\n                    <td valign=\"middle\" width=\"220px\" style=\"border-left: 1px solid #dddddd;\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.brand_name : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"240px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.brand_number : depth0), depth0))
    + "\r\n                    </td>\r\n\r\n                    <td valign=\"middle\" style=\"text-align: center;width:70px;border-right: 1px solid #dddddd;\">\r\n\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),1,"=",{"name":"xifCond","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),0,"=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                    </td>\r\n                </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "                           <a href=\"javascript:void(0)\" name=\"b_inactive\" class=\"buttons\" title=\"Inactive\" style=\"margin-left:0;\"><i class=\"fa fa-minus-circle\"></i></a>\r\n";
},"5":function(depth0,helpers,partials,data) {
    return "                           <a href=\"javascript:void(0)\" name=\"b_active\" class=\"buttons\" title=\"Active\" style=\"margin-left:0;\"><i class=\"fa fa-check-circle\"></i></a>\r\n";
},"7":function(depth0,helpers,partials,data) {
    return "                <tr>\r\n                    <td colspan=\"3\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid #bbbbbb;\">No Data.</td>\r\n                </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "    <div class=\"brand-list-title\">\r\n        <ul class=\"list-inline\">\r\n          <li style=\"width:230px;\"><b>CustomerID:</b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_id : stack1), depth0))
    + "</li>\r\n          <li style=\"width:240px;\"><b>CustomerName:</b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_name : stack1), depth0))
    + "</li>\r\n          <li>\r\n              <a id=\"addBrand\" href=\"javascript:void(0)\" class=\"buttons\"><i class=\"fa fa-plus\"></i>   Add Brand</a>\r\n          </li>\r\n        </ul>         \r\n    </div>\r\n    <div>\r\n        <table class=\"brand_list_header\" cellspacing=\"0\">\r\n            <thead>\r\n                <tr>\r\n                    <th width=\"220px\">Brand Name</th>\r\n                    <th width=\"240px\">Brand Number</th>\r\n                    <th width=\"70px\" style=\"/*border-right:0px;*/\">\r\n                        Operation\r\n                    </th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.brands : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "            </tbody>\r\n        </table>\r\n    </div>\r\n\r\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n        <ul id=\"pagination-brand\" class=\"clearfix pagebox\">\r\n        </ul>\r\n    </div>\r\n";
},"useData":true});
templates['mod_customer'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div>  \r\n   <form id=\"addCustomerForm\" name=\"addCustomerForm\">\r\n        <table width=\"660px\" >\r\n            <tr> \r\n                <td>\r\n                    <label for=\"customer_id\">CustomerID</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='customer_id' id='customer_id' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_id : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n\r\n            <tr>\r\n                <td>\r\n                    <label for=\"customer_name\">Customer Name</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='customer_name' id='customer_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_name : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n\r\n\r\n            <tr>\r\n                <td>\r\n                    <label for=\"warehouse_name\">Address Name</label>\r\n                </td>\r\n                <td>    \r\n                    <input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.title : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"house_address\">Address1</label>\r\n                </td>\r\n                <td>\r\n                    <input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_house_number : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"street_address\">Address2</label>\r\n                </td>\r\n                <td>\r\n                    <input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_street : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"city\">City</label>\r\n                </td>\r\n                <td>\r\n                    <input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"zipcode\">Zip Code</label>\r\n                </td>\r\n                <td>\r\n                    <input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_zip_code : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr id=\"tr_state_dd\">\r\n                <td>\r\n                    <label for=\"state_dd\">State</label>\r\n                </td>\r\n                <td>\r\n                    <select id=\"state_dd\" name=\"state_dd\" style=\"width:300px;\">\r\n                        <option value=\"\" ></option>\r\n                    </select>\r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            <tr  id=\"tr_state\">\r\n                <td>\r\n                    <label for=\"state\"></label>\r\n                </td>\r\n                <td>\r\n                    <input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"false\"/>\r\n                    <input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_pro_input : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\"></td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"country\">Country</label>\r\n                </td>\r\n                <td>\r\n                    <select id=\"country\" name=\"country\" style=\"width:300px;\">\r\n                        <option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\r\n                    \r\n                </td>\r\n                <td  class=\"status validator_style\">*</td>\r\n            </tr>\r\n            \r\n            <tr>\r\n                <td>\r\n                    <label for=\"contact\">Contact</label>\r\n                </td>\r\n                <td>\r\n                    <input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">Phone</label>\r\n                </td>\r\n                <td>\r\n                    <input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">E-Mail</label>\r\n                </td>\r\n                <td>\r\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_email : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">Fax</label>\r\n                </td>\r\n                <td>\r\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_fax : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"payment_term\">Payment Term</label>\r\n                </td>\r\n                <td>\r\n                    <select id=\"payment_term\" name=\"payment_term\" style=\"width:300px;\">\r\n                        <option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.payments : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\r\n                    \r\n                </td>\r\n                <td  class=\"status validator_style\"></td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>\r\n\r\n";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                <tr data-customerkey=\""
    + alias2(alias1((depth0 != null ? depth0.customer_key : depth0), depth0))
    + "\">\r\n\r\n                    <td valign=\"middle\" width=\"150px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"310px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.customer_name : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"280px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.send_house_number : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"250px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.send_street : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"100px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.city : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"80px\">\r\n                        "
    + alias2(alias1((depth0 != null ? depth0.send_zip_code : depth0), depth0))
    + "\r\n                    </td>\r\n                    <td valign=\"middle\" width=\"80px\">\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pro_id : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\r\n                    </td>\r\n\r\n                    <td valign=\"middle\" width=\"80px\" style=\"text-align: center;\">\r\n\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"if","hash":{},"fn":this.program(8, data, 0),"inverse":this.program(10, data, 0),"data":data})) != null ? stack1 : "")
    + "\r\n                    </td>\r\n\r\n                    <td valign=\"middle\" style=\"text-align: center;width:150px;\">\r\n\r\n                    <!--0:禁用，1:启用-->\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":this.program(12, data, 0),"inverse":this.program(14, data, 0),"data":data})) != null ? stack1 : "")
    + "                       \r\n                        \r\n                        <a href=\"javascript:void(0)\" name=\"brand\" class=\"buttons\" title=\"Brand\" style=\"margin-left:0;\"><i class=\"fa fa-cubes\"></i></a>\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":this.program(17, data, 0),"inverse":this.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "                    </td>\r\n                </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.compare || (depth0 && depth0.compare) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.pro_id : depth0),-1,{"name":"compare","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.pro_name : depth0), depth0))
    + "\r\n";
},"4":function(depth0,helpers,partials,data) {
    return "                                    "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.send_pro_input : depth0), depth0))
    + "\r\n";
},"6":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.send_pro_input : depth0), depth0))
    + "\r\n";
},"8":function(depth0,helpers,partials,data) {
    return "\r\n                            <span name=\"showTitles\" class=\"linked badge\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.titlecnt : depth0), depth0))
    + "</span>\r\n                                \r\n";
},"10":function(depth0,helpers,partials,data) {
    return "\r\n                            <span class=\"badge\">0</span>\r\n                            \r\n";
},"12":function(depth0,helpers,partials,data) {
    return "                            <a href=\"javascript:void(0)\" name=\"modify\" class=\"buttons\" title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>\r\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1;

  return "                            <!-- "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.active : depth0),"","=",{"name":"xifCond","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " -->\r\n";
},"15":function(depth0,helpers,partials,data) {
    return " -->\r\n                            <a href=\"javascript:void(0)\" name=\"modify\" class=\"buttons\" title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>\r\n                            <!-- ";
},"17":function(depth0,helpers,partials,data) {
    return "                            <a href=\"javascript:void(0)\" name=\"inactive\" class=\"buttons\" title=\"Inactive\" style=\"margin-left:0;\"><i class=\"fa fa-minus-circle\"></i></a>\r\n";
},"19":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.active : depth0),"","=",{"name":"xifCond","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.active : depth0),"0","=",{"name":"xifCond","hash":{},"fn":this.program(22, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                            \r\n";
},"20":function(depth0,helpers,partials,data) {
    return "                                <a href=\"javascript:void(0)\" name=\"inactive\" class=\"buttons\" title=\"Inactive\" style=\"margin-left:0;\"><i class=\"fa fa-minus-circle\"></i></a>\r\n";
},"22":function(depth0,helpers,partials,data) {
    return "                                <a href=\"javascript:void(0)\" name=\"active\" class=\"buttons\" title=\"Active\" style=\"margin-left:0;\"><i class=\"fa fa-check-circle\"></i></a>\r\n";
},"24":function(depth0,helpers,partials,data) {
    return "    	        <tr>\r\n    	            <td colspan=\"9\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\r\n    	        </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\r\n    <div>\r\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\r\n            <thead>\r\n                <tr>\r\n                    <th width=\"150px\">CustomerID</th>\r\n                    <th width=\"310px\">Customer Name</th>\r\n                    <th width=\"280px\">Address 1</th>\r\n                    <th width=\"250px\">Address 2</th>\r\n                    <th width=\"100px\">City</th>\r\n                    <th width=\"80px\">Zip Code</th>\r\n                    <th width=\"80px\">Province</th>\r\n                    <th width=\"80px\">Title</th>\r\n                    <th width=\"150px\" style=\"border-right:0px;\">\r\n                        Operation\r\n                    </th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(24, data, 0),"data":data})) != null ? stack1 : "")
    + "            </tbody>\r\n        </table>\r\n    </div>\r\n\r\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\r\n        </ul>\r\n    </div>\r\n";
},"useData":true});
templates['search_templet'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\r\n    <div>\r\n        <div class=\"eso_search_parent\">\r\n            <div class=\"eso_search_icon\">\r\n                <a>\r\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n\r\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\r\n            <a id=\"addCustomer\" href=\"javascript:void(0)\" class=\"buttons button_add_customer\"><i class=\"fa fa-plus\"></i>   Add Customer</a>\r\n        </div>\r\n\r\n        <div class=\"eso_search_input\">\r\n\r\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
templates['show_title'] = template({"1":function(depth0,helpers,partials,data) {
    return "	<li class=\"list-group-item\">\r\n	"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "\r\n	</li>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:400px;height:400px;overflow:auto;\">\r\n<ul class=\"list-group\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</ul>\r\n</div>";
},"useData":true});
})();