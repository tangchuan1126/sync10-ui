define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_brand'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n   <form id=\"addBrandForm\" name=\"addBrandForm\">\n        <table width=\"580px\" >\n            <tr>\n                <td style=\"width:125px;\">\n                    <label for=\"b_name\">Brand Name</label>\n                </td>\n                <td style=\"width:255px;\">    \n                    <input name='b_name' id='b_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\n                </td>\n                <td  class=\"status validator_style\" style=\"width:200px;\">*</td>\n            </tr>\n\n            <tr>\n                <td>\n                    <label for=\"b_number\">Brand Number</label>\n                </td>\n                <td>    \n                    <input name='b_number' id='b_number' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            \n        </table>\n    </form>\n</div>\n\n";
},"useData":true});
templates['add_customer'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div>\n   <form id=\"addCustomerForm\" name=\"addCustomerForm\">\n        <table width=\"660px\" >\n            <tr>\n                <td>\n                    <label for=\"customer_id\">CustomerID</label>\n                </td>\n                <td>    \n                    <input name='customer_id' id='customer_id' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n\n            <tr>\n                <td>\n                    <label for=\"customer_name\">Customer Name</label>\n                </td>\n                <td>    \n                    <input name='customer_name' id='customer_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n\n\n            <tr>\n                <td>\n                    <label for=\"warehouse_name\">Address Name</label>\n                </td>\n                <td>    \n                    <input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"house_address\">Address1</label>\n                </td>\n                <td>\n                    <input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"street_address\">Address2</label>\n                </td>\n                <td>\n                    <input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"city\">City</label>\n                </td>\n                <td>\n                    <input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"zipcode\">Zip Code</label>\n                </td>\n                <td>\n                    <input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr id=\"tr_state_dd\">\n                <td>\n                    <label for=\"state_dd\">State</label>\n                </td>\n                <td>\n                    <select id=\"state_dd\" name=\"state_dd\" style=\"width:300px;\">\n                        <option value=\"\" ></option>\n                    </select>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr  id=\"tr_state\">\n                <td>\n                    <label for=\"state\"></label>\n                </td>\n                <td>\n                    <input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"false\"/>\n                    <input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\"></td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"country\">Country</label>\n                </td>\n                <td>\n                    <select id=\"country\" name=\"country\" style=\"width:300px;\">\n                        <option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n                    \n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            \n            <tr>\n                <td>\n                    <label for=\"contact\">Contact</label>\n                </td>\n                <td>\n                    <input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">Phone</label>\n                </td>\n                <td>\n                    <input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">E-Mail</label>\n                </td>\n                <td>\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">Fax</label>\n                </td>\n                <td>\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"payment_term\">Payment Term</label>\n                </td>\n                <td>\n                    <select id=\"payment_term\" name=\"payment_term\" style=\"width:300px;\">\n                        <option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.payments : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n                    \n                </td>\n                <td  class=\"status validator_style\"></td>\n            </tr>\n        </table>\n    </form>\n</div>\n\n";
},"useData":true});
templates['advanced_search_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style>\n#ad_tr_state{display:none}\n#advanced_search_tab_content table input,select{margin-bottom:0px;}\n</style>\n\n<div id=\"advanced_search_tab_content\">\n\n	<table>\n		<tr>\n			<td>CustomerID:</br><input id=\"advanced_search_customer_id\" value=\"\" type=\"text\" class=\"input_text_password_select\" style=\"width:200px;\"/></td>\n			<td>Customer Name:</br><input id=\"advanced_search_customer_name\" value=\"\" type=\"text\" class=\"input_text_password_select\" style=\"width:200px;\"/></td>\n			<td>Country:</br>\n				<select id=\"country_list\" name=\"country_list\" style=\"width:186px;\">\n					<option value=\"\" ></option>\n					\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</select>\n			</td>\n			<td>State:</br>\n				<div id=\"ad_tr_state_dd\">\n					<select id=\"ad_state_dd\" name=\"ad_state_dd\" style=\"width:186px;\">\n						<option value=\"\"></option>\n					</select>\n				</div>\n			</td>\n			<td>\n				</br>\n				<div id=\"ad_tr_state\">	\n						<input name=\"ad_is_text_state\" id=\"ad_is_text_state\" type=\"hidden\" value=\"false\"/>\n						<input name='ad_state' id='ad_state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" style=\"width:180px;\" value=\"\"/>\n				</div>\n			</td>\n			<td>Is Active:</br>\n				<select id=\"active\" name=\"active\" style=\"width:135px;\">\n					<option value=\"\"></option>\n					<option value=\"-1\" >All</option>\n					<option value=\"1\" selected>Active</option>\n					<option value=\"0\" >Inactive</option>\n				</select>\n			</td>\n			<td><br/>\n				 <a id=\"advanced_search_btn\" name=\"advanced_search_btn\" href=\"javascript:void(0)\" class=\"buttons\"><i class=\"fa fa-search\"></i>   Search</a>\n			</td>\n		</tr>\n	</table>\n</div>";
},"useData":true});
templates['brand_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.brands : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "                <tr data-brandid=\""
    + alias2(alias1((depth0 != null ? depth0.cb_id : depth0), depth0))
    + "\">\n\n                    <td valign=\"middle\" width=\"220px\" style=\"border-left: 1px solid #dddddd;\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.brand_name : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"240px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.brand_number : depth0), depth0))
    + "\n                    </td>\n\n                    <td valign=\"middle\" style=\"text-align: center;width:70px;border-right: 1px solid #dddddd;\">\n\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),1,"=",{"name":"xifCond","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),0,"=",{"name":"xifCond","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n                    </td>\n                </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "                           <a href=\"javascript:void(0)\" name=\"b_inactive\" class=\"buttons\" title=\"Inactive\" style=\"margin-left:0;\"><i class=\"fa fa-minus-circle\"></i></a>\n";
},"5":function(depth0,helpers,partials,data) {
    return "                           <a href=\"javascript:void(0)\" name=\"b_active\" class=\"buttons\" title=\"Active\" style=\"margin-left:0;\"><i class=\"fa fa-check-circle\"></i></a>\n";
},"7":function(depth0,helpers,partials,data) {
    return "                <tr>\n                    <td colspan=\"3\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid #bbbbbb;\">No Data.</td>\n                </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "    <div class=\"brand-list-title\">\n        <ul class=\"list-inline\">\n          <li style=\"width:230px;\"><b>CustomerID:</b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_id : stack1), depth0))
    + "</li>\n          <li style=\"width:240px;\"><b>CustomerName:</b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_name : stack1), depth0))
    + "</li>\n          <li>\n              <a id=\"addBrand\" href=\"javascript:void(0)\" class=\"buttons\"><i class=\"fa fa-plus\"></i>   Add Brand</a>\n          </li>\n        </ul>         \n    </div>\n    <div>\n        <table class=\"brand_list_header\" cellspacing=\"0\">\n            <thead>\n                <tr>\n                    <th width=\"220px\">Brand Name</th>\n                    <th width=\"240px\">Brand Number</th>\n                    <th width=\"70px\" style=\"/*border-right:0px;*/\">\n                        Operation\n                    </th>\n                </tr>\n            </thead>\n            <tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.brands : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "            </tbody>\n        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination-brand\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n";
},"useData":true});
templates['mod_customer'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div>  \n   <form id=\"addCustomerForm\" name=\"addCustomerForm\">\n        <table width=\"660px\" >\n            <tr> \n                <td>\n                    <label for=\"customer_id\">CustomerID</label>\n                </td>\n                <td>    \n                    <input name='customer_id' id='customer_id' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_id : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n\n            <tr>\n                <td>\n                    <label for=\"customer_name\">Customer Name</label>\n                </td>\n                <td>    \n                    <input name='customer_name' id='customer_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.customer_name : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n\n\n            <tr>\n                <td>\n                    <label for=\"warehouse_name\">Address Name</label>\n                </td>\n                <td>    \n                    <input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"this.value=this.value.toUpperCase()\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.title : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"house_address\">Address1</label>\n                </td>\n                <td>\n                    <input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_house_number : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"street_address\">Address2</label>\n                </td>\n                <td>\n                    <input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_street : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"city\">City</label>\n                </td>\n                <td>\n                    <input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"zipcode\">Zip Code</label>\n                </td>\n                <td>\n                    <input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_zip_code : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr id=\"tr_state_dd\">\n                <td>\n                    <label for=\"state_dd\">State</label>\n                </td>\n                <td>\n                    <select id=\"state_dd\" name=\"state_dd\" style=\"width:300px;\">\n                        <option value=\"\" ></option>\n                    </select>\n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            <tr  id=\"tr_state\">\n                <td>\n                    <label for=\"state\"></label>\n                </td>\n                <td>\n                    <input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"false\"/>\n                    <input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_pro_input : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\"></td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"country\">Country</label>\n                </td>\n                <td>\n                    <select id=\"country\" name=\"country\" style=\"width:300px;\">\n                        <option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n                    \n                </td>\n                <td  class=\"status validator_style\">*</td>\n            </tr>\n            \n            <tr>\n                <td>\n                    <label for=\"contact\">Contact</label>\n                </td>\n                <td>\n                    <input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">Phone</label>\n                </td>\n                <td>\n                    <input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">E-Mail</label>\n                </td>\n                <td>\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_email : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">Fax</label>\n                </td>\n                <td>\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_fax : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"payment_term\">Payment Term</label>\n                </td>\n                <td>\n                    <select id=\"payment_term\" name=\"payment_term\" style=\"width:300px;\">\n                        <option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.payments : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                    </select>\n                    \n                </td>\n                <td  class=\"status validator_style\"></td>\n            </tr>\n        </table>\n    </form>\n</div>\n\n";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                <tr data-customerkey=\""
    + alias2(alias1((depth0 != null ? depth0.customer_key : depth0), depth0))
    + "\">\n\n                    <td valign=\"middle\" width=\"150px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.customer_id : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"310px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.customer_name : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"280px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.send_house_number : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"250px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.send_street : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"100px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.city : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"80px\">\n                        "
    + alias2(alias1((depth0 != null ? depth0.send_zip_code : depth0), depth0))
    + "\n                    </td>\n                    <td valign=\"middle\" width=\"80px\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pro_id : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\n                    </td>\n\n                    <td valign=\"middle\" width=\"80px\" style=\"text-align: center;\">\n\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"if","hash":{},"fn":this.program(8, data, 0),"inverse":this.program(10, data, 0),"data":data})) != null ? stack1 : "")
    + "\n                    </td>\n\n                    <td valign=\"middle\" style=\"text-align: center;width:150px;\">\n\n                    <!--0:禁用，1:启用-->\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":this.program(12, data, 0),"inverse":this.program(14, data, 0),"data":data})) != null ? stack1 : "")
    + "                       \n                        \n                        <a href=\"javascript:void(0)\" name=\"brand\" class=\"buttons\" title=\"Brand\" style=\"margin-left:0;\"><i class=\"fa fa-cubes\"></i></a>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.active : depth0),{"name":"if","hash":{},"fn":this.program(17, data, 0),"inverse":this.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "                    </td>\n                </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.compare || (depth0 && depth0.compare) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.pro_id : depth0),-1,{"name":"compare","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.pro_name : depth0), depth0))
    + "\n";
},"4":function(depth0,helpers,partials,data) {
    return "                                    "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.send_pro_input : depth0), depth0))
    + "\n";
},"6":function(depth0,helpers,partials,data) {
    return "                            "
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.send_pro_input : depth0), depth0))
    + "\n";
},"8":function(depth0,helpers,partials,data) {
    return "\n                            <span name=\"showTitles\" class=\"linked badge\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.titlecnt : depth0), depth0))
    + "</span>\n                                \n";
},"10":function(depth0,helpers,partials,data) {
    return "\n                            <span class=\"badge\">0</span>\n                            \n";
},"12":function(depth0,helpers,partials,data) {
    return "                            <a href=\"javascript:void(0)\" name=\"modify\" class=\"buttons\" title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1;

  return "                            <!-- "
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.active : depth0),"","=",{"name":"xifCond","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " -->\n";
},"15":function(depth0,helpers,partials,data) {
    return " -->\n                            <a href=\"javascript:void(0)\" name=\"modify\" class=\"buttons\" title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>\n                            <!-- ";
},"17":function(depth0,helpers,partials,data) {
    return "                            <a href=\"javascript:void(0)\" name=\"inactive\" class=\"buttons\" title=\"Inactive\" style=\"margin-left:0;\"><i class=\"fa fa-minus-circle\"></i></a>\n";
},"19":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing;

  return ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.active : depth0),"","=",{"name":"xifCond","hash":{},"fn":this.program(20, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias1).call(depth0,(depth0 != null ? depth0.active : depth0),"0","=",{"name":"xifCond","hash":{},"fn":this.program(22, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                            \n";
},"20":function(depth0,helpers,partials,data) {
    return "                                <a href=\"javascript:void(0)\" name=\"inactive\" class=\"buttons\" title=\"Inactive\" style=\"margin-left:0;\"><i class=\"fa fa-minus-circle\"></i></a>\n";
},"22":function(depth0,helpers,partials,data) {
    return "                                <a href=\"javascript:void(0)\" name=\"active\" class=\"buttons\" title=\"Active\" style=\"margin-left:0;\"><i class=\"fa fa-check-circle\"></i></a>\n";
},"24":function(depth0,helpers,partials,data) {
    return "    	        <tr>\n    	            <td colspan=\"9\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n    	        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n    <div>\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <thead>\n                <tr>\n                    <th width=\"150px\">CustomerID</th>\n                    <th width=\"310px\">Customer Name</th>\n                    <th width=\"280px\">Address 1</th>\n                    <th width=\"250px\">Address 2</th>\n                    <th width=\"100px\">City</th>\n                    <th width=\"80px\">Zip Code</th>\n                    <th width=\"80px\">Province</th>\n                    <th width=\"80px\">Title</th>\n                    <th width=\"150px\" style=\"border-right:0px;\">\n                        Operation\n                    </th>\n                </tr>\n            </thead>\n            <tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.customers : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(24, data, 0),"data":data})) != null ? stack1 : "")
    + "            </tbody>\n        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n";
},"useData":true});
templates['search_templet'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n    <div>\n        <div class=\"eso_search_parent\">\n            <div class=\"eso_search_icon\">\n                <a>\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\n                </a>\n            </div>\n        </div>\n\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\n            <a id=\"addCustomer\" href=\"javascript:void(0)\" class=\"buttons button_add_customer\"><i class=\"fa fa-plus\"></i>   Add Customer</a>\n        </div>\n\n        <div class=\"eso_search_input\">\n\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['show_title'] = template({"1":function(depth0,helpers,partials,data) {
    return "	<li class=\"list-group-item\">\n	"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "\n	</li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div style=\"width:400px;height:400px;overflow:auto;\">\n<ul class=\"list-group\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.titles : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</ul>\n</div>";
},"useData":true});
return templates;
});