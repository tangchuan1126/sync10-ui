"use strict";
 define(["jquery","backbone","handlebars","templates",
	"../model/customerModel",
	"./customerSearchResultView",
	"showMessage",
	"select2"
	],
	function($,Backbone,Handlebars,templates,models,SearchResultView){
		
			
	return  Backbone.View.extend({
	el:"#advanced_command",
	template:templates.advanced_search_templet,

	initialize:function(options){
		var dis = this;
		dis.view = options.view;
		dis.countryCollection = new models.CountryCollection();
	},
    setView:function(views){

        this.CustomerSearchResultView = views.CustomerSearchResultView;
    },
	render:function(){
			var v = this; 
			
			v.$el.html(v.template({countries:v.countryCollection.toJSON()[0].countrys}));


			$("#advanced_search_btn").click(function(evt){v.search(evt);});
			
			$("#country_list").select2({
					placeholder: "Select...",
					allowClear: true
			}).on('change',function(){
                            v.search();
                            var ccid = $("#country_list").select2("val");
                            if(ccid)
                            {
                                $.ajax({
                                    url:"/Sync10/_customer/province?ccid="+ccid,
                                    dataType:'json',
                                    type:'get',
                                    async:"false",
                                    success:function(data){
                                        data = data.provinces;

                                        if("" != data){
                                            $("#ad_is_text_state").val("false");
                                            $("#tr_state_dd").show(); 
                                            $("#tr_state").hide();
                                            var provinces = eval(data);
                                            $("#ad_state_dd").html("");
                                            var str = "";
                                            str = "<option value=''></option>";
                                            for(var i = 0; i < data.length; i ++){
                                                str += "<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>";
                                            }
                                            str += "<option value='100'>Input...</option>";
                                            $("#ad_state_dd").html(str);
                                            $("#ad_state_dd").select2("val","");
                                        }else{
                                            var dd_str = "<option value=''></option>";
                                            dd_str += "<option value='100'>Input...</option>";
                                            $("#ad_state_dd").html(dd_str);
                                            $("#ad_state_dd").select2("val","");
                                            $("#tr_state").hide();
                                            $("#ad_is_text_state").val("true");
                                        }



                                    },
                                    error:function(){
                                        console.log("error getting provinces");
                                    }
                                })
                            }
                            else
                            {
                                var str = "";
                                str = "<option value=''></option>";
                                $("#ad_state_dd").html(str);
                                $("#ad_state_dd").select2("val","");
                                $("#ad_state").val("");
                                $("#ad_tr_state").hide();
                            }
                            


                        });
			
			$("#ad_state_dd").select2({
					placeholder: "Select...",
					allowClear: true
			}).on('change',function(){
                            
                            var value = $("#ad_state_dd").val();
    
                            if(value == "100"){
                                $("#ad_state_dd").select2("val","100");
                                $("#ad_tr_state").show();
                                $("#ad_is_text_state").val("true");
                            }else{
                                v.search();
                                $("#ad_is_text_state").val("false");
                                $("#ad_tr_state").hide();
                                $("#ad_state").val("");
                                $("#tr_state_dd").show(); 
                            }
                        });
			
			$("#active").select2({
				placeholder: "Select...",
				allowClear: true
			}).on('change',function(){v.search();});

            $('#advanced_search_customer_id').on('keypress',function(event){
                if(event.keyCode == "13")    
                {
                    v.search();
                }
            });

            $('#advanced_search_customer_name').on('keypress',function(event){
                if(event.keyCode == "13")    
                {
                    v.search();
                }
            });

            $('#ad_state').on('keypress',function(event){
                if(event.keyCode == "13")    
                {
                    v.search();
                }
            });

	},
	events:{

	},
	search:function(evt){
		var v = this;
		var customer_id = $("#advanced_search_customer_id").val();
		var customer_name = $("#advanced_search_customer_name").val();
		var country = $("#country_list").val();
		var country_value = "";
		if(country!=""){
			country_value = $("#country_list option[value='"+country+"']").html();
		}
		var is_text_state = $("#ad_is_text_state").val();
		var state = $("#ad_state").val();
		var state_dd = $("#ad_state_dd").val();
		
		var state_value = "";
		if(is_text_state=="true"){
			state_value = state;
		}else{
			if(state_dd!=""){
				state_value = $("#ad_state_dd option[value='"+state_dd+"']").html();
				
			}
		}
		
		var active = $("#active").val();

		var searchModel = new models.SearchModel({

                searchCustomerId: customer_id.trim(),
                searchCustomerName: customer_name.trim(),
                searchStateValue: state_value.trim(),
                searchCountryValue: country_value.trim(),
                active: active.trim(),

                pageNo:"1",

                cmd:"advance"
            });

          	this.CustomerSearchResultView.render(searchModel);
	 
	}  
		
	});

			
		
});