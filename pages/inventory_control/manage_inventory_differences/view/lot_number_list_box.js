"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/lot_numbers_model",
  "./lp_type_list_box.js",
  "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,lot_models,lpTypeView,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#lot_number_list_box_container",
      template:templates.immybox_view,
      collection: new lot_models.LotImmyCollection(),

      render:function(){
        
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var title = $("#diff_selected_title").val()?$("#diff_selected_title").val():"0";
        var warehouse = $("#diff_selected_warehouse").val()?$("#diff_selected_warehouse").val():"0";
        var product_line = $("#diff_selected_product_line").val()?$("#diff_selected_product_line").val():"0";
        var product_cat = $("#diff_selected_product_cat").val()?$("#diff_selected_product_cat").val():"0";
        var models = $("#diff_selected_model").val()?$("#diff_selected_model").val():"0";

        var url = config.getLotNumbersJSON+ "?ps_id="+warehouse;
        if(title!="0"){url+= "&title_ids="+title}
        if(product_line!="0"){url+= "&product_line="+product_line}
        if(product_cat!="0"){url+= "&category="+product_cat}
        if(models!="0"){url+= "&model_numbers="+models}

        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});

        var html=dis.template({divId:"diff_lot_div", name:"Lot #", inputId:"diff_lot_number", selectedInput:"diff_selected_lot_number"});
        dis.$el.html(html);
        

        var place_holder = {
          text :"text",
          value : "0"};
         
       
        var titleImmydata = {
          renderTo: "#diff_lot_div",
          dataUrl: dis.collection,
         
          inputId: "#diff_lot_number",
          selectedInputId: "#diff_selected_lot_number",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        
      },

      pull_areas:function(){
        
       new lpTypeView().render();
      }
      
    });

}); 