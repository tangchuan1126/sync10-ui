"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/product_line_model.js",
  "./product_cat_list_box.js",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,productLineModel,categoryListView,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#product_line_list_box_container",
      template:templates.immybox_view,
      collection: new productLineModel.PLImmyCollection(),
      task:null,
      
      render:function(){
        
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var title = $("#diff_selected_title").val()?$("#diff_selected_title").val():"0";
        var warehouse = $("#diff_selected_warehouse").val()?$("#diff_selected_warehouse").val():"0";
        
        var url = config.getProductLineJSON+ "?ps_id="+warehouse;
        if(title!="0"){url+= "&title_ids="+title}
        
        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});

        var html=dis.template({divId:"diff_product_line_div", name:"Product Line", inputId:"diff_product_line", selectedInput:"diff_selected_product_line"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#diff_product_line_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_product_line",
          selectedInputId: "#diff_selected_product_line",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        
      },

      pull_areas:function(){
        
        var dis = this;
        var category = new categoryListView();
        category.render()
      }
      
    });

}); 