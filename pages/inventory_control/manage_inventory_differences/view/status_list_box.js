"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",
  
], function( config, $, Backbone, Handlebars, templates ,ImmyBox) {
    
    var ButtonView =  Backbone.View.extend({
      el:"#status_list_box_container",
      template:templates.immybox_view,
      
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"diff_status_div", name:"Status", inputId:"diff_status", selectedInput:"diff_selected_status"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#diff_status_div",
          dataUrl: [{text: 'Any', value: '0'},{text: 'Pending', value: '1'},
              {text: 'Approved', value: '2'},{text: 'Rejected', value: '3'}],
          inputId: "#diff_status",
          selectedInputId: "#diff_selected_status",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
         titleImmy.on("events.change",function(){});
        titleImmy.render();
        
      },

      
      
  });
return ButtonView;
}); 

