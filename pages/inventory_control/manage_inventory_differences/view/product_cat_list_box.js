"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/product_cat_model.js",
  "./model_list_box.js",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,product_cat_models,modelListView,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#product_cat_list_box_container",
      template:templates.immybox_view,
      collection: new product_cat_models.PCImmyCollection(),

      
      render:function(){
        
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var title = $("#diff_selected_title").val()?$("#diff_selected_title").val():"0";
        var warehouse = $("#diff_selected_warehouse").val()?$("#diff_selected_warehouse").val():"0";
        var product_line = $("#diff_selected_product_line").val()?$("#diff_selected_product_line").val():"0";
        
        var url = config.getProductCategoryJSON+ "?ps_id="+warehouse;
        if(title!="0"){url+= "&title_ids="+title}
        if(product_line!="0"){url+= "&product_line="+product_line}

        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});

        var html=dis.template({divId:"diff_product_cat_div", name:"Category", inputId:"diff_product_cat", selectedInput:"diff_selected_product_cat"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#diff_product_cat_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_product_cat",
          selectedInputId: "#diff_selected_product_cat",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        
      },

      pull_areas:function(){
        
        var dis = this;
        new modelListView().render()
      }
      
    });

}); 