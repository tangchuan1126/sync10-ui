"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/location_model.js",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models, ImmyBox) {
    
    return Backbone.View.extend({
      el:"#location_list_box_container",
      template:templates.immybox_view,
      
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"diff_location_div", name:"Location", inputId:"diff_location", selectedInput:"diff_selected_location"});
        dis.$el.html(html);
        dis.collection.fetch({dataType: "json",async: false});
        var titleImmydata = {
          renderTo: "#diff_location_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_location",
          selectedInputId: "#diff_selected_location",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){});
        titleImmy.render();
        
      },
            
    });

}); 