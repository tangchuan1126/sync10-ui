"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/area_model.js",
  "../model/location_model.js",
  "./location_list_box.js",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models,location_models,locationView, ImmyBox) {
    
    return Backbone.View.extend({
      el:"#area_list_box_container",
      template:templates.immybox_view,
            
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"diff_area_div", name:"Area", inputId:"diff_area", selectedInput:"diff_selected_area"});
        dis.$el.html(html);
        dis.collection.fetch({dataType: "json",async: false});
        var titleImmydata = {
          renderTo: "#diff_area_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_area",
          selectedInputId: "#diff_selected_area",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_loc();});
        titleImmy.render();
        dis.pull_loc();
        
      },
      
      pull_loc:function(){
        
        var val = $("#diff_selected_area").val();
        var col = new location_models.LocationImmyCollection(val);
        var areas = new locationView({collection:col});
        areas.render();
        
      }
      
    });

}); 