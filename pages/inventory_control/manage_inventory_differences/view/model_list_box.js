"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/model_numbers_model.js",
  "./lot_number_list_box",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,models,lotView,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#model_number_list_box_container",
      template:templates.immybox_view,
      collection: new models.MNImmyCollection(),

      
      
      render:function(){
        
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var title = $("#diff_selected_title").val()?$("#diff_selected_title").val():"0";
        var warehouse = $("#diff_selected_warehouse").val()?$("#diff_selected_warehouse").val():"0";
        var product_line = $("#diff_selected_product_line").val()?$("#diff_selected_product_line").val():"0";
        var product_cat = $("#diff_selected_product_cat").val()?$("#diff_selected_product_cat").val():"0";

        var url = config.getModelNumbersJSON+ "?ps_id="+warehouse;
        if(title!="0"){url+= "&title_ids="+title}
        if(product_line!="0"){url+= "&product_line="+product_line}
        if(product_cat!="0"){url+= "&category="+product_cat}

        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});

        var html=dis.template({divId:"diff_model_div", name:"Model #", inputId:"diff_model_number", selectedInput:"diff_selected_model_number"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#diff_model_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_model_number",
          selectedInputId: "#diff_selected_model_number",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        
      },

      pull_areas:function(){
        
        var dis = this;
        new lotView().render();

      }
      
    });

}); 