"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/count_difference_model.js",
  "../model/user_task_count_model.js",
  "./create_verification_view",
  "art_Dialog/dialog",
  "mCustomScrollbar",
  "bootstrap",
  "bootstrap_tab"

], function( config, $, Backbone, Handlebars, templates, HandleBarsExt, difference_models, userModel,createVerification, dialog, mCustomScrollbar) {

    var CountDifferencesView =  Backbone.View.extend({
      template: templates.count_differences,
      el:"#location_differences_container",
      
      initialize:function(data){
          var dis = this;
          dis.differenceId = data.differenceId;
          dis.parentView = data.parentView;
      },
      
      render:function(){

        var dis = this;
        dis.collection= new difference_models.DifferenceCollection({differenceId:dis.differenceId});
        dis.collection.fetch({dataType: "json",async: false});

        //add containers tabs
        var Containershtml =templates.container_differences_list({differences: dis.collection.toJSON()});
        dis.$el.html("");
        dis.$el.show();
        dis.$el.html(Containershtml);

        // add containers and products differences
        $.each(dis.collection.toJSON(),function(index,item){
          var info = templates.difference_info({difference:item});
          $("#"+item.CONTAINER).append(info);

          var systemTree = item.systemContainerTree? item.systemContainerTree.toJSON() : false;
          var systemView = templates.difference_tree({tree:systemTree,treeType:"system"});
          $("#system_physical_div .system_div").append(systemView);

          var physicalTree = item.physicalContainerTree ? item.physicalContainerTree.toJSON() : false;
          var physicalView = templates.difference_tree({tree:physicalTree,treeType:"physical"});
          $("#system_physical_div .physical_div").append(physicalView);

          if(item.CREATE_DIFFERENCE){
            var productView = templates.difference_product_info({calculatedDiffs:item.CREATE_DIFFERENCE.toJSON()});
            $("#product_count_difference_table").append(productView);
          }
          
          // verification task 
          if(item.VERIFICATION_TASKS){
            var VerificationTask = templates.difference_verification_task({list:item.VERIFICATION_TASKS});
            $("#difference_verification_task_list").html(VerificationTask);
            
            $.each(item.VERIFICATION_TASKS,function(ind, v_item){

              if(v_item.SHOW_TREE){
                  var systemTree = v_item.systemContainerTree? v_item.systemContainerTree : false;
                  var systemView = templates.difference_tree({tree:systemTree,treeType:"verification system"});
                  $("#verification_task_"+v_item.VERIFICATION_TASK_ID+" .system_div").append(systemView);
             
                  var physicalTree = v_item.physicalContainerTree ? v_item.physicalContainerTree : false;
                  var physicalView = templates.difference_tree({tree:physicalTree,treeType:"verification physical"});
                  $("#verification_task_"+v_item.VERIFICATION_TASK_ID+" .physical_div").append(physicalView);
              }

              if(v_item.CREATE_DIFFERENCE){
                var productView = templates.difference_product_info({calculatedDiffs:v_item.CREATE_DIFFERENCE.toJSON()});
                $("#verification_task_"+v_item.VERIFICATION_TASK_ID+" #product_count_difference_table").append(productView);
              }
              
            });
          }

          // styling the trees
          $.each($("#system_physical_div .system_div .cntr"), function(index,item){
            if($("#system_physical_div .physical_div .cntr_"+$(item).data("container_id")).length){
              if($(item).data("type")=="3"){
                $(item).addClass("different_container")
              }
            }else{
              $(item).addClass("missing_container")
            }
          });

          $.each($("#system_physical_div .physical_div .cntr"), function(index,item){
            if($("#system_physical_div .system_div .cntr_"+$(item).data("container_id")).length){
              if($(item).data("type")=="3"){
                $(item).addClass("different_container")
              }
            }else{
              $(item).addClass("new_container")
            }
          });

          //add locate link for missing and new containers 
          $.each($("#system_physical_div .system_div .missing_container"), function(index,it){
            $(it).append("<a class='locates_link' data-container='"+item.CONTAINER+"'></a>");

          });
          $.each($("#system_physical_div .physical_div .new_container"), function(index,it){
            $(it).append("<a class='locates_link' data-container='"+item.CONTAINER+"'></a>");

          });
          

          

        });
          
         //toggle tree inner containers
          $("#container_count_dif_div .cntr").click(function(evt){
            var div = $(evt.currentTarget).parent();
            $(div).children(":not(:first)").slideToggle();
            
          }); 
          $(".parent_child_container_div .cntr").trigger('click');

        $(".locates_link").click(function(evt){
            dis.locate(evt);
            evt.stopPropagation();
          });

        var selectedTab = -1;
        var selectedContent = -1;
        $.each(dis.collection.toJSON(),function(index,item){
          
          if(selectedTab==-1 && item.STATUS==1){
            selectedTab = item.CONTAINER_ID;
            selectedContent = item.CONTAINER;
          }
          var className = ""
          switch(item.STATUS){
            case 1:
              className = "";
              break;
            case 2:
              className = "approved_container";
              break;
            case 3:
              className = "rejected_container";
              break;
            

          }
          
          
          $("#"+item.CONTAINER_ID).addClass(className);
        });
        if(selectedTab==-1 && dis.collection.length >0){
          var item = dis.collection.models[0].toJSON();
          selectedTab = item.CONTAINER_ID;
          selectedContent = item.CONTAINER;
        }
        $("#"+selectedTab).addClass("active");
        $("#"+selectedContent).addClass("active");

        if($("a[name='approve_btn']")){
          
          $("a[name='approve_btn']").click(function(evt){dis.approve_difference(evt);});
        }
        if($("a[name='reject_btn']")){
          
          $("a[name='reject_btn']").click(function(evt){dis.reject_difference(evt);});
        }
        $("#back_btn_view_detail").click(function(evt){dis.back(evt)});
        
        $(".verification_task_reassign").click(function(evt){dis.open_reassign(evt)});
        
        if($("a[name='verification_approve_btn']")){
          
          $("a[name='verification_approve_btn']").click(function(evt){dis.approve_verification_task(evt);});
        }

        if($("button[name='create_verification_task_diff_btn']")){
          
          $("button[name='create_verification_task_diff_btn']").click(function(evt){dis.create_verification_task(evt);});
        }
     },
     locate:function(evt){
      
      var dis = this;
      var container_id = $(evt.target).parent().data("container_id");
      var container_name = $(evt.target).data("container");
      var info_div = $("#"+container_name).find(".info_div");
      $(info_div).html("loading...");
      $(info_div).show();
      var warehouse_id = $("#difference_warehouse_id").val();
        $.ajax({
            url:  config.systemLocation+container_id+"&ps_id="+warehouse_id,
            type: 'get',
            timeout: 60000,
            cache:false,
            dataType: 'json',
            contentType: "application/json",
            async:true,
            
            error: function(jqXHR, textStatus, errorThrown) {
              $(info_div).html("Cannot find this container in system");
             
            },
            success: function(data){
              var txt = ""
              if(data && data.container){
                var area_name = data.area_name;
                var location_name = data.location_name;
                var container = data.container.CONTAINER;
                
                if(data.container.CHILD){
                  txt = "<b>"+data.container.CHILD.CONTAINER+"</b> is located in area:<b>"+area_name+"</b> at location:<b>"+location_name+"</b> in <b>"+container+"</b>"
                }
                else{
                  txt = "<b>"+container+"</b> is located in area:<b>"+area_name+"</b> at location:<b>"+location_name
                }
                
              }else{
                txt = "Cannot find this container in system";
              }
              $(info_div).html(txt);
              
              
            }
          });
     },
     open_reassign:function(evt){
        var dis = this;
        var button_div = $(evt.target).parent();
        var task_id = $(evt.target).data("verification_task_id");
        var start_date = $(button_div).find(".verification_task_start_date").val();
        var end_date = $(button_div).find(".verification_task_end_date").val();

        var url = config.getUsersAndAssignLoadJSON;
        url += "?ps_id="+1000005;
        url += "&start_date="+start_date;
        url += "&end_date="+end_date;
        url += "&is_repeating="+"false";

        var collection = new userModel.UserCollection();
        collection.url = url;
        collection.fetch({dataType: "json",async: false});
        var html = templates.assign_users({users:collection.toJSON()})
        $("#verification_task_"+task_id+" .verification_task_reassign_block").html(html);

        $(".verification_task_reassign_block .employee_box").click(function(evt){
           $(".verification_task_reassign_block .employee_box").each(function(i,item){
                  $(item).removeClass("selected_employee");
           });
           $(this).addClass("selected_employee");
           dis.check_assign_user_ok();
        }); 
        dis.custom_scroll();
        $(".verification_task_reassign_block #verification_assign_user_ok").click(function(evt){dis.verification_assign_user(evt);})
        $(".verification_task_reassign_block #verification_assign_user_cancel").click(function(evt){dis.cancel_reassign(evt);})
     },

     verification_assign_user:function(evt){

        var dis = this;
        var task_id = parseInt($(evt.currentTarget).parent().parent().parent().parent().find("input.verification_task_id_value").val());
        var task_instance_id = parseInt($(evt.currentTarget).parent().parent().parent().parent().find("input.verification_task_instance_id_value").val());
        
        var name = $("#verification_task_"+task_id+" .verification_task_reassign_block .selected_employee .employee_name").html();
        var id = $("#verification_task_"+task_id+" .verification_task_reassign_block .selected_employee .employee_name").data("v_assign_user-id");

        
        var instances=[];
        var assignment = {user_id:id,instance_id:task_instance_id};
        var instance = {id:task_instance_id,assignment:assignment}
        instances.push(instance);
        var task = {id:task_id,instances:instances}

        $.ajax({
            url:  config.reassignVerificationTask,
            type: 'put',
            timeout: 60000,
            cache:false,
            data: JSON.stringify(task),
            dataType: 'json',
            contentType: "application/json",
            async:true,
            
            error: function(jqXHR, textStatus, errorThrown) {
            },
            success: function(data){
            }
          });
        $("#verification_task_"+task_id+" .verification_task_reassign_block").html("");
        $("#verification_task_"+task_id+" span.v_assigned_user_name").html(name);
      },
     cancel_reassign : function(evt){
        var task_id = $(evt.currentTarget).parent().parent().parent().parent().find("input.verification_task_id_value").val();
        var task_instance_id = $(evt.currentTarget).parent().parent().parent().parent().find("input.verification_task_instance_id_value").val()
        $("#verification_task_"+task_id+" .verification_task_reassign_block").html("");
     },
     check_assign_user_ok:function(){
        var dis = this;
        if($(".verification_task_reassign_block .selected_employee").length){
          $(".verification_task_reassign_block #verification_assign_user_ok").removeClass("disabled");
        }else{
          $(".verification_task_reassign_block #verification_assign_user_ok").addClass("disabled");
        }
      },
     custom_scroll:function(){
        
        var list_unstyled = $(".verification_task_reassign_block .employee_box_div");
        list_unstyled.mCustomScrollbar({
          axis: "y",
          scrollbarPosition: "outside"
        });
        list_unstyled.find(".mCSB_scrollTools").css("right", "8px");
      },
     back:function(evt){
        var dis = this;
        dis.parentView.render({});
        $("#location_differences_container").hide();
        $("#area_differences").show();
        
     },
     process:function(diff_ids,reference_type,status,comments){
      var dis = this;
         $.ajax({
            type: "put",
            url: config.processDifference+"?reference_id="+parseInt(diff_ids)+"&status="+status+"&comments="+comments+"&reference_type="+parseInt(reference_type),
            contentType: 'application/json; charset=UTF-8',
            timeout: 60000,
            cache:false,
            dataType: 'json',
           
            async:true,
            success: function(){dis.back();dis.refresh_list();},
            error:function(xhr, ajaxOptions, thrownError){
                console.log("error",xhr.responseText);
            },
           
            
          });
     },
     reject_difference:function(evt){
        var dis = this;
        evt.preventDefault();
        var parent = $(evt.target).parent().parent().parent();
        var diff_id = dis.differenceId;
        var type = parent.find("input[name='type']").val();
        var comments =$(parent).find(".comments_div textarea");
        var commentsVal = $(comments).val();
        if(commentsVal==null || commentsVal==""){
          dis.mark_invalid($(parent).find("span[name='validation_msg']")[0],comments);
        } else{
          dis.mark_valid($(parent).find("span[name='validation_msg']")[0],comments);
          dis.process(diff_id,type,3,commentsVal);
        }
         
     },
     approve_difference:function(evt){
        var dis = this;
        evt.preventDefault();
        var parent = $(evt.target).parent().parent().parent();
        var diff_id = dis.differenceId;
        var type = parent.find("input[name='type']").val();
        var comments =$(parent).find(".comments_div textarea");
        var commentsVal = $(comments).val();

        if(commentsVal==null || commentsVal==""){
          dis.mark_invalid($(parent).find("span[name='validation_msg']")[0],comments);
        } else{
          dis.mark_valid($(parent).find("span[name='validation_msg']")[0],comments);
          dis.process(diff_id,type,2,commentsVal);
        }
         
     },

     create_verification_task:function(evt){
        var dis = this;
        evt.preventDefault();
        var parent = $(evt.target).parent().parent().parent();
        var diff_id = dis.differenceId;
        var list = dis.collection; 
        new createVerification({parentView:dis}).render({differences:list.toJSON()});
         
     },
     approve_verification_task:function(evt){
        var dis = this;
        evt.preventDefault();
        var parent = $(evt.target).parent().parent().parent();
        var diff_id = parent.find("input[name='verification_instance_id']").val();
        var type = parent.find("input[name='type']").val();
        var comments =$(parent).find(".comments_div textarea");
        var commentsVal = $(comments).val();

        if(commentsVal==null || commentsVal==""){
          dis.mark_invalid($(parent).find("span[name='validation_msg']")[0],comments);
        } else{
          dis.mark_valid($(parent).find("span[name='validation_msg']")[0],comments);
          dis.process(diff_id,type,2,commentsVal);
        }
         
     },

    refresh_list:function(){
      var dis = this;
      dis.parentView.render();
      //this.render();
    },

    mark_valid:function(error_box,comments){
      $(error_box).text("");
      $(error_box).removeClass("visible").addClass("hidden");
            
     },
     mark_invalid:function(error_box,comments){
      $(error_box).text("please enter comments");
      $(error_box).removeClass("hidden").addClass("visible");
      $(comments).focus();
     },

  });
return CountDifferencesView;
}); 

