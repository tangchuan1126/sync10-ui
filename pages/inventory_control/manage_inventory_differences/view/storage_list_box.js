"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/storage_model.js",
  "../model/area_model.js",
  "../model/user_model.js",
  "./area_list_box.js",
  "../js/ImmyboxControl",
  "../model/title_model.js",
  "./title_list_box.js",
   "./status_list_box.js",
   "./scanned_by_list_box.js",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,storage_models,area_models,user_models,areaListBoxView, ImmyBox, title_models, titleListBoxView, statusListBoxView, scannedByListBoxView ) {
    
    return Backbone.View.extend({
      el:"#storage_list_box_container",
      template:templates.immybox_view,
      collection: new storage_models.StorageImmyCollection(),
      
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"diff_warehouse_div", name:"Warehouse", inputId:"diff_warehouse", selectedInput:"diff_selected_warehouse"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#diff_warehouse_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_warehouse",
          selectedInputId: "#diff_selected_warehouse",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        dis.pull_areas();
        
      },


      pull_areas:function(){
        var dis = this;
        var val = $("#diff_selected_warehouse").val();
        if(val=="0"){
          dis.hide_other_fields();
        }else{
          dis.show_other_fields();
          var titleCol = new title_models.TitleImmyCollection(val);
          var titles = new titleListBoxView({collection:titleCol});
          titles.render();
          var col = new area_models.AreaImmyCollection(val);
          var areas = new areaListBoxView({collection:col});
          areas.render();

          new statusListBoxView().render();
          var colu = new user_models.UserCollection(val);
          var users = new scannedByListBoxView({collection:colu});
          users.render();
        }
      },

      hide_other_fields:function(){
        var dis = this;
        $("#area_list_box_container").hide();
        $("#location_list_box_container").hide();
        $("#title_list_box_container").hide();
        $("#product_line_list_box_container").hide();
        $("#product_cat_list_box_container").hide();
        $("#model_number_list_box_container").hide();
        $("#lot_number_list_box_container").hide();
        $("#lp_type_list_box_container").hide();
        $("#scanned_by_list_box_container").hide();
        $("#status_list_box_container").hide();
       
      },

      show_other_fields:function(){
        var dis = this;
        $("#area_list_box_container").show();
        $("#location_list_box_container").show();
        $("#title_list_box_container").show();
        $("#product_line_list_box_container").show();
        $("#product_cat_list_box_container").show();
        $("#model_number_list_box_container").show();
        $("#lot_number_list_box_container").show();
        $("#lp_type_list_box_container").show();
        $("#scanned_by_list_box_container").show();
        $("#status_list_box_container").show();
        
      }
        
        

      
    });

}); 