"use strict";
define(
  [
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/location_difference_model.js",
  "./container_differences_list.js",
  "art_Dialog/dialog",

], 
function( config, $, Backbone, Handlebars, templates , HandleBarsExt, difference_models, containerDifferencesView, dialog) {

    return Backbone.View.extend({
      template: templates.location_differences_list,
      el:"#location_differences_container",
      scrollHandle : 0, 
      scrollStep : 50, 
      
   
      
      render:function(saa_id){
        var dis = this;

        var loading = dialog();
        loading.showModal();
        dis.collection= new difference_models.DifferenceCollection(saa_id);
        dis.collection.fetch({dataType: "json",async: false});
        var html =dis.template({differences: dis.collection.toJSON()});
        dis.$el.html("");
        dis.$el.html(html);
        
        var selectedTab = -1;
        var selectedContent = -1;
        var selectedSaaId = -1;
        $('#locationTabs a').click(function (evt) {
          evt.preventDefault();
          
          var saa_id,sal_id,slc_position_all;
          if($(evt.target).attr('href')){
            saa_id = $(evt.target).attr("name");
            sal_id = $(evt.target).parent().attr("id");
            slc_position_all = $(evt.target).find($("span")).attr("name");
          }else{
            saa_id = $(evt.target).parent().attr("name");
            sal_id = $(evt.target).parent().parent().attr("id");
            slc_position_all = $(evt.target).attr("name");
          }
          
         
          var loading = dialog();
          loading.showModal();

          new containerDifferencesView(dis).render(slc_position_all,saa_id,sal_id);
          loading.close().remove();
        });

        $.each(dis.collection.toJSON(), function(i, item) {

          if(item.APPROVE_STATUS==2){
            $("#"+item.SAL_ID+" > a > span").addClass("approved");
            $("#"+item.SAL_ID).addClass("approved_loc");
          }
          if(selectedTab==-1 && item.APPROVE_STATUS==1){
            selectedTab = item.SAL_ID;
            selectedContent = item.SLC_POSITION_ALL;
            selectedSaaId = item.SAA_ID;
          }
        });

         if(selectedTab==-1  &&  dis.collection.length>0){ // incase all are approved, select first
          var selected = dis.collection.models[0];
          selectedTab = selected.attributes["SAL_ID"];
          selectedContent = selected.attributes["SLC_POSITION_ALL"];
          selectedSaaId = selected.attributes["SAA_ID"];;

        }

        $("#"+selectedTab).addClass("active");
        $("#location_differences_scroll").animate({ scrollTop: $("#"+selectedTab).offset().top -400}, 'slow');
        
        $("#"+selectedContent).addClass("active");
        
        new containerDifferencesView(dis).render(selectedContent,selectedSaaId,selectedTab);
        
        dis.enableScrolling();
        loading.close().remove();
        $("#back_btn").click(function(evt){dis.back(evt)});
     },

     events:{
    
     },

     back:function(evt){
        var dis = this;
        var loading = dialog();
        loading.showModal();
        $("#area_differences").show();
        $("#filter_btn_bk").trigger("click");
        $("#location_differences_container").html("");
        loading.close().remove();
     },
     enableScrolling:function(){
      var dis = this;
      

        //Start the scrolling process
        $(".panner").on("mouseenter", function () {
            var data = $(this).data('scrollModifier'),
                direction = parseInt(data, 10);
                
            $(this).addClass('active');

            dis.startScrolling(direction, dis.scrollStep);
        });

        //Kill the scrolling
        $(".panner").on("mouseleave", function () {
            dis.stopScrolling();
            $(this).removeClass('active');
        });

    
     },
     startScrolling:function(modifier, step) {
      var dis = this;
        
        if (dis.scrollHandle === 0) {
            dis.scrollHandle = setInterval(function () {
                var parentsc = $("#location_differences_scroll");
                
                var newOffset = parentsc.scrollTop() + (dis.scrollStep * modifier);

                parentsc.scrollTop(newOffset);
            }, 100);
        }
      },

      stopScrolling:function() {
        var dis = this;
            clearInterval(dis.scrollHandle);
            dis.scrollHandle = 0;
        }
     
  });

}); 

