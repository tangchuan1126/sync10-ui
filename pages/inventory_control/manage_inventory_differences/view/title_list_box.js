"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "./product_line_list_box",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, ImmyBox,productLineView) {
    
    return Backbone.View.extend({
      el:"#title_list_box_container",
      template:templates.immybox_view,
      collection:null,

      initialize:function(data){
        var dis = this;
        dis.collection = data.collection;
      },
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"diff_title_div", name:"Title", inputId:"diff_title", selectedInput:"diff_selected_title"});
        dis.$el.html(html);
        dis.collection.fetch({dataType: "json",async: false});
        var titleImmydata = {
          renderTo: "#diff_title_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_title",
          selectedInputId: "#diff_selected_title",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull();});
        titleImmy.render();
        dis.pull();
        
      },
      pull:function(){
        new productLineView().render();
      }
           
    });

}); 