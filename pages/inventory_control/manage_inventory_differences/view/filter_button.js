"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "./differences_list.js",
      "../js/ImmyboxControl",
  "art_Dialog/dialog",
  "./storage_list_box",
  "./status_list_box",

  "bootstrap",
  
], function( config, $, Backbone, Handlebars, templates, differencesView ,ImmyBox,dialog, storageView,statusListBoxView) {
    
    var ButtonView =  Backbone.View.extend({
      template: templates.filter_button,
      el:"#filter_button_container",
     

      render:function(){
        var dis = this;
        var html =dis.template();
        dis.$el.html("");
        dis.$el.html(html);
        
       
        $("#diff_clear_btn").click(function(evt){dis.clear_search(evt)});

        $("#filter_btn").click(function(evt){dis.filter_btn_click(evt)});
        $("#filter_btn_bk").click(function(evt){dis.filter(evt)});
     },
     events:{
     
     },
     clear_search:function(){
        new storageView().render();
        new statusListBoxView().render();
     },
     filter_btn_click:function(evt){
        var dis = this;

          $("#filtered_storage_id").val($("#diff_selected_warehouse").val());
          $("#filtered_area_id").val($("#diff_selected_area").val());
          $("#filtered_location_id").val($("#diff_selected_location").val());
          $("#filtered_status").val($("#diff_selected_status").val());
          $("#filtered_title_id").val($("#diff_selected_title").val());
          $("#filtered_product_line_id").val($("#diff_selected_product_line").val());
          $("#filtered_product_cat_id").val($("#diff_selected_product_cat").val());
          $("#filtered_model_number_id").val($("#diff_selected_model_number").val());
          $("#filtered_lot_number_id").val($("#diff_selected_lot_number").val());
          $("#filtered_lp_type_id").val($("#diff_selected_lp_type_number").val());
          $("#filtered_scanned_by").val($("#diff_selected_scanned_by").val());
        dis.filter();


     },
     filter:function(evt){
        
        var filtered_storage_id = $("#filtered_storage_id").val();
        var filtered_area_id = $("#filtered_area_id").val();
        var filtered_location_id = $("#filtered_location_id").val();
        var filtered_title_id = $("#filtered_title_id").val();
        var filtered_status = $("#filtered_status").val();
        var filtered_product_line_id = $("#filtered_product_line_id").val();
        var filtered_product_cat_id = $("#filtered_product_cat_id").val();
        var filtered_lot_number_id = $("#filtered_lot_number_id").val();
        var filtered_model_number_id = $("#filtered_model_number_id").val();
        var filtered_lp_type_id = $("#filtered_lp_type_id").val();
        var filtered_scanned_by = $("#filtered_scanned_by").val();

        var loading = dialog();
        loading.showModal();
        new differencesView().render({ps_id:filtered_storage_id,area_id:filtered_area_id,
          location_id:filtered_location_id,status_id:filtered_status,sortby:"",sort:"",pageNo:1,title_id:filtered_title_id,
          product_line:filtered_product_line_id,category:filtered_product_cat_id,model_number:filtered_model_number_id,
          lot_number:filtered_lot_number_id,lp_type:filtered_lp_type_id,scanned_by:filtered_scanned_by})
        loading.close().remove();
        
     }
      
  });
return ButtonView;
}); 

