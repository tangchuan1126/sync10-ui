"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, ImmyBox,productLineView) {
    
    return Backbone.View.extend({
      el:"#scanned_by_list_box_container",
      template:templates.immybox_view,
      collection:null,

      initialize:function(data){
        var dis = this;
        dis.collection = data.collection;
      },
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"diff_scanned_by_div", name:"Scanned By", inputId:"diff_scanned_by", selectedInput:"diff_selected_scanned_by"});
        dis.$el.html(html);
        dis.collection.fetch({dataType: "json",async: false});
        var titleImmydata = {
          renderTo: "#diff_scanned_by_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#diff_scanned_by",
          selectedInputId: "#diff_selected_scanned_by",
          
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull();});
        titleImmy.render();

        
      },
      pull:function(){
        
      }
           
    });

}); 