define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\">\n<div class=\"input-group-addon\" >Area</div><input id=\"selected_area_id\" type=\"hidden\" value=\"0\"/>\n<input id='area_list_box' type='text' class='form-control' />\n</div>";
},"useData":true});
templates['assign_users'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"\">\n	<div class=\"body panel-body\">\n		<div class=\"employee_box_div\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n		<div class=\"clear\"></div>\n	</div>\n	<div class=\"panel-footer\">\n		<a  id=\"verification_assign_user_ok\" class=\"btn btn-primary disabled\">\n		<span class=\"glyphicon glyphicon-ok\"></span> Ok</a>\n		<a  id=\"verification_assign_user_cancel\" class=\"btn btn-warning\">\n		<span class=\"glyphicon glyphicon-cancel\"></span> Cancel</a>\n	</div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "			<div class=\"employee_box\" >\n				<div class=\"employee_name\" data-v_assign_user-id=\""
    + alias2(alias1((depth0 != null ? depth0.user_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.user_name : depth0), depth0))
    + "</div>\n				<div class=\"count\">"
    + alias2(alias1((depth0 != null ? depth0.count : depth0), depth0))
    + "</div>\n				<div class=\"clear\"></div>\n			</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['container_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "            <ul class=\"nav nav-tabs\" id=\"containerTabs\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </ul>\n            <div class=\"tab-content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                <li id=\""
    + alias2(alias1((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + "\">\n                    <a href=\"#"
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\" data-toggle=\"tab\" >\n                        "
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\n                        <span class=\"\" >&nbsp;&nbsp;&nbsp;&nbsp;</span>\n                    </a>\n                </li>\n";
},"4":function(depth0,helpers,partials,data) {
    return "                <div class=\"tab-pane\" id=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">\n                   \n                </div>\n";
},"6":function(depth0,helpers,partials,data) {
    return "        <div class=\"alert alert-info\" role=\"alert\">\n          \n          No Difference found.\n        </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\">\n  <div  class=\"panel-heading\">\n      <div class=\"left\" id=\"difference_heading_panel\">Cycle Count Difference</div>\n      <div id=\"back-buttons-div\" class=\"right\"><a href=\"#\" id=\"back_btn_view_detail\" class=\"btn btn-warning\">\n      <span class=\"glyphicon glyphicon-list-alt\"></span> Manage Differences List\n      </a></div>\n      <div class=\"clear\"></div>\n    </div>\n  <div class=\"panel-body\">\n\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n</div>";
},"useData":true});
templates['container_tree_differences'] = template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "        <div class=\"username\">\n           <b>Title:</b> "
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n        </div>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return "               <span class=\"parent\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                    <span class=\"child product\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CHILDREN : stack1),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"7":function(depth0,helpers,partials,data) {
    var stack1;

  return "                        <span class=\"child\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(8, data, 0),"inverse":this.program(10, data, 0),"data":data})) != null ? stack1 : "");
},"8":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                            <span class=\"child1 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"10":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"11":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                <span class=\"child1\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(12, data, 0),"inverse":this.program(14, data, 0),"data":data})) != null ? stack1 : "");
},"12":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                    <span class=\"child2 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                        <span class=\"child3\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(16, data, 0),"inverse":this.program(18, data, 0),"data":data})) != null ? stack1 : "");
},"16":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                            <span class=\"child3 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"18":function(depth0,helpers,partials,data) {
    return "                                            \n";
},"20":function(depth0,helpers,partials,data) {
    return "               <span class=\"new\">New to system</span>\n";
},"22":function(depth0,helpers,partials,data) {
    var stack1;

  return "               <span class=\"parent\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1),{"name":"if","hash":{},"fn":this.program(23, data, 0),"inverse":this.program(25, data, 0),"data":data})) != null ? stack1 : "");
},"23":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                  <span class=\"child product\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\n";
},"25":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CHILDREN : stack1),{"name":"each","hash":{},"fn":this.program(26, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"26":function(depth0,helpers,partials,data) {
    var stack1;

  return "                        <span class=\"child\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(27, data, 0),"inverse":this.program(29, data, 0),"data":data})) != null ? stack1 : "");
},"27":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                          <span class=\"child1 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"29":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(30, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"30":function(depth0,helpers,partials,data) {
    var stack1;

  return "                              <span class=\"child1\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(31, data, 0),"inverse":this.program(33, data, 0),"data":data})) != null ? stack1 : "");
},"31":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                <span class=\"child2 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"33":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.CHILDREN : depth0),{"name":"each","hash":{},"fn":this.program(34, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"34":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                  <span class=\"child2\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.P_NAME : depth0),{"name":"if","hash":{},"fn":this.program(35, data, 0),"inverse":this.program(37, data, 0),"data":data})) != null ? stack1 : "");
},"35":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                                      <span class=\"child3 product\">"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"37":function(depth0,helpers,partials,data) {
    return "                                      \n";
},"39":function(depth0,helpers,partials,data) {
    return "              <span class=\"missing\">Missing now</span>\n";
},"41":function(depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "  <table class=\"table table-striped\" class=\"count_diff_table\" >\n        <thead>\n            <th>Product Name</th>\n            <th>System Count</th>\n            <th>Physical Count</th>\n            \n        </thead>\n        <tbody >\n";
  stack1 = ((helper = (helper = helpers.calculatedDiffs || (depth0 != null ? depth0.calculatedDiffs : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"calculatedDiffs","hash":{},"fn":this.program(42, data, 0),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers.calculatedDiffs) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </tbody>\n  </table>      \n";
},"42":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    \n          <tr >\n            <td>"
    + alias3(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\n            <td>"
    + alias3(((helper = (helper = helpers.systemCount || (depth0 != null ? depth0.systemCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"systemCount","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"difference\">"
    + alias3(((helper = (helper = helpers.physicalCount || (depth0 != null ? depth0.physicalCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"physicalCount","hash":{},"data":data}) : helper)))
    + "</td>\n                  \n          </tr>\n";
},"44":function(depth0,helpers,partials,data) {
    var helper;

  return "    <div class=\"approval_div\">\n          <div class=\"comments_div\">\n          <input type=\"hidden\" name=\"diff_id\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.conDiffId || (depth0 != null ? depth0.conDiffId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"conDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\n              <label>Comments</label>\n              <textarea  name=\"comments\"></textarea>\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n          </div>\n          <div class=\"approve_btn_div\">\n          <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\n            <span class=\"glyphicon glyphicon-ok\">\n            Approve\n          </a>\n          <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\n            <span class=\"glyphicon glyphicon-remove\">\n            Reject\n          </a> \n          </div>\n    </div>\n";
},"46":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "  <div class=\"processed_div\">\n\n    <div class=\"username\">\n       <b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_TITLE : stack1), depth0))
    + "</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_BY_NAME : stack1), depth0))
    + "\n    </div>\n    <div class=\"username\">\n       <b>Comments:</b> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.COMMENTS : stack1), depth0))
    + "\n    </div>\n  </div>  \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<div class=\"count_container\">\n    <div id=\"container_count_dif_div\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  \n        <div class=\"username\">\n           <b>Scanned By:</b> "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.USERNAME : stack1), depth0))
    + "\n        </div>\n        <div id=\"system_physical_div\">\n          <div class=\"system_div\">\n            <h5>System Containers</h5>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.systemTree : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(20, data, 0),"data":data})) != null ? stack1 : "")
    + "          </div>   \n          <div class=\"physical_div\">\n            <h5>Physical Containers</h5>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.physicalTree : depth0),{"name":"if","hash":{},"fn":this.program(22, data, 0),"inverse":this.program(39, data, 0),"data":data})) != null ? stack1 : "")
    + "          </div>   \n          <div class=\"clear\"></div>\n        </div>  \n    </div>\n\n\n\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.calculatedDiffs : depth0),{"name":"if","hash":{},"fn":this.program(41, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(44, data, 0),"inverse":this.program(46, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['count_differences'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=helpers.helperMissing, alias2="function", buffer = 
  "<div class=\"count_container_content\">\n<div class=\"count_container\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  <div class=\"username\">\n     <b>Scanned By:</b> "
    + this.escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"username","hash":{},"data":data}) : helper)))
    + "\n  </div>\n  <table class=\"table table-striped\" class=\"count_diff_table\" >\n      <thead>\n          <th>Product Name</th>\n          <th>System Count</th>\n          <th>Physical Count</th>\n          \n      </thead>\n      <tbody >\n";
  stack1 = ((helper = (helper = helpers.productDifferences || (depth0 != null ? depth0.productDifferences : depth0)) != null ? helper : alias1),(options={"name":"productDifferences","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.productDifferences) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "      </tbody>\n  </table>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias1).call(depth0,(depth0 != null ? depth0.status : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(6, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var helper;

  return "  <div class=\"username\">\n     <b>Title:</b> "
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n  </div>\n";
},"4":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "  \n        <tr >\n          <td>"
    + alias3(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\n          <td>"
    + alias3(((helper = (helper = helpers.SYSTEM_COUNT || (depth0 != null ? depth0.SYSTEM_COUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SYSTEM_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\n          <td class=\"difference\">"
    + alias3(((helper = (helper = helpers.PHYSICAL_COUNT || (depth0 != null ? depth0.PHYSICAL_COUNT : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"PHYSICAL_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\n        </tr>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <div class=\"approval_div\">\n          <div class=\"comments_div\">\n              <input type=\"hidden\" name=\"diff_id\" value=\""
    + alias3(((helper = (helper = helpers.productDiffId || (depth0 != null ? depth0.productDiffId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"productDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\n              <label>Comments</label>\n              <textarea  name=\"comments\"></textarea>\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n          </div>\n          <div class=\"approve_btn_div\">\n            <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\n              <span class=\"glyphicon glyphicon-ok\">"
    + alias3(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ID","hash":{},"data":data}) : helper)))
    + "\n              Approve\n            </a>\n            <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\n              <span class=\"glyphicon glyphicon-remove\">\n              Reject\n            </a> \n          </div>\n    </div>\n";
},"8":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <div class=\"processed_div\">\n\n      <div class=\"username\">\n         <b>"
    + alias3(((helper = (helper = helpers.processed_by_title || (depth0 != null ? depth0.processed_by_title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"processed_by_title","hash":{},"data":data}) : helper)))
    + "</b>&nbsp;"
    + alias3(((helper = (helper = helpers.processed_by || (depth0 != null ? depth0.processed_by : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"processed_by","hash":{},"data":data}) : helper)))
    + "\n      </div>\n      <div class=\"username\">\n         <b>Comments:</b> "
    + alias3(((helper = (helper = helpers.comments || (depth0 != null ? depth0.comments : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"comments","hash":{},"data":data}) : helper)))
    + "\n      </div>\n    </div>  \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.productDifferences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            \n\n";
},"useData":true});
templates['create_verification_task'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "						<div class=\"v_t_assign_user_btn_div\">\n							<button type=\"button\" id=\"create_verification_assign_btn\" class=\"assign_verification_task_button btn btn-sm btn-primary  disabled\">Assign User</button>\n						</div>	\n						<div id=\"v_error_msg\" class=\"alert alert-danger\"></div>\n						\n						<table class=\"table table-striped\" width=\"100%\" >\n						<thead>\n						  	<th><input type=\"checkbox\" id=\"create_verification_select_all\"/></th>\n							<th>Area</th>\n					  		<th>Location</th>\n						  	<th>Title</th>\n						  	<th>Container</th>\n						  	<th>Scanned by</th>\n						   	<th >Assigned User</th>\n\n						</thead>\n						<tbody id=\"create_verification_differences_table_body\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</tbody>\n						</table>\n\n						\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						   <tr>\n						          <td><input type=\"checkbox\" id=\"create_verification_checkbox_"
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\" data-diff_id="
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + " class=\"create_verification_difference_list_checkbox\"/></td>\n						   		   <td>"
    + alias2(alias1((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\n						         <td>"
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "</td>\n						         <td>"
    + alias2(alias1((depth0 != null ? depth0.TITLE_NAME : depth0), depth0))
    + "</td>\n						         <td>"
    + alias2(alias1((depth0 != null ? depth0.LP_CONTAINER : depth0), depth0))
    + "</td>\n						         <td>"
    + alias2(alias1((depth0 != null ? depth0.SCANNED_BY_NAME : depth0), depth0))
    + "</td>\n						         <td></td>\n						   </tr>\n						   \n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"create_verification_task_page\">\n	<div class=\"div-table\" id=\"v_attr_table\">\n        <div class=\"div-table-row\">\n	        <div class=\"div-table-cell\">\n	          <div class=\"input-group required\" >\n	          	<div class=\"input-group-addon\">Task Code</div>\n				<input type=\"text\" name=\"verification_task_name\" id=\"verification_task_name\" class=\"form-control\" maxlength=\"30\"/>\n	          </div>\n	        </div>\n	        <div class=\"div-table-cell\" >\n	          <div class=\"input-group required\" id=\"create_verification_task_priority_view\">\n\n	          </div>\n	        </div>\n	        <div class=\"div-table-cell\" >\n	          <div class=\"input-group required\" >\n	          	<div class=\"input-group-addon\">End Date</div>\n				<input type=\"text\" name=\"verification_end_date\" id=\"verification_end_date\" class=\"form-control\" />\n	          </div>\n	        </div>\n      	</div>\n	</div>\n	\n	<div class=\"list_users_table_wrapper\">\n		<div class=\"div-table\" id=\"v_d_table\">\n\n			<div class=\"div-table-row\">\n				<div class=\"div-table-cell differences_list\">\n					<!-- -->\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n					<!-- -->\n				</div>\n				<div class=\"div-table-cell users_list panel panel-default\" id=\"list_of_users_verification_task\"></div>\n			</div>\n		</div>\n	</div>\n\n\n</div>";
},"useData":true});
templates['difference_info'] = template({"1":function(depth0,helpers,partials,data) {
    return "";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "			<div class=\"div-table-row\">\n				<div class=\"div-table-cell\"><b>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.PROCESSED_TITLE : stack1), depth0))
    + "</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.REVIEWED_BY_NAME : stack1), depth0))
    + "</div>\n			</div>\n			\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "			<div class=\"diff_comments_div\">\n				<div ><b>Comments:</b>&nbsp;"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.COMMENTS : stack1), depth0))
    + "</div>\n			</div>\n";
},"7":function(depth0,helpers,partials,data) {
    var stack1;

  return "		    <div class=\"approval_div\">\n		          <div class=\"comments_div\">\n			          <input type=\"hidden\" name=\"diff_id\" value=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.ID : stack1), depth0))
    + "\"/>\n			          <input type=\"hidden\" name=\"type\" value=\"1\"/>\n		              <label class=\"required\">Comments</label>\n		              <textarea  name=\"comments\"></textarea>\n		              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n		          </div>\n		          <div class=\"approve_btn_div\">\n			          <a name=\"approve_btn\" class=\"btn btn-success\" title=\"Approve\">\n			            <span class=\"glyphicon glyphicon-ok\"></span> Approve</a>\n			          \n			          \n			          <a name=\"reject_btn\" class=\"btn btn-danger\" title=\"Reject\">\n			            <span class=\"glyphicon glyphicon-remove\"></span> Reject</a>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.IS_VERIFICATION_TASK_CREATABLE : stack1),{"name":"if","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		          </div>\n		        \n		    </div>\n		  \n";
},"8":function(depth0,helpers,partials,data) {
    return "		          \n							<button type=\"button\" name=\"create_verification_task_diff_btn\" class=\"create_verification_task_diff_btn btn btn-primary\">Create Verification Task</button>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "<div class=\"count_container_content\">\n	<div class=\"count_container\">\n		<input type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.WAREHOUSE_ID : stack1), depth0))
    + "\" id=\"difference_warehouse_id\"/>\n		<div class=\"div-table\" style=\"width:100%\">\n			<div class=\"div-table-row\">\n				<div class=\"div-table-cell\"><b>Scanned By:</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.SCANNED_BY_NAME : stack1), depth0))
    + "</div>\n				<div class=\"div-table-cell\"><b>Area:</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.AREA_NAME : stack1), depth0))
    + "</div>\n				<div class=\"div-table-cell\"><b>Location:</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.LOCATION_NAME : stack1), depth0))
    + "</div>\n				<div class=\"div-table-cell\"><b>Lot Number:</b>&nbsp;"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.LOT_NUMBER : stack1), depth0))
    + "</div>\n\n			</div>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.STATUS : stack1),1,{"name":"ifCond","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "		</div>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.STATUS : stack1),1,{"name":"ifCond","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "		<div id=\"container_count_dif_div\">\n			<div id=\"system_physical_div\">\n      			<div class=\"system_div\">\n      				<h5>System View</h5>\n      			</div>\n      			<div class=\"alert alert-info info_div\"></div>\n      			<div class=\"physical_div\">\n        			<h5>Physical View</h5>\n        		</div>\n        		<div class=\"clear\"></div>\n        	</div>\n		</div>\n\n		<div id=\"product_count_difference_table\"></div>\n\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.STATUS : stack1),1,{"name":"ifCond","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n\n		  <div id=\"difference_verification_task_list\"></div>\n	</div>\n</div>";
},"useData":true});
templates['difference_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"verification_task_button_div\">\n<button type=\"button\" id=\"create_verification_task_btn\" class=\"verification_task_button btn btn-primary disabled\">Create Verification Task</button>\n\n</div>\n<div class=\"clear\"></div>\n<table class=\"table table-striped\" width=\"100%\" >\n<thead>\n  <th><input type=\"checkbox\" id=\"differences_select_all\"/></th>\n	<th>Area</th>\n  <th>Location</th>\n  <th>Title</th>\n  <th>Container</th>\n  <th class=\"sort_link\"><a class=\""
    + alias3(((helper = (helper = helpers.post_date_css || (depth0 != null ? depth0.post_date_css : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"post_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"scanned_at\">Scanned Date</a></th>\n  <th>Scanned by</th>\n   <th> Status</th>\n   <th>Verification Status</th>\n   <!--th>Reviewer</th>\n   <th class=\"sort_link\"><a class=\""
    + alias3(((helper = (helper = helpers.approve_date_css || (depth0 != null ? depth0.approve_date_css : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"approve_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"reviewed_at\">Reviewed Date</a></th-->\n   <th></th>\n\n</thead>\n<tbody id=\"differences_table_body\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</tbody>\n</table>\n\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "   <tr>\n          <td>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.IS_VERIFICATION_TASK_CREATABLE : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "          </td>\n   		   <td>"
    + alias2(alias1((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.TITLE_NAME : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.LP_CONTAINER : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.SCANNED_AT : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.SCANNED_BY_NAME : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.STATUS_STRING : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.VERIFICATION_STATUS : depth0), depth0))
    + "\n         <!--td>"
    + alias2(alias1((depth0 != null ? depth0.REVIEWED_BY_NAME : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.REVIEWED_AT : depth0), depth0))
    + "</td-->\n         <td><button type=\"button\"  class=\"review_btn_difference btn btn-default btn-sm\" title=\"Review\" name=\""
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">Review</button></td>\n   </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "              <input type=\"checkbox\" id=\"differences_checkbox_"
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\" data-diff_id="
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + " \n              class=\"difference_list_checkbox\"/>\n";
},"5":function(depth0,helpers,partials,data) {
    return "<div class=\"alert alert-info\">\n   No differences found\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "<div id=\"pagination\" class=\"pagebox\">\n   \n</div>";
},"useData":true});
templates['difference_product_info'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "  <table class=\"table table-striped\" class=\"count_diff_table\" >\n        <thead>\n            <th>Model Number</th>\n            <th>System Count</th>\n            <th>Physical Count</th>\n            <th>Difference Count</th>\n            \n        </thead>\n        <tbody >\n";
  stack1 = ((helper = (helper = helpers.calculatedDiffs || (depth0 != null ? depth0.calculatedDiffs : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"calculatedDiffs","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers.calculatedDiffs) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </tbody>\n  </table>      \n";
},"2":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    \n          <tr >\n            <td>"
    + alias3(((helper = (helper = helpers.MODEL_NUMBER || (depth0 != null ? depth0.MODEL_NUMBER : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"MODEL_NUMBER","hash":{},"data":data}) : helper)))
    + "</td>\n            <td>"
    + alias3(((helper = (helper = helpers.systemCount || (depth0 != null ? depth0.systemCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"systemCount","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"difference\">"
    + alias3(((helper = (helper = helpers.physicalCount || (depth0 != null ? depth0.physicalCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"physicalCount","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"\">"
    + alias3(((helper = (helper = helpers.differenceCount || (depth0 != null ? depth0.differenceCount : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"differenceCount","hash":{},"data":data}) : helper)))
    + "</td>      \n          </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.calculatedDiffs : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n";
},"useData":true});
templates['difference_tree'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "  <div class=\"parent_parent_container_div\" >\n    <span class=\"parent cntr_"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_ID : stack1), depth0))
    + " cntr\" data-container_id=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_ID : stack1), depth0))
    + "\" data-type=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_TYPE : stack1), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_NAME : stack1), depth0))
    + "</span>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.PRODUCTS : stack1),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CHILDREN : stack1),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "      <span class=\"child product prod_"
    + alias2(alias1((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + " prod\" data-product_id=\""
    + alias2(alias1((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.MODEL_NUMBER : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "            <div class=\"parent_child_container_div\">\n              <span class=\"child cntr_"
    + alias2(alias1((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + " cntr\" data-container_id=\""
    + alias2(alias1((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + "\" data-type=\""
    + alias2(alias1((depth0 != null ? depth0.CONTAINER_TYPE : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.CONTAINER_NAME : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.PRODUCTS : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.PRODUCTS : depth0),{"name":"each","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"6":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "                  <span class=\"child1 product prod_"
    + alias2(alias1((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + " prod\" data-product_id=\""
    + alias2(alias1((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.MODEL_NUMBER : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\n";
},"8":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias1).call(depth0,(depth0 != null ? depth0.treeType : depth0),"system",{"name":"ifCond","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias1).call(depth0,(depth0 != null ? depth0.treeType : depth0),"physical",{"name":"ifCond","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias1).call(depth0,(depth0 != null ? depth0.treeType : depth0),"verification system",{"name":"ifCond","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias1).call(depth0,(depth0 != null ? depth0.treeType : depth0),"verification physical",{"name":"ifCond","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  \n";
},"9":function(depth0,helpers,partials,data) {
    return "    <span class=\"new\">New to system</span>\n";
},"11":function(depth0,helpers,partials,data) {
    return "    <span class=\"missing\">Missing now</span>\n";
},"13":function(depth0,helpers,partials,data) {
    return "    <span class=\"\">--</span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.tree : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + " \n\n\n  ";
},"useData":true});
templates['difference_verification_task'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "<div class=\"verification_task_block \" id=\"verification_task_"
    + alias2(alias1((depth0 != null ? depth0.VERIFICATION_TASK_ID : depth0), depth0))
    + "\">\n	<input type=\"hidden\" class=\"verification_task_id_value\" value=\""
    + alias2(alias1((depth0 != null ? depth0.VERIFICATION_TASK_ID : depth0), depth0))
    + "\"/>\n	<input type=\"hidden\" class=\"verification_task_instance_id_value\" value=\""
    + alias2(alias1((depth0 != null ? depth0.INSTANCE_ID : depth0), depth0))
    + "\"/>\n	<h3>"
    + alias2(alias1((depth0 != null ? depth0.SERIAL_NUMBER : depth0), depth0))
    + " Verification Task: "
    + alias2(alias1((depth0 != null ? depth0.CODE : depth0), depth0))
    + "</h3>\n	<div class=\"verification_task_block_info\">\n		<div class=\"div-table\">\n			<div class=\"div-table-row\">\n				<div class=\"div-table-cell\"><b>Task Status:</b>&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.STATUS_STRING : depth0), depth0))
    + "</div>\n				<div class=\"div-table-cell\"><b>Assigned To:</b>&nbsp;<span class=\"v_assigned_user_name\">"
    + alias2(alias1((depth0 != null ? depth0.EMPLOYE_NAME : depth0), depth0))
    + "</span></div>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias3).call(depth0,(depth0 != null ? depth0.STATUS : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n			<div class=\"div-table-row\">\n				<div class=\"div-table-cell\"><b>Task Priority:</b>&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.PRIORITY_STRING : depth0), depth0))
    + "</div>\n				<div class=\"div-table-cell\"><b>End Before:</b>&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "</div>\n			</div>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias3).call(depth0,(depth0 != null ? depth0.STATUS : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "			\n		</div>\n	</div>	\n	<div class=\"verification_task_reassign_block\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SHOW_TREE : depth0),{"name":"if","hash":{},"fn":this.program(10, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		<div id=\"product_count_difference_table\"></div>\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias3).call(depth0,(depth0 != null ? depth0.MAIN_DIFFERENCE_STATUS : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(12, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.MAIN_DIFFERENCE_STATUS : depth0),1,{"name":"ifCond","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"3":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "					<div class=\"div-table-cell reassign_btn_cell\">\n						<input type=\"hidden\" class=\"verification_task_start_date\" value=\""
    + alias2(alias1((depth0 != null ? depth0.START_DATE : depth0), depth0))
    + "\"/>\n						<input type=\"hidden\" class=\"verification_task_end_date\" value=\""
    + alias2(alias1((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "\"/>\n						<button type=\"button\" class=\"verification_task_reassign btn btn-default btn-sm\" title=\"Reassign\" data-verification_task_id=\""
    + alias2(alias1((depth0 != null ? depth0.VERIFICATION_TASK_ID : depth0), depth0))
    + "\">Reassign</button>\n					</div>\n";
},"5":function(depth0,helpers,partials,data) {
    return "";
},"7":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "				<div class=\"div-table-row\">\n					<div class=\"div-table-cell\"><b>Scanned By:</b>&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.SCANNED_BY_NAME : depth0), depth0))
    + "</div>\n					<div class=\"div-table-cell\"><b>Scanned at:</b>&nbsp;"
    + alias2(alias1((depth0 != null ? depth0.SCANNED_AT : depth0), depth0))
    + "</div>\n				</div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.RESULT_STRING : depth0),{"name":"if","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"8":function(depth0,helpers,partials,data) {
    var helper;

  return "				<div class=\"div-table-row\">\n					<div class=\"div-table-cell verification_result\"><b>Result:</b>&nbsp;Verification <b>"
    + this.escapeExpression(((helper = (helper = helpers.RESULT_STRING || (depth0 != null ? depth0.RESULT_STRING : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"RESULT_STRING","hash":{},"data":data}) : helper)))
    + "</b> </div>\n				</div>\n";
},"10":function(depth0,helpers,partials,data) {
    return "		<div id=\"container_count_dif_div\">\n			\n			<div class=\"system_physical_div\">\n      			<div class=\"system_div\">\n      				<h5>System View</h5>\n      			</div>\n      			<div class=\"alert alert-info info_div\"></div>\n      			<div class=\"physical_div\">\n        			<h5>Physical View</h5>\n        		</div>\n        		<div class=\"clear\"></div>\n        	</div>\n        	\n		</div>\n";
},"12":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SHOW_APPROVE : depth0),{"name":"if","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"13":function(depth0,helpers,partials,data) {
    return "			    <div class=\"approval_div\">\n			          <div class=\"comments_div\">\n				          <input type=\"hidden\" name=\"verification_instance_id\" value=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.INSTANCE_ID : depth0), depth0))
    + "\"/>\n				          <input type=\"hidden\" name=\"type\" value=\"2\"/>\n			              <label class=\"required\">Comments</label>\n			              <textarea  name=\"comments\"></textarea>\n			              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n			          </div>\n			          <div class=\"approve_btn_div\">\n				          <a name=\"verification_approve_btn\" class=\"btn btn-success\" title=\"Approve\">\n				            <span class=\"glyphicon glyphicon-ok\"></span> Approve</a>\n				      </div>\n			    </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['filter_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "\n<div id=\"filter_btn_div\">\n	\n	<button id=\"filter_btn\" class=\"btn btn-info\" type=\"button\" title=\"Filter\" >\n	<span class=\"glyphicon glyphicon-search\"></span>\n		Search</button>\n	<button id=\"diff_clear_btn\" class=\"btn btn-info\" type=\"button\" title=\"Clear\" >Clear</button>\n	<input type=\"hidden\" id=\"filtered_status\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_area_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_location_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_storage_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_title_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_product_line_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_product_cat_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_model_number_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_lot_number_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_lp_type_id\" value=\"0\" />\n	<input type=\"hidden\" id=\"filtered_scanned_by\" value=\"0\" />\n	<button id=\"filter_btn_bk\" class=\"btn btn-default\" type=\"button\" title=\"Filter\" style=\"display:none;\">Filter</button>\n</div>\n";
},"useData":true});
templates['immybox_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"input-group-addon\" id=\""
    + alias3(((helper = (helper = helpers.divId || (depth0 != null ? depth0.divId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"divId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div><input type=\"hidden\" id=\""
    + alias3(((helper = (helper = helpers.selectedInput || (depth0 != null ? depth0.selectedInput : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"selectedInput","hash":{},"data":data}) : helper)))
    + "\"/>\n<input type=\"text\" id=\""
    + alias3(((helper = (helper = helpers.inputId || (depth0 != null ? depth0.inputId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"inputId","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control\"/>\n";
},"useData":true});
templates['location_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  <div id=\"location_differences_scroll_container\">\n    <div class=\"panner scroll_up\" data-scroll-modifier='-1'>&nbsp;</div>\n    <div id=\"location_differences_scroll\">\n      \n      <div id=\"location_differences\">\n        <div class=\"tabbable tabs-left\">\n          <ul class=\"nav nav-tabs\" id=\"locationTabs\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "          </ul>\n        </div>\n      </div>\n    </div>\n    <div class=\"panner scroll_down\" data-scroll-modifier='1'>&nbsp;</div>\n  </div>\n  <div id=\"container_differences\">  \n    <div class=\"tab-content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n  </div>\n\n\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "              <li id=\""
    + alias2(alias1((depth0 != null ? depth0.SAL_ID : depth0), depth0))
    + "\">\n                <a href=\"#"
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + alias2(alias1((depth0 != null ? depth0.SAA_ID : depth0), depth0))
    + "\">\n                  "
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\n                  <span class=\"\" name=\""
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\n                </a>\n              </li>\n";
},"4":function(depth0,helpers,partials,data) {
    return "        \n        <div class=\"tab-pane\" id=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\">\n            \n        </div>\n        \n";
},"6":function(depth0,helpers,partials,data) {
    return "\n    <div class=\"no_data\">\n       No Data\n    </div>\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"back_btn_div\">\n<button id=\"back_btn\" type=\"button\" class=\"btn btn-default\">Back</button>\n</div>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['location_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\">\n<div class=\"input-group-addon\" >Location</div><input id=\"selected_location_id\" type=\"hidden\" value=\"0\"/>\n<input id='location_list_box' type='text' class='form-control' />\n</div>\n\n";
},"useData":true});
templates['status_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\">\n<div class=\"input-group-addon\" >Status</div><input id=\"selected_status\" type=\"hidden\" value=\"0\"/>\n<input id='status_select' type='text' class='form-control' />\n</div>\n\n\n";
},"useData":true});
templates['storage_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\">\n	<div class=\"input-group-addon\">Warehouse</div><input id=\"selected_storage_id\" type=\"hidden\" value=\"0\"/>\n	<input id='storage_list_box' type='text' class='form-control' />\n	\n</div>";
},"useData":true});
templates['title_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group\">\n<div class=\"input-group-addon\" >Title</div><input id=\"selected_title_id\" type=\"hidden\" value=\"0\"/>\n<input id='title_list_box' type='text' class='form-control' />\n</div>\n\n\n";
},"useData":true});
return templates;
});