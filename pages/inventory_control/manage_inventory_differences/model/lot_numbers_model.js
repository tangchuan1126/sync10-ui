"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  var LotImmyModel = Backbone.Model.extend({
          parse:function(data){
            if(data!=""){
              this.text=data;
              this.value = data;
              return this;  
            }
            
          }

        
       });
       
      var LotImmyCollection =  Backbone.Collection.extend({
           model: LotImmyCollection,
           url: config.getLotNumbersJSON,
           
       });

      return {
	      LotImmyModel:LotImmyModel,
	      LotImmyCollection:LotImmyCollection,

	    };




}); //page_init