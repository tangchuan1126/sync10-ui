"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var PLImmyModel = Backbone.Model.extend({
          idAttribute: "ID",

          parse: function(data){
            
            this.text = data.NAME ;
            this.value = data.ID;
            return this;
          }
        
       });
       
      var PLImmyCollection =  Backbone.Collection.extend({
           model: PLImmyModel,
           url: config.getProductLineJSON,
           
       });

      return {
	      PLImmyModel:PLImmyModel,
	      PLImmyCollection:PLImmyCollection,

	    };




}); //page_init