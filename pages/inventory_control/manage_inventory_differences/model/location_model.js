"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var LocationImmyModel = Backbone.Model.extend({
          idAttribute: "SLC_ID",

          parse: function(data){
            
            this.text = data.SLC_POSITION ;
            this.value = data.SLC_ID;
            return this;
          }
        
       });
       
      var LocationImmyCollection =  Backbone.Collection.extend({
           model: LocationImmyModel,
           url: config.getLocationsOfArea,

           initialize: function(area_id){
              this.url = this.url+area_id;
          }

        
       });

      return {
	      LocationImmyModel:LocationImmyModel,
	      LocationImmyCollection:LocationImmyCollection,

	    };




}); //page_init