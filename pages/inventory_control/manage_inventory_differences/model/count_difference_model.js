"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      
      var SystemContainerTree = Backbone.Model.extend({
          idAttribute: "CONTAINER_ID",
          

          parse:function(data){
            var dis = this;
            return this.formatContainerNameInTree(data);
          },
          formatContainerNameInTree:function(tree){
            var dis = this;
            if(tree){
              if(tree.CONTAINER_NAME){
                tree.CONTAINER_NAME = tree.CONTAINER_TYPE==1? "CLP "+tree.CONTAINER_NAME : "TLP "+tree.CONTAINER_NAME;
              }
              if(tree.CHILDREN){
                $.each(tree.CHILDREN,function(index,item){
                    item.CONTAINER_NAME = item.CONTAINER_TYPE==1? "CLP "+item.CONTAINER_NAME : "TLP "+item.CONTAINER_NAME;
                });
              }
            }
            return tree;
          },
          
       });
      var PhysicalContainerTree = Backbone.Model.extend({
          idAttribute: "CONTAINER_ID",
          parse:function(data){
            var dis = this;
            return this.formatContainerNameInTree(data);
          },
          formatContainerNameInTree:function(tree){
            var dis = this;
            if(tree){
              if(tree.CONTAINER_NAME){
                tree.CONTAINER_NAME = tree.CONTAINER_TYPE==1? "CLP "+tree.CONTAINER_NAME : "TLP "+tree.CONTAINER_NAME;
              }
              if(tree.CHILDREN){
                $.each(tree.CHILDREN,function(index,item){
                    item.CONTAINER_NAME = item.CONTAINER_TYPE==1? "CLP "+item.CONTAINER_NAME : "TLP "+item.CONTAINER_NAME;
                });
              }
            }
            return tree;
          },
       });
      var ProductModel = Backbone.Model.extend({
          idAttribute: "PC_ID",
          
       });
      var ProductCollection =  Backbone.Collection.extend({
           model: ProductModel,

      });
      var ContainerDifferenceModel = Backbone.Model.extend({
          idAttribute: "ID",
          parse:function(data){
              if(data.SYSTEM_TREE){
                this.set("systemContainerTree",new SystemContainerTree(data.SYSTEM_TREE,{parse:true}));
                var col = new ProductCollection();
                col = this.countProducts(data.SYSTEM_TREE,col);
                this.set("systemProducts",col);
              }
              if(data.PHYSICAL_TREE){
                 this.set("physicalContainerTree" , new PhysicalContainerTree(data.PHYSICAL_TREE,{parse:true})); 
                 var col = new ProductCollection();
                  col = this.countProducts(data.PHYSICAL_TREE,col);   
                  this.set("physicalProducts",col) 
              }
              this.set("STATUS",data.STATUS);
              this.set("USERNAME",data.USER_NAME);
              this.set("ID",data.ID);
              
              this.set("COMMENTS",data.COMMENTS);
              this.set("PROCESSED_BY_NAME",data.PROCESSED_BY_NAME);
              if(data.STATUS == 3){
                this.set("PROCESSED_TITLE","Rejected By:");
              }
              if(data.STATUS == 2){
                this.set("PROCESSED_TITLE","Approved By:");
              }
          },

       });
      
       
       var ChildrenModel = Backbone.Model.extend({
          idAttribute: "ID",

       });
      var ChildrenCollection =  Backbone.Collection.extend({
           model: ChildrenModel,
      });
      var ContainerModel = Backbone.Model.extend({
          idAttribute: "ID",
          parse:function(data){
            var dis = this;
            
              if(data.SYSTEM_TREE){
                this.set("systemContainerTree",new SystemContainerTree(data.SYSTEM_TREE,{parse:true}));
                var col = new ProductCollection();
                col = this.countProducts(data.SYSTEM_TREE,col);
                this.set("systemProducts",col);
              }
              if(data.PHYSICAL_TREE){
                 this.set("physicalContainerTree" , new PhysicalContainerTree(data.PHYSICAL_TREE,{parse:true})); 
                 var col = new ProductCollection();
                  col = this.countProducts(data.PHYSICAL_TREE,col);   
                  this.set("physicalProducts",col) 
              }

              this.set("CONTAINER_ID",data.CONTAINER_ID);
              //this.set("CONTAINER",data.CONTAINER);
              this.set("ID",data.ID);
              this.set("WAREHOUSE_ID",data.WAREHOUSE_ID);
              this.set("CONTAINER",data.LP_CONTAINER);
              this.set("LP_CONTAINER",data.LP_CONTAINER);
              this.set("TITLE_NAME",data.TITLE_NAME);
              this.set("SCANNED_BY_NAME",data.SCANNED_BY_NAME);
              this.set("REVIEWED_BY_NAME",data.REVIEWED_BY_NAME);
              this.set("AREA_ID",data.AREA_ID);
              this.set("AREA_NAME",data.AREA_NAME);
              this.set("LOCATION_ID",data.LOCATION_ID);
              this.set("LOCATION_NAME",data.LOCATION_NAME);
              this.set("LOT_NUMBER",data.LOT_NUMBER);
              this.set("STATUS",data.STATUS);
              this.set("IS_VERIFICATION_TASK_CREATABLE",data.IS_VERIFICATION_TASK_CREATABLE);
              if(data.VERIFICATION_TASKS){
                this.set("VERIFICATION_TASKS",dis.formatVerificationTask(data));
              }
              this.set("CREATE_DIFFERENCE",dis.createDifference(this.get("systemProducts"),this.get("physicalProducts")));
              this.set("COMMENTS",data.COMMENTS);
              this.set("PROCESSED_BY_NAME",data.PROCESSED_BY_NAME);
              if(data.STATUS == 3){
                this.set("PROCESSED_TITLE","Rejected By:");
              }
              if(data.STATUS == 2){
                this.set("PROCESSED_TITLE","Approved By:");
              }
              
          },
          formatVerificationTask:function(data){
            var dis = this;
            var verification_tasks = [];
            if(data.VERIFICATION_TASKS){
              $.each(data.VERIFICATION_TASKS, function(index,item){
                var verification = item;
                verification.SHOW_TREE = false;
                switch(item.PRIORITY){
                  case 1:
                    verification.PRIORITY_STRING="Low";
                    break;
                  case 2:
                    verification.PRIORITY_STRING = "Medium";
                    break;
                  case 3:
                    verification.PRIORITY_STRING = "High";
                    break;
                }

                switch(item.STATUS){
                  case 1:
                    verification.STATUS_STRING= "New";
                    verification.SHOW_APPROVE = false;
                    break;
                  case 3:
                    verification.STATUS_STRING="Done";
                    verification.SHOW_APPROVE = true;
                    break;
                  case 5:
                    verification.STATUS_STRING="Reviewed";
                    verification.SHOW_APPROVE = false;
                    break;
                }
                
                switch(item.RESULT){
                  case 1:
                    verification.RESULT_STRING= "PASSED";
                    verification.SHOW_TREE = true;
                    break;
                  case 2:
                    verification.SHOW_TREE = true;
                    verification.RESULT_STRING="FAILED";
                    break;

                }
                verification.MAIN_DIFFERENCE_STATUS = data.STATUS;
                
                if(item.SYSTEM_TREE){
                  
                  verification.systemContainerTree = dis.formatContainerNameInTree(item.SYSTEM_TREE);
                  var col = new ProductCollection();
                  col = dis.countProducts(item.SYSTEM_TREE,col);
                  verification.systemProducts= col
                }
                if(item.PHYSICAL_TREE){
                  verification.physicalContainerTree = dis.formatContainerNameInTree(item.PHYSICAL_TREE); 
                  var col = new ProductCollection();
                  col = dis.countProducts(item.PHYSICAL_TREE,col);   
                  verification.physicalProducts = col; 
                }

                verification.CREATE_DIFFERENCE = dis.createDifference(verification.systemProducts,verification.physicalProducts);
                verification.SERIAL_NUMBER = verification_tasks.length+1;
                verification_tasks.unshift(verification);

              });
            }
            
            return verification_tasks;
          },

          countProducts:function(tree,col){
            var dis = this;
            if(tree.PC_ID){
              if(col.get(tree.PC_ID)){
                var prod = col.remove(tree.PC_ID);
                prod.set("QUANTITY",prod.get("QUANTITY")+tree.QUANTITY);
                col.add(prod);
              }else{
                var prod = new ProductModel();
                prod.set("PC_ID",tree.PC_ID);
                prod.set("MODEL_NUMBER",tree.MODEL_NUMBER);
                prod.set("QUANTITY",tree.QUANTITY);
                col.add(prod);
              }
              
            }
              if(tree.CHILDREN){
                $.each(tree.CHILDREN, function(index, item){
                  dis.countProducts(item, col);
                });
              }
            
            return col;
          },
          formatContainerNameInTree:function(tree){
            var dis = this;
            if(tree){
              if(tree.CONTAINER_NAME){
                tree.CONTAINER_NAME = tree.CONTAINER_TYPE==1? "CLP "+tree.CONTAINER_NAME : "TLP "+tree.CONTAINER_NAME;
              }
              if(tree.CHILDREN){
                $.each(tree.CHILDREN,function(index,item){
                    item.CONTAINER_NAME = item.CONTAINER_TYPE==1? "CLP "+item.CONTAINER_NAME : "TLP "+item.CONTAINER_NAME;
                });
              }
            }
            return tree;
          },
          createDifference:function(sysProds,phyProds){
            var col = new ProductCollection();
            /*var sysProds = this.get("systemProducts");
            var phyProds = this.get("physicalProducts");*/
            if(sysProds && phyProds){
              $.each(sysProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("MODEL_NUMBER",item.get("MODEL_NUMBER"));
                p.set("systemCount",item.get("QUANTITY"));
                if(phyProds.get(item.get("PC_ID"))){
                  if(item.get("QUANTITY") == phyProds.get(item.get("PC_ID")).get("QUANTITY")){

                  }else{
                      p.set("physicalCount",phyProds.get(item.get("PC_ID")).get("QUANTITY"));
                      p.set("differenceCount",parseInt(phyProds.get(item.get("PC_ID")).get("QUANTITY")) 
                        - parseInt(item.get("QUANTITY")));
                      col.add(p);
                  }
                  phyProds.remove(item.get("PC_ID"))
                  
                }else{
                  p.set("physicalCount",0);
                  col.add(p);
                }
                
              });
              $.each(phyProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("MODEL_NUMBER",item.get("MODEL_NUMBER"));
                p.set("systemCount",0);
                p.set("physicalCount",item.get("QUANTITY"));
                p.set("differenceCount",parseInt(item.get("QUANTITY")));
                col.add(p);
              });
            }else if(sysProds){
              $.each(sysProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("MODEL_NUMBER",item.get("MODEL_NUMBER"));
                p.set("physicalCount",0);
                p.set("systemCount",item.get("QUANTITY"));
                p.set("differenceCount",0-parseInt(item.get("QUANTITY")));
                col.add(p);
              });
            }else if(phyProds){
              $.each(phyProds.models, function(index,item){
                var p = new ProductModel();
                p.set("PC_ID",item.get("PC_ID"));
                p.set("MODEL_NUMBER",item.get("MODEL_NUMBER"));
                p.set("systemCount",0);
                p.set("physicalCount",item.get("QUANTITY"));
                 p.set("differenceCount",parseInt(item.get("QUANTITY")));
                col.add(p);
              });
            }else{
              return null;
            }
            
            return col;
          }
       });
      
      var DifferenceCollection =  Backbone.Collection.extend({
           model: ContainerModel,
           url: config.getDifferenceDetail,
           
           initialize: function(data){
              this.url = this.url+"?difference_id="+data.differenceId;
          },
          
       });

      return {
       
        DifferenceCollection:DifferenceCollection,

      };




}); //page_init