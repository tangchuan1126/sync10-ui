"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "/Sync10-ui/lib/DateUtil.js"
  
], function(config, $, Backbone, Handlebars,date) {

		  
      
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "ID",
          percentage_completed: 0,

          parse:function(data){

            var dateString = data.SCANNED_AT;
            var date = dateString.replace(/-/g, "/").split(".")[0];
            data["SCANNED_AT"] = new Date(date).format("yyyy-MM-dd");
            if(data.PROCESSED_AT){
              dateString = data.REVIEWED_AT;
              date = dateString.replace(/-/g, "/").split(".")[0];
              data["REVIEWED_AT"] = new Date(date).format("yyyy-MM-dd");
            }
            
            
            if(!data["TITLE_NAME"]){data["TITLE_NAME"]="--";}
            switch(data.STATUS){
              case 1:
                data["STATUS_STRING"]="Pending";
                data["REVIEWED_AT"] = "--";
                data["PROCESSED_BY_NAME"] = "--"
                break;
              case 2:
                data["STATUS_STRING"] = "Approved";
                break;
              case 3:
                data["STATUS_STRING"] = "Rejected";
                break;

            }

            switch(data.VERIFICATION_INSTANCE_STATUS){
              case 1:
                data["VERIFICATION_STATUS"]="New";
                break;
              case 2:
                data["VERIFICATION_STATUS"] = "In-Progress";
                break;
              case 3:
                data["VERIFICATION_STATUS"] = "Done";
                break;
              default:
                data["VERIFICATION_STATUS"] = "--";
                break;

            }
            return data;
          }
       });
       
      var DifferenceCollection =  Backbone.Collection.extend({
         model: DifferenceModel,
         url: config.getAllDifferences,
          ps_id:0,
          area_id:0,
          location_id:0,
          approve_status:0,
          sortby:"id",
          sort:"desc",
          pageNo:1,
          pageSize:10,
          title_id:0,
          product_line:0,
          category:0,
          model_number:0,
          lot_number:0,
          lp_type:0,
          scanned_by:0,

           initialize: function(params){
            
            
            if(params.ps_id){this.ps_id=parseInt(params.ps_id);}
            if(params.area_id){this.area_id=parseInt(params.area_id)}
            if(params.location_id){this.location_id=parseInt(params.location_id)}
            if(params.status_id){this.approve_status=parseInt(params.status_id)}
            if(params.sortby){this.sortby=params.sortby}
            if(params.sort){this.sort=params.sort}
            if(params.pageNo){this.pageNo=parseInt(params.pageNo)}
            if(params.pageSize){this.pageSize=parseInt(params.pageSize)}
            if(params.title_id){this.title_id=parseInt(params.title_id)}

            if(params.product_line){this.product_line=parseInt(params.product_line)}
            if(params.category){this.category=parseInt(params.category)}
            if(params.model_number){this.model_number=params.model_number}
            if(params.lot_number){this.lot_number=params.lot_number}
            if(params.lp_type){this.lp_type=parseInt(params.lp_type)}
            if(params.scanned_by){this.scanned_by=parseInt(params.scanned_by)}

            this.url = this.url+"?warehouse_id="+this.ps_id+"&area_id="+this.area_id+"&location_id="+this.location_id+
            "&status="+this.approve_status+"&sort_by="+this.sortby+"&title_id="+this.title_id+
            "&product_line="+this.product_line+"&product_category="+this.category;
            if(this.model_number!=0){ this.url += "&model_number="+this.model_number}
            if(this.lot_number!=0){this.url += "&lot_number="+this.lot_number}
            
            this.url += "&lp_type="+this.lp_type+"&scanned_by="+this.scanned_by+
            "&sort_direction="+this.sort+"&pageNo="+this.pageNo+"&pageSize="+this.pageSize;
              
          },
          parse:function(response){
              
              if(response.PAGECTRL){this.pageCtrl = response.PAGECTRL;}
              return response.DATA;
              //return response;
            }
          },
            {
              pageCtrl:{
                pageNo:1,
                pageSize:10
          },
    
       });

      return {
	      DifferenceModel:DifferenceModel,
	      DifferenceCollection:DifferenceCollection,

	    };




}); //page_init