"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var LPTImmyModel = Backbone.Model.extend({
          idAttribute: "LPT_ID",

          parse: function(data){
            
            this.text = data.LP_NAME;//data.LENGTH+"*"+data.WIDTH+"*"+data.HEIGHT ;
            this.value = data.LPT_ID;
            return this;
          }
        
       });
       
      var LPTImmyCollection =  Backbone.Collection.extend({
           model: LPTImmyModel,
           url: config.getLPTypeJSON,
           
       });

      return {
	      LPTImmyModel:LPTImmyModel,
	      LPTImmyCollection:LPTImmyCollection,

	    };




}); //page_init