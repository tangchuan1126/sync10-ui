"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var MNImmyModel = Backbone.Model.extend({
          parse:function(data){
            this.text=data;
            this.value = data;
            return this;
          }

        
       });
       
      var MNImmyCollection =  Backbone.Collection.extend({
           model: MNImmyModel,
           url: config.getModelNumbersJSON,
           
       });

      return {
	      MNImmyModel:MNImmyModel,
	      MNImmyCollection:MNImmyCollection,

	    };




}); //page_init