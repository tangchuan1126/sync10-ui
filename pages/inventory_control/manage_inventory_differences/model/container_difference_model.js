"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      
      
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "SAC_ID",
          parse:function(data){
            
            if(data["APPROVE_STATUS"] == 1){
              data["pending"] = true;
              
            }else{
              data["pending"] = false;
            }
            
            return data;
          }
       });
       
      var DifferenceCollection =  Backbone.Collection.extend({
           model: DifferenceModel,
           url: config.getContainerDifferencesJSON,
           selected:"",
           
           initialize: function(sal_id){
            
              this.url = this.url+"?sal_id="+sal_id;
              
          }

       });

      return {
        DifferenceModel:DifferenceModel,
        DifferenceCollection:DifferenceCollection,

      };




}); //page_init