"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var PCImmyModel = Backbone.Model.extend({
          idAttribute: "ID",

          parse: function(data){
            
            this.text = data.TITLE ;
            this.value = data.ID;
            return this;
          }
        
       });
       
      var PCImmyCollection =  Backbone.Collection.extend({
           model: PCImmyModel,
           url: config.getProductCategoryJSON,
           
       });

      return {
	      PCImmyModel:PCImmyModel,
	      PCImmyCollection:PCImmyCollection,

	    };




}); //page_init