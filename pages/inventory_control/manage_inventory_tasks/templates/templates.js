(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"instance_area_table\" id=\""
    + escapeExpression(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"instanceId","hash":{},"data":data}) : helper)))
    + "_area_table\" data-instanceId=\""
    + escapeExpression(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"instanceId","hash":{},"data":data}) : helper)))
    + "\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.repeating : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "	<table  class=\"table table-striped\">\r\n		<thead>\r\n			<th>Area Name</th>\r\n			<th>Status</th>\r\n			<th>Progress</th>\r\n			<th></th>\r\n		</thead>\r\n		<tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.areas : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</tbody>\r\n	</table>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  return "		<div class=\"instances_areas_table_back_div\">\r\n			<a href=\"#\" class=\"instances_areas_table_back btn btn-default btn-sm\">Back to Instances</a>\r\n		</div>\r\n";
  },"4":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "		\r\n			<tr>\r\n				<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\r\n				<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\r\n				\r\n					<td>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.SHOW_PROGRESS : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "					</td>\r\n				\r\n				<td>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.SHOW_DETAIL : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "				</td>\r\n			</tr>\r\n		\r\n";
},"5":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "						<div class=\"custom_progress_box\">\r\n			             <div class=\"custom_progress\" style=\"background-position:"
    + escapeExpression(lambda((depth0 != null ? depth0.PERCENT_DONE : depth0), depth0))
    + "px 0px;\">\r\n			                "
    + escapeExpression(lambda((depth0 != null ? depth0.DONECOUNT : depth0), depth0))
    + " / "
    + escapeExpression(lambda((depth0 != null ? depth0.TOTALCOUNT : depth0), depth0))
    + "\r\n			             </div>\r\n			           </div>  \r\n";
},"7":function(depth0,helpers,partials,data) {
  return "			        --\r\n";
  },"9":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "					<a href=\"#\" class=\"area_view_detail btn btn-default btn-sm\" data-areaId=\""
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_ID : depth0), depth0))
    + "\" >View Detail</a>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.areas : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['create_task_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div  id=\"buttons-div\">\r\n	<a href=\"#\" id=\"create\" class=\"btn btn-warning\">\r\n	<span class=\"glyphicon glyphicon-plus\"></span> Create New Task</a>\r\n</div>";
  },"useData":true});
templates['immybox_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"input-group-addon\" id=\""
    + escapeExpression(((helper = (helper = helpers.divId || (depth0 != null ? depth0.divId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"divId","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</div><input type=\"hidden\" id=\""
    + escapeExpression(((helper = (helper = helpers.selectedInput || (depth0 != null ? depth0.selectedInput : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"selectedInput","hash":{},"data":data}) : helper)))
    + "\"/>\r\n<input type=\"text\" id=\""
    + escapeExpression(((helper = (helper = helpers.inputId || (depth0 != null ? depth0.inputId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"inputId","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control\"/>\r\n\r\n";
},"useData":true});
templates['instances_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n<table id=\"instances_table\" class=\"table table-striped\">\r\n	<thead>\r\n		<th>Instance</th>\r\n		<th>Start Date</th>\r\n		<th>End Date</th>\r\n		<th>Status</th>\r\n		<th></th>\r\n	</thead>\r\n	<tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.instances : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</tbody>\r\n</table>\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "	\r\n		<tr>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "</td>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.START_DATE : depth0), depth0))
    + "</td>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "</td>\r\n			<td >"
    + escapeExpression(lambda((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\r\n			<td><a href=\"#\" class=\"instance_link btn btn-default btn-sm\" data-value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">View Detail</a></td>\r\n		</tr>\r\n	\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.instances : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['progress_areas'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"div-table-cell\" id=\"areas_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">\r\n	<div class=\"panel panel-default\">\r\n		<div class=\"panel-heading header\">\r\n			<div>"
    + escapeExpression(((helper = (helper = helpers.areaName || (depth0 != null ? depth0.areaName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaName","hash":{},"data":data}) : helper)))
    + "</div>\r\n		</div>\r\n		<div class=\"body panel-body\">\r\n			<div class=\"locations_block\" id=\"locations_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">	\r\n			</div>\r\n\r\n		</div>\r\n	</div>\r\n</div>";
},"useData":true});
templates['progress_locations'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div id=\"location_"
    + escapeExpression(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationId","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.locationName || (depth0 != null ? depth0.locationName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationName","hash":{},"data":data}) : helper)))
    + "\r\n</div>";
},"useData":true});
templates['progress_task'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  					<td class=\"labele\">Product Line</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_LINE_NAME : stack1), depth0))
    + "</td>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <td class=\"labele\">Category</td>\r\n            <td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_CATEGORY_NAME : stack1), depth0))
    + "</td>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  						<td class=\"labele\">End Date</td>\r\n  						<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1), depth0))
    + "</td>\r\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  						<td class=\"labele\">End After</td>\r\n  						<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_OCCURRENCE : stack1), depth0))
    + " Occurrences</td>\r\n";
},"9":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <td class=\"labele\">Status</td>\r\n            <td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STATUS : stack1), depth0))
    + "</td>\r\n";
},"11":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <td class=\"labele\">LP Type</td>\r\n            <td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LP_TYPE_NAME : stack1), depth0))
    + "</td>\r\n";
},"13":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "          <tr>\r\n            <td class=\"labele\">Title</td>\r\n            <td class=\"val\" colspan=\"4\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLE : stack1), depth0))
    + "</td>\r\n          </tr>\r\n";
},"15":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "          <tr>\r\n            <td class=\"labele\">Model #</td>\r\n            <td class=\"val\" colspan=\"4\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.MODELS : stack1), depth0))
    + "</td>\r\n          </tr>\r\n";
},"17":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "          <tr>\r\n            <td class=\"labele\">Lot #</td>\r\n            <td class=\"val\" colspan=\"4\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LOTS : stack1), depth0))
    + "</td>\r\n          </tr>\r\n";
},"19":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  				<tr>\r\n  					<td class=\"labele\">Repeat By</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_BY : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Repeat Every</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_EVERY : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Duration</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.DURATION : stack1), depth0))
    + "</td>\r\n  					\r\n  				</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<div class=\"panel panel-default\">\r\n  	<div id=\"progress_task_header\" class=\"panel-heading\">\r\n      <div class=\"left\">Task Progress</div>\r\n      <div id=\"progress-back-buttons-div\"><a href=\"#\" id=\"task_progress_back\" class=\"btn btn-warning\">\r\n      <span class=\"glyphicon glyphicon-list-alt\"></span> Manage Tasks List</a></div>\r\n      <div class=\"clear\"></div>\r\n  	</div>\r\n  	<div class=\"panel-body\">\r\n  			<table class=\"info_table\">\r\n  				<tr>\r\n  					<td class=\"labele\">Warehouse</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.WAREHOUSE_NAME : stack1), depth0))
    + "</td>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_LINE_NAME : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_CATEGORY_NAME : stack1), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "  					<td class=\"labele\">Created By</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.CREATED_BY_NAME : stack1), depth0))
    + "</td>\r\n  				</tr>\r\n  				<tr >\r\n  					<td class=\"labele\">Repeating</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.IS_REPEAT : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Type</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\r\n  					<td class=\"labele\">Start Date</td>\r\n  					<td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STARTS_ON : stack1), depth0))
    + "</td>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "  				</tr>\r\n          <tr>\r\n            <td class=\"labele\">Priority</td>\r\n            <td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRIORITY : stack1), depth0))
    + "</td>\r\n            <td class=\"labele\">Code</td>\r\n            <td class=\"val\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.CODE : stack1), depth0))
    + "</td>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STATUS : stack1), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LP_TYPE : stack1), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </tr>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLES : stack1), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.MODEL_NUMBERS : stack1), {"name":"if","hash":{},"fn":this.program(15, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LOT_NUMBERS : stack1), {"name":"if","hash":{},"fn":this.program(17, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.repeating : depth0), {"name":"if","hash":{},"fn":this.program(19, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "  			</table>\r\n        <div id=\"progress_user_area_location_header\">\r\n        <span class=\"glyphicon glyphicon-plus\"></span> Area / Location Assignments</div>	\r\n        <div id=\"progress_user_area_location\"></div>\r\n\r\n			<div id=\"second_level_progress_wrapper\">\r\n  			<div id=\"second_level_progress\">\r\n  				<div id=\"instances_progress\"></div>\r\n  				<div id=\"instances_area_progress\"></div>\r\n  				<div id=\"location_differences_container\"></div>\r\n          <div class=\"clear\"></div>\r\n  			</div>\r\n      </div>\r\n\r\n	</div>\r\n</div>";
},"useData":true});
templates['progress_users'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"div-table-row\" id=\"users_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\" data-user-id=\""
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\">\r\n	<div class=\"div-table-cell user_name\">"
    + escapeExpression(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userName","hash":{},"data":data}) : helper)))
    + "</div>\r\n	<div class=\"div-table-cell areas\" >\r\n		<div class=\"div-table\">\r\n			<div class=\"div-table-row\" id=\"areas_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n		</div>\r\n	</div>\r\n</div>\r\n	\r\n";
},"useData":true});
templates['reassign_areas'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"div-table-cell\" id=\"reassign_areas_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\" data-area_id=\""
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">\r\n	<div class=\"panel panel-default\">\r\n		<div class=\"panel-heading header\">\r\n			<input type=\"checkbox\" name=\"area_selection_checkbox\" id=\"select_all_loc_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n			<span>"
    + escapeExpression(((helper = (helper = helpers.areaName || (depth0 != null ? depth0.areaName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaName","hash":{},"data":data}) : helper)))
    + "</span>\r\n		</div>\r\n		<div class=\"body panel-body\">\r\n			<div class=\"locations_block\" >\r\n				<ul id=\"reassign_locations_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\" class=\"list-unstyled\">\r\n				</ul>	\r\n			</div>\r\n\r\n		</div>\r\n	</div>\r\n</div>";
},"useData":true});
templates['reassign_locations'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n<li id=\"reassign_location_"
    + escapeExpression(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationId","hash":{},"data":data}) : helper)))
    + "\" class=\"assigned\">\r\n	<label>\r\n		<input type=\"checkbox\" data-location_id=\""
    + escapeExpression(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationId","hash":{},"data":data}) : helper)))
    + "\" id=\"loc_"
    + escapeExpression(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<span>"
    + escapeExpression(((helper = (helper = helpers.locationName || (depth0 != null ? depth0.locationName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationName","hash":{},"data":data}) : helper)))
    + "</span>\r\n		<div class='assigned_user' data-assigned_user_id=\""
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\" id=\"reassign_location_user_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + escapeExpression(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userName","hash":{},"data":data}) : helper)))
    + "\">&nbsp;</div>\r\n		\r\n	</label>\r\n	\r\n</li>";
},"useData":true});
templates['reassign_users'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n	<div class=\"employee_box\">\r\n		<input type=\"checkbox\" id=\"employee_box_checkbox_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\" data-user_id=\""
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n		<span class=\"\" id=\"employee_box_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userName","hash":{},"data":data}) : helper)))
    + "</span>\r\n	</div>\r\n	\r\n";
},"useData":true});
templates['reassign_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "					<div class=\"body panel-body\">\r\n						<div class=\"employee_box_div\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.users : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "						</div>\r\n						<div class=\"clear\"></div>\r\n					</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "							<div class=\"employee_box\" >\r\n								<div class=\"employee_name\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.user_id : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.user_name : depth0), depth0))
    + "</div>\r\n								<div class=\"count\">"
    + escapeExpression(lambda((depth0 != null ? depth0.count : depth0), depth0))
    + "</div>\r\n								<div class=\"clear\"></div>\r\n							</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"reassign_view_table_wrapper\">\r\n	<div class=\"div-table\" id=\"reassign_view_table\">\r\n		<div class=\"div-table-row\">\r\n\r\n			<div class=\"div-table-cell left_side\">\r\n				<div id=\"reassign_view_area_locations\">\r\n					<div id=\"reassign_view_users_name_list\">\r\n						<div class=\"employee_box\" id=\"reassign_select_all_users_div\">\r\n							<input type=\"checkbox\" id=\"reassign_select_all_users\"/>\r\n							<span>Select All</span>\r\n						</div>\r\n						\r\n					</div>\r\n					<div id=\"reassign_assign_user_button_div\">\r\n						<a class=\"btn btn-primary disabled\" id=\"reassign_assign_user_button\">Reassign User</a>\r\n						<div class=\"clear\"></div>\r\n					</div>\r\n					<div id=\"reassign_view_areas_place\"></div>\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"div-table-cell right_side\">\r\n				<div id=\"reassign_view_users_side\"  class=\"panel panel-default\">\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "					<div class=\"panel-footer\">\r\n						<a  id=\"reassign_assign_user_ok\" class=\"btn btn-primary disabled\">\r\n						<span class=\"glyphicon glyphicon-ok\"></span> Ok</a>\r\n						<a  id=\"reassign_assign_user_cancel\" class=\"btn btn-warning\">\r\n						<span class=\"glyphicon glyphicon-cancel\"></span> Cancel</a>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<div class=\"clear\"></div>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n\r\n\r\n\r\n";
},"useData":true});
templates['search_panel'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"div-table\">\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_warehouse_list_view\">\r\n				\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_title_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_product_line_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_product_cat_list_view\"></div>\r\n		</div>\r\n		\r\n		\r\n	</div>\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_model_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_lot_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_lp_type_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_task_number_div\">\r\n				<div class=\"input-group-addon\">Task Code</div><input type=\"hidden\" id=\"search_selected_task_number\"/>\r\n				<input type=\"text\" id=\"search_task_number\" class=\"form-control\" maxlength=\"30\"/>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_priority_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_user_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_created_by_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_type_list_view\"></div>\r\n		</div>\r\n		\r\n		\r\n	</div>\r\n	\r\n	<div class=\"div-table-row\">\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_repeat_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_status_list_view\"></div>\r\n		</div>\r\n		<div class=\"div-table-cell\">\r\n			<div class=\"input-group\" id=\"search_start_date_view\">\r\n				<div class=\"input-group-addon\">Start Date</div>\r\n				<input type=\"text\" id=\"search_start_date\" />\r\n			</div>\r\n		</div>\r\n		<div class=\"div-table-cell\" id=\"search_end_date_view\">\r\n			<div class=\"input-group\" >\r\n				<div class=\"input-group-addon\">End Date</div>\r\n				<input type=\"text\" id=\"search_end_date\" />\r\n			</div>\r\n		</div>\r\n		\r\n		\r\n	</div>\r\n	<div class=\"div-table-row\">	\r\n		<div class=\"div-table-cell\"></div>\r\n		<div class=\"div-table-cell\"></div>\r\n		<div class=\"div-table-cell\"></div>\r\n		<div class=\"div-table-cell\" id=\"search_btn_div\">\r\n			<a href=\"#\" id=\"search_task_btn\" class=\"btn btn-info\"><span class=\"glyphicon glyphicon-search\"></span> Search</a>\r\n			<a href=\"#\" id=\"manage_clear_btn\" class=\"btn btn-info\"> Clear</a>\r\n		</div>\r\n	</div>\r\n	\r\n</div>";
  },"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "test";
  },"useData":true});
templates['title_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "\r\n<div class=\"input-group-addon\" id=\"title_div\">Title</div><input type=\"hidden\" id=\"search_selected_title\"/>\r\n<input type=\"text\" id=\"search_title\" class=\"form-control\"/>\r\n\r\n	";
  },"useData":true});
templates['user_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"input-group-addon\" id=\"user_div\">Assignee</div><input type=\"hidden\" id=\"search_selected_user\"/>\r\n<input type=\"text\" id=\"search_user\" class=\"form-control\"/>\r\n";
  },"useData":true});
})();