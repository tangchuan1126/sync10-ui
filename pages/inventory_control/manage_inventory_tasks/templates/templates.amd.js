define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"instance_area_table\" id=\""
    + alias3(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"instanceId","hash":{},"data":data}) : helper)))
    + "_area_table\" data-instanceId=\""
    + alias3(((helper = (helper = helpers.instanceId || (depth0 != null ? depth0.instanceId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"instanceId","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.repeating : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	<table  class=\"table table-striped\">\n		<thead>\n			<th>Area Name</th>\n			<th>Status</th>\n			<th>Progress</th>\n			<th></th>\n		</thead>\n		<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.areas : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</tbody>\n	</table>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    return "		<div class=\"instances_areas_table_back_div\">\n			<a href=\"#\" class=\"instances_areas_table_back btn btn-default btn-sm\">Back to Instances</a>\n		</div>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "		\n			<tr>\n				<td>"
    + alias2(alias1((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\n				<td>"
    + alias2(alias1((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\n				\n					<td>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SHOW_PROGRESS : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "					</td>\n				\n				<td>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.SHOW_DETAIL : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</td>\n			</tr>\n		\n";
},"5":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<div class=\"custom_progress_box\">\n			             <div class=\"custom_progress\" style=\"background-position:"
    + alias2(alias1((depth0 != null ? depth0.PERCENT_DONE : depth0), depth0))
    + "px 0px;\">\n			                "
    + alias2(alias1((depth0 != null ? depth0.DONECOUNT : depth0), depth0))
    + " / "
    + alias2(alias1((depth0 != null ? depth0.TOTALCOUNT : depth0), depth0))
    + "\n			             </div>\n			           </div>  \n";
},"7":function(depth0,helpers,partials,data) {
    return "			        --\n";
},"9":function(depth0,helpers,partials,data) {
    return "					<a href=\"#\" class=\"area_view_detail btn btn-default btn-sm\" data-areaId=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.AREA_ID : depth0), depth0))
    + "\" >View Detail</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.areas : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['create_task_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div  id=\"buttons-div\">\n	<a href=\"#\" id=\"create\" class=\"btn btn-warning\">\n	<span class=\"glyphicon glyphicon-plus\"></span> Create New Task</a>\n</div>";
},"useData":true});
templates['immybox_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"input-group-addon\" id=\""
    + alias3(((helper = (helper = helpers.divId || (depth0 != null ? depth0.divId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"divId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div><input type=\"hidden\" id=\""
    + alias3(((helper = (helper = helpers.selectedInput || (depth0 != null ? depth0.selectedInput : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"selectedInput","hash":{},"data":data}) : helper)))
    + "\"/>\n<input type=\"text\" id=\""
    + alias3(((helper = (helper = helpers.inputId || (depth0 != null ? depth0.inputId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"inputId","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control\"/>\n\n";
},"useData":true});
templates['instances_progress_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<table id=\"instances_table\" class=\"table table-striped\">\n	<thead>\n		<th>Instance</th>\n		<th>Start Date</th>\n		<th>End Date</th>\n		<th>Status</th>\n		<th></th>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.instances : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\n</table>\n\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	\n		<tr>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "</td>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.START_DATE : depth0), depth0))
    + "</td>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "</td>\n			<td >"
    + alias2(alias1((depth0 != null ? depth0.STATUS : depth0), depth0))
    + "</td>\n			<td><a href=\"#\" class=\"instance_link btn btn-default btn-sm\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">View Detail</a></td>\n		</tr>\n	\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.instances : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['progress_areas'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"div-table-cell\" id=\"areas_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"panel panel-default\">\n		<div class=\"panel-heading header\">\n			<div>"
    + alias3(((helper = (helper = helpers.areaName || (depth0 != null ? depth0.areaName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaName","hash":{},"data":data}) : helper)))
    + "</div>\n		</div>\n		<div class=\"body panel-body\">\n			<div class=\"locations_block\" id=\"locations_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">	\n			</div>\n\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['progress_locations'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"location_"
    + alias3(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.locationName || (depth0 != null ? depth0.locationName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationName","hash":{},"data":data}) : helper)))
    + "\n</div>";
},"useData":true});
templates['progress_task'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  					<td class=\"labele\">Product Line</td>\n  					<td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_LINE_NAME : stack1), depth0))
    + "</td>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return "            <td class=\"labele\">Category</td>\n            <td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_CATEGORY_NAME : stack1), depth0))
    + "</td>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "  						<td class=\"labele\">End Date</td>\n  						<td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1), depth0))
    + "</td>\n";
},"7":function(depth0,helpers,partials,data) {
    var stack1;

  return "  						<td class=\"labele\">End After</td>\n  						<td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_OCCURRENCE : stack1), depth0))
    + " Occurrences</td>\n";
},"9":function(depth0,helpers,partials,data) {
    var stack1;

  return "            <td class=\"labele\">Status</td>\n            <td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STATUS : stack1), depth0))
    + "</td>\n";
},"11":function(depth0,helpers,partials,data) {
    var stack1;

  return "            <td class=\"labele\">LP Type</td>\n            <td class=\"val\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LP_TYPE_NAME : stack1), depth0))
    + "</td>\n";
},"13":function(depth0,helpers,partials,data) {
    var stack1;

  return "          <tr>\n            <td class=\"labele\">Title</td>\n            <td class=\"val\" colspan=\"4\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLE : stack1), depth0))
    + "</td>\n          </tr>\n";
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return "          <tr>\n            <td class=\"labele\">Model #</td>\n            <td class=\"val\" colspan=\"4\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.MODELS : stack1), depth0))
    + "</td>\n          </tr>\n";
},"17":function(depth0,helpers,partials,data) {
    var stack1;

  return "          <tr>\n            <td class=\"labele\">Lot #</td>\n            <td class=\"val\" colspan=\"4\">"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LOTS : stack1), depth0))
    + "</td>\n          </tr>\n";
},"19":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "  				<tr>\n  					<td class=\"labele\">Repeat By</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_BY : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Repeat Every</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.REPEAT_EVERY : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Duration</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.DURATION : stack1), depth0))
    + "</td>\n  					\n  				</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel panel-default\">\n  	<div id=\"progress_task_header\" class=\"panel-heading\">\n      <div class=\"left\">Task Progress</div>\n      <div id=\"progress-back-buttons-div\"><a href=\"#\" id=\"task_progress_back\" class=\"btn btn-warning\">\n      <span class=\"glyphicon glyphicon-list-alt\"></span> Manage Tasks List</a></div>\n      <div class=\"clear\"></div>\n  	</div>\n  	<div class=\"panel-body\">\n  			<table class=\"info_table\">\n  				<tr>\n  					<td class=\"labele\">Warehouse</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.WAREHOUSE_NAME : stack1), depth0))
    + "</td>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_LINE_NAME : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRODUCT_CATEGORY_NAME : stack1),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  					<td class=\"labele\">Created By</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.CREATED_BY_NAME : stack1), depth0))
    + "</td>\n  				</tr>\n  				<tr >\n  					<td class=\"labele\">Repeating</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.IS_REPEAT : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Type</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TYPE : stack1), depth0))
    + "</td>\n  					<td class=\"labele\">Start Date</td>\n  					<td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STARTS_ON : stack1), depth0))
    + "</td>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.ENDS_ON : stack1),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "  				</tr>\n          <tr>\n            <td class=\"labele\">Priority</td>\n            <td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.PRIORITY : stack1), depth0))
    + "</td>\n            <td class=\"labele\">Code</td>\n            <td class=\"val\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.CODE : stack1), depth0))
    + "</td>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.STATUS : stack1),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LP_TYPE : stack1),{"name":"if","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "          </tr>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.TITLES : stack1),{"name":"if","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.MODEL_NUMBERS : stack1),{"name":"if","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.task : depth0)) != null ? stack1.LOT_NUMBERS : stack1),{"name":"if","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.repeating : depth0),{"name":"if","hash":{},"fn":this.program(19, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  			</table>\n        <div id=\"progress_user_area_location_header\">\n        <span class=\"glyphicon glyphicon-plus\"></span> Area / Location Assignments</div>	\n        <div id=\"progress_user_area_location\"></div>\n\n			<div id=\"second_level_progress_wrapper\">\n  			<div id=\"second_level_progress\">\n  				<div id=\"instances_progress\"></div>\n  				<div id=\"instances_area_progress\"></div>\n  				<div id=\"location_differences_container\"></div>\n          <div class=\"clear\"></div>\n  			</div>\n      </div>\n\n	</div>\n</div>";
},"useData":true});
templates['progress_users'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"div-table-row\" id=\"users_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\" data-user-id=\""
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"div-table-cell user_name\">"
    + alias3(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userName","hash":{},"data":data}) : helper)))
    + "</div>\n	<div class=\"div-table-cell areas\" >\n		<div class=\"div-table\">\n			<div class=\"div-table-row\" id=\"areas_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\"></div>\n		</div>\n	</div>\n</div>\n	\n";
},"useData":true});
templates['reassign_areas'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"div-table-cell\" id=\"reassign_areas_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\" data-area_id=\""
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"panel panel-default\">\n		<div class=\"panel-heading header\">\n			<input type=\"checkbox\" name=\"area_selection_checkbox\" id=\"select_all_loc_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\"/>\n			<span>"
    + alias3(((helper = (helper = helpers.areaName || (depth0 != null ? depth0.areaName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaName","hash":{},"data":data}) : helper)))
    + "</span>\n		</div>\n		<div class=\"body panel-body\">\n			<div class=\"locations_block\" >\n				<ul id=\"reassign_locations_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\" class=\"list-unstyled\">\n				</ul>	\n			</div>\n\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['reassign_locations'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<li id=\"reassign_location_"
    + alias3(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationId","hash":{},"data":data}) : helper)))
    + "\" class=\"assigned\">\n	<label>\n		<input type=\"checkbox\" data-location_id=\""
    + alias3(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationId","hash":{},"data":data}) : helper)))
    + "\" id=\"loc_"
    + alias3(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationId","hash":{},"data":data}) : helper)))
    + "\"/>\n		<span>"
    + alias3(((helper = (helper = helpers.locationName || (depth0 != null ? depth0.locationName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationName","hash":{},"data":data}) : helper)))
    + "</span>\n		<div class='assigned_user' data-assigned_user_id=\""
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\" id=\"reassign_location_user_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias3(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userName","hash":{},"data":data}) : helper)))
    + "\">&nbsp;</div>\n		\n	</label>\n	\n</li>";
},"useData":true});
templates['reassign_users'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n	<div class=\"employee_box\">\n		<input type=\"checkbox\" id=\"employee_box_checkbox_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\" data-user_id=\""
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\"/>\n		<span class=\"\" id=\"employee_box_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userName","hash":{},"data":data}) : helper)))
    + "</span>\n	</div>\n	\n";
},"useData":true});
templates['reassign_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "					<div class=\"body panel-body\">\n						<div class=\"employee_box_div\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "						</div>\n						<div class=\"clear\"></div>\n					</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<div class=\"employee_box\" >\n								<div class=\"employee_name\" id=\""
    + alias2(alias1((depth0 != null ? depth0.user_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.user_name : depth0), depth0))
    + "</div>\n								<div class=\"count\">"
    + alias2(alias1((depth0 != null ? depth0.count : depth0), depth0))
    + "</div>\n								<div class=\"clear\"></div>\n							</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"reassign_view_table_wrapper\">\n	<div class=\"div-table\" id=\"reassign_view_table\">\n		<div class=\"div-table-row\">\n\n			<div class=\"div-table-cell left_side\">\n				<div id=\"reassign_view_area_locations\">\n					<div id=\"reassign_view_users_name_list\">\n						<div class=\"employee_box\" id=\"reassign_select_all_users_div\">\n							<input type=\"checkbox\" id=\"reassign_select_all_users\"/>\n							<span>Select All</span>\n						</div>\n						\n					</div>\n					<div id=\"reassign_assign_user_button_div\">\n						<a class=\"btn btn-primary disabled\" id=\"reassign_assign_user_button\">Reassign User</a>\n						<div class=\"clear\"></div>\n					</div>\n					<div id=\"reassign_view_areas_place\"></div>\n				</div>\n			</div>\n\n			<div class=\"div-table-cell right_side\">\n				<div id=\"reassign_view_users_side\"  class=\"panel panel-default\">\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					<div class=\"panel-footer\">\n						<a  id=\"reassign_assign_user_ok\" class=\"btn btn-primary disabled\">\n						<span class=\"glyphicon glyphicon-ok\"></span> Ok</a>\n						<a  id=\"reassign_assign_user_cancel\" class=\"btn btn-warning\">\n						<span class=\"glyphicon glyphicon-cancel\"></span> Cancel</a>\n					</div>\n				</div>\n			</div>\n			<div class=\"clear\"></div>\n		</div>\n	</div>\n</div>\n\n\n\n\n";
},"useData":true});
templates['search_panel'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"div-table\">\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_warehouse_list_view\">\n				\n			</div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_title_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_product_line_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_product_cat_list_view\"></div>\n		</div>\n		\n		\n	</div>\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_model_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_lot_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_lp_type_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_task_number_div\">\n				<div class=\"input-group-addon\">Task Code</div><input type=\"hidden\" id=\"search_selected_task_number\"/>\n				<input type=\"text\" id=\"search_task_number\" class=\"form-control\" maxlength=\"30\"/>\n			</div>\n		</div>\n	</div>\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_priority_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_user_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_created_by_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_type_list_view\"></div>\n		</div>\n		\n		\n	</div>\n	\n	<div class=\"div-table-row\">\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_repeat_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_status_list_view\"></div>\n		</div>\n		<div class=\"div-table-cell\">\n			<div class=\"input-group\" id=\"search_start_date_view\">\n				<div class=\"input-group-addon\">Start Date</div>\n				<input type=\"text\" id=\"search_start_date\" />\n			</div>\n		</div>\n		<div class=\"div-table-cell\" id=\"search_end_date_view\">\n			<div class=\"input-group\" >\n				<div class=\"input-group-addon\">End Date</div>\n				<input type=\"text\" id=\"search_end_date\" />\n			</div>\n		</div>\n		\n		\n	</div>\n	<div class=\"div-table-row\">	\n		<div class=\"div-table-cell\"></div>\n		<div class=\"div-table-cell\"></div>\n		<div class=\"div-table-cell\"></div>\n		<div class=\"div-table-cell\" id=\"search_btn_div\">\n			<a href=\"#\" id=\"search_task_btn\" class=\"btn btn-info\"><span class=\"glyphicon glyphicon-search\"></span> Search</a>\n			<a href=\"#\" id=\"manage_clear_btn\" class=\"btn btn-info\"> Clear</a>\n		</div>\n	</div>\n	\n</div>";
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "test";
},"useData":true});
templates['title_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "\n<div class=\"input-group-addon\" id=\"title_div\">Title</div><input type=\"hidden\" id=\"search_selected_title\"/>\n<input type=\"text\" id=\"search_title\" class=\"form-control\"/>\n\n	";
},"useData":true});
templates['user_list_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"input-group-addon\" id=\"user_div\">Assignee</div><input type=\"hidden\" id=\"search_selected_user\"/>\n<input type=\"text\" id=\"search_user\" class=\"form-control\"/>\n";
},"useData":true});
return templates;
});