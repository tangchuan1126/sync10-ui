define([
    './config',
    'jquery',
    'backbone',
    'handlebars',
    'templates',
    "./view/search_panel",
    "./componentListMain",
    "./view/create_task_button",
    "bootstrap",

],
function(config,$,Backbone,Handlebars,templates, searchPanelView, component, createButton){
    
    $(function(){
        
        
        
        var queryOptions = {el: "#tasks_list", url:config.searchTasks,queryCondition:{}};
        var resultView  = new component(queryOptions);
        //resultView.render();
        var searchPanel = new searchPanelView({resView:resultView});
        searchPanel.render();
        new createButton({searchView:resultView}).render();
        
    }); 
    
});
