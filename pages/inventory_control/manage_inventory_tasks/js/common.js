"use strict";
define([
         "config", 
         "jquery",
        "jqueryui/tabs"
        
    ], 
 function(page_config,$){
  
	        var show_selected_areas = function(){
				$("#toc_area .subitmebox").hide();
				$("#toc_area .focusedInput").parent().show();
				$("#show_all_areas_only").show();
				$("#show_selected_areas_only").hide();
	        }
	        var show_all_areas = function(){
	        	$("#toc_area .subitmebox").show();
	        	$("#show_all_areas_only").hide();
	        	$("#show_selected_areas_only").show();
	        }
	        var checkEnableCreateButton = function(){
	        	

	        	var areaSelected= false;
	        	
		        if($("#toc_area :checked").length > 0){
		        
		            areaSelected = true;
		        }

		        if(areaSelected){
	        		
	          		$("#next_assign_user_div").removeClass("disabled");
	          		
	        	}else{
	        		$("#next_assign_user_div").addClass("disabled");
	        		
	        	}
	        	
	        	
	          
	        }
	        
	        var hide_other_fields = function(){
	          $("#toc_title").hide();
	          $("#toc_area").html("");
	          $("#users_body").hide();
	          $("#deadline_body").hide();
	          
	        }

	        var showError = function(msg){
	          $("#error_block").html(msg);
	          $("#error_block").show();
	        }

	        var hideError = function(){
	          $("#error_block").html("");
	          $("#error_block").hide();
	        }

  			var show_other_fields = function(){

	          $("#users_body").show();
	          $("#deadline_body").show();
	         
	        }
	        var show_users = function(dir){
	        	if($("#next_assign_user_div").hasClass("disabled")){

	        	}else{
	        		stretch(dir);
	        	}
		        
		     
		    }
		    var show_areas = function(){
		    	stretch("right");
		    }
		    var show_date = function(dir){
		    	
		    	if($("#next_date_selection_div").hasClass("disabled")){
		        }else{
		        	stretch(dir);
			    }
		     
		    }
		    var hide_date = function(){
		        $("#create_task_body").show();
		        $("#deadline_body").hide();
		     
		    }

		    var stretch = function(direction){
		    	var l='-=1020px';
	        	if(direction=="right"){l='+=1020px'}
        		$("#users_body").animate({left:l},500);
	        	$("#toc_area").animate({left:l},500);
		        $("#deadline_body").animate({left:l},500);
		       	$("#create_task_body").animate({left:l},500);
		    }

		    return {
		    	show_selected_areas:show_selected_areas,
		    	show_all_areas:show_all_areas,
		    	checkEnableCreateButton:checkEnableCreateButton,
		    	hide_other_fields:hide_other_fields,
		    	showError:showError,
		    	hideError:hideError,
		    	show_other_fields:show_other_fields,
		    	show_users:show_users,
		    	show_areas:show_areas,
		    	show_date:show_date,
		    	hide_date:hide_date
		    }
  });