"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../model/user_model.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,models,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_created_by_list_view",
      template:templates.immybox_view,
      collection: new models.UserCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        dis.collection.url = config.getUsersOfWarehouseJSON+warehouse;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_created_by_div", name:"Created By", inputId:"search_created_by", selectedInput:"search_selected_created_by"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_created_by_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_created_by",
          selectedInputId: "#search_selected_created_by"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
       
        
      },

      
      
    });

}); 