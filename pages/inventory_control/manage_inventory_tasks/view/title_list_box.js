"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/title_model.js",
  "./product_line_list_box.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,title_models,productLineListBoxView,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_title_list_view",
      template:templates.immybox_view,
      collection: new title_models.TitleImmyCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        dis.collection.url = config.getAllTitlesJSON+ "?ps_id="+warehouse;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_title_div", name:"Title", inputId:"search_title", selectedInput:"search_selected_title"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_title_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_title",
          selectedInputId: "#search_selected_title"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        //dis.pull_areas();
        
      },

      pull_areas:function(){
        var dis=this;

       new productLineListBoxView().render();

      },
        
      
      
    });

}); 