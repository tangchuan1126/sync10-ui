"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_repeat_list_view",
      template:templates.immybox_view,
      
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        
        var html=dis.template({divId:"search_repeat_div", name:"Repeat", inputId:"search_repeat", selectedInput:"search_selected_repeat"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#search_repeat_div",
          dataUrl: [{text:"All",value:"0"},{text:"Repeating",value:"1"},{text:"Non-Repeating",value:"2"}], 
          inputId: "#search_repeat",
          selectedInputId: "#search_selected_repeat"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
        
      },
        
      
      
    });

}); 