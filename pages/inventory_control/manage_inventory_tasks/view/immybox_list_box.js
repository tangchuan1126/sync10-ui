"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/immy_model",
  "../model/area_model",
  "../create_model/user_model",
  "../create_view/area_list_box",
  "../create_view/users_view",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, model,area_models,userModel,areaListBoxView,userView, ImmyBox ) {
    
    return Backbone.View.extend({
    
      template:templates.immybox_view,
      collection: new model.immyCollection(),
      
      render:function(data){
        var dis = this;
        //dis.el = data.el;
        var html=dis.template({divId:data.divId, name:data.name, inputId:data.inputId, selectedInput:data.selectedInputId});
        
        
        $(data.el).html(html);
        dis.collection.url = data.url;//config.getAllTitlesJSON+ "?ps_id="+data.warehouse_id;
        dis.collection.fetch({dataType: "json",async: false});
        
        var titleImmydata = {
          renderTo: "#"+data.divId,
          dataUrl: dis.collection,
          placeHolder:data.placeHolder, 
          inputId: "#"+data.inputId,
          selectedInputId: "#"+data.selectedInputId
        };

        var titleImmy = new ImmyBox(titleImmydata);
        if(data.onChange=="warehouse_onchange"){
          titleImmy.on("events.change",function(){dis.warehouse_onchange()});
        }
        
        titleImmy.render();
        
      },
      warehouse_onchange:function(){
        var warehouse = $("#create_selected_warehouse").val();
        //var title = $("#create_selected_title").val();
        if(warehouse && warehouse!="0"){
          var col= new area_models.AreaImmyCollection(warehouse,"0");
         /* if(title){col = new area_models.AreaImmyCollection(warehouse,title);}
          else{col = new area_models.AreaImmyCollection(warehouse,0);}*/
          var areas = new areaListBoxView({collection:col,task:null});
          
          if(areas.collection.length > 0){
                var userCollection = new userModel.UserCollection(warehouse);
                var userViewObj = new userView({collection:userCollection, userId:0});
                areas.render();
                userViewObj.render();

            }
        }
      }

    
      
    });

}); 