"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/product_cat_model.js",
  "./model_list_box.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,models,modelListBoxView,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_product_cat_list_view",
      template:templates.immybox_view,
      collection: new models.PCImmyCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        var title = $("#search_selected_title").val()?$("#search_selected_title").val():"0";
        var product_line = $("#search_selected_product_line").val()?$("#search_selected_product_line").val():"0";
        
        var url = config.getProductCategoryJSON+ "?ps_id="+warehouse+"&product_line="+product_line;
        if(title!="0"){url+= "&title_ids="+title}
        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_product_cat_div", name:"Category", inputId:"search_product_cat", selectedInput:"search_selected_product_cat"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_product_cat_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_product_cat",
          selectedInputId: "#search_selected_product_cat"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        //dis.pull_areas();
        
      },

      pull_areas:function(){
        var dis=this;

       new modelListBoxView().render();

      },
        
      
      
    });

}); 