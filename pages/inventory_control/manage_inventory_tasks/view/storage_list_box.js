"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/storage_model.js",
  "./title_list_box.js",
  "./created_by_list_box.js",
  "./assigned_to_list_box.js", 
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( $, Backbone, Handlebars, templates,storage_models,titleListBoxView,createdByView,assignedToView,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_warehouse_list_view",
      template:templates.immybox_view,
      collection: new storage_models.StorageImmyCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"search_warehouse_div", name:"Warehouse", inputId:"search_warehouse", selectedInput:"search_selected_warehouse"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_warehouse_div",
          dataUrl: dis.collection,
          placeHolder:"please select", 
          inputId: "#search_warehouse",
          selectedInputId: "#search_selected_warehouse"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();

        
      },

      pull_areas:function(){
        var dis=this;
        
        if($("#search_selected_warehouse").val()!="0"){
          common.show_search_fields();
          new titleListBoxView().render();
          new createdByView().render();
          new assignedToView().render();
          //$("#search_task_btn").trigger("click")
        }else{
          common.hide_search_fields();
          
        }
        
      },
        
      
      
    });

}); 