"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates) {
    
    return Backbone.View.extend({
      el:"#reassign_view_users_name_list",
      template:templates.reassign_users,

      
      render:function(data){
        var dis = this;
        if(!$("#employee_box_"+data.USER_ID).length){
        
          var html = dis.template({userId:data.USER_ID,userName:data.USER_NAME});
          dis.$el.append(html);
        }
        
        
        
      },
      
    });

}); 