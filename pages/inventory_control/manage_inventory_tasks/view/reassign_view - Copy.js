"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/user_model",
  "art_Dialog/dialog-plus",
  "../jsontohtml_templates",
  "./reassign_user_view",
  "./reassign_area_view",
  "./reassign_location_view",
    "mCustomScrollbar",
   "../create_js/common", 
  "bootstrap",
  "handlebars_ext"

], function( $,config, Backbone, Handlebars, templates, userModels, Dialog, Template, reassignUserView, reassignAreaView,reassignLocationView,mCustomScrollbar, common ) {
    
    return Backbone.View.extend({
      el:"#reassign_div",
      template:templates.reassign_view,
      collection: new userModels.UserCollection(),
      
      render:function(data){
        var dis = this;
        var assignedUser = data.assignedUser;
        dis.collection.url = config.getUsersOfWarehouseJSON+data.task.WAREHOUSE_ID;
        dis.collection.fetch({dataType: "json",async: false});
        var html=dis.template({users:dis.collection.sort().toJSON(), assignedUser:assignedUser});
        
        

        //dis.$el.html(html);
        var dialog = new Dialog({
              content: html,
              lock:true,
              width: 860,
              height: 480,
              title:"Reassign Task",
              ok: function (event) {
                dis.reassign_user(data.task.TASK_ID,dialog,data.list);
                
            },
              cancelVal: 'Cancel',
              cancel: function(dialog){
                this.close();
                
              }
          })
        var assignments = data.task.ASSIGNMENTS;
        $.each(assignments,function(index,item){
           
            new reassignUserView().render(item);
            new reassignAreaView().render(item);
            new reassignLocationView().render(item);
           
        });

        /*var list_unstyled = $("#reassign_view_areas_place .locations_block");
        list_unstyled.mCustomScrollbar({
          axis: "y",
          scrollbarPosition: "outside"
        });
        list_unstyled.find(".mCSB_scrollTools").css("right", "-10px");*/

        /*$("#reassign_view_areas_place .unassign_user_from_location").click(function(evt){
          var item = evt.target;
          var label = $(item).parent();
          label.parent().removeClass("assigned");
          label.children(".unassign_user_from_location").remove();
          label.children(".assigned_user").remove();
          evt.preventDefault();
        });
        $("#reassign_view_areas_place .assigned_user").click(function(evt){
          evt.stopPropagation();
          evt.preventDefault();
        })*/
       
        
        
       /* $("#reassign_view_users_side .employee_box").click(function(evt){
           $(".employee_box").each(function(i,item){
                $(item).removeClass("selected_employee");
           });
           $(this).addClass("selected_employee");
           dis.enable_ok_button();
        }); */
        
        //dis.bind_events_to_user_box();        
        
        //dis.bind_events_to_select_all(); 
        


        /*var emp_div = $("#reassign_view_users_side .employee_box_div");
        emp_div.mCustomScrollbar({
          axis: "y",
          scrollbarPosition: "outside"
        });
        emp_div.find(".mCSB_scrollTools").css("right", "-15px");*/
        
        /*$("#reassign_assign_user_button").click(function(evt){
            common.reassign_show_users();
            evt.stopPropagation();
        });*/

       
         $("#reassign_view_areas_place input").change(function(evt){
          
             evt.stopImmediatePropagation();
             dis.check_assign_user_button();  
             evt.preventDefault();
           
        });  

        /* $("#reassign_view_areas_place .header input").change(function(evt){
          
          var id = $(evt.target).parent().parent().parent().attr("id");
          var select_all = $(evt.target).prop("checked");
          $.each($("#"+id+" li"),function(index,item){
            if($(item).css("display")!="none"){
              $(item).find("input").prop("checked",select_all);
            }
          })
          dis.check_assign_user_button();   
          evt.stopPropagation();  
          evt.preventDefault();
        });*/



       /* $("#reassign_assign_user_ok").click(function(){
          dis.assign_user_ok();
        }); 
        $("#reassign_assign_user_cancel").click(function(){
          dis.assign_user_cancel();
        });
        $("#reassign_select_all_users").trigger("click")*/
        dialog.showModal();
      },

      enable_ok_button : function(){
        var dis = this;
        $("#reassign_assign_user_ok").removeClass("disabled");
      },

      disable_ok_button : function(){
        var dis = this;
        $("#reassign_assign_user_ok").addClass("disabled");
      },

      unselect_user:function(){
        var dis = this;
         $("#reassign_view_users_side .employee_box").each(function(i,item){
              $(item).removeClass("selected_employee");
         });
      },

      assign_user_ok:function(){
          console.log("assign_user_ok");
          var dis = this;
          dis.disable_ok_button();
          
          var employee = $("#reassign_view_users_side .selected_employee .employee_name");
          var user_id  = $(employee).attr('id');
          var user_name = $(employee).html();
          dis.mark_user_on_selected_location(user_id,user_name);
          dis.add_user_to_list(user_id,user_name);
          dis.clear_location_selection();
          common.reassign_hide_users();
          dis.unselect_user();
      },

      bind_events_to_select_all:function(){
        var dis = this;
        $("#reassign_select_all_users").change(function(evt){
          $.each($("#reassign_view_users_name_list .employee_box input"),function(index,item){
              var select_all = $("#reassign_select_all_users").prop("checked");
              $(item).prop("checked",select_all);
              if(select_all){
                  
                  $.each($("#reassign_view_areas_place .assigned_user"),function(index,item){
                      $(item).parent().parent().show();
                  });
               }else{

                  $.each($("#reassign_view_areas_place .assigned_user"),function(index,item){
                      $(item).parent().parent().hide();
                      $(item).parent().find("input").prop("checked",false);
                  });
               } 
           });
          dis.check_assign_user_button(); 
          evt.preventDefault();  
          evt.stopPropagation();  
        });
      },

      clear_location_selection:function(){
        var dis = this;
        $.each($("#reassign_view_areas_place li :checked"),function(index,item){
            $(item).prop("checked",false);
        })

      },

      bind_events_to_user_box:function(){
        var dis = this;
        dis.bind_events_to_user_box_input();
        $("#reassign_view_users_name_list .employee_box").click(function(evt){
           if(evt.target.type!="checkbox"){
             var div = evt.currentTarget;
             $(div).find("input").trigger("click");
           }      

           evt.stopPropagation();
           evt.preventDefault();      
        });
      },
      bind_events_to_user_box_input:function(){
        var dis = this;
        $("#reassign_view_users_name_list .employee_box input").change(function(evt){
           var userId = $(evt.target).data("id");
           if($(evt.target).prop("checked")){
              
              $.each($("#reassign_view_areas_place #reassign_location_user_"+userId),function(index,item){
                  $(item).parent().parent().show();
              });
           }else{
              $.each($("#reassign_view_areas_place #reassign_location_user_"+userId),function(index,item){
                  $(item).parent().parent().hide();
                  $(item).parent().find("input").prop("checked",false);
              });
           }      
           dis.check_assign_user_button();   

           evt.stopPropagation();  
           evt.preventDefault();
        }); 
      },

      
      bind_event_to_newly_added_input:function(){
        var dis=this;
        var new_box_input = $("#reassign_view_users_name_list .employee_box").last().find("input");
        
          $(new_box_input).change(function(evt){
             var userId = $(evt.target).data("id");
             if($(evt.target).prop("checked")){
                
                $.each($("#reassign_view_areas_place #reassign_location_user_"+userId),function(index,item){
                    $(item).parent().parent().show();
                });
             }else{
                $.each($("#reassign_view_areas_place #reassign_location_user_"+userId),function(index,item){
                    $(item).parent().parent().hide();
                    $(item).parent().find("input").prop("checked",false);
                });
             }      
             dis.check_assign_user_button();   
             evt.preventDefault();
             evt.stopPropagation();  
          });
          $(new_box_input).prop("checked",true);
      },

      bind_event_to_newly_added_box:function(){
        var dis=this;
        $("#reassign_view_users_name_list .employee_box").last().click(function(evt){
             if(evt.target.type!="checkbox"){
               var div = evt.currentTarget;
               $(div).find("input").trigger("click");
             }      

             evt.stopPropagation();   
             evt.preventDefault();   
          });
      },

      add_user_to_list:function(user_id,user_name){
        var dis=this;
        if(!$("#reassign_view_users_name_list #employee_box_checkbox_"+user_id).length){
          var html = templates.reassign_users({userId:user_id,userName:user_name});
          $("#reassign_view_users_name_list").append(html);

          dis.bind_event_to_newly_added_input();
          dis.bind_event_to_newly_added_box();
          
        }
      },
      mark_user_on_selected_location:function(user_id,user_name){
        var dis = this;
        $.each($("#reassign_view_areas_place .locations_block :checked"),function(index,item){
          $(item).parent().find(".assigned_user").remove();
          $(item).parent().find(".unassign_user_from_location").remove();
          var span = "<div class='assigned_user' data-id='"+user_id+"' id='reassign_location_user_"+user_id+"' title='"+user_name+"'>&nbsp;</div>";
          span += "<div class='unassign_user_from_location' data-id='"+user_id+"' title='Unassign'>&nbsp;</div>";
          $(item).parent().append(span);
            
        });

      },
      assign_user_cancel:function(){
          console.log("assign_user_cancel");
          var dis = this;
          dis.disable_ok_button();
          dis.unselect_user();
          common.reassign_hide_users();
      },

      check_assign_user_button:function(){
        console.log($("#reassign_view_areas_place li input:checked").length)
        if($("#reassign_view_areas_place li input:checked").length){
          $("#reassign_assign_user_button a").removeClass("disabled");
        }else{
          $("#reassign_assign_user_button a").addClass("disabled");
        }
        console.log("checked assigned user button")
      },

      get_assignments:function(){
        var dis = this;
        var assignments=[];
        $.each($("#reassign_view_areas_place").children(),function(index,item){
         
          var area_id = $(item).data("id");
          var ul = $(item).find("ul");
           $.each($(ul).children("li.assigned"),function(ind, it){
              var loc_id = $(it).find("input").data("checkboxid");
              var user_id = $(it).find(".assigned_user").data("id");
              var assignment = {"user_id":user_id, "location_id":loc_id, "area_id":area_id}
              assignments.push(assignment);
           });
        });
        return assignments;
      },

      reassign_user:function(taskId,dialog) {
        var dis = this;
        var user_id = 0;
        var assignments = dis.get_assignments();
        var data = {"id":taskId, "assignments":assignments};
        console.log(JSON.stringify(data))
         $.ajax({
                  url:  config.reAssignTaskURL,
                  type: 'put',
                  timeout: 60000,
                  cache:false,
                  data: JSON.stringify(data),
                  dataType: 'json',
                  contentType: "application/json",
                  async:true,
                  
                  error: function(jqXHR, textStatus, errorThrown) {
                    showMessage("Error while reassigning task.","error");
                    dialog.close();
                  },
                  success: function(data){
                    showMessage("Task has been reassigned.","succeed");
                    dialog.close();
                  }
                });
      },

      renderOneRow:function(olist,taskId)
      {
        $.getJSON(config.getSingleTaskDetail+taskId, {},
          function(json, textStatus) 
          {
            if(textStatus == "success")
            {
              olist.upSubitmeData({
              upitme:[{Root:"",itmeid:taskId,itmekey:"ID"}],
                DATA:json
                });
            Template.tbody.updateOneRow(taskId,json);
            }
        });
        
      }

    
      
    });

}); 