"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar) {
    
    return Backbone.View.extend({
      template:templates.progress_locations,
      collection:null,
      
      render:function(data){
        var dis = this;
        
          var html=dis.template({locationId:data.item.LOCATION_ID,locationName:data.item.LOCATION_NAME});
          $("#progress_user_area_location #locations_"+data.item.USER_ID+"_"+data.item.AREA_ID).append(html);
        
      },
      

      
    });

}); 