"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates) {
    
    return Backbone.View.extend({
      template:templates.reassign_areas,

      
      render:function(data){
        var dis = this;
        if(!$("#reassign_areas_"+data.AREA_ID).length){
          var html = dis.template({userId:data.USER_ID,areaName:data.AREA_NAME,areaId:data.AREA_ID});
          $("#reassign_view_areas_place").append(html);
        }
      },
      
    });

}); 