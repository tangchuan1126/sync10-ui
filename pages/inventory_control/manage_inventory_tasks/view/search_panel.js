"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "./storage_list_box.js",
  "./priority_list_box.js",
  "./type_list_box.js",
  "./repeat_list_box.js",
  "./status_list_box.js",
  "bootstrap.datetimepicker",
  "bootstrap",
  "require_css!bootstrap.datetimepicker-css",

  

], function( $,config, Backbone, Handlebars, templates, storageView,priorityView,typeView,repeatView,statusView, datepicker) {
    
    return Backbone.View.extend({
      el:"#search_box",
      template:templates.search_panel,
      resultView:null,

      initialize:function(data){
        var dis = this;
        dis.resultView = data.resView;
      },
      render:function(){
        var dis = this;
        var html=dis.template;
        dis.$el.html(html);
        
        new storageView().render();
        new priorityView().render();
        new typeView().render();
        new repeatView().render();
        new statusView().render();
       

        $("#search_start_date").datetimepicker({ format: "yyyy-mm-dd", 
          autoclose:1, minView:2}).on('changeDate', function(ev){
              var endTime= $('#search_end_date');
                  endTime.datetimepicker('setStartDate', ev.delegateTarget.value);
                }).on('keydown', function(event) {
                  return false;
                });
       
         $("#search_end_date").datetimepicker({ format: "yyyy-mm-dd",autoclose:1, minView:2});
         $("#search_task_btn").click(function(){dis.search();})
         $("#manage_clear_btn").click(function(){dis.clear_manage_task_search();})
      },
      clear_manage_task_search:function(){
        var dis = this;
        new storageView().render();
        new priorityView().render();
        new typeView().render();
        new repeatView().render();
        new statusView().render();
        $("#search_task_number").val("");
        $("#search_selected_task_number").val("");

        $("#search_start_date").val("");
        $("#search_end_date").val("");

      },

      search:function(){
        
        var dis = this;

        var start_date = $("#search_start_date").val();
        var end_date = $("#search_end_date").val();
        var warehouse = $("#search_selected_warehouse").val();
        var title = $("#search_selected_title").val();
        var user = $("#search_selected_assignee").val();
        var created_by = $("#search_selected_created_by").val();
        var type = $("#search_selected_type").val();
        var repeat=$("#search_selected_repeat").val();
        var status = $("#search_selected_status").val();
        var product_line = $("#search_selected_product_line").val();
        var product_cat = $("#search_selected_product_cat").val();
        var model = $("#search_selected_product_model").val();
        var lot = $("#search_selected_lot").val();
        var lp_type = $("#search_selected_product_lp_type").val();
        var name = $("#search_task_number").val();
        var priority = $("#search_selected_priority").val();

        
        if(model=="0"){model=null;}
        if(lot=="0"){lot=null;}
        var queryCondition = {"ps_id":warehouse,"title_id":title, "product_line":product_line,
        "catalog":product_cat,"p_name":model,"lot_number":lot,"lp_type":lp_type, "created_by":created_by,
        "start_date":start_date, "end_date":end_date, "user_id":user,"status":status,"type":type,
        "repeating":repeat, "task_code":name, "priority":priority  }
        
        //commenting this code to use the same compoundList with different options
        /*dis.resultView.undelegateEvents();
        dis.resultView.$el.html("");
        var queryOptions = {el: "#tasks_list", url:config.searchTasks,queryCondition:queryCondition};
         var rView  = new component(queryOptions);
         rView.render();
         dis.resultView = rView;*/
        dis.resultView.setQuery(queryCondition);
         
      },
      
    
      
    });

}); 