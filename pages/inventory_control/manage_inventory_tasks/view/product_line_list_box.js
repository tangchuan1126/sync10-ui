"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/product_line_model.js",
  "./product_cat_list_box.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,models,productCatListBoxView,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_product_line_list_view",
      template:templates.immybox_view,
      collection: new models.PLImmyCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        var title = $("#search_selected_title").val()?$("#search_selected_title").val():"0";
        
        var url = config.getProductLineJSON+ "?ps_id="+warehouse;
        if(title!="0"){url+= "&title_ids="+title}
        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_product_line_div", name:"Product Line", inputId:"search_product_line", selectedInput:"search_selected_product_line"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_product_line_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_product_line",
          selectedInputId: "#search_selected_product_line"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        //dis.pull_areas();
        
      },

      pull_areas:function(){
        var dis=this;

       new productCatListBoxView().render();

      },
        
      
      
    });

}); 