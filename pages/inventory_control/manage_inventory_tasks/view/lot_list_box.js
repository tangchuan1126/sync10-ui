"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/lot_numbers_model.js",
  "./lp_type_list_box.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,models,lpTypeListBoxView,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_lot_list_view",
      template:templates.immybox_view,
      collection: new models.LotImmyCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        var title = $("#search_selected_title").val()?$("#search_selected_title").val():"0";
        var product_line = $("#search_selected_product_line").val()?$("#search_selected_product_line").val():"0";
        var product_cat = $("#search_selected_product_cat").val()?$("#search_selected_product_cat").val():"0";
        var model = $("#search_selected_model").val()?$("#search_selected_model").val():"";
        
        var url = config.getLotNumbersJSON+ "?ps_id="+warehouse+"&product_line="+product_line+"&category="+product_cat;
        if(title!="0"){url+= "&title_ids="+title}
        if(model!=""){url+= "&model_numbers="+model}
        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_product_lot_div", name:"Lot #", inputId:"search_product_lot", selectedInput:"search_selected_product_lot"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_product_lot_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_product_lot",
          selectedInputId: "#search_selected_product_lot"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        //dis.pull_areas();
        
      },

      pull_areas:function(){
        var dis=this;

       new lpTypeListBoxView().render();

      },
        
      
      
    });

}); 