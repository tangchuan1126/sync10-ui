"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../create_js/common",
  "../model/progress_task_detail_model",
  "../Differences/view/location_differences_view",
  "./progress_users_view",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates,common, taskModels, differencesView, usersView ) {
    
    return Backbone.View.extend({
      el:"#progress_task",
      template:templates.progress_task,
      collection: null,
      taskId:0,
      
      render:function(data){
        var dis = this;
        dis.$el.html("");       
        var task = new taskModels.TaskModel();
        if(dis.taskId==0){
          dis.taskId = data.taskId;
        }
        
        task.url = config.getTaskProgress+dis.taskId;
        task.fetch({dataType: "json",async: false});
        task.formatFields();
        var repeating = task.get("IS_REPEAT")=="Yes"? true : null;
        var html=dis.template({task:task.toJSON(), repeating:repeating});
        
        dis.$el.html(html);   
        if(repeating){
          if(task.get("INSTANCES") && task.get("INSTANCES").length > 0){
            var ht = templates.instances_progress_view({instances:task.get("INSTANCES")});
            $("#instances_progress").append(ht);
            $.each(task.get("INSTANCES"), function(index,item){

              var h = templates.area_progress_view({repeating:repeating,instanceId:item.ID,areas:item.AREAS});
              $("#instances_area_progress").append(h);
            });
          }else{
            // no instances to show
            var areas = task.get("AREAS");
            var insId = "0";
            var h = templates.area_progress_view({instanceId:insId,areas:areas});
              $("#instances_area_progress").append(h);
              $(".instance_area_table").show();
              $("#instances_area_progress").show();
          }    

          $(".instance_link").click(function(e){dis.instanceClicked(e);});
          $(".instances_areas_table_back").click(function(e){dis.instancesAreaTableBack(e);});
          
        }else{
          var areas;
          var insId;
          if(task.get("INSTANCES") && task.get("INSTANCES").length > 0){
            areas = task.get("INSTANCES")[0].AREAS;
            insId = task.get("INSTANCES")[0].ID;
          }else{
            areas = task.get("AREAS");
            insId = "0";
          }
          
              var h = templates.area_progress_view({repeating:repeating,instanceId:insId,areas:areas});
              $("#instances_area_progress").append(h);
              $(".instance_area_table").show();
              $("#instances_area_progress").show();
          
        }
        $.each($(".instance_area_table"), function(index,item){
          var insId = item.dataset.instanceid;
          $.each($(item).find(".area_view_detail"), function(ind,data){
              var areaId= data.dataset.areaid;
              $(data).click(function(e){dis.view_detail(insId,areaId)});
          });
        });

        new usersView().render({assignments:task.get("ASSIGNMENTS")});
        $("#task_progress_back").click(function(e){dis.backToManage(e);});
        $("#progress_task").show();
        $("#task_order_content").hide();
        $("#progress_user_area_location_header").click(function(){
          if($("#progress_user_area_location").is(':visible')){
            $("#progress_user_area_location_header span").removeClass("glyphicon-minus").addClass("glyphicon-plus")
          }else{
            $("#progress_user_area_location_header span").removeClass("glyphicon-plus").addClass("glyphicon-minus")
          }
          $("#progress_user_area_location").slideToggle('slow');
        })
      },

      view_detail:function (insId, areaId) {
        var dis = this;
        var diffView = new differencesView().render({parent:dis,instanceId:insId,areaId:areaId});
        common.slide_progress("left");
      },

      instanceClicked:function (e) {
        var instanceId = e.currentTarget.dataset.value;
        $(".instance_area_table").hide();
        $("#"+instanceId+"_area_table").show();

        /*$("#instances_progress").hide();
        $("#instances_area_progress").show();*/
        common.slide_progress("left");
      },

      instancesAreaTableBack:function(e){
        
        /*$(".instance_area_table").hide();
        $("#instances_progress").show();*/
        common.slide_progress("right");
        
      },
      backToManage:function(e){
        
        $("#progress_task").hide();
        $("#task_order_content").show();
        
      }
      
    });

}); 