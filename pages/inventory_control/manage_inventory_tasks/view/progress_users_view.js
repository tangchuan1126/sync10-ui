"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "./progress_areas_view",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar,areaPreview) {
    
    return Backbone.View.extend({
      template:templates.progress_users,
      collection:null,
      
      render:function(data){
        var dis = this;

        $.each(data.assignments,function(i,item){
          
          if($("#progress_user_area_location #users_"+item.USER_ID).length){}
          else{
            var html=dis.template({userId:item.USER_ID,userName:item.USER_NAME});
            $("#progress_user_area_location").append(html);
            
          }
          new areaPreview().render({item:item})
          
        });
        
        

        
          var scrol = $("#progress_user_area_location .areas");
          // _ztreebox.height(_def.scrollH);
          scrol.mCustomScrollbar({
            axis: "x",
           /*scrollbarPosition: "inside"*/
          });
         
          var list_unstyled = $("#progress_user_area_location .locations_block");
          // _ztreebox.height(_def.scrollH);
          list_unstyled.mCustomScrollbar({
            axis: "y",
            //scrollInertia: scrollH,
            scrollbarPosition: "outside"
          });
          // scrol.find(".mCSB_scrollTools").css("top", "-22px");
          list_unstyled.find(".mCSB_scrollTools").css("right", "-22px");
          
      },
      

      
    });

}); 