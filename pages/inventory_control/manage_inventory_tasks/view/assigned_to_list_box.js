"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../model/user_model.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,models,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_user_list_view",
      template:templates.immybox_view,
      collection: new models.UserCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        dis.collection.url = config.getUsersOfWarehouseJSON+warehouse;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_assignee_div", name:"Assignee", inputId:"search_assignee", selectedInput:"search_selected_assignee"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_assignee_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_assignee",
          selectedInputId: "#search_selected_assignee"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
       
        
      },

      
      
    });

}); 