"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "./progress_locations_view",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar,locationView) {
    
    return Backbone.View.extend({
      template:templates.progress_areas,
      collection:null,
      
      render:function(data){
        var dis = this;
        if($("#progress_user_area_location #areas_"+data.item.USER_ID+"_"+data.item.AREA_ID).length){}
        else{
          var html=dis.template({areaId:data.item.AREA_ID,areaName:data.item.AREA_NAME,userId:data.item.USER_ID});
          $("#progress_user_area_location #areas_"+data.item.USER_ID).append(html);

        }
        new locationView().render({item:data.item})
        

        
      },
      

      
    });

}); 