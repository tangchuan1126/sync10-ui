"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/lp_type_model.js",

  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,models,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_lp_type_list_view",
      template:templates.immybox_view,
      collection: new models.LPTImmyCollection(),
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var warehouse = $("#search_selected_warehouse").val()?$("#search_selected_warehouse").val():"0";
        var title = $("#search_selected_title").val()?$("#search_selected_title").val():"0";
        var product_line = $("#search_selected_product_line").val()?$("#search_selected_product_line").val():"0";
        var product_cat = $("#search_selected_product_cat").val()?$("#search_selected_product_cat").val():"0";
        var model = $("#search_selected_model").val()?$("#search_selected_model").val():"";
        var lot = $("#search_selected_lot").val()?$("#search_selected_lot").val():"";
        
        var url = config.getLPTypeJSON+ "?ps_id="+warehouse+"&product_line="+
        product_line+"&category="+product_cat;
        if(title!="0"){url+= "&title_ids="+title}
        if(model!=""){url+= "&model_numbers="+model}
        if(lot!=""){url+= "&lot_numbers="+lot}
        
        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});
        
        var html=dis.template({divId:"search_product_lp_type_div", name:"LP Type", inputId:"search_product_lp_type", selectedInput:"search_selected_product_lp_type"});
        dis.$el.html(html);
        

        
        var titleImmydata = {
          renderTo: "#search_product_lp_type_div",
          dataUrl: dis.collection,
          placeHolder:"Any", 
          inputId: "#search_product_lp_type",
          selectedInputId: "#search_selected_product_lp_type"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
        
        
      },

      
        
      
      
    });

}); 