"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_status_list_view",
      template:templates.immybox_view,
      
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        
        var html=dis.template({divId:"search_status_div", name:"Status", inputId:"search_status", selectedInput:"search_selected_status"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#search_status_div",
          dataUrl: [{text:"All",value:"0"},{text:"New",value:"1"},{text:"In-Progress",value:"2"},
          {text:"Done",value:"3"},{text:"Pending Approval",value:"4"},{text:"Reviewed",value:"5"}], 
          inputId: "#search_status",
          selectedInputId: "#search_selected_status"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
        
      },
        
      
      
    });

}); 