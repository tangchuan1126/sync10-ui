"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates) {
    
    return Backbone.View.extend({
      template:templates.reassign_locations,

      
      render:function(data){
        var dis = this;
        if(!$("reassign_location_"+data.LOCATION_ID).length){
          var html = dis.template({locationId:data.LOCATION_ID,locationName:data.LOCATION_NAME,userId:data.USER_ID,userName:data.USER_NAME});
          $("#reassign_locations_"+data.AREA_ID).append(html);
        }
      },
      
    });

}); 