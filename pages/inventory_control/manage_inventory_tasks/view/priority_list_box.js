"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_priority_list_view",
      template:templates.immybox_view,
      
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        
        var html=dis.template({divId:"search_priority_div", name:"Priority", inputId:"search_priority", selectedInput:"search_selected_priority"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#search_priority_div",
          dataUrl:  [{text:"Any",value:"0"},{text:"Low",value:"1"},{text:"Medium",value:"2"},{text:"High",value:"3"}],  
          inputId: "#search_priority",
          selectedInputId: "#search_selected_priority"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
        
      },
        
      
      
    });

}); 