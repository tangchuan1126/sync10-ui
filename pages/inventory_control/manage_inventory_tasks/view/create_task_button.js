"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../create_view/new_task_view",
  "bootstrap"

], function( $,config, Backbone, Handlebars, templates, newTaskView ) {
    
    return Backbone.View.extend({
      el:"#create-div",
      template:templates.create_task_button,
      searchView:null,

      initialize:function(data){
        var dis = this;
        dis.searchView = data.searchView;
      },
      render:function(){
        var dis = this;
        var html=dis.template;
        dis.$el.html(html);
        $("#create").click(function(){dis.create();})
        $("#btn1").click(function(){dis.submitScan();})
      },

      create:function(){
        var dis = this;
        var ntv = new newTaskView({type:"new", data:null, searchView:dis.searchView}).render();
      },
      submitScan:function(){
        var dis = this;
        var file = $("#task_id_file").val();
        
        var dataJSON ; 
        $.ajax({
            url:  "./test_data_files/"+file+".json",
            type: 'get',
            timeout: 60000,
            cache:false,
            data: "",
            dataType:"json",
            async:false,
            
            error: function(jqXHR, textStatus, errorThrown) {
              dataJSON = jqXHR.responseText;
             
            },
            success: function(data){
              dataJSON=data;
            }
          });

       
        $.ajax({
            url:  config.submitScan,
            type: 'post',
            contentType: 'application/json; charset=UTF-8',
            timeout: 60000,
            cache:false,
            data: JSON.stringify(dataJSON),
            dataType: 'json',
            async:false,
            
            error: function(jqXHR, textStatus, errorThrown) {
              alert("Error while submit scan .");
             
            },
            success: function(data){
              
            }
          });
      },
      
    });

}); 