"use strict";
define([
  "../create_config/config.js",
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_js/common",
  "../js/ImmyboxControl.js",
  "bootstrap",


], function( config,$, Backbone, Handlebars, templates,common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#search_type_list_view",
      template:templates.immybox_view,
      
     
      render:function(){
        var dis = this;
        dis.$el.empty();
        
        var html=dis.template({divId:"search_type_div", name:"Task Type", inputId:"search_type", selectedInput:"search_selected_type"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#search_type_div",
          dataUrl:  [{text:"Any",value:"0"},{text:"Verify",value:"2"},{text:"Blind",value:"1"}],  
          inputId: "#search_type",
          selectedInputId: "#search_selected_type"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.render();
        
      },
        
      
      
    });

}); 