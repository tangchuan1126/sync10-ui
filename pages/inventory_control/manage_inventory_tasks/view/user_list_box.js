"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../model/user_model",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, userModels, ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#user_list_view",
      template:templates.user_list_view,
      collection: new userModels.UserCollection(),
      
      render:function(data){
        var dis = this;
        var html=dis.template;
        dis.$el.html(html);
        
        dis.collection.url = config.getUsersOfWarehouseJSON+warehouse;
        dis.collection.fetch({dataType: "json",async: false});
        
        var userImmydata = {
          renderTo: "#user_div",
          dataUrl: dis.collection.sort(),
          placeHolder:"Any", 
          inputId: "#search_user",
          selectedInputId: "#search_selected_user"
        };

        var userImmy = new ImmyBox(userImmydata);
        userImmy.on("events.change",function(){});
        userImmy.render();
        
        
        
      },

    
      
    });

}); 