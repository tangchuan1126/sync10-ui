define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getAreaForStorageJSON: "/Sync10/action/administrator/storage_location/GetLocationAreasJSON.action?ps_id=",
    getAreaLocationUserJSON: "/Sync10/_inventoryControl/cycleCount/search/searchLocations?ps_id=",
    getUsersOfWarehouseJSON: "/Sync10/_inventoryControl/search/usersOfWarehouse?ps_id=",
    getAreaDifferencesJSON: "/Sync10/_inventoryControl/search/filterStorageApproveAreas",
    getAllTitlesJSON: "/Sync10/_inventoryControl/cycleCount/search/titles",
    getProductLineJSON: "/Sync10/_inventoryControl/cycleCount/search/productLineList",
    getProductCategoryJSON: "/Sync10/_inventoryControl/cycleCount/search/productCategoryList",
    getLPTypeJSON: "/Sync10/_inventoryControl/cycleCount/search/lpType",
    getModelNumbersJSON: "/Sync10/_inventoryControl/cycleCount/search/modelNumbers",
    getLotNumbersJSON: "/Sync10/_inventoryControl/cycleCount/search/lotNumbers",
    getCountDifferenceOfContainerJSON: "/Sync10/_inventoryControl/search/getStorageApproveDifferents?sac_id=",
    saveApproveDifference: "/Sync10/_inventoryControl/update/approveDifferents",
    createTaskDifference: "/Sync10/_inventoryControl/cycleCount/add/task",
    updateTaskDetail: "/Sync10/_inventoryControl/cycleCount/update/task",
    editCopyTaskDetail: "/Sync10/_inventoryControl/cycleCount/search/taskDefinition?task_id=",
    getUsersAndAssignLoadJSON: "/Sync10/_inventoryControl/cycleCount/search/users",
    doesCodeExists: "/Sync10/_inventoryControl/cycleCount/search/isCodeExsiting?code=",
    
});
