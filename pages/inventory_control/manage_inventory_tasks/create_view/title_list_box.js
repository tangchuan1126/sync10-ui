"use strict";
requirejs.config({
    paths: {
        "mCustomScrollbarcss": "/Sync10-ui/lib/mCustomScrollbar/css/mCustomScrollbar.min",
        "BSMultiselect":"/Sync10-ui/bower_components/Bootstrap-Multiselect/dist/js/bootstrap-multiselect",
        "BSMultiselectcss":"/Sync10-ui/bower_components/Bootstrap-Multiselect/dist/css/bootstrap-multiselect",
    }
});
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/title_model.js",
  "../create_model/area_model.js",
    "../create_model/user_model.js",
  "./area_list_box.js",
  "./product_line_list_box.js",
  "../create_js/common",
  "mCustomScrollbar",
  "bootstrap",
  "BSMultiselect",
  "require_css!mCustomScrollbarcss",
  "require_css!BSMultiselectcss"

], function( config, $, Backbone, Handlebars, templates,title_models,area_models,userModel,areaListBoxView,productLineListView, common,mCustomScrollbar ) {
    
    return Backbone.View.extend({
      el:"#create_title_view",
      template:templates.select2_view,
      collection: new title_models.TitleImmyCollection(),
      task:null,
      
      
      render:function(data){
        
        var dis = this;
        dis.$el.empty();
        var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
        dis.collection.url = config.getAllTitlesJSON+ "?ps_id="+warehouse;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");

        var html=dis.template({name:"Title",id:"create_selected_title",list:dis.collection.toJSON()});
        dis.$el.html(html);
        dis.after_render();
      },

      pull_areas:function(){
        var dis = this;
        common.hideError();
        $("#create_title_view").show();
        $("#toc_area").empty();
         
        var productLine = new productLineListView();
        productLine.render({task:dis.task})
        
      },
      
      after_render:function(){
        var dis = this;
        var select1=$("#create_selected_title");
        
        if(dis.task){
          
          if(dis.task.TITLES){
            common.setDefaultValueForSelect2(select1,dis.task.TITLES);
          }
            
        }
        dis.pull_areas();
        select1.multiselect({enableFiltering:false,nonSelectedText:"All",onChange:function(){console.log("title change");dis.pull_areas();}});
        var multiselect_list = $("#create_title_view .multiselect-container");
           
         multiselect_list.mCustomScrollbar({
            axis: "y",
            
        });
      },

      
    });

}); 