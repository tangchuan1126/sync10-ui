"use strict";
define([
  "jquery",
  '../create_config/config',
  "backbone",
  "handlebars",
  "templates",
  "../create_model/task_request_model.js",
  "./preview_users_view.js",
  "../create_js/common",
  "immybox",
  "bootstrap",
  "showMessage",

], function( $,config, Backbone, Handlebars, templates, task_model, previewUsers , common) {
    
    return Backbone.View.extend({
      el:"#create_task_body",
      template:templates.create_task,
      collection:null,
      
      render:function(){
        var dis = this;

        var warehouse_id = $("#create_selected_warehouse").val();
        var warehouse_name = $("#create_warehouse").val();
        
        var product_line = $("#create_selected_product_line").val() == "0" ? null:$("#create_selected_product_line").val();
        var product_cat = $("#create_selected_product_cat").val() == "0" ? null:$("#create_selected_product_cat").val();
        var lp_type = $("#create_selected_lp_type").val() == "0" ? null:$("#create_selected_lp_type").val();
        var product_line_val = $("#create_selected_product_line").val() == "0" ? null:$("#create_product_line").val();
        var product_cat_val = $("#create_selected_product_cat").val() == "0" ? null:$("#create_product_cat").val();
        var lp_type_val = $("#create_selected_lp_type").val() == "0" ? null:$("#create_lp_type").val();
        var task_priority = $("#create_selected_task_priority").val();
        var task_priority_template = $("#create_task_priority").val();
        var task_name = $("#create_task_name").val();
        
        var isRepeat = $("#is_repeating").prop('checked')? "Yes" : "No";
        var scheduled = $("#is_repeating").prop('checked');
        var type = $("#is_blind").prop('checked')? "Blind" : "Verify";
        var typeVal = $("#is_blind").prop('checked')? 1 : 2;
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var duration = $("#task_duration").val() + " Day(s)";
        var repeatOptions = $("#repeat_options").val();
        var repeatEvery;
        var endsOn;
        var occurance;
        var endsType;
        var endsOnDate, repeatBy;
        if(repeatOptions == "Daily"){
          repeatBy = 1;
          repeatEvery = $("#repeat_options_daily").val() + " Day(s)";
        }
        if(repeatOptions == "Weekly"){
          repeatBy = 2;
          repeatEvery = $("#repeat_options_weekly").val()+ " Week(s)";
          
        }
        if(repeatOptions == "Monthly"){
          repeatBy = 3;
          repeatEvery = $("#repeat_options_monthly").val() + " Month(s)";
        }
        var startsOn = $("#starts_on").val();
        var endsOnOption = $("input[type='radio'][name='ends_on']:checked").val();
        if(endsOnOption=="never"){
            endsType = 1;
            endsOn = "Never";
        }else if(endsOnOption=="after"){
            endsType = 2;
            occurance = parseInt($("#occurances").val());
            endsOn = "After "+$("#occurances").val()+" occurrences"
        }else{
          endsType = 3;
            endsOn = $("#ends_on_date").val();
            endsOnDate = $("#ends_on_date").val();
        }
        var list = new task_model.ListCollection(); 
        var list_backend = new task_model.ListCollectionBackEnd(); 
        $.each($("#toc_area .subitmebox"), function(i,item){
          if($(item).find(".assigned_user").length>0){
            
            var area_id = $(item).attr("id").split("_")[3];
            var area_name = $(item).find(".panel_heading_title").html();
            $.each($(item).find(".assigned_user"),function(i,user_div){
              
              var user_name = $(user_div).attr("title");
              var user_id = $(user_div).data("id");
              
              var location_id = $(user_div).parent().find("input").data("checkboxid");
              var location_name = $(user_div).parent().find("span").html();
              var list_item = new task_model.ListModel({areaId:area_id,areaName:area_name,locationId:location_id,locationName:location_name,userId:user_id,userName:user_name});
              var list_item_back_end = new task_model.ListModelBackEnd({area_id:area_id,location_id:location_id,user_id:user_id})
              list.add(list_item);
              list_backend.add(list_item_back_end);
            });
          }
        });
        var buttonTitle = "Create Task";
        if($("#create_task_type").val()=="edit"){
          buttonTitle = "Update Task";
        }
        var title = $("#create_selected_title").val();
        var model_number = $("#create_selected_model_number").val();
        var lot_number = $("#create_selected_lot_number").val();
        var title_string = dis.get_multi_select_values("#create_selected_title");
        var model_string = dis.get_multi_select_values("#create_selected_model_number");
        var lot_string = dis.get_multi_select_values("#create_selected_lot_number");
        
        var html=dis.template({buttonTitle:buttonTitle,startsOn:startsOn,endsOn:endsOn,repeatEvery:repeatEvery,repeatOptions:repeatOptions,duration:duration,scheduled:scheduled,type:type,isRepeat:isRepeat,warehouse:warehouse_name,start_date:start_date,end_date:end_date,taskPriority:task_priority_template,taskName:task_name,productLine:product_line_val,productCat:product_cat_val,lpType:lp_type_val,modelNumber:model_string,lotNumber:lot_string,title:title_string});
        dis.$el.html(html);
        if(isRepeat=="No"){
          endsOnDate = end_date;
          startsOn = start_date;
        }
        var task = new task_model.TaskModel({is_repeat:scheduled,ends_type:endsType,starts_on:startsOn,ends_on:endsOnDate,occurrence:occurance,repeat_every:repeatEvery.split(" ")[0],repeat_by:repeatBy,duration:duration.split(" ")[0],type:typeVal,warehouse:parseInt(warehouse_id),code:task_name,priority:task_priority,product_line:product_line,category:product_cat,lp_type:lp_type,assignments:list_backend,titles:title, model_numbers:model_number, lot_numbers:lot_number});
        new previewUsers().render({list:list});
        
        
        $("#task_preview_back").click(function(){dis.back();});
        $("#create_task_btn").click(function(){dis.create_task(task);});
        
      },
      back:function(){

        common.task_preview_back("back");
      },
      get_multi_select_values:function(id,selected){
        var val = $(id).val();
        var str = "";
        if(val){
          $.each(val,function(index,item){
            if(str==""){
              str += $(id+" option[value='"+item+"']").html();
            }else{    
               str += ", "
              str += $(id+" option[value='"+item+"']").html();
             
            }
          });
        }
        return str;
      },
      create_task:function(task){
        var dis = this;

 
        var taskType = $("#create_task_type").val();
        
        if(taskType=="edit"){
          var taskId = $("#task_id").val();
          var created_by = $("#created_by").val();
          task.set({id:taskId,createdBy:created_by});
          
          $.ajax({
              url:  config.updateTaskDetail,
              type: 'put',
              data: JSON.stringify(task),
              contentType: 'application/json; charset=UTF-8',
              timeout: 60000,
              cache:false,
              dataType: 'json',
              async:true,
              
              error: function(jqXHR, textStatus, errorThrown) {
                showMessage("error while updating task.","error");
              },
              success: function(data){
                showMessage("Task has been updated successfully.","succeed");
                $("#back").click();
              }
            });
        }else{
          task.save(null,{success:function(){
              showMessage("Task has been created successfully.","succeed");
              $("#back").click();
          },
         error:function(){
            showMessage("error while creating task.","error");
          }});
        }
        
        
      }
      
    });

}); 