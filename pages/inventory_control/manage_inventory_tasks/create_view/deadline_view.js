"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "./create_task_view.js",
  "../create_js/common",
  "./task_priority_list_box",
  "bootstrap",
  "iToggle"


], function(config, $, Backbone, Handlebars, templates, taskView,common,taskPriorityListView) {
    
    return Backbone.View.extend({
      el:"#deadline_body",
      template:templates.deadline,
      task:null,
      
      initialize:function(data){
          var dis = this;
          dis.task = data.task;
      },
      setDefaultValue:function(){
        var dis = this;
        if(dis.task){
          $("#created_by").val(dis.task.CREATED_BY);

          $("#create_task_name").val(dis.task.CODE);
          if($("#create_task_type").val()=="edit"){
              $("#create_task_name").attr("readonly","readonly")
          }
          
          if(dis.task.TYPE==1){$("#is_blind").click();}

          if(dis.task.IS_REPEAT){
            $("#is_repeating").click();
            $("#task_duration option[value='"+dis.task.DURATION+"']").prop("selected",true);
            var repeat_by,repeat_options;
            switch(dis.task.REPEAT_BY){
              case 1:
                repeat_by="Daily";
                repeat_options = "repeat_options_daily";
                break;
              case 2:
                repeat_by="Weekly";
                repeat_options = "repeat_options_weekly";
                break;
              case 3:
                repeat_by="Monthly";
                repeat_options = "repeat_options_monthly";
                break;
            }

            $("#repeat_options option[value='"+repeat_by+"']").prop("selected",true);
            $("#repeat_options").change();
            $("#"+repeat_options+" option[value='"+dis.task.REPEAT_EVERY+"']").prop("selected",true);
            
            $("#starts_on").val(dis.task.STARTS_ON);
            $("#start_date").val(dis.task.STARTS_ON);
            var dt2 = $('#ends_on_date');
           // dt2.datepicker('option', 'minDate', dis.task.STARTS_ON);
           dt2.datetimepicker({ format: "yyyy-mm-dd",autoclose:1, minView:2,startDate:dis.task.STARTS_ON
              ,ValEmptyBtn:function(){common.check_area_selection();}
            }).on('clearDate',function(){common.check_area_selection();
                    }).on('keydown', function(event) {
                      return false;
                    });
            switch(dis.task.ENDS_TYPE){
              case 1:
                $("#ends_on_never").prop("checked",true);
                break;
              case 2:
                $("#ends_on_after").prop("checked",true);
                break;
              case 3:
                $("#ends_on_on").prop("checked",true);
                break;
            }
            
            if(dis.task.ENDS_ON){
              $('#ends_on_date').val(dis.task.ENDS_ON);
              $("#end_date").val(dis.task.ENDS_ON);
            }
            if(dis.task.ENDS_OCCURRENCE){
              $('#occurances').val(dis.task.ENDS_OCCURRENCE);
            }

          }else{
            $("#start_date").val(dis.task.STARTS_ON);
            $("#starts_on").val(dis.task.STARTS_ON);
            $("#end_date").val(dis.task.ENDS_ON);
            $('#ends_on_date').val(dis.task.ENDS_ON);
          }
          common.check_area_selection();
        }
      },
      render:function(){

        var dis = this;
        var html=dis.template();
        dis.$el.html(html);

        $("#start_date").datetimepicker({ format: "yyyy-mm-dd", startDate:new Date(),
          autoclose:1, minView:2,ValEmptyBtn:function(){common.check_area_selection();}}).on('changeDate', function(ev){
                  var endTime= $('#end_date');
                  endTime.datetimepicker('reset');
                  endTime.datetimepicker('setStartDate', ev.delegateTarget.value);
                  common.check_area_selection();
                }).on('clearDate',function(){common.check_area_selection();
                }).on('keydown', function(event) {
                  return false;
                });

        $("#end_date").datetimepicker({ format: "yyyy-mm-dd",autoclose:1, minView:2,startDate:new Date()
          ,ValEmptyBtn:function(){common.check_area_selection();}}).on('changeDate',
          function(){common.check_area_selection();
         }).on('clearDate',function(){common.check_area_selection();
        }).on('keydown', function(event) {return false;
                });
        
        //$("#end_date").datetimepicker({ dateFormat: "yy-mm-dd",minDate:1});
        $("#back_user_selection").click(function(){common.show_users("right");});
        $("#starts_on").datetimepicker({ format: "yyyy-mm-dd", startDate:new Date(),
          autoclose:1, minView:2
        ,ValEmptyBtn:function(){common.check_area_selection();}}).on('changeDate', function(ev){
              var endTime= $('#ends_on_date');
                  endTime.datetimepicker('reset');
                  endTime.datetimepicker('setStartDate', ev.delegateTarget.value);
                  common.check_area_selection();
                }).on('clearDate',function(){common.check_area_selection();
                }).on('keydown', function(event) {
                  return false;
                });
        var taskPriority = new taskPriorityListView();
        taskPriority.render({task:dis.task});

        $("#ends_on_date").datetimepicker({ format: "yyyy-mm-dd",autoclose:1, minView:2,startDate:new Date()
          ,ValEmptyBtn:function(){common.check_area_selection();}
        }).on('clearDate',function(){common.check_area_selection();
                }).on('keydown', function(event) {
                  return false;
                });
        $("#is_repeating").change(function(){dis.repeating_clicked();});
        $("#repeat_options").change(function(){dis.repeat_options_changed()})
        $("#ends_on_date").focus(function(){$("#ends_on_on").prop('checked',true);common.check_area_selection();});
        $("#occurances").focus(function(){$("#ends_on_after").prop('checked',true);common.check_area_selection();})
        $("#is_repeating").iToggle({defaultMaxWidth:117,defaultMaxHeight:32});
        $("#is_blind").iToggle({defaultMaxWidth:117,defaultMaxHeight:32});
        $("#deadline_body input").change(function(evt){common.check_area_selection(evt);})
        
        $("#deadline_body select").change(function(){common.check_area_selection();})
        $("#next_area_selection_div").addClass("disabled");
        $("#next_area_selection_div").click(function(){dis.show_areas_div();})
        if(dis.task){dis.setDefaultValue();}
      },
      
      callCommon:function(){
         common.checkEnableCreateButton();
      },
      show_areas_div:function(){
        var dis = this;
        if(!$("#next_area_selection_div").hasClass("disabled")){

        
            if($("#create_task_type").val()!="edit"){
              $.ajax({
                url:  config.doesCodeExists+$("#create_task_name").val(),
                type: 'get',
                contentType: 'application/json; charset=UTF-8',
                timeout: 60000,
                cache:false,
                async:true,
                
                error: function(jqXHR, textStatus, errorThrown) {
                  showMessage("unknown error","error");
                },
                success: function(data){
                  if(data){common.showError("Task Code already exists.");}
                  else{
                    common.hideError();
                    common.show_areas_div();
                  }
                }

              });
           }
           else{
              common.hideError();
              common.show_areas_div();
           } 
        }
      },
      repeat_options_changed:function(){
          
          var selected = $("#repeat_options").val();
          $("#Daily_table").hide();
          $("#Weekly_table").hide();
          $("#Monthly_table").hide();
          $("#"+selected+"_table").show();
          $("#"+selected+"_table").css("display","table")

      },
      repeating_clicked:function(){
        
        if($("#is_repeating_checkbox_div :checked").length>0){
          $("#repeating_div").show();
          $("#non-repeating-div").hide();
        }else{
          $("#repeating_div").hide();
          $("#non-repeating-div").show();
          
        }
      },

      create_task:function(){
       var show = true;
       if($("#create_task_type").val()!="edit"){
          $.ajax({
            url:  config.doesCodeExists+$("#create_task_name").val(),
            type: 'get',
            contentType: 'application/json; charset=UTF-8',
            timeout: 60000,
            cache:false,
            async:true,
            
            error: function(jqXHR, textStatus, errorThrown) {
              showMessage("unknown error","error");
            },
            success: function(data){
              if(data){common.showError("Task Code already exists.");show=false;}
              else{
                common.hideError();
                $("#filter_box").hide();
                $("#deadline_body").hide();
                $("#toc_area").hide();
                $("#users_body").hide();
               // $("#create_task_body").show();
                new taskView().render();
              }
            }

          });
       }
       else{
          common.hideError();
          $("#filter_box").hide();
          $("#deadline_body").hide();
          $("#toc_area").hide();
          $("#users_body").hide();
          //$("#create_task_body").show();
          new taskView().render();
       } 
        
       
       
      }

      
    });

}); 