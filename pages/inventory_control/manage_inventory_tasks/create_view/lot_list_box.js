"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/lot_numbers_model.js",
    "../create_js/common",
    "../js/ImmyboxControl",
  "./lp_type_list_box.js",
  "immybox",
  "bootstrap",


], function( config, $, Backbone, Handlebars, templates,models, common,ImmyBox ,lpTypeListView) {
    
    return Backbone.View.extend({
      el:"#create_lot_view",
      template:templates.select2_view,
      collection: new models.LotImmyCollection(),
      task:null,
      
      render:function(data){
        
        var dis = this;
        dis.$el.empty();
        
        var title = $("#create_selected_title").val()?$("#create_selected_title").val():"0";
        var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
        var product_line = $("#create_selected_product_line").val()?$("#create_selected_product_line").val():"0";
        var product_cat = $("#create_selected_product_cat").val()?$("#create_selected_product_cat").val():"0";
        var model = $("#create_selected_model_number").val()?$("#create_selected_model_number").val():"";
        
        var url = config.getLotNumbersJSON+ "?ps_id="+warehouse+"&product_line="+product_line+"&category="+product_cat;
        if(title!="0"){url+= "&title_ids="+title}
        if(model!=""){url+= "&model_numbers="+model}
        
        dis.collection.url = url;

        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");
        var html=dis.template({name:"Lot #",id:"create_selected_lot_number",list:dis.collection.toJSON()});
        dis.$el.html(html);
        $("#create_selected_lot_number").change(function(){dis.pull_areas();})

        dis.after_render();
      },

      pull_areas:function(){
          var dis = this;
          var lpType = new lpTypeListView();
          lpType.render({task:dis.task})
            
      },

      after_render:function(){
        var dis = this;
        var select3=$("#create_selected_lot_number");
        if(dis.task){
          
          if(dis.task.LOT_NUMBERS){
            common.setDefaultValueForSelect2(select3,dis.task.LOT_NUMBERS);
          }
            
        }
        dis.pull_areas();
        select3.multiselect({enableFiltering:false,nonSelectedText:"All",onChange:function(){dis.pull_areas();}});

        var multiselect_list = $("#create_lot_view .multiselect-container");
           
         multiselect_list.mCustomScrollbar({
            axis: "y",
            
        });

      },
      
    });

}); 