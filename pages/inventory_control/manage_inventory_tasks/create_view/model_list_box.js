"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/model_numbers_model.js",
  "./lot_list_box.js",
    "../create_js/common",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",


], function( config, $, Backbone, Handlebars, templates,models,lotListView,common,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#create_model_view",
      template:templates.select2_view,
      collection: new models.MNImmyCollection(),
      task:null,
      
      render:function(data){
        
        var dis = this;
        dis.$el.empty();
        var title = $("#create_selected_title").val()?$("#create_selected_title").val():"0";
        var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
        var product_line = $("#create_selected_product_line").val()?$("#create_selected_product_line").val():"0";
        var product_cat = $("#create_selected_product_cat").val()?$("#create_selected_product_cat").val():"0";
        
        var url = config.getModelNumbersJSON+ "?ps_id="+warehouse+"&product_line="+product_line+"&category="+product_cat;
        if(title!="0"){url+= "&title_ids="+title}
        
        dis.collection.url = url;

        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");
        var html=dis.template({name:"Model #",id:"create_selected_model_number",list:dis.collection.toJSON()});
        dis.$el.html(html);
       
        dis.after_render();
      },

      pull_areas:function(){
          var dis = this;
          var lot = new lotListView();
          lot.render({task:dis.task})
      },
      after_render:function(){
        var dis = this;
        var select2=$("#create_selected_model_number");
        
        if(dis.task){
          
         if(dis.task.MODEL_NUMBERS){
            common.setDefaultValueForSelect2(select2,dis.task.MODEL_NUMBERS);
          }
        }

        dis.pull_areas();
        select2.multiselect({enableFiltering:false,nonSelectedText:"All",onChange:function(){dis.pull_areas();}});
        var multiselect_list = $("#create_model_view .multiselect-container");
           
         multiselect_list.mCustomScrollbar({
            axis: "y",
            
        });
      },
      
    });

}); 