"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "./preview_locations_view",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar,locationView) {
    
    return Backbone.View.extend({
      el:"#selected_areas_create",
      template:templates.preview_areas,
      collection:null,
      
      render:function(data){
        var dis = this;
        if($("#areas_"+data.item.userId+"_"+data.item.areaId).length){}
        else{
          var html=dis.template({areaId:data.item.areaId,areaName:data.item.areaName,userId:data.item.userId});
          $("#areas_"+data.item.userId).append(html);
        }
        new locationView().render({item:data.item})
        
      },
      
    });

}); 