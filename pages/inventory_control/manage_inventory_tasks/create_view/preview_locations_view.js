"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar) {
    
    return Backbone.View.extend({
      el:"#selected_areas_create",
      template:templates.preview_locations,
      collection:null,
      
      render:function(data){
        var dis = this;
        
          var html=dis.template({locationId:data.item.locationId,locationName:data.item.locationName});
          $("#locations_"+data.item.userId+"_"+data.item.areaId).append(html);
        
      },
      

      
    });

}); 