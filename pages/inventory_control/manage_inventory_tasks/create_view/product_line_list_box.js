"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/product_line_model.js",
  "./product_category_list_box.js",
    "../create_js/common",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,productLineModel,categoryListView,common,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#create_product_line_view",
      template:templates.immybox_view,
      collection: new productLineModel.PLImmyCollection(),
      task:null,
      
      render:function(data){
        
        var dis = this;
        dis.task = data.task;
        dis.$el.empty();
        dis.$el.html("");
        var title = $("#create_selected_title").val()?$("#create_selected_title").val():"0";
        var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
        
        var url = config.getProductLineJSON+ "?ps_id="+warehouse;
        if(title!="0"){url+= "&title_ids="+title}
        
        dis.collection.url = url;
        dis.collection.fetch({dataType: "json",async: false});

        var html=dis.template({divId:"create_product_line_div", name:"Product Line", inputId:"create_product_line", selectedInput:"create_selected_product_line"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#create_product_line_view",
          dataUrl: dis.collection,
          placeHolder:"All", 
          inputId: "#create_product_line",
          selectedInputId: "#create_selected_product_line",
          defaultValue: dis.task?dis.task.PRODUCT_LINE:"0"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        
        if(!dis.collection || dis.collection.length ==0){
          dis.pull_areas();
        }
      },

      pull_areas:function(){
        var dis = this;
        var category = new categoryListView();
        category.render({task:dis.task})
      }
      
    });

}); 