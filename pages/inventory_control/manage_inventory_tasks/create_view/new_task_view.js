"use strict";
define([
    '../create_config/config',
    'jquery',
    'backbone',
    'handlebars',
    '../create_templates/templates.amd',
    "../create_model/task_request_model",
    "../create_model/edit_task_model",
    "./storage_list_box",
    "./parameters_view",
    "./deadline_view",
     "art_Dialog/dialog",
    "bootstrap",

], function(config,$,Backbone,Handlebars,templates,models, editTaskModel,storageListBoxView, paramView,deadlineView, dialog){
    
    return Backbone.View.extend({
      el:"#new_task_content",
      template:templates.new_task_view,
      type:"new",
      taskId:0,  
      searchView:null,

      initialize:function(data){
        var dis = this;
        dis.type = data.type;
        dis.taskId = data.taskId;
        dis.searchView = data.searchView;
        
      },

      render:function(){
        var dis = this;
        var heading = "Create";
        if(dis.type && dis.type!="new"){
            dis.fetchAndRender(dis.type,dis.taskId);
        }else{
            var html=dis.template({heading: heading+" Cycle Count Task", taskType:"new", id:dis.taskId});
            dis.$el.html(html);
            /*var paramsView = new paramView({});
            paramsView.render();*/
            //new deadlineView({task:dis.task}).render();
            var storageListBox = new storageListBoxView({task:{}});
            storageListBox.render();
        }
        
        $("#back").click(function(){dis.back();});
        $("#new_task_content").show();
        $("#task_order_content").hide();
      },
      fetchAndRender:function(type, taskId){
        var dis = this;
        var heading = "Create";
        var taskType = "edit";
        switch(type){
            case "edit":
                heading = "Edit";
                break;
            case "copy":
                heading = "Copy";
                taskType = "copy";
                break;
        }
        var task = new editTaskModel.TaskModel();
        task.url = config.editCopyTaskDetail+taskId;
        task.fetch({dataType: "json",async: false});
        /*var locations = new Array();
        $.each(task.toJSON().AREAS,function(index,item){
            $.each(item.LOCATIONS, function(i,it){
                locations.push(it.LOCATION_ID);
                
            })
        });
        task.set({locations:locations});*/


        var html=dis.template({heading: heading+" Cycle Count Task", taskType:taskType, id:taskId});
        dis.$el.html(html);
        var storageListBox = new storageListBoxView({task:task.toJSON()});
        storageListBox.render();

      },
      back:function(){
        var dis = this;
        dis.undelegateEvents();
        $("#new_task_content").html("").hide();
        $("#task_order_content").show();
        dis.searchView.reloadData();
        //this.remove();

        
      },
     
      
    });

}); 