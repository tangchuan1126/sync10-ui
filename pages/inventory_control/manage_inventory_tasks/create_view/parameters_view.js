"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../view/immybox_list_box",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,immyBoxList ) {
    
    return Backbone.View.extend({
      el:"#create_parameters",
      template:templates.parameter_view,
      task:null,
      
      selectDefaultValue:function(immyBoxIns){
        var dis= this;
        if(dis.task.TITLE_ID){
          immyBoxIns.selectChoiceByValue(dis.task.TITLE_ID);
          $("#selected_title_id").val(dis.task.TITLE_ID);
        }else{
          immyBoxIns.selectChoiceByValue("0");
          $("#selected_title_id").val("0");
        }
        
       
      },
      render:function(data){
        
        var dis = this;
        dis.$el.html("");
        dis.$el.html(dis.template());
        var warehouse = new immyBoxList();
        var u = config.getAllStorageJSON;
        warehouse.render({el:"#create_warehouse_view", name:"Warehouse", inputId:"create_warehouse",
          selectedInputId:"create_selected_warehouse",url:u,divId:"create_warehouse_div",placeHolder:"please select",onChange:"warehouse_onchange"});
      
        /*var title = new immyBoxList();
        var u = config.getAllStorageJSON;
        title.render({el:"#create_title_view", name:"Title", inputId:"create_title",
          selectedInputId:"create_selected_title",url:u,divId:"create_title_div",placeHolder:"All"});

        var productLine = new immyBoxList();
        var u = config.getAllStorageJSON;
        productLine.render({el:"#create_product_line_view", name:"Product Line", inputId:"create_product_line",
          selectedInputId:"create_selected_product_line",url:u,divId:"create_product_line_div",placeHolder:"All"});
      
      var productCat = new immyBoxList();
        var u = config.getAllStorageJSON;
        productCat.render({el:"#create_product_cat_view", name:"Category", inputId:"create_product_cat",
          selectedInputId:"create_selected_product_cat",url:u,divId:"create_product_cat_div",placeHolder:"All"});
      
      var lpType = new immyBoxList();
        var u = config.getAllStorageJSON;
        lpType.render({el:"#create_lp_type_view", name:"Category", inputId:"create_lp_type",
          selectedInputId:"create_selected_lp_type",url:u,divId:"create_lp_type_div",placeHolder:"All"});*/
      },
      
     }); 

}); 