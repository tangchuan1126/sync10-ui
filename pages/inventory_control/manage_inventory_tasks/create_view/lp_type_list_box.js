"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/lp_type_model.js",
  "../create_js/common",
  "../js/ImmyboxControl",
  "../create_model/area_model.js",
  "./area_list_box.js",
  "./deadline_view.js",
/*  "./product_line_list",*/
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,lp_type_models,common,ImmyBox, area_models,areaListBoxView,deadlineView /*,productLineListView*/) {
    
    return Backbone.View.extend({
      el:"#create_lp_type_view",
      template:templates.immybox_view,
      collection: new lp_type_models.LPTImmyCollection(),
      task:null,
      
      render:function(data){
        
        var dis = this;
        dis.task = data.task;
        dis.$el.empty();
        
        var title = $("#create_selected_title").val()?$("#create_selected_title").val():"0";
        var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
        var product_line = $("#create_selected_product_line").val()?$("#create_selected_product_line").val():"0";
        var product_cat = $("#create_selected_product_cat").val()?$("#create_selected_product_cat").val():"0";
        var model = $("#create_selected_model_number").val()?$("#create_selected_model_number").val():"";
        var lot = $("#create_selected_lot_number").val()?$("#create_selected_lot_number").val():"";
        
        var url = config.getLPTypeJSON+ "?ps_id="+warehouse+"&product_line="+
        product_line+"&category="+product_cat;
        if(title!="0"){url+= "&title_ids="+title}
        if(model!=""){url+= "&model_numbers="+model}
        if(lot!=""){url+= "&lot_numbers="+lot}
        
        dis.collection.url = url;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");
        var html=dis.template({divId:"create_lp_type_div", name:"LP Type", inputId:"create_lp_type", selectedInput:"create_selected_lp_type"});
        dis.$el.html(html);

        var titleImmydata = {
          renderTo: "#create_lp_type_view",
          dataUrl: dis.collection,
          placeHolder:"All", 
          inputId: "#create_lp_type",
          selectedInputId: "#create_selected_lp_type",
          defaultValue: dis.task?dis.task.LP_TYPE_ID:"0"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        if(!dis.collection || dis.collection.length ==0){
          dis.pull_areas();
        }
               
      },
      

      
      pull_areas:function(){
        var dis = this;
          common.hideError();
          $("#toc_area").empty();
          var title = $("#create_selected_title").val()?$("#create_selected_title").val():"";
          var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
          var product_line = $("#create_selected_product_line").val()?$("#create_selected_product_line").val():"0";
          var product_cat = $("#create_selected_product_cat").val()?$("#create_selected_product_cat").val():"0";
          var model = $("#create_selected_model_number").val()?$("#create_selected_model_number").val():"";
          var lot = $("#create_selected_lot_number").val()?$("#create_selected_lot_number").val():"";
          var lptype = $("#create_selected_lp_type").val()?$("#create_selected_lp_type").val():"0";

          var url_parameters = warehouse;
          if(title!=""){url_parameters+="&title_ids="+title}
          if(product_line!="0"){url_parameters += "&product_line="+product_line}
          if(product_cat!="0"){url_parameters += "&category="+product_cat}
          if(model!=""){url_parameters += "&model_numbers="+model}  
          if(lot!=""){url_parameters+="&lot_numbers="+lot}
          if(lptype!="0"){url_parameters+="&lp_type_id="+lptype;}
        
          /*new deadlineView({task:dis.task}).render();*/
          $("#deadline_body").css("left",0);
          $("#toc_area").css("left",0);
          $("#users_body").css("left",0);
          $("#create_task_body").css("left",0);
          var col = new area_models.AreaImmyCollection({url:url_parameters});
          var areas = new areaListBoxView({collection:col,task:dis.task});
          if(areas.collection.length > 0){
                
                areas.render();

            }else{
              common.showError("No Products for current selection");
              
            }

      }
      
    });

}); 