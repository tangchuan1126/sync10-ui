"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "mCustomScrollbar",
  "./preview_areas_view",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, mCustomScrollbar,areaPreview) {
    
    return Backbone.View.extend({
      el:"#selected_areas_create",
      template:templates.preview_users,
      collection:null,
      
      render:function(data){
        var dis = this;
        dis.$el.html("");

        $.each(data.list.toJSON(),function(i,item){
          
          if($("#users_"+item.userId).length){}
          else{
            var html=dis.template({userId:item.userId,userName:item.userName});
            $("#selected_areas_create").append(html);
            
          }
          new areaPreview().render({item:item})
          
        });
        
        

        
          var scrol = $("#selected_areas_create .areas");
          // _ztreebox.height(_def.scrollH);
          scrol.mCustomScrollbar({
            axis: "x",
           /*scrollbarPosition: "inside"*/
          });
         
          var list_unstyled = $("#selected_areas_create .locations_block");
          // _ztreebox.height(_def.scrollH);
          list_unstyled.mCustomScrollbar({
            axis: "y",
            //scrollInertia: scrollH,
            scrollbarPosition: "outside"
          });
          // scrol.find(".mCSB_scrollTools").css("top", "-22px");
          list_unstyled.find(".mCSB_scrollTools").css("right", "-22px");
          
      },
      

      
    });

}); 