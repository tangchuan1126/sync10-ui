"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/product_cat_model.js",
    "./model_list_box.js",
    "../create_js/common",
    "../js/ImmyboxControl",
  "immybox",
  "bootstrap",

], function( config, $, Backbone, Handlebars, templates,product_cat_models,modelListView, common,ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#create_product_cat_view",
      template:templates.immybox_view,
      collection: new product_cat_models.PCImmyCollection(),
      task:null,
      
      render:function(data){
        
        var dis = this;
        dis.$el.empty();
        var title = $("#create_selected_title").val()?$("#create_selected_title").val():"0";
        var warehouse = $("#create_selected_warehouse").val()?$("#create_selected_warehouse").val():"0";
        var product_line = $("#create_selected_product_line").val()?$("#create_selected_product_line").val():"0";
        
        var url = config.getProductCategoryJSON+ "?ps_id="+warehouse+"&product_line="+product_line;
        if(title!="0"){url+= "&title_ids="+title}
        
        dis.collection.url = url;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
        dis.$el.html("");
        var html=dis.template({divId:"create_product_cat_div", name:"Category", inputId:"create_product_cat", selectedInput:"create_selected_product_cat"});
        dis.$el.html(html);

        var titleImmydata = {
          renderTo: "#create_product_cat_view",
          dataUrl: dis.collection,
          placeHolder:"All", 
          inputId: "#create_product_cat",
          selectedInputId: "#create_selected_product_cat",
          defaultValue: dis.task?dis.task.CATEGORY:"0"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        if(!dis.collection || dis.collection.length ==0){
          dis.pull_areas();
        }
      },

      pull_areas:function(){
        var dis = this;
        var model = new modelListView();
        model.render({task:dis.task})
      }
      
    });

}); 