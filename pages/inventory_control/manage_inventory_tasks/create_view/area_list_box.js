"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../create_model/area_model",
  "../create_model/user_task_count_model",
  "./create_task_view.js",  
  "./users_view",
  "oso.lib/InventoryControls/Inventory",
    "../create_js/common",
  "bootstrap",

], function( $, Backbone, Handlebars, templates,area_models,userModel,taskView, userView, InventoryControl,common) {
    
    return Backbone.View.extend({
      el:"#toc_area",
      //template:templates.area_list_box,
      collection:null,
      task:null,

      initialize:function(data){
        var dis = this;
        dis.task = data.task;
        dis.collection.fetch({dataType: "json",async: false});
      },
      selectDefaultValue:function(){
        var dis=this;
       if(dis.task && dis.task.ASSIGNMENTS){
          dis.fetch_users();
          $.each(dis.task.ASSIGNMENTS, function(index,item){
            var loc = item.LOCATION_ID;
            var user_id = item.USER_ID;
            var user_name = $("#users_body div[data-user_id='"+user_id+"']").html();

            $("#toc_area_inven_"+loc+" .checkbox").addClass("assigned");
            var span = "<div class='assigned_user' data-id='"+user_id+"' title='"+user_name+"'>&nbsp;</div>";
            span += "<div class='unassign_user_from_location' data-id='"+user_id+"' title='Unassign'>&nbsp;</div>";
            $("#toc_area_inven_"+loc+" label").append(span);
          });
          $(".unassign_user_from_location").undelegate();
          $(".unassign_user_from_location").on("click", function(evt){common.unassign_user(evt)});
          common.check_preview();
        }
        
      },
      render:function(){
        var dis = this;
       // var html=dis.template({list: dis.collection.toJSON()});
        $("#toc_area").empty();
        $("#toc_area").html("");
        $("#toc_area").show();
        $("#no_area").hide();
//        $("#create_task_body").hide();
        
        // populate InventoryControl component for resultant areas
        var invdata = {
          renderTo: "#toc_area",//容器
          dataUrl: dis.collection.sort().toJSON(),//数据或[{},{}...]
          //postData:{a:"123",b:"345"},//往服务端传数据
          searchfieldkey:"AREA_NAME",//被匹配字段名
          scrollH:250,//高度，出滚动条
          itmepanelw:200,//宽度
          filterbuttonstxt: "clear",
          inventorytitle:"Select All",
          placeholder:"Filter Areas / Locations", 
          title_in_input:true
         // configurl:{"buttons":[]}
        };

        var Inventoryliste = new InventoryControl(invdata);
        
        Inventoryliste.on("events.titleallclick",function(jsondata){
          common.checkEnableCreateButton();
        });
        Inventoryliste.on("events.filter",function(jsondata){
          $(".Inventory_filter input").trigger("input");
        });
        Inventoryliste.on("events.itmeclick",function(jsondata){
          common.checkEnableCreateButton();
        });
        Inventoryliste.on("events.all-areas-checkbox",function(jsondata){
          common.checkEnableCreateButton();
        });
        Inventoryliste.on("events.on-render-complete",function(){
           dis.selectDefaultValue();
        });
        Inventoryliste.on("Initialize",function(){
          
        if($("#show_selected_areas_only").length==0){
          var button_group = '<div  id="areas_button_group">'
          +'<button type="button" class="btn btn-default active" id="show_all_areas_only">All Areas</button>'
          +'<button type="button" class="btn btn-default" id="show_selected_areas_only">Selected</button>'
          +'<button type="button" class="btn btn-default" id="show_assigned_areas_only">Assigned</button>'
          +'<button type="button" class="btn btn-default" id="show_unassigned_areas_only">Unassigned</button>'
          +'</div>'

          $("#toc_area .Inventory_box .Inventory_body").prepend(button_group);
          
          $("#toc_area .ptopfoot5px").append("<div id='next_task_preview_div' class='disabled'> <a class='next' id='next_task_preview'>Task Preview</a></div>");
          $("#toc_area .ptopfoot5px").append("<div class='clear'></div>");
          $("#next_task_preview_div").click(function(evt){dis.show_task_preview(evt)});
          $("#show_selected_areas_only").click(function(){common.show_selected_areas();});
          $("#show_all_areas_only").click(function(){common.show_all_areas();});
          $("#show_assigned_areas_only").click(function(){common.show_assigned_areas();});
          $("#show_unassigned_areas_only").click(function(){common.show_unassigned_areas();});
          $("<div class='assign_user_icon'><button class='btn btn-warning btn-sm disabled'><span class='glyphicon glyphicon-user'></span> Assign User</button>").insertAfter($("#toc_area .titleinputlable"));
          $('<a class="previous" id="back_date_selection"> Task Info </a>').insertBefore($("#toc_area .panel_topleft_box"));
          $("#back_date_selection").click(function(){common.show_date_deadline();});
          $("#toc_area .assign_user_icon").click(function(){dis.assign_user_view()});
          $("#users_body").hide()
        }
          
        });
        Inventoryliste.render();
        
       console.log("render area")

      },
      show_task_preview:function(evt){
        
        if($("#next_task_preview_div").hasClass("disabled")){}
        else{  
          new taskView().render();
         
          common.show_task_preview();
        }
        evt.stopPropagation();
        evt.preventDefault();
       
      },
      fetch_users:function(){
        var start_date,end_date,duration,is_repeating,ps_id;
          ps_id = $("#create_selected_warehouse").val();;
          is_repeating = $("#is_repeating").prop('checked');
          
          if($("#is_repeating").prop('checked')){
            start_date = $("#starts_on").val()
            duration = $("#task_duration").val();
          }else{
            start_date = $("#start_date").val();
            end_date = $("#end_date").val();
            
          }

          var userCollection = new userModel.UserCollection({"ps_id":ps_id, "start_date":start_date, 
            "end_date":end_date, "duration":duration,"is_repeating":is_repeating});
          var userViewObj = new userView({collection:userCollection});
          userViewObj.render();
      },
      assign_user_view:function(){
        var dis = this;
        if(!$(".assign_user_icon button").hasClass("disabled")){
          
          dis.fetch_users();
          common.show_users("left");
        }
      },
      showOnlySelectedAreas:function(){
          $(".task_box_unselected").each(function(i,item){
             $(item).parent().hide(); 
          });
      },

      find_in_attributes:function(searchString,model){
          var attributesList = ["name"];
          for (var i = attributesList.length - 1; i >= 0; i--) {
            if(model.get(attributesList[i])){
              if(model.get(attributesList[i]).toLowerCase().indexOf(searchString)>-1){
                return true;
              }
            }
          }
          return false;
      },

      filterAreas: function(searchString){
       
        var dis = this;
        $("#no_area").show();
        var result = dis.collection.select(function (model) {
           if(dis.find_in_attributes(searchString,model)){
              $("#area_"+model.area_id).show();
              $("#no_area").hide();
           }else{
              $("#area_"+model.area_id).hide();
           }
        });
        
      },
      show_all:function(){
        var dis = this;
        $.each(dis.collection.toJSON(), function(i, item) {
           $("#area_"+item.area_id).show();
        
        }); 
      },

      
    
      
    });

}); 