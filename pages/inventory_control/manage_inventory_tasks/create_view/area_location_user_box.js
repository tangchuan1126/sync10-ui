"use strict";
define(["jquery","bootstrap","metisMenu","CompondList/View","../js/view_config"],
	function($,bootstrap,metisMenu,CompondList,view_config) {
		return function(options) {
			var ListView = {
				
				initialize: function(options) {

					var list = new CompondList(view_config, {
						renderTo: options.el,
						/*dataUrl: options.url,
						PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
						//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",*/
					});
					//绑定事件
					list.render();

					
					//构造一个视图
					this.view = list;
				},
				render:function(options) {
					//先清空之前的数据
					this.view.collection.remove();
					if(typeof options =="object") {
						//清空原来的页面
						$(options.el).empty();
						this.view.collection.el = options.el;
						/*this.view.collection.url = options.url;
						this.view.collection.PostData = (typeof options.queryCondition =="object" )?options.queryCondition:{};*/
					}
					this.view.render();
				},
			};
			ListView.initialize(options);

			return ListView.view;
		};
});
