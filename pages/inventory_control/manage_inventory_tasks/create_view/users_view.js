"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/user_model.js",
  "../create_js/common",
    "mCustomScrollbar",
  "immybox",
  "bootstrap",

], function( $, Backbone, Handlebars, templates, userModel, common) {
    
    return Backbone.View.extend({
      el:"#users_body",
      template:templates.user_view,
      collection:null,
      userId:null,

      initialize:function(data){
        var dis = this;
        dis.collection.fetch({dataType: "json",async: false});
        
      },
      selectDefaultValue:function() {
        var dis = this;
        
        if(dis.userId!=null){
          $("#"+dis.userId).parent().addClass("selected_employee");
          dis.checkConditionToEnableLinks();
        }
      },
      checkConditionToEnableLinks:function(){
        if($(".selected_employee").length>0){
            $("#next_date_selection_div").removeClass("disabled");
           }else{
            $("#next_date_selection_div").addClass("disabled");
           }
      },
      render:function(){
        var dis = this;
        var html=dis.template({users: dis.collection.sort().toJSON()});

        dis.$el.html(html);
        //dis.$el.hide();   


        $(".employee_box").click(function(evt){
           
           $(".employee_box").each(function(i,item){
                  $(item).removeClass("selected_employee");
           });
           $(this).addClass("selected_employee");
           dis.checkConditionToEnableLinks();
           
        });

        //$("#back_area_selection").click(function(){common.show_areas();});
        //$("#next_date_selection").click(function(){common.show_date();});
        $("#assign_user_ok").click(function(){common.assign_user_ok();})
        $("#assign_user_cancel").click(function(){common.assign_user_cancel()})
        //dis.selectDefaultValue();

        var list_unstyled = $("#users_body .employee_box_div");
          list_unstyled.mCustomScrollbar({
            axis: "y",
            scrollbarPosition: "inside"
          });
          //list_unstyled.find(".mCSB_scrollTools").css("right", "-22px");
      },
      
    });

}); 