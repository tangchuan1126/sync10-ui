"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#create_task_priority_view",
      template:templates.immybox_view,
      task:null,

      render:function(data){
        var dis = this;
        dis.task = data.task;
        var html=dis.template({divId:"create_task_priority_div", name:"Task Priority", inputId:"create_task_priority", selectedInput:"create_selected_task_priority"});
        dis.$el.html(html);
        

        var typeImmydata = {
          renderTo: "#create_task_priority_div",
          dataUrl: [{text:"Low",value:"1"},{text:"Medium",value:"2"},{text:"High",value:"3"}], 
          inputId: "#create_task_priority",
          selectedInputId: "#create_selected_task_priority",
          defaultValue: dis.task.PRIORITY?dis.task.PRIORITY:1
        };
        
        var TypeImmy = new ImmyBox(typeImmydata);
        TypeImmy.render();
        
      },

    
      
    });

}); 