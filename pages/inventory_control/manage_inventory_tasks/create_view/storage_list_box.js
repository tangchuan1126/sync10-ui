"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "../create_templates/templates.amd.js",
  "../create_model/storage_model.js",
  "../create_model/area_model.js",
  "../create_model/user_model.js",
  "../create_model/task_request_model",
  "../create_model/title_model",
  "./area_list_box.js",
  "./title_list_box.js",
  "./users_view.js",
  "./deadline_view.js",
  "oso.lib/InventoryControls/Inventory",
    "../create_js/common",
  "../js/ImmyboxControl",
  "bootstrap",


], function( $, Backbone, Handlebars, templates,storage_models,area_models, userModel,task_model,title_models, areaListBoxView , titleListBoxView, userView,deadlineView, InventoryControl, common,ImmyBox) {
    
    return Backbone.View.extend({
      el:"#create_warehouse_view",
      template:templates.immybox_view,
      collection: new storage_models.StorageImmyCollection(),
      task:null,

      initialize:function(data){
        var dis = this;
        dis.task = data.task;
       
      },
      
      render:function(){
        var dis = this;
        dis.$el.empty();
        dis.$el.html("");
        var html=dis.template({divId:"create_warehouse_div", name:"Warehouse", inputId:"create_warehouse", selectedInput:"create_selected_warehouse"});
        dis.$el.html(html);
        
        var titleImmydata = {
          renderTo: "#create_warehouse_div",
          dataUrl: dis.collection,
          placeHolder:"please select", 
          inputId: "#create_warehouse",
          selectedInputId: "#create_selected_warehouse",
          defaultValue: dis.task?dis.task.WAREHOUSE_ID:"0"
        };

        var titleImmy = new ImmyBox(titleImmydata);
        titleImmy.on("events.change",function(){dis.pull_areas();});
        titleImmy.render();
        
        
      },

      pull_areas:function(){
        var dis=this;
        var val = $("#create_selected_warehouse").val();
        
        common.hideError();
        if(val!="0"){
          common.show_other_fields();
          $("#toc_area").empty();
          var titleListBox = new titleListBoxView();
          titleListBox.render({task:dis.task});
          new deadlineView({task:dis.task}).render();
        }else{
          common.hide_other_fields();
        }
        
      },
      
    });

}); 