"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var UserModel = Backbone.Model.extend({
          idAttribute: "user_id",
        
          parse: function(data){
            this.user_name = data.USER_NAME;
            this.user_id = data.USER_ID;
            this.count = data.TASK_COUNT;
            return this;
          }
       });
       
      var UserCollection =  Backbone.Collection.extend({
           model: UserModel,
           

          comparator: function(item) {
                return [item.get('count'),item.get('user_name')];
            },
       });

      return {
	      UserModel:UserModel,
	      UserCollection:UserCollection,

	    };




}); //page_init