"use strict";
define([
  "../config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      
      
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "SAC_ID",
          parse:function(data){
            
            if(data["APPROVE_STATUS"] == 1){
              data["pending"] = true;
              
            }else{
              data["pending"] = false;
            }
            
            return data;
          }
       });
       
      var DifferenceCollection =  Backbone.Collection.extend({
           model: DifferenceModel,
           url: config.containerDifferencesForLocation,
           selected:"",
           
           initialize: function(data){
            
              this.url = this.url+"?slc_id="+data.locationId+"&instance_id="+data.instanceId;
              
          }

       });

      return {
        DifferenceModel:DifferenceModel,
        DifferenceCollection:DifferenceCollection,

      };




}); //page_init