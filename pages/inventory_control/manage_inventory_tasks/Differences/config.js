define({
    
    locationDifferencesForArea: "/Sync10/_inventoryControl/cycleCount/search/scannedAreaLocations",
    containerDifferencesForLocation:"/Sync10/_inventoryControl/cycleCount/search/locationDifferences",
    processDifference:"/Sync10/_inventoryControl/cycleCount/update/difference",
    reassignVerificationTask: "/Sync10/_inventoryControl/cycleCount/update/reassignVerificationTask?task=",
    systemLocation: "/Sync10/_inventoryControl/cycleCount/search/systemLocation?container_id=",
    addVerificationTask: "/Sync10/_inventoryControl/cycleCount/add/verificationTask",
    doesCodeExists: "/Sync10/_inventoryControl/cycleCount/search/isCodeExsiting?code=",
});
