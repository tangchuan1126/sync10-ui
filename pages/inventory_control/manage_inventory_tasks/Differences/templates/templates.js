(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['assign_users'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"\">\r\n	<div class=\"body panel-body\">\r\n		<div class=\"employee_box_div\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.users : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\r\n		<div class=\"clear\"></div>\r\n	</div>\r\n	<div class=\"panel-footer\">\r\n		<a  id=\"verification_assign_user_ok\" class=\"btn btn-primary disabled\">\r\n		<span class=\"glyphicon glyphicon-ok\"></span> Ok</a>\r\n		<a  id=\"verification_assign_user_cancel\" class=\"btn btn-warning\">\r\n		<span class=\"glyphicon glyphicon-cancel\"></span> Cancel</a>\r\n	</div>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			<div class=\"employee_box\" >\r\n				<div class=\"employee_name\" data-v_assign_user-id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.user_id : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.user_name : depth0), depth0))
    + "</div>\r\n				<div class=\"count\">"
    + escapeExpression(lambda((depth0 != null ? depth0.count : depth0), depth0))
    + "</div>\r\n				<div class=\"clear\"></div>\r\n			</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['container_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "	<ul class=\"nav nav-tabs\" id=\"containerTabs\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " 	</ul>\r\n 	<div class=\"tab-content\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <li id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + "\">\r\n            <a href=\"#"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\" data-toggle=\"tab\" >\r\n                "
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\r\n                <span class=\"\" >&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n            </a>\r\n        </li>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "		<div class=\"tab-pane\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">\r\n           \r\n		</div>\r\n";
},"6":function(depth0,helpers,partials,data) {
  return "<div class=\"alert alert-info\" role=\"alert\">\r\n  \r\n  No Difference found.\r\n</div>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(6, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['container_tree_differences'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "               <span class=\"parent\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                    <span class=\"child product\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.systemTree : depth0)) != null ? stack1.CHILDREN : stack1), {"name":"each","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                        <span class=\"child\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.program(8, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"6":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                            <span class=\"child1 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"8":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"9":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                <span class=\"child1\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(10, data),"inverse":this.program(12, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"10":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                    <span class=\"child2 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"12":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"13":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                        <span class=\"child3\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(14, data),"inverse":this.program(16, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"14":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                            <span class=\"child3 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"16":function(depth0,helpers,partials,data) {
  return "                                            \r\n";
  },"18":function(depth0,helpers,partials,data) {
  return "               <span class=\"new\">New to system</span>\r\n";
  },"20":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "               <span class=\"parent\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CONTAINER : stack1), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1), {"name":"if","hash":{},"fn":this.program(21, data),"inverse":this.program(23, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"21":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                  <span class=\"child product\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.P_NAME : stack1), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.QUANTITY : stack1), depth0))
    + "</span>\r\n";
},"23":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.physicalTree : depth0)) != null ? stack1.CHILDREN : stack1), {"name":"each","hash":{},"fn":this.program(24, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"24":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                        <span class=\"child\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(25, data),"inverse":this.program(27, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"25":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                          <span class=\"child1 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"27":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(28, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"28":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                              <span class=\"child1\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(29, data),"inverse":this.program(31, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"29":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                <span class=\"child2 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"31":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.CHILDREN : depth0), {"name":"each","hash":{},"fn":this.program(32, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"32":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                                  <span class=\"child2\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.P_NAME : depth0), {"name":"if","hash":{},"fn":this.program(33, data),"inverse":this.program(35, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"33":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                                      <span class=\"child3 product\">"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"35":function(depth0,helpers,partials,data) {
  return "                                      \r\n";
  },"37":function(depth0,helpers,partials,data) {
  return "              <span class=\"missing\">Missing now</span>\r\n";
  },"39":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing, buffer = "  <table class=\"table table-striped\" class=\"count_diff_table\" >\r\n        <thead>\r\n            <th>Product Name</th>\r\n            <th>System Count</th>\r\n            <th>Physical Count</th>\r\n            \r\n        </thead>\r\n        <tbody >\r\n";
  stack1 = ((helper = (helper = helpers.calculatedDiffs || (depth0 != null ? depth0.calculatedDiffs : depth0)) != null ? helper : helperMissing),(options={"name":"calculatedDiffs","hash":{},"fn":this.program(40, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.calculatedDiffs) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </tbody>\r\n  </table>      \r\n";
},"40":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    \r\n          <tr >\r\n            <td>"
    + escapeExpression(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td>"
    + escapeExpression(((helper = (helper = helpers.systemCount || (depth0 != null ? depth0.systemCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"systemCount","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td class=\"difference\">"
    + escapeExpression(((helper = (helper = helpers.physicalCount || (depth0 != null ? depth0.physicalCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"physicalCount","hash":{},"data":data}) : helper)))
    + "</td>\r\n                  \r\n          </tr>\r\n";
},"42":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div class=\"approval_div\">\r\n          <div class=\"comments_div\">\r\n          <input type=\"hidden\" name=\"diff_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.conDiffId || (depth0 != null ? depth0.conDiffId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"conDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n              <label>Comments</label>\r\n              <textarea  name=\"comments\"></textarea>\r\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n          </div>\r\n          <div class=\"approve_btn_div\">\r\n          <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\r\n            <span class=\"glyphicon glyphicon-ok\">\r\n            Approve\r\n          </a>\r\n          <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\r\n            <span class=\"glyphicon glyphicon-remove\">\r\n            Reject\r\n          </a> \r\n          </div>\r\n    </div>\r\n";
},"44":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "  <div class=\"processed_div\">\r\n\r\n    <div class=\"username\">\r\n       <b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_TITLE : stack1), depth0))
    + "</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.PROCESSED_BY_NAME : stack1), depth0))
    + "\r\n    </div>\r\n    <div class=\"username\">\r\n       <b>Comments:</b> "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.COMMENTS : stack1), depth0))
    + "\r\n    </div>\r\n  </div>  \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "\r\n<div class=\"count_container\">\r\n    <div id=\"container_count_dif_div\">\r\n        <div class=\"username\">\r\n           <b>Scanned By:</b> "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.containerDifference : depth0)) != null ? stack1.USERNAME : stack1), depth0))
    + "\r\n        </div>\r\n        <div id=\"system_physical_div\">\r\n          <div class=\"system_div\">\r\n            <h5>System Containers</h5>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.systemTree : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(18, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </div>   \r\n          <div class=\"physical_div\">\r\n            <h5>Physical Containers</h5>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.physicalTree : depth0), {"name":"if","hash":{},"fn":this.program(20, data),"inverse":this.program(37, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </div>   \r\n          <div class=\"clear\"></div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n\r\n\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.calculatedDiffs : depth0), {"name":"if","hash":{},"fn":this.program(39, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(42, data),"inverse":this.program(44, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});
templates['count_differences'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, blockHelperMissing=helpers.blockHelperMissing, buffer = "<div class=\"count_container\">\r\n  <div class=\"username\">\r\n     <b>Scanned By:</b> "
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "\r\n  </div>\r\n  <table class=\"table table-striped\" class=\"count_diff_table\" >\r\n      <thead>\r\n          <th>Product Name</th>\r\n          <th>System Count</th>\r\n          <th>Physical Count</th>\r\n          \r\n      </thead>\r\n      <tbody >\r\n";
  stack1 = ((helper = (helper = helpers.productDifferences || (depth0 != null ? depth0.productDifferences : depth0)) != null ? helper : helperMissing),(options={"name":"productDifferences","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.productDifferences) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  buffer += "      </tbody>\r\n  </table>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(4, data),"inverse":this.program(6, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "  \r\n        <tr >\r\n          <td>"
    + escapeExpression(((helper = (helper = helpers.P_NAME || (depth0 != null ? depth0.P_NAME : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"P_NAME","hash":{},"data":data}) : helper)))
    + "</td>\r\n          <td>"
    + escapeExpression(((helper = (helper = helpers.SYSTEM_COUNT || (depth0 != null ? depth0.SYSTEM_COUNT : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"SYSTEM_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\r\n          <td class=\"difference\">"
    + escapeExpression(((helper = (helper = helpers.PHYSICAL_COUNT || (depth0 != null ? depth0.PHYSICAL_COUNT : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"PHYSICAL_COUNT","hash":{},"data":data}) : helper)))
    + "</td>\r\n        </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div class=\"approval_div\">\r\n          <div class=\"comments_div\">\r\n              <input type=\"hidden\" name=\"diff_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.productDiffId || (depth0 != null ? depth0.productDiffId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"productDiffId","hash":{},"data":data}) : helper)))
    + "\"/>\r\n              <label>Comments</label>\r\n              <textarea  name=\"comments\"></textarea>\r\n              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n          </div>\r\n          <div class=\"approve_btn_div\">\r\n            <a name=\"approve_btn\" class=\"btn btn-success\" href=\"#\" title=\"Approve\">\r\n              <span class=\"glyphicon glyphicon-ok\">"
    + escapeExpression(((helper = (helper = helpers.ID || (depth0 != null ? depth0.ID : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ID","hash":{},"data":data}) : helper)))
    + "\r\n              Approve\r\n            </a>\r\n            <a name=\"reject_btn\" class=\"btn btn-danger\" href=\"#\" title=\"Reject\">\r\n              <span class=\"glyphicon glyphicon-remove\">\r\n              Reject\r\n            </a> \r\n          </div>\r\n    </div>\r\n";
},"6":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div class=\"processed_div\">\r\n\r\n      <div class=\"username\">\r\n         <b>"
    + escapeExpression(((helper = (helper = helpers.processed_by_title || (depth0 != null ? depth0.processed_by_title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"processed_by_title","hash":{},"data":data}) : helper)))
    + "</b>&nbsp;"
    + escapeExpression(((helper = (helper = helpers.processed_by || (depth0 != null ? depth0.processed_by : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"processed_by","hash":{},"data":data}) : helper)))
    + "\r\n      </div>\r\n      <div class=\"username\">\r\n         <b>Comments:</b> "
    + escapeExpression(((helper = (helper = helpers.comments || (depth0 != null ? depth0.comments : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"comments","hash":{},"data":data}) : helper)))
    + "\r\n      </div>\r\n    </div>  \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.productDifferences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            \r\n\r\n";
},"useData":true});
templates['create_verification_task'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "						<div class=\"v_t_assign_user_btn_div\">\r\n							<button type=\"button\" id=\"create_verification_assign_btn\" class=\"assign_verification_task_button btn btn-sm btn-primary  disabled\">Assign User</button>\r\n						</div>	\r\n						<div id=\"v_error_msg\" class=\"alert alert-danger\"></div>\r\n						\r\n						<table class=\"table table-striped\" width=\"100%\" >\r\n						<thead>\r\n						  	<th><input type=\"checkbox\" id=\"create_verification_select_all\"/></th>\r\n							<th>Area</th>\r\n					  		<th>Location</th>\r\n						  	<th>Title</th>\r\n						  	<th>Container</th>\r\n						  	<th>Scanned by</th>\r\n						   	<th >Assigned User</th>\r\n\r\n						</thead>\r\n						<tbody id=\"create_verification_differences_table_body\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "						</tbody>\r\n						</table>\r\n\r\n						\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "						   <tr>\r\n						          <td><input type=\"checkbox\" id=\"create_verification_checkbox_"
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\" data-diff_id="
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + " class=\"create_verification_difference_list_checkbox\"/></td>\r\n						   		   <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\r\n						         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "</td>\r\n						         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.TITLE_NAME : depth0), depth0))
    + "</td>\r\n						         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.LP_CONTAINER : depth0), depth0))
    + "</td>\r\n						         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.SCANNED_BY_NAME : depth0), depth0))
    + "</td>\r\n						         <td></td>\r\n						   </tr>\r\n						   \r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"create_verification_task_page\">\r\n	<div class=\"div-table\" id=\"v_attr_table\">\r\n        <div class=\"div-table-row\">\r\n	        <div class=\"div-table-cell\">\r\n	          <div class=\"input-group required\" >\r\n	          	<div class=\"input-group-addon\">Task Code</div>\r\n				<input type=\"text\" name=\"verification_task_name\" id=\"verification_task_name\" class=\"form-control\" maxlength=\"30\"/>\r\n	          </div>\r\n	        </div>\r\n	        <div class=\"div-table-cell\" >\r\n	          <div class=\"input-group required\" id=\"create_verification_task_priority_view\">\r\n\r\n	          </div>\r\n	        </div>\r\n	        <div class=\"div-table-cell\" >\r\n	          <div class=\"input-group required\" >\r\n	          	<div class=\"input-group-addon\">End Date</div>\r\n				<input type=\"text\" name=\"verification_end_date\" id=\"verification_end_date\" class=\"form-control\" />\r\n	          </div>\r\n	        </div>\r\n      	</div>\r\n	</div>\r\n	\r\n	<div class=\"list_users_table_wrapper\">\r\n		<div class=\"div-table\" id=\"v_d_table\">\r\n\r\n			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell differences_list\">\r\n					<!-- -->\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n					<!-- -->\r\n				</div>\r\n				<div class=\"div-table-cell users_list panel panel-default\" id=\"list_of_users_verification_task\"></div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n\r\n\r\n</div>";
},"useData":true});
templates['difference_info'] = template({"1":function(depth0,helpers,partials,data) {
  return "";
},"3":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell\"><b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.PROCESSED_TITLE : stack1), depth0))
    + "</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.REVIEWED_BY_NAME : stack1), depth0))
    + "</div>\r\n			</div>\r\n			\r\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell\"><b>Comments:</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.COMMENTS : stack1), depth0))
    + "</div>\r\n			</div>\r\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "		    <div class=\"approval_div\">\r\n		          <div class=\"comments_div\">\r\n		          <input type=\"hidden\" name=\"diff_id\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.ID : stack1), depth0))
    + "\"/>\r\n		          <input type=\"hidden\" name=\"type\" value=\"1\"/>\r\n		              <label class=\"required\">Comments</label>\r\n		              <textarea  name=\"comments\"></textarea>\r\n		              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n		          </div>\r\n		          <div class=\"approve_btn_div\">\r\n			          <a name=\"approve_btn\" class=\"btn btn-success\" title=\"Approve\">\r\n			            <span class=\"glyphicon glyphicon-ok\">\r\n			            Approve\r\n			          </a>\r\n			         \r\n			          <a name=\"reject_btn\" class=\"btn btn-danger\" title=\"Reject\">\r\n			            <span class=\"glyphicon glyphicon-remove\">\r\n			            Reject\r\n			          </a> \r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.IS_VERIFICATION_TASK_CREATABLE : stack1), {"name":"if","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		          </div>\r\n		    </div>\r\n		 \r\n";
},"8":function(depth0,helpers,partials,data) {
  return "		          \r\n							<button type=\"button\" name=\"create_verification_task_diff_btn\" class=\"create_verification_task_diff_btn btn btn-primary\">Create Verification Task</button>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "<div class=\"count_container_content\">\r\n	<div class=\"count_container\">\r\n	<input type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.WAREHOUSE_ID : stack1), depth0))
    + "\" id=\"difference_warehouse_id\"/>\r\n		<div class=\"div-table\" style=\"width:100%\">\r\n			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell\"><b>Scanned By:</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.SCANNED_BY_NAME : stack1), depth0))
    + "</div>\r\n				<div class=\"div-table-cell\"><b>Area:&nbsp;</b>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.AREA_NAME : stack1), depth0))
    + "</div>\r\n				<div class=\"div-table-cell\"><b>Location:</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.LOCATION_NAME : stack1), depth0))
    + "</div>\r\n				<div class=\"div-table-cell\"><b>Lot Number:</b>&nbsp;"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.LOT_NUMBER : stack1), depth0))
    + "</div>\r\n			</div>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.STATUS : stack1), 1, {"name":"ifCond","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "		</div>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.STATUS : stack1), 1, {"name":"ifCond","hash":{},"fn":this.program(1, data),"inverse":this.program(5, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<div id=\"container_count_dif_div\">\r\n			<div id=\"system_physical_div\">\r\n      			<div class=\"system_div\">\r\n      				<h5>System View</h5>\r\n      			</div>\r\n      			<div class=\"alert alert-info info_div\"></div>\r\n      			<div class=\"physical_div\">\r\n        			<h5>Physical View</h5>\r\n        		</div>\r\n        		<div class=\"clear\"></div>\r\n        	</div>\r\n		</div>\r\n\r\n		<div class=\"product_count_difference_table\">\r\n\r\n		</div>\r\n\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.difference : depth0)) != null ? stack1.STATUS : stack1), 1, {"name":"ifCond","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		  <div id=\"difference_verification_task_list\"></div>\r\n	</div>\r\n</div>";
},"useData":true});
templates['difference_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"verification_task_button_div\">\r\n<button type=\"button\" id=\"create_verification_task_btn\" class=\"verification_task_button btn btn-primary disabled\">Create Verification Task</button>\r\n\r\n</div>\r\n<div class=\"clear\"></div>\r\n<table class=\"table table-striped\" width=\"100%\" >\r\n<thead>\r\n  <th><input type=\"checkbox\" id=\"differences_select_all\"/></th>\r\n	<th>Area</th>\r\n  <th>Location</th>\r\n  <th>Title</th>\r\n  <th class=\"sort_link\"><a class=\""
    + escapeExpression(((helper = (helper = helpers.post_date_css || (depth0 != null ? depth0.post_date_css : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"post_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"created_at\">Scanned Date</a></th>\r\n  <th>Scanned by</th>\r\n   <th> Status</th>\r\n   <th>Reviewer</th>\r\n   <th class=\"sort_link\"><a class=\""
    + escapeExpression(((helper = (helper = helpers.approve_date_css || (depth0 != null ? depth0.approve_date_css : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"approve_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"processed_at\">Reviewed Date</a></th>\r\n   <th></th>\r\n\r\n</thead>\r\n<tbody id=\"differences_table_body\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</tbody>\r\n</table>\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "   <tr>\r\n          <td>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.IS_VERIFICATION_TASK_CREATABLE : depth0), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "          </td>\r\n   		   <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.TITLE_NAME : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.CREATED_AT : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.USER_NAME : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.STATUS_STRING : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.PROCESSED_BY_NAME : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.PROCESSED_AT : depth0), depth0))
    + "</td>\r\n         <td><button type=\"button\" =\"review_btn\" class=\"review_btn_difference btn btn-default btn-sm\" title=\"Review\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">Review</button></td>\r\n   </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <input type=\"checkbox\" id=\"differences_checkbox_"
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + "\" data-diff_id="
    + escapeExpression(lambda((depth0 != null ? depth0.ID : depth0), depth0))
    + " \r\n            class=\"difference_list_checkbox\"/>\r\n";
},"5":function(depth0,helpers,partials,data) {
  return "<div class=\"no_data\">\r\n   No differences found\r\n</div>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(5, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "<div id=\"pagination\" class=\"pagebox\">\r\n   \r\n</div>";
},"useData":true});
templates['difference_product_info'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", helperMissing=helpers.helperMissing, blockHelperMissing=helpers.blockHelperMissing, buffer = "  <table class=\"table table-striped\" class=\"count_diff_table\" >\r\n        <thead>\r\n            <th>Model Number</th>\r\n            <th>System Count</th>\r\n            <th>Physical Count</th>\r\n            <th>Difference Count</th>\r\n        </thead>\r\n        <tbody >\r\n";
  stack1 = ((helper = (helper = helpers.calculatedDiffs || (depth0 != null ? depth0.calculatedDiffs : depth0)) != null ? helper : helperMissing),(options={"name":"calculatedDiffs","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.calculatedDiffs) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </tbody>\r\n  </table>      \r\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    \r\n          <tr >\r\n            <td>"
    + escapeExpression(((helper = (helper = helpers.MODEL_NUMBER || (depth0 != null ? depth0.MODEL_NUMBER : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"MODEL_NUMBER","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td>"
    + escapeExpression(((helper = (helper = helpers.systemCount || (depth0 != null ? depth0.systemCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"systemCount","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td class=\"difference\">"
    + escapeExpression(((helper = (helper = helpers.physicalCount || (depth0 != null ? depth0.physicalCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"physicalCount","hash":{},"data":data}) : helper)))
    + "</td>\r\n            <td class=\"\">"
    + escapeExpression(((helper = (helper = helpers.differenceCount || (depth0 != null ? depth0.differenceCount : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"differenceCount","hash":{},"data":data}) : helper)))
    + "</td>            \r\n          </tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.calculatedDiffs : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n";
},"useData":true});
templates['difference_tree'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "  <div class=\"parent_parent_container_div\" >\r\n    <span class=\"parent cntr_"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_ID : stack1), depth0))
    + " cntr\" data-container_id=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_ID : stack1), depth0))
    + "\" data-type=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_TYPE : stack1), depth0))
    + "\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CONTAINER_NAME : stack1), depth0))
    + "</span>\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.PRODUCTS : stack1), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.tree : depth0)) != null ? stack1.CHILDREN : stack1), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "  </div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "      <span class=\"child product prod_"
    + escapeExpression(lambda((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + " prod\" data-product_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.MODEL_NUMBER : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "            <div class=\"parent_child_container_div\" >\r\n              <span class=\"child cntr_"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + " cntr\" data-container_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER_ID : depth0), depth0))
    + "\" data-type=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER_TYPE : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER_NAME : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.PRODUCTS : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            </div>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.PRODUCTS : depth0), {"name":"each","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"6":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                  <span class=\"child1 product prod_"
    + escapeExpression(lambda((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + " prod\" data-product_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.PC_ID : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.MODEL_NUMBER : depth0), depth0))
    + "&nbsp;&nbsp;&nbsp;x&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.QUANTITY : depth0), depth0))
    + "</span>\r\n";
},"8":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.treeType : depth0), "system", {"name":"ifCond","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.treeType : depth0), "physical", {"name":"ifCond","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.treeType : depth0), "verification system", {"name":"ifCond","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.treeType : depth0), "verification physical", {"name":"ifCond","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"9":function(depth0,helpers,partials,data) {
  return "    <span class=\"new\">New to system</span>\r\n";
  },"11":function(depth0,helpers,partials,data) {
  return "    <span class=\"missing\">Missing now</span>\r\n";
  },"13":function(depth0,helpers,partials,data) {
  return "    <span class=\"\">--</span>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.tree : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(8, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + " \r\n\r\n\r\n  ";
},"useData":true});
templates['difference_verification_task'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "<div class=\"verification_task_block \" id=\"verification_task_"
    + escapeExpression(lambda((depth0 != null ? depth0.VERIFICATION_TASK_ID : depth0), depth0))
    + "\">\r\n	<input type=\"hidden\" class=\"verification_task_id_value\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.VERIFICATION_TASK_ID : depth0), depth0))
    + "\"/>\r\n	<input type=\"hidden\" class=\"verification_task_instance_id_value\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.INSTANCE_ID : depth0), depth0))
    + "\"/>\r\n	<h3>"
    + escapeExpression(lambda((depth0 != null ? depth0.SERIAL_NUMBER : depth0), depth0))
    + " Verification Task: "
    + escapeExpression(lambda((depth0 != null ? depth0.CODE : depth0), depth0))
    + "</h3>\r\n	<div class=\"verification_task_block_info\">\r\n		<div class=\"div-table\">\r\n			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell\"><b>Task Status:</b>&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.STATUS_STRING : depth0), depth0))
    + "</div>\r\n				\r\n				<div class=\"div-table-cell\"><b>Assigned To:</b>&nbsp;<span class=\"v_assigned_user_name\">"
    + escapeExpression(lambda((depth0 != null ? depth0.EMPLOYE_NAME : depth0), depth0))
    + "</span></div>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.STATUS : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</div>\r\n			<div class=\"div-table-row\">\r\n				<div class=\"div-table-cell\"><b>Task Priority:</b>&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.PRIORITY_STRING : depth0), depth0))
    + "</div>\r\n				<div class=\"div-table-cell\"><b>End Before:</b>&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "</div>\r\n			</div>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.STATUS : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "		</div>\r\n	</div>	\r\n	<div class=\"verification_task_reassign_block\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.SHOW_TREE : depth0), {"name":"if","hash":{},"fn":this.program(10, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<div id=\"product_count_difference_table\"></div>\r\n";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.MAIN_DIFFERENCE_STATUS : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(12, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</div>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.ifCond || (depth0 && depth0.ifCond) || helperMissing).call(depth0, (depth0 != null ? depth0.MAIN_DIFFERENCE_STATUS : depth0), 1, {"name":"ifCond","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"3":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "					<div class=\"div-table-cell reassign_btn_cell\">\r\n						<input type=\"hidden\" class=\"verification_task_start_date\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.START_DATE : depth0), depth0))
    + "\"/>\r\n						<input type=\"hidden\" class=\"verification_task_end_date\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.END_DATE : depth0), depth0))
    + "\"/>\r\n						<button type=\"button\" class=\"verification_task_reassign btn btn-default btn-sm\" title=\"Reassign\" data-verification_task_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.VERIFICATION_TASK_ID : depth0), depth0))
    + "\">Reassign</button>\r\n					</div>\r\n";
},"5":function(depth0,helpers,partials,data) {
  return "";
},"7":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "				<div class=\"div-table-row\">\r\n					<div class=\"div-table-cell\"><b>Scanned By:</b>&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.SCANNED_BY_NAME : depth0), depth0))
    + "</div>\r\n					<div class=\"div-table-cell\"><b>Scanned at:</b>&nbsp;"
    + escapeExpression(lambda((depth0 != null ? depth0.SCANNED_AT : depth0), depth0))
    + "</div>\r\n				</div>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.RESULT_STRING : depth0), {"name":"if","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"8":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "				<div class=\"div-table-row\">\r\n					<div class=\"div-table-cell verification_result\"><b>Result:</b>&nbsp;Verification <b>"
    + escapeExpression(((helper = (helper = helpers.RESULT_STRING || (depth0 != null ? depth0.RESULT_STRING : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"RESULT_STRING","hash":{},"data":data}) : helper)))
    + "</b> </div>\r\n				</div>\r\n";
},"10":function(depth0,helpers,partials,data) {
  return "		<div id=\"container_count_dif_div\">\r\n		\r\n			<div class=\"system_physical_div\">\r\n      			<div class=\"system_div\">\r\n      				<h5>System View</h5>\r\n      			</div>\r\n      			<div class=\"alert alert-info info_div\"></div>\r\n      			<div class=\"physical_div\">\r\n        			<h5>Physical View</h5>\r\n        		</div>\r\n        		<div class=\"clear\"></div>\r\n        	</div>\r\n        \r\n		</div>\r\n";
  },"12":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.SHOW_APPROVE : depth0), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"13":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			    <div class=\"approval_div\">\r\n			          <div class=\"comments_div\">\r\n				          <input type=\"hidden\" name=\"verification_instance_id\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.INSTANCE_ID : depth0), depth0))
    + "\"/>\r\n				          <input type=\"hidden\" name=\"type\" value=\"2\"/>\r\n			              <label class=\"required\">Comments</label>\r\n			              <textarea  name=\"comments\"></textarea>\r\n			              <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n			          </div>\r\n			          <div class=\"approve_btn_div\">\r\n				          <a name=\"verification_approve_btn\" class=\"btn btn-success\" title=\"Approve\">\r\n				            <span class=\"glyphicon glyphicon-ok\"></span> Approve</a>\r\n				      </div>\r\n			    </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
templates['location_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "  <div id=\"location_differences_scroll_container\">\r\n    <div class=\"panner scroll_up\" data-scroll-modifier='-1'>&nbsp;</div>\r\n    <div id=\"location_differences_scroll\">\r\n      \r\n      <div id=\"location_differences\">\r\n        <div class=\"tabbable tabs-left\">\r\n          <ul class=\"nav nav-tabs\" id=\"locationTabs\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"panner scroll_down\" data-scroll-modifier='1'>&nbsp;</div>\r\n  </div>\r\n  <div id=\"container_differences\">  \r\n    <div class=\"tab-content\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n  </div>\r\n  <div class=\"clear\"></div>\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "              <li id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_ID : depth0), depth0))
    + "\">\r\n                <a href=\"#"
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_ID : depth0), depth0))
    + "\">\r\n                  "
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\r\n                  <span class=\"\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n                </a>\r\n              </li>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        \r\n        <div class=\"tab-pane\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\" >\r\n            "
    + escapeExpression(lambda((depth0 != null ? depth0.LOCATION_NAME : depth0), depth0))
    + "\r\n        </div>\r\n        \r\n";
},"6":function(depth0,helpers,partials,data) {
  return "\r\n    <div class=\"no_data\">\r\n       No Data\r\n    </div>\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"back_btn_view_detail_div\">\r\n  <a href=\"#\" id=\"back_btn_view_detail\" class=\"btn btn-default btn-sm\">Back to Areas</a>\r\n</div>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(6, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
})();