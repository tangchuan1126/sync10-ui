"use strict";
define(
  [
  "../config",
  "jquery",
  "backbone",
  "handlebars",
  "../templates/templates.amd",
  'handlebars_ext',
  "../../create_js/common",
  "../model/location_difference_model.js",
  "../model/count_difference_model.js",
  "./count_differences.js",

], 
function( config, $, Backbone, Handlebars, templates , HandleBarsExt,common, difference_models, count_difference_models, countDifferencesView) {

    return Backbone.View.extend({
      template: templates.location_differences_list,
      el:"#location_differences_container",
      scrollHandle : 0, 
      scrollStep : 50, 
      instanceId:0,
      locationsModel:null,
      parent:null,

      render:function(data){
        var dis = this;
        dis.instanceId = data.instanceId;
        if(dis.parent==null){
          dis.parent = data.parent;
        }
        dis.locationsModel= new difference_models.DifferenceModel(data);
        dis.locationsModel.fetch({dataType: "json",async: false});
        
        var html =dis.template({differences: dis.locationsModel.get("locations").toJSON()});
        dis.$el.html("");
        dis.$el.html(html);
        
        var selectedTab = -1;
        var selectedContent = -1;
        var selectedSaaId = -1;
        $('#locationTabs a').click(function (evt) {
          evt.preventDefault();
          
          var saa_id,sal_id,slc_position_all;
          if($(evt.target).attr('href')){
            saa_id = $(evt.target).attr("name");
            sal_id = $(evt.target).parent().attr("id");
            slc_position_all = $(evt.target).find($("span")).attr("name");
          }else{
            saa_id = $(evt.target).parent().attr("name");
            sal_id = $(evt.target).parent().parent().attr("id");
            slc_position_all = $(evt.target).attr("name");
          }
          
          
          new countDifferencesView(dis).render({locationId:sal_id,instanceId:dis.instanceId,areaId:dis.locationsModel.get("AREA_ID"),containerId:slc_position_all});
          
        });

        $.each(dis.locationsModel.get("locations").toJSON(), function(i, item) {
          
          var className = "pending_loc"
          switch(item.STATUS){
            case 1:
              className = "";
              break;
            case 2:
              className = "";
              break;
            case 3:
              className = "done_loc";
              break;
            case 4:
              className = "pending_loc";
              break;
            case 5:
              className = "approved_loc";
              break;

          }
          
            //$("#"+item.LOCATION_ID+" > a > span").addClass("approved");
          $("#"+item.LOCATION_ID).addClass(className);

          if(selectedTab==-1 && item.STATUS==4){
            selectedTab = item.LOCATION_ID;
            selectedContent = item.LOCATION_NAME;
           // selectedSaaId = item.SAA_ID;
          }
        });

         if(selectedTab==-1  &&  dis.locationsModel.get("locations").length>0){ // incase all are approved, select first
          var selected = dis.locationsModel.get("locations").models[0];
          selectedTab = selected.attributes["LOCATION_ID"];
          selectedContent = selected.attributes["LOCATION_NAME"];
          //selectedSaaId = selected.attributes["SAA_ID"];;

        }
        new countDifferencesView(dis).render({locationId:selectedTab,instanceId:dis.instanceId,areaId:dis.locationsModel.get("AREA_ID"),containerId:selectedContent});
        
        $("#"+selectedTab).addClass("active");
        $("#location_differences_scroll").animate({ scrollTop: $("#"+selectedTab).offset().top -400}, 'slow');
        
        $("#"+selectedContent).addClass("active");
        
        dis.enableScrolling();
        //$("#instances_area_progress").hide();
        //$("#location_differences_container").show();
        $("#back_btn_view_detail").click(function(evt){dis.back(evt)});
     },

     events:{
    
     },

     back:function(evt){
        var dis = this;
        /*$("#location_differences_container").hide();
        $("#instances_area_progress").show();*/
        dis.parent.render({});
        //common.slide_progress("right")
        
     },
     enableScrolling:function(){
      var dis = this;
      

        //Start the scrolling process
        $(".panner").on("mouseenter", function () {
            var data = $(this).data('scrollModifier'),
                direction = parseInt(data, 10);
                
            $(this).addClass('active');

            dis.startScrolling(direction, dis.scrollStep);
        });

        //Kill the scrolling
        $(".panner").on("mouseleave", function () {
            dis.stopScrolling();
            $(this).removeClass('active');
        });

    
     },
     startScrolling:function(modifier, step) {
      var dis = this;
        
        if (dis.scrollHandle === 0) {
            dis.scrollHandle = setInterval(function () {
                var parentsc = $("#location_differences_scroll");
                
                var newOffset = parentsc.scrollTop() + (dis.scrollStep * modifier);

                parentsc.scrollTop(newOffset);
            }, 100);
        }
      },

      stopScrolling:function() {
        var dis = this;
            clearInterval(dis.scrollHandle);
            dis.scrollHandle = 0;
        }
     
  });

}); 

