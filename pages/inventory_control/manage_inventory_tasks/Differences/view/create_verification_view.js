"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "art_Dialog/dialog-plus",
  "./task_priority_view",
  "../model/user_task_count_model",
  "mCustomScrollbar",
  "bootstrap",
  "bootstrap.datetimepicker",
  "handlebars_ext",
  "showMessage",

], function( $,config, Backbone, Handlebars, templates,Dialog , priorityView,userModel,mCustomScrollbar) {
    
    return Backbone.View.extend({

      template:templates.create_verification_task,

      initialize:function(data){
        var dis = this;
        dis.parentView = data.parentView;
        dis.locationId = data.locationId;
        dis.areaId = data.areaId;
        dis.instanceId = data.instanceId;
        dis.containerId = data.containerId;
      },
      render:function(data){
        var dis = this;

        dis.collection = data.differences;
        var html=dis.template({differences:data.differences});
        
        
        var dialog = new Dialog({
              content: html,
              lock:true,
              width: 800,
              height: 480,
              title:"Create Verification Task",
              okValue:"Create",
              ok: function (event) {

                 return dis.create_verification_task();
              },
              cancelVal: 'Cancel',
              cancel: function(dialog){this.close();},
              onshow:function() { new priorityView().render();},
          })
        $("#verification_end_date").datetimepicker({ format: "yyyy-mm-dd", startDate:new Date(),
          autoclose:1, minView:2,ValEmptyBtn:function(){dis.check_preview();}
                }).on('changeDate', function(ev){dis.check_preview();
                }).on('clearDate',function(){dis.check_preview();
                }).on('keydown', function(event) {
                  return false;
                });
        $("#create_verification_select_all").click(function(){dis.select_all_checkbox();})  
        $(".create_verification_difference_list_checkbox").click(function(){dis.check_preview();});
        $("#create_verification_assign_btn").click(function(){dis.open_users_view();})  
        $("#v_error_msg").hide();

        dialog.showModal();
      },
      custom_scroll:function(){
        
        var list_unstyled = $("#list_of_users_verification_task .employee_box_div");
        list_unstyled.mCustomScrollbar({
          axis: "y",
          scrollbarPosition: "outside"
        });
        list_unstyled.find(".mCSB_scrollTools").css("right", "8px");
      },

      check_code_exists:function(){
        var result= "";
        $.ajax({
            url:  config.doesCodeExists+$("#verification_task_name").val(),
            type: 'get',
            contentType: 'application/json; charset=UTF-8',
            timeout: 60000,
            cache:false,
            async:false,
            error: function(jqXHR, textStatus, errorThrown) {
              
            },
            success: function(data){
              if(data){
                result =  "task code already exists.<br/>";
              }else{
                result = "";
              }
            }
          });
        return result;
      },

      create_verification_task:function(dialog){
 
       var dialogclos=false;
        var dis = this;
        var create="";
        $("#v_error_msg").html("");
        $("#v_error_msg").hide();
        if($("#verification_task_name").val()==""){create+="please enter task code.<br/>";}
        else{
          create += dis.check_code_exists();
        }
        
        if($("#verification_selected_task_priority").val()=="0"){create+="please select task priority<br/>";}
        if($("#verification_end_date").val()==""){create+="please enter end date.<br/>";}

        var all_assigned = true;
        $.each($("#create_verification_differences_table_body tr"),function(index,item){
          if($(item).children("td").last().html()==""){all_assigned=false;}
        });

        if(!all_assigned){create+="please assign user to all items.<br/>"}
        
        if(create==""){
          
          $("#v_error_msg").html("");
          $("#v_error_msg").hide();

          dialogclos=dis.send_create_request();
        }
        else{
          $("#v_error_msg").html(create);
          $("#v_error_msg").show();
          dialogclos=false;
          
        }

        return dialogclos;

      },
      send_create_request:function(){
        var dis = this;
        var result = false;
        var code = $("#verification_task_name").val()
        var priority = $("#verification_selected_task_priority").val();
        var end_date = $("#verification_end_date").val();
        var instances = [];
        $.each($("#create_verification_differences_table_body tr"),function(index,item){

          var diff_id = $(item).children("td").first().find("input").data('diff_id');
          var user_id = $(item).children("td").last().find("span").data('user_id');
          var assignment = {"user_id":user_id};
          instances.push({"assignment":assignment,"difference_id":diff_id});

        });
        var task = {"code":code,"priority":priority,"end_date":end_date, "instances":instances};

         $.ajax({
            url:  config.addVerificationTask,
            type: 'post',
            timeout: 60000,
            cache:false,
            data: JSON.stringify(task),
            dataType: 'json',
            contentType: "application/json",
            async:false,
            
            error: function(jqXHR, textStatus, errorThrown) {
              showMessage("Error while creating task.","error");
              
            },
            success: function(data){

              if(data.error && data.error == 33){
                $("#v_error_msg").html(data.message);
                $("#v_error_msg").show();
                showMessage(data.message,"error");
                result =  false;
                
              }else if(data.error && data.error == 32){
                $("#v_error_msg").html(data.message);
                $("#v_error_msg").show();
                showMessage(data.message,"error");
                $.each(data.value,function(index,item){
                  $("#create_verification_checkbox_"+item.ID).parent().parent().addClass("highlighted");
                  $("#create_verification_checkbox_"+item.ID).hide();
                  var di = "<div class='remove_difference' data-id='"+item.ID+"' title='remove'>&nbsp;</div>";
                  $("#create_verification_checkbox_"+item.ID).parent().append(di);
                });
                $("#create_verification_task_page .remove_difference").click(function(evt){
                  dis.removeDifference(evt);
                })
                result = false;
                
              }else{
                showMessage("Task has been created.","succeed");
                dis.parentView.render({locationId:dis.locationId,containerId:dis.containerId,areaId:dis.areaId,instanceId:dis.instanceId})
                result =  true;
              }
              
            }
          });
        return result;
      },
      removeDifference:function(evt){
        var dis= this;
        $(evt.currentTarget).parent().parent().remove();
        if($("#create_verification_task_page .differences_list table tr").length == 1){
          $("#v_error_msg").html("No differences to create task for.");
          $("#v_error_msg").show();
        }
      },
      open_users_view:function(){
        $("#v_error_msg").hide();
        $(".art-dialog-footer").hide();
        var dis = this;
        var start_date = new Date().toJSON().substring(0,10);
        var end_date = $("#verification_end_date").val();
        var is_repeating = false;
        var ps_id = dis.collection[0]["WAREHOUSE_ID"];

        var url = config.getUsersAndAssignLoadJSON;
        url += "?ps_id="+ps_id;
        url += "&start_date="+start_date;
        url += "&end_date="+end_date;
        url += "&is_repeating="+is_repeating;

        var collection = new userModel.UserCollection();
        collection.url = url;
        collection.fetch({dataType: "json",async: false});
        var html = templates.assign_users({users:collection.toJSON()})
        $("#list_of_users_verification_task").html(html);

        $("#list_of_users_verification_task .employee_box").click(function(evt){
           $("#list_of_users_verification_task .employee_box").each(function(i,item){
                  $(item).removeClass("selected_employee");
           });
           $(this).addClass("selected_employee");
           dis.check_assign_user_ok();
        }); 
        dis.custom_scroll()

        $("#verification_assign_user_ok").click(function(){dis.verification_assign_user();})
        $("#verification_assign_user_cancel").click(function(){dis.reassign_hide_users();})
        dis.reassign_show_users();
      },

      verification_assign_user:function(){

        var dis = this;
        var name = $("#list_of_users_verification_task .selected_employee .employee_name").html();
        var id = $("#list_of_users_verification_task .selected_employee .employee_name").data("v_assign_user-id");

        dis.add_user_to_selected_differences({userId:id,userName:name});
        dis.reassign_hide_users();
      },

      add_user_to_selected_differences:function(data){
        var dis = this;

        $.each($("#create_verification_differences_table_body :checked"),function(index,item){
          $(item).parent().parent().children("td").last().html("<span data-user_id='"+data.userId+"'>"+data.userName+"</span>");
        });
      },

      check_assign_user_ok:function(){
        var dis = this;
        if($("#list_of_users_verification_task .selected_employee").length){
          $("#verification_assign_user_ok").removeClass("disabled");
        }else{
          $("#verification_assign_user_ok").addClass("disabled");
        }
      },

      reassign_show_users : function(){
        var l='-=500px';
        $("#v_d_table .users_list").animate({left:l},500);
      },
      reassign_hide_users : function(){
        var l='+=500px';
        $("#v_d_table .users_list").animate({left:l},500);
        $(".art-dialog-footer").show();
      },
      select_all_checkbox:function(){
        var dis = this;
        var selected = $("#create_verification_select_all").prop("checked");
        $(".create_verification_difference_list_checkbox").prop("checked",selected);
        dis.check_preview();
       },

       

      check_preview:function(){
        var dis = this;
        var assign_user = true;
        var create = true;
        $(".art-dialog-footer").hide();
        if($("#verification_end_date").val()==""){assign_user=false;create=false;}
        if($("#create_verification_differences_table_body :checked").length){          
        }else{
           assign_user=false;
        }

        if($("#verification_task_name").val()==""){create=false;}
        if($("#verification_selected_task_priority").val()==0){create=false;}
        $.each($("#create_verification_differences_table_body tr"),function(index,item){
          if($(item).children("td").last().html()==""){create=false;}
        });

        if(assign_user){$("#create_verification_assign_btn").removeClass("disabled");}
        else{$("#create_verification_assign_btn").addClass("disabled");}

        if(create){$(".art-dialog-button button[data-id='ok']").removeClass("disabled");}
        else{$(".art-dialog-button button[data-id='ok']").addClass("disabled");}

        $(".art-dialog-footer").show();
      },

     

     

    
      
    });

}); 