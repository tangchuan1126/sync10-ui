"use strict";
define([
  "jquery",
  "config",
  "backbone",
  "handlebars",
  "templates",
  "../../js/ImmyboxControl",
  "bootstrap",

], function( $,config, Backbone, Handlebars, templates, ImmyBox ) {
    
    return Backbone.View.extend({
      el:"#create_verification_task_priority_view",
      template:templates.immybox_view,


      render:function(data){
        var dis = this;

        var html=dis.template({divId:"verification_task_priority_div", name:"Task Priority", inputId:"verification_task_priority", selectedInput:"verification_selected_task_priority"});
        dis.$el.html(html);
        var typeImmydata = {
          renderTo: "#verification_task_priority_div",
          dataUrl: [{text:"Low",value:"1"},{text:"Medium",value:"2"},{text:"High",value:"3"}], 
          inputId: "#verification_task_priority",
          selectedInputId: "#verification_selected_task_priority",
          defaultValue: 1
        };
        
        var TypeImmy = new ImmyBox(typeImmydata);
        TypeImmy.on("events.change",function(){});
        TypeImmy.render();
        
      },

    
      
    });

}); 