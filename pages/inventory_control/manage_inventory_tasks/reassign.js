define([
    './config',
    'jquery',
    'backbone',
    'handlebars',
    'templates',
    "./view/reassign_view",
    "bootstrap",

],
function(config,$,Backbone,Handlebars,templates, reassignView){
    
    $(function(){
        
    	new reassignView().render();
        
    }); 
    
});
