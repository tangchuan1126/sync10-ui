define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['create_task'] = template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "					\n						<td class=\"labele\">Starts on</td>\n						<td class=\"val\">"
    + this.escapeExpression(((helper = (helper = helpers.startsOn || (depth0 != null ? depth0.startsOn : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"startsOn","hash":{},"data":data}) : helper)))
    + "</td>\n					\n";
},"3":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "					\n						<td class=\"labele\">Starts Date</td>\n						<td class=\"val\">"
    + alias3(((helper = (helper = helpers.start_date || (depth0 != null ? depth0.start_date : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"start_date","hash":{},"data":data}) : helper)))
    + "</td>\n					\n						<td class=\"labele\">End Date</td>\n						<td class=\"val\">"
    + alias3(((helper = (helper = helpers.end_date || (depth0 != null ? depth0.end_date : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"end_date","hash":{},"data":data}) : helper)))
    + "</td>\n					\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "			<tr>\n				\n					<td class=\"labele\">Title</td>\n					<td colspan=\"8\" class=\"val\">"
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\n				\n			</tr>\n";
},"7":function(depth0,helpers,partials,data) {
    var helper;

  return "			<tr>\n				\n					<td class=\"labele\">Model Number</td>\n					<td colspan=\"8\" class=\"val\">"
    + this.escapeExpression(((helper = (helper = helpers.modelNumber || (depth0 != null ? depth0.modelNumber : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"modelNumber","hash":{},"data":data}) : helper)))
    + "</td>\n				\n			</tr>\n";
},"9":function(depth0,helpers,partials,data) {
    var helper;

  return "			<tr>\n				\n					<td class=\"labele\">Lot Number</td>\n					<td colspan=\"8\" class=\"val\">"
    + this.escapeExpression(((helper = (helper = helpers.lotNumber || (depth0 != null ? depth0.lotNumber : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"lotNumber","hash":{},"data":data}) : helper)))
    + "</td>\n				\n			</tr>\n\n";
},"11":function(depth0,helpers,partials,data) {
    var helper;

  return "					\n						<td class=\"labele\">Product Line</td>\n						<td class=\"val\" >"
    + this.escapeExpression(((helper = (helper = helpers.productLine || (depth0 != null ? depth0.productLine : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"productLine","hash":{},"data":data}) : helper)))
    + "</td>\n					\n";
},"13":function(depth0,helpers,partials,data) {
    var helper;

  return "					\n						<td class=\"labele\">Category</td>\n						<td class=\"val\">"
    + this.escapeExpression(((helper = (helper = helpers.productCat || (depth0 != null ? depth0.productCat : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"productCat","hash":{},"data":data}) : helper)))
    + "</td>\n					\n";
},"15":function(depth0,helpers,partials,data) {
    var helper;

  return "					\n						<td class=\"labele\">LP Type</td>\n						<td class=\"val\">"
    + this.escapeExpression(((helper = (helper = helpers.lpType || (depth0 != null ? depth0.lpType : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"lpType","hash":{},"data":data}) : helper)))
    + "</td>\n					\n";
},"17":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "			<tr>\n				\n					<td class=\"labele\">Duration</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.duration || (depth0 != null ? depth0.duration : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"duration","hash":{},"data":data}) : helper)))
    + "</td>\n				\n					<td class=\"labele\">Repeats</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.repeatOptions || (depth0 != null ? depth0.repeatOptions : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"repeatOptions","hash":{},"data":data}) : helper)))
    + "</td>\n				\n					<td class=\"labele\">Repeat every</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.repeatEvery || (depth0 != null ? depth0.repeatEvery : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"repeatEvery","hash":{},"data":data}) : helper)))
    + "</td>\n				\n					<td class=\"labele\">Ends</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.endsOn || (depth0 != null ? depth0.endsOn : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"endsOn","hash":{},"data":data}) : helper)))
    + "</td>\n				\n			</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<div class=\"panel panel-default\">\n	<div class=\"panel-heading header\">\n		<a class=\"previous\" id=\"task_preview_back\">Back</a>\n		<div class=\"clear\"></div>\n	</div>\n\n	<div class=\"body panel-body\">\n		<table class=\"create_task_table\">\n			<tr>\n				\n					<td class=\"labele\">Warehouse</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.warehouse || (depth0 != null ? depth0.warehouse : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"warehouse","hash":{},"data":data}) : helper)))
    + "</td>\n				\n					<td class=\"labele\">Task Code</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.taskName || (depth0 != null ? depth0.taskName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"taskName","hash":{},"data":data}) : helper)))
    + "</td>\n				 \n				 	<td class=\"labele\">Task Priority</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.taskPriority || (depth0 != null ? depth0.taskPriority : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"taskPriority","hash":{},"data":data}) : helper)))
    + "</td>\n				\n				<td colspan=\"2\"><a href=\"#\" id=\"create_task_btn\" class=\"btn btn-primary\">"
    + alias3(((helper = (helper = helpers.buttonTitle || (depth0 != null ? depth0.buttonTitle : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"buttonTitle","hash":{},"data":data}) : helper)))
    + "</a></td>\n			</tr>\n			\n			<tr>\n				\n					<td class=\"labele\">Repeat Task</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.isRepeat || (depth0 != null ? depth0.isRepeat : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"isRepeat","hash":{},"data":data}) : helper)))
    + "</td>\n				\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.scheduled : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "				\n				<td></td><td></td>\n			</tr>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.modelNumber : depth0),{"name":"if","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.lotNumber : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			<tr>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.productLine : depth0),{"name":"if","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.productCat : depth0),{"name":"if","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.lpType : depth0),{"name":"if","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				\n					<td class=\"labele\">Type</td>\n					<td class=\"val\">"
    + alias3(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)))
    + "</td>\n					\n				\n			</tr>\n			\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.scheduled : depth0),{"name":"if","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</table>\n		\n		<div >\n				<div class=\"div-table\" id=\"selected_areas_create\"></div>\n		</div>\n		\n	</div>\n</div>\n";
},"useData":true});
templates['deadline'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"panel panel-default\">\n		<div class=\"panel-heading header\">\n			\n			<div id=\"next_area_selection_div\"><a class=\"next\" id=\"area_link\">Area Selection</a></div>\n			<div class=\"clear\"></div>\n		</div>\n		\n		<div class=\"body panel-body\" >	\n\n		<div class=\"div-table\">\n			<div class=\"div-table-row\" >\n				<div class=\"div-table-cell\" id=\"is_repeating_checkbox_div\">\n					<div class=\"input-group\" id=\"create_is_repeat_view\">\n						\n						<input type=\"checkbox\" name=\"is_repeating\" id=\"is_repeating\" style=\"\" data-label-on=\"Repeat Task\" data-label-off=\"Non-repeat\" class=\"iToggle\"/>\n					</div>\n				</div>\n				<div class=\"div-table-cell\">\n					<div class=\"input-group\" id=\"create_is_blind_view\">\n						<input type=\"checkbox\" name=\"is_blind\" id=\"is_blind\"  data-label-on=\"Blind Task\" data-label-off=\"Verify Task\" class=\"iToggle\"/>\n					</div>\n				</div>\n				<div class=\"div-table-cell\">\n					<div class=\"input-group required\" >\n						<div class=\"input-group-addon\">Task Code</div>\n						<input type=\"text\" name=\"create_task_name\" id=\"create_task_name\" class=\"form-control\" maxlength=\"30\"/>\n					</div>\n				</div>\n				<div class=\"div-table-cell\">\n		          <div class=\"input-group required\" id=\"create_task_priority_view\"></div>\n		        </div>\n				\n			</div>\n		</div>\n\n		<div class=\"div-table\" id=\"non-repeating-div\">\n			<div class=\"div-table-row\" >\n				<div class=\"div-table-cell\" id=\"start_date_box\">\n					<div class=\"input-group required\" >\n						<div class=\"input-group-addon\">Start Date</div>\n						<input type=\"text\" name=\"start_date\" id=\"start_date\" class=\"form-control\" readonly=\"readonly\"/>\n					</div>\n				</div>\n			\n				<div class=\"div-table-cell\"  id=\"end_date_box\">\n					<div class=\"input-group required\" >\n						<div class=\"input-group-addon\">End Date</div>\n						<input type=\"text\" name=\"end_date\" id=\"end_date\" class=\"form-control\" readonly=\"readonly\"/>\n					</div>\n				</div>\n				<div class=\"div-table-cell\"><div class=\"input-group\" ></div></div>\n				<div class=\"div-table-cell\">\n					\n				</div>\n			</div>\n		</div>\n\n		<div id=\"repeating_div\">\n			<div class=\"div-table\">\n				<div class=\"div-table-row\" id=\"task_duration_div\">\n					<div class=\"div-table-cell\">\n						<div class=\"input-group\" >\n							<div class=\"input-group-addon\">Duration</div>\n							<select id=\"task_duration\" name=\"task_duration\" class=\"form-control\">\n								<option value=\"1\">1 Day</option>\n								<option value=\"2\">2 Days</option>\n								<option value=\"3\">3 Days</option>\n								<option value=\"4\">4 Days</option>\n								<option value=\"5\">5 Days</option>\n								<option value=\"6\">6 Days</option>\n								<option value=\"7\">7 Days</option>\n							</select> \n						</div>	\n							\n					</div>\n					<div class=\"div-table-cell\">\n						<div class=\"input-group\" >\n							<div class=\"input-group-addon\">Repeats</div>\n							<select id=\"repeat_options\" name=\"repeat_options\" class=\"form-control\">\n								<option value=\"Daily\">Daily</option>\n								<option value=\"Weekly\">Weekly</option>\n								<option value=\"Monthly\">Monthly</option>\n							</select>\n						</div>	\n					</div>\n					<div class=\"div-table-cell\">\n						<div id=\"Weekly_table\" class=\"div-table\">\n							<div class=\"div-table-row\">\n								<div class=\"div-table-cell\">\n										<div class=\"input-group\" >\n										<div class=\"input-group-addon\">Repeat every</div>\n										<select id=\"repeat_options_weekly\" name=\"repeat_options_weekly\" class=\"form-control\">\n											<option value=\"1\">1 Week</option>\n											<option value=\"2\">2 Weeks</option>\n											<option value=\"3\">3 Weeks</option>\n											<option value=\"4\">4 Weeks</option>\n											<option value=\"5\">5 Weeks</option>\n											<option value=\"6\">6 Weeks</option>\n											<option value=\"7\">7 Weeks</option>\n											<option value=\"8\">8 Weeks</option>\n											<option value=\"9\">9 Weeks</option>\n											<option value=\"10\">10 Weeks</option>\n										</select>\n									</div>\n								</div>\n							</div>\n							\n						</div>\n						<div id=\"Daily_table\" class=\"div-table\">\n							<div class=\"div-table-row\">\n								<div class=\"div-table-cell\">\n									<div class=\"input-group\" >\n										<div class=\"input-group-addon\">Repeat every</div>\n										<select id=\"repeat_options_daily\" name=\"repeat_options_daily\" class=\"form-control\">\n											<option value=\"1\">1 Day</option>\n											<option value=\"2\">2 Days</option>\n											<option value=\"3\">3 Days</option>\n											<option value=\"4\">4 Days</option>\n											<option value=\"5\">5 Days</option>\n											<option value=\"6\">6 Days</option>\n											<option value=\"7\">7 Days</option>\n											<option value=\"8\">8 Days</option>\n											<option value=\"9\">9 Days</option>\n											<option value=\"10\">10 Days</option>\n											<option value=\"11\">11 Days</option>\n											<option value=\"12\">12 Days</option>\n											<option value=\"13\">13 Days</option>\n											<option value=\"14\">14 Days</option>\n											<option value=\"15\">15 Days</option>\n											<option value=\"16\">16 Days</option>\n											<option value=\"17\">17 Days</option>\n											<option value=\"18\">18 Days</option>\n											<option value=\"19\">19 Days</option>\n											<option value=\"20\">20 Days</option>\n											<option value=\"21\">21 Days</option>\n											<option value=\"22\">22 Days</option>\n											<option value=\"23\">23 Days</option>\n											<option value=\"24\">24 Days</option>\n											<option value=\"25\">25 Days</option>\n											<option value=\"26\">26 Days</option>\n											<option value=\"27\">27 Days</option>\n											<option value=\"28\">28 Days</option>\n											<option value=\"29\">29 Days</option>\n											<option value=\"30\">30 Days</option>\n											\n										</select>\n									</div>\n								</div>\n							</div>\n						</div>\n						<div id=\"Monthly_table\" class=\"div-table\">\n							<div class=\"div-table-row\">\n								<div class=\"div-table-cell\">\n									<div class=\"input-group\" >\n										<div class=\"input-group-addon\">Repeat every</div>\n										<select id=\"repeat_options_monthly\" name=\"repeat_options_monthly\" class=\"form-control\">\n											<option value=\"1\">1 Month</option>\n											<option value=\"2\">2 Months</option>\n											<option value=\"3\">3 Months</option>\n											<option value=\"4\">4 Months</option>\n											<option value=\"5\">5 Months</option>\n											<option value=\"6\">6 Months</option>\n											<option value=\"7\">7 Months</option>\n											<option value=\"8\">8 Months</option>\n											<option value=\"9\">9 Months</option>\n											<option value=\"10\">10 Months</option>\n											<option value=\"11\">11 Months</option>\n											<option value=\"12\">12 Months</option>\n										</select>\n									\n									</div>\n								</div>\n							</div>\n						</div>\n					\n					</div>\n\n					<div class=\"div-table-cell\">\n						<div class=\"input-group required\" >\n							<div class=\"input-group-addon\">Starts on</div>\n							<input type=\"text\" id=\"starts_on\" value=\"\" class=\"form-control\"/>\n						</div>\n					</div>	\n				</div>\n				\n\n			</div>\n			<div class=\"div-table\" style=\"width:98%\">\n				<div class=\"div-table-row\">\n					\n					<div class=\"div-table-cell\" id=\"date_table\" style=\"width:75%\">\n						<div class=\"input-group required\" >\n							<div class=\"input-group-addon\">Ends</div>\n							<div class=\"div-table\" id=\"ends_table\">\n								<div class=\"div-table-row\">\n									<div class=\"div-table-cell\">\n										<input type=\"radio\" id=\"ends_on_never\" name=\"ends_on\" value=\"never\" checked=\"checked\">\n										<label for=\"ends_on_never\">Never</label>\n									</div>\n								\n									<div class=\"div-table-cell\" id=\"occ\">\n										<input type=\"radio\" id=\"ends_on_after\" name=\"ends_on\" value=\"after\">\n										<label for=\"ends_on_after\">After <input type=\"text\" id=\"occurances\" maxlength=\"3\"/> occurrences</label>\n									</div>\n								\n									<div class=\"div-table-cell\">\n										<input type=\"radio\" name=\"ends_on\" id=\"ends_on_on\" value=\"on\">\n										<label for=\"ends_on_on\">On <input type=\"text\" id=\"ends_on_date\" value=\"\" /></label>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n					\n				\n				</div>\n			</div>\n			\n		</div>\n	</div>\n</div>	\n";
},"useData":true});
templates['immybox_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"input-group-addon\" id=\""
    + alias3(((helper = (helper = helpers.divId || (depth0 != null ? depth0.divId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"divId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div><input type=\"hidden\" id=\""
    + alias3(((helper = (helper = helpers.selectedInput || (depth0 != null ? depth0.selectedInput : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"selectedInput","hash":{},"data":data}) : helper)))
    + "\"/>\n<input type=\"text\" id=\""
    + alias3(((helper = (helper = helpers.inputId || (depth0 != null ? depth0.inputId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"inputId","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control\"/>\n\n";
},"useData":true});
templates['new_task_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<div class=\"panel panel-default\">\n  <div id=\"inventory_task_header\" class=\"panel-heading\">\n      <div class=\"left\">"
    + alias3(((helper = (helper = helpers.heading || (depth0 != null ? depth0.heading : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"heading","hash":{},"data":data}) : helper)))
    + "</div>\n      <div id=\"back-buttons-div\"><a href=\"#\" id=\"back\" class=\"btn btn-warning\">\n      <span class=\"glyphicon glyphicon-list-alt\"></span> Manage Tasks List</a></div>\n      <div class=\"clear\"></div>\n    </div>\n  <div  class=\"panel-body\">\n    <input type=\"hidden\" name=\"create_task_type\" id=\"create_task_type\" value=\""
    + alias3(((helper = (helper = helpers.taskType || (depth0 != null ? depth0.taskType : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"taskType","hash":{},"data":data}) : helper)))
    + "\"/>\n    <input type=\"hidden\" name=\"task_id\" id=\"task_id\" value=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"/>\n    <div id=\"filter_box\" class=\"div-table\">\n      <div class=\"div-table-row\">\n        <div class=\"div-table-cell\">\n          <div class=\"input-group required\" id=\"create_warehouse_view\"></div>\n        </div>\n        <div class=\"div-table-cell\">\n          <div class=\"input-group\" id=\"create_title_view\"></div>\n        </div>\n        <div class=\"div-table-cell\">\n          <div class=\"input-group\" id=\"create_product_line_view\"></div>\n        </div>\n        <div class=\"div-table-cell\">\n          <div class=\"input-group\" id=\"create_product_cat_view\"></div>\n        </div>\n        \n        \n      </div>\n      <div class=\"div-table-row\">\n        <div class=\"div-table-cell\">\n          <div class=\"input-group\" id=\"create_model_view\">\n            \n          </div>\n        </div>\n        <div class=\"div-table-cell\">\n          <div class=\"input-group\" id=\"create_lot_view\">\n            \n          </div>\n        </div>\n        <div class=\"div-table-cell\">\n          <div class=\"input-group\" id=\"create_lp_type_view\"></div>\n        </div>\n        \n        \n      </div>\n      \n    </div>\n    \n    <div class=\"clear\"></div>\n    <div id=\"error_block\" class=\"alert alert-danger\"></div>\n    \n    <div id=\"all_container\">\n      <div id=\"all_container2\">\n        <div id=\"deadline_body\"></div>\n        <div id=\"toc_area\" class=\"toc_area\"></div>\n        <div id=\"users_body\" class=\"users_body\"></div>\n        <div id=\"create_task_body\"></div>\n        <div class=\"clear\"></div>\n      </div>\n    </div>\n  </div>\n</div>  ";
},"useData":true});
templates['parameter_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "";
},"useData":true});
templates['preview_areas'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"div-table-cell\" id=\"areas_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"panel panel-default\">\n		<div class=\"panel-heading header\">\n			<div>"
    + alias3(((helper = (helper = helpers.areaName || (depth0 != null ? depth0.areaName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaName","hash":{},"data":data}) : helper)))
    + "</div>\n		</div>\n		<div class=\"body panel-body\">\n			<div class=\"locations_block\" id=\"locations_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + alias3(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">	\n			</div>\n\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['preview_locations'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"location_"
    + alias3(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.locationName || (depth0 != null ? depth0.locationName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"locationName","hash":{},"data":data}) : helper)))
    + "\n</div>";
},"useData":true});
templates['preview_users'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n	\n<div class=\"div-table-row\" id=\"users_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\" data-user-id=\""
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"div-table-cell user_name\">"
    + alias3(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userName","hash":{},"data":data}) : helper)))
    + "</div>\n	<div class=\"div-table-cell areas\" >\n		<div class=\"div-table\">\n			<div class=\"div-table-row\" id=\"areas_"
    + alias3(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"userId","hash":{},"data":data}) : helper)))
    + "\"></div>\n		</div>\n	</div>\n</div>\n	\n";
},"useData":true});
templates['select2_view'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "		<option value=\""
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"input-group-addon\" >"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\n<select id=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"width:206px;display:none\" class=\"js-example-tags form-control select2-hidden-accessible\" multiple=\"\" tabindex=\"-1\" aria-hidden=\"true\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</select>\n";
},"useData":true});
templates['user_view'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\">\n		\n	<div class=\"body panel-body\">	\n		<div class=\"employee_box_div\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n		<div class=\"clear\"></div>\n	</div>\n	<div class=\"panel-footer\">\n		<a  id=\"assign_user_ok\" class=\"btn btn-primary\">\n		<span class=\"glyphicon glyphicon-ok\"></span> Ok</a>\n		<a  id=\"assign_user_cancel\" class=\"btn btn-warning\">\n		<span class=\"glyphicon glyphicon-cancel\"></span> Cancel</a>\n	</div>\n</div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "			<div class=\"employee_box\">\n				<div class=\"employee_name\" data-user_id=\""
    + alias2(alias1((depth0 != null ? depth0.user_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.user_name : depth0), depth0))
    + "</div>\n				<div class=\"count\">"
    + alias2(alias1((depth0 != null ? depth0.count : depth0), depth0))
    + "</div>\n				<div class=\"clear\"></div>\n			</div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
return templates;
});