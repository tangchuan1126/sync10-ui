(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['create_task'] = template({"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					\r\n						<td class=\"labele\">Starts on</td>\r\n						<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.startsOn || (depth0 != null ? depth0.startsOn : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"startsOn","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					\r\n						<td class=\"labele\">Starts Date</td>\r\n						<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.start_date || (depth0 != null ? depth0.start_date : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"start_date","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n						<td class=\"labele\">End Date</td>\r\n						<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.end_date || (depth0 != null ? depth0.end_date : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"end_date","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<tr>\r\n				\r\n					<td class=\"labele\">Title</td>\r\n					<td colspan=\"8\" class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n			</tr>\r\n";
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<tr>\r\n				\r\n					<td class=\"labele\">Model Number</td>\r\n					<td colspan=\"8\" class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.modelNumber || (depth0 != null ? depth0.modelNumber : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"modelNumber","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n			</tr>\r\n";
},"9":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<tr>\r\n				\r\n					<td class=\"labele\">Lot Number</td>\r\n					<td colspan=\"8\" class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.lotNumber || (depth0 != null ? depth0.lotNumber : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lotNumber","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n			</tr>\r\n\r\n";
},"11":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					\r\n						<td class=\"labele\">Product Line</td>\r\n						<td class=\"val\" >"
    + escapeExpression(((helper = (helper = helpers.productLine || (depth0 != null ? depth0.productLine : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"productLine","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n";
},"13":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					\r\n						<td class=\"labele\">Category</td>\r\n						<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.productCat || (depth0 != null ? depth0.productCat : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"productCat","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n";
},"15":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "					\r\n						<td class=\"labele\">LP Type</td>\r\n						<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.lpType || (depth0 != null ? depth0.lpType : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lpType","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n";
},"17":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "			<tr>\r\n				\r\n					<td class=\"labele\">Duration</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.duration || (depth0 != null ? depth0.duration : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"duration","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n					<td class=\"labele\">Repeats</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.repeatOptions || (depth0 != null ? depth0.repeatOptions : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"repeatOptions","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n					<td class=\"labele\">Repeat every</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.repeatEvery || (depth0 != null ? depth0.repeatEvery : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"repeatEvery","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n					<td class=\"labele\">Ends</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.endsOn || (depth0 != null ? depth0.endsOn : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"endsOn","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n			</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "\r\n<div class=\"panel panel-default\">\r\n	<div class=\"panel-heading header\">\r\n		<a class=\"previous\" id=\"task_preview_back\">Back</a>\r\n		<div class=\"clear\"></div>\r\n	</div>\r\n\r\n	<div class=\"body panel-body\">\r\n		<table class=\"create_task_table\">\r\n			<tr>\r\n				\r\n					<td class=\"labele\">Warehouse</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.warehouse || (depth0 != null ? depth0.warehouse : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"warehouse","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n					<td class=\"labele\">Task Code</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.taskName || (depth0 != null ? depth0.taskName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"taskName","hash":{},"data":data}) : helper)))
    + "</td>\r\n				 \r\n				 	<td class=\"labele\">Task Priority</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.taskPriority || (depth0 != null ? depth0.taskPriority : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"taskPriority","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n				<td colspan=\"2\"><a href=\"#\" id=\"create_task_btn\" class=\"btn btn-primary\">"
    + escapeExpression(((helper = (helper = helpers.buttonTitle || (depth0 != null ? depth0.buttonTitle : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"buttonTitle","hash":{},"data":data}) : helper)))
    + "</a></td>\r\n			</tr>\r\n			\r\n			<tr>\r\n				\r\n					<td class=\"labele\">Repeat Task</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.isRepeat || (depth0 != null ? depth0.isRepeat : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"isRepeat","hash":{},"data":data}) : helper)))
    + "</td>\r\n				\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.scheduled : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "				\r\n				<td></td><td></td>\r\n			</tr>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.title : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.modelNumber : depth0), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.lotNumber : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "			<tr>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.productLine : depth0), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.productCat : depth0), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.lpType : depth0), {"name":"if","hash":{},"fn":this.program(15, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "				\r\n					<td class=\"labele\">Type</td>\r\n					<td class=\"val\">"
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + "</td>\r\n					\r\n				\r\n			</tr>\r\n			\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.scheduled : depth0), {"name":"if","hash":{},"fn":this.program(17, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</table>\r\n		\r\n		<div >\r\n				<div class=\"div-table\" id=\"selected_areas_create\"></div>\r\n		</div>\r\n		\r\n	</div>\r\n</div>\r\n";
},"useData":true});
templates['deadline'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"panel panel-default\">\r\n		<div class=\"panel-heading header\">\r\n			\r\n			<div id=\"next_area_selection_div\"><a class=\"next\" id=\"area_link\">Area Selection</a></div>\r\n			<div class=\"clear\"></div>\r\n		</div>\r\n		\r\n		<div class=\"body panel-body\" >	\r\n\r\n		<div class=\"div-table\">\r\n			<div class=\"div-table-row\" >\r\n				<div class=\"div-table-cell\" id=\"is_repeating_checkbox_div\">\r\n					<div class=\"input-group\" id=\"create_is_repeat_view\">\r\n						\r\n						<input type=\"checkbox\" name=\"is_repeating\" id=\"is_repeating\" style=\"\" data-label-on=\"Repeat Task\" data-label-off=\"Non-repeat\" class=\"iToggle\"/>\r\n					</div>\r\n				</div>\r\n				<div class=\"div-table-cell\">\r\n					<div class=\"input-group\" id=\"create_is_blind_view\">\r\n						<input type=\"checkbox\" name=\"is_blind\" id=\"is_blind\"  data-label-on=\"Blind Task\" data-label-off=\"Verify Task\" class=\"iToggle\"/>\r\n					</div>\r\n				</div>\r\n				<div class=\"div-table-cell\">\r\n					<div class=\"input-group required\" >\r\n						<div class=\"input-group-addon\">Task Code</div>\r\n						<input type=\"text\" name=\"create_task_name\" id=\"create_task_name\" class=\"form-control\" maxlength=\"30\"/>\r\n					</div>\r\n				</div>\r\n				<div class=\"div-table-cell\">\r\n		          <div class=\"input-group required\" id=\"create_task_priority_view\"></div>\r\n		        </div>\r\n				\r\n			</div>\r\n		</div>\r\n\r\n		<div class=\"div-table\" id=\"non-repeating-div\">\r\n			<div class=\"div-table-row\" >\r\n				<div class=\"div-table-cell\" id=\"start_date_box\">\r\n					<div class=\"input-group required\" >\r\n						<div class=\"input-group-addon\">Start Date</div>\r\n						<input type=\"text\" name=\"start_date\" id=\"start_date\" class=\"form-control\" readonly=\"readonly\"/>\r\n					</div>\r\n				</div>\r\n			\r\n				<div class=\"div-table-cell\"  id=\"end_date_box\">\r\n					<div class=\"input-group required\" >\r\n						<div class=\"input-group-addon\">End Date</div>\r\n						<input type=\"text\" name=\"end_date\" id=\"end_date\" class=\"form-control\" readonly=\"readonly\"/>\r\n					</div>\r\n				</div>\r\n				<div class=\"div-table-cell\"><div class=\"input-group\" ></div></div>\r\n				<div class=\"div-table-cell\">\r\n					\r\n				</div>\r\n			</div>\r\n		</div>\r\n\r\n		<div id=\"repeating_div\">\r\n			<div class=\"div-table\">\r\n				<div class=\"div-table-row\" id=\"task_duration_div\">\r\n					<div class=\"div-table-cell\">\r\n						<div class=\"input-group\" >\r\n							<div class=\"input-group-addon\">Duration</div>\r\n							<select id=\"task_duration\" name=\"task_duration\" class=\"form-control\">\r\n								<option value=\"1\">1 Day</option>\r\n								<option value=\"2\">2 Days</option>\r\n								<option value=\"3\">3 Days</option>\r\n								<option value=\"4\">4 Days</option>\r\n								<option value=\"5\">5 Days</option>\r\n								<option value=\"6\">6 Days</option>\r\n								<option value=\"7\">7 Days</option>\r\n							</select> \r\n						</div>	\r\n							\r\n					</div>\r\n					<div class=\"div-table-cell\">\r\n						<div class=\"input-group\" >\r\n							<div class=\"input-group-addon\">Repeats</div>\r\n							<select id=\"repeat_options\" name=\"repeat_options\" class=\"form-control\">\r\n								<option value=\"Daily\">Daily</option>\r\n								<option value=\"Weekly\">Weekly</option>\r\n								<option value=\"Monthly\">Monthly</option>\r\n							</select>\r\n						</div>	\r\n					</div>\r\n					<div class=\"div-table-cell\">\r\n						<div id=\"Weekly_table\" class=\"div-table\">\r\n							<div class=\"div-table-row\">\r\n								<div class=\"div-table-cell\">\r\n										<div class=\"input-group\" >\r\n										<div class=\"input-group-addon\">Repeat every</div>\r\n										<select id=\"repeat_options_weekly\" name=\"repeat_options_weekly\" class=\"form-control\">\r\n											<option value=\"1\">1 Week</option>\r\n											<option value=\"2\">2 Weeks</option>\r\n											<option value=\"3\">3 Weeks</option>\r\n											<option value=\"4\">4 Weeks</option>\r\n											<option value=\"5\">5 Weeks</option>\r\n											<option value=\"6\">6 Weeks</option>\r\n											<option value=\"7\">7 Weeks</option>\r\n											<option value=\"8\">8 Weeks</option>\r\n											<option value=\"9\">9 Weeks</option>\r\n											<option value=\"10\">10 Weeks</option>\r\n										</select>\r\n									</div>\r\n								</div>\r\n							</div>\r\n							\r\n						</div>\r\n						<div id=\"Daily_table\" class=\"div-table\">\r\n							<div class=\"div-table-row\">\r\n								<div class=\"div-table-cell\">\r\n									<div class=\"input-group\" >\r\n										<div class=\"input-group-addon\">Repeat every</div>\r\n										<select id=\"repeat_options_daily\" name=\"repeat_options_daily\" class=\"form-control\">\r\n											<option value=\"1\">1 Day</option>\r\n											<option value=\"2\">2 Days</option>\r\n											<option value=\"3\">3 Days</option>\r\n											<option value=\"4\">4 Days</option>\r\n											<option value=\"5\">5 Days</option>\r\n											<option value=\"6\">6 Days</option>\r\n											<option value=\"7\">7 Days</option>\r\n											<option value=\"8\">8 Days</option>\r\n											<option value=\"9\">9 Days</option>\r\n											<option value=\"10\">10 Days</option>\r\n											<option value=\"11\">11 Days</option>\r\n											<option value=\"12\">12 Days</option>\r\n											<option value=\"13\">13 Days</option>\r\n											<option value=\"14\">14 Days</option>\r\n											<option value=\"15\">15 Days</option>\r\n											<option value=\"16\">16 Days</option>\r\n											<option value=\"17\">17 Days</option>\r\n											<option value=\"18\">18 Days</option>\r\n											<option value=\"19\">19 Days</option>\r\n											<option value=\"20\">20 Days</option>\r\n											<option value=\"21\">21 Days</option>\r\n											<option value=\"22\">22 Days</option>\r\n											<option value=\"23\">23 Days</option>\r\n											<option value=\"24\">24 Days</option>\r\n											<option value=\"25\">25 Days</option>\r\n											<option value=\"26\">26 Days</option>\r\n											<option value=\"27\">27 Days</option>\r\n											<option value=\"28\">28 Days</option>\r\n											<option value=\"29\">29 Days</option>\r\n											<option value=\"30\">30 Days</option>\r\n											\r\n										</select>\r\n									</div>\r\n								</div>\r\n							</div>\r\n						</div>\r\n						<div id=\"Monthly_table\" class=\"div-table\">\r\n							<div class=\"div-table-row\">\r\n								<div class=\"div-table-cell\">\r\n									<div class=\"input-group\" >\r\n										<div class=\"input-group-addon\">Repeat every</div>\r\n										<select id=\"repeat_options_monthly\" name=\"repeat_options_monthly\" class=\"form-control\">\r\n											<option value=\"1\">1 Month</option>\r\n											<option value=\"2\">2 Months</option>\r\n											<option value=\"3\">3 Months</option>\r\n											<option value=\"4\">4 Months</option>\r\n											<option value=\"5\">5 Months</option>\r\n											<option value=\"6\">6 Months</option>\r\n											<option value=\"7\">7 Months</option>\r\n											<option value=\"8\">8 Months</option>\r\n											<option value=\"9\">9 Months</option>\r\n											<option value=\"10\">10 Months</option>\r\n											<option value=\"11\">11 Months</option>\r\n											<option value=\"12\">12 Months</option>\r\n										</select>\r\n									\r\n									</div>\r\n								</div>\r\n							</div>\r\n						</div>\r\n					\r\n					</div>\r\n\r\n					<div class=\"div-table-cell\">\r\n						<div class=\"input-group required\" >\r\n							<div class=\"input-group-addon\">Starts on</div>\r\n							<input type=\"text\" id=\"starts_on\" value=\"\" class=\"form-control\"/>\r\n						</div>\r\n					</div>	\r\n				</div>\r\n				\r\n\r\n			</div>\r\n			<div class=\"div-table\" style=\"width:98%\">\r\n				<div class=\"div-table-row\">\r\n					\r\n					<div class=\"div-table-cell\" id=\"date_table\" style=\"width:75%\">\r\n						<div class=\"input-group required\" >\r\n							<div class=\"input-group-addon\">Ends</div>\r\n							<div class=\"div-table\" id=\"ends_table\">\r\n								<div class=\"div-table-row\">\r\n									<div class=\"div-table-cell\">\r\n										<input type=\"radio\" id=\"ends_on_never\" name=\"ends_on\" value=\"never\" checked=\"checked\">\r\n										<label for=\"ends_on_never\">Never</label>\r\n									</div>\r\n								\r\n									<div class=\"div-table-cell\" id=\"occ\">\r\n										<input type=\"radio\" id=\"ends_on_after\" name=\"ends_on\" value=\"after\">\r\n										<label for=\"ends_on_after\">After <input type=\"text\" id=\"occurances\" maxlength=\"3\"/> occurrences</label>\r\n									</div>\r\n								\r\n									<div class=\"div-table-cell\">\r\n										<input type=\"radio\" name=\"ends_on\" id=\"ends_on_on\" value=\"on\">\r\n										<label for=\"ends_on_on\">On <input type=\"text\" id=\"ends_on_date\" value=\"\" /></label>\r\n									</div>\r\n								</div>\r\n							</div>\r\n						</div>\r\n					</div>\r\n					\r\n				\r\n				</div>\r\n			</div>\r\n			\r\n		</div>\r\n	</div>\r\n</div>	\r\n";
  },"useData":true});
templates['immybox_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"input-group-addon\" id=\""
    + escapeExpression(((helper = (helper = helpers.divId || (depth0 != null ? depth0.divId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"divId","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</div><input type=\"hidden\" id=\""
    + escapeExpression(((helper = (helper = helpers.selectedInput || (depth0 != null ? depth0.selectedInput : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"selectedInput","hash":{},"data":data}) : helper)))
    + "\"/>\r\n<input type=\"text\" id=\""
    + escapeExpression(((helper = (helper = helpers.inputId || (depth0 != null ? depth0.inputId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"inputId","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control\"/>\r\n\r\n";
},"useData":true});
templates['new_task_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n<div class=\"panel panel-default\">\r\n  <div id=\"inventory_task_header\" class=\"panel-heading\">\r\n      <div class=\"left\">"
    + escapeExpression(((helper = (helper = helpers.heading || (depth0 != null ? depth0.heading : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"heading","hash":{},"data":data}) : helper)))
    + "</div>\r\n      <div id=\"back-buttons-div\"><a href=\"#\" id=\"back\" class=\"btn btn-warning\">\r\n      <span class=\"glyphicon glyphicon-list-alt\"></span> Manage Tasks List</a></div>\r\n      <div class=\"clear\"></div>\r\n    </div>\r\n  <div  class=\"panel-body\">\r\n    <input type=\"hidden\" name=\"create_task_type\" id=\"create_task_type\" value=\""
    + escapeExpression(((helper = (helper = helpers.taskType || (depth0 != null ? depth0.taskType : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"taskType","hash":{},"data":data}) : helper)))
    + "\"/>\r\n    <input type=\"hidden\" name=\"task_id\" id=\"task_id\" value=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\"/>\r\n    <div id=\"filter_box\" class=\"div-table\">\r\n      <div class=\"div-table-row\">\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group required\" id=\"create_warehouse_view\"></div>\r\n        </div>\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group\" id=\"create_title_view\"></div>\r\n        </div>\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group\" id=\"create_product_line_view\"></div>\r\n        </div>\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group\" id=\"create_product_cat_view\"></div>\r\n        </div>\r\n        \r\n        \r\n      </div>\r\n      <div class=\"div-table-row\">\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group\" id=\"create_model_view\">\r\n            \r\n          </div>\r\n        </div>\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group\" id=\"create_lot_view\">\r\n            \r\n          </div>\r\n        </div>\r\n        <div class=\"div-table-cell\">\r\n          <div class=\"input-group\" id=\"create_lp_type_view\"></div>\r\n        </div>\r\n        \r\n        \r\n      </div>\r\n      \r\n    </div>\r\n    \r\n    <div class=\"clear\"></div>\r\n    <div id=\"error_block\" class=\"alert alert-danger\"></div>\r\n    \r\n    <div id=\"all_container\">\r\n      <div id=\"all_container2\">\r\n        <div id=\"deadline_body\"></div>\r\n        <div id=\"toc_area\" class=\"toc_area\"></div>\r\n        <div id=\"users_body\" class=\"users_body\"></div>\r\n        <div id=\"create_task_body\"></div>\r\n        <div class=\"clear\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>  ";
},"useData":true});
templates['parameter_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
templates['preview_areas'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"div-table-cell\" id=\"areas_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">\r\n	<div class=\"panel panel-default\">\r\n		<div class=\"panel-heading header\">\r\n			<div>"
    + escapeExpression(((helper = (helper = helpers.areaName || (depth0 != null ? depth0.areaName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaName","hash":{},"data":data}) : helper)))
    + "</div>\r\n		</div>\r\n		<div class=\"body panel-body\">\r\n			<div class=\"locations_block\" id=\"locations_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "_"
    + escapeExpression(((helper = (helper = helpers.areaId || (depth0 != null ? depth0.areaId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"areaId","hash":{},"data":data}) : helper)))
    + "\">	\r\n			</div>\r\n\r\n		</div>\r\n	</div>\r\n</div>";
},"useData":true});
templates['preview_locations'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div id=\"location_"
    + escapeExpression(((helper = (helper = helpers.locationId || (depth0 != null ? depth0.locationId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationId","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.locationName || (depth0 != null ? depth0.locationName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"locationName","hash":{},"data":data}) : helper)))
    + "\r\n</div>";
},"useData":true});
templates['preview_users'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n	\r\n<div class=\"div-table-row\" id=\"users_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\" data-user-id=\""
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\">\r\n	<div class=\"div-table-cell user_name\">"
    + escapeExpression(((helper = (helper = helpers.userName || (depth0 != null ? depth0.userName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userName","hash":{},"data":data}) : helper)))
    + "</div>\r\n	<div class=\"div-table-cell areas\" >\r\n		<div class=\"div-table\">\r\n			<div class=\"div-table-row\" id=\"areas_"
    + escapeExpression(((helper = (helper = helpers.userId || (depth0 != null ? depth0.userId : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"userId","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n		</div>\r\n	</div>\r\n</div>\r\n	\r\n";
},"useData":true});
templates['select2_view'] = template({"1":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "		<option value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.value : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"input-group-addon\" >"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\r\n<select id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" style=\"width:206px;display:none\" class=\"js-example-tags form-control select2-hidden-accessible\" multiple=\"\" tabindex=\"-1\" aria-hidden=\"true\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.list : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</select>\r\n";
},"useData":true});
templates['user_view'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"panel panel-default\">\r\n		\r\n	<div class=\"body panel-body\">	\r\n		<div class=\"employee_box_div\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.users : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\r\n		<div class=\"clear\"></div>\r\n	</div>\r\n	<div class=\"panel-footer\">\r\n		<a  id=\"assign_user_ok\" class=\"btn btn-primary\">\r\n		<span class=\"glyphicon glyphicon-ok\"></span> Ok</a>\r\n		<a  id=\"assign_user_cancel\" class=\"btn btn-warning\">\r\n		<span class=\"glyphicon glyphicon-cancel\"></span> Cancel</a>\r\n	</div>\r\n</div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "			<div class=\"employee_box\">\r\n				<div class=\"employee_name\" data-user_id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.user_id : depth0), depth0))
    + "\">"
    + escapeExpression(lambda((depth0 != null ? depth0.user_name : depth0), depth0))
    + "</div>\r\n				<div class=\"count\">"
    + escapeExpression(lambda((depth0 != null ? depth0.count : depth0), depth0))
    + "</div>\r\n				<div class=\"clear\"></div>\r\n			</div>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1;
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.users : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { return stack1; }
  else { return ''; }
  },"useData":true});
})();