"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      var ListModel = Backbone.Model.extend({
          idAttribute: "location_id",
          initialize:function(data){
            this.locationId = data.locationId;
            this.locationName = data.locationName;
            this.areaId = data.areaId;
            this.areaName = data.areaName;
            this.userId = data.userId;
          }
          

       });
      var ListCollection =  Backbone.Collection.extend({
           model: ListModel,

      });
      var ListModelBackEnd = Backbone.Model.extend({
          initialize:function(data){
            this.location_id = data.location_id;
            this.area_id = data.area_id;
            this.user_id = data.user_id;
            return this;
          }
          

       });
      var ListCollectionBackEnd =  Backbone.Collection.extend({
           model: ListModelBackEnd,

      });
      
      var TaskModel = Backbone.Model.extend({
        url:config.createTaskDifference,
        idAttribute: "id",
        defaults:{
           warehouse:"",
          },
        initialize:function(data){
          
          this.warehouse = data.warehouse;
         // this.title = data.title;
          this.assignments = data.assignments;
         
 /*         this.is_repeat = data.isRepeat;
      
          this.ends_On = data.endsOn;
          this.startsOn = data.startsOn;
          
          this.type = data.type;
          //this.scheduled = data.scheduled;
          this.duration = data.duration;
          this.repeatBy = data.repeatBy;
          this.repeatEvery = data.repeatEvery;
          
          this.dayOfWeek = data.dayOfWeek;
          this.occurrence = data.occurrence;*/
          return this;
        }

       });
       
       
      
      

      return {
        TaskModel:TaskModel,
        ListModel:ListModel,
        ListCollection:ListCollection,
        ListCollectionBackEnd:ListCollectionBackEnd,
        ListModelBackEnd:ListModelBackEnd
        
      };




}); //page_init