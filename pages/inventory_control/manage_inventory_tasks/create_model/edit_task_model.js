"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      var TaskModel = Backbone.Model.extend({
        url:config.getTaskInfo,
        idAttribute: "id",
        defaults:{
           locations:null,
          },
       });
      return {
        TaskModel:TaskModel,
      };

});