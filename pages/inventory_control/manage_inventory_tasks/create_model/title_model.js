"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var TitleImmyModel = Backbone.Model.extend({
          idAttribute: "value",

          parse: function(data){
            
            this.text = data.TITLE_NAME ;
            this.value = data.TITLE_ID;
            return this;
          }
        
       });
       
      var TitleImmyCollection =  Backbone.Collection.extend({
           model: TitleImmyModel,
           url: config.getAllTitlesJSON,
           
       });

      return {
	      TitleImmyModel:TitleImmyModel,
	      TitleImmyCollection:TitleImmyCollection,

	    };




}); //page_init