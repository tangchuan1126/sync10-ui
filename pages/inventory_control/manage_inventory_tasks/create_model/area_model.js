"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var AreaImmyModel = Backbone.Model.extend({
          idAttribute: "area_id",
        
       });
       
      var AreaImmyCollection =  Backbone.Collection.extend({
           model: AreaImmyModel,
           url: config.getAreaLocationUserJSON,

           initialize: function(data){
              this.url = this.url+data.url;
              
              
          },
          comparator: function(item) {
                return item.get('AREA_NAME');
            },

        
       });

      return {
	      AreaImmyModel:AreaImmyModel,
	      AreaImmyCollection:AreaImmyCollection,

	    };




}); //page_init