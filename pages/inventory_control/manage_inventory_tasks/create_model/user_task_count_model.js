"use strict";
define([
  "../create_config/config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var UserModel = Backbone.Model.extend({
         idAttribute: "user_id",
        
          parse: function(data){
            this.user_name = data.USER_NAME;
            this.user_id = data.USER_ID;
            this.count = data.TASK_COUNT;
            return this;
          }
        
       });
       
      var UserCollection =  Backbone.Collection.extend({
           model: UserModel,
           url: config.getUsersAndAssignLoadJSON,

           initialize:function(data){
            this.url = this.url+"?ps_id="+data.ps_id;
            this.url += "&is_repeating="+data.is_repeating;
            this.url += "&start_date="+data.start_date;
            if(data.is_repeating){this.url += "&duration="+data.duration;}
            else{this.url += "&end_date="+data.end_date}
            
           },
           comparator: function(item) {
                return [item.get('count'),item.get('user_name')];
            },
       });

      return {
	      UserModel:UserModel,
	      UserCollection:UserCollection,

	    };




}); //page_init