"use strict";
define(
	["jquery",
	"config",
	"bootstrap",
	"metisMenu",
	"CompondList/View",
	"./view_config",
	"./jsontohtml_templates",
	"./view/reassign_view",
	"art_Dialog/dialog-plus",
	"create_view/new_task_view",
	"view/progress_task_detail",
	"artDialog",
	 
	  "showMessage"],
	function($,config,bootstrap,metisMenu,CompondList,view_config,Template, reassignView, Dialog, newTaskView, taskProgressView)
	{
		return  function(options)
		{
			var list  = new CompondList(view_config, {
	  				renderTo: options.el,
	    		 	 dataUrl: options.url,
	    		 	 PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
	    		 	
	  				  });
					 //绑定事件
			
			list.render();  
			list.on("events.stop",function(e)
			{
				var taskId = e.data.linedata.TASK_ID;
				stopTask(taskId,list);
			
			});
			list.on("taskDetails",function(e)
			{
				var _taskId = e.data.TASK_ID;
				var tpView = new taskProgressView();
				tpView.render({taskId:_taskId});
				
			});

			list.on("events.reassign",function(e)
			{
				var taskId = e.data.linedata.TASK_ID;
				var warehouseId = e.data.linedata.WAREHOUSE_ID;
				var assignedUser = e.data.linedata.USER_ID+"";
				new reassignView().render({task_id:e.data.linedata.TASK_ID,list:list});
				
			});
			list.on("events.edit",function(e)
			{
				var taskId = e.data.linedata.TASK_ID;
				editCopyTask("edit",taskId);

			});
			list.on("events.delete",function(e)
			{
				var taskId = e.data.linedata.TASK_ID;
				$("#"+taskId).addClass("red");
				var dialog = new Dialog({
					    content: config.deleteTaskString,
					    lock:true,
					    width: 350,
					    height: 50,
					    title:"Delete Task",
					    ok: function (event) {
					    	deleteTask(taskId,dialog)
						},
					    cancelVal: 'No',
					    cancel: function(){
					    	dialog.close().remove();;
					    	$("#"+taskId).removeClass("red");
					    }
					}).showModal();
				
			});

			list.on("events.copy",function(e)
			{
				var taskId = e.data.linedata.TASK_ID;
				editCopyTask("copy",taskId);
			});
			list.on("events.resume",function(e)
			{
				var taskId = e.data.linedata.TASK_ID;
				resumeTask(taskId,list);
				
			});
			function editCopyTask(type,taskId)
			{	
				var ntv = new newTaskView({type:type, taskId:taskId, searchView:list}).render();
		     	
			}

			
			function deleteTask(taskId,dialog){
				
				$.ajax({
                      url:  config.deleteTaskURL+taskId+"&state=4",
                      type: 'put',
                      timeout: 60000,
                      cache:false,
                      dataType: 'json',
                      async:true,
                      
                      error: function(jqXHR, textStatus, errorThrown) {
                        showMessage("Error while deleting task.","error");
                       
                        $("#"+taskId).hide("5000", function(){$("#"+taskId).remove()});
                        $("#foot_"+taskId).hide("5000",function(){});
                        $("#"+taskId).removeClass("red");
                         dialog.close().remove();;
                      },
                      success: function(data){
                      	
                        showMessage("Task has been deleted.","succeed");
                        $("#"+taskId).hide("slow");
                        $("#foot_"+taskId).hide("slow");
                        $("#"+taskId).removeClass("red");
                        dialog.close().remove();;
                         
                      }
                });
			}
			function renderOneRow(olist,taskId)
			{	$("#"+taskId).addClass("grey");
				var j = $.getJSON(config.getSingleTaskDetail+taskId, {},
				 	function(json, textStatus) 
				 	{
				 		if(textStatus == "success")
				 		{
				 			olist.upSubitmeData({
							upitme:[{Root:"",itmeid:taskId,itmekey:"TASK_ID"}],
  							DATA:json
  							});
						Template.tbody.updateOneRow(taskId,json);
						
			 			}
				}).always(function(data){
					$("#"+taskId).removeClass("grey");
				});

				
			}
			function stopTask(task_id)
			{
				$.ajax({
                      url:  config.stopTaskURL+task_id+"&state=2",
                      type: 'put',
                      timeout: 60000,
                      cache:false,
                      dataType: 'json',
                      async:true,
                      
                      error: function(jqXHR, textStatus, errorThrown) {
                        showMessage("Error while stopping task.","error");
                      },
                      success: function(data){
                        showMessage("Task cycle has been stopped.","succeed");
                        renderOneRow(list,task_id);
                      }
                });

			}
			function resumeTask(task_id)
			{
				$.ajax({
                      url:  config.resumeTaskURL+task_id+"&state=1",
                      type: 'put',
                      timeout: 60000,
                      cache:false,
                      dataType: 'json',
                      async:true,
                      
                      error: function(jqXHR, textStatus, errorThrown) {
                        showMessage("Error while resuming task.","error");
                      },
                      success: function(data){
                        showMessage("Task cycle has been resumed.","succeed");
                        renderOneRow(list,task_id);
                      }
                });
			}
			function removeView(){
				list.undelegateEvents();
				list.remove();
			}
			
			return list;
		};
});
