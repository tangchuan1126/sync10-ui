define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getUsersOfWarehouseJSON: "/Sync10/_inventoryControl/search/usersOfWarehouse?ps_id=",
    getAllTitlesJSON: "/Sync10/_inventoryControl/search/titles",
    searchTasks: "/Sync10/_inventoryControl/cycleCount/search/taskList",
   	stopTaskURL: "/Sync10/_inventoryControl/cycleCount/update/updateTaskState?task_id=",
   	resumeTaskURL: "/Sync10/_inventoryControl/cycleCount/update/updateTaskState?task_id=",
   	deleteTaskURL: "/Sync10/_inventoryControl/cycleCount/update/updateTaskState?task_id=",
   	reAssignTaskURL : "/Sync10/_inventoryControl/cycleCount/update/reassignTask",
   	getSingleTaskDetail: "/Sync10/_inventoryControl/cycleCount/search/taskDetail?task_id=",
    editCopyTaskDetail: "/Sync10/_inventoryControl/cycleCount/search/taskDefinition?task_id=",
    getTaskProgress: "/Sync10/_inventoryControl/cycleCount/search/taskProgress?task_id=",

    submitScan:"/Sync10/_inventoryControl/cycleCount/execute/submitLocationScan",
   	getUsersAndAssignLoadJSON: "/Sync10/_inventoryControl/cycleCount/search/users",
   	deleteTaskString: "Are you sure you want to delete this task?",
    getTaskAssignments: "/Sync10/_inventoryControl/search/taskAssignments?task_id=",
    getAreaLocationUserJSON: "/Sync10/_inventoryControl/search/detailedAreaLocations?ps_id=",
    reassignVerificationTask: "/Sync10/_inventoryControl/cycleCount/update/reassignVerificationTask?task=",
    processDifference:"/Sync10/_inventoryControl/cycleCount/update/difference",
    systemLocation: "/Sync10/_inventoryControl/cycleCount/search/systemLocation?container_id=",

    addVerificationTask: "/Sync10/_inventoryControl/cycleCount/add/verificationTask",
    doesCodeExists: "/Sync10/_inventoryControl/cycleCount/search/isCodeExsiting?code=",
});
