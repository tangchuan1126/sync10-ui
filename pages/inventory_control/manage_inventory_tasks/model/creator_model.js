"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var UserModel = Backbone.Model.extend({
          idAttribute: "ADID",
        
          parse: function(data){
            this.text = data.EMPLOYE_NAME;
            this.value = data.ADID;
            return this;
          }
       });
       
      var UserCollection =  Backbone.Collection.extend({
           model: UserModel,
           url: config.getUsersOfWarehouseJSON+"1000005",

          comparator: function(item) {
                return item.get('text');
            },
       });

      return {
	      UserModel:UserModel,
	      UserCollection:UserCollection,

	    };




}); //page_init