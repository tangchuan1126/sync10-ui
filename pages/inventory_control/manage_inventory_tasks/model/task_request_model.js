"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      var ListModel = Backbone.Model.extend({
          idAttribute: "location_id",
          initialize:function(data){
            this.location_id = data.location_id;
            this.location_name = data.location_name;
          }
          

       });
      var ListCollection =  Backbone.Collection.extend({
           model: ListModel,

      });
      var AreaListModel = Backbone.Model.extend({
          idAttribute: "area_id",
          initialize:function(data){
            this.area_id = data.area_id;
            this.area_name = data.area_name;
            this.locations = data.locations;
          }
          

       });
      var AreaListCollection =  Backbone.Collection.extend({
           model: AreaListModel,

      });
      var TaskModel = Backbone.Model.extend({
        url:config.createTaskDifference,
        idAttribute: "id",
        defaults:{
           user:"",
           warehouse:"",
          },
        initialize:function(data){
          this.user = data.user;
          this.warehouse = data.warehouse;
          this.title = data.title;
          this.areas = data.areas;
          this.start_date = data.start_date;
          this.end_date = data.end_date;
          this.isRepeat = data.isRepeat;
          this.type = data.type;
          this.scheduled = data.scheduled;
          this.duration = data.duration;
          this.repeatOptions = data.repeatOptions;
          this.repeatEvery = data.repeatEvery;
          this.endsOn = data.endsOn;
          this.startsOn = data.startsOn;
          this.dayOfWeek = data.dayOfWeek;
        }

       });
       
       
      
      

      return {
        TaskModel:TaskModel,
        ListModel:ListModel,
        ListCollection:ListCollection,
        AreaListModel:AreaListModel,
        AreaListCollection:AreaListCollection,
      };




}); //page_init