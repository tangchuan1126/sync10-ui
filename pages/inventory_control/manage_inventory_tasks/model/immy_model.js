"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  
      
      var immyModel = Backbone.Model.extend({
          idAttribute: "value",

          parse: function(data){
            
            this.text = data.title ;
            this.value = data.id;
            return this;
          }
        
       });
       
      var immyCollection =  Backbone.Collection.extend({
           model: immyModel,
           url: config.getAllTitlesJSON,
           
       });

      return {
	      immyModel:immyModel,
	      immyCollection:immyCollection,

	    };




}); //page_init