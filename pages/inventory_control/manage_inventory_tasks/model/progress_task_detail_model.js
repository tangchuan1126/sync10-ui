"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      var TaskModel = Backbone.Model.extend({
        url:config.editCopyTaskDetail,
        idAttribute: "TASK_ID",
        defaults:{
           locations:null,
          },

          getType:function(){
            var dis = this;
            return dis.get("TYPE")==2 ? "Verify" : "Blind";
          },
          getRepeating:function(){
            var dis = this;
            return dis.get("IS_REPEAT")==false ? "No" : "Yes";
          },
          getPriority:function(value){
              var val = value;
              switch (value){
                case 1:
                  val= "Low";
                  break;
                case 2:
                  val = "Medium";
                  break;
                case 3:
                  val = "High";
                  break;

              }
              return val;
            },
          formatFields:function(){
            var dis = this;
            
            if(dis.get("INSTANCES")){
              $.each(dis.get("INSTANCES"), function(index, item){
                item.STATUS = dis.transformStatus(item.STATUS);
                $.each(item.AREAS, function(i,it){

                  if(it.STATUS ==1){
                    it.SHOW_DETAIL = false;
                  }else{
                    it.SHOW_DETAIL = true;
                  }
                  if(it.STATUS == 2 || it.STATUS ==4){
                    it.SHOW_PROGRESS = true;
                    it.PERCENT_DONE = ((it.DONECOUNT/it.TOTALCOUNT)*100)*2;
                  }
                  it.STATUS = dis.transformStatus(it.STATUS);
                })
              });
            }else{

                $.each(dis.get("AREAS"), function(i,it){
                  
                  if(it.STATUS ==1){
                    it.SHOW_DETAIL = false;
                  }else{
                    it.SHOW_DETAIL = true;
                  }
                  if(it.STATUS == 2 || it.STATUS ==3){
                    it.SHOW_PROGRESS = true;
                  }
                  it.STATUS = dis.transformStatus(it.STATUS);

                })
              
            }

            dis.set("STATUS",dis.transformStatus(dis.get("STATUS")));
            dis.set("TYPE",dis.getType());
            dis.set("PRIORITY",dis.getPriority(dis.get("PRIORITY")));
            dis.set("IS_REPEAT",dis.getRepeating());
            dis.set("LP_TYPE",dis.get("LP_TYPE_NAME"));
            if(dis.get("TITLES")){dis.set("TITLE",dis.formatTitle());}
            if(dis.get("MODEL_NUMBERS")){dis.set("MODELS",dis.formatModels());}
            if(dis.get("LOT_NUMBERS")){dis.set("LOTS",dis.formatLots());}
            dis.set("REPEAT_EVERY",dis.formatRepeatEvery(dis.get("REPEAT_EVERY"),dis.get("REPEAT_BY")));
            dis.set("REPEAT_BY",dis.formatRepeatBy(dis.get("REPEAT_BY")));
            dis.set("DURATION",dis.formatDuration(dis.get("DURATION")));
            dis.set("ASSIGNMENTS",dis.get("ASSIGNMENTS"));

          },
          formatLots:function(){
            var dis = this;
            var val = "";
            $.each(dis.get("LOT_NUMBERS"),function(index,item){
              if(val==""){val+= item;}
              else{val+= ", "+item;}  
              
            })
            return val;
          },
          formatModels:function(){
            var dis = this;
            var val = "";
            $.each(dis.get("MODEL_NUMBERS"),function(index,item){
              if(val==""){val+= item;}
              else{val+= ", "+item;}  
              
            })
            return val;
          },
          formatTitle:function(){
            var dis = this;
            var val = "";
            $.each(dis.get("TITLES"),function(index,item){
              if(val==""){val+= item.TITLE_NAME}
              else{val+= ", "+item.TITLE_NAME;}  
              
            })
            return val;
          },
          formatLpType:function(){
            var dis = this;
            var val = "";
            if(dis.get("LP_TYPE_LENGTH")){val+=dis.get("LP_TYPE_LENGTH")}
            if(dis.get("LP_TYPE_WIDTH")){val+="*"+dis.get("LP_TYPE_WIDTH")}
            if(dis.get("LP_TYPE_HEIGHT")){val+="*"+dis.get("LP_TYPE_HEIGHT")}
            return val;
          },
          transformStatus:function(value){
              var val = value;
              switch (value){
                case 1:
                  val= "New";
                  break;
                case 2:
                  val = "In-progress";
                  break;
                case 3:
                  val = "Done";
                  break;
                case 4:
                  val = "Pending Approval";
                  break;
                case 5:
                  val = "Reviewed";
                  break;

              }
              return val;
            },
            formatRepeatBy:function(value){
                var val = value;
                switch (value){
                  case 1:
                    val= "Daily";
                    break;
                  case 2:
                    val = "Weekly";
                    break;
                  case 3:
                    val = "Monthly";
                    break;

                }
                return val;
              },
              formatRepeatEvery:function(value,repeatBy){
                var val="";
                switch (repeatBy){
                  case 1:
                    val= "Day(s)";
                    break;
                  case 2:
                    val = "Week(s)";
                    break;
                  case 3:
                    val = "Month(s)";
                    break;

                }
                return value + " "+ val;
              },
              formatDuration:function(val){
                
                return val+" Day(s)";
              },
              formatEndsOn:function(obj){
                
                var res = "";
                switch (obj.IS_REPEAT){
                  case true:
                    if(obj.ENDS_TYPE){
                      switch (obj.ENDS_TYPE){
                        case 1:
                          res = "Never"
                          break;
                        case 2:
                          res = "After "+obj.ENDS_OCCURRENCE+" occurrences"
                          break;
                        case 3:
                          res = obj.ENDS_ON; 
                          break;
                      }
                    }
                    break;
                  case false:
                    res = obj.ENDS_ON;
                    break;
                }
                
                return res;
              
                
              },
       });


      

      return {
        TaskModel:TaskModel,
      };

});