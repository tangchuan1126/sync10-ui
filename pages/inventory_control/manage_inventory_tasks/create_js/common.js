"use strict";
define([
         "config", 
         "jquery",
        "jqueryui/tabs"
        
    ], 
 function(page_config,$){

 			var chengeation = function(jqtarget){
        
		            var pbox = jqtarget.next(".select2-container--default").eq(0);
		            var ul = pbox.find(".select2-selection__rendered").eq(0);
		            //ul.height(34);
		            var liall = ul.find("li:not('.select2-search--inline')");
		            //

		            window.setTimeout(function() {

		                if (liall.length > 0) {
		                    var liallwidth = 80;
		                    for (var lival = 0; lival < liall.length; lival++) {

		                        var liobj = liall.eq(lival);
		                        var liw = liobj.width();
		                        liallwidth = liallwidth + liw;
		                    }
		                    if (liallwidth > 100) {
		                        ul.width(liallwidth);
		                        $(".select2-selection__rendered").width("auto");

		                    }
		                }
		            }, 300);             

		      }
  
  			var setDefaultValueForSelect2 = function(div, values){
		        $.each(values,function(index,item){
		          $(div).find('option[value="'+item+'"]').prop("selected",true);
		        })
		      }

  			var highlight_button_group = function(id){

  				$("#areas_button_group .active").removeClass("active");
  				$("#"+id).addClass("active");
  			}
	        var show_selected_areas = function(){
	        	$("#toc_area .subitmebox").hide();
				$("#toc_area .focusedInput").parent().show();
				highlight_button_group("show_selected_areas_only");
				
	        }
	        var show_all_areas = function(){
	        	$("#toc_area .subitmebox").show();
	        	highlight_button_group("show_all_areas_only");
	        }
	        var show_assigned_areas = function(){
	        	$("#toc_area .subitmebox").show();
				$.each($("#toc_area .subitmebox"),function(index,item){
					if($(item).find(".assigned").length == 0){
						$(item).hide();
					}
				});
				highlight_button_group("show_assigned_areas_only");
			}
			var show_unassigned_areas = function(){
	        	$("#toc_area .subitmebox").hide();
				$.each($("#toc_area .subitmebox"),function(index,item){
					if($(item).find("li>div").not(".assigned").length > 0){
						$(item).show();
					}
				});
				highlight_button_group("show_unassigned_areas_only");
			}
			
			var check_preview = function(){
				var valid = true;
		        if(validate_assigned_area()==false){valid=false;}
		        if(valid){
		          $("#next_task_preview_div").removeClass("disabled");
		        }else{
		          $("#next_task_preview_div").addClass("disabled");
		        }
		      }
		    var validate_assigned_area = function(){
		        var dis=this;
		        var valid = false;
		        if($("#toc_area .itmeInventory_box .assigned_user").length > 0){valid = true;}
		        return valid;
		      }
		     var validate_deadline = function(){
		        
		        var valid = true;
		        if($("#create_task_name").val()==""){valid = false;}
		        if($("#is_repeating").prop('checked')){
		          if(validate_schedule_dates()==false){valid=false;}
		        }else{
		          if($("#start_date").val()==""){valid = false;}
		          if($("#end_date").val()==""){valid = false;}
		        }

		        return valid;
		      }
		      var validate_schedule_dates = function(){
		        var v = true;
		        if($("#starts_on").val()==""){v=false;}
		        
		        if($('#ends_table input:radio[name="ends_on"]:checked').val()=="never"){

		        }else if($('#ends_table input:radio[name="ends_on"]:checked').val()=="after"){
		            if($("#occurances").val()==""){v=false;}
		        }else if($('#ends_table input:radio[name="ends_on"]:checked').val()=="on"){
		            if($("#ends_on_date").val()==""){v=false;}
		        } 
		        
		        return v;
		      }
		      var check_area_selection = function(){

		      	if(validate_deadline()){
		      		
		      		$("#next_area_selection_div").removeClass("disabled");
	        	}else{
	        		$("#next_area_selection_div").addClass("disabled");
	        	}
		      }
	        var checkEnableCreateButton = function(){
	        	var show_assign= false;
	        	if($("#toc_area .itmeui_box :checked").length > 0){
		            show_assign = true;
		        }
		        

		        if(show_assign){
	          		$(".assign_user_icon button").removeClass("disabled");
	        	}else{
	        		$(".assign_user_icon button").addClass("disabled");
	        	}
	        }
	        var adjust_area_space = function(){
	        	
	        	$("#toc_area .Inventory_box").addClass("inventory_box_adjusted");
	        }
	        var full_area_space = function(){
	        	$("#toc_area .Inventory_box").removeClass("inventory_box_adjusted");
	        }
	        var hide_other_fields = function(){
	          $("#toc_area").html("");
	          $("#users_body").hide();
	          $("#deadline_body").hide();
	          $("#create_title_view").hide();
	          $("#create_product_line_view").hide();
	          $("#create_product_cat_view").hide();
	          $("#create_model_view").hide();
			  $("#create_lot_view").hide();
			  $("#create_lp_type_view").hide();
 
	        }

	        var showError = function(msg){
	          $("#error_block").html(msg);
	          $("#error_block").show();
	        }

	        var hideError = function(){
	          $("#error_block").html("");
	          $("#error_block").hide();
	        }

  			var show_other_fields = function(){
  				
	          $("#users_body").show();
	          $("#deadline_body").show();
	          $("#create_title_view").show();
	          $("#create_product_line_view").show();
	          $("#create_product_cat_view").show();
	          $("#create_model_view").show();
			  $("#create_lot_view").show();
			  $("#create_lp_type_view").show();
	        }
	        var show_users = function(dir){
	        	if($("#next_assign_user_div").hasClass("disabled")){

	        	}else{
	        		show_selected_areas();
	        		adjust_area_space();
	        		$("#users_body").show();
	        		stretch(dir);
	        	}
		        
		     
		    }
		    var show_areas = function(){
		    	stretch("right");
		    }
		    var show_date = function(dir){
		    	
		    	if($("#next_date_selection_div").hasClass("disabled")){
		        }else{
		        	stretch(dir);
			    }
		     
		    }
		    var show_task_preview = function(){
		    	if($("#next_task_preview").hasClass("disabled")){
		        }else{
		        	
		        	stretch_main("left");
			    }
		    }
		    var show_areas_div = function(){
		    	if($("#next_area_selection_div").hasClass("disabled")){
		        }else{
		        	stretch_main("left");
			    }
		    }
		    var hide_date = function(){
		       /* $("#create_task_body").show();
		        $("#deadline_body").hide();*/
		     
		    }
		    var show_date_deadline = function(){
		    	stretch_main("right");
		    }
		    var stretch = function(direction){
		    	
		    	var l='-=710px';
	        	if(direction=="right"){l='+=710px'}
        		$("#users_body").animate({left:l},500);
	        	
		    }
		    var task_preview_back = function(){
		    	stretch_main("right");
		    }
		    var stretch_main = function(direction){
		    	
		    	var l='-=1020px';
	        	if(direction=="right"){l='+=1020px'}
        		console.log("stretch_main")
		        $("#deadline_body").animate({left:l},500);
		    	$("#toc_area").animate({left:l},500);
		        $("#users_body").animate({left:l},500);
		       	$("#create_task_body").animate({left:l},500);

		    }
		    var slide_progress = function(direction){
		    	stretch_progress(direction);
		    }

		    var stretch_progress = function(direction){
		    	
		    	var l='-=1020px';
	        	if(direction=="right"){l='+=1020px'}
        		
		        $("#instances_progress").animate({left:l},500);
		    	$("#instances_area_progress").animate({left:l},500);
		        $("#location_differences_container").animate({left:l},500);
		    	
		    }
		    var reassign_show_users = function(){
		    	var l='-=470px';
		    	$("#reassign_view_table .right_side").animate({left:l},500);
		    }
		    var reassign_hide_users = function(){
		    	var l='+=470px';
		    	$("#reassign_view_table .right_side").animate({left:l},500);
		    }
		    var mark_areas_assigned = function(user_id, user_name){
		    	var all_selected_areas = $("#toc_area .Inventory_body .itmeInventory_box .checkbox input:checked");

		    	$.each(all_selected_areas,function(index,item){
		    		$(item).trigger("click");
		    		var parent = $(item).parent();
		    		parent.parent().addClass("assigned")
		    		parent.find(".assigned_user").remove();
		    		parent.find(".unassign_user_from_location").remove();
		    		var span = "<div class='assigned_user' data-id='"+user_id+"' title='"+user_name+"'>&nbsp;</div>";
		    		span += "<div class='unassign_user_from_location' data-id='"+user_id+"' title='Unassign'>&nbsp;</div>";
		    		parent.append(span);
		    	});
		    	$(".unassign_user_from_location").undelegate();
		    	$(".unassign_user_from_location").on("click", function(evt){unassign_user(evt)});
		    }
		    var unassign_user = function(evt){

		    	var label = $(evt.target).parent();
		    	label.parent().removeClass("assigned")
		    	label.children(".unassign_user_from_location").remove();
		    	label.children(".assigned_user").remove();
		    	check_preview();
		    	evt.preventDefault();
		    }
		    var assign_user_ok = function(){
		    	var par = $(".selected_employee").get(0);
		    	var emp = $(par).find(".employee_name").get(0);
		    	var selected_employee_id = $(emp).data("user_id");
		    	var selected_employee_name =$(emp).html();
		    	mark_areas_assigned(selected_employee_id,selected_employee_name);
		    	
		    	show_all_areas();
		    	show_areas();
		    	full_area_space();
		    	un_select_user();
		    	check_preview();
		    	$("#users_body").hide()
		    }
		    var assign_user_cancel = function(){
		    	show_all_areas();
		    	show_areas();
		    	full_area_space();
		    	un_select_user();
		    	$("#users_body").hide()
		    }
		    var un_select_user = function(){
		    	$(".selected_employee").removeClass("selected_employee");
		    }
		    var show_search_fields = function(){
  				
	          $("#search_title_list_view").show();
	          $("#search_product_line_list_view").show();
	          $("#search_product_cat_list_view").show();
	          $("#search_model_list_view").show();
	          $("#search_lot_list_view").show();
	          $("#search_lp_type_list_view").show();
			  $("#search_task_number_div").show();
			  $("#search_priority_list_view").show();
			  $("#search_user_list_view").show();
			  $("#search_created_by_list_view").show();
			  $("#search_type_list_view").show();
			  $("#search_repeat_list_view").show(); 
			  $("#search_status_list_view").show();
			  $("#search_start_date_view").show(); 
			  $("#search_end_date_view").show(); 
			  $("#manage_clear_btn").show();
			  $("#search_task_btn").show();
	        }

	        var hide_search_fields = function(){
  				$("#search_title_list_view").hide();
		        $("#search_product_line_list_view").hide();
		          $("#search_product_cat_list_view").hide();
		          $("#search_model_list_view").hide();
		          $("#search_lot_list_view").hide();
		          $("#search_lp_type_list_view").hide();
				  $("#search_task_number_div").hide();
				  $("#search_priority_list_view").hide();
				  $("#search_user_list_view").hide();
				  $("#search_created_by_list_view").hide();
				  $("#search_type_list_view").hide();
				  $("#search_repeat_list_view").hide(); 
				  $("#search_status_list_view").hide();
				  $("#search_start_date_view").hide(); 
				  $("#search_end_date_view").hide();
				  $("#manage_clear_btn").hide();
				  $("#search_task_btn").hide();
	          
	        }

	        	
		    return {
		    	setDefaultValueForSelect2:setDefaultValueForSelect2,
		    	chengeation:chengeation,
		    	slide_progress:slide_progress,
		    	task_preview_back:task_preview_back,
		    	show_task_preview:show_task_preview,
		    	show_date_deadline:show_date_deadline,
		    	show_areas_div:show_areas_div,
		    	check_area_selection:check_area_selection,
		    	hide_search_fields:hide_search_fields,
		    	show_search_fields:show_search_fields,
		    	check_preview:check_preview,
		    	validate_assigned_area:validate_assigned_area,
		    	validate_deadline:validate_deadline,
		    	validate_schedule_dates:validate_schedule_dates,
		    	adjust_area_space:adjust_area_space,
		    	full_area_space:full_area_space,
		    	show_selected_areas:show_selected_areas,
		    	show_all_areas:show_all_areas,
		    	checkEnableCreateButton:checkEnableCreateButton,
		    	hide_other_fields:hide_other_fields,
		    	showError:showError,
		    	hideError:hideError,
		    	show_other_fields:show_other_fields,
		    	show_users:show_users,
		    	show_areas:show_areas,
		    	show_date:show_date,
		    	hide_date:hide_date,
		    	reassign_show_users:reassign_show_users,
		    	reassign_hide_users:reassign_hide_users,
		    	assign_user_ok:assign_user_ok,
		    	assign_user_cancel:assign_user_cancel,
		    	show_assigned_areas:show_assigned_areas,
		    	show_unassigned_areas:show_unassigned_areas,
		    	unassign_user:unassign_user
		    }
  });