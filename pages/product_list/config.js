(function(){
    var configObj = {
            customSeachCollection: {
                url:"/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=yes"
            },
            productsCollection: {
                url:"/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action"
            },
            RelationModel: {
                url:"/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action"
            },
            baseLableModel:{
                url:"/Sync10/action/administrator/lableTemplate/lableTemplate.action"
            },
            saveLableModel: {
                url:"/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action"
            }
    };
    if (typeof define === 'function' && define.amd) {
        define(configObj);
    }
    else {
        //传统模式，非AMD标准
        this.page_config = configObj;
    }
}).call(this);
