define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['base_container_edit'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "							      <option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\" \n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.key : depth0),((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.type : stack1),{"name":"ifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							      > "
    + alias2(alias1((depth0 != null ? depth0.value : depth0), depth0))
    + "</option>\n";
},"2":function(depth0,helpers,partials,data) {
    return "							          selected=\"true\"\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return "  "
    + ((stack1 = this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1), depth0)) != null ? stack1 : "")
    + "\n";
},"6":function(depth0,helpers,partials,data) {
    return "\n<table style=\"font-size: 13px;font-family: Arial;text-align:left;vertical-align:center;width:100%;empty-cells:show;\nborder-collapse: collapse; \">\n  <tr style=\"height:43px;text-align:center;\">\n    <td rowspan=\"2\" style=\"font-size: 100px;border-right:2px solid #999;border-bottom:2px solid #999;\"><center><b>C </b></center></td>\n    <td colspan=\"2\" style=\"border-right: 2px solid #999;border-bottom: 2px solid #999;border-left: 2px solid #999;width:20%\"><h2 style=\"margin-top:16px;\">Date</h2></td>\n    <td style=\"border-bottom: 2px solid #999;border-left: 2px solid #999;width:40%\"><h2 style=\"margin-top:16px;font-weight:bold;\">{DATE}&nbsp;</h2></td>\n  </tr>\n  <tr>\n    <td colspan=\"3\" style=\"height:75px;border-bottom: 2px solid #999;width:60%\">&nbsp;&nbsp;Packaging Type<div style=\"font-size: 28px;font-weight:bold;\"><b><center>{TYPEID}&nbsp;</center></b></div></td>\n  </tr>   \n  <tr style=\"height:70px;text-align:center\">\n  	<td colspan=\"4\" style=\"border-bottom: 2px solid black;padding-top:5px;\"><img src=\"/barbecue/barcode?data=$PCODE$&width=1&height=50&type=code39\"/><span style=\"display:none\"></span><div style=\"font-size: 20px;font-weight:bold;\">{PCODE}&nbsp;</div></td>\n  </tr>\n  <tr style=\"height:45px\"> \n    <td colspan=\"4\" style=\"border-bottom: 2px solid black;\">&nbsp;&nbsp;LOT<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{LOT}&nbsp;</center></div></td>\n  </tr>\n  <tr style=\"height:45px\">\n    <td colspan=\"2\" style=\"border-right: 2px solid black;border-bottom: 2px solid black;\">&nbsp;&nbsp;Customer<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{CUSTOMER}&nbsp;</center></div></td>\n    <td colspan=\"2\" style=\"border-bottom: 2px solid black;\">&nbsp;&nbsp;Title<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{TITLE}&nbsp;</center></div></td>\n  </tr>\n  <tr style=\"height:45px\">\n    <td colspan=\"2\" style=\"border-right: 2px solid black;border-bottom: 2px solid black;\">&nbsp;&nbsp;Total-Products<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{TOTPDT}&nbsp;</center></div></td>\n    <td colspan=\"2\" style=\"border-bottom: 2px solid black;\">&nbsp;&nbsp;Total-Packages<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{TOTPKG}&nbsp;</center></div></td>\n  </tr> \n  <tr style=\"height:45px\">\n    <td colspan=\"2\" style=\"border-right: 2px solid black;border-bottom: 2px solid black;\">&nbsp;&nbsp;Piece<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{PIECE}&nbsp;</center></div></td>\n    <td colspan=\"2\" style=\"border-bottom: 2px solid black;\">&nbsp;&nbsp;Packages<br><div style=\"font-size: 20px;font-weight:bold;\"><center>{PACKAGES}&nbsp;</center></div></td>\n  </tr> \n  <tr style=\"height:108px;text-align:center\">\n    <td colspan=\"4\"><img src=\"/barbecue/barcode?data=$CONID$&width=1&height=50&type=code39\"/><span style=\"display:none\"></span><br>\n    <div style=\"font-size: 30px;font-weight:bold;\"><center>CLP{CONID}</center></div></td> \n  </tr>\n</table>\n";
},"8":function(depth0,helpers,partials,data) {
    return "                <div class='upload'>\n                  <input type='button' id='uploadButton' value='upload'/>\n                </div> \n";
},"10":function(depth0,helpers,partials,data) {
    var stack1;

  return "                "
    + ((stack1 = this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1), depth0)) != null ? stack1 : "")
    + "\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing, alias4="function";

  return "\n<div id=\"main\" style=\"width: 950px;\">\n    <div class=\"lableinfo\" style=\"background: #EEE;width: 950px;font-size: 12px;\">\n        <div style=\"width: 98%;margin-top: 5px\">               \n                 <div>\n                 	 <div style=\"width:10%;float:left;margin-top: 5px;text-align:right\">Label Name:&nbsp;</div>\n	                 <div style=\"width:17.5%;float:left;margin-top: 5px;text-align:left\">\n	                 	<input type=\"text\" id=\"lableName\" name=\"lableName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_name : stack1), depth0))
    + "\" size=\"15\">\n	        			<input type=\"hidden\" id=\"prevLableName\" name=\"prevLableName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_name : stack1), depth0))
    + "\" size=\"15\">         \n					 </div>\n	                 <div style=\"width:10%;float:left;margin-top: 5px;text-align:right\">Printer Name:&nbsp;</div>\n	                 <div style=\"width:17.5%;float:left;margin-top: 5px;text-align:left\">\n	                     <input type=\"text\" id=\"printName\" name=\"printName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.print_name : stack1), depth0))
    + "\" size=\"15\">\n	                 </div> \n	                 <div style=\"width:10%;float:left;margin-top: 5px;text-align:right\">Label  Type:&nbsp;</div>\n	                 <div style=\"width:35%;float:left;margin-top: 5px;text-align:left\">\n	                     <input type=\"text\" id=\"lableType\" name=\"lableType\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_type : stack1), depth0))
    + "\" size=\"15\">\n	                 </div>\n                 </div>\n                 <div>\n                 	<div style=\"width:10%;float:left;margin-top: 5px;text-align:right\">Label Width:&nbsp;</div>\n	                 <div style=\"width:17.5%;float:left;margin-top: 5px;text-align:left\">\n	                    <input type=\"text\" id=\"lableWidth\" name=\"lableWidth\" disabled=\"disabled\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_width : stack1), depth0))
    + "\" size=\"5\">px\n	                 </div>\n	                 <div style=\"width:10%;float:left;margin-top: 5px;text-align:right\">Label Height:&nbsp;</div>\n	                 <div style=\"width:17.5%;float:left;margin-top: 5px;text-align:left\">\n	                 	<input type=\"text\" id=\"lableHeight\" disabled=\"disabled\" name=\"lableHeight\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_height : stack1), depth0))
    + "\" size=\"5\">px\n	                 </div>\n	               \n	                 <div style=\"width:10%;float:left;margin-top: 5px;text-align:right\">type:&nbsp;</div>\n	                 <div style=\"width:14%;float:left;margin-top: 5px;text-align:left\">	\n							<select id=\"type\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.containerTypeKey : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "							</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                     \n	                 </div>\n	                 <div style=\"float:left;margin-top: 5px\">\n	                    \n	                    <input class=\"short-button\" type=\"button\" value=\"generate\" id=\"generate\" name=\"generate\">\n	                     <input class=\"short-button\" type=\"button\" value=\"preview\" id=\"preview\" name=\"preview\">\n	                     <input class=\"short-short-button-ok\" type=\"button\" value=\"save\" id=\"create\" name=\"create\">\n	                 </div>\n                 </div>\n              \n        </div>\n    </div> \n    <div>\n\n        <div class=\"container_origin\" id=\"containerContent\">\n          <p style=\"margin-top:3px;margin-bottom:3px;\">Edit the code:</p>          \n          <textarea id=\"containerText\" wrap=\"logical\" onClick>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1),{"name":"if","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1),{"name":"unless","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</textarea>\n        </div>\n        <div class=\"container_button\" id=\"containerButton\">\n          <img id='upArrow' data-value='"
    + alias2(((helper = (helper = helpers.isUpload || (depth0 != null ? depth0.isUpload : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(depth0,{"name":"isUpload","hash":{},"data":data}) : helper)))
    + "' src=\"./img/upArrow.jpg\"><br>\n\n          <div class='wrap'>\n                            \n            <div class='inner'>\n              <div class='normal'>\n                <div style='text-align:left;'  id='brief'><label style='line-height:30px;'>Normal Tag</label></div>\n                <input type='button' class='long-button' data-value='DATE' id='date' name='msgInsert' value='date' /><br>\n                <input type='button' class='long-button' data-value='TYPEID' id='containerTypeId' name='msgInsert' value='type id' /><br>\n                <input type='button' class='long-button' data-value='CONID' id='pid' name='msgInsert' value='con id'/><br>\n                <input type='button' class='long-button' data-value='NAME' id='productName' name='msgInsert' value='name'/><br>\n                <input type='button' class='long-button' data-value='LOT' id='lot' name='msgInsert' value='lot'/><br>\n                <input type='button' class='long-button' data-value='CUSTOMER' id='customer' name='msgInsert' value='customer'/><br>\n                <input type='button' class='long-button' data-value='TITLE' id='title' name='msgInsert' value='title'/><br>\n                <input type='button' class='long-button' data-value='TOTPDT' id='totalProducts' name='msgInsert' value='tot products'/>\n                <input type='button' class='long-button' data-value='TOTPKG' id='totalPackages' name='msgInsert' value='tot packages'/>\n                <input type='button' class='long-button' data-value='PIECE' id='piece' name='msgInsert' value='piece'/><br>\n                <input type='button' class='long-button' data-value='PACKAGES' id='packages' name='msgInsert' value='packages'/><br>\n                <input type='button' class='long-button' data-value='PCODE' id='pcode' name='msgInsert' value='pcode'/><br>                \n              </div>\n              <div class='barcode'>    \n                <div style='text-align:left;'  id='brief'><label style='line-height:30px;'>Barcode Tag</label></div>            \n                <input type='button' class='long-button' data-value='DATE' id='dateBarcode' name='barcodeInsert' value='date'/><br>\n                <input type='button' class='long-button' data-value='TYPEID' id='containerTypeIdBarcode' name='barcodeInsert' value='type id'/><br>\n                <input type='button' class='long-button' data-value='PID' id='pidBarcode' name='barcodeInsert' value='pid'/><br>\n                <input type='button' class='long-button' data-value='NAME' id='productNameBarcode' name='barcodeInsert' value='name'/><br>\n                <input type='button' class='long-button' data-value='LOT' id='lotBarcode' name='barcodeInsert' value='lot'/><br>\n                <input type='button' class='long-button' data-value='CUSTOMER' id='customerBarcode' name='barcodeInsert' value='customer'/><br>\n                <input type='button' class='long-button' data-value='SUPPLIER' id='supplierBarcode' name='barcodeInsert' value='supplier'/><br>\n                <input type='button' class='long-button' data-value='TOTPDT' id='totalProductsBarcode' name='barcodeInsert' value='tot products'/>\n                <input type='button' class='long-button' data-value='TOTPKG' id='totalPackagesBarcode' name='barcodeInsert' value='tot packages'/>\n                <input type='button' class='long-button' data-value='PIECE' id='pieceBarcode' name='barcodeInsert' value='piece'/><br>\n                <input type='button' class='long-button' data-value='PACKAGES' id='packagesBarcode' name='barcodeInsert' value='packages'/><br>\n                <input type='button' class='long-button' data-value='PCID' id='pcidBarcode' name='barcodeInsert' value='pcid'/><br>\n              </div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.isUpload : depth0),{"name":"if","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n          </div>\n           <img id='downArrow' data-value='"
    + alias2(((helper = (helper = helpers.isUpload || (depth0 != null ? depth0.isUpload : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(depth0,{"name":"isUpload","hash":{},"data":data}) : helper)))
    + "' src=\"./img/downArrow.jpg\">\n        </div>\n        <div class=\"container_compile\" id=\"containerCompile\">\n            <p style=\"margin-top:3px;margin-bottom:4px;\">Result:</p>            \n            <div id=\"containerPreview\" class=\"container_preview\">\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1),{"name":"if","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </div>            \n       </div>       \n  </div>  \n</div>\n\n";
},"useData":true,"useDepths":true});
templates['base_lable'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "    <tr class=\"addLable\" data-lableid=\""
    + alias2(alias1((depth0 != null ? depth0.lable_id : depth0), depth0))
    + "\" data-name=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" data-lable=\""
    + alias2(alias1((depth0 != null ? depth0.lable_content : depth0), depth0))
    + "\" data-type=\""
    + alias2(alias1((depth0 != null ? depth0.type : depth0), depth0))
    + "\" data-labletype=\""
    + alias2(alias1((depth0 != null ? depth0.lable_type : depth0), depth0))
    + "\" data-temptype=\""
    + alias2(alias1((depth0 != null ? depth0.lable_template_type : depth0), depth0))
    + "\" data-print=\""
    + alias2(alias1((depth0 != null ? depth0.print_name : depth0), depth0))
    + "\" data-width=\""
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "\">\r\n      <td align=\"center\" valign=\"middle\"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >"
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\" style='word-break:break-all;' >"
    + alias2(alias1((depth0 != null ? depth0.print_name : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\" style=\"line-height:20px;\">"
    + alias2(alias1((depth0 != null ? depth0.lable_type : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\">"
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\">"
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "</td>\r\n\r\n      <td align=\"center\" valign=\"middle\" nowrap=\"nowrap\">\r\n        <input name=\"showTemp\" type=\"button\" class=\"short-button\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "\" data-width=\""
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "\" lablename=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" lableContent=\""
    + alias2(alias1((depth0 != null ? depth0.lable_content : depth0), depth0))
    + "\" value=\"Preview\" />\r\n \r\n      </td>\r\n    </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return " <a class=\"gop\" data-pageno=\"1\" >First</a>&nbsp;&nbsp;";
},"5":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">First</font>";
},"7":function(depth0,helpers,partials,data) {
    return " <a class=\"gop\" data-pageplus=\"-1\">Previous</a>&nbsp;&nbsp;";
},"9":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">Previous</font>";
},"11":function(depth0,helpers,partials,data) {
    return " <a class=\"gop\" data-pageplus=\"1\">Next</a>&nbsp;&nbsp; ";
},"13":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">Next</font>";
},"15":function(depth0,helpers,partials,data) {
    var stack1;

  return " <a class=\"gop\" data-pageno=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\">Last</a>";
},"17":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">Last</font>";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"zebraTable\" style=\"margin:0px;border-spacing:2px;\"> \r\n    <tr> \r\n        <th class=\"left-title\" style=\"text-align: center;\">TemplateName</th>\r\n        <th class=\"right-title\" style=\"vertical-align: center;text-align: center;\">PrinterName</th>\r\n        <th align=\"left\"  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">PaperType</th>\r\n        <th align=\"center\"  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">Width</th>\r\n        <th  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">Height</th>\r\n        \r\n        <th width=\"165\"  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">Operation</th>\r\n    </tr> \r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.lables : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </table>\r\n\r\n  <table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\">\r\n     <tr>\r\n        <td colspan=\"6\" class=\"turn-page-table\" valign=\"middle\" height=\"28\" align=\"right\" style=\"font-family: Verdana,Geneva,sans-serif;\r\n    font-size: 8pt;\"> \r\n            Page: "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\r\n            Total："
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp; \r\n            \r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"if","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"if","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"if","hash":{},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            Goto<input id=\"jump_p2\" type=\"text\" value=\"1\" style=\"width:28px;\" name=\"jump_p2\">\r\n            <button class=\"gop\" data-pageto=\"1\" >GO</button>\r\n        </td>\r\n    </tr>  \r\n</table>\r\n";
},"useData":true});
templates['base_lable_edit'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                           <option value=\""
    + alias2(alias1((depth0 != null ? depth0.type : depth0), depth0))
    + "\" data-width=\""
    + alias2(alias1((depth0 != null ? depth0.width : depth0), depth0))
    + "\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.height : depth0), depth0))
    + "\"\r\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.type : depth0),((stack1 = (depths[1] != null ? depths[1].model : depths[1])) != null ? stack1.lable_type : stack1),{"name":"ifCond","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                           > "
    + alias2(alias1((depth0 != null ? depth0.type : depth0), depth0))
    + "</option>        \r\n";
},"2":function(depth0,helpers,partials,data) {
    return "                                 selected = \"true\"\r\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return "            "
    + ((stack1 = this.lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1), depth0)) != null ? stack1 : "")
    + "\r\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "            <div style=\"background:#fff;padding:0px;margin-right:auto; margin-left:auto;width:"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_width : stack1), depth0))
    + "px;height:"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_height : stack1), depth0))
    + "px;font-size:12px;border:1px red solid;cursor:grab;\" id=\"edit\"></div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\r\n<div id=\"main\" style=\"width: 750px;\">\r\n    <div class=\"lableinfo\" style=\"background: #EEE;width: 750px;font-size: 12px;\">\r\n        <table style=\"width: 98%;border-collapse:separate;border-spacing:2px;\" >\r\n              <tr> \r\n                  <td align=\"right\">Label Name: </td>\r\n                  <td>\r\n                     <input type=\"text\" id=\"lableName\" name=\"lableName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_name : stack1), depth0))
    + "\" size=\"15\">\r\n			<input type=\"hidden\" id=\"prevLableName\" name=\"prevLableName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_name : stack1), depth0))
    + "\" size=\"15\">                  \r\n		 </td>\r\n                  <td align=\"right\">Paper Type: </td>\r\n                  <td>\r\n                    <select id=\"lableType\" name=\"lableType\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.paperType : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                     </select>\r\n                     <input type=\"hidden\" id=\"lableWidth\" name=\"lableWidth\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_width : stack1), depth0))
    + "\" size=\"5\">\r\n                     <input type=\"hidden\" id=\"lableHeight\" name=\"lableHeight\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_height : stack1), depth0))
    + "\" size=\"5\"> \r\n                   </td>\r\n                   <td>&nbsp;</td>\r\n               </tr>\r\n               <tr>\r\n               	  <td align=\"right\">Printer Name: </td>\r\n               	  <td>\r\n                     <input type=\"text\" id=\"printName\" name=\"printName\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.print_name : stack1), depth0))
    + "\" size=\"15\">\r\n                  </td>\r\n                  <td>&nbsp;</td>\r\n                  <td>&nbsp;</td>\r\n                 <td>\r\n                   <input type=\"hidden\" id=\"detailLableId\" name=\"detailLableId\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.detail_lable_id : stack1), depth0))
    + "\">\r\n                   <input type=\"hidden\" id=\"type\" name=\"type\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.type : stack1), depth0))
    + "\">\r\n                   <input type=\"hidden\" id=\"lableTemplateType\" name=\"lableTemplateType\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_template_type : stack1), depth0))
    + "\">\r\n                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n                   <input class=\"short-button\" type=\"button\" value=\"preview\" id=\"preview\" name=\"preview\">\r\n                   &nbsp;&nbsp;&nbsp;\r\n                   <input class=\"short-short-button-ok\" type=\"button\" value=\"save\" id=\"create\" name=\"create\">\r\n                 </td>\r\n              </tr>\r\n        </table>\r\n    </div>\r\n    <div class=\"lablebackground\">\r\n        <div class=\"tablecloth\" id=\"lableContent\">\r\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1),{"name":"if","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.lable_content : stack1),{"name":"unless","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\r\n\r\n        <div class=\"btns\">\r\n            <div class=\"elements\">\r\n            \r\n                <table id=\"custom\" style=\"border-collapse:separate;border-spacing:2px;\">\r\n                    <tr class=\"tr\">\r\n                      <td>Line</td>\r\n                      <td><button class=\"short-short-button-redtext\" style=\"color:black;\" id=\"addLine\">ADD</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td>VerticalLine</td>\r\n                        <td><button class=\"short-short-button-redtext\" style=\"color:black;\" id=\"addVertical\">ADD</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td>width:</td>\r\n                        <td><input type=\"text\" id=\"elWidth\" name=\"elWidth\" value=\"\" size=\"1\">px</td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td>height:</td>\r\n                        <td><input type=\"text\" id=\"elHeight\" name=\"elHeight\" value=\"\" size=\"1\">px</td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td>font-size:</td>\r\n                        <td><input type=\"text\" id=\"elFont\" name=\"elFont\" value=\"\" size=\"1\">px</td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td>text-align:</td>\r\n                        <td>\r\n                          <select id=\"elAlign\">\r\n                            <option value=\"left\">left</option>\r\n                            <option value=\"center\">center</option>\r\n                            <option value=\"right\">right</option>\r\n                          </select>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n\r\n                 <table id=\"common\" style=\"display:none;border-collapse:separate;border-spacing:2px;\">\r\n                	<tr class=\"tr\">\r\n                        <td colspan=\"2\" class=\"tag_font\"></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"PCID\">pcId</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"PCNAME\">pcName</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"CODE\">code</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"UPC\">upc</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"LOGO\">logo</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"LOT\">lot</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"SN\">sn</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"SHIPTO\">shipTo</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"WIDTH\">width</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"HEIGHT\">height</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                         <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"LENGTH\">length</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"WEIGHT\">weight</button></td>\r\n                    </tr>\r\n                    <tr class=\"tr\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"ORIGIN\">origin</button></td>\r\n                        <td class=\"common-element-td\">&nbsp;</td>\r\n                    </tr>\r\n                    <tr class=\"tr\" style=\"display:none;\">\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"CATEGORY\">category</button></td>\r\n                        <td class=\"common-element-td\"><button class=\"common-element-button\" data-val=\"ORIGIN\">origin</button></td>\r\n                    </tr>\r\n                </table>\r\n                \r\n                <table style=\"border-collapse:separate;border-spacing:2px;\">\r\n                 	<tr>\r\n                      <td colspan=\"2\" style=\"text-align:right;height:90px;\">\r\n                      	<button class=\"short-button-redtext\" style=\"color:black;font-size:10px;\" data-val=\"1\" id=\"addCommon\">common</button>\r\n                    	<button class=\"short-button-redtext\" style=\"color:black;font-size:10px;\" data-val=\"2\" id=\"addBarCode\">BarCode</button>\r\n                        <button class=\"short-button\" style=\"display:none;font-size:10px;\" id=\"goBack\">back</button>\r\n                        <input type=\"hidden\" id=\"commonType\" value=\"\"/>\r\n                      </td>\r\n                    </tr>\r\n                </table>\r\n            </div>\r\n            \r\n  \r\n  <div class=\"turn\">\r\n                 <div class=\"controler\">\r\n                    <div align=\"left\"><button  id=\"remove\">REMOVE</button>\r\n                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n                      SPED:<input type=\"text\" id=\"sped\" value=\"1\" maxlength=\"2\" size=\"1\"/></div>\r\n                  </div>\r\n              <div class=\"controlerBut\">\r\n                <br> \r\n                <div class=\"turnTop\">\r\n                	<input type=\"image\" id=\"turnTop\" src=\"./img/turn_up.png\" style=\"border:none;\" width=\"20px;\"height=\"20px;\"/>\r\n                </div>\r\n                <div class=\"leftRight\"> \r\n                    <div class=\"turnLeft\">\r\n                    <input type=\"image\" id=\"turnLeft\" src=\"./img/turn_left.png\" style=\"border:none;\" width=\"20px;\"height=\"20px;\"/>\r\n                    </div>\r\n                    <div class=\"turnRight\">\r\n                    <input type=\"image\" id=\"turnRight\" src=\"./img/turn_right.png\" style=\"border:none;\" width=\"20px;\"height=\"20px;\"/>\r\n                    </div>\r\n                </div>\r\n                <div class=\"turnBottom\">\r\n                <input type=\"image\" id=\"turnBottom\" src=\"./img/turn_down.png\" style=\"border:none;\" width=\"20px;\"height=\"20px;\"/>\r\n                </div>\r\n              </div>\r\n            </div>\r\n        </div>       \r\n    </div>\r\n  </div>\r\n";
},"useData":true,"useDepths":true});
templates['delete_lable'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "    <tr>\r\n      <td align=\"center\" valign=\"middle\"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >"
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\" style='word-break:break-all;' >"
    + alias2(alias1((depth0 != null ? depth0.print_name : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\" style=\"line-height:20px;\">"
    + alias2(alias1((depth0 != null ? depth0.lable_type : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\">"
    + alias2(alias1((depth0 != null ? depth0.title_name : depth0), depth0))
    + "</td>\r\n      <td align=\"center\" valign=\"middle\" s>\r\n        <input class=\"short-short-button-del\" data-templateid=\""
    + alias2(alias1((depth0 != null ? depth0.detail_lable_id : depth0), depth0))
    + "\" data-title="
    + alias2(alias1((depth0 != null ? depth0.title_id : depth0), depth0))
    + " data-lablename=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" name=\"del\" type=\"button\" value=\"Del\" />\r\n      </td>\r\n    </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    return "		<tr>\r\n			<td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\" colspan=\"5\">NO Data</td>\r\n		</tr>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n  <tr>\r\n    <td align=\"left\" >\r\n      <div class=\"right-title clearfix\" style=\"height:auto; border-left-width:1px;\">\r\n      <div class=\"col-md-7\">\r\n        <input class=\"form-control\" placeholder=\"Template Name\" name=\"input_lableName\" type=\"text\" id=\"input_lableName\" size=\"20\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.input_lableName || (depth0 != null ? depth0.input_lableName : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"input_lableName","hash":{},"data":data}) : helper)))
    + "\"/> \r\n        </div>\r\n        <div class=\"col-md-5\">\r\n        <button id=\"search\" data-submit=\"1\" class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-search\"></i>Search</button>\r\n        </div>\r\n      </div>\r\n    </td>\r\n  </tr>\r\n</table>\r\n\r\n\r\n<div style=\"height:300px; overflow-y:auto; margin-top: 5px;\">\r\n<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"zebraTable\">\r\n  <thead>\r\n    <tr> \r\n        <th class=\"left-title\" style=\"text-align: center;\">TemplateName</th>\r\n        <th class=\"right-title\" style=\"vertical-align: center;text-align: center;\">PrinterName</th>\r\n        <th align=\"left\"  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">PaperType</th>\r\n        <th align=\"center\"  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">Title</th>\r\n        <th width=\"165\"  class=\"right-title\" style=\"vertical-align: center;text-align: center;\">Operation</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody> \r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.detailLabels : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.detailLabels : depth0),{"name":"unless","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </tbody>\r\n  </table>\r\n  </div>\r\n\r\n";
},"useData":true});
templates['multiple_conditions'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"search_model_04\">\r\n	<input name=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "se\" type=\"checkbox\"  value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" title=\""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\" />\r\n	<a href=\"javascript:void(0)\" class=\"multiple_condition-01\" zhi=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" key=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\" title=\""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\" >"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\r\n</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.datas : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "<div class=\"multiple_condition-02\" align=\"center\">\r\n	<input type=\"button\" value=\"确定\" class=\"short-short-button-ok\" id=\"multiple_ok_button\" name=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\"/>\r\n	<input type=\"button\" value=\"取消\" class=\"short-short-button-del\" id=\"multiple_cancel_button\"  name=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\" />\r\n</div>\r\n";
},"useData":true});
templates['product_seach_name'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return " <!-- 名字搜索 -->\n<div id=\"easy_search_father\">\n	<div id=\"easy_search\" style=\"margin-top:-2px;\">\n		<img id=\"eso_search\" src=\"img/easy_search_2.png\" width=\"70\" height=\"50\" border=\"0\"/>\n	</div>\n</div>\n<table width=\"400\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n    <tr>\n	<td width=\"418\">\n		<div  class=\"search_shadow_bg\">\n			<input name=\"seachInput\" id=\"seachInput\" value=\"\"  type=\"text\"  class=\"search_input ui-autocomplete-input\" style=\"font-size:14px;font-family:Arial;color:#333333;width:400px;height:30px;font-weight:bold;\"/>\n		</div>\n	</td> \n    </tr>\n</table>	";
},"useData":true});
templates['products'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "	  <tr>\r\n		<td align=\"center\" valign=\"middle\" style='word-break:break-all'>\r\n			<fieldset style=\"border:2px solid blue;width:90%;padding:7px;margin-top: 0px;position: relative;border-radius: 5px;\">\r\n				<legend style=\"font-size:15px;font-weight:bold;padding:0 5px;border:medium none;width: auto;margin-bottom:0;\" > \r\n					"
    + alias2(alias1((depth0 != null ? depth0.pc_name : depth0), depth0))
    + "	\r\n				</legend>	\r\n				<span style=\"font-size: 12px\">Main Code:</span>\r\n				<span style=\"font-size: 12px; font-weight:bold;\">\r\n					<span style=\"font-size: 12px;font-weight:bold;\">"
    + alias2(alias1((depth0 != null ? depth0.p_code : depth0), depth0))
    + "</span>\r\n				</span><br>\r\n				<span style=\"font-size: 12px\">Dimension:</span>\r\n				<span style=\"font-size: 12px; font-weight:bold;\">\r\n					<span style=\"font-size: 12px;font-weight:bold;\">"
    + alias2(alias1((depth0 != null ? depth0.dimension : depth0), depth0))
    + "</span>\r\n				</span>\r\n				<div style=\"font-size: 12px;\">Product Category:</div>\r\n				<div style=\"padding-left: 15px;font-size: 12px\">\r\n					<span  style=\"font-size: 12px;font-weight:bold;\">\r\n						"
    + ((stack1 = alias1((depth0 != null ? depth0.catalog_text : depth0), depth0)) != null ? stack1 : "")
    + "\r\n					</span>\r\n				</div>	  								\r\n			</fieldset>	\r\n		</td>\r\n		<td align=\"center\" valign=\"middle\" style='word-break:break-all'>			\r\n			<fieldset style=\"border:2px red solid;padding:7px;width:90%;word-break:break-all;margin-top:0px;margin-bottom:15px;font-weight:bold;border-radius: 5px;\">\r\n				<legend style=\"font-size:15px;color:green;padding-top:0 5px;font-weight:bold; border:medium none;width: auto;\">Title\r\n					</span>\r\n				</legend>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.title_list : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	   		</fieldset>\r\n		</td>\r\n		<td align=\"center\" valign=\"middle\" style='word-break:break-all'>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.product_lable : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </td>\r\n            <td align=\"center\" valign=\"middle\" style='word-break:break-all'>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.contaioner_lable : depth0),{"name":"each","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </td>\r\n            \r\n            <td align=\"center\">\r\n            	<button type=\"button\" id=\"addSingleTemp\" class=\"btn btn-default btn-sm\" data-pcid=\""
    + alias2(alias1((depth0 != null ? depth0.pc_id : depth0), depth0))
    + "\">\r\n  					<span class=\"glyphicon glyphicon-plus\"></span> Add Template\r\n				</button>\r\n            </td>\r\n    		</tr>\r\n";
},"2":function(depth0,helpers,partials,data) {
    return "					<span>【"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title_name : depth0), depth0))
    + "】\r\n					</span>	\r\n";
},"4":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "		<div class=\"clearfix\" style=\"margin:5px 0;\">\r\n			<div style=\"width:200px; float:left; height:30px;\">\r\n				<a href=\"javascript:void(0)\" class=\"search_model_05\" style=\"color:#BE6307;text-decoration: underline;\" data-lable=\""
    + alias2(alias1((depth0 != null ? depth0.lable_content : depth0), depth0))
    + "\"  data-width=\""
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "\" name=\"showlable\"> "
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "</a>\r\n			</div>\r\n			<div class=\"btn-group\" role=\"group btn-group-sm\" aria-label=\"...\">\r\n				<button type=\"button\"  name=\"edit\" class=\"btn btn-default btn-sm\" data-pcid=\""
    + alias2(alias1((depths[1] != null ? depths[1].pc_id : depths[1]), depth0))
    + "\" data-templateid=\""
    + alias2(alias1((depth0 != null ? depth0.detail_lable_id : depth0), depth0))
    + "\" data-name=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" data-lable=\""
    + alias2(alias1((depth0 != null ? depth0.lable_content : depth0), depth0))
    + "\" data-type=\""
    + alias2(alias1((depth0 != null ? depth0.type : depth0), depth0))
    + "\" data-labletype=\""
    + alias2(alias1((depth0 != null ? depth0.lable_type : depth0), depth0))
    + "\" data-temptype=\""
    + alias2(alias1((depth0 != null ? depth0.lable_template_type : depth0), depth0))
    + "\" data-print=\""
    + alias2(alias1((depth0 != null ? depth0.print_name : depth0), depth0))
    + "\" data-width=\""
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "\">\r\n  					<span class=\"glyphicon glyphicon-pencil\"></span> Edit\r\n				</button>\r\n				<button type=\"button\" class=\"btn btn-default btn-sm\" data-relationid=\""
    + alias2(alias1((depth0 != null ? depth0.relation_id : depth0), depth0))
    + "\" data-templateid=\""
    + alias2(alias1((depth0 != null ? depth0.detail_lable_id : depth0), depth0))
    + "\" data-pcname=\""
    + alias2(alias1((depths[1] != null ? depths[1].pc_name : depths[1]), depth0))
    + "\" data-lablename=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" name=\"del\">\r\n  					<span class=\"glyphicon glyphicon-remove\"></span> Del\r\n				</button>\r\n			</div>\r\n		</div>\r\n";
},"6":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "			<div class=\"clearfix\" style=\"margin:5px 0;\">\r\n				<div style=\"width:200px; float:left; height:30px;\">\r\n					<a href=\"javascript:void(0)\" class=\"search_model_05\" style=\"color:#BE6307;text-decoration: underline;\" data-lable=\""
    + alias2(alias1((depth0 != null ? depth0.lable_content : depth0), depth0))
    + "\"  data-width=\""
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "\" name=\"showlable\"> "
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "</a>\r\n				</div>\r\n				<div class=\"btn-group\" role=\"group btn-group-sm\" aria-label=\"...\">\r\n					<button type=\"button\" class=\"btn btn-default btn-sm\" data-pcid=\""
    + alias2(alias1((depths[1] != null ? depths[1].pc_id : depths[1]), depth0))
    + "\" data-templateid=\""
    + alias2(alias1((depth0 != null ? depth0.detail_lable_id : depth0), depth0))
    + "\" data-name=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" data-lable=\""
    + alias2(alias1((depth0 != null ? depth0.lable_content : depth0), depth0))
    + "\" data-type=\""
    + alias2(alias1((depth0 != null ? depth0.type : depth0), depth0))
    + "\" data-labletype=\""
    + alias2(alias1((depth0 != null ? depth0.lable_type : depth0), depth0))
    + "\" data-temptype=\""
    + alias2(alias1((depth0 != null ? depth0.lable_template_type : depth0), depth0))
    + "\" data-print=\""
    + alias2(alias1((depth0 != null ? depth0.print_name : depth0), depth0))
    + "\" data-width=\""
    + alias2(alias1((depth0 != null ? depth0.lable_width : depth0), depth0))
    + "\" data-height=\""
    + alias2(alias1((depth0 != null ? depth0.lable_height : depth0), depth0))
    + "\" name=\"edit\">\r\n  						<span class=\"glyphicon glyphicon-pencil\"></span> Edit\r\n					</button>\r\n					<button type=\"button\" class=\"btn btn-default btn-sm\" data-relationid=\""
    + alias2(alias1((depth0 != null ? depth0.relation_id : depth0), depth0))
    + "\" data-pcname=\""
    + alias2(alias1((depths[1] != null ? depths[1].pc_name : depths[1]), depth0))
    + "\" data-templateid=\""
    + alias2(alias1((depth0 != null ? depth0.detail_lable_id : depth0), depth0))
    + "\" data-lablename=\""
    + alias2(alias1((depth0 != null ? depth0.lable_name : depth0), depth0))
    + "\" name=\"del\">\r\n  						<span class=\"glyphicon glyphicon-remove\"></span> Del\r\n					</button>\r\n				</div>\r\n			</div>\r\n";
},"8":function(depth0,helpers,partials,data) {
    return "		<tr>\r\n			<td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\" colspan=\"6\">NO Data</td>\r\n		</tr>\r\n";
},"10":function(depth0,helpers,partials,data) {
    return " <a class=\"gop\" style=\"cursor:pointer\"data-pageno=\"1\" >First</a>&nbsp;&nbsp;";
},"12":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">First</font>";
},"14":function(depth0,helpers,partials,data) {
    return " <a class=\"gop\" style=\"cursor:pointer\"data-pageplus=\"-1\">Previous</a>&nbsp;&nbsp;";
},"16":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">Previous</font>";
},"18":function(depth0,helpers,partials,data) {
    return " <a class=\"gop\" style=\"cursor:pointer\" data-pageplus=\"1\">Next</a>&nbsp;&nbsp; ";
},"20":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">Next</font>";
},"22":function(depth0,helpers,partials,data) {
    var stack1;

  return " <a class=\"gop\" style=\"cursor:pointer\" data-pageno=\""
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "\">Last</a>";
},"24":function(depth0,helpers,partials,data) {
    return " <font color=\"#AAAAAA\">Last</font>";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"search_result_body\">\r\n<table align=\"center\" cellpadding=\"3\" cellspacing=\"0\" class=\"table table-striped table-bordered \">\r\n<thead>\r\n	<tr> \r\n		<th width=\"20%\">Product Info</th>\r\n		<th width=\"20%\">Title</th>\r\n		<th width=\"30%\">Product Template</th>\r\n		<th width=\"30%\">Container Template</th>\r\n    	<th width=\"10%\">&nbsp;Operation</th>\r\n	</tr>\r\n	</thead>\r\n<tbody class=\"mytbody\">	\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.products : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers.unless.call(depth0,(depth0 != null ? depth0.products : depth0),{"name":"unless","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "	  </tbody>\r\n</table>\r\n\r\n<table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\">\r\n     <tr>\r\n        <td colspan=\"6\" class=\"turn-page-table\" valign=\"middle\" height=\"28\" align=\"right\" style=\"font-family: Verdana,Geneva,sans-serif;\r\n    font-size: 8pt;\"> \r\n            Page: "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageCount : stack1), depth0))
    + "&nbsp;&nbsp;\r\n            Total: "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.allCount : stack1), depth0))
    + "&nbsp;&nbsp; \r\n            \r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"if","hash":{},"fn":this.program(10, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.first : stack1),{"name":"unless","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"if","hash":{},"fn":this.program(14, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.fornt : stack1),{"name":"unless","hash":{},"fn":this.program(16, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"if","hash":{},"fn":this.program(18, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.next : stack1),{"name":"unless","hash":{},"fn":this.program(20, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            "
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"if","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers.unless.call(depth0,((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.last : stack1),{"name":"unless","hash":{},"fn":this.program(24, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n            Goto<input id=\"jump_p2\" type=\"text\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.pageCtrl : depth0)) != null ? stack1.pageNo : stack1), depth0))
    + "\" style=\"width:28px;\" name=\"jump_p2\">\r\n            <button class=\"gop\" data-pageto=\"1\" >GO</button>\r\n        </td>\r\n    </tr>  \r\n</table>\r\n</div>\r\n";
},"useData":true,"useDepths":true});
templates['seach_conditions'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div class=\"serch_conditions_01 "
    + alias3(((helper = (helper = helpers.parentKey || (depth0 != null ? depth0.parentKey : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"parentKey","hash":{},"data":data}) : helper)))
    + " "
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" id=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" key=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" zhi=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" name=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" type=\"div\">\r\n	"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "<span class=\"serch_conditions_02\" id=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" key=\""
    + alias3(((helper = (helper = helpers.key || (depth0 != null ? depth0.key : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" data-name=\""
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" zhi=\""
    + alias3(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" type=\"span\">"
    + alias3(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "&nbsp;×<span>\r\n</div>";
},"useData":true});
templates['seach_model'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.value : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div id=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\" class=\"search_model_01 "
    + alias2(alias1((depth0 != null ? depth0.parentKey : depth0), depth0))
    + " "
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">\r\n	<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n		<tr>\r\n			<td width=\"10%\" height=\"30px;\" align=\"right\"><span class=\"search_model_02\">"
    + alias2(alias1((depth0 != null ? depth0.title : depth0), depth0))
    + "</span></td>\r\n			<td width=\"81%\" valign=\"middle\">\r\n				<div class=\"search_model_03\" suan=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.value : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\r\n			</td>\r\n			<td class=\"search_model_06\">\r\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.select : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n";
},"3":function(depth0,helpers,partials,data,blockParams,depths) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "					<div class=\"search_model_04\">\r\n						<a href=\"javascript:void(0)\" class=\"search_model_05\" titleName=\""
    + alias2(alias1((depths[1] != null ? depths[1].title : depths[1]), depth0))
    + "\" parentKey=\""
    + alias2(alias1((depths[1] != null ? depths[1].parentKey : depths[1]), depth0))
    + "\" style=\"color:#BE6307\" zhi=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" key=\""
    + alias2(alias1((depths[1] != null ? depths[1].key : depths[1]), depth0))
    + "\" title=\""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\" >"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\r\n					</div>\r\n";
},"5":function(depth0,helpers,partials,data) {
    return "				<input id=\"multiSelect\" name=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.key : depth0), depth0))
    + "\" class=\"short-short-button-ok\" type=\"button\" value=\"Multi\" />\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.datas : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['show_view'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function";

  return "<div style=\"background: #FFF;padding:0px;float: left;width:"
    + this.escapeExpression(((helper = (helper = helpers.width || (depth0 != null ? depth0.width : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"width","hash":{},"data":data}) : helper)))
    + "px;position:relative;text-align:center;border:1px solid red;\" id=\"lableContent\">\r\n       "
    + ((stack1 = ((helper = (helper = helpers.lableContent || (depth0 != null ? depth0.lableContent : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"lableContent","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n</div>";
},"useData":true});
templates['single_conditions'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"search_model_04\">\r\n	<a href=\"javascript:void(0)\" class=\"search_model_05\" zhi=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" key=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\" title=\""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\" >"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\r\n</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.datas : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['template_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"btn-group\" role=\"group btn-group-sm\" aria-label=\"...\">\r\n<button id=\"add_button\" class=\"btn btn-default btn-sm\" type=\"button\">\r\n	<i class=\"glyphicon glyphicon-plus\"></i> Batch Add\r\n</button>\r\n\r\n<button type=\"button\" class=\"btn btn-default btn-sm\" id=\"delete_button\">\r\n  <span class=\"glyphicon glyphicon-remove\"></span> Batch Del\r\n</button>\r\n</div>";
},"useData":true});
templates['upload_photo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"photo_basic_layout\">\r\n	<div class=\"photo_basic_layout_select\">\r\n		<span style=\"font-weight:bold;\">Select The Picture&nbsp;&nbsp;&nbsp; </span><input id=\"_upload_photo\" name=\"_upload_photo\" type=\"file\" accept=\"image/*\" value=\"1\"/>\r\n	</div>\r\n	<div class=\"photo_basic_layout_header\">\r\n		<span>\r\n			Preview The Picture\r\n		</span>	    		\r\n		<label>\r\n			<form id=\"crop_image_coordinate\">\r\n\r\n				<input type=\"hidden\" id=\"x\" name=\"x\" />\r\n				<input type=\"hidden\" id=\"y\" name=\"y\" />\r\n				<input type=\"hidden\" id=\"w\" name=\"w\" />\r\n				<input type=\"hidden\" id=\"h\" name=\"h\" />\r\n				<input type=\"hidden\" id=\"bigImage\" name=\"bigImage\" value=\"\"/>\r\n\r\n				<input type=\"button\" id=\"crop_image\" class=\"normal\" value=\"Upload\">\r\n\r\n			</form>\r\n		</label>\r\n	</div>\r\n	<div class=\"photo_basic_layout_context\" id=\"showDiv\">\r\n		<input type=\"hidden\" id=\"jcrop_has_img_flag\" value=\"0\"/>\r\n		<img src=\"img/upload_image_t.gif\" id=\"jcrop_target\">\r\n		\r\n	</div>\r\n</div>\r\n";
},"useData":true});
return templates;
});