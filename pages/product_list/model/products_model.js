"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt) {
      //产品模板关系数据模型
      var RelationModel = Backbone.Model.extend({
        url:page_config.RelationModel.url,
        idAttribute: "relation_id",
        defaults: {
          detail_lable_id: "",
          lable_template_type: "",
          lable_name: "",
          lable_content: "",
        }
      });
      
      //模板数据模型
      var ProductModel = Backbone.Model.extend({
        url: page_config.productsCollection.url,
        idAttribute: "pc_id",
        defaults: {
          pc_name: "",
          title: "",
          catalog_id: "",
          product_lable: [],
          contaioner_lable: [],
          title_list:""
        }
      });

      //模板数据集合
      var ProductsCollection = Backbone.Collection.extend({
        model: ProductModel,
        url:page_config.productsCollection.url,
        parse: function(response) {
          if(typeof(response.pageCtrl)!='undefined' && response.pageCtrl!='' && response.pageCtrl!=null){
            ProductsCollection.pageCtrl = response.pageCtrl;
          }  
          ProductsCollection.datas = response.datas;        
          return response.products;
        }
      }, {
        pageCtrl: {
          pageNo: 1,
          pageSize: 10
        }
      }
      ,{
        datas: {
          key: "",
          title:"",
          select:false,
          value:[],
          parentKey:""
        }
      }
      );

     return {
      RelationModel:RelationModel,
      ProductModel:ProductModel,
      ProductsCollection:ProductsCollection
    };

  }); //page_init
