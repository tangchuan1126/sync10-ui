"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt) {

      //模板内容
      var LableModel = Backbone.Model.extend({
          defaults: {
            logo: "VVME",
            pcname: "mmmm",
            pcid: "E2501556",
            origin: "made in chian",
            supplierid: "S954200",
            billno: "E8654512",
            barcode:"NA",
          }
        });
      
      //添加模板数据模型
      var BaseLableModel = Backbone.Model.extend({
        url: page_config.saveLableModel.url,
        idAttribute: "detail_lable_id",
        defaults: {
          pc_id:"",
          lable_name: "",
          lable_type: "",
          lable_height: "",
          lable_width: "",
          print_name: "",
          lable_content: "",
          type:"",
          lable_template_type:""
        }
      });

      // //基础模板数据模型
      // var BaseLableModel = Backbone.Model.extend({
      //   url: page_config.baseLableModel.url,
      //   idAttribute: "lable_id",
      //   defaults: {
      //     lable_name: "",
      //     lable_type: "",
      //     lable_height: "",
      //     lable_width: "",
      //     print_name: "",
      //     lable_content: "",
      //     type:"",
      //     lable_template_type:""
      //   }
      // });

      //基础模板数据集合
      var BaseLableCollection = Backbone.Collection.extend({
          model: BaseLableModel,
          url:page_config.baseLableModel.url,
          parse: function(response) {
            BaseLableCollection.pageCtrl = response.pageCtrl;
            BaseLableCollection.containerTypeKey =  response.containerTypeKey;
          return response.lables;
          },
          pageCtrl: {
            pageNo: 1,
            pageSize: 20
          },
           containerTypeKey: [{key: 1, value: "CLP"}]
        });

     return {
      LableModel:LableModel,
      BaseLableModel:BaseLableModel,
      BaseLableCollection:BaseLableCollection
    };

  }); //page_init
