"use strict";
define([
         "config", 
         "jquery",
         "backbone",
         "handlebars",
	 	 "handlebars_ext",
         "templates",
         "advancedSearch",
         "./model/products_model",
         "./view/products_view",
         "./model/base_lable_model",
         "./view/base_lable_view",
         "./view/delete_lable_view",
         "auto",
         "blockui",
            "oso.lib/art/plugins/newArtDialog",
         "jqueryui/tabs",
         "jqueryui/dialog",
         "domready"
    ], 
 function(page_config,$,Backbone,Handlebars,handlebars_ext,templates,AdvancedSearch,models,productView,lableM,lableV,deleteLableV,auto,blockui, artDialog){
	var advancedSearchParsm ="";
	var titleName ="";
	//遮罩
    $.blockUI.defaults = {
         css: { 
          padding:        '8px',
          margin:         0,
          width:          '170px', 
          top:            '45%', 
          left:           '40%', 
          textAlign:      'center', 
          color:          '#000', 
          border:         '3px solid #999999',
          backgroundColor:'#ffffff',
          '-webkit-border-radius': '10px',
          '-moz-border-radius':    '10px',
          '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
          '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
         },
         //设置遮罩层的样式
         overlayCSS:  { 
          backgroundColor:'#000', 
          opacity:        '0.6' 
         },
         
         baseZ: 99999, 
         centerX: true,
         centerY: true, 
         fadeOut:  1000,
         showOverlay: true,
    };

    $(function(){

    //index.html中的选项卡
    $("#tabs").tabs({
    	activate: function( event, ui ) {
    		if($("#tabs").tabs("option","active")==0){
            	$("#template_button").css("display","");
            }else{
            	$("#template_button").css("display","none");
            }
    	}
    });
   
    $("#base_lable_tabs").tabs();//add tabs of template
    //#tab1
    var a = new AdvancedSearch({
		border : false,
		searchBtn : false,
		conditionTitle : "Condition:",
		dialogWidth : 650, 
		dialogHeight :  150,
		condition : "top",
		container : '#tab1',
		url : "/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=yes",
//		url : "/Sync10/_my_tomcat/getTitle",
		search : function(data, event){
			
			var name = ",";//获取选中的title_name， gql 15/05/20
			var parsm = "?1=1";
			for(var j = 0;j < data.length; j++){
//				console.log(data[j]);
				parsm += "&"+data[j].key+"="+data[j].id;
				name += data[j].name;
			}
			titleName = name.substring(1);
//			console.info(titleName);
			advancedSearchParsm=parsm;
			productView.render(parsm);
		},
		clear : function(data, event){
			var name = ",";//获取选中的title_name， gql 15/05/20
			var parsm = "?1=1";
			for(var j = 0;j < data.length; j++){
				parsm += "&"+data[j].key+"="+data[j].id;
				name += data[j].name;
			}
			titleName = name.substring(1);
			advancedSearchParsm=parsm;
			productView.render(parsm);
		}
	});
    
    
    var pcTempView = lableV.pcTempView;//产品基础模板
    var containerTempView = lableV.containerTempView;//容器基础模板
    var labelDeleteTempView = deleteLableV.labelDeleteTempView;//删除基础模板    
     
    //索引根据名字查询
    var SeachNameView = Backbone.View.extend({
        el:"#tab2",
        template:templates.product_seach_name,
        render:function(){
            var v = this;
            v.$el.html(v.template());
            auto.addAutoComplete($("#seachInput"),
    			"../../../Sync10/action/administrator/product/getSearchProductsJSON.action",
    			"merge_info","p_name");
        },
        events:{
        	"keydown #seachInput":"seachName",
        	"click #eso_search":"seachProduct"
        	
        },
        seachName:function(event){
        	if(event.keyCode==13){
        		var pcName = this.$el.find("#seachInput").val();
        		if(pcName!=""){
        			var parsm = "?search=seachName&pc_name="+pcName;
        			productView.render(parsm);
        		}
	     	}
        },
        seachProduct:function(event){
        	var pcName = this.$el.find("#seachInput").val();
        	if(pcName!=""){
        		var parsm = "?search=seachName&pc_name="+pcName;
         		productView.render(parsm);
        	}
        }
    });
    var SeachNameView = new SeachNameView();
	SeachNameView.render();
	
	//批量添加、删除模板
    var TemplateButtonView = Backbone.View.extend({
        el:"#template_button",
        template:templates.template_button,
        render:function(){
            var v = this;
            v.$el.append(v.template());
        },
        events:{
        	"click #add_button":"addBatchTemp",
        	"click #delete_button":"deleteBatchTemp"
        },
        addBatchTemp:function(event){
			if(advancedSearchParsm.indexOf("title") > 0){
				productView.pc_id = "";
				pcTempView.productView=productView;
				containerTempView.productView=productView;
				pcTempView.render(advancedSearchParsm,"1");
				containerTempView.render(advancedSearchParsm,"2","","1");//容器只添加clp的标签，type：1
			   // $("#base_lable_tabs").dialog({
		    //       title: "add base lable template",
		    //       draggable: true,
		    //       width: 900,
		    //       height: 600,
		    //       modal: true,
		    //     });
               //modify by huhy 
               var d = new artDialog({
                  lock: true,
                  title: "Add base label template",
                  draggable: true,
                  width: 900,
                  height: 600,
                  opacity:0.3,
                  content: $("#base_lable_tabs").get(0),
                  close: function(){
                    d.DOM.content.css({'height': 'auto', 'width': 'auto', 'margin-top': '0px'});
                  }
              });
               d.DOM.content.css({'height': '100%', 'width': '100%', 'margin-top': '-35px'});
		    }else{
	  		  // alert("Please select a title.");
                new artDialog({
                    title:'Warning',
                    lock: true,
                    icon: 'warning',
                    path: '/Sync10-ui/lib/art/plugins',
                    draggable: true,
                    width: 250,
                    height: 100,
                    opacity:0.3,
                    content: '<p>Please select a title.</p>'
                });
	  	  	}
        },
        deleteBatchTemp:function(event){
        	if(advancedSearchParsm.indexOf("title") > 0){
        		labelDeleteTempView.productView=productView;
        		labelDeleteTempView.render(advancedSearchParsm,titleName,null);//获取选中的title_name， gql 15/05/20
        	}else{
        		new artDialog({
                    title:'Warning',
                    lock: true,
                    icon: 'warning',
                    path: '/Sync10-ui/lib/art/plugins',
                    draggable: true,
                    width: 250,
                    height: 100,
                    opacity:0.3,
                    content: '<p>Please select a title.</p>'
                });
        	}
        	
        	
        }
    });
    var templateButtonView = new TemplateButtonView();
    templateButtonView.render();
  });
});
