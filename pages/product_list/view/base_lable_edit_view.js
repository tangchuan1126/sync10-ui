"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/base_lable_model",
  "showMessage"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, LabelModel) {
  // CreateLableView 拖动元素时所需要的变量
    var inteval = null;
    var element = null;
    var height = 243;
    var width = 115;
    var posX, posY;
    //标签类型
    var paperType =[{type:"60mmX30mm",width:"230",height:"105"},
                    {type:"80mmX40mm",width:"305",height:"145"},
                    {type:"100mmX50mm",width:"380",height:"185"},
                    {type:"102mmX152mm",width:"385",height:"555"}]
    
	//编辑基础模板视图
    var BaseLableEditView = Backbone.View.extend({
      template: templates.base_lable_edit,
      el: "#base_lable_edit",
      model:new LabelModel.BaseLableModel(),
      parsm:"",
      pcId:"",
      pcCollection:null,
      productView:null,
      initialize: function () {
        document.onkeydown = this.keyMove;
      },
      render: function(model,parsm,pcId) {
        var v = this;
        v.parsm = parsm;
        v.pcId=pcId;
        
        //获取该商品的所有模板
        var pc_id = v.productView.pc_id;//修改
        if(pcId){
        	pc_id = pcId;//add single
        }else if(!pc_id){
        	pc_id = "";//add batch
        }
        var pcModel = v.productView.procollection.findWhere({'pc_id':pc_id});
        v.pcCollection = new LabelModel.BaseLableCollection(pcModel?pcModel.get("product_lable"):null);
        console.log("pcId="+pcId+"    pc_id="+pc_id);
        
        this.model = model;
        this.$el.html(this.template({
          model: model.toJSON(),
          paperType:paperType
        }));
        height = parseInt(this.$el.find("#edit").css("height"));
        width = parseInt(this.$el.find("#edit").css("width"));
      },
      events: {
        "click #addLine": "addLine",
        "click #addVertical": "addVertical",
        "click .common-element-button": "addCommonElement",
//        "click #addElName": "addElName",
//        "click #addElText": "addElText",

        "mousedown #line": "move",
        "mousedown #verline": "move",
        "mousedown .elementName": "move",

        "mouseup #edit": "onmouseup",
        "mouseup #lableContent": "onmouseup",

        "mousedown #turnLeft": "turnLeft",
        "mouseup #turnLeft": "turnMouseUp",
        "mouseout #turnLeft": "turnMouseUp",

        "mousedown #turnRight": "turnRight",
        "mouseup #turnRight": "turnMouseUp",
        "mouseout #turnRight": "turnMouseUp",

        "mousedown #turnTop": "turnTop",
        "mouseup #turnTop": "turnMouseUp",
        "mouseout #turnTop": "turnMouseUp",

        "mousedown #turnBottom": "turnBottom",
        "mouseup #turnBottom": "turnMouseUp",
        "mouseout #turnBottom": "turnMouseUp",

        "click #turn": "turn",
        "click #remove": "removeElement",
        
        "click #addCommon": "addCommon",
        "click #addBarCode": "addBarCode",
        "click #goBack": "goBack",
//        "click #addImage": "addImage",
        
        "change #lableType": "setLableType",

        "change #lableWidth": "changeWidth",
        "change #lableHeight": "changeHeight",
        "change #elWidth": "changeElWidth",
        "change #elHeight": "changeElHeight",
        "change #elFont": "changeElFont",
        "change #elAlign": "changeElAlign",
        "click #preview": "previewPrinter",
        "click #create": "saveLabel",
        "change #lableName": "changeName"
      },
      verifyLableName:function(lableName){
    	  var v = this;
    	  var flag = true;
    	  var lableId = v.model.get("lable_id");
    	  var lablemodel = v.pcCollection.findWhere({'lable_name':lableName});
    	  if(lablemodel){
    		  showMessage("Lable Name【"+lableName+"】 already exists","alert");
    		  v.$el.find("#lableName").focus();
    		  flag = false;//若lable name已存在，不保存
    	  }
    	  return flag;
      },
      changeName:function(evt){
    	  var v = this;
    	  var flag = true;
    	  var lableId = v.model.get("lable_id");
    	  var lableName = v.$el.find("#lableName").val();
    	  var prevLableName = v.$el.find("#prevLableName").val();
          if(lableName!=prevLableName){
        	  flag = v.verifyLableName(lableName);//name 唯一验证
          }else if(!lableId){
        	  flag = v.verifyLableName(lableName);//name 唯一验证
          }
          return flag;
      },
      setLableType:function(evt){
      	var width = this.$el.find("#lableType").find("option:selected").data("width");
      	var height = this.$el.find("#lableType").find("option:selected").data("height");
      	this.$el.find("#lableWidth").val(width);
	    this.$el.find("#lableHeight").val(height);
	    this.changeWidth();
	    this.changeHeight();
	  },
      saveLabel: function(){
        $(element).css({"border-color": "#000","color": "#000","border": "0px",});//将选中元素颜色设置为黑色
        var v = this;
        
        var flag = v.verifyLableName(lableName);//name 唯一验证
        if(flag){
        	$.blockUI({message: '<img src="img/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开 
      	//保存数据
          var detailLableId = this.$el.find("#detailLableId").val();
          var lableName = this.$el.find("#lableName").val();
          var lableType = this.$el.find("#lableType").val();
          var lableWidth = this.$el.find("#lableWidth").val();
          var lableHeight = this.$el.find("#lableHeight").val();
          var printName = this.$el.find("#printName").val();
          var lableContent = this.$el.find("#lableContent").html();
          var type = this.$el.find("#type").val();
          var lableTemplateType = this.$el.find("#lableTemplateType").val();
         
          if(v.pcId&&v.pcId!=""){
            this.model.url=page_config.saveLableModel.url+v.parsm+"&add=single";
//      	  	console.log("add parsm="+this.model.url);
          }else{
            this.model.url=page_config.saveLableModel.url+v.parsm;
          }
           this.model.save({
                lable_name: lableName,
                lable_type: lableType,
                lable_height:lableHeight,
                lable_width: lableWidth,
                print_name: printName,
                lable_content: lableContent,
                type:type,
                lable_template_type:lableTemplateType,
                pc_id:v.pcId
              },{
                success: function(m) {
                $.unblockUI();       //遮罩关闭
//                  alert("save success");
                  showMessage("Success","succeed");
                  setTimeout(function(){
                	  v.productView.render(v.parsm);
                  },1500);

                  var d1 = $("#base_lable_edit").data('dialog');
                  var d2 = $("#base_lable_tabs").data("dialog");
                  if(d1){
                    d1.close();
                  }
                  if(d2){
                    d2.close();
                  }
                }
            });
        }
      },
    //上传图片
      addImage:function(){
    	  uploadPhotoView.render();
      	  $("#upload_photo").dialog({
  	        title: "upload photo ",
  	        width: '600',
  	        height: '580',
  	        draggable: true,
  	        modal: true
  	      });
      },
      addBarCode:function(evt){
          var btn = $(evt.target);
          var commonType= btn.data('val');
          $(this.el).find("#commonType").val(commonType);
          $(this.el).find(".tag_font").text("BarCode Tag");
          $(this.el).find("#common").css('display','');
          $(this.el).find("#custom").css('display','none');
          $(this.el).find("#addBarCode").css('display','none');//按钮显示
          $(this.el).find("#addCommon").css('display','');
          $(this.el).find("#goBack").css('display','');
       },
       addCommon:function(evt){
          var btn = $(evt.target);
          var commonType= btn.data('val');
          $(this.el).find("#commonType").val(commonType);
          $(this.el).find(".tag_font").text("Common Tag");
          $(this.el).find("#common").css('display','');//内容显示
          $(this.el).find("#custom").css('display','none');
          $(this.el).find("#addCommon").css('display','none');//按钮显示
          $(this.el).find("#addBarCode").css('display','');
          $(this.el).find("#goBack").css('display','');
       },
       goBack:function(){
       $(this.el).find("#commonType").val("");
          $(this.el).find("#common").css('display','none');
          $(this.el).find("#custom").css('display','');
          $(this.el).find("#goBack").css('display','none');//按钮显示
          $(this.el).find("#addCommon").css('display','');
          $(this.el).find("#addBarCode").css('display','');
       },
      /*********************** add elements ***********************/
       addLine: function(evt) {
           $(this.el).find("#edit").append(" <hr id='line' size='2px'style='background:#000000; paddind:0px;margin-left:1px;margin-top:5px;position:absolute;height:2px;width:" + (width - 2) + "px'/>");
       },
       addVertical: function(evt) {
           $(this.el).find("#edit").append(" <hr id='verline' size='2px'style='background:#000000;paddind:0px;margin-left:5px;margin-top:1px;position:absolute;width:2px;height:" + (height - 2) + "px'/>");
      },
      addElName: function(evt) {
        var elementName = $(this.el).find("#elementName").val().toUpperCase();;
        if(elementName!=""){
          $(this.el).find("#edit").append("<div  class='elementName'  style='font-family:Arial;position:absolute;paddind:0px;margin:0px;top:10px;left:10px;width:90px;font-size:12px;text-align:left;font-family: Arial;'>{"+elementName+"}</div>");
          $(this.el).find("#elementName").val('');
        }else{
//          alert("Please enter the name, and then pressed the ADD button.");
          showMessage("Please enter the name, and then pressed the ADD button.","alert");
          $(this.el).find("#elementName").focus();
        }
      },
      addElText: function(evt) {
          var elementText = $(this.el).find("#elementText").val();
          if(elementText!=""){//word-wrap: break-word; word-break: normal;
            $(this.el).find("#edit").append("<div class='elementName' style='font-family:Arial;position:absolute;top:10px;left:10px;width:120px;font-size:12px;text-align:left;'>"+elementText+"</div>");
            $(this.el).find("#elementText").val('');
          }else{
//            alert("Please enter the [Element Text], and then pressed the ADD button.");
            showMessage("Please enter the [Element Text], and then pressed the ADD button.","alert");
            $(this.el).find("#elementText").focus();
          }
      },
      addCommonElement: function(evt) {
          var btn = $(evt.target);
          var commonElement = btn.data("val");
          var commonType = $(this.el).find("#commonType").val();
          if(commonType=="1"){
            $(this.el).find("#edit").append("<div class='elementName' style='font-family:Arial;position:absolute;margin-top:10px;margin-left:10px;width:120px;font-size:12px;text-align:left;'>{"+commonElement+"}</div>");
          }else if(commonType=="2"){
            $(this.el).find("#edit").append("<img class='elementName' draggable='false' style='position:absolute;paddind:0px;margin-top:30px;margin-left:20px;display:block;' src='/barbecue/barcode?data=$"+commonElement+"$&width=1&type=code39&height=35'/>");
          }
        },
      //add elements END
  	  /*********************** mouse operation ***********************/
      changeMouse: function(evt) {
            //点击是添加样式和值
    	        $(element).css({
    	          "border-color": "#000",
    	          "border": "0px",
    	          "color": "#000"
    	        });
    	        
    	        $(evt.target).css({
    	          "border-color": "#3ccccc",
    	          "border": "1px solid",
    	          "color": "#3ccccc"
    	        });
        },
        mouseoutColor:function(evt){
      	  $(element).css({
    	          "border-color": "#000",
    	          "border": "0px",
    	          "color": "#000"
    	        });
      	  $(evt.target).css({
      		  "border-color": "#000",
    	          "border": "0px",
    	          "color": "#000"
    	        });
        },
        move: function(evt) {
          //点击是添加样式和值
      	evt.target.style.cursor = 'grabbing';
      	$('#edit').css('cursor', 'grabbing');
      	
          $(element).css({
            "border-color": "#000",
            "border": "0px",
            "color": "#000"
          });
          
          $(evt.target).css({
            "border-color": "#3ccccc",
            "border": "1px solid",
            "color": "#3ccccc"
          });
          
          element = evt.target;
          
          //barCoed不能修改元素宽度
          var elTagName = $(element).get(0).tagName;
          if(elTagName=='IMG'){
          	$("#elWidth").attr("disabled",true);
          }else{
          	$("#elWidth").attr("disabled",false);
          }
          
          var presentWidth = parseInt($(evt.target).css("width"));
          var presentHeight = parseInt($(evt.target).css("height"));
          var presentFont = parseInt($(evt.target).css("font-size"));
          var presentAlign = $(evt.target).css("text-align").trim();
          
          $(this.el).find("#elWidth").val(presentWidth);
          $(this.el).find("#elHeight").val(presentHeight);
          $(this.el).find("#elFont").val(presentFont);
          $(this.el).find("#elAlign").val(presentAlign);

          //mousemove start
          posX = evt.pageX - parseInt($(evt.target).css("margin-left"));
          posY = evt.pageY - parseInt($(evt.target).css("margin-top"));
          this.$el.find("#edit").bind("mousemove", this.mousemove);
          //mousemove  end
          
          evt.preventDefault();
          return false;
        },
        onmouseup: function(evt) {
          evt.target.style.cursor = 'grab';
  	    $('#edit').css('cursor', 'grab');
          $(this.el).find("#edit").unbind("mousemove");
        },
        mousemove: function(evt) {
      	evt.target.style.cursor = 'grabbing';
        	$('#edit').css('cursor', 'grabbing');
          var moveX = evt.pageX - posX;

          var _width = parseInt($(element).css("width"));
          var _height = parseInt($(element).css("height"));
          
          if (moveX >= 0 & moveX <= (width - _width)) {
            $(element).css("margin-left", moveX + "px");
          }
          var moveY = evt.pageY - posY;
          
          if (moveY >= 0 & moveY <= (height - _height)) {
            $(element).css("margin-top", moveY + "px");
          }
        },
        turnMouseUp: function() {
          clearInterval(inteval);
        },
        turn: function(evt) {
          var el = evt.target;
          $(el);
        },
        //移除元素
        removeElement: function() {
  	     if (element != null) {
  	        $(this.el).find("#elWidth").val("");
  	        $(this.el).find("#elHeight").val("");
  	        $(this.el).find("#elFont").val("");
  	        $(element).remove();
  	        element = null;
  	     }
  	  },
  	  /************** According to the arrow button to move **************/
  	 turnLeft: function(evt) {
         var sped = $("#sped").val() * 1;
         if (evt.button == 0) {
           inteval = setInterval(function() {
             if (element != null) {
               var left = parseInt($(element).css("margin-left"));
               left -= sped;
               if (left > 0) {
                 $(element).css("margin-left", left + "px");
               }
             }
           }, 40);
         }
       },
       turnRight: function(evt) {
         var sped = $("#sped").val() * 1;
         if (evt.button == 0) {
           inteval = setInterval(function() {
             if (element != null) {
               var left = parseInt($(element).css("margin-left"));
               left += sped;
               var _width = parseInt($(element).css("width"));
               if (left < width - _width) {
                 $(element).css("margin-left", left + "px");
               }
             }
           }, 40);
         }
       },
       turnTop: function(evt) {
         var sped = $("#sped").val() * 1;
         if (evt.button == 0) {
           inteval = setInterval(function() {
             if (element != null) {
               var top = parseInt($(element).css("margin-top"));
               top -= sped;
               if (top > 0) {
                 $(element).css("margin-top", top + "px");
               }
             }
           }, 40);
         }
       },
       turnBottom: function(evt) {
         var sped = $("#sped").val() * 1;
         if (evt.button == 0) {
           inteval = setInterval(function() {
             if (element != null) {
               var top = parseInt($(element).css("margin-top"));
               top += sped;
               var _height = parseInt($(element).css("height"));
               if (top < height - _height) {
                 $(element).css("margin-top", top + "px");
               }
             }
           }, 40);
         }
       },
       //end the arrow button to move
       /******** change attribute for the selected element **********/
       changeWidth: function(evt) {
         var lableWidth = this.$el.find("#lableWidth").val();
         if (lableWidth == "") {
//         	alert("Please enter the Lable Width");
         	showMessage("Enter the Lable Width","alert");
             this.$el.find("#lableWidth").focus();
         } else {
           this.$el.find("#edit").css("width", lableWidth);
           width = lableWidth;
         }
       },
       changeHeight: function(evt) {
         var lableHeight = this.$el.find("#lableHeight").val();
         if (lableHeight == "") {
//         	 alert("Please enter the Lable Height");
         	showMessage("Enter the Lable Height","alert");
              this.$el.find("#lableHeight").focus();
         } else {
           this.$el.find("#edit").css("height", lableHeight);
           height = lableHeight;

         }
       },
       changeElWidth: function(evt) {
         var elWidth = this.$el.find("#elWidth").val();
         if (elWidth == "") {
//         	alert("Please enter the width");
         	showMessage("Enter the width","alert");
             this.$el.find("#elWidth").focus();
         }else {
         //   if ($(element).attr("id") == "verline") {
         //     $(element).attr("size", elWidth);
         //   } else {

             $(element).css("width", elWidth + "px");
           // }
         }

       },
       changeElHeight: function(evt) {
     	var elTagName = $(element).get(0).tagName;
     	  
         var elHeight = this.$el.find("#elHeight").val();
         if (elHeight == "") {
//         	alert("Please enter the height");
         	showMessage("Enter the height","alert");
             this.$el.find("#elHeight").focus();
         }else {
         //   if ($(element).attr("id") == "line") {
         //     $(element).attr("size", elHeight + "px");
         //   } else {

//             $(element).css("height", elHeight + "px");
           // }
             
             if(elTagName=='IMG'){
             	var imgSrc = $(element).attr("src");
             	var indexOfHeight = imgSrc.indexOf("height");
             	var newSrc=imgSrc.substring(0,indexOfHeight)+"height="+elHeight;
//             	console.log(imgSrc+"\n"+newSrc);
             	$(element).attr("src",newSrc)
             }else{
             	$(element).css("height", elHeight + "px");
             }
         }
       },
       changeElFont: function(evt) {
         var elFont = this.$el.find("#elFont").val();
         if (elFont == "") {
//         	alert("Please enter the font-size");
         	showMessage("Enter the font-size","alert");
             this.$el.find("#elFont").focus();
         } else {
           $(element).css("font-size", elFont + "px");
         }
       },
       changeElAlign:function(evt){
         var elAlign = this.$el.find("#elAlign").val();
         $(element).css("text-align", elAlign);
       },
       // end
       //the keyboard arrows to move
       keyMove:function(event){
         var sped = $("#sped").val() * 1;
         if (sped != 0 && element != null) {   
           switch ((event || window.event).keyCode) 
           { 
             case 37:
                 var left = parseInt($(element).css("margin-left"));
                 left -= sped;
                 if (left > 0) {
                   $(element).css("margin-left", left + "px");
                 }
             break; 
             case 38:
                 var top = parseInt($(element).css("margin-top"));
                 top -= sped;
                 if (top > 0) {
                   $(element).css("margin-top", top + "px");
                 }
             break; 
             case 39: 
               var left = parseInt($(element).css("margin-left"));
                 left += sped;
                 var _width = parseInt($(element).css("width"));
                 if (left < width - _width) {
                   $(element).css("margin-left", left + "px");
                 } 
             break; 
             case 40: 
                 var top = parseInt($(element).css("margin-top"));
                 top += sped;
                 var _height = parseInt($(element).css("height"));
                 if (top < height - _height) {
                   $(element).css("margin-top", top + "px");
                 }
             break; 
           }
         }
       },
       previewPrinter: function(){
     	 $(element).css({"border-color": "#000","color": "#000","border": "0px",});//将选中元素颜色设置为黑色
         //获取打印机名字列表
         var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
          //判断是否有该名字的打印机
         var printer = "LabelPrinter";
         var printerExist = "false";
         var containPrinter = printer;
         var lableType = this.$el.find("#lableType").val().split("X");
         var size=[];
         if(lableType&&lableType!=''){
             size=this.$el.find("#lableType").val().split("X");
         }
//         var lableWidth = size[0]+"mm";
//         var lableHeight = size[1]+"mm";
         var lableWidth = size[0];
         var lableHeight = size[1];
//          alert("lableWidth="+lableWidth+"  lableHeight="+lableHeight);
         for(var i = 0;i<printer_count;i++){
           if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
             printerExist = "true";
             containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
             break;
           }
         }
         visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
         visionariPrinter.SET_PRINT_PAGESIZE(1,lableWidth,lableHeight,lableType);
         visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#lableContent').html());
         visionariPrinter.SET_PRINT_COPIES(1);
         visionariPrinter.PREVIEW();
       }
    });

     return new BaseLableEditView();

});