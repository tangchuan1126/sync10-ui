"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/base_lable_model",
  "./upload_photo_view",
  "showMessage"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, LabelModel, uploadPhotoView) {   
    var height = 426;
    var width = 390;
    var animateHeight = 0;
    var initialContainerModel = new LabelModel.BaseLableModel({
      lable_height: "426", 
      lable_width: "390", 
      lable_template_type: "2", 
      print_name: "LabelPrinter", 
      lable_type: "102mmX152mm"
    });

    function insertCursor(val) {
      var textObj = document.getElementById('containerText');
      var maxLength = $('#containerText').val().length;
      if (textObj.setSelectionRange) {
          var rangeStart = textObj.selectionStart;
          var rangeEnd = textObj.selectionEnd;
          if(rangeStart >= maxLength || rangeStart == 0){
//            alert('Please stay cursor in text area!');
            showMessage("Please stay cursor in text area!","alert");
            return;
          } 
          var tempStr1 = textObj.value.substring(0, rangeStart);
          var tempStr2 = textObj.value.substring(rangeEnd);
          textObj.value = tempStr1 + val + tempStr2;
          textObj.selectionStart = rangeStart + val.length;
          console.log(textObj.selectionStart);
          textObj.setSelectionRange(textObj.selectionStart, textObj.selectionStart);
      } else {
//        alert("This version of Mozilla based browser does not support setSelectionRange");
        showMessage("This version of Mozilla based browser does not support setSelectionRange","alert");
      }
    } 
  
    var CreateContainerView = Backbone.View.extend({
      template: templates.base_container_edit,
      el: "#base_container_edit",
      model: initialContainerModel,
      parsm:"",
      pcId:"",
      productView:null,
      pcCollection:null,
      containerTypeKey: LabelModel.BaseLableCollection.containerTypeKey,
      render: function(model, parsm, pcId, isUpload) { 
//        debugger;
        
        var v = this;
        v.parsm = parsm;
        v.pcId=pcId;
       
        //获取该商品的所有模板
        var pc_id = v.productView.pc_id;//修改
        if(pcId){
        	pc_id = pcId;//add single
        }else if(!pc_id){
        	pc_id = "";//add batch
        }
        var pcModel = v.productView.procollection.findWhere({'pc_id':pc_id});
        v.pcCollection = new LabelModel.BaseLableCollection(pcModel?pcModel.get("contaioner_lable"):null);
        console.log("pcId="+pcId+"    pc_id="+pc_id);
        
        this.model = model;
        this.$el.html(this.template({
          isUpload: isUpload,
          model: this.model.toJSON(),
          containerTypeKey: new LabelModel.BaseLableCollection().containerTypeKey
        }));                  
      },
      events: {
        "click #create": "saveContainer",
        "click #generate": "generateContainer", 
        "click #preview": "previewPrinter",
        "click input[name='msgInsert']": "insertMsg",
        "click input[name='barcodeInsert']": "insertBarcode",
        "click #downArrow": "animateDown",
        "click #upArrow": "animateUp",
        "click #uploadButton": "uploadDialog",
        "change #type": "changeType",
        "change #lableName": "changeName"
      },
      //上传图片
      uploadDialog: function(evt){
        //uploadView.uploadDialog();     
        uploadPhotoView.render();
        $("#upload_photo").dialog({
          title: "upload photo ",
          width: '600',
          height: '580',
          draggable: true,
          modal: true
        });      
      },
      insertMsg: function(evt){
        var value = $(evt.target).data("value");
        insertCursor('{' + value + '}');
      },
      insertBarcode: function(evt){
        var value = $(evt.target).data("value");
        insertCursor('<img src="/barbecue/barcode?data=$' + value + '$&width=1&height=50&type=code39"/>');
      },
      animateUp: function(evt){
        animateHeight += 339;
        if(animateHeight > 0){
          animateHeight = -339;
          if($(evt.target).data('value')){
            animateHeight -= 339;
          }
        }
        if(!$(".inner").is(":animated")){          
          $(".inner").css("position","relative").animate({'top': animateHeight+'px'});
          $('#upArrow').attr("disabled", true);
          $('#downArrow').attr("disabled", false);
        } 
      },
      animateDown: function(evt){
        animateHeight -= 339;
        var maxHeight = -339;
        if($(evt.target).data('value')){
            maxHeight -= 339;
          }
        if(animateHeight < maxHeight){
          animateHeight = 0;
        }
//        debugger;
        if(!$(".inner").is(":animated")){         
          $(".inner").css("position","relative").animate({'top': animateHeight+'px'});
          $('#upArrow').attr("disabled", false);
          $('#downArrow').attr("disabled", true);
        }   
      },
      changeType: function(evt){        
  		  var typeValue = "C";        
  	      if($(evt.target).val() == 1){
  	        typeValue = "C";
  	      }else if($(evt.target).val() == 3){
  	        typeValue = "T";
  	      }
  	      
          var content = $("#containerText").val(); //获取textarea内容  
          //正则获取container type        
          var reBed = /<center><b>\s*[CT]\s*<\/b><\/center>/;
          var target = content.match(reBed);     
          content = content.replace(target, "<center><b>"+typeValue+"</b></center>");
          
          var brBed = /<center>\s*[CT]LP\{\w*\}\s*<\/center>/;
          target = content.match(brBed);
          content = content.replace(target, "<center>"+typeValue+"LP{CONID}</center>");
          $("#containerText").val(content);
          
        //获取预览内容,存在预览的内容，修改相应类型
          var containerPreview= this.$el.find("#containerPreview").html().trim();
          if(containerPreview&&containerPreview!=""){
          	this.$el.find("#containerPreview").html(content);
          }
      },
      previewPrinter: function(){
        //获取打印机名字列表
        var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
         //判断是否有该名字的打印机
        var printer = "LabelPrinter";
        var printerExist = "false";
        var containPrinter = printer;
        for(var i = 0;i<printer_count;i++){
          if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
            printerExist = "true";
            containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
            break;
          }
        }
        visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
        visionariPrinter.SET_PRINT_PAGESIZE(1,"10.2cm","15.2cm","102X152");
        visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#containerPreview').html());
        visionariPrinter.SET_PRINT_COPIES(1);
        visionariPrinter.PREVIEW();
      },
      verifyLableName:function(lableName){
    	  var v = this;
    	  var flag = true;
    	  var lablemodel = v.pcCollection.findWhere({'lable_name':lableName});
    	  if(lablemodel){
    		  showMessage("Lable Name【"+lableName+"】 already exists","alert");
    		  v.$el.find("#lableName").focus();
    		  flag = false;//若lable name已存在，不保存
    	  }
    	  return flag;
      },
      changeName:function(evt){
    	  var v = this;
    	  var flag = true;
    	  var lableId = v.model.get("lable_id");
    	  var lableName = v.$el.find("#lableName").val();
    	  var prevLableName = v.$el.find("#prevLableName").val();
          if(lableName!=prevLableName){
        	  flag = v.verifyLableName(lableName);//name 唯一验证
          }else if(!lableId){
        	  flag = v.verifyLableName(lableName);//name 唯一验证
          }
          return flag;
      },
      saveContainer: function(){
    	  var v = this;
    	  var lableName = this.$el.find("#lableName").val();
    	  var flag = v.verifyLableName(lableName);//name 唯一验证
          if(flag){
	          $.blockUI({message: '<img src="/Sync10/administrator/js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开 
	          var lableType = this.$el.find("#lableType").val();
	          var lableWidth = this.$el.find("#lableWidth").val();
	          var lableHeight = this.$el.find("#lableHeight").val();
	          var printName = this.$el.find("#printName").val();
	          var lableContent = this.$el.find("#containerText").val();
	          var type = this.$el.find("#type").val(); 
	          
	          if(v.pcId&&v.pcId!=""){
	              this.model.url=page_config.saveLableModel.url+v.parsm+"&add=single";
	//        	  	console.log("add parsm="+this.model.url);
	            }else{
	              this.model.url=page_config.saveLableModel.url+v.parsm;
	            }
	          this.model.save({
	            lable_name: lableName,
	            lable_type: lableType,
	            lable_height: lableHeight,
	            lable_width: lableWidth,
	            print_name: printName,
	            lable_content: lableContent,
	            type:type,
	            lable_template_type:2,
	            pc_id:v.pcId
	          }, {
	            success: function(m) {
	              $.unblockUI();       //遮罩关闭
	//              alert("save success");
	              showMessage("Success","succeed");
	              v.productView.render(v.parsm);
	
	              var d1 = $("#base_container_edit").data('dialog');
	              var d2 = $("#base_lable_tabs").data("dialog");
	              if(d1){
	                d1.close();
	              }
	              if(d2){
	                d2.close();
	              }
	            }
	          });
	          initialContainerModel = new LabelModel.BaseLableModel({
	            lable_height: "426", 
	            lable_width: "390", 
	            lable_template_type: "2", 
	            print_name: "LabelPrinter", 
	            lable_type: "102X152"
	          });
          }
      },
      generateContainer: function(){
        var content = $("#containerText").val();        
        this.$el.find("#containerPreview").html(content);
      }
    });
    return new CreateContainerView();

}); //page_init

