"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates"
], function(page_config, $, Backbone, Handlebars, templates) {
    
    //预览视图,render时传入模板html片段
    return Backbone.View.extend({
      el: "#show_view",
      template: templates.show_view,
      render: function(lableContent, width, height) {
        this.$el.html(this.template({
          lableContent: lableContent,
          width: width+5,
          height: height+5
        }));
      },
    });

}); 