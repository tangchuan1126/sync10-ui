"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/products_model",
  "./show_view",
  "./base_lable_edit_view",
  "./base_container_edit_view",
  "oso.lib/art/plugins/newArtDialog",
  "showMessage"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, models, ShowView, editLabelView, editContainerView, artDialog) {
    var showView = new ShowView();

    function buildDialog(parsm, productView, title){
      // $("#labelDeleteTemp").dialog({
      //   title: "delete base label template",
      //   width: '900',
      //   height: '400',
      //   draggable: true,
      //   modal: true,
      //   close:function(){          
      //     productView.render(parsm);
      //   }
      // });
      var d = new artDialog({
        lock: true,
        title: "Batch delete label » title 【"+title+"】",
        draggable: true,
        width: 900,
        height: 400,
        opacity:0.3,
        content: $("#labelDeleteTemp").get(0),
        close:function(){          
          productView.render(parsm);
          d.DOM.content.css({'height': 'auto', 'width': 'auto', 'margin-top': '0px'});
        }
      });
      d.DOM.content.css({'height': '100%', 'width': '100%', 'margin-top': '-35px'});
      
      return d;
    }  

     //baseLableView
    var BaseLableView = Backbone.View.extend({
      template: templates.delete_lable,
      collection: new models.ProductsCollection(),
      parsm:"",
      productView:null,
      initialize:function(options){
          this.setElement(options.el);
      },
      render: function(parsm,title,labelname) {
        var v = this;
        v.parsm = parsm;
        v.title = title;
        if(labelname){          
          this.aquireLabels(labelname);
        }else{
          v.$el.html(v.template({}));
        }        
        onLoadInitZebraTable();
        v.dialog = buildDialog(parsm, v.productView, title);
      },
      events: {
        "click #search": "searchLabel",  
        "keypress #input_lableName": "enterSearch",            
        "click input[name='showTemp']": "showLabel",
        "click input[name='del']": "deleteLabel",

      },
      enterSearch: function(e){
        if (e.keyCode == 13) this.searchLabel();
      },
      deleteLabel: function(evt){
        var btn = $(evt.target);
        var relationId = btn.data("relationid") * 1;            
        var templateId = btn.data("templateid");
        var titleId = btn.data("title") * 1; 
        var labelName = btn.data("lablename");
        var searchName = $('#input_lableName').val(); 
        var labelModel = new models.ProductModel();
        labelModel.set({'pc_id':templateId});
        var v = this;
        var parsm = v.parsm;
        var title = v.title;

        //modify by huhy 
        var dialog = new artDialog({
          icon: 'question',
          title:'Delete['+labelName+']',
          path: '/Sync10-ui/lib/art/plugins',
          lock: true,
          width: 250,
          height: 100,
          opacity:0.3,
          content: '<p>Delete 【'+labelName+'】 label?</p>',
          ok: function () {
//            labelModel.url = labelModel.url + parsm +"&del=batch&lable_template_detail_id="+templateId;
            labelModel.url = labelModel.url + parsm +"&del=batch&title_id="+titleId+"&lable_name="+labelName;
          labelModel.destroy({
            success: function(relationModel, response) {
            	showMessage("Success","succeed");
                setTimeout(function(){
                	dialog.close();
                	v.dialog.close();
                	v.render(parsm, title, searchName);   
                },1500);
            },
            error: function() {
            	showMessage("Error","error");
            }
          });
          },
          okVal:'Sure',
          cancel: function () {
            dialog.close();
          },
          cancelVal:'Cancel'
        });
      },
      searchLabel: function(){
        var v = this;  
        var name = $('#input_lableName').val(); 
        v.aquireLabels(name);         
      },      
      showLabel: function(evt) {
        var evt = evt.target;
        var lableContent = $(evt).attr("lableContent");
        var lableName = $(evt).attr("lablename");
        var evt_width = $(evt).data("width")+100;
        var evt_height = $(evt).data("height")+150;
        evt_height = (evt_height>585)?585:evt_height;
        showView.render(lableContent, $(evt).data("width"), $(evt).data("height"));
        $("#show_view").dialog({
          title: "show Lable " + lableName,
          draggable: true,
          width: evt_width,
          height: evt_height,
          modal: true,
        });
      },
      aquireLabels: function(name){        
        if(name == "" || name == null) return;
        var v = this;
        v.collection.url = page_config.productsCollection.url+v.parsm;//查询条件的参数也传递过去
        this.collection.fetch({
          data: {
            name: name,
            search: "lableName"
          },
          success: function(collection) {     
            v.$el.html(v.template({
              input_lableName: name,
              detailLabels:  collection.toJSON()
            }));                 
            onLoadInitZebraTable();
          }
        });
      }
    });

    var labelDeleteTempView = new BaseLableView({el:"#labelDeleteTemp"});//É¾³ý»ù´¡Ä£°å    
    return {
      labelDeleteTempView:labelDeleteTempView      
    };
});