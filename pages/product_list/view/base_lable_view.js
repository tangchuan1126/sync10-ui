"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/base_lable_model",
  "./show_view",
  "./base_lable_edit_view",
  "./base_container_edit_view",
  "oso.lib/art/plugins/newArtDialog"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, models, ShowView, editLabelView, editContainerView, artDialog) {
    var showView = new ShowView();

    function buildDialog(targetDive, lableName, width){
      // $("#" + targetDive).dialog({
      //   title: "Edit "+lableName,
      //   width:  width,
      //   height: '585',
      //   draggable: true,
      //   modal: true
      // });
      var d = new artDialog({
        title:"Edit ["+lableName + "]",
        lock: true,
        draggable: true,
        width:  width,
        height: '585',
        opacity:0.3,
        content: $("#" + targetDive).get(0)
      });
      $("#" + targetDive).data('dialog', d);
    }

     //baseLable����б���ͼ
    var BaseLableView = Backbone.View.extend({
      template: templates.base_lable,
      collection: new models.BaseLableCollection(),
      parsm:"",
      pcId:"",
      productView:null,
      initialize:function(options){
          this.setElement(options.el);
          // this.model = model;
//          console.log("base lable view "+options.el);
      },
      render: function(parsm,tempType,pcId,type) {//type:容器标签，只查询CLP的，type值为1；产品标签type不传值
        var v = this;
        v.parsm = parsm;
        v.pcId = pcId;
        this.collection.fetch({
          data: {
            pageNo: this.collection.pageCtrl.pageNo,
            pageSize: this.collection.pageCtrl.pageSize,
            templateType:tempType,
            type:type
          },
          success: function(collection) {
            v.$el.html(v.template({
              lables: collection.toJSON(),
              pageCtrl: models.BaseLableCollection.pageCtrl
            }));
            onLoadInitZebraTable();
          }
        });
      },
      events: {
        "click .gop": "changePage",
        "dblclick .addLable": "addLable",
        "click input[name='showTemp']": "showLable",
      },
      changePage: function(evt) {
        var btn = $(evt.target);
        if (btn.data("pageno")) {
          LableCollection.pageCtrl.pageNo = parseInt(btn.data("pageno"));
        } else if (btn.data("pageplus")) {
          LableCollection.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
        } else if (btn.data("pageto")) {
          var pageto = $("#main").find("#jump_p2").val();
          LableCollection.pageCtrl.pageNo = parseInt(pageto);
        } else return;
        var lableName = $("#input_lableName").val();
        var lableType = $("#input_lableType").val();
        lableView.render();
      },
      addLable: function(evt) {
    	var dialog_width = 780;
        var btn = $(evt.target).parent('tr');
        var lableid = btn.data("lableid");
        var lableName = btn.data("name");
        var lableType = btn.data("labletype");
        var height = btn.data("height");
        var width = btn.data("width");
        var print = btn.data("print");
        var lableContent = btn.data("lable");
        var type = btn.data("type");
        var tempType = btn.data("temptype");        
        if (!lableid) return;
        var model = new models.BaseLableModel();
        // console.log(model);
        model.set({
          lable_name: lableName,
          lable_type: lableType,
          lable_height:height,
          lable_width: width,
          print_name: print,
          lable_content: lableContent,
          type:type,
          lable_template_type:tempType
        });

        // var model = this.collection.findWhere({
        //   lable_id: lableid
        // });
        // console.log(lableName);
        // console.log(this.collection);
        // console.log(model);
        // alert(this.parsm);
        var targetDiv ="";
        if(tempType == 1){
          editLabelView.productView=this.productView;
          editLabelView.render(model,this.parsm,this.pcId);
          targetDiv = "base_lable_edit";
        }else if(tempType == 2){
          editContainerView.productView=this.productView;
          editContainerView.render(model,this.parsm,this.pcId);
          targetDiv = "base_container_edit";
          dialog_width = 1000;
        }   
        buildDialog(targetDiv, lableName, dialog_width);
      },
      showLable: function(evt) {
        var evt = evt.target;
        var lableContent = $(evt).attr("lableContent");
        var lableName = $(evt).attr("lablename");
        var evt_width = $(evt).data("width")+100;
        var evt_height = $(evt).data("height")+150;
        showView.render(lableContent, $(evt).data("width"), $(evt).data("height"));
        // $("#show_view").dialog({
        //   title: "show Lable " + lableName,
        //   draggable: true,
        //   width: evt_width,
        //   height: evt_height,
        //   modal: true,
        // });
        new artDialog({
          title:"Show label",
          lock: true,
          draggable: true,
          width: evt_width,
          height: evt_height,
          opacity:0.3,
          content: $("#show_view").get(0)
        });
      },
    });

    var pcTempView = new BaseLableView({el:"#pcTemp"});//��Ʒ��ģ��
    var containerTempView = new BaseLableView({el:"#containerTemp"});//������ģ��
    return {
      pcTempView:pcTempView,
      containerTempView:containerTempView
    };
});