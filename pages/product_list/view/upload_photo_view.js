/*!
 * 上传图片视图
 */
"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "ajaxFileUploader"
], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates) {	
	function resize(){
		debugger;
		var targetImg = document.getElementById("jcrop_target");   
		var FitWidth=540;
		var FitHeight=400;
		var width = targetImg.width;
		var height = targetImg.height;
		var imgDRaio=width/height;
		var fitRaio=FitWidth/FitHeight;
        if (imgDRaio >= fitRaio) { 
        	if (width > FitWidth) { 
        		width=FitWidth;
      			height=width*(1/imgDRaio);            	
            }            
        } else {
        	if (height > FitHeight) { 
        		height=FitHeight;
   			 	width=imgDRaio*height; 
           }
        }
        targetImg.width = width;
        targetImg.height = height;
        $("#jcrop_has_img_flag").val()=="1";
	}
	
	function appendIamge(img, showDiv){     
	    console.log(img.complete );
	    showDiv.lastElementChild.remove(); 
	    showDiv.appendChild(img);
	    resize();
	}
	
	var UploadPhotoView = Backbone.View.extend({			
        template:templates.upload_photo,
        initialize:function(options){
            this.setElement(options.el);
        },
        events:{
			"change #_upload_photo":"previewPhoto",
			"click #crop_image":"uploadPhoto",
		},
		//选择上传文件
		previewPhoto:function(evt){				
			var imageType = /image.*/;
		    var docObj=document.getElementById("_upload_photo");   
		    var file = docObj.files[0];			    
		    if (file.type.match(imageType)) {
		        var showDiv = document.getElementById("showDiv");
		        var reader = new FileReader();
		        reader.onloadend  = function(e) {
	                debugger;
	                var img = document.createElement("img");
	                img.id = "jcrop_target";
	                img.classList.add("obj");
	                img.file = file; 
	                if(e.target.result != null){
	                	img.src = e.target.result;
	                	img.onload = function(){
	                        appendIamge(img, showDiv);
	                    }  
	                }    
	            };
		        reader.readAsDataURL(file);                                
		    } 				
		},
		//上传(upLoad)
		uploadPhoto:function(evt){
			var tmp = this;
			var photoVal = $("#_upload_photo").val();
			if($("#jcrop_has_img_flag").val()=="0"){
				alert("Please Select The Photo!");
//					$('#_upload_photo').darkTooltip({
//	            		trigger:'hover',
//	            		gravity:'west',
//	            		theme:'light',
//	            		content:'Please Select The Photo!'
//                	});
            	return false;
			}
			$.ajax({
				url:'/Sync10/action/administrator/file_up/JqueryFileUpSubmitFormAction.action',
				data:$('#crop_image_coordinate').serialize(),
				dataType:'JSON',
				type:'post',
				success:function(data){
					alert("Upload Success");
					var imgUrl = '/Sync10/upl_imags_tmp/'+photoVal;
					tmp.jcrop_api.setImage(imgUrl,function(){
						this.setOptions({
							allowSelect:false,
						});
					});
					$('#crop_image').darkTooltip({
	            		trigger:'none',
	            		gravity:'west',
	            		theme:'light',
	            		content:'Upload Success',
             		});

				},
				error:function(){
					$('#crop_image').darkTooltip({
	            		trigger:'none',
	            		gravity:'west',
	            		theme:'light',
	            		content:'System Exception,Failed To Upload!',
             		});
				},
			});
		},
        render:function(){
        	this.$el.html(this.template({}));
        }
    });
	
	var uploadPhotoView = new UploadPhotoView({el:"#upload_photo"});
	
	return uploadPhotoView;

});