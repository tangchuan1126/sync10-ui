"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext",
  "templates",
  "../model/products_model",
  "../model/base_lable_model",
  "./show_view",
  "./base_lable_edit_view",
  "./base_container_edit_view",
  "./base_lable_view",
  "oso.lib/art/plugins/newArtDialog",
  "jqueryui/dialog",
  "domready"
  ], function(page_config, $, Backbone, Handlebars, HandlebarsExt, templates, models, EditModel, ShowView, editLabelView, editContainerView, lableV, artDialog) {
   var showView = new ShowView();//预览视图
    var pcTempView = lableV.pcTempView;//产品基础模板
    var containerTempView = lableV.containerTempView;//容器基础模板
    function buildDialog(targetDive, lableName, width){
      // $("#" + targetDive).dialog({
      //   title: "Edit "+lableName,
      //   width: width,
      //   height: '585',
      //   draggable: true,
      //   modal: true
      // });
      //modify by huhy
        var d = new artDialog({
            lock: true,
            title: "Edit["+lableName+"]",
            draggable: true,
            width: width,
            height: '585',
            opacity:0.3,
            content: $("#" + targetDive).get(0),
        });
        $("#" + targetDive).data('dialog', d);
      //end
  }

    //products数据列表视图
    var ProductView = Backbone.View.extend({
     el:"#context",
     collection:new models.ProductsCollection(),
     template:templates.products,
     parsm:"",
     initialize:function(){
	//              console.log("products view");
	},
	render:function(parsm,del){
	  var v = this;
              //没有查询条件时不显示产品列表
              if (parsm!="?1=1") {
                v.parsm=parsm;
                this.collection = new models.ProductsCollection();
                this.collection.url = page_config.productsCollection.url+parsm+"&search=product";
                this.collection.fetch({
                  data:{
                      pageNo:models.ProductsCollection.pageCtrl.pageNo,
                      pageSize:models.ProductsCollection.pageCtrl.pageSize
                  },
                  success:function(collection){
                    // console.log(collection);
                	v.procollection = collection;
                    //产品模板列表模板
                    v.$el.find("#products").html(v.template({
                       products:collection.toJSON(),
                       pageCtrl:models.ProductsCollection.pageCtrl
                   })); 
                      onLoadInitZebraTable();//隔行换色
                  }
              }) ; 
            }else{
                v.$el.find("#products").html(""); 
            }
        },
        events:{
          "click .gop": "changePage",
          "click a[name='showlable']":"showLable",
          "click button[name='edit']":"editLable",
          "click button[name='del']":"delLable",
          "click #addSingleTemp":"addSingleTemp"
      },
      changePage: function(evt) {
            $.blockUI({message: '<img src="/Sync10/administrator/js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开 
            var btn = $(evt.currentTarget);
            if (btn.data("pageno")) {
              models.ProductsCollection.pageCtrl.pageNo = parseInt(btn.data("pageno"));
          } else if (btn.data("pageplus")) {
              models.ProductsCollection.pageCtrl.pageNo += parseInt(btn.data("pageplus"));
          } else if (btn.data("pageto")) {
              var pageto = $("#products").find("#jump_p2").val();
              models.ProductsCollection.pageCtrl.pageNo = parseInt(pageto);
          } else return;
            // alert(this.parsm);
            this.render(this.parsm);
            $.unblockUI();       //遮罩关闭
        },
        addSingleTemp:function(evt){
         if(this.parsm.indexOf("title") > 0){
            var pcId=$(evt.currentTarget).data("pcid");
            var v = this;
        	pcTempView.productView=this;
            containerTempView.productView=this;
            pcTempView.render(this.parsm,"1",pcId);
            containerTempView.render(this.parsm,"2",pcId,"1");
                  //modify by huhy
                  // $("#base_lable_tabs").dialog({
                  //     title: "add base lable template",
                  //     draggable: true,
                  //     width: 900,
                  //     height: 600,
                  //     modal: true,
                  // });
                var d = new artDialog({
                    lock: true,
                    title: "Add base label template",
                    draggable: true,
                    width: 900,
                    height: 600,
                    opacity:0.3,
                    content: $("#base_lable_tabs").get(0),
                    close:function(){
                        d.DOM.content.css({'height': 'auto', 'width': 'auto', 'margin-top': '0px'});
                    }
                });
                d.DOM.content.css({'height': '100%', 'width': '100%', 'margin-top': '-35px'});
              }else{
                // alert("Please select a title.");
                new artDialog({
                    title:'Warning',
                    lock: true,
                    icon: 'warning',
                    path: '/Sync10-ui/lib/art/plugins',
                    draggable: true,
                    width: 250,
                    height: 100,
                    opacity:0.3,
                    content: '<p>Please select a title.</p>'
                });
            }
        },
        editLable:function(evt){
        	var dialog_width = 780;  
//             console.log(evt.currentTarget);
			var btn = $(evt.currentTarget)
			var pcId = btn.data("pcid");
			this.pc_id = pcId;
//			console.log(pcId);
			var detailLableId = btn.data("templateid");
			var lableName = btn.data("name");
			var lableType = btn.data("labletype");
			var height = btn.data("height");
			var width = btn.data("width");
			var print = btn.data("print");
			var lableContent = btn.data("lable");
			var type = btn.data("type");
			var tempType = btn.data("temptype");
			if (!detailLableId) return;
			var model = new EditModel.BaseLableModel();
            // debugger;
            model.set({
              detail_lable_id:detailLableId,
              lable_name: lableName,
              lable_type: lableType,
              lable_height:height,
              lable_width: width,
              print_name: print,
              lable_content: lableContent,
              type:type,
              lable_template_type:tempType
          });
            // console.log(model);
            var targetDiv ="";
            if(tempType == 1){
              editLabelView.productView=this;
              editLabelView.render(model, this.parsm, "");
              targetDiv = "base_lable_edit";
          }else if(tempType == 2){
              editContainerView.productView=this;
              editContainerView.render(model, this.parsm, "", true);
              targetDiv = "base_container_edit";
              dialog_width = 1000;
          }   
          buildDialog(targetDiv, lableName, dialog_width);          
      },
      delLable:function(evt){
        var evt = evt.currentTarget;
        var relationId = $(evt).data("relationid") * 1;
            var lableName = $(evt).data("lablename");
            // var pcName = $(evt).data("pcname");
            var templateId = $(evt).data("templateid");
            // alert(relationId);
            if (!relationId) return;
            var relationModel = new models.RelationModel();
            relationModel.set({'relation_id':relationId});
            var v = this;
            var pars = v.parsm;

            // if (confirm("do you want to delete? " + lableName + " lable of "+pcName+"?")) {
            //modify by huhy  改confirm为dialog
            var dialog = new artDialog({
                icon: 'question',
                title:'Delete['+lableName+']',
                path: '/Sync10-ui/lib/art/plugins',
                lock: true,
                width: 250,
                height: 100,
                opacity:0.3,
                content: '<p>Do you want to delete?</p>',
                ok: function () {
                    relationModel.url = relationModel.url + "?relation_id=" + relationId+"&detail_lable_id="+templateId;
                    relationModel.destroy({
                        success: function(relationModel, response) {
                         v.collection = new models.ProductsCollection();
                         v.render(pars,"del");
                     },
                     error: function() {
                      alert("error");
                  }
              });
                },
                okVal:'Sure',
                cancel: function () {
                    dialog.close();
                },
                cancelVal:'Cancel'
            });
          //modify end
         // evt.stopPropagation();
     },
     showLable:function(evt){
       console.log("click showLable");
       var evt = $(evt.currentTarget);
       var lableContent = evt.data("lable");
       var lableName = evt.text();
       var width = evt.data("width");
       var height=evt.data("height");
       showView.render(lableContent, evt.data("width"), evt.data("height"));
           // $("#show_view").dialog({
           //    title: "show Lable " + lableName,
           //    draggable: true,
           //    width: evt.data("width") + 100,
           //    height: evt.data("height") + 150,
           //    modal: true
           //  });
           //modify by huhy 更换dialog
           new artDialog({
            title: "Show label ",
            lock: true,
            draggable: true,
            width: evt.data("width"),
            height: evt.data("height"),
            opacity:0.3,
            content: showView.el,
        });
       }

   });

return new ProductView();

}); //page_inits
