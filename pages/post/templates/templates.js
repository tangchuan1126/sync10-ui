(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_post'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"dialog_update_content\"> \r\n    <form id=\"addPostForm\" >\r\n        <table>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Post Name</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='postName' id='postName' type=\"text\" maxlength=\"30\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <span>Description</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>";
  },"useData":true});
templates['mod_post'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div class=\"dialog_update_content\"> \r\n    <form id=\"modPostForm\" >\r\n        <table>\r\n            <tr>\r\n                <td>\r\n                    <b class=\"require_property\">*</b>\r\n                    <span>Post Name</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='postName' id='postName' type=\"text\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" maxlength=\"30\"/>\r\n                    <input name='adpid' id='adpid' type=\"hidden\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <span>Description</span>\r\n                </td>\r\n                <td>&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.description : stack1), depth0))
    + "\" maxlength=\"100\"/>\r\n                </td>\r\n                <td class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n        </table>\r\n    </form>\r\n</div>";
},"useData":true});
templates['post_search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div> \r\n    <div> \r\n        <div class=\"eso_search_parent\">\r\n            <div class=\"eso_search_icon\">\r\n                <a>\r\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n  \r\n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\r\n            <a id=\"addPost\" href=\"javascript:void(0)\" class=\"buttons icon add button_add_post\">Add Post</a>\r\n        </div>\r\n        <div class=\"eso_search_input\">\r\n\r\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\r\n        </div>\r\n    </div>\r\n</div>";
  },"useData":true});
templates['post_search_result'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.resultList : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <tr data-adpid=\""
    + escapeExpression(lambda((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n                <td valign=\"middle\" width=\"430px\">\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"\">\r\n                    "
    + escapeExpression(lambda((depth0 != null ? depth0.description : depth0), depth0))
    + "\r\n                </td>\r\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\r\n                    <a href=\"javascript:void(0)\" name=\"modPost\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\r\n                </td>\r\n            </tr>\r\n";
},"4":function(depth0,helpers,partials,data) {
  return "	        <tr>\r\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\r\n	        </tr>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"search_result_list\"> \r\n    <div> \r\n        <table class=\"search_result_list_header\" cellspacing=\"0\">\r\n            <tr>\r\n                <th width=\"430px\">Post Name</th>\r\n                <th width=\"\">Description</th>\r\n                <th width=\"200px\" style=\"border-right:0px;\">\r\n                    Operation\r\n                </th>\r\n            </tr>\r\n        </table>\r\n\r\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.resultList : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(4, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </table>\r\n    </div>\r\n\r\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\r\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\r\n        </ul>\r\n    </div>\r\n</div>";
},"useData":true});
})();