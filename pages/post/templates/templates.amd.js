define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['add_post'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\"> \n    <form id=\"addPostForm\" >\n        <table>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Post Name</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='postName' id='postName' type=\"text\" maxlength=\"30\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Description</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n        </table>\n    </form>\n</div>";
},"useData":true});
templates['mod_post'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"dialog_update_content\"> \n    <form id=\"modPostForm\" >\n        <table>\n            <tr>\n                <td>\n                    <b class=\"require_property\">*</b>\n                    <span>Post Name</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='postName' id='postName' type=\"text\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" maxlength=\"30\"/>\n                    <input name='adpid' id='adpid' type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Description</span>\n                </td>\n                <td>&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <input class=\"input_text_password_select\" name='description' id='description' type='text' value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.description : stack1), depth0))
    + "\" maxlength=\"100\"/>\n                </td>\n                <td class=\"status validator_style\">&nbsp;</td>\n            </tr>\n        </table>\n    </form>\n</div>";
},"useData":true});
templates['post_search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div> \n    <div> \n        <div class=\"eso_search_parent\">\n            <div class=\"eso_search_icon\">\n                <a>\n                    <img style=\"cursor:pointer;\" id=\"eso_search\" src=\"image/easy_search_2.png\">\n                </a>\n            </div>\n        </div>\n  \n        <div style=\"position:absolute;width:0px;height:0px;z-index:1;\">\n            <a id=\"addPost\" href=\"javascript:void(0)\" class=\"buttons icon add button_add_post\">Add Post</a>\n        </div>\n        <div class=\"eso_search_input\">\n\n            <input id=\"search_key\" name=\"search_key\" type=\"text\" class=\"ui-autocomplete-input\" autocomplete=\"off\" role=\"textbox\" aria-autocomplete=\"list\" aria-haspopup=\"true\">\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['post_search_result'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <tr data-adpid=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\n                <td valign=\"middle\" width=\"430px\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"\">\n                    "
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "\n                </td>\n                <td valign=\"middle\" width=\"200px\" style=\"text-align: center\">\n                    <a href=\"javascript:void(0)\" name=\"modPost\" class=\"buttons icon edit\" style=\"margin-left: 1px;\" title=\"Edit\"></a>\n                </td>\n            </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    return "	        <tr>\n	            <td style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n	        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"search_result_list\"> \n    <div> \n        <table class=\"search_result_list_header\" cellspacing=\"0\">\n            <tr>\n                <th width=\"430px\">Post Name</th>\n                <th width=\"\">Description</th>\n                <th width=\"200px\" style=\"border-right:0px;\">\n                    Operation\n                </th>\n            </tr>\n        </table>\n\n        <table class=\"search_result_list_content\" cellspacing=\"0\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "        </table>\n    </div>\n\n    <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n</div>";
},"useData":true});
return templates;
});