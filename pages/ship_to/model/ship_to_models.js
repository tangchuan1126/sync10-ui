"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "handlebars_ext"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

		  var ShipToModel = Backbone.Model.extend({
          url: config.getAllShipToJson,
          idAttribute: "ship_to_id",
          defaults: {
            
          },
          
       });
      var CountryModel = Backbone.Model.extend({
          url: config.getAllCountriesJSON,
          idAttribute: "ccid",
       
       });
       var ShipToAddressModel = Backbone.Model.extend({
          idAttribute: "id",
          url: config.storageCatalogAction


       });
       var CountryCollection = Backbone.Collection.extend({
           model: CountryModel,
           url: config.getAllCountriesJSON,

           comparator: function(item) {
                return item.get('c_country');
            },
            parse:function(response){
                
                return response.countries;
            },
           initialize: function(){
            
              this.fetch({dataType: "json",async: false});
               
            }
       });
       var ShipToCollection = Backbone.Collection.extend({
           model: ShipToModel,
           url: config.getAllShipToJson,
           // comparator: function(item) {
           //      return item.get('ship_to_name');
           //  },
            parse:function(response){
              if(response.pageCtrl){this.pageCtrl = response.pageCtrl;}
              return response.items;
            },
           initialize: function(options){
              /*var dis = this;
              if(options!=null && options.type=="easySearch"){
                dis.url = config.searchAllShipToJson+"?q="+options.q;
              }
              else if(options!=null && options.type=="easyStorageSearch"){
                dis.url = config.searchAllStorageShipToJson+"?q="+options.q;
              }*/
            }
          },
            {
              pageCtrl:{
                pageNo:1,
                pageSize:10
            },
            
       });
       var ShipToAddressCollection =  Backbone.Collection.extend({
           model: ShipToAddressModel,
           url: config.storageCatalogAction,
           searchQuery:"",

           initialize: function(options){
              if(options.type == "easyStorageSearch"){
                if(options.searchType == "fuzzy"){
                    this.url = config.searchAllStorageShipToFuzzyJson+"?ship_to_id="+options.shipToId+"&q="+options.searchQuery; 
                }else{
                    this.url = config.searchAllStorageShipToJson+"?ship_to_id="+options.shipToId+"&q="+options.searchQuery; 
                }
                
                this.searchQuery = options.searchQuery;
              }else if(options.type == "advancedSearch"){
                    this.url = config.advancedSearchAllShipToJson+"?ship_to_id="+options.shipToId+"&"+options.searchQuery; 
              }
              else{
                this.url = this.url+"?storage_type_id="+options.shipToId; 
              }
              
            }
            // ,
            // comparator: function(item) {
            //     return item.get('name');
            // }
       });

        return {
	      ShipToAddressCollection:ShipToAddressCollection,
	      ShipToCollection:ShipToCollection,
	      ShipToAddressModel:ShipToAddressModel,
	      ShipToModel:ShipToModel,
	      CountryModel:CountryModel,
	      CountryCollection:CountryCollection
	    };




}); //page_init