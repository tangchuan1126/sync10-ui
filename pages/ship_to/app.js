define([
    './config',
    'jquery',
    'backbone',
    'handlebars',
    'templates',
    "./view/ship_to_view",
    "./view/ship_to_search_tab_view",
    "./view/ship_to_storage_search_tab_view",
    "./view/advanced_search_tab_view",
    "./view/add_storage_catalog_view",
    "./view/export_address_book",
    "blockui",
    "jqueryui/tabs"
],
function(config,$,Backbone,Handlebars,templates,ShipToView,ShipToSearchTabView,ShipToStorageSearchTabView, AdvancedSearchTabView,AddStorageCatalogView,ExportAddressBook, dialog){
	
    $(function(){
    	 $.blockUI.defaults = {
    	            css: {
    	                padding:        '8px',
    	                margin:         0,
    	                width:          '170px',
    	                top:            '45%',
    	                left:           '40%',
    	                textAlign:      'center',
    	                color:          '#000',
    	                border:         '3px solid #999999',
    	                backgroundColor:'#ffffff',
    	                '-webkit-border-radius': '10px',
    	                '-moz-border-radius':    '10px',
    	                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
    	                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
    	            },
    	            overlayCSS:  {
    	                backgroundColor:'#000',
    	                opacity:        '0.3'
    	            },
    	            baseZ: 99999,
    	            centerX: true,
    	            centerY: true,
    	            fadeOut:  1000,
    	            showOverlay: true
    	        };
    	$.blockUI({message:'<div class="block_message">Please wait......</div>'});
        var stv = new ShipToView();
        stv.render();
       
        $("#search_tabs").tabs();
        new ShipToSearchTabView({view:stv}).render();
        new ShipToStorageSearchTabView({view:stv}).render();
        new AdvancedSearchTabView({view:stv}).render();
        new ExportAddressBook({view:stv}).render();

        onLoadInitZebraTable();
        $.unblockUI();
    }); 
    
});
