define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addShipTo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\n	<form id=\"addShipToForm\" >\n		<table>\n			<tr>\n				<td>\n					<b class=\"require_property\" style=\"color:red\">*</b>\n					<span>Title</span>\n				</td>\n				<td>&nbsp;</td>\n			</tr>\n			<tr>\n				<td>\n					<input name='ship_to_title' id='ship_to_title' type=\"text\" />\n				</td>\n				<td class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			\n		</table>\n	</form>\n</div>";
},"useData":true});
templates['advancedSearchTab'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<script>\nfunction adCountryChanged(){\n	var value = $(\"#country_list\").val();\n	$.ajax({\n				url:\"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=\"+value,\n				dataType:'json',\n				type:'post',\n				async:\"false\",\n				success:function(data){\n					if(\"\" != data){\n						isTextState(false);\n						$(\"#ad_tr_state_dd\").show(); \n						$(\"#ad_tr_state\").hide();\n						var provinces = eval(data);\n						$(\"#ad_state_dd\").html(\"\");\n						var str = \"<option value=''></option>\";\n						for(var i = 0; i < data.length; i ++){\n							str += \"<option value=\"+data[i].pro_id+\">\"+data[i].pro_name+\"</option>\";\n						}\n						str += \"<option value='100'>Input...</option>\";\n						$(\"#ad_state_dd\").html(str);\n						$(\"#ad_state_dd\").select2(\"val\",\"\"); \n					}else{\n						var ad_str = \"<option value=''></option>\";\n						ad_str += \"<option value='100'>Input...</option>\";\n						$(\"#ad_state_dd\").html(ad_str);\n						$(\"#ad_state_dd\").select2(\"val\",\"\");\n						$(\"#ad_tr_state\").hide();\n						isTextState(true);\n					}\n				},\n				error:function(){\n					console.log(\"error getting states\");\n				}\n		  })\n}\nfunction isTextState(flag){\n	if(flag){\n		$(\"#ad_is_text_state\").val(\"true\");\n		//$(\"#ad_tr_state\").show();\n		//$(\"#ad_tr_state_dd\").hide();\n		\n	}else{\n		$(\"#ad_is_text_state\").val(\"false\");\n		//$(\"#ad_tr_state\").hide();\n		//$(\"#ad_tr_state_dd\").show();\n		\n	}\n\n}\n\nfunction adStateChanged(){\n	var value = $(\"#ad_state_dd\").val();\n	\n	if(value == \"100\"){\n		$(\"#ad_state_dd\").select2(\"val\",\"100\");\n		$(\"#ad_tr_state\").show();\n		isTextState(true);\n	}else{\n		isTextState(false);\n		\n		$(\"#ad_state\").val(\"\");\n		$(\"#ad_tr_state\").hide();\n		$(\"#ad_tr_state_dd\").show(); \n	}\n}\n</script>\n<style>\n#ad_tr_state{display:none}\n#advanced_search_tab_content table input,select{margin-bottom:0px;}\n</style>\n\n<div id=\"advanced_search_tab_content\">\n\n	<table>\n		<tr>\n			<td>Ship To Title:</br><input id=\"advanced_search_ship_to\" value=\"\" type=\"text\" class=\"input_text_password_select\"/></td>\n			<td>Address Name:</br><input id=\"advanced_search_storage\" value=\"\" type=\"text\" class=\"input_text_password_select\"/></td>\n			<td>Nation:</br>\n				<select id=\"country_list\" name=\"country_list\" onchange=\"adCountryChanged();\" style=\"width:236px;\">\n					<option value=\"\" ></option>\n					\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</select>\n			</td>\n			<td>Province:</br>\n				<div id=\"ad_tr_state_dd\">\n					<select id=\"ad_state_dd\" name=\"ad_state_dd\" onchange=\"adStateChanged();\" style=\"width:236px;\">\n						<option value=\"\"></option>\n						\n					</select>\n				</div>\n			</td>\n			<td>\n				</br>\n				<div id=\"ad_tr_state\">	\n						<input name=\"ad_is_text_state\" id=\"ad_is_text_state\" type=\"hidden\" value=\"true\"/>\n						<input name='ad_state' id='ad_state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" style=\"width:180px;\" value=\"\"/>\n				</div>\n			</td>\n			<td><br/>\n				<button type=\"button\" id=\"advanced_search_btn\" name=\"advanced_search_btn\" class=\"btn btn-default\">Search</button>\n			</td>\n		</tr>\n	</table>\n</div>";
},"useData":true});
templates['editStorageCatalogWidget'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.value : depth0)) != null ? stack1['en-us'] : stack1), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n<script>\nfunction isTextState(flag){\n	if(flag){\n		$(\"#is_text_state\").val(\"true\");\n		//$(\"#tr_state\").show();\n		//$(\"#tr_state_dd\").hide();\n		\n	}else{\n		$(\"#is_text_state\").val(\"false\");\n		//$(\"#tr_state\").hide();\n		//$(\"#tr_state_dd\").show(); \n		\n	}\n\n}\n\nfunction countryChanged(){\n	var value = $(\"#country\").val();\n	$.ajax({\n				url:\"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=\"+value,\n				dataType:'json',\n				type:'post',\n				async:\"false\",\n				success:function(data){ \n					if(\"\" != data){\n						isTextState(false);\n						$(\"#tr_state_dd\").show(); \n						$(\"#tr_state\").hide();\n						var provinces = eval(data);\n						$(\"#state_dd\").html(\"\");\n						var str = \"\";\n						str = \"<option value=''></option>\";\n						for(var i = 0; i < data.length; i ++){\n							str += \"<option value=\"+data[i].pro_id+\">\"+data[i].pro_name+\"</option>\";\n						}\n						str += \"<option value='100'>Input...</option>\";\n						$(\"#state_dd\").html(str);\n						$(\"#state_dd\").select2(\"val\",\"\");\n					}else{\n						var dd_str = \"<option value=''></option>\";\n						dd_str += \"<option value='100'>Input...</option>\";\n						$(\"#state_dd\").html(dd_str);\n						$(\"#state_dd\").select2(\"val\",\"\");\n						$(\"#tr_state\").hide();\n						$(\"#state\").val(\"\");\n						isTextState(false);\n					}\n				},\n				error:function(){\n					console.log(\"error getting states\");\n				}\n		  })\n}\n\nfunction selectCountryState(){\n	var model_country_id = $(\"#country_id\").val();\n	var model_state_id = $(\"#state_id\").val();\n	$(\"#country option[value='\"+model_country_id+\"']\").attr(\"selected\",\"selected\");\n	\n	\n}\nfunction stateChanged(){\n	var value = $(\"#state_dd\").val();\n	\n	if(value == \"100\"){\n		$(\"#state_dd\").select2(\"val\",\"100\");\n		$(\"#tr_state\").show();\n		isTextState(true);\n	}else{\n		isTextState(false);\n		$(\"#tr_state\").hide();\n		$(\"#state\").val(\"\");\n		$(\"#tr_state_dd\").show(); \n	}\n}\n</script>\n<div class=\"\">\n	<form id=\"editStorageCatalogForm\" name=\"editStorageCatalogForm\">\n		<table width=\"660px\" >\n			<tr>\n\n				<td>\n					<label for=\"warehouse_name\">Address Name</label>\n				</td>\n				<td>\n					<input name=\"storage_cat_id\" id=\"storage_cat_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"/>\n					<input name='ship_to_id' id='ship_to_id' type=\"hidden\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ship_to_id : stack1), depth0))
    + "\"/>\n					<input name=\"country_id\" id=\"country_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1['native'] : stack1), depth0))
    + "\"/>	\n					<input name=\"state_id\" id=\"state_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pro_id : stack1), depth0))
    + "\"/>\n					<input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.title : stack1), depth0))
    + "\" />	\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"house_address\">Address1</label>\n				</td>\n				<td>\n					<input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_house_number : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"street_address\">Address2</label>\n				</td>\n				<td>\n					<input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_street : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"city\">City</label>\n				</td>\n				<td>\n					<input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"zipcode\">Zip Code</label>\n				</td>\n				<td>\n					<input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_zip_code : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n		\n			<tr id=\"tr_state_dd\">\n				<td>\n					<label for=\"state_dd\">Province</label>\n				</td>\n				<td>\n					<select id=\"state_dd\" name=\"state_dd\" onchange=\"stateChanged();\" style=\"width:300px;\">\n						<option value=\"\" ></option>\n						\n					</select>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr  id=\"tr_state\">\n				<td>\n					<label for=\"state\"></label>\n				</td>\n				<td>\n					<input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"true\"/>\n					<input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_pro_input : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\"></td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"country\">Nation</label>\n				</td>\n				<td>\n					<select id=\"country\" name=\"country\" onchange=\"countryChanged();\" style=\"width:300px;\">\n						<option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n				</td>\n				\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>	\n			\n			<tr>\n				<td>\n					<label for=\"contact\">Contact</label>\n				</td>\n				<td>\n					<input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"phone\">Phone</label>\n				</td>\n				<td>\n					<input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\"/>\n				</td>\n				<td  class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			<tr>\n                <td>\n                    <label for=\"email\">E-Mail</label>\n                </td>\n                <td>\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\n                    value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_email : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"fax\">Fax</label>\n                </td>\n                <td>\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\n                    value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_fax : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n           <tr>\n                <td>\n                    <label for=\"is_bill_dc\">Bill Used</label>\n                </td>\n                <td>\n                	<select id=\"is_bill_dc\" name=\"is_bill_dc\" style=\"width:300px;\">\n						<option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.yes_not_arr : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n		</table>\n	</form>\n</div>\n\n<script>\n	selectCountryState();\n</script>";
},"useData":true});
templates['shipToAddress'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<table class=\"zebraTable\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >\n<tr>\n	<th style=\"width:15%;\">Address Name</th><th style=\"width:20%;\">Address1</th><th>Address2</th><th style=\"width:8%;\">City</th><th style=\"width:7%;\">Zip Code</th>\n	<th style=\"width:13%;\">Province</th><th style=\"width:6%;\">Operation</th>\n</tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.addresses : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</table>\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "   <tr>\n   		<td>"
    + alias2(alias1((depth0 != null ? depth0.title : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.send_house_number : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.send_street : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.city : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.send_zip_code : depth0), depth0))
    + "</td>\n   		<td>"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pro_id : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</td>\n   		<td id=\"storage_actions_"
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\n            &nbsp; &nbsp;<a title=\"Edit\" name=\"edit_storage\" style=\"text-decoration:none;\">\n               <img src=\"./img/edit.png\" />\n            </a> \n            &nbsp; &nbsp;\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),1,"=",{"name":"xifCond","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),0,"=",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            <input type=\"hidden\" value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" name=\"storage_id\"/>\n         </td>\n   </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.compare || (depth0 && depth0.compare) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.pro_id : depth0),-1,{"name":"compare","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.pro_name : depth0), depth0));
},"4":function(depth0,helpers,partials,data) {
    return this.escapeExpression(this.lambda((depth0 != null ? depth0.send_pro_input : depth0), depth0));
},"6":function(depth0,helpers,partials,data) {
    return "               <a title=\"Inactive\" name=\"pause_storage\">\n                  <img src=\"./img/inactive.png\" />\n               </a>\n";
},"8":function(depth0,helpers,partials,data) {
    return "               <a title=\"Active\" name=\"active_storage\">\n                  <img src=\"./img/active.png\" />\n               </a>\n";
},"10":function(depth0,helpers,partials,data) {
    return "<div class=\"no_data\">\n   No Data\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style>\ntd{padding:3px;}\nth{background-color:#e5e5e5;padding:3px;color:#333;border:1px solid #bbb;line-height:normal;font-size:12px;}\na{text-decoration:none;}\n\n</style>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.addresses : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(10, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['shipToSearchTab'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<style>\n.error{color:red;}\n</style>\n<div id=\"ship_to_search_content\" >\n	<table width=\"100%\" height=\"61\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >\n		<tr>\n	   		<td width=\"50%\" style=\"padding-top:3px;\">\n				<div id=\"easy_search_father\">\n					<div id=\"easy_search\"><a ><img id=\"eso_search\" src=\"img/easy_search_2.png\" width=\"70\" height=\"64\" border=\"0\"/></a></div>\n				</div>\n				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td width=\"418\">\n							<div  class=\"search_shadow_bg\">\n							 	<input name=\"search_key\" type=\"text\" class=\"search_input\" id=\"search_key\" value=\"\" style=\"width:398px\"/>\n							</div>\n						</td>\n						<td >\n							<div > \n							<button type=\"button\" id=\"add_ship_to_btn\" name=\"add_btn\" class=\"btn btn-default\">Add Ship To</button>\n							</div>\n						</td>\n					</tr>\n					<tr>\n						\n						<td >\n							<span class=\"error\" id=\"ship_to_title_error\"></span>\n						</td>\n						<td ></td>\n					</tr>\n				</table>\n			</td>\n		    <td width=\"23%\"></td>\n		    <td width=\"24%\" align=\"right\" valign=\"middle\">\n			</td>\n		</tr>\n	</table>\n</div>\n\n<script>\n$(function() {\n    $(\"#easy_search\").bind(\"contextmenu\", function(event) {\n        event.preventDefault ? event.preventDefault() : event.returnValue = false;\n    });\n}); \n</script>";
},"useData":true});
templates['shipToStorageSearchTab'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<style>\n.error{color:red;}\n</style>\n<div id=\"ship_to_storage_search_content\" >\n	<table width=\"100%\" height=\"61\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n		<tr>\n	   		<td width=\"30%\" style=\"padding-top:3px;\">\n				<div id=\"easy_search_father\">\n					<div id=\"easy_search\" ><a ><img id=\"eso_search\" src=\"img/easy_search_2.png\" width=\"70\" height=\"64\" border=\"0\"/></a></div>\n				</div>\n				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td width=\"418\">\n							<div  class=\"search_shadow_bg\">\n							 	<input name=\"search_key2\" type=\"text\" class=\"search_input\" id=\"search_key2\" value=\"\" style=\"width:398px\"/>\n							</div>\n						</td>\n						<td >\n							\n						</td>\n					</tr>\n					\n				</table>\n			</td>\n		    <td width=\"33%\"></td>\n		    <td width=\"24%\" align=\"right\" valign=\"middle\">\n			</td>\n		</tr>\n	</table>\n</div>\n\n<script>\n$(function() {\n    $(\"#easy_search2\").bind(\"contextmenu\", function(event) {\n        event.preventDefault ? event.preventDefault() : event.returnValue = false;\n    });\n}); \n</script>";
},"useData":true});
templates['shiptolist'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "    <div id=\"ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "_bg\" class=\"ship_to_title_bg\">\n        <div id=\"ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"ship_to_title\" title=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\">\n            "
    + alias2((helpers.truncate || (depth0 && depth0.truncate) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.ship_to_name : depth0),25,{"name":"truncate","hash":{},"data":data}))
    + "\n        </div>\n        <div id=\"ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"ship_to_title_edit\">\n            <input id=\"edit_ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\" name=\"editTitle\" style=\"border:1px solid #000;\"/>\n            <input type=\"hidden\" id=\"hidden_edit_ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\"/>\n            &nbsp;&nbsp;<a style=\"text-decoration:none;\" name=\"ok_btn\" title=\"save\"><img src=\"./img/ok.png\"/> </a>\n            &nbsp;&nbsp;<a name=\"cancel_btn\" title=\"cancel\"><img src=\"./img/cancel.png\"/></a>\n        </div>\n        <div id=\"edit_btn_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"edit_ship_to_btn\">\n            <a name=\"edit_btn\" title=\"Edit\"><img src=\"./img/edit.png\" border=\"0\" valign=\"middle\"/></a>\n            &nbsp;&nbsp;<a name=\"del_btn\" title=\"Delete\"><img src=\"./img/del.png\" border=\"0\"/></a>\n        </div>\n        <div id=\"add_storage_catalog_btn\">\n        <button type=\"button\" id=\"add_storage_catalog_btn_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" name=\"add_storage_catalog_btn_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"btn btn-default\">Add Address Book</button>\n        &nbsp;&nbsp;&nbsp;&nbsp;\n         <span class=\"error\" id=\"update_error_ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "_bg\"></span>\n        </div>\n         <input type=\"hidden\" name=\"shipToID\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\"/>\n         \n         <div class=\"search_panel_div\">\n\n             <label class=\"current_amount_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.current_amount : depth0), depth0))
    + "</label>\n             <label class=\"ship_to_amount\"> / "
    + alias2(alias1((depth0 != null ? depth0.current_amount : depth0), depth0))
    + "</label>\n             \n             <div style=\"display:inline-block;\" class=\"search_panel\" id=\"search_panel_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\">\n                <div id=\"search_input_div\">\n                <input type=\"text\" id=\"search_input\" value=\"\"/></div>\n                <div id=\"search_img_div\"><a name=\"search_img_link\" title=\"search\"><img src=\"./img/search.png\" border=\"0\" id=\"search_img\" valign=\"middle\"/></a></div>\n            </div>\n        </div>\n        <div style=\"clear:both\"></div>\n    </div>\n   \n    <div id=\"ship_to_address_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"ship_to_content\" ></div>\n";
},"4":function(depth0,helpers,partials,data) {
    return "<div class=\"no_data\">\n   No Data\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"ship_to_accordion\">\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.list : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>\n\n<div id=\"pagination\" class=\"pagebox\">\n   \n</div>";
},"useData":true});
templates['storageCatalogWidget'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.value : depth0)) != null ? stack1['en-us'] : stack1), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=this.lambda;

  return "<script>\nfunction isTextState(flag){\n	if(flag){\n		$(\"#is_text_state\").val(\"true\");\n	}else{\n		$(\"#is_text_state\").val(\"false\");\n	}\n\n}\nfunction countryChanged(){\n	var value = $(\"#country\").val();\n	$.ajax({\n				url:\"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=\"+value,\n				dataType:'json',\n				type:'post',\n				async:\"false\",\n				success:function(data){\n					if(\"\" != data){\n						isTextState(false);\n						$(\"#tr_state_dd\").show(); \n						$(\"#tr_state\").hide();\n						var provinces = eval(data);\n						$(\"#state_dd\").html(\"\");\n						var str = \"\";\n						str = \"<option value=''></option>\";\n						for(var i = 0; i < data.length; i ++){\n							str += \"<option value=\"+data[i].pro_id+\">\"+data[i].pro_name+\"</option>\";\n						}\n						str += \"<option value='100'>Input...</option>\";\n						$(\"#state_dd\").html(str);\n						$(\"#state_dd\").select2(\"val\",\"\");\n					}else{\n						var dd_str = \"<option value=''></option>\";\n						dd_str += \"<option value='100'>Input...</option>\";\n						$(\"#state_dd\").html(dd_str);\n						$(\"#state_dd\").select2(\"val\",\"\");\n						$(\"#tr_state\").hide();\n						isTextState(true);\n					}\n				},\n				error:function(){\n					console.log(\"error getting states\");\n				}\n		  })\n}\n\nfunction stateChanged(){\n	var value = $(\"#state_dd\").val();\n	\n	if(value == \"100\"){\n		$(\"#state_dd\").select2(\"val\",\"100\");\n		$(\"#tr_state\").show();\n		isTextState(true);\n	}else{\n		isTextState(false);\n		$(\"#tr_state\").hide();\n		$(\"#state\").val(\"\");\n		$(\"#tr_state_dd\").show(); \n	}\n}\n\n</script>\n<div class=\"\">\n	<form id=\"addStorageCatalogForm\" name=\"addStorageCatalogForm\">\n		<table width=\"660px\" >\n\n			<tr>\n				<td>\n					<label for=\"warehouse_name\">Address Name</label>\n				</td>\n				<td>	\n					<input name='ship_to_id' id='ship_to_id' type=\"hidden\"  value=\""
    + alias1(((helper = (helper = helpers.ship_to_id || (depth0 != null ? depth0.ship_to_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"ship_to_id","hash":{},"data":data}) : helper)))
    + "\"/>\n					<input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"\\/*this.value=this.value.toUpperCase()*\\/\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"house_address\">Address1</label>\n				</td>\n				<td>\n					<input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"street_address\">Address2</label>\n				</td>\n				<td>\n					<input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"city\">City</label>\n				</td>\n				<td>\n					<input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"zipcode\">Zip Code</label>\n				</td>\n				<td>\n					<input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n\n\n\n			<tr id=\"tr_state_dd\">\n				<td>\n					<label for=\"state_dd\">Province</label>\n				</td>\n				<td>\n					<select id=\"state_dd\" name=\"state_dd\" onchange=\"stateChanged();\" style=\"width:300px;\">\n						<option value=\"\" ></option>\n					</select>\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>\n			<tr  id=\"tr_state\">\n				<td>\n					<label for=\"state\"></label>\n				</td>\n				<td>\n					<input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"true\"/>\n					<input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\"></td>\n			</tr>			\n			<tr>\n				<td>\n					<label for=\"country\">Nation</label>\n				</td>\n				<td>\n					<select id=\"country\" name=\"country\" onchange=\"countryChanged();\" style=\"width:300px;\">\n						<option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n					\n				</td>\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\n			</tr>			\n						\n			<tr>\n				<td>\n					<label for=\"contact\">Contact</label>\n				</td>\n				<td>\n					<input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			<tr>\n				<td>\n					<label for=\"phone\">Phone</label>\n				</td>\n				<td>\n					<input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\n				</td>\n				<td  class=\"status validator_style\">&nbsp;</td>\n			</tr>\n			<tr>\n                <td>\n                    <label for=\"phone\">E-Mail</label>\n                </td>\n                <td>\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\n                    value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_email : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"phone\">Fax</label>\n                </td>\n                <td>\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\n                    value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_fax : stack1), depth0))
    + "\"/>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n            <tr>\n                <td>\n                    <label for=\"is_bill_dc\">Bill Used</label>\n                </td>\n                <td>\n                	<select id=\"is_bill_dc\" name=\"is_bill_dc\" style=\"width:300px;\">\n						<option value=\"\" ></option>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.yes_not_arr : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\n                </td>\n                <td  class=\"status validator_style\">&nbsp;</td>\n            </tr>\n		</table>\n	</form>\n</div>";
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "1";
},"useData":true});
return templates;
});