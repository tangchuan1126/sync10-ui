(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addShipTo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"dialog_update_content\">\r\n	<form id=\"addShipToForm\" >\r\n		<table>\r\n			<tr>\r\n				<td>\r\n					<b class=\"require_property\" style=\"color:red\">*</b>\r\n					<span>Title</span>\r\n				</td>\r\n				<td>&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<input name='ship_to_title' id='ship_to_title' type=\"text\" />\r\n				</td>\r\n				<td class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			\r\n		</table>\r\n	</form>\r\n</div>";
},"useData":true});
templates['advancedSearchTab'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<script>\r\nfunction adCountryChanged(){\r\n	var value = $(\"#country_list\").val();\r\n	$.ajax({\r\n				url:\"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=\"+value,\r\n				dataType:'json',\r\n				type:'post',\r\n				async:\"false\",\r\n				success:function(data){\r\n					if(\"\" != data){\r\n						isTextState(false);\r\n						$(\"#ad_tr_state_dd\").show(); \r\n						$(\"#ad_tr_state\").hide();\r\n						var provinces = eval(data);\r\n						$(\"#ad_state_dd\").html(\"\");\r\n						var str = \"<option value=''></option>\";\r\n						for(var i = 0; i < data.length; i ++){\r\n							str += \"<option value=\"+data[i].pro_id+\">\"+data[i].pro_name+\"</option>\";\r\n						}\r\n						str += \"<option value='100'>Input...</option>\";\r\n						$(\"#ad_state_dd\").html(str);\r\n						$(\"#ad_state_dd\").select2(\"val\",\"\"); \r\n					}else{\r\n						var ad_str = \"<option value=''></option>\";\r\n						ad_str += \"<option value='100'>Input...</option>\";\r\n						$(\"#ad_state_dd\").html(ad_str);\r\n						$(\"#ad_state_dd\").select2(\"val\",\"\");\r\n						$(\"#ad_tr_state\").hide();\r\n						isTextState(true);\r\n					}\r\n				},\r\n				error:function(){\r\n					console.log(\"error getting states\");\r\n				}\r\n		  })\r\n}\r\nfunction isTextState(flag){\r\n	if(flag){\r\n		$(\"#ad_is_text_state\").val(\"true\");\r\n		//$(\"#ad_tr_state\").show();\r\n		//$(\"#ad_tr_state_dd\").hide();\r\n		\r\n	}else{\r\n		$(\"#ad_is_text_state\").val(\"false\");\r\n		//$(\"#ad_tr_state\").hide();\r\n		//$(\"#ad_tr_state_dd\").show();\r\n		\r\n	}\r\n\r\n}\r\n\r\nfunction adStateChanged(){\r\n	var value = $(\"#ad_state_dd\").val();\r\n	\r\n	if(value == \"100\"){\r\n		$(\"#ad_state_dd\").select2(\"val\",\"100\");\r\n		$(\"#ad_tr_state\").show();\r\n		isTextState(true);\r\n	}else{\r\n		isTextState(false);\r\n		\r\n		$(\"#ad_state\").val(\"\");\r\n		$(\"#ad_tr_state\").hide();\r\n		$(\"#ad_tr_state_dd\").show(); \r\n	}\r\n}\r\n</script>\r\n<style>\r\n#ad_tr_state{display:none}\r\n#advanced_search_tab_content table input,select{margin-bottom:0px;}\r\n</style>\r\n\r\n<div id=\"advanced_search_tab_content\">\r\n\r\n	<table>\r\n		<tr>\r\n			<td>Ship To Title:</br><input id=\"advanced_search_ship_to\" value=\"\" type=\"text\" class=\"input_text_password_select\"/></td>\r\n			<td>Address Name:</br><input id=\"advanced_search_storage\" value=\"\" type=\"text\" class=\"input_text_password_select\"/></td>\r\n			<td>Nation:</br>\r\n				<select id=\"country_list\" name=\"country_list\" onchange=\"adCountryChanged();\" style=\"width:236px;\">\r\n					<option value=\"\" ></option>\r\n					\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</select>\r\n			</td>\r\n			<td>Province:</br>\r\n				<div id=\"ad_tr_state_dd\">\r\n					<select id=\"ad_state_dd\" name=\"ad_state_dd\" onchange=\"adStateChanged();\" style=\"width:236px;\">\r\n						<option value=\"\"></option>\r\n						\r\n					</select>\r\n				</div>\r\n			</td>\r\n			<td>\r\n				</br>\r\n				<div id=\"ad_tr_state\">	\r\n						<input name=\"ad_is_text_state\" id=\"ad_is_text_state\" type=\"hidden\" value=\"true\"/>\r\n						<input name='ad_state' id='ad_state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" style=\"width:180px;\" value=\"\"/>\r\n				</div>\r\n			</td>\r\n			<td><br/>\r\n				<button type=\"button\" id=\"advanced_search_btn\" name=\"advanced_search_btn\" class=\"btn btn-default\">Search</button>\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</div>";
},"useData":true});
templates['editStorageCatalogWidget'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.value : depth0)) != null ? stack1['en-us'] : stack1), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\r\n<script>\r\nfunction isTextState(flag){\r\n	if(flag){\r\n		$(\"#is_text_state\").val(\"true\");\r\n		//$(\"#tr_state\").show();\r\n		//$(\"#tr_state_dd\").hide();\r\n		\r\n	}else{\r\n		$(\"#is_text_state\").val(\"false\");\r\n		//$(\"#tr_state\").hide();\r\n		//$(\"#tr_state_dd\").show(); \r\n		\r\n	}\r\n\r\n}\r\n\r\nfunction countryChanged(){\r\n	var value = $(\"#country\").val();\r\n	$.ajax({\r\n				url:\"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=\"+value,\r\n				dataType:'json',\r\n				type:'post',\r\n				async:\"false\",\r\n				success:function(data){ \r\n					if(\"\" != data){\r\n						isTextState(false);\r\n						$(\"#tr_state_dd\").show(); \r\n						$(\"#tr_state\").hide();\r\n						var provinces = eval(data);\r\n						$(\"#state_dd\").html(\"\");\r\n						var str = \"\";\r\n						str = \"<option value=''></option>\";\r\n						for(var i = 0; i < data.length; i ++){\r\n							str += \"<option value=\"+data[i].pro_id+\">\"+data[i].pro_name+\"</option>\";\r\n						}\r\n						str += \"<option value='100'>Input...</option>\";\r\n						$(\"#state_dd\").html(str);\r\n						$(\"#state_dd\").select2(\"val\",\"\");\r\n					}else{\r\n						var dd_str = \"<option value=''></option>\";\r\n						dd_str += \"<option value='100'>Input...</option>\";\r\n						$(\"#state_dd\").html(dd_str);\r\n						$(\"#state_dd\").select2(\"val\",\"\");\r\n						$(\"#tr_state\").hide();\r\n						$(\"#state\").val(\"\");\r\n						isTextState(false);\r\n					}\r\n				},\r\n				error:function(){\r\n					console.log(\"error getting states\");\r\n				}\r\n		  })\r\n}\r\n\r\nfunction selectCountryState(){\r\n	var model_country_id = $(\"#country_id\").val();\r\n	var model_state_id = $(\"#state_id\").val();\r\n	$(\"#country option[value='\"+model_country_id+\"']\").attr(\"selected\",\"selected\");\r\n	\r\n	\r\n}\r\nfunction stateChanged(){\r\n	var value = $(\"#state_dd\").val();\r\n	\r\n	if(value == \"100\"){\r\n		$(\"#state_dd\").select2(\"val\",\"100\");\r\n		$(\"#tr_state\").show();\r\n		isTextState(true);\r\n	}else{\r\n		isTextState(false);\r\n		$(\"#tr_state\").hide();\r\n		$(\"#state\").val(\"\");\r\n		$(\"#tr_state_dd\").show(); \r\n	}\r\n}\r\n</script>\r\n<div class=\"\">\r\n	<form id=\"editStorageCatalogForm\" name=\"editStorageCatalogForm\">\r\n		<table width=\"660px\" >\r\n			<tr>\r\n\r\n				<td>\r\n					<label for=\"warehouse_name\">Address Name</label>\r\n				</td>\r\n				<td>\r\n					<input name=\"storage_cat_id\" id=\"storage_cat_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.id : stack1), depth0))
    + "\"/>\r\n					<input name='ship_to_id' id='ship_to_id' type=\"hidden\"  value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ship_to_id : stack1), depth0))
    + "\"/>\r\n					<input name=\"country_id\" id=\"country_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1['native'] : stack1), depth0))
    + "\"/>	\r\n					<input name=\"state_id\" id=\"state_id\" type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.pro_id : stack1), depth0))
    + "\"/>\r\n					<input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.title : stack1), depth0))
    + "\" />	\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"house_address\">Address1</label>\r\n				</td>\r\n				<td>\r\n					<input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_house_number : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"street_address\">Address2</label>\r\n				</td>\r\n				<td>\r\n					<input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_street : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"city\">City</label>\r\n				</td>\r\n				<td>\r\n					<input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.city : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"zipcode\">Zip Code</label>\r\n				</td>\r\n				<td>\r\n					<input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_zip_code : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n		\r\n			<tr id=\"tr_state_dd\">\r\n				<td>\r\n					<label for=\"state_dd\">Province</label>\r\n				</td>\r\n				<td>\r\n					<select id=\"state_dd\" name=\"state_dd\" onchange=\"stateChanged();\" style=\"width:300px;\">\r\n						<option value=\"\" ></option>\r\n						\r\n					</select>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr  id=\"tr_state\">\r\n				<td>\r\n					<label for=\"state\"></label>\r\n				</td>\r\n				<td>\r\n					<input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"true\"/>\r\n					<input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_pro_input : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\"></td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"country\">Nation</label>\r\n				</td>\r\n				<td>\r\n					<select id=\"country\" name=\"country\" onchange=\"countryChanged();\" style=\"width:300px;\">\r\n						<option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\r\n				</td>\r\n				\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>	\r\n			\r\n			<tr>\r\n				<td>\r\n					<label for=\"contact\">Contact</label>\r\n				</td>\r\n				<td>\r\n					<input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.contact : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"phone\">Phone</label>\r\n				</td>\r\n				<td>\r\n					<input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.phone : stack1), depth0))
    + "\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n                <td>\r\n                    <label for=\"email\">E-Mail</label>\r\n                </td>\r\n                <td>\r\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\r\n                    value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_email : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"fax\">Fax</label>\r\n                </td>\r\n                <td>\r\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\r\n                    value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_fax : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n           <tr>\r\n                <td>\r\n                    <label for=\"is_bill_dc\">Bill Used</label>\r\n                </td>\r\n                <td>\r\n                	<select id=\"is_bill_dc\" name=\"is_bill_dc\" style=\"width:300px;\">\r\n						<option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.yes_not_arr : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n		</table>\r\n	</form>\r\n</div>\r\n\r\n<script>\r\n	selectCountryState();\r\n</script>";
},"useData":true});
templates['shipToAddress'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "\r\n<table class=\"zebraTable\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >\r\n<tr>\r\n	<th style=\"width:15%;\">Address Name</th><th style=\"width:20%;\">Address1</th><th>Address2</th><th style=\"width:8%;\">City</th><th style=\"width:7%;\">Zip Code</th>\r\n	<th style=\"width:13%;\">Province</th><th style=\"width:6%;\">Operation</th>\r\n</tr>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.addresses : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</table>\r\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "   <tr>\r\n   		<td>"
    + alias2(alias1((depth0 != null ? depth0.title : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.send_house_number : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.send_street : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.city : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.send_zip_code : depth0), depth0))
    + "</td>\r\n   		<td>"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pro_id : depth0),{"name":"if","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</td>\r\n   		<td id=\"storage_actions_"
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">\r\n            &nbsp; &nbsp;<a title=\"Edit\" name=\"edit_storage\" style=\"text-decoration:none;\">\r\n               <img src=\"./img/edit.png\" />\r\n            </a> \r\n            &nbsp; &nbsp;\r\n"
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),1,"=",{"name":"xifCond","hash":{},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.xifCond || (depth0 && depth0.xifCond) || alias3).call(depth0,(depth0 != null ? depth0.active : depth0),0,"=",{"name":"xifCond","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            <input type=\"hidden\" value=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\" name=\"storage_id\"/>\r\n         </td>\r\n   </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.compare || (depth0 && depth0.compare) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.pro_id : depth0),-1,{"name":"compare","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.pro_name : depth0), depth0));
},"4":function(depth0,helpers,partials,data) {
    return this.escapeExpression(this.lambda((depth0 != null ? depth0.send_pro_input : depth0), depth0));
},"6":function(depth0,helpers,partials,data) {
    return "               <a title=\"Inactive\" name=\"pause_storage\">\r\n                  <img src=\"./img/inactive.png\" />\r\n               </a>\r\n";
},"8":function(depth0,helpers,partials,data) {
    return "               <a title=\"Active\" name=\"active_storage\">\r\n                  <img src=\"./img/active.png\" />\r\n               </a>\r\n";
},"10":function(depth0,helpers,partials,data) {
    return "<div class=\"no_data\">\r\n   No Data\r\n</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<style>\r\ntd{padding:3px;}\r\nth{background-color:#e5e5e5;padding:3px;color:#333;border:1px solid #bbb;line-height:normal;font-size:12px;}\r\na{text-decoration:none;}\r\n\r\n</style>\r\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.addresses : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(10, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['shiptolist'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.list : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "    <div id=\"ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "_bg\" class=\"ship_to_title_bg\">\r\n        <div id=\"ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"ship_to_title\" title=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\">\r\n            "
    + alias2((helpers.truncate || (depth0 && depth0.truncate) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.ship_to_name : depth0),25,{"name":"truncate","hash":{},"data":data}))
    + "\r\n        </div>\r\n        <div id=\"ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"ship_to_title_edit\">\r\n            <input id=\"edit_ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\" name=\"editTitle\" style=\"border:1px solid #000;\"/>\r\n            <input type=\"hidden\" id=\"hidden_edit_ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_name : depth0), depth0))
    + "\"/>\r\n            &nbsp;&nbsp;<a style=\"text-decoration:none;\" name=\"ok_btn\" title=\"save\"><img src=\"./img/ok.png\"/> </a>\r\n            &nbsp;&nbsp;<a name=\"cancel_btn\" title=\"cancel\"><img src=\"./img/cancel.png\"/></a>\r\n        </div>\r\n        <div id=\"edit_btn_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"edit_ship_to_btn\">\r\n            <a name=\"edit_btn\" title=\"Edit\"><img src=\"./img/edit.png\" border=\"0\" valign=\"middle\"/></a>\r\n            &nbsp;&nbsp;<a name=\"del_btn\" title=\"Delete\"><img src=\"./img/del.png\" border=\"0\"/></a>\r\n        </div>\r\n        <div id=\"add_storage_catalog_btn\">\r\n        <button type=\"button\" id=\"add_storage_catalog_btn_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" name=\"add_storage_catalog_btn_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"btn btn-default\">Add Address Book</button>\r\n        &nbsp;&nbsp;&nbsp;&nbsp;\r\n         <span class=\"error\" id=\"update_error_ship_to_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "_bg\"></span>\r\n        </div>\r\n         <input type=\"hidden\" name=\"shipToID\" value=\""
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\"/>\r\n         \r\n         <div class=\"search_panel_div\">\r\n\r\n             <label class=\"current_amount_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.current_amount : depth0), depth0))
    + "</label>\r\n             <label class=\"ship_to_amount\"> / "
    + alias2(alias1((depth0 != null ? depth0.current_amount : depth0), depth0))
    + "</label>\r\n             \r\n             <div style=\"display:inline-block;\" class=\"search_panel\" id=\"search_panel_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\">\r\n                <div id=\"search_input_div\">\r\n                <input type=\"text\" id=\"search_input\" value=\"\"/></div>\r\n                <div id=\"search_img_div\"><a name=\"search_img_link\" title=\"search\"><img src=\"./img/search.png\" border=\"0\" id=\"search_img\" valign=\"middle\"/></a></div>\r\n            </div>\r\n        </div>\r\n        <div style=\"clear:both\"></div>\r\n    </div>\r\n   \r\n    <div id=\"ship_to_address_"
    + alias2(alias1((depth0 != null ? depth0.ship_to_id : depth0), depth0))
    + "\" class=\"ship_to_content\" ></div>\r\n";
},"4":function(depth0,helpers,partials,data) {
    return "<div class=\"no_data\">\r\n   No Data\r\n</div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"ship_to_accordion\">\r\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.list : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>\r\n\r\n<div id=\"pagination\" class=\"pagebox\">\r\n   \r\n</div>";
},"useData":true});
templates['shipToSearchTab'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<style>\r\n.error{color:red;}\r\n</style>\r\n<div id=\"ship_to_search_content\" >\r\n	<table width=\"100%\" height=\"61\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >\r\n		<tr>\r\n	   		<td width=\"50%\" style=\"padding-top:3px;\">\r\n				<div id=\"easy_search_father\">\r\n					<div id=\"easy_search\"><a ><img id=\"eso_search\" src=\"img/easy_search_2.png\" width=\"70\" height=\"64\" border=\"0\"/></a></div>\r\n				</div>\r\n				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n					<tr>\r\n						<td width=\"418\">\r\n							<div  class=\"search_shadow_bg\">\r\n							 	<input name=\"search_key\" type=\"text\" class=\"search_input\" id=\"search_key\" value=\"\" style=\"width:398px\"/>\r\n							</div>\r\n						</td>\r\n						<td >\r\n							<div > \r\n							<button type=\"button\" id=\"add_ship_to_btn\" name=\"add_btn\" class=\"btn btn-default\">Add Ship To</button>\r\n							</div>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						\r\n						<td >\r\n							<span class=\"error\" id=\"ship_to_title_error\"></span>\r\n						</td>\r\n						<td ></td>\r\n					</tr>\r\n				</table>\r\n			</td>\r\n		    <td width=\"23%\"></td>\r\n		    <td width=\"24%\" align=\"right\" valign=\"middle\">\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n\r\n<script>\r\n$(function() {\r\n    $(\"#easy_search\").bind(\"contextmenu\", function(event) {\r\n        event.preventDefault ? event.preventDefault() : event.returnValue = false;\r\n    });\r\n}); \r\n</script>";
},"useData":true});
templates['shipToStorageSearchTab'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<style>\r\n.error{color:red;}\r\n</style>\r\n<div id=\"ship_to_storage_search_content\" >\r\n	<table width=\"100%\" height=\"61\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n		<tr>\r\n	   		<td width=\"30%\" style=\"padding-top:3px;\">\r\n				<div id=\"easy_search_father\">\r\n					<div id=\"easy_search\" ><a ><img id=\"eso_search\" src=\"img/easy_search_2.png\" width=\"70\" height=\"64\" border=\"0\"/></a></div>\r\n				</div>\r\n				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n					<tr>\r\n						<td width=\"418\">\r\n							<div  class=\"search_shadow_bg\">\r\n							 	<input name=\"search_key2\" type=\"text\" class=\"search_input\" id=\"search_key2\" value=\"\" style=\"width:398px\"/>\r\n							</div>\r\n						</td>\r\n						<td >\r\n							\r\n						</td>\r\n					</tr>\r\n					\r\n				</table>\r\n			</td>\r\n		    <td width=\"33%\"></td>\r\n		    <td width=\"24%\" align=\"right\" valign=\"middle\">\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n\r\n<script>\r\n$(function() {\r\n    $(\"#easy_search2\").bind(\"contextmenu\", function(event) {\r\n        event.preventDefault ? event.preventDefault() : event.returnValue = false;\r\n    });\r\n}); \r\n</script>";
},"useData":true});
templates['storageCatalogWidget'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.ccid : depth0), depth0))
    + "\" code=\""
    + alias2(alias1((depth0 != null ? depth0.c_code : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.c_country : depth0), depth0))
    + "</option>\r\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "							<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.value : depth0)) != null ? stack1['en-us'] : stack1), depth0))
    + "</option>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression, alias2=this.lambda;

  return "<script>\r\nfunction isTextState(flag){\r\n	if(flag){\r\n		$(\"#is_text_state\").val(\"true\");\r\n	}else{\r\n		$(\"#is_text_state\").val(\"false\");\r\n	}\r\n\r\n}\r\nfunction countryChanged(){\r\n	var value = $(\"#country\").val();\r\n	$.ajax({\r\n				url:\"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid=\"+value,\r\n				dataType:'json',\r\n				type:'post',\r\n				async:\"false\",\r\n				success:function(data){\r\n					if(\"\" != data){\r\n						isTextState(false);\r\n						$(\"#tr_state_dd\").show(); \r\n						$(\"#tr_state\").hide();\r\n						var provinces = eval(data);\r\n						$(\"#state_dd\").html(\"\");\r\n						var str = \"\";\r\n						str = \"<option value=''></option>\";\r\n						for(var i = 0; i < data.length; i ++){\r\n							str += \"<option value=\"+data[i].pro_id+\">\"+data[i].pro_name+\"</option>\";\r\n						}\r\n						str += \"<option value='100'>Input...</option>\";\r\n						$(\"#state_dd\").html(str);\r\n						$(\"#state_dd\").select2(\"val\",\"\");\r\n					}else{\r\n						var dd_str = \"<option value=''></option>\";\r\n						dd_str += \"<option value='100'>Input...</option>\";\r\n						$(\"#state_dd\").html(dd_str);\r\n						$(\"#state_dd\").select2(\"val\",\"\");\r\n						$(\"#tr_state\").hide();\r\n						isTextState(true);\r\n					}\r\n				},\r\n				error:function(){\r\n					console.log(\"error getting states\");\r\n				}\r\n		  })\r\n}\r\n\r\nfunction stateChanged(){\r\n	var value = $(\"#state_dd\").val();\r\n	\r\n	if(value == \"100\"){\r\n		$(\"#state_dd\").select2(\"val\",\"100\");\r\n		$(\"#tr_state\").show();\r\n		isTextState(true);\r\n	}else{\r\n		isTextState(false);\r\n		$(\"#tr_state\").hide();\r\n		$(\"#state\").val(\"\");\r\n		$(\"#tr_state_dd\").show(); \r\n	}\r\n}\r\n\r\n</script>\r\n<div class=\"\">\r\n	<form id=\"addStorageCatalogForm\" name=\"addStorageCatalogForm\">\r\n		<table width=\"660px\" >\r\n\r\n			<tr>\r\n				<td>\r\n					<label for=\"warehouse_name\">Address Name</label>\r\n				</td>\r\n				<td>	\r\n					<input name='ship_to_id' id='ship_to_id' type=\"hidden\"  value=\""
    + alias1(((helper = (helper = helpers.ship_to_id || (depth0 != null ? depth0.ship_to_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"ship_to_id","hash":{},"data":data}) : helper)))
    + "\"/>\r\n					<input name='warehouse_name' id='warehouse_name' type=\"text\" maxlength=\"100\"  class=\"input_text_password_select\" onkeyup=\"\\/*this.value=this.value.toUpperCase()*\\/\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"house_address\">Address1</label>\r\n				</td>\r\n				<td>\r\n					<input name='house_address' id='house_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"street_address\">Address2</label>\r\n				</td>\r\n				<td>\r\n					<input name='street_address' id='street_address' type=\"text\" maxlength=\"100\" class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"city\">City</label>\r\n				</td>\r\n				<td>\r\n					<input name=\"city\" id=\"city\" type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"zipcode\">Zip Code</label>\r\n				</td>\r\n				<td>\r\n					<input name='zipcode' id='zipcode' type=\"text\" maxlength=\"30\"  class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n\r\n\r\n\r\n			<tr id=\"tr_state_dd\">\r\n				<td>\r\n					<label for=\"state_dd\">Province</label>\r\n				</td>\r\n				<td>\r\n					<select id=\"state_dd\" name=\"state_dd\" onchange=\"stateChanged();\" style=\"width:300px;\">\r\n						<option value=\"\" ></option>\r\n					</select>\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>\r\n			<tr  id=\"tr_state\">\r\n				<td>\r\n					<label for=\"state\"></label>\r\n				</td>\r\n				<td>\r\n					<input name=\"is_text_state\" id=\"is_text_state\" type=\"hidden\" value=\"true\"/>\r\n					<input name='state' id='state' type=\"text\" maxlength=\"50\" class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\"></td>\r\n			</tr>			\r\n			<tr>\r\n				<td>\r\n					<label for=\"country\">Nation</label>\r\n				</td>\r\n				<td>\r\n					<select id=\"country\" name=\"country\" onchange=\"countryChanged();\" style=\"width:300px;\">\r\n						<option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\r\n					\r\n				</td>\r\n				<td  class=\"status validator_style\" style=\"color:red;\">*</td>\r\n			</tr>			\r\n						\r\n			<tr>\r\n				<td>\r\n					<label for=\"contact\">Contact</label>\r\n				</td>\r\n				<td>\r\n					<input name='contact' id='contact' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n				<td>\r\n					<label for=\"phone\">Phone</label>\r\n				</td>\r\n				<td>\r\n					<input name='phone' id='phone' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"/>\r\n				</td>\r\n				<td  class=\"status validator_style\">&nbsp;</td>\r\n			</tr>\r\n			<tr>\r\n                <td>\r\n                    <label for=\"phone\">E-Mail</label>\r\n                </td>\r\n                <td>\r\n                    <input name='email' id='email' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\r\n                    value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_email : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"phone\">Fax</label>\r\n                </td>\r\n                <td>\r\n                    <input name='fax' id='fax' type=\"text\" maxlength=\"45\" class=\"input_text_password_select\"\r\n                    value=\""
    + alias1(alias2(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.send_fax : stack1), depth0))
    + "\"/>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    <label for=\"is_bill_dc\">Bill Used</label>\r\n                </td>\r\n                <td>\r\n                	<select id=\"is_bill_dc\" name=\"is_bill_dc\" style=\"width:300px;\">\r\n						<option value=\"\" ></option>\r\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.yes_not_arr : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "					</select>\r\n                </td>\r\n                <td  class=\"status validator_style\">&nbsp;</td>\r\n            </tr>\r\n		</table>\r\n	</form>\r\n</div>";
},"useData":true});
templates['test'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "1";
},"useData":true});
})();