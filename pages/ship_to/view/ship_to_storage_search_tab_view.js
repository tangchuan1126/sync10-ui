"use strict";
 define(["config","jquery","backbone","handlebars","templates","auto",
 	"./ship_to_view",
 	"../model/ship_to_models",
     "artDialog",
 	],
 	function(config,$,Backbone,Handlebars,templates,auto,ShipToView,models, dialog){
		
			
	return  Backbone.View.extend({
 	el:"#tab_storage",
 	template:templates.shipToStorageSearchTab,
 	
  
  initialize:function(options){
    var dis = this;
    dis.view = options.view;
  },
  render:function(){
       	  var v = this;
          v.$el.html(v.template());
          auto.addAutoComplete($("#search_key2"),config.searchShipToStorageJSON,"merge_field","title");
         
    },
 	events:{
          "click #easy_search > a": "easySearchClick",
          "mouseup #easy_search": "easySearchRtClick",
          "contextmenu #easy_search": "stop",
          "keypress #search_key2": "easySearch",
         
   	},
    stop:function(e){
      e.preventDefault();
    },
    easySearchClick:function(evt){
      var dis=this;
      dis.search("normal"); 
    },
    easySearchRtClick:function(evt){
      var dis=this;
      if(evt.type=="mouseup" && evt.which==3){
        dis.search("fuzzy");
      }
      evt.preventDefault();
      
    },
    search:function(searchType){
      var v = this;
      var key = $("#search_key2").val().trim();
        if(key!=""){
        	$.blockUI({message:'<div class="block_message">Please wait......</div>'});
            
            v.view.$el.html("");
            var ve = new ShipToView({type:"easyStorageSearch",q:key,searchType:searchType});
            ve.render();
            $.unblockUI();
       }else{
            var ve = new ShipToView();
            ve.render();
            $.unblockUI();
       }
     },
   	easySearch:function(evt){
      var v = this;
      if(evt.keyCode==13){
        v.search("normal");
      } 

   	},
   	
	});

			
		
});