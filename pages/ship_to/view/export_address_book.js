"use strict";
 define(["config","jquery","backbone","handlebars","templates","auto",
 	"./ship_to_view",
     "artDialog",
 	"../model/ship_to_models",
 	"showMessage"
 	],
 	function(config,$,Backbone,Handlebars,templates,auto,ShipToView,dialog,models){

	return  Backbone.View.extend({

  initialize:function(options){
    var dis = this;
    dis.view = options.view;
  },
  render:function(){
    var v = this; 
    /*$("#export_ship_to").click(function(evt){v.exportShipTo(evt);});*/
    $("#shipToDownload").click(function(evt){v.downloadTemplate(evt);});

  },
 	events:{},
  downloadTemplate:function(evt){

    var url = '/../Sync10/administrator/ship_to/ShiptoAddress.xlsm';
  
    document.downloadTemplateForm.action = url;
    document.downloadTemplateForm.submit();
  },
  exportShipTo:function(evt){

    $.blockUI({message:'<div class="block_message">Please wait......</div>'});

    var v = this;
    var search_type = "common";
    var action_url = config.getAllShipToJson;
    var para = "";

    //ship to search
    var search_key = $("#search_key").val();

    // address book 
    var search_key2 = $("#search_key2").val();

    // advanced search
    var ship_to = $("#advanced_search_ship_to").val();
    var storage = $("#advanced_search_storage").val();
    var country = $("#country_list").val();
    var country_value = "";
    if(country!=""){
      country_value = $("#country_list option[value='"+country+"']").html();
    }
    var is_text_state = $("#ad_is_text_state").val();
    var state = $("#ad_state").val();
    var state_dd = $("#ad_state_dd").val();    
    var state_value = "";

    if(is_text_state=="true"){
      state_value = state;
    }else{
      if(state_dd!=""){
        state_value = $("#ad_state_dd option[value='"+state_dd+"']").html();
        
      }
    }
    // 判断search的key 否则返回所有
    if(search_key != ""){
        search_type = "easySearch";
        //action_url = config.searchAllShipToJson;
        para = "ship_to_name="+search_key+"&search_type="+search_type;
    }
    else if(search_key2 != ""){
        search_type = "easyStorageSearch";
        //action_url = config.searchAllStorageShipToJson;
        para = "address_name="+search_key2+"&search_type="+search_type;
    }
    else if(state_value!="" || storage!="" || ship_to!="" || country_value!=""){
        search_type = "advancedSearch";
        //action_url = config.advancedSearchAllShipToJson;
        para = "state_value="+state_value+"&search_type="+search_type+"&storage="+storage+"&ship_to="+ship_to+"&country_value="+country_value;
    }else{
        para = "search_type="+search_type;
    }
   
    // ajax 方法
    $.ajax({
          url: action_url,
          type: 'get',
          dataType: 'json',
          timeout: 60000,
          cache:false,
          async:false,
          data:para,
          beforeSend:function(request){
          },
          error: function(){
            showMessage("System error.", "error");
            $.unblockUI();
          },
          success: function(data){
            
            if(data["CANEXPORT"]==true)
            {
              window.location="/Sync10/"+data["FILEURL"];
              $.unblockUI();
            }
            else
            {
              $.unblockUI();
              showMessage("No data.", "alert");
            }
          }
        });
  }
   
	});

			
		
});