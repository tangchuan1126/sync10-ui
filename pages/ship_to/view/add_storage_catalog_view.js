"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/ship_to_models",
  "../js/updateDialog",
  "/Sync10/appkey/YesOrNotKey.js",
  "select2"
], function($, Backbone, Handlebars, templates, models, updateDialog, yesOrNotKey) {

    return Backbone.View.extend({
       el:"#add_storage_catalog_div",
       template:templates.storageCatalogWidget,
      
       initialize: function(model,view){
        var k = this;
        k.model = model;
        k.view = view;
      },
      
      render:function(callBack){
        var v = this;
        v.countryCollection = new models.CountryCollection();
        //yesOrNotKey处理
        var yesOrNotArr = new Array();
        for(var i in yesOrNotKey)
        {
            var yesOrNotObj = {"key":i,"value":yesOrNotKey[i]};
            yesOrNotArr.push(yesOrNotObj);
        }
        var html =v.template({"ship_to_name":v.model.ship_to_name, 
                        "ship_to_id":v.model.ship_to_id,
                        countries:v.countryCollection.toJSON(), "yes_not_arr":yesOrNotArr.reverse()});
        
        //this.$el.html(html);
        updateDialog(
              '[ '+v.model.ship_to_name+' ] Add Address Book'
              //,v.el
              ,html
              ,'680px'
              ,function(){
                
                  if($("#addStorageCatalogForm").valid()){
                    if($("#is_text_state").val()=="true"){
                      if($("#state").val()==""){
                        $("#state").parent().next().append("<label  class='error' for='state'> * please enter province</label>");
                        return false;
                      }else{
                        $("#state").parent().next().html("&nbsp;");
                      }
                    }else{
                      if($("#state_dd").val()==""){
                        $("#state_dd").parent().next().append("<label  class='error' for='state'> * please select province</label>");
                        return false;
                      }else{
                        $("#state").parent().next().html("&nbsp;");
                      }
                    }
                    $.blockUI({message:'<div class="block_message">Please wait......</div>'});
                    var para ="title="+$("#warehouse_name").val()+"&storage_type_id="+$("#ship_to_id").val();
                    var err= false;
                    $.ajax({
                          url:  '/Sync10/action/administrator/shipTo/checkStorageNameAvailability.action',
                          type: 'post',
                          timeout: 60000,
                          cache:false,
                          dataType: 'json',
                          data:para,
                          async:false,
                          
                          error: function(jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                          },
                          success: function(data){
                            
                            if(data.available=="false"){
                                $("#warehouse_name").parent().next().append("<label  class='error' for='warehouse_name'> Address name already exists</label>");
                                err =  true;
                            }
                            
                          }
                        });
                    if(err){$.unblockUI();return false;}

                    $.unblockUI();
                    var obj = new models.ShipToAddressModel();
                    var form = $("#addStorageCatalogForm");
                    obj.set({"storage_type_id": $("#ship_to_id").val(),"name":$("#warehouse_name").val()});
                    
                    obj.url = obj.url.split("?")[0] + "?storage_type_id="+$("#ship_to_id").val()+
                    "&name="+encodeURIComponent($("#warehouse_name").val())+"&house_address="+encodeURIComponent($("#house_address").val())+
                    "&street_address="+encodeURIComponent($("#street_address").val())+"&city="+encodeURIComponent($("#city").val())+
                    "&country="+encodeURIComponent($("#country").val())+"&state="+encodeURIComponent($("#state").val())+
                    "&state_dd="+encodeURIComponent($("#state_dd").val())+"&zipcode="+encodeURIComponent($("#zipcode").val())+
                    "&contact="+encodeURIComponent($("#contact").val())+"&phone="+encodeURIComponent($("#phone").val())+"&is_text_state="+$("#is_text_state").val()
                     +"&email="+encodeURIComponent($("#email").val())+"&fax="+encodeURIComponent($("#fax").val())+"&is_bill_dc="+$("#is_bill_dc").val()
                     +"&is_bill_dc="+$("#is_bill_dc").val();

                    obj.save({},{
                            success:function(modelReturned, response, options){
                              
                              // showMessage("Storage catalog has been added successfully.","succeed");
                              // obj = modelReturned;
                              // v.view.$el.html("");
                              // //v.view.collection.add(obj);  
                              // //v.view.render();
                              callBack(modelReturned);

                          },
                           error:function(collection, response, options){
                              console.log("error adding storage");
                            }
                          });
                    
                    
                      return true;
                  }else{
                    $("#addStorageCatalogForm").validate().errorList[0].element.focus();
                    return false;
                  }
              }
            );
        	
	        $("#country").select2({
	            placeholder: "Select...",
	            allowClear: true
	        });
	        
	        $("#state_dd").select2({
	            placeholder: "Select...",
	            allowClear: true
	        });
          $("#is_bill_dc").select2({
              allowClear: false
          });
          $("#is_bill_dc").select2("val","2");
	   
	        $("#tr_state").hide();
	        $("#tr_state_dd").show();
	        
      },
      submit:function(){
          
      },
      validator:function(){
          $("#addStorageCatalogForm").validate({
          rules: {
            warehouse_name:{
              required: true
            },
            house_address:{
              required: true
            },
            city:{
              required: true
            },
            zipcode:{
              required: true
            },
            country:{
              required: true
            },
            state_dd:{
              required: true
            },
            email: {
              email: true
            }
            
          },
          messages: {
            warehouse_name:{
              required: " Please enter address name"
            },
            
            house_address:{
              required: " Please enter address1"
            },
            city:{
              required: " Please enter city"
            },
            zipcode:{
              required: " Please enter zip code"
            },
            country:{
              required: " Please select nation"
            },
            state_dd:{
              required: " Please select province"
            }
            
          },
          ignore: "",
          errorPlacement: function(error, element) {  
              error.appendTo(element.parent().next());
          },
          //高亮显示错误
          highlight: function(element, errorClass) {},
          submitHandler: function() {}
          });
        }
    

    });

}); 
