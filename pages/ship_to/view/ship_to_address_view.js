"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars_ext",
  "handlebars",
  "templates",
  "../model/ship_to_models",
  "../js/updateDialog",
  "./edit_storage_catalog_view",
  "artDialog",
], function( config, $, Backbone, _Helper ,Handlebars, templates,models,updateDialog,editStorageCatalogView, dialog) {
    
    var ShipToAddressView =  Backbone.View.extend({
      template: templates.shipToAddress,
      searchQuery:"",
      searchType: "normal",

      initialize: function(options){
        var k = this;
          k.shipToId = options.shipToId;
          k.el="#ship_to_address_"+k.shipToId;  
          k.type = options.type;
          k.searchQuery = options.searchQuery;
          k.searchType = options.searchType;
          k.ListView = options.ListView;
      },

      find_in_attributes:function(searchString,model){
          var attributesList = ["title","send_house_number","send_street","city","send_zip_code","pro_name","send_pro_input","c_country"];
          for (var i = attributesList.length - 1; i >= 0; i--) {
            if(model.get(attributesList[i])){
              if(model.get(attributesList[i]).toLowerCase().indexOf(searchString)>-1){

                //$("#current_amount").append(model.length);
                return true;
              }
            }
          }
          return false;
      },

      filterStorages: function(searchString){
        var dis = this;
        var result = dis.collection.select(function (model) {
           return dis.find_in_attributes(searchString,model);
        });
        dis.renderFiltered(new models.ShipToAddressCollection(result));
        
        $(".current_amount_"+dis.shipToId).text(new models.ShipToAddressCollection(result).length);
      } ,

      renderFiltered:function(filteredList){
        var vi = this;
        var html =vi.template({addresses: filteredList.toJSON()});
        $(vi.el).html("");
        $(vi.el).html(html);
        vi.pause_address();
        vi.active_address();
        vi.add_event_to_edit_storage();
        vi.add_event_to_search_box();
         onLoadInitZebraTable();
      } , 

      pause_address:function(){
        var dis = this;
        $("#ship_to_address_"+dis.shipToId+" a[name='pause_storage']").unbind("click");
        $("#ship_to_address_"+dis.shipToId+" a[name='pause_storage']").click({view: dis},function(evt){
            var adView = evt.data.view;
            var btn = $(evt.target);
            var container_id = $(evt.target).parent().parent().attr("id");
            var storage_id = $("#"+container_id+" > input[name='storage_id']").val();
            var obj = adView.collection.get(storage_id);
            updateDialog(
              'Inactive Address'
              ,'<div style="width:300px;height:100%;text-align:left;"><b>Inactive '+obj.toJSON().title+' ?</b></div>'
              ,'300px'
              ,function(params){
                  var obj = params.model;
                  var ship_to_id = obj.toJSON().ship_to_id;
                  var adView = params.view;
                  $.blockUI({message:'<div class="block_message">Please wait......</div>'});
                  obj.url = obj.url+"?storage_id="+obj.id+"&style=pause";

                  obj.save(null,{
                            success:function(){
                              //collection, response, options
                              showMessage("Address book has been inactived.","succeed");
                              //adView.render();
                              
                              var list = dis.ListView;
                              list.collection.url = config.getAllShipToJson;
                              list.type="common";
                              list.collection.fetch({dataType: "json",async: false});
                              list.render();
                              list.openShipToStorageTable(ship_to_id);
                              onLoadInitZebraTable();
                            },
                            error:function(){
                              console.log("error while inactive storage.");
                            }
                          });
                 
                  $.unblockUI();
              
              },{model:obj, view:adView});
        });
      },

      active_address:function(){
        var dis = this;
        $("#ship_to_address_"+dis.shipToId+" a[name='active_storage']").unbind("click");
        $("#ship_to_address_"+dis.shipToId+" a[name='active_storage']").click({view: dis},function(evt){
            var adView = evt.data.view;
            var btn = $(evt.target);
            var container_id = $(evt.target).parent().parent().attr("id");
            var storage_id = $("#"+container_id+" > input[name='storage_id']").val();
            var obj = adView.collection.get(storage_id);
            updateDialog(
              'Active Address'
              ,'<div style="width:300px;height:100%;text-align:left;"><b>Active '+obj.toJSON().title+' ?</b></div>'
              ,'300px'
              ,function(params){
                  var obj = params.model;
                  var ship_to_id = obj.toJSON().ship_to_id;
                  var adView = params.view;
                  $.blockUI({message:'<div class="block_message">Please wait......</div>'});
                  obj.url = obj.url+"?storage_id="+obj.id+"&style=active";

                  obj.save(null,{
                            success:function(){
                               showMessage("Address book has been actived.","succeed");
                                //adView.render();
                                
                                var list = dis.ListView;

                                list.collection.url = config.getAllShipToJson;
                                list.type="common";
                                list.collection.fetch({dataType: "json",async: false});
                                list.render();
                                list.openShipToStorageTable(ship_to_id);
                                onLoadInitZebraTable();
                            },
                            error:function(){
                              console.log("error while active storage.");
                            }
                          });
                  
                  $.unblockUI();
              
              },{model:obj, view:adView});
        });
      },


      add_event_to_edit_storage:function(){
        var vi = this;
        $("#ship_to_address_"+vi.shipToId+" a[name='edit_storage']").unbind("click");
        $("#ship_to_address_"+vi.shipToId+" a[name='edit_storage']").click({view: vi},function(evt){
            var adView = evt.data.view;
            var container_id = $(evt.target).parent().parent().attr("id");
            var storage_id = $("#"+container_id+" > input[name='storage_id']").val();
            var obj = adView.collection.get(storage_id);
            var edit_storage_view = new editStorageCatalogView(obj,adView);
            edit_storage_view.render();
            edit_storage_view.validator();
        });
      },

      add_event_to_search_box:function(){
        var vi = this;
        $("#search_panel_"+vi.shipToId+" a[name='search_img_link']").unbind("click");
        $("#search_panel_"+vi.shipToId+" a[name='search_img_link']").click(function(evt){
            var searchString = $("#search_panel_"+vi.shipToId+" input").val().trim();
            if(searchString !=""){
                vi.filterStorages(searchString.toLowerCase());
            }else{
              vi.render();
            }
        });

        $("#search_panel_"+vi.shipToId+" input").unbind("keypress");
        $("#search_panel_"+vi.shipToId+" input").keypress(function(evt){
            var searchString = $("#search_panel_"+vi.shipToId+" input").val().trim();
            if(evt.keyCode==13){
                if(searchString !=""){
                  vi.filterStorages(searchString.toLowerCase());
                }else{
                  vi.render();
                }
            }
            
        });
        
        // when search string is removed either by backspace or by delete key, show all storages
        $("#search_panel_"+vi.shipToId+" input").unbind("keyup");
        $("#search_panel_"+vi.shipToId+" input").keyup(function(evt){
            var searchString = $("#search_panel_"+vi.shipToId+" input").val().trim();
            if(evt.keyCode==8 || evt.keyCode==46){
                if(searchString ==""){
                  vi.render();
                }
            }
            
        });

        $("#search_panel_"+vi.shipToId+" input").unbind("focus");	
        $("#search_panel_"+vi.shipToId+" input").focus(function(evt){
          if($("#ship_to_address_"+vi.shipToId).css("display")=="none"){
            $("#ship_to_accordion > div.ship_to_content").slideUp();
            $("#ship_to_address_"+vi.shipToId).slideToggle();
          }
            
        });
      },
      render:function(){
        var vi = this; 
        vi.collection= new models.ShipToAddressCollection({shipToId: vi.shipToId, type: vi.type, searchQuery:vi.searchQuery, searchType: vi.searchType});
        vi.collection.fetch({dataType: "json",async: false});
        var html =vi.template({addresses: vi.collection.toJSON()});
        $(vi.el).html("");
        $(vi.el).html(html);
        
        $(".current_amount_"+vi.shipToId).text(vi.collection.length);
        
        //hide delete ship to button if it has any storage catalog
        if(vi.collection.length>0 || vi.searchType == "fuzzy" || vi.type=="advancedSearch"){
          $("#edit_btn_"+vi.shipToId+" a[name='del_btn']").hide();
          
        }else{
          $("#edit_btn_"+vi.shipToId+" a[name='del_btn']").show();

        }

        vi.pause_address();
        vi.active_address();
        vi.add_event_to_edit_storage();
        vi.add_event_to_search_box();
         onLoadInitZebraTable();    
     },
      
  });
return ShipToAddressView;
}); 

