"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  "../model/ship_to_models",
  "./ship_to_address_view",
  "./add_ship_to_view",
  "./add_storage_catalog_view",
   "../js/updateDialog",
     "Paging",
     "artDialog",
    "validate",
    "jqueryui/tooltip",
    "darktooltip",
    "showMessage",
    "handlebars_ext"

], function(config,$, Backbone, Handlebars, templates,models,addressView,addShipToView,addStorageCatalogView,updateDialog,Paging,dialog) {
   
       return Backbone.View.extend({
      el:"#ship_to_content",
      template: templates.shiptolist,
      collection: new models.ShipToCollection(),
      type:"common",
      searchQuery:"",
      searchType:"normal",
      currentPage: 1,
      pageSize:10,
      totalPages:0,
      
      
      initialize: function(options){
          var k = this;
          $.blockUI({message:'<div class="block_message">Please wait......</div>'});
          if(options!=null && options.type=="easySearch"){
              k.collection.url = config.searchAllShipToJson+"?q="+options.q;
              k.type="easySearch";
              k.searchQuery = options.q;
          }else if(options!=null && options.type=="easyStorageSearch"){
            if(options.searchType == "fuzzy"){
              k.collection.url = config.searchAllStorageShipToFuzzyJson+"?q="+options.q; 
              k.searchType = "fuzzy"
            }else{
              k.collection.url = config.searchAllStorageShipToJson+"?q="+options.q;  
            }
            
            k.type="easyStorageSearch";
            k.searchQuery = options.q;
          }else if(options!=null && options.type=="advancedSearch"){
              k.collection.url = config.advancedSearchAllShipToJson+"?"+options.params;
              k.type="advancedSearch";
              k.searchQuery = options.params;
          }else if(options==null){
              k.collection.url =  config.getAllShipToJson;
              k.type="common";
             
          }

          k.collection.fetch({dataType: "json",async: false});

          $.unblockUI();
      },
      openShipToStorageTable:function(ship_to_id){
        if($("#ship_to_address_"+ship_to_id).css("display")=="none"){
          $("#ship_to_accordion > div.ship_to_content").slideUp();
          $("#ship_to_address_"+ship_to_id).slideToggle();
        }
      },
      render:function(){
        var v = this;
        $.blockUI({message:'<div class="block_message">Please wait......</div>'});
        var html =v.template({list: v.collection.toJSON()});
        $(v.el).html(html);
        $.each(v.collection.toJSON(), function(i, item) {           
            var adView = new addressView({shipToId: item.ship_to_id, type:v.type, searchQuery:v.searchQuery, searchType:v.searchType, ListView:v});
            adView.render();            
            $("#ship_to_"+item.ship_to_id).click({ship_to_id: item.ship_to_id, dis:v},function(evt){ 
            // click on title, opens its storage area
              var dis = evt.data.dis;
              var ship_to_id = evt.data.ship_to_id;
              dis.openShipToStorageTable(ship_to_id);
            });
            $("#add_storage_catalog_btn_"+item.ship_to_id).click({model: item, view: adView, dis: v},function(evt){
              // add storage catalog button
              var dis = evt.data.dis;
              var model = evt.data.model;
              var adView = evt.data.view;
              var ship_to_id = evt.data.model.ship_to_id;
              dis.openShipToStorageTable(ship_to_id);
              var add_storage_view = new addStorageCatalogView(model,adView);
              add_storage_view.render(function callBack(modelReturned){
                showMessage("Storage catalog has been added successfully.","succeed");
                v.collection.url = config.getAllShipToJson;
                v.type="common";
                v.collection.fetch({dataType: "json",async: false});
                var shipToId = modelReturned.toJSON().storage_type_id;
         
                v.render();
                v.openShipToStorageTable(shipToId);
              });

              add_storage_view.validator();

            });
            
            $("#ship_to_accordion > div.ship_to_content").css("display","none");
            $("#ship_to_accordion > div.ship_to_content").first().css("display","block");
           onLoadInitZebraTable();
           
        });
        $("a[name='del_btn'] img").click(function(evt){v.delete_shipto(evt);});

        $("a[name='edit_btn']").click(function(evt){v.edit_shipto(evt);});
        $("a[name='ok_btn']").click(function(evt){v.edit_ok(evt);});
        $("a[name='cancel_btn']").click(function(evt){v.edit_cancel(evt);});
       


        Paging.update_page(v.collection.pageCtrl.pageCount,v.collection.pageCtrl.pageNo,"pagination",function(pageNo){
          $.blockUI({message:'<div class="block_message">Please wait......</div>'});
          v.collection.fetch({
                  data:{
                    pageNo:pageNo,
                    pageSize:v.collection.pageCtrl.pageSize
                  },
                  success:function(collection){
                   v.render();
                   $.unblockUI();
                  }
            });
         
        });
        $.unblockUI();
      },
      events:{
          
          
      },

      edit_shipto:function(evt){
        var btn = $(evt.target);
        var container_id = $(evt.target).parent().parent().parent().attr("id");
        //$("#update_error_"+container_id).html("");

        $("#"+container_id+" > div.ship_to_title").css("display","none");
        $("#"+container_id+" > div.ship_to_title_edit").css("display","block");
        $("#"+container_id+" > div.ship_to_title_edit > input").focus();

        var ship_to_id = $("#"+container_id+" > div.ship_to_title_edit").attr("id");

        $("#edit_"+ship_to_id).val($("#hidden_edit_"+ship_to_id).val());
      },
      delete_shipto:function(evt){
        var btn = $(evt.target);
        var d = this;
        var container_id = btn.parent().parent().parent().attr("id");
        var id = $("#"+container_id+" > input[name='shipToID']").val();
        updateDialog(
              'Delete Ship To'
              ,'<div style="width:300px;height:100%;text-align:left;"><b>Delete '+$("#edit_ship_to_" + id).val()+' ?</b></div>'
              ,'300px'
              ,function(params){
                var d=params.dis;
                var btn = params.button;
                $.blockUI({message:'<div class="block_message">Please wait......</div>'});
                  var container_id = btn.parent().parent().parent().attr("id");
                  var id = $("#"+container_id+" > input[name='shipToID']").val();
                  var model = d.collection.get(id);
                  model.url = model.url+"?ship_to_id="+id;
                  model.destroy({
                      success:function(collection, response, options){
                        showMessage("Ship to account has been deleted.","succeed");
                        d.collection.fetch({dataType: "json",async: false});
                        d.$el.html("");
                        d.render();
                        
                      },
                      error:function(collection, response, options){
                          showMessage("Error while deleting ship to.","error");
                        }
                    });
                  $.unblockUI();
              },{dis:d,button:btn}
              );
        
      },
      edit_ok:function(evt){
        var btn = $(evt.target);
        var d = this;
        $.blockUI({message:'<div class="block_message">Please wait......</div>'});
        
        var container_id = $(evt.target).parent().parent().parent().attr("id");
        
        var parent_id = $(evt.target).parent().parent().attr("id");
        
        var title = $("#"+parent_id+" > input[name='editTitle']").val();
        var or_title = $("#hidden_edit_"+parent_id).val();

        if(title == or_title){
          d.edit_cancel(evt);
          return;
        }

        if(title==""){
            showMessage("Please enter title.","alert");
            $("#"+parent_id+" > input[name='editTitle']").focus();
        }else{
          $("#"+container_id+" > div.ship_to_title_edit").css("display","none");
          $("#"+container_id+" > div.ship_to_title").css("display","block");
          $("#"+container_id+" > div.edit_ship_to_btn").css("display","block");
            var id = $("#"+container_id+" > input[name='shipToID']").val();
            var obj = d.collection.get(id);

            //obj.set({"ship_to_name":title});
            obj.url = obj.url.split("?")[0]+"?ship_to_id="+id+"&ship_to_name="+encodeURIComponent(title);

            obj.save(null,{

                success: function(modelReturned, response, options){
                  showMessage("Ship to account updated successfully.","succeed");
                  obj = modelReturned;
                  d.render();
                },
                error: function(collection, response, options){

                   if(response.status == 50000){
                      showMessage("Ship To Title already exists in system.","alert");
                      //$("#update_error_"+container_id).html("Ship To Title already exists in system");
                    }else{
                      showMessage("Error while updating storage","error");
                    }
                    //$("#edit_"+parent_id).val("");
                  }
              });
            
        }
        
        $.unblockUI();
        
      },
      edit_cancel:function(evt){
        var btn = $(evt.target);
        var container_id = $(evt.target).parent().parent().parent().attr("id");
        $("#"+container_id+" > div.ship_to_title_edit").css("display","none");
        $("#"+container_id+" > div.ship_to_title").css("display","block");
        $("#"+container_id+" > div.edit_ship_to_btn").css("display","block");
        var parent_id = $(evt.target).parent().parent().attr("id");
        //$("#"+parent_id+" > input[name='editTitle']").val($("#"+container_id+" > div.ship_to_title").html().trim());
        $("#"+parent_id+" > input[name='editTitle']").val($("#"+container_id+" > div.ship_to_title").html());
        $.unblockUI();
      },
      open_storage_catalog_window:function(evt){
        var dis = this;
        var model = evt.data.model;
        var adView = evt.data.view;
        var model = dis.collection.get(id);
        var add_storage_view = new addStorageCatalogView(model.toJSON(),adView);
        add_storage_view.render();
        add_storage_view.validator();

      }
      
 });
}); 