"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  'handlebars_ext',
  "templates",
  "../model/ship_to_models",
  "../js/updateDialog",
  "/Sync10/appkey/YesOrNotKey.js",
  "select2"
 
], function( $, Backbone, Handlebars,HandlebarsExt, templates, models, updateDialog, yesOrNotKey) {
    
    return Backbone.View.extend({
      el:"#add_storage_catalog_div",
      template:templates.editStorageCatalogWidget,
      
      initialize: function(model,view){
        var k = this;
        k.model = model;
        k.view = view;
      },
      
      render:function(){
        var v = this;
        v.countryCollection = new models.CountryCollection();

         //yesOrNotKey处理
        var yesOrNotArr = new Array();
        for(var i in yesOrNotKey)
        {
            var yesOrNotObj = {"key":i,"value":yesOrNotKey[i]};
            yesOrNotArr.push(yesOrNotObj);
        }
        var html =v.template({model:v.model.toJSON(), countries:v.countryCollection.toJSON(), "yes_not_arr":yesOrNotArr.reverse()});
        //this.$el.html(html);
        updateDialog(
              '[ '+v.model.toJSON().ship_to_name+' ] [ '+v.model.toJSON().title+' ] Edit Address Book'
              //,v.el
              ,html
              ,'600px'
              ,function(params){
                
                var obj = params.model;
                var v = params.view;
                  if($("#editStorageCatalogForm").valid()){
                    if($("#is_text_state").val()=="true"){
                      if($("#state").val()==""){
                        $("#state").parent().next().append("<label  class='error' for='state'> * please enter province</label>");
                        return false;
                      }else{
                        $("#state").parent().next().html("&nbsp;");
                      }
                    }else{
                      if($("#state_dd").val()==""){
                        $("#state_dd").parent().next().append("<label  class='error' for='state'> * please select province</label>");
                        return false;
                      }else{
                        $("#state").parent().next().html("&nbsp;");
                      }
                    }
                   
                    var para ="title="+$("#warehouse_name").val()+"&storage_type_id="+$("#ship_to_id").val()+"&sign=edit"+"&storage_cat_id="+$("#storage_cat_id").val();
                    var err= false;
                    $.ajax({
                          url:  '/Sync10/action/administrator/shipTo/checkStorageNameAvailability.action',
                          type: 'post',
                          timeout: 60000,
                          cache:false,
                          dataType: 'json',
                          data:para,
                          async:false,
                          
                          error: function(jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                          },
                          success: function(data){
                            
                            if(data.available=="false"){
                                $("#warehouse_name").parent().next().append("<label  class='error' for='warehouse_name'> Address name already exists</label>");
                                err =  true;
                            }
                            
                          }
                        });
                    if(err){$.unblockUI();return false;}

                    $.unblockUI();

                    var model = obj;
                    model.url = model.url.split("?")[0] + "?storage_id="+$("#storage_cat_id").val()+"&house_address="+encodeURIComponent($("#house_address").val())+
                    "&street_address="+encodeURIComponent($("#street_address").val())+"&city="+encodeURIComponent($("#city").val())+
                    "&country="+encodeURIComponent($("#country").val())+"&state="+encodeURIComponent($("#state").val())+
                    "&state_dd="+encodeURIComponent($("#state_dd").val())+"&zipcode="+encodeURIComponent($("#zipcode").val())+
                    "&contact="+encodeURIComponent($("#contact").val())+"&phone="+encodeURIComponent($("#phone").val())+"&is_text_state="+$("#is_text_state").val()+"&name="+encodeURIComponent($("#warehouse_name").val())
                    +"&email="+encodeURIComponent($("#email").val())+"&fax="+encodeURIComponent($("#fax").val())+"&is_bill_dc="+$("#is_bill_dc").val()
                    +"&is_bill_dc="+$("#is_bill_dc").val();

                   
                    model.save(null,{
                            success:function(){
                              showMessage("Storage catalog has been updated successfully.","succeed");
                              v.render();
                            },
                            error:function(){
                              console.log("error updating storage");
                            }
                          });

                    
                    return true;
                  }else{
                    $("#editStorageCatalogForm").validate().errorList[0].element.focus();
                    return false;
                  }
              },{view:v.view,model:v.model}
            );
        $("#country").select2({
            placeholder: "Select...",
            allowClear: true
        });
        
        $("#state_dd").select2({
            placeholder: "Select...",
            allowClear: true
        });
        $("#is_bill_dc").select2({
              allowClear: false
        });
        $("#is_bill_dc").select2("val",v.model.toJSON().is_bill_dc);
        var pro_id = v.model.toJSON().pro_id;
        //pro_id等于空，就相当于输入state。
    	if(pro_id == undefined || pro_id == "" ||pro_id == -1){
    		$("#tr_state").show();
    		$("#tr_state_dd").show();
        var model_state_id = pro_id;
        $.ajax({
        url:"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+v.model.toJSON().native,
        dataType:'json',
        type:'post',
        async:"false",
        success:function(data){
          if("" != data){
            var provinces = eval(data);
            $("#state_dd").html("");
            var str = "";
            str = "<option value=''></option>";
            for(var i = 0; i < data.length; i ++){
              str += "<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>";
            }
            str += "<option value='100'>Input...</option>";
            $("#state_dd").html(str);
            $("#state_dd").select2("val","100");
          }else{
            var str2 = "<option value=''></option>";
            str2 += "<option value='100'>Input...</option>";
            $("#state_dd").html(str2);
            $("#state_dd").select2("val","100");
          }
        },
        error:function(){
          console.log("error getting states");
        }
      })
    	}else{
    		$("#tr_state").hide();
    		$("#tr_state_dd").show();
    		var model_state_id = pro_id;
    		$.ajax({
				url:"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+v.model.toJSON().native,
				dataType:'json',
				type:'post',
				async:"false",
				success:function(data){
					if("" != data){
						var provinces = eval(data);
						$("#state_dd").html("");
						var str = "";
						str = "<option value=''></option>";
						for(var i = 0; i < data.length; i ++){
							str += "<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>";
						}
						str += "<option value='100'>Input...</option>";
						$("#state_dd").html(str);
						$("#state_dd").select2("val",model_state_id);
					}else{
						$("#state_dd").html("");
					}
				},
				error:function(){
					console.log("error getting states");
				}
		  })
    	}
        
      },
      //暂定event方法的区域
      
      submit:function(){
          
      },
      validator:function(){
          $("#editStorageCatalogForm").validate({
          rules: {
            warehouse_name:{
              required: true
            },
            house_address:{
              required: true
            },
            city:{
              required: true
            },
            zipcode:{
              required: true
            },
            country:{
              required: true
            },
            state_dd:{
              required: true
            },
            email: {
              email: true
            }
            
          },
          messages: {
            warehouse_name:{
              required: " Please enter address name"
            },
            house_address:{
              required: " Please enter address1"
            },
            city:{
              required: " Please enter city"
            },
            zipcode:{
              required: " Please enter zip code"
            },
            country:{
              required: " Please select nation"
            },
            state_dd:{
              required: " Please select province"
            }
          },
          ignore: "",
          errorPlacement: function(error, element) {  
              error.appendTo(element.parent().next());
          },
          //高亮显示错误
          highlight: function(element, errorClass) {},
          submitHandler: function() {}
          });
        }
    

    });

}); 
