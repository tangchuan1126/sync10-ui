"use strict";
 define(["config","jquery","backbone","handlebars","templates","auto",
 	"./ship_to_view",
     "artDialog",
 	"../model/ship_to_models",
 	"showMessage"
 	],
 	function(config,$,Backbone,Handlebars,templates,auto,ShipToView,dialog,models){

	return  Backbone.View.extend({
 	el:"#tab_ship_to",
 	template:templates.shipToSearchTab,

  initialize:function(options){
    var dis = this;
    dis.view = options.view;
  },
  render:function(){
       	  var v = this;
          v.$el.html(v.template());
          auto.addAutoComplete($("#search_key"),config.searchShipToJSON,"merge_field","ship_to_name");
         
    },
 	events:{
          "click #easy_search > a": "easySearchClick",
          "mouseup #easy_search": "easySearchRtClick",
          "keypress #search_key": "easySearch",
          "keyup #search_key": "easySearch",
          "click #add_ship_to_btn": "addShipTo"
   	},
    easySearchClick:function(evt){
      var dis=this;
      dis.search(); 
    },
    easySearchRtClick:function(evt){
      var dis=this;
      if(evt.type=="mouseup" && evt.which==3){
        dis.search();
      }
      evt.preventDefault();
    },
    search:function(){
      var v = this;
      var key = $("#search_key").val().trim();
      var ve;
      $.blockUI({message:'<div class="block_message">Please wait......</div>'});
      v.view.$el.html("");
      if(key!=""){
        ve = new ShipToView({type:"easySearch",q:key});    
      }else{
        ve = new ShipToView();
      } 
      ve.render();
      v.view = ve;
      $.unblockUI();
     },
   	easySearch:function(evt){
      var v = this;
      var key = $("#search_key").val().trim();
      $("#ship_to_title_error").html("");
      if(evt.keyCode==13){
        v.search();
      }else if(evt.keyCode==8 || evt.keyCode==46){
          // if list is already search, then when user removes search keyword, get the complete list again
          if(key=="" && v.view.type!="common"){ 
            v.search();
          }
      } 


   	},
   	addShipTo: function (evt) {




   		var v = this;
   		var key = $("#search_key").val().trim();
   		if(key==""){$("#ship_to_title_error").html("Please enter title");$("#search_key").focus();}
   		else{
   			$("#ship_to_title_error").html("");
         
   			$.blockUI({message:'<div class="block_message">Please wait......</div>'});
        
            var obj = new models.ShipToModel();
            obj.set({"ship_to_name": key});
            obj.url = obj.url.split("?")[0] + "?ship_to_name="+encodeURIComponent(key);
            obj.save({},{async: false,
                    success:function(modelReturned, response, options){
                      
                      showMessage("Ship to account has been added successfully.","succeed");
                      obj = modelReturned;
                      v.view.$el.html("");
                      var ve = new ShipToView();
				              ve.render();
                  },
                   error:function(collection, response, options){
                      
                      if(response.status == 50000){
                        $("#ship_to_title_error").html("Ship To Title already exists in system");$("#search_key").focus();
                        
                      }else{
                        showMessage("Error while adding ship to account.","error");
                      }
                      
                    }
                  });
            
            $.unblockUI();
           
   		}
   	}
   
	});

			
		
});