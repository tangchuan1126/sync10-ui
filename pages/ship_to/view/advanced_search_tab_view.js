"use strict";
 define(["config","jquery","backbone","handlebars","templates",
	"../model/ship_to_models",
	"./ship_to_view",
	"showMessage",
	"select2"
	],
	function(config,$,Backbone,Handlebars,templates,models,ShipToView){
		
			
	return  Backbone.View.extend({
	el:"#tab_advanced",
	template:templates.advancedSearchTab,

	initialize:function(options){
		var dis = this;
		dis.view = options.view;
		dis.countryCollection = new models.CountryCollection();
	},
	render:function(){
			var v = this; 
			v.$el.html(v.template({countries:v.countryCollection.toJSON()}));
			$("#advanced_search_btn").click(function(evt){v.search(evt);});
			
			$("#country_list").select2({
					placeholder: "Select...",
					allowClear: true
			}).on('change',function(){v.search();});
			
			$("#ad_state_dd").select2({
					placeholder: "Select...",
					allowClear: true
			}).on('change',function(){
				var selected = $(this).select2("val");
				if(selected != '100'){v.search();}				
			});



            $('#advanced_search_ship_to').on('keypress',function(event){
                if(event.keyCode == "13")    
                {
                    v.search();
                }
            });

            $('#advanced_search_storage').on('keypress',function(event){
                if(event.keyCode == "13")    
                {
                    v.search();
                }
            });
             $('#ad_state').on('keypress',function(event){
                if(event.keyCode == "13")    
                {
                    v.search();
                }
            });
	},
	events:{

	},
	search:function(evt){

		var v = this;
		var ship_to = $("#advanced_search_ship_to").val();
		var storage = $("#advanced_search_storage").val();
		var country = $("#country_list").val();
		var country_value = "";
		if(country!=""){
			country_value = $("#country_list option[value='"+country+"']").html();
		}
		var is_text_state = $("#ad_is_text_state").val();
		var state = $("#ad_state").val();
		var state_dd = $("#ad_state_dd").val();
		
		
		var state_value = "";
		if(is_text_state=="true"){
			state_value = state;
		}else{
			if(state_dd!=""){
				state_value = $("#ad_state_dd option[value='"+state_dd+"']").html();
				
			}
		}
		//state_value!="" || storage!="" || ship_to!="" || country_value!=""
		if(state_value=="" && storage=="" && ship_to=="" && country_value==""){
			v.view.$el.html("");
			var ve = new ShipToView();
			ve.render();
				
		}else{
			v.view.$el.html("");
			var ve = new ShipToView({type:"advancedSearch",params:"ship_to="+ship_to+"&storage="+storage+"&country="+country_value+"&state="+state_value});
			ve.render();
		}
	 
	}  
		
	});

			
		
});