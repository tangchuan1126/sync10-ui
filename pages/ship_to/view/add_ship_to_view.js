"use strict";
define([
  "jquery",
  "backbone",
  "handlebars",
  "templates"
], function( $, Backbone, Handlebars, templates) {
    
    return Backbone.View.extend({
      el:"#add_shipto_div",
      template:templates.addShipTo,
      
      render:function(){
        
       this.$el.html(this.template());
      },
      submit:function(){
          
      },
      validator:function(){
            
          $("#addShipToForm").validate({
            rules: {
              ship_to_title:{
                required: true
              }
            },
            messages: {
              ship_to_title:{
                required: "Please enter title"
              }
            },
            ignore: "",
            errorPlacement: function(error, element) {  
                error.appendTo(element.parent().next());
            },
            //高亮显示错误
            highlight: function(element, errorClass) {},
            submitHandler: function() {
            }
          });
        }
    });

}); 

