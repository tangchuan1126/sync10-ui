define({
    getAllShipToJson:"/Sync10/action/administrator/shipTo/shipTo.action",
    searchAllShipToJson:"/Sync10/action/administrator/shipTo/easySearchShipToJSONAction.action",
    getAllCountriesJSON:"/Sync10/action/administrator/shipTo/country.action",
    getAllStorageCatalogForShipTo:"/Sync10/action/administrator/shipTo/shipToStorage.action",
    storageCatalogAction:"/Sync10/action/administrator/shipTo/shipToStorage.action",
    searchShipToJSON: "/Sync10/action/administrator/shipTo/searchShipToJSONAction.action",
    searchShipToStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=5&page_size=20",
    searchAllStorageShipToJson:"/Sync10/action/administrator/shipTo/easySearchShipToStorageJSONAction.action",
    searchAllStorageShipToFuzzyJson:"/Sync10/action/administrator/shipTo/easySearchShipToStorageFuzzyJSONAction.action",
    advancedSearchAllShipToJson:"/Sync10/action/administrator/shipTo/advancedSearchShipToStorageJSONAction.action",

    
});
