(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<label for=\"area_list_box\">Area</label>\r\n<input id='area_list_box' type='text' class='form-control' />\r\n<input id=\"selected_area_id\" type=\"hidden\" value=\"0\"/>\r\n";
  },"useData":true});
templates['container_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n	<ul class=\"nav nav-tabs\" id=\"containerTabs\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " 	</ul>\r\n 	<div class=\"tab-content\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <li id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SAC_ID : depth0), depth0))
    + "\">\r\n            <input type=\"hidden\" name=\"con_id\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CON_ID : depth0), depth0))
    + "\"/>\r\n            <input type=\"hidden\" name=\"ps_id\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.PS_ID : depth0), depth0))
    + "\"/>\r\n            <input type=\"hidden\" name=\"slc_id\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\"/>\r\n            <input type=\"hidden\" name=\"pending\" value=\""
    + escapeExpression(lambda((depth0 != null ? depth0.pending : depth0), depth0))
    + "\"/>\r\n            <a href=\"#"
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SAC_ID : depth0), depth0))
    + "\">\r\n                "
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\r\n                <span class=\"\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n            </a>\r\n        </li>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "		\r\n		<div class=\"tab-pane\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">\r\n				\r\n		</div>\r\n		\r\n";
},"6":function(depth0,helpers,partials,data) {
  return "\r\n    <div class=\"no_data\">\r\n       No Data\r\n    </div>\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(6, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['count_differences'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "         \r\n              <div class=\"system_div\">\r\n              <h5>System Containers</h5>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.systemContainerTree : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "              </div>   \r\n";
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "                 <span class=\"parent\">"
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</span>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.children : depth0), {"name":"each","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"3":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                     <span class=\"child\">"
    + escapeExpression(lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</span>\r\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n              <div class=\"physical_div\">\r\n              <h5>Reported Containers</h5>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.physicalContainerTree : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "              </div>   \r\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "      <div id=\"count_dif_div\">\r\n        <h5>Count Differences</h5>\r\n\r\n\r\n        <table class=\"table table-striped\" width=\"100%\" id=\"count_diff_table\" style=\"vertical-align:middle\">\r\n            <thead>\r\n                <th>Product Name</th>\r\n                <th>System Count</th>\r\n                <th>Reported Count</th>\r\n                <th>Comments</th>\r\n                \r\n            </thead>\r\n            <tbody id=\"count_diff_table_body_"
    + escapeExpression(((helper = (helper = helpers.sac_id || (depth0 != null ? depth0.sac_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sac_id","hash":{},"data":data}) : helper)))
    + "\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.countDifferences : depth0), {"name":"each","hash":{},"fn":this.program(8, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            </tbody>\r\n        </table>\r\n        \r\n      </div>\r\n";
},"8":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "    		\r\n		          <tr id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SLD_ID : depth0), depth0))
    + "\">\r\n                <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "</td>\r\n                <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.SYS_STORE : depth0), depth0))
    + "</td>\r\n                <td class=\"difference\">"
    + escapeExpression(lambda((depth0 != null ? depth0.ACTUAL_STORE : depth0), depth0))
    + "</td>\r\n                <td>\r\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pending : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.program(11, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                </td>\r\n                \r\n              </tr>\r\n             \r\n    		\r\n";
},"9":function(depth0,helpers,partials,data) {
  return "                      <textarea col=\"80\" rows=\"2\" name=\"comments\"></textarea>\r\n                      <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\r\n";
  },"11":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                    <span class=\"note\">"
    + escapeExpression(lambda((depth0 != null ? depth0.NOTE : depth0), depth0))
    + "</span>\r\n";
},"13":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <div id=\"approve_btn_div\">\r\n          <input type=\"hidden\" name=\"CON_ID\" value=\""
    + escapeExpression(((helper = (helper = helpers.con_id || (depth0 != null ? depth0.con_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"con_id","hash":{},"data":data}) : helper)))
    + "\"/>\r\n          <input type=\"hidden\" name=\"PS_ID\" value=\""
    + escapeExpression(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\"/>\r\n          <input type=\"hidden\" name=\"SAC_ID\" value=\""
    + escapeExpression(((helper = (helper = helpers.sac_id || (depth0 != null ? depth0.sac_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sac_id","hash":{},"data":data}) : helper)))
    + "\"/>\r\n          <input type=\"hidden\" name=\"SLC_ID\" value=\""
    + escapeExpression(((helper = (helper = helpers.slc_id || (depth0 != null ? depth0.slc_id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"slc_id","hash":{},"data":data}) : helper)))
    + "\"/>\r\n          <button name=\"approve_btn\" class=\"btn btn-default\" type=\"button\" title=\"Filter\">Approve</button>\r\n          \r\n  \r\n      </div>\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n<div id=\"count_container\">\r\n    <div style=\"padding:5px 0px;\">User: Jason</div>\r\n      <div id=\"container_count_dif_div\">\r\n      \r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.systemContainerTree : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.physicalContainerTree : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\r\n      </div>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.countDifferences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.pending : depth0), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</div>";
},"useData":true});
templates['difference_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "\r\n<table class=\"table table-striped\" width=\"100%\" >\r\n<thead>\r\n	<th>Area</th>\r\n  <th class=\"sort_link\"><a class=\""
    + escapeExpression(((helper = (helper = helpers.post_date_css || (depth0 != null ? depth0.post_date_css : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"post_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"post_date\">Posted On</a></th>\r\n  <th>Posted by</th>\r\n   <th class=\"progress_header\">Progress</th>\r\n   <th>Overall Status</th>\r\n   <th>Reviewer</th>\r\n   <th class=\"sort_link\"><a class=\""
    + escapeExpression(((helper = (helper = helpers.approve_date_css || (depth0 != null ? depth0.approve_date_css : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"approve_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"approve_date\">Reviewed On</a></th>\r\n   <th></th>\r\n\r\n</thead>\r\n<tbody>\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</tbody>\r\n</table>\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "   <tr>\r\n   		<td>"
    + escapeExpression(lambda((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.POST_DATE : depth0), depth0))
    + "</td><td>"
    + escapeExpression(lambda((depth0 != null ? depth0.COMMIT_NAME : depth0), depth0))
    + "</td>\r\n        \r\n         <td>\r\n           <div class=\"custom_progress_box\">\r\n             <div class=\"custom_progress\" style=\"background-position:"
    + escapeExpression(lambda((depth0 != null ? depth0.percentage_completed : depth0), depth0))
    + "px 0px;\">\r\n                "
    + escapeExpression(lambda((depth0 != null ? depth0.DIFFERENCE_APPROVE_LOCATION_COUNT : depth0), depth0))
    + " / "
    + escapeExpression(lambda((depth0 != null ? depth0.DIFFERENCE_LOCATION_COUNT : depth0), depth0))
    + "\r\n             </div>\r\n           </div>            \r\n         </td>\r\n         <td>\r\n";
  stack1 = ((helpers.sif || (depth0 && depth0.sif) || helperMissing).call(depth0, (depth0 != null ? depth0.DIFFERENCE_APPROVE_LOCATION_COUNT : depth0), (depth0 != null ? depth0.DIFFERENCE_LOCATION_COUNT : depth0), {"name":"sif","hash":{},"fn":this.program(3, data),"inverse":this.program(5, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "         </td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.APPROVE_NAME : depth0), depth0))
    + "</td>\r\n         <td>"
    + escapeExpression(lambda((depth0 != null ? depth0.APPROVE_DATE : depth0), depth0))
    + "</td>\r\n         <td><button type=\"button\" id=\"review_btn\" class=\"review_btn btn btn-default btn-sm\" title=\"Review\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SAA_ID : depth0), depth0))
    + "\">Review</button></td>\r\n   </tr>\r\n";
},"3":function(depth0,helpers,partials,data) {
  return "               Approved\r\n";
  },"5":function(depth0,helpers,partials,data) {
  return "               Pending\r\n";
  },"7":function(depth0,helpers,partials,data) {
  return "<div class=\"no_data\">\r\n   No Data\r\n</div>\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "<div id=\"pagination\" class=\"pagebox\">\r\n   \r\n</div>";
},"useData":true});
templates['filter_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "\r\n<div id=\"status_dd_div\">\r\n<label for=\"status_select\">Status</label>\r\n<input id='status_select' type='text' class='form-control' />\r\n<input id=\"selected_status\" type=\"hidden\" value=\"0\"/>\r\n</div>\r\n<div id=\"filter_btn_div\">\r\n	<button id=\"filter_btn\" class=\"btn btn-default\" type=\"button\" title=\"Filter\" >Filter</button>\r\n</div>\r\n<input type=\"hidden\" id=\"filtered_status\" value=\"0\" />\r\n<input type=\"hidden\" id=\"filtered_area_id\" value=\"0\" />\r\n<input type=\"hidden\" id=\"filtered_storage_id\" value=\"0\" />\r\n<button id=\"filter_btn_bk\" class=\"btn btn-default\" type=\"button\" title=\"Filter\" style=\"display:none;\">Filter</button>";
  },"useData":true});
templates['location_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "  <div id=\"location_differences_scroll_container\">\r\n    <div class=\"panner scroll_up\" data-scroll-modifier='-1'>&nbsp;</div>\r\n    <div id=\"location_differences_scroll\">\r\n      \r\n      <div id=\"location_differences\">\r\n        <div class=\"tabbable tabs-left\">\r\n          <ul class=\"nav nav-tabs\" id=\"locationTabs\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"panner scroll_down\" data-scroll-modifier='1'>&nbsp;</div>\r\n  </div>\r\n  <div id=\"container_differences\">  \r\n    <div class=\"tab-content\">\r\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.differences : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\r\n  </div>\r\n\r\n\r\n";
},"2":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "              <li id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SAL_ID : depth0), depth0))
    + "\">\r\n                <a href=\"#"
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SAA_ID : depth0), depth0))
    + "\">\r\n                  "
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\r\n                  <span class=\"\" name=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n                </a>\r\n              </li>\r\n";
},"4":function(depth0,helpers,partials,data) {
  var lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        \r\n        <div class=\"tab-pane\" id=\""
    + escapeExpression(lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\">\r\n            \r\n        </div>\r\n        \r\n";
},"6":function(depth0,helpers,partials,data) {
  return "\r\n    <div class=\"no_data\">\r\n       No Data\r\n    </div>\r\n\r\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div id=\"back_btn_div\">\r\n<button id=\"back_btn\" type=\"button\" class=\"btn btn-default\">Back</button>\r\n</div>\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(6, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});
templates['storage_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<label for=\"storage_list_box\">Warehouse</label>\r\n<input id='storage_list_box' type='text' class='form-control' />\r\n<input id=\"selected_storage_id\" type=\"hidden\" value=\"0\"/>\r\n";
  },"useData":true});
})();