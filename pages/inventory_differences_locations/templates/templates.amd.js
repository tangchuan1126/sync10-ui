define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['area_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<label for=\"area_list_box\">Area</label>\n<input id='area_list_box' type='text' class='form-control' />\n<input id=\"selected_area_id\" type=\"hidden\" value=\"0\"/>\n";
},"useData":true});
templates['container_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n	<ul class=\"nav nav-tabs\" id=\"containerTabs\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + " 	</ul>\n 	<div class=\"tab-content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "        <li id=\""
    + alias2(alias1((depth0 != null ? depth0.SAC_ID : depth0), depth0))
    + "\">\n            <input type=\"hidden\" name=\"con_id\" value=\""
    + alias2(alias1((depth0 != null ? depth0.CON_ID : depth0), depth0))
    + "\"/>\n            <input type=\"hidden\" name=\"ps_id\" value=\""
    + alias2(alias1((depth0 != null ? depth0.PS_ID : depth0), depth0))
    + "\"/>\n            <input type=\"hidden\" name=\"slc_id\" value=\""
    + alias2(alias1((depth0 != null ? depth0.SLC_ID : depth0), depth0))
    + "\"/>\n            <input type=\"hidden\" name=\"pending\" value=\""
    + alias2(alias1((depth0 != null ? depth0.pending : depth0), depth0))
    + "\"/>\n            <a href=\"#"
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + alias2(alias1((depth0 != null ? depth0.SAC_ID : depth0), depth0))
    + "\">\n                "
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\n                <span class=\"\" name=\""
    + alias2(alias1((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\n            </a>\n        </li>\n";
},"4":function(depth0,helpers,partials,data) {
    return "		\n		<div class=\"tab-pane\" id=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.CONTAINER : depth0), depth0))
    + "\">\n				\n		</div>\n		\n";
},"6":function(depth0,helpers,partials,data) {
    return "\n    <div class=\"no_data\">\n       No Data\n    </div>\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['count_differences'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "         \n              <div class=\"system_div\">\n              <h5>System Containers</h5>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.systemContainerTree : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "              </div>   \n";
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return "                 <span class=\"parent\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"3":function(depth0,helpers,partials,data) {
    return "                     <span class=\"child\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.text : depth0), depth0))
    + "</span>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n              <div class=\"physical_div\">\n              <h5>Reported Containers</h5>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.physicalContainerTree : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "              </div>   \n";
},"7":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "      <div id=\"count_dif_div\">\n        <h5>Count Differences</h5>\n\n\n        <table class=\"table table-striped\" width=\"100%\" id=\"count_diff_table\" style=\"vertical-align:middle\">\n            <thead>\n                <th>Product Name</th>\n                <th>System Count</th>\n                <th>Reported Count</th>\n                <th>Comments</th>\n                \n            </thead>\n            <tbody id=\"count_diff_table_body_"
    + this.escapeExpression(((helper = (helper = helpers.sac_id || (depth0 != null ? depth0.sac_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"sac_id","hash":{},"data":data}) : helper)))
    + "\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.countDifferences : depth0),{"name":"each","hash":{},"fn":this.program(8, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </tbody>\n        </table>\n        \n      </div>\n";
},"8":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "    		\n		          <tr id=\""
    + alias2(alias1((depth0 != null ? depth0.SLD_ID : depth0), depth0))
    + "\">\n                <td>"
    + alias2(alias1((depth0 != null ? depth0.P_NAME : depth0), depth0))
    + "</td>\n                <td>"
    + alias2(alias1((depth0 != null ? depth0.SYS_STORE : depth0), depth0))
    + "</td>\n                <td class=\"difference\">"
    + alias2(alias1((depth0 != null ? depth0.ACTUAL_STORE : depth0), depth0))
    + "</td>\n                <td>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pending : depth0),{"name":"if","hash":{},"fn":this.program(9, data, 0),"inverse":this.program(11, data, 0),"data":data})) != null ? stack1 : "")
    + "                </td>\n                \n              </tr>\n             \n    		\n";
},"9":function(depth0,helpers,partials,data) {
    return "                      <textarea col=\"80\" rows=\"2\" name=\"comments\"></textarea>\n                      <br><span class=\"text-danger hidden\" name=\"validation_msg\"></span>\n";
},"11":function(depth0,helpers,partials,data) {
    return "                    <span class=\"note\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.NOTE : depth0), depth0))
    + "</span>\n";
},"13":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "    <div id=\"approve_btn_div\">\n          <input type=\"hidden\" name=\"CON_ID\" value=\""
    + alias3(((helper = (helper = helpers.con_id || (depth0 != null ? depth0.con_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"con_id","hash":{},"data":data}) : helper)))
    + "\"/>\n          <input type=\"hidden\" name=\"PS_ID\" value=\""
    + alias3(((helper = (helper = helpers.ps_id || (depth0 != null ? depth0.ps_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"ps_id","hash":{},"data":data}) : helper)))
    + "\"/>\n          <input type=\"hidden\" name=\"SAC_ID\" value=\""
    + alias3(((helper = (helper = helpers.sac_id || (depth0 != null ? depth0.sac_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"sac_id","hash":{},"data":data}) : helper)))
    + "\"/>\n          <input type=\"hidden\" name=\"SLC_ID\" value=\""
    + alias3(((helper = (helper = helpers.slc_id || (depth0 != null ? depth0.slc_id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"slc_id","hash":{},"data":data}) : helper)))
    + "\"/>\n          <button name=\"approve_btn\" class=\"btn btn-default\" type=\"button\" title=\"Filter\">Approve</button>\n          \n  \n      </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n<div id=\"count_container\">\n    <div style=\"padding:5px 0px;\">User: Jason</div>\n      <div id=\"container_count_dif_div\">\n      \n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.systemContainerTree : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.physicalContainerTree : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n      </div>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.countDifferences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.pending : depth0),{"name":"if","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['difference_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "\n<table class=\"table table-striped\" width=\"100%\" >\n<thead>\n	<th>Area</th>\n  <th class=\"sort_link\"><a class=\""
    + alias3(((helper = (helper = helpers.post_date_css || (depth0 != null ? depth0.post_date_css : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"post_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"post_date\">Posted On</a></th>\n  <th>Posted by</th>\n   <th class=\"progress_header\">Progress</th>\n   <th>Overall Status</th>\n   <th>Reviewer</th>\n   <th class=\"sort_link\"><a class=\""
    + alias3(((helper = (helper = helpers.approve_date_css || (depth0 != null ? depth0.approve_date_css : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"approve_date_css","hash":{},"data":data}) : helper)))
    + "\" href=\"#\"  name=\"approve_date\">Reviewed On</a></th>\n   <th></th>\n\n</thead>\n<tbody>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</tbody>\n</table>\n\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "   <tr>\n   		<td>"
    + alias2(alias1((depth0 != null ? depth0.AREA_NAME : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.POST_DATE : depth0), depth0))
    + "</td><td>"
    + alias2(alias1((depth0 != null ? depth0.COMMIT_NAME : depth0), depth0))
    + "</td>\n        \n         <td>\n           <div class=\"custom_progress_box\">\n             <div class=\"custom_progress\" style=\"background-position:"
    + alias2(alias1((depth0 != null ? depth0.percentage_completed : depth0), depth0))
    + "px 0px;\">\n                "
    + alias2(alias1((depth0 != null ? depth0.DIFFERENCE_APPROVE_LOCATION_COUNT : depth0), depth0))
    + " / "
    + alias2(alias1((depth0 != null ? depth0.DIFFERENCE_LOCATION_COUNT : depth0), depth0))
    + "\n             </div>\n           </div>            \n         </td>\n         <td>\n"
    + ((stack1 = (helpers.sif || (depth0 && depth0.sif) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.DIFFERENCE_APPROVE_LOCATION_COUNT : depth0),(depth0 != null ? depth0.DIFFERENCE_LOCATION_COUNT : depth0),{"name":"sif","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "         </td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.APPROVE_NAME : depth0), depth0))
    + "</td>\n         <td>"
    + alias2(alias1((depth0 != null ? depth0.APPROVE_DATE : depth0), depth0))
    + "</td>\n         <td><button type=\"button\" id=\"review_btn\" class=\"review_btn btn btn-default btn-sm\" title=\"Review\" name=\""
    + alias2(alias1((depth0 != null ? depth0.SAA_ID : depth0), depth0))
    + "\">Review</button></td>\n   </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "               Approved\n";
},"5":function(depth0,helpers,partials,data) {
    return "               Pending\n";
},"7":function(depth0,helpers,partials,data) {
    return "<div class=\"no_data\">\n   No Data\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "<div id=\"pagination\" class=\"pagebox\">\n   \n</div>";
},"useData":true});
templates['filter_button'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "\n<div id=\"status_dd_div\">\n<label for=\"status_select\">Status</label>\n<input id='status_select' type='text' class='form-control' />\n<input id=\"selected_status\" type=\"hidden\" value=\"0\"/>\n</div>\n<div id=\"filter_btn_div\">\n	<button id=\"filter_btn\" class=\"btn btn-default\" type=\"button\" title=\"Filter\" >Filter</button>\n</div>\n<input type=\"hidden\" id=\"filtered_status\" value=\"0\" />\n<input type=\"hidden\" id=\"filtered_area_id\" value=\"0\" />\n<input type=\"hidden\" id=\"filtered_storage_id\" value=\"0\" />\n<button id=\"filter_btn_bk\" class=\"btn btn-default\" type=\"button\" title=\"Filter\" style=\"display:none;\">Filter</button>";
},"useData":true});
templates['location_differences_list'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  <div id=\"location_differences_scroll_container\">\n    <div class=\"panner scroll_up\" data-scroll-modifier='-1'>&nbsp;</div>\n    <div id=\"location_differences_scroll\">\n      \n      <div id=\"location_differences\">\n        <div class=\"tabbable tabs-left\">\n          <ul class=\"nav nav-tabs\" id=\"locationTabs\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "          </ul>\n        </div>\n      </div>\n    </div>\n    <div class=\"panner scroll_down\" data-scroll-modifier='1'>&nbsp;</div>\n  </div>\n  <div id=\"container_differences\">  \n    <div class=\"tab-content\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.differences : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n  </div>\n\n\n";
},"2":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "              <li id=\""
    + alias2(alias1((depth0 != null ? depth0.SAL_ID : depth0), depth0))
    + "\">\n                <a href=\"#"
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\" data-toggle=\"tab\" name=\""
    + alias2(alias1((depth0 != null ? depth0.SAA_ID : depth0), depth0))
    + "\">\n                  "
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\n                  <span class=\"\" name=\""
    + alias2(alias1((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\">&nbsp;&nbsp;&nbsp;&nbsp;</span>\n                </a>\n              </li>\n";
},"4":function(depth0,helpers,partials,data) {
    return "        \n        <div class=\"tab-pane\" id=\""
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.SLC_POSITION_ALL : depth0), depth0))
    + "\">\n            \n        </div>\n        \n";
},"6":function(depth0,helpers,partials,data) {
    return "\n    <div class=\"no_data\">\n       No Data\n    </div>\n\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"back_btn_div\">\n<button id=\"back_btn\" type=\"button\" class=\"btn btn-default\">Back</button>\n</div>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.differences : depth0)) != null ? stack1.length : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['storage_list_box'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<label for=\"storage_list_box\">Warehouse</label>\n<input id='storage_list_box' type='text' class='form-control' />\n<input id=\"selected_storage_id\" type=\"hidden\" value=\"0\"/>\n";
},"useData":true});
return templates;
});