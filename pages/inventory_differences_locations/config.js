define({
    
    getAllStorageJSON: "/Sync10/action/administrator/shipTo/searchShipToStorageJSONAction.action?storage_type=1",
    getAreaForStorageJSON: "/Sync10/action/administrator/storage_location/GetLocationAreasJSON.action?ps_id=",
    //getAreaDifferencesJSON: "/Sync10/action/administrator/location/GetAreaDifferencesJSONAction.action",
    getAreaDifferencesJSON: "/Sync10/app/search/filterStorageApproveAreas",
    //getLocationDifferencesJSON: "/Sync10/action/administrator/location/GetLocationDifferencesJSONAction.action?saa_id=",
    getLocationDifferencesJSON: "/Sync10/app/search/getStorageApproveLocations?saa_id=",
    //getContainerDifferencesJSON: "/Sync10/action/administrator/location/GetContainerDifferencesJSONAction.action",
    getContainerDifferencesJSON: "/Sync10/app/search/getStorageApproveContainers",
    getCountDifferenceOfContainerJSON: "/Sync10/app/search/getStorageApproveDifferents?sac_id=",
    saveApproveDifference: "/Sync10/app/update/approveDifferents",

   
});
