"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars",
  "templates",
  'handlebars_ext',
  "../model/area_difference_model.js",
  "./location_differences_list.js",
  "Paging",
  "art_Dialog/dialog",


], function( config, $, Backbone, Handlebars, templates , HandleBarsExt, difference_models, locationDifferencesView,Paging,dialog) {

    return Backbone.View.extend({
      template: templates.difference_list,
      el:"#differences_list_container",
      
      
      render:function(data){

        var dis = this;
        var loading = dialog();
        loading.showModal();
        dis.collection= new difference_models.DifferenceCollection(data);        
        dis.collection.fetch({dataType: "json",async: false});

        var post_date_css = "";
        var approve_date_css = "";
        if(dis.collection.sortby=="post_date"){
          if(dis.collection.sort=="true"){
            post_date_css = "up";
          }else{
            post_date_css = "down"
          }
        }else{
          if(dis.collection.sort=="true"){
            approve_date_css = "up";
          }else{
            approve_date_css = "down"
          }
        }
        var html =dis.template({differences: dis.collection.toJSON(), approve_date_css:approve_date_css, post_date_css:post_date_css});
        dis.$el.html("");
        dis.$el.html(html);

        
        Paging.update_page(dis.collection.pageCtrl.pageCount,dis.collection.pageCtrl.pageNo,"pagination",function(pageNo){
          dis.render({pageNo:pageNo,area_id:dis.collection.area_id,ps_id:dis.collection.ps_id,
            sortby:dis.collection.sortby,sort:dis.collection.sort});
        });

        $("a[name='post_date']").click(function(evt){dis.sort_list(evt);});
        $("a[name='approve_date']").click(function(evt){dis.sort_list(evt);});
        $(".review_btn").click(function(evt){dis.detail_review(evt);});
        loading.close().remove();
        
     },
     events:{

     },

     sort_list:function(evt){
      var dis = this;
      var sortby = $(evt.target).attr("name");
      var loading = dialog();
      loading.showModal();
      dis.collection.sortby = sortby;
      if(dis.collection.sort == "false"){
        
        dis.collection.sort = "true";
      
      }else{
        
        dis.collection.sort = "false";

      }
      
      dis.render({pageNo:1,area_id:dis.collection.area_id,ps_id:dis.collection.ps_id,
            sortby:dis.collection.sortby,sort:dis.collection.sort});
      loading.close().remove();
    },

    detail_review:function(evt){
        var dis = this;
        $("#area_differences").hide();

        var loading = dialog();
        loading.showModal();

        var saa_id = $(evt.target).attr("name");
        new locationDifferencesView().render(saa_id);
        loading.close().remove();
    },

    
      
  });
}); 

