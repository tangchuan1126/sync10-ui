"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      
      var SystemContainerTree = Backbone.Model.extend({
          idAttribute: "id",

          parse:function(data){
            this.children = data.children;
            return data;
          }
       });
      var PhysicalContainerTree = Backbone.Model.extend({
          idAttribute: "id",

       });
      var DifferenceModel = Backbone.Model.extend({
          idAttribute: "PC_ID",
          parse:function(data){
            if(data.APPROVE_STATUS==1){
              data["pending"]=true
            }else{
              data["pending"]=false;
            }
            return data;
          }
       });
       
       var ChildrenModel = Backbone.Model.extend({
          idAttribute: "id",

       });
      var ChildrenCollection =  Backbone.Collection.extend({
           model: ChildrenModel,

           initialize:function(data){
            
           }
      });
      var SystemContainerTreeCollection =  Backbone.Collection.extend({
           model: SystemContainerTree,
      });
      var PhysicalContainerTreeCollection =  Backbone.Collection.extend({
           model: PhysicalContainerTree,

           initialize:function(data){
            this.children = new ChildrenCollection();
           },
           parse:function(data){
            this.children = data.children;
            return data;
           }
      });
      var CountDifferenceCollection =  Backbone.Collection.extend({
           model: DifferenceModel,
           
      });
      var combinedModel = Backbone.Model.extend({
          

       });
      var DifferenceCollection =  Backbone.Collection.extend({
           model: combinedModel,
           url: config.getCountDifferenceOfContainerJSON,
           
           initialize: function(sac_id){
              this.url = this.url+sac_id;
          },
          parse:function(data){
            //var data = '{"systemContainerTree":{"id":1288971,"text":"CLP1288971","children":[{"id":1288972,"text":"BLP1288972","parent_id":1288971},{"id":1288973,"text":"BLP1288973","parent_id":1288971},{"id":1288974,"text":"BLP1288974","parent_id":1288971},{"id":1288975,"text":"BLP1288975","parent_id":1288971}],"parent_id":0},"differents":[{"PS_ID":100000,"APPROVE_STATUS":1,"SLD_ID":1446,"CON_ID":1288971,"SAA_ID":19,"PC_ID":1093152,"SLC_ID":1019467,"SYS_STORE":500.0,"SAC_ID":1446,"ACTUAL_STORE":0.0}],"physicalContainerTree":{"id":1288971,"text":"CLP1288971","children":[{"id":1288976,"text":"BLP1288976","parent_id":1288971},{"id":1288977,"text":"BLP1288977","parent_id":1288971},{"id":1288978,"text":"BLP1288978","parent_id":1288971},{"id":1288979,"text":"BLP1288979","parent_id":1288971}],"parent_id":0}}';
            
              this.countDifferenceCollection = new CountDifferenceCollection(data.differents,{parse:true});
              this.systemContainerTreeCollection = new SystemContainerTreeCollection(data.systemContainerTree,{parse:true});
              this.physicalContainerTreeCollection = new PhysicalContainerTreeCollection(data.physicalContainerTree,{parse:true});
          }
       });

      return {
        DifferenceModel:DifferenceModel,
        DifferenceCollection:DifferenceCollection,

      };




}); //page_init