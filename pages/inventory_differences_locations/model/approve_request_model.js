"use strict";
define([
  "config",
  "jquery",
  "backbone",
  "handlebars"
], function(config, $, Backbone, Handlebars, HandlebarsExt) {

      
      
      var DifferenceModel = Backbone.Model.extend({
        url:config.saveApproveDifference,


       });
       
       var ChildrenModel = Backbone.Model.extend({
          idAttribute: "sld_id",
          defaults:{
            note:""
          },
          initialize:function(data){
            this.note = data.note;
            this.sld_id = data.sld_id;
          }

       });
      var ChildrenCollection =  Backbone.Collection.extend({
           model: ChildrenModel,

      });
      
      

      return {
        DifferenceModel:DifferenceModel,
        ChildrenModel:ChildrenModel,
        ChildrenCollection:ChildrenCollection,

      };




}); //page_init