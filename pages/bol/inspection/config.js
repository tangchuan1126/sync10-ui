define({
	Sync10UIPage: {
		url: "/Sync10-ui/pages"
	},
	Sync10Page: {
		url: "/Sync10/administrator"
	},
	keySearchURl: {
		url: "/Sync10/_b2b/transportOrderIndex/searchForQuality"
	},
	searchURl: {
		url:"/Sync10/_b2b/transportOrder/filterDefaultForQuality"
	},
	workURl: {
		url:"/Sync10/_b2b/transportOrder/filterForQuality"
	},
	searchDefaultURl: {
		url:"/Sync10/_b2b/transportOrder/filterForQuality"
	},
	getByIdForQuality: {
		url:"/Sync10/_b2b/transportOrder/filterByIdsForQuality"
	},
	autoComplete: {
		url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
	},
	storageTree: {
		url:"/Sync10/_b2b/storage/"
	},
	adminTree: {
		url:"/Sync10/_b2b/admin/"
	},
	adminSesion: {
		url:"/Sync10/_b2b/admin/session"
	}
	/*,
	filterModel: {
		url:"/Sync10-ui/pages/temp/filterTransport.action"
	}
	*/
});