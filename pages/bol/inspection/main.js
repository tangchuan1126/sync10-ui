"use strict";
require(["../../../requirejs_config","./config"] ,function($,config) {
	require(["jquery",'backbone','handlebars',"jqueryui/tabs", "art_Dialog/dialog",
		"./js/view/searchView","./js/view/filterView",
		"./js/view/workView","./transportListMain","auto","domready!"],
		function($,Backbone,Handlebars,ui,Dialog,SearchView,FilterView,WorkView,TransportList,auto) {
		$.getJSON(config.adminSesion.url,{},function(d){
			var queryOptions = {el: "#data", url:config.searchDefaultURl.url,queryCondition:{}};
			var resultView = new TransportList(queryOptions);
			resultView.on("events.Error", function(e){
				//$("#tabs").hide();
				$(".loding,.LodingModal").hide();
				var aretr = Dialog({
					title: '服务端状态',
					width: 150,
					content: '请求出错！'
				});
				aretr.showModal();
			});
			$("#tabs").tabs({active: 1}).show();
			var t = new SearchView({el:"#transport_search",resultView:resultView});
			t.render();
			auto.addAutoComplete($("#search_key"),config.autoComplete.url,"MERGE_FIELD","TRANSPORT_ID");
			var worker = new WorkView({el:"#transport_work",resultView:resultView});
			worker.render();
			var temp = new FilterView({resultView:resultView});
			temp.render();
		}).error(function(e){
			if (e.status==401) {
				var aretr = Dialog({
					title: '错误',
					width: 150,
					content: '请确认已经登陆系统！'
				});
				aretr.showModal();
			} else {
				var aretr = Dialog({
					title: '服务端状态',
					width: 150,
					content: '请求出错！'
				});
				aretr.showModal();
			}
		});
	});
});