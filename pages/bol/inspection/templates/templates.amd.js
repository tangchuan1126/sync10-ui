define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['filter_transport'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "					<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=helpers.helperMissing, alias2="function", alias3=helpers.blockHelperMissing, buffer = 
  "<table id=\"filtertable\" class=\"filter\" >\n	<tr>\n		<td>\n			<div id=\"send_psid\" style=\"width:200px\"></div>\n		</td>\n		<td>\n			<div id=\"receive_psid\" style=\"width:200px\">收货仓库</div>\n		</td>\n		<td>\n			<select id=\"status\" class=\"form-control\">\n				<option value=\"0\">货物状态</option>\n";
  stack1 = ((helper = (helper = helpers.product_state || (depth0 != null ? depth0.product_state : depth0)) != null ? helper : alias1),(options={"name":"product_state","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.product_state) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"stock_in_set\" class=\"form-control\">\n				<option value=\"0\">运费流程</option>\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : alias1),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.stock_in_set) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td colspan=\"2\">\n			<div id=\"create_account_id\" style=\"width:200px\">选择职员&nbsp;&nbsp;</div>\n		</td>\n	</tr>\n	<tr>\n		<td>\n			<select id=\"declaration\" class=\"form-control\">\n				<option value=\"0\">出口报关</option>\n";
  stack1 = ((helper = (helper = helpers.declaration || (depth0 != null ? depth0.declaration : depth0)) != null ? helper : alias1),(options={"name":"declaration","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.declaration) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"clearance\" class=\"form-control\">\n				<option value=\"0\">进口清关</option>\n";
  stack1 = ((helper = (helper = helpers.clearance || (depth0 != null ? depth0.clearance : depth0)) != null ? helper : alias1),(options={"name":"clearance","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.clearance) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"invoice\" class=\"form-control\">\n				<option value=\"0\">发票流程</option>\n";
  stack1 = ((helper = (helper = helpers.invoice || (depth0 != null ? depth0.invoice : depth0)) != null ? helper : alias1),(options={"name":"invoice","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.invoice) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select id=\"drawback\" class=\"form-control\">\n				<option value=\"0\">退税流程</option>\n";
  stack1 = ((helper = (helper = helpers.drawback || (depth0 != null ? depth0.drawback : depth0)) != null ? helper : alias1),(options={"name":"drawback","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.drawback) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</select>\n		</td>\n		<td>\n			<button id=\"filter\" class=\"buttons big\">filter</button>\n		</td>\n	</tr>\n</table>";
},"useData":true});
templates['followup'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<table>\n	<tr>\n		<td>\n			当前状态：\n		</td>\n		<td>\n			运输中\n		</td>\n		<td colspan=\"4\"></td>\n	</tr>\n	<tr>\n		<td>\n			跟进流程：\n		</td>\n		<td>\n			<select>\n				<option>货物状态</option>\n			</select>\n		</td>\n		\n		<td>\n			预计本阶段完成时间：\n		</td>\n		<td>\n			<input/>\n		</td>\n	</tr>\n	<tr>\n		<td >\n			备注\n		</td>\n		<td colspan=\"5\">\n			<textarea id=\"testID\" rows=\"5\" cols=\"60\" ></textarea>\n		</td>\n	</tr>\n</table>";
},"useData":true});
templates['search_transport'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<table width=\"100%\" height=\"61\" border=\"0\" cellpadding=\"0\"cellspacing=\"0\">\n	<tr>\n		<td width=\"30%\" style=\"padding-top: 3px;\">\n			<div id=\"easy_search_father\"> \n				<div id=\"easy_search\">\n					<img id=\"eso_search\" src=\"./imgs/easy_search.gif\" width=\"70\" height=\"29\" border=\"0\" />  \n				</div>\n			</div>\n				<table width=\"485\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td width=\"418\">\n							<div class=\"search_shadow_bg\">\n								<input name=\"search_key\" type=\"text\" class=\"search_input form-control\" style=\"font-size: 17px; font-family: Arial; color: #333333\" id=\"search_key\" />\n							</div>\n						</td>\n						<td width=\"67\"></td>\n					</tr>\n				</table> \n		</td>\n		<td width=\"33%\"></td>\n		<td width=\"24%\"></td>\n	</tr>\n</table>";
},"useData":true});
templates['transport_list'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "			<tr height=\"30px\" data-id=\""
    + alias2(alias1((depth0 != null ? depth0.transport_id : depth0), depth0))
    + "\">\n				<td>\n					"
    + alias2(alias1((depth0 != null ? depth0.transport_id : depth0), depth0))
    + "\n				</td>\n				<td>\n					"
    + alias2(alias1((depth0 != null ? depth0.transport_name : depth0), depth0))
    + "\n				</td>\n				<td>\n					"
    + alias2(alias1((depth0 != null ? depth0.transport_name : depth0), depth0))
    + "\n				</td>\n				<td>\n					"
    + alias2(alias1((depth0 != null ? depth0.transport_name : depth0), depth0))
    + "\n				</td>\n			</tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "<table id=\"dataTable\" width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"zebraTable\" isNeed=\"true\" isBottom=\"true\">\n	<thead id=\"tableHead\" width=\"100%\">\n		<tr>\n			<th width=\"25%\" nowrap=\"nowrap\" class=\"right-title\"\n						style=\"vertical-align: center; text-align: center;\">转运单基本信息</th>\n			<th width=\"27%\" nowrap=\"nowrap\" class=\"right-title\"\n						style=\"vertical-align: center; text-align: center;\">库房及运输信息</th>\n			<th width=\"18%\" nowrap=\"nowrap\" class=\"right-title\"\n						style=\"vertical-align: center; text-align: center;\">流程信息</th>\n			<th width=\"20%\" nowrap=\"nowrap\" class=\"right-title\"\n						style=\"vertical-align: center; text-align: center;\">跟进</th>\n		</tr>\n	</thead>\n	<tbody>\n";
  stack1 = ((helper = (helper = helpers.transport || (depth0 != null ? depth0.transport : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"transport","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers.transport) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "	</tbody>\n</table>			\n";
},"useData":true});
templates['work_transport'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "					<option value=\""
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=helpers.helperMissing, alias2="function", alias3=helpers.blockHelperMissing, buffer = 
  "<table id=\"worktable\" class=\"filter\" >\n	<tr>\n		<td>\n			<select class=\"status form-control\">\n				<option value=\"0\">货物状态</option>\n";
  stack1 = ((helper = (helper = helpers.product_state || (depth0 != null ? depth0.product_state : depth0)) != null ? helper : alias1),(options={"name":"product_state","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.product_state) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select class=\"stock_in_set form-control\">\n				<option value=\"0\">运费流程</option>\n";
  stack1 = ((helper = (helper = helpers.stock_in_set || (depth0 != null ? depth0.stock_in_set : depth0)) != null ? helper : alias1),(options={"name":"stock_in_set","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.stock_in_set) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td colspan=\"3\">\n			<div class=\"create_account_id\" style=\"width:200px\">选择职员&nbsp;&nbsp;</div>\n		</td>\n	</tr>\n	<tr>\n		<td>\n			<select class=\"qualityInspection form-control\">\n				<option value=\"0\">质检流程</option>\n";
  stack1 = ((helper = (helper = helpers.qualityInspection || (depth0 != null ? depth0.qualityInspection : depth0)) != null ? helper : alias1),(options={"name":"qualityInspection","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.qualityInspection) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select class=\"productFile form-control\">\n				<option value=\"0\">实物图片流程</option>\n";
  stack1 = ((helper = (helper = helpers.productFile || (depth0 != null ? depth0.productFile : depth0)) != null ? helper : alias1),(options={"name":"productFile","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.productFile) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select class=\"tag form-control\">\n				<option value=\"0\">内部标签流程</option>\n";
  stack1 = ((helper = (helper = helpers.tag || (depth0 != null ? depth0.tag : depth0)) != null ? helper : alias1),(options={"name":"tag","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.tag) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</select>\n		</td>\n		<td>\n			<select class=\"tagThird form-control\">\n				<option value=\"0\">第三方标签流程</option>\n";
  stack1 = ((helper = (helper = helpers.tagThird || (depth0 != null ? depth0.tagThird : depth0)) != null ? helper : alias1),(options={"name":"tagThird","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === alias2 ? helper.call(depth0,options) : helper));
  if (!helpers.tagThird) { stack1 = alias3.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</select>\n		</td>\n		<td>\n			<button id=\"worker\" class=\"buttons big\">filter</button>\n		</td>\n	</tr>\n</table>";
},"useData":true});
return templates;
});