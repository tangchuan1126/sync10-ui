"use strict";
define(["jquery","bootstrap","metisMenu","CompondList/View","./view_config","./outbound","artDialog","./jsontohtml_templates","./config"],
	function($,bootstrap,metisMenu,CompondList,view_config,Outbound,Dialog,templates,config) {
		return function(options) {
			var baseUrL = "/Sync10/administrator";
			var ListView = {
				initialize: function(options) {
					var list = new CompondList(view_config, {
						renderTo: options.el,
						dataUrl: options.url,
						pageSize: 9,
						PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
						//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
					});
					//绑定事件
					list.render();
					var listIds = [];
					window.updateItem = function(transport_id) {
						$.getJSON(config.getByIdForQuality.url+"?ids="+transport_id,{},function(d){
							//var updata = '{upitme:[{Root:"DATA",itmeid:'+transport_id+',itmekey:"TRANSPORT_ID"},{Root:"AUTHS",itmeid:'+transport_id+',itmekey:"ID"}]}';
							var itemlist = [];
							var item1 = {};
							item1.Root = "DATA";
							item1.itmeid = transport_id;
							item1.itmekey = "TRANSPORT_ID";
							itemlist.push(item1);
							var item2 = {};
							item2.Root = "AUTHS";
							item2.itmeid = transport_id;
							item2.itmekey = "ID";
							itemlist.push(item2);
							var updataObj = new Object();
							updataObj.upitme = itemlist;
							updataObj.DATA = d.DATA[0];
							updataObj.AUTHS = d.AUTHS[0];
							list.upSubitmeData(updataObj);
							templates.tbody.updatatr(d,transport_id,view_config.View_control);
						}).error(function(e){
							//alert(e);
						});
						list.Settopnvaheigth();
					};
					window.refreshWindow = function() {
						updateItem(listIds.pop());
					};
					window.clearWindow = function(id,ln) {
						if(listIds.length==ln) {
							if (ln>0 && listIds[ln-1] == id) {
								listIds.pop();
							}
						}
					};
					list.on("events.QUALITYINSPECTION",function(e) {
						//质检流程
						var transport_id = e.data.linedata.TRANSPORT_ID;
						listIds.push(transport_id);
						artDialog.open(baseUrL + "/transport/transport_quality_inspection.html?transport_id="+transport_id , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px',lock:true,opacity:0.3,fixed:true,close:function(){
								setTimeout("clearWindow("+transport_id+","+listIds.length+");", 300);
							}
						});
					});
					list.on("events.PRODUCTFILE",function(e) {
						//实物图片
						var transport_id = e.data.linedata.TRANSPORT_ID;
						listIds.push(transport_id);
						artDialog.open(baseUrL + "/transport/transport_product_file.html?transport_id="+transport_id , {title: "实物图片["+transport_id+"]",width:'850px',height:'600px', lock: true,opacity: 0.3,fixed: true,close:function(){
								setTimeout("clearWindow("+transport_id+","+listIds.length+");", 300);
							}
						});
					});
					list.on("events.TAG",function(e) {
						//跟进内部标签
						var transport_id = e.data.linedata.TRANSPORT_ID;
						listIds.push(transport_id);
						artDialog.open(baseUrL + "/transport/transport_tag.html?transport_id="+transport_id , {title: "跟进内部标签["+transport_id+"]",width:'600px',height:'300px',lock: true,opacity: 0.3,fixed: true,close:function(){
								setTimeout("clearWindow("+transport_id+","+listIds.length+");", 300);
							}
						});
					});
					list.on("events.TAGTHIRD",function(e) {
						//跟进第三方标签
						var transport_id = e.data.linedata.TRANSPORT_ID;
						listIds.push(transport_id);
						artDialog.open(baseUrL + "/transport/transport_third_tag.html?transport_id="+transport_id , {title: "跟进第三方标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true,close:function(){
								setTimeout("clearWindow("+transport_id+","+listIds.length+");", 300);
							}
						});
					});
					list.on("events.FOLLOWUP",function(e) {
						//跟进备货
						var transport_id = e.data.linedata.TRANSPORT_ID;
						listIds.push(transport_id);
						artDialog.open(baseUrL +"/transport/transport_follow_up.html?transport_id="+transport_id , {title: "转运单["+transport_id+"]跟进",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true,close:function(){
								setTimeout("clearWindow("+transport_id+","+listIds.length+");", 300);
							}
						});
					});
					list.on("events.moreLogs",function(e) {
						//自定义事件没有linedata
						var transport_id = e.data.TRANSPORT_ID;
						/*
						var d = new Dialog({
							url: baseUrL +"/transport/transport_logs.html?transport_id="+transport_id,
							//content:html,
							title: "日志 转运单号:["+transport_id+"]",width:'970px',height:'500px', lock: true, esc: true
						});
						d.showModal();
						*/
						artDialog.open(baseUrL +"/transport/transport_logs.html?transport_id="+transport_id, {title: '日志 转运单号:'+transport_id ,width:'970px',height:'500px', lock: true,opacity: 0.3});
					});
					list.on("transportDetail",function(e) {
						window.open(baseUrL +"/transport/transport_order_quality_detail.html?transport_id="+e.data.TRANSPORT_ID);
					});
					//构造一个视图
					this.view = list;
				},
				render:function(options) {
					//先清空之前的数据
					this.view.collection.remove();
					if(typeof options =="object") {
						//清空原来的页面
						$(options.el).empty();
						this.view.collection.el = options.el;
						this.view.collection.url = options.url;
						this.view.collection.PostData = (typeof options.queryCondition =="object" )?options.queryCondition:{};
					}
					this.view.render();
				},
			};
			ListView.initialize(options);
			return ListView.view;
		};
});
