define(["jquery","backbone","../../config"], function($,Backbone,config) {
	return Backbone.Model.extend({
		url: config.getByIdForQuality.url,
		initialize:function(options) {
			if (options) {
				if (options.id) {
					this.url = config.getByIdForQuality.url.replace("${id}",options.id);
				}
			}
		}
	});
});