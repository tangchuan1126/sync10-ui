define(["jquery","backbone","../../config","../../templates",
"../model/filterModel","../../transportListMain",
"../../key/TransportOrderKey",
"../../key/TransportStockInSetKey",
"../../key/DeclarationKey",
"../../key/ClearanceKey",
"../../key/InvoiceKey",
"../../key/DrawbackKey",
"../../key/TransportQualityInspectionKey",
"../../key/TransportProductFileKey",
"../../key/TransportTagKey",
"../../key/TransportThirdTagKey",
"dateUtil","oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"],
function($,Backbone,config,templates,FilterModel,TransportList,
TransportOrderKey,TransportStockInSetKey,DeclarationKey,ClearanceKey,InvoiceKey,DrawbackKey,
TransportQualityInspectionKey,TransportProductFileKey,TransportTagKey,TransportThirdTagKey,
DateUitl,AsynLoadQueryTree) {
	return Backbone.View.extend(
	{
			el:"#transport_filter",
			template:templates.work_transport,
			model:new FilterModel(),
			initialize:function(options)
			{
				this.resultView = options.resultView;
			},
			events:
			{
				"click #worker" : "worker"
			},
			queryOptions:
			{
				el: "#data",
				url: "/Sync10-ui/pages/temp/sendPS.json",
				queryCondition:{
				}
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({
					product_state: this.getSelect(TransportOrderKey,DateUitl.getLanguage(),[1,5,2,8,3,4,7]),//TransportOrderKey
					stock_in_set: this.getSelect(TransportStockInSetKey,DateUitl.getLanguage()),//
					declaration: this.getSelect(DeclarationKey,DateUitl.getLanguage()),
					clearance: this.getSelect(ClearanceKey,DateUitl.getLanguage()),
					invoice: this.getSelect(InvoiceKey,DateUitl.getLanguage()),
					drawback: this.getSelect(DrawbackKey,DateUitl.getLanguage()),
					qualityInspection: this.getSelect(TransportQualityInspectionKey,DateUitl.getLanguage()),
					productFile: this.getSelect(TransportProductFileKey,DateUitl.getLanguage()),
					tag: this.getSelect(TransportTagKey,DateUitl.getLanguage()),
					tagThird: this.getSelect(TransportThirdTagKey,DateUitl.getLanguage())
				}));
				var adminTree = new AsynLoadQueryTree({
					renderTo: "#worktable .create_account_id",
					dataUrl: config.adminTree.url,
					PostData: {rootType:"创建人"},
					scrollH: 400,
					Async: true,
					Parentclick: false,
					placeholder: "创建人"
				});
				adminTree.render();
			},
			getSelect: function(objlist,language,ul)
			{
				var list = [];
				if(ul) {
					try {
						for(var i=0;i<ul.length;i++) {
							var key = ul[i];
							var li = {};
							li.key = key;
							li.name = objlist[key][language];
							list.push(li);
						}
					} catch (e) {
						console.error(objlist);
						console.error(ul);
					}
					return list;
				}
				_.map(objlist,function(item,key) {
					var li = {};
					li.key = key;
					li.name = item[language];
					list.push(li);
				});
				return list;
			},
			worker:function() {
				//this.resultView.render(this.queryOptions);
				this.resultView.setQuery({
					status: $("#worktable .status").val(),
					stock_in_set: $("#worktable .stock_in_set").val(),
					create_account_id: $("#worktable .create_account_id").attr("data-val"),
					qualityInspection: $("#worktable .qualityInspection").val(),
					productFile: $("#worktable .productFile").val(),
					tag: $("#worktable .tag").val(),
					tagThird: $("#worktable .tagThird").val(),
					"dataUrl":config.workURl.url
				});
				//new TransportList(queryOptionsa);
			}
	});

}
);