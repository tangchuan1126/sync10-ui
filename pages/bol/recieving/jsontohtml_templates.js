define(["JSONTOHTML",
	"dateUtil",
	"./Map",
	"./KeyUtil",
	"../key/TransportOrderkey",
	"../key/ClearanceKey",
	"../key/DeclarationKey",
	"../key/TransportCertificateKey",
	"../key/TransportStockInSetKey"
	], function(JSONTOHTML,DateUtil,Map,Key,TransportOrderkey,ClearanceKey,DeclarationKey,TransportCertificateKey,TransportStockInSetKey) {

	//第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index)
					{
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}",
			style: function(obj, index) {
				if (index==3) {
					return "width:25%";
				} else if (index==2) {
					return "width:26%";
				} else{
					return "";
				}
			}
		}

	};

	var locallanguage = DateUtil.getLanguage();
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		//初始化，将json数据分类：分为对应的行和列
		AUTHS:null,
		int: function(jsons,View_control) 
		{
			//初始化全局配置
			_tbody.idAttribute = View_control.idAttribute;
			_tbody.authIdAttribute = View_control.authIdAttribute;
			_tbody.headallobj = View_control.head;	
			_tbody.footer = View_control.footer;

			var data = new Map(_tbody.idAttribute,jsons.DATA);
			var auths = new Map(_tbody.authIdAttribute,jsons.AUTHS);
			var pageSize=jsons.PAGECTRL.pageSize;

			//按表头转换成行和列
			var __tr=[];	
			for (var tri = 0; tri < jsons.DATA.length; tri++) 
			{
				var rowData = jsons.DATA[tri];
	            var __td = [];
				for (var headkey in _tbody.headallobj) 
				{
					var column = _tbody.headallobj[headkey];
					var columnFiled = column.field;
					var columnData = eval("({" + columnFiled + ":rowData})");
					__td.push(columnData);
				}

				__tr.push({"rowId":rowData[_tbody.idAttribute],"children": __td});

				if(tri >= pageSize)
				{
					break;
				}
			}

			//拼装tr,trfooter，
			var __rowsHtml = "";
			for (var trkey in __tr) 
			{
				var _row = __tr[trkey];
				var  rowId = _row.rowId;
				__rowsHtml += (JSONTOHTML.transform(_row, _tbody.tr));		
				var trfooterdata = {
					"colspan": _tbody.headallobj.length,
					"children": _tbody.footer,
					"rowId":rowId,
					"rowData":data.get(rowId),
					"auth":auths.get(rowId)
				};
				//将行值传给按钮行，用于判断按钮是否需要显示
				__rowsHtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
			}
			data = null;
			auths = null;
			return __rowsHtml;

		},
		keyname: function(obj) 
		{
			//列对象，只有一个key,那就是列名对应的字段
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			//行对象ID
			"id":function(obj)
			{
				return obj.rowId;
			},
			"data-itmeid":function(obj){
				return _tbody.idAttribute+"|"+obj.rowId;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {				
				//chuldren :列数组[]
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			"id":function(obj)
			{
				return "foot_"+obj.rowData[_tbody.idAttribute];
			},
			class: "TFOOTbutton",
			html: function(obj, index) {
				var rowData = obj.rowData,
					auth = obj.auth,
					tfootData = _tbody.idAttribute+"|"+obj.rowId,
					trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tfootData+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) 
				{
					var itmea = obj.children[akey];
					if(_utils.isHasButton(itmea[0],auth))
					{
						var name = itmea[1];
						if(itmea[0] =="events.followup")
						{
							name = itmea[1]+Key.get(TransportOrderkey,rowData.TRANSPORT_STATUS);
						}
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + name+ '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
	
		td: {
			tag: "td",
			class: function(obj, index) {

				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				if("basicInfo" == keynames)
				{
					classname = "td_" + "TRAILER";
				}
				else
				{
					classname = "td_" + keynames;
				}
				
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "inherit";
				}
				return valignsing;
			},
			html: function(obj, index) {

				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);

				if (keynames != "" && typeof keynames != "undefined")
				{
					if(typeof keynames == "string")
					{
						if (typeof cellTemplate[keynames] != "undefined" && cellTemplate[keynames] != "") 
						{
							retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
						}
					}
					else if(typeof keynames == "object")
					{
						retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
					}

				}
				return retuhtml;
			}
		},
		updateOneRow:function(parentId,rowData,auths)
		{
			var __td = [];
			for (var headkey in this.headallobj) 
			{
				var column = this.headallobj[headkey];
				var columnFiled = column.field;
				var columnData = eval("({" + columnFiled + ":rowData})");
				__td.push(columnData);
			}
			var trfooterdata = {
					"colspan": this.headallobj.length,
					"children": this.footer,
					"rowId":parentId,
					"rowData":rowData,
					"auth":auths
					};
			$("#foot_"+parentId).remove();
			$("#"+parentId).empty().html(JSONTOHTML.transform(__td,this.td)).after(JSONTOHTML.transform(trfooterdata,this.trfooter));
		}

	}


	var cellTemplate =
	{
		basicInfo:
		{
			tag: "fieldset",
			class: "TRAILER",
			children: [
			{
			  tag:"span",
			  class:"fieldset_tfoot",
			  html:function(obj,index)
			  {
			  	return "<em>总容器："+obj.CONTAINER_COUNT+"</em>";
			  }
			},
			{
				tag: 'legend',
				class: "td_legend",
				children:
				[
					{"tag":"span","class":"valuestyle buttonallevnts","data-eventname":"transportDetail","html":"T${TRANSPORT_ID}"},
					{"tag":"i","html":" "},
					{
						"tag":"span",
						"class":function(obj,index){return _utils._stateClass( obj.TRANSPORT_STATUS)},
						"html":function(obj,index){return TransportOrderkey[obj.TRANSPORT_STATUS][locallanguage];}
					}
				]
			}, 
			{
				 tag: "div",
				 class: "Tractor_ulitme",
				 children: 
				 [
					{tag: "ul",children:
					[
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"创建人:"},
									{"tag":"span","class":"valstyle","html":"${CREATE_ACCOUNT}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"TITLE:"},
									{"tag":"span","class":"valstyle","html":"${TITLE_NAME}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"允许装箱:"},
									{"tag":"span","class":"valstyle","html":"${CREATE_ACCOUNT}"}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"创建时间:"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.TRANSPORT_DATE);}}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"更新时间:"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.UPDATEDATE);}}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle spanBold","html":"ETA:"},
									{"tag":"span","class":"valstyle spanBold","html":function(obj,index){return DateUtil.formatDate(obj.TRANSPORT_RECEIVE_DATE);}}
								]
							}
					]}	
				 ]
			}
			]
		},
		transportInfo: 
		{
				 tag: "div",
				 //class: "Tractor_ulitme",
				 class: "Log_container",
				 children:
				 [
				 	{tag:"ul",children:
				 	[
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"收货仓库:"},
 							{"tag":"span","class":"valstyle","html":"${RECEIVE_PSNAME}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"提货仓库:"},
 							{"tag":"span","class":"valstyle","html":"${SEND_PSNAME}"}
				 		]},
				 		{tag:"div",class:"box1px",html:""},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"运单号:"},
 							{"tag":"span","class":"valstyle","html":"${DELIVER_ZIP_CODE}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"货运公司:"},
 							{"tag":"span","class":"valstyle","html":"${TRANSPORT_WAYBILL_NAME}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"承运公司:"},
 							{"tag":"span","class":"valstyle","html":"${CARRIERS}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"始发国/始发港:"},
 							{"tag":"span","class":"valstyle","html":function(obj,index){return _utils.handleEmpty(obj.TRANSPORT_SEND_COUNTRY_NAME,"无")+"/"+_utils.handleEmpty(obj.TRANSPORT_SEND_PLACE,"无");}}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"目的国/目的港:"},
 							{"tag":"span","class":"valstyle","html":function(obj,index){return _utils.handleEmpty(obj.TRANSPORT_RECEIVE_COUNTRY_NAME,"无")+"/"+_utils.handleEmpty(obj.TRANSPORT_RECEIVE_PLACE,"无");}}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"总体积:"},
 							{"tag":"span","class":"valstyle","html":"${TRANSPORT_VOLUME} ${TRANSPORT_VOLUME_UNIT}"}
				 		]},
				 		{tag:"li",children:
				 		[
				 			{"tag":"span","class":"namestyle","html":"总重量:"},
 							{"tag":"span","class":"valstyle","html":"${TRANSPORT_WEIGHT} ${TRANSPORT_WEIGHT_UNIT}"}
				 		]}
				 	]}
				 ]
		},
		FLOWS: 
		{
			tag: "div",
			class: "Log_container flows",
			html:function(obj,index)
			{
				var html = "" ;
				var box1px = "<div class='box1px'></div>";
				var length = obj.FLOWS.length;
				_.map(obj.FLOWS,function(flow,index)
					{
						if(flow.TYPE == "4")
						{
							var fontClass = _utils._flowClass(flow,obj);
							var  res = _utils._flowState(flow,obj);

							//设置显示状态
							flow.STATUS = "<font class='"+fontClass+"'  >"+res.STATUS+"</font>";
							flow.OVER=res.OVER;
							//设置完成天数
							html+= JSONTOHTML.transform(flow,cellTemplate._ulFlows);
							if(index != length - 1)
							{
								//html+= box1px;
							}
						}
						
					});
				return html;
			}
		},
		_ulFlows:
		{
			tag:"ul",
			class:"flows",
			children:
			[
				{tag:"li", children:
				[
					{tag:"span",class:"namestyle",html:"${TYPENAME}:"},
					{tag:"span",class:"valstyle",html:"${STATUS}"},
					{tag:"span",class:"rightstyle",html:"${OVER}"}
				]},
				{tag:"li", children:
				[
					{tag:"span",class:"flowname",html:"${EMPLOYE_NAMES}"}
				]}
			]
		},
		_getOverTime:function(flow,obj)
		{

		},
		_liList:
		{
			tag: "li",
			html: function(obj, index) 
			{
				return '<span class="namestyle">'+obj.TYPE+':</span><span class="valstyle">' + obj.EMPLOYE_NAMES + '</span>';
			}
		},
		LOGS: 
		{
			tag: "div",
			class: "Log_container",
			html: function(obj, index) 
			{

				var ulsring = "";
				var arryjson = obj.LOGS;
				var box1px = "<div class='box1px'></div>";
				for (var _key = 0; _key < arryjson.length; _key++)
				{


					var arryitmes = arryjson[_key];
					var ul = JSONTOHTML.transform(arryitmes,cellTemplate._ulLogs);  
					//var ul = '<ul><li><span class="logvaluestyle">' + arryitmes.FOLLOWUPTYPE + ':&nbsp;&nbsp;</span><span >' + arryitmes.ADMINNAME + '</span>&nbsp;&nbsp;<span>' + arryitmes.TRANSPORT_DATE + '</span></li><li><span>' + arryitmes.TRANSPORT_CONTENT + '</span></li>';

					//ul += '</ul>';

					if (ulsring != "" && typeof ulsring != "undefined") 
					{
						ulsring = ulsring + box1px + ul;
					}
					 else 
					{
						ulsring = ul;
					}

					if (_key == 2) 
					{
						ulsring = ulsring +'<span class="valuestyle buttonallevnts" data-eventname="events.moreLogs"><a class="more">更多</a></span>';
						break;
					}
				}

				return ulsring;
			}
		},
		_ulLogs:
		{
			tag:"ul",
			children:
			[
				{tag:"li",children:[
					{tag:"span",class:"logvaluestyle",html:"${FOLLOWUPTYPE}:&nbsp;&nbsp;"},
					{tag:"span",class:"",html:"${ADMINNAME}"},
					{tag:"span",class:"rightstyle",html:function(obj,index){return DateUtil.shortFormatDate(obj.TRANSPORT_DATE)}},
				]},
				{tag:"li",children:[
					{tag:"span",class:"",html:"${TRANSPORT_CONTENT}"}
				]}
			]
		},
	}

	var _utils = 
	{
		handleEmpty:function(str,defaultValue)
		{
			if(str == "" || str == undefined || str.length == 0 || str == "null" || str == null)
			{
				return defaultValue;
			}
			else
			{
				return str;
			}
		},
		getAuthRecord:function(id)
		{
			var auth ={};
			_.map(_tbody.AUTHS,function(authObj,key)
				{
					if(""+authObj.ID==id+"")
					{
						auth =authObj;
					}
			});
			return auth;
		},
		isHasButton:function(itmea,auth)
		{
			var res = false;
			if(itmea =="events.followup")
			{
				res = auth["BUTTON_FOLLOWUP"];
			}
			else if(itmea =="events.recievePicture")
			{
				res = auth["BUTTON_RECIEVEPICTURE"];
			}
			else if(itmea =="events.goodsArriveDelivery")
			{
				res = auth["BUTTON_GOODSARRIVEDELIVERY"];
			}
			else if(itmea =="events.bookDoorOrLocation")
			{
				res = auth["BUTTON_BOOKDOORORLOCATION"];
			}
			else if(itmea =="events.bookDoorOrLocationUpdateOrView")
			{
				res = auth["BUTTON_BOOKDOORORLOCATIONUPDATEORVIEW"];
			}
			else
			{
				res = false;
			}
			res = true;
			return res;
		},
		_stateClass:function(state)
		{
			//运输流程
			var tempclass="";
			if(state == "2")
			{
				tempclass=" colorbule";
			}
			else if(state =="3")
			{
				tempclass=" colorred";
			}
			else if (state =="5")
			{
				tempclass=" colorgreen";
			}
			else
			{
				tempclass="";
			}
			return tempclass;
		},
		//获取显示颜色
		_flowClass:function(flow,obj)
		{
			var type = flow.TYPE;
			var state = flow.STATUS
			var cls = "fontBold ";
			if(type == "5")
			{
				//运费 流程，判断 STOCK_IN_SET_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.STOCK_IN_SET_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "5")//运费完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "6")
			{
				//进口清关 CLEARANCE_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.CLEARANCE_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//清关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需清关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type =="7")
			{
				//出口报关DECLARATION_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.DECLARATION_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//报关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需报关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "9")
			{
				//单证 CERTIFICATE_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.CERTIFICATE_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//报关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需报关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "4")
			{
				cls =_utils._stateClass(state);
			}
			return cls;
		},
		//获取责任方
		_flowState:function(flow,obj)
		{
			var result = new Object();
			var calTime = function(over)
			{
				var res = "";
				if(over !="" && over != undefined && over != "undefined")
				{
					res = over+"天完成";
				}
				else
				{
					res = DateUtil.getDiffDate(obj.TRANSPORT_DATE,"dd")+"天";
				}
				
				return res;
			};
			var res ={"1":"(发货方)","2":"(收货方)"};
			switch(parseInt(flow.TYPE))
			{
				case 4://货物状态
					  result.OVER = calTime(obj.ALL_OVER);
					  result.STATUS = TransportOrderkey[flow.STATUS].zh;
					   break;
				case 5://运费
					 result.OVER = calTime(obj.STOCK_IN_SET_OVER);
					 result.STATUS = res[obj.STOCK_IN_SET_RESPONSIBLE]+TransportStockInSetKey[flow.STATUS].zh;
					 break;
				case 6://清关
					  result.OVER = calTime(obj.CLEARANCE_OVER);
					  result.STATUS= res[obj.CLEARANCE_RESPONSIBLE]+ClearanceKey[flow.STATUS].zh;
					 break;
				case 7://报关
					  result.OVER = calTime(obj.DECLARATION_OVER);
					  result.STATUS= res[obj.DECLARATION_RESPONSIBLE]+DeclarationKey[flow.STATUS].zh;
					 break;
				case 9://单证
					  result.OVER = calTime(obj.CERTIFICATE_OVER);
					  result.STATUS= res[obj.CERTIFICATE_RESPONSIBLE]+TransportCertificateKey[flow.STATUS].zh;
					 break;
				default:
					 result.OVER="0.0";
					 result.STATUS="";
			}
			return result;
		},
	}
	
	return {
		tbody: _tbody,
		table: _table
	}

});