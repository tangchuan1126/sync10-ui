define(["./jsontohtml_templates"], function(tmp) {

	return {
		"View_control": {
			"head": [{
				"title": "转运单基本信息",
				"field": "basicInfo"
			}, {
				"title": "库房及运输信息",
				"field": "transportInfo"
			}, {
				"title": "流程信息",
				"field": "FLOWS"
			}, {
				"title": "跟进",
				"field": "LOGS"
			}
			],
			"meta": {

				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"

			},
			"idAttribute":"TRANSPORT_ID",
			"authIdAttribute":"ID",
			"footer": [
				[
					"events.recievePicture", "收货图片"
				],
				[
					"events.followup", "跟进"
				],
				[
					"events.goodsArriveDelivery", "到货派送"
				],
				[
					"events.bookDoorOrLocation", "卸货司机签到"
				],
				[
					"events.bookDoorOrLocationUpdateOrView", "卸货司机已签到"
				]
			],
			"templates": tmp
		}
	}

});