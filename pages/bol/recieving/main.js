"use strict";
require(["../../../requirejs_config","./config"] ,function($,config)
	{
		require(["jquery", 'backbone','handlebars',
			"jqueryui/tabs","./js/view/searchView",
			"./js/view/filterView","./transportListMain",
			"auto"],
			function($,Backbone,Handlebars,ui,SearchView,FilterView,TransportList,auto)
		{
			 
			$.getJSON(config.session.url,{},function(data)
				{
					var psId= data.ps_id;
					if(psId != undefined && psId != "")
					{
						var queryOptions = {el: "#data", url:config.searchDefaultURL.url,queryCondition:{"receive_psid":psId}};
						var resultView  = new TransportList(queryOptions);
						resultView.on("events.Error", function(e)
						{
							$(".loding,.LodingModal").hide();
							alert("无法从服务器获取数据,请稍后在试。");
						});
						$("#tabs").tabs({active: 1}).show();
						new SearchView({"self_psId":psId,resultView:resultView}).render();
						auto.addAutoComplete($("#search_key"),config.autoComplete.url,"MERGE_FIELD","TRANSPORT_ID");
						new FilterView({"self_psId":psId,resultView:resultView}).render();
					}
					
				}).error(function(e) 
				{
					if(e.status =="401")
					{
						alert("您没有登录，或者登录信息失效,请登录后重试!");
					}
					else
					{
						alert("获取登录信息失败，请联系管理员!");
					}
	   				
				});

		});
	}
	);