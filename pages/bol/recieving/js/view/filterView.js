define(["jquery","backbone","../../config","../../templates",
"../model/filterModel","../../transportListMain",
"../../../key/TransportOrderKey","../../../key/TransportStockInSetKey","../../../key/DeclarationKey","../../../key/ClearanceKey","../../../key/InvoiceKey","../../../key/DrawbackKey",
"../../DateUtil","oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"],
function($,Backbone,config,templates,FilterModel,TransportList,
TransportOrderKey,TransportStockInSetKey,DeclarationKey,ClearanceKey,InvoiceKey,DrawbackKey,
DateUitl,AsynLoadQueryTree) {
	return Backbone.View.extend(
	{
			el:"#transport_filter",
			template:templates.filter_transport,
			model:new FilterModel(),
			initialize:function(options)
			{
				this.resultView = options.resultView;
				this.param = options;
			},
			events:
			{
				"click #filter" : "filter"
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({
					product_state: this.getSelect(TransportOrderKey,DateUitl.getLanguage(),[7,1,5,2,3]),//TransportOrderKey
					stock_in_set: this.getSelect(TransportStockInSetKey,DateUitl.getLanguage()),//
					declaration: this.getSelect(DeclarationKey,DateUitl.getLanguage()),
					clearance: this.getSelect(ClearanceKey,DateUitl.getLanguage()),
					invoice: this.getSelect(InvoiceKey,DateUitl.getLanguage()),
					drawback: this.getSelect(DrawbackKey,DateUitl.getLanguage())
				}));
				var sendPsTree = new AsynLoadQueryTree({
					renderTo: "#send_psid",
					dataUrl: config.storageTree.url,
					scrollH:400,
					Async:true,
					placeholder:"发货仓库"
				});
				sendPsTree.render();
				var receivePsTree = new AsynLoadQueryTree({
					renderTo: "#receive_psid",
					selectid: {value:this.param.self_psId},
					dataUrl: config.storageTree.url,
					scrollH:400,
					Async:true,
					placeholder:"收货仓库"
				});
				receivePsTree.render();
				var adminTree = new AsynLoadQueryTree({
					renderTo: "#create_account_id",
					dataUrl: config.adminTree.url,
					scrollH:400,
					Async:true,
					placeholder:"创建人"
				});
				adminTree.render();
				
			},
			getSelect: function(objlist,language,ul)
			{
				var list = [];
				if(ul) {
					for(var i=0;i<ul.length;i++) {
						var key = ul[i];
						var li = {};
						li.key = key;
						li.name = objlist[key][language];
						list.push(li);
					}
					return list;
				}
				_.map(objlist,function(item,key) {
					var li = {};
					li.key = key;
					li.name = item[language];
					list.push(li);
				});
				return list;
			},
			filter:function()
			{
				this.resultView.setQuery({
					send_psid: $("#send_psid").attr("data-val"),
					receive_psid: $("#receive_psid").attr("data-val"),
					status: $("#status").val(),
					stock_in_set: $("#stock_in_set").val(),
					create_account_id: $("#create_account_id").attr("data-val"),
					declaration: $("#declaration").val(),
					clearance: $("#clearance").val(),
					invoice: $("#invoice").val(),
					drawback: $("#drawback").val(),
					"dataUrl":config.filterRecieving.url
				});
			}
	});

}
);