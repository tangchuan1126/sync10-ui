"use strict";
define(["jquery","backbone",'handlebars',"../../templates","../../config","../../transportListMain"],
	function($,Backbone,HandleBars,templates,config,TransportList)
	{
		return Backbone.View.extend(
		{
			el:"#transport_search",
			template:templates.search_transport,
			initialize:function(opts)
			{
				this.resultView = opts.resultView
			},
			render:function() {
				var temp = this;
				//console.log(temp.template({}));
				temp.$el.html(temp.template({}));
				//contextmenu=function(event) {
				$("#eso_search").bind("mouseup",function(evt) {
					if (evt.button==2) {
						//evt.preventDefault();
						temp.searchRight();
					}
				});
				$("#eso_search")[0].oncontextmenu = new Function("return false;");
				//注册键盘事件事件
				//window.document.oncontextmenu = new Function("return false;");
				$("#search_key").bind("keydown",function(evt)
				{
					if(evt.keyCode==13)
					{
						temp.searchLeft();
					}
				});
				return this;
			},
			events:
			{
				"click #eso_search":"searchLeft"
			},
			searchRight:function(evt)
			{
				var val = $("#search_key").val().replace(/\'/g,'');
				if (val=="") {
					alert("你好像忘记填写关键词了？");
				} else {
					$("#search_key").val(val);
					this.resultView.setQuery({"key":val,"search_mode":2,"dataUrl":config.keySearchURL.url});
				}
			},
			searchLeft:function(evt)
			{
				var val = $("#search_key").val().replace(/\'/g,'');
				if (val=="")
				{
					alert("你好像忘记填写关键词了？");
				}
				else
				{
					val = "\'"+val.toLowerCase()+"\'";
					$("#search_key").val(val);
					this.resultView.setQuery({"key":val,"search_mode":1,"dataUrl":config.keySearchURL.url});
				}
			}
		});
	}
);