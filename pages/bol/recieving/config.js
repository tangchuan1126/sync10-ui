define({
	filterRecieving:
	{
		//url:"/Sync10-ui/pages/temp/transport_manage.json"
		url: "/Sync10/_b2b/transportOrder/filterDefaultForReceive"
	},
	searchDefaultURL:
	{
		//url:"/Sync10-ui/pages/temp/transport_manage.json"
		url: "/Sync10/_b2b/transportOrder/filterForReceive"
	},
	keySearchURL: {
		url:"/Sync10/_b2b/transportOrder/filterDefaultForReceive"
	},
	autoComplete: 
	{
		url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
	},
	storageTree: 
	{
		url:"/Sync10/_b2b/storage/"
	},
	adminTree:
	 {
		url:"/Sync10/_b2b/admin/"
	},
	session:
	{
		url:"/Sync10/_b2b/admin/session"
	}
});