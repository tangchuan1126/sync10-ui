"use strict";
define(
	["jquery","bootstrap","metisMenu","CompondList/View","./view_config","./outbound","./jsontohtml_templates","artDialog"],
	function($,bootstrap,metisMenu,CompondList,view_config,Outbound,Template)
	{
		return  function(options)
		{
		
					 var list  = new CompondList(view_config, {
	  		   		 renderTo: options.el,
	    		 	 dataUrl: options.url,
	    		 	 PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
	    		 	 //dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
	  				  });
					 //绑定事件

		   			 list.render();   

		   			// console.log(window.top);
		   			//收货图片
					list.on("events.recievePicture",function(e)
					{
						var obj = Outbound.showPictrueOnline('39',e.data.linedata.TRANSPORT_ID,"",0,"transport");
						if(!obj) {return;}
						if(window.top && window.top.openPictureOnlineShow)
						{
							window.top.openPictureOnlineShow(obj);
						}
						else
						{
							var param = jQuery.param(obj);
							var uri = "/Sync10/administrator/file/picture_online_show.html?"+param;
							artDialog.open(uri,{title: 'photo', width: 1100, height: 600, lock: true, esc: true});
						}
					});

					//到货派送
					list.on("events.goodsArriveDelivery",function(e)
					{
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var url = '/Sync10/administrator/transport/transport_goods_arrive_delivery.html?transport_id='+transport_id;
						artDialog.open(url, {title: '到货派送['+transport_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3,
							close:function(){renderOneRow(list,transport_id);}
						});

					//alert(e.data.linedata.TRANSPORT_ID);
					});
					//卸货司机签到
					list.on("events.bookDoorOrLocation",function(e)
					{
						var rel_occupancy_use = "2";
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var uri = '/Sync10/administrator/transport/book_door_or_location.html?transport_id='+transport_id+'&rel_occupancy_use='+rel_occupancy_use; 
						artDialog.open(uri , {title: "转运单["+transport_id+"]司机签到",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true,
							close:function(){renderOneRow(list,transport_id);}});
						});
					list.on("events.bookDoorOrLocationUpdateOrView",function(e)
					{
						var rel_occupancy_use = "2";
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var uri = '/Sync10/administrator/transport/book_door_or_location_update_or_view.html?transport_id='+transport_id+'&rel_occupancy_use='+rel_occupancy_use; 
						$.artDialog.open(uri , {title: "转运单["+transport_id+"]司机已签到",width:'800px',height:'450px', lock: true,opacity: 0.3,fixed: true,
							close:function(){renderOneRow(list,transport_id);}
						});
					});
				
					list.on("transportDetail",function(e)
					{
							var transportId= e.data.TRANSPORT_ID;
							window.open("/Sync10/administrator/transport/transport_order_in_detail.html?transport_id="+transportId);
					});
					list.on("events.moreLogs",function(e)
					{
						var transport_id = e.data.TRANSPORT_ID;
						var uri ="/Sync10/administrator/transport/transport_logs.html?transport_id="+transport_id;
						artDialog.open(uri, {title: '日志 转运单号:'+transport_id,width:'970px',height:'500px', lock: true,opacity: 0.3});
					});

					list.on("events.followup",function(e)
					{
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var uri ="/Sync10/administrator/transport/transport_follow_up.html?transport_id="+transport_id;
	 					artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true,
	 						close:function(){renderOneRow(list,transport_id);}
	 					});
					});

					function renderOneRow(olist,transport_id)
					{
						$.getJSON('/Sync10/_b2b/transportOrder/filterByIdsForManage?ids='+transport_id, {},
						 	function(json, textStatus) 
						 	{
						 		if(textStatus == "success")
						 		{
						 			olist.upSubitmeData({
										upitme:[{Root:"DATA",itmeid:transport_id,itmekey:"TRANSPORT_ID"},
												{Root:"AUTHS",itmeid:transport_id,itmekey:"ID"}],
			  							DATA:json.DATA[0],
			  							AUTHS:json.AUTHS[0]});
									Template.tbody.updateOneRow(transport_id,json.DATA[0],json.AUTHS[0]);
						 		}
						});
					}
			return list;
		};
});
