define(["./DateUtil"], function(DateUtil) 
{
	var key =
	{
		locallanguage: DateUtil.getLanguage(),
		get:function(key,keyId)
		{
			var result ="没有找到对应的KEY";
			if(key[keyId] )
			{
				result = key[keyId][this.locallanguage];
				if(result == undefined || result =="" )
				{
					result="没有找到对应的KEY";
				}
			}
			return result;
		}
	}
	return key;
});