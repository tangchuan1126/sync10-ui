"use strict";
require(["../../../requirejs_config","./config"] ,function($,config) {
	require(["jquery",'backbone','handlebars',"art_Dialog/dialog","./js/view/searchView","./js/view/filterTab","./transportListMain","auto","domready!","bootstrap"],
		function($,Backbone,Handlebars,Dialog,SearchView,filterTab,TransportList,auto) {
			function renderMe(d) {
				var queryOptions = {el: "#data", url:config.searchDefaultURl.url,queryCondition:{send_psid:d.ps_id}};
				var resultView = new TransportList(queryOptions);
				resultView.on("events.Error", function(e){
					//$("#tabs").hide();
					$(".loding,.LodingModal").hide();
					var aretr = Dialog({
						title: '服务端状态',
						width: 150,
						content: '请求出错！'
					});
					aretr.showModal();
				});
				//resultView.render();
				//$("#tabs").tabs({active: 1}).show();
				$("#tabs").show();
				var t = new SearchView({el:"#transport_search",resultView:resultView});
				t.render();
				//auto.addAutoComplete($("#search_key"),config.autoComplete.url,"MERGE_FIELD","TRANSPORT_ID");
				new filterTab({resultView:resultView,psId:d.ps_id}).render();
				$("#pickuplist").bind('click', {}, function(event) {
					var ids = [];
					$("#data").find("input[class='check-item']:checked").each(function(){
						ids.push($(this).val());
					});
					if (ids.length>0) {
						alert("调用批量打印拣货单:"+ids);
					} else {
						alert("选择bol");
					}
				});
				$("#shiplable").bind('click', {}, function(event) {
					var ids = [];
					$("#data").find("input[class='check-item']:checked").each(function(){
						ids.push($(this).val());
					});
					if (ids.length>0) {
						alert("调用批量打印SHIPPING LABLE:"+ids);
					} else {
						alert("选择bol");
					}
				});
				
			}
		function now() {
			return new Date();
		}
		$.ajax({
			type: "GET",
			url: config.adminSesion.url,
			dataType: "json",
			timeout: 30000,
			error: function(XHR,textStatus,errorThrown) {
				if (textStatus==401) {
					var aretr = Dialog({
						title: '错误',
						width: 150,
						content: '请确认已经登陆系统！'
					});
					aretr.showModal();
				} else {
					var aretr = Dialog({
						title: '服务端状态',
						width: 150,
						content: '请求出错！'
					});
					aretr.showModal();
				}
			},
			success: function(data,textStatus) {
				if (textStatus=="success") {
					renderMe(data);
				}
			},
			headers: {
				"X-HTTP-Method-Override": "GET"
			}
		});
	});
});