define(["jquery","backbone","../../config","../../templates","../model/filterModel",
"../../key/TransportOrderKey","immybox","dateUtil","oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"],
	function($,Backbone,config,templates,FilterModel,TransportOrderKey,immybox,DateUitl,AsynLoadQueryTree) {
	return Backbone.View.extend(
	{
			el:"#transport_filter",
			template:templates.filter_tab,
			model:new FilterModel(),
			psId: "",
			initialize:function(options)
			{
				this.resultView = options.resultView;
				this.psId = options.psId;
			},
			events:
			{
				"click #filter" : "filter"
			},
			queryOptions:
			{
				el: "#data",
				url: "/Sync10-ui/pages/temp/sendPS.json",
				queryCondition:{
					send_psid: $("#send_psid").val(),
					receive_psid: $("#receive_psid").val(),
					status: $("#status").val(),
					//stock_in_set: $("#stock_in_set").val(),
					create_account_id: $("#create_account_id").val()
					//declaration: $("#declaration").val(),
					//clearance: $("#clearance").val(),
					//invoice: $("#invoice").val(),
					//drawback: $("#drawback").val()
				}
			},
			render:function() 
			{
				var v = this;
				v.$el.html(v.template({
					//send_ps:response.toJSON().send_ps,
					//recive_ps:response.toJSON().recive_ps
					/*
					product_state: this.getSelect(TransportOrderKey,DateUitl.getLanguage(),[7,1,5,2,3]),//TransportOrderKey
					stock_in_set: this.getSelect(TransportStockInSetKey,DateUitl.getLanguage()),//
					declaration: this.getSelect(DeclarationKey,DateUitl.getLanguage()),
					clearance: this.getSelect(ClearanceKey,DateUitl.getLanguage()),
					invoice: this.getSelect(InvoiceKey,DateUitl.getLanguage()),
					drawback: this.getSelect(DrawbackKey,DateUitl.getLanguage())
					*/
				}));
				var sendPsTree = new AsynLoadQueryTree({
					renderTo: "#send_psid",
					selectid: {value: v.psId},
					PostData: {rootType:"发货仓库"},
					dataUrl: config.storageTree.url,
					scrollH: 400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder:"发货仓库"
				});
				sendPsTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				sendPsTree.render();
				var receivePsTree = new AsynLoadQueryTree({
					renderTo: "#receive_psid",
					//selectid: {value:"100006"},
					PostData: {rootType:"收货仓库"},
					dataUrl: config.storageTree.url,
					scrollH:400,
					multiselect: false,
					Async: true,
					Pleaseselect: "Clean Select",
					placeholder:"收货仓库"
				});
				receivePsTree.on("Pleaseselect",function(jqinput){
					jqinput.val("");
				});
				receivePsTree.render();
				$('#status').immybox({
					Pleaseselect:'Clean Select',
					choices: v.getSelect(TransportOrderKey,DateUitl.getLanguage(),[1,2,3,4,5,6,7,8])
				});
				var adminTree = new AsynLoadQueryTree({
					renderTo: "#create_account_id",
					dataUrl: config.adminTree.url,
					scrollH: 400,
					PostData: {rootType:"创建人"},
					multiselect: false,
					Async: true,
					Parentclick: false,
					placeholder: "创建人"
				});
				adminTree.render();
				return this;
			},
			getSelect: function(objlist,language,ul) {
				var list = [];
				if(ul) {
					for(var i=0;i<ul.length;i++) {
						var key = ul[i];
						var li = {};
						li.value = key;
						li.text = objlist[key][language];
						list.push(li);
					}
					return list;
				}
				_.map(objlist,function(item,key) {
					var li = {};
					li.value = key;
					li.text = item[language];
					list.push(li);
				});
				return list;
			},
			/*
			getSelect: function(objlist,language,ul)
			{
				var list = [];
				if(ul) {
					for(var i=0;i<ul.length;i++) {
						var key = ul[i];
						var li = {};
						li.key = key;
						li.name = objlist[key][language];
						list.push(li);
					}
					return list;
				}
				_.map(objlist,function(item,key) {
					var li = {};
					li.key = key;
					li.name = item[language];
					list.push(li);
				});
				return list;
			},
			*/
			filter:function()
			{
				//this.resultView.render(this.queryOptions);
				this.resultView.setQuery({
					send_psid: $("#send_psid").attr("data-val"),
					receive_psid: $("#receive_psid").attr("data-val"),
					status: $("#status").attr("data-value"),
					create_account_id: $("#create_account_id").attr("data-val"),
					/*
					stock_in_set: $("#stock_in_set").val(),
					declaration: $("#declaration").val(),
					clearance: $("#clearance").val(),
					invoice: $("#invoice").val(),
					drawback: $("#drawback").val(),
					*/
					"dataUrl":config.searchURl.url
				});
				//new TransportList(queryOptionsa);
			}
	});
});