"use strict";
define(["jquery","backbone",'handlebars',"../../templates","../../config", "auto","../../transportListMain"],
	function($,Backbone,HandleBars,templates,config,auto,TransportList)
	{
		return Backbone.View.extend(
		{
			template:templates.search_transport,
			queryOptions:
			{
				el:"#data",
				url:config.searchURl.url,
				queryCondition:{}
			},
			initialize:function(opts)
			{
				this.setElement(opts.el);
				this.searchUrl = opts.searchUrl;
				this.resultView = opts.resultView
			},
			render:function() {
				var temp = this;
				temp.$el.html(temp.template({}));
				//contextmenu=function(event) {
				$("#eso_search").bind("mouseup",function(evt) {
					if (evt.button==2) {
						//evt.preventDefault();
						temp.searchRight();
					}
				});
				$("#eso_search")[0].oncontextmenu = new Function("return false;");
				//注册键盘事件事件
				//window.document.oncontextmenu = new Function("return false;");
				$("#search_key").bind("keydown",function(evt)
				{
					if(evt.keyCode==13)
					{
						temp.searchLeft();
					}
				});
				return this;
			},
			events:
			{
				"click #eso_search":"searchLeft"
			},
			searchRight:function(evt)
			{
				var val = $("#search_key").val().replace(/\'/g,'');
				if (val=="") {
					alert("你好像忘记填写关键词了？");
				} else {
					$("#search_key").val(val);
					this.resultView.setQuery({"key":val,"search_mode":2,"dataUrl":config.keySearchURl.url});
				}
			},
			searchLeft:function(evt)
			{
				var val = $("#search_key").val().replace(/\'/g,'');
				if (val=="")
				{
					alert("你好像忘记填写关键词了？");
				}
				else
				{
					val = "\'"+val.toLowerCase()+"\'";
					$("#search_key").val(val);
					//var queryOptions = {el: "#data", url: "/Sync10-ui/pages/temp/sendPS.json",queryCondition:{}};
					this.resultView.setQuery({"key":val,"search_mode":1,"dataUrl":config.keySearchURl.url});
					//this.resultView.render(this.queryOptions);
					//new TransportList(this.queryOptions);
				}
			}
		});
	}
);