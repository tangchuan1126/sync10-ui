define(["jquery","backbone","./transportModel"], function($,Backbone,TransportModel) {
	return Backbone.Collection.extend({
		Model: TransportModel,
		initialize:function(url) {
			this.url = url;
		},
		parse:function(response) {
			return response.transport;
		}
	});
});