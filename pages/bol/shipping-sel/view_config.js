define(["./jsontohtml_templates"], function(tmp) {
	return {
		"View_control": {
			"head": [
			{
				"title": "&nbsp;",
				"field": {"name":"checkItem"}
			}, {
				"title": "Base Info",
				"field": {"name":"basicInfo"}
			}, {
				"title": "Transport Info",
				"field": {"name":"transportInfo"}
			}, {
				"title": "Flow Info",
				"field": "FLOWS"
			}, {
				"title": "Log Info",
				"field": "LOGS"
			}
			],
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"footer": [
				[
					"events.PRINT1", "Print SHIPPING LABLE"
				],
				[
					"events.PRINT2", "Pick Task"
				],
				/*
				[
					"events.OUTBOUND", "装货图片"
				],
				*/
				[
					"events.FOLLOWUP", "note"
				]
				/*,
				[
					"events.BOOKDOORORLOCATION", "装货司机签到"
				],
				[
					"events.BOOKDOORORLOCATIONUPDATEORVIEW", "装货司机已签到"
				]
				*/
			],
			"templates": tmp
		}
	}
});