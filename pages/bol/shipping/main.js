"use strict";
require(["../../../requirejs_config","./config"] ,function($,config) {
	require(["jquery",'backbone','handlebars',"jqueryui/tabs", "art_Dialog/dialog",
		"./js/view/searchView","./js/view/filterView","./transportListMain","auto","domready!"],
		function($,Backbone,Handlebars,ui,Dialog,SearchView,FilterView,TransportList,auto) {
			function renderMe(d) {
				var queryOptions = {el: "#data", url:config.searchDefaultURl.url,queryCondition:{send_psid:d.ps_id}};
				var resultView = new TransportList(queryOptions);
				resultView.on("events.Error", function(e){
					//$("#tabs").hide();
					$(".loding,.LodingModal").hide();
					var aretr = Dialog({
						title: '服务端状态',
						width: 150,
						content: '请求出错！'
					});
					aretr.showModal();
				});
				$("#tabs").tabs({active: 1}).show();
				var t = new SearchView({el:"#transport_search",resultView:resultView});
				t.render();
				auto.addAutoComplete($("#search_key"),config.autoComplete.url,"MERGE_FIELD","TRANSPORT_ID");
				var temp = new FilterView({resultView:resultView,psId:d.ps_id});
				temp.render();
			}
			function now() {
				return new Date();
			}
			$.ajax({
				type: "GET",
				url: config.adminSesion.url,
				dataType: "json",
				timeout: 30000,
				error: function(XHR,textStatus,errorThrown) {
					if (textStatus==401) {
						var aretr = Dialog({
							title: '错误',
							width: 150,
							content: '请确认已经登陆系统！'
						});
						aretr.showModal();
					} else {
						var aretr = Dialog({
							title: '服务端状态',
							width: 150,
							content: '请求出错！'
						});
						aretr.showModal();
					}
				},
				success: function(data,textStatus) {
					if (textStatus=="success") {
						renderMe(data);
					}
				},
				headers: {
					"X-HTTP-Method-Override": "GET"
				}
			});
	});
});