define({
	Sync10UIPage: {
		url: "/Sync10-ui/pages"
	},
	Sync10Page: {
		url: "/Sync10/administrator"
	},
	keySearchURl: {
		url: "/Sync10/_b2b/transportOrderIndex/searchForSend"
	},
	searchURl: {
		url:"/Sync10/_b2b/transportOrder/filterDefaultForSend"
	},
	searchDefaultURl: {
		url:"/Sync10/_b2b/transportOrder/filterForSend"
	},
	getByIdForSend: {
		url:"/Sync10/_b2b/transportOrder/filterByIdsForSend"
	},
	autoComplete: {
		url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
	},
	storageTree: {
		url:"/Sync10/_b2b/storage/"
	},
	adminTree: {
		url:"/Sync10/_b2b/admin/"
	},
	
	adminSesion: {
		url:"/Sync10/_b2b/admin/session"
	}
});