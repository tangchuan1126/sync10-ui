"use strict";
define(["jquery","underscore","bootstrap","metisMenu","CompondList/View","./view_config","./outbound","artDialog","./jsontohtml_templates","./config"],
	function($,_,bootstrap,metisMenu,CompondList,view_config,Outbound,Dialog,templates,config) {
		return function(options) {
			var baseUrL = "/Sync10/administrator";
			var ListView = {
				initialize: function(options) {
					var list = new CompondList(view_config, {
						renderTo: options.el,
						dataUrl: options.url,
						pageSize: 9,
						PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
						//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
					});
					//绑定事件
					list.render();
					list.on("events.OUTBOUND",function(e) {
						//发货图片
						var obj = Outbound.showPictrueOnline('40',e.data.linedata.TRANSPORT_ID,"",0,"transport");
						if(!obj) {return;}
						if(window.top && window.top.openPictureOnlineShow){
							window.top.openPictureOnlineShow(obj);
						} else {
							var param = jQuery.param(obj);
							//var uri = baseUrL +"/file/picture_online_show.html?"+param;
							artDialog.open(baseUrL +"/file/picture_online_show.html?"+param , {title: '图片预览',width:'1100px',height:'600px',lock:true,opacity:0.3,fixed: true});
						}
					});
					//跟进备货
					list.on("events.FOLLOWUP",function(e) {
						var transport_id = e.data.linedata.TRANSPORT_ID;
						var uri = baseUrL +"/transport/transport_follow_up.html?transport_id="+transport_id;
						window.refreshWindow = null;
						window.refreshWindow = function() {
							$.getJSON(config.getByIdForSend.url+"?ids="+transport_id,{},function(d){
								//var updata = '{upitme:[{Root:"DATA",itmeid:'+transport_id+',itmekey:"TRANSPORT_ID"},{Root:"AUTHS",itmeid:'+transport_id+',itmekey:"ID"}]}';
								var itemlist = [];
								var item1 = {};
								item1.Root = "DATA";
								item1.itmeid = transport_id;
								item1.itmekey = "TRANSPORT_ID";
								itemlist.push(item1);
								var item2 = {};
								item2.Root = "AUTHS";
								item2.itmeid = transport_id;
								item2.itmekey = "ID";
								itemlist.push(item2);
								var updataObj = new Object();
								updataObj.upitme = itemlist;
								updataObj.DATA = d.DATA[0];
								updataObj.AUTHS = d.AUTHS[0];
								list.upSubitmeData(updataObj);
								templates.tbody.updatatr(d,transport_id,view_config.View_control);
							}).error(function(e){
								//alert(e);
							});
							list.Settopnvaheigth();
						}
						artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
					});
					list.on("events.BOOKDOORORLOCATION",function(e) {
						//司机签到
						var transport_id = e.data.linedata.TRANSPORT_ID;
						artDialog.open(baseUrL +"/transport/book_door_or_location.html?transport_id="+transport_id+"&rel_occupancy_use=1" , {id:"1_"+transport_id,title: "转运单["+transport_id+"]司机签到",width:'800px',height:'750px', lock: true,opacity: 0.3,fixed: true});
						var iframeobj = $("iframe[name='Open1_"+transport_id+"']")[0];
						var iframeabcd = iframeobj.contentWindow;
						function iframeload() {
							iframeabcd.windowClose = function() {
								window.top.artDialog.list["1_"+transport_id].close();
								$.getJSON(config.getByIdForSend.url+"?ids="+transport_id,{},function(d){
									//var updata = '{upitme:[{Root:"DATA",itmeid:'+transport_id+',itmekey:"TRANSPORT_ID"},{Root:"AUTHS",itmeid:'+transport_id+',itmekey:"ID"}]}';
									var itemlist = [];
									var item1 = {};
									item1.Root = "DATA";
									item1.itmeid = transport_id;
									item1.itmekey = "TRANSPORT_ID";
									itemlist.push(item1);
									var item2 = {};
									item2.Root = "AUTHS";
									item2.itmeid = transport_id;
									item2.itmekey = "ID";
									itemlist.push(item2);
									var updataObj = new Object();
									updataObj.upitme = itemlist;
									updataObj.DATA = d.DATA[0];
									updataObj.AUTHS = d.AUTHS[0];
									list.upSubitmeData(updataObj);
									templates.tbody.updatatr(d,transport_id,view_config.View_control);
								}).error(function(e){
									//alert(e);
								});
								list.Settopnvaheigth();
							}
						}
						if (iframeobj.attachEvent) {//IE
							iframeobj.attachEvent('onload', iframeload);
						} else {//nonIE
							iframeobj.onload = iframeload;
						}
					});
					list.on("events.BOOKDOORORLOCATIONUPDATEORVIEW",function(e) {
						//司机签到
						var transport_id = e.data.linedata.TRANSPORT_ID;
						artDialog.open(baseUrL +"/transport/book_door_or_location_update_or_view.html?transport_id="+transport_id+"&rel_occupancy_use=1" , {id:"2_"+transport_id,title: "转运单["+transport_id+"]司机已签到",width:'800px',height:'450px', lock: true,opacity: 0.3,fixed: true});
						var iframeobj = $("iframe[name='Open2_"+transport_id+"']")[0];
						var iframeabcd = iframeobj.contentWindow;
						function iframeload2() {
							iframeabcd.refreshWindow = function() {
								//window.top.artDialog.list["2_"+transport_id].close();
								iframeabcd.location.reload();
								$.getJSON(config.getByIdForSend.url+"?ids="+transport_id,{},function(d){
									var itemlist = [];
									var item1 = {};
									item1.Root = "DATA";
									item1.itmeid = transport_id;
									item1.itmekey = "TRANSPORT_ID";
									itemlist.push(item1);
									var item2 = {};
									item2.Root = "AUTHS";
									item2.itmeid = transport_id;
									item2.itmekey = "ID";
									itemlist.push(item2);
									var updataObj = new Object();
									updataObj.upitme = itemlist;
									updataObj.DATA = d.DATA[0];
									updataObj.AUTHS = d.AUTHS[0];
									list.upSubitmeData(updataObj);
									templates.tbody.updatatr(d,transport_id,view_config.View_control);
								}).error(function(e){
									//alert(e);
								});
								list.Settopnvaheigth();
							}
						}
						if (iframeobj.attachEvent) {//IE
							iframeobj.attachEvent('onload', iframeload2);
						} else {//nonIE
							iframeobj.onload = iframeload2;
						}
					});
					list.on("events.moreLogs",function(e) {
						//自定义事件没有linedata
						var transport_id = e.data.TRANSPORT_ID;
						artDialog.open(baseUrL +"/transport/transport_logs.html?transport_id="+transport_id, {title: '日志 转运单号:'+transport_id,width:'970px',height:'500px', lock: true,opacity: 0.3});
					
					});
					list.on("transportDetail",function(e) {
						window.open(baseUrL +"/transport/transport_order_out_detail.html?transport_id="+e.data.TRANSPORT_ID);
					});
					//构造一个视图
					this.view = list;
				},
				render:function(options) {
					//先清空之前的数据
					this.view.collection.remove();
					if(typeof options =="object") {
						//清空原来的页面
						$(options.el).empty();
						this.view.collection.el = options.el;
						this.view.collection.url = options.url;
						this.view.collection.PostData = (typeof options.queryCondition =="object" )?options.queryCondition:{};
					}
					this.view.render();
				},
			};
			ListView.initialize(options);
			return ListView.view;
		};
});
