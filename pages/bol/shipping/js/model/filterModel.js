define(["jquery","backbone","../../config"], function($,Backbone,config) {
	return Backbone.Model.extend({
		url: config.getByIdForSend.url,
		initialize:function(options) {
			if (options) {
				if (options.id) {
					this.url = config.getByIdForSend.url.replace("${id}",options.id);
				}
			}
		}
	});
});