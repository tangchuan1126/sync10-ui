define({
    "0": {
        "en": "未申请转账",
        "zh": "未申请转账"
    },
    "1": {
        "en": "<font color='red'>部分付款<\/font>",
        "zh": "<font color='red'>部分付款<\/font>"
    },
    "2": {
        "en": "<font color='green'>付款完成<\/font>",
        "zh": "<font color='green'>付款完成<\/font>"
    },
    "3": {
        "en": "<font color='blue'>取消申请<\/font>",
        "zh": "<font color='blue'>取消申请<\/font>"
    },
    "4": {
        "en": "凭证未提交",
        "zh": "凭证未提交"
    },
    "5": {
        "en": "已申请转账",
        "zh": "已申请转账"
    }
});