define(['backbone','../config'], function (Backbone,Config) {

    
    //分类模型
    var ProductCategoryModel = Backbone.Model.extend({
    	url:Config.uri.base,
		idAttribute : "id",    	
    	default : {
    		id : "",
    		title : "",
    		parentId : 0
    	}
    });


    return  ProductCategoryModel; 
});