define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['addCategory'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "	  		<div id=\"category\" class=\"dialog_update_content\">\n		  		<form id=\"categoryForm\" method=\"post\" onkeydown=\"if(event.keyCode==13) return false;\">\n		  			<table>\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Title</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"title\" id=\"title\" maxlength=\"50\" />\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>\n		  			</table>\n				</form>   \n			</div> ";
},"useData":true});
templates['searchCategory'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id=\"categoryForm\">\n		<input type=\"text\" placeholder=\"Search\"  style=\"padding: 6px;border-radius: 4px;border: 1px solid silver;font-family: inherit;font-size: inherit;line-height: inherit;width:68%;\" id=\"search\">\n  		<a href=\"javascript:void(0)\" id=\"filter-clear\" class=\"fa fa-times clear\" style=\"display: none;left:200px;width:10px;color:#5e5e5e\"></a>\n  		<a href=\"javascript:void(0)\" class=\"buttons add\" style=\"height:16px;width:36px;left:10px;\" title=\"Add\">Add</a>		\n</div>\n\n";
},"useData":true});
templates['tree'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "                        <li><span data-id=\""
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.text : depth0), depth0))
    + "</span>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"if","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                        </li>\n";
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return "                                "
    + ((stack1 = (helpers.tree || (depth0 && depth0.tree) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.children : depth0),{"name":"tree","hash":{},"data":data})) != null ? stack1 : "")
    + "\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "               <ul>\n"
    + ((stack1 = helpers.each.call(depth0,depth0,{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "               </ul>";
},"useData":true});
templates['updateCategory'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "	  		<div id=\"category\" class=\"dialog_update_content\">\n		  		<form id=\"categoryForm\" method=\"post\" onkeydown=\"if(event.keyCode==13) return false;\">\n		  			<table>\n			            <tr>\n			                <td>\n			                    <b class=\"require_property\">*</b>\n			                    <span>Title</span>\n			                </td>\n			                <td>&nbsp;</td>\n			            </tr>\n			            <tr>\n			                <td>\n			                    <input type=\"text\" class=\"input_text_password_select\" name=\"title\" id=\"title\" maxlength=\"50\" value=\""
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\"/>\n			                </td>\n			                <td class=\"status validator_style\">&nbsp;</td>\n			            </tr>\n		  			</table>\n				</form>   \n			</div> ";
},"useData":true});
return templates;
});