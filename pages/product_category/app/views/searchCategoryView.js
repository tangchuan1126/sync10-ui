/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/ProductCategoryModel",
    "../views/searchCategoryView",
    "../config",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,ProductCategoryModel,SearchCategoryView,Config){
    /**页面:修改账号*/
	var tmp ;
    return Backbone.View.extend({


        initialize:function(options){
        	tmp = this;
            this.setElement(options.el);
            this.tree = options.tree;
        },  
        template:templates.searchCategory,
        setView : function(views){
        	this.treeView = views.treeView;
        },
        events:{
        	"click a.add" : "add",
        	"keyup #search" : "search",
        	"click a.clear" : "clear",
        },
        render:function(){
        	this.$el.html(this.template({}));
        	
        },
        validate : function(){
        	var flag = true;
        	var title = $("#search").val();
        	if($.trim(title).length == 0){
        		showMessage("Please enter category name","alert");
        		flag = false;
        	}
    		$.ajax({
    			   type: "get",
    			   async : false,
    			   url: Config.uri.exist.replace("{parentId}","0"),
    			   data : {title : title},
    			   success : function(data){
    				   if(!data.success){
    					   showMessage("Exist","alert");
    					   flag = false;
    				   }
    			   }
    		});
        	return flag;	
        },
        add : function(){
            var model = new ProductCategoryModel;

        	var msg =  "Are you sure to add this category?";
        	var title =  "Add Product Category" ;
            art.dialog({
                title:title
                ,lock: true
                ,opacity:0.3
                ,width:400
                ,height:70
                ,init:function(){
                	
                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');
        			
                    this.content(msg);
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";

                }
                ,icon:'warning'
                ,button: [{
                    name: 'Sure',
                    callback: function () {
                        if(tmp.validate()){
                            model.save({
                                "title": $.trim( $("#search").val() ),
                                "level" : 1,
                                "parentId" : "0"
                            },{
                                success: function (data) {
                                    if(data.get("success")){
                                        showMessage("Success","succeed");
                                        $("#search").val("");
                                        setTimeout(function(){
                                            var ref = $(tmp.tree).jstree(true);
                                            data.attributes.text = data.attributes.title;
                                            data.attributes.type = "root";
                                            data.attributes.icon = Config.icon.leaf;
                                            data.attributes.others = {count:0};
                                            ref.create_node('#', data.attributes);
                                        }, 1500);
                                    }else{
                                    	var errors = data.get("errors");
                                    	if(errors.data){
                                    		showMessage("Exist","alert");
                                    	}
                                    	
                                		if(errors.server){
                                			showMessage("Failure","error");
                                		}
                                    }
                                },
                                error: function(model, response, options){
                                	showMessage("Failure","error");
                                }
                            });    
                        }               
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancle'
            });	 	            
        },
        "search" : function(event){
        	tmp.treeView.$el.jstree(true).search($("#search").val());
        	if($.trim($("#search").val()).length > 0){
        		$("a.clear").show();
        	}else{
        		$("a.clear").hide();
        	}
        	
        },
        "clear" : function(event){
        	$("#search").val("");
        	$("a.clear").hide();
        	$("#search").focus();
        },
    });
});
