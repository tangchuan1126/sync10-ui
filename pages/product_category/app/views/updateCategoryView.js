/**
 * Created by subin on 2014.9.12.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/ProductCategoryModel",
    "../config",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,ProductCategoryModel,Config){
	
	var tmp;
	var dialog;
    /**页面:修改账号*/
    return Backbone.View.extend({
        initialize:function(options){
            this.setElement(options.el);
            this.form = options.form;
            this.model = new ProductCategoryModel;
        },
        template:templates.updateCategory,
        setSelectNode : function(node){
        	this.node = node;
        },
        validator:function(){
            $(tmp.form).validate({
                rules: {
                    title: {
                        required: true,
                        remote: {
                            url: Config.uri.exist.replace("{parentId}",tmp.model.get("parentId")),
                            dataType: "json",
                            data:{
                                title : function(){
                                    return $(tmp.form).find("#title").val();
                                },
                                id : tmp.model.get("id")
                            },
                            dataFilter: function (data) {

                                return $.parseJSON(data).success;
                            }
                        }
                    }
                },
                onkeyup:false,
                messages: {
                    title: {
                        required: "Enter Title",
                        remote : "Exist"
                    }
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        },
        render:function(model){
        	tmp = this;
            this.model = model;
            dialog = art.dialog({
                title:'Update Category'
                ,lock: true
                ,opacity:0.3
                ,init:function(){
                	
                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');
                    this.content(tmp.template(model.toJSON()));
                    $("#title").focus();
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";

                },
                button: [{
                    name: 'Submit',
                    callback: function () {
                    	return tmp.update();
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
            tmp.validator();
            $("#categoryForm #title").bind("keyup",tmp.finish);
        },
        "update" : function(event){
            var categoryForm = $(tmp.form);

            if (categoryForm.valid()) {

                //禁用按钮
                dialog.button({

                    name: 'Submit',
                    focus: true,
                    disabled: true
                });
                var model = new ProductCategoryModel;
                model.url = model.url + "/" + tmp.model.id;
                model.save({
                	"id" : tmp.model.id,
                    "title": $.trim( categoryForm.find("#title").val() )
                },{
                    success: function (data) {
                        if(data.get("success")){
                            showMessage("Success","succeed");
                            setTimeout(function(){
                            	dialog.close();
                                var ref = tmp.$el.jstree(true);
                                data.attributes.text = data.attributes.title;
                                ref.rename_node($("#" + model.get("id")),model.get("title"));
                                var currentNode = ref.get_node($("#" + model.get("id")));
                                
                                $("#" + model.get("id")).children("div:first").html("<span style='position:relative;float:right;'>" + currentNode.original.others.count + "</span>");
                            }, 1500);
                                                                       
                        }else{
                        	var errors = data.get("errors");
                    		if(errors.server){
                    			showMessage("Failure","error");
                                //开启按钮
                                dialog.button({

                                    name: 'Submit',
                                    focus: true,
                                    disabled: false
                                });                    			
                    		}else if(errors.data){
                    			$("#title").parent().next().html("<label id=\"title-error\" class=\"error\" for=\"title\">Exist</label>");
                    			$("#title").focus();
                                //开启按钮
                                dialog.button({

                                    name: 'Submit',
                                    focus: false,
                                    disabled: false
                                });                    			
                    		}


                        }
                    },
                    error: function(model, response, options){
                    	showMessage("Failure","error");
                        dialog.button({
                            name: 'Submit',
                            focus: true,
                            disabled: false
                        });                                	
                    }
                });
            } else {
                categoryForm.validate().errorList[0].element.focus();
            }

            return false;        	
        },
        "finish" : function(event){
        	if(event.keyCode == 13){
        		tmp.update();
        	}
        }        
    });
});
