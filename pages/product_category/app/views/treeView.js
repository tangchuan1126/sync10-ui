define(['jquery','backbone','handlebars','templates','../models/ProductCategoryModel','../config','jstree','jqueryui/dialog','showMessage', 'blockui','artDialog'], 
function ($,Backbone,Handlebars,templates,ProductCategoryModel,Config) {
	
    (function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();
	
    Handlebars = Handlebars.default;
    var treeTemplate = templates.tree;

    Handlebars.registerHelper('tree', function(children) {
        return treeTemplate(children);
    });

var ROOT_ID = '#';
var select_node;
var parentId = 0;
var tree;
var tree_view;
var finish_initialize = false;
//标识在移动分类节点时，是否点击了取消
var is_cancel = false;
function cleanMenuForm(){
	$("#menuForm :text").val("");
	$("#menuForm :radio:first").click();
	$("#menuForm span").html("");
	$("#menuForm *").show();
};



//删除节点
function deleteNode(node,isMenu){
	var msg =  "Are you sure to delete this category?";
	var title =  "Delete Product Category" ;
    art.dialog({
        title:title
        ,lock: true
        ,opacity:0.3
        ,width:300
        ,height:70
        ,init:function(){
        	
            var w1 = $(window).width(), H = $('html');
            H.css('overflow', 'hidden');
            var w2 = $(window).width();
            H.css('margin-right', (w2 - w1) + 'px');
			
            this.content(msg);
        }
        ,close:function(){

            document.body.parentNode.style.overflow="scroll";
            document.body.parentNode.style.marginRight="";

        }
        ,icon:'warning'
        ,button: [{
            name: 'Sure',
            callback: function () {
           	 var url = Config.uri.base + "/" + node.id  ;
        	 $.blockUI({message:''});
        	 $.ajax({
        		   type: "delete",
        		   url: url,  
        		   timeout:2000,
        		   contentType:"application/json",
        		   success: function(result){
        		      if(result.success){
        		    	  showMessage("Success","succeed");
        		    	  tree.delete_node(node);
        		    	  
        		    	  paintNodeAndChild(node.parent);  
        		    	  
        		    	  if(node.parent != ROOT_ID){
            		    	  var children = tree.get_children_dom($("#" + node.parent));
            		    	  if(children.length == 0){
            		    		  tree.set_icon($("#" + node.parent),Config.icon.leaf);
            		    	  }        		    		  
        		    	  }
    	        		  setTimeout(function(){
    	        			 $.unblockUI();
    	        		  },1500);			    	  
        		      }else{
        		    	  if(result.error.NOT_ALLOWED){
        		    		  showMessage("Can't be delete,because some products are associated with this category.","alert");
        		    	  }else{
        		    		  showMessage("Failed","error");
        		    	  }
        		    	  
    	        		  setTimeout(function(){
    	        			 $.unblockUI();
    	        		  },2300);        		    	  
        		      }
        		   },
		  		   "error":function(XMLHttpRequest, status, error){
					   showMessage("Failed","error");
					   setTimeout(function(){
							 $.unblockUI();
						 },2300);				   
				   }	        	 
        	 });
            },
            focus: true
        }]
        ,cancel:true
        ,cancelVal:'Cancle'
    });	 	
}

//调整节点顺序及节点之间的关系
function changeOrder(e,data){
	 if(data.parent != data.old_parent || data.position != data.old_position){
		 var params = "parentId=" + ((data.parent == '#')?0:data.parent) + "&oldParentId=" + ((data.old_parent == '#')?0:data.old_parent) + "&position=" + data.position + "&oldPosition=" + data.old_position;
		 $.blockUI({message:''});
		 
		 $.ajax({
			   type: "patch",
			   timeout:2000,
			   url: Config.uri.changeOrder.replace("{id}",data.node.id) + "?" + params, 
			   success: function(result){
				   
			      if(result.success){
			    	  showMessage("Success","succeed");
			    	  if(data.parent != data.old_parent){
			    		  if(data.parent != ROOT_ID){
			    			  tree.set_icon($("#" + data.parent),Config.icon.node);
			    		  }
			    		  
				    	  // var new_parent_node = tree.get_node($("#" + data.parent));

				    	  if(data.old_parent != ROOT_ID){
					    	  var children = tree.get_children_dom($("#" + data.old_parent));
	        		    	  if(children.length == 0){
	        		    		  tree.set_icon($("#" + data.old_parent),Config.icon.leaf);
	        		    	  }				    		  
				    	  }
			    		  paintNodeAndChild(data.parent);  
			    		  paintNodeAndChild(data.old_parent); 
			    		  var increase_count = data.node.original.others.count;
			    		  paintNodeAndParent(data.old_parent,(0 - increase_count));
			    		  paintNodeAndParent(data.parent,increase_count);
			    		  
			    		  
			    		  
			    	  }
					  setTimeout(function(){
						 $.unblockUI();
					  },1500);		    	  
			      }else{
			    	  cancelChange(data);
			    	  showMessage("Failed","error");
					  setTimeout(function(){
						 $.unblockUI();
					  },2300);						    	  
			      }
			   },
	  		   "error":function(XMLHttpRequest, status, error){
	  			   cancelChange(data);
				   showMessage("Failed","error");
					 setTimeout(function(){
						 $.unblockUI();
					 },2300);				   
			   }	   					   
		 });
		 
		 
	 }		 
}

function cancelChange(data){
	  is_cancel = true;
	  if(data.parent != ROOT_ID){
		  tree.open_node($("#" + data.parent));
	  }
	  
	  var position = data.old_position;
	  if(data.parent == data.old_parent && data.position < data.old_position){
		  position += 1;
	  }		
  	  var parent = "#";
  	  if( data.old_parent != ROOT_ID ){
  		  parent = $("#" + data.old_parent);
  	  }
	  
	  tree.move_node($("#" + data.node.id),parent,position);
	  paintNodeAndChild(data.parent);  
	  paintNodeAndChild(data.old_parent);  
}

//在产品分类节点后面加上 与之相关的产品数目
function dealCount(nodeId){
	var children;
	if(nodeId == ROOT_ID){
		children = tree.get_children_dom("#"); 
	}else{
		children = tree.get_children_dom($("#" + nodeId)); 
	}
	if(children.length > 0){
		children.each(function(){
			var child = tree.get_node($(this));
			$("#" + child.id).children("div:first").html("<span style='position:relative;float:right;'>" + child.original.others.count + "</span>");
			dealCount(child.id);
		});			
	}

}

//重新绘制节点以及其子节点
function paintNodeAndChild(nodeId){
	if(nodeId != ROOT_ID){
		var node = tree.get_node($("#" + nodeId));
		$("#" + nodeId).children("div:first").html("<span style='position:relative;float:right;'>" + node.original.others.count + "</span>");
	}
	dealCount(nodeId); 	
}

//重新计算节点及父节点的产品数，产品树在原数上增加 increase_count
function paintNodeAndParent(nodeId,increase_count){
	if(nodeId != ROOT_ID){
		var parent = tree.get_parent($("#" + nodeId));
		var node = tree.get_node($("#" + nodeId));
		node.original.others.count += increase_count;
		$("#" + nodeId).children("div:first").html("<span style='position:relative;float:right;'>" + node.original.others.count + "</span>");
		if(parent != ROOT_ID){
			paintNodeAndParent(parent,increase_count);
		}
		
	}
	
	
}

function changeSelectStyle(node){
	  $("div.right_select").removeClass("right_select").css("background-color","");
	  $("a.right_select").removeClass("right_select").css("background-color","");
	  $("#" + node.id + " div:first").css("background-color","#d3eb9a").addClass("right_select");
	  $("#" + node.id + " a:first").css("background-color","#d3eb9a").addClass("right_select");	
};

    
	var treeData;
	$.ajax({
				 type : "get",
				 url  : "/Sync10/basicdata/productCategory",
				 async : false,
				 success : function(data){
					 treeData = data;
				 }
			}
	);
	
    var treeView = Backbone.View.extend({
        initialize:function(options){
            this.setElement(options.el);
            tree_view = this;
        },
        setView:function(options){
        	this.addCategoryView = options.addCategoryView;
        	this.updateCategoryView = options.updateCategoryView;
        },
        events:{
            "select_node.jstree":"selectTreeNode",
            "deselect_node.jstree":"deSelectTreeNode",  
            "move_node.jstree":"moveNode",
			"show_contextmenu.jstree":"showMenu"
        },
        render:function(){
        	
            this.$el.jstree({"core":{"multiple" : true ,"data":treeData,"check_callback":true,"animation" : 0,"themes" : {'responsive' : false }},
                  "dnd" : {
                      "drag_selection" : function(){
                    	  return false;
                      },
                      "check_while_dragging": function(){
                    	  return false;
                      },
                      "is_draggable":function(array){
                    	  return true;
                      }
                    },
              	  "types" : {
          		    "#" : {
          		    	"max_depth" : Config.MAX_LEVEL, 
          		    }
          		  },                        
            	"plugins": [ "contextmenu","dnd", "search",  "types", "wholerow"],
            	"contextmenu":{"select_node":false,"items":this.rightMenu}
                 }).on('ready.jstree', function (e, data){
            		
                    finish_initialize = true;
                    
                    $("li[role='treeitem']").each(function(){
    					var node = tree.get_node($(this));
    					$("#" + node.id).children("div:first").html("<span style='position:relative;float:right;'>" + node.original.others.count + "</span>");
    					                    	
                    });
                    
            	}).on("open_node.jstree",function(e,data){
            		
            		var id = data.node.original.id;
            		dealCount(id);
            	}).on("create_node.jstree",function(event,data){
            		paintNodeAndChild(data.parent);  
            	});
            tree = this.$el.jstree(true);
            this.getNodeId = function(node){
                return node.id;
            };
        },
        selectTreeNode:function(e,data){
        },
        deSelectTreeNode : function(e,data){

        },
        moveNode : function(e,data){
    	
    		if(is_cancel){
    			is_cancel = false;
    			return ;
    		}
    		
    		if(data.parent == data.old_parent){
    			changeOrder(e,data);
    			return;
    		}
    		
    		var msg =  "Are you sure to move category " + data.node.text + "?";
    		var title =  "Move Product Category" ;
    	    art.dialog({
    	        title:title
    	        ,lock: true
    	        ,opacity:0.3
    	        ,width:300
    	        ,height:70
    	        ,init:function(){
    	        	
    	            var w1 = $(window).width(), H = $('html');
    	            H.css('overflow', 'hidden');
    	            var w2 = $(window).width();
    	            H.css('margin-right', (w2 - w1) + 'px');
    				
    	            this.content(msg);
    	        }
    	        ,close:function(){

    	            document.body.parentNode.style.overflow="scroll";
    	            document.body.parentNode.style.marginRight="";

    	        }
    	        ,icon:'warning'
    	        ,button: [{
    	            name: 'Sure',
    	            callback: function () {
    	            	changeOrder(e,data);
    	            },
    	            focus: true
    	        }]
    	        ,cancel: function(){
    	        	  is_cancel = true;
    	        	  var position = data.old_position;
  			    	
			    	  if(data.parent != ROOT_ID){
			    		  tree.open_node($("#" + data.parent));
			    	  }
  			    	  
  			    	  var parent = "#";
  			    	  if( data.old_parent != ROOT_ID ){
  			    		  parent = $("#" + data.old_parent);
  			    	  }
  			    	  
  			    	  tree.move_node($("#" + data.node.id),parent, position);
  			    	 paintNodeAndChild(data.parent);
  				     paintNodeAndChild(data.old_parent);    
    	        }
    	        ,cancelVal:'Cancle'
    	    });	           	
        },
		showMenu:function(node,position){
			var width =  $("#" + select_node.original.id).find("a:first").width();
			$("ul.jstree-default-contextmenu").css({"left": (position.x + width) + "px"});
		},
		rightMenu:function(node){
			  select_node = node;
			  changeSelectStyle(node);
			  var items = {};
			  if(node.original.level < Config.MAX_LEVEL){
			      items.addItem = { 
						   label: "Add",
						   action: function () {
							   tree_view.addCategoryView.setSelectNode(select_node);
							   tree_view.addCategoryView.render();
						   }
				  };				  
			  }

			  items.updateItem = { 
					   label: "Update",
					   action: function () {
						   tree_view.updateCategoryView.setSelectNode(select_node);
						   
						   var model = new ProductCategoryModel();
						   model.url = model.url + "/" + node.id;
						   model.fetch({async:false});
						   tree_view.updateCategoryView.render(model);						   
					   }
			  };
			  
			  items.deleteItem = { 
					   label: "Delete",
					   action: function(){
						   deleteNode(node, (node.original.level < 3) );
					   }
			  };					  
			  
			 return items;			
		}
    });

    return treeView;
});



