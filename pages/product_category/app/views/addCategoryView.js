/**
 * Created by subin on 2014.9.12.
 */
"use strict";

var i ;

define([
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../models/ProductCategoryModel",
    "../config",
    "jqueryui/tooltip",
    "validate",
    "artDialog",
    "showMessage"
],function($,Backbone,Handlebars,templates,ProductCategoryModel,Config){
	
	var tmp;
	var dialog;
    /**页面:修改账号*/
    return Backbone.View.extend({
        initialize:function(options){
            this.setElement(options.el);
            this.form = options.form;
        },
        template:templates.addCategory,
        setSelectNode : function(node){
        	this.node = node;
        },
        validator:function(){
        	
            $(tmp.form).validate({
                rules: {
                    title: {
                        required: true,
                        remote: {
                            url: Config.uri.exist.replace("{parentId}",tmp.node.original.id),
                            dataType: "json",
                            data:{
                                title:function(){
                                    return $(tmp.form).find("#title").val();
                                }
                            },
                            dataFilter: function (data) {

                                return $.parseJSON(data).success;
                            }
                        }
                    }
                },
                onkeyup:false,
                messages: {
                    title: {
                        required: "Enter Title",
                        remote : "Exist"
                    }
                },
                ignore: "",
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent().next());
                }
            });
        },
        render:function(){
        	tmp = this;
            dialog = art.dialog({
                title:'Add Category'
                ,lock: true
                ,opacity:0.3
                ,init:function(){
                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');
                    this.content(tmp.template({
                    }));
                    $("#title").focus();
                }
                ,close:function(){
                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";
                },
                button: [{
                    name: 'Submit',
                    callback: function () {
                    	return tmp.add();
                    },
                    focus: true
                }]
                ,cancel:true
                ,cancelVal:'Cancel'
            });
            $("#categoryForm #title").bind("keyup",tmp.finish);
            tmp.validator();
        },
        "add" : function(){
            var categoryForm = $(tmp.form);
            if (categoryForm.valid()) {
                //禁用按钮
                dialog.button({
                    name: 'Submit',
                    focus: true,
                    disabled: true
                });
                var model = new ProductCategoryModel;
                model.save({
                    "title": $.trim( categoryForm.find("#title").val() ),
                    "parentId" : tmp.node.original.id,
                    "level" : (tmp.node.original.level + 1)
                },{
                    success: function (data) {
                        if(data.get("success")){
                            showMessage("Success","succeed");
                            setTimeout(function(){
                            	dialog.close();
                                var ref = tmp.$el.jstree(true);
                                data.attributes.text = data.attributes.title;
                                data.attributes.icon = Config.icon.leaf;
                                data.attributes.others = {count:0};
                                ref.create_node(tmp.node, data.attributes);
                                ref.set_icon($("#" + tmp.node.original.id),Config.icon.node);
                            }, 1500);
                                                                       
                        }else{
                        	var errors = data.get("errors");
                    		if(errors.server){
                    			showMessage("Failure","error");
                                //开启按钮
                                dialog.button({
                                    name: 'Submit',
                                    focus: true,
                                    disabled: false
                                });                    			
                    		}else if(errors.data){
                    			showMessage(errors.data,"alert");
                                //开启按钮
                                dialog.button({
                                    name: 'Submit',
                                    focus: false,
                                    disabled: false
                                }); 
                                $("#title").focus();
                    		}

                        }
                    },
                    error: function(model, response, options){
                    	showMessage("Failure","error");
                        dialog.button({
                            name: 'Submit',
                            focus: true,
                            disabled: false
                        });                                	
                    }
                });
            } else {
                categoryForm.validate().errorList[0].element.focus();
            }
            return false;        	
        },
        "finish" : function(event){
        	if(event.keyCode == 13){
        		tmp.add();
        	}
        }
    });
});
