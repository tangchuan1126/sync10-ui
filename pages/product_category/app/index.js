/**
 * Created by lujintao on 2015.3.25.
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "./views/treeView",
    "./views/addCategoryView",
    "./views/updateCategoryView",    
    "./views/searchCategoryView",    
    "blockui",
    "jqueryui/tabs"
],function($ ,Backbone,Handlebars,TreeView,AddCategoryView,UpdateCategoryView,SearchCategoryView){
    $(function(){
    	$("#category_tree").height($(window).height() - 60);       	
    });
    window.onresize = function(){
    	$("#category_tree").height($(window).height() - 60);   
    };	
	$("#main").show();
    var treeView = new TreeView({el:"#category_tree"});
    var addCategoryView = new AddCategoryView({el:"#category_tree","form":"#categoryForm"});
    var updateCategoryView = new UpdateCategoryView({el:"#category_tree","form":"#categoryForm"});
    treeView.setView({
    	addCategoryView: addCategoryView,
    	updateCategoryView : updateCategoryView
    });
    treeView.render();
    var searchCategoryView = new SearchCategoryView({el:"#search_div",tree : "#category_tree"});
    searchCategoryView.setView({
    	treeView : treeView
    });
    searchCategoryView.render();
    $.unblockUI();

 
});