define(
	{
		"uri":{
			"base" : "/Sync10/basicdata/productCategory",
			"exist" : "/Sync10/basicdata/productCategory/exist/{parentId}",
			"changeOrder" : "/Sync10/basicdata/productCategory/order/{id}"
		},
		"icon" : {
			"leaf" : "img/leaf.png",
			"node" : "img/node.png"
		},
		"ROOT_PARENT" : "0",
		"MAX_LEVEL" : 4
	}
);