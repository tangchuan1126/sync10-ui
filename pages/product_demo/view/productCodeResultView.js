/**
 * Created by liyi on 2015.4.23
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars_ext",
    "templates",
    "../model/productCodeModel",
    "art_Dialog/dialog-plus",
    "select2"
],function(config,$,Backbone,Handlebars,templates,models){
    
    return Backbone.View.extend({
        template:templates.productcode_result,
        el:"#search_result",
        collection: new models.ProductCodeCollection(),
        initialize:function(){
        },
        events:{
           "click #addCode":"addCode",
           "click a[name='delCode']":"delCode",
           "click a[name='modCode']":"editCode"
        },
        validator:function(evt,v){
            var ignore = $("input[type='radio'][id='retaile_type']:checked").length==0?true:false;
            var addCode = $("#addCodeForm");
            var pcode_id = $(evt.target).parent().parent('tr')[0].dataset.pcode_id;
            var pc_id = $(evt.target).data("pc_id");
            var codeType = addCode.find("input[type='radio']:checked").val();
            var p_code = addCode.find("#p_code")[0];
            var oldp_code = addCode.find("#p_code").attr("oldp_code");

            var retailer_id = addCode.find("#retailer_id")[0];
            var oldRetailer_id = addCode.find("#retailer_id").attr("oldretailer_id");
            if(p_code.value=="") {
              v.shoeErrow("Please enter code",p_code);
              return;
            } else {
              v.shoeErrow("",p_code);
            }
            if(!ignore) {
                if(retailer_id.value=="") {
                    v.shoeErrow("Please select retailer",retailer_id);
                    return;
                } else {
                    v.shoeErrow("",retailer_id);
                }
            }
            if(oldp_code!=undefined&&oldp_code!="") {
                if((oldp_code+""+oldRetailer_id)==(p_code.value+""+retailer_id.value)){
                    v.render(pc_id);
                    return;
                }
            }
            var url = config.productCodeUrl.checkUrl;
            $.post(url,{p_code:p_code.value,pcode_id:pcode_id,code_type:codeType,retailer_id:retailer_id.value,pc_id:pc_id},function(data){
                if(data.success) {
                  var proModel = new models.ProductCodeModel();
                  proModel.set({p_code:addCode.find("#p_code").val(),code_type:codeType,retailer_id:addCode.find("#retailer_id").val(),pc_id:pc_id});
                  proModel.url=config.productCodeUrl.addUrl;
                  if(pcode_id!="") {
                    proModel.url=config.productCodeUrl.modUrl;
                    proModel.set("pcode_id",pcode_id);
                  }  
                  proModel.save({},{
                      success: function (data) {
                        if(data.get("success")) {
                          v.shoeErrow("",retailer_id);
                          v.shoeErrow("",p_code );
                          v.render(pc_id);
                        }
                    }
                  });
                } else {
                    if(data.p_code){
                        v.shoeErrow("Code already exists",p_code);
                    } else {
                        v.shoeErrow("",p_code);
                    }
                    if(!ignore){
                        if(data.retailer_id) {
                            v.shoeErrow("Retailer or code already exists",retailer_id);
                        } else {
                            v.shoeErrow("",retailer_id);
                        }
                    }
                }

                  
            }
        );
        },
        shoeErrow:function(error,element) {  
          var nextTr = $(element.parentNode.parentNode).next("tr")[0];
          var index = $(element.parentNode).index();
          $(nextTr.children[index]).text(error);
        },
        setView:function(views){
            this.productCodeDelView = views.productCodeDelView;
        },
        addCode:function(evt){
            var v = this;
            v.validator(evt,v);
        },
        delCode:function(evt){
            var pcode_id = $(evt.target).parent().parent('tr').data("pcode_id");
            var pModel = this.collection.findWhere({pcode_id:pcode_id});
            this.productCodeDelView.setView({productCodeResultView:this});
            this.productCodeDelView.render(pModel,this.pc_id);
        },
        editCode:function(evt){
            var pcode_id = $(evt.target).parent().parent('tr').data("pcode_id");
            var pModel = this.collection.findWhere({pcode_id:pcode_id});
            $("#retailer_id").val(pModel.get("retailer_id"));
            $("#retailer_id").attr("oldretailer_id",pModel.get("retailer_id"));
            $("#p_code").val(pModel.get("p_code"));
            $("#p_code").attr("oldp_code",pModel.get("p_code"));
            $("#retailer_id").select2("val",pModel.get("retailer_id"));
            $("#retaileTd").parent('tr').data("pcode_id",pcode_id);
            $("#retaileTd").css("opacity",1);
            $("#addCode").text("EditCode");
            $("#addCode").attr("title","EidtCode");
            $('.validator_style').text('');
            $("#retaile_type")[0].checked=true;
        },
        render:function(pc_id){
            var v = this;
            v.pc_id = pc_id;
            $.blockUI({message:'<div class="block_message">Please wait......</div>'});
            if(pc_id!=null){
                v.collection.url = config.productCodeUrl.getAllUrl;
                v.collection.reset();
                v.collection.fetch({
                    async: false,
                    data:{
                        pc_id:pc_id                        
                    }
                });
                v.retailerCollection = new models.ShipToCollection();
                v.$el.html(v.template({
                        result:v.collection.toJSON(),
                        retailers : v.retailerCollection.toJSON(),
                        pc_id:pc_id  
                }));
                $("#retailer_id").select2({
                placeholder: "Select...",
                allowClear: true
            }); 
            }
            
            $.unblockUI(); 

        }
    });
});
