/**
 * Created by liyi on 2015.4.23
 */
"use strict";
define(['../config',
    "jquery",
    "backbone",
    "handlebars",
    "templates",
    "../model/productCodeModel",
    "artDialog"
],function(config,$,Backbone,Handlebars,templates,models){

    return Backbone.View.extend({

        template:templates.del_productcode,
        initialize:function(options){
        },
        setView:function(views){
            this.productCodeResultView = views.productCodeResultView;
        },
        render:function(pModel,pc_id){
            var tmp = this;
            art.dialog({
                title:'Del ProductCode » '+pModel.get("p_code")
                ,lock: true
                ,opacity:0.3
                ,init:function(){

                    var w1 = $(window).width(), H = $('html');
                    H.css('overflow', 'hidden');
                    var w2 = $(window).width();
                    H.css('margin-right', (w2 - w1) + 'px');

                    this.content(tmp.template({
                        model:pModel.toJSON()
                    }));
                }
                ,close:function(){

                    document.body.parentNode.style.overflow="scroll";
                    document.body.parentNode.style.marginRight="";
                }
                ,icon:'warning'
                ,ok:function(){

                    var pcodeModel = new models.ProductCodeModel({
                        pcode_id:pModel.get("pcode_id")
                    });

                    pcodeModel.url =config.productCodeUrl.delUrl+"?pcode_id="+pModel.get("pcode_id");

                    pcodeModel.destroy({
                        success: function(model, response){

                            if(response.success){
                                
                                tmp.productCodeResultView.render(pc_id);
                            }else{

                                art.dialog({
                                    title:'Message'
                                    ,lock:true
                                    ,opacity:0.3
                                    ,content:"Delete Failure"
                                    ,icon:'warning'
                                    ,ok:true
                                    ,okVal:'Sure'
                                });
                            }
                        }
                    });
                }
                ,okVal:'Sure'
                ,cancel:true
                ,cancelVal:'Cancel'
            });
        }
    });
});
