﻿(function () {
    var configObj = {
        //order信息
        orderUrl: {
            url: "/Sync10/_load/getWmsOrderByComCusPoReq" //"/sync10-ui/pages/load/loaddata.js" //
        },
        //save load
        addLoadUrl: {   
            url: "/Sync10/_load/addLoad"
        },
        //删除load
        delLoadUrl: {
            url: "/Sync10/_load/delLoad"
        },
        loadListUrl: {
            url: "/Sync10/_load/getMasterOrderListByPage"      //"/sync10-ui/pages/load/loaddata.json"// "/Sync10/load/getWmsLoadList?load_no&status&start_time&end_time&page=1&page_size=1"// 
        },
        addAppoiontmentUrl: {   //约车    
            url: "/Sync10-ui/pages/appointment/add.html"
        },
        editMasterOrderUrl: {  //编辑界面
            url: "/Sync10-ui/pages/master_order/add.html"
        },
        getLoadUrl: {
            url: "/Sync10/_load/getWmsLoadById"  //http://192.168.1.8/Sync10/load/getWmsLoadById?load_id=
        },
        customerUrl: {  //customer json
            url: "./json/carrier.json"
        },
        companyUrl: {   //company json
            url: "./json/carrier.json"
        }

        //load列表:/Sync10/load/getWmsLoadList?load_no&status&start_time&end_time&page=1&page_size=1
        //添加Load:http://192.168.1.8/Sync10/load/addLoad?data=json
        //查询单个load：http://192.168.1.8/Sync10/load/getWmsLoadById?load_id=
        //编辑Load:http://192.168.1.8/Sync10/load/editLoad?data=json
        //删除Load:http://192.168.1.8/Sync10/load/delLoad?data=json
        //关联Order列表:/Sync10/load/getWmsOrderByComCusPoReq
        //customer json:http://192.168.1.8/Sync10/load/customerList
        //company json：http://192.168.1.8/Sync10/load/companyList
    };
    define(configObj);
}).call(this);