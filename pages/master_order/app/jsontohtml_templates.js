define(["JSONTOHTML"],
function(JSONTOHTML) {
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index) {
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			},
			{
				tag: "tbody",
				html: ""
			}]
		},
		{
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		},
		{
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			html: "${title}"
		}
	};
	var _tbody = {
		head: null,
		int: function(jsons, View_control) {
			var headallobj = View_control.head;
			var footer = View_control.footer;
			_tbody.head = headallobj;
			var trfooterdata = {
				"colspan": headallobj.length,
				"children": footer,
				"trid": ""
			};
			var pageSize = jsons.PAGECTRL.pageSize;
			//console.log(jsons.DATA);
			var __tr = _tbody.TotrJson(jsons.DATA, pageSize);
			var __trhtml = "";
			//console.log(__tr);
			for (var trkey in __tr) {
                
			    var _tritmes = __tr[trkey];


				__trhtml += (JSONTOHTML.transform(_tritmes, _tbody.tr));
				trfooterdata["trid"] = _tritmes.trid;
				__trhtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));
			}
			return __trhtml;
		},
		TotrJson: function(DATA, pageSize) {
			var headallobj = _tbody.head;
			var __tr = [];
			for (var tri = 0; tri < DATA.length; tri++) {
				var tritme = DATA[tri];
				var __td = [];
				for (var headkey in headallobj) {
					var tritmes = headallobj[headkey];
					var Rowname = tritmes.field;
					var Rowobj = {};
					if (typeof Rowname == "object") {
						__td.push({
							Options: Rowname
						});
					} else {
						Rowobj = eval("tritme." + Rowname);
						var setobjs = eval("({" + Rowname + ":Rowobj})");
						__td.push(setobjs);
					}
				}
				__tr.push({
					"trid": tritme.trid,
					"children": __td
				});
				if (tri >= pageSize) {
					break;
				}
			}
			return __tr;
		},
		keyname: function(obj) {
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id": function (obj) {
			    //console.log(obj);
				return obj.trid;
			},
			"data-itmeid": function(obj) {
				return "trid|" + obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="trid|' + obj.trid + '" class="buttons-group">';
				var ahtml = "";
				//console.log(obj);
				for (var akey in obj.children) {
					var itmea = obj.children[akey];
					ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
				}
				trhtmls += ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		td: {
		    tag: "td",
		    width: function (obj, index) {
		        var width = "20%";
		        switch (index) {
		            case 0:
		                width = "30%";
		                break;
		            case 1:
		                width = "50%";
		                break;
		            case 2:
		                width = "20%";
		                break;
		        }
		        return width;
		    },
			class: function(obj, index) {
				var classname = "";
				var keynames = _tbody.keyname(obj);
				classname = "td_" + keynames;
				return classname;
			},
			valign: function (obj, index) {
			    return "center";
				//if(index == 0){
				//	return "center";
				//} else {
				//	return "top";
				//}
			},
			html: function(obj, index) {
				var retuhtml = "没找到模版";
				var keynames = _tbody.keyname(obj);
				if (keynames != "" && typeof keynames != "undefined") {
					if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
					}
				}
				return retuhtml;
			}
		},
		MASTERORDER: {
			tag: "fieldset",
			class: "tableBlue",
			children: [
                {
				tag: "legend",
				html: function(obj, index) {
					var legendhtml = "";
					legendhtml = '<span class="Sub_order">Master:' + obj.MASTERORDER.MASTERID + '</span>';
					return legendhtml;
				}
			},
			{
				tag: "div",
				class: "Tractor_ulitme",
				children: [{
					tag: "ul",
					html: function(obj, index) {
						return (JSONTOHTML.transform(obj.MASTERORDER.CHILDREN, _tbody.fieldset1divulli));
					}
				}]
			}]
		},
		fieldset1divulli: {
			tag: "li",
			html: function(obj, index) {
				return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		"ORDERLIST": {
		    tag: "div",
		    children: [{
		        tag: "div",
		        html: function (obj, index) {
		            return (JSONTOHTML.transform(obj.ORDERLIST, _tbody.ORDER2li));
		        }
		    }]
		},
		ORDER2li: {
		    tag: "fieldset",
		    class: "tableBlue",
		    children: [{
		        tag: "legend",
		        html: function(obj, index) {
		            var legendhtml = "";
		            legendhtml += '<span class="Sub_order">OrderNo:' + obj.ORDERNO + '</span>';
		            return legendhtml;
		        }
		    },
			{
				tag: "div",
				class: "Tractor_ulitme",
				children: [{
					tag: "ul",
					html: function(obj, index) {
						return (JSONTOHTML.transform(obj.CHILDREN, _tbody.fieldset3divulli));
					}
				}]
			}]
		},
		fieldset3divulli: {
			tag: "li",
			html: function(obj, index) {
					return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		updatatr: function(jsondata, trid) {
			var tritmeobj = $(trid);
			var tritmedata = [];
			tritmedata.push(jsondata);
			var uptrdata = _tbody.TotrJson(tritmedata, 1);
			//console.log(uptrdata[0].children);
			tritmeobj.empty().html(JSONTOHTML.transform(uptrdata[0].children, _tbody.td));
		}
	}
	return {
		tbody: _tbody,
		table: _table
	}
});