"use strict";
require(['/Sync10-ui/requirejs_config.js'], function() {
    var cssfileall = [
        /*"require_css!bootstrap-css/bootstrap.min",
	                  "require_css!Font-Awesome", 
	                  "require_css!../css/main.css", 
	                  "require_css!../css/easysearch.css", 
	                  "require_css!oso.lib/bootstrap/css/VisiStyle.css",
	                  "require_css!oso.lib/slidePanel/css/style.css"*/ ];

	require(cssfileall, function() {
	    require(["jquery",
            "bootstrap",
            "oso.lib/bootstrap-datetimepicker-plus/datetimepicker-plus",
            "metisMenu",
            "CompondList/View",
            "art_Dialog/dialog-plus",
            "compond_list_config",
            "jsontohtml_templates",
            'backbone',
            'handlebars',
            "../view/searchSample",
            "../view/searchAdvanced",
            "slidePanel",
            "../config",
            "../model/loadModel",
            "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
            "require_css!bootstrap.datetimepicker/css/bootstrap-datetimepicker.min", "blockui"],
			function ($, bootstrap, datetimepicker, metisMenu, CompondList, Dialog, list_config, templates, backbone, Handlebars, searchSample, searchAdvanced, SlidePanel, Config, Model, AsynLoadQueryTree) {
			    (function () {

			        $.blockUI.defaults = {
			            css: {
			                padding: '8px',
			                margin: 0,
			                width: '170px',
			                top: '45%',
			                left: '40%',
			                textAlign: 'center',
			                color: '#000',
			                border: '3px solid #999999',
			                backgroundColor: '#ffffff',
			                '-webkit-border-radius': '10px',
			                '-moz-border-radius': '10px',
			                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			                '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
			            },
			            overlayCSS: {
			                backgroundColor: '#000',
			                opacity: '0.6'
			            },
			            baseZ: 99999,
			            centerX: true,
			            centerY: true,
			            fadeOut: 1000,
			            showOverlay: true
			        };
			    })();


			    $(document).ajaxStart(function () {
			        $.blockUI({ message: '<img src="/Sync10-ui/pages/load/css/img/dy_loading.gif" class="loading" align="absmiddle"/> 正在处理，请稍后.' });   //遮罩打开 
			    });

			    $(document).ajaxError(function () {
			        $.blockUI({ message: '出现错误，请刷新!<div><a class="closeBlock">关闭</a></div>' });   //遮罩打开 
			        $(".closeBlock").click(function () {
			            $.unblockUI();
			        });
			    });

			    $(document).ajaxSuccess(function () {
			        $.unblockUI();
			    });


			    window.Parentpush = {};
			    //$('#myTab a:first').tab('show');
			    $('#myTab a:last-child').tab('show');
			    
	
			    $('.quick-create').on('click', function (e) {
			        e.stopPropagation();
			        return false;
			    });

			    var sample = new searchSample({
				    el: "#searchSampleTab"
			    }).render();
	
			    var advanced = new searchAdvanced({
				    el: "#searchAdvancedTab"
			    }).render();
			
			    var fromTree = new AsynLoadQueryTree({
			        renderTo: "#searchLoadCustomer",
			        dataUrl: Config.customerUrl.url,
			        scrollH: 400,//多高后出现滚动条
			        Async: false,//非异步获取树节点，（一次加载完成）
			        multiselect: false,//多选
			        PostData: { "a": "1", "b": "2" },//异步时请求每个节点时往服务端传值,
			        placeholder: "",//默认input框值
			        Parentclick: true//开启父节点可以选中
			    });
			    fromTree.render();

			    var fromTree2 = new AsynLoadQueryTree({
			        renderTo: "#searchLoadCompany",
			        dataUrl: Config.companyUrl.url,
			        scrollH: 400,//多高后出现滚动条
			        Async: false,//非异步获取树节点，（一次加载完成）
			        multiselect: false,//多选
			        PostData: { "a": "1", "b": "2" },//异步时请求每个节点时往服务端传值,
			        placeholder: "",//默认input框值
			        Parentclick: true//开启父节点可以选中
			    });
			    fromTree2.render();

			    

			    $("#addPackage").on('click', function () {
			        var slide = new SlidePanel({
			            title: "Create Master Order",
			            url: Config.editMasterOrderUrl.url,
			            duration: 500,
			            topLine: 0,
			            slideLine: "85%"
			        });
	                slide.open();
			    });

			    $(function () {
			        var list;
			        var listView = function () {
			            list = new CompondList(list_config, {
			                renderTo: "#main_div",
			                dataUrl: Config.loadListUrl.url,
			                PostData: { //load_no&status&start_time&end_time&page=1&page_size=1
			                    "load_no": $("#searchLoadPoNo").val(),
			                    "customer": $("#searchLoadCustomer").attr("data-val"),
			                    "company": $("#searchLoadCompany").attr("data-val"),
			                    "end_time": $("#searchLoadEndMABD").val(),
			                    "begin_time": $("#searchLoadBeginMABD").val()
			                }
                            //fetchtype:"POST"
			            });
			            list.render();
			            Parentpush.RestwintopMeun = list.Settopnvaheigth;
			            Parentpush.Dialog_api = Dialog;
			        }
			        listView();

			        
			        
                    /*搜索*/
			        $("#btnLoadFilter").on("click", function () {
			            listView();
			        });

			        $("#btnReset").on("click", function () {
			            $("#searchLoadPoNo").val("");
			            $("#searchLoadCustomer").attr("data-val", "").attr("data-name", "").val("");
			            $("#searchLoadCompany").attr("data-val","").attr("data-name", "").val("");
			            $("#searchLoadEndMABD").val("");
			            $("#searchLoadBeginMABD").val("");
			            listView();
			        });

			        /*创建load*/
			        $("#btnQuickCreateLoad").on("click", function () {
			            var f = true;
			            if ($("#inputAddLoadNo").val() == "" | $("#inputAddLoadNo").val() == undefined) {
			                if (!$("#inputAddLoadNo").parent().hasClass("has-feedback")) {
			                    $("#inputAddLoadNo").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
			                    $("#inputAddLoadNo").focus();
			                }
			                f = false;
			            } else {
			                $("#inputAddLoadNo").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
			            }

			            if (f) {
			                var v = this;
			                var model = new Model.LoadModel({
			                    loadNo: $("#inputAddLoadNo").val(),
			                });

			                model.save({
			                    id: v.loadid,
			                    loadNo: $("#inputAddLoadNo").val(),
			                    orderList: []
			                }, {
			                    success: function (e) {
			                        if (e.changed.result !=0) {
			                            var d = new Dialog({
			                                content: e.changed.error,
			                                icon: 'question',
			                                lock: true,
			                                width: 200,
			                                height: 40,
			                                title: '保存失败',
			                                cancelVal: '关闭',
			                                cancel: function () { }
			                            }).show();
			                        } else {
			                            $("#inputAddLoadNo").val("");
			                            listView();
			                        }
			                    },
			                    faile: function (e) {
			                        var d = new Dialog({
			                            content: "保存失败,请重试!",
			                            icon: 'question',
			                            lock: true,
			                            width: 200,
			                            height: 40,
			                            title: '保存失败',
			                            cancelVal: '关闭',
			                            cancel: function () {}
			                        }).show();
			                    }
			                });
			            }

			        });
				    
			        list.on("events.more", function (e) {
			            var btnobj = e.button;
			            var table = btnobj.prev("table").eq(0);

			            if (table.find(".trHidden").is(":hidden")) {
			                table.find(".trHidden").show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			                    table.find(".trHidden").removeClass("fadeInDown");
			                    btnobj.html("Hide");
			                });
			            } else {
			                table.find(".trHidden").addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			                    table.find(".trHidden").removeClass("fadeOutUp").hide();
			                    btnobj.html("More...");
			                });
			            }
			        });

			        list.on("events.moreOption", function (e) {
			            var btnobj = e.button;
			            var table = btnobj.parents(".Log_container").find("div");

			            if (table.is(":hidden")) {
			                table.show().addClass("fadeInDown").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			                    table.removeClass("fadeInDown");
			                    btnobj.html("Hide");
			                });
			            } else {
			                table.addClass("fadeOutUp").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			                    table.removeClass("fadeOutUp").hide();
			                    btnobj.html("More...");
			                });
			            }
			        });

			        list.on("events.CreateAppoiontment", function (e) {
			            var btntxt = e.button.text();
			            console.log(e);
				        
				        var slide = new SlidePanel({
				            url: Config.addAppoiontmentUrl.url + "?orderid=" + e.data.linedata.trid + "&ordertype=load&appid=" + e.data.linedata.LOAD.aid,
				            title: 'Create Appoiontment',
				            duration: 500,
				            topLine: 0,
				            slideLine: "85%",
				            dynamicParam: '{"orderid":' + e.data.linedata.trid + ',"ordertype":"load"}'
				        });
				        slide.open();

				    });

				    list.on("events.Edit", function (e) {
				        var btntxt = e.button.text();
				        
				        var slide = new SlidePanel({
				            title: "Edit Load",
				            url: Config.editLoadUrl.url+ "?loadid=" + e.data.linedata.trid + "&loadtype=Edit",//&orderno=" + e.data.linedata.LOAD.LOADNO,
				            duration: 500,
				            topLine: 0,
				            slideLine: "85%"
				        });
				        slide.open();
				   
				    });

				    list.on("events.Delete", function (e) {
					    var btntxt = e.button.text();
					    var d = new Dialog({
						    content: '您确定删除本信息吗?',
						    icon: 'question',
						    lock: true,
						    width: 200,
						    height: 70,
						    title: '删除信息',
						    okVal: 'Yes',
						    ok: function () {
						        $.ajax({
						            type: 'post',
						            url: Config.delLoadUrl.url,
						            data: 'load_id=' + e.data.linedata.trid,
						            dataType: 'json',
						            success: function (data) {
						            	if(data!=null){
						            		if(data.result == "-2") {
						            			alert(data.error);
						            		}
						            	}
						                listView();
						            },
						            error: function () {}
						        });
						    },
						    cancelVal: 'No',
						    cancel: function() {
							    console.log("No");
						    }
					    }).showModal();
				    });

				    window.addEventListener('message', function (e) {
				        $(".slide-header-close").click();
				        listView();
				    }, false);

				    $("#searchLoadBeginMABD").datetimepicker({ format: 'mm-dd-yyyy' });
				    $("#searchLoadEndMABD").datetimepicker({ format: 'mm-dd-yyyy' });
			    });
			    
			
			NProgress.done();
		});
	});
});