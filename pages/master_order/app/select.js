"use strict";
require(['/Sync10-ui/requirejs_config.js'], function() {
	var cssfileall = ["require_css!bootstrap-css/bootstrap.min", 
	                  "require_css!Font-Awesome", 
	                  "require_css!../css/main.css", 
	                  "require_css!../css/style.css", 
	                  "require_css!../css/easysearch.css", 
	                  "require_css!oso.lib/bootstrap/css/VisiStyle.css",
	                  "require_css!oso.lib/slidePanel/css/style.css" ];
	
	require(cssfileall, function() {
		require(["jquery", "bootstrap", "metisMenu", "art_Dialog/dialog-plus", 'backbone', 'handlebars', "../view/searchOrders", "../view/loadDetail", "oso.lib/table/js/table"], 
				function($, bootstrap, metisMenu, Dialog, backbone, Handlebars, searchOrders, LoadDetail, Table) {

			window.Parentpush = {};

			var _temp_selected_rows = new Array();
			var all_data_rows = new Array();
	
			var search = new searchOrders({
				el: "#searchOrders"
			}).render();
			
			var loadDetail = new LoadDetail({
				el: '#load'
			}).render();
			
			var table = new Table({
	    		container : '#table',
	    		height: 420,
	    		rownumbers : false,
	            checkbox : true,
	            singleSelection : false,
	    		columns:[
		    		{field:'ORDERNO',title:'OrderNo'},
		    		{field:'PONO',title:'PONo'},
		    		{field:'CUSTOMERID',title:'Customer'},
		    		{field:'COMPANYID',title:'Company'},
		    		{field:'SHIPTOADDRESS',title:'ShipToAddress'},
		    		{field:'SHIPTOZIPCODE',title:'ShipToZipCode'}
	    		],
	    		toolbar : [
	    		            {
	    		                text : 'Add to Load',
	    		                iconCls : 'add',
	    		                handler : function(){
	    		                	
	    		                    console.log(window.parent.parent.parent.parent);
	    		                    
	    		                    var rows = table.getSelected();
	    		                    if(!rows[0]){
	    		                    	return;
	    		                    }
	    		                    
//	    		                    parent.all_data_rows = parent.all_data_rows.concat(rows);
	    		                    
	    		                    table.deleteLocalData(rows, function(row){
	    		                        table.reloadCurrent(true);
	    		                    });
	    		                    
	    		                }
	    		            }
	    		        ]
	    	});

	    	$.get('a.json').success(function(data){
	    		all_data_rows = data.rows;
	    		if(all_data_rows.length > 0){
		    		table.loadData(all_data_rows,function(index, row){
	                    table.reloadCurrent(true);
                    });
		    	}
	    	});

	    	
	    	
			NProgress.done();
		});
	});
});