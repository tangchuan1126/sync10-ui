define(["./jsontohtml_templates"], function(tmp) {
	return {
		"View_control": {
			"head": [{
				"title": "Master Order",
				"field": "MASTERORDER"
			}, {
				"title": "Order",
				"field": "ORDERLIST"
			}],
			"footer": [
                [
					"events.CreateAppoiontment", "Appoiontment"
                ],
				[
					"events.Edit", "Edit"
				],
				[
					"events.Delete", "Delete"
				]
			],
			"templates": tmp
		}
	}

});