﻿"use strict";
define(["jquery",
    "backbone",
    'handlebars',
    "../templates/templates.amd",
    "compondBox",
    "oso.lib/table/js/table",
    "art_Dialog/dialog-plus",
    "../model/loadModel",
    "../config", "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
            "bootstrap.datetimepicker/js/bootstrap-datetimepicker.min",
            "require_css!bootstrap.datetimepicker/css/bootstrap-datetimepicker.min"],
    function ($, Backbone, HandleBars, templates, CompondBox, Table, Dialog, Model, config, AsynLoadQueryTree, datetimepicker) {
    return Backbone.View.extend({
        el: null,
        template: templates.addLoadTable,
        initialize: function () {
            //this.listenTo(this, "update-model", this.updateModel);
        },
        render: function () {
            var v = this;

            var quickSearch = "order" + templates.quickSelectOrder().trim();

            var temp = new CompondBox({
                content: v.template().trim(),
                title: quickSearch,
                container: v.el,
                isExpand: true,
                isMerger: false
            });

            $(".quick-search").on("click", function (e) { e.stopPropagation(); return false; })
            $("#btnAddOrder").on("click", function () { v.addOrder(); });

            v.parentTable = new Table({
                container: '#addTableList',
                height: 800,
                rownumbers: false,
                localData: [],
                checkbox: true,
                noDataMsg: "No Data",
                groupKey:"ship_to_address1",
                pagination:false,
                singleSelection: false,
                columns: [
                    { field: 'ship_to_address1', title: 'ShipToAddress & Customer' },
                    { field: 'order_number', title: 'OrderNo', width: 120 },
                    { field: 'po_no', title: 'PONo', width: 120 },
                    { field: 'company_id', title: 'Company', width: 100 }
                    
                ],
                toolbar: [
	    		    {
	    		        text: 'Revmoe',
	    		        iconCls: 'glyphicon glyphicon-trash',
	    		        handler: function () {
	    		            var rows = parentTable.getSelected();
	    		            if (!rows[0]) {
	    		                return;
	    		            }

	    		            deleteLocalData(rows, function (row) {
	    		                parentTable.reloadCurrent(true);
	    		            });
	    		        }
	    		    }
                ]
            });


        },
        addOrder: function () {
            var v = this;
            var h = $(templates.addAllOrderLoadTable().trim());
            var d = Dialog({
                title: "关联order",
                width: 800,
                height: 420,
                lock: true,
                opacity: 0.6,
                content: h,
                cancelValue: '关闭',
                cancel: function () {
                }
            });
            d.reset();
            d.showModal();

            v.init();
            v.getOrderList();

            $("#btnOrderFilter").on("click", function () {
                v.getOrderList();
            });

        },
        getOrderList: function () {
            var v = this;

            $("#allTableList").html("");
            var table2 = new Table({
                container: '#allTableList',
                height: 300,
                url: config.orderUrl.url,
                queryParams: [{
                    company_id: $("#searchOrderCompany").val(),
                    customer_id: $("#searchOrderCustomer").val(),
                    req_start: $("#searchOrderBeginRequest").val(),
                    req_end: $("#searchOrderEndRequest").val(),
                    po_no: $("#searchOrderPoNo").val(),
                    checkIds: v.GetSelectID()
                }],
                method: "post",
                groupKey: "ship_to_address1",
                rownumbers: false,
                checkbox: true,
                singleSelection: false,
                pagination:true,
                pageSize: 50,
                columns: [
                    { field: 'ship_to_address1', title: 'ShipToAddress & Customer' },
                    { field: 'order_number', title: 'OrderNo', width: 110 },
                    { field: 'po_no', title: 'PONo', width: 110 },
                    { field: 'company_id', title: 'Company', width: 90 }
                ],
                toolbar: [
                    {
                        text: 'Add To Load',
                        iconCls: 'add',
                        handler: function () {
                            var rows = table2.getSelected();
                            if (!rows[0]) {
                                return;
                            }
                            if (v.select_data_rows == undefined || v.select_data_rows=="") {
                                v.select_data_rows = "";
                                v.select_data_rows = rows;
                            } else {
                                v.Deduplication(rows);
                            }

                            table2.reload({
                                company_id: $("#searchOrderCompany").val(),
                                customer_id: $("#searchOrderCustomer").val(),
                                req_start: $("#searchOrderBeginRequest").val(),
                                req_end: $("#searchOrderEndRequest").val(),
                                po_no: $("#searchOrderPoNo").val(),
                                checkIds: v.GetSelectID()
                            });
                            v.parentTable.loadData(v.select_data_rows);

                            //console.log(v.parentTable.localData);
                            //v.parentTable.localData = v.select_data_rows;
                            //v.parentTable.reload();

                        }
                    }
                ]
            });

            //$.get(url).success(function (data) {
            //    all_data_rows = data.DATA;

            //    if (all_data_rows.length > 0) {
            //        table2.loadData(all_data_rows, function (index, row) {
            //            table2.reloadCurrent(true);
            //        });
            //    }
            //});
            
        },
        init: function () {
            console.log("init");
            var fromTree = new AsynLoadQueryTree({
                renderTo: "#searchOrderCompany",
                dataUrl: "./json/shipTo.json",
                scrollH: 400,
                multiselect: false,
                Async: true
            });
            fromTree.render();

            var fromTree2 = new AsynLoadQueryTree({
                renderTo: "#searchOrderCustomer",
                dataUrl: "./json/shipTo.json",
                scrollH: 400,
                multiselect: false,
                Async: true
            });
            fromTree2.render();

            $("#searchOrderBeginRequest").datetimepicker({ format: 'yyyy-mm-dd' });
            $("#searchOrderEndRequest").datetimepicker({ format: 'yyyy-mm-dd' });
        },
        Deduplication: function (rows) {
            var v = this;
            var flag = true;
            var obj = [];

            for (var j = 0; j < v.select_data_rows.length; j++) {
                obj.push(v.select_data_rows[j]);
            }

            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    if (v.select_data_rows[j] == rows[i]) {
                        flag = false;
                    }
                }
                if (flag) {
                    obj.push(rows[i]);
                }
            }
            v.select_data_rows = null;
            v.select_data_rows = obj;


            console.log(obj);

        },
        RemoveRows: function (rows) {
            var v = this;
            var flag = true;
            var obj = [];

            for (var j = 0; j < v.select_data_rows.length; j++) {
                obj.push(v.select_data_rows[j]);
            }

            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    if (v.select_data_rows[j].contains(rows[i])) {
                        flag = false;
                    }
                }
                if (flag) {
                    obj.push(rows[i]);
                }
            }
            v.select_data_rows = null;
            v.select_data_rows = obj;


            console.log(obj);
        },
        GetSelectID: function () {
            var v = this;
            var ids = new Array();
            if (v.select_data_rows != null && v.select_data_rows != undefined && v.select_data_rows != "") {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    ids.push(v.select_data_rows[j].order_id);
                }
            }
            return ids;
        },
        addSum: function (count) {
            
        }
    });
});