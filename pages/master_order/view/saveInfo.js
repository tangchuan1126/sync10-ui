﻿"use strict";
define(["jquery", "backbone", 'handlebars', "../templates/templates.amd", "art_Dialog/dialog-plus", "../model/loadModel"], function ($, Backbone, HandleBars, templates, Dialog, LoadModel) {
    return Backbone.View.extend({
        template: templates.searchSample,
        initialize: function () {
            //this.listenTo(this, "update-model", this.updateModel);
        },
        setView: function (view) {
            this.LoadDetail = view.LoadDetail,
            this.AddLoadTable = view.AddLoadTable
        },
        render: function () {
            var v = this;
            
        },
        save: function () {

            //LoadModel.OrderModel
            //LoadModel.LoadModel

            console.log("save");
        },
        verification: function () {
            var f = true;
            if ($("#addLoadNo").val() == "" | $("#addLoadNo").val() == undefined) {
                if (!$("#addLoadNo").parent().hasClass("has-feedback")) {
                    $("#addLoadNo").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $("#addLoadNo").focus();
                }
                f = false;
            } else {
                $("#addLoadNo").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            return f;
        }
    });
});