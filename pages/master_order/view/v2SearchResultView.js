﻿"use strict";
define(["jquery",
    "backbone",
    "oso.lib/bootstrap-datetimepicker-plus/datetimepicker-plus",
    'handlebars',
    "../templates/templates.amd",
    "compondBox",
    "oso.lib/table/js/table",
    "art_Dialog/dialog-plus",
    "../model/loadModel",
    "../config",
    "oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
    "require_css!bootstrap.datetimepicker/css/bootstrap-datetimepicker.min"],
    function ($, Backbone, datetimepicker, HandleBars, templates, CompondBox, Table, Dialog, Model, config, AsynLoadQueryTree) {
    return Backbone.View.extend({
        el: null,
        template: templates.loadDetail,
        initialize: function () {
            var v = this;
            $(function () {
                var loadid = v.GetRequest("loadid");
                var loadtype = v.GetRequest("loadtype");
                var orderno = v.GetRequest("orderno");

                var temp = new CompondBox({
                    content: templates.loadDetail().trim(),
                    title: "Master Order Info",
                    container: "#masterOrderContainer",
                    isExpand: true
                });

                var quickSearch = "order" + templates.quickSelectOrder().trim();
                var temp = new CompondBox({
                    content: templates.addLoadTable().trim(),
                    title: quickSearch,
                    container: "#masterOrderTableContainer",
                    isExpand: true,
                    isMerger: false
                });

                v.parentTable = new Table({
                    container: '#addTableList',
                    height: 800,
                    rownumbers: false,
                    localData: [],
                    checkbox: true,
                    noDataMsg: "No Data",
//                    groupKey: "connect",
                    pagination: false,
                    singleSelection: false,
                    columns: [
                        { field: 'order_no', title: 'OrderNo', width: 100 },
                        { field: 'po', title: 'PONo', width: 100 },
                        { field: 'company_id', title: 'Company', width: 90 },
                        { field: 'total_pallets', title: '托盘数', width: 80 },
                        { field: 'mabd', title: 'mabd', width: 120 }
//                        ,
//                        { field: 'connect', title: 'Customer & ShipToAddress' }

                    ],
                    toolbar: [
                        {
                            text: 'Revmoe',
                            iconCls: 'glyphicon glyphicon-trash',
                            handler: function () {
                                var rows = v.parentTable.getSelected();
                                if (!rows[0]) {
                                    return;
                                }
                                if (confirm("确定删除关联?")) {
                                    console.log(rows);
                                    v.parentTable.deleteLocalData(rows, function (row) {
                                        var obj = [];
                                        var flag = true;

                                        for (var j = 0; j < v.select_data_rows.length; j++) {
                                            for (var i = 0; i < rows.length; i++) {
                                                if (row[i].order_id == v.select_data_rows[j].order_id) {
                                                    flag = false;
                                                }
                                            }
                                            if (flag) {
                                                obj.push(v.select_data_rows[j]);
                                            }
                                            else {
                                                flag = true;
                                            }
                                        }
                                        v.select_data_rows = obj;
                                        v.parentTable.reloadCurrent(true);
                                    });
                                }
                            }
                        }
                    ]
                });

                if (loadid && loadtype) {
                    v.loadid = loadid;
                    $(".createTitle").html("Edit Load");
                    $.ajax({
                        type: 'post',
                        url: config.getLoadUrl.url,
                        data: 'load_id=' + loadid,
                        success: function (data) {
                            try {
                                $("#addLoadNo").val(data.LoadNo);
                                v.parentTable.loadData(data.OrderList);
                                v.parentTable.reload(true);
                                v.select_data_rows = data.OrderList;
                            } catch (err) { }
                        },
                        error: function (e) {
                            var d = new Dialog({
                                content: "加载失败!",
                                icon: 'question',
                                lock: true,
                                width: 200,
                                height: 40,
                                title: '加载失败',
                                cancelVal: '关闭',
                                cancel: function () { }
                            }).show();
                        }
                    });


                } else {
                    $(".createTitle").html("Create Master Order");
                }
            });
        },
        render: function () {
            var v = this;
            $(".quick-search").on("click", function (e) { e.stopPropagation(); return false; });
            $("#inputQuickOrder").on('keypress', function (event) {
                if (event.keyCode == "13") {
                    v.addOrder($("#inputQuickOrder").val());
                }
            });

            $("#btnSaveInfo").on("click", function () { v.SaveInfo(); });
            $("#btnAddOrder").on("click", function () { v.addOrder(); });
        },
        SaveInfo: function () {
            var v = this;
            if (v.verification()) {
                var count = 0;
                if (v.select_data_rows) {
                    count = v.select_data_rows.length;
                }

                var h = $("<div class='bol-body form-horizontal' role='form'>"
                    + "<div class='form-group'>"
                    + "<label class='col-sm-5 control-label'>Load No</label>"
                    + "<div class='col-sm-7'>"
                    + "    <p class='form-control-static'>" + $("#addLoadNo").val() + "</p>"
                    + "</div></div>"
                    + "<div class='form-group'>"
                    + "<label class='col-sm-5 control-label'>关联Order数量</label>"
                    + "<div class='col-sm-7'>"
                    + "    <p class='form-control-static'>" + count + "</p>"
                    + "</div></div>"
                    + "</div>");

                var d = Dialog({
                    title: "确定保存",
                    width: 300,
                    height: 120,
                    lock: true,
                    opacity: 0.6,
                    content: h,
                    okValue: "保存",
                    ok: function () {
                        v.save();
                    },
                    cancelValue: '取消',
                    cancel: function () {
                    }
                });
                d.reset();
                d.showModal();
            }
        },
        addOrder: function (poNo) {
            var v = this;
            var h = $(templates.addAllOrderLoadTable().trim());
            var d = Dialog({
                title: "关联order",
                width: 800,
                height: 420,
                lock: true,
                opacity: 0.6,
                content: h,
                cancelValue: '关闭',
                cancel: function () {
                }
            });
            d.reset();
            d.showModal();

            $("#searchOrderPoNo").val(poNo);
            v.init();
            v.getOrderList();

            $("#btnOrderFilter").on("click", function () {
                v.getOrderList();
            });

        },
        getOrderList: function () {
            var v = this;

            $("#allTableList").html("");
            var table2 = new Table({
                container: '#allTableList',
                height: 220,
                url: config.orderUrl.url,
                queryParams: [{
                    company_id: $("#searchOrderCompany").val(),
                    customer_id: $("#searchOrderCustomer").val(),
                    req_start: $("#searchOrderBeginRequest").val(),
                    req_end: $("#searchOrderEndRequest").val(),
                    order_no: $("#searchOrderPoNo").val(),
                    checkIds: v.GetSelectID()
                }],
                method: "post",
//                groupKey: "connect",
                rownumbers: false,
                checkbox: true,
                singleSelection: false,
                pagination: true,
                pageSize: 50,
                columns: [
                    { field: 'order_no', title: 'OrderNo', width: 100 },
                    { field: 'po', title: 'PONo', width: 100 },
                    { field: 'company_id', title: 'Company', width: 90 },
                    { field: 'total_pallets', title: '托盘数', width: 80 },
                    { field: 'mabd', title: 'mabd', width: 120 }
//                    ,
//                    { field: 'connect', title: 'Customer & ShipToAddress'}
                ],
                toolbar: [
                    {
                        text: 'Add To Load',
                        iconCls: 'add',
                        handler: function () {
                            var rows = table2.getSelected();
                            if (!rows[0]) {
                                return;
                            }

                            if (confirm("确定关联order,关联后信息将暂不显示.")) {
                                if (v.select_data_rows == undefined || v.select_data_rows == "") {
                                    v.select_data_rows = "";
                                }
                                v.Deduplication(rows);

                                table2.reload({
                                    queryParams: [{
                                        company_id: $("#searchOrderCompany").val(),
                                        customer_id: $("#searchOrderCustomer").val(),
                                        req_start: $("#searchOrderBeginRequest").val(),
                                        req_end: $("#searchOrderEndRequest").val(),
                                        order_no: $("#searchOrderPoNo").val(),
                                        checkIds: v.GetSelectID()
                                    }]
                                });

                                v.parentTable.loadData(v.select_data_rows);
                                v.parentTable.reload(true);
                            }
                        }
                    }
                ]
            });
        },
        init: function () {
            var fromTree = new AsynLoadQueryTree({
                renderTo: "#searchOrderCompany",
                dataUrl: "./json/shipTo.json",
                scrollH: 400,
                multiselect: false,
                Async: true
            });
            fromTree.render();

            var fromTree2 = new AsynLoadQueryTree({
                renderTo: "#searchOrderCustomer",
                dataUrl: "./json/shipTo.json",
                scrollH: 400,
                multiselect: false,
                Async: true
            });
            fromTree2.render();

            $("#searchOrderBeginRequest").datetimepicker({ format: 'yyyy-mm-dd' });
            $("#searchOrderEndRequest").datetimepicker({ format: 'yyyy-mm-dd' });
        },
        Deduplication: function (rows) {
            var v = this;
            var flag = true;
            var obj = [];

            for (var i = 0; i < v.select_data_rows.length; i++) {
                obj.push(v.select_data_rows[i]);
            }

            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    if (v.select_data_rows[j] == rows[i]) {
                        flag = false;
                    }
                }
                if (flag) {
                    obj.push(rows[i]);
                }
            }
            v.select_data_rows = null;
            v.select_data_rows = obj;
        },
        RemoveRows: function (rows) {
            var v = this;
            var flag = true;
            var obj = [];

            for (var j = 0; j < v.select_data_rows.length; j++) {
                obj.push(v.select_data_rows[j]);
            }

            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    if (v.select_data_rows[j].contains(rows[i])) {
                        flag = false;
                    }
                }
                if (flag) {
                    obj.push(rows[i]);
                }
            }
            v.select_data_rows = null;
            v.select_data_rows = obj;


            console.log(obj);
        },
        GetSelectID: function () {
            var v = this;
            var ids = new Array();
            if (v.select_data_rows != null && v.select_data_rows != undefined && v.select_data_rows != "") {
                for (var j = 0; j < v.select_data_rows.length; j++) {
                    ids.push(v.select_data_rows[j].order_id);
                }
            }
            
            return ids.toString();
        },
        save: function () {
            var v = this;
            var model = new Model.LoadModel();

            model.save({
                id: v.loadid,
                loadNo: $("#addLoadNo").val(),
                orderList: v.select_data_rows
            },{
                success: function (e) {
                    if (e.changed.result != 0) {
                        var d = new Dialog({
                            content: e.changed.error,
                            icon: 'question',
                            lock: true,
                            width: 200,
                            height: 40,
                            title: '保存失败',
                            cancelVal: '关闭',
                            cancel: function () { }
                        }).show();
                    } else {
                        window.parent.postMessage("", '*');
                    }
                },
                faile: function (e) {
                    var d = new Dialog({
                        content: "保存失败,请重试!",
                        icon: 'question',
                        lock: true,
                        width: 200,
                        height: 40,
                        title: '保存失败',
                        cancelVal: '关闭',
                        cancel: function () { }
                    }).show();
                }
            });

            console.log(model);

        },
        verification: function () {
            var f = true;
            if ($("#addLoadNo").val() == "" | $("#addLoadNo").val() == undefined) {
                if (!$("#addLoadNo").parent().hasClass("has-feedback")) {
                    $("#addLoadNo").parent().addClass("has-warning has-feedback").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").end();
                    $("#addLoadNo").focus();
                }
                f = false;
            } else {
                $("#addLoadNo").parent().removeClass("has-warning").removeClass("has-feedback").find("span").remove(".form-control-feedback").end()
            }

            return f;
        },
        GetRequest: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = window.location.search.substr(1).match(reg);  //匹配目标参数
            if (r != null) return unescape(r[2]);
            return null; //返回参数值
        }
    });
});