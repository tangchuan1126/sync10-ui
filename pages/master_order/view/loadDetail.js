"use strict";
define(["jquery", "backbone", 'handlebars', "../templates/templates.amd", "compondBox"], function ($, Backbone, HandleBars, templates, CompondBox) {
	return Backbone.View.extend({
		el: null,
		template: templates.loadDetail,
        initialize: function() {
            //this.listenTo(this, "update-model", this.updateModel);
        },
        render: function () {
            var v = this;

			var temp = new CompondBox({
			    content: v.template().trim(),
			    title: "Master Order Info",
			    container: v.el,
			    isExpand: true
			});
		}
	});
});