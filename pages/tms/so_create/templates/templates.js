(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Pallets'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"row\">\r\n	<div class=\"col-xs-8 \" id=\"condition\">\r\n	\r\n	</div>\r\n	<div class=\"col-xs-4\" id=\"searchArea\">\r\n	</div>\r\n	\r\n</div>";
  },"useData":true});
templates['Search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "";
},"useData":true});
templates['SoBasicInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "<div class=\"row form-inline\">\r\n    <div class=\"form-group col-xs-3\">\r\n        <input type=\"hidden\" id=\"soId\" value=\""
    + escapeExpression(((helper = (helper = helpers.SO_ID || (depth0 != null ? depth0.SO_ID : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"SO_ID","hash":{},"data":data}) : helper)))
    + "\"/>\r\n	    <label for=\"shipFrom\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.ShipFrom : stack1), depth0))
    + "</label>\r\n	    <input type=\"text\" class=\"form-control\" id=\"shipFrom\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.SHIPPER_WHS_NAME : stack1), depth0))
    + "\" data-val=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.SHIPPER_WHS_ID : stack1), depth0))
    + "\" readonly/>\r\n    </div>\r\n    <div class=\"form-group col-xs-3\">\r\n	    <label for=\"shipTo\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.ShipTo : stack1), depth0))
    + "</label>\r\n        <div class=\"\" style=\"display:inline\">\r\n	       <input type=\"text\" class=\"form-control\" id=\"shipTo\" placeholder=\"To WHS\" value=\""
    + escapeExpression(((helper = (helper = helpers.CONSIGNEE_WHS_NAME || (depth0 != null ? depth0.CONSIGNEE_WHS_NAME : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"CONSIGNEE_WHS_NAME","hash":{},"data":data}) : helper)))
    + "\" data-val=\""
    + escapeExpression(((helper = (helper = helpers.CONSIGNEE_WHS_ID || (depth0 != null ? depth0.CONSIGNEE_WHS_ID : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"CONSIGNEE_WHS_ID","hash":{},"data":data}) : helper)))
    + "\" >\r\n           <span class=\"error\" title=\"Please select ship to whs\"></span>\r\n        </div>  \r\n    </div>\r\n    <div class=\"form-group col-xs-3\">\r\n	    <label for=\"deadLine\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.DeadLine : stack1), depth0))
    + "</label>\r\n         <div class=\"\" style=\"display:inline\">\r\n	       <!-- <input type=\"text\" class=\"form-control\" id=\"deadLine\" placeholder=\"DeadLine\"\r\n            value=\""
    + escapeExpression(((helpers.formatDate || (depth0 && depth0.formatDate) || helperMissing).call(depth0, (depth0 != null ? depth0.DEADLINE : depth0), {"name":"formatDate","hash":{},"data":data})))
    + "\"> -->\r\n            <input type=\"text\" class=\"form-control\" id=\"deadLine\" placeholder=\"DeadLine\"\r\n            value=\""
    + escapeExpression(((helper = (helper = helpers.DEADLINE || (depth0 != null ? depth0.DEADLINE : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"DEADLINE","hash":{},"data":data}) : helper)))
    + "\">\r\n            <span class=\"error\" title=\"Please pick a time\"></span>\r\n        </div> \r\n    </div>\r\n     <div class=\"form-group col-xs-3\">\r\n     	 <a title=\"Save basic info\" class=\"tabel-tool-icon btn\" href=\"javascript:void(0)\" id=\"saveBasicInfoBtn\"><i class=\"glyphicon glyphicon-ok\" ></i></a>\r\n	    <a title=\"Edit basic info\" class=\"tabel-tool-icon btn\" href=\"javascript:void(0)\" id=\"editBasicInfoBtn\"><i class=\"glyphicon glyphicon-cog\"></i></a>\r\n    </div>\r\n </div>";
},"useData":true});
})();