define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Pallets'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"row\">\n	<div class=\"col-xs-8 \" id=\"condition\">\n	\n	</div>\n	<div class=\"col-xs-4\" id=\"searchArea\">\n	</div>\n	\n</div>";
},"useData":true});
templates['Search'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "";
},"useData":true});
templates['SoBasicInfo'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression, alias4=this.lambda;

  return "<div class=\"row form-inline\">\n    <div class=\"form-group col-xs-3\">\n        <input type=\"hidden\" id=\"soId\" value=\""
    + alias3(((helper = (helper = helpers.SO_ID || (depth0 != null ? depth0.SO_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"SO_ID","hash":{},"data":data}) : helper)))
    + "\"/>\n	    <label for=\"shipFrom\">"
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.ShipFrom : stack1), depth0))
    + "</label>\n	    <input type=\"text\" class=\"form-control\" id=\"shipFrom\" value=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.SHIPPER_WHS_NAME : stack1), depth0))
    + "\" data-val=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.SHIPPER_WHS_ID : stack1), depth0))
    + "\" readonly/>\n    </div>\n    <div class=\"form-group col-xs-3\">\n	    <label for=\"shipTo\">"
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.ShipTo : stack1), depth0))
    + "</label>\n        <div class=\"\" style=\"display:inline\">\n	       <input type=\"text\" class=\"form-control\" id=\"shipTo\" placeholder=\"To WHS\" value=\""
    + alias3(((helper = (helper = helpers.CONSIGNEE_WHS_NAME || (depth0 != null ? depth0.CONSIGNEE_WHS_NAME : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CONSIGNEE_WHS_NAME","hash":{},"data":data}) : helper)))
    + "\" data-val=\""
    + alias3(((helper = (helper = helpers.CONSIGNEE_WHS_ID || (depth0 != null ? depth0.CONSIGNEE_WHS_ID : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"CONSIGNEE_WHS_ID","hash":{},"data":data}) : helper)))
    + "\" >\n           <span class=\"error\" title=\"Please select ship to whs\"></span>\n        </div>  \n    </div>\n    <div class=\"form-group col-xs-3\">\n	    <label for=\"deadLine\">"
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.DeadLine : stack1), depth0))
    + "</label>\n         <div class=\"\" style=\"display:inline\">\n	       <!-- <input type=\"text\" class=\"form-control\" id=\"deadLine\" placeholder=\"DeadLine\"\n            value=\""
    + alias3((helpers.formatDate || (depth0 && depth0.formatDate) || alias1).call(depth0,(depth0 != null ? depth0.DEADLINE : depth0),{"name":"formatDate","hash":{},"data":data}))
    + "\"> -->\n            <input type=\"text\" class=\"form-control\" id=\"deadLine\" placeholder=\"DeadLine\"\n            value=\""
    + alias3(((helper = (helper = helpers.DEADLINE || (depth0 != null ? depth0.DEADLINE : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"DEADLINE","hash":{},"data":data}) : helper)))
    + "\">\n            <span class=\"error\" title=\"Please pick a time\"></span>\n        </div> \n    </div>\n     <div class=\"form-group col-xs-3\">\n     	 <a title=\"Save basic info\" class=\"tabel-tool-icon btn\" href=\"javascript:void(0)\" id=\"saveBasicInfoBtn\"><i class=\"glyphicon glyphicon-ok\" ></i></a>\n	    <a title=\"Edit basic info\" class=\"tabel-tool-icon btn\" href=\"javascript:void(0)\" id=\"editBasicInfoBtn\"><i class=\"glyphicon glyphicon-cog\"></i></a>\n    </div>\n </div>";
},"useData":true});
return templates;
});