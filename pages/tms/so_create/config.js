define({
	chooseSoPallets:{
		url:"/Sync10-ui/pages/tms/so_choose_pallets/index.html"
	},
	soPallets:{
		url:"/Sync10/_tms/so/getSOPallets"
	},
    saveSOBasicInfo:{
    	url:"/Sync10/_tms/so/saveSOBasicInfo"
    },
    updateSO:{
		url:"/Sync10/_tms/so/modifySOBasicInfo"
	},
    deleteSOPallets:{
    	url:"/Sync10/_tms/so/deleteSOPallets"
    },

	createSO: {
		url:"/Sync10-ui/pages/tms/so_create/index.html"
	},
	getSOBasicInfo:
	{
		url:"/Sync10/_tms/so/getSOBasicInfo"
	},
	searchURl: {
		url:"./json/so.data.json"
	},
	BOLURl: {
		url:"./json/so.bol.json"
	},
	shipFrom: {
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	shipTo: {
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	status: {
		url: "./json/so.status.json"
	},
	adminSesion: {
		url:"/Sync10/_tms/admin/getLoginAdmin"
	}
});