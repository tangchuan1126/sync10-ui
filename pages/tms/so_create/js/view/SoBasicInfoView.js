define(["jquery","backbone","templates","../../config",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"../../MsgHelp",
"bootstrap.datetimepicker","../../../handlebar_ext"],
	function($,Backbone,templates,config,AsynLoadQueryTree,MsgHelp) {
	return Backbone.View.extend({
		el: "#basicInfo",
		template: templates.SoBasicInfo,
		count:0,
		initialize:function(options) {
			if (options) {
				this.so_id = options.so_id;
				this.eventsMgr =options.eventsMgr;
			}
		},
		events:{
			"click #saveBasicInfoBtn" : "saveBasicInfo",
			"click #editBasicInfoBtn" : "eidt",
		},

		
		render: function(data)
		{
			var soI18n = window.regional['defaultLanguage'];
			var v = this;
			v.$el.html(v.template({soI18n:soI18n,data:data}));
			$("#editBasicInfoBtn").hide();
			//$("#saveBasicInfoBtn").hide();
			$("#deadLine").keydown(function(){ return false;});
			$("#deadLine").datetimepicker({format:'mm/dd/yyyy hh:ii',language:'en'});

			var shipToTree = new AsynLoadQueryTree({
				renderTo: "#shipTo",
				//selectid: {value: data.CONSIGNEE_WHS_ID},
				PostData: {rootType:"Ship To"},
				dataUrl: config.shipFrom.url,
				scrollH: 400,
				multiselect: false,
				Parentclick:true,
				Async: true,
				placeholder:"Ship To"
			});
			shipToTree.render();
			$("#shipTo").keydown(function(event) {return false;});

			//如果是修改模式,则先disabled掉
			if(data && data.SO_ID != null && data.SO_ID != undefined )
			{
				this.editBasicInfo(false);
			}
			return this;
		},
		validate:function()
		{
			var shipFrom = $("#shipFrom").attr("data-val");
			if(shipFrom == "")
			{
			  alert("please login!");
			}
		
			this._errorInfo( $("#shipTo"),this.isEmpty($("#shipTo").val()),"");
			this._errorInfo( $("#deadLine"),this.isEmpty($("#deadLine").val()),"");
		},
		isEmpty:function(str)
		{
			 return str==undefined||str==null||$.trim(str)=="";
		},
		valiResult:function()
		{
		   return !($("#basicInfo").find('.has-error').length > 0);
		},
		_errorInfo:function(obj,display,title)
		{
			if(display)
			{
				obj.parent().addClass('has-error');
			}
			else
			{
				obj.parent().removeClass('has-error');
			}
			
			if(title != "")
			{
				obj.next().attr("title",title);
			}
		},
		saveBasicInfo:function()
		{
			var that = this;
			this.validate();
			if(this.valiResult())
			{
				var soId = $("#soId").val();
				if(this.isEmpty(soId))
				{
					that.addSOBasicInfo();
				}
				else
				{
					//modify so
					this.modifySOBasicInfo();
				}
				
			}
		},
		addSOBasicInfo:function()
		{
			var that = this;
			var shipFrom = $("#shipFrom").attr("data-val");
			var shipTo = $("#shipTo").attr("data-val");
			var deadLine = $("#deadLine").val();
			$.ajax({
					url: config.saveSOBasicInfo.url,
					type: 'POST',
					dataType: 'JSON',
				    //contentType:"application/json; charset=UTF-8",
					data: {"shipFrom":shipFrom,"shipTo":shipTo,"deadLine":deadLine},
				})
				.done(function(soId) 
				{
					MsgHelp.showMessage("Save success!","succeed");
					if(soId != null && soId != undefined && soId != "")
					{
						$("#soId").val(soId);
						that.editBasicInfo(false);
						if(that.count == 0 && that.so_id == -1)
						{
							that.eventsMgr.trigger('showPalletsInfo');
							that.count =1;
						}
					}
				})
				.fail(function() 
				{
					MsgHelp.showMessage("Save failed!","error");
				});
		},
		modifySOBasicInfo:function()
		{	
			var that = this;
			var so_id = $("#soId").val();
			var shipFrom = $("#shipFrom").attr("data-val");
			var shipTo = $("#shipTo").attr("data-val");
			var deadLine = $("#deadLine").val();
			$.ajax({
					url: config.updateSO.url,
					type: 'POST',
					dataType: 'JSON',
				    //contentType:"application/json; charset=UTF-8",
					data: {"so_id":so_id,"shipFrom":shipFrom,"shipTo":shipTo,"deadLine":deadLine},
				})
				.done(function(count) 
				{
					if(count > 0 )
					{
						MsgHelp.showMessage("Modify success!","succeed");
						that.editBasicInfo(false);
					}
				})
				.fail(function() 
				{
					MsgHelp.showMessage("Modify failed!","error");
				});
		},
		eidt:function()
		{
		  this.editBasicInfo(true);
		},
		editBasicInfo:function(isEdit)
		{
			if(isEdit)
			{
				$("#shipTo").attr('disabled', false);
				$("#deadLine").attr('disabled', false);
				$("#saveBasicInfoBtn").show();
				$("#editBasicInfoBtn").hide();
			}
			else
			{
				$("#shipTo").attr('disabled', true);
				$("#deadLine").attr('disabled', true);
				$("#saveBasicInfoBtn").hide();
				$("#editBasicInfoBtn").show();
			}
			
		},
		
		isSave:function()
		{
			return $("#saveBasicInfoBtn").css("display")=="none";
		},
	});
});