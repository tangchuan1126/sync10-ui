define(["jquery","backbone","templates","../../config","oso.lib/table/js/table","../../MsgHelp",
	"/Sync10-ui/lib/slidePanel/js/slidePanel.js","artDialog","require_css!oso.lib/slidePanel/css/style.css"],
	function($,Backbone,templates,config,Table,MsgHelp,SlidePanel) {
	var soI18n = window.regional['defaultLanguage'];	
	return Backbone.View.extend({
		el: "#palletsInfo",
		template: templates.Pallets,
		eventsMgr:{},
		initialize:function(options) {
			if (options) {
				this.so_id = options.so_id;
				this.eventsMgr =options.eventsMgr;
				this.ps_id=options.ps_id;
				if(this.so_id !== -1)
				{
				   
				   $("#palletsInfo-title").show();
				   $("#palletsInfo").show();
				   $("#foot").show();
				}
			}
		},
		events:
		{
			"click #addBtn" : "addPallets"
		},
		preRender:function()
		{
			var v = this;
			//v.$el.html(v.template({}));
		},
		render: function(data)
		{
			var that = this;
			this.basicInfo = data;
			this.preRender(); 
			this.initPalletsTable();
			this.eventsMgr.on("showPalletsInfo",function(){
				$("#palletsInfo-title").slideDown('slow', function()
				{
					$("#palletsInfo").show('slow', function() 
					{
						$("#foot").fadeIn('slow', function(){});
					});
				});
			});
			return this;
		},
		getBasicInfo:function(v)
		{
			return{
			so_id:$("#soId").val(),
			shipFrom:v.ps_id,
			shipTo:$("#shipTo").val()
			};
		},
		showPalletsInfo:function()
		{
			var that = this;
			$("#palletsInfo-title").slideDown('slow', function() {
				$("#palletsInfo").show('slow', function() {
					$("#foot").fadeIn('slow', function() {
					});
				});
			});
		},
		initPalletsTable:function()
		{
			var that = this;
			var table = new Table({
			container : '#palletsInfo',
			//url : "../data/palltes.json",
			url:config.soPallets.url,
			//title : 'DATA LIST',
			//height: 250,
			queryParams:{"SO_ID":that.so_id,"type":9},
			rownumbers : false,
			singleSelection : false,
			pagination: true,
			pageSize:50,
			pageNumber:1,
			pageList:["50","100","200","300","400","500"],
			checkbox : true,
			isEnable:function(row){
                if(row.HANDLE_STATUS == 0 || row.HANDLE_STATUS =="0" ){
                    return true;
                }else
                {
                	 return false;
                }
            },
			datasName: 'DATA',
			columns:[
				//{field:'B2B_DETAIL_ID',title:'ID',width:10,hidden:true},
				{field:'ID',title:soI18n.NO,width:100},
				{field:'TO_WHS_NAME',title:soI18n.ShipTo,width:120},
				{field:'WEIGHT',title:soI18n.Weight,width:100},
				{field:'REQUESTDAY',title:soI18n.RequestDay,width:100},
				{field:'TYPE',title:soI18n.Type,width:100, formatter:function(data, row){
					if(data==1)
					{
						return "Package";
					}else{return "Pallet"}
				}},
			],
			toolbar : 
			[
	            {
	                text :soI18n.add,iconCls : 'add',
	                btnClass : (this.basicInfo && this.basicInfo.STATUS !=1) ?'hide':'',
	                handler : function()
	                {
	                	var isSave = $("#editBasicInfoBtn").css("display")=='none'?false:true;
	                	if(isSave)
	                	{
	                		   var data = that.getBasicInfo(that);
			                    var uri=config.chooseSoPallets.url+"?SO_ID="+data.so_id+"&shipTo="+data.shipTo+"&shipFrom="+data.shipFrom;
								
								/*artDialog.open(uri , {id:"edit",sta:{},title: "Choose Pallets & Packages",width:'950px',height:'550px',
								 lock: true,
								 opacity: 0.3,
								 fixed: true,
									close:function()
									{
										var soId = $("#soId").val();
										table.reload({
											"queryParams":{SO_ID:soId,type:9}
											});

										//that.param.eventsMgr.trigger('briefInfoUpdate',{name:"aa"});
									}
								});*/
                                var slide = new SlidePanel({
			                       title: soI18n.ChoosePalletsAndPackages,
			                       url: uri,
			                       duration: 500,
			                       topLine: 0,
			                       slideLine: "85%"	
			          
			            		 });
	                                slide.open();



	                	}
	                	else
	                	{
	                		MsgHelp.showMessage("Please save SO basic info first !","error");
	                	}
	                }
	            },
	            {
	                text : soI18n.deleteBtn,iconCls : 'remove',
	               btnClass : (this.basicInfo && this.basicInfo.STATUS !=1) ?'hide':'',
	                handler : function()
	                {
	                    var rows = table.getSelected();
	                    if(rows.length > 0)
	                    {
	                    	 $.ajax({
	                    	url: config.deleteSOPallets.url,
	                    	type: 'POST',
	                    	dataType: 'json',
	                    	contentType:"application/json; charset=UTF-8",
	                    	data: JSON.stringify({"SO_ID":$("#soId").val(),"pallets":rows}),
		                    })
		                    .done(function() {
		                    	  table.reload({});
		                    })
		                    .fail(function() {
		                    	console.log("error");
		                    });
	                    }
	                }
	            },
	            {
	                //text : 'Pallets<span class="badge" id="totalPallets">0</span>',
	                 text : soI18n.Pallets,
	                handler:function()
	                {
	                	table.reload({
	                         "queryParams":{SO_ID:$("#soId").val(),type:0}
	                         });
	                }
	            },
	            {
	                //text : 'Packages<span class="badge" id="totalPackages">0</span>',
	                text :soI18n.Packages,
	                handler:function()
	                {
	                	table.reload({
		                	"queryParams":{SO_ID:$("#soId").val(),type:1}
		                	});
	                }
	            },
	            {
	                //text : 'Packages<span class="badge" id="totalPackages">0</span>',
	                text :soI18n.Unfinished,
	                handler:function()
	                {
	                	table.reload({
		                	"queryParams":{SO_ID:$("#soId").val(),status:0}
		                	});
	                }
	            },
	            {
	                //text : 'Packages<span class="badge" id="totalPackages">0</span>',
	                text :soI18n.Finished,
	                handler:function()
	                {
	                	table.reload({
		                	"queryParams":{SO_ID:$("#soId").val(),status:1}
		                	});
	                }
	            },
	            {
	                //text : 'All<span class="badge" id="all">0</span>',
	                 text :soI18n.All,
	                handler:function(){
	                   table.reload({
	                         "queryParams":{SO_ID:$("#soId").val(),type:9},
	                    });
	                }
	            }
       		],
       		loadSuccess:function()
			{
				this.refreshWidth();

				//if(that.so_id != -1)
				//{
				    //修改模式 马上展示
				//	$("#palletsInfo").show();
				//}
			}
			});
			this.dataTable = table;
		},
	});
});