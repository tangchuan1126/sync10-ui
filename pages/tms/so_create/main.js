"use strict";
require(["../../../requirejs_config","./config"], function($,config) {
	require(["require_css!Font-Awesome"]);
	var so_language =  './i18n/so-' + window.navigator.languages[0];
	require(["jquery","./js/view/SoBasicInfoView","./js/view/PalletsView","artDialog",so_language],
	function($,SoBasicInfoView,PalletsView,Dialog){
		var soI18n = window.regional['defaultLanguage'];
		   $.ajax({
			type: "GET",
			url: config.adminSesion.url,
			dataType: "json",
			timeout: 30000,
			error: function(XHR,textStatus,errorThrown) 
			{
				if (textStatus==401) {
					var aretr = Dialog({
						title: '错误',
						width: 150,
						content: '请确认已经登陆系统！'
					});
					aretr.showModal();
				} else {
					var aretr = Dialog({
						title: '服务端状态',
						width: 150,
						content: '请求出错！'
					});
					aretr.showModal();
				}
			},
			success: function(data,textStatus) 
			{
				$("#soBasicInfo").text(soI18n.soBasicInfo);
				$("#returnBtn").text(soI18n.returnBtn);
				$("#palletsInfo").text(soI18n.palletsInfo);
				$("#saveBtn").text(soI18n.saveBtn);
				if (textStatus=="success") 
				{
					var ps_id = data.ps_id;
				}
			

		   function getQueryString(name) {
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			}
			var so_id = getQueryString("so_id");
		    var eventsAcrossView =  _.extend({}, Backbone.Events);
		    var basicInfoView = {};
		    var palletsInfoView = {};
		    var param ={};
		    if(so_id == "" || so_id ==undefined)
		    {
		        //新增
		       $.getJSON('/Sync10/_tms/admin/getPsName',  function(json, textStatus) {
			       if(textStatus == "success")
			       {
			       		param = {eventsMgr:eventsAcrossView,"so_id":-1,"ps_id":ps_id};
			    		basicInfoView = new SoBasicInfoView(param).render({"SHIPPER_WHS_NAME":json.ps_name,"SHIPPER_WHS_ID":json.ps_id});
						palletsInfoView = new PalletsView(param).render();
					}
		       });
		    }
		    else
		    {
		       //修改
		    	param ={eventsMgr:eventsAcrossView,"so_id":so_id,"ps_id":ps_id};
		    	$.getJSON(config.getSOBasicInfo.url, {"so_id": so_id}, function(json, textStatus) 
		    	{
		    		console.log(json);
		    		basicInfoView = new SoBasicInfoView(param).render(json.DATA);
					palletsInfoView = new PalletsView(param).render(json.DATA);
		    	});
		    }
			
			
	  	window.onresize = function(event){
	 		 window.parent.eventsMgr.trigger('resize');
		};
		
		$("#returnBtn,#saveBtn").click(function(event) {
			if(basicInfoView.isSave())
			{
				window.parent.eventsMgr.trigger('renderSoList');
			}
			else
			{
				if(confirm("You have not save!"))
				{
					if(so_id == "" || so_id ==undefined)
					{
					    //新增完毕,刷新列表
						window.parent.eventsMgr.trigger('renderSoList');
					}
					else
					{
						//修改完毕刷新行记录
						window.parent.eventsMgr.trigger('renderOneSO',so_id);
					}
					
				}
			}
		});
		}
	});
});
});