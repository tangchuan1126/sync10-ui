define({
    "1": {
        "en": "Create ",
        "zh": "创建主单据"
    },
    "2": {
        "en": "Update",
        "zh": "修改记录"
    },
    "3": {
        "en": "Appointment",
        "zh": "预约"
    },
    "9": {
        "en": "Delete",
        "zh": "删除记录"
    },
});