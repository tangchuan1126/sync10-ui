define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['condition'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div id=\"choose\" class=\"panel panel-default\">\n    <div class=\"panel-body \">\n \n        <div class=\"form-inline\">\n            <div class=\"input-group\">\n                 <span class=\"input-group-addon form-label\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.RequestDay : stack1), depth0))
    + "</span>\n                 <input class=\"form-control hours\" type=\"number\"  min=\"0\" step=\"1\" id=\"requestDay_min\" placeholder="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Day : stack1), depth0))
    + ">\n                 <span class=\"input-group-addon to\" >~</span>\n                 <input  class=\"form-control hours\"  type=\"number\" min=\"0\" step=\"1\" id=\"requestDay_max\" placeholder="
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Day : stack1), depth0))
    + ">\n            </div>\n            <div class=\"input-group\">\n                 <span class=\"input-group-addon form-label\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.ShipTo : stack1), depth0))
    + "</span>\n                 <input type=\"text\" class=\"form-control\" placeholder=\"\" id=\"shipTo\" value=\"\">\n            </div>\n            <div class=\"input-group\" >\n               <button type=\"button\" class=\"btn btn-primary\" id=\"search\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Search : stack1), depth0))
    + "</button>\n            </div>\n            <div class=\"input-group\" >\n               <button type=\"button\" class=\"btn btn-primary\" id=\"addSoFinish\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Submit : stack1), depth0))
    + "</button>\n            </div>  \n        </div>\n    </div>\n</div>";
},"useData":true});
templates['result'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id=\"data\" ><div id=\"palletsContainer\"></div></div>\n</div>\n";
},"useData":true});
return templates;
});