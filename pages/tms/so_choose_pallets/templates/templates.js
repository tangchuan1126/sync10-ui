(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['condition'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div id=\"choose\" class=\"panel panel-default\">\r\n    <div class=\"panel-body \">\r\n \r\n        <div class=\"form-inline\">\r\n            <div class=\"input-group\">\r\n                 <span class=\"input-group-addon form-label\" >"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.RequestDay : stack1), depth0))
    + "</span>\r\n                 <input class=\"form-control hours\" type=\"number\"  min=\"0\" step=\"1\" id=\"requestDay_min\" placeholder="
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Day : stack1), depth0))
    + ">\r\n                 <span class=\"input-group-addon to\" >~</span>\r\n                 <input  class=\"form-control hours\"  type=\"number\" min=\"0\" step=\"1\" id=\"requestDay_max\" placeholder="
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Day : stack1), depth0))
    + ">\r\n            </div>\r\n            <div class=\"input-group\">\r\n                 <span class=\"input-group-addon form-label\" >"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.ShipTo : stack1), depth0))
    + "</span>\r\n                 <input type=\"text\" class=\"form-control\" placeholder=\"\" id=\"shipTo\" value=\"\">\r\n            </div>\r\n            <div class=\"input-group\" >\r\n               <button type=\"button\" class=\"btn btn-primary\" id=\"search\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Search : stack1), depth0))
    + "</button>\r\n            </div>\r\n            <div class=\"input-group\" >\r\n               <button type=\"button\" class=\"btn btn-primary\" id=\"addSoFinish\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.soI18n : depth0)) != null ? stack1.Submit : stack1), depth0))
    + "</button>\r\n            </div>  \r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
templates['result'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"data\" ><div id=\"palletsContainer\"></div></div>\r\n</div>\r\n";
  },"useData":true});
})();