;( function( w ) {
	w.regional = {};
	w.regional['en-US'] = {
	    /* So add */	
			soBasicInfo:'So Basic Info',	
			returnBtn:'Return',	
			palletsInfo:'Pallets Info',	
			saveBtn:'Finish',
			ShipTo:'ShipTo',
			DeadLine:'DeadLine',
			
			add:'add',
			All:'All',
			Finished:'Finished',
			Unfinished:'Unfinished',
			Packages:'Packages',
			Pallets:'Pallets',
			deleteBtn:'delete',
			NO:'NO.',
			RequestDay:'Request Day',
			Weight:'Weight(LBS)',
			Type:'Type',
			RequestDay:'Request Day',
			Day:'Day',
			Search:'Search',
			State:'State',
			Submit:'Submit',
			Waybill:'Waybill',
			TotalPalletsAndPackages:'Total Pallets & Packages',
			ShipFrom:'ShipFrom'
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'en-US' ];

}( window ) )