;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
	    /* So add */	
			soBasicInfo:'发货计划基本信息',	
			returnBtn:'返回',	
			palletsInfo:'货物信息',	
			saveBtn:'保存',	
			ShipFrom:'始发地',
			ShipTo:'目的地',
			DeadLine:'期限',
			add:'添加',
			All:'全部',
			Finished:'已完成',
			Unfinished:'未完成',
			Packages:'包裹',
			Pallets:'托盘',
			deleteBtn:'删除',
			NO:'编号.',
			RequestDay:'到达天数',
			Weight:'重量',
			RequestDay:'到达天数',
			Day:'天',
			Search:'查询',
			State:'状态',
			Submit:'提交',
			Waybill:'运单编号',
			TotalPalletsAndPackages:'全部托盘和包裹',
			Type:'类型'
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )