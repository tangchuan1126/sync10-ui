define(
{
    shipTo: {
        url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
    },
    addPalletsToSO:
    {
         url:"/Sync10/_tms/so/batchAddPalletsToSO"
    },
    searchPallets: 
    {
        //url:"/Sync10-ui/pages/bol/data/source.json"
        //url:"/Sync10/_b2b/transportOrder/filterForManage"
         url:"/Sync10/_tms/so/getPalletsListByWhs"
    },
    searchBol:
    {
         url:"/Sync10-ui/pages/bol/data/bol_detail.json"
    },
    keySearchURL: {
        url:"/Sync10/_b2b/transportOrder/filterDefaultForSend"
    },
    searchDefaultURL:
    {
        url:"/Sync10-ui/pages/temp/transport_manage.json"
         // url:"/Sync10/_b2b/transportOrder/filterDefaultForManage.json"
    },
 
    session:
    {
        url:"/Sync10/_b2b/admin/session"
    },
    createSO: {
        url:"/Sync10-ui/pages/tms/so_create/index.html"
        //url:"/Sync10-ui/pages/tms/so_add/index.html"
    }
});