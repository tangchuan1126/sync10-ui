define(["jquery","backbone","../../config","../../templates",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree","artDialog"],
function($,Backbone,config,templates,AsynLoadQueryTree,artDialog) {
	return Backbone.View.extend(
	{
			el:"#condition",
			template:templates.condition,
			model:new (Backbone.Model.extend({url:config.searchBol.url}))(),
			initialize:function(options)
			{
				this.resultView =options.resultView;
				this.condition = options.condition;
				this.model.set({
				"SO_ID":this.condition.SO_ID,
				"shipTo":this.condition.shipTo,
				"shipFrom":this.condition.shipFrom
				});
			},
			events:
			{
				"click #search" : "search",
				"click #addSoFinish":"addSoFinish"
			},
			preRender:function()
			{
				var soI18n = window.regional['defaultLanguage'];
				this.$el.html(this.template({data:this.model.toJSON(),soI18n:soI18n}));
			},
			isEmpty:function(str)
			{
				return str == "" || str == null || str == undefined ||str =="undefined" || str == "null";
			},
			render:function()
			{
				this.preRender();

				var toTree = new AsynLoadQueryTree({
				renderTo: "#shipTo",
				PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 400,
				multiselect: false,
				Parentclick:true,
				Async: true,
				placeholder:"Ship To"
				});
				toTree.render();


				//this.search();
			},
			search:function()
			{
				var requestDay_min = $("#requestDay_min").val();
				var requestDay_max = $("#requestDay_max").val();
				var shipTo = $("#shipTo").attr("data-val");
				this.resultView.render({"shipTo":shipTo,"shipFrom":this.model.get("shipFrom"),"requestDay_min":requestDay_min,"requestDay_max":requestDay_max});
			},
			addSoFinish:function()
			{
			    var rows = this.resultView.table.getSelected();
                 //console.info(rows);
	            this.resultView.addToSO(rows);
				var _docuel=window.parent.document;
				var slide_contentbox=_docuel.querySelector(".slide-wrap");
     			var slide_modal=_docuel.querySelector(".slide-modal");
				slide_contentbox.style.display="none";
      			slide_modal.style.display="none"; 
      			window.parent.parent.eventsMgr.trigger("replaceCurrentPage",config.createSO.url+"?so_id="+this.condition["SO_ID"]);
			}
						
	});

}
);