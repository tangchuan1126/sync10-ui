define(["jquery","backbone","../../config","../../templates","/Sync10-ui/lib/PalletsControls/Pallets.js","oso.lib/table/js/table","artDialog"],
function($,Backbone,config,templates,pallets,Table) {
	var soI18n = window.regional['defaultLanguage'];
	return Backbone.View.extend(
	{
			el:"#result",
			template:templates.result,
			//model:new FilterModel(),
			queryCondition:{},
			initialize:function(options)
			{
			    this.queryCondition = options.condition;
			},
			events:
			{
				"click #selectAll" : "selectAll",
				"click #quickSelect" : "quickSelect",
				"click .confirm" : "confirm"
			},
			initTable:function()
			{
				var that = this;
	            var table = new Table({
				container : '#result',
				//url : "../data/palltes.json",
				url:config.searchPallets.url,
				height: 300,
				rownumbers : false,
				singleSelection : false,
				pagination: false,
				checkbox : true,
				pagination:false,
				queryParams:this.queryCondition,
				method:"POST",
				groupKey:"PARENTNAME",
				pageList: ["20","30","50","100","200"],
				datasName: 'DATA',
				columns:[
					//{field:'B2B_DETAIL_ID',title:'ID',width:10,hidden:true},
					{field:'PARENTNAME',title:soI18n.State,width:100,align:"center"},
					{field:'PSNAME',title:soI18n.ShipTo,width:100,align:"center"},
					{field:'COUNT',title:soI18n.TotalPalletsAndPackages,width:100,align:"center"},
					{field:'SO_ID',title:soI18n.Waybill,width:100,align: 'center'}
				],
				/*toolbar : [
	            {
	                text : 'Add to SO',
	                iconCls : 'add',
	                handler : function(){
	                console.log(this);
	                   var rows = table.getSelected();
	                  that.addToSO(rows);
	                }
	            }
	       		]   */         
				});
				this.table=table;
			},
			addToSO:function(rows)
			{
			    var that = this;
			  
				if(rows && rows.length > 0)
				{
					var ids ="";
					for(var i=0,len=rows.length;i<len;i++)
					{
						ids = ids + rows[i].PSID+",";
					}
					ids = ids.substring(0,ids.length -1 );
				   var so_id = this.queryCondition["SO_ID"];
				   var condition = {"so_id":so_id,"current_whs_id":this.queryCondition.shipFrom,"ids":ids};
				   if(that.seachCondition != undefined && that.seachCondition != null)
				   {
					   var requestDay_min = this.seachCondition.requestDay_min;
					   var requestDay_max = this.seachCondition.requestDay_max;
					   if(!that.isEmpty(requestDay_min))
					   {
					   	 condition.requestDay_min=requestDay_min;
					   }
					   if(!that.isEmpty(requestDay_max))
					   {
					   	 condition.requestDay_max=requestDay_max;
					   }
				   	}

				  $.ajax({
				  	url: config.addPalletsToSO.url,
				  	type: 'POST',
				  	async:false,
				  	contentType:"application/json; charset=UTF-8",
				  	dataType: 'json',
				  	data: JSON.stringify(condition),
				  })
				  .done(function() {
				  	that.render();
				  })
				  .fail(function() {
				  	console.log("error");
				  });
				  
				}
			},
			isEmpty:function(str)
			{
				str = $.trim(str);
				return str =="" || str == undefined || str == null || str=="null";
			},
			render:function(condition)
			{
				var v = this;
				//v.$el.html(v.template({}));
				if(condition!= null && condition != undefined )
				{
					this.seachCondition = condition;
					this.table.reload({queryParams:condition});
				}
				else
				{
					this.table.reload({});
				}
				
			},
			//更新概括信息
			updateInfo:function()
			{
				$("#selectedVolume").html(this.selectedVolume+"m3");
				$("#selectedWeight").html(this.selectedWeight+"kg");
				$("#selectedPallets").html(this.selectedPallets);
				$("#selectedPackages").html(this.selectedPackages);
			},
			resetInfo:function()
			{
				this.selectedVolume = 0;
				this.selectedWeight = 0;
				this.selectedPallets = 0;
				this.selectedPackages = 0;
			},
			
			//确定事件
			confirm:function()
			{
				
			}
	});

}
);