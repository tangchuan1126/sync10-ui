"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
	var so_language =  './i18n/so-' + window.navigator.languages[0];
		require(["jquery",
			"./js/view/conditionView",
			"./js/view/resultView",
			"bootstrap",so_language],
			function($,ConditionView,ResultView)
			{
				function getQueryString(name) {
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var condition = {
			    	"SO_ID":getQueryString("SO_ID"),
			    	"shipFrom":getQueryString("shipFrom")
			    	};
			  	var resultView = new ResultView({"condition":condition});
			  		resultView.initTable();
			  	new ConditionView({"resultView":resultView,"condition":condition}).render();
			});
	});