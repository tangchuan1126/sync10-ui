define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['fromAddress'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "						<option value=\""
    + alias2(alias1((depth0 != null ? depth0.C_ID : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.C_NAME : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel-body form-horizontal\" id=\"formAddressForm\">\n    <div class=\"form-group\">\n		<label for=\"etd\" class=\"col-xs-3 control-label required\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</label>\n		<div class=\"col-xs-6\">\n			<input type=\"text\" class=\"form-control\" id=\"etd\" value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.param : depth0)) != null ? stack1.bolInfo : stack1)) != null ? stack1.ETD : stack1), depth0))
    + "\"/>\n		</div>\n	</div>\n    <div class=\"form-group\">\n        <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Warehouse : stack1), depth0))
    + "</label>\n        <div class=\"col-xs-6\">\n           <input type=\"text\" class=\"form-control\"   placeholder=\"From Wharehourse\" id=\"send_psid\" />\n        </div>  \n    </div>\n    <div class=\"form-group\">\n      <label for=\"send_country\" class=\"col-xs-3 control-label \">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Country : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <select id=\"send_country\"  class=\"form-control\">\n"
    + ((stack1 = helpers.blockHelperMissing.call(depth0,alias1(((stack1 = (depth0 != null ? depth0.param : depth0)) != null ? stack1.country : stack1), depth0),{"name":"param.country","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</select>\n      </div>\n   </div>\n    <div class=\"form-group\">\n      <label for=\"send_state\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.State : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <select id=\"send_state\"  class=\"form-control\">\n			   </select>\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"send_city\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.City : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n          <select id=\"send_city\"  class=\"form-control\">\n          </select>\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"send_address\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Address : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"send_address\" />\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"send_zipcode\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.ZipCode : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"send_zipcode\"/> \n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"send_linkman\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Contact : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"send_linkman\" />\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"send_linkman_phone\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Phone : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"send_linkman_phone\" />\n      </div>\n   </div>\n   \n</div> \n<div class=\"footer\">\n	<div class=\"opbar\">\n		<button type=\"button\" id=\"fromAddress_next\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Next : stack1), depth0))
    + "</button>\n	</div>	\n</div>\n";
},"useData":true});
templates['preview'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<br/>\n<br/>\n<div id=\"formToInfo\" >\n		<div class=\"col-sm-6\">\n			<div class=\"panel panel-info\">\n				<div class=\"panel-heading\">\n			      "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.From : stack1), depth0))
    + "  : "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_whs : stack1), depth0))
    + "\n			    </div>\n			   <div class=\"panel-body\">\n					<address>\n						"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_address : stack1), depth0))
    + "<br>\n						"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_city_name : stack1), depth0))
    + ", "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_state_name : stack1), depth0))
    + " ,"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_country_name : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_zipCode : stack1), depth0))
    + "<br>\n						<strong><abbr title=\"Phone\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.Phone : stack1), depth0))
    + ":</abbr> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_linkman_phone : stack1), depth0))
    + "</strong\n					</address>\n\n					<address>\n						 <strong>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.send_linkman : stack1), depth0))
    + "</strong><br>\n						 <strong>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ETD : stack1), depth0))
    + ":"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.fromAddress : depth0)) != null ? stack1.etd : stack1), depth0))
    + "</strong>\n					</address>\n				</div>	\n			</div>\n		</div>\n		<div class=\"col-sm-6\">\n			<div class=\"panel panel-info\">\n			   <div class=\"panel-heading\">\n			     "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.To : stack1), depth0))
    + "  : "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_whs : stack1), depth0))
    + "\n			   </div>\n			   <div class=\"panel-body\">\n					<address>\n						"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_address : stack1), depth0))
    + "<br>\n						"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_city_name : stack1), depth0))
    + ", "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_state_name : stack1), depth0))
    + " ,"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_country_name : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_zipCode : stack1), depth0))
    + "<br>\n						<strong><abbr title=\"Phone\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.Phone : stack1), depth0))
    + ":</abbr> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_linkman_phone : stack1), depth0))
    + "</strong\n					</address>\n\n					<address>\n						 <strong>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.receive_linkman : stack1), depth0))
    + "</strong><br>\n						 <strong>MABD:"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.toAddress : depth0)) != null ? stack1.mabd : stack1), depth0))
    + "</strong>\n					</address>\n			   </div>\n			</div>\n		</div>\n</div>\n<div class=\"opbar\">\n		<button type=\"button\" id=\"preview_pre\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.Prev : stack1), depth0))
    + "</button>\n	<button type=\"button\" id=\"save\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.Save : stack1), depth0))
    + "</button>\n</div>";
},"useData":true});
templates['toAddress'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "            <option value=\""
    + alias2(alias1((depth0 != null ? depth0.C_ID : depth0), depth0))
    + "\">"
    + alias2(alias1((depth0 != null ? depth0.C_NAME : depth0), depth0))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel-body form-horizontal\" id=\"toAddressForm\">\n    <div class=\"form-group\">\n		<label for=\"mabd\" class=\"col-xs-3 control-label required\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.MABD : stack1), depth0))
    + "</label>\n		<div class=\"col-xs-6\">\n			<input type=\"text\" class=\"form-control\" id=\"mabd\" value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.param : depth0)) != null ? stack1.bolInfo : stack1)) != null ? stack1.MABD : stack1), depth0))
    + "\" />\n		</div>\n	</div>\n    <div class=\"form-group\">\n        <label for=\"receive_psid\"class=\"col-xs-3 control-label required\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Warehouse : stack1), depth0))
    + "</label>\n        <div class=\"col-xs-6\">\n          <input type=\"text\" class=\"form-control \" placeholder=\"To Wharehourse\" id=\"receive_psid\">\n        </div>  \n    </div>\n    <div class=\"form-group\">\n      <label for=\"receive_country\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Country : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <select id=\"receive_country\"  class=\"form-control\">\n"
    + ((stack1 = helpers.blockHelperMissing.call(depth0,alias1(((stack1 = (depth0 != null ? depth0.param : depth0)) != null ? stack1.country : stack1), depth0),{"name":"param.country","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			</select>\n      </div>\n   </div>\n    <div class=\"form-group\">\n      <label for=\"receive_state\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.State : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <select id=\"receive_state\"  class=\"form-control\">\n			   </select>\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"receive_city\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.City : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n          <select id=\"receive_city\"  class=\"form-control\">\n          </select>\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"receive_address\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Address : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"receive_address\" />\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"receive_zipcode\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.ZipCode : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"receive_zipcode\"/> \n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"receive_linkman\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Contact : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"receive_linkman\" />\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"receive_linkman_phone\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Phone : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n         <input type=\"text\" class=\"form-control\" id=\"receive_linkman_phone\" />\n      </div>\n   </div>\n</div>  \n<div class=\"footer\">\n	<div class=\"opbar\">\n		<button type=\"button\" id=\"toAddress_pre\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Prev : stack1), depth0))
    + "</button>\n		<button type=\"button\" id=\"toAddress_next\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.bolI18n : depth0)) != null ? stack1.Preview : stack1), depth0))
    + "</button>\n	</div>\n</div>";
},"useData":true});
return templates;
});