define(["jquery","backbone","../../config",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"../../templates",
"../../MsgHelp",
"jqueryui/datepicker",
"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,AsynLoadQueryTree,templates,MsgHelp) {
	var bolI18n = window.regional['defaultLanguage'];
	return Backbone.View.extend(
	{
			el:"#toAddress",
			template:templates.toAddress,
			//model:new FilterModel(),
			initialize:function(options)
			{
				this.initParam = options;
			},
			events:
			{
				"click #filter" : "filter",
				"click #toAddress_pre":"preStep",
				//"click #toAddress_next":"nextStep"
			},
			render:function(param)
			{
				var that = this;
				that.$el.html(that.template({param:param,bolI18n:bolI18n}));
             	//$("#mabd").datetimepicker({ format: 'mm/dd/yyyy' , autoclose:1, minView: 2, forceParse: 0 ,startDate:new Date()});
				var option = {
				    	format: 'mm/dd/yyyy',
				    	autoclose:1,
				        minView: 2,
		       			bgiconurl:"",
				        forceParse: 0 
				    };
				$("#mabd").datetimepicker(option)
	    		.on('changeDate', function(ev){
	    			var startTime = $('#mabd');
	    			startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
	    		});
	        	var selectedid = "";
            	var selectedName = "";
	        	if(this.initParam.op=="edit")
	        	{
	        		//修改的时候 设置默认值
	        		that.initAddressInfo(param.bolInfo.RECEIVE_PSID);   
	        		selectedid = param.bolInfo.RECEIVE_PSID;
	        		selectedName = param.bolInfo.CONSIGNEE_WHS_NAME;
	        	}
	        	else
	        	{
	        		that.initAddressInfo(param.soInfo.CONSIGNEE_WHS_ID);   
	        		selectedid = param.soInfo.CONSIGNEE_WHS_ID;
	        		selectedName = param.soInfo.CONSIGNEE_WHS_NAME;
	        		
	        	}
	        	that.initShiToTree(selectedid,selectedName);
				 return this;

			},
			initShiToTree:function(selectid,selectedName)
			{
				var that = this;
				var toTree = new AsynLoadQueryTree({
				renderTo: "#receive_psid",
				//selectid: {value: v.shipFrom},
				PostData: {rootType:"Ship From"},
				dataUrl: config.shipFrom.url,
				scrollH: 400,
				multiselect: false,
				Parentclick:true,
				Async: true,
				placeholder:"To Wharehourse"
				});
				//toTree.render();
				toTree.on("events.Itemclick",function(treeId,treeNode)
				{                   
	               that.initAddressInfo(treeNode.data);              
	        	});

	        	$("#receive_psid").attr('data-val',selectid);
	        	$("#receive_psid").val(selectedName);
			},
			initAddressInfo:function(ps_id)
			{
				var that = this;
				$.ajax({
					url: config.getWhsInfo.url,
					type: 'GET',
					dataType: 'json',
					data: {"ps_id": ps_id},
				})
				.done(function(data) {
					//选择国家
					$("#receive_country").val(data.DELIVER_NATION);
					//$("#receive_city").val(data.DELIVER_CITY);
					//初始化省
					that.initState(data.DELIVER_NATION,data.DELIVER_PRO_ID);
					//初始化市
					that.initCity(data.DELIVER_PRO_ID,data.DELIVER_CITY_ID);

					$("#receive_address").val(data.DELIVER_ADDRESS);
					$("#receive_zipcode").val(data.DELIVER_ZIP_CODE);
					$("#receive_linkman").val(data.DELIVER_CONTACT);
					$("#receive_linkman_phone").val(data.DELIVER_PHONE);

				})
				.fail(function() {
					console.log("error");
				});
			},
			initState:function(ca_id,selectid)
			{
			  console.log(ca_id,selectid);
				$.ajax({
					url: config.getAreaList.url,
					type: 'GET',
					dataType: 'json',
					data: {"AREA_ID": ca_id},
				})
				.done(function(states) 
				{
					var options = $("#receive_state")[0].options;
					options.length=0;
					$.each(states.DATA, function(index, obj) {
						 var temOption  = new Option(obj.NAME,obj.ID);
						 if(obj.ID==selectid)
						 {
						 	temOption.selected = true;
						 }
						options.add(temOption);
					});
				})
				.fail(function() {
					console.log("error");
				});
				
			},
			initCity:function(c_id,selectid)
			{
				$.ajax({
					url: config.getAreaList.url,
					type: 'GET',
					dataType: 'json',
					data: {"AREA_ID": c_id},
				})
				.done(function(citys) 
				{
				 console.log(selectid);
					var options = $("#receive_city")[0].options;
					options.length=0;
					$.each(citys.DATA, function(index, obj) {
						 var temOption  = new Option(obj.NAME,obj.ID);

						 if(obj.ID == selectid)
						 {
						 	temOption.selected = true;
						 }
						 options.add(temOption);
					});
				})
				.fail(function() {
					console.log("error");
				});
			},
			getData:function()
			{
				return {
				"receive_psid":$("#receive_psid").attr('data-val'),
				"receive_whs":$("#receive_psid").val(),
				"receive_country":$("#receive_country").val(),
				"receive_country_name":$("#receive_country").find("option:selected").text(),
				"receive_state":$("#receive_state").val(),
				"receive_state_name":$("#receive_state").find("option:selected").text(),
				"receive_city":$("#receive_city").val(),
				"receive_city_name":$("#receive_city").find("option:selected").text(),
				"receive_address":$("#receive_address").val(),
				"receive_zipcode":$("#receive_zipcode").val(),
				"receive_linkman":$("#receive_linkman").val(),
				"receive_linkman_phone":$("#receive_linkman_phone").val(),
				"mabd":$("#mabd").val()
				};
			},
			validate:function()
			{
				var data = this.getData(); 
				this._errorInfo($("#receive_psid"),this.isEmpty(data.receive_psid),"");
				this._errorInfo($("#receive_country"),this.isEmpty(data.receive_country),"");
				this._errorInfo($("#receive_state"),this.isEmpty(data.receive_state),"");
				this._errorInfo($("#receive_city"),this.isEmpty(data.receive_city),"");
				this._errorInfo($("#receive_address"),this.isEmpty(data.receive_address),"");
				this._errorInfo($("#receive_zipcode"),this.isEmpty(data.receive_zipcode),"");
				this._errorInfo($("#receive_linkman"),this.isEmpty(data.receive_linkman),"");
				this._errorInfo($("#receive_linkman_phone"),this.isEmpty(data.receive_linkman_phone),"");
				this._errorInfo($("#mabd"),this.isEmpty(data.mabd),"");
				return this.checkResult();
			},
			isEmpty:function(val)
			{
				return val== undefined || val == null || $.trim(val) == "";
			},
			checkResult:function()
			{
				var res = $("#toAddressForm").find('.has-error').length > 0 ?false:true
				if(!res)
				{
					$("#toAddressLink").parent().find('span').removeClass().addClass('checkerror');
					MsgHelp.showMessage("Please modify the errors !","error");
				}
				else
				{
					$("#toAddressLink").parent().find('span').removeClass().addClass('checked');
				}
				return res;
			},
			_errorInfo:function($obj,display,info)
			{
				$obj.parent().removeClass('has-error');
				if(display)
				{
					$obj.parent().addClass('has-error');
				}
			},
			preStep:function()
			{
				$('#tabs li:eq(0) a').tab('show') ;
			},
			nextStep:function()
			{
				//$('#previewLink').css({display: 'block'}).tab('show');
			},


	});

}
);