define(["jquery","backbone","../../config","../../templates","art_Dialog/dialog-plus","jqueryui/datepicker"],
function($,Backbone,config,templates,Dialog) {
	return Backbone.View.extend(
	{
			el:"#preview",
			template:templates.preview,
			//model:new FilterModel(),
			initialize:function()
			{
				
			},
			events:
			{
				"click #save" : "save",
				"click #preview_pre":"preview_pre"
			},
			render:function(options)
			{
				if(options != null)
				{
					this.param = options;
					this.fromAddress = options.fromAddress;
					this.toAddress = options.toAddress;
				}
				var that = this;
				//for(var key in this.addressData)
				//{
				//	this.addressData[key] = this.addressData[key].val();
				//}
				var bolI18n = window.regional['defaultLanguage'];
				this.$el.html(this.template({
					fromAddress:this.fromAddress,
					toAddress:this.toAddress,
					lanModel:bolI18n
					//palletsData:this.palletsData
				}));

			
				 
			},
			preview_pre:function()
			{
				$('#tabs li:eq(1) a').tab('show') ;
			},

			save:function()
			{
				this.getData();
				if(this.param.op=="add")
				{
					this.addBol();
				}
				else
				{
					this.updateBol();
				}
			},
			addBol:function()
			{	
				var data = $.extend(this.getData(),this.param.soInfo);
				$.ajax({
					url: config.saveBol.url,
					type: 'POST',
					contentType:"application/json; charset=UTF-8",
					dataType: 'json',
					data: JSON.stringify(data),
				})
				.done(function(bol_id) {
					if(bol_id != "" && bol_id!=undefined && bol_id != null && bol_id!="null")
					{
						//artDialog.data("returnData",bol_id);
					
						//artDialog.close();
						var dialog = top.dialog;
						dialog.close(bol_id).remove();
					}
				})
				.fail(function() {
					console.log("error");
				});
			},
			updateBol:function()
			{
				var data = $.extend(this.getData(),this.param.bolInfo);
				$.ajax({
					url: config.updateBol.url,
					type: 'POST',
					contentType:"application/json; charset=UTF-8",
					dataType: 'json',
					data: JSON.stringify(data),
				})
				.done(function(bol_id) {
					if(bol_id != "" && bol_id!=undefined && bol_id != null && bol_id!="null")
					{
						//artDialog.data("returnData",bol_id);
						//artDialog.opener.postMessage(obj["Palletdata"],"*");
						//artDialog.close();
						var dialog = top.dialog;
						dialog.close("...");//.remove();
					}

				})
				.fail(function() {
					console.log("error");
				});
			},
			getData:function()
			{
				var allData = $.extend({},this.fromAddress , this.toAddress,this.param);
				return data={
					"send_psid" : allData.send_psid,
					"send_country" : allData.send_country,
					"send_country_name" : allData.send_country_name,
					"send_state" : allData.send_state,
					"send_state_name" : allData.send_state_name,
					"send_city" : allData.send_city,
					"send_city_name" : allData.send_city_name,
					"send_address" : allData.send_address,
					"send_zipcode" : allData.send_zipcode,
					"send_linkman" : allData.send_linkman,
					"send_linkman_phone" : allData.send_linkman_phone,
					"etd" : allData.etd,
					"receive_psid" : allData.receive_psid,
					"receive_country" : allData.receive_country,
					"receive_country_name" : allData.receive_country_name,
					"receive_state" : allData.receive_state,
					"receive_state_name" : allData.receive_state_name,
					"receive_city" : allData.receive_city,
					"receive_city_name" : allData.receive_city_name,
					"receive_address" : allData.receive_address,
					"receive_zipcode" : allData.receive_zipcode,
					"receive_linkman" : allData.receive_linkman,
					"receive_linkman_phone" : allData.receive_linkman_phone,
					"mabd" : allData.mabd
				};
			}

	});

}
);