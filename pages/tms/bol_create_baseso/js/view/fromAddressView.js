define(["jquery","backbone","../../config",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"../../templates",
"../../MsgHelp",
"bootstrap.datetimepicker",
"../../../handlebar_ext",
"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,AsynLoadQueryTree,templates,MsgHelp) {
	var bolI18n = window.regional['defaultLanguage'];
	return Backbone.View.extend(
	{
			el:"#fromAddress",
			template:templates.fromAddress,
			//model:new FilterModel(),
			initialize:function(options)
			{	
				this.initParam = options;
			},
			events:
			{
				"click #filter" : "filter",
				"click #fromAddress_next":"nextStep"
			},
			render:function(param)
			{
				var that = this;
				//$("#fromAddress").html(that.template({"country":param.country}));
				that.$el.html(that.template({param:param,bolI18n:bolI18n}));
				var option = {
				    	format: 'mm/dd/yyyy',
				    	autoclose:1,
				        minView: 2,
		       			bgiconurl:"",
				        forceParse: 0 
				    };
				$("#etd").datetimepicker(option)
	    		.on('changeDate', function(ev){
	    			var startTime = $('#etd');
	    			startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
	    		});
				
			

            	var selectedid = "";
            	var selectedName = "";
	        	if(this.initParam.op=="edit")
	        	{
	        		console.log(param.bolInfo);
	        		//修改的时候 设置默认值
	        		that.initAddressInfo(param.bolInfo.SEND_PSID);   
	        		selectedid = param.bolInfo.SEND_PSID;
	        		selectedName = param.bolInfo.SHIPPER_WHS_NAME;
	        	}
	        	else
	        	{
	        		that.initAddressInfo(param.soInfo.SHIPPER_WHS_ID);   
	        		selectedid = param.soInfo.SHIPPER_WHS_ID;
	        		selectedName = param.soInfo.SHIPPER_WHS_NAME;
	        		
	        	}

	        	that.initFromTree(selectedid,selectedName);
				return this;
			},
			initFromTree:function(selectid,selectedName)
			{
				var that = this;
				var fromTree = new AsynLoadQueryTree({
				renderTo: "#send_psid",
				//selectid: {value: v.shipFrom},
				PostData: {rootType:"Ship From"},
				dataUrl: config.shipFrom.url,
				selectid: selectid,
				scrollH: 400,
				multiselect: false,
				Parentclick:true,
				Async: true,
				placeholder:"From Wharehourse"
				});
				fromTree.render();
				fromTree.on("events.Itemclick",function(treeId,treeNode)
				{                   
	               that.initAddressInfo(treeNode.data);              
	        	});

	        	$("#send_psid").attr('data-val',selectid);
	        	$("#send_psid").val(selectedName);
			},
			initAddressInfo:function(ps_id)
			{
				var that = this;
				$.ajax({
					url: config.getWhsInfo.url,
					type: 'GET',
					dataType: 'json',
					data: {"ps_id": ps_id},
				})
				.done(function(data) {
					//选择国家
					$("#receive_country").val(data.SEND_NATION);
					//$("#receive_city").val(data.DELIVER_CITY);
					//初始化省
					that.initState(data.SEND_NATION,data.SEND_PRO_ID);
					//初始化市
					that.initCity(data.SEND_PRO_ID,data.SEND_CITY_ID);

					$("#send_address").val(data.ADDRESS);
					$("#send_zipcode").val(data.SEND_ZIP_CODE);
					$("#send_linkman").val(data.SEND_CONTACT);
					$("#send_linkman_phone").val(data.SEND_PHONE);

				})
				.fail(function() {
					console.log("error");
				});
			},
			initState:function(ca_id,selectid)
			{
				$.ajax({
					url: config.getAreaList.url,
					type: 'GET',
					dataType: 'json',
					data: {"AREA_ID": ca_id},
				})
				.done(function(states) 
				{
					var options = $("#send_state")[0].options;
					options.length=0;
					options.add(new Option("Please select",""));
					$.each(states.DATA, function(index, obj) {
						 var temOption  = new Option(obj.NAME,obj.ID);
						 if(obj.ID==selectid)
						 {
						 	temOption.selected = true;
						 }
						options.add(temOption);
					});
				})
				.fail(function() {
					console.log("error");
				});
				
			},
			initCity:function(c_id,selectid)
			{
				$.ajax({
					url: config.getAreaList.url,
					type: 'GET',
					dataType: 'json',
					data: {"AREA_ID": c_id},
				})
				.done(function(citys) 
				{
					var options = $("#send_city")[0].options;
					options.length=0;
					options.add(new Option("Please select",""));
					$.each(citys.DATA, function(index, obj) {
						 var temOption  = new Option(obj.NAME,obj.ID);

						 if(obj.ID == selectid)
						 {
						 	temOption.selected = true;
						 }
						 options.add(temOption);
					});
				})
				.fail(function() {
					console.log("error");
				});
			},

			getData:function()
			{
				return {
				"send_psid":$("#send_psid").attr('data-val'),
				"send_whs":$("#send_psid").val(),
				"send_country":$("#send_country").val(),
				"send_country_name":$("#send_country").find("option:selected").text(),
				"send_state":$("#send_state").val(),
				"send_state_name":$("#send_state").find("option:selected").text(),
				"send_city":$("#send_city").val(),
				"send_city_name":$("#send_city").find("option:selected").text(),
				"send_address":$("#send_address").val(),
				"send_zipcode":$("#send_zipcode").val(),
				"send_linkman":$("#send_linkman").val(),
				"send_linkman_phone":$("#send_linkman_phone").val(),
				"etd":$("#etd").val()
				};
			},

			validate:function()
			{
				var data = this.getData(); 
				this._errorInfo($("#send_psid"),this.isEmpty(data.send_psid),"");
				this._errorInfo($("#send_country"),this.isEmpty(data.send_country),"");
				this._errorInfo($("#send_state"),this.isEmpty(data.send_state),"");
				this._errorInfo($("#send_city"),this.isEmpty(data.send_city),"");
				this._errorInfo($("#send_address"),this.isEmpty(data.send_address),"");
				this._errorInfo($("#send_zipcode"),this.isEmpty(data.send_zipcode),"");
				this._errorInfo($("#send_linkman"),this.isEmpty(data.send_linkman),"");
				this._errorInfo($("#send_linkman_phone"),this.isEmpty(data.send_linkman_phone),"");
				this._errorInfo($("#etd"),this.isEmpty(data.etd),"");
				return this.checkResult();
			},
			isEmpty:function(val)
			{
				return val== undefined || val == null || $.trim(val) == "";
			},
			checkResult:function()
			{
				var res = $("#formAddressForm").find('.has-error').length > 0 ?false:true
				if(!res)
				{
					$("#fromAddressLink").parent().find('span').removeClass().addClass('checkerror');
					MsgHelp.showMessage("Please modify the errors !","error");
				}
				else
				{
					$("#fromAddressLink").parent().find('span').removeClass().addClass('checked');
				}
				return res;
			},
			_errorInfo:function($obj,display,info)
			{
				$obj.parent().removeClass('has-error');
				if(display)
				{
					$obj.parent().addClass('has-error');
				}
			},
			nextStep:function()
			{
				$('#tabs li:eq(1) a').tab('show') ;
			},

	});

}
);