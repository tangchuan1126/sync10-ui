"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
	var bol_language =  './i18n/bol-' + window.navigator.languages[0];
		require(["jquery",
			"./js/view/fromAddressView",
			"./js/view/toAddressView",
			"./js/view/preview",
			"bootstrap",bol_language],
			function($,FromAddressView,ToAddressView,Preview)
			{
			var bolI18n = window.regional['defaultLanguage'];
			$("#fromAddressLink").text(bolI18n.FromAddress);
			$("#toAddressLink").text(bolI18n.ToAddress);
			$("#previewLink").text(bolI18n.Preview);
				function getQueryString(name) 
				{
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var so_id = getQueryString("so_id");
			    var bol_id = getQueryString("bol_id");
			    var op = "add";
			    if(bol_id != ""  && bol_id != undefined && bol_id != null)
			    {
			    	op = "edit";
			    }

			    var initParam ={"so_id":so_id,"bol_id":bol_id,"op":op};

				var preview = new Preview();
				var fromAddressInfo = new FromAddressView(initParam);
				var toAddressInfo = new ToAddressView(initParam);

				var addPrevEvent = function()
				{
					$("#toAddress_next").on('click',  function(event) 
					{
						var res = fromAddressInfo.validate();
						res = toAddressInfo.validate() && res;
						console.log(res);
						if(res)
						{
							//所有页签校验成功，展示预览页面
							$('#previewLink').css({display: 'block'}).tab('show');
						}
						else
						{
							//MsgHelper.showMessage("Please modify the error !","error");
						}
					});
				}

				$.getJSON('/Sync10/_tms/bol/getCountry',  function(country, textStatus) 
				{
			    	if(op=="edit")
				    {
				    	//修改,取BOL基本信息
				    	new (Backbone.Model.extend({"url":config.bolDetail.url}))().fetch({
						data:{"bol_id":bol_id},
						success:function(data)
						{
							var param = {
								"bolInfo" : data.toJSON(),
								"country" : country
							};
							 fromAddressInfo.render(param);
							 toAddressInfo.render(param);
							 addPrevEvent();
						}});
				    }
				    else
				    {
				    	//新增的时候，默认为sofrom一致
				    	new (Backbone.Model.extend({"url":config.soInfo.url}))().fetch({
						data:{"so_id":so_id},
						success:function(data)
						{
							//新增
					    	var param = {
									"soInfo" : data.toJSON().DATA,
									"country" : country
								};
					    	 fromAddressInfo.render(param);
							 toAddressInfo.render(param);
							  addPrevEvent();
						}});
				    	
				    }
					 

				});

				

				$('a[data-toggle="tab"]').on('show.bs.tab', function (e) 
				{
					//上一步，校验，但是不阻止
					//下一步，当前页签必须校验成功
					var prev = $(e.target).attr("data-value") > $(e.relatedTarget).attr("data-value")?false:true;
					var result = false;
					if(e.relatedTarget.id =="fromAddressLink")
					{
						result = fromAddressInfo.validate() ;
					}
					else if(e.relatedTarget.id =="toAddressLink")
					{
						result = toAddressInfo.validate(); 
					}else
					{
						result = true;
					}

					if(e.target.id =="previewLink")
					{
						//preview.render({addressData:addressInfo.getData(),palletsData:palletsInfo.getData()});
						var data = {"fromAddress":fromAddressInfo.getData(),
							"toAddress":toAddressInfo.getData()
							};
							if(op=="add")
							{
								data.op = "add";
								data.soInfo = {"type":"1","so_id":so_id};
							}
							if(op == "edit")
							{
								data.op = "edit";
								data.bolInfo = {"bol_id":bol_id};
							}
						preview.render(data);
					}
					else
					{
						$("#previewLink").css({display: 'none'});
					}
					
					if(!result)
					{
						if(!prev )
						{
							return e.preventDefault();
						}
					}
					else
					{
						//...
					}


				   //e.target // 激活的标签页
				   //e.relatedTarget // 前一个激活的标签页
				});


		});
	});