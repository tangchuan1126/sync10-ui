;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
			FromAddress:"发货地址",
			ToAddress:"收货地址",
			Preview:"预览",
			ETD:"预计到达时间",
			Warehouse:"仓库名称",
			Country:"国家",
			State:"省份",
			City:"城市",
			Address:"地址",
			ZipCode:"邮政编码",
			Contact:"联系人",
			Phone:"电话",
			Next:"下一步",
			MABD:"必须到达日期",
			Prev:"上一步",
			Save:"保存",
			From:"发货仓库",
			To:"收货仓库"
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )