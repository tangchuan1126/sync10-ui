;( function( w ) {
	w.regional = {};
	w.regional['en-US'] = {
			FromAddress:"From Address",
			ToAddress:"To Address",
			Preview:"Preview",
			ETD:"ETD",
			Warehouse:"Warehouse",
			Country:"Country",
			State:"State",
			City:"City",
			Address:"Address",
			ZipCode:"ZipCode",
			Contact:"Contact",
			Phone:"Phone",
			Next:"Next",
			MABD:"MABD",
			Prev:"Prev",
			Save:"Save",
			From:"From",
			To:"To"
			
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'en-US' ];

}( window ) )