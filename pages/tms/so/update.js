"use strict";
require(["../../../requirejs_config","./config"], function($,config) {
	require([
		"bootstrap",
		"require_css!oso.lib/bootstrap/css/bootstrap.min.css",
		"require_css!oso.lib/bootstrap/css/plugins/timeline.css",
		"require_css!Font-Awesome",
		"require_css!oso.lib/PalletsControls/css/style.css",
		"require_css!jqueryui-css",
		"jqueryui/i18n/datepicker-zh-CN",
		"jqueryui/i18n/datepicker-en-AU"
		//
	]);
	//jquery-ui.css jquery-ui-timepicker-addon.css
	require(["jquery","./js/view/DetailTab",'oso.lib/PalletsControls/Pallets','jqueryui/datepicker','dateUtil',"artDialog","oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"],
		function($,DetailTab,Pallets,datepicker,dateUtil,artDialog,AsynLoadQueryTree) {
			var request = {
				QueryString : function(val) {
					var uri = window.location.search;
					var re = new RegExp("" +val+ "=([^&?]*)", "ig");
					return ((uri.match(re))?(uri.match(re)[0].substr(val.length+1)):null);
				}
			}
			//var temp = new DetailTab();
			//temp.render();
			//console.log(request.QueryString("id"));
			var _selectData = [];
			var totlePallets = 0;
			var totlePackages = 0;
			var totleWeight = 0.0;
			var totleVolume = 0.0;
			function format_json(json){
				if (jQuery.isArray(json)) {
					totlePallets = 0;
					totlePackages = 0;
					totleWeight = 0.0;
					totleVolume = 0.0;
					$.each(json, function(index, objVal) {
						if (!objVal.title) {
							objVal.title=[];
						}
						if (objVal.TYPE) {
							objVal.title=[];
							objVal.title.push("Palletid:"+objVal.id+"");
							totlePallets+=1;
						} else {
							totlePackages+=1;
						}
						if (objVal.DEADLINE) {
							objVal.title.push("Deadline:"+ objVal.DEADLINE);
						}
						if (objVal.WEIGHT) {
							totleWeight+= objVal.WEIGHT;
						}
						if (objVal.VOLUME) {
							totleVolume+= objVal.VOLUME;
						}
						if (objVal.SHIPTO) {
							objVal.title.push("ShipTo:"+ objVal.SHIPTO);
						}
						//objVal.index = index;
					});
				}
				//console.log(json);
				return json;
			}
			function _cal() {
				var SelectPallets = 0;
				var SelectPackages = 0;
				var SelectWeight = 0.0;
				var SelectVolume = 0.0;
				$.each(_selectData, function(index, objVal) {
					if (objVal.TYPE) {
						SelectPallets+=1;
					} else {
						SelectPackages+=1;
					}
					if (objVal.WEIGHT) {
						SelectWeight+= objVal.WEIGHT;
					}
					if (objVal.VOLUME) {
						SelectVolume+= objVal.VOLUME;
					}
				});
				$(".SPallets").html("Select Pallets:"+SelectPallets);
				$(".SPackages").html("Select Packages:"+SelectPackages);
				$(".SWeight").html("Select Weight:"+SelectWeight);
				$(".SVolume").html("Select Volume:"+SelectVolume);
			}
			function format_json2(json){
				//console.log(json);
				return json;
			}
			var nowString = dateUtil.formatDate(new Date());
			$("#shipTo").change(function(){
				//console.log(11);
			});
			function contains(a,par,obj) {
				var i = a.length;
				while (i--) {
					if (a[i][par] === obj) {
						return i;
					}
				}
				return -1;
			}
			function getParameters() {
				var paramters = "DeadlineMax=" + $("#DeadlineMax").val();
				if ($("#DeadlineMin").val()!='') {
					paramters += "&DeadlineMin="+ $("#DeadlineMin").val();
				}
				if ($("#shipTo").val()!='') {
					paramters += "&shipTo="+ $("#shipTo").val();
				}
				return paramters;
			}
			if (dateUtil.getLanguage()=="zh") {
				$.datepicker.setDefaults($.datepicker.regional[ "zh-CN" ]);
				nowString = nowString.substring(0,10);
			} else {
				$.datepicker.setDefaults($.datepicker.regional[ "en" ]);
				nowString = nowString.substring(0,10);
			}
			$("#mabd").datepicker({minDate: new Date(),maxDate:"+30d",defaultDate: "+0"});//new Date()
			$("#plantime").datepicker({minDate:new Date(),maxDate:"+30d",defaultDate: "+0"});
			$("span.mabd").eq(0).click(function(){
				$("#mabd").datepicker('show');
				/*
				data-date="2013-02-21T15:25:00Z"
				({
					format: "dd/MM/yyyy hh:ii",
					autoclose: true,
					todayBtn: true,
					startDate: "02/14/2013 00:00",
					minuteStep: 10
				});
				*/
			});
			$("span.plantime").eq(0).click(function(){
				$("#plantime").datepicker('show');
			});
			var toTree = new AsynLoadQueryTree({
				renderTo: "#shipTo",
				selectid: {value: '0'},
				PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 400,
				multiselect: false,
				Async: true,
				placeholder:"Ship To"
			});
			toTree.render();
			var p_list = new Pallets({
				renderTo: ".Express",
				dataUrl: {url:"./json/d.json?"+getParameters(),formatjson:format_json},
				tabledataurl:{url:"./json/table.json?{id}",formatjson:format_json2},
				scrollH: 200,//最高200超出会滚动
				columns:1,//每行的列数，默认流式布局
				colstyle:"col-md-11" //可以数字也可以col-md-*默认是col-md-3
			});
			p_list.on("Initialize",function(opt){
				$("#sall1").click(function(){
					var _this=$(this);
					var dftcheck=_this.prop("checked");
					p_list.command("ALLchecked",dftcheck);
				});
				$("#LPallets").html("Pallets:"+totlePallets);
				$("#LPackages").html("Packages:"+totlePackages);
			});
			$("#Search").click(function(){
				if($("#DeadlineMax").val()=="") {
					$("#DeadlineMax").parent().addClass("has-error");
					return false;
				} else {
					$("#DeadlineMax").parent().removeClass("has-error");
				}
				p_list = new Pallets({
					renderTo: ".Express",
					dataUrl: {url:"./json/d.json?"+getParameters(),formatjson:format_json},
					tabledataurl:{url:"./json/table.json?{id}",formatjson:format_json2},
					scrollH:200,columns:1,colstyle:"col-md-11"
				});
				p_list.on("Initialize",function(opt){
					$("#sall1").click(function(){
						var _this=$(this);
						var dftcheck=_this.prop("checked");
						p_list.command("ALLchecked",dftcheck);
					});
					$("#LPallets").html("Pallets:"+totlePallets);
					$("#LPackages").html("Packages:"+totlePackages);
				});
			});
			$("#sall2").click(function(){
				var _this=$(this);
				var dftcheck=_this.prop("checked");
				var toptitlebody = $("#selectDetail").find(".toptitlebody");
				for (var fj = 0; fj < toptitlebody.length; fj++) {
					var itmebody = toptitlebody.eq(fj);
					var titlecheck = itmebody.find("input[type=checkbox]");
					if (titlecheck.length > 0) {
						titlecheck.eq(0).prop("checked",dftcheck);
					}
				}
			});
			$("#btn_gt").click(function(){
				var toptitlebody = $("#listDetail").find(".toptitlebody");
				for (var fj = 0; fj < toptitlebody.length; fj++) {
					var itmebody = toptitlebody.eq(fj);
					var titlecheck = itmebody.find(":checked");
					if (titlecheck.length > 0) {
						var Palletsbox = titlecheck.parents(".Palletsbox").eq(0);
						var ti = $("#selectDetail").find("#"+Palletsbox[0].id+"_sel");
						if (ti.length==0) {
							$("<div id='"+Palletsbox[0].id+"_sel' class='col-md-12 Palletsbox'><div class=\"pallets_title borderbottomw0px\">"+Palletsbox.find(".pallets_title").eq(0).html().replace("fa-toggle-down","fa-close").replace("fa-toggle-up","fa-close")+"</div></div>").appendTo("#selectDetail");
						}
					}
				}
				var selecteds = p_list.command("titlecheckboxall");
				$.each(selecteds.Palletdata, function(index, objVal) {
					if (contains(_selectData,"id",objVal.id)<0) {
						_selectData.push(objVal);
					}
				});
				
				$("#selectDetail").find(".fa-close").unbind("click").click(function(){
					var _tid = $(this).parents(".Palletsbox").eq(0).attr("id").split(/_/)[1];
					var ti = contains(_selectData,"id",_tid);
					if (ti>=0) {
						_selectData.splice(ti,1);
					}
					$(this).parents(".Palletsbox").eq(0).remove();
					_cal();
					//console.log(_selectData);
				});
				_cal();
			});
			$("#btn_lt").click(function(){
				var toptitlebody = $("#selectDetail").find(".toptitlebody");
				var ids = [];
				for (var fj = 0; fj < toptitlebody.length; fj++) {
					var itmebody = toptitlebody.eq(fj);
					var titlecheck = itmebody.find(":checked");
					if (titlecheck.length > 0) {
						//ids.push(titlecheck.parents(".Palletsbox").eq(0).attr("id").split(/_/)[1]);
						var _tid = titlecheck.parents(".Palletsbox").eq(0).attr("id").split(/_/)[1];
						var ti = contains(_selectData,"id",_tid);
						if (ti>=0) {
							_selectData.splice(ti,1);
						}
						titlecheck.parents(".Palletsbox").eq(0).remove();
					}
				}
				_cal();
			});
			$(".form-control").change(function(){
				if($(this).attr("required")===undefined) {
					return;
				}
				if ($(this).val()!='') {
					$(this).parent().removeClass("has-error");
				} else {
					$(this).parent().addClass("has-error");
				}
			});
			$("#btn_next").click(function(){
				$('#tabs .nav-tabs a:last').tab('show');
			});
			$("#btn_prev").click(function(){
				$('#tabs .nav-tabs a:first').tab('show');
			});
			$("#btn_save").click(function(){
				var toptitlebody = $("#selectDetail").find(".toptitlebody");
				var error = false;
				if($("#mabd").val()=="") {
					$("#mabd").parent().addClass("has-error");
					error = true;
				} else {
					$("#mabd").parent().removeClass("has-error");
				}
				if($("#plantime").val()=="") {
					$("#plantime").parent().addClass("has-error");
					error = true;
				} else {
					$("#plantime").parent().removeClass("has-error");
				}
				if (error) {
					$('#tabs .nav-tabs a:first').tab('show');
					return false;
				}
				for (var fj = 0; fj < toptitlebody.length; fj++) {
					var itmebody = toptitlebody.eq(fj);
					console.log(itmebody.parents(".Palletsbox").eq(0).attr("id").split(/_/)[1]);
				}
				$.artDialog.confirm("Save SO["+request.QueryString("id")+"] OK.",function(){
					console.log(request.QueryString("id"));
					closeWindow();
				}, function(){});
			});
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				if(e.target.hash=="#SoReview") {
					$("#SoReview div.mabd").eq(0).html($("#mabd").val());
					$("#SoReview div.plantime").eq(0).html($("#plantime").val());
					//$("#SoReview div.shipTo").eq(0).html($("#shipTo").val());
					$("#SoReview .reviewdata").empty();
					var line = "";
					$("#selectDetail").find(".toptitlebody").each(function(index){
						var Palletsbox = $(this).parents(".Palletsbox").eq(0);
						$("<div id='"+Palletsbox[0].id+"' class='col-md-4 Palletsbox'>"+Palletsbox.html().replace("<div style=\"height: 22px;\" class=\"toptitlebody\">","<div class=\"toptitlebody\">").replace("<input type=\"checkbox\">","<input type=\"checkbox\" style='display:none'>").replace("fa-close","")+"</div>").appendTo("#SoReview .reviewdata");
					});
				} else {
					
				}
			});
			$("#tabs").show();
		}
	);
});