define(["jquery","backbone","templates","../../config","oso.lib/table/js/table"], function($,Backbone,templates,config,Table) {
	return Backbone.View.extend({
		el: "#SoBol",
		template: templates.BOLView,
		initialize:function(options) {
			if (options) {
				this.soid = options.id;
			}
		},
		render: function(){
			var v = this;
			v.$el.html(v.template());
			var table = new Table({
				container: '#BOLS',
				url: config.BOLURl.url+'?soid='+ v.soid,
				title: 'LOAD List',
				height: 300,
				rownumbers: true,
				singleSelection: true,
				pagination: false,
				columns:[
					{field:'BOLID',title:'LOAD ID',width:100, formatter:
						function(data, row){
							return "<a href='javascript:void(0)' class='BOLCLICK' data-id='"+data+"'>"+data+"</a>";
						}
					},
					{field:'SHIPTO',title:'Ship To',width:100},
					{field:'PROGRESS',title:'Progress',width:100,align:'center',formatter:
						function(data, row){
							return (data*100) + '%';
						}
					},
					{field:'STATUS',title:'Status',width:100},
					{field:'DEADLINE',title:'Deadline',width:100},
					{field:'APPOINTMENTNO',title:'Apponitment',width:200,formatter:
						function(data, row){
							var rt = "";
							for (var i=0;i<data.length;i++) {
								if (i==0) {
									rt = "<a href='javascript:void(0)' class='APPCLICK' data-id='"+data[i]+"'>"+data[i]+"</a>";
								} else {
									rt += ",<a href='javascript:void(0)' class='APPCLICK' data-id='"+data[i]+"'>"+ data[i]+"</a>";
								}
							}
							return rt;
						}
					}
				]
			});
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				if(e.target.hash=="#SoBol") {
					table.refreshWidth();
				}
			});
		}
	});
});