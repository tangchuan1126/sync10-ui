define(["jquery","underscore", "backbone", "handlebars", "templates","../../config",
	"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree", "immybox"],
	function($,_,Backbone,handlebars,templates,config,AsynLoadQueryTree,immybox) {
	return Backbone.View.extend({
		el: "#SoSearch",
		shipFrom: "",
		shipTo: "",
		template: templates.SearchTab,
		initialize:function(options) {
			//this.resultView = options.resultView;
			if (options) {
				this.resultView = options.resultView;
				if (!_.isUndefined(options.shipFrom)) {
					this.shipFrom = options.shipFrom;
				}
				if (!_.isUndefined(options.shipTo)) {
					this.shipTo = options.shipTo;
				}
			}
		},
		getDataTypes:function(result) {
			var dataTypes = [];
			for(prop in result) {
				dataTypes[dataTypes.length]={"text":result[prop],"value":prop};
			}
			return dataTypes;
		},

		render: function(){
			var soI18n = window.regional['defaultLanguage'];
			var v = this;
			v.$el.html(v.template({soI18n:soI18n}));
			var fromTree = new AsynLoadQueryTree({
				renderTo: "#shipFrom",
				selectid: {value: v.shipFrom},
				PostData: {rootType:"Ship From"},
				dataUrl: config.shipFrom.url,
				scrollH: 400,
				multiselect: false,
				Async: true,
				placeholder:"Ship From"
			});
			fromTree.render();
			//$("#shipFrom").keydown(function(event) {return false;});
			var toTree = new AsynLoadQueryTree({
				renderTo: "#shipTo",
				selectid: {value: v.shipTo},
				PostData: {rootType:"Ship To"},
				dataUrl: config.shipTo.url,
				scrollH: 400,
				multiselect: false,
				Async: true,
				placeholder:"Ship To"
			});
			toTree.render();
			$("#shipTo").keydown(function(event) {return false;});

		/*	$.getJSON(config.status.url, function (result) {
				$('#status').immybox({
					choices: result
				});
			});*/
			var result=v.getDataTypes(soI18n.result);
			$('#status').immybox({
				choices: result
			});
			
			$("#SearchTabBtn").click(function(){
				var search = {};
				if (!v.filterQueryCondition($("#DeadlineMin").val())) {
					search.DeadlineMin =$("#DeadlineMin").val();
				}
				if (!v.filterQueryCondition($("#DeadlineMax").val())) {
					search.DeadlineMax =$("#DeadlineMax").val();
				}
				if (!v.filterQueryCondition($("#ProgressMin").val())) {
					search.progress_min = $("#ProgressMin").val();
				}
				if (!v.filterQueryCondition($("#ProgressMax").val())) {
					search.progress_max =$("#ProgressMax").val();
				}
				if (!v.filterQueryCondition($("#shipTo").val())) {
					search.shipTo =$("#shipTo").data("val");
				}
				/*if (!v.filterQueryCondition($("#shipFrom").val())) {
					search.shipFrom =v.shipFrom;
				}*/
				if (!v.filterQueryCondition($("#status").attr("data-value"))) {
					search.status = $("#status").attr("data-value");
				}

                if (!v.filterQueryCondition($("#deadLineStartDay").val())) {
					search.deadLineStartDay =$("#deadLineStartDay").val();
				}
				if (!v.filterQueryCondition($("#deadLineEndDay").val())) {
					search.deadLineEndDay =$("#deadLineEndDay").val();
				}
                search.shipFrom =v.shipFrom;
				v.resultView.setQuery(search);
			});
		},
		filterQueryCondition:function(str)
		{
			return str==undefined ||str==0||str=="0"||str=="";
		},
	});
});