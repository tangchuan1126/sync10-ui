define(["jquery","underscore","backbone","templates","../../config"],
function($,_,Backbone,templates,config) {
	return Backbone.View.extend({
		el: "#SoTool",
		template: templates.ToolTab,
		initialize:function(options) {
			if (options) {
				this.resultView = options.resultView;
			}
		},
		render: function(){
			var v = this;
			v.$el.html(v.template());
		}
	});
});