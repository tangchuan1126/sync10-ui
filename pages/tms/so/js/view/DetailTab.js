define(["jquery","backbone","templates","../../config"],
	function($,Backbone,templates,config) {
	return Backbone.View.extend({
		el: "#SoDetail",
		template: templates.detailTab,
		initialize:function(options) {
			if (options) {
				//this.resultView = options.resultView;
			}
		},
		render: function(){
			var v = this;
			v.$el.html(v.template());
		}
	});
});