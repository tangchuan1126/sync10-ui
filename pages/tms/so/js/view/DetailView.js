define(["jquery","backbone","templates","../../config",'oso.lib/PalletsControls/Pallets'], function($,Backbone,templates,config,Pallets) {
	return Backbone.View.extend({
		el: "#SoDetail",
		template: templates.DetailView,
		initialize:function(options) {
			if (options) {
				this.soid = options.id;
				//this.resultView = options.resultView;
			}
		},
		render: function(){
			var v = this;
			v.$el.html(v.template());
			var totlePallets = 0;
			var totlePackages = 0;
			var totleWeight = 0.0;
			var totleVolume = 0.0;
			function format_json(json){
				if (jQuery.isArray(json)) {
					totlePallets = 0;
					totlePackages = 0;
					totleWeight = 0.0;
					totleVolume = 0.0;
					$.each(json, function(index, objVal) {
						if (!objVal.title) {
							objVal.title=[];
						}
						if (typeof(objVal.titlecheckbox)!='undefined') {
							delete objVal.titlecheckbox;
						}
						if (objVal.TYPE) {
							objVal.title=[];
							objVal.title.push("Palletid:"+objVal.id+"");
							totlePallets+=1;
						} else {
							objVal.title=[];
							objVal.title.push("Packageid:"+objVal.id+"");
							totlePackages+=1;
						}
						if (objVal.DEADLINE) {
							objVal.title.push("Deadline:"+ objVal.DEADLINE);
						}
						if (objVal.WEIGHT) {
							totleWeight+= objVal.WEIGHT;
						}
						if (objVal.VOLUME) {
							totleVolume+= objVal.VOLUME;
						}
						if (objVal.SHIPTO) {
							objVal.title.push("ShipTo:"+ objVal.SHIPTO);
						}
						if (objVal.BOLID) {
							objVal.title.push("BOL:"+ objVal.BOLID);
						}
					});
				}
				//console.log(json);
				return json;
			}
			var p_list = new Pallets({
				renderTo: ".viewdata",
				dataUrl: {url:"./json/d.json?soid="+v.soid,formatjson:format_json},
				tabledataurl:{url:"./json/table.json?{id}",formatjson:format_json},
				scrollH: 200,//最高200超出会滚动
				columns:3,//每行的列数，默认流式布局
				colstyle:"col-md-4" //可以数字也可以col-md-*默认是col-md-3
			});
		}
	});
});