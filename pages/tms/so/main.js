"use strict";
require(["../../../requirejs_config","./config"], function($,config) {
	
  	// 根据浏览器语言加载不同语言包
  	var so_language =  './i18n/so-' + window.navigator.languages[0];
	require(["jquery","./listMain","./js/view/searchTab","./js/view/toolTab","artDialog",so_language],
		function($,listMain,SearchTab,ToolTab,Dialog) {
		var soI18n = window.regional['defaultLanguage'];
			$.ajax({
			type: "GET",
			url: config.adminSesion.url,
			dataType: "json",
			timeout: 30000,
			error: function(XHR,textStatus,errorThrown) 
			{
				if (textStatus==401) {
					var aretr = Dialog({
						title: '错误',
						width: 150,
						content: '请确认已经登陆系统！'
					});
					aretr.showModal();
				} else {
					var aretr = Dialog({
						title: '服务端状态',
						width: 150,
						content: '请求出错！'
					});
					aretr.showModal();
				}
			},
			success: function(data,textStatus) 
			{
				
				$("#advancedSearch").text(soI18n.AdvancedSearch);
				$("#commonTool").text(soI18n.CommonTool);
				$("#SoCreate").text(soI18n.CreateSo);
				
				console.log(data);
				if (textStatus=="success") 
				{
					var ps_id = data.ps_id;
					var queryOptions = {
						el: "#data",
						url:config.searchURl.url,
						queryCondition:{shipFrom:ps_id}
					};
					
					var resultView = new listMain(queryOptions);
					resultView.on("events.Error", function(e){
						$(".loding,.LodingModal").hide();
						var aretr = Dialog({
							title: '服务端状态',
							width: 150,
							content: '请求出错！'
						});
						aretr.showModal();
					});
					
					var temp = new SearchTab({resultView:resultView,shipFrom:ps_id});
					temp.render();
					var temp1 = new ToolTab({resultView:resultView});
					temp1.render();
				
					$("#SoCreate").click(function(){
					 window.eventsMgr.trigger('replaceCurrentPage',config.createSO.url)
						//artDialog.open("./create.html",{title:"Create SO", width:'1100px', height:'600px', lock:true, opacity:0.3, fixed: true});
					});

					window.eventsMgr = _.extend({}, Backbone.Events);

					//重新计算iframe高度，让iframe高度为浏览器可见区高度，ifame自动产生滚动条
					window.eventsMgr.on('resize',  function() {
						var clientHeight = document.documentElement.clientHeight;
						$("#createFrame").height(clientHeight-15);
						//console.log($("#createFrame").contents().find("#createBody").height());
						 
					    //console.log($("#createFrame")[0].contentWindow.document.body.offsetHeight);
						
					});
					//iframe覆盖本页面
					window.eventsMgr.on('replaceCurrentPage',  function(url) 
					{
						 $("#listContainer").fadeOut('1000', function() 
						 {
					 		 $("#createFrame")[0].src= url;//config.createSO.url;
					 		 $("#createFrame").load(function()
					 	 	{
					 	 		var clientHeight = document.documentElement.clientHeight;
					 	 		$("#createFrame").height(clientHeight-15)
								//var mainheight = $(this).contents().find("body").height()+30;
								//$(this).height(mainheight);
								$("#createContainer").fadeIn('1000', function() {});
							}); 
					  	});
					});

					//刷新SO列表页面
					window.eventsMgr.on('renderSoList', function(param) {
							resultView.render();
							$("#createContainer").fadeOut('1000', function() {
								 $("#listContainer").fadeIn('1000', function() {
							 		$("#createFrame").empty();
								 });
							});
					});
				
					//刷新SO列表页面
					window.eventsMgr.on('renderOneSO', function(so_id) {
							//resultView.trigger('updateOneRow',so_id);
							resultView.render();
							$("#createContainer").fadeOut('1000', function() {
								 $("#listContainer").fadeIn('1000', function() {
							 		$("#createFrame").empty();
								 });
							});
							
					});
				}
			}
			});

			
		}
	);
});