define(["JSONTOHTML","dateUtil",
		"oso.lib/fieldsetControls/fieldset",
		"./KeyUtil",
		"../key/SOStatusKey"], 
function(JSONTOHTML,DateUtil,fieldset,KeyUtil,SOStatusKey) {
	var soI18n = window.regional['defaultLanguage'];
	//第一次创建时带上表头
	var _table = {
		thead: [
			{
				tag: "table",
				class: "checkin_box",
				children: [{
					tag: "thead",
					children: [{
						tag: "tr",
						html: function(obj, index){
							var thdata = obj.children[0].children;
							return (JSONTOHTML.transform(thdata.children, _table.th));
						}
					}]
				},
				{
					tag: "tbody",
					html: ""
				}]
			}, {
				tag: "div",
				class: "tfootpage",
				html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
			}, {
				tag: "div",
				class: "SelectMorebox"
			}
		],
		th: {
			tag: "th",
			html: "${title}",
			style: function(obj, index) {
				if (index==3) {
					return "width:22%";
				} else if (index==2) {
					return "width:22%";
				} else{
					return "";
				}
			}
		}
	};
	
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var tdID = "SO_ID";
	var fsid = [];
	var _tbody = {
		//初始化，将json数据分类：分为对应的行和列
		AUTHS:[],
		fieldsets:[],
		int: function(jsons,View_control) {

			//数据转换成表格形式
			//["tr":"index","children":[{},{}]]
			_tbody.AUTHS = {};
			var headallobj = View_control.head;
			var footer = View_control.footer;
			var pageSize=jsons.PAGECTRL.pageSize;
			//按表头转换成行和列
			var __trhtml = "";
			//获得权限
			_.map(jsons.AUTHS,function(auth,key) {
				//
				_tbody.AUTHS[auth.SO_ID] = auth;
			});
			for (var tri = 0; tri < jsons.DATA.length; tri++) {
				var tritme = jsons.DATA[tri];
				//_.map(_tbody.AUTHS,function(obj,key){});
				var __td = [];
				for (var headkey in headallobj) {
					__td.push(tritme);
				}
				//显示行
				var __tr=[];
				__tr.push({"trid":tritme.SO_ID,"children": __td});
				__trhtml += (JSONTOHTML.transform(__tr[0], _tbody.tr));
				//按钮行
				__tr=[];
				__tr.push({"trid":tritme.SO_ID,"colspan":headallobj.length,"children": footer,"linedata":tritme});
				__trhtml += (JSONTOHTML.transform(__tr[0], _tbody.trfooter));
				if(tri >= pageSize) {
					break;
				}
			}
			return __trhtml;
		},
		tr: {
			"tag": "tr",
			"data-itmeid":function(obj){
				return tdID+"|"+obj.trid;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {
				//自由列数console
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tdID+"|"+obj.trid+'" class="buttons-group">';
				var ahtml = "";
				var id = obj.trid;
				for (var akey in obj.children){
					var itmea = obj.children[akey];
					if(_tbody.isHasButton(itmea[0],obj.linedata,_tbody.AUTHS[obj.trid])) {
						ahtml += '<a href="javascript:void(0)" data-eventname="'+ itmea[0] +'" class="buttons">' + itmea[1] + '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		isHasButton:function(itema,linedata,AUTHS) {

			var res = false;
			
			if (itema=="events.CLOSE") 
			{
				//res = AUTHS["BTN_CLOSE"];
			} else if (itema=="events.EDIT") 
			{
				if(linedata.STATUS == 1||linedata.STATUS == 2)
				{
					res = true;
				}
			} else if (itema=="events.CREATEAPPOINTMENT") 
			{
				res = AUTHS["BTN_CREATEAPPMT"];
			} else if (itema=="events.CREATEBOL") 
			{
				if(linedata.STATUS == 1||linedata.STATUS == 2)
				{
					res = true;
				}
			} else 
			{
				res = false;
			}
			return res;
		},
		mainInfo: {"tag":"div","class":"container", "children":[
			{"tag":"div","class":"row","children":[
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":soI18n.ShipTo+" :"},
						{"tag":"span","class":"valstyle","html":"<b>${CONSIGNEE_WHS_NAME}</b>"}
						]}
				]},
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":soI18n.Deadline+" :"},
						{"tag":"b","class":"valstyle","html":"${DEADLINE}"}
						//{"tag":"span","class":"valstyle","html":function(obj,index){
						//	return "<b>"+DateUtil.formatDate(obj.DEADLINE)+"</b>";
						//}}
						]}
				]},
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":soI18n.Unfinished+" :"},
						{"tag":"span", "title":"Remain Pallets : ${REMAINPALLETS} , Remain Packages : ${REMAINPACKAGES}",
							"class":function(obj, index){
								if (obj.REMAINPALLETS==0 && obj.REMAINPACKAGES==0) {
									return "valstyle color-green";
								} else {
									return "valstyle color-red"
								}
						    },
						   "html":"${REMAINPALLETS} Pallets,${REMAINPACKAGES} Packages"}
						]}
				]}
			]},
			{"tag":"div","class":"row","children":[
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":soI18n.TotalWeight+" :"},
						{"tag":"span","class":"valstyle","html":"${TOTAL_WEIGHT}"}
						]}
				]},
				/**
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":"Total Volume:"},
						{"tag":"span","class":"valstyle","html":"${VOLUME} ${VOLUMEUNIT}"}
						]}
				]},
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":"MABD:"},
						{"tag":"span","class":"valstyle","html":function(obj,index){
							return DateUtil.formatDate(obj.MABD);
						}}
					]}
				]}
			]}, 
			{"tag":"div","class":"row","children":[
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":"Create By:"},
						{"tag":"span","class":"valstyle","html":"${CREATE_ACCOUNT}"}
						]}
				]},**/
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":soI18n.Creator+" :"},
						{"tag":"span","class":"valstyle","html":"${CREATE_ACCOUNT}"}
						]}
				]},
				{"tag":"div","class":"col-md-4","children":[
					{"tag":"li","children":[
						{"tag":"span","class":"namestyle","html":soI18n.CreateTime+" :"},
						{"tag":"span","class":"valstyle","html":"${CREATE_TIME}"}
						
						//{"tag":"span","class":"valstyle","html":function(obj,index){
						//	return DateUtil.formatDate(obj.CREATE_TIME);
						//}}
					]}
				]}
			]},
			{"tag":"div","class":"row","children":[
				{"tag":"div","class":"col-md-12","style":"display:none;","id":"BOLS${SO_ID}","html":function(obj, index) {
					return "";
					//return JSONTOHTML.transform(obj.BOLS, _tbody.BOLList);
				}}
			]}
		]},
		td: {
			tag: "td",
			html: function(obj, index) {
				var context = JSONTOHTML.transform(obj, _tbody.mainInfo);
				var _fieldset_data = {
					Eventparentbox:"#data",
					dataUrl: {
						"Rootbox":{"tag":"fieldset","class":"filesetstyle subfilesetstyle"},
						"Title":{
							"tag": "legend",
							"class": "td_legend",
							"children": {
								"left": {
									"tag":"span","children": [
										{"tag":"span","class":"SOID","html":"<a href='javascript:void(0)'>"+obj.SO_ID+"</a>","data-id":obj.SO_ID},
										{"tag":"span","class":"STATUS","html":function(){/** return KeyUtil.get(SOStatusKey,obj.STATUS);**/ return SOStatusKey[obj.STATUS]['en']}}
									]
								},
								//"right": {"tag":"span","class":"progress","html":obj.PROGRESS+"%"}
								"right": {"tag":"div","class":"progress","children":[
								 {"tag":"div","class":"progress-bar progress-bar-success","html":obj.PROGRESS+"%","style":function(){if(parseFloat(obj.PROGRESS)!=0) return "min-width:3em;width:"+obj.PROGRESS+"%";}},
								 {"tag":"span","html":function(){if(parseFloat(obj.PROGRESS)==0) return obj.PROGRESS+"%";}}
								]}
							}
						},
						"Content":[context],
						"Foot": {"right":{"tag":"label","class":"glyphicon glyphicon-arrow-down MOREBOL","data-id":obj.SO_ID,"html":"<a href='javascript:void(0)'>"+soI18n.ShowBOL+"</a>"}}
					}
				};
				var _fieldset_liste = new fieldset(_fieldset_data);
				_fieldset_liste.render();
				return _fieldset_liste.tohtml();
			}
		},
		//====================================工具方法===================================//
		//处理空字符串
		emptyHandle:function(str,defaultValue) {
			if(typeof str !="undefined" && (""+str) !="")
			{
				return str;
			} else {
				if(typeof str =="undefined") return "";
				return defaultValue;
			}
		},
		//更新一条数据
		updateOneRow: function(jsondata, trid) {
			var __td = [];
			var headallobj = this.head;
			for (var headkey in headallobj) {
				__td.push(jsondata.DATA[0]);
			}
			var strid = "'"+tdID+"|"+trid+"'";
			//TR Data
			$("tr[data-itmeid="+strid+"]").html(JSONTOHTML.transform(__td, _tbody.td));
			var ahtml = "";
			var footer = this.footer;
			for (var akey in footer){
				var itmea = footer[akey];
				if(_tbody.isHasButton(itmea[0],trid,jsondata.AUTHS[0])) {
					ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>';
				}
			}
			$("div[data-itmeid="+strid+"]").html(ahtml);
		}
	}
	return {
		tbody: _tbody,
		table: _table
	}
});