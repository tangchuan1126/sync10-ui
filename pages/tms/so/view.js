"use strict";
require(["../../../requirejs_config","./config"], function($,config) {
	require([
		"bootstrap",
		"require_css!oso.lib/bootstrap/css/bootstrap.min.css",
		"require_css!oso.lib/bootstrap/css/plugins/timeline.css",
		"require_css!Font-Awesome",
		"require_css!oso.lib/PalletsControls/css/style.css"
	]);
	//jquery-ui.css jquery-ui-timepicker-addon.css
	require(["jquery","./js/view/DetailView","./js/view/BOLView",'oso.lib/PalletsControls/Pallets',"artDialog"],
		function($,DetailView,BOLView,Pallets,artDialog) {
			var request = {
				QueryString : function(val) {
					var uri = window.location.search;
					var re = new RegExp("" +val+ "=([^&?]*)", "ig");
					return ((uri.match(re))?(uri.match(re)[0].substr(val.length+1)):null);
				}
			}
			var temp = new DetailView({id:request.QueryString("id")});
			temp.render();
			var temp = new BOLView({id:request.QueryString("id")});
			temp.render();
			$("body").show();
		}
	);
});