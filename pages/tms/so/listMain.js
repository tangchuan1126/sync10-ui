"use strict";
define(["jquery","underscore","bootstrap","CompondList/View","./view_config","./jsontohtml_templates","./config","oso.lib/table/js/table","art_Dialog/dialog-plus","dateUtil",
"../key/BolStatusKey","/Sync10-ui/lib/slidePanel/js/slidePanel.js","domready!","require_css!oso.lib/slidePanel/css/style.css"],
	function($,_,bootstrap,CompondList,view_config,Template,config,Table,Dialog,DateUtil,BolStatusKey,SlidePanel) {
	var soI18n = window.regional['defaultLanguage'];
		return function(options) {
			var list = new CompondList(view_config, {
				renderTo: options.el,
				dataUrl: options.url,
				pageSize: 9,
				PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
				//dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
			});
			//绑定事件
			list.render();
			list.on("events.CLOSE",function(e) {
				var d = Dialog({
				    title: 'Info',
				    width:"300px",
				    content: ' Do You want close SO '+e.data.linedata.SO_ID+" ?",
				    okValue: 'Confirm',
				    ok: function () {
				    	d.close().remove();
					    $.ajax({
							url: config.closeSO.url,
							type: 'GET',
							dataType: 'json',
							data: {"so_id": e.data.linedata.SO_ID,"status":"3"}
						})
						.done(function() {
							list.render();
						})
						.fail(function() {
							console.log("error");
						});
				    },
				    cancelValue: 'Cancel',
				    cancel: function () {}
				}).showModal();
			});

			list.on("events.EDIT",function(e) {
				var so_id = e.data.linedata.SO_ID;
				//replaceCurrentPage事件在main函数中注册
				window.eventsMgr.trigger("replaceCurrentPage",config.createSO.url+"?so_id="+so_id);
				//$.artDialog.open("./update.html?id="+SOID,{title:"Edit SO["+SOID+"]", width:'1100px', height:'600px', lock:true, opacity:0.3, fixed: true});
			});
			list.on("events.CREATEAPPOINTMENT",function(e) {
				console.log(e.data.linedata.SOID);
			});
			list.on("events.CREATEBOL",function(e) {
				var so_id = e.data.linedata.SO_ID
				var url = config.createBol.url+"?so_id="+so_id;

				var d = Dialog({
					title: soI18n.CreateLoad,
					url:url,
					fixed: false,
					skin: 'test1',
					width:'770px',
					height:'480px',
					onclose:function()
					{
						var bol_id = d.returnValue;
						if(bol_id!= "" && bol_id != undefined && bol_id != null)
						{
							window.open("/Sync10-ui/pages/tms/bol_pallets/index.html?bol_id="+bol_id,"addBolPallets");
						}
						d.remove();
						//renderOneRow(list,transport_id);
					}
				}).showModal();

				top.dialog = d;
				/**
				artDialog.open(url , {title: "CreateBOL",width:'770px',height:'480px', lock: true,opacity: 0.3,fixed: true,
					close:function()
					{
						var bod_id = artDialog.data("returnData");
						if(bod_id!= "" && bod_id != undefined && bod_id != null)
						{
							artDialog.data("returnData","");
							window.open("/Sync10-ui/pages/tms/bol_pallets/index.html?bol_id="+bod_id,"addBolPallets");
						}
						//renderOneRow(list,transport_id);
					}
				});
				**/
			});
			var listDom = list.$el;
			listDom.delegate(".MOREBOL","click", function(d){
				if($(this).hasClass("glyphicon-arrow-down")) {
					$('#BOLS'+$(this).data("id")).html("").show();
					var table = new Table({
						container : '#BOLS'+$(this).data("id"),
						url : config.BOLURl.url+'?so_id='+$(this).data("id"),
						title : 'BOL List',
						height: 200,
						//rownumbers : true,
						singleSelection : true,
						pagination: false,
						columns:[
							{field:'BOL_ID',title:soI18n.LOADID,width:100, formatter:
								function(data, row){
									return "<a href='javascript:void(0)' class='BOLCLICK' data-id='"+data+"'>"+data+"</a>";
								}
							},
							{field:'CONSIGNEE_WHS_NAME',title:soI18n.ShipTo,width:100},
						/**	{field:'PROGRESS',title:'Progress',width:100,align:'center',formatter:
								function(data, row){
									return "%" ;//(data*100) + '%';
								}
							}, **/
							{field:'STATUS',title:soI18n.Status,width:100,formatter:function(data, row){return BolStatusKey[data]['en']}},
							{field:'MABD',title:soI18n.MABD,width:100 /**,formatter:function(data, row){return DateUtil.formatDate(data);}**/},
							{field:'APPOINTMENTNO',title:soI18n.Appointment,width:200,formatter:
								function(data, row){
									if(data)
									{
										var rt = "";
										for (var i=0;i<data.length;i++) {
											if (i==0) {
												rt = "<a href='javascript:void(0)' class='APPCLICK' data-id='"+data[i]+"'>"+data[i]+"</a>";
											} else {
												rt += ",<a href='javascript:void(0)' class='APPCLICK' data-id='"+data[i]+"'>"+ data[i]+"</a>";
											}
										}
										return rt;
									}
								
								}
							}
						]
					});
					$(this).removeClass("glyphicon-arrow-down").addClass("glyphicon-arrow-up").html("<a href='javascript:void(0)'>"+soI18n.HideBOL+"</a>");
				} else {
					$('#BOLS'+$(this).data("id")).hide();
					$(this).removeClass("glyphicon-arrow-up").addClass("glyphicon-arrow-down").html("<a href='javascript:void(0)'>"+soI18n.ShowBOL+"</a>");
				}
			});
			listDom.delegate(".SOID","click", function(d){
				var SOID = $(this).data("id");
				window.eventsMgr.trigger("replaceCurrentPage",config.createSO.url+"?so_id="+SOID);
				//$.artDialog.open("./view.html?id="+SOID,{title:"View SO["+SOID+"]", width:'1100px', height:'600px', lock:true, opacity:0.3, fixed: true});
			});
			listDom.delegate(".BOLCLICK","click", function(d){
				console.log($(this).data("id"));
				//Sync10-ui/pages/bol/bol_detail/index.html
				//window.open(config.bolPallets.url+"&bol_id="+$(this).data("id"));
				var slide = new SlidePanel({
		            title: "BOLPlates",
		            url: config.bolPallets.url+"&bol_id="+$(this).data("id"),
		            duration: 500,
		            topLine: 0,
		            slideLine: "75%"
		           // closebefore : SlidePanelCloseBefore,
		            //ShutdownCallback : SlidePanelShutdownCallback
		        });
                slide.open();
			});
			listDom.delegate(".APPCLICK","click", function(d){
				console.log($(this).data("id"));
					});
			
			list.on("updateOneRow",  function(SO_ID) 
			{
				$.getJSON(config.searchURl.url, {"so_id": SO_ID,"pageNo":1,"pageSize":10},
				 	function(json, textStatus) 
				 	{
				 		if(textStatus == "success")
				 		{
				 			list.upSubitmeData({
							upitme:[{Root:"DATA",itmeid:SO_ID,itmekey:"SO_ID"},
									{Root:"AUTHS",itmeid:SO_ID,itmekey:"SO_ID"}],
  							DATA:json.DATA[0],
  							//AUTHS:json.AUTHS[0]});
  							AUTHS:null});
						//Template.tbody.updateOneRow(SO_ID,json.DATA[0],json.AUTHS[0]);
						Template.tbody.updateOneRow(json,SO_ID,view_config);
				 		}
				});
			});			

			return list;
		};
});