define(["./jsontohtml_templates"], function(tmp) {
	var soI18n = window.regional['defaultLanguage'];
	return {
		"View_control": {
			"head": [{
				"title":soI18n.SoInfo,
				"field": {"name":"SO"}
			}],
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"footer": [
				//["events.CLOSE", "Close"],
				["events.EDIT",soI18n.EditSO],
				//["events.CREATEAPPOINTMENT", "Create Appointment"],
				["events.CREATEBOL",soI18n.CreateLoad]
			],
			"templates": tmp
		}
	}
});