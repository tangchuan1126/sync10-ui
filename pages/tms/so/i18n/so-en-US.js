;( function( w ) {
	w.regional = {};
	w.regional['en-US'] = {
	    /* So add */	
		AdvancedSearch:'Advanced Search',	
		CommonTool:'Common Tool',
		CreateSo:'Create So',
		To:'To',
		DeadlineDay:'Deadline',
		Status:'Status',
		Search：'Search',
		SoInfo:'SO Info',
		ShipTo:'Ship To',
		Deadline:'Deadline',
		Unfinished:'Unfinished',
		TotalWeight:'Total Weight',
		Creator:'Creator',
		CreateTime:'Create Time',
		ShowBOL:'Show LOAD',
		HideBOL:'Hide LOAD',
		EditSO:'Edit SO',
		CreateLoad:'Create Load',
		LOADID:'LOAD ID',
		MABD:'MABD',
		Appointment:'Appointment',
		SelectStatus:'Select Status',
		day:'day',
		result:{
			"0":"All",
			"1":"Empty",
			"2":"Planning",
			"3":"Finish",
			"9":"Close"
		}
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'en-US' ];

}( window ) )