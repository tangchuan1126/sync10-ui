;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
	    /* So add */	
		AdvancedSearch:'高级查询',	
		CommonTool:'公共工具',
		CreateSo:'创建发货计划',
		To:'目的地',
		DeadlineDay:'剩余天数',
		Status:'状态',
		Search:'查询',
		SoInfo:'SO 详细信息',
		ShipTo:'目的地',
		Deadline:'期限',
		Unfinished:'未完成',
		TotalWeight:'总重量',
		Creator:'创建人',
		CreateTime:'创建时间',
		ShowBOL:'显示运单',
		HideBOL:'隐藏运单',
		EditSO:'编辑发货计划',
		CreateLoad:'创建运单',
		LOADID:'运单编号',
		MABD:'必须到达日期',
		Appointment:'约车',
		SelectStatus:'请选择状态',
		day:'天',
		result:{
			"0":"全部",
			"1":"空的",
			"2":"处理中",
			"3":"完成",
			"9":"关闭"
		}
			

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )