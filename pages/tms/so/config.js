define({
	createBol:{
		url:"/Sync10-ui/pages/tms/bol_create_baseso/index.html"
		//url:"/Sync10-ui/pages/tms/bol_create_v2/index.html"
	},
	createSO: {
		url:"/Sync10-ui/pages/tms/so_create/index.html"
		//url:"/Sync10-ui/pages/tms/so_add/index.html"
	},
	bolPallets:{
        url:"/Sync10-ui/pages/tms/bol_pallets/editIndex.html?op=0&sourcepath=so"
    },
	searchURl: {
		//url:"./json/so.data.json"
		url:"/Sync10/_tms/so/getSOList"
	},
	closeSO:{
		url:"/Sync10/_tms/so/closeSO"
	},
	BOLURl: {
		//url:"./json/so.bol.json"
		url:"/Sync10/_tms/bol/getBolListBySOId"
	},
	adminSesion: {
		url:"/Sync10/_tms/admin/getLoginAdmin"
	},
	////////////////////////////////////////
	shipFrom: {
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	shipTo: {
		url: "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType"
	},
	status: {
		url: "./json/so.status.json"
	},
	session:{
		url:"/Sync10/_fileserv/redis_session"
	}
	
});