;( function( w ) {
	w.regional = {};
	
	w.regional['zh-CN'] = {
			ADVANCESEARCH:{
				title:"高级查询",
				addRoute_btn:"创建路由牌",
				currentpsid_lable:"当前站点",
				routeName_lable:"路由名称",
				routeName_placeholder:"路由名称",
				currentpsid_placeholder:"请选择当前站点",
				permitPsId_lable:"目的地分拨中心",
				permitPsId_placeholder:"请选择目的地分拨中心",
				routePsId_lable:"可到达分拨中心",
				routePsId_placeholder:"请选择可到达分拨中心",
				routeType_lable:"路由类型",
				routeType_placeholder:"请选择路由类型",
				status_lable:"状态",
				status_placeholder:"请选择路由状态",
				search_btn:"查询",
				reset_btn:"重置",
				all:"全部",
				STATUSKEY:{
					"0":"停用",
					"1":"启用"
				},
				ROUTETYPEKEY:{
					"0":"临时",
					"1":"永久"
				},
				panel_title:"路由信息"
			},
			ROUTE:{ //路由信息
				tabtitle:"库存信息",//Route
				currentInventory:"总件数",
				currentPiao:"总票数",
				currentWeight:"总重量",
				nodata:"此站没有目的地信息!",
				route_name:"路由名称",
				routeName_placeholder:"路由名称",
				route_store:"路由站点",
				routeStore_placeholder:"选择路由站点",
				stauts_lable:"状态",
				linetype_lable:"路由类型",
				uesStartTime_placeholder:"选择起始时间",
				uesEndTime_placeholder:"选择结束时间",
				uesStartTime_lable:"生效起始时间",
				uesEndTime_lable:"生效结束时间",
				next_btn:"下一步",
				routeinfo_title:"路由牌信息",
				enable_lable:"启用",
				disable_labe:"停用",
				forever_lable:"永久",
				provisional_lable:"临时",
				view_lable:"预览",
				view_route_title:"路由牌",
				operatBy:"操作人",
				operatDate:"操作时间",
				remark:"备注"
				
			},
			ROUTE_LINE:{
				tabtitle:"线路信息",//Route_Line,
				car_type_title:"车型",
				depart_title:"始发地",
				starting_time_title:"始发时间",
				destination_title:"目的地",
				arrive_time_title:"到达时间",
				line_use_cycle_title:"运行周期",
				must_arrive_time_title:"规定到达时间",
				must_leave_time_title:"规定离开时间",
				warningmsg:"今天此线路没有班车!",
				stop_name_title:"经停地",
				nocar:"今日无班次",
				nodata:"此站没有线路信息!",
				dest_title:"目的地信息",
				line_type_title:"线路类型",
				prev_btn:"上一步",
				add_btn:"添加目的地",
				destination_placeholder:"选择目的地站点",
				save_btn:"保存",
				stop_type_lable:"经停",
				origination_type_lable:"始发"
				
			},
			LOG:{
				tabtitle:"操作日志",
				title:"",
				operator_title:"操作人",
				operationtime_title:"操作时间",
				operate_title:"操作类型",
				remarks_title : "备注",
				createKey:"创建",
				updateKey:"修改"
			},
			BUTTONS:{
				editroute_btn:"修改",
				deleteroute_btn:"刪除",
				morelog_btn:"更多日志",
				moreline_btn:"更多路线"
				
			}
			

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )