/**
 * Created by liyi on 2015.5.20.
 */

"use strict";
define([
    "../config",
    "jquery",
    "backbone",
    "../i18n/route-"+ window.navigator.languages[0]
],function(page_config,$,Backbone){
	var searchLanguage = window.regional['defaultLanguage'].ADVANCEDSEARCH;
    //路由信息
	var routeModel = Backbone.Model.extend({
        url: page_config.routeUrl.getAllUrl,
        idAttribute: "ID"
        
    });
	//目的地信息
	var destModel = Backbone.Model.extend({
       
    });
	//目的地集合
	var destCollection = Backbone.Collection.extend({
		model:destModel
	});
	
	
   //日志信息
	var logModel = Backbone.Model.extend({
        idAttribute: "ID"
        
    });
	var logCollection = Backbone.Collection.extend({
		model:logModel,
		parse: function (response) {
			// route_lineCollection.pageCtrl = response.PAGECTRL;
	         return response.DATA;
	    }
	});
	var route_lineModel = Backbone.Model.extend({
        idAttribute: "ID"
        
    });
	var route_lineCollection = Backbone.Collection.extend({
		model:route_lineModel,
		parse: function (response) {
			 route_lineCollection.pageCtrl = response.PAGECTRL;
	         return response.DATA;
	    }
	}, {
    	pageCtrl: {
            pageNo: 1,
            pageSize: 5
        }
    });
	var trNodeInfoModel = Backbone.Model.extend({
		idAttribute:"trId",
        logs:logCollection,
        route:routeModel,
        route_lines:route_lineCollection,
        set: function(attributes, options) {
        	this.attributes.logs=attributes.logs;
        	this.attributes.route=attributes.route;
        	this.attributes.route_lines=attributes.route_lines;
        	this.attributes.trId=attributes.trId;
        	this.id=attributes.trId;
        }
        
    });
    var routeCollection = Backbone.Collection.extend({
        model: trNodeInfoModel,
        parse: function (response) {
        	routeCollection.pageCtrl = response.PAGECTRL;
            return response.DATA;
        }
    }, {
    	pageCtrl: {
            pageNo: 1,
            pageSize: 5
        }
    });

    return {
    	RouteModel:routeModel
        ,RouteCollection:routeCollection
        ,LogCollection:logCollection
        ,Route_lineCollection:route_lineCollection
        ,DestCollection:destCollection
        ,DestModel:destModel
    };

});