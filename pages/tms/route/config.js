(function(){
    var configObj = {
    	routeUrl:{
            addUrl:"/Sync10-ui/pages/tms/route/createRoute.html",
            saveUrl:"/Sync10/_tms/route/saveRouteInfoAndItem",
            editUrl:"/Sync10/_tms/route/modifyRouteInfoAndItem",
            deleteUrl:"/Sync10/_tms/damage/deleteDamageById",
            getAllUrl:"/Sync10/_tms/route/getRouteList",
            checkRouteUrl:"/Sync10/_tms/route/checkRoute",
            checkRouteNameUrl:"/Sync10/_tms/route/checkRouteName",
            getRouteNameUrl:"/Sync10/_tms/route/findBranchRouteNameByPsId"
        },
        storageTree: {
            url:"/Sync10/_tms/tmsWayItem/WHSList"
        },
        adminSesion:{
        	url:"/Sync10/_tms/admin/getLoginAdmin"
        }
    
    };
	define(configObj);
	
}).call(this);
