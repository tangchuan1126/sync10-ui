/**
 * Created by liyi on 2015.5.11.
 */
"use strict";
define([ "jquery", "backbone", '../config', "handlebars_ext", "../templates",
		"./LogsView","../model/routeModel", "Paging", "art_Dialog/dialog-plus","artDialog"

], function($, Backbone, config, Handlebars, templates,LogsView, models, Paging,
		Dialog) {

	return Backbone.View.extend({
		template : templates.routeSearch,
		initialize : function(options) {
			this.setElement(options.el);
			this.collection = new models.RouteCollection;
		},
		setView : function(views) {
			this.lineListView = views.lineListView;
		},
		Settopnvaheigth : function() {
			var topthead = $(".topthead");
			if (topthead.length > 0) {
				var _this = this;
				var _toptheadfind_table = topthead.find("table").eq(0);
				var __table = _this.$el.find("table").eq("0");
				var bodyw = $("body").width();
				var list_tablew = 0;
				if (bodyw > __table.width()) {
					list_tablew = (bodyw - _this.$el.width());
				}
				topthead.css("left", list_tablew / 2);
				var _thead = __table.find("thead");
				var top_th = topthead.find("th");
				var thwall = [];
				var _thall = _thead.find("th");
				for (var th_i = 0; th_i < _thall.length; th_i++) {
					var itmeth = _thall.eq(th_i);
					thwall.push(itmeth.width());
				}
				var p1px = 0;
				if (__table.width() == 1240) {
					p1px = 1;
				}

				for (var th_b = 0; th_b < top_th.length; th_b++) {
					var itmethb = top_th.eq(th_b);
					itmethb.width(thwall[th_b] - p1px);
				}

				_toptheadfind_table.width(__table.width());

			}

		},
		render:function(searchParam) {
			var tmp = this;
			$.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
			var data = {};
			if(searchParam){
				data=searchParam;
			}
			data.pageNo=models.RouteCollection.pageCtrl.pageNo;
			data.pageSize=models.RouteCollection.pageCtrl.pageSize;
			this.collection.reset();
			var i18nLanguage = window.regional[ 'defaultLanguage' ];
			this.collection.url = config.routeUrl.getAllUrl;
			this.collection.fetch({
				data:data,
				success : function(collection) {
					tmp.$el.html(tmp.template({
						resultList : collection.toJSON(),
						lanModel:i18nLanguage,
						pageCtrl:models.RouteCollection.pageCtrl
					}));
					var table = tmp.$el.find("table.checkin_box").eq(0);
					tmp.topnvascroll(table,tmp);//滚动时始终把表头显示出来
					 Paging.language='cn';
					Paging.update_page( models.RouteCollection.pageCtrl.pageCount ,
							  models.RouteCollection.pageCtrl.pageNo , "pagination" ,
							  function(pageNo) { models.RouteCollection.pageCtrl.pageNo =
							  pageNo; tmp.render(searchParam); });
					$.unblockUI();
				}
			});
		},
		topnvascroll : function(table, v) {
			// 滚动时浮动
			var _this = v;
			var _thead = table.eq("0").find("thead");
			setTimeout(function() {
				var tableth = _thead.html();
				var topthead = $(".topthead");
				_this.Settopnvaheigth();
				var _toppx = window.parseInt(table.position().top);
				$(window).scroll(function(e) {
					var toppx = $(this).scrollTop();
					if (toppx > _toppx) {
						topthead.show();
					} else {
						topthead.hide();
					}

					var win_scrollLeft = $(window).scrollLeft();
					if (win_scrollLeft > 0) {
						topthead.css("left", "-" + win_scrollLeft + "px");
					}
				});
				$(window).resize(function() {
					var topthead = $(".topthead");
					var __table = _this.$el.find("table").eq("0");
					var table_left = __table.offset();
					topthead.css("left", table_left.left + "px");
					$(window).scrollLeft(0);
					_this.Settopnvaheigth();

				});

			}, 300);
			// end 滚动时

		},
		events : {
			"click a[name='editRoute']" : "editRoute",
			"click a[name='deleteRoute']" : "deleteRoute",
			"click a[name='moreLines']" : "moreLines",
			"click a[name='moreLogs']" : "moreLogs"
		},
		editRoute : function(evt) {
			var routeId = $(evt.target).parent().parent().parent("tr").attr("data-trid");
			var trModel = this.collection.findWhere({trId:parseInt(routeId)});
			var url = config.routeUrl.addUrl+"?route_id="+routeId;
			
			window.routemodel=trModel; //全局变量
			var d = Dialog({
					title: "路由信息",
					url:url,
					fixed: false,
					skin: 'test1',
					width:'770px',
					height:'400px',
					onclose:function(){
						d.remove();
					}
			}).showModal();
			top.dialog = d;
		},
		deleteRoute : function(evt) {
			var trNode =$(evt.target).parent().parent().parent("tr");
			var tBodyNode = trNode.parent();
			var upTrNode = tBodyNode.children().eq(trNode.index()-1);
			var routeId = $(evt.target).parent().parent().parent("tr").attr("data-trid");
			upTrNode.remove();
			trNode.remove();
		},
		moreLines:function(evt) {
			var routeId = $(evt.target).parent().parent().parent("tr").attr("data-trid");
			var trModel = this.collection.findWhere({trId:parseInt(routeId)});
			this.lineListView.render(trModel);
		},
		moreLogs:function(evt) {
			var routeId = $(evt.target).parent().parent().parent("tr").attr("data-trid");
			var trModel = this.collection.findWhere({trId:parseInt(routeId)});
			var logsView = new LogsView().render(trModel);
		}
	});
});
