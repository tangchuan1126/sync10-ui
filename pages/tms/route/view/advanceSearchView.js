define(["jquery","backbone","../config",
"../templates",
"immybox",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree"

],
function($,Backbone,config,templates,immybox,AsynLoadQueryTree) {
	return Backbone.View.extend({
			el:"#searchAdvancedTab",
			template:templates.advanceSearch,
			initialize:function(options){
				this.resultView=options.resultView;
				this.psId=options.psId;
			},
			events: {
				"click #btnLoadFilter" : "filter",
				"click #btnReset" : "reset"
			},
			reset:function(){
				$("#currentPsId").removeAttr("data-val").removeAttr("data-name").val(null);
				$("#permitPsId").removeAttr("data-val").removeAttr("data-name").val(null);
				$("#routePsId").removeAttr("data-val").removeAttr("data-name").val(null);
				$("#status").removeAttr("data-value").val(null);
				$("#routeType").removeAttr("data-value").val(null);
				$("#route_name").removeAttr("data-value").val(null);
			},
			getDataTypes:function(typeObj,searchLanguage) {
				var dataTypes = [{"text":searchLanguage.all,"value":"-1"}];
				for(prop in typeObj) {
					dataTypes[dataTypes.length]={"text":typeObj[prop],"value":prop};
				}
				return dataTypes;
			},
			render:function() {
				var searchLanguage = window.regional['defaultLanguage'].ADVANCESEARCH;//查询界面的国际化
				var v = this;
				v.$el.html(v.template({model:searchLanguage}));
				$("#searchAdvancedTabA").text(searchLanguage.title);
				$("#addRoute").append("<i class='glyphicon glyphicon-plus'></i>&nbsp;"+searchLanguage.addRoute_btn);
				$('#status').immybox({//路由状态
					choices: v.getDataTypes(searchLanguage.STATUSKEY,searchLanguage)
				});
				$('#routeType').immybox({//路由类型
					choices: v.getDataTypes(searchLanguage.ROUTETYPEKEY,searchLanguage)
				});
				
			},
			initTree:function(){
				/*var currentPs = new AsynLoadQueryTree({
					renderTo: "#currentPsId",
					Pleaseselect: "Please select",
					dataUrl: config.storageTree.url,
					scrollH: 400,
					multiselect: false,
					Parentclick:true,
					Async: false
					
					});
				currentPs.render();*/
				var parm = {"ps_id":this.psId,"type":1}
				$.ajax({
					url: config.routeUrl.getRouteNameUrl,
					data:parm,
					success:function(data){
						$("#route_name").immybox({
							choices:data.routeList
				        });
					}
				});
				var permitPsId = new AsynLoadQueryTree({
					renderTo: "#permitPsId",
					Pleaseselect: "Please select",
					dataUrl: config.storageTree.url,
					scrollH: 400,
					multiselect: false,
					Parentclick:true,
					Async: true
					
					});
				permitPsId.render();
				var routePsId = new AsynLoadQueryTree({
					renderTo: "#routePsId",
					Pleaseselect: "Please select",
					dataUrl: config.storageTree.url,
					scrollH: 400,
					multiselect: false,
					Parentclick:true,
					Async: true
					
					});
				routePsId.render();
			}
			,
			filter:function() {
				this.resultView.render({
				    "current_ps_id":this.psId,
					"permit_ps_id": $("#permitPsId").attr("data-val"),
					"route_ps_id": $("#routePsId").attr("data-val"),
					"status": $("#status").attr("data-value"),
					"routeType": $("#routeType").attr("data-value"),
					"routeId":$("#route_name").attr("data-value"),
					"type":1
				});
			}
			
	});

}
);