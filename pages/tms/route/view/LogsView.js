define(
["jquery",
 "backbone",
 "../config",
 "handlebars_ext",
"../templates",
"../model/routeModel",
"art_Dialog/dialog-plus"
],
function($,Backbone,config,Handlebars,templates,models,Dialog) {
	return Backbone.View.extend({
		template:templates.logs,
		collection:new models.LogCollection,
		render:function(trModel){
			$.blockUI({message:'<div class="block_message">Please wait......</div>'});
			var logsLanguage = window.regional['defaultLanguage'].LOG;//查询界面的国际化
			var that = this;
			that.$el.html(that.template({resultList:trModel.toJSON().logs,lanModel:logsLanguage}));
        	that.$el.hide().appendTo('body');
        	that.ad = new Dialog({
                content: that.$el.show(),
                lock:true,
                title: "操作日志",
                onshow: function(){
                }

            }).width(610).showModal();
        	$.unblockUI();
			/*var that = this;
			this.collection.reset();
			this.collection.url=config.logs.getAllUrl;
			this.collection.fetch({
                data:{
                  // pageNo:models.Route_lineCollection.pageCtrl.pageNo,
                  //  pageSize:models.Route_lineCollection.pageCtrl.pageSize,
                    routeId:routeId
                },
                success:function(collection){
                	that.$el.html(that.template({resultList:collection.toJSON(),lanModel:logsLanguage}));
                	that.$el.hide().appendTo('body');
                	that.ad = new Dialog({
                        content: that.$el.show(),
                        lock:true,
                        title: "操作日志",
                        onshow: function(){
                        }

                    }).width(610).showModal();
                	$.unblockUI();
                }
			});*/
		},
		getOpreateKey:function(data){
			_.map(data.LOGS,function(ele, index) {
				data.LOGS[index].OPERATE_NAME= Key.get(OrderOperateTypeKey,ele.OPERATE);
			});
		}
	})	

});