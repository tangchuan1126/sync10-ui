define(["jquery","backbone",
        "handlebars_ext","../config","../templates",
        "art_Dialog/dialog-plus",
        "../model/routeModel",
        "/Sync10-ui/lib/DateUtil.js",
        "jqueryui/datepicker"],
function($,Backbone,Handlebars,config,templates,Dialog,models) {
	return Backbone.View.extend(
	{
			el:"#preview",
			template:templates.preview,
			//model:new FilterModel(),
			initialize:function(currentLogin){
				this.currentLogin=currentLogin;
				//employe_name
				
			},
			events:{
				"click #save" : "save",
				"click #preview_pre":"preview_pre"
			},
			render:function(options){
				this.currentLogin.opreateTime=new Date().format("MM/dd/yyyy hh:mm");
				var language = window.regional['defaultLanguage'];//界面的国际化
				if(options != null){
					this.param = options;
					this.routeInfo = options.routeInfo;
					this.destinationInfo = options.destinationInfo;
				}
				var that = this;
				this.$el.html(this.template({
					routeInfo:this.routeInfo.toJSON(),
					currentLogin:this.currentLogin,
					destinationInfo:this.destinationInfo.toJSON(),
					lanModel:language
				}));
			},
			preview_pre:function(){
				$('#tabs li:eq(1) a').tab('show') ;
			},
			save:function(){
				this.postRoute();
			},
			postRoute:function(){
				this.routeInfo.set("DESTINATION",this.destinationInfo);
				if(this.param.op=="add") {
					url=config.routeUrl.saveUrl;
				}else {
					url=config.routeUrl.editUrl;
				}
				this.routeInfo.url=url;
				this.routeInfo.save({},{
					success:function(modelReturned, response, options){
						var dialog = top.dialog;
						var mains="";
					    var iframes=window.top.window.frames["iframe"];
					    if(!iframes || typeof iframes =="undefined"){
					    	//mains=window.parent;
					    	window.top.eventsMgr.trigger("renderList");
					    }else{
					    	mains=window.top.window.frames["iframe"].frames["main"];
					    	mains.eventsMgr.trigger("renderList");
					    }
						dialog.close(modelReturned).remove();
					},
					error:function(collection, response, options){
						console.log("error adding ");
                  }})
			}
	});

}
);