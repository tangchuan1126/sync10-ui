/**
 * Created by liyi on 2015.5.11
 */
"use strict";
define([
    "jquery",
    "backbone",
    "handlebars",
    "view/advanceSearchView",
    "view/exceptionSearchResultView",
    "view/exceptionDelView",
    "blockui",
    "jqueryui/tabs"
],function(
    $
    ,Backbone
    ,Handlebars
    ,AdvanceSearchView
    ,ExceptionSearchResult
    ,ExceptionDelView
){
    (function(){
        $.blockUI.defaults = {
            css: {
                padding:        '8px',
                margin:         0,
                width:          '170px',
                top:            '45%',
                left:           '40%',
                textAlign:      'center',
                color:          '#000',
                border:         '3px solid #999999',
                backgroundColor:'#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius':    '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS:  {
                backgroundColor:'#000',
                opacity:        '0.3'
            },
            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut:  1000,
            showOverlay: true
        };
    })();
    $("#all_tabs").tabs();
    var setActiveTab = function(i){
        $("#all_tabs").tabs("option","active",i);
    };
    var exceptionSearchResult = new ExceptionSearchResult({el:"#data"});
    new AdvanceSearchView({resultView:exceptionSearchResult}).render();
    setActiveTab(0);
    var exceptionDelView = new ExceptionDelView();
    exceptionDelView.setView({exceptionSearchResultView:exceptionSearchResult});
    exceptionSearchResult.setView({exceptionDelView:exceptionDelView});
    exceptionSearchResult.render();
    window.eventsMgr = _.extend({}, Backbone.Events);
	//刷新主界面
	window.eventsMgr.on('renderList', function(param) {
		exceptionSearchResult.render();
	});
});