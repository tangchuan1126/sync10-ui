(function(){
    var configObj = {
        exceptionUrl:{
            addUrl:"/Sync10/_tms/damage/saveDamageInfo",
            editUrl:"/Sync10/_tms/",
            deleteUrl:"/Sync10/_tms/damage/deleteDamageById",
            updateUrl:"/Sync10/_tms/",
            getAllUrl:"/Sync10/_tms/damage/getDamageList",
            dealwithUrl:"/Sync10-ui/pages/tms/exception/dealwith.html",
            getDatilUrl:"/Sync10/_tms/damage/getDamageAndResultById"
        },
        exceptionResultUrl:{
        	addUrl:"/Sync10/_tms/damage/saveDamageResult",
        	editUrl:"/Sync10/_tms/damage/modifyDamageResultInfo"
        },
        personUrl:{
        	getAllUrl:"/Sync10/_tms/damage/getUserByPsId"
        }
    
    };
	define(configObj);
	
}).call(this);
