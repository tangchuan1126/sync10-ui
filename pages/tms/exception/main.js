"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery",
		         "blockui",
			"./view/exceptionDealWithResultView",
			"./view/exceptionDetailResultView",
			"nprogress",
			"./model/exceptionModel",
			"bootstrap",
			"require_css!oso.lib/nprogress/style.css",
			"require_css!oso.lib/nprogress/nprogress.css"
			],
			function($,blockUI,ExceptionDealWithResultView,ExceptionDetailResultView,NProgress,models)
			{
				NProgress.start();
				
				function getQueryString(name) {
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
				
			    var id = getQueryString("id");
			    var isDealWith = getQueryString("isDealWith"); //如果是处理按钮  页面有按钮
			    var status = getQueryString("status"); //获取异常状态  如果是 1 不显示result 2是要result可编辑
			   // if(isDealWith!="1"&&status=="1") {
			    //	$("#dealwith").hide();
			    //	$("#dealwithL").hide();
			  //  }
			    var eventAcrossView = _.extend({}, Backbone.Events);
			    var model = new models.ExcModel;
			    var resultModel = new models.ExceptionResultModel;
				 $.ajax({ 
				        url: config.exceptionUrl.getDatilUrl+"?damage_id="+id, //将来改为session信息
				        type: 'GET', 
				        dataType: 'json', 
				        timeout: 5000, 
				        success: function(data){
				        	model = new models.ExcModel(model.parse(data.EXCEPTION)); //异常信息
				        	if(status!="1") {
				        		resultModel = new models.ExceptionResultModel(resultModel.parse(data.EXCEPTIONRESULT));
				        	} 
				        	var options={isReadonly:isDealWith};
				        	if(isDealWith!='0')
				        		new ExceptionDetailResultView({el:"#exception"}).render(model);
				        	else{
				        		$("#exceptionL").hide();
				        		$("#dealwithL").addClass("active");
				        		$("#dealwith").addClass("in");
				        		$("#dealwith").addClass("active");
				        	}
				        	new ExceptionDealWithResultView({el:"#dealwith"}).render(resultModel,options);
				        	
				        	
				        	NProgress.done();
				        } ,
				        error:function(e)
				        {
				        	console.log(e);
				        	NProgress.done();
				        }
				    }); 
				    
				    //NProgress.done();
			});
	});