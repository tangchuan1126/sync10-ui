define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['advanceSearch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"condition\">\n	 <div class=\"row\">\n	 		<div class=\"col-sm-4\"> \n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-120\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.documentno_lable : stack1), depth0))
    + "</span>\n                     <input id=\"pallet_id\" type=\"text\" class=\"form-control min-200\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.documentno_placeholder : stack1), depth0))
    + "\">\n                </div>\n            </div>\n            <div class=\"col-sm-4\"> \n                <div class=\"input-group \">\n                     <span class=\"input-group-addon form-label min-120\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.documenttype_lable : stack1), depth0))
    + " </span>\n                     <input class=\"form-control min-200\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.documenttype_placeholder : stack1), depth0))
    + "\" id=\"pallet_type\">\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-120\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.process_lable : stack1), depth0))
    + "</span>\n                     <input type=\"text\" class=\"form-control min-200\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.process_placeholder : stack1), depth0))
    + "\" id=\"happn_link\">\n                </div> \n            </div> \n        </div> \n        <div class=\"row\">\n        	<div class=\"col-sm-4\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-120\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.exceptiontype_lable : stack1), depth0))
    + "</span>\n                     <input type=\"text\" class=\"form-control min-200\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.exceptiontype_placeholder : stack1), depth0))
    + "\" id=\"exception_type\">\n                </div>\n            </div>  \n            <div class=\"col-sm-4\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-120\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.status_lable : stack1), depth0))
    + "</span>\n                     <input id=\"status\" type=\"text\" class=\"form-control min-200\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.status_placeholder : stack1), depth0))
    + "\">\n                </div>\n            </div>\n        	<div class=\"col-sm-3\">\n                <button type=\"button\" class=\"btn btn-info\" id=\"filter\" style=\"width:100px;float:right;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.search : stack1), depth0))
    + "</button>\n            </div>  \n        </div>   \n</div>\n";
},"useData":true});
templates['del_exception'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"delete_account_dialog\">\n    "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1['delete'] : stack1), depth0))
    + "\n	<span>\n        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ID : stack1), depth0))
    + "\n    </span>\n    ?\n</div>";
},"useData":true});
templates['exception_dealwith'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "\n<div class=\"panel-body form-horizontal\">\n	<div class=\"form-group\">\n		<label for=\"etd\" class=\"col-xs-3 control-label required\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.responsible_lable : stack1), depth0))
    + "</label>\n		<div class=\"col-xs-6\">\n			<input type=\"text\" class=\"form-control\" id=\"responsible\" name=\"responsible\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.responsible_placeholder : stack1), depth0))
    + "\"/>\n			<input type=\"hidden\" id=\"responsible_id\" name=\"responsible_id\" />\n			<input type=\"hidden\" id=\"id\" name=\"id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ID : stack1), depth0))
    + "\"> \n		</div>\n	</div>\n	<div class=\"form-group\">\n		<label for=\"etd\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.dealWithResult_lable : stack1), depth0))
    + "</label>\n		<div class=\"col-xs-6\">\n			<label class=\"radio-inline\">\n			  <input type=\"radio\" name=\"status\" id=\"resulet1\" checked value=\"0\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.dealWithResult_0 : stack1), depth0))
    + " \n			</label>\n			<label class=\"radio-inline\">\n			  <input type=\"radio\" name=\"status\" id=\"resulet2\" value=\"1\" title=\"\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.dealWithResult_1 : stack1), depth0))
    + " \n			</label>\n			<label class=\"radio-inline\">\n			  <input type=\"radio\" name=\"status\" id=\"resulet3\" value=\"2\" title=\"\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.dealWithResult_2 : stack1), depth0))
    + " \n			</label>\n		</div>\n	</div>\n	\n	<div class=\"form-group\">\n		<label for=\"etd\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.remark_lable : stack1), depth0))
    + "</label>\n		<div class=\"col-xs-6\">\n		<textarea class=\"form-control\" rows=\"2\" cols=\"10\" id=\"result_content\" name=\"result_content\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.RESULT_CONTENT : stack1), depth0))
    + "</textarea>\n		</div>\n	</div>\n</div>\n<div class=\"footer\">\n	<div class=\"opbar\">\n		<button type=\"button\" id=\"submit\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.submit : stack1), depth0))
    + "</button>\n	</div>\n</div>";
},"useData":true});
templates['exception_dealwithread'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"table\">\n      <tbody>\n        <tr >\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.responsible_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.RESPONSIBLE_NAME : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\" class=\"warning\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.dealWithResult_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\" class=\"warning\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.STATUSNAME : stack1), depth0))
    + "</td>\n          <td class=\"warning\"></td>\n        </tr>\n        <tr> \n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operatorname_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.OPERATOR_NAME : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operatordate_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.OPERATOR_TIME : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.remark_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.RESULT_CONTENT : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        \n      </tbody>\n    </table>";
},"useData":true});
templates['exception_detail'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"table\">\n      <tbody>\n        <tr class=\"active\">\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.bol_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.BOL_ID : stack1), depth0))
    + "\n          	<input type=\"hidden\" id=\"damage_id\" name=\"damage_id\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ID : stack1), depth0))
    + "\"> \n          </td>\n          <td>&nbsp;</td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.process_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.HAPPEN_LINK : stack1), depth0))
    + "</td>\n          <td></td>\n          \n        </tr>\n        <tr class=\"success\">\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.exceptiontype_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.EXCEPTION_TYPE : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.documenttype_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.PALLET_TYPE : stack1), depth0))
    + "</td>\n          <td></td>\n          \n        </tr>\n        <tr class=\"info\">\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.documentno_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.PALLET_ID : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.status_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.STATUSNAME : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr class=\"warning\">\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.timeremaining_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.REMAININGDAY : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operatorname_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.OPERATOR_NAME : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operatordate_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.OPERATOR_TIME : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        <tr>\n          <th scope=\"row\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.remark_lable : stack1), depth0))
    + "</th>\n          <td style=\"text-align: left;\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.DESCRIPTION : stack1), depth0))
    + "</td>\n          <td></td>\n        </tr>\n        \n      </tbody>\n    </table>\n\n";
},"useData":true});
templates['search_result_templet'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "            <tr data-id=\""
    + alias2(alias1((depth0 != null ? depth0.ID : depth0), depth0))
    + "\">\n            	<td>\n                   "
    + alias2(alias1((depth0 != null ? depth0.BOL_ID : depth0), depth0))
    + "\n                </td>\n                <td >\n                    "
    + alias2(alias1((depth0 != null ? depth0.PALLET_ID : depth0), depth0))
    + "\n                </td>\n                <td>\n                    "
    + alias2(alias1((depth0 != null ? depth0.HAPPEN_LINK : depth0), depth0))
    + "\n                </td>\n                <td>\n                    "
    + alias2(alias1((depth0 != null ? depth0.EXCEPTION_TYPE : depth0), depth0))
    + "\n                </td>\n                <td >\n                    "
    + alias2(alias1((depth0 != null ? depth0.PALLET_TYPE : depth0), depth0))
    + "\n                </td>\n                \n                <td  style=\"display:none\">\n                    \n                </td>\n                <td >\n                    "
    + alias2(alias1((depth0 != null ? depth0.STATUSNAME : depth0), depth0))
    + "\n                </td>\n                \n                <td >\n                 	"
    + alias2(alias1((depth0 != null ? depth0.REMAININGDAY : depth0), depth0))
    + "  \n                </td>\n                 <td >\n                 	"
    + alias2(alias1((depth0 != null ? depth0.OPERATOR_NAME : depth0), depth0))
    + "  \n                </td>\n                <td >\n                 	"
    + alias2(alias1((depth0 != null ? depth0.OPERATOR_TIME : depth0), depth0))
    + "  \n                </td>\n               \n                <td >\n                    <button type=\"button\" name=\"detailException\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias3).call(depth0,"this.STATUS==3",{"name":"xif","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + " title=\"Result\" style=\"padding: 3px 7px;\" class=\"btn btn-default\" aria-label=\"Center Align\"><span class=\"glyphicon glyphicon-list\" aria-hidden=\"true\"></span></button>\n                    <button type=\"button\" name=\"dealwith\" title=\"DealWith\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias3).call(depth0,"this.STATUS!=3",{"name":"xif","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + " style=\"padding: 3px 7px;\" class=\"btn btn-default\" aria-label=\"Left Align\"><span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span></button>\n                    <button type=\"button\" style=\"padding: 3px 7px;display:none\" class=\"btn btn-default\" aria-label=\"Left Align\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></button>\n                    <button type=\"button\" title=\"Delete\" name=\"delException\" style=\"padding: 3px 7px;display:none\" class=\"btn btn-default\" "
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias3).call(depth0,"this.STATUS!=3",{"name":"xif","hash":{},"fn":this.program(3, data, 0),"inverse":this.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + " aria-label=\"Left Align\"><span class=\"glyphicon glyphicon-remove-sign\" aria-hidden=\"true\"></span></button>\n                </td>\n            </tr> \n";
},"3":function(depth0,helpers,partials,data) {
    return "";
},"5":function(depth0,helpers,partials,data) {
    return "disabled";
},"7":function(depth0,helpers,partials,data) {
    return "	        <tr>\n	            <td colspan=\"9\" style=\"text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;\">No Data.</td>\n	        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div>\n<table class=\"table table-hover table-bordered\">\n        <thead>\n          <tr>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.bol_title : stack1), depth0))
    + "</th>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.documentno_title : stack1), depth0))
    + "</th>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.process_title : stack1), depth0))
    + "</th>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.exceptiontype_title : stack1), depth0))
    + "</th>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.documenttype_title : stack1), depth0))
    + "</th>\n            <th style=\"display:none\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.files_title : stack1), depth0))
    + "</th>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.status_title : stack1), depth0))
    + "</th>\n            <th >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.timeremaining_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.operatorname_title : stack1), depth0))
    + "</th> \n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.operatordate_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.resultTitle : depth0)) != null ? stack1.operation_title : stack1), depth0))
    + "</th>\n          </tr>\n        </thead>\n        <tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "        </tbody>\n      </table>\n      </div>\n      <div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\">\n        </ul>\n    </div>\n";
},"useData":true});
return templates;
});