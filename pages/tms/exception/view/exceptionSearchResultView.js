/**
 * Created by liyi on 2015.5.11.
 */
"use strict";
define([
    "jquery",
    "backbone",
    '../config',
    "handlebars_ext",
    "../templates",
    "../model/exceptionModel",
    "Paging",
    "slidePanel",
    "require_css!oso.lib/slidePanel/css/style.css"
    
],function($,Backbone,config,handlebars_ext,templates,models,Paging,SlidePanel){

    return Backbone.View.extend({
        template:templates.search_result_templet,
        initialize:function(options){
            this.setElement(options.el);
            this.collection = new models.ExcCollection;
        },
        setView:function(views){
            this.exceptionDetailResultView = views.exceptionDetailResultView;
            this.exceptionDelView = views.exceptionDelView;
        },
        render:function(searchParam){
        	var data = {};
            //搜索参数
            if(!searchParam){
                searchParam = new models.SearchModel();
                $.blockUI({message:'<img src="image/loadingAnimation6.gif" align="absmiddle"/>'});
            } else {
            	data=searchParam;
            }
            data.pageNo=models.ExcCollection.pageCtrl.pageNo;
            data.pageSize=models.ExcCollection.pageCtrl.pageSize;
            var tmp = this;
            this.collection.reset();
            this.collection.url=config.exceptionUrl.getAllUrl;
            var titleLanguage = window.regional['defaultLanguage'].RESULTTITLE;//查询列表标题的国际化
            this.collection.fetch({
                data:data,
                success:function(collection){
                	tmp.$el.html(tmp.template({
                        resultList:collection.toJSON(),
                        resultTitle:titleLanguage,
                        pageCtrl:models.ExcCollection.pageCtrl
                    }));
                	
                    Paging.update_page(
                        models.ExcCollection.pageCtrl.pageCount
                    	, models.ExcCollection.pageCtrl.pageNo
                        , "pagination"
                        , function(pageNo) {
                            models.ExceptionCollection.pageCtrl.pageNo = pageNo;
                            tmp.render(searchParam);
                    });

                    $.unblockUI();
                }   
            });
        },
        events:{
            "click button[name='delException']":"delException",
            "click button[name='detailException']":"detailException",
            "click button[name='dealwith']":"dealWith"
        },
        
        dealWith:function(evt) {
        	var v= this;
        	var slidePanelCloseBefore = function(param){
            	v.render();
            };
            var titleLanguage = window.regional['defaultLanguage'].DEALWITH;
        	var id = $(evt.target).parents("tr").data("id");
        	var model = this.collection.findWhere({ID:id});
        	var slide = new SlidePanel({
	            title: titleLanguage.title,
	            url: config.exceptionUrl.dealwithUrl+"?id="+id+"&isDealWith=1&status="+model.toJSON().STATUS,
	            duration: 500,
	            topLine: 0,
	            slideLine: "50%",
	            ShutdownCallback : slidePanelCloseBefore
	        });
         slide.open();
        },
        detailException:function(evt){
        	var detalLanguage = window.regional['defaultLanguage'].DETAIL;
        	var id = $(evt.target).parents("tr").data("id");
        	var model = this.collection.findWhere({ID:id});
        	var slide = new SlidePanel({
	            title: detalLanguage.title+"_"+id,
	            url: config.exceptionUrl.dealwithUrl+"?id="+id+"&isDealWith=0&status="+model.toJSON().STATUS,
	            duration: 500,
	            topLine: 0,
	            slideLine: "50%"
	        });
         slide.open();
        },
        delException:function(evt){
        	var id = $(evt.target).parents("tr").data("id");
            var model = this.collection.findWhere({ID:id});
        	this.exceptionDelView.render(model);
        }
    });
});
