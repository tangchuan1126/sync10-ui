define(["jquery","backbone","../config",
"../templates",
"immybox"

],
function($,Backbone,config,templates,immybox) {
	return Backbone.View.extend(
	{
			el:"#advanceSearchA",
			template:templates.advanceSearch,
			initialize:function(options)
			{
				this.resultView = options.resultView;
				this.param=options;
			},
			events:
			{
				"click #filter" : "filter"
			},
			getDataTypes:function(typeObj,searchLanguage) {
				var dataTypes = [{"text":searchLanguage.all,"value":"-1"}];
				for(prop in typeObj) {
					dataTypes[dataTypes.length]={"text":typeObj[prop],"value":prop};
				}
				return dataTypes;
			},
			render:function()
			{
				var searchLanguage = window.regional['defaultLanguage'].ADVANCEDSEARCH;//查询界面的国际化
				$("#title").text(searchLanguage.title);
				$("#tab").text(searchLanguage.searchtitle);
				var v = this;
				v.$el.html(v.template({model:searchLanguage}));
				$('#happn_link').immybox({//异常环节
					choices:v.getDataTypes(searchLanguage.PROCESSKEY,searchLanguage)
				});
				
				$('#exception_type').immybox({
					choices:v.getDataTypes(searchLanguage.EXCEPTIONKEY,searchLanguage)
				});
				
				$('#pallet_type').immybox({//货物类型
					choices:v.getDataTypes(searchLanguage.DOCUMENTTYPEKEY,searchLanguage)
				});
				$('#status').immybox({//单据状态
					choices: v.getDataTypes(searchLanguage.STATUSKEY,searchLanguage)
				});
				
			},
			filter:function()
			{
				this.resultView.render({
				    "exception_type":$("#exception_type").attr("data-value"),
					"happn_link": $("#happn_link").attr("data-value"),
					"pallet_type": $("#pallet_type").attr("data-value"),
					"status": $("#status").attr("data-value"),
					"pallet_id": $("#pallet_id").val()
				});
			}
			
	});

}
);