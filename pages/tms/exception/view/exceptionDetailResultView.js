/**
 * Created by liyi on 2015.5.11.
 */
"use strict";
define([
    "jquery",
    "backbone",
    '../config',
    "handlebars",
    "../templates",
    "../model/exceptionModel",
    "artDialog"
    
],function($,Backbone,config,Handlebars,templates,models){

    return Backbone.View.extend({
        template:templates.exception_detail,
        initialize:function(options){
            this.setElement(options.el);
        },
        setView:function(views){
          
        },
        render:function(model){
        	var detailLanguage = window.parent.regional['defaultLanguage'].DETAIL;//异常明细的国际化
            var v = this;
            $("#exceptionLink").text(detailLanguage.tabTitle);
            v.$el.html(v.template({model:model.toJSON(),lanModel:detailLanguage}));
        }
        
    });
});
