/**
 * Created by liyi on 2015.5.11.
 */
"use strict";
define([
    "jquery",
    "backbone",
    '../config',
    "handlebars",
    "../templates",
    "../model/exceptionModel",
    "immybox"
],function($,Backbone,config,Handlebars,templates,models,immybox){
    return Backbone.View.extend({
        template:templates.exception_dealwith,
        initialize:function(options){
            this.setElement(options.el);
        },
        setView:function(views){
            this.exceptionDetailResultView = views.exceptionDetailResultView;
            this.exceptionDelView = views.exceptionDelView;
        },
        events:{
            "click #submit":"addExpResult"
        },
        changeItem:function(e,d){
        	$("#responsible_id").val(d.value);
        	$("#responsible").val(d.text);
        },
        addExpResult:function(){
        	var url = "";
        	var exceptionResultModel = new models.ExceptionResultModel;
        	var damage_id = $("#damage_id").val();
        	var status = $("input[name='status']:checked").val();
        	var responsible= $("#responsible").val();
        	var responsible_id =$("#responsible_id").val();
        	var result_content = $("#result_content").val();
        	exceptionResultModel.set({damage_id:damage_id,status:status,responsible_name:responsible,responsible_id:responsible_id,result_content:result_content});
        	if($("#id").val()!="") {
        		url = config.exceptionResultUrl.editUrl;
        		exceptionResultModel.set("id",$("#id").val());
        		exceptionResultModel.set("ID",$("#id").val());
        	} else {
        		url = config.exceptionResultUrl.addUrl;
        	}
        	exceptionResultModel.url=url;
        	exceptionResultModel.save({},{
                success: function (data) {
                	if(data) {
                		var _docuel=window.parent.document;
        				var slide_contentbox=_docuel.querySelector(".slide-wrap");
             			var slide_modal=_docuel.querySelector(".slide-modal");
        				slide_contentbox.style.display="none";
              			slide_modal.style.display="none";
              			window.parent.eventsMgr.trigger("renderList");
                	}
                }
        	});
        },
        getDataTypes:function(data) {
			var dataTypes = [];
			for(var i=0;i<data.length;i++) {
				dataTypes[dataTypes.length]={"text":data[i].EMPLOYE_NAME,"value":data[i].ADID};
			}
			return dataTypes;
		},
        render:function(model,options){
        	var v = this;
        	
        	var dealWithLanguage = window.parent.regional['defaultLanguage'].DEALWITH;//异常处理的国际化
        	if(options.isReadonly=="0") { //展示
        		v.template=templates.exception_dealwithread;
        		v.$el.html(v.template({model:model.toJSON(),lanModel:dealWithLanguage}));
        	} else {
        		v.$el.html(v.template({model:model.toJSON(),lanModel:dealWithLanguage}));
        		$.get(config.personUrl.getAllUrl,function(data){
        			$('#responsible').immybox({//异常环节
        				Defaultselect:model.toJSON().RESPONSIBLE_ID,
        				change:v.changeItem,
    					choices:v.getDataTypes(data.DATA)
    				});
        			 
        		})
        		
        	}
        	$("#dealwithLink").text(dealWithLanguage.tabTitle);
        	var currentStatus = model.toJSON().STATUS;
        	
        	var statusNodes = $("INPUT[name='status']");     
        	for(var i=0;i<statusNodes.length;i++) {
        		if(statusNodes[i].value==currentStatus) {
        			statusNodes[i].checked=true;
        			break;
        		}
        	}
        	
        	
        }
    });
});
