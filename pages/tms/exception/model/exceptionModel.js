/**
 * Created by liyi on 2015.5.11.
 */

"use strict";
define([
    "../config",
    "jquery",
    "backbone",
    "../i18n/exception-"+ window.navigator.languages[0]
],function(page_config,$,Backbone){
	var searchLanguage = window.regional['defaultLanguage'].ADVANCEDSEARCH;
    var exceptionModel = Backbone.Model.extend({
        url: page_config.exceptionUrl.getAllUrl,
        idAttribute: "ID",
        parse: function(data,options) {
        	if(data==undefined) return;
        	if(data.EXCEPTION_TYPE!=undefined)
        		data.EXCEPTION_TYPE = searchLanguage.EXCEPTIONKEY[data.EXCEPTION_TYPE];
        	if(data.PALLET_TYPE!=undefined)
        		data.PALLET_TYPE = searchLanguage.DOCUMENTTYPEKEY[data.PALLET_TYPE];
        	if(data.STATUS!=undefined)
        		data.STATUSNAME = searchLanguage.STATUSKEY[data.STATUS];
        	if(data.HAPPEN_LINK!=undefined)
        		data.HAPPEN_LINK = searchLanguage.PROCESSKEY[data.HAPPEN_LINK];
           return data;
        }	
    });

    var exceptionResultModel = Backbone.Model.extend({
    	idAttribute: "ID",
    	parse: function(data,options) {
    		if(data.STATUS!=undefined) {
    			data.STATUSNAME=searchLanguage.EXCEPTIONRESULT[data.STATUS];
    		}
    		return data;
    	}
    });
    var exceptionCollection = Backbone.Collection.extend({
        url: page_config.exceptionUrl.getAllUrl,
        model: exceptionModel,
        parse: function (response) {
        	exceptionCollection.pageCtrl = response.PAGECTRL;
            return response.DATA;
        }
    }, {
    	pageCtrl: {
            pageNo: 1,
            pageSize: 15
        }
    });


    //搜索参数模型
    var SearchModel = Backbone.Model.extend({
        defaults: {
            searchConditions: ""
        }
    });

    return {
    	ExcModel:exceptionModel
        ,ExcCollection:exceptionCollection
        ,SearchModel:SearchModel
        ,ExceptionResultModel:exceptionResultModel
    };

});