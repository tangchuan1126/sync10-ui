;( function( w ) {
	w.regional = {};
	var globalBol="运单号";
	var globalProcess="异常环节";
	var globalExceptionType="异常类型";
	var globalDocumentType="货物类型";
	var globalDocumentNo="货物号";
	var globalStatus="异常处理状态";
	var globalTimeremaining="剩余时间(天)";
	
	var globalOperatorName="操作人";
	var globalOperatorDate="操作时间";
	var globalRemark="备注";
	
	w.regional['zh-CN'] = {
		"ADVANCEDSEARCH":{ //查询条件
			"process_lable" : globalProcess,
			"process_placeholder":"请选择异常环节",
			"exceptiontype_lable":globalExceptionType,
			"exceptiontype_placeholder":"请选择异常类型",
			"documenttype_lable":globalDocumentType,
			"documenttype_placeholder":"请选择货物类型",
			"documentno_lable":globalDocumentNo,
			"documentno_placeholder":"请输入货物号",
			"status_lable":globalStatus,
			"status_placeholder":"请选择异常状态",	
			"search":"查询",
			"searchtitle":"高级查询",
			"all":"全部",
			"title":"货物异常信息",
			"EXCEPTIONKEY":{
				"1":"损坏",
				"2":"遗失",
				"3":"错收"
			},
			"PROCESSKEY":{
				"1":"收货",
				"2":"分拣",
				"3":"发货"
			},
			"DOCUMENTTYPEKEY":{
				"2":"托盘",
				"1":"包裹"
			},
			"STATUSKEY":{
				"1":"创建",
				"2":"处理中",
				"3":"完成"
				
			},
			"EXCEPTIONRESULT":{
				"0":"待定",
				"1":"不正常",
				"2":"正常"
			}
		},
		"RESULTTITLE":{//表格列表标题
			"bol_title":globalBol,
			"process_title":globalProcess,
			"exceptiontype_title":globalExceptionType,
			"documenttype_title":globalDocumentType,
			"documentno_title":globalDocumentNo,
			"status_title":globalStatus,
			"timeremaining_title":globalTimeremaining,
			"files_title":"异常文件",
			"operation_title":"操作",
			"operatordate_title":globalOperatorDate,
			"operatorname_title":globalOperatorName
		},
		"DETAIL":{ //明细信息
			"title":"异常明细",
			"tabTitle":"异常信息",
			"bol_lable":globalBol,
			"process_lable" : globalProcess,
			"exceptiontype_lable":globalExceptionType,
			"documenttype_lable":globalDocumentType,
			"documentno_lable":globalDocumentNo,
			"status_lable":globalStatus,
			"timeremaining_lable":globalTimeremaining,
			"operatorname_lable":globalOperatorName,
			"operatordate_lable":globalOperatorDate,
			"remark_lable":globalRemark
		},
		"DEALWITH":{//异常处理
			"title":"异常结果",
			"tabTitle":"异常处理结果",
			"responsible_lable":"责任人",
			"responsible_placeholder":"请选择责任人",
			"dealWithResult_lable":"处理结果",
			"dealWithResult_0":"待定",
			"dealWithResult_1":"不正常",
			"dealWithResult_2":"正常",
			"operatorname_lable":globalOperatorName,
			"operatordate_lable":globalOperatorDate,
			"remark_lable":globalRemark,
			"submit":"提交"
		},
		"DELETEEXCEPTION":{//删除异常信息
			"delete":"删除",
			"title":"删除异常",
			"sure":"确认",
			"cancel":"取消"
		}

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )