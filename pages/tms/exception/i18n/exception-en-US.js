;( function( w ) {
	w.regional = {};
	var globalBol="Bol";
	var globalProcess="Process";
	var globalExceptionType="Exception Type";
	var globalDocumentType="Document Type";
	var globalDocumentNo="Document No";
	var globalStatus="Status";
	var globalTimeremaining="Time remaining(Day)";
	var globalOperatorName="Operator Name";
	var globalOperatorDate="Operator Date";
	var globalRemark="Remark";
	
	w.regional['en-US'] = {
		"ADVANCEDSEARCH":{ //查询条件
			"process_lable" : globalProcess,
			"process_placeholder":"Select Process",
			"exceptiontype_lable":globalExceptionType,
			"exceptiontype_placeholder":"Select Exception Type",
			"documenttype_lable":globalDocumentType,
			"documenttype_placeholder":"Select Document Type",
			"documentno_lable":globalDocumentNo,
			"documentno_placeholder":"Enter Document No",
			"status_lable":globalStatus,
			"status_placeholder":"Select Status",	
			"search":"Search",
			"searchtitle":"Advanced Search",
			"all":"All",
			"title":"Exception-Infomation",
			"EXCEPTIONKEY":{
				"1":"Damage",
				"2":"Lost",
				"3":"Wrong Receive"
			},
			"PROCESSKEY":{
				"1":"Receiving",
				"2":"Pickup",
				"3":"Pickup"
			},
			"DOCUMENTTYPEKEY":{
				"2":"Pallet",
				"1":"TN"
			},
			"STATUSKEY":{
				"1":"Create",
				"2":"Processing",
				"3":"Finish"
			},
			"EXCEPTIONRESULT":{
				"0":"Pending",
				"1":"Not normal",
				"2":"Normal"
			}
		},
		"RESULTTITLE":{//表格列表标题
			"bol_title":globalBol,
			"process_title":globalProcess,
			"exceptiontype_title":globalExceptionType,
			"documenttype_title":globalDocumentType,
			"documentno_title":globalDocumentNo,
			"status_title":globalStatus,
			"timeremaining_title":globalTimeremaining,
			"files_title":"files",
			"operatordate_title":globalOperatorDate,
			"operatorname_title":globalOperatorName
			"operation_title":"Operation"
		},
		"DETAIL":{ //明细信息
			"title":"Exception Detail",
			"tabTitle":"Exception Information",
			"bol_lable":globalBol,
			"process_lable" : globalProcess,
			"exceptiontype_lable":globalExceptionType,
			"documenttype_lable":globalDocumentType,
			"documentno_lable":globalDocumentNo,
			"status_lable":globalStatus,
			"timeremaining_lable":globalTimeremaining,
			"operatorname_lable":globalOperatorName,
			"operatordate_lable":globalOperatorDate,
			"remark_lable":globalRemark
		},
		"DEALWITH":{//异常处理
			"title":"Result",
			"tabTitle":"Exception Deal With Result",
			"responsible_lable":"Responsible",
			"responsible_placeholder":"Select Responsible",
			"dealWithResult_lable":"Deal With Result",
			"dealWithResult_0":"Pending",
			"dealWithResult_1":"Not normal",
			"dealWithResult_2":"Normal",
			"operatorname_lable":globalOperatorName,
			"operatordate_lable":globalOperatorDate,
			"remark_lable":globalRemark,
			"submit":"提交"
		},
		"DELETEEXCEPTION":{//删除异常信息
			"delete":"Deleted",
			"title":"Del Exception",
			"sure":"Sure",
			"cancel":"cancel"
		}

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'en-US' ];

}( window ) )