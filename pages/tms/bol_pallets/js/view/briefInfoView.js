define(["jquery","backbone","../../config","../../templates","artDialog","../../../handlebar_ext.js"],
function($,Backbone,config,templates) {
	return Backbone.View.extend(
	{
			el:"#briefInfo",
			template:templates.briefInfo,
			model:new (Backbone.Model.extend({url:config.bolBaiscInfo.url}))(),
			initialize:function(param)
			{
				var that =this;
				this.bol_id= param.BOL_ID;

			},
			events:
			{
				"click #edit" : "edit",
				"click #appointment" : "appointment",
				
			},
		
			
			render:function(data)
			{
				var soinfoLanguage = window.regional['defaultLanguage'].SOINFO;
				var v = this;
				console.log(data);
				var renderData = data;
				if(renderData == null || renderData == undefined)
				{
					this.model.url = this.model.url+"?bol_id="+this.bol_id;
					this.model.fetch({
						success:function(bolData)
						{
							renderData = bolData.toJSON();
							//console.log(bolData.toJSON());
							v.$el.html(v.template({model:renderData,lanModel:soinfoLanguage}));
						}
					});
				}
				else
				{
					v.$el.html(v.template({model:renderData,lanModel:soinfoLanguage}));
				}
				return this;
			},
			getSelect: function(objlist,language,ul)
			{
				
			}
	});

}
);this