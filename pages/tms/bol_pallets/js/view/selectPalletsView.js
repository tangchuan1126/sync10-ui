define(["jquery","backbone","../../config","../../templates","oso.lib/table/js/table"],
function($,Backbone,config,templates,Table) {
	return Backbone.View.extend(
	{
			el:"#selectDiv",
			template:templates.selectPallets,
			//model:new FilterModel(),
			initialize:function(param)
			{
				this.param = param;
				this.bol_id = param.data.BOL_ID;
			},
			events:
			{
				"click #select" : "displaySelected",
				"click #search" : "reloadTable"
			},
			render:function(data)
			{
				var palletsLanguage = window.regional['defaultLanguage'].ADDPALLETS;
				var v = this;
				v.$el.html(v.template({lanModel:palletsLanguage}));
				return this;
			},
			displaySelected: function()
			{
				var rows = this.table.getSelected();
	            this.addTOBol(rows);
				if(this.param.op == 0)
				{
					//编辑模式，先进入已选择页面
					$("#selectedDiv").removeClass('back');
			 		$("#selectDiv").removeClass('front');
				}
				else
				{
					//新增模式，先进入可选择页面
					$("#selectedDiv").addClass('front');
			 		$("#selectDiv").addClass('back');
				}
			 	this.param.eventAcrossView.trigger('renderSelected');
			},
			reloadTable:function()
			{
				if(this.table)
				{
					var requestDay_min = $("#requestDay_min").val();
					var requestDay_max = $("#requestDay_max").val();
					var shipTo = $("#shipTo").val();
					var shipFrom = this.param.SHIPPER_WHS_ID;
					this.table.reload({queryParams:{
						"requestDay_min":requestDay_min,
						"requestDay_max":requestDay_max,
						"consignee_whs_id":shipTo,
						"so_id":this.param.data.SO_ID
					}});
				}
				else
				{
					this.initTable();
				}
			},
			initTable:function()
			{
				var palletsLanguage = window.regional['defaultLanguage'].ADDPALLETS;
				var queryCondition = {
					"so_id":this.param.data.SO_ID
				};
				var url = null ;
				if(this.param.data.SO_ID)
				{
					url = config.searchSOPallets.url;
				}
				else
				{

				}
				var that = this;
	            var table = new Table({
				container : '#result',
				//url : "../data/palltes.json",
				url:url,
			//	height: 300,
				rownumbers : false,
				singleSelection : false,
				pagination: false,
				checkbox : true,
				pagination:true,
				queryParams:queryCondition,
				method:"POST",
				pageSize:30,
				pageNumber:1,
				pageList: ["20","30","50","100","200"],
				datasName: 'DATA',
				columns:[
					//{field:'B2B_DETAIL_ID',title:'ID',width:10,hidden:true},
					{field:'ID',title:palletsLanguage.no_title,width:100},
					{field:'TO_WHS_NAME',title:palletsLanguage.shipto_title,width:120},
					{field:'WEIGHT',title:palletsLanguage.weight_title,width:100},
					{field:'REQUESTDAY',title:palletsLanguage.requestday_title,width:100},
					{field:'TYPE',title:palletsLanguage.type_title,width:100,formatter:function(data, row){
						if(data=="1")
							return "Package"
						else
							return "Pallet"
					}},
				]/*,
				toolbar : [
	            {
	                text : 'Add to BOL',
	                iconCls : 'add',
	                handler : function(){
	                   var rows = table.getSelected();
	                   console.log(rows);
	                   that.addTOBol(rows);
	                }
	            }
	       		] */           
				});
				this.table=table;
			},
			addTOBol:function(rows)
			{
				var that = this;
				if(rows.length==0){
					that.reloadTable();
					return;
				}
				$.ajax({
					url: config.addPalletsToBol.url,
					type: 'POST',
					dataType: 'json',
					async:false,
					contentType:"application/json; charset=UTF-8",
					data: JSON.stringify({
						"bol_id":this.bol_id+"",
						"so_id":this.param.data.SO_ID+"",
						"pallets":rows
						}),
				})
				.done(function() {
					that.reloadTable();
				})
				.fail(function() {
					console.log("error");
				});
			
				

			}
	});

}
);