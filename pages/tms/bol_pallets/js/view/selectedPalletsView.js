define(["jquery","backbone","../../config","../../templates","oso.lib/table/js/table","artDialog"],
function($,Backbone,config,templates,Table) {
	return Backbone.View.extend(
	{
			el:"#selectedDiv",
			template:templates.selectedPallets,
			//model:new FilterModel(),
			initialize:function(param)
			{
				this.param = param;
			},
			events:
			{
				"click #selected" : "displaySelect",
				"click #finish" : "finish"
			},
			preRender:function()
			{
				var oldpalletsLanguage = window.regional['defaultLanguage'].OLDPALLETS;
				var v = this;
				v.$el.html(v.template({lanModel:oldpalletsLanguage
					
				}));
			},
			render:function()
			{
				var v = this;
				this.preRender(); 
				return this;
			},
			finish:function()
			{
				//window.close();
				var _docuel=window.parent.document;
				var slide_contentbox=_docuel.querySelector(".slide-wrap");
     			var slide_modal=_docuel.querySelector(".slide-modal");
				slide_contentbox.style.display="none";
      			slide_modal.style.display="none";
      			if(this.param.sourcepath == "so"){
                    window.parent.eventsMgr.trigger("renderSoList");
      			}else {
      				window.parent.eventsMgr.trigger('renderBolList');
      			}
			},
			reloadTable:function()
			{
				if(this.table)
				{
					this.table.reload({queryParams:{
						"bol_id":this.param.data.BOL_ID,
						"so_id":this.param.data.SO_ID
					}});
				}
				else
				{
					this.initTable();
				}
			},
			initTable:function()
			{
				var palletsLanguage = window.regional['defaultLanguage'].ADDPALLETS;
				var queryCondition = {
					"bol_id":this.param.data.BOL_ID,
					"so_id":this.param.data.SO_ID
				};
				var that = this;
	            var table = new Table({
				container : '#palletDetail',

				//url : "../data/palltes.json",
				url:config.searchBolPallets.url,
			//	height: 300,
				rownumbers : false,
				noDataMsg : "<i>"+palletsLanguage.nodatamsg+"<i>",
				singleSelection : false,
				pagination: false,
				checkbox : true,
				pagination:true,
				queryParams:queryCondition,
				method:"POST",
				pageSize:30,
				pageNumber:1,
				pageList: ["20","30","50","100","200"],
				datasName: 'DATA',
				columns:[
					//{field:'B2B_DETAIL_ID',title:'ID',width:10,hidden:true},
					{field:'ID',title:palletsLanguage.no_title,width:100},
					{field:'TO_WHS_NAME',title:palletsLanguage.shipto_title,width:120},
					{field:'WEIGHT',title:palletsLanguage.weight_title,width:100},
					{field:'REQUESTDAY',title:palletsLanguage.requestday_title,width:100},
					{field:'TYPE',title:palletsLanguage.type_title,width:100,formatter:function(data, row){
						if(data=="1")
							return "Package"
						else
							return "Pallet"
					}},
				],
				toolbar : [
				{
	                text : palletsLanguage.add_button,
	                iconCls : 'add',
	                handler : function(){
	                   that.displaySelect();
	                }
	            },
	            {
	                text : palletsLanguage.remove_button,
	                iconCls : 'remove',
	                handler : function(){
	                   var rows = table.getSelected();
	                   console.log(rows);
	                   that.deleteBolPallets(rows);
	                }
	            }
	       		]            
				});
				this.table=table;
			},
			displaySelect: function()
			{
				if(this.param.op == 0)
				{
					//编辑模式
					$("#selectedDiv").addClass('back');
			 		$("#selectDiv").addClass('front');
				}
				else
				{
					//创建模式
					$("#selectedDiv").removeClass('front');
			 		$("#selectDiv").removeClass('back');
				}
				
			 	this.param.eventAcrossView.trigger('renderSelect');
			},
			deleteBolPallets:function(rows)
			{
				var that = this;
				$.ajax({
					url: config.deleteBolPallets.url,
					type: 'POST',
					dataType: 'json',
					contentType:"application/json; charset=UTF-8",
					data: JSON.stringify({
						"bol_id":this.param.data.BOL_ID+"",
						"so_id":this.param.data.SO_ID+"",
						"pallets":rows
						}),
				})
				.done(function() {
					that.reloadTable();
				})
				.fail(function() {
					console.log("error");
				});
			}
			
	});

}
);