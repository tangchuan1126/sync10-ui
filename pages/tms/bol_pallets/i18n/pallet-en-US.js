;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
		SOINFO:{ //SO
			title:"Load",
			from_lable:"From",
			shipto_lable:"ShipTo",
			etd_lable:"ETD",
			mabd_lable:"MABD"
			
		},
		OLDPALLETS:{//已经选择的pallets
			msg:"The selected pallets or packages",
			finish_button:"Finish"
		},
		ADDPALLETS:{ //选择pallet列表
			add_button:"Add",
			remove_button:"Remove",
			no_title:"No.",
			shipto_title:"Ship To",
			weight_title:"Weight(LBS)",
			requestday_title:"Request Day",
			type_title:"Type",
			msg:"Please select palletes or packages from so",
			submit:"Submit",
			search:"Search",
			nodatamsg:"You hava not choosen any pallet or package !"
		}
	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )