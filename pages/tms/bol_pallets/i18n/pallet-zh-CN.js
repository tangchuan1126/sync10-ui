;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
		SOINFO:{ //SO
			title:"装车单",
			from_lable:"始发地",
			shipto_lable:"目的地",
			etd_lable:"预计到达时间",
			mabd_lable:"必须到达日期"
		},
		OLDPALLETS:{//已经选择的pallets
			msg:"请点击添加托盘或者包裹",
			finish_button:"完成"
		},
		ADDPALLETS:{ //选择pallet列表
			add_button:"添加",
			remove_button:"删除",
			no_title:"编号",
			shipto_title:"目的地",
			weight_title:"重量",
			requestday_title:"所需时间",
			type_title:"货物类型",
			msg:"请选择托盘或者包裹 ",
			submit:"提交",
			search:"查询",
			nodatamsg:"没有可选择的托盘或者包裹!"
		}
		

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )