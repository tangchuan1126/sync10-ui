"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		var palletLanguage = "./i18n/pallet-"+ window.navigator.languages[0];
		require(["jquery",
		         "blockui",
			//"./js/view/ViewProxy",
			"./js/view/briefInfoView",
			"./js/view/selectPalletsView",
			"./js/view/selectedPalletsView",
			"nprogress",
			"bootstrap",
			palletLanguage,
			"require_css!oso.lib/nprogress/style.css",
			"require_css!oso.lib/nprogress/nprogress.css"
			],
			function($,blockUI,BriefInfoView,SelectView,SelectedView,NProgress)
			{
				NProgress.start();
				
				function getQueryString(name) {
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var bol_id = getQueryString("bol_id");
			    var op = getQueryString("op");
			    var sourcepath = getQueryString("sourcepath"); //为了区分so 与 bol 列表刷新
			    //op  0:编辑
			    var eventAcrossView = _.extend({}, Backbone.Events);
				 $.ajax({ 
				        url: config.bolBaiscInfo.url+"?bol_id="+bol_id, //将来改为session信息
				        type: 'GET', 
				        dataType: 'json', 
				        timeout: 5000, 
				        success: function(data){ 
				        	var param = {
						 		"eventAcrossView" : eventAcrossView,
						 		"data":data,
						 		"op":op,
						 		"sourcepath":sourcepath
						 	};
				        	new BriefInfoView(param).render(data);
				 			var selectView = new SelectView(param).render();
				 			var selectedView = new SelectedView(param).render();

				 			if(op== 0)
				 			{
				 				//编辑模式，初始化已选择
				 				selectedView.initTable();
				 			}
				 			else
				 			{
				 				selectView.initTable()
				 			}
				 			eventAcrossView.on('renderSelected', function(event) {
				 				selectedView.reloadTable();
				 			});
				 			eventAcrossView.on('renderSelect', function(event) {
				 				selectView.reloadTable();
				 			});
				 			NProgress.done();
				        } ,
				        error:function(e)
				        {
				        	console.log(e);
				        	NProgress.done();
				        }
				    }); 
				    
				    NProgress.done();
			});
	});