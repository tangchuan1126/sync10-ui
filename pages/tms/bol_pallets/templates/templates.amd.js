define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['briefInfo'] = template({"1":function(depth0,helpers,partials,data) {
    return "";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "<ol class=\"breadcrumb opbar-title\">\n  <li>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SO_ID : stack1), depth0))
    + "</li>\n  <li class=\"active\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.title : stack1), depth0))
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.BOL_ID : stack1), depth0))
    + "</li>\n</ol>\n<div class=\"briefInfo-container\">\n	<div class=\"row\">\n		<div class=\"col-sm-3\">\n			<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.from_lable : stack1), depth0))
    + " :</span>\n			<span class=\"valuestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SHIPPER_WHS_NAME : stack1), depth0))
    + "</span>\n		</div>\n		<div class=\"col-sm-3\">\n			<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.shipto_lable : stack1), depth0))
    + " :</span>\n			<span class=\"valuestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONSIGNEE_WHS_NAME : stack1), depth0))
    + "</span>\n		</div>\n		<div class=\"col-sm-3\">\n			<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.etd_lable : stack1), depth0))
    + " :</span>\n			<!--<span class=\"valuestyle\">"
    + ((stack1 = (helpers.formatDate || (depth0 && depth0.formatDate) || alias3).call(depth0,(depth0 != null ? depth0.ETD : depth0),{"name":"formatDate","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</span> -->\n			<span class=\"valuestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</span>\n		</div>\n		<div class=\"col-sm-3\">\n			<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.mabd_lable : stack1), depth0))
    + " :</span>\n			<!--<span class=\"valuestyle\">"
    + ((stack1 = (helpers.formatDate || (depth0 && depth0.formatDate) || alias3).call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.MABD : stack1),{"name":"formatDate","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</span>-->\n			<span class=\"valuestyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.MABD : stack1), depth0))
    + "</span>\n		</div>\n	</div>\n</div>";
},"useData":true});
templates['selectPallets'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div  class=\"panel panel-info opbar-title\">\n   <div class=\"panel-heading opbar-title\">\n     	<span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.msg : stack1), depth0))
    + "</span>\n     	<button id=\"select\" class=\"btn btn-info opbar-buttons\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.submit : stack1), depth0))
    + "</button>\n   </div>\n   <div  class=\"panel-body\" id=\"selectPallets\">\n	   	<div id=\"queryCondition\">\n	   		<div class=\"form-inline\">\n	            <div class=\"input-group\">\n	                 <span class=\"input-group-addon form-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.requestday_title : stack1), depth0))
    + " </span>\n	                 <input class=\"form-control hours\" type=\"number\" min=\"0\" step=\"1\" id=\"requestDay_min\" placeholder=\"day\">\n	                 <span class=\"input-group-addon to\">~</span>\n	                 <input class=\"form-control hours\" type=\"number\" min=\"0\" step=\"1\" id=\"requestDay_max\" placeholder=\"day\">\n	            </div>\n	            <div class=\"input-group\">\n	                 <span class=\"input-group-addon form-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.shipto_title : stack1), depth0))
    + "</span>\n	                 <input type=\"text\" class=\"form-control\" placeholder=\"\" id=\"shipTo\" value=\"\">\n	            </div>\n	            <div class=\"input-group\">\n	               <button type=\"button\" class=\"btn btn-primary\" id=\"search\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.search : stack1), depth0))
    + "</button>\n	            </div>  \n        	</div>\n	   	</div>\n	   	<div id=\"result\">\n\n	   	</div>\n	   	<div>\n	   		\n	   	</div>\n     \n\n   </div>\n</div>\n\n";
},"useData":true});
templates['selectedPallets'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div  class=\"panel panel-default opbar-title\">\n   <div class=\"panel-heading\">\n     	<span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.msg : stack1), depth0))
    + "</span>\n      <div class=\"opbar-buttons\">\n         <button id=\"selected\" class=\"btn btn-info \" style='display:none'>Add</button>\n         <button id=\"finish\" class=\"btn btn-success \" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.finish_button : stack1), depth0))
    + "</button>\n      </div>\n   </div>\n   <div  class=\"panel-body\" id=\"palletDetail\">\n   </div>\n</div>\n";
},"useData":true});
return templates;
});