(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['briefInfo'] = template({"1":function(depth0,helpers,partials,data) {
  return "";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, buffer = "<ol class=\"breadcrumb opbar-title\">\r\n  <li>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SO_ID : stack1), depth0))
    + "</li>\r\n  <li class=\"active\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.title : stack1), depth0))
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.BOL_ID : stack1), depth0))
    + "</li>\r\n</ol>\r\n<div class=\"briefInfo-container\">\r\n	<div class=\"row\">\r\n		<div class=\"col-sm-3\">\r\n			<span class=\"namestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.from_lable : stack1), depth0))
    + " :</span>\r\n			<span class=\"valuestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.SHIPPER_WHS_NAME : stack1), depth0))
    + "</span>\r\n		</div>\r\n		<div class=\"col-sm-3\">\r\n			<span class=\"namestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.shipto_lable : stack1), depth0))
    + " :</span>\r\n			<span class=\"valuestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CONSIGNEE_WHS_NAME : stack1), depth0))
    + "</span>\r\n		</div>\r\n		<div class=\"col-sm-3\">\r\n			<span class=\"namestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.etd_lable : stack1), depth0))
    + " :</span>\r\n			<!--<span class=\"valuestyle\">";
  stack1 = ((helpers.formatDate || (depth0 && depth0.formatDate) || helperMissing).call(depth0, (depth0 != null ? depth0.ETD : depth0), {"name":"formatDate","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "</span> -->\r\n			<span class=\"valuestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ETD : stack1), depth0))
    + "</span>\r\n		</div>\r\n		<div class=\"col-sm-3\">\r\n			<span class=\"namestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.mabd_lable : stack1), depth0))
    + " :</span>\r\n			<!--<span class=\"valuestyle\">";
  stack1 = ((helpers.formatDate || (depth0 && depth0.formatDate) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.MABD : stack1), {"name":"formatDate","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</span>-->\r\n			<span class=\"valuestyle\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.MABD : stack1), depth0))
    + "</span>\r\n		</div>\r\n	</div>\r\n</div>";
},"useData":true});
templates['selectedPallets'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div  class=\"panel panel-default opbar-title\">\r\n   <div class=\"panel-heading\">\r\n     	<span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.msg : stack1), depth0))
    + "</span>\r\n      <div class=\"opbar-buttons\">\r\n         <button id=\"selected\" class=\"btn btn-info \" style='display:none'>Add</button>\r\n         <button id=\"finish\" class=\"btn btn-success \" >"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.finish_button : stack1), depth0))
    + "</button>\r\n      </div>\r\n   </div>\r\n   <div  class=\"panel-body\" id=\"palletDetail\">\r\n   </div>\r\n</div>\r\n";
},"useData":true});
templates['selectPallets'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div  class=\"panel panel-info opbar-title\">\r\n   <div class=\"panel-heading opbar-title\">\r\n     	<span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.msg : stack1), depth0))
    + "</span>\r\n     	<button id=\"select\" class=\"btn btn-info opbar-buttons\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.submit : stack1), depth0))
    + "</button>\r\n   </div>\r\n   <div  class=\"panel-body\" id=\"selectPallets\">\r\n	   	<div id=\"queryCondition\">\r\n	   		<div class=\"form-inline\">\r\n	            <div class=\"input-group\">\r\n	                 <span class=\"input-group-addon form-label\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.requestday_title : stack1), depth0))
    + " </span>\r\n	                 <input class=\"form-control hours\" type=\"number\" min=\"0\" step=\"1\" id=\"requestDay_min\" placeholder=\"day\">\r\n	                 <span class=\"input-group-addon to\">~</span>\r\n	                 <input class=\"form-control hours\" type=\"number\" min=\"0\" step=\"1\" id=\"requestDay_max\" placeholder=\"day\">\r\n	            </div>\r\n	            <div class=\"input-group\">\r\n	                 <span class=\"input-group-addon form-label\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.shipto_title : stack1), depth0))
    + "</span>\r\n	                 <input type=\"text\" class=\"form-control\" placeholder=\"\" id=\"shipTo\" value=\"\">\r\n	            </div>\r\n	            <div class=\"input-group\">\r\n	               <button type=\"button\" class=\"btn btn-primary\" id=\"search\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.search : stack1), depth0))
    + "</button>\r\n	            </div>  \r\n        	</div>\r\n	   	</div>\r\n	   	<div id=\"result\">\r\n\r\n	   	</div>\r\n	   	<div>\r\n	   		\r\n	   	</div>\r\n     \r\n\r\n   </div>\r\n</div>\r\n\r\n";
},"useData":true});
})();