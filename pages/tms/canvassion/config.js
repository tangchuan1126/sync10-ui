define( {
	// 根据 waybill_id 获取单条 waybill 的所有货物数据
	getItemsById: {
		 url: "/Sync10/_tms/tmsWaybill/getItemsById"
	},
	// 获取所有国家
	getAllCountry: {
		url: "/Sync10/_tms/tmsWaybill/getAllCountry"
	},
	cityInfo:{
		url:"/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action"
	},
	getContact: {
		url: "/Sync10/_tms/tmsWaybill/getContact"
	},
	getPalletPhysicsType: {
		url: '/Sync10/_tms/tmsWaybill/getPalletPhysicsType'	
	}
} );