;( function( w ) {
	w.regional = {};
	w.regional['en-US'] = {
		/* compond_list_config */
		itemsInfo: 'Items Info',
		shipperInfo: 'Shipper/Consignee',
		feeInfo: 'Fee Info',
		logInfo: 'Log',

		/* */
		shipmentType: 'Shippment Type'

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'en-US' ];

}( window ) )
