;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
		/* compond_list_config */
		itemsInfo: '货物信息',
		shipperInfo: '发件人信息',
		feeInfo: '收费信息',
		logInfo: '日志',

		/* */
		shipmentType: '发货类型'

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )