define( ["./jsontohtml_templates_1"], function( tmp ) {
	var canvassionI18n = window.regional['defaultLanguage'];

	return {
		"View_control": {
			"head": [{
				"title": canvassionI18n.itemsInfo,
				"field": "WAYBILL"
			}, {
				"title": canvassionI18n.shipperInfo,
				"field": "SHIPPERANDCONSIGNEE"
			}, {
				"title": canvassionI18n.feeInfo,
				"field": "FEE"
			}, {
				"title": canvassionI18n.logInfo,
				"field": "ORDERLOG"
			}],
			"idAttribute": "WAYBILL_ID",
			// 这里有什么作用？
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"footer": [
				[
					"events.Edit", "Edit",
					"events.Print","Print",
					"events.DetailsInfo","DetailsInfo",
				]
			],
			"templates": tmp
		}
	}

});