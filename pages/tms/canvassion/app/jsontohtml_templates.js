define( ["JSONTOHTML",
		 "../view/searchAdvanced"
		 ], function(
		 	JSONTOHTML,
		 	searchSample,
		 	searchAdvanced ) {

	var canvassionI18n = window.regional['zh-CN'];
	// 第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function( obj, index ) {
						var thdata = obj.children[0].children;
						// 用 thdata.children 去渲染 _table.th
						return ( JSONTOHTML.transform( thdata.children, _table.th ) );
					}
				}]
			},
			{
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			// CompoundList 中 View 对 pagebox 使用 Paging 插件
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}],
		th: {
			tag: "th",
			html: "${title}",
			// 设置 th 的样式
			style: function( obj,index ) {
				if ( index == 0 ) {
					return 'width:25%';
				} else if ( index == 1 ) {
					return 'width:18%';
				} else if ( index == 2 ) {
					return 'width:25%';
				} else if ( index == 3 ) {
					return 'width:18%';
				} else {
					return '';
				}
			}
		}
	};

	// 表身翻身时也独立用到，可以共享到其他有用到和这个列表用法一样的地方
	var _tbody = {
		int: function( jsons, View_control ) {
			// 初始化全局变量
			_tbody.idAttribute = View_control.idAttribute;
			_tbody.footer = View_control.footer;
			_tbody.headallobj = View_control.head;
			var trfooterdata = {
				"colspan": _tbody.headallobj.length,
				"children": _tbody.footer,
				"trid": ""
			};

			var pageSize = jsons.PAGECTRL.pageSize;

			var that = this;
			// 需要用到的全局数据放到 globalCollection 中
			// var setTypeCollection = eval( "({" + "TYPECOLLECTION" + ":jsons.TYPECOLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setTypeCollection );
			// var setVolumeUnitCollection = eval( "({" + "VOLUMEUNITCOLLECTION" + ":jsons.VOLUMECOLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setVolumeUnitCollection );
			// var setWeightUnitCollection = eval( "({" + "WEIGHTUNITCOLLECTION" + ":jsons.WEIGHTCOLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setWeightUnitCollection );
			// var setSpecialCollection = eval( "({" + "SPECIALTYPECOLLECTION" + ":jsons.SPECIALTYPECOLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setSpecialCollection );
			// var setWhscollection = eval( "({" + "WHSCOLLECTION" + ":jsons.WHSCOLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setWhscollection );
			// var setFeetypeCollection = eval( "({" + "PAYMENT_METHODCOLLECTION" + ":jsons.FEE_TYPE_COLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setFeetypeCollection );
			// var setCreaterCollection = eval( "({" + "CREATERCOLLECTION" + ":jsons.CREATERCOLLECTION})" );
			// that.globalCollection = $.extend( that.globalCollection, setCreaterCollection );
			// var setTrackstatus = eval( "({" + "TRACKSTATUS" + ":jsons.TRACKSTATUS})" );
			// that.globalCollection = $.extend( that.globalCollection, setTrackstatus );

			var __tr = _tbody.TotrJson( jsons.DATA, pageSize );
			var __trhtml = '';
			for (var trkey in __tr) {
				var _tritmes = __tr[trkey];
				__trhtml += ( JSONTOHTML.transform( _tritmes, _tbody.tr ) );
				trfooterdata["trid"] = _tritmes.trid;
				__trhtml += ( JSONTOHTML.transform( trfooterdata, _tbody.trfooter ) );
			}

			return __trhtml;
		},
		// 判断对象是否为 null
		isEmpty: function( obj ) {
			for ( var name in obj ) {
				return false;
			}

			return true;
		},
		// 组织数据
		organizationalData: function( key, value ) {
				var newData = new Object();
				newData[key] = value;
				
				return newData;
		},
		TotrJson: function( DATA, pageSize ) {
			var that = this;
			var __tr=[];
			var headallobj = _tbody.headallobj;
			// 如果 headallobj 不为 {}
			if ( !that.isEmpty( headallobj ) ) {
				for ( var tri = 0; tri < DATA.length; tri++ ) {
					var rowData = DATA[tri];
					var __td = [];
					for ( var headkey in headallobj ) {
						// 获取列
						var column = headallobj[headkey];
						// 获取列名
						var columnFiled = column.field;
						// 组织数据 {列: 行数据}
						var columnData = that.organizationalData( columnFiled, rowData );
						__td.push( columnData );
					}
					__tr.push( {
						"rowId": rowData[_tbody.idAttribute],
						"children": __td
					} );

					if ( tri >= pageSize ) {
						break;
					}
				}
			}

			return __tr;
		},
		keyname: function( obj ) {
			// console.log(obj);
			var key = "";
			for (var rowkey in obj) {

				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id": function(obj) {
				return obj.trid;
			},
			"data-itmeid": function( obj ) {
				var waybill_id;
				if ( obj.children ) {
					waybill_id = obj.children[0].WAYBILL_ID;
				} else {
					waybill_id = obj
				}

				return "WAYBILL_ID|" + waybill_id;
			},
			"class": "tr_itme_style",
			"html": function( obj, index ) {
				return ( JSONTOHTML.transform( obj.children, _tbody.td ) );
			}
		},
		trfooter: {
			tag: "tr",
			class: "TFOOTbutton",
			html: function(obj, index) {
				var trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="WAYBILL_ID|' + obj.trid + '" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) {
					var itmea = obj.children[akey];
					ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + itmea[1] + '</a>'
						+'<a href="javascript:void(0)" data-eventname="' + itmea[2] + '"  class="buttons">' + itmea[3] + '</a>';
				}
				trhtmls += ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
		td: {
			tag: "td",
			class: function(obj, index) {

				var classname = "";
				var keynames = _tbody.keyname(obj);
				classname = "td_" + keynames.toLowerCase();
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "top";
				if(!!obj.SHIPPER ||!!obj.CONSIGNEE||!!obj.FEE){
					valignsing="middle";
				}
				return valignsing;
			},
			html: function( obj, index ) {

				var retuhtml = "没找到模版";

				var keynames = _tbody.keyname(obj);
				if (keynames != "" && typeof keynames != "undefined") {
					if (typeof eval("_tbody." + keynames) != "undefined" && eval("_tbody." + keynames) != "") {
						retuhtml = (JSONTOHTML.transform(obj, eval("_tbody." + keynames)));
					}
				}
				return retuhtml;
			}
		},
		WAYBILL: {
			tag: "fieldset",
			class: "fsStyle",
			children: [{
				tag: "legend",
				html: function(obj, index) {
					var legendhtml = "";
					legendhtml = '<span class="waybill_id">' + obj.WAYBILL.waybill_id + '</span>';
					return legendhtml;
				}
			},
			{
				tag: "div",
				class: "Tractor_ulitme",
				children: [{
					tag: "ul",
					html: function( obj, index ) {
						console.info( obj );
						var that = this;
						var details = obj.WAYBILL.WAYBILL.DETAILS;
						var items = details.ITEMS;
						var desc = obj.WAYBILL.WAYBILL.CONTENT;
						var quantity = obj.WAYBILL.WAYBILL.QUANTITY;
						var type_name = "";
						var weight = "";
						var weight_unit = "";
						var weight_unit_text = "";
						var volume = "";
						var volume_unit = "";
						var volume_unit_text;
						var special = [];
						var special_text = "";
						var createtime = obj.WAYBILL.CREATE_TIME;
						var MABD = obj.WAYBILL.MABD;
						var sendLabel = "";
						var volumeLabel = "";
						var weightLabel = "";
						var descLabel = "";
						var quantityLabel = "";
						var specialLabel = "";
						var createTimeLabel = "";
						var MABDLable = "";
						// var typecollection = _tbody.globalCollection.TYPECOLLECTION;
						// for (var i = 0; i < typecollection.length; i++) {
						// 	if( typecollection[i].ID == details.SEND_TYPE ) {
						// 		type_name = typecollection[i].TYPE_NAME;
						// 	}
						// };

						if ( items && items.length ) {
							weight_unit = items[0].WEIGHT_UNIT;

							for ( var i = 0; i < items.length; i++ ) {
								if(i==0) {
									weight = items[0].WEIGHT;
								} else {
									if ( items[i].WEIGHT_UNIT == weight_unit ) {

										//判断重量单位是否相同
										weight = Number( weight ) + Number( items[i].WEIGHT );
									};									
								}


								if ( quantity == 1 ) {
									volume = items[i].LENGTH + "*" + items[i].WIDTH + "*" + items[i].HEIGHT;
									volume_unit = items[i].VOLUME_UNIT;
								};

				
								if ( items[i].DOUBLE_FLAG != "" && items[i].DOUBLE_FLAG != undefined) {
									var double_flag_arr = items[i].DOUBLE_FLAG.split( ',' );
									for ( var x = 0; x < double_flag_arr.length; x++ ) {
									
										if($.inArray(double_flag_arr[x], special)==-1) {
											special.push( double_flag_arr[x] );
										}
									}
									
								};



							
							};
						};

						

						// var volumeUnitCollection = _tbody.globalCollection.VOLUMEUNITCOLLECTION;
						// for ( var i = 0; i < volumeUnitCollection.length; i++ ) {
						// 	// 获取体积单位
						// 	if ( volumeUnitCollection[i].UNIT_CODE == volume_unit ) {
						// 		volume_unit_text = volumeUnitCollection[i].UNIT_NAME;

						// 	}
						// };

						// var weightUnitCollection = _tbody.globalCollection.WEIGHTUNITCOLLECTION;

						// for ( var i = 0; i < weightUnitCollection.length; i++ ) {
						// 	// 获取重量单位
						// 	if ( weightUnitCollection[i].UNIT_CODE == weight_unit ) {
						// 		weight_unit_text = weightUnitCollection[i].UNIT_NAME;
						// 	}
						// };									

						// var specialcollection = _tbody.globalCollection.SPECIALTYPECOLLECTION;
						// for ( var i = 0; i < specialcollection.length; i++ ) {

						// 	if ( special.length > 0 ) {
								
						// 		for ( var j = 0; j < special.length; j++ ) {
						// 			if ( specialcollection[i].SPECIALTYPE == special[j] ) {
						// 				special_text += specialcollection[i].SPECIALTYPEVALUE + ', ';
						// 				// console.log(special_text);
						// 			};
						// 		};
						// 	};							
						// };
					


						special_text = special_text.substring( 0, special_text.length - 2 );

						// 发货类型
						sendLabel = '<li><span class="namestyle waybillInfo">' + "Shipment Type" + ':</span><span class="valstyle">' + type_name + '</span></li>';
						// 重量
						weightLabel = '<li><span class="namestyle waybillInfo">' + "Weight" + ':</span><span class="valstyle">' + weight + ' ' + weight_unit_text + '<span style="margin-top: 5px;"></span>' + '</span></li>';
						// 如果订单包裹数量大于 1，不展示体积，在详细信息中展示
						if ( quantity == 1 && items.length == 1 ) {
							// 获取体积
							volumeLabel += '<li><span class="namestyle waybillInfo">' + "Volume" + ':</span><span class="valstyle">' + volume + ' ' + volume_unit_text + '<span style="margin-top: 5px;"><sup>2</sup></span></span></li>';
						};

						// 描述
						descLabel = '<li><span class="namestyle waybillInfo">' + "Description" + ':</span><span class="valstyle">' + desc + '</span></li>';
					  	// 获取详细信息
					  	var txt=obj.WAYBILL.QUANTITY;

					  	var btnDetails = "";
					  	if(txt!="1" && typeof txt!="undefined"){
					  		//return "<em>"+obj.ORDER.DETAILS+"</em>";
					  		// btnDetails = '<a style="margin-left:40px;" href="javascript:void(0)" data-eventname="events.DetailsInfo" class="btn btn-default">Details <i class="fa fa-angle-double-down"></i> <i style="display:none;"class="fa fa-angle-double-right"></i></a>';
					    	var items = obj.WAYBILL.DETAILS.ITEMS;
					    	for (var i = 0; i < items.length; i++) {
					    		btnDetails += "<li class='itemDetails'>"
					    					+"<fieldset>"
					    					+"<legend>"
					    					+items[i].WAYBILL_ITEM_ID
					    					+"</legend>"
					    					+"<ul>"
					    					+"<li><span class='namestyle'>CONTENT:</span>"
					    					+"<span class='valstyle'>"+items[i].CONTENT+"</span></li>"
					    					+"<li><span class='namestyle'>VOLUME:</span>"
					    					+"<span class='valstyle'>"+items[i].LENGTH +" x "+items[i].LENGTH+" x "+items[i].HEIGHT+"</span></li>"
					    					+"<li><span class='namestyle'>WEIGHT:</span>"
					    					+"<span class='valstyle'>"+items[i].WEIGHT+"</span></li>"
					    					+"</ul>"
					    					+"</fieldset>"
					    					+'</li>';
					    	};
					    }	
	    						
						// 数量
						quantityLabel = '<li class="detailsShow"><span class="namestyle waybillInfo">' + "Quantity" + ':</span><span class="valstyle">' + quantity + '</span>'+btnDetails+'</li>';
						// 特殊配送
						if( special_text != "" ) {
							specialLabel = '<li><span class="namestyle" style="color:#E00">' + "Properties" + ':</span><span class="valstyle" style="color:#E00">' + special_text + '</span></li>'
						}
						// 创建时间
						createTimeLabel = '<li><span class="namestyle waybillInfo">' + "CREATE TIME" + ':</span><span class="valstyle">' + createtime + '</span></li>';
						// MABD
						MABDLable = '<li><span class="namestyle waybillInfo">' + "MABD" + ':</span><span class="valstyle">' + MABD + '</span></li>';
						return  sendLabel
								+ weightLabel
								+ volumeLabel
								+ descLabel
								+ quantityLabel
								+ specialLabel
								+ createTimeLabel
								+ MABDLable;
					}
				}]
			},{
				  tag:"span",
				  class:"fieldset_tfoot",
				  html:function(obj,index){

				    return '';
				  }
			}]
		},
		fieldset1divulli: {
			tag: "li",
			html: function(obj, index) {

				if(obj.LABLE == '特殊配送')
					return '<span class="namestyle" style="color:#E00">' + obj.LABLE + ':</span><span class="valstyle" style="color:#E00">' + obj.VALUE + '</span>';
				else
					return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		SHIPPER: {
			tag: "div",
			class: "fsStyle_shipper",
			children: [{
				tag: "ul",
				class:"shipper_ul",
				html: function( obj, index ) {
					// 发货仓库
					var shipper_whs_id = "";
					var shipper_whs_name = "";
					var shipper_whsLabel = "";
					// 联系人
					var shipper_name = "";
					var shipper_nameLabel = "";
					// 联系电话
					var shipper_tel = "";
					var shipper_telLabel= "";
					// 地址
					var shipper_address = "";
					var shipper_addressLabel = "";
					if ( obj ) {
						shipper_whs_id = obj.SHIPPER.SHIPPER_WHS_ID;
						shipper_name = obj.SHIPPER.SHIPPER_NAME;
						shipper_address = obj.SHIPPER.SHIPPER_ADDRESS;
						shipper_tel = obj.SHIPPER.SHIPPER_TEL;
					};

					// var whscollection = _tbody.globalCollection.WHSCOLLECTION;
					// for (var i = 0; i < whscollection.length; i++) {
					// 	if ( shipper_whs_id == whscollection[i].ID ) {
					// 		shipper_whs_name = whscollection[i].NAME;
					// 	};
					// };
					shipper_whsLabel = '<li><span class="namestyle">' + "Shipper Storage" + ': </span><span class="valstyle">' + shipper_whs_name + '</span></li>';
					shipper_addressLabel = '<li><span class="namestyle">' + "Shipper Contact" + ': </span><span class="valstyle">' + shipper_name + '</span></li>';
					shipper_nameLabel = '<li><span class="namestyle">' + "Shipper Phone" + ': </span><span class="valstyle">' + shipper_tel + '</span></li>';
					shipper_telLabel = '<li><span class="namestyle">' + "Shipper Address" + ': </span><span class="valstyle">' + shipper_address + '</span></li>';

					return shipper_whsLabel + shipper_nameLabel + shipper_telLabel + shipper_addressLabel;
				}
			}]
		},
		fieldset2divulli: {
			tag: "li",
			html: function(obj, index) {
				return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		CONSIGNEE: {
			tag: "div",
			class: "fsStyle_shipper",
			children: [{
				tag: "ul",
				class:"shipper_ul",
				html: function( obj, index ) {
					// console.log( obj );
					// 收货仓库
					var consignee_whs_id = "";
					var consignee_whs_name = "";
					var consignee_whsLabel = "";
					// 收货地区
					var consignee_area_code = "";
					var consignee_area = "";
					// 联系人
					var consignee_name = "";
					var consignee_nameLabel = "";
					// 联系电话
					var consignee_tel = "";
					var consignee_telLabel= "";
					// 地址
					var consignee_address = "";
					var consignee_addressLabel = "";
					// 邮编
					var consignee_post_code = "";
					if ( obj ) {
						consignee_whs_id = obj.CONSIGNEE.CONSIGNEE_WHS_ID;
						consignee_area_code = obj.CONSIGNEE.CONSIGNEE_AREA_CODE;
						consignee_name = obj.CONSIGNEE.CONSIGNEE_NAME;
						consignee_address = obj.CONSIGNEE.CONSIGNEE_ADDRESS;
						consignee_tel = obj.CONSIGNEE.CONSIGNEE_TEL;
						consignee_areaname = obj.CONSIGNEE.AREANAME;
						consignee_post_code = obj.CONSIGNEE.CONSIGNEE_POST_CODE;
					};

					// var whscollection = _tbody.globalCollection.WHSCOLLECTION;
					// for (var i = 0; i < whscollection.length; i++) {
					// 	if ( consignee_whs_id == whscollection[i].ID ) {
					// 		consignee_whs_name = whscollection[i].NAME;
					// 	};
					// };

					consignee_whsLabel = '<li><span class="namestyle">' + "Consignee Storage" + ':</span><span class="valstyle">' + consignee_whs_name + '</span></li>';
					consignee_areaLabel = '<li><span class="namestyle">' + "Consignee Area" + ':</span><span class="valstyle">' + consignee_areaname + '</span></li>';
					consignee_addressLabel = '<li><span class="namestyle">' + "Consignee Contact" + ':</span><span class="valstyle">' + consignee_name + '</span></li>';
					consignee_nameLabel = '<li><span class="namestyle">' + "Consignee Phone" + ':</span><span class="valstyle">' + consignee_tel + '</span></li>';
					consignee_telLabel = '<li><span class="namestyle">' + "Consignee Address" + ':</span><span class="valstyle">' + consignee_address + '</span></li>';
					consignee_post_codeLabel = '<li><span class="namestyle">' + "Consignee Zip" + ':</span><span class="valstyle">' + consignee_post_code + '</span></li>';

					return consignee_whsLabel + consignee_areaLabel + consignee_nameLabel + consignee_telLabel + consignee_addressLabel + consignee_post_codeLabel;
				}
			}]
		},
		fieldset3divulli: {
			tag: "li",
			html: function(obj, index) {
				return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		FEE: {
			tag: "div",
			class: "fsStyle_fee",
			children: [{
				tag: "ul",
				class:"fee_ul",
				html: function( obj, index ) {
					// 付款方式
					var fee_type_id = "";
					var fee_type = "";
					var fee_typeLabel = "";
					// 声明价值
					var declared_value = "";
					var declared_valueLabel = "";
					// 保价费用
					var fee_insured = "";
					var fee_insuredLabel = "";
					// 运费
					var fee_tracking = "";
					var fee_trackingLabel = "";
					// 合计
					var fee_total = "";
					var fee_totalLabel = "";
					if ( obj ) {
						fee_type_id = obj.FEE.FEE_TYPE;
						declared_value = obj.FEE.DECLARED_VALUE;
						fee_insured = obj.FEE.FEE_INSURED;
						fee_waybill = obj.FEE.FEE_WAYBILL;
						fee_total = obj.FEE.FEE_TOTAL;
					};


					// var payment_methodCollection = _tbody.globalCollection.PAYMENT_METHODCOLLECTION;
					// for (var i = 0; i < payment_methodCollection.length; i++) {
					// 	if ( fee_type_id == payment_methodCollection[i].ID ) {
					// 		fee_type = payment_methodCollection[i].PAYMENT_NAME;
					// 	};
					// };

					fee_typeLabel = '<li><span class="namestyle">' + "Freight Terms" + ':</span><span class="valstyle">' + fee_type + '</span></li>';
					declared_valueLabel = '<li><span class="namestyle">' + "Declare Value" + ':</span><span class="valstyle">' + declared_value + '</span></li>';
					fee_insuredLabel = '<li><span class="namestyle">' + "Val.lns" + ':</span><span class="valstyle">' + fee_insured + '</span></li>';
					fee_trackingLabel = '<li><span class="namestyle">' + "Charge" + ':</span><span class="valstyle">' + fee_waybill + '</span></li>';
					fee_totalLabel = '<li><span class="namestyle">' + "Total Charge" + ':</span><span class="valstyle">' + fee_total + '</span></li>';

					return fee_typeLabel + declared_valueLabel + fee_insuredLabel + fee_trackingLabel + fee_totalLabel;					
				}
			}]
		},
		fieldset4divulli: {
			tag: "li",
			html: function(obj, index) {
				return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		ORDERLOG: {
			tag: "div",
			class: "Tractor_ulitme",
			children: [{
				tag: "ul",
				html: function( obj, index ) {
					var logInfos = "";

					var ORDERLOGS = obj.ORDERLOG;
					for ( var i = 0; i < ORDERLOGS.length; i++ ) {
						if ( ORDERLOGS[i].LOGTYPE == 1 ) {
							logInfos += '<li><span class="namestyle" style="color:#E00">' + ORDERLOGS[i].LOGINFO + ':</span><span class="valstyle" style="color:#E00">' + ORDERLOGS[i].LOGTIME + '</span></li>';
						} else {
							logInfos += '<li><span class="namestyle">' + ORDERLOGS[i].LOGINFO + ':</span><span class="valstyle">' + ORDERLOGS[i].LOGTIME + '</span></li>';
						}
						

					};
					// console.log( logInfos );
					return logInfos;						
				}
			}]
		},
		fieldset5divulli: {
			tag: "li",
			html: function(obj, index) {
				if(index == 0)
					return '<span class="namestyle" style="color:#E00">' + obj.LABLE + ':</span><span class="valstyle" style="color:#E00">' + obj.VALUE + '</span>';
				else
					return '<span class="namestyle">' + obj.LABLE + ':</span><span class="valstyle">' + obj.VALUE + '</span>';
			}
		},
		Options: {
			tag: "ul",
			html: function(obj, index) {
				var ullihtml = "";
				if (typeof obj == "object") {
					for (var key in obj.Options) {
						var btusring = obj.Options[key];
						ullihtml += '<li><button class="' + btusring[0] + '" data-eventname="' + btusring[1] + '">' + btusring[0] + '</button></li>';
					}
				}
				return ullihtml;
			}
		},
		updatatr: function(jsondata, trid) {
			var tritmeobj = $(trid);
			var tritmedata = [];
			tritmedata.push(jsondata);
			var uptrdata = _tbody.TotrJson(tritmedata, 1);
			// console.log(uptrdata[0].children);
			tritmeobj.empty().html(JSONTOHTML.transform(uptrdata[0].children, _tbody.td));
		}
	}
	return {
		tbody: _tbody,
		table: _table
	}
});