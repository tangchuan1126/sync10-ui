"use strict";
require(['/Sync10-ui/requirejs_config.js'], function() {
	var cssfileall = ["require_css!bootstrap-css/bootstrap.min", 
	                  "require_css!Font-Awesome", 
	                  "require_css!../css/main.css", 
	                  "require_css!../css/easysearch.css", 
	                  "require_css!oso.lib/bootstrap/css/VisiStyle.css"];
	
	require( cssfileall, function() {
		require( ['jquery', 
				  'bootstrap', 
				  'metisMenu', 
				  'art_Dialog/dialog-plus', 
				  'backbone', 
				  'handlebars', 
		          '../view/shipper', 
		          '../view/consignee', 
		          '../view/packages', 
		          '../view/fee',
				  "domready!"
		          ], function( $, bootstrap, metisMenu, Dialog, backbone, Handlebars,
						shipper, consignee, packages, fee) {
			// 关闭 slide
	   		var _docuel = window.parent.document;
     		var slide_contentbox = _docuel.querySelector( '.slide-wrap' );
     		var slide_modal=_docuel.querySelector( '.slide-modal' );

			var waybill_id = "";
			var shipperView;
			var consigneeView;
			var itemView;
			var feeView;
			if( window.parent.HTMLElement.prototype.getInfo ) {

				var info = $( _docuel ).find( "#dataInfo" )[0].getInfo();
				var shipperInfo = info.SHIPPER;
				var consigneeInfo = info.CONSIGNEE;
				var orderInfo = info.WAYBILLITMES;				
				var feeInfo = info.FEE;	
				waybill_id = info.WAYBILL.waybill_id;
				shipperView=new shipper( {
					el: "#shipper"
				} );
				shipperView.render(shipperInfo);
				consigneeView=new consignee( {
					el: "#consignee"
				} );
				consigneeView.render(consigneeInfo);
				itemView=new packages( {
					el: "#packages"
				} );
				console.log(orderInfo);
				itemView.render(orderInfo);
				feeView=new fee( {
					el: "#fee"
				} );
				feeView.render(feeInfo);
			} else {

				shipperView=new shipper( {
					el: "#shipper"
				} );
				shipperView.render();
				consigneeView=new consignee( {
					el: "#consignee"
				} )


				

				consigneeView.render();


				itemView=new packages( {
					el: "#packages"
				} );
				itemView.render();
				feeView=new fee( {
					el: "#fee"
				} );
				feeView.render();
				// 收件人仓库默认国家为当前登陆人所在国家
				if(!!window.parent.Parentpush.globalData.consignee_country_id) {
					var consignee_country_id = window.parent.Parentpush.globalData.consignee_country_id;
					// $("#consignee_country").val(consignee_country_id);
					// $("#consignee_country").trigger("change");
				}				
			}

			var oldTab = "shipperView";
			$("#myTab a").click(function(obj){
				eval(oldTab+".validate()");
				oldTab=$(obj.currentTarget).attr("href").substring(1)+"View";
			});

			$( '#next_fee' ).on( 'click', function(e) {
				if(!shipperView.validate()||!consigneeView.validate()||!itemView.validate()||!feeView.validate()){
					var validateTextArr = [];
					if(!shipperView.validate()){
						validateTextArr.push("发件人");
					}					
					if(!consigneeView.validate()){
						validateTextArr.push("收件人");
					}					
					if(!itemView.validate()){
						validateTextArr.push("货物信息");
					}					
					if(!feeView.validate()){
						validateTextArr.push("费用信息");
					}

					// var validateText = validateTextArr.join(",");
					// $("#lastOper").append('<span id="myAlert" class="alert alert-danger">'
   		// 					+'<a href="#" class="close" data-dismiss="alert">'
     //  					    +'&times;'
   		// 					+'</a>'
   		// 					+validateText
   		// 					+'输入不合法</span>');
					// setTimeout(function(){
					// 	$("#myAlert").alert('close');
					// },2000);
					// return false;
				};
				var sent_type = $( "#consignee_posttype" ).val();
				var content = $( "#shipper_remark" ).val();
				var quantity = $( "#itemsNumber" ).text();
				var shipper_name = $( "#shipper_name" ).val();
				var shipper_tel = $( "#shipper_tel" ).val();
				var shipper_address = $( "#shipper_address" ).val();
				var consignee_whs_id=$("#consignee_whs_id").data("val");
				var consignee_country = $( "#consignee_country" ).val();
				var consignee_areacode = $( "#consignee_area" ).val();
				var consignee_name = $( "#consignee_name" ).val();
				var consignee_tel = $( "#consignee_tel" ).val();
				var consignee_address = $( "#consignee_address" ).val();
				var consignee_postcode = $( "#consignee_postcode" ).val();
				
				// 运单信息
				var orderInfo = {
					"SEND_TYPE": sent_type,
					"CONTENT": content,
					"QUANTITY": quantity,
					"SHIPPER_NAME": shipper_name,
					"SHIPPER_TEL": shipper_tel,
					"SHIPPER_ADDRESS": shipper_address,
					"CONSIGNEE_WHS_ID": consignee_whs_id,
					"CONSIGNEE_COUNTRY": consignee_country,
					"CONSIGNEE_AREACODE": consignee_areacode,
					"CONSIGNEE_NAME": consignee_name,
					"CONSIGNEE_TEL": consignee_tel,
					"CONSIGNEE_ADDRESS": consignee_address,
					"CONSIGNEE_POST_CODE": consignee_postcode
				};

				// 当寄件类型为指定日期时，添加和更新添加 mabd
				if ( $( "#consignee_posttype" ).val() == 4 ) {
					var mabd =  $( "#consignee_mabd" ).val();
					$.extend( waybillInfo, { "MABD": mabd } );
				}
				
				// 货物信息
				var itemsArr = [];
				var items = {};
				
				var weight = $( "#item_actual_weight" ).val();
				var oper = $( "#next_fee span#waybill_oper").text();
				if( $( "#quantityType>#oneItem" ).hasClass( "active" ) ) {
					items = $(".items").not( "#itemList>.items" );
					var content = $( items ).find( "#item_content" ).val();
					var weightUnit = $( items ).find( "[name=item_wunit_all]" ).val();
					var length = $( items ).find( "[name=item_length_all]" ).val();
					var width = $( items ).find( "[name=item_width_all]" ).val();
					var height = $( items ).find( "[name=item_height_all]" ).val();

					var volumeUnit = $( items ).find( "[name=item_vunit_all]" ).val();
					var double_flag_ele = $( "[name=item_property]:checked" );
					var double_flag_arr = [];
					var double_flag = "";
					for ( var i = 0; i < double_flag_ele.length; i++ ) {
						double_flag_arr.push( $( double_flag_ele[i] ).val() );
					}


							
					double_flag = double_flag_arr.join( "," );	
					var elePackageType = $( ".special_property" ).not( "#itemList>.items>.special_property" ).find( ".selPackageType:checked" );
					var package_type = elePackageType.val();

					var waybill_item_id = $(".items").data("itemid");

					item = $.extend( item, {
							"CONTENT": content
						}, {
							"WEIGHT": weight
						}, {
							"WEIGHT_UNIT": weightUnit
						}, {
							"LENGTH": length
						}, {
							"WIDTH": width
						}, {
							"HEIGHT": height
						}, {
							"VOLUME_UNIT": volumeUnit
						}, {
							"DOUBLE_FLAG": double_flag
						}, {
							"PACKAGING": package_type
						}, {
							"WAYBILL_ITEM_ID":waybill_item_id
						}
					)
					// debugger;
					// 包装信息为托盘时添加 palletId
					if ( package_type == 2 ) {
						var pallet_id = $( elePackageType ).parent().parent().parent().next().find( "[name=item_plate_all]" ).val();
						$.extend( item, { "PALLET_ID": pallet_id } );
					}
					
					if ( oper == '更新' ) {
						// console.log( $( ".item" ) );
						var waybill_item_id = $( items ).data( "itemid" );
						$.extend( item, { "WAYBILL_ITEM_ID": waybill_item_id } );
					}							

					itemsArr.push( item );
				} else {
					items = $( "#itemList>.items" );
					var itemsLength = items.length;
					
					if ( itemsLength && itemsLength > 0 ) {
						for ( var i = 0; i < itemsLength; i++ ) {
							var item = {};
							var content = $( items[i] ).find( "#item_content" ).val();
							var weightUnit = $( items[i] ).find( "[name=item_wunit_all]" ).val();
							var length = $( items[i] ).find( "[name=item_length_all]" ).val();
							var width = $( items[i] ).find( "[name=item_width_all]" ).val();
							var height = $( items[i] ).find( "[name=item_height_all]" ).val();

							var volumeUnit = $( items[i] ).find( "[name=item_vunit_all]" ).val();
							var double_flag_ele = $( items[i] ).find( "[name=item_property]:checked" );
							var double_flag_arr = [];
							var double_flag = "";
							for ( var j = 0; j < double_flag_ele.length; j++ ) {
								double_flag_arr.push( $( double_flag_ele[j] ).val() );
							}


							
							double_flag = double_flag_arr.join( "," );
							if (double_flag_ele.length==0) {
								double_flag = 0;
							}
							var package_type_count = $( items[i] ).find( ".package_type" ).data( "count" );
							var elePackageType = $( items[i] ).find( ".selPackageType:checked" );
							var package_type = elePackageType.val();
							var pallet_id = $( items[i] ).find( "#item_plate_all" ).val();
							
							item = $.extend( item, {
									"CONTENT": content
								}, {
									"WEIGHT": weight
								}, {
									"WEIGHT_UNIT": weightUnit
								}, {
									"LENGTH": length
								}, {
									"WIDTH": width
								}, {
									"HEIGHT": height
								}, {
									"VOLUME_UNIT": volumeUnit
								}, {
									"DOUBLE_FLAG": double_flag
								}, {
									"PACKAGING": package_type
								}
							);
							if ( oper == '更新' ) {
								// console.log( $( ".item" ) );

								var waybill_item_id = $( items[i] ).data("itemid");
								// debugger;
								$.extend( item, { "WAYBILL_ITEM_ID": waybill_item_id } );
							}
							

							// 包装信息为托盘时添加 palletId
							// if ( package_type == 2 ) {
							// 	var pallet_id = $( elePackageType ).parent().parent().parent().next().find( "[name=packages_plate_all]" ).val();
							// 	$.extend( item, { "PALLET_ID": pallet_id } );
							// }							
							
							itemsArr.push( item );
						}
					}					
				}
				

				
				var fee_type = $( "[name=send_package_type]:checked" ).val();

				var fee_account = $( "#fee_account" ).val();
				var fee_declared_value = $( "#fee_declared" ).val();
				if(fee_declared_value=="") {
					fee_declared_value="0.00";
				}				
				var fee_insured = $( "#fee_insured" ).val();
				if(fee_insured=="") {
					fee_insured="0.00";
				}
				var fee_waybill = $( "#fee_waybill" ).val();
				var fee_wrap = $( "#fee_wrap" ).val();
				if(fee_wrap=="") {
					fee_wrap="0.00";
				}
				var fee_other = $( "#fee_other" ).val();
				if(fee_other=="") {
					fee_other="0.00";
				}
				var fee_total = Number( fee_insured)  
								+ Number( fee_waybill )
								+ Number( fee_wrap )
								+ Number( fee_other );
	
				$( "#fee_total" ).val( fee_total );
				
				// 付费信息
				var feeInfo = {
					"FEE_TYPE": fee_type,
					"MONTHLY_PAYMENT": fee_account,
					"DECLARED_VALUE": fee_declared_value,
					"FEE_INSURED": fee_insured,
					"FEE_WAYBILL": fee_waybill,
					"FEE_PACKAGING": fee_wrap,
					"FEE_OTHER": fee_other,
					"FEE_TOTAL": fee_total
				};
				console.log( oper );
				if ( oper == '更新' ) {
					// console.log( $( ".item" ) );
					var waybill_fee_id = $( "#feeId" ).data("feeid");
					$.extend( feeInfo, { "WAYBILL_FEE_ID": waybill_fee_id } );
				}	
				
				var dataInfo = {};
				
				var oper = $( "#next_fee span#waybill_oper").text();
				if ( oper == 'Finish' ) {
					dataInfo = {
						WAYBILLINFO: waybillInfo,
						ITEMS: itemsArr,
						FEEINFO: feeInfo
					}
					

					dataInfo = JSON.stringify( dataInfo ); 
					
					$.ajax( {
						url: '/Sync10/_tms/tmsWaybill/addWaybill',
						data: 'DATA=' + dataInfo,
						dataType: 'json',
						type: 'post',
						success:function( data ){
							window.parent.Parentpush.compandList.render();
						 	slide_contentbox.style.display = "none";
							slide_modal.style.display = "none";
						},
						error:function(){
							
						}
					} )
					
				} else if ( oper == '更新' ) {
					console.info( 'waybill_id:', waybill_id );
					orderInfo = $.extend( orderInfo, { "WAYBILL_ID": waybill_id } );
					dataInfo = {
						WAYBILLINFO: orderInfo,
						ITEMS: itemsArr,
						FEEINFO: feeInfo
					}
					console.log( dataInfo );
					dataInfo = JSON.stringify( dataInfo ); 		
					$.ajax( {
						url: '/Sync10/_tms/tmsWaybill/editWaybill',
						data: 'DATA=' + dataInfo,
						dataType: 'json',
						type: 'post',
						success:function( data ){
						 	slide_contentbox.style.display = "none";
							slide_modal.style.display = "none";
							window.parent.Parentpush.compandList.render();
						},
						error:function(){
							
						}
					} )
				} else {
					alert( 'System error!' );
				}
			});	

			//测试用
			$(document).ready(function(){
				$("#test").click(function(){
					$("#shipper_name").val("刘一")
					$("#shipper_tel").val("18701151870")
					$("#consignee_address").val("北京朝阳区东三环中路")
					$("#shipper_remark").val("addWaybillWithOne 测试")
					$("#consignee_name").val("陈二")
					$("#consignee_tel").val("13811291222")
					$("#shipper_address").val("广州市天河区珠江新城珠江西路 10 号")
					$("#consignee_postcode").val("510623")
					$("#item_content").val("茶叶")
					$("#fee_account").val("601108812100015")
					$("input[name=item_length_all]").val("50")
					$("input[name=item_width_all]").val("50")
					$("input[name=item_height_all]").val("50")
					$("select[name=item_vunit_all]").val("2")
					$("#item_actual_weight").val("5000")
					$("#item_fee_weight").val("5000")
					$("#fee_waybill").val("16")
					$("#fee_total").val("16")

					var consignee_whs_id=$("#consignee_whs_id").data("val");
					console.log(consignee_whs_id)

				})
			})

			NProgress.done();
		});
	});
});