"use strict";
require(['/Sync10-ui/requirejs_config.js'], function() {
	var cssfileall = ["require_css!bootstrap-css/bootstrap.min",
	                  "require_css!Font-Awesome",
	                  "require_css!../css/main.css",
	                  "require_css!oso.lib/bootstrap/css/VisiStyle.css",
	                  "require_css!oso.lib/slidePanel/css/style.css",
  	                  "require_css!../css/easysearch.css",];

  	// 根据浏览器语言加载不同语言包
  	// var canvassion_language =  '../i18n/canvassion-' + window.navigator.languages[0];
  	var canvassion_language =  '../i18n/canvassion-' + 'en-US';
	require(cssfileall, function() {
		require( ["jquery",
				  "bootstrap",
				  "metisMenu",
				  "CompondList/View",
				  "art_Dialog/dialog-plus",
				  "./compond_list_config",
				  "backbone",
				  'handlebars',
				  "slidePanel",
				  'art_Dialog/dialog-plus',
				  '../view/commonTools',
				  '../view/searchAdvanced',
				  "../config",
				  canvassion_language
    			  ],
		function( $,
			 	  bootstrap,
			 	  metisMenu,
			 	  CompondList,
			 	  Dialog,
			 	  list_config,
			 	  backbone,
			 	  Handlebars,
				  SlidePanel,
				  artDialog,
				  CommonToolsView,
				  searchAdvancedView,
				  Config ) {
			window.Parentpush = {};
			$( '#myTab a:first' ).tab( 'show' );

			// // 渲染常用查找
			// new CommonToolsView( {
			// 	el: "#searchSampleTab"
			// } ).render();

			// var advanced = new searchAdvancedView({
			// 	el: "#searchAdvancedTab"
			// }).render();
			$( "#addPackage" ).on( 'click', function(){
				var slide = new SlidePanel({
		            url: "/Sync10-ui/pages/tms/canvassion/add.html",
		            title: '<i class="fa fa-table fa-fw"></i> Create Waybill',
		            duration: 500,
		            topLine: 0,
		            slideLine:"65%"
		        });
	            slide.open();
	            $(".slide-container").addClass("borderClass");
			} );

			$( function() {
				var list = new CompondList( list_config, {
					renderTo: "#main_div",
					dataUrl: "/Sync10/_tms/tmsWaybill/tmsWaybillList"
				} );

				list.render();

				Parentpush.compandList = list;


				list.on("DetailsInfo",function(e) {
						var waybill_id = $(e.button).data("itmeid");
						console.log(waybill_id);
						var url = Config.getItemsById.url;
						url= url+"?waybill_id="+waybill_id;
						$.getJSON(url,function(result){
							if(result!=null){
								var details = '<div class="divdetails">'
									+'<table class="table table-bordered">'
									+'<thead>'
									+'<th>CONTENT</th>'
									+'<th>WEIGHT</th>'
									+'<th>VOLUME</th>'
									+'</thead>'
									+'<tbody>';
								var items = result.DATA;
								for (var i = 0; i < items.length; i++) {
									var weight_unit_name="";
									var volume_unit_name="";
									
									if (items[i].WEIGHT_UNIT_NAME!=undefined) {
										weight_unit_name=items[i].WEIGHT_UNIT_NAME;
									}else{
										weight_unit_name="";
									}

									if (items[i].VOLUME_UNIT_NAME!=undefined) {
										volume_unit_name=items[i].VOLUME_UNIT_NAME;
									}else{
										volume_unit_name="";
									}									
									details += '<tr>'
											+ '<td>'+items[i].CONTENT+'</td>'
											+ '<td>'+items[i].WEIGHT+' '+weight_unit_name+'</td>'
											+ '<td>'+items[i].WIDTH +'*' +items[i].WIDTH+'*'+items[i].LENGTH+' ' + volume_unit_name+'<sup>3</sup>'+'</td>'
											+'</tr>';
								};
								details+="</tbody></table></div>";
								console.log(details);
								var d = artDialog( {
									title: "Items",
								    content: details,
								    id: "itemsDialog"
								} ).show();

							}



						})



					// }

				});
				list.on("events.Edit", function(e) {
					var btntxt = $(e.button.parent()).data("itmeid");
					console.log(btntxt)
					var btntxt = btntxt.replace("WAYBILL_ID|","");
					if(HTMLElement.prototype.getInfo) {
						delete HTMLElement.prototype.getInfo;
					}
					
					$.ajax({
						url: '/Sync10/_tms/tmsWaybill/getTmsWaybillById',
						data: 'waybillId=' + btntxt,
						dataType: 'json',
						type: 'get',
						success:function( data ){
							console.log( data );
							HTMLElement.prototype.getInfo = function() {
								return data.DATA[0];
							}
							var slide = new SlidePanel( {	
								url: "/Sync10-ui/pages/tms/canvassion/add.html",
								title: '<span id="dataInfo"><i class="fa fa-table fa-fw"></i> ' + btntxt + '</span>',
								duration: 500,
								topLine: 0,
								slideLine: "65%"
							} );			

							slide.open();
						},
						error:function(){
							
						}
					});

				});
				list.on( "events.Print", function( e ) {
					var waybill_id = e.button.parent().data( "itmeid" ).split( '|' )[1];
		            $.ajax( { 
		                url: '/Sync10/_tms/tmsWaybill/getTmsWaybillById',
		                data: 'waybillId=' + waybill_id,
		                dataType: 'json',
		                type: 'get',
		                success: function( data ){
		                    // 根据 waybill_id 查询不到记录，提示 System error!
		                    if ( data.DATA.RESULT && data.DATA.RESULT == "norecord" ) {
		                 				                            
		                    } else {
		                        Parentpush.orderInfo = data.DATA[0];
								var d = artDialog( {
									title: "Print",
								    url: '/Sync10-ui/pages/tms_canvassion/index.html?waybillId=' + waybill_id,
								    id: "printDialog"
								} )	
								.width( 500 )
								.height( 700 )
								.show();	 

								Parentpush.d = d;	                       
		                    }
		                },
		                error: function(){
		                    
		                }
		            } );					
				});
	
				NProgress.done();
			});
		});
	});
});