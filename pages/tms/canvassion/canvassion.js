{
	"DATA": [{
		"ORDER": {
			"TRACKING_NO": "1000001",
			"DETAILS": "2",
			"CHILDREN": [{
				"LABLE": "发货类型",
				"VALUE": "限时"
			}, {
				"LABLE": "重量",
				"VALUE": "30.00 KG"
			}, {
				"LABLE": "体积",
				"VALUE": "20*20*20 cm"
			}, {
				"LABLE": "描述",
				"VALUE": "电子器件"
			}, {
				"LABLE": "件数",
				"VALUE": "2"
			}, {
				"LABLE": "特殊配送",
				"VALUE": "易碎，易爆"
			}, {
				"LABLE": "创建时间",
				"VALUE": "11/07/14 11:39"
			}, {
				"LABLE": "MABD",
				"VALUE": "14/07/14 11:39"
			}]
		},
		"SHIPPER": {
			"CHILDREN": [{
				"LABLE": "发货仓库",
				"VALUE": "GZ"
			}, {
				"LABLE": "联系人",
				"VALUE": "张三"
			}, {
				"LABLE": "联系电话",
				"VALUE": "13809090909"
			}, {
				"LABLE": "地址",
				"VALUE": "街道号，门牌号"
			}],
			"TRACKING_NO": "1000001",
			"DETAILS": "1"
		},
		"CONSIGNEE": {
			"CHILDREN": [{
				"LABLE": "收货仓库",
				"VALUE": "BJ"
			}, {
				"LABLE": "收货地区",
				"VALUE": "北京/北京"
			}, {
				"LABLE": "联系人",
				"VALUE": "李四"
			}, {
				"LABLE": "联系电话",
				"VALUE": "13811111111"
			}, {
				"LABLE": "地址",
				"VALUE": "街道号，门牌号"
			}, {
				"LABLE": "邮编",
				"VALUE": "100085"
			}]
		},
		"FEE": {
			"CHILDREN": [{
				"LABLE": "付款方式",
				"VALUE": "寄方付"
			}, {
				"LABLE": "声明价值",
				"VALUE": "1000 元"
			}, {
				"LABLE": "保价费用",
				"VALUE": "500 元"
			}, {
				"LABLE": "运费",
				"VALUE": "500 元"
			}, {
				"LABLE": "合计",
				"VALUE": "1000 元"
			}]
		},
		"STATUS": {
			"CHILDREN": [{
				"LABLE": "GZ仓库已发出",
				"VALUE": "11/7/14 11:22:12"
			}, {
				"LABLE": "GZ仓库已打包",
				"VALUE": "11/7/14 11:22:12"
			}, {
				"LABLE": "GZ仓库已分捡",
				"VALUE": "11/7/14 11:22:12"
			}, {
				"LABLE": "GZ仓库已收件",
				"VALUE": "11/7/14 11:22:12"
			}]
		}
	}],
	"PAGECTRL": {
		"allCount": 1234,
		"first": false,
		"fornt": false,
		"last": true,
		"next": true,
		"pageCount": 13,
		"pageNo": 1,
		"pageSize": 2,
		"rowCount": 3
	},
	"keys":{"itme":[{"td":"00123"},{"td":"77456"}]}
}