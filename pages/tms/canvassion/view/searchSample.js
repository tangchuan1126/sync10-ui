"use strict";
define(["jquery", "backbone", 'handlebars', "../templates/templates.amd"], function($, Backbone, HandleBars, templates) {
	return Backbone.View.extend({
		template: templates.searchSample,
        initialize: function() {
            //this.listenTo(this, "update-model", this.updateModel);
        },
		render: function() {
			var v = this;
			v.$el.html(v.template);
		},
		events:{
                "click #eso_search":"search",
                "keyup #search_key":"enterSearch"
        },
        search:function() {
			var val = $.trim($("#search_key").val());

			if($.trim(val) == "") {
				alert("Please enter key word for searching");
			} else {
				$("#search_key").val(val);

				var list = window.Parentpush.compandList;
		        // 开启搜索
        		// Thekeyword 往服务端传名值对
        		var urls = "/Sync10/_tms/tmsWaybill/getTmsWaybillById";
        		var findset = {
	        			Thekeyword: {
	        				"waybillId": val,
	        				"searchMode":1,
	        				"dataUrl": urls
	        			}
        		};
        		
				list.setQuery( findset.Thekeyword );
			}
        },
        enterSearch:function(event){
        	var that = this;
        	var keycode = (event.keyCode ? event.keyCode : event.which);  
		    if(keycode == '13'){  
		       that.search();
		    } 
        }
	});
});