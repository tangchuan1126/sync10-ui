"use strict";
define([ 'jquery', 
		"backbone", 
		'handlebars', 
		"../templates/templates.amd",
		"oso.lib/table/js/table",
		'../config',
		'bootstrap' ], 
function( 
		$, 
		Backbone, 
		HandleBars, 
		templates,
		Table,
		Config ) {
		return Backbone.View.extend({
			el: null,       
			template: templates.items,
			itemsData: [],						//  保存货物信息
			pacielTable: {},
			render: function( orderInfo ) {
				var that = this;
				console.log(orderInfo);
				if( orderInfo ) {
					if ( orderInfo.ITEMS.length > 1 ) {
						that.$el.html( that.template( {
							orderInfo: orderInfo
						} ) );
						
						$( "#oneItem" ).removeClass( "active" );
						$( "#moreItem" ).addClass( "active" );
						$( "#itemList>.items" ).remove();
						var items = orderInfo.ITEMS;
						for ( var i = 0; i < items.length; i++ ) {

							$( ".items" ).not( "#itemList>.items" ).clone( true ).appendTo( $( "#itemList" ) );			
							$( ".special_property" ).not( "#itemList>.items>.special_property" ).clone( true ).appendTo( $( "#itemList>.items:last" ) );
							var markCount = $( "#itemList>.items" ).length;						
							$( "#itemList>.items:last>.special_property" ).find( ".package_type" ).data( "count", markCount );
							$( "#itemList>.items:last>.special_property" ).find( ".package_type" ).find( "input[type=radio]" ).attr( "name", "package_type" + "_" +  markCount );

							var item_types = $( "#itemList>.items:last" ).find( "input[type=radio][name=package_type" + "_" + markCount + "]" );

							for ( var j = 0; j < item_types.length; j++ ) {
								if ( $( item_types[j]).val() == items[i].PACKAGING ) {
									$( item_types[j] ).prop( "checked", true );
								} else {
									$( item_types[j] ).prop( "checked", false );
								}		
							}			
							
							var double_flag_arr = items[i].DOUBLE_FLAG.split( ',' );
							var item_propertys = $( "#itemList>.items:last" ).find( "[name=item_property]" );
							
							for ( var k = 0; k < item_propertys.length; k++ ) {
								for ( var x = 0; x < double_flag_arr.length; x++ ) {
									if ( $( item_propertys[k] ).val() == double_flag_arr[x] ) {
										$( item_propertys[k] ).prop( "checked", true );
									} 
								}
							}
							
							$( "#itemList>.items:last" ).find( "#item_content" ).val( items[i].CONTENT );
							$( "#itemList>.items:last" ).find( "[name=item_length]" ).val( items[i].LENGTH );
							$( "#itemList>.items:last" ).find( "[name=item_width]" ).val( items[i].WIDTH );
							$( "#itemList>.items:last" ).find( "[name=item_height]" ).val( items[i].HEIGHT );
							$( "#itemList>.items:last" ).find( "[name=item_vunit]" ).val( items[i].VOLUME_UNIT );
							$( "#itemList>.items:last" ).find( "[name=subWeight]" ).val( items[i].WEIGHT );
							$( "#itemList>.items:last" ).find( "[name=item_wunit]" ).val( items[i].WEIGHT_UNIT );
							$( "#itemList>.items:last" ).attr("data-itemid",items[i].item_ID);

						}
						if (orderInfo.WEIGHT!=undefined&&orderInfo.WEIGHT!="") {
							$("#item_actual_weight").val(orderInfo.WEIGHT);
						};
						if (orderInfo.WEIGHT_UNIT!=undefined&&orderInfo.WEIGHT_UNIT!="") {
							$("#item_actual_wunit").val(orderInfo.WEIGHT_UNIT);
						};	
						if (orderInfo.WEIGHT!=undefined&&orderInfo.WEIGHT!="") {
							$("#item_fee_weight").val(orderInfo.WEIGHT);
						};
						if (orderInfo.WEIGHT_UNIT!=undefined&&orderInfo.WEIGHT_UNIT!="") {
							$("#item_fee_wunit").val(orderInfo.WEIGHT_UNIT);
						};											
						that.toShow();
						$( ".items" ).not( "#itemList>.items" ).hide();
						$( ".special_property" ).not( "#itemList>.items>.special_property" ).hide();
						$( "#itemsInfo" ).fadeIn( "slow" );
						that.setItemsNumber();
					} else {
						
						that.$el.html( that.template( {
							orderInfo: orderInfo,
							itemInfo: orderInfo.ITEMS[0]
						} ) );
						$( "[name=item_vunit]").val( orderInfo.ITEMS[0].VOLUME_UNIT );
						$( "#item_actual_wunit" ).val( orderInfo.ITEMS[0].WEIGHT_UNIT );
						$( "#item_fee_wunit" ).val( orderInfo.ITEMS[0].WEIGHT_UNIT );

						var double_flag_arr = orderInfo.ITEMS[0].DOUBLE_FLAG.split( ',' );
						var item_propertys = $( "[name=item_property]" );
						
						for ( var k = 0; k < item_propertys.length; k++ ) {
							for ( var x = 0; x < double_flag_arr.length; x++ ) {
								if ( $( item_propertys[k] ).val() == double_flag_arr[x] ) {
									$( item_propertys[k] ).prop( "checked", true );
								} 
							}
						}	

						var item_types = $( "input[type=radio][name=package_type" );

						for ( var j = 0; j < item_types.length; j++ ) {
							if ( $( item_types[j]).val() == orderInfo.ITEMS[0].PACKAGING ) {
								$( item_types[j] ).prop( "checked", true );
							} else {
								$( item_types[j] ).prop( "checked", false );
							}		
						}	

						$( ".items" ).attr("data-itemid",orderInfo.ITEMS[0].WAYBILL_ID);
					}

				} else {
					that.$el.html( that.template( ) );
				}

	            var pacielTable = new Table({
            		container : '#pacielList',
		    		//url : 'a.json',
		    		localData : that.itemsData,
		    		//title : 'Test Table',
		    		height : 300,
		    		datasName : 'data',
		    		pageSizeName:'size',
		    		pageNumberName:'num',
		    		infoField :'info',
		    		groupKey: 'item_content',
	    			onClickRow: function( rowIndex,rowData,checkbox ) {


	                //localData返回所有数据
	                //this.getLocalData

	                // console.dir(this.getLocalData);

	                 //checkbox --input document
	                 var row_checked=checkbox.checked;

	                 //dom--tr document
	                 // var tr_obj=$(checkbox).parents("tr").eq(0);
	                 //如果row_checked为true那就表示当前行是被选中的

	          },

	            // isEnable:function(row){
	            // 	if(row.age.indexOf('20') !== -1){
	            // 		return false;
	            // 	}
	            // },
	            rownumbers : true,
	            checkbox : true,
	            singleSelection : false,
	            pagination:false,
	            columns:[
	            	{ field: 'item_content', title: 'Description', width: '15%', align: 'center' },
	            	{ field: 'item_length', title: 'Volume', width: '15%', align: 'center', formatter: function ( data, row ) {
	            		if (   row.item_length != '' 
	            			&& row.item_width  != ''
	            			&& row.item_height != ''
	            			&& row.item_vunit  != '' ) {
	            			return row.item_length + '*' + row.item_width + '*' + row.item_height + ' ' + row.item_vunit_name + '<sup>3</sup>';
	            		}
	            	} },
	            	{ field: 'item_actual_weight', title: 'WEIGHT', width: '10%', align: 'center' },
	            	{ field: 'properties_name', title: 'Properties', width: '15%', align: 'center' },
	            	{ field: 'parcelType', title: 'Pallet', align: 'center', formatter: function ( data, row) {
	            		if ( row.palletTypeName != '' ) {
	            			return row.palletTypeName;
	            		}
	            	} }
	            ],
	            toolbar : [
	            {
	            	text : 'add',
	            	iconCls : 'add',
	            	btnClass : 'btn-abc',
	            	handler : function(){

	            		// var rows = table.getSelected();

	            		// table.deleteLocalData(rows, function(row){
	            		// 	table.reloadCurrent(true);
	            		// });
						$('#setQuantity').show();
						that.addPaciel();

	            	}
	            },
	            {
	            	text : 'update',
	            	iconCls : 'update',
	            	handler : function(){
	            		var rows = that.pacielTable.getSelected();
	            		if (rows.length==0) {
	            			alert('You must choose one line of record.')
	            			return false;
	            		};
	            		if (rows.length>1) {
	            			alert('You can only choose one line of record.')
	            			return false;
	            		};
	            		var _index =  rows[0]._index;
	            		console.log(rows[0])
	            		
	            		$("#addItems").attr('data-index',_index);
					var item_content       = rows[0].item_content								//  货物内容
					var item_length 	   = rows[0].item_length							//  货物体积中的长度
					var item_width  	   = rows[0].item_width							//  货物体积的宽度
					var item_height 	   = rows[0].item_height							//  货物体积的高度

					var item_actual_weight = rows[0].item_actual_weight					//  货物实际重量					
					var item_vunit = rows[0].item_vunit									//  货物体积单位					
					var item_wunit = rows[0].item_wunit									//  货物体积单位					
					var parcelType = rows[0].parcelType									
					var palletTypeName = rows[0].palletTypeName									
					if (parcelType==2) {
						var palletType = rows[0].palletType;
					}			
					var properties=rows[0].properties;
					var property_arr = properties.slice(',');
					var property_arr_eles = $('[name=item_property]');
					for (var i = 0; i < property_arr.length; i++) {
						for (var j = 0; j < property_arr_eles.length; j++) {
							if ($(property_arr_eles[j]).val()==property_arr[i]) {
								$(property_arr_eles[j]).prop('checked',true);
							};
						};
					};
					that.isShowPalletType();

            		$('#item_content').val(item_content);
            		$('[name=item_length]').val(item_length);
            		$('[name=item_width]').val(item_width);
            		$('[name=item_height]').val(item_height);
            		$('#item_wunit').val(item_wunit);
            		$('#item_vunit').val(item_vunit);
            		$('#item_actual_weight').val(item_actual_weight);
            		$('#item_fee_weight').val(item_actual_weight);
            		$('#parcelType').val(parcelType);	
            		var parcelType_eles = $('[name=parcel_type]');
            		for (var i = 0; i < parcelType_eles.length; i++) {
            			if ($(parcelType_eles[i]).val()==parcelType) {
            				$(parcelType_eles[i]).prop('checked',true);
            			}
            		};
            		that.isShowPalletType();
            		$('#palletType').data('value',palletTypeName);
            		$('#setQuantity').hide();
            		
						that.addPaciel(1);
	            	}
	            }, {
	            	text : 'delete',
	            	iconCls : 'remove',
	            	handler : function () {
	            		var rows = that.pacielTable.getSelected();
	            		that.pacielTable.deleteLocalData( rows, function ( row ) {
					 		var itemQuantity= that.pacielTable.getTotal();
			 				$('#itemQuantity').text(parseInt(itemQuantity));
	            			that.pacielTable.reloadCurrent( true );
	            		} );
	            	}
	            }
	            ]
	        });			

	        that.pacielTable = pacielTable;




				 $( "#item_actual_weight" ).bind("change", function(){
				  	var actual_weight = $( "#item_actual_weight" ).val();

				  	if( actual_weight != "" && actual_weight != undefined ) {
				  		$( "#item_fee_weight" ).val( actual_weight )
				  	}

			  	})
				 $( "#item_actual_wunit" ).bind( "change", function() {
	
				  	var actual_wunit = $( "#item_actual_wunit" ).val();

				  	if( actual_wunit != "" && actual_wunit != undefined ) {
				  		$( "#item_fee_wunit" ).val( actual_wunit )
				  	}
			  	})	

				 // 初始化 itemQuantity，货物数量
				 $('#itemQuantity').text(0);
				//  设置是否显示托盘物料类型
			  	that.isShowPalletType();	
			  	//  默认不显示添加货物面板
			  	$( '#containerAddParcel' ).find( '.info-body' ).hide();		 
			},
			events: {
				"click #oneItem": "oneItem",
				"click #moreItem": "moreItem",
				"click #addItems": "addItems",
				"click #minusItems": "minusItems",
				"click #itemDetails": "itemDetails",
				"click #parcelType": 'isShowPalletType',
				"blur #item .vr":"valiSingle",
				"click #next_item" : "nextStep",
				"click #previous_item" : "prevStep",
				"change #selectItems": "selectItems",
				"click span#listMinus":"listMinus",
				'click span#addPaciel': 'addPaciel'
			},
			//  切换 containerParcelTable 和 containerAddParcel 容器的内容展示，并改变容器右上侧相对应的图标
			listMinus : function () {

				var that = this;
			
				//  如果 containerParcelTable 容器的内容为隐藏
				if ( $( '#containerParcelTable' ).find( '.info-body' ).is( ':hidden' ) ) {
					$( '#containerParcelTable' ).find( '.info-body' ).show();
					$( '#containerAddParcel' ).find( '.info-body' ).hide();
					$( '#containerParcelTable' ).find( '#listMinus>.glyphicon' )
												.removeClass( 'glyphicon-plus' )
												.addClass( 'glyphicon-minus' );
					$( '#containerAddParcel' ).find( '#addPaciel>.glyphicon' )
											  .removeClass( 'glyphicon-minus' )
											  .addClass( 'glyphicon-plus' );												
				} else {
					$( '#containerParcelTable' ).find( '.info-body' ).hide();
					$( '#containerAddParcel' ).find( '.info-body' ).show();
					$( '#containerParcelTable' ).find( '#listMinus>.glyphicon' )
												.removeClass( 'glyphicon-minus' )
												.addClass( 'glyphicon-plus' );
					$( '#containerAddParcel' ).find( '#addPaciel>.glyphicon' )
											  .removeClass( 'glyphicon-plus' )
											  .addClass( 'glyphicon-minus' );																	
				}

				// 重新计算 pacielTable 宽度
				that.pacielTable.refreshWidth();
				
			},		
			//  切换 containerParcelTable 和 containerAddParcel 容器的内容展示，并改变容器右上侧相对应的图标	
			addPaciel: function () {
	
				//  如果 containerAddParcel 容器的内容为隐藏
				if ( $( '#containerAddParcel' ).find( '.info-body' ).is( ':hidden' ) ) {
					$( '#containerAddParcel' ).find( '.info-body' ).show();
					$( '#containerParcelTable' ).find( '.info-body' ).hide();
					$( '#containerAddParcel' ).find( '#addPaciel>.glyphicon' )
											  .removeClass( 'glyphicon-plus' )
											  .addClass( 'glyphicon-minus' );
					$( '#containerParcelTable' ).find( '#listMinus>.glyphicon' )
												.removeClass( 'glyphicon-minus' )
												.addClass( 'glyphicon-plus' );											  
				} else {
					$( '#containerAddParcel' ).find( '.info-body' ).hide();
					$( '#containerParcelTable' ).find( '.info-body' ).show();
					$( '#containerAddParcel' ).find( '#addPaciel>.glyphicon' )
											  .removeClass( 'glyphicon-minus' )
											  .addClass( 'glyphicon-plus' );
					$( '#containerParcelTable' ).find( '#listMinus>.glyphicon' )
												.removeClass( 'glyphicon-plus' )
												.addClass( 'glyphicon-minus' );											  
				}
			},
			//  1、设置是否显示托盘物料类型
			//      1)如果默认包裹类型为包裹
			//      2）默认类型不为包裹
			//		    显示托盘物料类型
			isShowPalletType: function () {
				var parcelType_checked_eles = $( '.parcelTypeEle:checked' );			//  被选中的包裹类型元素集合
				var parcelType = $( parcelType_checked_eles[0] ).val();					//  被选中的包括类型
				if ( parcelType == 1 ) {
					$( '#palletType' ).addClass( 'eleNone' );
					$( '#addItems' ).appendTo( $( '#parcelType' ) );
				} else {
					$( '#palletType' ).removeClass( 'eleNone' );
					$.getJSON( Config.getPalletPhysicsType.url, function ( json ) {
							$( '#item_plate' ).immybox( {
								Pleaseselect: 'Clean Select',
								choices: json.DATA
							} );
					} );

					$( '#addItems' ).appendTo( $( '#palletType' ) );				
				}
			},		
			//  1、保存货物信息
			//		1）获取表单中的值
			//		2）将货物信息放到 item 对象中
			//		3）将 item 对象存放到 itemsData 数组
			//  2、展示货物信息
			//  	1）将数据添加到 parcelTable 中
			//		2）隐藏 containerAddParcel 容器、显示 containerParcelTable 容器
			addItems: function () {
				var that = this;	
				//  1_1）获取表单中的值
				var item_content       = $( '#item_content' ).val(); 								//  货物内容
				var item_length 	   = $( '[name=item_length]' ).val();  							//  货物体积中的长度
				var item_width  	   = $( '[name=item_width]' ).val();							//  货物体积的宽度
				var item_height 	   = $( '[name=item_height]').val();   							//  货物体积的高度

				var item_vunit_name    = $( '[name=item_vunit]' ).find( 'option:selected').text();	//  货物体积单位名字				
				var item_vunit    = $( '#item_vunit' ).val();										//  货物体积单位				

				var item_actual_weight = $( '#item_actual_weight' ).val();  						//  货物实际重量					
				var item_wunit_name    = $('[name=item_wunit]').find( 'option:selected').text();					//  货物重量单位名字	
				var item_wunit   = $('[name=item_wunit]').val();					//  货物重量单位	
	
				var property_checked_eles = $( '[name=item_property]:checked' );					//  被选中的特殊属性元素集合
				var property_temp_arr = [];															//  临时存放属性的数组
				var property_name_temp_arr = [];													//  临时存放属性名字的数组
				var properties = '';																//  特殊属性（最终结果）
				var properties_name = '';															//  特殊名字属性（临时展示）
				//  取出被选中的特殊属性元素集合中元素的 val 值放进 property_temp_arr
				for ( var i = 0; i < property_checked_eles.length; i++ ) {
					property_temp_arr.push( $( property_checked_eles[i] ).val() );
					property_name_temp_arr.push( $( property_checked_eles[i] ).next( 'label' ).html() );
				}	

				properties = property_temp_arr.join( ',' );								//  货物属性，如 '2, 3, 5'
				properties_name = property_name_temp_arr.join( ',' );					//  货物属性，如 'WHE, ELE, TIR'
				var parcelType_checked_eles = $( '.parcelTypeEle:checked' );			//  被选中的包裹类型元素集合
				var parcelType = $( parcelType_checked_eles[0] ).val();					//  被选中的包括类型
				var palletType;															//  物料类型
				var palletTypeName;														//  物料类型名称
				if ( parcelType == 2 ) {
					palletType = $( '#item_plate' ).data( 'value' );
					palletTypeName = $( '#item_plate' ).val();
				} 
				//  1_2）将货物信息放到 item 对象中
				var item = {
					item_content: item_content,
					item_length: item_length,
					item_width: item_width,
					item_height: item_height,
					item_vunit_name: item_vunit_name,
					item_vunit: item_vunit,
					item_actual_weight: item_actual_weight,
					item_wunit_name: item_wunit_name,
					properties: properties,
					properties_name: properties_name,
					parcelType: parcelType,
					palletType: palletType,
					palletTypeName: palletTypeName
				};

				//如果是修改
				if ($('#addItems').data('index')!=undefined) {
					var index = $('#addItems').data('index');
					var allData = that.pacielTable.getLocalData();
					console.log(allData);
					var newItem = $.extend(true, {}, item);
					allData[index]=newItem;
					console.log(allData);
					that.itemsData=allData;
					that.pacielTable.loadData(that.itemsData);
					that.pacielTable.reload( true );
			 		var itemQuantity= that.pacielTable.getTotal();
			 		$('#itemQuantity').text(parseInt(itemQuantity));	
			 		$('#addItems').removeAttr('index');					
				} else {
				 	//  1_3）将 item 对象存放到 itemsData 数组
				 	//  判断是否有数量
				 	var item_quantity = $( '#item_quantity' ).val();
				 	if ( item_quantity != '' && !isNaN( item_quantity ) ) {
				 		for ( var i = 0; i < item_quantity; i++ ) {
				 	var newItem = $.extend(true, {}, item);
				 			that.itemsData.push( newItem);
				 		}
				 	} else {
				 		 	var newItem = $.extend(true, {}, item);
				 			that.itemsData.push( newItem);
				 	}		

			 	

				 	 // 2_1）将数据添加到 parcelTable 中


				 	that.pacielTable.addLocalData( that.itemsData, function() {
				 		that.itemsData=[];
				 		// 重新加载 parcelTable 中的数据
				 		that.pacielTable.reload( true );
				 		var itemQuantity= that.pacielTable.getTotal();
				 		$('#itemQuantity').text(parseInt(itemQuantity));	 		
				 	} );					 				
				}

		

				// 清空值
				$("#item_content").val("");
				$("[name=item_length]").val("");
				$("[name=item_width]").val("");
				$("[name=item_height]").val("");
				$('#item_actual_weight').val("");
				$('#item_fee_weight').val("");
				$('[name=item_vunit]').val("");
				$('#item_quantity').val("");
				var parcelType_eles= $('[name=parcel_type]');
				for (var i = 0; i < parcelType_eles.length; i++) {
					if($(parcelType_eles[i]).val()==1) {
						$(parcelType_eles[i]).prop('checked',true);
					}
				};
				var double_flag_ele = $( "[name=item_property]:checked" );
				var double_flag_arr = [];
				var double_flag = "";
				for ( var i = 0; i < double_flag_ele.length; i++ ) {
					$( double_flag_ele[i] ).attr("checked",false);
				}
				that.isShowPalletType();
				//  展示货物信息
				that.listMinus();
				$('#setQuantity').show();
			},			
			prevStep: function() {
				$( '#tab_2' ).click();
				// 校验货物信息
				this.validateResult();
				return false;
			},
			nextStep:function(){
				$( '#tab_4' ).click();
				// 校验货物信息
				this.validateResult();
			},

			minusItems: function( evt ) {	
				var that = this;
				if( $( evt.target ).attr( "id" ) && $( evt.target ).attr( "id" ) == "minusItems" ) {
					$( evt.target ).parent().parent().remove();
				} else {
					$( evt.target ).parent().parent().parent().remove();
				}
				
				that.setItemsNumber();
			},
			setItemsNumber: function( num ) {			
				if ( num != undefined ) {
					$( "#itemsNumber" ).html( num );
				} else {
					var itemsNumber = $( "#itemList>.items" ).length;
					$( "#itemsNumber" ).html( itemsNumber );
				}
				
			},
			valiSingle:function(e)
			{
				var method =$(e.target).attr("data-vr");
				this[method](e);
			},
			isEmpty:function(obj){
				if($(obj).val()=="") {
					if(!$(obj).parent().hasClass("has-error")){
						$(obj).parent().addClass("has-error");
						var ele = '<div class="col-sm-3 error show">'
							+'<span class="error"></span>'
							+'</div>';
						$(obj).parent().after(ele);
					}
				}else{
					$(obj).parent().removeClass('has-error');

					$(obj).parent().parent().find('.error').remove();
				}
			},
			validate:function()  {

				return this.validateResult();
			},
			validateResult:function(){
				if( $("#item .has-error").length > 0  )
				{
					$('#myTab li:eq(2)').find(".check").removeClass("checked").addClass('checkerror');
					return false;
				}
				else
				{
					$('#myTab li:eq(2)').find(".check").removeClass("checkerror").addClass('checked');
					return true;
				}
			},
			valiConsigneeName:function(){
				var that = this;
				that.isEmpty("#consignee_name");
			},
			clearVal:function( item_types ){
				$( "#itemList>.items:last" ).find( "#item_content" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=item_length]" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=item_width]" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=item_height]" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=subWeight]" ).val( "" );
				
				var item_propertys = $( "#itemList>.items:last" ).find( "[name=item_property]" );
				for ( var i = 0; i < item_propertys.length; i++ ) {
						$( item_propertys[i] ).prop( "checked", false );	
									
				};

				var markCount = $( "#itemList>.items:last>.special_property" ).find( ".package_type" ).data( "count" );
				var item_types = $( "#itemList>.items:last" ).find( "input[type=radio][name=package_type" + "_" + markCount + "]" );

				for ( var i = 0; i < item_types.length; i++ ) {
					if( $( item_types[i]).val() == 1 ) {
						$( item_types[i] ).prop( "checked", true );
					}else {
						$( item_types[i] ).prop( "checked", false );
					}		
				}

				$( "#item_plate" ).val( -1 );
			}


		} );
});