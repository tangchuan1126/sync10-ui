"use strict";
define(["jquery", 
		"backbone", 
		'handlebars', 
		"../templates/templates.amd"], 
function( 
		$, 
		Backbone, 
		HandleBars, 
		templates ) {
		return Backbone.View.extend({
			el: null,       
			template: templates.packages,
			render: function( orderInfo ) {
				console.info( orderInfo );
				var that = this;
				if( orderInfo ) {
					if ( orderInfo.ITEMS.length > 1 ) {
						that.$el.html( that.template( {
							orderInfo: orderInfo
						} ) );
						
						$( "#oneItem" ).removeClass( "active" );
						$( "#moreItem" ).addClass( "active" );
						$( "#itemList>.items" ).remove();
						var items = orderInfo.ITEMS;
						for ( var i = 0; i < items.length; i++ ) {

							$( ".items" ).not( "#itemList>.items" ).clone( true ).appendTo( $( "#itemList" ) );			
							$( ".special_property" ).not( "#itemList>.items>.special_property" ).clone( true ).appendTo( $( "#itemList>.items:last" ) );
							var markCount = $( "#itemList>.items" ).length;						
							$( "#itemList>.items:last>.special_property" ).find( ".package_type" ).data( "count", markCount );
							$( "#itemList>.items:last>.special_property" ).find( ".package_type" ).find( "input[type=radio]" ).attr( "name", "package_type" + "_" +  markCount );

							var packages_types = $( "#itemList>.items:last" ).find( "input[type=radio][name=package_type" + "_" + markCount + "]" );

							for ( var j = 0; j < packages_types.length; j++ ) {
								if ( $( packages_types[j]).val() == items[i].PACKAGING ) {
									$( packages_types[j] ).prop( "checked", true );
								} else {
									$( packages_types[j] ).prop( "checked", false );
								}		
							}			
							
							var double_flag_arr = items[i].DOUBLE_FLAG.split( ',' );
							var packages_propertys = $( "#itemList>.items:last" ).find( "[name=packages_property]" );
							
							for ( var k = 0; k < packages_propertys.length; k++ ) {
								for ( var x = 0; x < double_flag_arr.length; x++ ) {
									if ( $( packages_propertys[k] ).val() == double_flag_arr[x] ) {
										$( packages_propertys[k] ).prop( "checked", true );
									} 
								}
							}
							
							$( "#itemList>.items:last" ).find( "#packages_content" ).val( items[i].CONTENT );
							$( "#itemList>.items:last" ).find( "[name=packages_length_all]" ).val( items[i].LENGTH );
							$( "#itemList>.items:last" ).find( "[name=packages_width_all]" ).val( items[i].WIDTH );
							$( "#itemList>.items:last" ).find( "[name=packages_height_all]" ).val( items[i].HEIGHT );
							$( "#itemList>.items:last" ).find( "[name=packages_vunit_all]" ).val( items[i].VOLUME_UNIT );
							$( "#itemList>.items:last" ).find( "[name=subWeight]" ).val( items[i].WEIGHT );
							$( "#itemList>.items:last" ).find( "[name=packages_wunit_all]" ).val( items[i].WEIGHT_UNIT );
							$( "#itemList>.items:last" ).attr("data-itemid",items[i].item_ID);

						}
						if (orderInfo.WEIGHT!=undefined&&orderInfo.WEIGHT!="") {
							$("#packages_actual_weight").val(orderInfo.WEIGHT);
						};
						if (orderInfo.WEIGHT_UNIT!=undefined&&orderInfo.WEIGHT_UNIT!="") {
							$("#packages_actual_wunit").val(orderInfo.WEIGHT_UNIT);
						};	
						if (orderInfo.WEIGHT!=undefined&&orderInfo.WEIGHT!="") {
							$("#packages_fee_weight").val(orderInfo.WEIGHT);
						};
						if (orderInfo.WEIGHT_UNIT!=undefined&&orderInfo.WEIGHT_UNIT!="") {
							$("#packages_fee_wunit").val(orderInfo.WEIGHT_UNIT);
						};											
						that.toShow();
						$( ".items" ).not( "#itemList>.items" ).hide();
						$( ".special_property" ).not( "#itemList>.items>.special_property" ).hide();
						$( "#itemsInfo" ).fadeIn( "slow" );
						that.setItemsNumber();
					} else {
						
						that.$el.html( that.template( {
							orderInfo: orderInfo,
							itemInfo: orderInfo.ITEMS[0]
						} ) );
						$( "[name=packages_vunit_all]").val( orderInfo.ITEMS[0].VOLUME_UNIT );
						$( "#packages_actual_wunit" ).val( orderInfo.ITEMS[0].WEIGHT_UNIT );
						$( "#packages_fee_wunit" ).val( orderInfo.ITEMS[0].WEIGHT_UNIT );

						var double_flag_arr = orderInfo.ITEMS[0].DOUBLE_FLAG.split( ',' );
						var packages_propertys = $( "[name=packages_property]" );
						
						for ( var k = 0; k < packages_propertys.length; k++ ) {
							for ( var x = 0; x < double_flag_arr.length; x++ ) {
								if ( $( packages_propertys[k] ).val() == double_flag_arr[x] ) {
									$( packages_propertys[k] ).prop( "checked", true );
								} 
							}
						}	

						var packages_types = $( "input[type=radio][name=package_type" );

						for ( var j = 0; j < packages_types.length; j++ ) {
							if ( $( packages_types[j]).val() == orderInfo.ITEMS[0].PACKAGING ) {
								$( packages_types[j] ).prop( "checked", true );
							} else {
								$( packages_types[j] ).prop( "checked", false );
							}		
						}	
						console.log(orderInfo.ITEMS[0].WAYBILL_ID)
						$( ".items" ).attr("data-itemid",orderInfo.ITEMS[0].WAYBILL_ID);
					}

				} else {
					that.$el.html( that.template( ) );
				}

				$.ajax({
					url: '/Sync10/_tms/sorting/tmsOrder/getPalletList',
					dataType: 'json',
					type: 'get',
					success:function( data ){
						if(data&&data.DATA&&data.DATA.length>0){
							var length = data.DATA.length;
							 $( "[name=packages_plate_all]" ).empty();
							 $( "[name=packages_plate_all]" ).append('<option value="-1">请选择</option>');   //为Select追加一个Option(下拉项)
							 for (var i = 0; i < length; i++) {
							 	if ( orderInfo&&orderInfo.DETAILS&&orderInfo.ITEMS[0].PALLET_ID == data.DATA[i].PALLET_ID ) {
							 								 		$( "[name=packages_plate_all]" )
						 			.append( '<option selected value="' + data.DATA[i].PALLET_ID + '">' 
						 					+ data.DATA[i].PALLET_ID + "——" + data.DATA[i].TITLE + '</option>'); 
							 	} else {
							 								 		$( "[name=packages_plate_all]" )
						 			.append( '<option value="' + data.DATA[i].PALLET_ID + '">' 
						 					+ data.DATA[i].PALLET_ID + "——" + data.DATA[i].TITLE + '</option>'); 
							 	}
							 	
							 }
						}
					},
					error:function(){
						
					}

				});

				 $( "#packages_actual_weight" ).bind("change", function(){
				  	var actual_weight = $( "#packages_actual_weight" ).val();

				  	if( actual_weight != "" && actual_weight != undefined ) {
				  		$( "#packages_fee_weight" ).val( actual_weight )
				  	}

			  	})
				 $( "#packages_actual_wunit" ).bind( "change", function() {
	
				  	var actual_wunit = $( "#packages_actual_wunit" ).val();

				  	if( actual_wunit != "" && actual_wunit != undefined ) {
				  		$( "#packages_fee_wunit" ).val( actual_wunit )
				  	}
			  	})	
			  	that.selectPackageType();			 
			},
			events: {
				"click #oneItem": "oneItem",
				"click #moreItem": "moreItem",
				"click #addItems": "addItems",
				"click #minusItems": "minusItems",
				"click #itemDetails": "itemDetails",
				"click .selPackageType": "selectPackageType",
				"blur #packages .vr":"valiSingle",
				"click #next_packages" : "nextStep",
				"click #previous_packages" : "prevStep"
			},
			prevStep: function() {
				$( '#tab_2' ).click();
				// 校验货物信息
				this.validateResult();
				return false;
			},
			nextStep:function(){
				$( '#tab_4' ).click();
				// 校验货物信息
				this.validateResult();
			},
			oneItem: function() {
				if ( $( "#oneItem" ).hasClass( "active" ) ) {
					return;
				}
				$( "#moreItem" ).removeClass( "active" );
				$( "#oneItem" ).addClass( "active" );
				$( "#itemList>.items" ).remove();
				var packages_types = $( ".special_property" ).not( "#itemList>.items>.special_property" ).find( "input[type=radio][name=package_type]" );

				for ( var i = 0; i < packages_types.length; i++ ) {
					if( $( packages_types[i] ).val() == 1 ) {
						$( packages_types[i] ).prop( "checked", true );
					}else {
						$( packages_types[i] ).prop( "checked", false );
					}
				}
				$( ".items" ).not( "#itemList>.items" ).show();
				$( ".special_property" ).not( "#itemList>.items>.special_property" ).show();
				$( "#itemsInfo" ).hide();
			},
			moreItem: function() {
				
				if ( $( "#moreItem" ).hasClass( "active" ) ) {
					return;
				}
				var that = this;

				$( "#oneItem" ).removeClass( "active" );
				$( "#moreItem" ).addClass( "active" );
				$( ".items" ).not( "#itemList>.items" ).clone( true ).appendTo( $( "#itemList" ) );
				
				$( ".special_property" ).not( "#itemList>.items>.special_property" ).clone( true ).appendTo( $( "#itemList>.items" ) );
				var markCount = $( "#itemList>.items" ).length;
				$( "#itemList>.items>.special_property" ).find( ".package_type" ).data( "count", markCount );
				$( "#itemList>.items>.special_property" ).find( ".package_type" ).find( "input[type=radio]" ).attr( "name", "package_type" + "_" +  markCount );
				
				// 将单选按钮默认为 1（包裹）
				var sts = $( "#itemList>.items>.special_property" ).find( ".package_type" ).find( ".selPackageType" );
				for ( var i = 0; i < sts.length; i++ ) {
					if ( $( sts[i] ).val() == 1 ) {
						$( sts[i] ).prop( "checked", true );
					} else {
						$( sts[i] ).prop( "checked", false );
					}
				}
				that.selectPackageType();
				// 清空被复制的元素的值
				that.setItemsNumber( 1 );
				that.clearCloneVal();
				that.toShow();
				$( ".items" ).not( "#itemList>.items" ).hide();
				$( ".special_property" ).not( "#itemList>.items>.special_property" ).hide();
				
				$( "#itemsInfo" ).fadeIn( "slow" );
			},
			addItems: function() {
				var that = this;
				$( ".items" ).not( "#itemList>.items" ).clone( true ).appendTo( $( "#itemList" ) );
				$( ".special_property" ).not( "#itemList>.items>.special_property" ).clone( true ).appendTo( $( "#itemList>.items:last" ) );
				var markCount = $( "#itemList>.items" ).length;
				$( "#itemList>.items:last>.special_property" ).find( ".package_type" ).data( "count", markCount );
				$( "#itemList>.items:last>.special_property" ).find( ".package_type" ).find( "input[type=radio]" ).attr( "name", "package_type" +           
				"_" + markCount );
				// 清空被复制的元素的值
				that.clearCloneVal();
				// 将单选按钮默认为 1（包裹）
				var sts = $( "#itemList>.items>.special_property:last" ).find( ".package_type" ).find( ".selPackageType" );
				for ( var i = 0; i < sts.length; i++ ) {
					console.log( $( sts[i]).val() );
					if ( $( sts[i] ).val() == 1 ) {
						$( sts[i] ).prop( "checked", true );
					} else {
						$( sts[i] ).prop( "checked", false );
					}
				}
				that.selectPackageType();				
				$( "#itemList>.items>.special_property" ).show();	
				that.toShow();
				$( "#itemList>.items" ).show();
				
				that.setItemsNumber();

			},
			minusItems: function( evt ) {	
				var that = this;
				if( $( evt.target ).attr( "id" ) && $( evt.target ).attr( "id" ) == "minusItems" ) {
					$( evt.target ).parent().parent().remove();
				} else {
					$( evt.target ).parent().parent().parent().remove();
				}
				
				that.setItemsNumber();
			},
			itemDetails: function() {
				if( $( "#itemList>.items" ).is( ":hidden" ) ) {
					$( "#itemList>.items" ).show();     
				} else{
					$( "#itemList>.items" ).hide();
				}
			},
			selectPackageType: function() {
				var elePackageTypes = $( ".selPackageType:checked" );
				for (var i = 0; i < elePackageTypes.length; i++) {
					if ( $( elePackageTypes[i] ).val() == 1 ) {
						$( elePackageTypes[i] ).parent().parent().parent().next().removeClass( "eleNone" );
					} else {
						$( elePackageTypes[i] ).parent().parent().parent().next().addClass( "eleNone" );
					}
				}
			},
			setItemsNumber: function( num ) {			
				if ( num != undefined ) {
					$( "#itemsNumber" ).html( num );
				} else {
					var itemsNumber = $( "#itemList>.items" ).length;
					$( "#itemsNumber" ).html( itemsNumber );
				}
				
			},
			toShow: function() {
				$( "#itemList>.items>.weightGroup" ).show();
				$( "#itemList>.items>.form-group>#minusitems" ).show();
			},
			valiSingle:function(e)
			{
				var method =$(e.target).attr("data-vr");
				this[method](e);
			},
			isEmpty:function(obj){
				if($(obj).val()=="") {
					if(!$(obj).parent().hasClass("has-error")){
						$(obj).parent().addClass("has-error");
						var ele = '<div class="col-sm-4 error show">'
							+'<span class="error"></span>'
							+'</div>';
						$(obj).parent().after(ele);
					}
				}else{
					$(obj).parent().removeClass('has-error');

					$(obj).parent().parent().find('.error').remove();
				}
			},
			validate:function()  {
				//this.valiConsigneeName();
				//this.valiCustomerDN();
				//this.valiRetailPO();
				//	this.valiShipFrom();

				//Mabd
				//this.valiMabd();
				//this.valiShipDate();
				//this.valiDeliWindow();
				return this.validateResult();
			},
			validateResult:function(){
				if( $("#packages .has-error").length > 0  )
				{
					$('#myTab li:eq(2)').find(".check").removeClass("checked").addClass('checkerror');
					return false;
				}
				else
				{
					$('#myTab li:eq(2)').find(".check").removeClass("checkerror").addClass('checked');
					return true;
				}
			},
			valiConsigneeName:function(){
				var that = this;
				that.isEmpty("#consignee_name");
			},
			clearCloneVal:function( packages_types ){
				$( "#itemList>.items:last" ).find( "#packages_content" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=packages_length_all]" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=packages_width_all]" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=packages_height_all]" ).val( "" );
				$( "#itemList>.items:last" ).find( "[name=subWeight]" ).val( "" );
				
				var packages_propertys = $( "#itemList>.items:last" ).find( "[name=packages_property]" );
				for ( var i = 0; i < packages_propertys.length; i++ ) {
						$( packages_propertys[i] ).prop( "checked", false );	
									
				};

				var markCount = $( "#itemList>.items:last>.special_property" ).find( ".package_type" ).data( "count" );
				var packages_types = $( "#itemList>.items:last" ).find( "input[type=radio][name=package_type" + "_" + markCount + "]" );

				for ( var i = 0; i < packages_types.length; i++ ) {
					if( $( packages_types[i]).val() == 1 ) {
						$( packages_types[i] ).prop( "checked", true );
					}else {
						$( packages_types[i] ).prop( "checked", false );
					}		
				}

				$( "#packages_plate_all" ).val( -1 );
			}


		} );
});