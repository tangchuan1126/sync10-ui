"use strict";
define( ["jquery", "backbone", 'handlebars', "../templates/templates.amd",
	"handlebars_ext",
	"oso.lib/bankInput/bankInput"], 
	function( $, Backbone, HandleBars, templates,HandlebarsHelper ) {


	HandlebarsHelper.registerHelper('debug',function(data){
		console.log(data);
	});

	return Backbone.View.extend({
		el: null,
		template: templates.fee,
		render: function( feeInfo ) {
			var v = this;
			if( feeInfo ) {
				v.$el.html( v.template( {
					feeInfo: feeInfo
				} ) );

				$( "#next_fee span#waybill_oper").text( "更新" );
			} else {
				v.$el.html( v.template( ) );
			}



			$("#fee_account").bankInput(); 			
		},
		events:{
			"click input[type=radio][name=send_package_type]": "threeInfo",
			"blur #consignee .vr":"valiSingle",
			"click #previous_fee" : "previousStep"

		},
		previousStep:function(e){


			var that = this;
			$( '#tab_3' ).click();
			//console.log($('#tab_3'));
			// 校验发件人信息
			//that.validateResult();

			return false;
		},
		threeInfo:function(){
			
				var send_package_type = $("input[type=radio][name=send_package_type]:checked").val();
				if(send_package_type == 3){
					$("#threeInfo").show();
				} else {
					$("#threeInfo").hide();
				}
		},
		validate:function()  {
			//this.valiConsigneeName();
			//this.valiCustomerDN();
			//this.valiRetailPO();
			//	this.valiShipFrom();

			//Mabd
			//this.valiMabd();
			//this.valiShipDate();
			//this.valiDeliWindow();
			return this.validateResult();
		},
		validateResult:function(){
			if( $("#fee .has-error").length > 0  )
			{
				$('#myTab li:eq(3)').find(".check").removeClass("checked").addClass('checkerror');
				return false;
			}
			else
			{
				$('#myTab li:eq(3)').find(".check").removeClass("checkerror").addClass('checked');
				return true;
			}
		}
	});
});