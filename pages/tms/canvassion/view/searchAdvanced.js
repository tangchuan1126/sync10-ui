"use strict";
define( ["jquery", 
		"backbone", 
		'handlebars', 
		"../templates/templates.amd",
		"bootstrap.datetimepicker",
		"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
		"require_css!bootstrap.datetimepicker-css",
		"domready!"],
 function( $, Backbone, HandleBars, templates,datetimepicker,AsynLoadQueryTree ) {
	var canvassionI18n = window.regional['defaultLanguage'];
	return Backbone.View.extend( {
		template: templates.searchAdvanced,
		render: function( globalCollection ) {
			var v = this;
			//  渲染模板
			v.$el.html( v.template( { ci: canvassionI18n }) );

            var shippertree = new AsynLoadQueryTree( {
                renderTo: "#shipFrom",
                Pleaseselect: "请选择",
                Async: true,
                //  异步时，value 是服务端写好的面包屑路径
                //  非异步是直接 id--selectid: 11110
                multiselect: false,
                Parentclick: true,
                //  selectid: { value: '1189716' },
                dataUrl: '/Sync10/_tms/tmsWayItem/WHSList',
                scrollH: 400
          	} );
            shippertree.render();
			//  为运单时间绑定时间控件
			$( '#startTime' ).datetimepicker({
		       format: 'yyyy-mm-dd - hh:ii:ss',
		       autoclose: 1,
		       todayBtn: true,
		       todayHighlight: true,
		       minuteStep: 5
			});
            
			$( '#endTime' ).datetimepicker( {
		       format: 'yyyy-mm-dd - hh:ii:ss',
		       autoclose: 1,
		       todayBtn: true,
		       todayHighlight: true,
		       minuteStep: 5
			} );
            
			// $("#masd_time").datetimepicker({
		 //       format: "yyyy-mm-dd - hh:ii:ss",
		 //       autoclose: 1,
		 //       todayBtn: true,
		 //       todayHighlight: true,
		 //       minuteStep: 5
			// });



		 //    var consigneetree = new AsynLoadQueryTree({
   //                  renderTo: "#consigneeztreebox",
   //                  Pleaseselect:"请选择",
   //                  //dataUrl: "./d.json",
   //                  //异步时，value是服务端写好的面包屑路径
   //                  //非异步是直接id--selectid:11110
   //                  multiselect:false,
   //                  Parentclick:true,
   //                  //selectid:{value:"1189716"},
   //                  dataUrl: "/Sync10/_tms/tmsWayItem/WHSList",
   //                  scrollH:400
   //        	});
     
   //          consigneetree.render();
		},
		events:{
			"click #reset": "reset",
			"click #filter": "filter",
			"change #send_type": "typeChanged"
		},
		reset:function(){
			$( "#ct_start" ).val( "" );
			$( "#ct_end" ).val( "" );
			$( "#masd_time" ).val( "" );
			$( "#creater" ).val( "0" )
			$( "#shipperztreebox" ).val( "" );
			$( "#consigneeztreebox" ).val( "" );
			$( "#send_type" ).val( "0" );
			$( "#trackstatus" ).val( "0" );

		},
		filter: function(){
            var v = this;
            var ct_start = $( "#ct_start" ).val();
            var ct_end = $( "#ct_end" ).val();
            
            var creater = $( "#creater" ).val();
            var shipperStorage = $( "#shipperztreebox" ).val();
            var consigneeStorage = $( "#consigneeztreebox" ).val();
            var send_type = $( "#send_type" ).val();
            var trackstatus = $( "#trackstatus" ).val();
            var list = window.Parentpush.compandList;
            var urls = "/Sync10/_tms/tmsWaybill/tmsWaybillList";

            var findset = { 
                Thekeyword: { 
                    "ct_start": ct_start, 
                    "ct_end": ct_end,
                    "creater": creater,
                    "shipperStorage": shipperStorage,
                    "consigneeStorage": consigneeStorage,
                    "send_type": send_type,
                    "trackstatus": trackstatus
                }
            };  
         	if ( $( "#send_type" ).val() == 4 ) {
				var masd_time = $( "#masd_time" ).val();
				$.extend( findset.Thekeyword, { "masd_time": masd_time} );
         	}
            list.setQuery( findset.Thekeyword ); 
		},
		// 当发送类型为指定日期时，显示 mabd
		typeChanged:function(){
			if ( $( "#send_type" ).val() == 4 ) {
				$( "#masd_time" ).parent().removeClass( "eleNone" );
				$( "#masd_time" ).parent().prev().removeClass( "eleNone" );
			} else {
				$( "#masd_time" ).parent().addClass( "eleNone" );
				$( "#masd_time" ).parent().prev().addClass( "eleNone" );
			}
		}
	});
});