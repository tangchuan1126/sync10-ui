'use strict';
define( ['jquery', 
		 'backbone', 
		 'handlebars', 
 		"bootstrap.datetimepicker",
 		'art_Dialog/dialog-plus',
		 
 		"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
	 		"../config", 
	 	'../templates/templates.amd'], 
function ( $, 
		   Backbone, 
		   HandleBars, 
		   datetimepicker,
		   dialog,		   
		   AsynLoadQueryTree,
		   config,
		   templates ) {
	return Backbone.View.extend( {
		el: null,
		template: templates.shipper,
		render: function( shipperInfo ) {
			var that = this;
			// 当 shipperInfo 不为空，则为编辑状态
			if( shipperInfo ) {
				that.$el.html( that.template( {
					shipperInfo: shipperInfo
				} ) );
			// 否则为添加状态
			} else {
				that.$el.html( that.template( ) );
			}	

			$( "#shipper_appt" ).datetimepicker({
		        format: "yyyy-mm-dd hh:ii:ss",
		        // Whether or not to close the datetimepicker immediately when a date is selected
		        autoclose: 1,
				// If true or "linked", displays a "Today" button at the bottom of the datetimepicker to select the current date. 
				// If true, the "Today" button will only move the current date into view; if "linked", the current date will 
				// also be selected.		        
		        todayBtn: 'linked',
		        // If true, highlights the current date.
		        todayHighlight: true,
		        // The lowest view that the datetimepicker should show. 可最小选到小时
		        minView: 1,
		        startDate: new Date()
			})	
			var selectedId = "11036";//默认选择的的国家
			/*
			this.initCountry(selectedId);	
			this.initStates(selectedId,"");
			*/
		},
		events:
		{
			"click #next_shipper" : "nextStep",
			'blur #shipper .vr': 'valiSingle',
			'keyup #shipper_contact':'autoContact',
			'blur #shipper_contact':'blurContact'
		},
		autoContact:function(e){
			// var that = this;
			var obj =$(e.target);

			// console.log(obj.val());

			var contact=$.trim($('#shipper_contact').val());
			if (contact.length>=3) {
				// console.dir(obj);
		
				// console.log(this.__proto__.events);
				//obj.off('blur');
				// var object = {};
				// _.extend(object, Backbone.Events);
				// object.unbind("blur", 'blurContact');
				$(this.el).undelegate('#shipper_contact', 'blur');
				var test = 1;
				if(test==1) {
					var d = dialog({
					    title: 'Add Customer',
					    content: 'Users do not exist, whether to add？',
					    ok: function () {
					        var that = this;
					        this.title('Under submission..');
					        setTimeout(function () {
					            that.close().remove();
					        }, 2000);
					        return false;
					    },
					    cancel: function () {
				
					    }
					});
					d.show();
				} else {

				}
			}
    


        // $(this).unbind("blur");

			
		},
		initCountry:function(selectedId)
		{
			var that = this;
			$.ajax({
				url: config.getAllCountry.url,
				type: 'GET',
				dataType: 'json'
			})
			.done(function(countrys) 
			{
				that.country = $("#shipper_country").immybox({
					Pleaseselect:'Clean Select',
      				Defaultselect:selectedId,
					choices: countrys.DATA,
					change:function(e,data)
					{

						var value = data.value ||data.VALUE
 						if(value)
 						{
 							that.initStates(value,"");
 							$("#shipper_state").attr("data-value","");
 							$("#shipper_state").val("");
 						}
 						
					}
				});

				/**
			 	$("#country").empty();
				var options = $("#country")[0].options;
				var selectedId = "11036";
				$.each(countrys,function(i){
					var option = new Option(countrys[i].C_COUNTRY,countrys[i].CCID); 
					if(selectedId == countrys[i].CCID)
					{
						option.selected=true;
					}
					options.add(option);
				});

				that.initStates(selectedId,-1);  **/
			})
			.fail(function(e) {
				console.log(e);
			})
			.always(function() {
				//console.log("complete");
			});
		},	
		initStates:function(countryId,selectedId)
		{	
		   var that = this;
			$.ajax({
				url: config.cityInfo.url,
				type: 'GET',
				dataType: 'json',
				data: {ccid: countryId},
			}).done(function(json) 
			{	
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].pro_id,"text":json[i].p_code});
				}
				if(that.state)
				{
					that.state[0].Rechoicesdata(rls,selectedId);
				}
				else
				{	
					var states = $("#shipper_state").immybox({
						Pleaseselect:'Clean Select',
          				Defaultselect:selectedId,
						choices: rls,
						change:function(e,data) {
							var value = data.value ||data.VALUE
							$("#shipper_whs_id").attr("data-val","");
							$("#shipper_whs_id").attr("data-val","");
							$("#shipper_whs_id").unbind();
							$("#shipper_whs_id").val("");								
						  	// 初始化收件仓库
						    var shippertree = new AsynLoadQueryTree( {
					            renderTo: "#shipper_name",
					            Pleaseselect: "Please Select",
					            //异步时，value是服务端写好的面包屑路径
					            //非异步是直接id--selectid:11110
					            multiselect: false,
					            Parentclick: true,
					            dataUrl: "/Sync10/_tms/tmsWayItem/WHSList?state_id="+value,
					            scrollH:400
				          	} );

				            shippertree.render(); 							
	 					// 	if(value)
		 				// 		{
		 				// 			that.initStates(value,"");
		 				// 			$("#consignee_state").attr("data-value","");
		 				// 			$("#consignee_state").val("");
		 				// 		}
						}
					});
					that.state = states;
				}	
				
			/**
				$("#state").empty();
				var options = $("#state")[0].options;
				options.add(new Option('',''));
				$.each(states,function(i){
					var option = new Option(states[i].pro_name,states[i].pro_id); 
					if(selectedId == states[i].pro_id)
					{
						option.selected=true;
					}
					options.add(option);
				});  **/
			});
		},					
		blurContact:function(){
			console.log('blurContact');
		},
		nextStep:function(){
			var that = this;
			$( '#tab_2' ).click();
			// 校验发件人信息
			that.validateResult();
		},
		valiSingle: function ( e ) {
			var method = $( e.target ).attr( 'data-vr' );
			// console.log(method);
			// this[ method ]( e );
		},
		valiShipperName:function(){
			var that = this;
			that.isEmpty("#shipper_name");

		},
		valiShipperTel:function(){
			var that = this;
			var result = that.isEmpty("#shipper_tel");
			// if (result) {
			// 	that.isValid("#shipper_tel","tel");
			// }
		},
		valiShipperAdd1: function () {
			this.isEmpty( '#shipper_address_1' );
		},
		isValid:function(sel,type){
			var that = this;
			if (type=='tel') {
				var str = $(sel).val();
				if (that.checkMobile(str) || that.checkPhone(str)) {
					$(sel).parent().removeClass('has-error');

					$(sel).parent().parent().find('.error').remove();
				} else {
					if(!$(sel).parent().hasClass("has-error")){
						$(sel).parent().addClass("has-error");
						var ele = '<div class="col-sm-2 error show">'
							+'<span class="error"></span>'
							+'<span class="invalidTel">Invalid telephone!</span>'
							+'</div>';
						$(sel).parent().after(ele);
					}
					return false;
				}
			}
		},
		// 验证手机号码
		checkMobile: function( str ) {
			var re = /^1\d{10}$/;
			if ( re.test( str ) ) {
				return true;
			} else {
				return false;
			}
		},
		// 验证电话号码
		checkPhone: function( str ) {
			var re = /^0\d{2,3}-?\d{7,8}$/;
			if ( re.test( str ) ) {
				return true;
			} else{
				return false;
			}
		},
		isEmpty:function(obj){
			if($(obj).val()=="") {
				if(!$(obj).parent().hasClass("has-error")){
					$(obj).parent().addClass("has-error");
					var ele = '<div class="col-sm-4 error show">'
						+'<span class="error"></span>'
						+'</div>';
					$(obj).parent().after(ele);
				}
				
				return false;
			}else{
				$(obj).parent().removeClass('has-error');

				$(obj).parent().parent().find('.error').remove();

				return true;
			}
		},
		validate: function () {
			this.valiShipperName();	// 校验发件人名字
			this.valiShipperTel();	// 校验发件人电话
			this.valiShipperAdd1();	// 校验发件人地址

			return this.validateResult();
		},
		validateResult: function () {
			if( $( '#shipper .has-error' ).length > 0  ) {
				$( '#myTab li:eq(0)' ).find( '.check' ).removeClass( 'checked' ).addClass( 'checkerror' );

				return false;
			} else {
				$( '#myTab li:eq(0)' ).find( '.check' ).removeClass( 'checkerror' ).addClass( 'checked' );
				
				return true;
			}
		},
		removeEvent:function(obj, type, fn){
			  if (obj.removeEventListener){
			  		
			      obj.removeEventListener( type, fn, false );
			  }else if (obj.detachEvent) {
			      obj.detachEvent(  "on" +type, obj["e"+type+fn] );
			      obj["e"+type+fn] = null;
			  }

		}

	});
});