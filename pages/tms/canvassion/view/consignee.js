"use strict";
define( [
		"jquery", 
		"backbone", 
		'handlebars', 
		"../templates/templates.amd",
		
		"bootstrap.datetimepicker",
		"../config",
		"immybox",
		"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
		"require_css!bootstrap.datetimepicker-css"],
function( 
		$, 
		Backbone, 
		HandleBars, 
		templates, 
		datetimepicker,
		config,
		immybox,
		AsynLoadQueryTree ) {
	var canvassionI18n = window.parent.regional['defaultLanguage'];
	return Backbone.View.extend({
		el: null,
		template: templates.consignee,
		consigneetree: null,
		render: function( consigneeInfo ) {
			var that = this;
			if( consigneeInfo ) {
				that.$el.html( that.template( {
					consigneeInfo: consigneeInfo
				} ) );

			  	// 初始化收件仓库
			    var consigneetree = new AsynLoadQueryTree( {
		            renderTo: "#consignee_whs_id",
		            Pleaseselect: "请选择",
		            //异步时，value是服务端写好的面包屑路径
		            //非异步是直接id--selectid:11110
		            multiselect: false,
		            Parentclick: true,
		            selectid:consigneeInfo.CONSIGNEE_WHS_ID,
		            dataUrl: "/Sync10/_tms/tmsWayItem/WHSList",
		            scrollH:400
	          	} );
	            consigneetree.render(); 
	            that.consigneetree=consigneetree;


				// $( "#consignee_country" ).val( consigneeInfo.CONSIGNEE_COUNTRY );
				// that.countryChanged( consigneeInfo.PID );
				// that.provinceChanged( consigneeInfo.CONSIGNEE_AREACODE );
				that.initialPosttype(consigneeInfo);	            
	

			} else {
				that.$el.html( that.template( ) );
				// var consigneetree;

				// $.getJSON(url,function(result){

				// 	console.log(result);
				//   	初始化收件仓库
				//      consigneetree= new AsynLoadQueryTree( {
			 //            renderTo: "#consignee_whs_id",
			 //            Pleaseselect: "请选择",
			 //            Async:false,
			 //            // 异步时，value是服务端写好的面包屑路径
			 //            // 非异步是直接id--selectid:11110
			 //            multiselect: false,
			 //            Parentclick: true,
			 //            //selectid:{value:"1189716"},
			 //            dataUrl: result,
			 //            scrollH:400
		  //         	} );

				//      consigneetree.render();  
				// });

				








				

	            // consigneetree.on("Initialize",function(e){

	            // 		//consigneetree.render();

	            //  consigneetree.reselect_id("1000002",function(){
             //      console.log(22334);
             //    });

	            // });	 

	                   	
	         
	           // that.consigneetree=consigneetree;
               // that.consigneetree.reselect_id("1000002",function(){
               //    console.log(22334);
               //  });	            
	            that.initialPosttype(); 	
			}


			$( "#consignee_mabd" ).datetimepicker({
		        format: "yyyy-mm-dd - hh:00:00",
		        autoclose: 1,
		        todayBtn: true,
		        todayHighlight: true,
		        minView: 1,
		        startDate: new Date()
			})


			// $( "#consignee_mabd" ).bind( "change",function() {
			//   	var mabd = $( "#consignee_mabd" ).val();
			//   	console.log( mabd );
			//   	if( mabd != "" && mabd != undefined ) {
			//   		$( "#consignee_eta" ).val( mabd )
			//   	}
			// })

			var mabd = $( "#consignee_mabd" ).val();
		  	if ( mabd.length > 19 ) {
		  		
		  		mabd = mabd.substring( 0, 19 );
		  		$( "#consignee_mabd" ).val( mabd );
		  	}	

			var eta = that.getSpecialDay("2")
			$("#consignee_eta").val(eta);

			var selectedId = "11036";//默认选择的的国家
			/*
			this.initCountry(selectedId);	

			this.initStates(selectedId,"");
			*/
		},
		events: {
			"change #consignee_state": "provinceChanged",
			"change #consignee_posttype": "posttypeChanged",
			"blur #consignee .vr": "valiSingle",
			"click #next_consignee" : "nextStep",
			"click #previous_consignee": "prevStep",
			"change #consignee_area":"valiArea",
			"input #consignee_postcode":'postcodeChange'
		},
		initStorage:function(){
				var url="/Sync10/_tms/tmsWayItem/WHSList";
				
					  	// 初始化收件仓库
		     var  consigneetree= new AsynLoadQueryTree( {
	            renderTo: "#consignee_whs_id",
	            Pleaseselect: "请选择",
	            Async:true,
	            // 异步时，value是服务端写好的面包屑路径
	            // 非异步是直接id--selectid:11110
	            multiselect: false,
	            Parentclick: true,
	            //selectid:{value:"1189716"},
	            dataUrl: url,
	            scrollH:400
          	} );

		     consigneetree.render();  
		},
		postcodeChange:function(){
			var postcode=$('#consignee_postcode').val();
			$("#consignee_whs_id").attr("data-val","");
			$("#consignee_whs_id").attr("data-val","");
			$("#consignee_whs_id").unbind();
			$("#consignee_whs_id").val("");								
		  	// 初始化收件仓库
		    var consigneetree = new AsynLoadQueryTree( {
	            renderTo: "#consignee_whs_id",
	            Pleaseselect: "Please Select",
	            //异步时，value是服务端写好的面包屑路径
	            //非异步是直接id--selectid:11110
	            multiselect: false,
	            Parentclick: true,
	            dataUrl: "/Sync10/_tms/tmsWayItem/WHSList?zipcode="+postcode,
	            scrollH:400
          	} );

            consigneetree.render(); 
		},
		consigneeWhsIdChanged: function() {
			this.isEmpty( "#consignee_whs_id" );
		},
		//验证仓库id不为空
		valiConsigneeWhsId:function(){
			this.isEmpty( "#consignee_whs_id" );
		},
		initialPosttype:function(consigneeInfo){
			var that = this;
			$.ajax({
				url: '/Sync10/_tms/tmsWaybill/sentTypeList',
				dataType: 'json',
				type: 'get',
				success:function( data ){
					if(data&&data.DATA&&data.DATA.length>0){
						var length = data.DATA.length;
						 $( "#consignee_posttype" ).empty();
						 $( "#consignee_posttype" ).append( '<option value="-1">请选择</option>' );   //为 Select 追加一个 Option（下拉项）
						 for (var i = 0; i < length; i++) {

						 		if ( consigneeInfo && consigneeInfo.SEND_TYPE == data.DATA[i].ID ) {
						 			$("#consignee_posttype").append('<option selected value="' + data.DATA[i].ID + '">'+data.DATA[i].TYPE_NAME+'</option>'); 
						 		} else {
						 			//默认选中为普通货物
						 			if (data.DATA[i].ID==1) {
						 				$("#consignee_posttype").append('<option selected value="' + data.DATA[i].ID + '">'+data.DATA[i].TYPE_NAME+'</option>'); 
						 			}else{
						 				$("#consignee_posttype").append('<option value="' + data.DATA[i].ID + '">'+data.DATA[i].TYPE_NAME+'</option>'); 
						 			}
						 			
						 		}
						 		
						 	
						 };
					}
					that.posttypeChanged();


				},
				error:function(){
					
				}
			});				
		},
		prevStep: function() {
			$( '#tab_1' ).click();
			// 校验收件人信息
			this.validateResult();
			// iframe 触发了 unloadHandler，具体的原因需要仔细研究插件的流程，这里使用 return false 处理
			return false;
		},
		nextStep:function(){
			$( '#tab_3' ).click();
			// 校验收件人信息
			this.validateResult();
			return false;
		},
		valiSingle:function(e)
		{
			var method =$(e.target).attr("data-vr");
			this[method](e);
		},
		valiArea:function(){
			var that = this;
			var areaCode = $( "#consignee_area" ).val();
			if (areaCode!=undefined&&areaCode!="-1") {

				$.ajax({
					url: '/Sync10/_tms/tmsWaybill/getWhsByAreaId',
					data: 'AREA_ID=' + areaCode,
					dataType: 'json',
					type: 'get',
					success: function( data ){
						
						if ( data.DATA != null ) {
							// 获取到当前所选地区对应的仓库
							var whs_id = data.DATA.WHS_ID;
							if ( whs_id != undefined && whs_id != '' ) {
								console.log(that.consigneetree);
								// 如何将相应值赋值到元素中		

               // that.consigneetree.reselect_id("1000002",function(){
               //    console.log(22334);
               //  });

           					         					
							}
						} else {
					
						}

					},
					error:function(){
						
					}
				});				
			};			
			that.isSelect("#consignee_area");
		},
		initCountry:function(selectedId)
		{
			var that = this;
			$.ajax({
				url: config.getAllCountry.url,
				type: 'GET',
				dataType: 'json'
			})
			.done(function(countrys) 
			{
				that.country = $("#consignee_country").immybox({
					Pleaseselect:'Clean Select',
      				Defaultselect:selectedId,
					choices: countrys.DATA,
					change:function(e,data)
					{

						var value = data.value ||data.VALUE
 						if(value)
 						{
 							that.initStates(value,"");
 							$("#consignee_state").attr("data-value","");
 							$("#consignee_state").val("");
 						}
 						
					}
				});

				/**
			 	$("#country").empty();
				var options = $("#country")[0].options;
				var selectedId = "11036";
				$.each(countrys,function(i){
					var option = new Option(countrys[i].C_COUNTRY,countrys[i].CCID); 
					if(selectedId == countrys[i].CCID)
					{
						option.selected=true;
					}
					options.add(option);
				});

				that.initStates(selectedId,-1);  **/
			})
			.fail(function(e) {
				console.log(e);
			})
			.always(function() {
				//console.log("complete");
			});
		},	
		initStates:function(countryId,selectedId)
		{	
		   var that = this;
			$.ajax({
				url: config.cityInfo.url,
				type: 'GET',
				dataType: 'json',
				data: {ccid: countryId},
			}).done(function(json) 
			{	
				var rls=[];
				for (var i=0;i<json.length;i++) {
					rls.push({"value":json[i].pro_id,"text":json[i].p_code});
				}
				if(that.state)
				{
					that.state[0].Rechoicesdata(rls,selectedId);
				}
				else
				{	
					var states = $("#consignee_state").immybox({
						Pleaseselect:'Clean Select',
          				Defaultselect:selectedId,
						choices: rls,
						change:function(e,data) {
							var value = data.value ||data.VALUE
							$("#consignee_whs_id").attr("data-val","");
							$("#consignee_whs_id").attr("data-val","");
							$("#consignee_whs_id").unbind();
							$("#consignee_whs_id").val("");								
						  	// 初始化收件仓库
						    var consigneetree = new AsynLoadQueryTree( {
					            renderTo: "#consignee_whs_id",
					            Pleaseselect: "Please Select",
					            //异步时，value是服务端写好的面包屑路径
					            //非异步是直接id--selectid:11110
					            multiselect: false,
					            Parentclick: true,
					            dataUrl: "/Sync10/_tms/tmsWayItem/WHSList?state_id="+value,
					            scrollH:400
				          	} );

				            consigneetree.render(); 							
	 					// 	if(value)
		 				// 		{
		 				// 			that.initStates(value,"");
		 				// 			$("#consignee_state").attr("data-value","");
		 				// 			$("#consignee_state").val("");
		 				// 		}
						}
					});
					that.state = states;
				}	
				
			/**
				$("#state").empty();
				var options = $("#state")[0].options;
				options.add(new Option('',''));
				$.each(states,function(i){
					var option = new Option(states[i].pro_name,states[i].pro_id); 
					if(selectedId == states[i].pro_id)
					{
						option.selected=true;
					}
					options.add(option);
				});  **/
			});
		},			
		isSelect:function( obj,arrs ) {
			if ( $( obj ).val() == "-1" ) {
				if( !$( obj ).parent().hasClass( "has-error" ) ) {
					$( obj ).parent().addClass( "has-error" );
					var ele = '<div class="col-sm-4 error show">'
						+'<span class="error"></span>'
						+'</div>';
					$( obj ).parent().after( ele );
				}

				return false;
			} else {
				$( obj ).parent().removeClass( 'has-error' );

				$( obj ).parent().parent().find( '.error' ).remove();

				return true;
			}

				if ( arrs != undefined ) {
					for ( var i = 0; i < arrs.length; i++ ) {
						
						$( arrs[i] ).empty();
						$( arrs[i] ).append( '<option value="-1">请选择</option>' );
						$( arrs[i] ).parent().removeClass( 'has-error' );

						$( arrs[i] ).parent().parent().find( '.error' ).remove();						 
					}
				}			
		},
		// provinceChanged:function( consignee_areacode ) {
		// 	var that = this;
		// 	var provinceCode = $("#consignee_province").val();
		// 	if(consignee_areacode!=undefined&&consignee_areacode.type == 'change') {
		// 		consignee_areacode = 'undefined';
		// 	}
		// 	if (provinceCode!=undefined&&provinceCode!=""&&provinceCode!="-1") {
		// 		$.ajax({
		// 			url: '/Sync10/_tms/tmsOrder/getSubAreaList',
		// 			data: 'AREA_ID=' + provinceCode,
		// 			dataType: 'json',
		// 			type: 'get',
		// 			success:function( data ){
		// 				if(data&&data.DATA&&data.DATA.length>0){
		// 					var length = data.DATA.length;
		// 					 $("#consignee_area").empty();
		// 					 $("#consignee_area").append('<option value="-1">请选择</option>');   //为Select追加一个Option(下拉项)
		// 					 for (var i = 0; i < length; i++) {
		// 					 	if ( consignee_areacode != undefined && consignee_areacode != "" ) {
		// 						 	if ( data.DATA[i].ID == consignee_areacode ) {
		// 						 		$("#consignee_area").append('<option selected value="'+data.DATA[i].ID+'">'+data.DATA[i].NAME+'</option>'); 
		// 						 	} else {
								 		
		// 						 		$("#consignee_area").append('<option value="'+data.DATA[i].ID+'">'+data.DATA[i].NAME+'</option>'); 
		// 						 	}
		// 					 	} else {

		// 						 	if (i==0) {
		// 						 		$("#consignee_area").append('<option selected value="'+data.DATA[i].ID+'">'+data.DATA[i].NAME+'</option>'); 
		// 						 	} else {
		// 						 		$("#consignee_area").append('<option value="'+data.DATA[i].ID+'">'+data.DATA[i].NAME+'</option>'); 
		// 						 	}
		// 					 	}
		// 					 }
		// 				}

		// 				that.valiArea();
	
		// 			},
		// 			error:function(){
						
		// 			}
		// 		});				
		// 	};		
		// 	// 当省选项为 -1 时，移除市下拉框选项
		// 	that.isSelect( "#consignee_province", ["#consignee_area"] );
		// },
		isEmpty:function(obj){
			if($(obj).val()=="") {
				if(!$(obj).parent().hasClass("has-error")){
					$(obj).parent().addClass("has-error");
					var ele = '<div class="col-sm-4 error show">'
						+ '<span class="error"></span>'
						+ '</div>';
					$(obj).parent().after(ele);
				}

				return false;
			}else{
				$(obj).parent().removeClass('has-error');

				$(obj).parent().parent().find('.error').remove();

				return true;
			}
		},
		validate:function()  {
			this.valiConsigneeName();
			this.isSelect("#consignee_country");
			this.isSelect("#consignee_province");
			this.isSelect("#consignee_area");
			
			

			return this.validateResult();
		},
		validateResult:function(){
			if( $( "#consignee .has-error" ).length > 0  ) {
				$('#myTab li:eq(1)').find(".check").removeClass("checked").addClass('checkerror');
				return false;
			} else {
				$('#myTab li:eq(1)').find(".check").removeClass("checkerror").addClass('checked');
				return true;
			}
		},
		// 验证收件人仓库
		validateConsigneeWhsId: function() {

		},
		// 验证收件人名字
		valiConsigneeName: function() {
			this.isEmpty( "#consignee_name" );
		},
		// 验证联系人电话
		valiConsigneeTel: function() {
			var result = this.isEmpty( "#consignee_tel" );
			if ( result ) {
				// this.isValid( "#consignee_tel","tel" );
			}
		},
		/* 如果选择指定日期，让用户选择*/
		posttypeChanged: function() {
			var that=this;
			var curDate;
			var posttype = $( "#consignee_posttype" ).val();
			if ( posttype == 2 || posttype == 3 || posttype == 4 ) {
				$( "#consignee_mabd" ).parent().parent().removeClass( "eleNone" );
				if ($( "#consignee_posttype" ).val() == 2) {
					curDate = that.getCurrentDate( "6" );
					$('#consignee_mabd').val(curDate);
				// 如果选择四日到，默认填充 MABD 为当前日期 + 4 天
				} else if ( $( "#consignee_posttype" ).val() == 3 ) {
					curDate = that.getSpecialDay( "4" );
					$( '#consignee_mabd' ).val( curDate );
				}
			} else {
				$( "#consignee_mabd" ).parent().parent().addClass( "eleNone" );
			}
		},
		// 验证手机号码
		checkMobile: function( str ) {
			var re = /^1\d{10}$/;
			if ( re.test( str ) ) {
				return true;
			} else {
				return false;
			}
		},
		// 验证电话号码
		checkPhone: function( str ) {
			var re = /^0\d{2,3}-?\d{7,8}$/;
			if ( re.test( str ) ) {
				return true;
			} else{
				return false;
			}
		},		
		// 校验是否有效
		isValid: function( sel,type ) {
			if ( type == 'tel' ) {
				var str = $( sel ).val();
				if ( this.checkMobile( str ) || this.checkPhone( str ) ) {
					$( sel ).parent().removeClass( 'has-error' );

					$( sel ).parent().parent().find( '.error' ).remove();
				} else {
					if( !$( sel ).parent().hasClass( "has-error" ) ) {
						$( sel ).parent().addClass( "has-error" );
						var ele = '<div class="col-sm-4 error show">'
							+'<span class="error"></span>'
							+'<span class="invalidTel">Invalid telephone!</span>'
							+'</div>';
						$( sel ).parent().after( ele );
					}
					return false;
				}
			}
		},
		//获取当前时间
		//ruleDay：在当前时间的日期上要添加的天数
		getSpecialDay:function( ruleDay ) {
			var that = this;
			var fixZero = that.fixZero;
			var date = new Date();

			var year = date.getFullYear();
			var month=date.getMonth() + 1;
			var day = date.getDate();
			var monthDay;
			//预计到达时间的day
			var resultDay;
			var flag = that.isLeapYear( year );
			monthDay = that.getMonthDay( month, flag );
			var etaDay = parseInt(day)+parseInt(ruleDay);
			if (etaDay>monthDay) {
				if (month==12) {
					year=year+1;
					month = month % 12;
					resultDay=etaDay-monthDay;
				} else{
					month= month % 12;
					resultDay=etaDay-monthDay;					
				}
			}else{
				resultDay = etaDay;
			}

			var curDate = year+"-"+fixZero(month)+"-"+fixZero(resultDay)
				+" - "+fixZero(date.getHours())+":"+fixZero(date.getMinutes())+":"+fixZero(date.getSeconds());
			
			return curDate;
		},
		//判断是否是闰年
		isLeapYear:function(iYear){
			if (iYear%4==0&&iYear%100!=0) {
				return true;
			}else{
				if (iYear%400==0) {
					return true;
				}else{
					return false;
				}
			}
		},
		//获取这个月有多少天
		getMonthDay:function(month,isYear){
			if (month==1||month==3||month==5||month==7||month==8||month==10||month==12) {
				return "31";
			} else if(month==4||month==6||month==9||month==11) {
				return "30";
			} else if(month==2){
				if (isYear) {
					return "29";
				} else{
					return "28";
				}
			}
		},
		//如果小于，前面补零
		fixZero:function(parDate){
			if (parDate<10) {
				parDate="0"+parDate;
			}

			return parDate;
		},
		//获取当前日期
		getCurrentDate:function(hour){
			var date = new Date();
			var that = this;
			var fixZero=that.fixZero;
			//如果当前时间超过某个时刻，提示用户今天无法到达（即日到规则）
			return date.getFullYear()+"-"+fixZero(date.getMonth()+1)+"-"+fixZero(date.getDate())
				+" - "+fixZero(hour)+":00:00";
		}


	});
});