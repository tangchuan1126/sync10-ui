define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['advanceSearch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"form-inline\">\n    <div class=\"form-group col-sm-4\">\n        <div class=\"input-group\">\n            <span class=\"input-group-addon\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.routeName_lable : stack1), depth0))
    + "</span>\n            <input id=\"route_name\" type=\"text\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.routeName_placeholder : stack1), depth0))
    + "\" class=\"form-control\"  autocomplete=\"off\" >\n        </div>\n    </div>\n    \n    <div class=\"form-group col-sm-4\" style=\"display:none\">\n        <div class=\"input-group\">\n            <span class=\"input-group-addon\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.currentpsid_lable : stack1), depth0))
    + "</span>\n            <input id=\"currentPsId\" type=\"text\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.currentpsid_placeholder : stack1), depth0))
    + "\" class=\"form-control immybox immybox_witharrow\"  autocomplete=\"off\" >\n        </div>\n    </div>\n    <div class=\"form-group col-sm-4\">\n        <div class=\"input-group\">\n            <span class=\"input-group-addon\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.permitPsId_lable : stack1), depth0))
    + "</span> \n            <input id=\"permitPsId\" type=\"text\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.permitPsId_placeholder : stack1), depth0))
    + "\" class=\"form-control immybox immybox_witharrow\" >\n        </div>\n    </div>\n	\n    \n    <div class=\"form-group col-sm-4\">\n        <div class=\"input-group\">\n            <span class=\"input-group-addon\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.routeType_lable : stack1), depth0))
    + "</span>\n            <input id=\"routeType\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.routeType_placeholder : stack1), depth0))
    + "\" type=\"text\" class=\"form-control\"  autocomplete=\"off\">\n        </div>\n    </div>\n	\n    <div class=\"form-group col-sm-4\">\n        <div class=\"input-group\">\n            <span class=\"input-group-addon\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.status_lable : stack1), depth0))
    + "</span>\n            <input id=\"status\" type=\"text\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.status_placeholder : stack1), depth0))
    + "\" class=\"form-control\">\n        </div>\n    </div>\n	<div class=\"form-group col-sm-4\"></div>\n    <div class=\"col-sm-3\" >\n        <button id=\"btnLoadFilter\" type=\"button\" class=\"btn btn-info\" style=\"width:100px\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.search_btn : stack1), depth0))
    + "</button>\n        <button id=\"btnReset\" type=\"button\" class=\"btn btn-default\" style=\"width:100px\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.reset_btn : stack1), depth0))
    + "</button>\n    </div>\n</div>";
},"useData":true});
templates['branch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel-body form-horizontal\" id=\"routeinfoForm\">\n	<div class=\"form-group\">\n        <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.route_name : stack1), depth0))
    + "</label>\n        <div class=\"col-xs-6\">\n           <input type=\"text\" class=\"form-control\" oldData=\"\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.routeName_placeholder : stack1), depth0))
    + "\" id=\"route_name\"/>\n        </div>\n        <label class=\"col-xs-3 control-label validator_style\" style=\"text-align: left;padding-left: 0px;\"></label> \n    </div>\n	<div class=\"form-group\" style=\"display:none\"> \n        <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.route_store : stack1), depth0))
    + "</label>\n        <div class=\"col-xs-6\">\n           <input type=\"text\" class=\"form-control immybox immybox_witharrow\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.routeStore_placeholder : stack1), depth0))
    + "\" id=\"route_ps_id\"/>\n           <input type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.ID : stack1), depth0))
    + "\" id=\"route_id\">\n           <input type=\"hidden\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.CURRENT_PS_ID : stack1), depth0))
    + "\" id=\"current_ps_id\">\n        </div>\n        <label class=\"col-xs-3 control-label validator_style\" style=\"text-align: left;padding-left: 0px;\"></label> \n    </div>\n    <div class=\"form-group\"> \n      <label for=\"send_state\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.stauts_lable : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n      	<label class=\"radio-inline\">\n  			<input type=\"radio\" name=\"route_status\" id=\"status_1\" checked=\"checked\"  value=\"1\" > "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.enable_lable : stack1), depth0))
    + "\n		</label>\n		<label class=\"radio-inline\">\n  			<input type=\"radio\" name=\"route_status\" id=\"status_0\"   value=\"0\"> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.disable_labe : stack1), depth0))
    + "\n		</label>\n      </div>\n   </div>\n   <div class=\"form-group\">\n      <label for=\"send_city\" class=\"col-xs-3 control-label\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.linetype_lable : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n        <label class=\"radio-inline\">\n  			<input type=\"radio\" name=\"line_type\" id=\"line_type_1\" checked=\"checked\" value=\"1\"> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.forever_lable : stack1), depth0))
    + "\n		</label>\n		<label class=\"radio-inline\">\n  			<input type=\"radio\" name=\"line_type\" id=\"line_type_0\" value=\"0\"> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.provisional_lable : stack1), depth0))
    + "\n		</label>\n      </div>\n   </div>\n   <div class=\"form-group hidenContent\" >\n      <label for=\"send_city\" class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.uesStartTime_lable : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n          <input type=\"text\" class=\"form-control\"  style=\"display:none;\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.uesStartTime_placeholder : stack1), depth0))
    + "\" id=\"use_startTime\"/>\n      </div>\n   </div>\n   <div class=\"form-group hidenContent\">\n      <label for=\"send_city\" class=\"col-xs-3 control-label required\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.uesEndTime_lable : stack1), depth0))
    + "</label>\n      <div class=\"col-xs-6\">\n          <input type=\"text\" class=\"form-control\"  style=\"display:none;\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.uesEndTime_placeholder : stack1), depth0))
    + "\" id=\"use_endTime\" />\n      </div>\n   </div>\n\n</div>\n<div class=\"footer\">\n	<div class=\"opbar\">\n		<button type=\"button\" id=\"route_next\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.next_btn : stack1), depth0))
    + "</button>\n	</div>	\n</div>\n";
},"useData":true});
templates['branchSearch'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression, alias3=helpers.helperMissing;

  return "		<tr class=\"tr_itme_style\">\n			<td width=\"18%\" class=\"td_Route\" valign=\"center\">\n			<fieldset class=\"RouteGreen\">\n					<legend>\n						<span style=\" padding: 0 5px;\" class=\"Sub_route\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.route : depth0)) != null ? stack1.ROUTE_NAME : stack1), depth0))
    + "</span>\n					</legend>\n					<div class=\"Tractor_ulitme\">\n						<ul>\n							<li>\n								<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.ROUTE : stack1)) != null ? stack1.currentPiao : stack1), depth0))
    + "：</span>\n								<span class=\"valstyle\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.route : depth0)) != null ? stack1.WAYBILLCOUNT : stack1), depth0))
    + "票</span>\n								<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.ADVANCESEARCH : stack1)) != null ? stack1.status_lable : stack1), depth0))
    + "：</span>\n								<span class=\"valstyle\">"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias3).call(depth0,"this.route.STATUS==1",{"name":"xif","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span> \n							</li>\n							<li>\n								<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.ROUTE : stack1)) != null ? stack1.currentInventory : stack1), depth0))
    + "：</span>\n								<span class=\"valstyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.route : depth0)) != null ? stack1.INVENTORY : stack1)) != null ? stack1.IDCOUNT : stack1), depth0))
    + "件</span>\n								<span class=\"namestyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.ADVANCESEARCH : stack1)) != null ? stack1.routeType_lable : stack1), depth0))
    + "：</span>\n								<span class=\"valstyle\">"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias3).call(depth0,"this.route.ROUTE_TYPE==1",{"name":"xif","hash":{},"fn":this.program(7, data, 0, blockParams, depths),"inverse":this.program(9, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\n							</li>\n							<li><span class=\"namestyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.ROUTE : stack1)) != null ? stack1.currentWeight : stack1), depth0))
    + "：</span>\n							<span class=\"valstyle\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.route : depth0)) != null ? stack1.INVENTORY : stack1)) != null ? stack1.WEIGHT : stack1), depth0))
    + "吨</span></li>\n						</ul> \n					</div> \n				</fieldset>				\n			</td>\n			<td width=\"65%\" class=\"td_Route\" valign=\"inherit\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.branchs : depth0),{"name":"if","hash":{},"fn":this.program(11, data, 0, blockParams, depths),"inverse":this.program(17, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "			</td>\n			<td width=\"17%\" class=\"td_OPTION\" valign=\"center\">\n				<div class=\"Log_container\">\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.logs : depth0),{"name":"if","hash":{},"fn":this.program(19, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div>\n			</td>\n		</tr>\n		<tr class=\"TFOOTbutton\" data-trid=\""
    + alias2(alias1((depth0 != null ? depth0.trId : depth0), depth0))
    + "\">\n			<td  align=\"right\">\n			<div class=\"buttons-group\">\n					<a href=\"javascript:void(0)\" name=\"editRoute\" class=\"buttons\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.BUTTONS : stack1)) != null ? stack1.editroute_btn : stack1), depth0))
    + "</a>\n					<a href=\"javascript:void(0)\" style=\"display:none\" name=\"deleteRoute\" class=\"buttons\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.BUTTONS : stack1)) != null ? stack1.deleteroute_btn : stack1), depth0))
    + "</a>\n				</div>\n			</td>\n				<td  align=\"right\">\n				</td>\n				<td  align=\"right\">\n				<div class=\"buttons-group\">\n"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || alias3).call(depth0,"this.logs.length>4",{"name":"xif","hash":{},"fn":this.program(26, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</div></td>\n		</tr>\n		";
},"3":function(depth0,helpers,partials,data) {
    return "启用";
},"5":function(depth0,helpers,partials,data) {
    return "停用";
},"7":function(depth0,helpers,partials,data) {
    return "永久";
},"9":function(depth0,helpers,partials,data) {
    return "临时";
},"11":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "				<table class=\"table table-bordered\">\n					<tr>\n						<th style=\"width:20%\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.BRANCH : stack1)) != null ? stack1.listtable_name : stack1), depth0))
    + "</th>\n						<th style=\"width:20%\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.BRANCH : stack1)) != null ? stack1.listtable_waybillcount : stack1), depth0))
    + "</th>\n						<th style=\"width:20%\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.BRANCH : stack1)) != null ? stack1.listtable_idcount : stack1), depth0))
    + "</th>\n						<th style=\"width:20%\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.BRANCH : stack1)) != null ? stack1.listtable_weight : stack1), depth0))
    + "</th>\n						<th style=\"width:20%\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.ROUTE : stack1)) != null ? stack1.stauts_lable : stack1), depth0))
    + "</th>\n					<tr>\n"
    + ((stack1 = (helpers.foreach || (depth0 && depth0.foreach) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.branchs : depth0),{"name":"foreach","hash":{},"fn":this.program(12, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "				</table>\n";
},"12":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "					<tr>\n						<td>"
    + alias2(alias1((depth0 != null ? depth0.BRANCH_NAME : depth0), depth0))
    + "</td>\n						<td>"
    + alias2(alias1((depth0 != null ? depth0.WAYBILLCOUNT : depth0), depth0))
    + "</td>\n						<td>"
    + alias2(alias1((depth0 != null ? depth0.IDCOUNT : depth0), depth0))
    + "</td>\n						<td>"
    + alias2(alias1((depth0 != null ? depth0.WEIGHT : depth0), depth0))
    + "</td>\n						<td>"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.STATUS==1",{"name":"xif","hash":{},"fn":this.program(13, data, 0, blockParams, depths),"inverse":this.program(15, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</td>\n					</tr>\n";
},"13":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depths[4] != null ? depths[4].lanModel : depths[4])) != null ? stack1.ROUTE : stack1)) != null ? stack1.enable_lable : stack1), depth0));
},"15":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depths[4] != null ? depths[4].lanModel : depths[4])) != null ? stack1.ROUTE : stack1)) != null ? stack1.disable_labe : stack1), depth0));
},"17":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "				<div class=\"Tractor_ulitme nodata\"><div style=\"text-align:center\"><b>"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.BRANCH : stack1)) != null ? stack1.nodata : stack1), depth0))
    + "</b></div></div>\n";
},"19":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.foreach || (depth0 && depth0.foreach) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.logs : depth0),{"name":"foreach","hash":{},"fn":this.program(20, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"20":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.$index<4",{"name":"xif","hash":{},"fn":this.program(21, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"21":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "					<ul>\n						<li> \n							<span class=\"opreationName\">"
    + alias2(alias1((depth0 != null ? depth0.OPERATOR_NAME : depth0), depth0))
    + "</span>\n							<span class=\"logvaluestyle \" >"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.OPERATE_TYPE==1",{"name":"xif","hash":{},"fn":this.program(22, data, 0, blockParams, depths),"inverse":this.program(24, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</span>\n						</li>\n						<li>\n							<span class=\"rightstyle opreationTime\">"
    + alias2(alias1((depth0 != null ? depth0.OPERATE_TIME : depth0), depth0))
    + "</span>\n						</li>\n					</ul>\n					<div class=\"box1px\"></div>\n";
},"22":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depths[5] != null ? depths[5].lanModel : depths[5])) != null ? stack1.LOG : stack1)) != null ? stack1.createKey : stack1), depth0));
},"24":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depths[5] != null ? depths[5].lanModel : depths[5])) != null ? stack1.LOG : stack1)) != null ? stack1.updateKey : stack1), depth0));
},"26":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "						<a href=\"javascript:void(0)\" name=\"moreLogs\" class=\"buttons\">"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.BUTTONS : stack1)) != null ? stack1.morelog_btn : stack1), depth0))
    + "</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<table class=\"checkin_box\">\n	<thead>\n		<tr>\n			<th>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.tabtitle : stack1), depth0))
    + "</th>\n			<th>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.tabtitle : stack1), depth0))
    + "</th>\n			<th>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.LOG : stack1)) != null ? stack1.tabtitle : stack1), depth0))
    + "</th>\n		</tr>\n	</thead>\n	<tbody>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "\n	</tbody>\n</table>\n<div class=\"pagebox_right\" style=\"margin-top:10px;height:40px;display: block;\">\n        <ul id=\"pagination\" class=\"clearfix pagebox\"></ul>\n</div>\n<div class=\"topthead\" style=\"left: 1px; display: none;\">\n<table class=\"checkin_box\">\n<thead>\n	<tr>\n		<th>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.tabtitle : stack1), depth0))
    + "</th>\n		<th>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.tabtitle : stack1), depth0))
    + "</th>\n		<th>"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.LOG : stack1)) != null ? stack1.tabtitle : stack1), depth0))
    + "</th>\n	</tr>\n</thead>\n</table>\n</div>";
},"useData":true,"useDepths":true});
templates['destination'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "﻿\n<div class=\"panel-body form-horizontal\" id=\"destinationForm\">\n	<div class=\"form-group\">\n		<input type=\"hidden\" id=\"dest_id_\" value=\"\"/>	\n        <label class=\"col-xs-2 control-label required\" >"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.destination_title : stack1), depth0))
    + "</label>\n        <div class=\"col-xs-4\">\n           <input type=\"text\" class=\"form-control immybox immybox_witharrow\"   placeholder=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.destination_placeholder : stack1), depth0))
    + "\" id=\"permit_ps_id_0\"/>\n        </div>\n        <label for=\"send_psid\"class=\"col-xs-2 control-label required\" >"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.stauts_lable : stack1), depth0))
    + "</label>\n        <div class=\"col-xs-3\">\n      	 <label class=\"radio-inline\">\n  			<input type=\"radio\" name=\"dest_status_0\" id=\"enable_type\" value=\"1\" checked> "
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.enable_lable : stack1), depth0))
    + "\n		</label>\n		<label class=\"radio-inline\">\n  			<input type=\"radio\" name=\"dest_status_0\" id=\"disable_type\" value=\"0\"> "
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.disable_labe : stack1), depth0))
    + "\n		</label>\n      </div>\n      <label class=\"col-xs-1 control-label\" style=\"text-align: left;\">\n      	<span name=\"delDest\" class=\"glyphicon glyphicon-remove-circle\" aria-hidden=\"true\" style=\"cursor: pointer;\" title=\"删除\"></span>\n      </label>\n    </div> \n</div>\n\n<div class=\"footer\">\n	<div class=\"opbar\">\n		<button type=\"button\" id=\"addDest\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.add_btn : stack1), depth0))
    + "</button>	\n		<button type=\"button\" id=\"destination_pre\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.prev_btn : stack1), depth0))
    + "</button>\n		<button type=\"button\" id=\"destination_next\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.view_lable : stack1), depth0))
    + "</button>\n	</div>	\n</div>";
},"useData":true});
templates['logs'] = template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "	          <tr>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.OPERATOR_NAME : depth0), depth0))
    + "</td>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.OPERATE_TIME : depth0), depth0))
    + "</td>\n	                <td>"
    + ((stack1 = (helpers.xif || (depth0 && depth0.xif) || helpers.helperMissing).call(depth0,"this.OPERATE_TYPE==1",{"name":"xif","hash":{},"fn":this.program(2, data, 0, blockParams, depths),"inverse":this.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</td>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.DATA : depth0), depth0))
    + "</td>\n	          </tr>\n";
},"2":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return this.escapeExpression(this.lambda(((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.createKey : stack1), depth0));
},"4":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return this.escapeExpression(this.lambda(((stack1 = (depths[2] != null ? depths[2].lanModel : depths[2])) != null ? stack1.updateKey : stack1), depth0));
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel panel-default dialogLineContainer\" >\n      <table class=\"table  table-striped\">\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operator_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operationtime_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operate_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.remarks_title : stack1), depth0))
    + "</th>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.resultList : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </table>\n</div>\n";
},"useData":true,"useDepths":true});
templates['preview'] = template({"1":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "		    		   <div class=\"form-group\">\n		       			 	<label class=\"col-xs-4 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.uesStartTime_lable : stack1), depth0))
    + "：</label>\n		        			<label class=\"col-xs-6 control-label\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.routeInfo : depth0)) != null ? stack1.USE_STARTTIME : stack1), depth0))
    + "</label> \n		    		   </div>\n		    		   <div class=\"form-group\">\n		       			 	<label class=\"col-xs-4 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.uesEndTime_lable : stack1), depth0))
    + "：</label>\n		        			<label class=\"col-xs-6 control-label\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.routeInfo : depth0)) != null ? stack1.USE_ENDTIME : stack1), depth0))
    + "</label> \n		    		   </div>\n";
},"3":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "			    	<div class=\"form-group\">\n		       			 	<label class=\"col-xs-6 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depths[1] != null ? depths[1].lanModel : depths[1])) != null ? stack1.BRANCH : stack1)) != null ? stack1.destination_title : stack1), depth0))
    + "：</label>\n		        			<label class=\"col-xs-6 control-label\" >"
    + alias2(alias1((depth0 != null ? depth0.BRANCH_NAME : depth0), depth0))
    + "</label> \n		    		</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "﻿<div id=\"formToInfo\" >\n		<div class=\"col-sm-6\"> \n			<div class=\"panel panel-info\">\n				<div class=\"panel-heading\">\n			      "
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.view_route_title : stack1), depth0))
    + "： "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.routeInfo : depth0)) != null ? stack1.ROUTE_NAME : stack1), depth0))
    + "\n			    </div>\n			    <div class=\"panel-body\">\n			   		<div class=\"form-group\">\n       			 		<label class=\"col-xs-6 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.linetype_lable : stack1), depth0))
    + "：</label>\n        				<label class=\"col-xs-6 control-label\" >\n        					"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.routeInfo : depth0)) != null ? stack1.ROUTE_TYPE_NAME : stack1), depth0))
    + "\n        				</label> \n    		   		</div>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.routeInfo : depth0)) != null ? stack1.use_startTime : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "		    		   <div class=\"form-group\">\n		       			 	<label class=\"col-xs-6 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.operatBy : stack1), depth0))
    + "：</label>\n		        			<label class=\"col-xs-6 control-label\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.currentLogin : depth0)) != null ? stack1.employe_name : stack1), depth0))
    + "</label> \n		    		   </div>\n		    		   <div class=\"form-group\">\n		       			 	<label class=\"col-xs-6 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.operatDate : stack1), depth0))
    + "：</label>\n		        			<label class=\"col-xs-6 control-label\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.currentLogin : depth0)) != null ? stack1.opreateTime : stack1), depth0))
    + "</label> \n		    		   </div>\n		    		   <!--<div class=\"form-group\">\n		       			 	<label class=\"col-xs-6 control-label\" style=\"text-align: right;\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.ROUTE : stack1)) != null ? stack1.remark : stack1), depth0))
    + "：</label>\n		        			<label class=\"col-xs-6 control-label\" ></label> \n		    		   </div>-->\n			    </div>\n			</div>\n		</div>\n		<div class=\"col-sm-6\"> \n			<div class=\"panel panel-info\">\n				<div class=\"panel-heading\">\n			      	"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.dest_title : stack1), depth0))
    + "\n			    </div>\n			    <div class=\"panel-body\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.destinationInfo : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "			    </div>\n			</div>\n		</div>	    \n			    \n			    	\n</div>\n<div class=\"opbar\">\n		<button type=\"button\" id=\"preview_pre\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.prev_btn : stack1), depth0))
    + "</button>\n	<button type=\"button\" id=\"save\" class=\"btn btn-info\">"
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.BRANCH : stack1)) != null ? stack1.save_btn : stack1), depth0))
    + "</button>\n</div>";
},"useData":true,"useDepths":true});
return templates;
});