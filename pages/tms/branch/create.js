"use strict";
require(["../../../requirejs_config"] ,function(con){
		require(["jquery",
		    "./config",     
			"view/createBranchView",
			"view/createDestinationView",
			"view/preview",
			"bootstrap","bootstrap.datetimepicker"],
			function($,config,CreateBranchView,CreateDestinationView,Preview){
			
			$(function(){
				$.ajax({
					type: "GET",
					url: config.adminSesion.url,
					dataType: "json",
					timeout: 30000,
				}).done(function(data) {//获得当前登录人
				function getQueryString(name) {
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			    var route_id = getQueryString("route_id");
			    var op = "add";
			    if(route_id != ""  && route_id != undefined && route_id != null){
			    	op = "edit";
			    }
			   
			    var mains="";
			    var iframes=window.top.window.frames["iframe"];
			    if(!iframes || typeof iframes =="undefined"){
			    	mains=window.parent;
			    }else{
			    	mains=window.top.window.frames["iframe"].frames["main"];
			    }
			    var trModel = mains.routemodel;
			    var initParam ={"route_id":route_id,"op":op,routeInfo:trModel,"ps_id":data.ps_id};

				var preview = new Preview(data);
				var createBranchView = new CreateBranchView(initParam);
				var createDestinationView = new CreateDestinationView(initParam);

				var addPrevEvent = function(){
					$("#destination_next").on('click',  function(event) { //点击预览
						var res = createBranchView.validate();
						res = createDestinationView.validate() && res;
						//console.log(res);
						if(res){
							//所有页签校验成功，展示预览页面
							$('#previewLink').css({display: 'block'}).tab('show');
						} else {
							//MsgHelper.showMessage("Please modify the error !","error");
						}
					});
				}
				createBranchView.render();
				createDestinationView.render();
				addPrevEvent();
				

				$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
					//上一步，校验，但是不阻止
					//下一步，当前页签必须校验成功
					var prev = $(e.target).attr("data-value") > $(e.relatedTarget).attr("data-value")?false:true;
					var result = false;
					if(e.relatedTarget.id =="routeinfoLink"){
						result = createBranchView.validate() ;
					}
					else if(e.relatedTarget.id =="destionationinfoLink"){
						result = createDestinationView.validate(); 
					}else{
						result = true;
					}

					if(e.target.id =="previewLink"){
						//preview.render({addressData:addressInfo.getData(),palletsData:palletsInfo.getData()});
						var data = {"routeInfo":createBranchView.getData(),
							"destinationInfo":createDestinationView.getData()
							};
							if(op=="add"){
								data.op = "add";
							}
							if(op == "edit"){
								data.op = "edit";
							}
						preview.render(data);
					}
					else{
						$("#previewLink").css({display: 'none'});
					}
					
					if(!result){
						if(!prev ){
							return e.preventDefault();
						}
					}
					else{
						//...
					}
				});
				
				
			});	
			});
		});
	});