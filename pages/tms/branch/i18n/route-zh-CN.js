;( function( w ) {
	w.regional = {};
	
	w.regional['zh-CN'] = {
			ADVANCESEARCH:{
				title:"高级查询",
				addRoute_btn:"创建路由牌",
				currentpsid_lable:"当前站点",
				routeName_lable:"路由名称",
				routeName_placeholder:"路由名称",
				currentpsid_placeholder:"请选择当前站点",
				permitPsId_lable:"目的网点",
				permitPsId_placeholder:"请选择目的网点",
				routeType_lable:"路由类型",
				routeType_placeholder:"请选择路由类型",
				status_lable:"状态",
				status_placeholder:"请选择路由状态",
				search_btn:"查询",
				reset_btn:"重置",
				all:"全部",
				STATUSKEY:{
					"0":"停用",
					"1":"启用"
				},
				ROUTETYPEKEY:{
					"0":"临时",
					"1":"永久"
				},
				panel_title:"网点路由信息"
			},
			ROUTE:{ //路由信息
				tabtitle:"库存信息",//Route
				currentInventory:"总件数",
				currentPiao:"总票数",
				currentWeight:"总重量",
				nodata:"此站没有目的地信息!",
				route_name:"路由名称",
				routeName_placeholder:"路由名称",
				route_store:"路由站点",
				routeStore_placeholder:"选择路由站点",
				stauts_lable:"状态",
				linetype_lable:"路由类型",
				uesStartTime_placeholder:"选择起始时间",
				uesEndTime_placeholder:"选择结束时间",
				uesStartTime_lable:"生效起始时间",
				uesEndTime_lable:"生效结束时间",
				next_btn:"下一步",
				routeinfo_title:"路由牌信息",
				enable_lable:"启用",
				disable_labe:"停用",
				forever_lable:"永久",
				provisional_lable:"临时",
				view_lable:"预览",
				view_route_title:"路由牌",
				operatBy:"操作人",
				operatDate:"操作时间",
				remark:"备注"
				
			},
			BRANCH:{
				tabtitle:"网点信息",
				destination_title:"目的地",
				warningmsg:"今天此线路没有班车!",
				nodata:"此站没有网点信息!",
				dest_title:"目的地信息",
				prev_btn:"上一步",
				add_btn:"添加目的地",
				destination_placeholder:"选择目的网点",
				save_btn:"保存",
				listtable_name:"名称",
				listtable_waybillcount:"票数",
				listtable_idcount:"件数",
				listtable_weight:"重量(kg)"
			},
			LOG:{
				tabtitle:"操作日志",
				title:"",
				operator_title:"操作人",
				operationtime_title:"操作时间",
				operate_title:"操作类型",
				remarks_title : "备注",
				createKey:"创建",
				updateKey:"修改"
			},
			BUTTONS:{
				editroute_btn:"修改",
				deleteroute_btn:"刪除",
				morelog_btn:"更多日志",
				moreline_btn:"更多路线"
				
			}
			

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )