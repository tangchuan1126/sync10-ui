"use strict";

require([
     "../../../requirejs_config"
], function(){
	var route_language =  './i18n/route-' + window.navigator.languages[0];
    require([route_language,"index"]);
});
