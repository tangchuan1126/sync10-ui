define(["jquery","backbone","../config",
"../templates",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"../model/branchModel",
"jqueryui/datepicker",
"require_css!bootstrap.datetimepicker-css"],
function($,Backbone,config,templates,AsynLoadQueryTree,models) {
	return Backbone.View.extend(
	{
			el:"#destinationinfo",
			template:templates.destination,
			collection:new models.DestCollection,
			initialize:function(options){
				var tmp = this;
				this.initParam = options;
				if(options.op!="add"){
					this.collection=new models.DestCollection(options.routeInfo.toJSON().branchs);
				}
				
			},
			events:{
				"click #destination_pre":"preStep",
				"click #addDest":"addDest",
				"click span[name='delDest']" : "deletDest"
			},
			deletDest:function(evt){
				var length = 1;//最后一条不能删
				if(this.initParam.op=="edit") length=2;
				if($(evt.target).parent().parent().parent().children().length==length) return; 	
				$(evt.target).parent().parent().remove();
			},
			getRandomNum:function(){
				return new Date().getTime();
			},
			addDestModel:function(model) {
				var tmpNode =  $("#destinationForm").children()[$("#destinationForm").children().length-1];
				var name = $(tmpNode).find("input[name^='dest_status_']").attr("name");
				var id = $(tmpNode).find("input[id^='permit_ps_id_']").attr("id");
				if(name==undefined) {
					name="0";
					id="0";
				}else {
					name=name.substring(name.length-1);
					id=id.substring(id.length-1);
					var randomNum =  this.getRandomNum();
					name = randomNum+""+(parseInt(name)+1);
					id = randomNum+""+(parseInt(id)+1);
				}
				var newNode = this.cloneNode.clone(); //添加新行
				var statusDom=newNode.find("input[name^='dest_status_']");//状态dom
				var treeAll=newNode.find("input[id^='permit_ps_id_']");//目的地dom
				var destIds=newNode.find("input[id^='dest_id_']");//
				
				statusDom.attr("name","dest_status_"+name);
				treeAll.attr("id","permit_ps_id_"+id);
				destIds.attr("id","dest_id_"+id);
				treeAll.parent().find(".treecontrolsbox").remove();
				$("#destinationForm").append(newNode);
				
				if(this.initParam.op=="edit"&&model!=null) {
					var destName = "dest_status_"+name;
					if(model.toJSON().STATUS=="1") {
						newNode.find("#enable_type")[0].checked=true;
					} else {
						newNode.find("#disable_type")[0].checked=true;
					}
					var destId = "dest_id_"+id;
					$("input[id^='"+destId+"']").val(model.toJSON().ID);
					$("#permit_ps_id_"+id).val(model.toJSON().BRANCH_NAME);
					$("#permit_ps_id_"+id).attr("data-val",model.toJSON().PERMIT_PS_ID);
					$("#permit_ps_id_"+id).attr("data-name",model.toJSON().BRANCH_NAME);
				}else {
					this.initTreeNode("#permit_ps_id_"+id,"");
				}
			},
			addDest:function(){
				this.addDestModel(null);
			},
			initTreeNode:function(domId,selectedId){
				var currentPs = new AsynLoadQueryTree({
					renderTo: domId,
					//Pleaseselect: "Please select",
					dataUrl:config.storageTree.url,
					scrollH: 400,
					multiselect: false,
					Parentclick:true,
					Async: true,
					selectid:selectedId
				});
				currentPs.render();
			},
			render:function(){
				var destLanguage = window.regional['defaultLanguage'];//界面的国际化
				$("#destinationinfoLink").text(destLanguage.BRANCH.dest_title);
				var that = this;
				that.$el.html(that.template({
					lanModel:destLanguage
				}));
				this.cloneNode =  $("#destinationForm").children(0).clone();
				that.initTreeNode("#permit_ps_id_0","");
				if(that.initParam.op=="edit") {
					$("#destinationForm").children(0).hide();
					if(that.collection.length>0) {
						for(var i=0;i<that.collection.length;i++) {
							that.addDestModel(that.collection.at(i));
						}
						that.loadAllDestTree();
					}
				}
				
			},
			loadAllDestTree:function(){ //加载已经存在的目的地树
				var permit_ps_ids =  $("input[id^='permit_ps_id_']");
				for(var i=1;i<permit_ps_ids.length;i++){
					this.initTreeNode("#"+$(permit_ps_ids[i]).attr("id"),this.collection.at(i-1).toJSON().PERMIT_PS_ID);
				}
			},
			getDataByDom:function(){
				this.collection.reset();
				var permit_ps_ids =  $("input[id^='permit_ps_id_']");
				var permit_status = $("input[name^='dest_status_']:checked");
				var destIds = $("input[id^='dest_id_']");
				var i =0;
				if(this.initParam.op=="edit")
					i=1;
				for(;i<permit_ps_ids.length;i++) {
					this._errorInfo($(permit_ps_ids[i]),this.isEmpty($(permit_ps_ids[i]).attr('data-val')),"请选择目的地站点!");
					this._errorInfo($(permit_status[i]),this.isEmpty($(permit_status[i]).val()),"");
					var destModel = new models.DestModel;
					destModel.set({
						"PERMIT_PS_ID":$(permit_ps_ids[i]).attr('data-val'),
						"DEST_STATUS":$(permit_status[i]).val(),
						"BRANCH_NAME":$(permit_ps_ids[i]).attr('data-name')
					});
					if(destIds[i].value!="") {
						destModel.set("ID",destIds[i].value);
					}
					this.collection.add(destModel);
				}
			}
			,
			validate:function(){
				this.getDataByDom();
				return this.checkResult();
			},
			isEmpty:function(val){
				return val== undefined || val == null || $.trim(val) == "";
			},
			getData:function(){
				return this.collection;
			},
			_errorInfo:function($obj,display,info){
				$obj.parent().removeClass('has-error');
				if(display){
					$obj.parent().addClass('has-error');
					$obj.attr("title",info);
				}
			},
			preStep:function() {
				$('#tabs li:eq(0) a').tab('show') ;
			},checkResult:function(){
				var res = $("#destinationForm").find('.has-error').length > 0 ?false:true
				if(!res){
					$("#destinationinfoLink").parent().find('span').removeClass().addClass('checkerror');
					//MsgHelp.showMessage("Please modify the errors !","error");
				}else{
					$("#destinationinfoLink").parent().find('span').removeClass().addClass('checked');
				}
				return res;
			}


	});

}
);