define(["jquery","bootstrap.datetimepicker","backbone","../config",
"../templates",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"../model/branchModel",
,"../i18n/bootstrap-datetimepicker.zh-CN"],
function($,datetimepicker,Backbone,config,templates,AsynLoadQueryTree,models) {
	
		
	var routeLanguage = window.regional['defaultLanguage'].ROUTE;//界面的国际化
	return Backbone.View.extend({
			el:"#routeinfo",
			template:templates.branch,
			initialize:function(options){	
				this.initParam = options;
				if(options.op!="add")
					this.model=new models.RouteModel(options.routeInfo.toJSON().route);
				else 
					this.model=new models.RouteModel; 
			},
			events:{
				"click #route_next":"nextStep",
				"click input[name='line_type']":"changeType"
			},
			changeType:function(e){
				if(e.target.value=="0") { //显示临时字段
					$(".hidenContent").css("opacity","1");
					$("#use_endTime").show();
					$("#use_startTime").show();
				} else {
					$(".hidenContent").css("opacity","0");
					$("#use_endTime").hide();
					$("#use_startTime").hide();
				}
			},
			render:function() {
				var that = this;
				$("#routeinfoLink").text(routeLanguage.route_name);
				$("#previewLink").text(routeLanguage.view_lable);
				that.$el.html(that.template({
					lanModel:routeLanguage,
					model:that.model.toJSON()
				}));
				
				$("#route_name").blur(function(evt){
					var route_name = $(evt.target).val();
					if(that.isEmpty(route_name)) {
						that._errorInfo($("#route_name"),that.isEmpty(route_name),"请输入路由名称!");
						$("#route_name").parent().next().text("");
						return;
					}
					if(route_name==$(evt.target).attr("oldData")) return;
					var parm ={"routeName":route_name,"ps_id":that.initParam.ps_id};	
					$.ajax({
						url: config.routeUrl.checkRouteNameUrl,
						data:parm,
						success:function(data){
							that._errorInfo($("#route_name"),data,"此路由名称已存在！",true);
						}
					});
				});
					/*var currentPs = new AsynLoadQueryTree({
						renderTo: "#route_ps_id",
						//Pleaseselect: "Please select",
						dataUrl: config.storageTree.url,
						scrollH: 400,
						multiselect: false,
						Parentclick:true,
						Async: true,
						selectid:that.model.toJSON().ROUTE_PS_ID
					});
					currentPs.on("events.Itemclick",function(treeId,treeNode){                   
							var parm ="route_ps_id="+treeNode.data;	
							$.ajax({
								type:'POST',
								url: config.routeUrl.checkRouteUrl,
								data:parm,
								success:function(data){
									that._errorInfo($("#route_ps_id"),data,"此路由站点已存在！",true);
								}
							});	
		             });
					currentPs.render();*/
				
				var option ={
				        format: "mm/dd/yyyy hh",
				        todayBtn:  1,
						autoclose: 1,
						todayHighlight: 1,
						startView: 2,
						minView: 1,
						forceParse: 0,
						language:  'zh-CN'
					};
				$('#use_startTime').datetimepicker(option);
				$('#use_endTime').datetimepicker(option);
				
				if(that.initParam.op=="edit") {
					//$("#route_ps_id").val(that.model.toJSON().ROUTE_PS_NAME);
					//$("#route_ps_id").attr("readonly",true);
					//$("#route_ps_id").attr("data-val",that.model.toJSON().ROUTE_PS_ID);
					$("#route_name").attr("oldData",that.model.toJSON().ROUTE_NAME)
					$("#route_name").attr("readonly",true);
					$("#route_name").val(that.model.toJSON().ROUTE_NAME);
					
					if(that.model.toJSON().STATUS=="1") { //启用
						$("#status_1")[0].checked=true;
					} else {
						$("#status_0")[0].checked=true;
					}
					if(that.model.toJSON().ROUTE_TYPE=="1") {//永久
						$("#line_type_1")[0].checked=true;
					} else {
						$("#line_type_0")[0].checked=true;
						$(".hidenContent").css("opacity","1");
						$("#use_endTime").val(that.model.toJSON().USE_ENDTIME);
						$("#use_startTime").val(that.model.toJSON().USE_STARTTIME);
						$("#use_endTime").show();
						$("#use_startTime").show();
					}
					
				}
			},
			getData:function(){
				var routeModel = new models.RouteModel;
				routeModel.set({
					"ROUTE_PS_ID":$("#route_ps_id").attr('data-val'),
					"ROUTE_PS_NAME":$("#route_ps_id").attr('data-name'),
					"ROUTE_NAME":$("#route_name").val(),
					"USE_STARTTIME":$("#use_startTime").val(),
					"USE_ENDTIME":$("#use_endTime").val(),
					"ROUTE_TYPE":$("input[name='line_type']:checked").val(),
					"ROUTE_TYPE_NAME":$("input[name='line_type']:checked").val()==1?routeLanguage.forever_lable:routeLanguage.provisional_lable,
					"STATUS":$("input[name='route_status']:checked").val(),
					"TYPE":"2"
					});
				if($("#route_id").val()!=""){
					routeModel.set("ID",$("#route_id").val());
					routeModel.set("CURRENT_PS_ID",$("#current_ps_id").val());
				}
				return routeModel;
			},
			validate:function(){
				var data = this.getData(); 
				if(this.isEmpty(data.toJSON().ROUTE_NAME)) {
					this._errorInfo($("#route_name"),this.isEmpty(data.toJSON().ROUTE_NAME),"请输入路由名称!");
					$("#route_name").parent().next().text("");
				}
				/*if(this.isEmpty(data.toJSON().ROUTE_PS_ID)) {
					this._errorInfo($("#route_ps_id"),this.isEmpty(data.toJSON().ROUTE_PS_ID),"请选择路由站点!");
					$("#route_ps_id").parent().next().text("");
				}*/
				
				this._errorInfo($("input[name='line_type']"),this.isEmpty(data.toJSON().ROUTE_TYPE),"");
				if($("input[name='line_type']:checked").val()=="0") { //代表临时
					this._errorInfo($("#use_startTime"),this.isEmpty(data.toJSON().USE_STARTTIME),"请选择开始日期!");
					this._errorInfo($("#use_endTime"),this.isEmpty(data.toJSON().USE_ENDTIME),"请选择结束日期!");
				} else {
					$("#use_startTime").parent().removeClass('has-error');
					$("#use_endTime").parent().removeClass('has-error');
				}
				this._errorInfo($("input[name='route_status']:checked"),this.isEmpty(data.toJSON().STATUS),"");
				return this.checkResult();
			},
			checkResult:function(){
				var res = $("#routeinfoForm").find('.has-error').length > 0 ?false:true
				if(!res){
					$("#routeinfoLink").parent().find('span').removeClass().addClass('checkerror');
				}else{
					$("#routeinfoLink").parent().find('span').removeClass().addClass('checked');
				}
				return res;
			},
			isEmpty:function(val){
				return val== undefined || val == null || $.trim(val) == "";
			},
			_errorInfo:function($obj,display,info,flag){
				if(display){
					$obj.parent().addClass('has-error');
					$obj.attr("title",info);
					if(flag) {
						$obj.parent().next().text(info);
					} 
				}else {
					if(flag)
						$obj.parent().next().text("");
					$obj.parent().removeClass('has-error');
				}
			},
			nextStep:function(){
				$('#tabs li:eq(1) a').tab('show') ;
			},

	});
	

});