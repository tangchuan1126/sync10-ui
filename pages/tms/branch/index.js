/**
 * Created by liyi on 2015.6.8
 */
"use strict";
define([
    "jquery",
    "config",
    "backbone",
    "handlebars",
    "./view/advanceSearchView",
    "./view/branchSearchResultView",
    "art_Dialog/dialog-plus","artDialog",
    "blockui",
    "jqueryui/tabs"
],function(
    $
    ,config
    ,Backbone
    ,Handlebars
    ,AdvanceSearchView
    ,RouteSearchResultView
    ,Dialog
){
	$.ajax({
		type: "GET",
		url: config.adminSesion.url,
		dataType: "json",
		timeout: 30000,
		})
		.done(function(data) {
		    (function(){
		        $.blockUI.defaults = {
		            css: {
		                padding:        '8px',
		                margin:         0,
		                width:          '170px',
		                top:            '45%',
		                left:           '40%',
		                textAlign:      'center',
		                color:          '#000',
		                border:         '3px solid #999999',
		                backgroundColor:'#ffffff',
		                '-webkit-border-radius': '10px',
		                '-moz-border-radius':    '10px',
		                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		                '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		            },
		            //设置遮罩层的样式
		            overlayCSS:  {
		                backgroundColor:'#000',
		                opacity:        '0.3'
		            },
		            baseZ: 99999,
		            centerX: true,
		            centerY: true,
		            fadeOut:  1000,
		            showOverlay: true
		        };
		    })();
		    var language = window.regional['defaultLanguage'].ADVANCESEARCH;
		    $("#panel_title").append(language.panel_title);
		    var routeSearchResultView = new RouteSearchResultView({el:"#main_div"});
		    var psId = data.ps_id;//当前登录人仓库id
		    var advanceSearchView= new AdvanceSearchView({resultView:routeSearchResultView,psId:psId});
		    advanceSearchView.render();
		    routeSearchResultView.render({"ps_id":psId});
		    advanceSearchView.initTree();
		    $("#addRoute").on('click', function () {
				var uri = config.routeUrl.addUrl+"?route_id=";
				var d = Dialog({
						title: "路由信息",
						url:uri,
						fixed: false,
						skin: 'test1',
						width:'770px',
						height:'400px',
						onclose:function(){
							d.remove();
						}
				}).showModal();
				top.dialog = d;
		    });
		    
		    window.eventsMgr = _.extend({}, Backbone.Events);
			//刷新主界面
			window.eventsMgr.on('renderList', function(param) {
				routeSearchResultView.render({"ps_id":psId});
			});
		});
});