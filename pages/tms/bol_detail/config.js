define(
{
   bolDetail: 
    {
        //url:"/Sync10-ui/pages/temp/transport_manage.json"
        url:"/Sync10-ui/pages/bol/data/bol_detail.json"
        
    },

    
    keySearchURL: {
        url:"/Sync10/_b2b/transportOrder/filterDefaultForSend"
    },
    searchDefaultURL:
    {
        url:"/Sync10-ui/pages/temp/transport_manage.json"
         // url:"/Sync10/_b2b/transportOrder/filterDefaultForManage.json"
    },
    filterSelfWork:
    {
        url:"/Sync10-ui/pages/temp/transport_manage.json"
    },
    autoComplete: 
    {
        url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
    },
    storageTree: 
    {
        url:"/Sync10/_b2b/storage/"
    },
    adminTree:
     {
        url:"/Sync10/_b2b/admin/"
    },
    session:
    {
        url:"/Sync10/_b2b/admin/session"
    }
});