define(["jquery","backbone","../../config","../../templates"],
function($,Backbone,config,templates) {
	return Backbone.View.extend(
	{
			el:"#addressInfo",
			template:templates.addressInfo,
			//model:new FilterModel(),
			initialize:function(param)
			{
				var that =this;
				this.bolId = param.bolId;
				this.eventAcrossView = param.eventAcrossView;
				this.eventAcrossView.on('addressInfoUpdate', function() {
					that.render();
				});
			},
			events:
			{
				"click #createBol" : "createBol"
			},
			render:function(data)
			{
				var v = this;
				v.$el.html(v.template(data));
				return this;
			},
			getSelect: function(objlist,language,ul)
			{
				
			}
	});

}
);