define(["jquery","backbone","../../config","../../templates","artDialog","../../../handlebar_ext.js"],
function($,Backbone,config,templates) {
	return Backbone.View.extend(
	{
			el:"#briefInfo",
			template:templates.briefInfo,
			model:new (Backbone.Model.extend({url:config.bolDetail.url}))(),
			initialize:function(param)
			{
				var that =this;
				this.param = param;
				this.param.eventAcrossView.on('briefInfoUpdate', function(data) {
					that.render();
				});
			},
			events:
			{
				"click #edit" : "edit",
				"click #appointment" : "appointment",
				
			},
			render:function(data)
			{
				var v = this;
				var renderData = data;
				if(renderData == null || renderData == undefined)
				{
					//console.log(this.model.url);
					//this.model.url = config.bolDetail.url;
					this.model.fetch({success:function(bolData)
						{
							renderData = bolData.toJSON();
							//console.log(bolData.toJSON());
							v.$el.html(v.template(renderData));
						}
					});
				}
				else
				{
					v.$el.html(v.template(renderData));
				}
				v.param.eventAcrossView.trigger('addressInfoUpdate',renderData);
				return this;
			},
			edit:function()
			{
			   window.close();
				var that =this;
				var uri="/Sync10-ui/pages/bol/bol_edit/index.html?bolId="+this.param.bolId;
				artDialog.open(uri , {id:"edit",title: "EditBOL",width:'950px',height:'550px',_data:"111212", lock: true,opacity: 0.3,fixed: true,
					close:function()
					{
						that.render();
						that.param.eventAcrossView.trigger('addressInfoUpdate');
					}
				});
			},
			getSelect: function(objlist,language,ul)
			{
				
			}
	});

}
);this