define(["jquery","backbone","../../config","../../templates","/Sync10-ui/lib/PalletsControls/Pallets.js","artDialog"],
function($,Backbone,config,templates,pallets) {
	return Backbone.View.extend(
	{
			el:"#detail",
			template:templates.detail,
			//model:new FilterModel(),
			pallets:null,
			dataCollection:null,
			initialize:function(param)
			{
				var that =this;
				this.param = param;
				this.param.eventAcrossView.on('detailUpdate', function() {
					//that.render();
				});
			},
			events:
			{
				"click #addDetail" : "addDetail",
				"click #deleteDetail" : "deleteDetail"
				
			},
			preRender:function()
			{
				var v = this;
				v.$el.html(v.template({
					
				}));

				// var panelBody =$('#palletDetail');
				// panelBody.mCustomScrollbar({
				// 	axis: "y",
				// 	scrollInertia: 200,
				// 	scrollbarPosition: "outside"
				// });
				// console.log(panelBody);
				// panelBody.find(".mCSB_scrollTools").css("right", "10px");
				
			},
			render:function()
			{
				var v = this;
				this.preRender(); 
                 $(function() {
                     var test_val = {
                        renderTo: "#palletDetail",
                        dataUrl: {url:"../data/source.json",formatjson:function(json){
		                    return v.formatToPallets(json);
		                }},
		                tabledataurl:"../data/table.json?{id}",
                        scrollH: 200,//最高200超出会滚动
                        columns:3,//每行的列数，默认流式布局
                        colstyle:"col-md-4" //可以数字也可以col-md-*默认是col-md-3
                     };

                  v.pallets = new pallets(test_val);  
             });                 

			},
			//格式化后台抓取的数据
			formatToPallets:function(json)
			{
				var pallets = new Array();
				$.each(json, function(index, pallet) {
					var obj =
					{
						"id":pallet.PALLET_ID,
						"weight":pallet.WEIGHT,
						"volume":pallet.VOLUME,
						"deadline":pallet.DEADLINE,
						"type":"1",
						"open":"false",
						"titlecheckbox":"",
						"title":[pallet.PALLET_ID,"ShipTo  :  "+pallet.SHIPTO,"\n","Volume  :  "+pallet.VOLUME,"Weight  :  "+pallet.WEIGHT,"DeadLine  :  "+pallet.DEADLINE]
					};
					pallets.push(obj);
				});
				this.dataCollection = pallets;
				this.computerInfo(pallets);
				return pallets;
			},
			computerInfo:function(pallets)
			{
				var volumeTotal=0;
					weightTotal = 0,
					palletsTotal = 0,
					packagesTotal = 0;
				$.each(pallets, function(index, pallet) {
					volumeTotal += parseFloat(pallet.volume);
					weightTotal += parseFloat(pallet.weight);
					if(pallet.type=="1")
					{
						palletsTotal += 1;
					}
					else
					{
						packagesTotal += 1;
					}
				});
				$("#totalVolume").html(volumeTotal+"m3");
				$("#totalWeight").html(weightTotal+"kg");
				$("#totalPallets").html(palletsTotal);
				$("#totalPackages").html(packagesTotal);
			},
			addDetail:function()
			{
				console.log(this.bolId);
				var that =this;
				var uri="/Sync10-ui/pages/bol/bol_choose/index.html?bolId="+this.param.bolId;
				artDialog.data("operation","edit");
				artDialog.open(uri , {id:"edit",sta:{},title: "ChooseBOL",width:'950px',height:'550px', lock: true,opacity: 0.3,fixed: true,
					close:function()
					{
						console.log(artDialog.data("returnData"));
						that.param.eventAcrossView.trigger('briefInfoUpdate',{name:"aa"});
					}
				});
			},
			deleteDetail:function()
			{
				var allData = this.pallets.command("titlecheckboxall");
				//删除操作，重新渲染

				this.render();
				that.eventAcrossView.trigger('briefInfoUpdate');
			}
			
	});

}
);