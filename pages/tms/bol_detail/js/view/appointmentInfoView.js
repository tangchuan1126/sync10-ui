define(["jquery","backbone","../../config","../../templates"],
function($,Backbone,config,templates) {
	return Backbone.View.extend(
	{
			el:"#appointment",
			template:templates.appointmentInfo,
			//model:new FilterModel(),
			initialize:function(param)
			{
				this.bolId = param.bolId;
				this.eventAcrossView =param.eventAcrossView;
			},
			events:
			{
				"click #createBol" : "createBol"
			},
			render:function()
			{
				var v = this;
				v.$el.html(v.template({
					
				}));
				
			},
			getSelect: function(objlist,language,ul)
			{
				
			},
			filter:function()
			{
				//this.resultView.render(this.queryOptions);
				this.resultView.setQuery({
					send_psid: $("#send_psid").attr("data-val"),
					receive_psid: $("#receive_psid").attr("data-val"),
					status: $("#status").val(),
					stock_in_set: $("#stock_in_set").val(),
					create_account_id: $("#create_account_id").attr("data-val"),
					declaration: $("#declaration").val(),
					clearance: $("#clearance").val(),
					invoice: $("#invoice").val(),
					drawback: $("#drawback").val(),
					"dataUrl":config.filterTransportion.url
				});
				//new TransportList(queryOptionsa);
			},
			createBol:function()
			{
				var uri = "/Sync10-ui/pages/bol/bol_add/index.html";
				artDialog.open(uri , {title: "EditBOL["+"transport_id"+"]",width:'500px',height:'550px', lock: true,opacity: 0.3,fixed: true,
				});
			}
	});

}
);