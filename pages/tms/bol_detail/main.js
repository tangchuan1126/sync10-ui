"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
		require(["jquery",
			//"./js/view/ViewProxy",
			"./js/view/briefInfoView",
			"./js/view/addressInfoView",
			"./js/view/appointmentInfoView",
			"./js/view/detailView",
			"bootstrap"],
			function($,BriefInfoView,AddressInfoView,AppointmentInfoView,DetailView)
			{

				function getQueryString(name) {
				    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				    var r = window.location.search.substr(1).match(reg);
				    if (r != null) return unescape(r[2]); return null;
			    }
			 	var param = {
			 		"eventAcrossView": _.extend({}, Backbone.Events),
			 		"bolId":getQueryString("bolId")
			 	};

				 $.ajax({ 
				        url: config.bolDetail.url, //将来改为session信息
				        type: 'GET', 
				        dataType: 'json', 
				        timeout: 5000, 
				        success: function(data){ 
				        	new AddressInfoView(param);
				        	//在briefView页面渲染address页面，没有将address直接交给briefInfoView
							new BriefInfoView(param).render();
				        } ,
				        error:function(e)
				        {
				        	console.log(e);
				        }
				    }); 

				 new AppointmentInfoView(param).render();
				 new DetailView(param).render();
			  	 $('#detailBody').on('show.bs.collapse', function () {
       				 //alert('展开时11111111');
       			  });
       			 $('#appointmentBody').on('show.bs.collapse', function () {
       				// alert('展开时22222');
       			  });

			});
	});