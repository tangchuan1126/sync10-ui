define(
["jquery",
 "backbone",
 "../../config",
"../../templates",
"../../KeyUtil",
"../../../key/OrderOperateTypeKey"
],
function($,Backbone,config,templates,Key,OrderOperateTypeKey)
{
	return Backbone.View.extend({
		template:templates.logs,
		render:function(data)
		{
			var logsLanguage = window.regional['defaultLanguage'].LOGS;//查询界面的国际化
			this.data = data;
			this.getOpreateKey(data);
			$(this.el).html(this.template({model:data,lanModel:logsLanguage}));
			
			
			return this;
		},
		getOpreateKey:function(data)
		{
			_.map(data.LOGS,function(ele, index) {
				data.LOGS[index].OPERATE_NAME= Key.get(OrderOperateTypeKey,ele.OPERATE);
			});
		},
		html:function()
		{
			return this.el;
		}
	})	

});