define(["jquery","backbone","../../config",
"oso.lib/AsynLoadQueryTree/AsynLoadQueryTree",
"../../templates","../../../key/BolStatusKey",
"immybox",
"bootstrap.datetimepicker",
"require_css!bootstrap.datetimepicker-css"
],
function($,Backbone,config,AsynLoadQueryTree,templates,BolStatusKey,immybox) {
	return Backbone.View.extend(
	{
			el:"#advanceSearchA",
			template:templates.advanceSearch,
			//model:new FilterModel(),
			initialize:function(options)
			{
				this.resultView = options.resultView;
				this.param=options;
			},
			events:
			{
				"click #filter" : "filter"
			},
			render:function()
			{
				var searchLanguage = window.regional['defaultLanguage'].ADVANCEDSEARCH;//查询界面的国际化
				var v = this;
				v.$el.html(v.template({
					lanModel:searchLanguage
				}));
				$("#title").append(searchLanguage.title);
				$("#advanceSearchAL").text(searchLanguage.searchtitle);
				$("#commonToolsL").text(searchLanguage.commontitle);
				var option = {
				    	format: 'mm/dd/yyyy',
				    	autoclose:1,
				        minView: 2,
		       			bgiconurl:"",
				        forceParse: 0 
				    };
				$("#mabd_min").datetimepicker(option)
		    		.on('changeDate', function(ev){
			    	var startTime = $('#mabd_min');
		            startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
	            });

				$("#mabd_max").datetimepicker(option)
		    		.on('changeDate', function(ev){
			    	var startTime = $('#mabd_max');
		            startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
	            });
				
				$("#etd_max").datetimepicker(option)
	    				.on('changeDate', function(ev){
	    					var startTime = $('#etd_max');
	    					startTime.datetimepicker('setStartDate', ev.delegateTarget.value);
	    		});
				$("#etd_min").datetimepicker(option)
					.on('changeDate', function(ev){
					var startTime = $('#etd_min');
					startTime.datetimepicker('setEndDate', ev.delegateTarget.value);
				});
				


            	var fromTree = new AsynLoadQueryTree({
				renderTo: "#shipFrom",
				Pleaseselect: "Please select",
				selectid: {value: v.shipFrom},
				PostData: {rootType:"Ship From"},
				dataUrl: config.shipFrom.url,
				scrollH: 400,
				multiselect: false,
				Parentclick:true,
				Async: true
				
				});
				fromTree.render();
				var toTree = new AsynLoadQueryTree({
					renderTo: "#shipTo",
					Pleaseselect: "Please select",
					selectid: {value: v.shipTo},
					PostData: {rootType:"Ship To"},
					dataUrl: config.shipTo.url,
					scrollH: 400,
					multiselect: false,
					Parentclick:true,
					Async: true
					//placeholder:"Ship To"
				});
				toTree.render();
				$('#status').immybox({
					choices: [
						{"text":"All","value":"0"},
						{"text":"Create","value":"1"},
						{"text":"AddDetail","value":"2"},
						{"text":"Picking","value":"3"},
						{"text":"Picked","value":"4"},
						{"text":"Loading","value":"5"},
						{"text":"Loaded","value":"6"},
						{"text":"ShippedPart","value":"7"},
						{"text":"Shipped","value":"8"},
						{"text":"Canceled","value":"9"}
					]
				});
				
			},
			getSelect: function(objlist,language,ul)
			{
				
			},
			filter:function()
			{
				var status = $("#status").attr("data-value");
				if(status =="0")
				{
					status ="";
				}
				this.resultView.setQuery({
				    "so_id":$("#so_id").val(),
					"send_psId": $("#shipFrom").attr("data-val"),
					"receive_psId": $("#shipTo").attr("data-val"),
					"status": status,
					"etd_min": $("#etd_min").val(),
					"etd_max": $("#etd_max").val(),
					"mabd_min": $("#mabd_min").val(),
					"mabd_max": $("#mabd_max").val()
					//"dataUrl":config.searchDefaultURL.url
				});
			}
			
	});

}
);