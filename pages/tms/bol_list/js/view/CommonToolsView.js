"use strict";
define(["jquery","backbone",'handlebars',"../../templates","../../config"],
	function($,Backbone,HandleBars,templates,config,TransportList)
	{
		return Backbone.View.extend(
		{
			el:"#commonTools",
			template:templates.commonTools,
			initialize:function(opts)
			{
				this.resultView = opts.resultView
				
			},
			render:function() {
				var temp = this;
				this.$el.html(this.template({}));
				
				return this;
			},
			events:
			{
				"click #eso_search":"searchLeft"
			},
	
			searchLeft:function(evt)
			{
				var val = $("#search_key").val().replace(/\'/g,'');
				if (val=="")
				{
					alert("你好像忘记填写关键词了？");
				}
				else
				{
					val = "\'"+val.toLowerCase()+"\'";
					$("#search_key").val(val);
					this.resultView.setQuery({"key":val,"search_mode":1,"dataUrl":config.keySearchURL.url});
				}
			}
		});
	}
);