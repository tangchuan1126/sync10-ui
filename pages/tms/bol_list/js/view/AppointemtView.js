define(
["jquery",
 "backbone",
 "../../config",
"../../templates",
"immybox",
"bootstrap.datetimepicker"
],
function($,Backbone,config,templates,immybox)
{
	return Backbone.View.extend({
		template:templates.appointment,
		initialize:function(options)
		{
			this.bol_id = options.bol_id;
		},
		render:function(data)
		{
			var appointmentLanguage = window.regional['defaultLanguage'].APPOINTMENT;//约车界面的国际化
			$(this.el).html(this.template({model:data,lanModel:appointmentLanguage}));
			return this;
		},
		init:function(data)
		{
			$('#appointment_time').datetimepicker({startDate: new Date(), autoclose: 1, minView: 1,forceParse: 0, format:'mm/dd/yyyy hh:00'});
			$.getJSON(config.carrier.url,{action:"getAllFreight",from_ccid:11036, /**to_ccid: data.DELIVER_CCID**/}, function (json) {
				var t = [];
				$.each(json, function(index, obj) {
					obj.text = obj.value + " - "+ obj.text;
				});
				if(data && data.CARRIER_ID)
				{
					$('#carrier').immybox({
					Pleaseselect:'<i>Clean Select</i>',
					textname:"text",
					valuename:"value",
					Defaultselect: data.CARRIER_ID,
					choices: json
					});
				}
				else
				{	
					$('#carrier').immybox({
					Pleaseselect:'<i>Clean Select</i>',
					textname:"text",
					valuename:"value",
					//Defaultselect: data.CARRIERS,
					choices: json
					});
				}	
			});
		},
		validate:function()
		{
			var data = this.getData();
			this._errorInfo($("#appointment_time"),data.appointment_time =='','Please select appointment time!');
			this._errorInfo($("#carrier"),data.carrier_name =='','Please select SCAC');

			if(data.carrier_linkman.replace(/[^\x00-\xff]/g,"  ").length >= 50)
			{
				this._errorInfo($("#contact"),true,'Name is too long !');
			}
			else
			{
				if(data.carrier_linkman == "")
				{
					this._errorInfo($("#contact"),true,'Please input contact !');
				}
				else
				{
					this._errorInfo($("#contact"),false);
				}
			}
				
			if(data.carrier_linkman_tel != "")
			{
				var  pattern =/^([\d-+\s]*)$/;
				if(pattern.test(data.carrier_linkman_tel))
				{
					this._errorInfo($("#phone"),false);
				}
				else
				{
					this._errorInfo($("#phone"),true,"Please input a right phone number !");
				}
			}
			else
			{
				this._errorInfo($("#phone"),true,"Please input  phone number !");
			}
			
		},
		validateResult:function()
		{
			return $("#appointmentContainer").find('.has-error').length < 1 ;
		},
		_errorInfo:function($obj,display,title)
		{
			$obj.parent().removeClass('has-error');
			if(display)
			{
				$obj.parent().addClass('has-error');
			}
			if(title)
			{
				$obj.parent().attr("title",title);
			}
		},
		getData:function()
		{
			return {
			    "bol_id":this.bol_id,
				"appointment_time":$.trim($("#appointment_time").val()),
				"carrier_id":$("#carrier").attr("data-value"),
				"carrier_name":$("#carrier").val(),
				"carrier_linkman":$("#contact").val(),
				"carrier_linkman_tel":$("#phone").val(),
				"driver_license":$("#driver_license").val(),
				"driver_name":$("#driver_name").val(),
				"licenseplate":$("#licenseplate").val()
			};
		},
		html:function()
		{
			return this.el;
		}
	})	

});