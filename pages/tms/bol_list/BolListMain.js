"use strict";
define(
	["jquery","bootstrap","metisMenu","CompondList/View","./view_config",
	"./jsontohtml_templates","./config",
	"/Sync10-ui/lib/slidePanel/js/slidePanel.js",
	"./js/view/LogsView",
	"./js/view/AppointemtView",
	"art_Dialog/dialog-plus","artDialog",
	"require_css!oso.lib/slidePanel/css/style.css"
	],
	function($,bootstrap,metisMenu,CompondList,view_config,Template,config,SlidePanel,LogsView,AppointemtView,Dialog)
	{
		return  function(options)
		{
			var language = window.regional['defaultLanguage'];//国际化文件
			var list  = new CompondList(view_config, {
	  				renderTo: options.el,
	    		 	 dataUrl: options.url,
	    		 	 pageSize:9,
	    		 	 PostData: (typeof options.queryCondition =="object" )?options.queryCondition:{}
	    		 	 //dataUrl: "/Sync10-ui/pages/temp/CommondList.action",
	  				  });
					 //绑定事件

		   	list.render();   
			list.on("events.EditBOL",function(e)
			{
				//var uri = "/Sync10-ui/pages/bol/bol_add/index.html";
				var bolId = e.data.linedata.BOL_ID;
				//var uri = "/Sync10-ui/pages/tms/bol_edit/index.html?bol_id="+bolId;
				var uri = config.editBol.url+"?bol_id="+bolId;
				var d = Dialog({
						title: language.LOAD.title+bolId,
						url:uri,
						fixed: false,
						skin: 'test1',
						width:'770px',
						height:'480px',
						onclose:function()
						{
							if(this.returnValue)
							{
								list.render(); 
							}
							
							d.remove();
							//renderOneRow(list,transport_id);
						}
				}).showModal();

				top.dialog = d;
				/**
				artDialog.open(uri , {title: "EditBOL",width:'770px',height:'480px', lock: true,opacity: 0.3,fixed: true,
					close:function()
					{
						//renderOneRow(list,transport_id);
					}
				}); **/
			});
			var slidePanelCloseBefore = function( param ){
            	list.render();
				
			};
			list.on("events.BOLPlates",function(e)
			{
				var bolId = e.data.linedata.BOL_ID;
				//window.open(config.bolPallets.url+"&bol_id="+bolId);
				var slide = new SlidePanel({
			            title: language.PALLETS.title,
			            url: config.bolPallets.url+"&bol_id="+bolId,
			            duration: 500,
			            topLine: 0,
			            slideLine: "85%",
			            ShutdownCallback : slidePanelCloseBefore
			        });
	             slide.open();
			});

			list.on("events.Delete",function(e)
			{
				var bol_id = e.data.linedata.BOL_ID;
				var d = Dialog({
				content:language.DELETE.msg+bol_id+' ?',
				title: language.DELETE.title,
				fixed: false,
				okValue:language.DELETE.confirm,
				ok:function()
				{
					$.ajax({
						url: '/Sync10/_tms/bol/deleteBol',
						type: 'GET',
						dataType: 'json',
						data: {"bol_id": bol_id},
					})
					.done(function() {
						list.render(); 
					})
					.fail(function() {
						console.log("error");
					});
					d.close().remove();
				},
				cancelValue:language.DELETE.cancel,
				cancel:function(){},
				close:function()
				{
					d.close().remove();
				},
				}).showModal();
			});

		


			//详情
			list.on("BolDetail",function(e)
			{
				var bolId= e.data.BOL_ID;
				//window.open("/Sync10-ui/pages/tms/bol_detail/index.html?bolId="+bolId);
				window.open(config.bolPallets.url+"&bol_id="+bolId);
			});

			//更多日志
			list.on("events.Appointment",function(e)
			{
				var bol_id = e.data.linedata.BOL_ID;
				//$.getJSON(config.appointment.url, {bol_id: bol_id}, function(json, textStatus) 
				//{
					var appointmentData = e.data.linedata.APPOINTMENT;
					if(appointmentData == null)
					{
							appointmentData ={};
					}
					var appointmentView = new AppointemtView({"bol_id":bol_id}).render({"appointment":appointmentData});
					var d = Dialog({
							title: language.APPOINTMENT.title,
							content:appointmentView.el,
							fixed: false,
							skin: 'test',
							onshow:function()
							{
								appointmentView.init(appointmentData);
							},
							okValue:language.APPOINTMENT.confirm,
							width:'450',
							ok:function(){
								appointmentView.validate();
								if(appointmentView.validateResult())
								{
									var data =  appointmentView.getData();
									$.ajax({
										url: config.saveAppointment.url,
										type: 'POST',
										dataType: 'json',
										contentType:"application/json; charset=UTF-8",
										data: JSON.stringify(data),
									})
									.done(function(json) {
										list.render(); 
									})
									.fail(function() {
										console.log("error");
									})
									.always(function() {
										console.log("complete");
									});
									
								}
								else
								{
									return false;
								}
							},
							cancelValue:language.APPOINTMENT.cancel,
							cancel:function()
							{

							},
							close:function()
							{
								appointmentView = null;
								d.close().remove();
							}
					}).showModal();
				//});
			});

			//更多日志
			list.on("events.moreLogs",function(e)
			{
				var bol_id = e.data.BOL_ID;
				var logs = e.data.LOGS;
				var logsView = new LogsView().render({"LOGS":logs});
				var d = Dialog({
						title: language.LOGS.bol_title+bol_id,
						content:logsView.el,
						fixed: false,
						skin: 'test',
						quickClose: true,
						close:function()
						{
							logsView = null;
							d.close().remove();
						}
				}).showModal();

				/**
				artDialog({
					content:logsView.el,
					title: 'Logs:BOL'+bol_id,
					//width:'600px',
					//height:'500px',
					lock:true,
					opacity:0.3,
					close:function()
					{
						logsView = null;
					}
				});  **/

				//var transport_id = e.data.TRANSPORT_ID;
				//var uri ="/Sync10/administrator/transport/transport_logs.html?transport_id="+transport_id;
				///artDialog.open(uri, {title: '日志 转运单号:'+transport_id,width:'970px',height:'500px', lock: true,opacity: 0.3});
			});
	
			
			function renderOneRow(olist,transport_id)
			{
				$.getJSON('/Sync10/_b2b/transportOrder/filterByIdsForManage?ids='+transport_id, {},
				 	function(json, textStatus) 
				 	{
				 		if(textStatus == "success")
				 		{
				 			olist.upSubitmeData({
							upitme:[{Root:"DATA",itmeid:transport_id,itmekey:"TRANSPORT_ID"},
									{Root:"AUTHS",itmeid:transport_id,itmekey:"ID"}],
  							DATA:json.DATA[0],
  							AUTHS:json.AUTHS[0]});
						Template.tbody.updateOneRow(transport_id,json.DATA[0],json.AUTHS[0]);
				 		}
				});
			}
			return list;
		};
});
