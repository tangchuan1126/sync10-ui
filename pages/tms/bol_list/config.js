define(
{
    carrier: {
        url:"/Sync10-ui/pages/b2b/b2b_detail/data/carrier.json"
        //url:"/Sync10/action/administrator/order/b2BOrderJsonAction.action"
    },
    saveAppointment:{
        url:"/Sync10/_tms/appointment/saveAppointment"
    },
    editBol: 
    {
         //url:"/Sync10-ui/pages/bol/bol_add/index.html"
         url:"/Sync10-ui/pages/tms/bol_create_baseso/index.html"
    },
    status:
    {
        url:"/Sync10-ui/pages/tms/key/BolStatusKey.js"
    },
   createBol: 
    {
         //url:"/Sync10-ui/pages/bol/bol_add/index.html"
         url:"/Sync10-ui/pages/tms/bol_create/index.html"
    },
    bolPallets:{
        url:"/Sync10-ui/pages/tms/bol_pallets/editIndex.html?op=0"
    },
    //-------------------------------------------------//
   filterTransportion: 
    {
         url:"/Sync10-ui/pages/bol/bol_list/json/bol.json"
    },
    keySearchURL: {
        url:"/Sync10-ui/pages/bol/bol_list/json/bol.json"
    },
    searchDefaultURL:
    {
       // url:"/Sync10-ui/pages/bol/bol_list/json/bol.json"
        // url:"/Sync10-ui/pages/bol/data/bol_list.json"
        url:"/Sync10/_tms/bol/getBolList"
    },
    filterSelfWork:
    {
        url:"/Sync10-ui/pages/temp/bol.json"
    },
    autoComplete: 
    {
        url:"/Sync10/_b2b/transportOrderIndex/searchAuto"
    },
    storageTree: 
    {
        url:"/Sync10/_b2b/storage/"
    },
    adminTree:
     {
        url:"/Sync10/_b2b/admin/"
    },
    adminSesion: {
        url:"/Sync10/_tms/admin/getLoginAdmin"
    },
    shipFrom: {
        //url: "../so/json/so.shipFrom.json"
        url: "/Sync10/_tms/tmsWayItem/WHSList"
    },
    shipTo: {
    	url: "/Sync10/_tms/tmsWayItem/WHSList"
    },
});