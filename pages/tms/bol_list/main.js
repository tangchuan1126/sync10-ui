"use strict";
require(["../../../requirejs_config","./config"] ,function(con,config)
	{
	var bol_language =  './i18n/bol-' + window.navigator.languages[0];
		require(["jquery", 
			"./js/view/advanceSearchView",
			"./js/view/CommonToolsView",
			"./BolListMain",
			"/Sync10-ui/lib/slidePanel/js/slidePanel.js",
			"artDialog",
			"bootstrap",
			"domready",
			bol_language
			],
			function($,SearchView,GroupView,BOLListView,SlidePanel,artDialog)
			{
				$.ajax({
				type: "GET",
				url: config.adminSesion.url,
				dataType: "json",
				timeout: 30000,
				})
				.done(function(data) {
					var psId = data.ps_id;
					var queryOptions = {el: "#data", url:config.searchDefaultURL.url,queryCondition:{"send_psId":psId,"receive_psId":""}};
					var resultView  = new BOLListView(queryOptions);
					resultView.on("events.Error", function(e)
					{
					$(".loding,.LodingModal").hide();
						alert("Unable to get data from the server, please try again later");
					});
					new SearchView({"self_psId":psId,resultView:resultView}).render();
					var list = new GroupView({"self_psId":psId,resultView:resultView}).render();
					window.eventsMgr = _.extend({}, Backbone.Events);
					//刷新主界面
					window.eventsMgr.on('renderBolList', function(param) {
						resultView.render();
					});
					/*$("#createBol").bind('click', {}, function(event) {
						var uri = config.createBol.url;
						artDialog.open(uri , {title: "Create BOL",width:'950px',height:'550px', lock: true,opacity: 0.3,fixed: true,
							close:function()
							{
								var bod_id = artDialog.data("returnData");
								if(bod_id!= "" && bod_id != undefined && bod_id != null)
								{
									artDialog.data("returnData","");
									window.open("/Sync10-ui/pages/tms/bol_pallets/index.html?bol_id="+bod_id,"addBolPallets");
								}
							}
						});
					});*/
				})
				.fail(function() {
					console.log("error");
				});
				

				



					//新版dialog
					// $("#createBol").bind('click', {}, function(event) {
					// 	var uri = config.createBol.url;
					// 	Dialog({url:uri,title:"CreateBOL",width:'950px',height:'600px', lock: true,opacity: 0.3,fixed: true
					// 	}).showModal();
					// });

				// var slide = new SlidePanel({
    //             url: config.createBol.url,
    //             title: 'Create BOL',
    //             duration: 500,
    //             topLine: 0,
    //             slideLine:"85%",
    //         	});

				//  $('#createBol').click(function () {
    //             	slide.open();
    //         	});
			});
	});