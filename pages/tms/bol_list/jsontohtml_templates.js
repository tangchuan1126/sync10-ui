define(["JSONTOHTML",
	"DateUtil",
	"./Map",
	"./KeyUtil",
	"../key/BolStatusKey",
	"../key/OrderOperateTypeKey"
	], function(JSONTOHTML,
		DateUtil,
		Map,
		Key,
		BolStatusKey,
		OrderOperateTypeKey
		) {
	var resultLanguage = window.regional['defaultLanguage'].RESULTTITLE;//查询列表国际化
	var appointmentLanguage = window.regional['defaultLanguage'].APPOINTMENT;
	//第一次创建时带上表头
	var _table = {
		thead: [{
			tag: "table",
			class: "checkin_box",
			children: [{
				tag: "thead",
				children: [{
					tag: "tr",
					html: function(obj, index)
					{
						var thdata = obj.children[0].children;
						return (JSONTOHTML.transform(thdata.children, _table.th));
					}
				}]
			}, {
				tag: "tbody",
				html: ""
			}]
		}, {
			tag: "div",
			class: "tfootpage",
			html: "<ul id='pagebox' class='clearfix pagebox'></ul>"
		}, {
			tag: "div",
			class: "SelectMorebox"
		}],
		th: {
			tag: "th",
			style:function(obj,index)
			{
				if(obj.field =="bolInfos")
				{
					return "width:22%";
				}
				else if(obj.field =="addressInfo")
				{
					return "width:28%";
				}else if(obj.field =="appointment")
				{
					return "width:28%";
				}
				else
				{

				}
			},
			html: "${title}"
		}

	};
	
	//表身翻页时也独立用到--可以共享到其他有用到和这个列表一样的地方
	var _tbody = {
		int: function(jsons,View_control) 
		{
			//try
			//{
				var start = new Date().getTime();
				//初始化全局配置
				_tbody.idAttribute = View_control.idAttribute;
				_tbody.authIdAttribute = View_control.authIdAttribute;
				_tbody.headallobj = View_control.head;	
				_tbody.footer = View_control.footer;
				
				var data = new Map(_tbody.idAttribute,jsons.DATA);
				var auths = new Map(_tbody.authIdAttribute,jsons.DATA);
				var pageSize=jsons.PAGECTRL.pageSize;

				//按表头转换成行和列
				var __tr=[];	
				for (var tri = 0; tri < jsons.DATA.length; tri++) 
				{
					var rowData = jsons.DATA[tri];
	                var __td = [];
					for (var headkey in _tbody.headallobj) 
					{
						var column = _tbody.headallobj[headkey];
						var columnFiled = column.field;
						var columnData = eval("({" + columnFiled + ":rowData})");
						__td.push(columnData);
					}

					__tr.push({"rowId":rowData[_tbody.idAttribute],"children": __td});

					if(tri >= pageSize)
					{
						break;
					}
				}

				//拼装tr,trfooter，
				var __rowsHtml = "";
				for (var trkey in __tr) 
				{
					var _row = __tr[trkey];
					var  rowId = _row.rowId;
					__rowsHtml += (JSONTOHTML.transform(_row, _tbody.tr));
					var trfooterdata = {
						"colspan": _tbody.headallobj.length,
						"children": _tbody.footer,
						"rowId":rowId,
						"rowData":data.get(rowId),
						"auth":auths.get(rowId)
						};
					//将行值传给按钮行，用于判断按钮是否需要显示
					__rowsHtml += (JSONTOHTML.transform(trfooterdata, _tbody.trfooter));

				}
				data = null;
				auths = null;
				var end = new Date().getTime();
				console.log(end-start);

				return __rowsHtml;
			//}catch(e)
			//{

			//	alert("数据解析异常，请联系管理员");
			//}
		},
		keyname: function(obj) 
		{
			//列对象，只有一个key,那就是列名对应的字段
			var key = "";
			for (var rowkey in obj) {
				key = rowkey;
			}
			return key;
		},
		tr: {
			"tag": "tr",
			"id":function(obj)
			{
				return obj.rowId;
			},
			//行对象ID
			"data-itmeid":function(obj){
				return _tbody.idAttribute+"|"+obj.rowId;
			},
			"class": "tr_itme_style",
			"html": function(obj, index) {				
				//chuldren :列数组[]
				return (JSONTOHTML.transform(obj.children, _tbody.td));
			}
		},
		trfooter: {
			tag: "tr",
			"id":function(obj)
			{
				return "foot_"+obj.rowData[_tbody.idAttribute];
			},
			class: "TFOOTbutton",
			html: function(obj, index) {
				var rowData = obj.rowData,
					auth = obj.auth,
					tfootData = _tbody.idAttribute+"|"+obj.rowId,
					trhtmls = "";
				trhtmls = '<td colspan="' + obj.colspan + '" align="right">';
				trhtmls += '<div data-itmeid="'+tfootData+'" class="buttons-group">';
				var ahtml = "";
				for (var akey in obj.children) 
				{
					var itmea = obj.children[akey];
					if(_utils.isHasButton(itmea[0],auth))
					{
						var name = itmea[1];
						ahtml += '<a href="javascript:void(0)" data-eventname="' + itmea[0] + '" class="buttons">' + name+ '</a>';
					}
				}
				trhtmls +=ahtml;
				trhtmls += '</div></td>';
				return trhtmls;
			}
		},
	
		td: {
			tag: "td",
			class: function(obj, index) {

				//单元格样式名
				var classname = "";
				var keynames = _tbody.keyname(obj);
				if("bolInfos" == keynames || "appointment" == keynames)
				{
					classname = "td_" + "TRAILER";
				}
				else
				{
					classname = "td_" + keynames;
				}
				
				return classname;
			},
			valign: function(obj, index) {
				var valignsing = "";
				if (index == 4) {
					valignsing = "center";
				} else {
					valignsing = "inherit";
				}
				return valignsing;
			},
			html: function(obj, index) {

				var retuhtml = "没找到模版";
				//拿key去对应当前列名
				var keynames = _tbody.keyname(obj);

				if (keynames != "" && typeof keynames != "undefined")
				{
					if(typeof keynames == "string")
					{
						if (typeof cellTemplate[keynames] != "undefined" && cellTemplate[keynames] != "") 
						{
							retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
						}
					}
					else if(typeof keynames == "object")
					{
						retuhtml = (JSONTOHTML.transform(obj[keynames], cellTemplate[keynames]));
					}

				}
				return retuhtml;
			}
		},
		updateOneRow:function(parentId,rowData,auths)
		{
			var __td = [];
			for (var headkey in this.headallobj) 
			{
				var column = this.headallobj[headkey];
				var columnFiled = column.field;
				var columnData = eval("({" + columnFiled + ":rowData})");
				__td.push(columnData);
			}
			var trfooterdata = {
					"colspan": this.headallobj.length,
					"children": this.footer,
					"rowId":parentId,
					"rowData":rowData,
					"auth":auths
					};
			$("#foot_"+parentId).remove();
			$("#"+parentId).empty().html(JSONTOHTML.transform(__td,this.td)).after(JSONTOHTML.transform(trfooterdata,this.trfooter));
		}

	}



	var cellTemplate =
	{
		bolInfos:
		{
			tag: "fieldset",
			class: "TRAILER bolInfos",
			children: [
			{
			  tag:"span",
			  class:"fieldset_tfoot ",
			  html:function(obj,index)
			  {
			  	return "<em><i>"+resultLanguage.loadinfo.so_lable+" ："+obj.SO_ID+"</i></em>";
			  }
			},
			{
				tag: 'legend',
				class: "td_legend",
				children:
				[
					{"tag":"span","class":"lightlink buttonallevnts","data-eventname":"BolDetail","html":"L${BOL_ID}"},
					{
						"tag":"span",
						"class":function(obj,index){return /*_utils._stateClass( obj.TRANSPORT_STATUS)*/ "status text-bule"},
						"html":function(obj,index){return Key.get(BolStatusKey,obj.STATUS);}
					},
				]
			}, 
			{
				 tag: "div",
				 class: "Tractor_ulitme",
				 children: 
				 [
					{tag: "ul",children:
					[
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":resultLanguage.loadinfo.shipfrom_lable+" :"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return obj.SHIPPER_WHS_NAME;}}
								]
							}, /**
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":"Ship To :"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return obj.CONSIGNEE_WHS_NAME;}}
								]
							},  **/
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":resultLanguage.loadinfo.specialdelivery_lable+" :"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return 'Air';}}
								]
							}, 
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":resultLanguage.loadinfo.createuser_lable+" :"},
									{"tag":"span","class":"valstyle","html":function(obj,index){return obj.CREATE_ACCOUNT;}}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":resultLanguage.loadinfo.createtime_lable+" :"},
									{"tag":"span","class":"valstyle","html":"${CREATE_TIME}"}
									//{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.CREATE_TIME);}}
								]
							},
							{tag: "li",children:
								[
									{"tag":"span","class":"namestyle","html":resultLanguage.loadinfo.etd_lable+" :"},
									{"tag":"span","class":"valstyle","html":"${ETD}"}
									//{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.ETD);}}
								]
							},{tag: "li","class":"",children:
								[
									{"tag":"span","class":"namestyle ","html":resultLanguage.loadinfo.mabd_lable+" :"},
									{"tag":"span","class":"valstyle","html":"${MABD}"}
									//{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.MABD);}}
								]
							}
							
					]}	
				 ]
			}
			]
		}, 
		palletsInfo: 
		{
				 tag: "div",
				 class: "Log_container",
				 children:
				 [
				 	{tag:"ul",children:
				 	[

				 		{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.shipto_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${CONSIGNEE_WHS_NAME}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.shiptoaddress_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_ADDRESS}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.shiptocity_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_CITY_NAME},${RECEIVE_STATE_NAME},${RECEIVE_COUNTRY_NAME},${RECEIVE_ZIPCODE}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.consignee_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_LINKMAN}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.phone_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_LINKMAN_PHONE}"}
							]
						},
				 		{tag:"div",class:"box1px",html:""},
				 		{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.totalpallets_lable+" :"},
								{"tag":"span","class":"valstyle","html":function(obj,index){return obj.TOTAL_PALLETS+"";}}
							]
						}, 
				 		{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.totalpackages_lable+" :"},
								{"tag":"span","class":"valstyle","html":function(obj,index){return obj.TOTAL_PACKAGES+"";}}
							]
						}, 
				 		{tag:"li",children:
					 		[
					 			{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.totalweight_lable+" :"},
	 							{"tag":"span","class":"valstyle","html":function(obj,index){return obj.TOTAL_WEIGHT;}}
					 		]
				 		}
				 		
				 		//{tag: "li",children:
						//	[
							//	{"tag":"span","class":"namestyle","html":"MinSurplusDays :"},
							//	{"tag":"span","class":"valstyle","html":"3(10)"}
							//]
						//},
						//{tag: "li",class:"text-red",children:
						//	[
						//		{"tag":"span","class":"namestyle","html":"MABD :"},
						//		{"tag":"span","class":" valstyle","html":"2014-11-21"}
						//	]
						//}
				 	]}
				 ]
		},
		addressInfo:{
			tag: "fieldset",
			class: "TRAILER border-green ",
			style:"margin-top:20px;",
			//class: "TRAILER ",
			children: [
			{
				tag: 'legend',
				class: "td_legend",
				children:
				[
					{"tag":"b","class":"legendTitle","html":resultLanguage.consigneeinfo.shipto_lable+" :"},
					{"tag":"b","class":"legendTitle","html":"${CONSIGNEE_WHS_NAME}"}
				]
			}, 
			{
				 tag: "div",
				 class: "Tractor_ulitme",
				 children: 
				 [
					{tag: "ul",children:
					[
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.shiptoaddress_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_ADDRESS}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.shiptocity_lable+" :"},
								{"tag":"span","class":"valstyle longvaluestyle","title":"${RECEIVE_CITY_NAME},${RECEIVE_STATE_NAME},${RECEIVE_COUNTRY_NAME},${RECEIVE_ZIPCODE}","html":"${RECEIVE_CITY_NAME},${RECEIVE_STATE_NAME},${RECEIVE_COUNTRY_NAME},${RECEIVE_ZIPCODE}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.consignee_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_LINKMAN}"}
							]
						},
						{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.phone_lable+" :"},
								{"tag":"span","class":"valstyle","html":"${RECEIVE_LINKMAN_PHONE}"}
							]
						},
						{tag:"div",class:"box1px",html:""},
				 		{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.totalpallets_lable+" :"},
								{"tag":"span","class":"valstyle","html":function(obj,index){return obj.TOTAL_PALLETS+"";}}
							]
						}, 
				 		{tag: "li",children:
							[
								{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.totalpackages_lable+" :"},
								{"tag":"span","class":"valstyle","html":function(obj,index){return obj.TOTAL_PACKAGES+"";}}
							]
						}, 
				 		{tag:"li",children:
					 		[
					 			{"tag":"span","class":"namestyle","html":resultLanguage.consigneeinfo.totalweight_lable+" :"},
	 							{"tag":"span","class":"valstyle","html":function(obj,index){return obj.TOTAL_WEIGHT;}}
					 		]
				 		}
							
					]}	
				 ]
			}
			]
		},
		appointment:
		{
			 tag: "div",
			 class: "Tractor_ulitme appointment",
			 "html":function(obj,index)
			 {
				 if(obj.APPOINTMENT)
				 {
				 	return  JSONTOHTML.transform(obj.APPOINTMENT,cellTemplate._appointmentUI);
				 }
				 else
				 {
				 	return "<div style='text-align:center'><b>"+appointmentLanguage.nodata+"</b></div>";
				 }
			 	
			 }
		},
		_appointmentUI:
		{
			tag: "ul",children:
			[
				{tag: "li",children:
					[
						{"tag":"span","class":"namestyle","html":appointmentLanguage.scac_lable+" :"},
						{"tag":"span","class":"valstyle longvaluestyle","html":"${CARRIER_NAME}","title":"${CARRIER_NAME}"}
						//{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.ETD);}}
					]
				},
				{tag: "li","class":"",children:
					[
						{"tag":"span","class":"namestyle ","html":appointmentLanguage.time_lable+" :"},
						{"tag":"span","class":"valstyle","html":"${APPOINTMENT_TIME}"}
						//{"tag":"span","class":"valstyle","html":function(obj,index){return DateUtil.formatDate(obj.MABD);}}
					]
				},
				{tag: "li","class":"",children:
					[
						{"tag":"span","class":"namestyle ","html":appointmentLanguage.contact_lable+" :"},
						{"tag":"span","class":"valstyle","html":"${CARRIER_LINKMAN}"}
					]
				},
				{tag: "li","class":"",children:
					[
						{"tag":"span","class":"namestyle ","html":appointmentLanguage.phone_lable+" :"},
						{"tag":"span","class":"valstyle","html":"${CARRIER_LINKMAN_TEL}"}
					]
				},
				{tag: "li","class":"",children:
					[
						{"tag":"span","class":"namestyle ","html":appointmentLanguage.driverlicense_lable+" :"},
						{"tag":"span","class":"valstyle","html":"${DRIVER_LICENSE}"}
					]
				},
				{tag: "li","class":"",children:
					[
						{"tag":"span","class":"namestyle ","html":appointmentLanguage.drivername_lable+" :"},
						{"tag":"span","class":"valstyle","html":"${DRIVER_NAME}"}
					]
				},
				{tag: "li","class":"",children:
					[
						{"tag":"span","class":"namestyle ","html":appointmentLanguage.licenseplate_lable+" :"},
						{"tag":"span","class":"valstyle","html":"${LICENSEPLATE}"}
					]
				}
			]
		},
		Logs: 
		{
			tag: "div",
			class: "Log_container",
			html: function(obj, index) 
			{
				var ulsring = "";
				var arryjson = obj.LOGS;
				var box1px = "<div class='box1px'></div>";
				if (arryjson) {
					for (var _key = 0; _key < arryjson.length; _key++)
					{
						if (_key == 3) 
						{
							ulsring = ulsring +'<span class=" buttonallevnts logMore" data-eventname="events.moreLogs"><a class="more">More Logs &gt;&gt;</a></span>';
							break;
						}
						var arryitmes = arryjson[_key];
						var ul = JSONTOHTML.transform(arryitmes,cellTemplate._ulLogs);
						if (ulsring != "" && typeof ulsring != "undefined") 
						{
							ulsring = ulsring + box1px + ul;
						} else {
							ulsring = ul;
						}
					}
				}
				return ulsring;
			}
			
		},
		_ulLogs:
		{
			tag:"ul",
			children:
			[
				{tag:"li",children:[
					{tag:"span",class:"opreationName",html:"${OPERATOR_NAME}"},
					{tag:"span",class:"logvaluestyle spanToA","data-eventname":"events.moreLogs",html: function(obj,index) {
							//return B2BOrderLogTypeKey[obj.B2B_TYPE][DateUtil.getLanguage()]+" ";
							return Key.get(OrderOperateTypeKey,obj.OPERATE);
						}
					}
				]},
				{tag:"li",children:[
					{tag:"span",class:"rightstyle opreationTime",html:"${OPERATE_TIME}"}
					//{tag:"span",class:"",html:"${B2B_CONTENT}"}
				]} 
			]
		},
	}

	var _utils = 
	{
		handleEmpty:function(str,defaultValue)
		{
			if(str == "" || str == undefined || str.length == 0 || str == "null" || str == null)
			{
				return defaultValue;
			}
			else
			{
				return str;
			}
		},
		isHasButton:function(itmea,auth)
		{
			var res = false;
			if(itmea =="events.followup")
			{
				res = auth["BUTTON_FOLLOWUP"];
			}
			else if(itmea =="events.STOP")
			{
				res = auth["BUTTON_STOP"];
			}
			else if(itmea =="events.DECLARATION")
			{
				res = auth["BUTTON_DECLARATION"];
			}
			else if(itmea =="events.CLEARANCE")
			{
				res = auth["BUTTON_CLEARANCE"];
			}
			else if(itmea =="events.STOCK_IN_SET")
			{
				res = auth["BUTTON_STOCK_IN_SET"];
			}
			else if(itmea =="events.CERTIFICATE")
			{
				res = auth["BUTTON_CERTIFICATE"];
			}
			else
			{
				res = false;
			}
			return true;
		},
		_stateClass:function(state)
		{
			//运输流程
			var tempclass="";
			if(state == "2")
			{
				tempclass=" colorbule";
			}
			else if(state =="3")
			{
				tempclass=" colorred";
			}
			else if (state =="5")
			{
				tempclass=" colorgreen";
			}
			else
			{
				tempclass="";
			}
			return tempclass;
		},
		//获取显示颜色
		_flowClass:function(flow,obj)
		{
			var type = flow.TYPE;
			var state = flow.STATUS
			var cls = "fontBold ";
			if(type == "5")
			{
				//运费 流程，判断 STOCK_IN_SET_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.STOCK_IN_SET_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "5")//运费完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "6")
			{
				//进口清关 CLEARANCE_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.CLEARANCE_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//清关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需清关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type =="7")
			{
				//出口报关DECLARATION_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.DECLARATION_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//报关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需报关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "9")
			{
				//单证 CERTIFICATE_RESPONSIBLE
				if(obj.OPERATION_TYPE == obj.CERTIFICATE_RESPONSIBLE)
				{
					//是由本人负责，则根据状态判断颜色
					if(state == "4")//报关完成
					{
						if(flow.SUM_FILE != undefined && parseInt(flow.SUM_FILE) > 0)
						{
							cls = "fontBold colorgreen" ;
						}
						else
						{
							//完成，但是没有上传文件
							cls = "fontBold colorred" ;
						}
					}
					else if(state != "1")
					{
						//未完成 1是无需报关
						cls = "fontBold colorblue" ;
					}
				}
				else
				{
					//不是本方负责，直接显示灰色
					cls = "fontBold fontSilver" ;
				}
			}
			else if(type == "4")
			{
				cls =_utils._stateClass(state);
			}
			return cls;
		}

	}
	
	return {
		tbody: _tbody,
		table: _table
	}

});