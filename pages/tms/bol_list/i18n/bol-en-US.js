;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
		ADVANCEDSEARCH:{ //查询条件
			so_lable : "SO",
			so_placeholder:"SO",
			from_lable:"From",
			from_placeholder:"Select From",
			etd_lable:"ETD",
			etdstart_placeholder:"Start Date",
			etdend_placeholder:"End Date",
			status_lable:"Status",
			status_placeholder:"Select Status",
			shipto_lable:"Ship To",
			shipto_placeholder:"Select Ship To",	
			mabd_label:"MABD",
			mabdstart_placeholder:"Start Date",
			mabdend_placeholder:"End Date",
			search:"Search",
			searchtitle:"Advanced Search",
			commontitle:"Common Tools",
			title:"Load-Transportion"
		},
		LOGS:{ //明细信息
			bol_title:"Load",
			operator_title:"Operator",
			operationtime_title:"Operation Time",
			operate_title:"Operate",
			remarks_title : "Remarks"
		},
		LOAD:{
			title:"Load : "
		},
		PALLETS:{
			title:"Pallets"
		},
		RESULTTITLE:{//表格列表标题
			loadinfo_title:"Load Info",
			consigneeinfo_title:"Consignee Info",
			appointment_title:"Appointment",
			log_title:"Logs",
			pallets_button:"Pallets",
			editload_button:"EditLoad",
			appointment_button:"Appointment",
			delete_button:"Delete",
			loadinfo:{
				shipfrom_lable:"Ship From",
				specialdelivery_lable:"Special Delivery",
				createuser_lable:"CreateUser",
				createtime_lable:"CreateTime",
				etd_lable:"ETD",
				mabd_labe:"MABD",
				so_lable:"SO"
			},
			consigneeinfo:{//收货信息
				shiptoaddress_lable:"Ship-to Address",
				shiptocity_lable:"Ship-to City",
				consignee_lable:"Consignee",
				phone_lable:"Phone",
				totalpallets_lable:"Total Pallets",
				totalpackages_lable:"Total Packages",
				totalweight_lable:"Total Weigh"
			}	
			
		},
		APPOINTMENT:{//约车信息
			title:"Appointment",
			time_lable:"Time",
			time_placeholder:"Select Appointment Time",
			scac_lable:"SCAC",
			scac_placeholder:"Select SCAC",
			contact_lable:"Contact",
			contact_placeholder:"Contact",
			phone_lable:"Phone",
			phone_placeholder:"Contact Phone",
			driverlicense_lable:"Driver License",
			driverlicense_placeholder:"Driver License",
			drivername_lable:"Driver Name",
			drivername_placeholder:"Driver Name",
			licenseplate_lable:"Licenseplate",
			licenseplate_placeholder:"Licenseplate",
			confirm:"Confirm",
			cancel:"Cancel",
			nodata:"No Appointment"
		},
		DELETE:{//删除Load
			title:"info",
			msg:"Do you want to delete Load",
			confirm:"Confirm",
			cancel:"Cancel"
		}

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )