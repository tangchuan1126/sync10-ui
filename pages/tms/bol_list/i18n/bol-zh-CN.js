;( function( w ) {
	w.regional = {};
	w.regional['zh-CN'] = {
		ADVANCEDSEARCH:{ //查询条件
			so_lable : "发货计划单",
			so_placeholder:"请输入发货计划单号",
			from_lable:"发货点",
			from_placeholder:"请选择发货地点",
			etd_lable:"预计到货日期",
			etdstart_placeholder:"请选择开始日期",
			etdend_placeholder:"请选择结束日期",
			status_lable:"状态",
			status_placeholder:"请选择发货状态",
			shipto_lable:"发货目的地",
			shipto_placeholder:"请选择目的地",	
			mabd_label:"必须到达日期",
			mabdstart_placeholder:"请选择开始日期",
			mabdend_placeholder:"请选择结束日期",
			search:"查询",
			searchtitle:"高级查询",
			commontitle:"公共工具",
			title:"装车转运信息"
		},
		RESULTTITLE:{//表格列表标题
			loadinfo_title:"装车信息",
			consigneeinfo_title:"收货人信息",
			appointment_title:"约车信息",
			log_title:"操作日志",
			pallets_button:"托盘单",
			editload_button:"修改装车单",
			appointment_button:"约车",
			delete_button:"删除",
			loadinfo:{//装车信息
				shipfrom_lable:"发货地点",
				specialdelivery_lable:"发货方式",
				createuser_lable:"创建人",
				createtime_lable:"创建时间",
				etd_lable:"预计到达时间",
				mabd_labe:"必须到达时间",
				so_lable:"发货计划"
			},
			consigneeinfo:{//收货信息
				shipto_lable:"发货",
				shiptoaddress_lable:"收货地址",
				shiptocity_lable:"收货城市",
				consignee_lable:"收货人",
				phone_lable:"电话",
				totalpallets_lable:"总托盘数",
				totalpackages_lable:"总包裹数",
				totalweight_lable:"总重量"
			}
			
		},
		LOGS:{ //日志明细信息
			bol_title:"装车单",
			operator_title:"操作人",
			operationtime_title:"操作时间",
			operate_title:"操作类型",
			remarks_title : "备注"
		},
		LOAD:{
			title:'装车单 : '
		},
		PALLETS:{
			title:"托盘单"
		},
		APPOINTMENT:{//约车信息
			title:"约车",
			time_lable:"时间",
			time_placeholder:"请选择约车时间",
			scac_lable:"SCAC",
			scac_placeholder:"Select SCAC",
			contact_lable:"联系说明",
			contact_placeholder:"请输入联系说明",
			phone_lable:"手机",
			phone_placeholder:"请输入手机号",
			driverlicense_lable:"驾驶证",
			driverlicense_placeholder:"请输入驾驶证号",
			drivername_lable:"司机",
			drivername_placeholder:"请输入司机名称",
			licenseplate_lable:"车牌号",
			licenseplate_placeholder:"请输入车牌号",
			confirm:"确认",
			cancel:"取消",
			nodata:"无约车信息"
		},
		DELETE:{//删除Load
			title:"信息",
			msg:"确定要删除这张装车单",
			confirm:"确认",
			cancel:"取消"
		}

	};

	// 设置
	w.regional[ 'defaultLanguage' ] = w.regional[ 'zh-CN' ];

}( window ) )