define(["./jsontohtml_templates"], function(tmp) {
	var languageTableTitle = window.regional['defaultLanguage'].RESULTTITLE;//查询结果表头国际化
	return {
		"View_control": {
			"head": [{
				"title": languageTableTitle.loadinfo_title,
				"field": "bolInfos"
			}, 
			{
				"title": languageTableTitle.consigneeinfo_title,
				"field": "addressInfo"
			},
			{
				"title": languageTableTitle.appointment_title,
				"field": "appointment"
			},
			/**,
			{
				"title": "Pallets",
				"field": "palletsInfo"
			}, 
			**/
			{
				"title": languageTableTitle.log_title,
				"field": "Logs"
			} 
			],
			"meta": {
				"orderNumber.associ": "click",
				"orderNumber.catalog": "tree",
				"orderNumber.printBtn": "button"
			},
			"idAttribute":"BOL_ID",
			"authIdAttribute":"ID",
			"footer": [
				[
					"events.BOLPlates", languageTableTitle.pallets_button
				],
				[
					"events.EditBOL", languageTableTitle.editload_button
				],
				[
					"events.Appointment", languageTableTitle.appointment_button
				],
				[
					"events.Delete", languageTableTitle.delete_button
				]
			],
			"templates": tmp
		}
	}

});