define(['handlebars.runtime'], function(Handlebars) {
  Handlebars = Handlebars["default"];  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['advanceSearch'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"condition\">\n	 <div class=\"row\">\n             <div class=\"col-sm-3\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-80\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.so_lable : stack1), depth0))
    + "</span>\n                     <input type=\"text\" class=\"form-control \" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.so_placeholder : stack1), depth0))
    + "\" id=\"so_id\">\n                </div>\n            </div> \n            <div class=\"col-sm-3\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-80\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.from_lable : stack1), depth0))
    + "</span>\n                     <input type=\"text\" class=\"form-control immybox immybox_witharrow \" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.from_placeholder : stack1), depth0))
    + "\" id=\"shipFrom\">\n                </div>\n            </div>   \n            <div class=\"col-sm-4\"> \n                <div class=\"input-group \">\n                     <span class=\"input-group-addon form-label min-80\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.etd_lable : stack1), depth0))
    + " </span>\n                     <input class=\"form-control date-input\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.etdstart_placeholder : stack1), depth0))
    + "\" id=\"etd_min\">\n                     <span class=\"input-group-addon to\" >~</span>\n                     <input  class=\"form-control date-input\"  id=\"etd_max\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.etdend_placeholder : stack1), depth0))
    + "\">\n                </div>\n            </div>   \n        </div>    \n        <div class=\"row\">\n            <div class=\"col-sm-3\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-80\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.status_lable : stack1), depth0))
    + "</span>\n                     <input id=\"status\" type=\"text\" class=\"form-control min-200\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.status_placeholder : stack1), depth0))
    + "\">\n                </div>\n            </div> \n            <div class=\"col-sm-3\">\n                <div class=\"input-group\">\n                     <span class=\"input-group-addon form-label min-80\" id=\"to\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.shipto_lable : stack1), depth0))
    + "</span>\n                     <input type=\"text\" class=\"form-control immybox immybox_witharrow\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.shipto_placeholder : stack1), depth0))
    + "\" id=\"shipTo\">\n                </div>\n            </div>   \n            <div class=\"col-sm-4\"> \n                <div class=\"input-group \">\n                     <span class=\"input-group-addon form-label min-80\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.mabd_label : stack1), depth0))
    + " </span>\n                     <input class=\"form-control date-input\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.mabdstart_placeholder : stack1), depth0))
    + "\"  id=\"mabd_min\">\n                     <span class=\"input-group-addon to\" >~</span>\n                     <input  class=\"form-control date-input\"  id=\"mabd_max\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.mabdend_placeholder : stack1), depth0))
    + "\">\n                </div>\n            </div>   \n            <div class=\"col-sm-2\">\n                <button type=\"button\" class=\"btn btn-info\" id=\"filter\" style=\"width:100px\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.search : stack1), depth0))
    + "</button>\n            </div>   \n        </div>\n</div>\n";
},"useData":true});
templates['appointment'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "    <div class=\"panel-body form-horizontal\" id=\"appointmentContainer\" >\n        	<div class=\"form-group\">\n                <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.time_lable : stack1), depth0))
    + "</label>\n                <div class=\"col-xs-9\">\n                   <input type=\"text\" class=\"form-control\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.time_placeholder : stack1), depth0))
    + "\" id=\"appointment_time\"  style=\"background-color: #fff !important;cursor: pointer;\" readonly value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.APPOINTMENT_TIME : stack1), depth0))
    + "\"/>\n                </div>  \n          </div>\n          <div class=\"form-group\">\n              <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.scac_lable : stack1), depth0))
    + "</label>\n              <div class=\"col-xs-9\">\n                 <input type=\"text\" class=\"form-control\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.scac_placeholder : stack1), depth0))
    + "\" id=\"carrier\" />\n              </div>  \n          </div>\n          <div class=\"form-group\">\n              <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.contact_lable : stack1), depth0))
    + "</label>\n              <div class=\"col-xs-9\">\n                 <input type=\"text\" class=\"form-control\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.contact_placeholder : stack1), depth0))
    + "\" id=\"contact\" \n                  value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.CARRIER_LINKMAN : stack1), depth0))
    + "\"/>\n              </div>  \n          </div>\n          <div class=\"form-group\">\n              <label for=\"send_psid\"class=\"col-xs-3 control-label required\" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.phone_lable : stack1), depth0))
    + "</label>\n              <div class=\"col-xs-9\">\n                 <input type=\"text\" class=\"form-control\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.phone_placeholder : stack1), depth0))
    + "\" id=\"phone\" \n                 value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.CARRIER_LINKMAN_TEL : stack1), depth0))
    + "\" />\n              </div>  \n          </div>\n           <div class=\"form-group\">\n              <label for=\"send_psid\"class=\"col-xs-3 control-label \" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.driverlicense_lable : stack1), depth0))
    + "</label>\n              <div class=\"col-xs-9\">\n                 <input type=\"text\" class=\"form-control\"  placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.driverlicense_placeholder : stack1), depth0))
    + "\" id=\"driver_license\" value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.DRIVER_LICENSE : stack1), depth0))
    + "\" />\n              </div>  \n          </div>\n           <div class=\"form-group\">\n              <label for=\"send_psid\"class=\"col-xs-3 control-label \" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.drivername_lable : stack1), depth0))
    + "</label>\n              <div class=\"col-xs-9\">\n                 <input type=\"text\" class=\"form-control\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.drivername_placeholder : stack1), depth0))
    + "\" id=\"driver_name\" \n                 value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.DRIVER_NAME : stack1), depth0))
    + "\"  />\n              </div>  \n          </div>\n           <div class=\"form-group\">\n              <label for=\"send_psid\"class=\"col-xs-3 control-label \" >"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.licenseplate_lable : stack1), depth0))
    + "</label>\n              <div class=\"col-xs-9\">\n                 <input type=\"text\" class=\"form-control\"   placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.licenseplate_placeholder : stack1), depth0))
    + "\" id=\"licenseplate\" \n                 value=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.appointment : stack1)) != null ? stack1.LICENSEPLATE : stack1), depth0))
    + "\"  />\n              </div>  \n          </div>\n    </div>\n";
},"useData":true});
templates['commonTools'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"row\">\n	<div class=\"form-group col-md-6\">\n			<div class=\"input-group\">\n			<input id=\"SearchKey\" type=\"text\" class=\"form-control min-200 ui-autocomplete-input\" data-ui-autocomplete=\"\" autocomplete=\"off\">\n			<div class=\"input-group-btn\">\n				<button id=\"eso_search\" class=\"btn btn-info\" type=\"button\" style=\"width:120px\">Go!</button>\n			</div>\n		</div>\n	</div>\n</div>\n";
},"useData":true});
templates['logs'] = template({"1":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "	          <tr>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.OPERATOR_NAME : depth0), depth0))
    + "</td>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.OPERATE_TIME : depth0), depth0))
    + "</td>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.OPERATE_NAME : depth0), depth0))
    + "</td>\n	                <td>"
    + alias2(alias1((depth0 != null ? depth0.REMARKS : depth0), depth0))
    + "</td>\n	          </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"panel panel-default dialogLineContainer\" >\n      <table class=\"table  table-striped\">\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operator_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operationtime_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.operate_title : stack1), depth0))
    + "</th>\n            <th>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.lanModel : depth0)) != null ? stack1.remarks_title : stack1), depth0))
    + "</th>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.model : depth0)) != null ? stack1.LOGS : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </table>\n</div>\n";
},"useData":true});
return templates;
});