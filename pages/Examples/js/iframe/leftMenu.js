require(['/Sync10-ui/requirejs_config.js'], function() {


    require(["jquery", "bootstrap", "/Sync10-ui/lib/Navigation_Search/NavigationMenu.js", "json2htmlEvents", "mCustomScrollbar"], function($, bootstrap, NavigationMenu, jqjson2html) {


        $(function() {

            // var _h = windowofset().h;
            // var _w = windowofset().w;
            var dom = window.parent.document;
            //top.window
            //Menu,参数：容器，资源路径，点击事件,搜索按钮
            /* 2015-05-19 */
            var Menuset = {
                renderTo: "#Compond_navbar",
                dataUrl: "/Sync10/action/loginFunctionMenu.action",
                MiniMenu: true,
                scrollH: $(window).outerHeight() - 88
            };

            var left_meun = new NavigationMenu(Menuset);
            left_meun.render();

            left_meun.on("events.showMenuItem", function(itemData) {

                var main = $(dom).find("frame[name='main']");
                //{itemData.url,itemData.itme}
                main.attr("src", itemData.url);

            });

            left_meun.on("events.menuItemError", function(itemData) {

                window.parent.window.alert(itemData.error);

            });
            

        });

    });


});
