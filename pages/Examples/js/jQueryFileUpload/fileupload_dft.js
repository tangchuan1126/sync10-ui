define(["jquery","/Sync10-ui/lib/createElement.js","FileUpload/jquery.fileupload","FileUpload/jquery.fileupload-ui","underscore"], function($,cretefile) { 

 //创建

  var fileuploadclass={
     //配置
    regObject:{
	all:{reg:/(\.|\/)(\w+)$/i,alertText:"All Type"},
	picture:{reg:/(\.|\/)(gif|jpe?g|png)$/i,alertText:"(gif,jpg,jpeg,png)"},
	office:{reg:/(\.|\/)(doc|docx|xls|pdf)$/i,alertText:"(doc,docx,xls,pdf)"},
	question:{reg:/(\.|\/)(doc|ppt|xls|pdf)$/i,alertText:"(doc,ppt,xls,pdf)"},
	xls:{reg:/(\.|\/)(xls)$/i,alertText:"(xls)"},
	xlsx:{reg:/(\.|\/)(xlsx)$/i,alertText:"(xlsx)"},
	xlsm:{reg:/(\.|\/)(xlsm)$/i,alertText:"(xlsm)"},
	kml:{reg:/(\.|\/)(kml)$/i,alertText:"(kml)"},
	picture_office:{reg:/(\.|\/)(gif|jpe?g|png|doc|docx|xls|xlsx|pdf)$/i,alertText:"(gif,jpg,jpeg,png,doc,docx,xls,xlsx,pdf)"}
    },
    _default:{    	
    	   	erroor:null,
    	   	renderTo:null,
    	   	formposturl:null,
    	   	showfileurl:null,
    	   	uptypeinfoTo:null,
    	   	templateurl:"./fileupload_tlp.html"
      },
     _int:function(obj){

          var _def=fileuploadclass._default;

           _def=_.extend(_def,obj);

           return {render:fileuploadclass.render,on:fileuploadclass.events};

     },
     render:function(){

        fileuploadclass.style();

     },
     events: function(evtnstxt, callback) {

      var _def = fileuploadclass._default;

      switch (evtnstxt) {
        case "events.Error":
          _def.upError = callback;
          break;
        case "events.uploadOK":
          _def.uploadOK = callback;
          break;
      }

    },
     style:function(){
      
      var _this=fileuploadclass;
      
      var pam="/Sync10-ui/bower_components/";

     	var datas=[
     	{tag:"link",id:"bootstrap",rel:"stylesheet",href:pam+"bootstrap/css/bootstrap.min.css"},
        {tag:"link",id:"stylefileup",rel:"stylesheet",href:pam+"jQueryFileUpload/css/style.css"},
       {tag:"link",id:"Gallery",rel:"stylesheet",href:"//blueimp.github.io/Gallery/css/blueimp-gallery.min.css"},
        {tag:"link",id:"fileuploadcss",rel:"stylesheet",href:pam+"jQueryFileUpload/css/jquery.fileupload.css"},
       {tag:"link",id:"fileupload_ui",rel:"stylesheet",href:pam+"jQueryFileUpload/css/jquery.fileupload-ui.css"},
       {lable:"style",id:"reup_style_v0",classtxt:"body .table>tbody>tr>td{vertical-align:middle;line-height: normal;}"}
     	];

     	var cretefileval={dataUrl:datas}
           
         var css = new cretefile(cretefileval);
         css.on("events.fileload",_this.template);
         css.render();

     },
     template:function(){

      var _this=fileuploadclass;
      var _def=_this._default;
      var tlpurl=_def.templateurl;
      var renderTo=$(_def.renderTo);

        renderTo.load(tlpurl,function(){

        	//console.log(222);        	
        	 _this.create();	
        	             
        });


     },
     create:function(){

      var _this=fileuploadclass;
      var _def=_this._default;
      var regObject=_this.regObject;       

	  var limitSize = '8' * 1
	  var reg = 'picture_office';
	  var limitNum = '100' * 1;

	  //模版内的容器
      var renderTo=$(_def.renderTo);

      var formobj=renderTo.find(".fileupload_form");

     // console.log(_def.renderTo);

	 formobj.fileupload({
	  	        url:_def.formposturl,
				formAcceptCharset: 'utf-8',
				acceptFileTypes: regObject[reg]["reg"],
				maxFileSize: limitSize * 1000000,
				maxNumberOfFiles: limitNum
			});

	  //提示上传信息
	  /*
	  $(_def.uptypeinfoTo).html(regObject[reg]["alertText"]);
			
			var fileName = $.trim('');
			if (fileName.length > 0 && _def.showfileurl) {
				//console.log(1111);
				// 要回显出已经上传的文件
				$.ajax({
					url:_def.showfileurl + fileName,
					dataType: 'json',
					success: function(data) {
						if (data && data.length > 0) {

							formobj.fileupload('option', 'done')
								.call(formobj, null, {
									result: data
								});
						}
					}
				})
			}

			*/


     }



    };


return fileuploadclass._int;


});