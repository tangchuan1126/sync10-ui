define(["jquery","JSONTOHTML","ztree","ztree_exhide","underscore"], function($,JSONTOHTML,ztree) {


    var treeclass={
    	   _default:{
    	   	treeError:null,
    	   	Itemclick:null,
    	   	JSONData:null
    	   },
           _int:function(obj){

           var _def=treeclass._default;

           _def=_.extend(_def,obj);

           return {render:treeclass.render,on:treeclass.events};

           },
           Dombox:function(){
             var _def=treeclass._default;
           
             var renderTo=$(_def.renderTo);
             var jsonfomat=[{
             	tag:"div",
             	class:"treecontrolsbox",
             	children:[{
             		tag:"input",
             		type:"text",
             		class:"form-control",
             		placeholder:"Search..."
             	},{
             		tag:"div",
             		class:"ztreebox",
             		children:[{
             			tag:"ul",
                  id:function(){
                    var d = new Date();
                    var ids="tree_"+d.getTime();
                    return ids
                  },
             			class:"ztree"
             		}]
             	}]
             }];
 
             renderTo.html(JSONTOHTML.transform({}, jsonfomat)); 
             treeclass.inputevntes();

           },
           inputevntes:function(){


           var _def=treeclass._default;
           var renderTo=$(_def.renderTo);
           var input=renderTo.find(".form-control");
           
            var ztreebox=renderTo.find(".ztree");
            var idsring=ztreebox.attr("id");
           

           var timeoutsring="";
           input.click(function(){
           	var _this=$(this);
           	var treebox=_this.next(".ztreebox");
           	treebox.show();
           }).on('input',function(e){ 

           var _thisinput=$(this);        

         var treeObj= $.fn.zTree.getZTreeObj(idsring); 
          		
					

					  var _this = $(this);
					  var _val = $.trim(_this.val());
            _val=_val.replace(/\/|\.|<|>/g,"");

              if(timeoutsring!="" && typeof timeoutsring!="undefined"){
                      window.clearTimeout(timeoutsring);
              }

                   if (_val != "" && typeof _val != "undefined") {
                 
                    timeoutsring=setTimeout(function(){
                         treeclass.searchtree(treeObj,_val);
                        _thisinput.focus();
                    },200); 


                   }else{
                    
                    timeoutsring=setTimeout(function(){

                         var nodes_ = treeObj.transformToArray(treeObj.getNodesByParam("isHidden", true));
                         treeObj.showNodes(nodes_);

                           var nodesall =treeObj.transformToArray(treeObj.getNodes());

                         _.map(nodesall,function(allitmeval,allkey){

                            if(allitmeval.children){
                            var subitmelen= allitmeval.children.length;
                             if (subitmelen > 0) {                              
                              treeObj.expandNode(allitmeval, false, null, null);
                              _thisinput.focus();
                             }
                           }

                         });


                    },200); 

                   }

                  return false;

               }).keydown(function(e){
            
             //后退问题
              var vals=$(this).val();
              if (e.keyCode == 8 && vals=="" || typeof vals=="undefined"){
                 window.event.returnValue = false;
                 $(this).focus();
              }else{
                window.event.returnValue=true;
              }

             });

           },
           searchtree:function(treeObj,_val){

            
            //得到搜索后结果
            var nodeList = treeObj.transformToArray(treeObj.getNodesByParamFuzzy("name", _val, null));
           
           
          if(nodeList.length > 0){

           // console.log(JSON.stringify(nodeList));

             //全部隐藏
            var nodesall =treeObj.transformToArray(treeObj.getNodes());
             treeObj.hideNodes(nodesall);
           
               //遍历结果
                 _.map(nodeList,function(nodesdli,subkey){

                     //n上级递归
                     showparentnode(nodesdli);
                    //显示出匹配上的具体itme
                    treeObj.showNode(nodesdli);

                 });
       

          }

          //向上查找父节点递归---显示
          function showparentnode(pnode){
               var pare_node_ = pnode.getParentNode();
               if(pare_node_){

                    if(pare_node_.isHidden){
                      //显示父节点
                      treeObj.showNode(pare_node_);
                      //打开自己父节点
                      treeObj.expandNode(pare_node_, true, null, null); 
                      //查找上级父节点                     
                       var p_nodes=pare_node_.getParentNode();

                       if(p_nodes){

                         showparentnode(pare_node_);

                       }


                      }

               }

          }



           },
           render:function(){

           	var _def=treeclass._default;

            treeclass.Dombox();

          $.getJSON(_def.dataUrl, function(JSONData){

           //存储临时数据
            _def.JSONData=JSONData;
            treeclass.createtree();

           }).error(function(e){
                
              if(treeclass.Itemclick){
              	 treeclass.Itemclick(e);
              }
           	
           });  


           },
           createtree:function(){

            //创建树

           	var _def=treeclass._default;
           	var ztredata=_def.JSONData;
            var renderTo=$(_def.renderTo);
            var ztreebox=renderTo.find(".ztree");
           	var setting = {
		        view: {
		          dblClickExpand: false//,
		          //addDiyDom: addDiyDom
		        },
		        check: {
		          enable: true,
		          nocheckInherit: true
		        },
		        data: {
		          simpleData: {
		            enable: true
		          }
		        },
		        callback: {
		        //  onClick: onClickulr,
		         // onCheck:onSetCheck
		        }
		      };

		      $.fn.zTree.init(ztreebox, setting, ztredata);

           	//console.log(ztredata);


           },
           events:function(functionname,callback){

           var _def=treeclass._default; 

           	switch(functionname){
        	case "events.treeError":
             _def.treeError=callback;
        	break;
        	case "events.Itemclick":
             _def.Itemclick=callback;
        	break;
           }

          }


    }

return treeclass._int;

});