require(['/Sync10-ui/requirejs_config.js', '/Sync10-ui/lib/nprogress/nprogress.js'], function(config, NProgress) {

  require(["require_css!oso.lib/nprogress/nprogress.css"], function() {

    document.body.style.visibility = "visible";
    NProgress.start();


    var cssfileall = [
     "require_css!Font-Awesome",
     "require_css!bootstrap-css/bootstrap.min",
      "require_css!oso.lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css",
      "require_css!/Sync10-ui/pages/Examples/css/VisiStyle.css"
    ];



    require(cssfileall);


    require(["jquery", "bootstrap", "oso.lib/Navigation_Search/NavigationMenu", "json2htmlEvents", "utility", "art_Dialog/dialog-plus", "mCustomScrollbar"], function($, bootstrap, NavigationMenu, jqjson2html, utility, artdialog) {

      $(function() {

        var rightcontainer = $("#rightcontainer");

        var leftmuenswitch = $(".leftmuenswitch");
        var switchbutton = leftmuenswitch.find("span");
        var leftmunebox = $("div[role='navigation']");

        //set mennu and rightbox heigth
        var topOffset = 56;
        var _h = utility.windowofset().h - topOffset;
        var _w = utility.windowofset().w;
        var minutop = 65;
        rightcontainer.css("height", (_h) + "px");


        //end menu and rightbox


        //Menu,参数：容器，资源路径，点击事件,搜索按钮
        var Menuset = {
          renderTo: "#Compond_navbar",
          dataUrl: "./js/Menu.json",
          scrollH: (_h - minutop)
        };

        var left_meun = new NavigationMenu(Menuset);
        left_meun.render();



        left_meun.on("events.showMenuItem", function(itemData) {

          // console.log(itemData.url);
          //{itemData.url,itemData.itme}
          $("#rightcontainer").attr("src", itemData.url);

        });

        left_meun.on("events.menuItemError", function(itemData) {

          console.log(itemData);

          //{itemData.error,itemData.itme}
          var d = new artdialog({
            content: itemData.error,
            icon: 'question',
            lock: true,
            width: 200,
            height: 70,
            title: '提示'
          });
          d.show();

        });

        left_meun.on("Initialize", function() {

          $("#wrapper").show();

        })

        //end leftMenu



        //监听右侧ifrem
        rightcontainer.load(function() {
          ifrmewidth(0);
        });

        //屏幕宽度
        function windwresizes() {

          var windw = utility.windowofset().w;
          //var leftmuneboxw = leftmunebox.width() + 28;
           var leftmuneboxw = leftmunebox.width() + 50;
          var bodyw = windw - leftmuneboxw;
          return bodyw;
        }


        function ifrmewidth(ringwidt) {

          var ifremdom = $("#rightcontainer").contents().find("body");

          var _bodyw = windwresizes();

          ifremdom.css({
            "padding": "15px 0px 0px 0px",
            "width": (_bodyw) + "px",
            "margin": "0px auto"
          });


        }

        //end ifrem

        //放大缩小button
        var pagewrapper = $("#page-wrapper");

        switchbutton.click(function() {

          var leftws = leftmunebox.width();
          var reigthifrem = $("#rightcontainer");

          if (leftmunebox.is(":hidden")) {

            leftmunebox.show();
            pagewrapper.css({
              "margin-left": leftws + "px"
            });
            //var ifrw=_w - (leftws + 29);
            ifrmewidth(0);
            windowresize();

          } else {

            leftmunebox.hide();
            leftmuenswitch.css("margin-left", "0px");
            pagewrapper.css({
              "padding-left": "0px",
              "margin-left": "0px"
            });
            var _ifremdom = reigthifrem.contents();
            var ifrembody = _ifremdom.find("body");

            ifrembody.css({
              "padding": "15px 0px 0px 20px",
              "width": "98%",
              "margin": "0px auto"
            });

          }

          var _contentWindow = reigthifrem[0].contentWindow;
          if (_contentWindow.Parentpush) {
            var Parentpush = _contentWindow.Parentpush;
            if (Parentpush.RestwintopMeun) {
              Parentpush.RestwintopMeun();
            }
          }

          return false;
        });



        windowresize();

        //窗口缩小放大事件
        $(window).bind("resize", function() {

          windowresize();

        });
        //end  window resize 



        function windowresize() {

          var _hload = utility.windowofset().h - topOffset;
          var _wload = utility.windowofset().w;

          if (_wload < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
          } else {
            $('div.navbar-collapse').removeClass('collapse')
          }

          if (_hload < 1) _hload = 1;
          if (_hload > topOffset) {

            rightcontainer.css("height", (_hload) + "px");
            var side_menu = $(".side-menubox");
            side_menu.css("height", (_hload - minutop) + "px");
            $("#page-wrapper").css("min-height", (_hload) + "px");

            var _w = 250;
            if(window.parseInt(leftmunebox.width())> _w)
            _w = leftmunebox.width();

            leftmuenswitch.css({
              "height": _hload + "px",
              "margin-left": _w + "px",
              "width": "10px"
            });
            var toph = (_hload / 2) - 81;
            switchbutton.css("top", toph + "px");
          }

        }


        //一些小例子将来可以删除
        var examples_demo = $("#examples_demo");
        var __lia = examples_demo.find("li");
        __lia.click(function() {
          var _this_a = $(this).find('a');
          rightcontainer.attr("src", _this_a.attr("href"));
          var dropdown = _this_a.parents(".dropdown").eq(0);
          dropdown.trigger("click");
          return false;
        });

        $("#aa123").click(function(e){
           e.stopPropagation();
           console.log(222222);
        });


        //end 小例子

        NProgress.done();

      });
      //end window.onload


    });


  });
  //bootstrap css end


});
//requirejs_config